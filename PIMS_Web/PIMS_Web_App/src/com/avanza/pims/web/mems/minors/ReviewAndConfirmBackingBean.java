package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.memsdisbursementlistbpel.proxy.MEMSDisbursementListBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.DisburseSrc;
import com.avanza.pims.entity.DisbursementDetails;
import com.avanza.pims.entity.DisbursementListType;
import com.avanza.pims.entity.PeriodicDisbursementList;
import com.avanza.pims.entity.Request;
import com.avanza.pims.exporter.ListBasedExporterMeta;
import com.avanza.pims.exporter.reportdetail.PeriodicListAddedDeletedReport;
import com.avanza.pims.exporter.reportdetail.ReportDetail;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.MinorTransformUtil;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.PeriodicDisbursementDetailsView;
import com.avanza.ui.util.ResourceUtil;
@SuppressWarnings( "unchecked" )
public class ReviewAndConfirmBackingBean extends AbstractSearchBean 
{
	private static final long serialVersionUID = 7781522377600902324L;
	private static final String COMPLETE_CLICKED =  "COMPLETE_CLICKED";
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	private final String TASK_TYPE = "TASK_TYPE";
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_REJECTED = "PAGE_MODE_REJECTED";
	private static final String PAGE_MODE_EXECUTE = "PAGE_MODE_EXECUTE";
	private static final String PAGE_MODE_DRAFT_LIST = "PAGE_MODE_DRAFT_LIST";
	
	private String pageMode = PAGE_MODE_VIEW ;
	private transient Logger logger = Logger.getLogger(ReviewAndConfirmBackingBean.class);
	DecimalFormat oneDForm = new DecimalFormat("#.00");
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable dataTable;

	private String requestId = null;
	private String hdnListMonth ="";
	private String hdnListYear = "";
	private HtmlInputText htmlAssetNumber = new HtmlInputText();
	private HtmlInputText htmlAssetNameEn = new HtmlInputText();
	private HtmlInputText htmlHiddenAccount = new HtmlInputText();

	private HtmlInputText htmlAssetDesc = new HtmlInputText();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlBeneficiary = new HtmlInputText();
	private HtmlInputText htmlFilePersonName = new HtmlInputText();
	private HtmlInputText htmlAmount = new HtmlInputText();
    private HtmlCommandButton btnDisburse = new HtmlCommandButton();
    private HtmlCommandButton btnSelectAll = new HtmlCommandButton();
    private HtmlCommandButton btnAddGrpSuppliers = new HtmlCommandButton();
	private HtmlInputText htmlAssetNameAr = new HtmlInputText();

	private HtmlSelectOneMenu disbursementTypeSelectMenu = new HtmlSelectOneMenu();

	private String disbursementSourceSelectMenu = new String();

	private HtmlSelectOneMenu recSourceSelectMenu = new HtmlSelectOneMenu();

	private HtmlSelectOneMenu disbursementListSelectMenu = new HtmlSelectOneMenu();

	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	
	
	private HtmlInputText htmlToBeneficiary = new HtmlInputText();
	
	private String totalAmount="";
	HashMap searchDisbursementMap = new HashMap();
	private String txtRejectionReason;
	private List<String> successMessages;
	private List<PeriodicDisbursementDetailsView> dataList = new ArrayList<PeriodicDisbursementDetailsView>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	List<SelectItem> recSourceList = new ArrayList<SelectItem>();
	List<SelectItem> disbursementSource = new ArrayList<SelectItem>();
	List<SelectItem> disbursementList = new ArrayList<SelectItem>();
	
	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	

	private RequestView requestView = new RequestView();
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	private Boolean markUnMarkAll;
	

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputText getHtmlAssetNumber() {
		return htmlAssetNumber;
	}

	public void setHtmlAssetNumber(HtmlInputText htmlAssetNumber) {
		this.htmlAssetNumber = htmlAssetNumber;
	}

	public HtmlInputText getHtmlAssetNameNameEn() {
		return htmlAssetNameEn;
	}

	public void setHtmlAssetNameNameEn(HtmlInputText htmlAssetNameNameEn) {
		this.htmlAssetNameEn = htmlAssetNameNameEn;
	}

	public HtmlInputText getHtmlAssetDesc() {
		return htmlAssetDesc;
	}

	public void setHtmlAssetDesc(HtmlInputText htmlAssetDesc) {
		this.htmlAssetDesc = htmlAssetDesc;
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlBeneficiary() {
		return htmlBeneficiary;
	}

	public void setHtmlBeneficiary(HtmlInputText htmlBeneficiary) {
		this.htmlBeneficiary = htmlBeneficiary;
	}

	public HtmlInputText getHtmlFilePersonName() {
		return htmlFilePersonName;
	}

	public void setHtmlFilePersonName(HtmlInputText htmlFilePersonName) {
		this.htmlFilePersonName = htmlFilePersonName;
	}

	public HtmlInputText getHtmlAssetNameAr() {
		return htmlAssetNameAr;
	}

	public void setHtmlAssetNameAr(HtmlInputText htmlAssetNameAr) {
		this.htmlAssetNameAr = htmlAssetNameAr;
	}

		public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}

	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		try 
		{
			sessionMap = context.getExternalContext().getSessionMap();
			if (!isPostBack())
			{
				setRowsPerPage(100);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField("costCenter");
				setSortItemListAscending(false);
				setTotalAmount("0.0");
				
				loadCombos(); 
				checkTask();
				viewRootMap.put("canAddAttachment", true); 
			    viewRootMap.put("canAddNote", true);
				viewRootMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_REQUEST);
			}

		} 
		catch (Exception es) 
		{
    		logger.LogException("init|Error Occured", es);
    		errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void loadCombos()throws Exception
	{
		loadDisbursementSourceList();
		
		//loadMonthList
	}
	@Override
	public void prerender() 
	{
		try
		{
			super.prerender();
			
			if ( !hasAllRowsDisbursed() && getPageMode().equals(PAGE_MODE_EXECUTE))
			{
			  btnDisburse.setRendered(true);
			  btnAddGrpSuppliers.setRendered(true);
			  btnSelectAll.setRendered(true);
			}
			else
			{
			  btnDisburse.setRendered(false);
			  btnAddGrpSuppliers.setRendered(false);
			  btnSelectAll.setRendered(false);
			}
//			double amount = 0;
//			for (PeriodicDisbursementDetailsView item : getDataList() ) 
//			{
//				amount += item.getAmount();
//				
//			}
//			totalAmount = String.valueOf( amount );
		} 
		catch (Exception es) 
		{
			
			logger.LogException("prerender|Error Occured", es);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
	
		}
	}
	private boolean hasAllRowsDisbursed() throws Exception
	{
		for (PeriodicDisbursementDetailsView item : getDataList() ) 
		{
			if( item.getIsDisbursed() == null  || item.getIsDisbursed().longValue() == 0)
			{
				return false;
			}
		}
		
		return true;
	}
	
	public List<SelectItem> loadDisbursementSourceList() throws Exception
	{
		HashMap<Long, String> disbursementSourceList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();
		disbursementSourceList = service.getDisbursementSourceList(CommonUtil.getIsEnglishLocale());
		Set s = disbursementSourceList.entrySet();
		Iterator it = s.iterator();
		while (it.hasNext()) 
		{
			Map.Entry m = (Map.Entry) it.next();
			Long key = (Long) m.getKey();
			String value = (String) m.getValue();
			selectItems.add(new SelectItem(key.toString(), value));

		}

		this.setDisbursementSource(selectItems);

		return selectItems;

	}



   @SuppressWarnings("unchecked")
	public void tabAttachmentsComments_Click() 
	{

		try 
		{
			viewRootMap.put("canAddNote", true);
			loadAttachmentsAndComments(this.getRequestView().getRequestId());
		} 
		catch (Exception ex) 
		{
			logger.LogException("tabAttachmentsComments_Click| Exception Occured...", ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings("unchecked")
	public void tabRequestHistoryClick()
	{
    	
    	try	
    	{
    	
    	  if(this.getRequestView().getRequestId() != null )
    	  {
    		  RequestHistoryController rhc=new RequestHistoryController();
    		  rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,this.getRequestView().getRequestId().toString());
    	  }
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "tabRequestHistoryClick|Error Occured..",ex);
    		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    	}
	}

	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId)throws Exception
	{

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		//viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);

		if (requestId != null) {
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	private void saveCommensAttachment(String eventDesc) throws Exception 
	{
		Long requestId =null;
		if(viewMap.get(WebConstants.REQUEST_ID) != null)
		{
			 requestId =new Long( viewMap.get(WebConstants.REQUEST_ID).toString());
		     saveComments( requestId );
			 saveAttachments( requestId.toString() );
			 if(eventDesc!= null && eventDesc.length() > 0)
			 {
				 CommonUtil.saveSystemComments(WebConstants.REQUEST, eventDesc, requestId);
			 }
		}
	}
	public Boolean saveComments(Long referenceId) throws Exception
    {
		Boolean success = false;
	    String notesOwner = WebConstants.REQUEST;
	    NotesController.saveNotes(notesOwner, referenceId);
	    if(txtRejectionReason!=null && this.txtRejectionReason.length()>0)
    	{
    	  CommonUtil.saveRemarksAsComments(referenceId , txtRejectionReason, notesOwner) ;
    	}
	    success = true;
    	return success;
    }
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
	    if(referenceId!=null)
	    {
		 success = CommonUtil.updateDocuments();
	    }
    	return success;
    }

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}
	public boolean getPageModeRejected() 
	{
		
		return getPageMode().equals(PAGE_MODE_REJECTED);

	}
	public boolean getPageModeDraftList() 
	{
		
		return getPageMode().equals(PAGE_MODE_DRAFT_LIST);

	}
	public boolean getPageModeView() 
	{
		
		return getPageMode().equals(	PAGE_MODE_VIEW );

	}
	public boolean getPageModeDisbursementRequired() 
	{
		
		return getPageMode().equals(PAGE_MODE_EXECUTE);

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}

	public List<SelectItem> getDisbursementTypesList() {

		HashMap<Long, String> disbursementTypeList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();

		try {
			disbursementTypeList = service.getDisbursementTypeList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementTypeList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}

		} catch (PimsBusinessException e) {
			logger.LogException("getDisbursementTypesList() crashed", e);
		} catch (Exception e) {
			logger.LogException("getDisbursementTypesList() crashed", e);
		}
		return selectItems;
	}

	public List<SelectItem> getDisbursementSourceList() {

		HashMap<Long, String> disbursementSourceList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();

		try {
			disbursementSourceList = service.getDisbursementSourceList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementSourceList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}

		}
		catch (Exception e) 
		{
			logger.LogException("getDisbursementTypesList() crashed", e);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return selectItems;
	}

	public void doSearch() 
	{
		try 
		{

			loadDataList();
		} catch (Exception e) 
		{
			logger.LogException("doSearch|Error Occured:", e);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
    @SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) 
    {
    
	    try
	    {
	    	
			PeriodicDisbursementDetailsView gridViewRow = (PeriodicDisbursementDetailsView) dataTable.getRowData();
			String javaScriptText = " var screen_width = 970;"
					+ "var screen_height = 550;"
					+ "var popup_width = screen_width;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
					+ WebConstants.PERSON_ID
					+ "="
					+ gridViewRow.getBeneficiaryId()
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
	
			executeJavaScript(javaScriptText);
	
		} 
	    catch (Exception es) 
		{
			
			logger.LogException("init|Error Occured", es);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

    public void onSelectAll()
    {           
    	try
    	{
    		markUnMarkAll=true;
    		onMarkUnMarkChanged();
    	} 
    	catch(Exception ex) 
    	{
		  logger.LogException("[Exception occured in onSelectAll()]", ex);
		  errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    }

    public void onSearch()
    {
    	try
		{
			pageFirst();
		} 
    	catch(Exception ex) 
    	{
		  logger.LogException("[Exception occured in onSearch()]", ex);
		  errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    }

    @SuppressWarnings( "unchecked" )
	protected void loadDisbursementList( List<DisbursementDetails> disbursementDetailList ) throws Exception 
	{
		if(disbursementDetailList.size()<=0)
		{return;}
		DisbursementDetails singleRow = disbursementDetailList.get(0);
		PeriodicDisbursementList periodicList = singleRow.getPeriodicDisbursementList();
		DisbursementListType disbursementListType = periodicList.getDisbursementListType();
		
		List<SelectItem> disbursementList = new ArrayList<SelectItem>();
		disbursementList.add(		
								new SelectItem(
												disbursementListType.getTypeId().toString(), 
												disbursementListType.getDescription()
											  )
							);
		this.setDisbursementList(disbursementList);
		
		DisburseSrc disbSource = singleRow.getDisburseSrc();
		if (disbSource != null) 
		{
			List<SelectItem> disbursementSource = new ArrayList<SelectItem>();
			if( CommonUtil.getIsEnglishLocale())						
				disbursementSource.add(new SelectItem(disbSource.getDisSrcId().toString(), disbSource.getDisSrcNameEn()));
			else
				disbursementSource.add(new SelectItem(disbSource.getDisSrcId().toString(), disbSource.getDisSrcNameAr()));
			this.setDisbursementSource(disbursementSource);
			this.setDisbursementSourceSelectMenu(disbSource.getDisSrcId().toString());
		}
	}
    
    public void doSearchItemList()
	{
		try {
				setTotalAmount("0.0");
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}
    @SuppressWarnings( "unchecked" )
	public void loadDataList() 
	{
    	try 
		{
//    		if (dataList != null) dataList.clear();
    		
    		boolean fetchBalance=true;
			Map<String, Object> searchCriteriaMap = getSearchCriteria();
			DisbursementService service = new DisbursementService();
			requestView  = getRequestView();
			int totalRows = 0;
			totalRows = service.getTotalNumberOfPeriodicRecords( requestView.getRequestId(),searchCriteriaMap );
			setTotalRows(totalRows);
			doPagingComputations();
			setTotalAmount( 
							oneDForm.format( 
											 service.getSumOfPeriodicRecords( requestView.getRequestId(), searchCriteriaMap ) 
										   ) 
						  );
	    	List<DisbursementDetails> disbursementDetailList = service.getDisbursementListByRequestId(
	    																								requestView.getRequestId(),
	    																								searchCriteriaMap,
	    																								getRowsPerPage(),
	    																								getCurrentPage(),
	    																								getSortField(),
	    																								isSortItemListAscending()
	    																							  );
	    	loadDisbursementList( disbursementDetailList );
			if (disbursementDetailList.isEmpty() || disbursementDetailList == null) 
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
				forPaging(0);
			}
			if( markUnMarkAll == null ){markUnMarkAll =false;}
			this.setDataList( 
								MinorTransformUtil.tranformToPeriodicDisbursementDetailsView( disbursementDetailList,searchCriteriaMap, fetchBalance,markUnMarkAll )	
							);
			forPaging(getTotalRows());
		}
		catch (Exception ex) 
		{
			logger.LogException( "loadDataList|Error Occured:", ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
    
	@SuppressWarnings("unchecked")
	private Map<String, Object> getSearchCriteria() throws Exception
	{
		Map<String, Object> searchDisbursementMap = new HashMap<String, Object>();
		boolean isSearchCriteriaProvided = false;
		Object beneficiary = htmlBeneficiary.getValue();
		Object toBeneficiary = htmlToBeneficiary.getValue();
		Object toAccountNum = htmlAssetNumber.getValue();
		Object toBank = htmlAssetNameAr.getValue();
		Object costCenter = htmlAssetDesc.getValue();
		Object paymentMethod = disbursementTypeSelectMenu.getValue();
		
		if( paymentMethod  !=null && !paymentMethod .toString().equals("-1"))
        {
			isSearchCriteriaProvided = true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.PAYMENT_METHOD,paymentMethod.toString());
        }
		if( toBank !=null && toBank.toString().trim().length() > 0  )
        {
			isSearchCriteriaProvided = true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_BANK ,toBank.toString());
        }
        if( toAccountNum !=null && toAccountNum.toString().trim().length() > 0  )
        {
        	isSearchCriteriaProvided = true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_ACC_NUM,toAccountNum.toString());
        }
        if( toBeneficiary !=null && toBeneficiary.toString().trim().length() > 0  )
        {
        	isSearchCriteriaProvided = true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_BENEFICIARY_NAME,toBeneficiary.toString());
        }
        if( beneficiary !=null && beneficiary.toString().trim().length() > 0  )
        {
        	isSearchCriteriaProvided =true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.BENEFICIARY_NAME,beneficiary.toString());
        }
        if( costCenter!=null && costCenter.toString().trim().length() > 0  )
        {
        	isSearchCriteriaProvided =true;
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.COST_CENTER,costCenter.toString());
        }

        return searchDisbursementMap;
	}

	private List<DisbursementDetails>  getAllDisbursements() throws Exception
	{
		DisbursementService service = new DisbursementService();
		Map<String, Object> searchCriteriaMap = getSearchCriteria();
		requestView  = getRequestView();
		List<DisbursementDetails> disbursementDetailList = service.getDisbursementListByRequestId(
																									requestView.getRequestId(),
																									searchCriteriaMap,
																									null,
																									null,
																									null,
																									null
																								  );
		return disbursementDetailList;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	

	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getFileStatusList() {
		if (viewRootMap.get("fileStatusList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileStatusList");
		return new ArrayList<SelectItem>();
	}

	public List<PeriodicDisbursementDetailsView> getDataList()  
	{

		dataList = (List<PeriodicDisbursementDetailsView>) viewRootMap.get(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_GRID_VIEW);
		if (dataList == null)
			dataList = new ArrayList<PeriodicDisbursementDetailsView>();
		return dataList;
	}

	public void setDataList(List<PeriodicDisbursementDetailsView> dataList) 
	{
		this.dataList = dataList;
		if (this.dataList != null)
			viewRootMap.put(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_GRID_VIEW,this.dataList);
	}

	public List<PeriodicDisbursementDetailsView> getSelectedRows() throws Exception
	{
		List<PeriodicDisbursementDetailsView> selectedRowsList = new ArrayList<PeriodicDisbursementDetailsView>(0);
		dataList = getDataList();
		getDisbursementSourceSelectMenu();
		if (dataList != null && dataList.size() > 0) 
		{
			DisburseSrc disSrc = null;
			if( 
				disbursementSourceSelectMenu != null && 
				!disbursementSourceSelectMenu.equals("0") &&
				!disbursementSourceSelectMenu.equals("-1")
			  )
			{
				disSrc = new DisburseSrc();
				disSrc.setDisSrcId( 
										Long.valueOf( disbursementSourceSelectMenu) 
								   );
			}
			for (PeriodicDisbursementDetailsView disbursementDetailsView : dataList)
			{
				if (disbursementDetailsView.getSelected()  && disbursementDetailsView.getPaymentDetailFormView() != null) 
				{
					disbursementDetailsView.setDisburseSrc(disSrc);
					selectedRowsList.add(disbursementDetailsView);
				}
		    }
		 }
		 return selectedRowsList;
	}

	public void obtainChildParams() 
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>();
		try
		{
			if (!sessionMap.containsKey(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW)){return;}
	
			PeriodicDisbursementDetailsView parentRow = new PeriodicDisbursementDetailsView();
			parentRow = (PeriodicDisbursementDetailsView) sessionMap.remove(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW);
			
			for (PeriodicDisbursementDetailsView row : this.getDataList()) 
			{
				if (parentRow.getDisDetId().longValue() == row.getDisDetId()) 
				{
					
					PaymentCriteriaView criteriaView  = parentRow.getPaymentDetailFormView();
					row.setPaymentDetailFormView(	criteriaView	);
					
					DisbursementService.modifyPaymentDetails( parentRow.getDisDetId(),
															  criteriaView,
															  getLoggedInUserId()
															);
					
					break;
				}
			}
			
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
		} 
		catch (Exception ex) 
		{
			logger.LogException( "obtainChildParams|Error Occured:", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public List<PeriodicDisbursementDetailsView> hasOnDisburseErrors(boolean isFinance) throws Exception 
	{
			boolean hasErrors = false;
			boolean hasSelected = false;
			boolean paymentDetailsMissing =false;
			boolean hasInSufficientAccBalance =false;
			String inSufficientAccBalanceBeneficiary="";
			String inSufficientAccBalanceBlockingBeneficiary="";
			String benefNames="";
			List<PeriodicDisbursementDetailsView> selectedRowsList = new ArrayList<PeriodicDisbursementDetailsView>(0);
			dataList = getDataList();
			getDisbursementSourceSelectMenu();
			boolean isFromSelfAccount =  this.disbursementSourceSelectMenu != null && 
									     this.disbursementSourceSelectMenu.equals("0");
					
			for (PeriodicDisbursementDetailsView gridView : dataList) 
			{
		    			if ( 
		    					gridView.getIsDisbursed().compareTo(0L) == 1  || 
		    					gridView.getSelected() == null || 
		    				   !gridView.getSelected() 
		    			   ){continue;}
		    		
						hasSelected = true;
						if(gridView.getCostCenter() == null || gridView.getCostCenter().trim().length()<=0 )
						{
							
							String msg = java.text.MessageFormat.format(
																		 CommonUtil.getBundleMessage(  "disburseFinance.msg.associateCostCenter"  ), 
																		 gridView.getBeneficiary() 
																	   );
							errorMessages.add(msg);
							return null;
							
						}
						if( isFinance && gridView.getPaymentDetailFormView() == null  )
						{
							paymentDetailsMissing = true;
							benefNames +=gridView.getBeneficiary()+",";
						}
						if(isFromSelfAccount)
						
						{
							String msg = CommonUtil.getBundleMessage(  "mems.payment.err.msg.amntGreaterThanBalanceBlockingWithName"  );

							int validateBalanceCode = validateBalance(  
																		gridView.getAccountBalance() ,
																		gridView.getBlockingBalance(),
																		gridView.getRequestedBalance(),
																		gridView.getAmount(),
																		gridView.getBeneficiaryId(),
																		gridView.getBeneficiary(),
																		gridView.getInheritanceFileId()
																	  ) ;
								if ( validateBalanceCode > 0 )
								{
									inSufficientAccBalanceBlockingBeneficiary ="1";
								}
						}
							
						
						if ( hasSelected && 
							 !paymentDetailsMissing && 
							 !hasInSufficientAccBalance &&
							 
							 (
							    !isFromSelfAccount ||
							   inSufficientAccBalanceBlockingBeneficiary == null ||
							   inSufficientAccBalanceBlockingBeneficiary.trim().length() <= 0 
							 )
						   )
						{
							DisburseSrc disSrc = null;
							if( disbursementSourceSelectMenu != null )
							{
								disSrc = new DisburseSrc();
								disSrc.setDisSrcId( 
														Long.valueOf( disbursementSourceSelectMenu ) 
												   );
							}
							gridView.setDisburseSrc(disSrc);
							selectedRowsList.add(gridView);
						}
			}
		
		if ( !hasSelected || dataList.size() <= 0 || dataList == null ) 
		{
			hasErrors = true;
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));
		} 
		else if( paymentDetailsMissing )
		{
			hasErrors = true;
			benefNames = benefNames.substring(0,benefNames.length()-1);
			 
			String msg = java.text.MessageFormat.format( CommonUtil.getBundleMessage("reviewConfirmDisbursement.messages.paymentDetailsrequired"),
														benefNames
									                   );
			errorMessages.add(msg);
			
		}
		else if (hasInSufficientAccBalance) 
		{
			hasErrors = true;
			inSufficientAccBalanceBeneficiary = inSufficientAccBalanceBeneficiary.substring(0,inSufficientAccBalanceBeneficiary.length()-1);
			String msg = java.text.MessageFormat.format( CommonUtil.getBundleMessage("reviewConfirmDisbursement.messages.balanceNotEnough"),
														inSufficientAccBalanceBeneficiary
									                   );
			errorMessages.add(msg);
		}
		if( inSufficientAccBalanceBlockingBeneficiary!= null && inSufficientAccBalanceBlockingBeneficiary.trim().length() > 0 )
		{
			hasErrors = true;
		}
		if( hasErrors )
		{
			selectedRowsList = new ArrayList<PeriodicDisbursementDetailsView>();
		}
		return selectedRowsList;
	}

	public void btnDisburse_Click() 
	{
		successMessages= new ArrayList<String>();
		errorMessages= new ArrayList<String>();
		try 
		{
			 getDisbursementSourceSelectMenu();
			 List<PeriodicDisbursementDetailsView> selectedRows = hasOnDisburseErrors( true ) ;
		  	 if ( selectedRows==null || selectedRows.size() <= 0 ){return;}
			  
			  DisbursementService service = new DisbursementService();
			  Long returnVal = service.disbursePeriodicPayments(selectedRows,getLoggedInUserId());
			  saveCommensAttachment( "" );
			  if (	returnVal != null	) 
			  {
				successMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.DISB_SUCCESS));
				for (PeriodicDisbursementDetailsView obj : selectedRows) 
				{
				  obj.setIsDisbursed(new Long(1));
				  obj.setSelected(false);
				}
			  }
			else
			{
				  errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.GENERAL_DISB_ERROR));
			}
		} 
		catch (Exception ex) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("btnDisburse_Click Error Occured...", ex);
		}

	}

	public Boolean hasError() throws Exception
	{
		boolean hasErrors = false;
		boolean containsUnDisbursed = false;
		dataList = getDataList();
		for (PeriodicDisbursementDetailsView item : dataList) {
			
			
			if (item.getIsDisbursed().compareTo(new Long(0)) == 0) {
				containsUnDisbursed = true;
			}
		}

		if (containsUnDisbursed == true) 
		{
			hasErrors = true;
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.COMPLETE_OPERATION_ERROR));
		}

		return hasErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{
			Long requestId = -1l;
			PeriodicDisbursementDetailsView  gridRow = ( PeriodicDisbursementDetailsView  )dataTable.getRowData();
			if( getRequestView().getRequestId() != null )
			{
				requestId  = getRequestView().getRequestId();
			}

			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+gridRow.getBeneficiaryId()  +"','"+
								  														 gridRow.getInheritanceFileId()+"','"+ 
								  														 requestId+
								  												 "');"
							 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			PeriodicDisbursementDetailsView  gridRow = ( PeriodicDisbursementDetailsView )dataTable.getRowData(); 
				
			executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+gridRow.getBeneficiaryId() +"','"+gridRow.getInheritanceFileId()+"');"
							  );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onOpenAddedDeletedTotalBeneficiaryExcel() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{
			requestView = getRequestView();
			ListBasedExporterMeta exporterMeta = new ListBasedExporterMeta();
			ReportDetail reportDetail = new PeriodicListAddedDeletedReport(
																			requestView, 
																			requestView.getDisbursementListTypeId().toString() 
																		   );


			exporterMeta.setExportFileName("Beneficiaries");
			exporterMeta.setReportDetail(reportDetail);
			
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_EXPORTER_META, exporterMeta);
			executeJavaScript("openLoadReportPopup('" + ReportConstant.EXPORT_LIST_BASED_EXCEL_REPORT + "');");
		}
		catch (Exception exception) 
		{
			logger.LogException( "onOpenAddedDeletedTotalBeneficiaryExcel--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	@SuppressWarnings( "unchecked" )
	public void onMarkUnMarkSingle( )throws Exception 
	{
		
		try 
		{
			setMarkUnMarkAll(false); 
		} catch (Exception ex) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onMarkUnMarkSingle| Error Occured...", ex);
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onMarkUnMarkChanged( )throws Exception 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
            dataList = getDataList();
            for (PeriodicDisbursementDetailsView obj : dataList) 
            {
            	if( obj.getIsDisbursed().compareTo(0L) == 0 )
            	{
            		obj.setSelected(markUnMarkAll);
            	}
				
			}

		} catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onMarkUnMarkChanged| Error Occured...", ex);
		}

	}

	@SuppressWarnings("unchecked")
	public void onAddGRPSuppliers() 
	{
		errorMessages = new ArrayList<String>();
		try
		{
			
				List<Long> ids = new ArrayList<Long>();
				if( !getMarkUnMarkAll() )
				{
					for(
						PeriodicDisbursementDetailsView row   :getDataList() 
					   )
					{
						
						if( getMarkUnMarkAll() && row.getIsDisbursed().compareTo(0L) == 1 ) {continue;}
						else if ( row.getIsDisbursed().compareTo(0L) == 1  || row.getSelected() == null || !row.getSelected()){continue;}
						
						ids.add(row.getDisDetId());
					}
				}
				else if( getMarkUnMarkAll() )
				{

					for(
						DisbursementDetails row   :getAllDisbursements()
					   )
					{
						
						if( getMarkUnMarkAll() && row.getIsDisbursed().compareTo(0L) == 1 ) {continue;}
						ids.add(row.getDisDetId());
					}
				}
				if( ids.size() <= 0  )
				{
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));
					return;
				}
				else
				{
					sessionMap.put( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS,ids); 
					String javaScriptText = " javaScript:openAddGrpSuppliersPopup( );";				
					executeJavaScript(javaScriptText);
				}
		}
		catch(Exception e)
		{
			logger.LogException( "onAddGRPSuppliers", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
	}	
	
	
	@SuppressWarnings( "unchecked" )
	public boolean hasSendToFinanceError( )throws Exception 
	{
		boolean hasError =  false;
		if(
			this.disbursementSourceSelectMenu == null || 
			this.disbursementSourceSelectMenu.equals("-1")
		  ) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("PeriodicDisbursements.msg.disburseSrcRequired"));
			hasError = true;
		}
		if( this.requestView.getPeriodicMonth() == null || this.requestView.getPeriodicMonth().length() <= 0 || this.requestView.getPeriodicMonth().equals("-1") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("reviewConfirm.msg.InvalidMonth"));
			hasError = true;
			
		}
		else if( this.requestView.getPeriodicYear()== null || this.requestView.getPeriodicYear().length() <= 0 || this.requestView.getPeriodicYear().equals("-1") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("reviewConfirm.msg.InvalidYear"));
			hasError = true;
			
		}
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onSendToFinance( )throws Exception 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try 
		{
			getDisbursementSourceSelectMenu();
			if(  hasSendToFinanceError() )return;
			List<PeriodicDisbursementDetailsView> selectedRows = hasOnDisburseErrors( true ) ;
			if ( selectedRows==null || selectedRows.size() <= 0 ){return;}
			
			
			updateRequestStatus( getPageModeDraftList() );
			successMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.FINANCE_SUCCESS_MESSAGE));
			setPageMode(PAGE_MODE_VIEW);
			saveCommensAttachment(MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT);

		} catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onSendToFinance| Error Occured...", ex);
		}

	}


	
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteItem( )throws Exception 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			DisbursementService ds = new DisbursementService();
			
			PeriodicDisbursementDetailsView row = ( PeriodicDisbursementDetailsView )dataTable.getRowData();
			row.setUpdatedBy(getLoggedInUserId());
			dataList = getDataList();
			if( dataList.size() <= 1 )
			{
				requestView = getRequestView();
				requestView.setUpdatedBy(getLoggedInUserId());
				ds.cancelPeriodicDraftList(requestView);
				successMessages.add(	CommonUtil.getBundleMessage("periodicDisbursement.msg.requestCancelled")	);
				setPageMode(PAGE_MODE_VIEW);
			}
			else
			{
				ds.deletePeriodicDisbursementFromList( row );
				dataList.remove(row);
				successMessages.add(CommonUtil.getBundleMessage("periodicDisbursement.msg.itemDeleted"));
				
			}
			
		} 
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onDeleteItem| Error Occured...", ex);
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onCancelRequest( )throws Exception 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			requestView = getRequestView();
			requestView.setUpdatedBy(getLoggedInUserId());
			DisbursementService ds = new DisbursementService();
			ds.cancelPeriodicDraftList(requestView);
			if( getPageModeRejected() )
			{
				completeTask(TaskOutcome.CANCEL);
			}
			successMessages.add(	CommonUtil.getBundleMessage("periodicDisbursement.msg.requestCancelled")	);
			saveCommensAttachment(MessageConstants.RequestEvents.REQUEST_CANCELED );
			setPageMode(PAGE_MODE_VIEW);

		} catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onCancelRequest| Error Occured...", ex);
		}

	}

	@SuppressWarnings( "unchecked" )
	private void updateRequestStatus( boolean invokeBPEL ) throws Exception 
	{
		String userId = CommonUtil.getLoggedInUser();
		requestView = getRequestView();
		requestView.setUpdatedBy( userId);
		
		if( 
			this.requestView.getPeriodicMonth() != null && 
			this.requestView.getPeriodicMonth().trim().length() >0 && 
		   !this.requestView.getPeriodicMonth().equals("-1") &&
		    this.requestView.getPeriodicYear() != null && 
			this.requestView.getPeriodicYear().trim().length() >0 && 
		   !this.requestView.getPeriodicYear().equals("-1") 	
		  )
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			requestView.setCirculationDate( df.parse( "01/"+
													  String.valueOf( Integer.parseInt(  this.requestView.getPeriodicMonth() ) )+"/"+
													  String.valueOf( Integer.parseInt(  this.requestView.getPeriodicYear() )  )
													)
										   );
		}
		String selectedSource = disbursementSourceSelectMenu != null ?disbursementSourceSelectMenu:null;
		new DisbursementService().sendPeriodicDisbursementToFinance( requestView,getDataList(),selectedSource);
		
		if ( invokeBPEL )
		{
			MEMSDisbursementListBPELPortClient bpelPortClient= new MEMSDisbursementListBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.PeriodicDisbursement.BPEL_END_POINT);
			bpelPortClient.setEndpoint(endPoint);
			bpelPortClient.initiate(requestView.getRequestId(), userId, null, null);
		}
		else
		{
			completeTask(TaskOutcome.OK);
		}

	}

	private boolean hasRejectFromFinanceErrors() throws Exception
	{
		if( txtRejectionReason == null || txtRejectionReason.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( "socialResearchRecommendation.msg.rejectionReasonRequired" )  );
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejectFromFinance( ) 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			
			
			if (hasRejectFromFinanceErrors() ){ return; }
			RequestView rv = getRequestView();
			long requestId =Long.valueOf( viewMap.get( WebConstants.REQUEST_ID ).toString() );
			new  RequestService().setRequestStatus( requestId,WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT, CommonUtil.getLoggedInUser()	);
			completeTask(TaskOutcome.REJECT);
			rv.setStatusDataValue( WebConstants.REQUEST_STATUS_REJECTED);
			setPageMode(PAGE_MODE_VIEW);
			saveCommensAttachment(MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE);
			successMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.COMPLETE_OPERATION_SUCCESS));
		}
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onRejectFromFinance Error Occured...", ex);
		}

	}

    @SuppressWarnings( "unchecked" )
	public void btnComplete_Click()
    {
		successMessages = new ArrayList<String>();
		try 
		{
			dataList = getDataList();
			if (hasError()) 
			{
				return;
			}
			RequestView rv = getRequestView();
			long requestId =Long.valueOf( viewMap.get( WebConstants.REQUEST_ID ).toString() );
			new  RequestService().setRequestAsCompleted(	requestId, CommonUtil.getLoggedInUser()	);
			completeTask(TaskOutcome.OK);
			rv.setStatusDataValue( WebConstants.REQUEST_STATUS_COMPLETE );
			viewMap.put(COMPLETE_CLICKED,true);
			saveCommensAttachment(MessageConstants.RequestEvents.REQUEST_COMPLETED);
			successMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.COMPLETE_OPERATION_SUCCESS));
		}
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("btnComplete_Click Error Occured...", ex);
		}

	}
	public Boolean getShowCompleteButton()
	{
		if( getPageMode().equals(PAGE_MODE_DRAFT_LIST) || getPageMode().equals(PAGE_MODE_REJECTED) )
			return false;
		if ( viewMap.get(WebConstants.REQUEST_VIEW) != null )
		{
			RequestView rv = (RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
			if ( rv.getStatusDataValue().compareTo(WebConstants.REQUEST_STATUS_COMPLETE) == 0  ) 
			{
			 return false;
			}
			else
			{
				return true;
			}
		}
		else 
			return false;
	}
	public Boolean getIsVisible()
	{
		Boolean renderComplete = true;
		if (viewMap.get("COMPLETE_CLICKED")!=null)
		{
			renderComplete=false;
			
		}
		return renderComplete;
		
	}
	

	public void openPaymentsPopUp(javax.faces.event.ActionEvent event) 
	{
		try
		{
			PeriodicDisbursementDetailsView disbDetView = (PeriodicDisbursementDetailsView) dataTable.getRowData();
	
			String javaScriptText = "openPaymentDetailsPopUp('" +WebConstants.PERSON_ID+ "="+ disbDetView.getBeneficiaryId()+ "')";
	 		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW,disbDetView);
			executeJavaScript(javaScriptText);
	
	
		} catch (Exception es) {
			
			logger.LogException("openPaymentsPopUp|Error Occured", es);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public HtmlSelectOneMenu getDisbursementTypeSelectMenu() {
		return disbursementTypeSelectMenu;
	}

	public void setDisbursementTypeSelectMenu(
			HtmlSelectOneMenu disbursementTypeSelectMenu) {
		this.disbursementTypeSelectMenu = disbursementTypeSelectMenu;
	}

	public HtmlInputText getHtmlAmount() {
		return htmlAmount;
	}

	public void setHtmlAmount(HtmlInputText htmlAmount) {
		this.htmlAmount = htmlAmount;
	}

	public void disbursementTypeChanged() {

	}

	public HtmlInputText getHtmlHiddenAccount() {

		return htmlHiddenAccount;

	}

	public void setHtmlHiddenAccount(HtmlInputText htmlHiddenAccount) {
		this.htmlHiddenAccount = htmlHiddenAccount;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public HtmlSelectOneMenu getDisbursementListSelectMenu() {
		return disbursementListSelectMenu;
	}

	public void setDisbursementListSelectMenu(
			HtmlSelectOneMenu disbursementListSelectMenu) {
		this.disbursementListSelectMenu = disbursementListSelectMenu;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<SelectItem> getDisbursementList() 
	{
		List<SelectItem> disbursementList = new ArrayList<SelectItem>();
		if (viewRootMap.containsKey(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_TYPE))
			disbursementList = (ArrayList<SelectItem>) viewRootMap.get(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_TYPE);
		return disbursementList;

	}

	public void setDisbursementList(List<SelectItem> disbursementList) {

		this.disbursementList = disbursementList;
		if (disbursementList != null) {
			viewMap.put(
					WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_TYPE,
					disbursementList);
		}

	}
	@SuppressWarnings("unchecked")
	public String getDisbursementSourceSelectMenu() 
	{
		if( viewMap.get("disbursementSourceSelectMenu" ) != null )
		{
			disbursementSourceSelectMenu = viewMap.get("disbursementSourceSelectMenu").toString();
		}
		return disbursementSourceSelectMenu;
	}

	@SuppressWarnings("unchecked")
	public void setDisbursementSourceSelectMenu(
			String disbursementSourceSelectMenu) {
		this.disbursementSourceSelectMenu = disbursementSourceSelectMenu;
		if(this.disbursementSourceSelectMenu != null)
		{
		 viewMap.put("disbursementSourceSelectMenu",this.disbursementSourceSelectMenu);
		}
	}

	public List<SelectItem> getDisbursementSource() {

		if (viewMap.containsKey(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_SOURCE))
		{
			return (ArrayList<SelectItem>) viewMap.get(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_SOURCE);
		}
		else
			return (ArrayList<SelectItem>) null;

	}

	public void setDisbursementSource(List<SelectItem> disbursementSource) {
		this.disbursementSource = disbursementSource;

		if (disbursementSource != null) 
		{
			viewMap.put(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_SOURCE,disbursementSource);
		}
	}

	public HtmlSelectOneMenu getRecSourceSelectMenu() {
		return recSourceSelectMenu;
	}

	public void setRecSourceSelectMenu(HtmlSelectOneMenu recSourceSelectMenu) {
		this.recSourceSelectMenu = recSourceSelectMenu;
	}

	public List<SelectItem> getRecSourceList() {

		if (viewRootMap
				.containsKey(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_REC_SOURCE))
			return (ArrayList<SelectItem>) viewRootMap
					.get(WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_REC_SOURCE);
		else
			return (ArrayList<SelectItem>) null;

	}

	public void setRecSourceList(List<SelectItem> recSourceList) {
		this.recSourceList = recSourceList;

		if (recSourceList != null) {
			viewRootMap
					.put(
							WebConstants.REVIEW_CONFIRM_DISB.LIST_DISBURSEMENT_REC_SOURCE,
							recSourceList);
		}
	}

	public RequestView getRequestView() {
		if (viewRootMap.get(WebConstants.REQUEST_VIEW) != null)
			requestView = (RequestView) viewRootMap.get(WebConstants.REQUEST_VIEW);
		return requestView;
	}

	public void setRequestView(RequestView requestView) 
	{
		this.requestView = requestView;
		if (this.requestView != null)
			viewRootMap.put(WebConstants.REQUEST_VIEW , requestView);
	}

	public String getSuccessMessages() 
	{

		String messageList = "";
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : successMessages) {
				messageList += message + " <br></br>";
			}

		}

		return messageList;

	}
	
	@SuppressWarnings("unchecked")
	public void checkTask()throws Exception
	{
		UserTask userTask = null;
		Long requestId = null;
		userTask = (UserTask) sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);;
		
		//from task list
		if(userTask!=null)
		{ 
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			viewMap.put(TASK_TYPE, userTask.getTaskType());
			Map taskAttributes =  userTask.getTaskAttributes();
			String taskType = userTask.getTaskType();
			if(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)!=null)
			{
				requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				getRequestByRequestId(requestId);
				if( taskType.equals( "ReviewAndDisburseByFinance" ) )
				{
					setPageMode(  PAGE_MODE_EXECUTE );
				}
				else if( taskType.equals( "ReassignSourceBySocialResearch" ) )
				{
					setPageMode(  PAGE_MODE_REJECTED ) ;
				}
			}
		}
		//from periodic search screen
		else if( sessionMap.get( "FROM_PRDC_LIST" ) != null )
		{
			Long disListTypeId = Long.valueOf( sessionMap.remove( "FROM_PRDC_LIST" ).toString() ); 
			DisbursementService service = new DisbursementService();
			Request request = service.getNewPeriodicListRequest( disListTypeId );
			requestId = request.getRequestId();
			getRequestByRequestId( requestId );
			setPageMode(  PAGE_MODE_DRAFT_LIST );
		}
		//from request search
		else if ( sessionMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) sessionMap.remove( WebConstants.REQUEST_VIEW );;
			
			setRequestView( requestView );
			requestId = requestView.getRequestId();
			if( requestView.getStatusDataValue().equals(WebConstants.REQUEST_STATUS_NEW)  
				 
			  )
			{
				setPageMode( PAGE_MODE_DRAFT_LIST );
			}
			else if( requestView.getStatusDataValue().equals(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) )
			{
				setPageMode(  PAGE_MODE_REJECTED );
			}
			else if( requestView.getStatusDataValue().equals(WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ) )
			{
				setPageMode( PAGE_MODE_EXECUTE );
			}
				
		}
		
		loadAttachmentsAndComments(requestId);
		if( requestId != null)
		{
		   viewMap.put(WebConstants.REQUEST_ID, requestId);
		   loadAttachmentsAndComments(requestId);
		   loadDataList();
		   
		 }
	 }

	private void getRequestByRequestId(Long requestId)throws PimsBusinessException 
	{
		RequestService reqSrv = new RequestService();
		requestView= reqSrv.getRequestById(requestId);
		setRequestView( requestView );
		viewMap.put(WebConstants.REQUEST_ID, requestId);
	}
	
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception 
	{
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
        String loggedInUser = CommonUtil.getLoggedInUser();
        if(viewMap.containsKey(TASK_LIST_USER_TASK))
        {
        	BPMWorklistClient client = new BPMWorklistClient(contextPath);
        	userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
			if(userTask!=null)
			{
				client.completeTask(userTask, loggedInUser, taskOutcome);
			}
        }
		return success;
	}

	public HtmlCommandButton getBtnDisburse() {
		return btnDisburse;
	}

	public void setBtnDisburse(HtmlCommandButton btnDisburse) {
		this.btnDisburse = btnDisburse;
	}

	@SuppressWarnings("unchecked")
	public String getPageMode() {
		if( viewMap.get("pageMode") != null )
		{
			this.pageMode = viewMap.get("pageMode").toString();
		}
		return pageMode;
	}

	@SuppressWarnings("unchecked")
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
		if(  this.pageMode != null )
		{
			viewMap.put("pageMode", this.pageMode );
		}
	}

	
	public String getHdnListMonth() {
		return hdnListMonth;
	}

	public void setHdnListMonth(String hdnListMonth) {
		this.hdnListMonth = hdnListMonth;
	}

	public String getHdnListYear() {
		return hdnListYear;
	}

	public void setHdnListYear(String hdnListYear) {
		this.hdnListYear = hdnListYear;
	}

	public Boolean getMarkUnMarkAll() {
		return markUnMarkAll;
	}

	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
	}

	public String getTxtRejectionReason() {
		return txtRejectionReason;
	}

	public void setTxtRejectionReason(String txtRejectionReason) {
		this.txtRejectionReason = txtRejectionReason;
	}

	public HtmlCommandButton getBtnAddGrpSuppliers() {
		return btnAddGrpSuppliers;
	}

	public void setBtnAddGrpSuppliers(HtmlCommandButton btnAddGrpSuppliers) {
		this.btnAddGrpSuppliers = btnAddGrpSuppliers;
	}

	@SuppressWarnings("unchecked")
	public String getTotalAmount() {
		if( viewMap.get("prdctotalAmount") != null )
		{
			totalAmount = viewMap.get("prdctotalAmount").toString();
		}
		return totalAmount;
	}

	@SuppressWarnings("unchecked")
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
		if(this.totalAmount != null)
		{
			viewMap.put("prdctotalAmount", this.totalAmount );
		}
		
	}

	public HtmlInputText getHtmlToBeneficiary() {
		return htmlToBeneficiary;
	}

	public void setHtmlToBeneficiary(HtmlInputText htmlToBeneficiary) {
		this.htmlToBeneficiary = htmlToBeneficiary;
	}

	public HtmlCommandButton getBtnSelectAll() {
		return btnSelectAll;
	}

	public void setBtnSelectAll(HtmlCommandButton btnSelectAll) {
		this.btnSelectAll = btnSelectAll;
	}
}
