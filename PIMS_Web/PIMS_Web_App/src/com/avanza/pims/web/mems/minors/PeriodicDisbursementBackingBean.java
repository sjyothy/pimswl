package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.memsdisbursementlistbpel.proxy.MEMSDisbursementListBPELPortClient;
import com.avanza.pims.bpel.reneworstopdisbursementbpelproxy.proxy.RenewOrStopDisbursementBPELPortClient;
import com.avanza.pims.entity.Request;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PeriodicDisbursementGridView;
import com.avanza.ui.util.ResourceUtil;


public class PeriodicDisbursementBackingBean extends AbstractMemsBean
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(PeriodicDisbursementBackingBean.class);

	private HtmlDataTable dataTable;

	private HtmlInputText htmlAssetNumber = new HtmlInputText();
	private HtmlInputText htmlAssetNameEn = new HtmlInputText();
	private HtmlInputText htmlHiddenAccount = new HtmlInputText();

	private HtmlInputText htmlAssetDesc = new HtmlInputText();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlBeneficiary = new HtmlInputText();
	private HtmlInputText htmlToBeneficiary = new HtmlInputText();
	private HtmlInputText htmlToBank = new HtmlInputText();
	private HtmlInputText htmlToAccountNum = new HtmlInputText();
	private HtmlInputText htmlCostCenter = new HtmlInputText();
	private HtmlInputText htmlFilePersonName = new HtmlInputText();
	private HtmlInputText htmlAmount = new HtmlInputText();

	private HtmlSelectOneMenu disbursementTypeSelectMenu = new HtmlSelectOneMenu();
	@SuppressWarnings( "unchecked" )
	private List<SelectItem> disbursementTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu disbursementSourceSelectMenu = new HtmlSelectOneMenu();
	@SuppressWarnings( "unchecked" )
	private List<SelectItem> disbursementSourceList = new ArrayList<SelectItem>();
	private List<SelectItem> periodicDisbursementStausList = new ArrayList<SelectItem>();
	
	private String amountFrom = "";
	private String amountTo = "";
	private String status = "";
	private String disbursementListCount = "0";
	
	private HtmlCalendar calEndTo = new HtmlCalendar();
	private HtmlCalendar calEndFrom = new HtmlCalendar();
	private HtmlCalendar calStartTo = new HtmlCalendar();
	private HtmlCalendar calStartFrom = new HtmlCalendar();
	private HtmlCalendar expectedMaturityDateFrom = new HtmlCalendar();
	private HtmlCalendar expectedMaturityDateTo = new HtmlCalendar();
	
	
	
	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	HashMap searchDisbursementMap = new HashMap();
	private List<PeriodicDisbursementGridView> dataList = new ArrayList<PeriodicDisbursementGridView>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
   
	private List<String> successMessages;

	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";

	private String DEFAULT_SORT_FIELD = "expMaturityDate";

	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;

	private List<String> errorMessages = new ArrayList<String>();

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputText getHtmlAssetNumber() {
		return htmlAssetNumber;
	}

	public void setHtmlAssetNumber(HtmlInputText htmlAssetNumber) {
		this.htmlAssetNumber = htmlAssetNumber;
	}

	public HtmlInputText getHtmlAssetNameNameEn() {
		return htmlAssetNameEn;
	}

	public void setHtmlAssetNameNameEn(HtmlInputText htmlAssetNameNameEn) {
		this.htmlAssetNameEn = htmlAssetNameNameEn;
	}

	public HtmlInputText getHtmlAssetDesc() {
		return htmlAssetDesc;
	}

	public void setHtmlAssetDesc(HtmlInputText htmlAssetDesc) {
		this.htmlAssetDesc = htmlAssetDesc;
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlBeneficiary() {
		return htmlBeneficiary;
	}

	public void setHtmlBeneficiary(HtmlInputText htmlBeneficiary) {
		this.htmlBeneficiary = htmlBeneficiary;
	}

	public HtmlInputText getHtmlFilePersonName() {
		return htmlFilePersonName;
	}

	public void setHtmlFilePersonName(HtmlInputText htmlFilePersonName) {
		this.htmlFilePersonName = htmlFilePersonName;
	}


	public void checkState() {

		ApplicationBean appBena = new ApplicationBean();
		appBena.getRelationList();
	}

	public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}

	public void init() {
		super.init();
		viewMap = getFacesContext().getViewRoot().getAttributes();
		try 
		{
			if (!isPostBack())
			{
				
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(true);
				loadDropDowns();
				getNewDisbursementRequest();
			}

		} catch (Exception es) {
			logger.LogException("init|Error occured:", es);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	private void getNewDisbursementRequest( )throws Exception 
	{
		DisbursementService service = new DisbursementService();
		if( disbursementTypeSelectMenu.getValue()== null  || disbursementTypeSelectMenu.getValue().toString().equals("-1") )
		{
			disbursementListCount="0";
			return;
		}
		Request request = service.getNewPeriodicListRequest(
				 											 Long.valueOf( disbursementTypeSelectMenu.getValue().toString() )
				                                           );
		if( request != null && request.getDisbursementDetails() != null )
		{
			disbursementListCount =  String.valueOf( request.getDisbursementDetails().size() );
		}
		
	}

	private void loadDropDowns() throws Exception 
	{
		loadDisbursementTypeList();
		//loadDisbursementSourceList();
		loadDisbursementStatusList();
	}
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	
	@SuppressWarnings( "unchecked" )
	private void loadDisbursementSourceList() throws Exception{

		HashMap<Long, String> disbursementSourceList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();
			disbursementSourceList = service.getDisbursementSourceList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementSourceList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}
			this.setDisbursementSourceList(selectItems);
	}
	@SuppressWarnings( "unchecked" )
	private void loadDisbursementStatusList() throws Exception
	{

		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		List<DomainDataView> ddList = CommonUtil.getDomainDataListForDomainType(WebConstants.PeriodicListStatus.LIST_STATUS_KEY);
		for (DomainDataView obj: ddList) 
		{
				String value = isEnglishLocale()?obj.getDataDescEn():obj.getDataDescAr();
				selectItems.add(new SelectItem(obj.getDomainDataId().toString(),
						                         value
						                       )
			                   );
		}
		Collections.sort(selectItems, ListComparator.LIST_COMPARE);
		this.setStatus(  WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE_ID.toString() );
		this.setPeriodicDisbursementStausList( selectItems );
	}
	@SuppressWarnings( "unchecked" )
	private void loadDisbursementTypeList() throws Exception {
		HashMap<Long, String> disbursementTypeList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();
			disbursementTypeList = service.getDisbursementTypeList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementTypeList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}
			this.setDisbursementTypeList(selectItems);
	}

	@SuppressWarnings("unchecked")
	public void obtainChildParams() {

		successMessages = new ArrayList<String>();
		try
		{
		if (sessionMap.containsKey(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW)) 
		{
			PeriodicDisbursementGridView parentRow = new PeriodicDisbursementGridView();
			parentRow = (PeriodicDisbursementGridView) sessionMap.get(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW);
			sessionMap.remove(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW);
			
			viewMap.put(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW,parentRow);
			
			for (PeriodicDisbursementGridView row : this.getDataList()) 
			{

				if (parentRow.getPeriodicDisbursementId().longValue() == row.getPeriodicDisbursementId()) {
					row.setPaymentDetailFormView(parentRow.getPaymentDetailFormView());

					break;
				}

			}
			successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_PAYMENT_INFO));
			this.getDataList();

		}
		}
		catch (Exception e)
		{
			logger.LogException("obtainChildParams|Error", e);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public Integer getRecordSize()
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}


	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getDisbursementTypesList() throws Exception {

		HashMap<Long, String> disbursementTypeList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();
			disbursementTypeList = service.getDisbursementTypeList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementTypeList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
		return selectItems;
	}

	@SuppressWarnings( "unchecked" )   
	public void doSearch() {
		try 
		{

			doSearchItemList();
		} 
		catch (Exception e) 
		{
			logger.LogException("doSearch|Error", e);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	public void doSearchItemList()
	{
		try
		{
				loadDataList();
		
		}
		catch (Exception e)
		{
		    	
	    	errorMessages = new ArrayList<String>(0);
	 		logger.LogException("doSearchItemList|Error Occured", e);
	 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	private List<PeriodicDisbursementGridView> loadDataList() throws Exception
	{
		if (dataList != null)
			dataList.clear();

			List<PeriodicDisbursementGridView> list = new ArrayList();
			searchDisbursementMap = getSearchCriteria();
			DisbursementService searchService = new DisbursementService();
			int totalRows = searchService.searchDisbursementsGetTotalNumberOfRecords(searchDisbursementMap);
			setTotalRows(totalRows);
			
			doPagingComputations();
			
			list = searchService.getDisbursementsByCriteria(searchDisbursementMap, getRowsPerPage(), getCurrentPage(),
															getSortField(), isSortItemListAscending()
														    );

			if (list.isEmpty() || list == null) 
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			
			settingPagingVariables(list, totalRows);
			getNewDisbursementRequest();
		return list;

	}
   @SuppressWarnings( "unchecked" )
	private void settingPagingVariables(List<PeriodicDisbursementGridView> list, int totalRows) 
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put("periodicDisbursementList", list);
		recordSize = totalRows;
		viewMap.put("recordSize", totalRows);
		paginatorRows = getPaginatorRows();
		paginatorMaxPages = recordSize / paginatorRows;
		if ((recordSize % paginatorRows) > 0)
			paginatorMaxPages++;
		if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, Object> getSearchCriteria() throws Exception{
		searchDisbursementMap = new HashMap<String, Object>();

		String emptyValue = "-1";

		Object disbursementType = disbursementTypeSelectMenu.getValue();
		Object objexpectedMaturityDateFrom = expectedMaturityDateFrom.getValue();
		Object objexpectedMaturityDateTo = expectedMaturityDateTo.getValue();
		Object startFrom = calStartFrom.getValue();
		
		Object startTo = calStartTo.getValue();
		Object endFrom = calEndFrom.getValue();
		Object endTo = calEndTo.getValue();
		Object beneficiary = htmlBeneficiary.getValue();
		Object toBeneficiary = htmlToBeneficiary.getValue();
		Object toAccountNum = htmlToAccountNum.getValue();
		Object toBank = htmlToBank.getValue();
		Object costCenter = htmlCostCenter.getValue();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
        String  dateStr =null;
        
        if( toBank !=null && toBank.toString().trim().length() > 0  )
        {
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_BANK ,toBank.toString());
        }
        if( toAccountNum !=null && toAccountNum.toString().trim().length() > 0  )
        {
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_ACC_NUM,toAccountNum.toString());
        }
        if( toBeneficiary !=null && toBeneficiary.toString().trim().length() > 0  )
        {
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.TO_BENEFICIARY_NAME,toBeneficiary.toString());
        }
        if( beneficiary !=null && beneficiary.toString().trim().length() > 0  )
        {
    		searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.BENEFICIARY_NAME,beneficiary.toString());
        }
         
        if(objexpectedMaturityDateFrom != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) objexpectedMaturityDateFrom);
        	    objexpectedMaturityDateFrom = formatter.parse( dateStr +" 00:00:00" );
    			searchDisbursementMap.put(  
    										WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.EXP_MAT_FROM,
    										objexpectedMaturityDateFrom
										 );
        }
        if(objexpectedMaturityDateTo != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) objexpectedMaturityDateTo);
        	    objexpectedMaturityDateTo = formatter.parse( dateStr +" 23:59:59" );
    			searchDisbursementMap.put(  
    										WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.EXP_MAT_TO,
    										objexpectedMaturityDateTo
										 );
        }
        
        if(startFrom != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) startFrom);
        	    startFrom = formatter.parse( dateStr +" 00:00:00" );
    			searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.START_FROM,
					    					startFrom
										 );
        }
        
        if( startTo != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) startTo);
        	    startTo= formatter.parse( dateStr +" 23:59:59" );
    			searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.START_TO,
					    					startTo
										 );
        }
        if(endFrom != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) endFrom);
        	    endFrom = formatter.parse( dateStr +" 00:00:00" );
    			searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.END_FROM,
					    					endFrom
										 );
        }
        
        if( endTo != null )
        {
        	    dateStr = formatterWithOutTime.format((Date) endTo);
        	    endTo= formatter.parse( dateStr +" 23:59:59" );
    			searchDisbursementMap.put(  WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.END_TO,
					    					endTo
										 );
        }
		if (disbursementType != null && !disbursementType.toString().trim().equals("") && !disbursementType.toString().trim().equals(emptyValue)) 
		{
			searchDisbursementMap.put(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.DISBURSEMENT_LIST_TYPE,
									  disbursementType.toString()
									 );
		}
		if( this.getStatus()  != null && !this.getStatus() .equals("-1") )
		{
			searchDisbursementMap.put( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.STATUS,
					                   this.getStatus()
					                   
					 				  );
		}
		if( this.getAmountFrom() != null && this.getAmountFrom().trim().length() > 0)
		{
			searchDisbursementMap.put( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.AMOUNT_FROM,
										this.getAmountFrom()
					 				  );
		}
		if( this.getAmountTo()!= null && this.getAmountTo().trim().length() > 0)
		{
			searchDisbursementMap.put( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.AMOUNT_TO,
										this.getAmountTo()
					 				  );
		}
		return searchDisbursementMap;
	}

	
	@SuppressWarnings("unchecked")
	public List<PeriodicDisbursementGridView> getDataList() {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		dataList = (List<PeriodicDisbursementGridView>) viewMap.get("periodicDisbursementList");
		if (dataList == null)
			dataList = new ArrayList<PeriodicDisbursementGridView>();
		return dataList;
	}

	public void setDataList(List<PeriodicDisbursementGridView> dataList) {
		this.dataList = dataList;
	}

	public List<PeriodicDisbursementGridView> getSelectedRows(Boolean calledFromReseacher) throws Exception 
	{
		List<PeriodicDisbursementGridView> selectedRowsList = new ArrayList<PeriodicDisbursementGridView>(0);
		dataList = getDataList();
		
		if (dataList == null || dataList.size() <= 0){return selectedRowsList;}
		
		
		for (PeriodicDisbursementGridView selectedRows : dataList)
		{
			if (  selectedRows.getSelected() == null || !selectedRows.getSelected() ) continue;
			if ( selectedRows.getPaymentDetailFormView() != null) 
			{
				if (disbursementSourceSelectMenu.getValue() != null) 
				{
					String disbursementSource = disbursementSourceSelectMenu.getValue().toString();
					selectedRows.setDisbSourceId(disbursementSource);
				}
				selectedRowsList.add(selectedRows);
			} 
			else if (	!selectedRows.getDateConflictBool() && calledFromReseacher ) 
			{
				selectedRowsList.add(selectedRows);
			}
		}
		return selectedRowsList;
	}

	public boolean hasSendToFinanceErrors()throws Exception 
	{
		boolean hasErrors = false;
		List<PeriodicDisbursementGridView> selectedRowsList = new ArrayList<PeriodicDisbursementGridView>(0);
		boolean hasSelected = false;
		dataList = getDataList();
		if (dataList != null && dataList.size() > 0) 
		{

			for (PeriodicDisbursementGridView selectedRows : dataList) 
			{
				// IF ANY SELECTED ROW HAS THE CONFLICBOOLEAN TO TRUE ,
				// RAISE ERROR here
				if (selectedRows.getSelected()) 
				{
					hasSelected = true;
					if ( selectedRows.getPaymentDetailFormView() == null) 
					{
						hasErrors = true;
						errorMessages
								.add(CommonUtil
										.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.PAYMENT_DETAIL_DATE_CONFLICTS));
						selectedRowsList.clear(); // empty the list so that ,
						// none of the rows could
						// proceed with the
						// addition.
						break;
					}
					if (selectedRows.getStatusDataValue().compareToIgnoreCase(WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE) != 0) 
					{
						hasErrors = true;
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.STATUS_NOT_ACTIVE));
						break;
					}
				}

			}

		}
		if (!hasSelected || dataList == null || dataList.size() <= 0) 
		{
			hasErrors = true;
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));

		}
		if (disbursementSourceSelectMenu.getValue() == null
				&& disbursementTypeSelectMenu.getValue() != null
				&& (new Long(disbursementTypeSelectMenu.getValue().toString())) != WebConstants.DISBURSEMENTS_TYPE_LIST.MONTHLY_ALIMONY) 
		{

			hasErrors = true;
			errorMessages
					.add(CommonUtil
							.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.DISB_SOURCE_MISSING));
		}

		return hasErrors;
	}

	public boolean hasSendToResearcerErrors()throws Exception {
		boolean hasErrors = false;
		List<PeriodicDisbursementGridView> selectedRowsList = new ArrayList<PeriodicDisbursementGridView>(
				0);
		boolean hasSelected = false;
		dataList = getDataList();
		if (dataList != null && dataList.size() > 0) {

			for (PeriodicDisbursementGridView selectedRows : dataList) {
				// IF ANY SELECTED ROW HAS THE CONFLICBOOLEAN TO TRUE ,
				// RAISE ERROR here
				if (selectedRows.getSelected()) 
				{
					hasSelected = true;
					if ( selectedRows.getDateConflictBool() ) 
					{
						hasErrors = true;
						errorMessages.add( CommonUtil.getBundleMessage( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.DATE_CONFLICT_FOUND ) );
						selectedRowsList.clear(); 
						break;
					}
				}
			}
		}
		if (!hasSelected || dataList == null || dataList.size() <= 0) 
		{
			hasErrors = true;
			errorMessages.add( CommonUtil.getBundleMessage( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED ) );
		}
		if (
				disbursementSourceSelectMenu.getValue() == null && 
				disbursementTypeSelectMenu.getValue() != null && 
				(	new Long(disbursementTypeSelectMenu.getValue().toString() )  ) != WebConstants.DISBURSEMENTS_TYPE_LIST.MONTHLY_ALIMONY
		   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));
		}
		return hasErrors;
	}

	public void sendToFinance(javax.faces.event.ActionEvent event)throws Exception 
	{
		Boolean calledFromSendToResearcher = false;
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {

			if (hasSendToFinanceErrors()){return;}
			
			List<PeriodicDisbursementGridView> selectedRows = getSelectedRows(calledFromSendToResearcher);
			Boolean request = addPeriodicDisburseSendToFinanceRequest(selectedRows);
			if (request == true) 
			{
				successMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.FINANCE_SUCCESS_MESSAGE));
				for (PeriodicDisbursementGridView periodicDisbursementGridView : selectedRows) 
				{
					periodicDisbursementGridView.setSelected(false);
					periodicDisbursementGridView.setIsSentToFinance(Long.valueOf( 1));
				}
			}
		} catch (Exception ex) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("sendToFinance| Error Occured...", ex);
		}

	}

	private Boolean addPeriodicDisburseSendToFinanceRequest(List<PeriodicDisbursementGridView> selectedRows) throws Exception 
	{
		boolean result = false;
		Long requestId= new RequestService().addPeriodicDisburseSendToFinanceRequest(selectedRows,getLoggedInUserId());

		MEMSDisbursementListBPELPortClient bpelPortClient= new MEMSDisbursementListBPELPortClient();
		SystemParameters parameters = SystemParameters.getInstance();
		String endPoint = parameters.getParameter(WebConstants.PeriodicDisbursement.BPEL_END_POINT);
		String userId = CommonUtil.getLoggedInUser();
		bpelPortClient.setEndpoint(endPoint);
		bpelPortClient.initiate(requestId, userId, null, null);
		result =  true;
		return result;
	}
	
	@SuppressWarnings( "unchecked" )
	public void sendToResearchers(javax.faces.event.ActionEvent event) throws Exception
	{
		Boolean calledFromSendToReseacher = true;
		successMessages = new ArrayList<String>();
		errorMessages = new ArrayList<String>();
		List<PeriodicDisbursementGridView> selectedRows = getSelectedRows( calledFromSendToReseacher );
		try 
		{
			if (hasSendToResearcerErrors()) {return;}
			
			HashMap<String, List<PeriodicDisbursementGridView>> map = new HashMap<String, List<PeriodicDisbursementGridView>>();
			map = obtainResearcherIdFromRows(selectedRows);
			for ( String researcherId : map.keySet() ) 
			{
				List<PeriodicDisbursementGridView> value = map.get( researcherId );
				Long requestId = addPeriodicDisbursementRequest( researcherId, value );
				if ( requestId != null ) 
				{
					successMessages.add(
										 CommonUtil.getBundleMessage( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.RESEARCHER_SUCCESS_MESSAGE ) 
									   );
					for ( PeriodicDisbursementGridView periodicDisbursementGridView : selectedRows ) 
					{
						periodicDisbursementGridView.setSelected(false);
						periodicDisbursementGridView.setIsSentToFinance(new Long(1));
					}
				}
			}
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("sendToResearchers| Error Occured...", ex);
		}
	}

	public Long addPeriodicDisbursementRequest(String researcherId,List<PeriodicDisbursementGridView> viewList) throws Exception 
	{
		String endPoint = SystemParameters.getInstance().getParameter("MEMSReneworStopDisbursement");
		RenewOrStopDisbursementBPELPortClient port = new RenewOrStopDisbursementBPELPortClient();
		port.setEndpoint(endPoint);
		String researcherIdWithPimsAdmin= researcherId + ",pims_admin";
		Long requestId = new RequestService().addPeriodicDisburseSendToResearcherRequest(viewList,getLoggedInUserId());
		port.initiate(requestId, getLoggedInUserId(), researcherIdWithPimsAdmin, null, null);
		return requestId;
	}

	@SuppressWarnings( "unchecked" )
	private HashMap<String, List<PeriodicDisbursementGridView>> obtainResearcherIdFromRows(
			List<PeriodicDisbursementGridView> selectedRows) {

		HashMap<String, List<PeriodicDisbursementGridView>> map = new HashMap<String, List<PeriodicDisbursementGridView>>();
		for (PeriodicDisbursementGridView periodicDisbursementGridView : selectedRows) {

			if (!map
					.containsKey(periodicDisbursementGridView.getResearcherId())) {
				List<PeriodicDisbursementGridView> relatedBeneficiaries = new ArrayList<PeriodicDisbursementGridView>(
						0);
				relatedBeneficiaries.add(periodicDisbursementGridView);
				map.put(periodicDisbursementGridView.getResearcherId(),
						relatedBeneficiaries);
			} else {

				List<PeriodicDisbursementGridView> relatedBeneficiaries = (List<PeriodicDisbursementGridView>) map
						.get(periodicDisbursementGridView.getResearcherId());
				relatedBeneficiaries.add(periodicDisbursementGridView);
				map.put(periodicDisbursementGridView.getResearcherId(),
						relatedBeneficiaries);

			}
		}
		// TODO Auto-generated method stub
		return map;
	}
    @SuppressWarnings( "unchecked" )
    private boolean hasAddToDisbursementError(PeriodicDisbursementGridView gridViewRow ) throws Exception
    {
    	boolean hasError = false;
    	DisbursementService service = new DisbursementService();
    	Request request =  service.isPresentInDraftList(	gridViewRow.getPeriodicDisbursementId(),
									   						Long.valueOf( disbursementTypeSelectMenu.getValue().toString() )
					    								); 
    	if( request != null )
    	{
    		String message = java.text.MessageFormat.format(
    														ResourceUtil.getInstance().getProperty( "periodicDisbursement.msg.alreadyPresentInDraftList" ),
    														gridViewRow.getBeneficiaryName(),
    														request.getRequestNumber()
    													    );
    		errorMessages.add( message );
    		return true;
    		
    	}
    	
    	
    	return hasError;
    }
    public void onAddAllToDisbursementList()
    {
    	
        errorMessages =  new ArrayList<String>();
	    try	
	    {
	    	DisbursementService service = new DisbursementService();
	    	searchDisbursementMap = getSearchCriteria();
	    	List<PeriodicDisbursementGridView >list = service.getDisbursementsByCriteria(searchDisbursementMap, null, null,null, null);
	    	List<PeriodicDisbursementGridView> sendToList = new ArrayList<PeriodicDisbursementGridView>();
	    	Map<Long,Long>  map = service.alreadyPresentInDraftList(Long.valueOf( disbursementTypeSelectMenu.getValue().toString() ));
             for (PeriodicDisbursementGridView gridViewRow : list) 
             {
					if ( 	gridViewRow.getIsRenewalRequired() ==0 && 
							gridViewRow.getIsRenewed()==0 && 
							gridViewRow.getStatusDataValue().equals( "ACTIVE" ) &&
							gridViewRow.getHasNewDisbursement().equals("0")&& 
							!map.containsKey( gridViewRow.getPeriodicDisbursementId()	)
					   )
					{
						sendToList.add(  gridViewRow	); 
					}
             }
			  service.addToDisbursementList(  sendToList, 
																 CommonUtil.getLoggedInUser() , 
																 Long.valueOf( disbursementTypeSelectMenu.getValue().toString() ) 
															   );
            loadDataList();
			getNewDisbursementRequest(); 
			successMessages =  new ArrayList<String>();
			successMessages.add( CommonUtil.getBundleMessage( "periodicDisbursement.addedToDisbursementList"));
	    } 
	    catch (Exception es) 
	    {
			logger.LogException("onAddAllToDisbursementList|Error occured:", es);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}	

    }
    @SuppressWarnings( "unchecked" )
	public void onAddToDisbursementList( ) 
    {
    	errorMessages =  new ArrayList<String>();
	    try	
	    {
	    	PeriodicDisbursementGridView   gridViewRow = (PeriodicDisbursementGridView) dataTable.getRowData();
	    	if( hasAddToDisbursementError( gridViewRow  ) )
	    	{
	    		return;
	    	}
			List<PeriodicDisbursementGridView> list = new ArrayList<PeriodicDisbursementGridView>(1);
			list.add(  gridViewRow	); 
			DisbursementService service = new DisbursementService();
			Request request =  service.addToDisbursementList(  list, 
															   CommonUtil.getLoggedInUser() , 
															   Long.valueOf( disbursementTypeSelectMenu.getValue().toString() ) 
															 );
			gridViewRow.setHasNewDisbursement("1");
			gridViewRow.setNewDisbursementRequestNum(request.getRequestNumber());
			getNewDisbursementRequest(); 
			successMessages =  new ArrayList<String>();
			successMessages.add( CommonUtil.getBundleMessage( "periodicDisbursement.addedToDisbursementList"));
	    } 
	    catch (Exception es) 
	    {
			logger.LogException("onAddToDisbursementList|Error occured:", es);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}	

	}

    @SuppressWarnings( "unchecked" )
	public String  onNavigateToDisbursementListScreen( ) 
    {
	    try	
	    {
	    	
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put( "FROM_PRDC_LIST", disbursementTypeSelectMenu.getValue().toString()  );
			return "DisbursementListScreen";
	    } catch (Exception es) {
			logger.LogException("onNavigateToDisbursementListScreen|Error occured:", es);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}	
        return "";
	}
    @SuppressWarnings( "unchecked" )
	public void openPaymentsPopUp(javax.faces.event.ActionEvent event) 
    {
	    try	
	    {
			PeriodicDisbursementGridView gridViewRow = (PeriodicDisbursementGridView) dataTable.getRowData();
			String javaScriptText = " var screen_width = 800;"
					+ "var screen_height = 250;"
					+ "var popup_width = screen_width-200;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('paymentDetailsPopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP&"
					+ WebConstants.PERSON_ID
					+ "="
					+ gridViewRow.getBeneficiaryId()
					+ "&"
					+ WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT
					+ "="
					+ gridViewRow.getAmount()
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
	
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW,gridViewRow);
			sendToParent(javaScriptText);
	    } catch (Exception es) {
			logger.LogException("openPaymentsPopUp|Error occured:", es);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}	

	}
    @SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) 
    {
	    try
	    {
			PeriodicDisbursementGridView gridViewRow = (PeriodicDisbursementGridView) dataTable.getRowData();
			String javaScriptText = " var screen_width = 970;"
					+ "var screen_height = 550;"
					+ "var popup_width = screen_width;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
					+ WebConstants.PERSON_ID
					+ "="
					+ gridViewRow.getBeneficiaryId()
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
	
			sendToParent(javaScriptText);
	    } catch (Exception es) {
			logger.LogException("openManageBeneficiaryPopUp|Error occured:", es);
			errorMessages =  new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}	

	}


	public String sendToParent(String javaScript) throws Exception 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript);
		return "";
	}

	public HtmlSelectOneMenu getDisbursementTypeSelectMenu() {
		return disbursementTypeSelectMenu;
	}

	public void setDisbursementTypeSelectMenu(
			HtmlSelectOneMenu disbursementTypeSelectMenu) {
		this.disbursementTypeSelectMenu = disbursementTypeSelectMenu;
	}

	public HtmlSelectOneMenu getDisbursementSourceSelectMenu() {
		return disbursementSourceSelectMenu;
	}

	public void setDisbursementSourceSelectMenu(
			HtmlSelectOneMenu disbursementSourceSelectMenu) {
		this.disbursementSourceSelectMenu = disbursementSourceSelectMenu;
	}

	public HtmlInputText getHtmlAmount() {
		return htmlAmount;
	}

	public void setHtmlAmount(HtmlInputText htmlAmount) {
		this.htmlAmount = htmlAmount;
	}

	public void disbursementTypeChanged() 
	{
		errorMessages = new ArrayList<String>(0);
		try 
		{

			Long valueReturn = new Long( disbursementTypeSelectMenu.getValue().toString() );
			if (valueReturn != null && valueReturn.longValue() == WebConstants.DISBURSEMENTS_TYPE_LIST.MONTHLY_ALIMONY)
			{
				disbursementSourceSelectMenu.setDisabled(true);
			}
			else
			{
				disbursementSourceSelectMenu.setDisabled(false);
			}
			loadDataList();
		} 
		catch (Exception e) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			logger.LogException( "disbursementTypeChanged| Error Occured...", e);
		}
	}

	public HtmlInputText getHtmlHiddenAccount() {

		return htmlHiddenAccount;

	}

	public void setHtmlHiddenAccount(HtmlInputText htmlHiddenAccount) {
		this.htmlHiddenAccount = htmlHiddenAccount;
	}
	@SuppressWarnings("unchecked")
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	@SuppressWarnings("unchecked")
	public String getSuccessMessages() {
		String methodName = "getSuccessMessages";
		logger.logInfo(methodName + "|" + " Start");

		String messageList = "";
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : successMessages) {
				messageList += message + " <br></br>";
			}

		}

		logger.logInfo(methodName + "|" + " Finish");
		return messageList;

	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementTypeList() {
		List<SelectItem> returnList = new ArrayList<SelectItem>();
		if (viewMap.get("disbursementTypeList") != null) {
			returnList = (ArrayList<SelectItem>) viewMap
					.get("disbursementTypeList");
		}
		return returnList;
	}
	@SuppressWarnings("unchecked")
	public void setDisbursementTypeList(List<SelectItem> disbursementTypeList) {

		this.disbursementTypeList = disbursementTypeList;
		if (disbursementTypeList != null) {
			Collections.sort(disbursementTypeList, ListComparator.LIST_COMPARE);
			viewMap.put("disbursementTypeList", disbursementTypeList);
		}

	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementSourceList() {

		List<SelectItem> returnList = new ArrayList<SelectItem>();
		if (viewMap.get("disbursementSourceList") != null) {
			returnList = (ArrayList<SelectItem>) viewMap
					.get("disbursementSourceList");
		}
		return returnList;

	}
	@SuppressWarnings("unchecked")
	public void setDisbursementSourceList(
			List<SelectItem> disbursementSourceList) {
		this.disbursementSourceList = disbursementSourceList;

		if (disbursementSourceList != null) {
			Collections.sort(disbursementSourceList,
					ListComparator.LIST_COMPARE);
			viewMap.put("disbursementSourceList", disbursementSourceList);
		}
	}
	@SuppressWarnings("unchecked")
	public String getAmountFrom() {
		if( viewMap.get("amountFrom") != null )
		{
			amountFrom = viewMap.get("amountFrom").toString();
		}
		return amountFrom;
	}
	@SuppressWarnings("unchecked")
	public void setAmountFrom(String amountFrom) {
		this.amountFrom = amountFrom;
		if(this.amountFrom != null )
		{
			viewMap.put( "amountFrom", this.amountFrom); 
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getAmountTo() {
		if( viewMap.get("amountTo") != null )
		{
			amountTo = viewMap.get("amountTo").toString();
		}
		return amountTo;
	}
	@SuppressWarnings("unchecked")
	public void setAmountTo(String amountTo) {
		this.amountTo = amountTo;
		if(this.amountTo != null )
		{
			viewMap.put( "amountTo", this.amountTo); 
		}
	}

	public HtmlCalendar getCalEndTo() {
		return calEndTo;
	}

	public void setCalEndTo(HtmlCalendar calEndTo) {
		this.calEndTo = calEndTo;
	}

	public HtmlCalendar getCalEndFrom() {
		return calEndFrom;
	}

	public void setCalEndFrom(HtmlCalendar calEndFrom) {
		this.calEndFrom = calEndFrom;
	}

	public HtmlCalendar getCalStartTo() {
		return calStartTo;
	}

	public void setCalStartTo(HtmlCalendar calStartTo) {
		this.calStartTo = calStartTo;
	}

	public HtmlCalendar getCalStartFrom() {
		return calStartFrom;
	}

	public void setCalStartFrom(HtmlCalendar calStartFrom) {
		this.calStartFrom = calStartFrom;
	}

	public String getStatus() {
		if( viewMap.get("status" ) != null )
		{
			this.status = viewMap.get( "status" ).toString();
		}
		return status;
	}

	@SuppressWarnings("unchecked")
	public void setStatus(String status) {
		this.status = status;
		if( this.status != null )
		{
			viewMap.put("status", this.status);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPeriodicDisbursementStausList() {
		if( viewMap.get( "periodicDisbursementStausList" ) != null )
		{
			periodicDisbursementStausList = (ArrayList<SelectItem>)viewMap.get( "periodicDisbursementStausList" ); 
		}
		return periodicDisbursementStausList;
	}
	@SuppressWarnings("unchecked")
	public void setPeriodicDisbursementStausList(
			List<SelectItem> periodicDisbursementStausList) {
		this.periodicDisbursementStausList = periodicDisbursementStausList;
		if( this.periodicDisbursementStausList != null )
		{
			viewMap.put( "periodicDisbursementStausList" ,this.periodicDisbursementStausList); 
		}
	}

	public String getDisbursementListCount() {
		
		return disbursementListCount;
	}

	public void setDisbursementListCount(String disbursementListCount) {
		this.disbursementListCount = disbursementListCount;
	}

	public HtmlCalendar getExpectedMaturityDateFrom() {
		return expectedMaturityDateFrom;
	}

	public void setExpectedMaturityDateFrom(HtmlCalendar expectedMaturityDateFrom) {
		this.expectedMaturityDateFrom = expectedMaturityDateFrom;
	}

	public HtmlCalendar getExpectedMaturityDateTo() {
		return expectedMaturityDateTo;
	}

	public void setExpectedMaturityDateTo(HtmlCalendar expectedMaturityDateTo) {
		this.expectedMaturityDateTo = expectedMaturityDateTo;
	}

	public HtmlInputText getHtmlToBeneficiary() {
		return htmlToBeneficiary;
	}

	public void setHtmlToBeneficiary(HtmlInputText htmlToBeneficiary) {
		this.htmlToBeneficiary = htmlToBeneficiary;
	}

	public HtmlInputText getHtmlToBank() {
		return htmlToBank;
	}

	public void setHtmlToBank(HtmlInputText htmlToBank) {
		this.htmlToBank = htmlToBank;
	}

	public HtmlInputText getHtmlToAccountNum() {
		return htmlToAccountNum;
	}

	public void setHtmlToAccountNum(HtmlInputText htmlToAccountNum) {
		this.htmlToAccountNum = htmlToAccountNum;
	}

	public HtmlInputText getHtmlCostCenter() {
		return htmlCostCenter;
	}

	public void setHtmlCostCenter(HtmlInputText htmlCostCenter) {
		this.htmlCostCenter = htmlCostCenter;
	}

}
