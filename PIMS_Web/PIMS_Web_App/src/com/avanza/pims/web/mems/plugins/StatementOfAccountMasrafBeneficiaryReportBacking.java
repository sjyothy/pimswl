package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class StatementOfAccountMasrafBeneficiaryReportBacking extends AbstractMemsBean
{	
	
	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	StatementOfAccountCriteria criteria;
	private Date fromDate;
	private Date toDate;
	
	public StatementOfAccountCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(StatementOfAccountCriteria criteria) {
		this.criteria = criteria;
	}

	public StatementOfAccountMasrafBeneficiaryReportBacking() 
	{
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	
		criteria  =  new StatementOfAccountCriteria();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if(  viewMap.get( "criteria" ) != null )
		{
			criteria= ( StatementOfAccountCriteria )viewMap.get( "criteria" )  ;
		}
		if(	criteria.getFromDate() == null	)
		{
			criteria.setFromDate( criteria.getFormatterWithOutTime().parse("01/01/2015") );
		}
		if(	criteria.getToDate() == null	)
		{
			criteria.setToDate( new Date() );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( criteria != null )
		{
			viewMap.put(  "criteria" , criteria);
		}
	}
	

	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccount()
	{
		try
		{
			updateValuesFromMaps();
			StatementOfAccountCriteria statmntReportCriteria= new StatementOfAccountCriteria(
																								ReportConstant.Report.STATEMENT_OF_ACCOUNT_MASRAF_BENEFICIARY,
																								ReportConstant.Processor.STATMENT_OF_ACCOUNT_MASRAF_BENEFICIARY,
																								CommonUtil.getLoggedInUser()
																							 );
			if(criteria.getFromDate()  != null )
			{
				statmntReportCriteria.setFromDate(   criteria.getFromDate() );
			}
			else
			{
				statmntReportCriteria.setFromDate(   statmntReportCriteria.getFormatterWithOutTime().parse("01/01/2015") );
				
			}
			if(criteria.getToDate() != null  )
			{
				statmntReportCriteria.setToDate(     criteria.getToDate() );
			}
			else
			{
				statmntReportCriteria.setToDate(     
												statmntReportCriteria.getFormatterWithOutTime().parse(
																									  statmntReportCriteria.getFormatterWithOutTime().format( new Date() ) 
																									 ) 
											   );
			}
			if( criteria.getMasrafBeneficiaryName() != null && criteria.getMasrafBeneficiaryName().trim().length() > 0  )
			{
				statmntReportCriteria.setMasrafBeneficiaryName( criteria.getMasrafBeneficiaryName().trim() );
			}
			if( criteria.getMasrafName() != null && criteria.getMasrafName().trim().length() > 0  )
			{
				statmntReportCriteria.setMasrafName( criteria.getMasrafName().trim() );
			}
			statmntReportCriteria.setLoggedInUser( getLoggedInUserId() );
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


}
