package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.entity.ZakatLog;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.ZakatService;
import com.avanza.ui.util.ResourceUtil;

public class ZakatHistoryPopup extends AbstractMemsBean
{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	List<ZakatLog> list = new ArrayList<ZakatLog>();
	private HtmlDataTable dataTable;
	public ZakatHistoryPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		Long id	  = null;
		
		if( request.getParameter("zakatId") != null)
		{
			id =  new Long(request.getParameter("zakatId").toString());
		}
		list = ZakatService.getZakatHistoryFromZakatId(  id );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	public List<ZakatLog> getList() {
		return list;
	}

	public void setList(List<ZakatLog> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}	
    

}
