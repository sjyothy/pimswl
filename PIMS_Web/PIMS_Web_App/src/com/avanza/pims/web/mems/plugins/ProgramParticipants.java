package com.avanza.pims.web.mems.plugins;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import oracle.bpel.services.identity.xpath.GetDefaultRealmNameFunction;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.ProgramParticipantsView;
import com.avanza.ui.util.ResourceUtil;
@SuppressWarnings("unchecked")
public class ProgramParticipants extends AbstractMemsBean
{
	private static final long serialVersionUID = 8517238816949483790L;
	List<ProgramParticipantsView> selectedList; 
	private List<ProgramParticipantsView> list;
	private HtmlDataTable dataTable;
	private ProgramParticipantsView programParticipants;
	private String listLoadedFromDb;
	private HtmlInputText txtExternalParticipantName;
	private Boolean markUnMarkAll;
	private HtmlCommandButton btnAddInternalParticipants ;
	private HtmlCommandButton btnPresent;
	private HtmlCommandButton btnAbsent;
	private HtmlCommandButton btnAddParticipant;
	private SocialProgramsBean parentBean;
	
	public ProgramParticipants() 
	{	
		programParticipants = new ProgramParticipantsView();
		list = new ArrayList<ProgramParticipantsView>(0);
		txtExternalParticipantName = new HtmlInputText();
		btnPresent = new HtmlCommandButton();
		btnAbsent = new HtmlCommandButton();
		btnAddParticipant = new HtmlCommandButton();
		btnAddInternalParticipants = new HtmlCommandButton();
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			if( sessionMap.containsKey(WebConstants.SELECTED_PERSONS_LIST) )
			{
				onMessageFromSearchPerson();
			}
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	public void showControlForDefaultView()
	{
		if( programParticipants.getType() ==  null  || Integer.valueOf( programParticipants.getType() ) == -1   )
		{
				txtExternalParticipantName.setDisabled(true);
				txtExternalParticipantName.setStyleClass("INPUT_TEXT READONLY");
				btnAddInternalParticipants.setRendered(false);
				btnAddParticipant.setRendered(false);
		}
		if(  list == null || list.size() <= 0 )
		{
			btnAbsent.setRendered(false);
			btnPresent.setRendered(false);
		}
		//IF LIST HAS DATA BUT ALL THE ROWS ARE DELETED
		else 
		{
			boolean hasUndeletedRow=false;
			for (ProgramParticipantsView obj : list) {
				if(obj.getIsDeleted().equals("0") )
				{
					hasUndeletedRow=true;
					break;
				}
			
			}
			if( !hasUndeletedRow )
			{
				btnAbsent.setRendered(false);
				btnPresent.setRendered(false);
			}
			else
			{
				btnAbsent.setRendered(true);
				btnPresent.setRendered(true);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		
		
		// PURPOSE VIEW
		if ( viewMap.get( WebConstants.SocialProgramParticipants.VO ) != null )
		{
			programParticipants = (ProgramParticipantsView) viewMap.get( WebConstants.SocialProgramParticipants.VO );
		}		
		
		// PURPOSE VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramParticipants.DATA_LIST ) != null )
		{
			list = ( List<ProgramParticipantsView>) viewMap.get( WebConstants.SocialProgramParticipants.DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programParticipants != null  )
		{
			viewMap.put( WebConstants.SocialProgramParticipants.VO, programParticipants );
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramParticipants.DATA_LIST, list );
		}
		showControlForDefaultView();
	}
	@SuppressWarnings("unchecked")
	public void onLoadList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadParticipants()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramParticipants( programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
	}
	public void onMarkUnMarkChanged()
	{
		final String METHOD_NAME  = "chkonMarkUnMarkChanged|";
		try
		{
			updateValuesFromMaps();
			for ( ProgramParticipantsView obj: list) 
			{
				
				   obj.setSeleted( this.getMarkUnMarkAll() );
				
			}
			updateValuesToMaps();
			logger.logInfo( METHOD_NAME +"Finish" );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	@SuppressWarnings("unchecked")
	public void onDeleteParticipants( )
	{
	    final String METHOD_NAME = "onDeleteParticipants()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramParticipantsView item = ( ProgramParticipantsView )dataTable.getRowData(); 
			
			if(  item.getProgPartId() != null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			if( item.getIsPresent().equals("1") )
			{
				int presentParticipants = parentBean.getSocialProgramView().getPresentParticipants()!= null && parentBean.getSocialProgramView().getPresentParticipants().length()>0 ?
                        Integer.valueOf( parentBean.getSocialProgramView().getPresentParticipants()):
                      	  0;
				parentBean.getSocialProgramView().setPresentParticipants( String.valueOf(presentParticipants - 1));
			}
			updateValuesToMaps();
			
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSetAsWinners( )
	{
	    final String METHOD_NAME = "onSetAsWinners()";
	    
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			//list =  new SocialProgramService().setProgramParticipantsAsWinner( list ,getLoggedInUserId() );
			updateValuesFromMaps();
			ProgramParticipantsView receivedItem = (ProgramParticipantsView)sessionMap.get(WebConstants.SocialProgramParticipants.VO);
			sessionMap.remove(WebConstants.SocialProgramParticipants.VO);
			for (ProgramParticipantsView listItem : list) {
				
				if( listItem.getMyHashCode() == receivedItem.getMyHashCode() )
				{
					receivedItem.setIsWinner("1");
					int index = list.indexOf(listItem);
					list.remove(index);
					list.add(index,receivedItem);
					break;
				}
			}
			
			updateValuesToMaps();
		    successMessages.add( MessageFormat.format( ResourceUtil.getInstance().getProperty("socialProgram.messages.personMarkedAsWinner"), 
		    		(receivedItem.getPersonId()!= null?receivedItem.getInternalPersonName():receivedItem.getExternalParticipantName()) ));
			parentBean.setSuccessMessages(successMessages);
			
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	public void onWinnerClick()
	{
		//
		final String METHOD_NAME = "onWinnerClick()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramParticipantsView row = (ProgramParticipantsView)dataTable.getRowData();
			sessionMap.put(WebConstants.SocialProgramParticipants.VO,row);
            openPopUp("javaScript:openParticipantWinnerCommentsPopup()");
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	private List<ProgramParticipantsView> getSelectedRows()throws Exception
	{
	   selectedList  = new ArrayList<ProgramParticipantsView>(0);
 		for (ProgramParticipantsView item : list) {
			if( item.isSeleted() )
			{
				selectedList.add( item );
			}
		}
 		return selectedList;
	}
	private boolean hasAnySelected() throws Exception
	{
		if( selectedList == null || selectedList.size() <=0 )
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty( "socialProgram.messages.selectionReq" ) );
			parentBean.setErrorMessages( errorMessages );
			return false;
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	public void onSetAsAbsent( )
	{
	    final String METHOD_NAME = "onSetAsAbsent()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			getSelectedRows();
			//list =  new SocialProgramService().setProgramParticipantsAsAbsent( list ,getLoggedInUserId() );
			if( hasAnySelected() )
			{
				int presentParticipants = parentBean.getSocialProgramView().getPresentParticipants()!= null && parentBean.getSocialProgramView().getPresentParticipants().length()>0 ?
                        Integer.valueOf( parentBean.getSocialProgramView().getPresentParticipants()):
                      	  0;
				for (ProgramParticipantsView item : selectedList )
				{
					if( item.isSeleted() )
					{
						item.setIsPresent("0");
						item.setIsWinner( "0");
						item.setWinnerComments( "" );
						presentParticipants = presentParticipants - 1;
					}
				}
				updateValuesToMaps();
				parentBean.getSocialProgramView().setPresentParticipants( String.valueOf( presentParticipants ) );
			}
			logger.logInfo( METHOD_NAME + " --- Finish--- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSetAsPresent( )
	{
	    final String METHOD_NAME = "onSetAsPresent()";
		try
		{
			updateValuesFromMaps();
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			getSelectedRows();
			
			if( hasAnySelected() )
			{
				int presentParticipants = parentBean.getSocialProgramView().getPresentParticipants()!= null && parentBean.getSocialProgramView().getPresentParticipants().length()>0 ?
						                              Integer.valueOf( parentBean.getSocialProgramView().getPresentParticipants()):
						                            	  0;
				for (ProgramParticipantsView item : selectedList) 
				{
					if( item.isSeleted() )
					{
						item.setIsPresent("1");
					    presentParticipants = presentParticipants+1;
					}
				}
				parentBean.getSocialProgramView().setPresentParticipants( String.valueOf( presentParticipants ) );
				//list =  new SocialProgramService().setProgramParticipantsAsPresent( list ,getLoggedInUserId() );
			    updateValuesToMaps();
			}
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String onSave()
	{
		final String METHOD_NAME = "onSave()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramParticipantsView newVo = new ProgramParticipantsView();
				
				setDefaultFields(newVo);
				newVo.setPersonId( programParticipants.getPersonId() );
				newVo.setExternalParticipantName( programParticipants.getExternalParticipantName() );
				
				list.add(0, newVo);
				//saving it in viewMap to retrieve it from the parent bean later ,
				programParticipants.setInternalPersonName( "" ); 
				programParticipants.setPersonId( null );
				programParticipants.setExternalParticipantName( "" );
				
				updateValuesToMaps();
			}
			
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
		
		return null;
	}

	private void setDefaultFields(ProgramParticipantsView newVo) throws Exception
	{
		
		newVo.setMyHashCode(newVo.hashCode());
		newVo.setCreatedBy(getLoggedInUserId() );
		newVo.setCreatedByEn( getLoggedInUserObj().getFullName());
		newVo.setCreatedByAr( getLoggedInUserObj().getSecondaryFullName() );
		newVo.setUpdatedBy(getLoggedInUserId() );
		newVo.setUpdatedByEn( getLoggedInUserObj().getFullName());
		newVo.setUpdatedByAr( getLoggedInUserObj().getSecondaryFullName() );
		
		newVo.setCreatedOn( new Date() );
		newVo.setIsPresent("0" );
		newVo.setIsWinner( "0" );
		newVo.setIsDeleted( "0" );
		newVo.setSeleted(false);
		newVo.setType( programParticipants.getType() );
	}
	public void onParticipantTypeChange()
	{
		try
		{
			if( programParticipants.getType() !=  null  )
			{
				if( Long.valueOf( programParticipants.getType() ) == WebConstants.SocialProgramParticipants.TYPE_INTERNAL.longValue()   )
				{
					txtExternalParticipantName.setDisabled(true);
					txtExternalParticipantName.setStyleClass("INPUT_TEXT READONLY");
					programParticipants.setExternalParticipantName(  "" );
					btnAddInternalParticipants.setRendered(true);
					btnAddParticipant.setRendered(false);
				}
				else if( Long.valueOf( programParticipants.getType() )== WebConstants.SocialProgramParticipants.TYPE_EXTERNAL.longValue() )
				{
					txtExternalParticipantName.setDisabled(false);
					txtExternalParticipantName.setStyleClass("INPUT_TEXT");
					programParticipants.setPersonId(null);
					programParticipants.setInternalPersonName( "" );
					btnAddInternalParticipants.setRendered(false);
					btnAddParticipant.setRendered(true);
				}
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onParticipantTypeChange --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	public void onMessageFromSearchPerson()
	{
		try
		{
			List<PersonView> personList = new ArrayList<PersonView>();
			personList = (ArrayList<PersonView>)sessionMap.get( WebConstants.SELECTED_PERSONS_LIST );
			sessionMap.remove( WebConstants.SELECTED_PERSONS_LIST );
			int presentParticipants = parentBean.getSocialProgramView().getPresentParticipants()!= null && parentBean.getSocialProgramView().getPresentParticipants().length()>0 ?
                    Integer.valueOf( parentBean.getSocialProgramView().getPresentParticipants()):
                  	  0;
			for (PersonView person : personList)
			{
              ProgramParticipantsView participant = new ProgramParticipantsView();
              setDefaultFields( participant );
              participant.setPersonId( person.getPersonId() );
              participant.setInternalPersonName( person.getPersonFullName() );
              presentParticipants = presentParticipants + 1;
              list.add( participant );
			}
			parentBean.getSocialProgramView().setPresentParticipants( String.valueOf( presentParticipants ) );
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onMessageFromSearchPerson --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	public void onAddInternalParticipants()
	{
		try
		{
			updateValuesFromMaps();
			String javaScript ="javaScript:showPersonPopupForParticipants();";
			List<Long> idToExclude = new ArrayList<Long>( 0 );
			for (ProgramParticipantsView item : list) {
				if( item.getPersonId() != null && item.getIsDeleted() !=  null &&!item.getIsDeleted().equals("1") )
				{
					idToExclude.add( item.getPersonId() );
				}
			}
			if( idToExclude.size() > 0 )
			{
				sessionMap.put(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED,idToExclude);
			}
			openPopUp(javaScript);
			
		}
		catch ( Exception e )
		{
			logger.LogException( "onAddInternalParticipants --- EXCEPTION --- ", e );
			if( sessionMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED)!=null )
			{
				sessionMap.remove(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED);
			}
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		parentBean.setErrorMessages( errorMessages );
		}
	}
	public void openPopUp(String javaScript)
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        AddResourceFactory.getInstance(facesContext).addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript );
	}

	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() throws Exception
	{
		boolean valid = true;
		errorMessages = new ArrayList<String>(0);
		// VALIDATION CHECKS STARTS HERE
		if( programParticipants.getType() == null || programParticipants.getType().trim().length() <= 0 || 
			programParticipants.getType().trim().equals("-1"))
		{
		 errorMessages.add(ResourceUtil.getInstance().getProperty( "socialProgram.messages.participantTypeReq" ) );
		 valid = false;
		}
		if( programParticipants.getPersonId()==null && ( 
				                                  programParticipants.getExternalParticipantName() == null ||  
				                                  programParticipants.getExternalParticipantName().trim().length() <= 0 )	
		  )
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty( "socialProgram.messages.participantNameReq" ) );
	      valid = false;
		}
		
		parentBean.setErrorMessages( errorMessages );
		
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}

	
	@SuppressWarnings( "unchecked" )
	public String getListLoadedFromDb() {
		if( viewMap.get( "participantlistLoadedFromDb" ) != null )
		{
			listLoadedFromDb = viewMap.get( "participantlistLoadedFromDb" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "participantlistLoadedFromDb",this.listLoadedFromDb ); 
		}
	}

	public List<ProgramParticipantsView> getList() {
		return list;
	}

	public void setList(List<ProgramParticipantsView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public ProgramParticipantsView getProgramParticipants() {
		return programParticipants;
	}

	public void setProgramParticipants(ProgramParticipantsView programParticipants) {
		this.programParticipants = programParticipants;
	}

	public HtmlInputText getTxtExternalParticipantName() {
		return txtExternalParticipantName;
	}

	public void setTxtExternalParticipantName(
			HtmlInputText txtExternalParticipantName) {
		this.txtExternalParticipantName = txtExternalParticipantName;
	}

	public Boolean getMarkUnMarkAll() {
		if(viewMap.get("participantmarkUnMarkAll")!= null )
			markUnMarkAll = (Boolean)viewMap.get("participantmarkUnMarkAll");
		return markUnMarkAll;
		
	}

	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
		if( this.markUnMarkAll != null )
		{
			viewMap.put("participantmarkUnMarkAll",this.markUnMarkAll);
		}
	}

	public HtmlCommandButton getBtnAddInternalParticipants() {
		return btnAddInternalParticipants;
	}

	public void setBtnAddInternalParticipants(
			HtmlCommandButton btnAddInternalParticipants) {
		this.btnAddInternalParticipants = btnAddInternalParticipants;
	}

	public HtmlCommandButton getBtnPresent() {
		return btnPresent;
	}

	public void setBtnPresent(HtmlCommandButton btnPresent) {
		this.btnPresent = btnPresent;
	}

	public HtmlCommandButton getBtnAbsent() {
		return btnAbsent;
	}

	public void setBtnAbsent(HtmlCommandButton btnAbsent) {
		this.btnAbsent = btnAbsent;
	}

	public HtmlCommandButton getBtnAddParticipant() {
		return btnAddParticipant;
	}

	public void setBtnAddParticipant(HtmlCommandButton btnAddParticipant) {
		this.btnAddParticipant = btnAddParticipant;
	}
	

}
