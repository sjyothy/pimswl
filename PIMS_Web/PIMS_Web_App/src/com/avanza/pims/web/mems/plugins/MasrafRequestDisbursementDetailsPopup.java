package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.endowment.EndowmentDisbursementService;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class MasrafRequestDisbursementDetailsPopup extends AbstractMemsBean
{	
	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	List<DisbursementDetailsView > list = new ArrayList<DisbursementDetailsView>();
	private HtmlDataTable dataTable;
	public MasrafRequestDisbursementDetailsPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( viewMap.get( "list" ) != null )
		{
			list =  (ArrayList<DisbursementDetailsView>)viewMap.get( "list" );
		}
		else
		{
			Long masrafId = null;
			Long requestId = null;
			if( request.getParameter("masrafId") != null)
			{
				masrafId =  Long.valueOf( request.getParameter("masrafId").toString() );
			}
			if( request.getParameter("requestId") != null)
			{
				requestId =  new Long(request.getParameter("requestId").toString());
				if( requestId.compareTo(0l) <=0  )
				{
					requestId = null;
				}
			}
			List<Long> disDetIdNotIn = null;
			if( sessionMap .get("disDetIdNotIn") != null)
			{
				List<Long> split =  (ArrayList<Long>) sessionMap.remove( "disDetIdNotIn");
				disDetIdNotIn = new ArrayList<Long>();
				for (Long string : split)
				{
						disDetIdNotIn.add( string );
				}
			}
			list = EndowmentDisbursementService.getRequestDisbursementsForMasraf(masrafId, requestId, disDetIdNotIn);
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	public List<DisbursementDetailsView> getList() {
		return list;
	}

	public void setList(List<DisbursementDetailsView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}	
    

}
