package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class RecommFollowupBean extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private DomainDataView monthsDD = new DomainDataView();
	private DomainDataView yearsDD = new DomainDataView();
	private DomainDataView daysDD = new DomainDataView();
	@SuppressWarnings("unchecked")
	@Override
	public void init()
	{
		super.init();
		if(!isPostBack())
		{
			viewMap.put("canAddAttachment",true);
			if(sessionMap.get(WebConstants.PAGE_MODE) != null)
			{
				String pageMode = sessionMap.get(WebConstants.PAGE_MODE).toString();
				sessionMap.remove(WebConstants.PAGE_MODE);
				if(pageMode.compareTo(WebConstants.InheritanceFilePageMode.IS_POPUP) == 0)
					viewMap.put("IS_POPUP", true);
			}
			if(sessionMap.containsKey(WebConstants.RecommenationFollowup.RECOMMENDATION_ID))
			{
				Long recommId =new Long(sessionMap.get(WebConstants.RecommenationFollowup.RECOMMENDATION_ID).toString());
				viewMap.put(WebConstants.RecommenationFollowup.RECOMMENDATION_ID, recommId);
				viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID,recommId.toString());
				sessionMap.remove(WebConstants.RecommenationFollowup.RECOMMENDATION_ID);
			}	
			
			loadAttachments(null);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() 
	{
		super.prerender();
		if(viewMap.get(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB) != null)
		{
			List<String> msg = new ArrayList<String>();
			msg = (List<String>) viewMap.get(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB);
			errorMessages.addAll(msg);
			viewMap.remove(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB);
		}
	}
	
	public boolean isViewModePopup()
	{
		boolean isView = false;
		isView = viewMap.containsKey("IS_POPUP");
		return isView;
	}
	@SuppressWarnings("unchecked")
	public void loadAttachments(Long objectId)
	{
		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		
    	if(viewMap.get(WebConstants.RecommenationFollowup.RECOMMENDATION_ID) != null)
		{
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.RecommenationFollowup.EXTERNAL_ID);
			viewMap.put("noteowner", WebConstants.RecommenationFollowup.NOTES_OWNER);
		}
    	
		if(objectId!= null)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, objectId.toString());
			viewMap.put("entityId", objectId.toString());
		}
	}
	public void onSave()
	{
		MemsFollowupView followupToUpdate =null;
		if(viewMap.get("memsFollowupView") != null)
			followupToUpdate = (MemsFollowupView) viewMap.get("memsFollowupView"); 
			
	}
	public DomainDataView getMonthsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.MONTHS) != null)
			monthsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.MONTHS);
		return monthsDD;
	}

	@SuppressWarnings("unchecked")
	public void setMonthsDD(DomainDataView monthsDD) 
	{
		if(monthsDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.MONTHS,monthsDD);
		this.monthsDD = monthsDD;
	}

	public DomainDataView getYearsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.YEARS) != null)
			yearsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.YEARS);
		return yearsDD;
	}

	@SuppressWarnings("unchecked")
	public void setYearsDD(DomainDataView yearsDD) {
		if(yearsDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.YEARS,yearsDD);
		this.yearsDD = yearsDD;
	}

	public DomainDataView getDaysDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.DAYS) != null)
			daysDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.DAYS);
		return daysDD;
	}

	@SuppressWarnings("unchecked")
	public void setDaysDD(DomainDataView daysDD) {
		if(daysDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.DAYS,daysDD);
		this.daysDD = daysDD;
	}
}
