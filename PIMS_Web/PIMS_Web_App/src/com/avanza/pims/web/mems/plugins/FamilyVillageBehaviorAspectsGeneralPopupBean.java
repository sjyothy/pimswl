package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageBehavioralAspectService;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.vo.mems.BehaviorAspectsGeneralView;
import com.avanza.pims.ws.vo.mems.FamilyAspectFinancialView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBehaviorAspectsGeneralPopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List<String> angerManifestations;
	private List<String> angerReasons;
	private List<String> behaviorInPain;
	private List<String> behaviorRoutineChange;
	BehaviorAspectsGeneralView pageView;	
	
	private Long personId;
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageBehaviorAspectsGeneralPopupBean() 
	{
		pageView = new BehaviorAspectsGeneralView();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					pageView.setPersonId(personId);
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (BehaviorAspectsGeneralView)viewMap.get("pageView");
		}
		if(viewMap.get("angerManifestations") != null)
		{
			angerManifestations =  (ArrayList<String>)viewMap.get("angerManifestations");
		}
		
		if(viewMap.get("angerReasons") != null)
		{
			angerReasons =  (ArrayList<String>)viewMap.get("angerReasons");
		}
		if(viewMap.get("behaviorInPain") != null)
		{
			behaviorInPain =  (ArrayList<String>)viewMap.get("behaviorInPain");
		}
		if(viewMap.get("behaviorRoutineChange") != null)
		{
			behaviorRoutineChange =  (ArrayList<String>)viewMap.get("behaviorRoutineChange");
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
		if( angerManifestations != null && angerManifestations.size()>0)
		{
			viewMap.put("angerManifestations",angerManifestations);
		}
		if( angerReasons!= null && angerReasons.size()>0)
		{
			viewMap.put("angerReasons",angerReasons);
		}
		if( behaviorInPain!= null && behaviorInPain.size()>0)
		{
			viewMap.put("behaviorInPain",behaviorInPain);
		}
		if( behaviorRoutineChange != null && behaviorRoutineChange.size()>0)
		{
			viewMap.put("behaviorRoutineChange",behaviorRoutineChange);
		}
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		
//		if( pageView.getMonthlyIncomeIdString() == null || pageView.getMonthlyIncomeIdString().equals("-1")) {
//			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.monthlyIncomeRequired"));
//			return true;
//		}
//		
//		if( pageView.getMonthlyIncomeSourceId() == null || pageView.getMonthlyIncomeSourceId().equals("-1")) {
//			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.incomeSourceRequired"));
//			return true;
//		}
//		
//		if( pageView.getDescription() == null || pageView.getDescription().trim().length() <=0) {
//			errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.descriptionRequired "));
//			return true;
//		}
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			String csv = "";
			
			for (String eachangerManifestations : angerManifestations) {
				csv = csv + eachangerManifestations+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setAngerManifestation( csv.substring(0,csv.length()-1));
			}
			for (String eachangerReasons: angerReasons) {
				csv = csv + eachangerReasons+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setAngerReasons( csv.substring(0,csv.length()-1));
			}
			for (String eachbehaviorInPain: behaviorInPain) {
				csv = csv + eachbehaviorInPain+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setBehaviorInPain( csv.substring(0,csv.length()-1));
			}
			for (String eachbehaviorRoutineChange: behaviorRoutineChange) {
				csv = csv + eachbehaviorRoutineChange+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setBehaviorRoutineChange( csv.substring(0,csv.length()-1));
			}
			if(hasErrors())return;
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageBehavioralAspectService.addBehaviorAspectsGeneral( pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public BehaviorAspectsGeneralView getPageView() {
		return pageView;
	}

	public void setPageView(BehaviorAspectsGeneralView pageView) {
		this.pageView = pageView;
	}

	public List<String> getAngerManifestations() {
		return angerManifestations;
	}

	public void setAngerManifestations(List<String> angerManifestations) {
		this.angerManifestations = angerManifestations;
	}

	public List<String> getAngerReasons() {
		return angerReasons;
	}

	public void setAngerReasons(List<String> angerReasons) {
		this.angerReasons = angerReasons;
	}


	public List<String> getBehaviorInPain() {
		return behaviorInPain;
	}

	public void setBehaviorInPain(List<String> behaviorInPain) {
		this.behaviorInPain = behaviorInPain;
	}


	public List<String> getBehaviorRoutineChange() {
		return behaviorRoutineChange;
	}

	public void setBehaviorRoutineChange(List<String> behaviorRoutineChange) {
		this.behaviorRoutineChange = behaviorRoutineChange;
	}


}
