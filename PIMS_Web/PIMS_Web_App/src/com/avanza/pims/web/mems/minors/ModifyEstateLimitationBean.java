package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.proxy.MEMSModifyEstateLimitationBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class ModifyEstateLimitationBean extends AbstractMemsBean 
{

	public interface Page_Mode {
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String VIEW = "PAGE_MODE_VIEW";
		public static final String NEW = "NEW";
		public static final String SENT_BACK = "SENT_BACK";
		public static final String REV_REQ = "REV_REQ";
		public static final String APPROVAL_REQ = "APPROVAL_REQ";
		public static final String APPROVED = "APPROVED";
		public static final String REJECTED = "REJECTED";
		public static final String COMPLETED = "COMPLETED";
		public static final String RESEARCHER_ACK_REQUIRED = "RESEARCHER_ACK_REQUIRED";
		
	}
    public interface TAB_ID {
		public static final String ApplicationDetails="tabApplicationDetails";
		public static final String BasicInfo="tabModifyEstateLimitationInheritedAssets";
		public static final String Attachment="attachmentTab";
		public static final String Review ="tabReview";
		
	}
    private String pageMode="";
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    private String txtRemarks;
    private HtmlDataTable dataTableInheritedAsset;
    
    InheritanceFileService fileService= new InheritanceFileService();
    InheritanceFileView fileView = new InheritanceFileView();
    private RequestView requestView ;
    List<InheritedAssetView> inheritedAssetsList = new ArrayList<InheritedAssetView>();
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    
    @SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put("procedureType",WebConstants.ModifyEstateLimitation.PROCEDURE_TYPE );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.ModifyEstateLimitation.PROCEDURE_TYPE );
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if( sessionMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ) != null  )
		{
			setDataForFirstTime();
		}
		else if ( sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			getDataFromTaskList();
			getDataFromSearch();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW ) != null )
		{
			getDataFromSearch();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromTaskList()throws Exception
	{
		UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );  
		setUserTask( userTask );
		if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
		
		PropertyService ps = new PropertyService();
		requestView= ps.getRequestById( new Long( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) )  );
		
	}

	@SuppressWarnings( "unchecked" )
	private void refreshRequestView()throws Exception
	{
		requestView = RequestService.getModifyEstateLimitationRequestById( requestView.getRequestId() );
		getDataFromSearch();

	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		if( getRequestMap().get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView)getRequestMap().remove( WebConstants.REQUEST_VIEW );
		}
		loadInheritedAssetFromEstateLimitationRequestId();
		loadAttachmentsAndComments( requestView.getRequestId() );
		populateApplicationDetailsTab();
		getPageModeFromRequestStatus();
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime()throws Exception
	{
		long fileId = Long.valueOf( sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() );
		InheritanceFileView file = fileService.getInheritanceFileByFileId(fileId);
		requestView = new RequestView();
		requestView.setCreatedBy( getLoggedInUserId() );
		requestView.setUpdatedBy( getLoggedInUserId() );
		requestView.setInheritanceFileId(fileId );
		requestView.setInheritanceFileView( file );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.MODIFY_ESTATE_LIMITATION );
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		loadInheritedAssetFromInheritanceFileId();
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( Page_Mode.NEW );
		tabPanel.setSelectedTab( "commentsTab"  );
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null || 
			getStatusNew()
		  ) 
		{
			if(getStatusNew())
			{
				onAttachmentsCommentsClick();
			}
			return; 
		}
		else
		{
			tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
		}
		if( getStatusReviewRequired() )
		{
			setPageMode( Page_Mode.REV_REQ );
			tabPanel.setSelectedTab( TAB_ID.Review );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
		}
		if( getStatusRejectedResubmitted() )
		{
			setPageMode( Page_Mode.SENT_BACK );
			tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
		}
		else if( getStatusApprvalReq() || getStatusReviewDone() )
		{
			setPageMode( Page_Mode.APPROVAL_REQ );
			if( getStatusReviewDone() )
			{
				tabPanel.setSelectedTab( TAB_ID.Review );
			}
			else
			{
//				tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
				tabPanel.setSelectedTab( "commentsTab"  );
				onAttachmentsCommentsClick();
			}
		}
		else if( getStatusApproved() )
		{
			setPageMode( Page_Mode.APPROVED );
			tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
		}
		else if( getStatusRejected() )
		{
			setPageMode( Page_Mode.REJECTED  );
			tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
		}
		else if( getStatusCompleted() )
		{
			UserTask task = getUserTask(); 
			if(  task != null )
			{
				setPageMode( Page_Mode.RESEARCHER_ACK_REQUIRED );
			}
			else
			{
				setPageMode( Page_Mode.COMPLETED );
			}
			
			tabPanel.setSelectedTab( TAB_ID.BasicInfo  );
		}
		
	}

	
	/**
	 * @return
	 */
	public boolean getStatusCompleted() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0;
	}

	/**
	 * @return
	 */
	public boolean getStatusRejected() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REJECTED_ID ) == 0;
	}

	/**
	 * @return
	 */
	public boolean getStatusApproved() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_APPROVED_ID ) == 0;
	}

	/**
	 * @return
	 */
	public boolean getStatusApprvalReq() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID) == 0;
	}
	
	public boolean getStatusReviewRequired() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID ) == 0;
	}
	
	public boolean getStatusReviewDone() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}
	/**
	 * @return
	 */
	public boolean getStatusRejectedResubmitted() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID ) == 0;
	}
	
	public boolean getStatusNew() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_NEW_ID ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if(  viewMap.get( "InheritedAssetView" ) != null )
		{
			inheritedAssetsList = ( ArrayList<InheritedAssetView> )viewMap.get( "InheritedAssetView" );
		}
		updateValuesToMap();
	}
	
	
	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if(  inheritedAssetsList != null  )
		{
			viewMap.put( "InheritedAssetView", inheritedAssetsList  );
		}
	}

	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenManageAssetPopup()
	{
		
		try
		{
			updateValuesFromMap() ;
			InheritedAssetView inheritedAssetView  = (  InheritedAssetView  )dataTableInheritedAsset.getRowData();
			executeJavaScript(
								"openManageAssetPopup("+ inheritedAssetView .getAssetMemsView().getAssetId()+");"
							 );
		}
		catch ( Exception e )
		{
			logger.LogException( "onOpenManageAssetPopup--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenModifyAssetPopupForAdd()
	{
		
		try
		{
			updateValuesFromMap() ;
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, requestView.getInheritanceFileView() );
			executeJavaScript("onOpenModifyAssetPopup('add');");
		}
		catch ( Exception e )
		{
			logger.LogException( "onOpenModifyAssetPopupForAdd--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenModifyAssetPopup()
	{
		
		try
		{
			updateValuesFromMap() ;
			InheritedAssetView inheritedAssetView  = (  InheritedAssetView  )dataTableInheritedAsset.getRowData();
			sessionMap.put( WebConstants.ModifyEstateLimitation.InheritedAssetView , inheritedAssetView );
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, requestView.getInheritanceFileView() );
			if(pageMode.equals(Page_Mode.VIEW) || pageMode.equals(Page_Mode.COMPLETED) )
			{
				executeJavaScript("onOpenModifyAssetPopup('view');");
			}
			else
			{
				executeJavaScript("onOpenModifyAssetPopup('edit');");
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onOpenModifyAssetPopup--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDeleteAssetFromRequest()
	{
		try
		{
			InheritedAssetView inheritedAssetView  = (  InheritedAssetView  )dataTableInheritedAsset.getRowData();
			
			updateValuesFromMap() ;
			if(  requestView.getRequestId() == null || inheritedAssetView.getInheritedAssetId() == null )
			{
			  inheritedAssetsList.remove( inheritedAssetView );	
			}
			else
			{
				inheritedAssetView.setIsDeleted( 1L );
			}

			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteAssetFromRequest--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDisableAsset()
	{
		try
		{
			InheritedAssetView inheritedAssetView  = (  InheritedAssetView  )dataTableInheritedAsset.getRowData();
			
			updateValuesFromMap() ;

			DomainData dd = EntityManager.getBroker().findById(DomainData.class, WebConstants.InheritedAssetStatus.INACTIVE_ID );
			inheritedAssetView.setStatusAr(dd.getDataDescAr());
			inheritedAssetView.setStatusEn(dd.getDataDescEn());
			inheritedAssetView.setStatus( WebConstants.InheritedAssetStatus.INACTIVE_ID );

			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDisableAsset--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEnableAsset()
	{
		try
		{
			InheritedAssetView inheritedAssetView  = (  InheritedAssetView  )dataTableInheritedAsset.getRowData();
			updateValuesFromMap() ;
			
			DomainData dd = EntityManager.getBroker().findById(DomainData.class, WebConstants.InheritedAssetStatus.ACTIVE_ID );
			inheritedAssetView.setStatusAr(dd.getDataDescAr());
			inheritedAssetView.setStatusEn(dd.getDataDescEn());
			inheritedAssetView.setStatus( WebConstants.InheritedAssetStatus.ACTIVE_ID );
			
			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onEnableAsset--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromModifyAssetPopup()
	{
		try	
		{
			updateValuesFromMap();
			if( sessionMap.get( WebConstants.ModifyEstateLimitation.InheritedAssetView ) != null )
			{
				InheritedAssetView fromPopup = ( InheritedAssetView )sessionMap.remove( WebConstants.ModifyEstateLimitation.InheritedAssetView );
				int   i = 0 ;
				boolean hasAdded = false;
				for ( InheritedAssetView inheritedAsset : inheritedAssetsList )
				{
					if ( 
							(
							  fromPopup.getParentInheritedAssetId() == null &&
							  
							  inheritedAsset.getMyHashCode() == fromPopup.getMyHashCode()
							)
							||
							(
							  fromPopup.getParentInheritedAssetId() != null &&
							  inheritedAsset.getParentInheritedAssetId() != null && 
							  inheritedAsset.getParentInheritedAssetId().compareTo( fromPopup.getParentInheritedAssetId() ) == 0
					        )
						 
					   )
					{
					   
					   inheritedAssetsList.set( i, fromPopup );
					   hasAdded = true;
					   break;
					}
					i++;
				}
				if( !hasAdded )
				{
					fromPopup.setIsDeleted(Constant.DEFAULT_IS_DELETED);
					fromPopup.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
					inheritedAssetsList.add( 0, fromPopup );
				}
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromModifyAssetPopup|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		if ( isRequestNull && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( cmbReviewGroup.getValue() == null || StringHelper.isEmpty( cmbReviewGroup.getValue().toString().trim() ) || 
			this.cmbReviewGroup.getValue().toString().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( txtRemarks == null || StringHelper.isEmpty( txtRemarks.trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	public void onReview() 
	{
		try 
		{
				updateValuesFromMap();
				if( !isReviewValidated() )
				return ;
				try
				{
					
					
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					
						persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID );
						
						ReviewRequestView reviewRequestView = new ReviewRequestView();
						reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
						reviewRequestView.setCreatedOn( new Date() );
						reviewRequestView.setRfc( txtRemarks.toString().trim() );
						reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
						reviewRequestView.setRequestId( getRequestView().getRequestId() );
						
						UtilityService utilityService = new UtilityService();
						utilityService.persistReviewRequest( reviewRequestView );
						
						saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW );
						setTaskOutCome(  TaskOutcome.FORWARD ) ;
						
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
				txtRemarks = "";
				cmbReviewGroup.setValue("-1");
				refreshRequestView();
				setPageMode(Page_Mode.VIEW);
				updateValuesToMap();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_REQUIRED));
				
				
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onReviewDone() 
	{
		try 
		{
			updateValuesFromMap();
			
			try
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();

			    if( requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0 )
			    { 
				   persistRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID);
			    }
			    else
			    {
				   persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
			    }

				ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
				UtilityService utilityService = new UtilityService();
				utilityService.persistReviewRequest( reviewRequestView );
				
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_REVIEWED );
				
				setTaskOutCome( TaskOutcome.OK );
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch ( Exception e )
			{
				 ApplicationContext.getContext().getTxnContext().rollback();
				 throw e;
			}
			finally
			{
				 ApplicationContext.getContext().getTxnContext().release();
			}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}


	@SuppressWarnings( "unchecked")
	public void onApprove()  
	{
		try
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }			
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					persistRequest( WebConstants.REQUEST_STATUS_APPROVED_ID );
					saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL );
					setTaskOutCome( TaskOutcome.APPROVE);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL ));
		}
		catch ( Exception e )
		{
			 logger.LogException( "onApprove|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onReject()  
	{
		try
		{
			updateValuesFromMap();
			if( !hasRemarksProvided()  ){ return; }			
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					persistRequest( WebConstants.REQUEST_STATUS_REJECTED_ID  );
					saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_REJECTED );
					setTaskOutCome( TaskOutcome.CLOSE);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_REJECTED  ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onReject|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onSendBack()  
	{
		try
		{
			updateValuesFromMap();
			if( !hasRemarksProvided()  ){ return; }			
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					persistRequest( WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID );
					saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENTBACK );
					setTaskOutCome( TaskOutcome.REJECT );
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_SENTBACK  ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onApprove|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}

	
	@SuppressWarnings( "unchecked")
	public void onResubmitted()  
	{
		try
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }			
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					persistRequest( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
					saveCommentsAttachment( "request.event.sentForModifyEstateLimitation" );
					setTaskOutCome( TaskOutcome.OK );
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty( "request.event.sentForModifyEstateLimitation" ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onResubmitted|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onComplete()  
	{
		try
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }			
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					    requestView.setUpdatedBy( getLoggedInUserId() );
						new InheritanceFileService().completeModifyEstateLimitationRequest( requestView );
					    saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_APPROVED );
					    setTaskOutCome( TaskOutcome.APPROVE);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_APPROVED ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onComplete|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onSendBackFromComplete()  
	{
		try
		{
			updateValuesFromMap();
			if( !hasRemarksProvided()  ){ return; }
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					    requestView.setUpdatedBy( getLoggedInUserId() );
//						new InheritanceFileService().completeModifyEstateLimitationRequest( requestView );
					    persistRequest( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
					    saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENTBACK);
					    setTaskOutCome( TaskOutcome.REJECT);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_SENTBACK ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onComplete|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}

	@SuppressWarnings( "unchecked")
	public void onCancelFromComplete()  
	{
		try
		{
			updateValuesFromMap();
			if( !hasRemarksProvided()  ){ return; }
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					    requestView.setUpdatedBy( getLoggedInUserId() );
					    persistRequest( WebConstants.REQUEST_STATUS_REJECTED_ID );
					    saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_CANCELED);
					    setTaskOutCome( TaskOutcome.CLOSE);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_CANCELED ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onComplete|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}

	
	@SuppressWarnings( "unchecked")
	public void onResearcherDone()  
	{
		try
		{
			updateValuesFromMap();
				try
				{
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					persistRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID );
					saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_RESEARCHER_ACK );
					setTaskOutCome( TaskOutcome.OK);
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
			refreshRequestView();
			setPageMode(Page_Mode.VIEW);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.RequestEvents.REQUEST_RESEARCHER_ACK ) );
		}
		catch ( Exception e )
		{
			 logger.LogException( "onResearcherDone|Error Occured..",e );
			 errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	@SuppressWarnings( "unchecked")
	public void onSubmit()  
	{
		try
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }			
			
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSModifyEstateLimitation" );
			 MEMSModifyEstateLimitationBPELPortClient port=new MEMSModifyEstateLimitationBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
			 saveCommentsAttachment( "request.event.sentForModifyEstateLimitation" );
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 refreshRequestView();
			 setPageMode(Page_Mode.VIEW);
			 successMessages.add(
					 				ResourceUtil.getInstance().getProperty( "request.event.sentForModifyEstateLimitation" ) 
					 			);
			 updateValuesToMap();
			 
			 ApplicationContext.getContext().getTxnContext().commit() ;
		}
		catch ( Exception e )
		{
			ApplicationContext.getContext().getTxnContext().rollback() ;
			logger.LogException( "onSubmit|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private boolean hasRemarksProvided() throws Exception
	{
		errorMessages = new ArrayList<String>();
		if(txtRemarks == null || txtRemarks.trim().length() <= 0)
    	{
    		errorMessages.add(	ResourceUtil.getInstance().getProperty(	"endowmentProgram.msg.reasonRequired")  );
    		return false;
    	}

		return true;
		
	}
	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		boolean hasSelected = false;
		for ( InheritedAssetView inhAsset : inheritedAssetsList )
		{
			if ( inhAsset.getIsSelected() != null && inhAsset.getIsSelected() ) 
			{
				hasSelected = true;
				break;
			};
				
		}
		if( !hasSelected )
		{
			errorMessages.add(	ResourceUtil.getInstance().getProperty(	"modifyEstateLimitationAsset.msg.atleastOneAssetRequired" ) );
    		tabPanel.setSelectedTab(	TAB_ID.BasicInfo);
			hasSaveErrors  = true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(	ResourceUtil.getInstance().getProperty(	MessageConstants.Attachment.MSG_MANDATORY_DOCS	)  );
    		tabPanel.setSelectedTab(	TAB_ID.Attachment );
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked")
	public void onSave()  
	{
		try
		{
			updateValuesFromMap();
			if( hasSaveErrors() ) return;
			saveRequestInTransaction();
			refreshRequestView();
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.RequestEvents.REQUEST_SAVED ));
			
			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
            
			persistRequest( status );
			
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
	
		requestView.setOldStatusId(requestView.getStatusId());
		if(status!=null) requestView.setStatusId(status);
		List<InheritedAssetView > list  = new ArrayList<InheritedAssetView>();
		for ( InheritedAssetView inhAsset : inheritedAssetsList )
		{
			if ( inhAsset.getIsSelected() == null || !inhAsset.getIsSelected() ) continue;
			list.add( inhAsset );
				
		}
		requestView.setInheritedAssetsList( list );
		requestView.setUpdatedBy(getLoggedInUserId());
		requestView.setUpdatedOn(new Date());
		fileService.persistModifyEstateLimitationRequest( requestView );
		
	}
	
	@SuppressWarnings( "unchecked")
	public void onInheritedAssetsTab()  
	{
		try
		{
			updateValuesFromMap();
			if( requestView.getRequestId() == null )
			{
				loadInheritedAssetFromInheritanceFileId();
			}
			else
			{
				loadInheritedAssetFromEstateLimitationRequestId();
			}
				
			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onInheritedAssetsTab|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	
	@SuppressWarnings( "unchecked")
	private List<InheritedAssetView> loadInheritedAssetFromInheritanceFileId() throws Exception 
	{
		List<InheritedAssetView> iAssetList = new ArrayList<InheritedAssetView>();
		iAssetList = new InheritanceFileService().getInheritedAssetsByFileId( requestView.getInheritanceFileId() );
		if(iAssetList == null || iAssetList.size() <= 0)
		{ return null;}
		inheritedAssetsList = new ArrayList<InheritedAssetView>();
		
		for(InheritedAssetView old : iAssetList)
		{
			InheritedAssetView newAsset = new InheritedAssetView();
			newAsset.setMyHashCode( newAsset.hashCode() );
			newAsset.setIsSelected(true);
			newAsset.setIsDeleted(  Constant.DEFAULT_IS_DELETED );
			newAsset.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
			newAsset.setCreatedBy(getLoggedInUserId());
			newAsset.setUpdatedBy(getLoggedInUserId());
			AssetMemsView newAssetMems = new AssetMemsView ();
			getNewAssetMemsFromOldAssetMems( newAssetMems, old.getAssetMemsView() );
			newAsset.setAssetMemsView( newAssetMems );
			newAsset.setParentInheritedAssetId( old.getInheritedAssetId() );
			 
			newAsset.setStatus( old.getStatus() );
			newAsset.setStatusEn( old.getStatusEn() );
			newAsset.setStatusAr(  old.getStatusAr() );
			newAsset.setTotalShareValue(  old.getTotalShareValue() );
			newAsset.setTotalShareValueString(  old.getTotalShareValueString() );
			newAsset.setOldTotalShareValue(  old.getOldTotalShareValue()  );
			newAsset.setIsConfirm( old.getIsConfirm() );
			newAsset.setIsConfirmBool( old.getIsConfirmBool() );
			newAsset.setIsFinal(  old.getIsFinal() );
			if( newAsset.getAssetMemsView().getManagerId() == null )
			{
				newAsset.getAssetMemsView().setManagerName(CommonUtil.getBundleMessage("inheritanceFile.inheritedAssetTab.managerAmaf"));
			}
			inheritedAssetsList.add(newAsset);
		}
		return inheritedAssetsList;
	}
	

	@SuppressWarnings( "unchecked" )
	private void getNewAssetMemsFromOldAssetMems( AssetMemsView newAssetMems, AssetMemsView oldAssetMems ) throws Exception
	{
		newAssetMems.setAssetId( null );
		newAssetMems.setAccountName(  oldAssetMems.getAccountName() );
		newAssetMems.setAmount(  oldAssetMems.getAmount() );
		newAssetMems.setAmountString( oldAssetMems.getAmountString() );
		newAssetMems.setAssetNameAr(	oldAssetMems.getAssetNameAr()	);
		
		newAssetMems.setAssetNameEn( oldAssetMems.getAssetNameEn() );
		newAssetMems.setAssetNumber( oldAssetMems.getAssetNumber() );
		newAssetMems.setAssetStatus( oldAssetMems.getAssetStatus() );
		newAssetMems.setAssetTypeView( oldAssetMems.getAssetTypeView() );
		newAssetMems.setAssociatedCncrndDepttId( oldAssetMems.getAssociatedCncrndDepttId() );
		newAssetMems.setBankView( oldAssetMems.getBankView() );
		newAssetMems.setCompany( oldAssetMems.getCompany() );
		newAssetMems.setConcernedDepartmentViewList( oldAssetMems.getConcernedDepartmentViewList() );
		newAssetMems.setCreatedBy( getLoggedInUserId() );
		newAssetMems.setCreatedOn(new Date() );
		newAssetMems.setUpdatedBy( getLoggedInUserId() );
		newAssetMems.setUpdatedOn(new Date() );
		newAssetMems.setDescription( oldAssetMems.getDescription() );
		newAssetMems.setDevolution( oldAssetMems.getDevolution() );
		newAssetMems.setDueDate( oldAssetMems.getDueDate() );
		newAssetMems.setExpectedRevenue( oldAssetMems.getExpectedRevenue() );
		newAssetMems.setExpectedRevenueString( oldAssetMems.getExpectedRevenueString() );
		newAssetMems.setExtractedFromInheritedAsstId( oldAssetMems.getExtractedFromInheritedAsstId() );
		newAssetMems.setFieldNumber( oldAssetMems.getFieldNumber() );
		newAssetMems.setFieldParty( oldAssetMems.getFieldParty() );
		newAssetMems.setFieldStatus( oldAssetMems.getFieldStatus() );
		newAssetMems.setFieldType( oldAssetMems.getFieldType() );
		newAssetMems.setFromDate( oldAssetMems.getFromDate() );
		newAssetMems.setGovtDepttView( oldAssetMems.getGovtDepttView() );
		newAssetMems.setIncomeExpbool( oldAssetMems.isIncomeExpbool() );
		newAssetMems.setIsAmafAccount( oldAssetMems.getIsAmafAccount() );
		newAssetMems.setIsAmafAccountBool( oldAssetMems.getIsAmafAccountBool() );
		newAssetMems.setIsAmafAccountBool(  oldAssetMems.getIsAmafAccountBool() );
		newAssetMems.setIsDeleted( 0l );
		newAssetMems.setIsIncomeExp( oldAssetMems.getIsIncomeExp() );
		newAssetMems.setIsManagerAmaf( oldAssetMems.getManagerId() == null?1l:0l );
		newAssetMems.setLicenseName( oldAssetMems.getLicenseName() );
		newAssetMems.setManagerId( oldAssetMems.getManagerId() );
		newAssetMems.setManagerView( oldAssetMems.getManagerView() );
		newAssetMems.setMigrated( oldAssetMems.getMigrated() );
		newAssetMems.setMigratedOn(oldAssetMems.getMigratedOn() );
		newAssetMems.setOldAssetId( oldAssetMems.getAssetId() );
		newAssetMems.setRecordStatus ( 1l );
		newAssetMems.setRegionName( oldAssetMems.getRegionName()  );
		newAssetMems.setRegionView( oldAssetMems.getRegionView() );
		newAssetMems.setRevenueType( oldAssetMems.getRevenueType() );
		newAssetMems.setRevenueTypeIdString( oldAssetMems.getRevenueTypeIdString() );
		newAssetMems.setRevenueTypeString( oldAssetMems.getRevenueTypeString() );
		newAssetMems.setRevenueTypeStringAr( oldAssetMems.getRevenueTypeStringAr() );
		newAssetMems.setSelected( oldAssetMems.isSelected() );
		newAssetMems.setShareAmount( oldAssetMems.getShareAmount() );
		newAssetMems.setSharePercent( oldAssetMems.getSharePercent() );
		newAssetMems.setTotalAssetShareInFile( oldAssetMems.getTotalAssetShareInFile() );
		newAssetMems.setVehicleCategory( oldAssetMems.getVehicleCategory() );
		newAssetMems.setWeight( oldAssetMems.getWeight() );
		newAssetMems.setWeightString( oldAssetMems.getWeightString() );
	}
	
	@SuppressWarnings( "unchecked")
	private List<InheritedAssetView> loadInheritedAssetFromEstateLimitationRequestId() throws Exception 
	{
		List<InheritedAssetView> iAssetList = new ArrayList<InheritedAssetView>();
		
		iAssetList = new InheritanceFileService().getInheritedAssetsByEstateLimitationRequestId( requestView.getRequestId() );
		if(iAssetList == null || iAssetList.size() <= 0)
		{ return null;}
		
		inheritedAssetsList = new ArrayList<InheritedAssetView>();
		for(InheritedAssetView old : iAssetList)
		{
			old.setMyHashCode( old.hashCode() );
			old.setIsSelected(  true );
			old.setUpdatedBy(getLoggedInUserId());
			
			if(	old.getAssetMemsView() != null  )
			{
				if(	old.getAssetMemsView().getManagerId() == null)
				{
					old.getAssetMemsView().setManagerName(CommonUtil.getBundleMessage("inheritanceFile.inheritedAssetTab.managerAmaf"));
				}
			}
			inheritedAssetsList.add( old );
		}
		return inheritedAssetsList;
	}
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	  {
		  String notesOwner = WebConstants.REQUEST;
		  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	  }
	    	
    }
	
	@SuppressWarnings( "unchecked" )
	public void saveComments(  ) throws Exception
    {
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
    }
	
	@SuppressWarnings( "unchecked" )
	public void saveAttachments(String referenceId)throws Exception
    {
		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
    	CommonUtil.updateDocuments();
    }

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			updateValuesFromMap();
			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		    updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }

	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( Page_Mode.VIEW ) && 
			( getPageMode().equals( Page_Mode.NEW)  || getPageMode().equals( Page_Mode.SENT_BACK ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResearcherAckRequiredButton()
	{
		if( !getPageMode().equals( Page_Mode.VIEW ) && 
			 getPageMode().equals( Page_Mode.RESEARCHER_ACK_REQUIRED )   
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( Page_Mode.VIEW ) &&  getPageMode().equals( Page_Mode.NEW ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCompleteButton()
	{
		if(  !getPageMode().equals( Page_Mode.VIEW ) && getPageMode().equals( Page_Mode.APPROVED) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewDoneButton()
	{
		if(  !getPageMode().equals( Page_Mode.VIEW ) && getPageMode().equals( Page_Mode.REV_REQ ) )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproveRejectButton()
	{
		if(  !getPageMode().equals( Page_Mode.VIEW ) && getPageMode().equals( Page_Mode.APPROVAL_REQ)   )
		{
			return true;
		}
		return false;
	}

	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( Page_Mode.VIEW ) && getPageMode().equals( Page_Mode.SENT_BACK ) )
		{
			return true;
		}
		return false;
	}

	
	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public InheritanceFileView getFileView() {
		return fileView;
	}

	public void setFileView(InheritanceFileView fileView) {
		this.fileView = fileView;
	}

	public List<InheritedAssetView> getInheritedAssetsList() {
		return inheritedAssetsList;
	}

	public void setInheritedAssetsList(List<InheritedAssetView> inheritedAssetsList) {
		this.inheritedAssetsList = inheritedAssetsList;
	}

	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() 
	{
		if( viewMap.get("pageMode")!= null )
		{
			pageMode = viewMap.get("pageMode").toString();
		}
		return pageMode;
	}
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
		if( this.pageMode != null )
		{
			viewMap.put( "pageMode", this.pageMode );
		}
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public HtmlDataTable getDataTableInheritedAsset() {
		return dataTableInheritedAsset;
	}

	public void setDataTableInheritedAsset(HtmlDataTable dataTableInheritedAsset) {
		this.dataTableInheritedAsset = dataTableInheritedAsset;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}
}
