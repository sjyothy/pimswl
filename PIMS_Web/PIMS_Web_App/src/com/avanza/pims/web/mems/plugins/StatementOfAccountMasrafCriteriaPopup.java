package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class StatementOfAccountMasrafCriteriaPopup extends AbstractMemsBean
{	
	
	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	MasarifSearchView  masraf;
	private Date fromDate;
	private Date toDate;
	private Boolean showSubMasarif;
	public StatementOfAccountMasrafCriteriaPopup() 
	{
		masraf = new MasarifSearchView ();
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( sessionMap.get( WebConstants.Masraf.MasrafStatement ) != null)
		{
			masraf = ( MasarifSearchView )sessionMap.remove( WebConstants.Masraf.MasrafStatement)  ;
		}
		else if(  viewMap.get( WebConstants.Masraf.MasrafStatement ) != null )
		{
			masraf = ( MasarifSearchView )viewMap.remove( WebConstants.Masraf.MasrafStatement)  ;
		}
		if(this.toDate == null)
		{
			this.toDate= new Date();
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( masraf != null )
		{
			viewMap.put( WebConstants.Masraf.MasrafStatement, masraf);
		}
	}

	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccount()
	{
		try
		{
			updateValuesFromMaps();
			if( masraf == null || masraf.getMasrafId() == null ) return;
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(
																	ReportConstant.Report.STATEMENT_OF_ACCOUNT_MASRAF,
																	ReportConstant.Processor.STATEMENT_OF_ACCOUNT_MASRAF,
																	CommonUtil.getLoggedInUser()
																 );
			if( fromDate != null )
			{
				statmntReportCriteria.setFromDate( fromDate );
			}
			if(toDate != null )
			{
				statmntReportCriteria.setToDate( toDate ); 
			}
			else
			{
			    statmntReportCriteria.setToDate( new Date() ); 
			}
			if( showSubMasarif )
			{
				statmntReportCriteria.setShowSubMasrafTrx("1");
			}
			else
			{
				statmntReportCriteria.setShowSubMasrafTrx("0");
			}
			statmntReportCriteria.setMasrafId( masraf.getMasrafId() );

			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public MasarifSearchView getMasraf() {
		return masraf;
	}

	public void setMasraf(MasarifSearchView masraf) {
		this.masraf = masraf;
	}

	public Boolean getShowSubMasarif() {
		return showSubMasarif;
	}

	public void setShowSubMasarif(Boolean showSubMasarif) {
		this.showSubMasarif = showSubMasarif;
	}


}
