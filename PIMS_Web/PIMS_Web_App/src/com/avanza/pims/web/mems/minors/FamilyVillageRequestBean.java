package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.VillaStaff;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.FamilyVillageFileBeneficiariesTabBacking;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.FamilyVillageVillaView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class FamilyVillageRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(FamilyVillageRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_REV_REQ = "PAGE_MODE_REV_REQ";
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "PAGE_MODE_APPROVAL_REQUIRED";
	private static final String PAGE_MODE_REJECTED= "PAGE_MODE_REJECTED";
	private static final String PAGE_MODE_COMPLETED = "PAGE_MODE_COMPLETED";

	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
	private static final String TAB_BENEFICIARY ="beneficiaryTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;

    private String txtRemarks;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private RequestView requestView ;
	private List<SelectItem> familyVillageStaffList = new ArrayList<SelectItem>();
	private String selectOneFamilyVillageVillaStaff;
	private String selectOneVillaCategory;
	private String selectOneFamilyVillageVillaStaffRole;
    protected HtmlCommandButton btnSendBack =new HtmlCommandButton();
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    private VillaStaff villaStaffAdd = new VillaStaff();
    private List<VillaStaff> villaStaffList = new ArrayList<VillaStaff>();
    private HtmlDataTable dataTableVillaStaff;
	public FamilyVillageRequestBean(){}
	
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_FAMILY_VILLAGE_FILE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_FAMILY_VILLAGE_FILE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromRequestSearch();
		  
		}
		else if(sessionMap.get(WebConstants.InheritanceFile.FILE_ID)!= null)
		{
			getDataFromFileSearch(
					
										Long.valueOf( sessionMap.remove(WebConstants.InheritanceFile.FILE_ID).toString() )
			);
			
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{
		if(viewMap.get(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY) != null)
		{
			List<String> errMsg =(List<String>) viewMap.get(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_BENEFICIARY);
			viewMap.remove(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY);
		}
		else if(viewMap.get(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_BENEFICIARY);
			viewMap.remove(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY);
		}
	
		
	}
	public void prerender(){
		try{
				getErrorMessagesFromTab();
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( Constant.MemsRequestType.FAMILY_VILLAGE_FILE);
		InheritanceFileView fileView = new InheritanceFileView();
		fileView.setFileTypeId(String.valueOf( Constant.InheritanceFileType.FAMILY_VILLAGE_ID ) ) ;
		fileView.setFileTypeKey( Constant.InheritanceFileType.FAMILY_VILLAGE);
		fileView.setCreatedOn(new Date());
		fileView.setCreatedBy(  getLoggedInUserId()  );
		fileView.setUpdatedBy(  getLoggedInUserId()  );
		fileView.setUpdatedOn(new Date());
		fileView.setStatusId(WebConstants.InheritanceFileStatus.NEW_ID);
		FamilyVillageVillaView villaView = new FamilyVillageVillaView();
		fileView.setFamilyVillageVillaView(villaView);
		requestView.setInheritanceFileView(fileView);
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		else if(  getStatusApprovalRequired() || getStatusReviewDone() )
		{
			setPageMode(PAGE_MODE_APPROVAL_REQUIRED);
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			
		}
		else if(  getStatusReviewRequired() )
		{
			setPageMode(PAGE_MODE_REV_REQ );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab("tabReview" );
			
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab(TAB_APPLICANTS );
			
		}
		else if (
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED) == 0
				)
		{
			setPageMode(PAGE_MODE_REJECTED);
			tabPanel.setSelectedTab(TAB_APPLICANTS);
		}
		else if (getStatusCompleted() ||
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVED) == 0
				
				)
		{
			setPageMode(PAGE_MODE_COMPLETED);
			
			tabPanel.setSelectedTab("villaDetailsTab");
			onVillaStaffTab();
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusApprovalRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) == 0;
	}
	public boolean getStatusReviewRequired() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID ) == 0;
	}
	
	public boolean getStatusReviewDone() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
//		if(viewMap.get("selectOneFamilyVillageVillaStaff")!= null )
//		{
//			selectOneFamilyVillageVillaStaff = viewMap.get("selectOneFamilyVillageVillaStaff").toString();	
//		}
//		if(viewMap.get("selectOneFamilyVillageVillaStaffRole")!= null )
//		{
//			selectOneFamilyVillageVillaStaffRole = viewMap.get("selectOneFamilyVillageVillaStaffRole").toString();	
//		}
		if(viewMap.get("selectOneVillaCategory")!= null )
		{
			selectOneVillaCategory = viewMap.get("selectOneVillaCategory").toString();	
		}
		
		if(viewMap.get("familyVillageStaffList")!= null )
		{
			familyVillageStaffList = (ArrayList<SelectItem>)viewMap.get("familyVillageStaffList");	
		}
		if(viewMap.get("villaStaffAdd")!= null )
		{
			villaStaffAdd = (VillaStaff)viewMap.get("villaStaffAdd");	
		}
		
		if(viewMap.get("villaStaffList")!= null )
		{
			villaStaffList = (ArrayList<VillaStaff>)viewMap.get("villaStaffList");	
		}
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
//		if( selectOneFamilyVillageVillaStaff != null )
//		{
//		  viewMap.put(  "selectOneFamilyVillageVillaStaff", selectOneFamilyVillageVillaStaff);
//		}
//		if( selectOneFamilyVillageVillaStaffRole != null  )
//		{
//		  viewMap.put(  "selectOneFamilyVillageVillaStaffRole", selectOneFamilyVillageVillaStaffRole);
//		}
		if( familyVillageStaffList != null )
		{
		  viewMap.put(  "familyVillageStaffList", familyVillageStaffList);
		}
		if(villaStaffList != null)
		{
			viewMap.put("villaStaffList", villaStaffList );
			
		}		
		if( villaStaffAdd != null )
		{
		  viewMap.put(  "villaStaffAdd", villaStaffAdd);
		}
		if(selectOneVillaCategory!= null && !selectOneVillaCategory.equals("-1"))
		{
			viewMap.put("selectOneVillaCategory", selectOneVillaCategory);
		}
		

	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromFileSearch(Long fileId)throws Exception
	{
		Request request = FamilyVillageFileService.getOpenFileRequestForFileId(fileId);
		  getRequestDetails( request.getRequestId() );
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromRequestSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() || null == requestView.getApplicantView().getPersonId() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											( 
											requestView.getApplicantView()!= null &&
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId)
											);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getRequestById( id );
		InheritanceFileView inheritenceFileView = FamilyVillageFileService.getInheritanceFileDetails( requestView.getInheritanceFileView().getInheritanceFileId());
		this.selectOneVillaCategory = inheritenceFileView.getFamilyVillageVillaView().getVillaCategoryId().toString();
		requestView.setInheritanceFileView(inheritenceFileView);
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}

	
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromAddFamilyVillageBeneficiary()
	{
		try	
		{
            onBeneficiariesTab();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchVilla|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchVilla()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageVillaView villa = (FamilyVillageVillaView)sessionMap.get(WebConstants.SELECTED_ROW);
            requestView.getInheritanceFileView().setFamilyVillageVillaView(villa);
            selectOneVillaCategory = villa.getVillaCategoryId().toString();
            
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchVilla|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onVillaDetailsTab()
	{
		try	
		{
            updateValuesFromMap();       
            
//            familyVillageStaffList= new CommonUtil().getFamilyVillageVillaStaffGroup();
//			if(requestView !=null && requestView.getInheritanceFileId()!=null )
//			{
//			 villaStaffList =  FamilyVillageFileService.getVillaStaffList(requestView.getInheritanceFileId());
//			}
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onVillaDetailsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onVillaStaffTab()
	{
		try	
		{
            updateValuesFromMap();       
            
            familyVillageStaffList= new CommonUtil().getFamilyVillageVillaStaffGroup();
			if(requestView !=null && requestView.getInheritanceFileId()!=null )
			{
			 villaStaffList =  FamilyVillageFileService.getVillaStaffList(requestView.getInheritanceFileId());
			}
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onVillaStaffTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onBeneficiariesTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageFileBeneficiariesTabBacking bean = (FamilyVillageFileBeneficiariesTabBacking)getBean("pages$tabFamilyVillageFileBeneficiary");
            bean.populateTab(requestView);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onBeneficiariesTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings("unchecked")
	private boolean hasAddVillaDetailsErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAddVillaDetails()
	{
		try	
		{
			if(requestView == null || requestView.getRequestId() ==null) return;
		}
		catch(Exception ex)
		{
			logger.LogException("onAddVillaDetails|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings("unchecked")
	private boolean hasAddVillaStaffErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( selectOneFamilyVillageVillaStaff== null || selectOneFamilyVillageVillaStaff.equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.StaffNameRequired"));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( selectOneFamilyVillageVillaStaffRole== null || selectOneFamilyVillageVillaStaffRole.equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.StaffRoleRequired"));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onDeleteVillaStaff()
	{
		try	
		{
			updateValuesFromMap();
			VillaStaff  villaStaff= (VillaStaff)dataTableVillaStaff.getRowData();
			villaStaff.setIsDeleted(1l);
			villaStaff.setUpdatedBy(getLoggedInUserId());
			villaStaff.setUpdatedOn(new Date());
			EntityManager.getBroker().persist(villaStaff);
			villaStaffList.remove(villaStaff);
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onAddVillaStaff|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	@SuppressWarnings( "unchecked" )
	public void onAddVillaStaff()
	{
		try	
		{
			 updateValuesFromMap();
			if(hasAddVillaStaffErrors())return;
			if(requestView == null || requestView.getRequestId() ==null) {
				if( hasSaveErrors() ){ return; }
					saveRequestInTransaction();
			};
//			VillaStaff clonedPojo = (VillaStaff)BeanUtils.cloneBean(villaStaffAdd);
			VillaStaff pojo = new VillaStaff();
			pojo.setCreatedBy(getLoggedInUserId());
			pojo.setUpdatedBy(getLoggedInUserId());
			pojo.setUpdatedOn(new Date());
			pojo.setCreatedOn(new Date());
			pojo.setIsDeleted(0l);
			pojo.setIsDisabled(0l);
			InheritanceFile file  = new InheritanceFile();
			file.setInheritanceFileId(requestView.getInheritanceFileId());
			pojo.setInheritanceFile(file);
			pojo.setSecUser( 
									(UserDbImpl) com.avanza.core.security.SecurityManager.getUser(selectOneFamilyVillageVillaStaff) 
								 );
			pojo.setRole(
								EntityManager.getBroker().findById(DomainData.class, Long.valueOf(selectOneFamilyVillageVillaStaffRole ) ) 
							  );
			EntityManager.getBroker().persist(pojo);
			villaStaffList =  FamilyVillageFileService.getVillaStaffList(requestView.getInheritanceFileId());
//			villaStaffAdd = new VillaStaff();
			pojo=new VillaStaff();
			selectOneFamilyVillageVillaStaffRole ="-1";
			selectOneFamilyVillageVillaStaff = "-1";
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onAddVillaStaff|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	 
	
	@SuppressWarnings("unchecked")
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
//			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
//			return true;
//		}
		if( selectOneVillaCategory==null || selectOneVillaCategory.equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.villacategoryrequired"));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( requestView.getInheritanceFileView().getFamilyVillageVillaView().getVillaNumber()==null || 
			requestView.getInheritanceFileView().getFamilyVillageVillaView().getVillaNumber().trim().length()<=0 ) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.villanumberrequired"));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( requestView.getInheritanceFileView().getFamilyVillageVillaView().getVillaAddress() ==null || 
			requestView.getInheritanceFileView().getFamilyVillageVillaView().getVillaAddress().trim().length()<=0 ) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.villaaddressrequired"));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
//		if(villaStaffList==null || villaStaffList.size()<=0)
//		{
//			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.villaaddressrequired"));
//		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			if(requestView.getStatusId().compareTo(WebConstants.REQUEST_STATUS_NEW_ID) ==0 )
			{
				saveRequestInTransaction();
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			}
			else
			{
				persistRequest(null);
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_UPDATED );
			}
			getRequestDetails( requestView.getRequestId() );
			
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onSaveAfterApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			persistRequest(  null );
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_UPDATED );
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSaveAfterApprove --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = Constant.REQUEST_STATUS_NEW_ID;
            if(requestView.getStatusId() != null){status = null;}
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest( Long status ) throws Exception 
	{
		FamilyVillageFileService service = new FamilyVillageFileService();
		if(status!=null) requestView.setStatusId(status);
		requestView.setUpdatedBy(getLoggedInUserId());
		requestView.setUpdatedOn(new Date());
		if(selectOneVillaCategory != null && !selectOneVillaCategory.equals("-1"))
		{
			requestView.getInheritanceFileView().getFamilyVillageVillaView().setVillaCategoryId(Long.valueOf(selectOneVillaCategory));
		}
		service.persistFamilyVillageFile(requestView);
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 InheritanceFileView inheritanceFileView = requestView.getInheritanceFileView();
			 inheritanceFileView.setStatusId(Constant.InheritanceFileStatus.APPROVAL_REQUIRED_ID);
			 persistRequest( status );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.APPROVE);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSentBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.REJECT);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( cmbReviewGroup.getValue() == null || StringHelper.isEmpty( cmbReviewGroup.getValue().toString().trim() ) || 
			this.cmbReviewGroup.getValue().toString().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( txtRemarks == null || StringHelper.isEmpty( txtRemarks.trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	public void onReview() 
	{
		try 
		{
				updateValuesFromMap();
				if( !isReviewValidated() )
				return ;
				try
				{
					
					
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					
						persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID );
						
						ReviewRequestView reviewRequestView = new ReviewRequestView();
						reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
						reviewRequestView.setCreatedOn( new Date() );
						reviewRequestView.setRfc( txtRemarks.toString().trim() );
						reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
						reviewRequestView.setRequestId( getRequestView().getRequestId() );
						
						UtilityService utilityService = new UtilityService();
						utilityService.persistReviewRequest( reviewRequestView );
						
						saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW );
						setTaskOutCome(  TaskOutcome.FORWARD ) ;
						
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
				txtRemarks = "";
				cmbReviewGroup.setValue("-1");
				updateValuesToMap();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_REQUIRED));
				
				
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onReviewDone() 
	{
		try 
		{
			updateValuesFromMap();
			
			try
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
				ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
				UtilityService utilityService = new UtilityService();
				utilityService.persistReviewRequest( reviewRequestView );
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_REVIEWED );
				
				setTaskOutCome( TaskOutcome.OK );
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch ( Exception e )
			{
				 ApplicationContext.getContext().getTxnContext().rollback();
				 throw e;
			}
			finally
			{
				 ApplicationContext.getContext().getTxnContext().release();
			}

			
			updateValuesToMap();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}


	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onApproved()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_APPROVED_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		String event  =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
//			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 InheritanceFileView inheritanceFileView = requestView.getInheritanceFileView();
			 inheritanceFileView.setStatusId(Constant.InheritanceFileStatus.APPROVED_ID);
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApproved--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_REJECTED_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 InheritanceFileView inheritanceFileView = requestView.getInheritanceFileView();
			 inheritanceFileView.setStatusId(Constant.InheritanceFileStatus.REJECTED_ID);
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
//			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
//			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveAfterApproveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_COMPLETED)  )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewDoneButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_REV_REQ ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApprovalButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintButton()
	{
		if(  getStatusCompleted() )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

		public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public HtmlCommandButton getBtnSendBack() {
		return btnSendBack;
	}

	public void setBtnSendBack(HtmlCommandButton btnSendBack) {
		this.btnSendBack = btnSendBack;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}


	public List<SelectItem> getFamilyVillageStaffList() {
		return familyVillageStaffList;
	}


	public void setFamilyVillageStaffList(List<SelectItem> familyVillageStaffList) {
		this.familyVillageStaffList = familyVillageStaffList;
	}


	public String getSelectOneFamilyVillageVillaStaff() {
		return selectOneFamilyVillageVillaStaff;
	}


	public void setSelectOneFamilyVillageVillaStaff(
			String selectOneFamilyVillageVillaStaff) {
		this.selectOneFamilyVillageVillaStaff = selectOneFamilyVillageVillaStaff;
	}


	public String getSelectOneFamilyVillageVillaStaffRole() {
		return selectOneFamilyVillageVillaStaffRole;
	}


	public void setSelectOneFamilyVillageVillaStaffRole(
			String selectOneFamilyVillageVillaStaffRole) {
		this.selectOneFamilyVillageVillaStaffRole = selectOneFamilyVillageVillaStaffRole;
	}


	public List<VillaStaff> getVillaStaffList() {
		return villaStaffList;
	}


	public void setVillaStaffList(List<VillaStaff> villaStaffList) {
		this.villaStaffList = villaStaffList;
	}


	public HtmlDataTable getDataTableVillaStaff() {
		return dataTableVillaStaff;
	}


	public void setDataTableVillaStaff(HtmlDataTable dataTableVillaStaff) {
		this.dataTableVillaStaff = dataTableVillaStaff;
	}


	public String getSelectOneVillaCategory() {
		return selectOneVillaCategory;
	}


	public void setSelectOneVillaCategory(String selectOneVillaCategory) {
		this.selectOneVillaCategory = selectOneVillaCategory;
	}


	
	

}