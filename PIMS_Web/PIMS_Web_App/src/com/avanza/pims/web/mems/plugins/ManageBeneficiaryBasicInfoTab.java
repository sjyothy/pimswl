package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;

public class ManageBeneficiaryBasicInfoTab extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;
	private List<SelectItem> cityList;
	private String countryId;
	private StatementOfAccountCriteria reportCriteria=new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
	private HtmlSelectOneMenu cmbPassportType = new HtmlSelectOneMenu();
	
	private String newCC="",oldCC ="";
	@SuppressWarnings("unused")
	private PersonView beneficiary = new PersonView();

	public ManageBeneficiaryBasicInfoTab(){
		cityList = new ArrayList<SelectItem>();
	}
	
	public void init() {
		super.init();
		if (!isPostBack()) {
			try {
				
				if(viewMap.get(WebConstants.PERSON_ID)!=null)
				{
					fillPersonView(Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString()));
				}
				
			} catch (PimsBusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void prerender() {
		super.prerender();
	}

	public PersonView getBeneficiary() {
		if (viewMap.get("BENEFICIARY_PERSON_VIEW") != null) {
			beneficiary = (PersonView) viewMap.get("BENEFICIARY_PERSON_VIEW");
		}
		return beneficiary;
	}

	public void setBeneficiary(PersonView beneficiary) {
		this.beneficiary = beneficiary;
	}

	public void saveFile() {

		HashMap<String, Object> fileMap = null;
		
		PersonView filePerson = (PersonView) viewMap.get("BENEFICIARY_PERSON_VIEW");
		ManageBeneficiaryBean manageBene = (ManageBeneficiaryBean) getBean("pages$ManageBeneficiary");
		//check if account number 
		if(StringHelper.isNotEmpty(filePerson.getAccountNumber()) && filePerson.getBankId().compareTo("-1") == 0){
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectBank"));
			manageBene.setErrorMessages(errorMessages);
			return ;
		}
		try {
			InheritanceFileService inheritanceFileService = new InheritanceFileService();
			inheritanceFileService.updatePersonWithContactInfo(filePerson);
			successMessages.add(CommonUtil.getBundleMessage("manageBeneficiary.successMsg.beneUpdated"));
			manageBene.setSuccessMessages(successMessages);

		} catch (Exception e) {
			logger.LogException("saveFile crashed", e);
			e.printStackTrace();
		}

	}

	public void fillPersonView(Long personId) throws PimsBusinessException {
		
		
/*		
		CollectionTrx collectionTrx = new CollectionTrx();
		CollectionTrxDetail collectionTrxDetail = new  CollectionTrxDetail();
		PaymentDetails paymentDetails = new PaymentDetails();
		Request request = new Request();
		
		CollectionDistributionManager cm = new CollectionDistributionManager();
		cm.saveAndUpdateCollection(collectionTrx, collectionTrxDetail, paymentDetails, request);*/

		if(personId!=null)
		{
		PersonView personDetail = new PersonView();
		personDetail = new PropertyService().getPersonInformationById(personId);
		if(personDetail.isMasroom())
			cmbPassportType.setValue("Masroom");
		else
			cmbPassportType.setValue("Passport");
				
		if( personDetail.getCountryId() != null )
		{
		 loadCity(Long.parseLong(personDetail.getCountryId()));
		}
		viewMap.put("BENEFICIARY_PERSON_VIEW", personDetail);
		personId = null;
		}
	}

	private void loadCityListByCountryId(String countryId2) {
		
		
	}

	public void countryChanged() {
		if (beneficiary.getCountryId() != null)
			loadCity(Long.parseLong(beneficiary.getCountryId()));
	}
	
	

	public void loadCity(Long selectedCoutryId) 
	{
		String methodName = "loadCity";
		logger.logInfo(methodName + "|" + "Start");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try {

			Set<RegionView> regionViewList = null;
			if (selectedCoutryId != null
					&& selectedCoutryId.compareTo(new Long(-1)) != 0) {
				regionViewList = psa.getCity(selectedCoutryId);
				Iterator itrator = regionViewList.iterator();
				for (int i = 0; i < regionViewList.size(); i++) {
					RegionView rV = (RegionView) itrator.next();
					SelectItem item;
					if (CommonUtil.getIsEnglishLocale()) {
						item = new SelectItem(rV.getRegionId().toString(), rV
								.getDescriptionEn());
					} else
						item = new SelectItem(rV.getRegionId().toString(), rV
								.getDescriptionAr());

					cmbCityList.add(item);
				}
				Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);

				viewMap.put(WebConstants.InheritanceFile.CITY_LIST,
							cmbCityList);

				logger.logInfo(methodName + "|" + "Finish");
			}
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);

		}

	}

	public List<SelectItem> getCityList() {
		if (viewMap.get(WebConstants.InheritanceFile.CITY_LIST) != null)
			cityList = (List<SelectItem>) viewMap
					.get(WebConstants.InheritanceFile.CITY_LIST);
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}
	

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	public void openStatmntOfAccount()
	{
		Long personId=Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString());
		if (personId!=null)
		
		{
			reportCriteria.setPersonId(personId);
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	public void onAdultFromMinor() 
	{
		try 
		{
			Long personId=Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString());
			if ( personId == null ) return;
			CommonUtil.onAdultFromMinor(personId, getLoggedInUserId());
			beneficiary  = getBeneficiary();
			beneficiary.setMinor(false);
			setBeneficiary(beneficiary);
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onAdultFromMinor --- CRASHED --- ", exception);
		}
	}
	
	public void onMinorFromAdult() 
	{
		try 
		{
			Long personId=Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString());
			if ( personId == null ) return;
			CommonUtil.onMinorFromAdult( personId, getLoggedInUserId() );
			beneficiary  = getBeneficiary();
			beneficiary.setMinor(true);
			setBeneficiary(beneficiary);
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onMinorFromAdult --- CRASHED --- ", exception);
		}
	}


	private void executeJavascript(String javascript) 
	{
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "executeJavascript --- CRASHED --- ", exception);
		}
	}

	public String getNewCC() {
		if(viewMap.get("CC_NEW") != null)
			newCC = viewMap.get("CC_NEW").toString();
		return newCC;
	}

	public void setNewCC(String newCC) {
		this.newCC = newCC;
	}

	public String getOldCC() {
		if(viewMap.get("CC_OLD") != null)
			oldCC = viewMap.get("CC_OLD").toString();
		return oldCC;
	}

	public void setOldCC(String oldCC) {
		this.oldCC = oldCC;
	}
	
	public void passportTypeChanged() {
		if (cmbPassportType.getValue() != null ){
			if(cmbPassportType.getValue().toString().equals("Passport"))
				beneficiary.setMasroom(false);
			else
				beneficiary.setMasroom(true);
			
		}
	}

	public HtmlSelectOneMenu getCmbPassportType() {
		return cmbPassportType;
	}

	public void setCmbPassportType(HtmlSelectOneMenu cmbPassportType) {
		this.cmbPassportType = cmbPassportType;
	}
	

}
