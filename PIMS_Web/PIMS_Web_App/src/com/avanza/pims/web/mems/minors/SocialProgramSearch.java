package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.mems.SocialProgramView;
import com.avanza.ui.util.ResourceUtil;



public class SocialProgramSearch extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(SocialProgramSearch.class);
	
	private HtmlDataTable dataTable;
	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String  VIEW_MODE="pageMode";
	private String  MODE_SELECT_ONE_POPUP="MODE_SELECT_ONE_POPUP";
	private String  MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP";
	private String  DEFAULT_SORT_FIELD = "programNum";	
    private com.avanza.pims.Utils.SocialProgramSearch searchCriteria = new com.avanza.pims.Utils.SocialProgramSearch();	
    private List<SocialProgramView> list = new ArrayList<SocialProgramView>();
 
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
    public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
     @SuppressWarnings( "unchecked" )
     @Override 
	 public void init() 
     {    	
    	 super.init();
    	 
    	 try
    	 {
              HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();	
		     if(!isPostBack())
		     {
		    	 	setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
			        setSortField(DEFAULT_SORT_FIELD);
			        setSortItemListAscending(true);
		    	 
		    	 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    
	      	    	}
		          
		    	 }
		     }
		    		 
    	 }
    	 catch(Exception es)
    	 {
    		 errorMessages = new ArrayList<String>(0);
     		logger.LogException("init"+"|Error Occured", es);
     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		 
    	 }
	        
	 }




		//We have to look for the list population for facilities
	private List<String> errorMessages;
	
  
		
	public String doAdd()
	{
		return "ADD";
	}
	public void doSearch (){
		try {
  	       
		     loadDataList();
		}
	    catch (Exception e){
	    	errorMessages = new ArrayList<String>(0);
     		logger.LogException("doSearch|Error Occured", e);
     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    }
	}
	
	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}
	@SuppressWarnings( "unchecked" )
	public List<SocialProgramView> loadDataList() throws Exception 
	{
			SocialProgramService service = new SocialProgramService();
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  service.getSocialProgramRecordSize( searchCriteria );
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			list =  service.getAllSocialPrograms(searchCriteria ,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
			if(list.isEmpty() || list==null)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));	
			}
			this.setList( list );
			forPaging(totalRows);
	    
			return list;
	}
	@SuppressWarnings( "unchecked" )
	private void forPaging(int totalRows) {
		recordSize = totalRows;
		this.setRecordSize( totalRows );
		paginatorRows = getPaginatorRows();
		paginatorMaxPages = recordSize/paginatorRows;
		if((recordSize%paginatorRows)>0)
			paginatorMaxPages++;
		if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
	}
		
		
	
	@SuppressWarnings( "unchecked" )
	public String onEdit()
	{
		try
		{
			SocialProgramView view = (SocialProgramView) this.dataTable.getRowData();
			sessionMap.put(WebConstants.SocialSolidarityProgram.SOCIAL_PROGRAM_VO_FROM_SEARCH, view);
			
		}
		catch (Exception e){
	    	errorMessages = new ArrayList<String>(0);
     		logger.LogException("onEdit"+"|Error Occured", e);
     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    }
		return "editSocialProgram";
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	

	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	
		public boolean getIsPageModeSelectOnePopUp()
	{
		isPageModeSelectOnePopUp= false;
		if(viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE)!=null && viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_ONE_POPUP))
		{
			isPageModeSelectOnePopUp= true;
		}
		return isPageModeSelectOnePopUp;
		
	}
	public boolean getIsViewModePopUp()
	{
		boolean returnVal = false;
		
		if(viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE)!=null)
		{
			if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())		
				returnVal =  true;
		}
		 
		else 
		{
			returnVal =  false;
		}
		
		return returnVal;
		
	}
	public boolean getIsPageModeSelectManyPopUp()
	{
		isPageModeSelectManyPopUp= false;
		if(viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE)!=null && viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_MANY_POPUP))
		{
			isPageModeSelectManyPopUp= true;
		}
		return isPageModeSelectManyPopUp;
	}
	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;
	private boolean sortAscending = true;
	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");
		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}
		// Sort results.
		if (sortField != null) {
			Collections.sort(this.getList(), new ListComparator(sortField,
					sortAscending));
		}
	}
	
	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getMODE_SELECT_ONE_POPUP() {
		return MODE_SELECT_ONE_POPUP;
	}

	public void setMODE_SELECT_ONE_POPUP(String mode_select_one_popup) {
		MODE_SELECT_ONE_POPUP = mode_select_one_popup;
	}

	public String getMODE_SELECT_MANY_POPUP() {
		return MODE_SELECT_MANY_POPUP;
	}

	public void setMODE_SELECT_MANY_POPUP(String mode_select_many_popup) {
		MODE_SELECT_MANY_POPUP = mode_select_many_popup;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings( "unchecked" )
	public Integer getRecordSize() {
		if ( viewMap.get("recordSize")!= null )
		{
		  recordSize = (Integer) viewMap.get("recordSize");
		}
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	@SuppressWarnings( "unchecked" )
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if( this.recordSize != null )
		viewMap.put("recordSize", this.recordSize );
	}
	
	
	public boolean isSortAscending() {
		return sortAscending;
	}
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	public com.avanza.pims.Utils.SocialProgramSearch getSearchCriteria() {
		return searchCriteria;
	}
	public void setSearchCriteria(
			com.avanza.pims.Utils.SocialProgramSearch searchCriteria) {
		this.searchCriteria = searchCriteria;
	}
	@SuppressWarnings( "unchecked" )
	public List<SocialProgramView> getList() {
		if ( viewMap.get( "SocialProgramList") != null )
		{
			list = (ArrayList<SocialProgramView>)viewMap.get( "SocialProgramList") ;
		}
		return list;
	}
	@SuppressWarnings( "unchecked" )
	public void setList(List<SocialProgramView> list) {
		this.list = list;
		if( this.list != null )
		{
			viewMap.put( "SocialProgramList",  this.list); 
			
		}
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	
		
}
