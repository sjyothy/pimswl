package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.InheritanceFileBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.OfficialCorrespondenceView;

public class OfficialCorrespondenceTabBean extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private List<OfficialCorrespondenceView> officialCorresList;
	private HtmlDataTable officialCorresDataTable;
	private HtmlSelectOneMenu cboGovtDepttId;
	private HtmlSelectOneMenu cboBankId; 
	private HtmlInputTextarea txtArDescription;
	private HtmlInputText txtFilePath;
	private HtmlInputFileUpload fileUploadCtrl;
	private UploadedFile selectedFile;
	private final String  CORRESPONDENCE_LIST;
	private final String  TASK_TYPE;
    DomainDataView offCorresOpen;
    DomainDataView offCorresClose;
    private final String noteOwner ;
	private final String externalId;
	private final String procedureTypeKey ;
	
	
	public OfficialCorrespondenceTabBean() 
	{
		officialCorresList = new ArrayList<OfficialCorrespondenceView>();
		cboGovtDepttId = new HtmlSelectOneMenu();
		cboBankId = new HtmlSelectOneMenu();
		txtArDescription = new HtmlInputTextarea();
		txtFilePath = new HtmlInputText();
	    CORRESPONDENCE_LIST = "CORRESPONDENCE_LIST";
	    offCorresOpen = new DomainDataView();
	    offCorresClose = new DomainDataView();
	    noteOwner 					= WebConstants.InheritanceFile.NOTES_OWNER;
		externalId 					= WebConstants.InheritanceFile.EXTERNAL_ID_OFFICIAL_CORRES;
		procedureTypeKey 			= WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE;
		TASK_TYPE = "TASK_TYPE";
	} 
	public void init() 
	{
		
		super.init();
		if (!isPostBack()) 
		{
//			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
//		Above code has been commented bcoz it was reinitializing the viewMap values that are put by inheritanceFileBean	
		}
	}
	@SuppressWarnings("unchecked")
	public List<OfficialCorrespondenceView> getOfficialCorresList() 
	{
		if(viewMap.get(CORRESPONDENCE_LIST) != null)
			officialCorresList = (List<OfficialCorrespondenceView>) viewMap.get(CORRESPONDENCE_LIST); 
		return officialCorresList;
	}
	@SuppressWarnings("unchecked")
	public void setOfficialCorresList(List<OfficialCorrespondenceView> officialCorresList) 
	{
		if(officialCorresList != null && officialCorresList.size() > 0)
			viewMap.put(CORRESPONDENCE_LIST, officialCorresList);
		this.officialCorresList = officialCorresList;
	}
	public HtmlDataTable getOfficialCorresDataTable() {
		return officialCorresDataTable;
	}
	public void setOfficialCorresDataTable(HtmlDataTable officialCorresDataTable) {
		this.officialCorresDataTable = officialCorresDataTable;
	}
	public HtmlSelectOneMenu getCboGovtDepttId() {
		return cboGovtDepttId;
	}
	public void setCboGovtDepttId(HtmlSelectOneMenu cboGovtDepttId) {
		this.cboGovtDepttId = cboGovtDepttId;
	}
	public HtmlSelectOneMenu getCboBankId() {
		return cboBankId;
	}
	public void setCboBankId(HtmlSelectOneMenu cboBankId) {
		this.cboBankId = cboBankId;
	}
	public HtmlInputTextarea getTxtArDescription() {
		return txtArDescription;
	}
	public void setTxtArDescription(HtmlInputTextarea txtArDescription) {
		this.txtArDescription = txtArDescription;
	}
	public HtmlInputText getTxtFilePath() {
		return txtFilePath;
	}
	public void setTxtFilePath(HtmlInputText txtFilePath) {
		this.txtFilePath = txtFilePath;
	}
	@SuppressWarnings("unchecked")
	public void saveCorrespondence()
	{
		if(isValidated())
		{
			try
			{
				
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				
				officialCorresList = getOfficialCorresList();
				OfficialCorrespondenceView offCorresView;
				offCorresView = getFilledOffCorrspndncView();
				offCorresView = new InheritanceFileService().saveCorrespondence(offCorresView);
				
				if(fileUploadCtrl.getUploadedFile() != null)
				{
					//For Document
					viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
					viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
					viewMap.put("noteowner",WebConstants.InheritanceFile.NOTES_OWNER);
					viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, offCorresView.getOffCorrId().toString());
					viewMap.put("entityId", offCorresView.getOffCorrId());
					//For Document				
					String documentTitle = CommonUtil.getBundleMessage("mems.inheritanceFile.officialCoresTab.docTitle");
					DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), documentTitle.toString(), null);
					if(documentView == null)
					{
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.filenotUploaded"));
						viewMap.put(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR, errorMessages);
					}
				}
				
				ApplicationContext.getContext().getTxnContext().commit();
//				setFieldsForGrid();
				InheritanceFileBean filebean = (InheritanceFileBean) getBean("pages$inheritanceFile");
				filebean.officialCorrespondenceTabClick();
				
//				getOfficialCorresList();
				clearFields();
			}
			catch (Exception e) 
			{
				logger.LogException("saveCorrespondence Carshed|",e);
				ApplicationContext.getContext().getTxnContext().rollback();
			}
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
		}
	}
	private void clearFields() 
	{
		cboBankId.setValue("-1");
		cboGovtDepttId.setValue("-1");
		txtArDescription.setValue(null);
	}
	private OfficialCorrespondenceView getFilledOffCorrspndncView() throws Exception 
	{
		OfficialCorrespondenceView view = new  OfficialCorrespondenceView();
		Long fileId = null;
		
		if(viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null)
			fileId = new Long(viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
		
		if(cboBankId.getValue().toString().compareTo("-1") != 0)
			view.getBank().setBankId(new Long(cboBankId.getValue().toString()));
		else
			view.getGovtDeptt().setGovtDepttId(new Long(cboGovtDepttId.getValue().toString()));
		view.setOffCorrId(null);
		view.setUpdatedBy(CommonUtil.getLoggedInUser());
		view.setUpdatedOn(new Date());
		view.setCreatedBy(CommonUtil.getLoggedInUser());
		view.setCreatedOn(new Date());
		view.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		view.setDescription(txtArDescription.getValue().toString());
		view.getInheritanceFile().setInheritanceFileId(fileId );
		
		if(getOffCorresOpen() != null)
			{
				DomainDataView ddOpen = getOffCorresOpen();
				view.setStatus(ddOpen.getDomainDataId());
				view.setStatusEn(ddOpen.getDataDescEn());
				view.setStatusAr(ddOpen.getDataDescAr());
			}
		
		UserView user = new UtilityService().getUserByLoginId(CommonUtil.getLoggedInUser());
		if(isEnglishLocale())
			view.setCreatedByName(user.getFullNamePrimary());
		else
			view.setCreatedByName(user.getFullNameSecondary());
		return view;
	}
	
//	private OfficialCorrespondenceView getFieldsForGrid(OfficialCorrespondenceView view) throws Exception 
//	{
//		List<OfficialCorrespondenceView> offCorrrList = new ArrayList<OfficialCorrespondenceView>();
//		offCorrrList = new InheritanceFileService().getOffCorrespondence(view);
//		view = offCorrrList.get(0);
//		if(getOffCorresOpen() != null)
//			{
//				DomainDataView ddOpen = getOffCorresOpen();
//				view.setStatus(ddOpen.getDomainDataId());
//				view.setStatusEn(ddOpen.getDataDescEn());
//				view.setStatusAr(ddOpen.getDataDescAr());
//			}
//		
//		UserView user = new UtilityService().getUserByLoginId(CommonUtil.getLoggedInUser());
//		if(isEnglishLocale())
//			view.setCreatedByName(user.getFullNamePrimary());
//		else
//			view.setCreatedByName(user.getFullNameSecondary());
//		return view;
//	}
	@SuppressWarnings("unchecked")
	private boolean isValidated() 
	{
		boolean validate = true;
		if(cboBankId.getValue() != null && cboGovtDepttId.getValue() != null)
		{
			String bankId = cboBankId.getValue().toString();
			String govtDepttId = cboGovtDepttId.getValue().toString();
			if(bankId.compareTo("-1") == 0 && govtDepttId.compareTo("-1") == 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
			}
			else
			if(bankId.compareTo("-1") != 0 && govtDepttId.compareTo("-1") != 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
			}
		}
		
		if(txtArDescription.getValue() == null || txtArDescription.getValue().toString().length() <= 0 )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.descriptionReq"));
		}
		else
		if(txtArDescription.getValue().toString().length() > 250 )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.descriptionTooLarge"));
		}	
//		if(fileUploadCtrl.getUploadedFile()==null)
//        {
//			validate = false;
//        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
//        }
		if(errorMessages != null && errorMessages.size() > 0)
		{
			viewMap.put(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR, errorMessages);
		}
		return validate;
	}
	@SuppressWarnings("unchecked")
	public void closeCorrespondence()
	{
		officialCorresList = getOfficialCorresList();
		List<OfficialCorrespondenceView> correspndncToClose = new ArrayList<OfficialCorrespondenceView>();
		if(officialCorresList != null && officialCorresList.size() > 0)
		{
			for(OfficialCorrespondenceView view : officialCorresList)
			{
				if(view.isSelected())
					{
						view.setStatus(getOffCorresClose().getDomainDataId());
						view.setStatusEn(getOffCorresClose().getDataDescEn());
						view.setStatusAr(getOffCorresClose().getDataDescAr());
						view.setOpened(false);
						correspndncToClose.add(view);
					}
			}
		}
		if(correspndncToClose == null || correspndncToClose.size() <= 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.selectCrspndncToClose"));
			viewMap.put(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR, errorMessages);
		}
		else
		{
			try 
			{
				new InheritanceFileService().closeCorrespondences(correspndncToClose);
			} catch (Exception e) {
				logger.LogException("closeCorrespondence crashed", e);
			}
		}
	}
	public DomainDataView getOffCorresOpen() 
	{
		if(viewMap.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN) != null)
			offCorresOpen = (DomainDataView) viewMap.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN);
		return offCorresOpen;
	}
	public void setOffCorresOpen(DomainDataView offCorresOpen) {
		this.offCorresOpen = offCorresOpen;
	}
	public DomainDataView getOffCorresClose() 
	{
		if(viewMap.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED) != null)
			offCorresClose = (DomainDataView) viewMap.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED);
		return offCorresClose;
	}
	public void setOffCorresClose(DomainDataView offCorresClose) {
		this.offCorresClose = offCorresClose;
	}
	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}
	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}
	public UploadedFile getSelectedFile() {
		return selectedFile;
	}
	public void setSelectedFile(UploadedFile selectedFile) {
		this.selectedFile = selectedFile;
	}
	public void deleteOffCorrspndnc()
	{
		OfficialCorrespondenceView correspondenceToBeDeleted = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
		int indexToDelete = officialCorresDataTable.getRowIndex();
		try 
		{
			new InheritanceFileService().deleteOfficialCorrspndnc(correspondenceToBeDeleted.getOffCorrId());
			officialCorresList.remove(indexToDelete);
			setOfficialCorresList(officialCorresList);
		} 
		catch (Exception e) 
		{
			logger.LogException("deleteOffCorrspndnc Crahshed", e);
		}
	}
	@SuppressWarnings("unchecked")
	public String downloadFile()
	{
		try {
				OfficialCorrespondenceView offView = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
				if(offView.getDocument() != null && offView.getDocument().getDocumentId() != null)
				{
					sessionMap.put("documentId",  offView.getDocument().getDocumentId().toString());
				}
							
	     } catch (Exception e) {			
	    	logger.LogException("downloadFile crashed |", e);
		}
		return "DownloadFile";
	}
	@SuppressWarnings("unchecked")
	public void setFieldsForGrid()
	{
		if(viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null)
		{
			long fileId = new Long(viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
			List<OfficialCorrespondenceView> officialCorresList = new ArrayList<OfficialCorrespondenceView>();
		try {
				officialCorresList = new InheritanceFileService().getAllOffCorrByFileId(fileId);
				if(officialCorresList != null && officialCorresList.size() > 0)
				{
					for(OfficialCorrespondenceView offCorr :  officialCorresList)
					{
						UserView user = new UtilityService().getUserByLoginId(offCorr.getCreatedBy());
						if(isEnglishLocale())
							offCorr.setCreatedByName(user.getFullNamePrimary());
						else
							offCorr.setCreatedByName(user.getFullNameSecondary());
						offCorr.setOpened(true);
					}
					viewMap.put(CORRESPONDENCE_LIST, officialCorresList);
				}
			} 
		catch (Exception e)
			{
				logger.LogException("officialCorrespondenceTabClick crashed| ", e);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public void openFollowupPopup()
	{
		OfficialCorrespondenceView offCorrView = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
		sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.IS_POPUP);
		sessionMap.put(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID,offCorrView.getOffCorrId()) ;
		if(viewMap.get(TASK_TYPE) != null)
			sessionMap.put(TASK_TYPE,viewMap.get(TASK_TYPE)) ;
			
		executeJavascript("openFollowupPopup();");
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
}
