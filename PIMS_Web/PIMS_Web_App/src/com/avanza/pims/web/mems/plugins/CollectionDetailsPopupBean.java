package com.avanza.pims.web.mems.plugins;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.CollectionProcedureBackingBean.Page_Mode;
import com.avanza.pims.web.util.CommonUtil;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlInputText;

import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.CollectionPaymentDetailsView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class CollectionDetailsPopupBean extends AbstractMemsBean 
{
    private String amountLeft;	
	CollectionProcedureView  collectionProcedureView = new  CollectionProcedureView  ( );
	List<CollectionProcedureView> collectionProcedureViewList = new ArrayList<CollectionProcedureView>( 0 );
	CollectionProcedureService collectionService = new CollectionProcedureService();
	private HtmlDataTable dataTable;
	private Long fileId;
	private Long assetMemsId;
	private List<SelectItem> assetNameList = new ArrayList<SelectItem>();
	private RequestView requestView;
	private String pageMode;
	private CollectionPaymentDetailsView parentObj;
	private HtmlInputText inputTextAmount = new HtmlInputText();
	private HtmlCommandButton btnDone = new HtmlCommandButton();
	private HtmlCommandButton btnAdd = new HtmlCommandButton();
	public void init()
	
	{
		String METHOD_NAME = "init";
		try
		{
			super.init();
			if( !isPostBack() )
			{
				loadTabData( );
			}
		}
		catch( Exception e )
		{
			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", e );
		}
	}
	public void prerender()
	{
		String METHOD_NAME = "prerender";
		try
		{
		
			if( this.getAssetNameList() != null &&  this.getAssetNameList().size() == 1 )
			{
				collectionProcedureView =  this.getCollectionProcedureView();
				collectionProcedureView.setSelectOneAssetId(   ( (SelectItem)this.getAssetNameList().get(0) ).getValue().toString() ); 
				this.setCollectionProcedureView(  collectionProcedureView );
				onAssetNameChange();
			}
		}
		catch( Exception e )
		{
			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", e );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadTabData( ) throws Exception
	{
		  String methodName ="loadTabData";
		  logger.logInfo( methodName +"Start" );
		  if( sessionMap.get( WebConstants.ASSET_ID )  != null )
		  {
				this.setAssetId( ( Long )sessionMap.get( WebConstants.ASSET_ID ) );
				sessionMap.remove( WebConstants.ASSET_ID );
				loadAssetNamesByAssetId();

		  }     		  

		  if( sessionMap.get( WebConstants.InheritanceFile.FILE_ID ) != null )
		  {
			  this.setFileId( ( Long )sessionMap.get( WebConstants.InheritanceFile.FILE_ID ) );
			  sessionMap.remove( WebConstants.InheritanceFile.FILE_ID  );
			  loadAssetNamesByInheritanceFileId();
			  this.assetNameList.add(0,new SelectItem("-1",""));
		  }
		  if( sessionMap.get( WebConstants.REQUEST ) != null )
		  {
				 this.setRequestView( ( RequestView )sessionMap.get( WebConstants.REQUEST ) );
				 sessionMap.remove( WebConstants.REQUEST   );
		  }		  			
		  if( sessionMap.get( WebConstants.PAGE_MODE ) != null   )
		  {
			  this.setPageMode(sessionMap.get( WebConstants.PAGE_MODE ).toString() );
			  sessionMap.remove( WebConstants.PAGE_MODE ); 
		  }
		  if(	sessionMap.get(WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ) != null )
		  {
			  this.setParentObj( (CollectionPaymentDetailsView ) sessionMap.get(WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ) );
			  sessionMap.remove(WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW );
			  //So that amount left and amount to collect will be same if there is no entry in grid
			  setAmountLeft( 0D,true );
			  if( this.getParentObj().getCollectionTrxView() != null && this.getParentObj().getCollectionTrxView().size() >0 )
			  {
				  this.setCollectionProcedureViewList( this.getParentObj().getCollectionTrxView() );
				  for (CollectionProcedureView view : this.getCollectionProcedureViewList())
				  {
					  setAmountLeft(  view.getAmount(),false);
				}
			  }
			  
			  
			  
		  }
		  logger.logInfo(methodName +"End");
	}
	@SuppressWarnings( "unchecked" )
	private void loadAssetNamesByInheritanceFileId() throws Exception 
	{
		String methodName = "loadAssetNamesByInheritanceFileId|";
		List<SelectItem> selectList = new ArrayList<SelectItem>();
		List<AssetMemsView> assetMems;
		try 
		{
			logger.logInfo(methodName+"Start|FileId:%s",this.getFileId()  );
			
			if (this.getFileId() != null) 
			{
				assetMems = new UtilityService().getAssetViewByInheritanceFileId(  this.getFileId(),null );
				populateAssetMems( assetMems);
			}
		}

		catch (Exception ex) {
			logger.LogException(methodName + "Error Occured:", ex);
			throw ex;

		}

	}

	@SuppressWarnings( "unchecked" )
	private void populateAssetMems(	List<AssetMemsView> assetMems) {
		List<SelectItem> selectList = new ArrayList<SelectItem>();
		viewMap.put("AssetMems", assetMems);
		for (AssetMemsView assetMems2 : assetMems)
		{
			SelectItem item = new SelectItem();

			item.setValue(assetMems2.getAssetId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(assetMems2.getAssetNameEn());
			else
				item.setLabel(assetMems2.getAssetNameAr());

			selectList.add(item);
			setAssetMemsMap( assetMems2 );

		}

		this.setAssetNameList(selectList);
	}
     	
	@SuppressWarnings( "unchecked" )
	private void setAssetMemsMap( AssetMemsView assetMems )
	{
		HashMap<Long,AssetMemsView> map = new HashMap<Long, AssetMemsView>( 0 );
		if( viewMap.get("AssetMemsMap") != null )
		{
			map = ( HashMap<Long, AssetMemsView> )viewMap.get("AssetMemsMap"); 
		}
		if( !map.containsKey( assetMems.getAssetId() ) )
		{
			map.put(assetMems.getAssetId() , assetMems );
		}
		viewMap.put("AssetMemsMap", map);
	}
	@SuppressWarnings( "unchecked" )
	private AssetMemsView getAssetMemsFromMapByAssetId( Long assetId )
	{
		HashMap<Long,AssetMemsView> map = new HashMap<Long, AssetMemsView>( 0 );
		if( viewMap.get("AssetMemsMap") != null )
		{
			map = ( HashMap<Long, AssetMemsView> )viewMap.get("AssetMemsMap"); 
		}
		if( map.containsKey( assetId ) )
		{
			return (AssetMemsView )map.get(assetId );
		}
		
			return null;
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void loadAssetNamesByAssetId() throws Exception 
	{
		String methodName = "loadAssetNamesByAssetId|";
		
		try 
		{
			logger.logInfo(methodName+"Start|Asset Id:%s",this.getAssetId());
			AssetMemsView assetMems;
			
			if (this.getAssetId() != null) 
			{
				List<AssetMemsView> assetMemsList = new ArrayList<AssetMemsView>( 0 );
				assetMems = new UtilityService().getAssetViewById( this.getAssetId() );
				assetMemsList.add( assetMems );
				populateAssetMems( assetMemsList );
				}
			logger.logInfo(methodName+"Finish");
		}

		catch (Exception ex) 
		{
			logger.LogException(methodName + "Error Occured:", ex);
            throw ex;
		}

	}

	public void openManageAsset()
	{
		
		CollectionProcedureView selectedRow = (CollectionProcedureView) dataTable.getRowData();
		String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 550;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.ASSET_ID+"="+selectedRow.getAssetId()
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

		executeJavascript(javaScriptText);
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenDistributePopUp()
	{
		
		String METHOD_NAME = "onOpenDistributePopUp";
		try
		{
			CollectionProcedureView selectedRow = (CollectionProcedureView) dataTable.getRowData();
			String collectionTrxId="" ;
			Boolean pageModeConfirm=(isPageModeConfirm()!=true? (isPageModeView()!=true?false:true) : true);
			if ( this.getRequestView().getRequestId() != null && selectedRow.getCollectionTrxId() != null)
			{
				collectionTrxId= "collectionTrxId="+selectedRow.getCollectionTrxId().toString()+"&";
			}
			String javaScriptText = " var screen_width = 800;"
					+ "var screen_height = 500;"
					+ "var popup_width = screen_width-200;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('distributePopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP&"+collectionTrxId
					+ (
							this.getFileId() != null && selectedRow.getAssetId() != null ? 
					    WebConstants.InheritanceFile.INHERITANCE_FILE_ID + "=" + this.getFileId().toString()+ "&" + 
					    WebConstants.ASSET_ID+ "="+ selectedRow.getAssetId().toString()
						: (
							this.getFileId() != null ? ( WebConstants.InheritanceFile.INHERITANCE_FILE_ID + "=" + this.getFileId().toString() )
									: WebConstants.ASSET_ID+ "="+ this.getAssetId().toString()
						)
									
					  ) + "&"+Page_Mode.CONFIRM+"="+pageModeConfirm.toString() 
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

			sessionMap.put("CollectionTransactionBatch", selectedRow);

			executeJavascript( javaScriptText );
		}
		catch( Exception e )
		{
			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

		
		
	}
	public boolean isPageModeAddEdit()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals(Page_Mode.ADD_EDIT) )
		{
			
			return true;
		}
		return false;
		
	}
	
	public boolean isPageModeConfirm()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.CONFIRM ) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeView()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.VIEW) )
		{
			
			return true;
		}
		return false;
		
	}
	@SuppressWarnings("unchecked")
	public RequestView getRequestView() {

		if (viewMap.containsKey("requestView"))
			requestView = (RequestView) viewMap.get("requestView");

		return requestView;
	}

	@SuppressWarnings("unchecked")
	public void setRequestView(RequestView requestView) {

		this.requestView = requestView;
		if (this.requestView != null)
			viewMap.put("requestView", this.requestView);
	}

	@SuppressWarnings( "unchecked" )
	public void onMessageFromDistributePopUp()
	{
		
		String METHOD_NAME = "onMessageFromDistributePopUp";
		try
		{
			logger.logInfo( METHOD_NAME + " --- Start--- " );
			if (sessionMap.containsKey("CollectionTransactionBatch"))
			{
				CollectionProcedureView collView = (CollectionProcedureView) sessionMap.get("CollectionTransactionBatch");
				sessionMap.remove("CollectionTransactionBatch");
				if (collView.getDistributionInfo() != null) 
				{
					for (CollectionProcedureView row : this.getCollectionProcedureViewList() ) 
					{
						if (collView.getHashCode() == row.getHashCode()) 
						{
						  row.setDistributionInfo(collView.getDistributionInfo());
						  row.setDistributionOption(collView.getDistributionOption());
						  if (collView.getIsDistributedByGeneralAccount() != null)
						  {
							  row.setIsDistributedByGeneralAccount(collView.getIsDistributedByGeneralAccount());
						  }
						  else
						  {
							  row.setIsDistributedByGeneralAccount(false);
						  }
						  break;
						}
					}
					successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_DIST_INFO));
				}
	
			}
	
			logger.logInfo( METHOD_NAME + " --- Finish--- " );
		}
		catch( Exception e )
		{
			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
    public void onDelete()
	{
	  String methodName = "onDelete|";
	  
	  try
	  {
		    logger.logInfo( methodName + " --- Start--- " );
		    CollectionProcedureView vo = ( CollectionProcedureView )dataTable.getRowData();
		    this.setAmountLeft( vo.getAmount() , true);
		    
		    if( vo.getCollectionTrxId() == null )
		    {
		    		
		    	List<CollectionProcedureView > list = this.getCollectionProcedureViewList();	
		    	list.remove( vo );
		    	this.setCollectionProcedureViewList( list );
		    
		    }
		    else
		    {
		    	vo.setIsDeleted( 1L );
		    }
		    	
		  logger.logInfo( methodName + " --- Finish--- " );
	  }
	  catch( Exception e )
	  {
		  logger.LogException( methodName+ " --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
		
	}
	
	@SuppressWarnings( "unchecked" )
    public void onAssetNameChange()
	{
	  String methodName = "onAssetNameChange|";
	  
	  try
	  {
		    logger.logInfo( methodName + " --- Start--- " );
		    collectionProcedureView = this.getCollectionProcedureView();
		    if( collectionProcedureView.getSelectOneAssetId() != null && !collectionProcedureView.getSelectOneAssetId().equals("-1") )
		    {
		     AssetMemsView  asset = getAssetMemsFromMapByAssetId(  new Long ( collectionProcedureView.getSelectOneAssetId() ) );
		     collectionProcedureView.setAssetTypeEn( asset.getAssetTypeView().getAssetTypeNameEn() );
		     collectionProcedureView.setAssetTypeAr( asset.getAssetTypeView().getAssetTypeNameAr() );
		     collectionProcedureView.setAssetNameAr( asset.getAssetNameAr() );
			  collectionProcedureView.setAssetNameEn( asset.getAssetNameEn() );
			  
		     if( asset.getAssetTypeView().getAssetTypeId().equals( WebConstants.AssetType.CASH ) )
		     {
		       collectionProcedureView.setAmount( asset.getAmount() );
		       collectionProcedureView.setAmountString( asset.getAmountString() );
		       inputTextAmount.setReadonly(true);
		       inputTextAmount.setStyleClass("INPUT_TEXT READONLY");
		     }
		     else
		     {
		    	 inputTextAmount.setReadonly(false); 
		    	 inputTextAmount.setStyleClass("INPUT_TEXT");
		     }
		    }
		    else
		    {
		    	String assetId =collectionProcedureView.getSelectOneAssetId();
				  collectionProcedureView = new CollectionProcedureView();
				  collectionProcedureView.setSelectOneAssetId( assetId );
				  inputTextAmount.setReadonly(false);
				  inputTextAmount.setStyleClass("INPUT_TEXT");
		    }
		    this.setCollectionProcedureView(collectionProcedureView);
		  logger.logInfo( methodName + " --- Finish--- " );
	  }
	  catch( Exception e )
	  {
		  logger.LogException( methodName+ " --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
		
	}
	
	@SuppressWarnings( "unchecked" )
    public void onEdit()
	{
	  String methodName = "onEdit|";
	  try
	  {
		    logger.logInfo( methodName + " --- Start--- " );
		    collectionProcedureView = ( CollectionProcedureView )dataTable.getRowData();
		    collectionProcedureView.setIsBeingEdited(1L);
		    if( collectionProcedureView.getAssetId() == null || collectionProcedureView.getAssetId().compareTo(0L) <= 0 )
		    {
		    	collectionProcedureView.setSelectOneAssetId( "-1" );
		    }
            this.setAmountLeft(collectionProcedureView.getAmount(),true  );		    	
		    
		    btnDone.setRendered( false );
		    this.setCollectionProcedureView( collectionProcedureView );
		  logger.logInfo( methodName + " --- Finish--- " );
	  }
	  catch( Exception e )
	  {
		  logger.LogException( methodName+ " --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
		
	}

	@SuppressWarnings( "unchecked" )
    public void onAdd()
	{
	  String methodName = "onAdd|";
	  
	  try
	  {
		    logger.logInfo( methodName + " --- Start--- " );
			if( !hasAddToGridErrors() )
			{
				    addToGrid();
	
				    if( this.collectionProcedureView.getIsBeingEdited()== null || this.collectionProcedureView.getIsBeingEdited().longValue() == 0L )
                  {
				     collectionProcedureViewList = getCollectionProcedureViewList();
					 collectionProcedureView.setIsBeingEdited( 0L );
					 collectionProcedureViewList.add( collectionProcedureView );
					 this.setCollectionProcedureViewList( collectionProcedureViewList );
					}
				    else
				    {
						 collectionProcedureView.setIsBeingEdited( 0L );
				    }
					
					if( inputTextAmount.isReadonly() )
					{
					 inputTextAmount.setReadonly(false); 
			    	 inputTextAmount.setStyleClass("INPUT_TEXT");
					}
					if( !btnDone.isRendered() )
					{
						btnDone.setRendered( true );
					}
					this.setAmountLeft( collectionProcedureView.getAmount() , false);
					this.setCollectionProcedureView( new CollectionProcedureView( ) ) ;
			}
		  logger.logInfo( methodName + " --- Finish--- " );
	  }
	  catch( Exception e )
	  {
		  logger.LogException( methodName+ " --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
		
	}
	private boolean hasAddToGridErrors()
	{
		boolean hasError = false;
		errorMessages = new ArrayList<String>(0);
		if( collectionProcedureView.getAmountString() == null ||  collectionProcedureView.getAmountString().trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("collectionProcedure.collectionDetailsPopup.message.emptyAmount"));
			hasError = true;
		}
		else
		{
			Double amount = null;
			Double amountLeft = new Double ( this.getAmountLeft() ); 
			try
			{
				
			  amount = new Double( collectionProcedureView.getAmountString().trim()  );
			}
			catch(Exception e)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("collectionProcedure.collectionDetailsPopup.message.invalidAmount"));
				hasError = true;
			}
			if( amount != null  )
			{
				if( amount <= 0 )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collectionProcedure.collectionDetailsPopup.message.amountLessThanZero"));
					hasError = true;
					
				}
				if( amountLeft < amount )
				{
					String message = ResourceUtil.getInstance().getProperty("collectionProcedure.collectionDetailsPopup.message.amountEnteredExceededAmountToCollect");
					errorMessages.add( MessageFormat.format(message, amountLeft,amount) );
					hasError = true;
				}
				
				
				
			}
			
		}
	
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	private void addToGrid()
	{
		
			collectionProcedureView.setHashCode(  collectionProcedureView.hashCode() );
			
			if( collectionProcedureView.getSelectOneAssetId() != null && ! collectionProcedureView.getSelectOneAssetId().equals("-1") )
			{
				collectionProcedureView.setAssetId(   new Long(collectionProcedureView.getSelectOneAssetId() ) );
			}
			collectionProcedureView.setAmount( new Double( collectionProcedureView.getAmountString() ));
			collectionProcedureView.setIsDeleted( 0L );
			
	}
	private boolean hasOnClosePopUpError()
	{
		boolean hasError = false;
		if( this.getCollectionProcedureViewList()== null || this.getCollectionProcedureViewList().size() <= 0 || 
				this.getAmountLeft() == null || ( new Double (this.getAmountLeft() )).compareTo( 0d ) > 0 )
		{
			String message = ResourceUtil.getInstance().getProperty("collectionProcedure.collectionDetailsPopup.message.amountLeftToCollect");
			errorMessages.add( MessageFormat.format(message, this.getAmountLeft() ) );
			hasError =true;
		}
		
		
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
    public void onClosePopup()
	{
	  String methodName = "onClosePopup|";
	  
	  try
	  {
		    logger.logInfo( methodName + " --- Start--- " );
		    if( !hasOnClosePopUpError() )
		    {
			    CollectionPaymentDetailsView parentObj = this.getParentObj();
			    parentObj.setCollectionTrxView( this.getCollectionProcedureViewList() );
			    parentObj.setTotalCollectionTrxAmount(0d );
			    for ( CollectionProcedureView obj : this.getCollectionProcedureViewList()) {
			    	
			    	parentObj.setTotalCollectionTrxAmount( parentObj.getTotalCollectionTrxAmount()  + obj.getAmount() );
					
				}
			    sessionMap.put( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW, parentObj);
			    executeJavascript("closeWindowSubmit();");
		    }
		    logger.logInfo( methodName + " --- Finish--- " );
	  }
	  catch( Exception e )
	  {
		  logger.LogException( methodName+ " --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
		
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	public Long getFileId() {
		if( viewMap.get( "InheritanceFileId" )!= null )
			fileId = new Long( viewMap.get( "InheritanceFileId" ).toString() ) ;
		return fileId;
	}

	@SuppressWarnings( "unchecked" )
	public void setFileId(Long fileId) {
		this.fileId = fileId;
		if( this.fileId != null )
			viewMap.put("InheritanceFileId",fileId  );
	}
	@SuppressWarnings( "unchecked" )
	public Long getAssetId() {
		if( viewMap.get( "AssetId" )!= null )
			fileId = new Long( viewMap.get( "AssetId" ).toString() ) ;
		return fileId;
	}

	@SuppressWarnings( "unchecked" )
	public void setAssetId(Long assetId) {
		this.assetMemsId = assetId;
		if( this.assetMemsId != null )
			viewMap.put( "AssetId", assetMemsId  );
	}
		
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getAssetNameList() {

		assetNameList = (ArrayList<SelectItem>) viewMap.get("assetNameList");
		
		if (assetNameList == null)
		{
			assetNameList = new ArrayList<SelectItem>();
		}
		

		return assetNameList;
	}
	@SuppressWarnings( "unchecked" )
	public void setAssetNameList(List<SelectItem> assetNameList) {
		this.assetNameList = assetNameList;
		if (assetNameList != null)
			viewMap.put("assetNameList", assetNameList);
	}

	public CollectionProcedureView getCollectionProcedureView() {
		if ( viewMap.get( "CollectionProcedureViewForScreen" ) != null )
		{
			collectionProcedureView = ( CollectionProcedureView )viewMap.get( "CollectionProcedureViewForScreen" ) ;
		}
		return collectionProcedureView;
	}

	@SuppressWarnings(  "unchecked")
	public void setCollectionProcedureView(
			CollectionProcedureView collectionProcedureView) {
		
		this.collectionProcedureView = collectionProcedureView;
		if( this.collectionProcedureView != null )
		{
			viewMap.put("CollectionProcedureViewForScreen", this.collectionProcedureView );
		}
	}
	@SuppressWarnings( "unchecked" )
	public List<CollectionProcedureView> getCollectionProcedureViewList() {
		if( viewMap.get( "collectionProcedureViewList" ) != null)
		{
			collectionProcedureViewList = ( List<CollectionProcedureView> )viewMap.get( "collectionProcedureViewList" );
		}
		return collectionProcedureViewList;
	}

	@SuppressWarnings( "unchecked" )
	public void setCollectionProcedureViewList(
			List<CollectionProcedureView> collectionProcedureViewList) {
		this.collectionProcedureViewList = collectionProcedureViewList;
		if( this.collectionProcedureViewList  != null )
		viewMap.put("collectionProcedureViewList", collectionProcedureViewList);
	}
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( 	viewMap.get( "pageMode" ) != null)
		{
			pageMode = viewMap.get( "pageMode" ).toString();
		}
		return pageMode;
	}
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
		if( this.pageMode != null )
		{
			viewMap.put( "pageMode", this.pageMode );
		}
	}
	@SuppressWarnings( "unchecked" )
	public CollectionPaymentDetailsView getParentObj() {
		if( viewMap.get( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ) != null )
		{
			parentObj = ( CollectionPaymentDetailsView  )viewMap.get( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ); 
		}
		return parentObj;
		
	}
    @SuppressWarnings( "unchecked" )
	public void setParentObj(CollectionPaymentDetailsView parentObj) {
		this.parentObj = parentObj;
		if ( this.parentObj != null )
		{
			viewMap.put( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ,this.parentObj );
		}
	}

	public HtmlInputText getInputTextAmount() {
		return inputTextAmount;
	}

	public void setInputTextAmount(HtmlInputText inputTextAmount) {
		this.inputTextAmount = inputTextAmount;
	}


    public String getAmountLeft() {
    	if( viewMap.get( "amountLeft" ) != null )
    	{
    		amountLeft = viewMap.get( "amountLeft" ).toString();
    	}
    	return amountLeft;
    	
    }
@SuppressWarnings( "unchecked" )
    public void setAmountLeft(String amountLeft) {
    	this.amountLeft = amountLeft;
    	if( this.amountLeft != null )
    	{
    		viewMap.put("amountLeft", amountLeft);
    	}
    }
    public void setAmountLeft(Double amount, boolean isAdd) {
    	Double amountLeft; 
    	if(!isAdd)
    	{
    		if( this.getAmountLeft() != null  )
    		{
    			amountLeft= new Double( this.getAmountLeft() ) - amount;
    		}
    		else
    		{
    	     amountLeft= new Double( this.getParentObj().getAmount() ) - amount;
    		}
    	}
    	else
    	{
    		if( this.getAmountLeft() != null  )
    		{
    			amountLeft= new Double( this.getAmountLeft() ) + amount;
    		}
    		else
    		{
    		amountLeft= new Double( this.getParentObj().getAmount() ) + amount;
    	    }
    	}
    	this.setAmountLeft( amountLeft.toString() );
    }

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

	public HtmlCommandButton getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(HtmlCommandButton btnAdd) {
		this.btnAdd = btnAdd;
	}

}
