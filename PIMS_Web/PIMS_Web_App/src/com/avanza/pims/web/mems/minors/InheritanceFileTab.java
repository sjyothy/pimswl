package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.InheritanceFileTabView;
import com.avanza.ui.util.ResourceUtil;

public class InheritanceFileTab extends AbstractController


{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5160927008167503230L;
	SystemParameters parameters = SystemParameters.getInstance();
	  DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));

	private HtmlInputHidden editMode = new HtmlInputHidden();

	

	private List<InheritanceFileTabView> dataList = new ArrayList<InheritanceFileTabView>();
	private org.apache.myfaces.component.html.ext.HtmlDataTable dataTable = new org.apache.myfaces.component.html.ext.HtmlDataTable();	
	
	private transient Logger logger = Logger.getLogger(InheritanceFileTab.class);
	private List<String> errorMessages = new ArrayList<String>();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String PAGE_MODE_ASSET = "PAGE_MODE_ASSET";
	private String PAGE_MODE_ASSET_FILE = "PAGE_MODE_ASSET_FILE";
	private String pageMode="" ;
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	Map viewRootMap=context.getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	Map session = getFacesContext().getExternalContext().getSessionMap();

	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings( "unchecked" )
	public void init(){
		
		super.init();
		try
		{
			Map viewMap = viewRootMap;
						
			if(!isPostBack())
			{
				
			
				if(viewMap.get(WebConstants.PERSON_ID)!=null) // tab working from manage beneficiary
				{
					loadDataList(Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString()));
					
				}
				else if(viewMap.get(WebConstants.ASSET_ID)!=null) // tab working from manage asset
				{
					
					loadDataListForAssetScreen(Long.parseLong(viewMap.get(WebConstants.ASSET_ID).toString()));
					this.setPageMode(PAGE_MODE_ASSET);
				}
				
			
			
				
			}

			



		}
		catch(Exception e){
			logger.LogException("init|Error Occured", e );
		}
//		loadDataList();

	}
	@SuppressWarnings("unchecked")
	public void preprocess()
	{

		super.preprocess();
	

	}
	@SuppressWarnings( "unchecked" )
	public List<InheritanceFileTabView> getInheritenceFileDataList()
	{
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<InheritanceFileTabView>)viewMap.get("inheritanceFileList");
		if(dataList==null)
			dataList = new ArrayList<InheritanceFileTabView>();
		return dataList;
	}
	@SuppressWarnings( "unchecked" )
	private void loadDataList(Long personId)throws Exception  
	{
		 if(dataList != null)
			 dataList.clear();
		 
		    List<InheritanceFileTabView> list = new ArrayList<InheritanceFileTabView>();
			InheritanceFileService fileService = new InheritanceFileService();
			int totalRows =  fileService.displayInheritenceFileByPersonId(personId);
			setTotalRows(totalRows);
			list =  fileService.displayRecordsInheritenceFileByPersonId(personId,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending(),getIsEnglishLocale());
			viewRootMap.put("inheritanceFileList", list);
	}
	@SuppressWarnings( "unchecked" )
	private void loadDataListForAssetScreen(Long assetId) throws Exception 
	{
		 if(dataList != null)
			 dataList.clear();
		 
		    List<InheritanceFileTabView> list = new ArrayList<InheritanceFileTabView>();
			InheritanceFileService fileService = new InheritanceFileService();
			int totalRows =  fileService.getNoOfRecordsInheritanceFileByAssetId(assetId);
			setTotalRows(totalRows);
			list =  fileService.getInheritanceFileByAssetId(assetId);
			viewRootMap.put("inheritanceFileList", list);
	}
	
    @SuppressWarnings("unchecked")
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    @SuppressWarnings("unchecked")    
	public String getLocale()
    {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	//////////added for showing record size with the paginator
	public Integer getRecordSize(){
		if (dataList != null) {
			return dataList.size();
		}
		else{
			return 0;
		}
	}
	public List<InheritanceFileTabView> getDataList() {
		return dataList;
	}
	public void setDataList(List<InheritanceFileTabView> dataList) {
		this.dataList = dataList;
	}
	public org.apache.myfaces.component.html.ext.HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(org.apache.myfaces.component.html.ext.HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlInputHidden getEditMode() {
		return editMode;
	}
	public void setEditMode(HtmlInputHidden editMode) {
		this.editMode = editMode;
	}

	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if(viewRootMap.get("PAGE_MODE_ASSET_FILE") != null)
		this.pageMode=viewRootMap.get("PAGE_MODE_ASSET_FILE").toString();
		return pageMode;
	}

	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
		if(pageMode!=null)
			viewRootMap.put("PAGE_MODE_ASSET_FILE", pageMode);
	}
	
	  @SuppressWarnings( "unchecked" )
	  public void openFilePopUp() 
	  {
		       String methodName = "openFilePopUp|";
				try	
				{	
				
					InheritanceFileTabView obj = ( InheritanceFileTabView) dataTable.getRowData();
					logger.logInfo(methodName + " --- STARTED --- ");
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put( WebConstants.InheritanceFile.FILE_ID, 
							obj.getFileId() );
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
					executeJavascript(   "javaScript:openFile();" );
				    
				    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
				}
				catch (Exception exception) 
				{
					logger.LogException(methodName + " --- CRASHED --- ", exception);
					errorMessages = new ArrayList<String>(0);
		    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				}
	  }
	  private void executeJavascript(String javascript) 
		{
			String METHOD_NAME = "executeJavascript()";
			try 
			{
				logger.logInfo(METHOD_NAME + " --- STARTED --- ");
				FacesContext facesContext = FacesContext.getCurrentInstance();			
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
			} 
			catch (Exception exception) 
			{			
				logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
			}
		}
	
	

	
    
}
