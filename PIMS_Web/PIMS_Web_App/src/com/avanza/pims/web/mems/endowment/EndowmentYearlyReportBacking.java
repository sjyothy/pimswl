package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.EndowmentYearlyReportCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class EndowmentYearlyReportBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	private EndowmentYearlyReportCriteria criteria;
	private String forYear;
	private List<SelectItem> yearsList  = new ArrayList<SelectItem>();
	
	public String getForYear() {
		return forYear;
	}

	public void setForYear(String forYear) {
		this.forYear = forYear;
	}

	public List<SelectItem> getYearsList() {
		return yearsList;
	}

	public void setYearsList(List<SelectItem> yearsList) {
		this.yearsList = yearsList;
	}

	public EndowmentYearlyReportCriteria getCriteria() 
	{
		return criteria;
	}

	public void setCriteria(EndowmentYearlyReportCriteria criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings( "unchecked" )
	public EndowmentYearlyReportBacking() 
	{
		criteria = new EndowmentYearlyReportCriteria(	
														ReportConstant.Report.ENDOWMENT_YEARLY_REPORT,
														ReportConstant.Processor.ENDOWMENT_YEARLY_REPORT,
														getLoggedInUserObj().getFullName()
													);
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if(this.criteria.getToDate() == null)
		{
			this.criteria.setToDate ( new Date() );
		}
		if( viewMap.get("yearsList") != null )
		{
			this.yearsList  = (ArrayList<SelectItem>)viewMap.get("yearsList"); 
		}
		else
		{
			this.yearsList   = new ArrayList<SelectItem>();
			for (int i=2015;i<=2030;i++)
			{
				String year = String.valueOf( i ) ;
				this.yearsList.add( new SelectItem ( year,year ) );
			}
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{	
		
		if(this.yearsList != null)
		{
			viewMap.put("yearsList",this.yearsList);
		}
	}

	@SuppressWarnings("unchecked")
	public void onOpenReport()
	{
		try
		{
			updateValuesFromMaps();
			if( forYear != null )
			{
				criteria.setForYear(forYear);
				criteria.setFromDate(   criteria.getFormatterWithOutTime().parse("01/01/"+forYear) );
				criteria.setToDate(     criteria.getFormatterWithOutTime().parse("31/12/"	 +forYear) );
				
				
				
			}
			
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, criteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenReport|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

}
