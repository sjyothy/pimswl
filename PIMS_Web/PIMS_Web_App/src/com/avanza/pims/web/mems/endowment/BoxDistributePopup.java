package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;
import javax.faces.model.SelectItem;

import com.avanza.pims.entity.BoxDistributionHistory;
import com.avanza.pims.entity.BoxPlaceSubType;
import com.avanza.pims.entity.BoxPlaceType;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.DonationBoxService;
import com.avanza.ui.util.ResourceUtil;

    public class BoxDistributePopup extends AbstractMemsBean {
	private static final long serialVersionUID = 8517238816949483790L;
    public HtmlInputText txtDistributedBy = new HtmlInputText();
	public Date distributedOn;
	private String cmbPlace;
	private String cmbSubPlace; 
	private String cmbCommunity;
	private HtmlInputText txtOtherDesc;
	private HtmlInputText txtOtherPlaceDesc;
	private String txtAddress;
	List<SelectItem> donationBoxSubPlaces = new ArrayList<SelectItem>();
	public BoxDistributePopup() {
	
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		final String METHOD_NAME = "init()";
		errorMessages = new ArrayList<String>();
		try 
		{
			if(sessionMap.get(WebConstants.SELECTED_ROWS) != null)
			{
				viewMap.put(WebConstants.SELECTED_ROWS,sessionMap.remove(WebConstants.SELECTED_ROWS));
			}
		} 
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}

	private boolean hasDoneErrors() throws Exception
	{
		boolean hasDoneErrors = false;
		errorMessages = new ArrayList<String>();
		if( txtDistributedBy.getValue()== null || txtDistributedBy.getValue().toString().trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.distributedByRequired")  );
			hasDoneErrors = true;
		}
		if( distributedOn == null )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.distributedOnRequired")  );
			hasDoneErrors = true;
		}
		
		if( cmbPlace ==null || cmbPlace.equals("-1"))
	    {
	    	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.placeRequired")  );
	    	hasDoneErrors = true;
	    }
		else if(  cmbPlace.equals("-1")  && 
        		( txtOtherPlaceDesc.getValue() ==null || txtOtherPlaceDesc.getValue().toString().trim().length()<=0 ) 
              )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.otherPlaceRequired")  );
        	hasDoneErrors = true;
        }   
        if( cmbCommunity ==null || cmbCommunity.equals("-1") )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.communityRequired")  );
        	hasDoneErrors = true;
        }
        else if(  cmbCommunity .equals("-2")  && 
        		( txtOtherDesc.getValue() ==null || txtOtherDesc.getValue().toString().trim().length()<=0 ) 
              )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.otherCommunityDescRequired")  );
        	hasDoneErrors = true;
        }
		return hasDoneErrors;
	}
	@SuppressWarnings( "unchecked" )
	public void onSubPlaceChanged()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			
				if( cmbSubPlace.equals("0") )
				{
					txtOtherPlaceDesc.setDisabled(false);
					txtOtherPlaceDesc.setStyleClass( "");
				}
				else
				{
					txtOtherPlaceDesc.setStyleClass( "READONLY");
				}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSubPlaceChanged--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}		
	}
	@SuppressWarnings( "unchecked" )
	public void onPlaceChanged()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			if( cmbPlace != null && !cmbPlace.equals("-1") )
			{
					txtOtherPlaceDesc.setStyleClass( "READONLY");
					getBoxSubPlace(cmbPlace);
			}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPlaceChanged--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void getBoxSubPlace(String placeId)throws Exception
	{
		donationBoxSubPlaces = new ArrayList<SelectItem>();
		//get data from db and put prepared list in viewMap
		List<BoxPlaceSubType> places = null;
		places = new DonationBoxService().getBoxSubPlacesByPlaceId(placeId);
		for(BoxPlaceSubType place :  places)
		{
				SelectItem item = null;
				String name ;
				if(isEnglishLocale())
				{
					name = place.getBoxPlaceType().getTypeNameEn()+" - "+ place.getSubTypeNameEn();
				}
				else
				{
					name = place.getBoxPlaceType().getTypeNameAr()+" - "+ place.getSubTypeNameAr();
				}
				item = new SelectItem(place.getSubTypeId().toString(),name);
				donationBoxSubPlaces.add(item);
		}
	    Collections.sort(donationBoxSubPlaces, ListComparator.LIST_COMPARE);
	    setDonationBoxSubPlaces(donationBoxSubPlaces);
	    
	}
    
	@SuppressWarnings( "unchecked" )
	public void onCommunityChanged()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			if(cmbCommunity.equals("-2"))
			{
				txtOtherDesc.setDisabled(false);
				txtOtherDesc.setStyleClass("");
			}
			else
			{
				txtOtherDesc.setValue("");
				txtOtherDesc.setDisabled(true);
				txtOtherDesc.setStyleClass("READONLY");
			}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCommunityChanged--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void onDone() 
	{
		errorMessages = new ArrayList<String>();
		try 
		{
			if( hasDoneErrors() ) return;
			if(viewMap.get(WebConstants.SELECTED_ROWS) != null)
			{
				sessionMap.put(WebConstants.SELECTED_ROWS,viewMap.remove(WebConstants.SELECTED_ROWS));
			}
			BoxDistributionHistory bdh = new BoxDistributionHistory();
			bdh.setDistributedBy(txtDistributedBy.getValue().toString().trim());
			bdh.setDistributedOn(distributedOn);
			bdh.setCreatedBy(getLoggedInUserId());
			bdh.setCreatedOn( new Date() );
			bdh.setCommunityId( new Long(cmbCommunity) );
			if( txtOtherDesc.getValue() != null && txtOtherDesc.getValue().toString().trim().length() > 0 )
			{
				bdh.setOtherCommDesc(txtOtherDesc.getValue().toString() );
			}
			BoxPlaceType bpt = new BoxPlaceType();
			bpt.setTypeId( new Long( cmbPlace ) );
			bdh.setBoxPlaceType( bpt );
			if( cmbSubPlace != null && ! cmbSubPlace.equals("-1"))
			{
				BoxPlaceSubType bpst = new BoxPlaceSubType();
				bpst.setSubTypeId(  new Long( cmbSubPlace ) );
				bdh.setBoxPlaceSubType(  bpst );
				if(bpst.getSubTypeId().compareTo(0L)==0)
				{
					bdh.setOtherPlaceSubType( txtOtherPlaceDesc.getValue().toString().trim() );
				}
			}
			bdh.setAddress( txtAddress );
			sessionMap.put(WebConstants.DonationBox.BoxDistribution, bdh);
			executeJavaScript("javaScript:closeAndSendToParent();");
		} 
		catch (Exception exception) 
		{
			logger.LogException( "onDone --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}

	public HtmlInputText getTxtDistributedBy() {
		return txtDistributedBy;
	}

	public void setTxtDistributedBy(HtmlInputText txtDistributedBy) {
		this.txtDistributedBy = txtDistributedBy;
	}

	public Date getDistributedOn() {
		return distributedOn;
	}

	public void setDistributedOn(Date distributedOn) {
		this.distributedOn = distributedOn;
	}

	public String getCmbPlace() {
		return cmbPlace;
	}

	public void setCmbPlace(String cmbPlace) {
		this.cmbPlace = cmbPlace;
	}
	public String getCmbSubPlace() {
		if( viewMap.get("cmbSubPlace") != null )
		{
			cmbSubPlace = viewMap.get("cmbSubPlace").toString(); 
		}
		return cmbSubPlace;
	}

	@SuppressWarnings("unchecked")
	public void setCmbSubPlace(String cmbSubPlace) {
		this.cmbSubPlace = cmbSubPlace;
		if(this.cmbSubPlace  != null )
		{
		viewMap.put("cmbSubPlace",cmbSubPlace ) ;
		}
	}

	public String getCmbCommunity() {
		return cmbCommunity;
	}

	public void setCmbCommunity(String cmbCommunity) {
		this.cmbCommunity = cmbCommunity;
	}


	public String getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(String txtAddress) {
		this.txtAddress = txtAddress;
	}

	public HtmlInputText getTxtOtherDesc() {
		return txtOtherDesc;
	}

	public void setTxtOtherDesc(HtmlInputText txtOtherDesc) {
		this.txtOtherDesc = txtOtherDesc;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDonationBoxSubPlaces() {
		if( viewMap.get("donationBoxSubPlaces") != null )
		{
			donationBoxSubPlaces = (ArrayList<SelectItem>)viewMap.get("donationBoxSubPlaces");
		}
		return donationBoxSubPlaces;
	}
	@SuppressWarnings("unchecked")
	public void setDonationBoxSubPlaces(List<SelectItem> donationBoxSubPlaces) {
		this.donationBoxSubPlaces = donationBoxSubPlaces;
		if( this.donationBoxSubPlaces != null )
		{
			viewMap.put("donationBoxSubPlaces",donationBoxSubPlaces);
		}
	}

	public HtmlInputText getTxtOtherPlaceDesc() {
		return txtOtherPlaceDesc;
	}

	public void setTxtOtherPlaceDesc(HtmlInputText txtOtherPlaceDesc) {
		this.txtOtherPlaceDesc = txtOtherPlaceDesc;
	}

}
