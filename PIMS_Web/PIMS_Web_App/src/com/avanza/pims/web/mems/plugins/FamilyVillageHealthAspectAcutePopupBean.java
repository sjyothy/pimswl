package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageHealthAspectService;
import com.avanza.pims.ws.vo.mems.HealthAspectsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageHealthAspectAcutePopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -1L;
	
	HealthAspectsView pageView;	
	private Long personId;
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageHealthAspectAcutePopupBean() 
	{
		pageView = new HealthAspectsView();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					pageView.setPersonId(personId);
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (HealthAspectsView)viewMap.get("pageView");
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( pageView.getDiseaseTypeId() == null || pageView.getDiseaseTypeId().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.HealthAspect.AcuteDiseaseRequired"));
			return true;
		}
		
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
		
			if(hasErrors())return;
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageHealthAspectService.addHealthAspect(pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public HealthAspectsView getPageView() {
		return pageView;
	}

	public void setPageView(HealthAspectsView pageView) {
		this.pageView = pageView;
	}



}
