package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.generatorViews.DisbursReasonTypeCritView;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.ui.util.ResourceUtil;

public class DisbursReasonTypeBacking extends AbstractSearchBean {
	private static final long serialVersionUID = 1L;
	private String DEFAULT_SORT_FIELD = "";
	InheritanceFileService service = new InheritanceFileService();

	DisbursReasonTypeCritView criteria = new DisbursReasonTypeCritView();
 	 List<DisbursReasonTypeCritView> list = new ArrayList<DisbursReasonTypeCritView>();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(true);
				checkDataInQueryString();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() {

		if (request.getParameter(VIEW_MODE) != null) {

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			} else if (request.getParameter(VIEW_MODE).equals(
					MODE_SELECT_MANY_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}

	public String onEdit() {
		try {
			 //GET_SELECTED_ROW
			DisbursReasonTypeCritView row =  (DisbursReasonTypeCritView) dataTable.getRowData();

				if (row != null) {
					sessionMap.put(WebConstants.SELECTED_ROW, row);
				}
				 //OPEN_UPDATE_POPUP
				executeJavascript("openEditPopup();");
		} catch (Exception e) {
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	public void onAdd() {
		executeJavascript("openAddPopup();");
	}

	public void onSearch()
	{
		try 
		{
			loadDataList();
			setFirstRow(0);
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public String onDelete() {
		try {
			DisbursReasonTypeCritView selectedRow = (DisbursReasonTypeCritView) dataTable.getRowData();
			list.remove(selectedRow);
			selectedRow.setDeleted(true);
			service.persisDisbursReasonType(selectedRow, CommonUtil.getLoggedInUser());
			successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterDelete"));
		} catch (Exception e) {
			logger.LogException("onDelete |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	//We have to look for the list population for facilities

	public void doSearch() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearch" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearchItemList" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public List<DisbursReasonTypeCritView> loadDataList() throws Exception {
		
		/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
		int totalRows = 0;
		totalRows = service.getDisbursementReasonRecordSize(criteria);
		setTotalRows(totalRows);
		doPagingComputations();
		//////////////////////////////////////////////////////////////////////////////////////////////
		list =  service.getAllDisbursementReason(criteria ,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
		
		if (list.isEmpty() || list == null) {
			errorMessages = new ArrayList<String>();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			if(viewMap.get("LIST") != null)
			viewMap.remove("LIST");
		}
		this.setList( list );
		forPaging(totalRows);//searchedRows
		return list;
	}

	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null) {
			recordSize = (Integer) viewMap.get("recordSize");
		}
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if (this.recordSize != null)
			viewMap.put("recordSize", this.recordSize);
	}

	public DisbursReasonTypeCritView getCriteria() {
		return criteria;
	}

	public void setCriteria(DisbursReasonTypeCritView criteria) {
		this.criteria = criteria;
	}

	public List<DisbursReasonTypeCritView> getList() {
		if(viewMap.get("LIST") != null){
			list = (List<DisbursReasonTypeCritView>) viewMap.get("LIST");
		}
		return this.list;
	}

	public void setList(List<DisbursReasonTypeCritView> list) {
		if(list != null && list.size() > 0){
			viewMap.put("LIST", list);
		}
		this.list = list;
	}
	public boolean isSelectOnePopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_ONE_POPUP) == 0)
			return true;
		else
			return false;
	}
	public boolean isSelectManyPopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_MANY_POPUP) == 0)
			return true;
		else
			return false;
	}
	
	public boolean isPopup(){
		return (isPageModeSelectManyPopUp() || isPageModeSelectOnePopUp());
	}
	public void onSingleSelect(){
		//TODO: implement it
	}
	public void onMultiSelect(){
		//TODO: implement it
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
}
