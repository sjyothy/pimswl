package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.EndowmentsSearchCriteria;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Masraf;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;

public class SearchEndowments extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "endowmentNum";
	private final String DATA_LIST = "DATA_LIST";
	EndowmentService service = new EndowmentService();
	private String  CONTEXT="context";
	EndowmentsSearchCriteria criteria = new EndowmentsSearchCriteria();
	List<Endowment> dataList = new ArrayList<Endowment>();
	HtmlSelectOneMenu managerType = new HtmlSelectOneMenu();
    private Masraf masraf ;
	private String masrafId;

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
//			setCriteria(criteria);
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(true);
		checkDataInQueryString();
	}
	
	@SuppressWarnings("unchecked")
	private void checkDataInQueryString() {
		checkPageMode();
		if (request.getParameter(CONTEXT)!=null )
   	 	{
			viewMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
   	 	}
		if(sessionMap.get( "MasrafId")!= null)
		{
			Long id = Long.valueOf( sessionMap.remove("MasrafId").toString() );
			Masraf masrafPojo = EntityManager.getBroker().findById(Masraf.class, id );
			masraf = new Masraf();
			masraf.setMasrafId( masrafPojo.getMasrafId() );
			masraf.setMasrafName( masrafPojo.getMasrafName() );
			masrafId = masrafPojo.getMasrafId().toString();
			setMasraf(masraf);
		}
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() 
	{
		super.prerender();
		try 
		{
			if( getIsContextRevenueFollowup() || getIsContextThirdPartyRevenue() )
			{
				criteria.setManagerType("0");
				managerType.setDisabled(true);
				managerType.setValue("0");
				setCriteria(criteria);
			}
			if( getIsContextMasrafDisbursement() )
			{
				getMasraf();
				criteria.setMasrafId( masraf.getMasrafId() );
				criteria.setMasrafName( masraf.getMasrafName() );
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("prerender|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked") 
	private Boolean getIsContextThirdPartyRevenue() 
	{
		
		if(viewMap.get(CONTEXT)!=null)
		{
			String contextScreenName=(String)viewMap.get(CONTEXT);
			if(contextScreenName.equals("thirdPartyRevenueReq"))
				return true;
			else
				return false;			
		}
		return false;
		
	}
	@SuppressWarnings("unchecked") 
	private Boolean getIsContextRevenueFollowup() 
	{
		
		if(viewMap.get(CONTEXT)!=null)
		{
			String contextScreenName=(String)viewMap.get(CONTEXT);
			if(contextScreenName.equals("revenueFollowup"))
				return true;
			else
				return false;			
		}
		return false;
		
	}
	@SuppressWarnings("unchecked") 
	private Boolean getIsContextMasrafDisbursement() 
	{
		
		if(viewMap.get(CONTEXT)!=null)
		{
			String contextScreenName=(String)viewMap.get(CONTEXT);
			if(contextScreenName.equals("masrafDisbursement"))
				return true;
			else
				return false;			
		}
		return false;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			Endowment obj = null;
			obj = (Endowment) dataTable.getRowData();
			if( viewMap.get(CONTEXT) != null && (
					viewMap.get(CONTEXT).toString().equals("generalCollectionRequest")  ||
					viewMap.get(CONTEXT).toString().equals("endowmentManage")
					)
				)
			{
				EndowmentView eView = new EndowmentView();
				eView.setEndowmentId( obj.getEndowmentId() );
				eView.setCostCenter( obj.getCostCenter() );
				eView.setEndowmentNum( obj.getEndowmentNum() );
				eView.setEndowmentName( obj.getEndowmentName() );
				sessionMap.put(WebConstants.SELECTED_ROW,eView);
			}
			else
			{
				sessionMap.put(WebConstants.SELECTED_ROW,obj);
			}
			executeJavascript("sendMessageToParent();");
			 
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() 
	{
		try 
		{
			dataList = getDataList();
			List<Endowment> selectedEndowments = new ArrayList<Endowment>();
			for(Endowment obj: dataList)
			{
				if( obj.isSelected() )
				{
					selectedEndowments.add(obj);
				}
			}
			
			if(selectedEndowments.size() <= 0)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add( CommonUtil.getBundleMessage("endowment.errMsg.noSelectMade") );
				return;
			}
			viewMap.put(WebConstants.SELECTED_ROWS, selectedEndowments);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			Endowment row = (Endowment) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(WebConstants.EndowmentManage.Endowment, row);
			}
		
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "endowmentManagement";
	}

	@SuppressWarnings("unchecked")
	public String onCreateDisbursementRequest() 
	{
		try 
		{
			Endowment row = (Endowment) dataTable.getRowData();
			EndowmentView view = new EndowmentView();
			view = EndowmentTransformUtil.transformToEndowmentView(view, row);
			if (row != null && view != null) 
			{
				sessionMap.put(WebConstants.SELECTED_ROW, view);
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onCreateDisbursementRequest|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "endowmentDisbursement";
	}

	
	@SuppressWarnings("unchecked")
	public String onExpense() 
	{
		try 
		{
			Endowment row = (Endowment) dataTable.getRowData();
			EndowmentView view = new EndowmentView();
			view = EndowmentTransformUtil.transformToEndowmentView(view, row);
			if (row != null && view != null) 
			{
				sessionMap.put(WebConstants.SELECTED_ROW, view);
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onExpense |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "endowmentExpense";
	}

	
	@SuppressWarnings("unchecked")
	public void onSearch() 
	{
		try 
		{
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{
		criteria = getCriteria();
		int totalRows = 0;
		if( managerType.getValue() != null )
		{
			criteria.setManagerType( managerType.getValue().toString());
		}
		if(masraf != null && masraf.getMasrafId() != null )
		{
			criteria.setMasrafId(masraf.getMasrafId());
		}
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();
		
		dataList = service.searchEndowment(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
		
		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		else
		{logger.logDebug("Records Found:%s",dataList.size());}
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}



	@SuppressWarnings("unchecked")
	public List<Endowment> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<Endowment>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<Endowment> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javascript);
	}

	@SuppressWarnings("unchecked")
	public EndowmentsSearchCriteria getCriteria() {
		if( viewMap.get("criteria") != null )
		{
			criteria = (EndowmentsSearchCriteria)viewMap.get("criteria");
		}
		return criteria;
	}
	
	@SuppressWarnings("unchecked")
	public void setCriteria(EndowmentsSearchCriteria criteria) {
		this.criteria = criteria;
		if( this.criteria != null )
		{
			viewMap.put("criteria",criteria); 
		}
	}

	public HtmlSelectOneMenu getManagerType() {
		return managerType;
	}

	public void setManagerType(HtmlSelectOneMenu managerType) {
		this.managerType = managerType;
	}
	
	@SuppressWarnings("unchecked")
	public Masraf getMasraf() 
	{
		if( viewMap.get("masraf") != null )
		{
			masraf  = (Masraf)viewMap.get("masraf") ; 
		}
		return masraf;
	}

	@SuppressWarnings("unchecked")
	public void setMasraf(Masraf masraf) 
	{
		this.masraf = masraf;
		if( this.masraf != null)
		{
			viewMap.put("masraf",this.masraf); 
		}
	}
	
	public String getMasrafId() {
		return masrafId;
	}

	public void setMasrafId(String masrafId) {
		this.masrafId = masrafId;
	}
		
}

	
