package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.RevenueDistributionDetailsService;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.mems.DistributionDetailsEndRevView;
import com.avanza.ui.util.ResourceUtil;

public class DistributeEndowmentRevenue extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	RevenueDistributionDetailsService service;
	private String pageMode;
	private PaymentScheduleView paymentToDistribute = new PaymentScheduleView();
	private List<DistributionDetailsEndRevView> dataList = new ArrayList<DistributionDetailsEndRevView>();
	private HtmlDataTable dataTable;
	private String totalAmountToDistribute;
	
	public DistributeEndowmentRevenue() 
	{
		service        			= new RevenueDistributionDetailsService();
		pageMode                = Constant.EndowmentFile.MODE_READ_ONLY;
		dataTable               = new HtmlDataTable();
		totalAmountToDistribute = "0.0";
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
//			if( !isPostBack() )
//			{
//			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			pageMode = viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE).toString();
		}
		if(sessionMap.get(WebConstants.DistributeEndowmentRevenue.PaymentView) != null)
		{
		  paymentToDistribute =  (PaymentScheduleView) sessionMap.remove(WebConstants.DistributeEndowmentRevenue.PaymentView) ;
		  dataList            =   service.getDistributionDetailsForEndowmentIdPaymentId( paymentToDistribute );
		  
		}
		else if( viewMap.get(WebConstants.DistributeEndowmentRevenue.PaymentView) != null)
		{
			paymentToDistribute =  (PaymentScheduleView) viewMap.get(WebConstants.DistributeEndowmentRevenue.PaymentView) ;
		}
		if( viewMap.get(WebConstants.DistributeEndowmentRevenue.DistributionList) != null)
		{
			dataList =  (ArrayList<DistributionDetailsEndRevView>) viewMap.get(WebConstants.DistributeEndowmentRevenue.DistributionList) ;
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
		  viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, pageMode);
		}
		if (paymentToDistribute != null )
		{
		  viewMap.put(WebConstants.DistributeEndowmentRevenue.PaymentView, paymentToDistribute) ;
		}
		if( dataList != null )
		{
			viewMap.put(WebConstants.DistributeEndowmentRevenue.DistributionList,dataList);
		}
		if( viewMap.get("totalAmountToDistribute" ) != null )
		{
			totalAmountToDistribute = viewMap.get("totalAmountToDistribute" ) .toString();
		}
	}	
	
	public void populateValuesInObject() throws Exception
	{
//		if( endowmentFileBen.getEndFileBenId()==null )
//		{
//			endowmentFileBen.setCreatedBy(	getLoggedInUserId()	);
//		}
//		endowmentFileBen.setUpdatedBy(	getLoggedInUserId()	);
	}
	
	private boolean hasDoneErrors() throws Exception
	{
		boolean hasErrors = false;
		Double amountToDistribute = 0d;
		for ( DistributionDetailsEndRevView  obj :dataList)
		{
			amountToDistribute += obj.getDistributedAmount();
		}
		
		if( paymentToDistribute.getAmount().compareTo(amountToDistribute) != 0)
		{
			String msg = java.text.MessageFormat.format( 
														ResourceUtil.getInstance().getProperty("endowmentDistribute.msg.sumOfAmountNotEqualToAmountToDistribute"),
														amountToDistribute.toString(),
														paymentToDistribute.getAmount().toString()											
													  );
			errorMessages.add(msg);
			hasErrors = true;
		}
		return hasErrors;
	}
	
	public void openStatmntOfAccount()
	{

		try	
		{
			DistributionDetailsEndRevView row = (DistributionDetailsEndRevView)dataTable.getRowData();
			
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
			statmntReportCriteria.setPersonId(row.getEndFileBen().getPerson().getPersonId());
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
	    	executeJavaScript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onBeneficiaryChange|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}

    @SuppressWarnings( "unchecked" )
	public void onDistribute()
	{
		
		try
		{
			updateValuesFromMaps();
			errorMessages = new ArrayList<String>();
			
			if( hasDoneErrors() )return;
			
			paymentToDistribute = service.distributePayment(paymentToDistribute, dataList , getLoggedInUserId() );
			saveSystemComments();
			sessionMap.put(WebConstants.DistributeEndowmentRevenue.PaymentView, paymentToDistribute );
			executeJavaScript("javaScript:sendInfoToParent();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onDistribute--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    @SuppressWarnings( "unchecked" )
	public void saveSystemComments() throws Exception
    {
	   final String notesOwner = WebConstants.REQUEST;
	   NotesController.saveSystemNotesForRequest(
			                                        notesOwner, 
			   										"thirdPartRevenue.event.PaymentDistributed", 
			   										paymentToDistribute.getRequestId() , 
			   										paymentToDistribute.getPaymentNumber()
	   										     );
    }
    public boolean getShowDistributeButton()
    {
    	if( paymentToDistribute.getIsDistributed() != null && paymentToDistribute.getIsDistributed().equals("1"))
    	{
    		return false;
    	}
    	return true;
    }
    
    public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public List<DistributionDetailsEndRevView> getDataList() {
		return dataList;
	}

	public void setDataList(List<DistributionDetailsEndRevView> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public PaymentScheduleView getPaymentToDistribute() {
		return paymentToDistribute;
	}

	public void setPaymentToDistribute(PaymentScheduleView paymentToDistribute) {
		this.paymentToDistribute = paymentToDistribute;
	}

	public String getTotalAmountToDistribute() {
		return totalAmountToDistribute;
	}

	public void setTotalAmountToDistribute(String totalAmountToDistribute) {
		this.totalAmountToDistribute = totalAmountToDistribute;
	}

}
