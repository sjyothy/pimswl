package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;

public class EndowmentPersonBeneficiaryBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = 1L;

	EndowmentService endowmentService;
	private String pageMode;
	PersonView beneficiary;
	HttpServletRequest request;
	List<EndFileAsso> dataListEndowemntFileAsso;
	private HtmlDataTable dataTableFileAsso;
	public EndowmentPersonBeneficiaryBean() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		pageMode                   	 = WebConstants.PAGE_MODE_EDIT;
		dataListEndowemntFileAsso    = new ArrayList<EndFileAsso>();
		endowmentService = new EndowmentService();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
			if(!isPostBack())
			{
				loadAttachmentsAndComments( beneficiary.getPersonId() ); 
				onEndowmentTab();
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void prerender() 
	{}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( sessionMap.get(WebConstants.PAGE_MODE)!= null )
		{
			viewMap.put(
						WebConstants.PAGE_MODE,sessionMap.remove(WebConstants.PAGE_MODE)
			           );
		}
		if ( viewMap.get( WebConstants.PAGE_MODE ) != null )
		{
			pageMode = viewMap.get(WebConstants.PAGE_MODE).toString();
		}
		
		if ( viewMap.get( "dataListEndowemntFileAsso" ) != null )
		{
			dataListEndowemntFileAsso = (ArrayList<EndFileAsso>)viewMap.get("dataListEndowemntFileAsso");
		}
		if( viewMap.get(Constant.Endowments.PERSON) != null)
		{
			beneficiary = ( PersonView )viewMap.get(Constant.Endowments.PERSON);
			
		}
		else if( request.getParameter( "personId") != null)
		{
			Long personId = Long.valueOf( request.getParameter("personId").toString());
			beneficiary = new PropertyService().getPersonById(personId);
		}
		updateValuesToMaps();
	}
	
	
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
		  viewMap.put( WebConstants.PAGE_MODE, pageMode);
		}
		if( beneficiary != null  )
		{
		  viewMap.put(Constant.Endowments.PERSON, beneficiary );
		}
		
		if (
			dataListEndowemntFileAsso != null && 
			dataListEndowemntFileAsso.size() > 0 
		   )
		{
		  viewMap.put("dataListEndowemntFileAsso", dataListEndowemntFileAsso );
		}
	}	
	
	private boolean hasDoneErrors() throws Exception
	{
		boolean hasErrors = false;
		return hasErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEndowmentTab()
	{
		try	
		{
			updateValuesFromMaps();
            dataListEndowemntFileAsso =  endowmentService.getEndowmentFileAssosByPersonId(beneficiary.getPersonId() );
            updateValuesToMaps();
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowmentTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	

	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);

//		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.EXTERNAL_ID_PERSON);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    	viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_PERSON);
    	viewMap.put("noteowner", WebConstants.NOTES_OWNER_PERSON);
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
    @SuppressWarnings( "unchecked" )
	public void onDone()
	{
		
		try
		{
			updateValuesFromMaps();
			errorMessages = new ArrayList<String>();
			if( hasDoneErrors() )
			{return;}
			executeJavaScript("sendDataToParent();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onDone--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_EDIT ) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public PersonView getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(PersonView beneficiary) {
		this.beneficiary = beneficiary;
	}

	public List<EndFileAsso> getDataListEndowemntFileAsso() {
		return dataListEndowemntFileAsso;
	}

	public void setDataListEndowemntFileAsso(
			List<EndFileAsso> dataListEndowemntFileAsso) {
		this.dataListEndowemntFileAsso = dataListEndowemntFileAsso;
	}

	public HtmlDataTable getDataTableFileAsso() {
		return dataTableFileAsso;
	}

	public void setDataTableFileAsso(HtmlDataTable dataTableFileAsso) {
		this.dataTableFileAsso = dataTableFileAsso;
	}

}
