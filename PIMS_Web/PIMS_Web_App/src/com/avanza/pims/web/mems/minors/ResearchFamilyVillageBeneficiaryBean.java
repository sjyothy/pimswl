package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.SurveyFormController;
import com.avanza.pims.web.mems.plugins.FamilyVillageBehaviorAspectsTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageBeneficiaryRecommendationTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageEducationAspectTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageFamilyAspectsTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageHealthAspectTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageInterventionPlanTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillageProblemAspectTabBacking;
import com.avanza.pims.web.mems.plugins.FamilyVillagePsychologyAspectsTabBacking;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;


public class ResearchFamilyVillageBeneficiaryBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(ResearchFamilyVillageBeneficiaryBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private String VIEW_MODE = "pageMode";
	private String PAGE_MODE_POPUP = "MODE_SELECT_ONE_POPUP";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_FAMILY_ASPECT_SOCIAL ="tabFamilyAspectSocial";
	private static final String TAB_ASPECT_PSYCHOLOGY ="tabAspectPsychology";
	private static final String TAB_ASPECT_INTERVENTION_PLAN ="tabFamilyVillageInterventionPlan";
	private static final String TAB_ASPECT_PROBLEM_ASPECT ="tabFamilyVillageProblemAspect";
	private static final String TAB_ASPECT_EDUCATION_ASPECT ="tabFamilyVillageEducationAspect";
	private static final String TAB_ASPECT_BEHAVIORAL_ASPECT ="tabFamilyVillageBehavioralAspect";
	
	private static final String TAB_ASPECT_HEALTH_ASPECT ="tabFamilyVillageHealthAspect";
    private String pageTitle;
    private String pageMode;
    private String txtRemarks;
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	FamilyVillageFileService fvService = new FamilyVillageFileService();
	private PersonView beneficiaryPerson = new PersonView();

     public ResearchFamilyVillageBeneficiaryBean(){
    	 
     }
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
    

	@SuppressWarnings("unchecked")
	public void onOpenResearcherForm()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			
			updateValuesFromMap();
			sessionMap.put(WebConstants.PERSON_ID, beneficiaryPerson.getPersonId());
			executeJavascript("openResearchFormFamilyVillage();");
			
		} catch (Exception e) {
			logger.LogException("onOpenResearcherForm--- EXCEPTION --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} finally {
		}
	}
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_PERSON);
		viewMap.put("noteowner", WebConstants.NOTES_OWNER_PERSON);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		if(request.getParameter(VIEW_MODE)!=null && request.getParameter(VIEW_MODE).equals(PAGE_MODE_POPUP)) 
	   	{ 	      	    		
	 	    viewMap.put(VIEW_MODE, PAGE_MODE_POPUP);
	 	}
	   	if(request.getParameter(WebConstants.PERSON_ID)!=null) 
	   	{ 	      	    		
	   		beneficiaryPerson.setPersonId( Long.valueOf(request.getParameter(WebConstants.PERSON_ID).toString()) );
	    }
	   	else if(sessionMap.get(WebConstants.PERSON_ID )!= null )
	   	{
	   		beneficiaryPerson.setPersonId( Long.valueOf(sessionMap.remove(WebConstants.PERSON_ID).toString() ) );
	   	}
	   	isViewModePopUp();
		getData();
		loadAttachmentsAndComments( beneficiaryPerson.getPersonId() );
		//First Tab Click Action will be invoked below. If control transferred from Any other page then tab related to that page will be invoked
		if(sessionMap.remove("CONTROL_TRANSFERRED_FROM")!= null )
	   	{
	   	    onBehaviorAspectsTab();	
	   	    tabPanel.setSelectedTab(TAB_ASPECT_BEHAVIORAL_ASPECT);
	   	}
		else
		{
			onFamilyAspectsTab();
		}
		updateValuesFromMap();
	}
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_FAMILY_ASPECT_SOCIAL);
		}
	
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_FAMILY_ASPECT_SOCIAL);
		}
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_PSYCHOLOGY);
		}
	
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_PSYCHOLOGY);
		}
		
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_INTERVENTION_PLAN);
		}
	
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_INTERVENTION_PLAN);
		}
		
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_PROBLEM_ASPECT);
		}
	
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_PROBLEM_ASPECT);
		}
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.HEALTH_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.HEALTH_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_HEALTH_ASPECT);
		}
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.HEALTH_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.HEALTH_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_HEALTH_ASPECT);
		}
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.EDUCATION_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.EDUCATION_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_EDUCATION_ASPECT);
		}
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.EDUCATION_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.EDUCATION_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_EDUCATION_ASPECT);
		}
		
		if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS ) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_BEHAVIORAL_ASPECT);
		}
		else if(viewMap.get(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ASPECT_BEHAVIORAL_ASPECT);
		}
		
		
	}
	public void prerender(){
		try{
				getErrorMessagesFromTab();
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON ) != null )
		{
			beneficiaryPerson= ( PersonView)viewMap.get( WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON) ;
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( beneficiaryPerson != null )
		{
		  viewMap.put(  WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON , beneficiaryPerson);
		}
			

	}
	@SuppressWarnings( "unchecked" )
	private void getData()throws Exception
	{
		beneficiaryPerson = fvService.getBeneficiaryPersonById(beneficiaryPerson.getPersonId());
		
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(beneficiaryPerson.getPersonId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.NOTES_OWNER_PERSON;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, beneficiaryPerson.getPersonId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.NOTES_OWNER_PERSON;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(beneficiaryPerson.getPersonId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, beneficiaryPerson.getPersonId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
			 loadAttachmentsAndComments( beneficiaryPerson.getPersonId() );
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }

	public void onFamilyAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageFamilyAspectsTabBacking bean = (FamilyVillageFamilyAspectsTabBacking)getBean("pages$tabFamilyVillageFamilyAspects");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onFamilyAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onPsychologicalAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillagePsychologyAspectsTabBacking bean = (FamilyVillagePsychologyAspectsTabBacking)getBean("pages$tabFamilyVillagePsychologyAspects");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPsychologyAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onBehaviorAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageBehaviorAspectsTabBacking bean = (FamilyVillageBehaviorAspectsTabBacking)getBean("pages$tabFamilyVillageBehaviorAspects");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onBehaviorAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	public void onInterventionPlanTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageInterventionPlanTabBacking bean = (FamilyVillageInterventionPlanTabBacking)getBean("pages$tabFamilyVillageInterventionPlan");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onInterventionPlanTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	public void onRecommendationsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageBeneficiaryRecommendationTabBacking bean = (FamilyVillageBeneficiaryRecommendationTabBacking)getBean("pages$tabFamilyVillageBeneficiaryRecommendation");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onRecommendationsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	public void onHealthAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageHealthAspectTabBacking bean = (FamilyVillageHealthAspectTabBacking)getBean("pages$tabFamilyVillageHealthAspect");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onHealthAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onProblemAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageProblemAspectTabBacking bean = (FamilyVillageProblemAspectTabBacking)getBean("pages$tabFamilyVillageProblemAspect");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onProblemAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onEducationAspectsTab()
	{
		try	
		{
            updateValuesFromMap();       
            FamilyVillageEducationAspectTabBacking bean = (FamilyVillageEducationAspectTabBacking)getBean("pages$tabFamilyVillageEducationAspect");
            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onEducationAspectsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	public boolean isViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().equals( PAGE_MODE_POPUP) )
		{
				returnVal = true;
		}
		else {
			returnVal = false;
		}

		return returnVal;

	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.NOTES_OWNER_PERSON,beneficiaryPerson.getPersonId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
		public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}


	public PersonView getBeneficiaryPerson() {
		return beneficiaryPerson;
	}


	public void setBeneficiaryPerson(PersonView beneficiaryPerson) {
		this.beneficiaryPerson = beneficiaryPerson;
	}


	
	

}