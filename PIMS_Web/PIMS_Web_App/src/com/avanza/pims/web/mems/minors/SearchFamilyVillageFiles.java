package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.EndProgSearchCriteria;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.constant.Constant.VillaCategory;
import com.avanza.pims.entity.EndowmentProgram;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FamilyVillageFileView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.ui.util.ResourceUtil;

public class SearchFamilyVillageFiles extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "inheritanceFileId";
	private final String DATA_LIST = "DATA_LIST";
	FamilyVillageFileService service = new FamilyVillageFileService ();

	EndProgSearchCriteria criteria = new EndProgSearchCriteria();
	List<FamilyVillageFileView> dataList = new ArrayList<FamilyVillageFileView>();
	private String fileNumber;
	private String beneficiaryName;
	private Date createdOnFrom;
	private Date createdOnTo;
	private String costCenter;
	private String villaNumber;
	private String selectOneVillaCategory;
	private String createdBy;
	private String status;

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	public boolean getIsPageModeSelectOnePopUp()
	{
		if(viewMap.get(VIEW_MODE)!=null && viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_ONE_POPUP))
		{
			return true;
		}
		return false;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			FamilyVillageFileView obj = null;
			obj = (FamilyVillageFileView) dataTable.getRowData();
			
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked") 
	public List<SelectItem> getFileStatusList() throws Exception {

		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			
			if (viewMap.get("FileStatusList") != null) {

				return (List<SelectItem>) viewMap.get("FileStatusList");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName("INH_FILE_STATUS");
//			{
//				ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),new Locale("ar"));
//				String msgAr = bundleAr.getString("commons.All");
//				ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), new Locale("ar"));
//				String msgEn = bundleEn.getString("commons.All");
//				selectItems.add(new SelectItem("-1", isEnglishLocale() ?msgEn: msgAr));
//			}
			for (DomainDataView dd : list) 
			{
				if(dd.getDomainDataId().compareTo(Constant.InheritanceFileStatus.NEW_ID)!= 0 &&
				   dd.getDomainDataId().compareTo(Constant.InheritanceFileStatus.APPROVAL_REQUIRED_ID)!= 0 &&
				   dd.getDomainDataId().compareTo(Constant.InheritanceFileStatus.APPROVED_ID )!= 0 &&
				   dd.getDomainDataId().compareTo(Constant.InheritanceFileStatus.REJECTED_ID)!= 0 
			      )
					continue;
				
				selectItems.add(new SelectItem(dd.getDomainDataId().toString(), isEnglishLocale() ? 
						dd.getDataDescEn()
						: dd.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("FileStatusList", selectItems);

		} catch (Exception e) {
			logger.LogException("getFileStatusList() crashed", e);
		}
		return selectItems;

	}


	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "OPEN_FAMILY_VILLAGE_FILE";
	}
	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			FamilyVillageFileView row = (FamilyVillageFileView) dataTable.getRowData();
			
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, row.getInheritanceFileId());
			return "EDIT_FAMILY_VILLAGE_FILE";
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	private Map<Object,Object> getSearchCriteriaMap()
	{
		Map<Object,Object> map  = new HashMap<Object, Object>();
		if(StringHelper.isNotEmpty(fileNumber))
		{
			map.put(Constant.INHERITANCE_SEARCH_CRITERIA.FILE_NUMBER, fileNumber.trim().toLowerCase());
		}
		if(StringHelper.isNotEmpty(beneficiaryName))
		{
			map.put(Constant.INHERITANCE_SEARCH_CRITERIA.BENEFICIARY, beneficiaryName.trim().toLowerCase());
		}
		if( !selectOneVillaCategory.equals("-1"))
		{
			map.put(Constant.VillaSearchCriteria.VILLA_CATEGORY, selectOneVillaCategory);
		}
		if(createdOnFrom!= null)
		{
			map.put(Constant.INHERITANCE_SEARCH_CRITERIA.CREATED_ON_FROM, createdOnFrom);
			
		}
		if(createdOnTo!= null)
		{
			map.put(Constant.INHERITANCE_SEARCH_CRITERIA.CREATED_ON_TO, createdOnTo);
			
		}
		return map;
	}
	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		Map searchCriteriaMap  = getSearchCriteriaMap();
		int totalRows = 0;
		totalRows = service.searchFamilyVillageFilesGetTotalNumberOfRecords( searchCriteriaMap );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.searchFiles(searchCriteriaMap, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}


	@SuppressWarnings("unchecked")
	public List<FamilyVillageFileView> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<FamilyVillageFileView>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<FamilyVillageFileView> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public Date getCreatedOnFrom() {
		return createdOnFrom;
	}

	public void setCreatedOnFrom(Date createdOnFrom) {
		this.createdOnFrom = createdOnFrom;
	}

	public Date getCreatedOnTo() {
		return createdOnTo;
	}

	public void setCreatedOnTo(Date createdOnTo) {
		this.createdOnTo = createdOnTo;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getVillaNumber() {
		return villaNumber;
	}

	public void setVillaNumber(String villaNumber) {
		this.villaNumber = villaNumber;
	}

	public String getSelectOneVillaCategory() {
		return selectOneVillaCategory;
	}

	public void setSelectOneVillaCategory(String selectOneVillaCategory) {
		this.selectOneVillaCategory = selectOneVillaCategory;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
