package com.avanza.pims.web.mems.endowment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;

import com.avanza.core.util.Logger;
import com.avanza.pims.Utils.EndowmentsSearchCriteria;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.EndFileBen;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentExpense;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.TabRevenueAndExpensesList;
import com.avanza.pims.web.mems.plugins.ThirdPartyPaymentsTab;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentExpenseService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.mems.EndowmentCompensateVO;
import com.avanza.pims.ws.vo.mems.EndowmentExpenseView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;


public class EndowmentManageBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(EndowmentManageBean.class);
	HttpServletRequest request;
    private EndowmentView endowment;
    private String totalBeneficiaryPercentage;
	private List<EndFileAsso> listEndFileAsso ;
	private List<EndFileBen> listEndFileBen ;
	private HtmlDataTable dataTableFileAsso;
	
	private List<EndowmentExpenseView> dataListExpenses = new ArrayList<EndowmentExpenseView>();
	private HtmlDataTable dataTableExpenses;
	private HtmlDataTable dataTableFileBen;
    private String hdnPersonId;
    private String hdnPersonName;
    private HtmlSelectBooleanCheckbox chkIsManagerAmaf;
    private HtmlSelectBooleanCheckbox chkOnAgreement;
    private String pageMode;
    private String bankIdStr;
    private EndowmentService service;
    private HtmlPanelGrid gridCash = new HtmlPanelGrid();
    private HtmlPanelGrid gridCheque = new HtmlPanelGrid();
    private HtmlPanelGrid gridBankTransfer = new HtmlPanelGrid();
    private HtmlPanelGrid gridTransportation = new HtmlPanelGrid();
	private HtmlPanelGrid gridVehicles = new HtmlPanelGrid();
	private HtmlPanelGrid gridAnimals = new HtmlPanelGrid();
	private HtmlPanelGrid gridLandProperties = new HtmlPanelGrid();
	private HtmlPanelGrid gridLicenses = new HtmlPanelGrid();
	private HtmlPanelGrid gridStockshares  =new HtmlPanelGrid();
	private Integer expensesRecordsSize=0; 
	List<EndowmentCompensateVO> compensateDataList = new ArrayList<EndowmentCompensateVO>();
	
	List<EndowmentCompensateVO> manageCompensateDataList = new ArrayList<EndowmentCompensateVO>();
	
	EndowmentCompensateVO compensateVO = new EndowmentCompensateVO();
	
	EndowmentService endowmentService = new EndowmentService();
	
	private HtmlDataTable dataTableEndowmentCompensate;
	
	DecimalFormat decimalFormatter = new DecimalFormat("#.00");
	    
    public HtmlPanelGrid getGridCash() {
		return gridCash;
	}
	public void setGridCash(HtmlPanelGrid gridCash) {
		this.gridCash = gridCash;
	}
	public EndowmentManageBean()
    {
    	request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	dataTableFileAsso        = new HtmlDataTable();
    	dataTableFileBen  		 = new HtmlDataTable();
    	dataTableExpenses        =  new HtmlDataTable();
    	chkIsManagerAmaf = new HtmlSelectBooleanCheckbox();
    	chkOnAgreement = new HtmlSelectBooleanCheckbox();
    	service  = new EndowmentService();
    	listEndFileAsso = new ArrayList<EndFileAsso>();
    	listEndFileBen  = new ArrayList<EndFileBen>(); 
    	
    }
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
				updateValuesFromMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		updateValuesFromMap();
		loadAttachmentsAndComments( endowment.getEndowmentId() );
	}
	
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
			getErrorMessagesFromTab();
			displayExtraFieldsGridsBasedOnType();
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( sessionMap.get( WebConstants.EndowmentManage.Endowment  ) != null )
		{
			Endowment end = (Endowment)sessionMap.remove( WebConstants.EndowmentManage.Endowment  ) ;
			getEndowmentDetailsById( end.getEndowmentId() );
			if( endowment != null && endowment.getBankId() != null )
			{
				bankIdStr= endowment.getBankId().toString();
			}
		}
		else if(request.getParameter("endowmentId")!=null)
		{
			long endowmentId = Long.valueOf( request.getParameter("endowmentId").toString() );
			getEndowmentDetailsById( endowmentId );
			if( endowment != null && endowment.getBankId() != null )
			{
				bankIdStr= endowment.getBankId().toString();
			}
		} 
		else if( viewMap.get( WebConstants.EndowmentManage.Endowment  ) != null )
		{
			endowment = (EndowmentView)viewMap.get( WebConstants.EndowmentManage.Endowment  ) ;
		}
		if( request.getParameter("BuildFrom")!= null ){
			onCompensateTabClick();
		}
		if( viewMap.get(WebConstants.EndowmentManage.Files) != null)
		{
			listEndFileAsso = (ArrayList<EndFileAsso>)viewMap.get(WebConstants.EndowmentManage.Files) ; 
		}
		if( viewMap.get(WebConstants.EndowmentManage.Beneficiaries) != null)
		{
			listEndFileBen = (ArrayList<EndFileBen>)viewMap.get(WebConstants.EndowmentManage.Beneficiaries) ; 
		}
		if(request.getParameter("pageMode")!=null)
		{
			pageMode = request.getParameter("pageMode").toString() ;
		}
		if( viewMap.get("totalBeneficiaryPercentage" ) != null )
		{
			totalBeneficiaryPercentage = viewMap.get("totalBeneficiaryPercentage" ) .toString();
		}
		if( viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate) != null )
		{
			compensateVO = (EndowmentCompensateVO)viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate);
		}
		
		if( viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate) != null )
		{
			compensateVO = (EndowmentCompensateVO)viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate);
		}
		
		if( viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate_List) != null )
		{
			compensateDataList = (List<EndowmentCompensateVO>)viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate_List);
		}
		if( viewMap.get(WebConstants.EndowmentManage.Endowment_Compensate_List) != null )
		{
			manageCompensateDataList = (List<EndowmentCompensateVO>)viewMap.get(WebConstants.EndowmentManage.New_Endowment_List);
		}
		if(viewMap.get(WebConstants.EndowmentManage.RECORD_SIZE) != null)
			recordSize = (Integer)viewMap.get(WebConstants.EndowmentManage.RECORD_SIZE);
		
		 updateValuesToMap();

	}
@Override
public void setRecordSize(Integer recordSize) {
	// TODO Auto-generated method stub
	super.setRecordSize(recordSize);
}
	/**
	 * 
	 */
	private void getEndowmentDetailsById(Long endowmentId) throws Exception 
	{
		endowment = service.getEndowmentsViewById(endowmentId);
		if( endowment.getIsAmafManager().equals("1") )
		{
			chkIsManagerAmaf.setValue(true);
			hdnPersonId           = "";
			hdnPersonName         = "";
		}
		else if( endowment.getManager()!= null && endowment.getManager().getPersonId() != null)
		{
			chkIsManagerAmaf.setValue(false);
			hdnPersonId           = endowment.getManager().getPersonId().toString();
			hdnPersonName         = endowment.getManager().getPersonFullName() ;
		}
		if(endowment.getOnAgreement().equals("1"))
		{
			chkOnAgreement.setValue( true );
		}
		else
		{
			chkOnAgreement.setValue( false );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( endowment != null )
		{
			 viewMap.put( WebConstants.EndowmentManage.Endowment ,endowment ) ;
			 endowment.setBankIdStr(bankIdStr);
		}
		
		if( listEndFileAsso  != null)
		{
			 viewMap.put(WebConstants.EndowmentManage.Files, listEndFileAsso ) ; 
		}
		if( listEndFileBen  != null)
		{
			viewMap.put(WebConstants.EndowmentManage.Beneficiaries,  listEndFileBen ) ; 
		}
		if(totalBeneficiaryPercentage  != null )
		{
			viewMap.put("totalBeneficiaryPercentage" ,totalBeneficiaryPercentage );
		}
		if(bankIdStr != null)
		{
			viewMap.put("bankIdStr",bankIdStr);
		}
		if(compensateVO != null){
			viewMap.put(WebConstants.EndowmentManage.Endowment_Compensate, compensateVO ) ;
		}
		if(compensateDataList != null){
			viewMap.put(WebConstants.EndowmentManage.Endowment_Compensate_List, compensateDataList ) ;
		}
		if(manageCompensateDataList != null){
			viewMap.put(WebConstants.EndowmentManage.New_Endowment_List, manageCompensateDataList ) ;
		}
		if(recordSize != null)
			viewMap.put(WebConstants.EndowmentManage.RECORD_SIZE,recordSize);
	}
	
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{}

	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);

		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.EndowmentManage.PROCEDURE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    	viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_ENDOWMENT);
		viewMap.put("noteowner", WebConstants.EndowmentManage.NOTES_OWNER);
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(endowment.getEndowmentId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.EndowmentManage.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, endowment.getEndowmentId() );
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
    	String notesOwner = WebConstants.EndowmentManage.NOTES_OWNER;
    	NotesController.saveNotes(notesOwner, endowment.getEndowmentId());
    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		
		boolean isPercentagesEmpty=  
												( endowment.getRevenuePercentageString()== null || endowment.getRevenuePercentageString().trim().length()<=0) 
										&&
												( endowment.getReconstructionPercentageString()== null || endowment.getReconstructionPercentageString().trim().length()<=0);
		boolean isPercentageInValid=Double.valueOf(  endowment.getRevenuePercentageString())+Double.valueOf(  endowment.getReconstructionPercentageString())!=100d ;
									
		if( isPercentagesEmpty || isPercentageInValid)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.RevenueReconstructionPercentageInvalid"));
			hasSaveErrors=true;
			
		}
		return hasSaveErrors;
		
	}
	private boolean hasCompensateErrors()throws Exception{
		boolean hasSaveErrors=false;
		
		if(StringUtils.isBlank(compensateVO.getNewEndowment().getEndowmentName())){
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.enodwmentSearch"));
			hasSaveErrors = true;
		}
		if(compensateVO.getPercentage() == null){
			errorMessages.add(ResourceUtil.getInstance().getProperty("accountType.add.error.percentage"));
			hasSaveErrors = true;
		}
		else if(compensateVO.getPercentage() < 0 ){
			errorMessages.add(ResourceUtil.getInstance().getProperty("accountType.add.error.percentage.negative"));
			hasSaveErrors = true;
		}
		else if(compensateVO.getPercentage() > 100 ){
			errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.error.greaterPercentage"));
			hasSaveErrors = true;
		}
		else if((compensateVO.getTotalPercantage()+compensateVO.getPercentage().doubleValue()) > 100){
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.sumPercentage"));
			hasSaveErrors = true;
		}
		else if(compensateVO.getNewEndowmentPercentage()<= 0 || compensateVO.getNewEndowmentPercentage() > 100 ){
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.newEndowmentpercentageInvalidValue"));
			hasSaveErrors = true;
		}
		
		else
		{
			
			if((compensateVO.getTotalNewEndowmentPercantage() +compensateVO.getNewEndowmentPercentage().doubleValue()) > 100){
		
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.newEndowmentpercentageSumShouldBeHundred"));
			hasSaveErrors = true;
			}
		}
		if(compensateDataList.contains(compensateVO)){
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.error.duplicateEndowment"));
			hasSaveErrors = true;
		}
		if(endowment.getEndowmentId().equals(compensateVO.getNewEndowment().getEndowmentId())){
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.error.parentEndowment"));
			hasSaveErrors = true;
		}
		
		return hasSaveErrors;
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			
			endowment.setUpdatedBy( getLoggedInUserId() );
			if( 
					chkOnAgreement.getValue() != null && 
					((Boolean)chkOnAgreement.getValue() )==true 
			  ) 
			{
				endowment.setOnAgreement( "1" );
			}
			else
			{
				endowment.setOnAgreement( "0" );
			}
			if(bankIdStr != null && !bankIdStr.equals("-1"))
			{
				endowment.setBankIdStr(bankIdStr);
			}
			service.persist( endowment );
			getEndowmentDetailsById( endowment.getEndowmentId() );
			saveCommentsAttachment("common.messages.Updated");
			successMessages.add( ResourceUtil.getInstance().getProperty("commons.save"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{
			if(endowment == null || endowment.getEndowmentId() ==null) return;
			
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(
		    									WebConstants.EndowmentManage.NOTES_OWNER,
		    									endowment.getEndowmentId().toString()
		    								);
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	
	
	@SuppressWarnings( "unchecked" )
	public void onCompensateTabClick()
	{
		try	
		{
//			loadEndowmentCompensate(endowment.getEndowmentId());
			EndowmentsSearchCriteria searchCriteria = new EndowmentsSearchCriteria();
			Endowment searchEndowment = new Endowment();
			searchEndowment.setEndowmentId(endowment.getEndowmentId());
			searchCriteria.setEndowment(searchEndowment);
			searchCriteria.setCompensate(true);
			manageCompensateDataList = service.getNewEndowmentCompensateVOByEndowmentId(searchCriteria);
			setTotalCompensateRecords(manageCompensateDataList == null ? 0:manageCompensateDataList.size());
		}
		catch(Exception ex)
		{
			logger.LogException("onCompensateTabClick|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onNewCompensateTabClick()
	{
		try	
		{
			loadEndowmentCompensate(endowment.getEndowmentId());	
		}
		catch(Exception ex)
		{
			logger.LogException("onNewCompensateTabClick|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	public void onExpensesTab()
	{
		try	
		{
			if(endowment == null || endowment.getEndowmentId() ==null ) return;
			EndowmentExpenseService expenseService = new EndowmentExpenseService();
			EndowmentExpense criteria = new EndowmentExpense();
			Endowment endowment = new Endowment();
			endowment.setEndowmentId( endowment.getEndowmentId()  );
			criteria.setEndowment(endowment);
			expensesRecordsSize =  expenseService.getTotalNumberOfRecords(criteria);
			dataListExpenses    =  expenseService.getEndowmentExpensesViewList(criteria, null, null, "expenseId", false);
		}
		catch(Exception ex)
		{
			logger.LogException("onExpensesTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onThirdPartyPaymentsTab()
	{
		try	
		{
			if(endowment == null || endowment.getEndowmentId() ==null ) return;
            ThirdPartyPaymentsTab tab = ( ThirdPartyPaymentsTab )getBean( "pages$thirdPartyPaymentsTab" );
		    tab.onDataFromParent(
		    						endowment.getEndowmentId(), 
		    						null 
		    					);
		}
		catch(Exception ex)
		{
			logger.LogException("onThirdPartyPaymentsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRevenueAndExpensesListTab()
	{
		try	
		{
			if(endowment == null || endowment.getEndowmentId() ==null ) return;
            TabRevenueAndExpensesList tab = ( TabRevenueAndExpensesList )getBean( "pages$tabRevenueAndExpensesList" );
		    tab.onDataFromParent(
		    						endowment.getEndowmentId()
		    					);
		}
		catch(Exception ex)
		{
			logger.LogException("onRevenueAndExpensesListTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromDistributeEndowmentRevenue()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			PaymentScheduleView fromPopup=(PaymentScheduleView)sessionMap.remove(WebConstants.DistributeEndowmentRevenue.PaymentView);
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromDistributeEndowmentRevenue--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onEndowmentFileAssoTab()
	{
		try	
		{

			updateValuesFromMap();
			if(endowment == null || endowment.getEndowmentId() ==null) return;
			listEndFileAsso = service.getEndowmentsFileAssoByEndowmentId( endowment.getEndowmentId()  );
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowmentFileAssoTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	@SuppressWarnings( "unchecked" )
	public void onOpenPersonBeneficiaryPopup()
	{
		try	
		{
			updateValuesFromMap();
			EndFileBen row = (EndFileBen)dataTableFileBen.getRowData();
			if( row.getPerson().getPersonId() != null )
			{
			 executeJavaScript("javaScript:openManagePersonBeneficiaryPopup("+row.getPerson().getPersonId()+");");
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenPersonBeneficiaryPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	public void onOpenMasrafPopup()
	{
		try	
		{

			updateValuesFromMap();
			EndFileBen row = (EndFileBen)dataTableFileBen.getRowData();
			if( row.getMasrafId() != null )
			{
				executeJavaScript("javaScript:openManageMasrafPopup("+row.getMasrafId()+");");
			}
			
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowmentFileAssoTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	@SuppressWarnings( "unchecked" )
	public void onEndowmentFileBenTab()
	{
		try	
		{
			updateValuesFromMap();
			if(endowment == null || endowment.getEndowmentId() ==null) return;
			listEndFileBen = service.getEndowmentsBeneficiariesByEndowmentId( endowment.getEndowmentId()  );
			double totalPer= 0.0d;
			for (EndFileBen item : listEndFileBen) {
				if(item.getSharePercentageFile() != null )
				{
					totalPer += item.getSharePercentageFile();
				}
			}
			
			totalBeneficiaryPercentage = decimalFormatter.format(totalPer);
		
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowmentFileBenTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	

	@SuppressWarnings("unchecked")
	public void onOpenEndowmentStatementOfAccountCriteriaPopup()
	{
		try
		{
			updateValuesFromMap();
			if(endowment == null || endowment.getEndowmentId() ==null) return;
			sessionMap.put(WebConstants.EndowmentManage.EndowmentStatement, endowment);
			executeJavaScript( "openStatementofAccountEndowmentCriteriaPopup();");
			
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenEndowmentStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			compensateVO = new EndowmentCompensateVO();
			
			if( sessionMap.get(WebConstants.SELECTED_ROW) != null  )
			{
				
				compensateVO.setNewEndowment( (EndowmentView)sessionMap.remove(WebConstants.SELECTED_ROW));
				compensateVO.setTotalNewEndowmentPercantage( 
									EndowmentService.getNewEndowmentCompensationTotalPercentage(compensateVO.getNewEndowment().getEndowmentId(),compensateVO.getId())
														    );
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.endowmentNotCostCenter"));
				return;
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchEndowments|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onSaveCompensate(){
		try{
			if(hasCompensateErrors()) return;
			compensateVO.setEndowment(endowment);
			Long id = endowmentService.persist(compensateVO,getLoggedInUserId());
			
			compensateVO.setId(id);
			EndowmentCompensateVO endowmentCompensateVO = compensateVO;
			compensateDataList.add(endowmentCompensateVO);

			DecimalFormat df = new DecimalFormat("#.00"); 
			endowmentCompensateVO.setPercentage(Double.parseDouble(df.format(endowmentCompensateVO.getPercentage())));
			Double totalPercentage = compensateVO.getTotalPercantage()+endowmentCompensateVO.getPercentage().doubleValue();
			Double totalNewEndowmentPercentage = compensateVO.getTotalNewEndowmentPercantage()+endowmentCompensateVO.getNewEndowmentPercentage().doubleValue();
			Double totalAmount = compensateVO.getTotalAmount()+endowmentCompensateVO.getCompensationAmount().doubleValue();
			compensateVO = new EndowmentCompensateVO();
			
			compensateVO.setNewEndowment(new EndowmentView());
			compensateVO.setTotalPercantage(totalPercentage);
			compensateVO.setTotalAmount(totalAmount);
			compensateVO.setTotalNewEndowmentPercantage( totalNewEndowmentPercentage);
			
			updateValuesToMap();
			
			NotesController.saveSystemNotesForRequest(WebConstants.EndowmentManage.NOTES_OWNER,
					"endowment.messages.compensateAdded", 
					endowment.getEndowmentId(),
					new Object[]{endowmentCompensateVO.getPercentage().doubleValue(),endowmentCompensateVO.getNewEndowment().getEndowmentName()});
			
			successMessages.add( ResourceUtil.getInstance().getProperty("successMsg.costCenterAdded"));
			
		}catch(Exception ex){
			logger.LogException("onSaveCompensate|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		
		}
	}
	
	public void onDeleteCompensate(){
		try{
			EndowmentCompensateVO compensateVOToDelete = ((EndowmentCompensateVO)dataTableEndowmentCompensate.getRowData());
			service.deleteCompensate(compensateVOToDelete.getId());
			compensateDataList.remove(compensateVOToDelete);
			setTotalCompensateRecords(compensateDataList == null ? 0:compensateDataList.size());
			compensateVO.setTotalPercantage(compensateVO.getTotalPercantage() - compensateVOToDelete.getPercentage().doubleValue());
			if(compensateVOToDelete.getCompensationAmount() != null ){
			compensateVO.setTotalAmount(compensateVO.getTotalAmount() - compensateVOToDelete.getCompensationAmount().doubleValue());
			}
			updateValuesToMap();
			
			NotesController.saveSystemNotesForRequest(WebConstants.EndowmentManage.NOTES_OWNER,
					"endowment.messages.compensateDeleted", 
					endowment.getEndowmentId(),
					new Object[]{compensateVOToDelete.getNewEndowment().getEndowmentName()});
			successMessages.add( ResourceUtil.getInstance().getProperty("successMsg.costCenterDelete"));
			
		}catch(Exception ex){
			logger.LogException("onDeleteCompensate|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		
		}
		
	}
	
	public void onEditCompensate(){
		try{
			EndowmentCompensateVO compensateVOToEdit = ((EndowmentCompensateVO)dataTableEndowmentCompensate.getRowData());
			if(StringUtils.isNotEmpty(compensateVO.getNewEndowment().getEndowmentNum())){
				EndowmentCompensateVO compensateVOToAdd = compensateVO; 
				compensateDataList.add(compensateVOToAdd);
			}
			compensateVO.setNewEndowment(compensateVOToEdit.getNewEndowment());
			compensateVO.setId(compensateVOToEdit.getId());
			compensateVO.setPercentage(compensateVOToEdit.getPercentage());
			compensateVO.setNewEndowmentPercentage(compensateVOToEdit.getNewEndowmentPercentage());
			compensateVO.setCompensationAmount(compensateVOToEdit.getCompensationAmount());
			compensateVO.setTotalPercantage(Double.valueOf(decimalFormatter.format
															(compensateVO.getTotalPercantage() - compensateVOToEdit.getPercentage())));
			if(compensateVOToEdit.getCompensationAmount() != null){
			compensateVO.setTotalAmount(Double.valueOf(decimalFormatter.format(
					compensateVO.getTotalAmount() - compensateVOToEdit.getCompensationAmount().doubleValue())));
			}
			compensateVO.setTotalNewEndowmentPercantage( 
							EndowmentService.getNewEndowmentCompensationTotalPercentage(compensateVO.getNewEndowment().getEndowmentId(),compensateVO.getId())
							);
			compensateDataList.remove(compensateVOToEdit);
			setTotalCompensateRecords(compensateDataList == null ? 0:compensateDataList.size());
			updateValuesToMap();
		}catch(Exception ex){
			logger.LogException("loadEndowmentCompensate|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	public void loadEndowmentCompensate(Long id){
		
		try{
			compensateVO = new EndowmentCompensateVO();
			compensateDataList = service.getEndowmentCompensateVOByEndowmentId(id,compensateVO);
			setTotalCompensateRecords(compensateDataList == null ? 0:compensateDataList.size());
			
			updateValuesToMap();
		}catch(Exception ex){
			logger.LogException("loadEndowmentCompensate|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}
	
	void setTotalCompensateRecords(int size){
		
		setRecordSize(size);
	}
	

	/**
	 * 
	 */
	private void displayExtraFieldsGridsBasedOnType()throws Exception
	{
		gridLandProperties.setRendered(false);
		gridStockshares.setRendered(false);
		gridTransportation.setRendered(false);
		gridVehicles.setRendered(false);
		gridAnimals.setRendered(false);
		gridLicenses.setRendered(false);
		gridCash.setRendered(false);
		if(isCash())
		{
			gridCash.setRendered(true);
		}
		if(isLandProperties())
		{
			gridLandProperties.setRendered(true);
		}
		if(isLicenses())
		{
			gridLicenses.setRendered(true);
		}
		else if( isTransportation() )
		{
			gridTransportation.setRendered(true);
		}
		else if ( isVehicles() )
		{
			gridVehicles.setRendered(true);
		}
		else if (  isStockshares())
		{
			gridStockshares.setRendered(true);
		}
		else if( isAnimals()  )
		{
			gridAnimals.setRendered(true);
		}
	}
	
	public boolean isTransportation() 
	{
		
		return 	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.TRANSPORTATIONS.toString());
	}
	public boolean isVehicles() 
	{
		return 	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.VEHICLES.toString());
	}
	public boolean isAnimals() 
	{
		return 	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.ANIMAL.toString());
	}
	public boolean isStockshares() 
	{
		return 	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.STOCK_SHARES.toString());
	}
	public boolean isLicenses() 
	{
		return 	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.LICENSES.toString());
	}


    public boolean isLandProperties() 
    {
		return	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.LAND_PROPERTIES.toString());
	}
    public boolean isCash() 
    {
		return	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.CASH.toString());
	}
    public boolean isCheque() 
    {
		return	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.CHEQUE.toString());
	}
    public boolean isBankTransfer() 
    {
		return	endowment.getAssetTypeIdStr()!= null &&
            	endowment.getAssetTypeIdStr().equals(WebConstants.AssetType.BANK_TRANSFER.toString());
	}
	
	public EndowmentView getEndowment() {
		return endowment;
	}

	public void setEndowment(EndowmentView endowment) {
		this.endowment = endowment;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public HtmlSelectBooleanCheckbox getChkIsManagerAmaf() {
		return chkIsManagerAmaf;
	}

	public void setChkIsManagerAmaf(HtmlSelectBooleanCheckbox chkIsManagerAmaf) {
		this.chkIsManagerAmaf = chkIsManagerAmaf;
	}
	public String getPageMode() {
		return pageMode;
	}
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}
	public List<EndFileAsso> getListEndFileAsso() {
		return listEndFileAsso;
	}
	public void setListEndFileAsso(List<EndFileAsso> listEndFileAsso) {
		this.listEndFileAsso = listEndFileAsso;
	}
	public HtmlDataTable getDataTableFileAsso() {
		return dataTableFileAsso;
	}
	public void setDataTableFileAsso(HtmlDataTable dataTableFileAsso) {
		this.dataTableFileAsso = dataTableFileAsso;
	}
	public HtmlPanelGrid getGridTransportation() {
		return gridTransportation;
	}
	public void setGridTransportation(HtmlPanelGrid gridTransportation) {
		this.gridTransportation = gridTransportation;
	}
	public HtmlPanelGrid getGridVehicles() {
		return gridVehicles;
	}
	public void setGridVehicles(HtmlPanelGrid gridVehicles) {
		this.gridVehicles = gridVehicles;
	}
	public HtmlPanelGrid getGridAnimals() {
		return gridAnimals;
	}
	public void setGridAnimals(HtmlPanelGrid gridAnimals) {
		this.gridAnimals = gridAnimals;
	}
	public HtmlPanelGrid getGridLandProperties() {
		return gridLandProperties;
	}
	public void setGridLandProperties(HtmlPanelGrid gridLandProperties) {
		this.gridLandProperties = gridLandProperties;
	}
	public HtmlPanelGrid getGridLicenses() {
		return gridLicenses;
	}
	public void setGridLicenses(HtmlPanelGrid gridLicenses) {
		this.gridLicenses = gridLicenses;
	}
	public HtmlPanelGrid getGridStockshares() {
		return gridStockshares;
	}
	public void setGridStockshares(HtmlPanelGrid gridStockshares) {
		this.gridStockshares = gridStockshares;
	}
	public List<EndFileBen> getListEndFileBen() {
		return listEndFileBen;
	}
	public void setListEndFileBen(List<EndFileBen> listEndFileBen) {
		this.listEndFileBen = listEndFileBen;
	}
	public HtmlDataTable getDataTableFileBen() {
		return dataTableFileBen;
	}
	public void setDataTableFileBen(HtmlDataTable dataTableFileBen) {
		this.dataTableFileBen = dataTableFileBen;
	}
	public HtmlSelectBooleanCheckbox getChkOnAgreement() {
		return chkOnAgreement;
	}
	public void setChkOnAgreement(HtmlSelectBooleanCheckbox chkOnAgreement) {
		this.chkOnAgreement = chkOnAgreement;
	}
	public String getTotalBeneficiaryPercentage() {
		return totalBeneficiaryPercentage;
	}
	public void setTotalBeneficiaryPercentage(String totalBeneficiaryPercentage) {
		this.totalBeneficiaryPercentage = totalBeneficiaryPercentage;
	}
	public HtmlPanelGrid getGridCheque() {
		return gridCheque;
	}
	public void setGridCheque(HtmlPanelGrid gridCheque) {
		this.gridCheque = gridCheque;
	}
	public HtmlPanelGrid getGridBankTransfer() {
		return gridBankTransfer;
	}
	public void setGridBankTransfer(HtmlPanelGrid gridBankTransfer) {
		this.gridBankTransfer = gridBankTransfer;
	}
	public String getBankIdStr() {
		return bankIdStr;
	}
	public void setBankIdStr(String bankIdStr) {
		this.bankIdStr = bankIdStr;
	}
	public EndowmentCompensateVO getCompensateVO() {
		return compensateVO;
	}
	public void setCompensateVO(EndowmentCompensateVO compensateVO) {
		this.compensateVO = compensateVO;
	}
	public List<EndowmentCompensateVO> getCompensateDataList() {
		return compensateDataList;
	}
	public void setCompensateDataList(List<EndowmentCompensateVO> compensateDataList) {
		this.compensateDataList = compensateDataList;
	}
	public HtmlDataTable getDataTableEndowmentCompensate() {
		return dataTableEndowmentCompensate;
	}
	public void setDataTableEndowmentCompensate(
			HtmlDataTable dataTableEndowmentCompensate) {
		this.dataTableEndowmentCompensate = dataTableEndowmentCompensate;
	}
	public List<EndowmentCompensateVO> getManageCompensateDataList() {
		return manageCompensateDataList;
	}
	public void setManageCompensateDataList(
			List<EndowmentCompensateVO> manageCompensateDataList) {
		this.manageCompensateDataList = manageCompensateDataList;
	}
	public List<EndowmentExpenseView> getDataListExpenses() {
		return dataListExpenses;
	}
	public void setDataListExpenses(List<EndowmentExpenseView> dataListExpenses) {
		this.dataListExpenses = dataListExpenses;
	}
	public HtmlDataTable getDataTableExpenses() {
		return dataTableExpenses;
	}
	public void setDataTableExpenses(HtmlDataTable dataTableExpenses) {
		this.dataTableExpenses = dataTableExpenses;
	}
}
