package com.avanza.pims.web.util.validator;

import com.avanza.core.util.StringHelper;

public final class ThirdPartyPropUnitValidator {

	public static boolean isUnitNumberValidated(String unitNumber) throws Exception{
		if (StringHelper.isEmpty(unitNumber) || unitNumber.trim().length() <= 0)
			return false;
		return true;
	}
	
	public static boolean isFloorNumberValidated(String floorNumber) throws Exception{
		if (StringHelper.isEmpty(floorNumber) || floorNumber.trim().length() <= 0)
			return false;
		return true;
	}
}
