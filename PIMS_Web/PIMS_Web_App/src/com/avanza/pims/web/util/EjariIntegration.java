package com.avanza.pims.web.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import com.avanza.core.util.Logger;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.EjariResponse;
import com.avanza.pims.ws.vo.TerminateContractInEjariVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EjariIntegration 
{
	private static Logger logger = Logger.getLogger(EjariIntegration.class);
	static SystemParameters parameters = SystemParameters.getInstance();
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	static String ejariWSEndPoint= parameters.getParameter( "EjariIntegrationRegisterContract" );
	
	public static EjariResponse registerContract(Long contractId, String initiatedBy)throws Exception
	{
			String postXML = UtilityService.getLeaseContractEjariDetails(contractId.toString(),"-1");
			String endPoint  = ejariWSEndPoint+"/RegisterContractInEjari";
			logger.logInfo("EjariURL:%s",endPoint );
			String responseJSON = CommonUtil.POST(endPoint, postXML,"application/xml");
			logger.logInfo("Ejari|Response:%s",responseJSON );
			Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();    
			EjariResponse response= jsonObj.fromJson(responseJSON, EjariResponse.class);
			if(response.getContractNumber()!= null)
			{
				PropertyService.updateContractAndTenantAfterEjariRegister(contractId, initiatedBy, response);
			}
		
		return response;
		
	}
	
	public static EjariResponse renewContract(Long contractId, Contract oldContract, String initiatedBy)throws Exception
	{
			 
			String postXML = UtilityService.getLeaseContractEjariDetails(
																		 contractId.toString(),
																		 oldContract.getContractId().toString()
																		);
			String endPoint  = ejariWSEndPoint+"/RenewContractInEjari";
			String responseJSON = CommonUtil.POST(endPoint, postXML,"application/xml");
			
			Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();    
			EjariResponse response= jsonObj.fromJson(responseJSON, EjariResponse.class);
			PropertyService.assignOldContractEjariNumberToNewContract(contractId , initiatedBy);
			return response;
		
	}
	public static boolean hasRegistrationErrors(
												Contract contract, 
												List<String>errorMessages
											   )throws Exception
	{
		boolean hasErrors = false;
		if( contract.getEjariNumber() ==null || contract.getEjariNumber().trim().length()<=0 )
		{
			errorMessages.add(
								ResourceBundle.getBundle(
														 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
														 FacesContext.getCurrentInstance().getViewRoot().getLocale()
														).getString("commons.msg.errrorTerminateContractEjariRequired")
							 );
			
			hasErrors = true;
		}
		return hasErrors;
	}
	public static EjariResponse terminateContractInEjariResponse (Long contractId,String requestNumber, String initiatedBy)throws Exception
	{
		Contract contract =  EntityManager.getBroker().findById(Contract.class, contractId);
		Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
		TerminateContractInEjariVO  vo = new TerminateContractInEjariVO();
		vo.setContractNumber(contract.getEjariNumber());
		if(requestNumber != null)
		{
			vo.setTerminationReason("Due to request number "+requestNumber+" from PIMS");
		}
		else
		{
			vo.setTerminationReason("From PIMS");
		}
		if(contract.getSettlementDate() != null)
		{
			vo.setSettlementDate(dateFormat.format(contract.getSettlementDate()));
		}
		else
		{
			vo.setSettlementDate("-1");
		}
		String postJSON = jsonObj.toJson(vo);
		String endPoint  = ejariWSEndPoint+"/TerminateContractInEjari";
		String responseJSON = CommonUtil.POST(
												endPoint,
												postJSON,
												"application/json"
											 );
		    
		EjariResponse response= jsonObj.fromJson(responseJSON, EjariResponse.class);
		if(response.getMsg().equals("NoErrors"))
		{
			PropertyService.updateContractAfterTerminateInEjari(
																contract, 
																initiatedBy
															   );
		}
		return response;
		
	}
	public static String getEjariCertificateLink(String ejariTenancyContractNumber )throws Exception
	{
	
		String endPoint  = ejariWSEndPoint+"/PrintCertificateByContractNumber/"+ejariTenancyContractNumber;
		return endPoint;
		
	}
	public static String getEjariContractLink(String ejariTenancyContractNumber )throws Exception
	{
	
		String endPoint  = ejariWSEndPoint+"/PrintTenancyContract/"+ejariTenancyContractNumber;
		return endPoint;
		
	}
	public static String getEjariWebServiceEndPoint()
	{
		return ejariWSEndPoint;
	}
    
	public static boolean hasRegistrationErrors(
												Long contractId, 
												List<String>errorMessages
											   )throws Exception
	{
		logger.logInfo("hasRegistrationErrors:Start");
		Contract contract  =  EntityManager.getBroker().findById(Contract.class, contractId);
		Unit unit = contract.getContractUnits().iterator().next().getUnit();
		Person tenant = contract.getTenant();
		boolean hasErrors = false;
		if( unit.getEjariReferenceNumber()==null || unit.getEjariReferenceNumber().trim().length()<=0 )
		{
			errorMessages.add(
								ResourceBundle.getBundle(
															 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
															 FacesContext.getCurrentInstance().getViewRoot().getLocale()
														).getString("commons.msg.errrorARNRequired")
							);
			hasErrors = true;
		}
		//Person is Individual
		if( tenant.getIsCompany().compareTo( 0L )==0 )
		{
			hasErrors = hasIndividualCustomerErrors(errorMessages, tenant,hasErrors);
		}
		else
		{
			hasErrors = hasCompanyCustomerErrors(errorMessages, tenant,hasErrors);
		}
		logger.logInfo("hasRegistrationErrors:Finish");
		return hasErrors;
		
	}

	private static boolean hasIndividualCustomerErrors(
															List<String> errorMessages, 
															Person tenant, 
															boolean hasErrors
													  )throws Exception
	{
		if( tenant.getSocialSecNumber()==null || tenant.getSocialSecNumber().trim().length()<=0 )
		{
			errorMessages.add(
								ResourceBundle.getBundle(
															 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
															 FacesContext.getCurrentInstance().getViewRoot().getLocale()
														).getString("commons.msg.errrorEmiratesIdRequired")
							);
			hasErrors = true;
		}
		if( tenant.getNationalityId()==null  )
		{
			errorMessages.add(
								ResourceBundle.getBundle(
															 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
															 FacesContext.getCurrentInstance().getViewRoot().getLocale()
														).getString("person.Msg.NationalityRequired")
							);
			hasErrors = true;
		}
//			if( tenant.getPassportNumber()==null || tenant.getPassportNumber().trim().length()<=0 )
//			{
//				errorMessages.add(
//									ResourceBundle.getBundle(
//																 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
//																 FacesContext.getCurrentInstance().getViewRoot().getLocale()
//															).getString("commons.msg.errrorPassportRequired")
//								);
//				hasErrors = true;
//			}
		if( tenant.getCellNumber() ==null || tenant.getCellNumber().trim().length()<=0 )
		{
			errorMessages.add(
								ResourceBundle.getBundle(
															 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
															 FacesContext.getCurrentInstance().getViewRoot().getLocale()
														).getString("commons.msg.errrorMobileNoRequired")
							);
			hasErrors = true;
		}
		return hasErrors;
	}
	
	private static boolean hasCompanyCustomerErrors(
													List<String> errorMessages, 
													Person tenant, 
													boolean hasErrors
	  												)throws Exception
	{
			if( tenant.getLicenseNumber()==null || tenant.getLicenseNumber().trim().length()<=0 )
			{
				errorMessages.add(
										ResourceBundle.getBundle(
													 FacesContext.getCurrentInstance().getApplication().getMessageBundle(), 
													 FacesContext.getCurrentInstance().getViewRoot().getLocale()
												).getString("contractor.message.licenseNumberRequired")
								 );
				hasErrors = true;
			}
			return hasErrors ;
	}
}

