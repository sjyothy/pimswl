package com.avanza.pims.web.util.validator;

import com.avanza.core.util.StringHelper;

public final class MasrafValidator {

	public static boolean isMasrafNameValidated(String name) throws Exception{
		if (StringHelper.isEmpty(name) || name.trim().length() <= 0)
			return false;
		return true;
	}
	
	public static boolean isBankValidated(String bankId) throws Exception{
		if (StringHelper.isEmpty(bankId) &&  bankId.compareTo("-1") == 0)
			return false;
		return true;
	}
	
	public static boolean isBankAccValidated(String accNumber) throws Exception{
		if (StringHelper.isEmpty(accNumber) || accNumber.trim().length() <= 0)
			return false;
		return true;
	}
	
	public static boolean isGRPAccValidated(String accNumber) throws Exception{
		if (StringHelper.isEmpty(accNumber) || accNumber.trim().length() <= 0)
			return false;
		return true;
	}
	
	
	
}
