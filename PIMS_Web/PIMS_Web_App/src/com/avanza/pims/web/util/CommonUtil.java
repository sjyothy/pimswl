package com.avanza.pims.web.util;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.MEMSPaymentReceiptReportCriteria;
import com.avanza.pims.report.criteria.PaymentReceiptReportCriteria;
import com.avanza.pims.report.criteria.RequestDetailsReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.web.workflow.WorkFlowVO;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.SearchAssetsService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssignedPersonTypeView;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SearchAssetsCriteriaView;
import com.avanza.pims.ws.vo.StatementOfAccountCriteriaView;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.ui.util.ResourceUtil;

public class CommonUtil {
	private static Logger logger = Logger.getLogger(CommonUtil.class);
	static ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	/** Creates a new instance of SystemParameters */

	private static final String USER_NOTE_TYPE = "User";
	private static final String NOTE_TYPE = "NOTE_TYPE";

	public CommonUtil() {
	}

	@SuppressWarnings("unchecked")
	public static DomainDataView getIdFromType(List<DomainDataView> ddv,
			String type) {
		for (int i = 0; i < ddv.size(); i++) {
			DomainDataView domainDataView = (DomainDataView) ddv.get(i);
			if (domainDataView.getDataValue().equals(type))
				return domainDataView;

		}
		return null;

	}
	@SuppressWarnings("unchecked")
	public static DomainDataView getDomainDataFromId(List<DomainDataView> ddv,
			Long id) {
		for (int i = 0; i < ddv.size(); i++) {
			DomainDataView domainDataView = (DomainDataView) ddv.get(i);
			if (domainDataView.getDomainDataId().compareTo(id) == 0)
				return domainDataView;

		}
		return null;

	}
	@SuppressWarnings("unchecked")
	public static List<DomainDataView> getDomainDataListForDomainType(
			String domainType) {
		List<DomainDataView> ddvList = new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		// Iterates for all the domain Types
		while (itr.hasNext()) {
			DomainTypeView dtv = itr.next();

			if (dtv.getTypeName().compareTo(domainType) == 0) {
				Set<DomainDataView> dd = dtv.getDomainDatas();
				// Iterates over all the domain datas
				for (DomainDataView ddv : dd) {
					ddvList.add(ddv);
				}
				break;
			}
		}
		itr = null;

		return ddvList;
	}
	@SuppressWarnings("unchecked")
	public static String addJavaScript(String page, String javascript) {
		String methodName = "addJavaScript";
		logger.logInfo(methodName + "|" + "Start..");
		logger.logInfo(methodName + "|" + "page.." + page);
		logger.logInfo(methodName + "|" + "javascript.." + javascript);
		final String viewId = page;

		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javascript);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	@SuppressWarnings("unchecked")
	public FeeConfigurationView getFeeForType(String feeType)
			throws PimsBusinessException {
		String method = "getFeeForType";
		logger.logInfo(method + "|" + "Start..");
		logger.logInfo(method + "|"
				+ "Searchig Fee Configuration for fee type.." + feeType);
		Long feeTypeId = new Long(0);
		FeeConfigurationView feeConfigurationView = null;
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<FeeConfigurationView> feeConfigurationList = new ArrayList<FeeConfigurationView>();

		// DomainDataView ddv= psa.getDomainDataByValue(feeType);

		List<DomainDataView> ddvList = this
				.getDomainDataListForDomainType(WebConstants.FEE_TYPE);
		DomainDataView ddv = this.getIdFromType(ddvList, feeType);

		feeTypeId = ddv.getDomainDataId();
		logger.logInfo(method + "|" + "Getting Fee with type id:" + feeTypeId);
		feeConfigurationList = psa.getFeeConfigByFeeType(feeTypeId);
		logger.logInfo(method + "|" + "Checking for size of list...");
		if (feeConfigurationList.size() > 0) {
			logger.logInfo(method + "|" + "List contains "
					+ feeConfigurationList.size() + " items");
			feeConfigurationView = (FeeConfigurationView) feeConfigurationList
					.get(0);

		}
		logger.logInfo(method + "|" + "Finish..");
		return feeConfigurationView;

	}
	@SuppressWarnings("unchecked")
	public List<PaymentConfigurationView> getFeeForCondition(
			String conditionKey,
			List<PaymentConfigurationView> paymentConfigurationList)
			throws PimsBusinessException {
		String method = "getFeeForType";
		logger.logInfo(method + "|" + "Start..");
		logger.logInfo(method + "|"
				+ "Searchig Payment Configuration for conditionKey..:"
				+ conditionKey);
		Long conditionId = new Long(0);
		PaymentConfigurationView paymentConfigurationView = null;
		List<PaymentConfigurationView> pcvList = new ArrayList<PaymentConfigurationView>(
				0);
		List<DomainDataView> ddvList = this
				.getDomainDataListForDomainType(WebConstants.CONDITION);
		DomainDataView ddv = this.getIdFromType(ddvList, conditionKey);
		conditionId = ddv.getDomainDataId();
		logger.logInfo(method + "|" + "Getting Fee  with type id :"
				+ conditionId);
		for (PaymentConfigurationView paymentConfigurationView2 : paymentConfigurationList) {
			if (paymentConfigurationView2.getPaymentTypeId().compareTo(
					WebConstants.PAYMENT_TYPE_FEES_ID) == 0
					&& paymentConfigurationView2.getConditionId() != null
					&& paymentConfigurationView2.getConditionId().compareTo(
							conditionId) == 0) {
				pcvList.add(paymentConfigurationView2);

			}
		}
		logger.logInfo(method + "|" + "Finish..");
		return pcvList;

	}
	@SuppressWarnings("unchecked")
	public static String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();

	}
	@SuppressWarnings("unchecked")
	public static String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	@SuppressWarnings("unchecked")
	public List<PaymentConfigurationView> getPaymentConfigByProcedureType(
			String procedureType) throws PimsBusinessException {
		String method = "getFeeConfigByProceureType";
		logger.logInfo(method + "|" + "Start..");
		logger.logInfo(method + "|"
				+ "Searchig Fee Configuration for procedure type.."
				+ procedureType);
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<PaymentConfigurationView> feeConfigurationList = new ArrayList<PaymentConfigurationView>();

		feeConfigurationList = psa
				.getPaymentConfigurationForProcedureType(procedureType);
		logger.logInfo(method + "|" + feeConfigurationList.size()
				+ " rows of fee retrieved for procedure type..."
				+ procedureType);

		logger.logInfo(method + "|" + "Finish..");
		return feeConfigurationList;
	}
	@SuppressWarnings("unchecked")
	public static String getStringFromDate(Date dateVal) {
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try {
			if (dateVal != null)
				dateStr = formatter.format(dateVal);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	@SuppressWarnings("unchecked")
	public static String getLoggedInUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	/**
	 * Returns all the request keys associated with desired procedure Type
	 * 
	 * @return List<RequestKey>
	 * @throws PimsBusinessException
	 * @author Danish Farooq
	 */
	@SuppressWarnings("unchecked")
	public List<RequestKeyView> getRequestKeysForProcedureType(
			String procedureType) throws PimsBusinessException {
		String method = "getRequestKeysForProcedureType";
		logger.logInfo(method + "|" + "Start..");
		logger.logInfo(method + "|"
				+ "Searchig all Request keys for procedure type.."
				+ procedureType);
		List<RequestKeyView> rkList = new ArrayList<RequestKeyView>();
		RequestServiceAgent rsa = new RequestServiceAgent();
		rkList = rsa.getRequestKeys(procedureType);
		logger.logInfo(method + "|" + "Got.." + rkList.size()
				+ " request keys for procedure type" + procedureType);

		logger.logInfo(method + "|" + "Finish..");
		return rkList;

	}

	/**
	 * Returns the request key view associated with desired procedure Type and
	 * keyName
	 * 
	 * @return List<RequestKey>
	 * @throws PimsBusinessException
	 * @author Danish Faroosq
	 */
	@SuppressWarnings("unchecked")
	public RequestKeyView getRequestKeysForProcedureKeyName(
			String procedureType, String keyName) throws PimsBusinessException {
		List<RequestKeyView> rkViewList = getRequestKeysForProcedureType(procedureType);
		for (RequestKeyView requestKeyView : rkViewList) {
			if (requestKeyView.getKeyName().equals(keyName)) {
				return requestKeyView;
			}
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public String getNumberFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
	}
	@SuppressWarnings("unchecked")
	public static String getDoubleFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
	}
	@SuppressWarnings("unchecked")
	public static Date getDateFromString(String dateStr) {
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		Date dateVal = null;
		try {
			if (dateStr.length() == pattern.length())
				dateVal = formatter.parse(dateStr);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateVal;
	}
	@SuppressWarnings("unchecked")
	public Number getNumberValue(String doubleStr, String pattern) {
		Number numberVal = null;
		try {
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return numberVal;
	}
	@SuppressWarnings("unchecked")
	public static Double getDoubleValue(String doubleStr) {
		Number numberVal = new Double(0D);

		try {
			String pattern = getDoubleFormat();
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return numberVal.doubleValue();
	}
	@SuppressWarnings("unchecked")
	public static String getBundleMessage(String key) {
		String message = "";
		try {
			if (StringHelper.isNotEmpty(key))
				message = ResourceUtil.getInstance().getProperty(key);
			else
				message = "";

		} catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}
	@SuppressWarnings("unchecked")
	public static String getParamBundleMessage(String key, Object... params) {
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key, params);
		} catch (Exception exception) {
			logger.LogException(
					"getParamBundleMessage(String,Object...) crashed ",
					exception);
		}
		return message;
	}
	
	/**
	 * Returns the formatted string for the given list of params
	 * @param key - message resource key
	 * @param localeString - locale string like en or ar
	 * @param params - varaible list of arguments 
	 * @return the formatted string for the given
	 */
	@SuppressWarnings("unchecked")
	public static String getParamBundleMessage(String key, String localeString,Object... params) {
		String message = null;
		try {
			
			Locale locale = new Locale(localeString);

			ResourceBundle messages = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),locale);
			
			MessageFormat formatter = new MessageFormat("");
			formatter.setLocale(locale);

			formatter.applyPattern(messages.getString(key));
			message = formatter.format(params);
			
		} catch (Exception exception) {
			logger.LogException(
					"getParamBundleMessage(String,Object...) crashed ",
					exception);
		}
		return message;
	}
	@SuppressWarnings("unchecked")
	public static String getInvalidMessage(String field) {

		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try {
			message = message
					.append(getBundleMessage(field))
					.append(" ")
					.append(
							getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
			logger
					.logInfo("getInvalidMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger
					.LogException("getInvalidMessage(String) crashed ",
							exception);
		}
		return message.toString();
	}

	public static String getErrorMessages(List<String> errorMessages) {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		}/*
			 * else { messageList = "<UL>"; for (String message :
			 * errorMessages) { messageList = messageList + "<LI>" + message; }
			 * messageList = messageList + "</UL>"; }
			 */
		else {
			messageList = "";
			for (String message : errorMessages) {
				messageList = messageList + "<br>* " + message;
			}
			// messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);
	}

	public static String getFormattedDoubleString(Double val, String pattern) {
		String doubleStr = "0.00";
		try {
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return doubleStr;
	}

	public static Boolean getIsEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}

	public static Boolean getIsArabicLocale() {
		return !getIsEnglishLocale();
	}

	public static Integer getMonthsBetween(Date fromDate, Date toDate) {
		Integer months = 0;

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(fromDate);
		long ldate1 = fromDate.getTime();

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(toDate);
		long ldate2 = toDate.getTime();

		int hr1 = (int) (ldate1 / 3600000); // 60*60*1000
		int hr2 = (int) (ldate2 / 3600000);

		int days1 = (int) hr1 / 24;
		int days2 = (int) hr2 / 24;

		int dateDiff = days2 - days1;
		int weekOffset = (cal2.get(Calendar.DAY_OF_WEEK) - cal1
				.get(Calendar.DAY_OF_WEEK)) < 0 ? 1 : 0;
		int weekDiff = dateDiff / 7 + weekOffset;
		int yearDiff = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
		months = yearDiff * 12 + cal2.get(Calendar.MONTH)
				- cal1.get(Calendar.MONTH);

		return months;
	}

	public static Integer getDaysBetween(Date fromDate, Date toDate) {
		Date startDate = fromDate;
		Date endDate = toDate;
		Calendar calFirst = Calendar.getInstance();
		Calendar calSecond = Calendar.getInstance();

		calFirst.setTime(startDate);
		calSecond.setTime(endDate);

		Integer days = CommonUtil.getDaysBetween(calFirst, calSecond);

		return days;
	}

	public static int getDaysBetween(Calendar d1, Calendar d2) {
		if (d1.after(d2)) { // swap dates so that d1 is start and d2 is end
			Calendar swap = d1;
			d1 = d2;
			d2 = swap;
		}
		int days = d2.get(Calendar.DAY_OF_YEAR) - d1.get(Calendar.DAY_OF_YEAR);
		int y2 = d2.get(Calendar.YEAR);
		if (d1.get(Calendar.YEAR) != y2) {
			d1 = (Calendar) d1.clone();
			do {
				days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);
				d1.add(Calendar.YEAR, 1);
			} while (d1.get(Calendar.YEAR) != y2);
		}
		return days;
	} // getDaysBetween()

	public static String getFieldRequiredMessage(String field) {
		StringBuffer message = new StringBuffer("");
		logger.logInfo("getFieldRequiredMessage(String) started...");
		try {
			message = message
					.append(getBundleMessage(field))
					.append(" ")
					.append(
							getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.IS_REQUIRED));
			logger
					.logInfo("getFieldRequiredMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("getFieldRequiredMessage(String) crashed ",
					exception);
		}
		return message.toString();
	}
@	SuppressWarnings( "unchecked" )
	public static Boolean updateDocuments() throws PimsBusinessException 
	{
		Boolean success = false;
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<Long> docIdList = (List<Long>) viewMap.get("documentIdList");
		String associatedObjectId = (String) viewMap.get(WebConstants.Attachment.ASSOCIATED_OBJECT_ID);
		if (
				docIdList != null && !docIdList.isEmpty() && StringHelper.isNotEmpty( associatedObjectId )
		   ) 
		{
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			success = utilityServiceAgent.updateDocuments(docIdList,associatedObjectId);
			if (success) 
			{
				docIdList = new ArrayList<Long>();
				viewMap.put("documentIdList", docIdList);
			}
		} 
		else
		{
			success = true;
		}
		return success;
	}



	public static Boolean saveSystemComments(String notesOwner, String sysNote,
			Long requestId,String... placeHolderVals) throws Exception 
			
	{
		Boolean success = false;
		NotesController.saveSystemNotesForRequest(notesOwner, sysNote,requestId,placeHolderVals);
		success = true;
		return success;
	}

	public AssignedPersonTypeView getAssignedPersonTypeFromSet(Set<AssignedPersonTypeView> assignedPersonTypeViewSet,String personType)throws Exception 
	{
		DomainDataView ddv = new DomainDataView();
		ddv = getIdFromType(this.getDomainDataListForDomainType(WebConstants.PERSON_TYPE),personType);
		Iterator iter = assignedPersonTypeViewSet.iterator();

		if (ddv != null)
			while (iter.hasNext()) {
				AssignedPersonTypeView aptView = (AssignedPersonTypeView) iter
						.next();
				if (aptView.getPersonTypeId().compareTo(ddv.getDomainDataId()) == new Long(0)) 
				{
					return aptView;
				}
			}
		return null;
	}

	public RequestView getIncompleteRequestForRequestType(Long contractId,
			Long requestType) throws PimsBusinessException {

		PropertyServiceAgent psa = new PropertyServiceAgent();
		ContractView contractView = new ContractView();
		contractView.setContractId(contractId);
		RequestView requestView = new RequestView();
		requestView.setContractView(contractView);// Id(contractView.getContractId());
		RequestTypeView requestTypeView = new RequestTypeView();
		requestView.setRequestTypeId(requestType);

		List<RequestView> requestViewList = psa.getAllRequests(requestView,null, null, null);
		
		// if there is some request for change tenant name against this
		// contract and its status is not completed then display that request
		// otherwise create new screen
		requestView = null;
		for (RequestView requestView2 : requestViewList) {

			if (
					requestView2.getStatusId().compareTo(WebConstants.REQUEST_STATUS_COMPLETE_ID) != 0 && 
					requestView2.getStatusId().compareTo(WebConstants.REQUEST_STATUS_REJECTED_ID) != 0&& 
					requestView2.getStatusId().compareTo(WebConstants.REQUEST_STATUS_CANCELED_ID) != 0
				) 
			{
				requestView = requestView2;
				break;
			}
		}

		return requestView;
	}

	public static TimeZone getTimeZone() {
		return TimeZone.getDefault();

	}

	public static List<ContactInfo> getEmailContactInfos(PersonView personView) {
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while (iter.hasNext()) {
			ContactInfoView ciView = (ContactInfoView) iter.next();
			// IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if (ciView.getEmail() != null
					&& ciView.getEmail().length() > 0
					&& !previousEmailAddressMap.containsKey(ciView.getEmail()
							.toLowerCase())) {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),
						ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView
						.getCellNumber(), null, ciView.getEmail()));
				// emailList.add(ciView);
			}
		}
		return emailList;
	}

	/**
	 * saves the remarks given the parameters
	 * 
	 * @param entityId
	 * @param remarks
	 * @param notesOwner
	 */
	public static void saveRemarksAsComments(Long entityId, String remarks,
			String notesOwner) {
		saveRemarksAsComments(entityId, remarks, notesOwner, new Date());
	}

	public static void saveRemarksAsComments(Long entityId, String remarks,
			String notesOwner, Date remarksDate) {
		NotesServiceAgent notesServiceAgent = new NotesServiceAgent();
		NotesVO remarksVO = createRemarksNote(notesOwner, entityId, remarks,
				remarksDate);
		notesServiceAgent.addNote(remarksVO);
	}

	/**
	 * creates a NotesVO given the parameters
	 * 
	 * @param notesOwner
	 * @param entityId
	 * @param remarks
	 * @return NotesVO
	 */
	public static NotesVO createRemarksNote(String notesOwner, Long entityId,
			String remarks) {
		return createRemarksNote(notesOwner, entityId, remarks, new Date());
	}

	public static NotesVO createRemarksNote(String notesOwner, Long entityId,
			String remarks, Date remarksDate) {
		NotesVO notesVO = new NotesVO();

		try {
			notesVO.setNoteowner(notesOwner);
			DomainDataView domainDataView = new UtilityServiceAgent()
					.getDomainDataByValue(NOTE_TYPE, USER_NOTE_TYPE);
			if (domainDataView != null)
				notesVO.setNoteTypeId(domainDataView.getDomainDataId());
			notesVO.setEntityId(entityId);
			notesVO.setNote(remarks);
			notesVO.setDeleted(false);
			notesVO.setRecordStatus(1);
			notesVO.setCreatedBy(getLoggedInUser());
			notesVO.setUpdatedBy(getLoggedInUser());
			notesVO.setCreatedOn(remarksDate);
			notesVO.setUpdatedOn(remarksDate);
		} catch (PimsBusinessException e) {
			notesVO = null;
		}
		return notesVO;
	}

	public static Boolean saveComments(Long referenceId, String notesOwner)
			throws PimsBusinessException {
		Boolean success = false;
		try {
			NotesController.saveNotes(notesOwner, referenceId);
			success = true;
		} catch (PimsBusinessException exception) {
			logger.LogException("saveComments| crashed ", exception);
			throw exception;
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	public static Boolean saveAttachments(Long referenceId) throws PimsBusinessException 
	{
		Boolean success = false;
		try 
		{
			if (referenceId != null) 
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID,referenceId.toString());
				success = CommonUtil.updateDocuments();
			}
		} 
		catch (PimsBusinessException throwable) 
		{
			success = false;
			logger.LogException("saveAtttachments crashed ", throwable);
		}

		return success;
	}
	@SuppressWarnings("unchecked")
	public static void loadAttachmentsAndComments(String associatedObjectId) {
		if (StringHelper.isNotEmpty(associatedObjectId)) {
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID,associatedObjectId);
			viewMap.put("entityId", associatedObjectId);
		}
	}
	@SuppressWarnings("unchecked")
	public static void loadAttachmentsAndComments(String procedureTypeKey,
			String externalId, String noteOwner, String nolType) {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, procedureTypeKey);
		if (nolType != null)
			viewMap.put(WebConstants.Attachment.PROCEDURE_SUB_KEY, nolType);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;

		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", noteOwner);
	}
	@SuppressWarnings("unchecked")
	public static void loadAttachmentsAndComments(String procedureTypeKey,String externalId, String noteOwner) {
		loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner,null);
	}
	
	@SuppressWarnings("unchecked")
	public static DocumentView upload(UploadedFile file, String documentTitle,String comments) 
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Long documentId = null;
		DocumentView documentView = null;
		try 
		{

			if (file != null) 
			{
				documentView = new DocumentView();

				String updatedBy = CommonUtil.getLoggedInUser();
				String createdBy = CommonUtil.getLoggedInUser();
				Date updatedOn = new Date();
				Date createdOn = new Date();

				String repositoryId = (String) viewMap.get(WebConstants.Attachment.REPOSITORY_ID);
				String externalId = (String) viewMap.get(WebConstants.Attachment.EXTERNAL_ID);
				String associatedObjectId = (String) viewMap.get(WebConstants.Attachment.ASSOCIATED_OBJECT_ID);

				logger
						.logInfo(
								"repositoryId : %s | externalId : %s | associatedObjectId : %s ",
								repositoryId, externalId, associatedObjectId);

				String documentFileName = getFileNameForPath(file.getName());
				String documentPath = file.getName();
				String contentType = file.getContentType();
				Long sizeInBytes = file.getSize();
				documentId = documentView.getDocumentId();

				logger.logInfo(
								"documentFileName : %s | contentType : %s | sizeInBytes : %s ",
								documentFileName, contentType, sizeInBytes
							  );

				documentView.setUpdatedBy(updatedBy);
				documentView.setUpdatedOn(updatedOn);
				documentView.setCreatedOn(createdOn);
				documentView.setCreatedBy(createdBy);
				documentView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				documentView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

				documentView.setDocumentId(documentId);
				documentView.setAssociatedObjectId(associatedObjectId);
				documentView.setDocumentFileName(documentFileName);
				documentView.setDocumentTitle(documentTitle);
				documentView.setDocumentPath(documentPath);
				documentView.setContentType(contentType);
				documentView.setComments(comments);
				documentView.setDocTypeId(WebConstants.Attachment.DOCUMENT_TYPE_REPORT_ID);
				documentView.setExternalId(externalId);
				documentView.setRepositoryId(repositoryId);
				documentView.setSizeInBytes(sizeInBytes);
				documentView.setIsProcDoc(false);

				UtilityServiceAgent usa = new UtilityServiceAgent();
				if (file.getInputStream().available() != 0) {
					// InputStream inputStream = file.getInputStream();
					// BufferedInputStream bis = new
					// BufferedInputStream(inputStream);

					byte[] fileByteArray = file.getBytes();
					byte[] encodedFileByteArray = Base64.encodeBase64(fileByteArray);
					String base64EncodedString = new String(encodedFileByteArray);
					documentView = usa.addAttachment(documentView,base64EncodedString);
				}

				documentId = documentView.getDocumentId();
			} else
				logger.logInfo("no file selected for upload!!!");

		} catch (Exception ex) {
			logger.LogException("upload crashed...", ex);
		}
		return documentView;
	}

	private static String getFileNameForPath(String path) {

		if (path == null || path.equalsIgnoreCase("")) {
			return path;
		}
		return path.substring(path.lastIndexOf("\\") + 1, path.length());
	}

	@SuppressWarnings("unchecked")
	public static Map getArgMap() {
		Map argMap = new HashMap();
		argMap.put("userId", getLoggedInUser());
		argMap.put("dateFormat", getDateFormat());
		argMap.put("isEnglishLocale", getIsEnglishLocale());
		return argMap;
	}

	public static boolean isAllPaymentsCollected(
			ArrayList<PaymentScheduleView> psvList) {
		boolean isAllPaymentsCollected = true;
		DomainDataView ddvPending = getIdFromType(
				getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
				WebConstants.PAYMENT_SCHEDULE_PENDING);
		for (PaymentScheduleView paymentScheduleView : psvList) {

			if (paymentScheduleView.getStatusId().compareTo(
					ddvPending.getDomainDataId()) == 0) {
				return false;
			}

		}

		return isAllPaymentsCollected;
	}

	public static void printPaymentReceipt(String contractId,
			String paymentScheduleId, String paymentReceiptId,
			FacesContext facesContext, String procedureName) {
		PaymentReceiptReportCriteria reportCriteria = new PaymentReceiptReportCriteria(
				ReportConstant.Report.PAYMENT_RECEIPT_REPORT_ENAR,
				ReportConstant.Processor.PAYMENT_RECEIPT_REPORT, CommonUtil
						.getLoggedInUser());
		HttpServletRequest request = (HttpServletRequest) facesContext
				.getExternalContext().getRequest();
		reportCriteria.setReportProcedureNameAr(procedureName);
		reportCriteria.setReportProcedureNameEn(procedureName);
		reportCriteria.setContractId(contractId);
		reportCriteria.setPaymentScheduleId(paymentScheduleId);
		reportCriteria.setReceiptNumber(paymentReceiptId);
		request.getSession().setAttribute(
				ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup();
	}

	private static void openPopup() {
		logger.logInfo("openPopup() started...");
		try {

			String javaScriptText = "var screen_width = screen.width;var screen_height = screen.height;var popup_width = screen_width-250;"
					+ "var popup_height = screen_height-500;var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;"
					+ "window.open('"
					+ ReportConstant.LOAD_REPORT_JSP
					+ "','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');";

			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	public static Boolean updateChequeDocuments(String paymentReceiptId)
			throws PimsBusinessException {
		Boolean success = false;
		if (FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().containsKey(
						WebConstants.Attachment.ATTACHED_CHEQUE_IDS)
				&& FacesContext.getCurrentInstance().getExternalContext()
						.getSessionMap().get(
								WebConstants.Attachment.ATTACHED_CHEQUE_IDS) != null) {
			List<Long> docIdList = (List<Long>) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS);
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().remove(
							WebConstants.Attachment.ATTACHED_CHEQUE_IDS);
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			String associatedObjectId = paymentReceiptId;
			if (docIdList != null && !docIdList.isEmpty()
					&& StringHelper.isNotEmpty(associatedObjectId)) {
				UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
				success = utilityServiceAgent.updateDocuments(docIdList,
						associatedObjectId);
				if (success) {
					docIdList = new ArrayList<Long>();
					viewMap.put(WebConstants.Attachment.ATTACHED_CHEQUE_IDS,
							docIdList);
				}
			} else
				success = true;
		}
		return success;
	}

	public static String getSuccessMessages(List<String> successMessages) {
		String messageList;
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "";
			for (String message : successMessages) {
				messageList = messageList + "<br> " + message;
			}
		}
		return (messageList);
	}

	public static String UTF2Dec(String sValue) {
		StringBuffer sb = new StringBuffer();

		try {
			char[] ac = sValue.toCharArray();
			int iValue;
			String sTemp = null;
			String result = null;
			String str = null;

			for (int ndx = 0; ndx < ac.length; ndx++) {
				iValue = ac[ndx];

				if (iValue < 0x10) {
					sTemp = "000";
				} else if (iValue < 0x100) {
					sTemp = "00";
				} else if (iValue < 0x1000) {
					sTemp = "0";
				}

				result = sTemp + Integer.toHexString(iValue);
				str = new String("&#"
						+ new Integer(Integer.parseInt(result, 16)) + ";");
				sb.append(str);
			}// for

		}// try

		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}// catch

		finally {
			return sb.toString();
		}// finally

	}// UTF2Dec

	/**
	 * This method is used to convert a string provided from the web to UTF8
	 * format
	 * 
	 * @param sValue
	 *            which is the string that needs to be converted
	 * 
	 * @return a converted string
	 */
	public static String convert2UTF(String sValue) {
		String convertedString = "";
		StringBuffer sb = new StringBuffer();

		try {

			if (sValue != null) {
				convertedString = new String(sValue.getBytes("ISO-8859-1"),
						"UTF-8");
			}// if

		}// try

		catch (UnsupportedEncodingException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}// catch

		finally {
			return convertedString;
		}// finally

	}

	/**
	 * This method checks if for individual nationality is selected and for
	 * company License Issue Place is selected
	 * 
	 * @param PersonView
	 * 
	 * @return true: if person contains country ,false if person does not
	 *         contain country for migrated data the person does not have have
	 *         country with him
	 */
	public static boolean isPersonContainCountry(PersonView person) {
		String methodName = "isPersonContainCountry|";
		logger.logInfo(methodName + "Start| personId %s", person.getPersonId());
		if (person.getIsCompany().longValue() == 1l) {
			if (person.getLicenseIssuePlaceId() == null
					|| person.getLicenseIssuePlaceId().longValue() < 0)
				return false;
		} else if (person.getNationalityId() == null
				|| person.getNationalityId().longValue() < 0)
			return false;
		return true;
	}

	public static int getDaysDifference(Date fromDate, Date toDate)throws Exception 
	{
//		Calendar calendarFrom = Calendar.getInstance();
//		Calendar calendarTo = Calendar.getInstance();
//		calendarFrom.setTime(fromDate);
//		calendarTo.setTime(toDate);
//		Integer days = new Integer(0);
//		int To_day = calendarTo.get(Calendar.DAY_OF_MONTH);
//		int From_day = calendarFrom.get(Calendar.DAY_OF_MONTH);
//		int To_month = calendarTo.get(Calendar.MONTH);
//		int From_month = calendarFrom.get(Calendar.MONTH);
//		int To_year = calendarTo.get(Calendar.YEAR);
//		boolean isToYearLeap = isLeapYear( calendarTo.get(Calendar.YEAR) );
//		boolean isToDayLastDayOfFeb = calendarTo.get(Calendar.MONTH)== 1 && ( (isToYearLeap && To_day > 28 ) || (!isToYearLeap && To_day == 28 ) );
//		if(  isToDayLastDayOfFeb )
//		{
//			To_day = To_day+ (To_day==28? 2: 1 );
//		}
//	
//		int From_year = calendarFrom.get(Calendar.YEAR);
//		int toAdd = 0;
//		if (To_day != 31)
//			toAdd = 1;
//		if (To_day < From_day) {
//			To_day += 30;
//			To_month -= 1;
//		}
//
//		while (To_year != From_year) {
//			To_month += 12;
//			To_year -= 1;
//		}
//
//		days = To_day - From_day + toAdd;
//		// if days are greater than 29 then add 1 to To_Month and take mod with
//		// 30 to get remaining days
//		if (days > 29) {
//			// This formula will tell exactly how many months will be added for
//			// e.g. for 31 days 1 month, for 61 days 2 months,
//			// for 91 days 3 months etc.
//			To_month += (days - (days % 30)) / 30;
//			days = days % 30;
//		}
//
//		days = days + Math.abs((To_month - From_month) * 30);
//
//		return days;
		return UtilityService.getDaysDifference( fromDate, toDate );
	}
	 
	    public static boolean isLeapYear(int year ) 
	    { 
	        
	        boolean isLeapYear;

	        // divisible by 4
	        isLeapYear = (year % 4 == 0);
	        // divisible by 4 and not 100
	        isLeapYear = isLeapYear && (year % 100 != 0);
	        // divisible by 4 and not 100 unless divisible by 400
	        isLeapYear = isLeapYear || (year % 400 == 0);

	        return isLeapYear;
	    }
	
	public static boolean isOldPendingPaymentsExists(Long contractId,
			Double rent) throws Exception {
		boolean exists = false;
		Double totalCollectedRent = 0.0;
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<PaymentScheduleView> oldPayments;
		oldPayments = psa.getContractPaymentSchedule(contractId, null);
		DomainDataView ddRealized = psa.getDomainDataByValue(
				WebConstants.PAYMENT_SCHEDULE_STATUS, WebConstants.REALIZED);
		DomainDataView ddPmntModeCash = psa.getDomainDataByValue(
				WebConstants.PAYMENT_SCHEDULE_MODE,
				WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		DomainDataView ddPmntModeCheque = psa.getDomainDataByValue(
				WebConstants.PAYMENT_SCHEDULE_MODE,
				WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
		DomainDataView ddPmntModeBT = psa.getDomainDataByValue(
				WebConstants.PAYMENT_SCHEDULE_MODE,
				WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);

		for (PaymentScheduleView pschView : oldPayments) 
		{
			if (pschView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID) == 0) 
			{
				if ( 
						pschView.getPaymentModeId().compareTo(ddPmntModeCheque.getDomainDataId()) == 0 && 
					    pschView.getStatusId().compareTo(ddRealized.getDomainDataId()) == 0 
				   )
				{	
					totalCollectedRent += pschView.getAmount();
				}
				else if (
						 pschView.getPaymentModeId().compareTo(ddPmntModeCash.getDomainDataId()) == 0 || 
						 pschView.getPaymentModeId().compareTo(ddPmntModeBT.getDomainDataId()) == 0
						)
				{
					totalCollectedRent += pschView.getAmount();
				}
			}

		}
		if (totalCollectedRent.doubleValue() >= rent.doubleValue())
			exists = false;
		else
			exists = true;

		return exists;
	}

	@SuppressWarnings("unchecked")
	public static Integer[] calculateMonthsDaysBetween(Date dateFrom,Date dateTo) throws Exception 
	{
		return UtilityService.calculateMonthsDaysBetween(dateFrom, dateTo);
	}

	public List<SelectItem> getResearcherGroup() throws PimsBusinessException {

		String groupNamesCSV = new UtilityServiceAgent()
				.getValueFromSystemConfigration(WebConstants.SYS_CONFIG_RESEARCHER_GROUP);
		if (groupNamesCSV != null) {
			String[] groupNames = groupNamesCSV.split(",");
			List groupList = Arrays.asList(groupNames);
			return getUsersByGroupId(groupList);
		} else
			return null;

	}

	public static List<SelectItem> getUsersByGroupId(List groupList) {

		List<SelectItem> groupUserList = new ArrayList<SelectItem>();

		Search query = new Search();
		query.clear();
		query.addFrom(UserGroup.class);
		Criterion groupsIdCriterion = SimpleCriterion.in("userGroupId",
				(List<String>) groupList);
		query.addCriterion(groupsIdCriterion);
		List<UserGroup> allUserGroups = EntityManager.getBroker().find(query);
		if (allUserGroups != null) 
		{
			for (UserGroup userGroup : allUserGroups) 
			{
				// UserGroup grp = allUserGroups.get(0);
				Set<User> userSet = userGroup.getUsers();
				for (User user : userSet) 
				{

					if (user.getLoginId() != null) 
					{
						groupUserList.add(new SelectItem(user.getLoginId(),user.getSecondaryFullName()));
					}
				}

			}
		} else
			return null;

		return groupUserList;
	}

	public static List<SelectItem> getAssetTypesList()
			throws PimsBusinessException {

		List<SelectItem> items = new ArrayList<SelectItem>();
		List<SearchAssetsCriteriaView> assetTypesList = new ArrayList<SearchAssetsCriteriaView>();
		assetTypesList = new SearchAssetsService().getAssetTypesList();
		if (assetTypesList != null && assetTypesList.size() > 0) {
			for (SearchAssetsCriteriaView assets : assetTypesList) {
				SelectItem item;
				item = new SelectItem(assets.getAssetTypeid().toString(),
						getIsEnglishLocale()? assets.getAssetNameEn():assets.getAssetNameAr());
				items.add(item);
			}
		}
		return items;		
	}
	
	/**
	 * 	The function returns the list of all the user groups in the system
	 * @return List of all the user groups in the system
	 * @author Farhan Muhammad Amin
	 */
	public static List<UserGroup> getAllUserGroups() throws Exception {
		Search query = new Search();
		query.clear();
		
		query.addFrom(UserGroupDbImpl.class);				
		return SecurityManager.findGroup(query);		
	}
	
	public static List<SelectItem> getCreatedByList() throws Exception
	{
		
		List<UserDbImpl> secUserPojo = new InheritanceFileService().getCreatedByList();
		 
			List<SelectItem> createdByList = new ArrayList<SelectItem>(0);
			for (UserDbImpl secUser : secUserPojo) {
				SelectItem item = new SelectItem();

				if (secUser != null && secUser.getLoginId() != null) {
					item.setLabel(secUser.getFullName());
					item.setValue(secUser.getLoginId());

				}
				createdByList.add(item);
			}
        return createdByList;		
	}
	
	public static DomainDataView getDomainDataView(final String domainKey, final Long domainSubKeyId) {
		List<DomainDataView> ddv = CommonUtil.getDomainDataListForDomainType(domainKey);
		return CommonUtil.getDomainDataFromId(ddv, domainSubKeyId);
	}
		
	public static Long getDomainDataId(String domainKey, String domainSubKey) {
		List<DomainDataView> ddv = CommonUtil.getDomainDataListForDomainType(domainKey);
		return CommonUtil.getIdFromType(ddv, domainSubKey).getDomainDataId(); 
	}
	
	public static HashMap<Long, String> getCaringTypeMap()throws Exception
	{	
		HashMap<Long, String> caringMap = new  HashMap<Long, String>();
		List<CaringTypeView> caringTypeViewList = new UtilityServiceAgent().getCaringTypeList();
		if(caringTypeViewList != null && caringTypeViewList.size() > 0)
		{
			for(CaringTypeView caringType : caringTypeViewList)
			{
				caringMap.put(caringType.getCaringTypeId(), CommonUtil.getIsEnglishLocale()?caringType.getCaringTypeNameEn():caringType.getCaringTypeNameAr());
			}
		}
		return caringMap;
	}

	public static void printMEMSPaymentReceipt(String requestId, String collectionTransactionId, FacesContext facesContext)throws Exception
	{
		MEMSPaymentReceiptReportCriteria reportCriteria = new MEMSPaymentReceiptReportCriteria(
				ReportConstant.Report.MEMS_PAYMENT_RECEIPT_REPORT_ENAR,
				ReportConstant.Processor.MEMS_PAYMENT_RECEIPT_REPORT, CommonUtil.getLoggedInUser() );
		HttpServletRequest request = (HttpServletRequest) facesContext
				.getExternalContext().getRequest();		
		reportCriteria.setCollectionTransactionId(collectionTransactionId);		
		reportCriteria.setRequestId( requestId );
		request.getSession().setAttribute(
				ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup();
	}
	
	public static void printMiscellaneousPaymentReceipt(String requestId, String paymentScheduleIdId, FacesContext facesContext)throws Exception
	{
		MEMSPaymentReceiptReportCriteria reportCriteria = new MEMSPaymentReceiptReportCriteria(
																								ReportConstant.Report.MISC_PAYMENT_RECEIPT_REPORT_ENAR,
																								ReportConstant.Processor.MISC_PAYMENT_RECEIPT_REPORT, 
																								CommonUtil.getLoggedInUser() 
																							   );
		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();		
		reportCriteria.setPaymentScheduleId( paymentScheduleIdId );		
		reportCriteria.setRequestId( requestId );
		request.getSession().setAttribute( ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria );
		openPopup();
	}
	public static List<Long> getAssetTypeIdWhereDepttMandatory() throws Exception
	{
		List<Long>  assetTypeIds = new ArrayList<Long>();
		List<SearchAssetsCriteriaView> assetTypesList = null;
		assetTypesList = new SearchAssetsService().getAssetTypesList();
		if(assetTypesList != null && assetTypesList.size() > 0)
		{
			for(SearchAssetsCriteriaView assetType : assetTypesList)
			{
				if( assetType.getIsMandatory().compareTo(1L) == 0 )
					assetTypeIds.add(assetType.getAssetTypeid());
			}
		}
    	return assetTypeIds;
	}
	
	
	@SuppressWarnings("unchecked")
	public static boolean isPaymentBouncedReplacedOrExempted(Long paymentStatusId) {
		List<Long> paymentScheduleBouncedStatusList = (List<Long>) servletcontext.getAttribute(WebConstants.ServletContext.PAYMENT_SCHEDULE_BOUNCED_REPLACED_ExEMPTED_STATUS_LIST);
		return paymentScheduleBouncedStatusList.contains(paymentStatusId);
	}
	
	public static void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public static String printMemsReport(Long requestId) throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		FacesContext facesContext = FacesContext.getCurrentInstance();		
		requestDetailsReportCriteria=new RequestDetailsReportCriteria( ReportConstant.Report.REQUEST_DETAILS_MEMS, 
				                                                       ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,
				                                                       CommonUtil.getLoggedInUser()
				                                                      );
		if( requestId != null ) 
		{
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}	
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	public static String printMasrafDisbursementReport(Long requestId) throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		FacesContext facesContext = FacesContext.getCurrentInstance();		
		requestDetailsReportCriteria=new RequestDetailsReportCriteria( ReportConstant.Report.REQUEST_DETAILS_MASRAF_DISBURSEMENT, 
				                                                       ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,
				                                                       CommonUtil.getLoggedInUser()
				                                                      );
		if( requestId != null ) 
		{
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}	
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	public static String printEndowmentDisbursementReport(Long requestId) throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		FacesContext facesContext = FacesContext.getCurrentInstance();		
		requestDetailsReportCriteria=new RequestDetailsReportCriteria( ReportConstant.Report.REQUEST_DETAILS_ENDOWMENT_DISBURSEMENT, 
				                                                       ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,
				                                                       CommonUtil.getLoggedInUser()
				                                                      );
		if( requestId != null ) 
		{
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}	
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	public static String printReport(Long requestId)throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		
		FacesContext facesContext = FacesContext.getCurrentInstance();		
		
		if(CommonUtil.getIsEnglishLocale()) {
			requestDetailsReportCriteria=new RequestDetailsReportCriteria(ReportConstant.Report.REQUEST_DETAILS_REPORT_AR, ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,CommonUtil.getLoggedInUser());
		}	
		else {    
			requestDetailsReportCriteria=new RequestDetailsReportCriteria(ReportConstant.Report.REQUEST_DETAILS_REPORT_AR, ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,CommonUtil.getLoggedInUser());
		}	
		if(requestId!=null) {
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}	
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	public static String printZawayaReport(Long requestId)throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		
		User loggedInUser= ApplicationContext.getContext().get(WebContext.class).getAttribute(CoreConstants.CurrentUser);
		String loggedInUserName = loggedInUser.getSecondaryFullName();
		
		if(CommonUtil.getIsEnglishLocale() || loggedInUserName == null )
		{
			loggedInUserName= loggedInUser.getFullName();
		}
		requestDetailsReportCriteria=new RequestDetailsReportCriteria(
																		"ZawayaRequest.rpt", 
																		ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,
																		loggedInUserName
																	  );
		
		if(requestId!=null) {
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	public static String printZawayaReportAuction(Long requestId)throws Exception
	{
		RequestDetailsReportCriteria requestDetailsReportCriteria;
		
		User loggedInUser= ApplicationContext.getContext().get(WebContext.class).getAttribute(CoreConstants.CurrentUser);
		String loggedInUserName = loggedInUser.getSecondaryFullName();
		
		if(CommonUtil.getIsEnglishLocale() || loggedInUserName == null )
		{
			loggedInUserName= loggedInUser.getFullName();
		}
		requestDetailsReportCriteria=new RequestDetailsReportCriteria(
																		"ZawayaReportAuction.rpt", 
																		ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,
																		loggedInUserName
																	  );
		
		if(requestId!=null) {
			requestDetailsReportCriteria.setRequestId(requestId.toString());
		}
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
    public static void viewPaymentDetails(PaymentScheduleView paymentScheduleView) {
    	//PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	FacesContext facesContext = FacesContext.getCurrentInstance();		
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    	request.getSession().setAttribute("PAYMENT_SCHEDULE_VIEW",paymentScheduleView);
    	openPopup("PaymentsDetails.jsf", "");

    }

	private static void openPopup(String URLtoOpen, String extraJavaScript) {
		String methodName="openPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+1024+',height='+450+',left=0,top=40,scrollbars=yes,status=yes');";
		if (extraJavaScript!=null&& extraJavaScript.trim().length()>0)    {   
				javaScriptText=extraJavaScript;
		}	

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
	}
	public static boolean isLoggedInUserCreatedRecord(String createdByUserId) throws Exception
	{
		boolean result = false;
		logger.logDebug( "isLoggedInUserCreatedRecord|createdByUserId:%s|",createdByUserId);
		User loggedInUser = SecurityManager.getUser(getLoggedInUser());
		User createdByUser = SecurityManager.getUser(createdByUserId);
		
		List<UserGroup> loggedInUserGroups =new ArrayList<UserGroup>(0);
		List<UserGroup> createdByUserGroups = new ArrayList<UserGroup>(0);

		loggedInUserGroups = loggedInUser.getUserGroups();
		createdByUserGroups = createdByUser.getUserGroups();
		if( !isListEmpty(loggedInUserGroups) && !isListEmpty(createdByUserGroups))
		{
			LoggedInLoop:	
			for(UserGroup loggedInGroup : loggedInUserGroups)
			{
						for(UserGroup createdByGroup : createdByUserGroups)
						{
							if(loggedInGroup.getUserGroupId().compareTo(createdByGroup.getUserGroupId()) == 0)
							{
								result = true;
								break LoggedInLoop;
							}
						}
			}
		}
		return result;
	} 	
	private static boolean isListEmpty(List list){
		if(list != null && list.size() > 0 ){
			return false;
		}
		else
			return true;
	}
	public static String getCSVGroupsOfUserById(String userId){
		User user = SecurityManager.getUser(userId);
		List<UserGroup> userGroups =new ArrayList<UserGroup>(0);
		userGroups = user.getUserGroups();
		String userGroupsCSV = "";

		if(userGroups != null && userGroups.size() > 0){
			if(CommonUtil.getIsEnglishLocale()){
				for(UserGroup group : userGroups){//select different language name if one is not available
					if(group.getPrimaryName() != null){
						userGroupsCSV += "["+group.getPrimaryName()+"]"; 
					}
					else{
						userGroupsCSV += "["+group.getSecondaryName()+"]";
					}
					userGroupsCSV+= ",";
				}
			}
			else{
				for(UserGroup group : userGroups){
					if(group.getSecondaryName() != null){
						userGroupsCSV += "["+group.getSecondaryName()+"]"; 
					}
					else{
						userGroupsCSV += "["+group.getPrimaryName()+"]";
					}
				}
			}
			userGroupsCSV = userGroupsCSV.substring(0, userGroupsCSV.length()-1);
		}
		return userGroupsCSV;
	}
	public static String CombineCostCenter(String oldCC, String newCC){
		return  StringHelper.isNotEmpty(oldCC)?oldCC+" / "+(StringHelper.isNotEmpty(newCC)? newCC:""):StringHelper.isNotEmpty(newCC)? newCC:"";
	}
	
	public static List<SelectItem> getSecUserList() throws Exception
	{
		List<UserDbImpl> secUserPojo = new InheritanceFileService().getSecUserList();
		List<SelectItem> createdByList = new ArrayList<SelectItem>(0);
		for (UserDbImpl secUser : secUserPojo) 
		{
		 SelectItem item = new SelectItem();
		 item.setLabel(secUser.getFullName());
		 item.setValue(secUser.getLoginId());
		 createdByList.add(item);
		}
        return createdByList;		
	}
	
	public static  void updateRequestTaskAcquiredBy(UserTask userTask,String acquiredBy) throws PimsBusinessException 
	{
		Map<String,String> taskAttributes =  userTask.getTaskAttributes();
		
		if( taskAttributes.containsKey("REQUEST_ID") )
		{
		   RequestService rs = new RequestService();
		   rs.updateRequestTaskAcquiredBy( new Long( taskAttributes.get("REQUEST_ID") ), userTask.getTaskId(), acquiredBy);
		}
	}
	
	public static void onAdultFromMinor( long personId,String updatedBy )throws Exception
	{
		try 
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 InheritanceFileService.onAdultFromMinor(personId, updatedBy);
			 saveSystemComments("Person", "common.person.AdultFromMinor", personId);
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	public static void onMinorFromAdult( long personId,String updatedBy )throws Exception
	{
		try 
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 InheritanceFileService.onMinorFromAdult(personId, updatedBy);
			 saveSystemComments("Person", "common.person.MinorFromAdult", personId);
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	public static StatementOfAccountCriteriaView transformToStatementOfAccountCriteriaView(StatementOfAccountCriteria arg ) throws Exception
	{
		StatementOfAccountCriteriaView ret = new StatementOfAccountCriteriaView ();
		ret.setCostCenters(arg.getCostCenters() )   ;
		ret.setEndowmentFileId(arg.getEndowmentFileId())  ;
		ret.setEndowmentFileName(arg.getEndowmentFileName())  ;
		ret.setEndowmentFileNumber(arg.getEndowmentFileNumber())  ;
		ret.setEndowmentId(arg.getEndowmentId())  ;
		ret.setFormatterReportDisplay(arg.getFormatterReportDisplay())  ;
		ret.setForYear(arg.getForYear())  ;
		ret.setFromDate(arg.getFromDate())  ;
		ret.setInheritanceBeneficiaryId(arg.getInheritanceBeneficiaryId())  ;
		ret.setInheritancefileId(arg.getInheritancefileId())  ;
		ret.setLoggedInUser(arg.getLoggedInUser())  ;
		ret.setMasrafBeneficiaryName(arg.getMasrafBeneficiaryName())  ;
		ret.setMasrafId(arg.getMasrafId())  ;
		ret.setMasrafName(arg.getMasrafName())  ;
		ret.setParentMasrafName(arg.getParentMasrafName())  ;
		ret.setPersonId(arg.getPersonId())  ;
		ret.setPreviousYear(arg.getPreviousYear())  ;
		ret.setShowSubMasrafTrx(arg.getShowSubMasrafTrx())  ;
		ret.setToDate(arg.getToDate())  ;
		ret.setToDatePreviousYear(arg.getToDatePreviousYear())  ;
		ret.setWaqifGRPNumber(arg.getWaqifGRPNumber())  ;
		ret.setWaqifName(arg.getWaqifName())  ;
		return ret;
	}
	public static String GET(String url) throws RuntimeException, 
    Exception 
	{
	
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		logger.logInfo("Url To Call:" + url);
		HttpGet request = new HttpGet(url);
		request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
		//        request.addHeader("content-type", "application/x-www-form-urlencoded");
		CloseableHttpResponse response1 = httpclient.execute(request);
		try {
		
		HttpEntity entity = response1.getEntity();
		if (entity != null) {
		return EntityUtils.toString(entity, HTTP.UTF_8).replace("&lt;","<").replace("&gt;",">");
		}
		
		
		} 
		catch(Exception e){throw e;}
		finally {
		response1.close();
		}
		return "";
	
	}

	
    public static String POST(String callUrl, String postInfo,String contentType) throws RuntimeException, Exception 
    {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        logger.logInfo("Url To Call:" + callUrl);
        HttpPost request = new HttpPost(callUrl);
        request.setHeader("Accept-Charset", "UTF-8");
        request.addHeader("content-type", contentType);
        request.addHeader("charset", "utf-8");
        
        StringEntity params = new StringEntity(postInfo,"UTF-8");
        request.setEntity(params);

        CloseableHttpResponse response1 = httpclient.execute(request);
        try {

            HttpEntity entity = response1.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, HTTP.UTF_8).replace("&lt;","<").replace("&gt;",">");
            }


        }
        catch(Exception e){throw e;}
        
        finally {
            response1.close();
        }
        return "";

    }
    
    public List<SelectItem> getAssignedInheritanceExpertGroup() throws PimsBusinessException {

		String groupNamesCSV = new UtilityServiceAgent()
				.getValueFromSystemConfigration(WebConstants.SYS_CONFIG_ASSIGNED_INHERITANCE_EXPERT_GROUP);
		if (groupNamesCSV != null) {
			String[] groupNames = groupNamesCSV.split(",");
			List groupList = Arrays.asList(groupNames);
			return getUsersByGroupId(groupList);
		} else
			return null;

	}
    
    public List<SelectItem> getFamilyVillageVillaStaffGroup() throws PimsBusinessException {

		String groupNamesCSV = new UtilityServiceAgent()
				.getValueFromSystemConfigration(WebConstants.SYS_CONFIG_FAMILY_VILLAGE_VILLA_STAFF_GROUP);
		if (groupNamesCSV != null) {
			String[] groupNames = groupNamesCSV.split(",");
			List groupList = Arrays.asList(groupNames);
			return getUsersByGroupId(groupList);
		} else
			return null;

	}

    public static void completeTask( 
                                                                        WorkFlowVO workFlowVO,
                                                                        TaskListVO userTask,
                                                                        String userId
                                                                   )throws Exception
        {
        
                WFClient client = new WFClient();
                workFlowVO.setUserId(userId);
                workFlowVO.setTaskId(userTask.getTaskId());
                workFlowVO.setProcedureTaskId(userTask.getProcedureTaskId());
                String result = client.completeTask(workFlowVO);
                
                logger.logInfo("result:client.completeTask " +  result);
        }

    
}

