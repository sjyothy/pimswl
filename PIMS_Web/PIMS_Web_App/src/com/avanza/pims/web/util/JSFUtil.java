package com.avanza.pims.web.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

public class JSFUtil {
    /**
         * <p>Return the value to be rendered, as a String (converted
         * if necessary), or <code>null</code> if the value is null.</p>
         * @param componnent  The component that needs to display the value
         * as a String
         * @return A String value of the component
         */
    public static String getValueAsString(UIOutput componnent)
    {
        return convertValueToString(componnent,componnent.getValue());
    }
    
    
    public static String getRequestParamAsString(String strRequestParam)
    {
    	   FacesContext context = FacesContext.getCurrentInstance();
    	   if(context.getExternalContext().getRequestMap().get(strRequestParam)==null)
    		   return new String("");
    	   else
    		   return context.getExternalContext().getRequestMap().get(strRequestParam).toString();
    }
    
    /**
     * Converts an Object (which may or may not be the value of the
     * component) to a String using the converter associated
     * with the component. This method can be used to convert the
     * value of the component, or the value of an Object associated
     * with the component, such as the objects representing the
     * options for a listbox or a checkboxgroup.
     * @param component The component that needs to display the value
     * as a String
     * @param realValue The object that the component is to display
     * @throws ConverterException if the conversion fails
     * 
     * @return If converting the Object to a String fails
     */
    private static String convertValueToString(UIComponent component,
                                              Object realValue) 
            throws ConverterException {
              
      //  if(DEBUG) log("convertValueToString(UIComponent, Object)"); 

        // The way the RI algorithm is written, it ends up returning
        // and empty string if the realValue is null and there is no 
        // converter, and null if there is a converter (the converter
        // is never applied). I don't think that's right, but I'm 
        // not sure what the right thing to do is. I return an empty
        // string for now. 
        
        if(realValue == null) { 
            return new String();  
        }
        
        if(realValue instanceof String) { 
            return (String)realValue; 
        } 

       /* 
       // not being used
       // Converter converter = ((ValueHolder)component).getConverter(); 
        
        // Case 1: no converter specified for the component. Try 
        // getting a default converter, and if that fails, invoke 
        // the .toString() method. 
        if (converter == null) {
               */
            // if converter attribute set, try to acquire a converter
            // using its class type. (avk note: this is the comment from 
            // the RI - not sure what it's supposed to mean)
            
            Converter converter =  getConverterForClass(realValue.getClass());
            
            // if there is no default converter available for this identifier,
            // assume the model type to be String. Otherwise proceed to case 2.
            if (converter == null) {
                return String.valueOf(realValue); 
            }
      //  }
        
        // Case 2: we have found a converter.
        FacesContext context = FacesContext.getCurrentInstance(); 
        return converter.getAsString(context, component, realValue); 
    }

    /**
       * This method retrieves an appropriate converter based on the
       * type of an object. 
       * @param converterClass The name of the converter class
       * @return An instance of the appropriate converter type
       */
      private static Converter getConverterForClass(Class converterClass) {

          if (converterClass == null) {
              return null;
          }
          try {
              ApplicationFactory aFactory =
                      (ApplicationFactory) FactoryFinder.getFactory(
                      FactoryFinder.APPLICATION_FACTORY);
              Application application = aFactory.getApplication();
              return (application.createConverter(converterClass));
          } catch (Exception e) {
              return (null);
          }
      }
      
      /**
       * 
       * @param key the key against the the value is required
       * @param messageBundleName the fully qualified class name of the ResourceBundle being used by the application
       * @param currLocale the current Locale for the user
       * @return the value against the key from the Resource Bundle
       */
    public static String getLocalizedMessage( String key, String messageBundleName,Locale currLocale )
    {
            ResourceBundle resourceBundle = ResourceBundle.getBundle(messageBundleName, currLocale);
            return resourceBundle.getString(key);
    }


    /**
     * 
     * @param context the Current Faces Context Instance
     * @param severity represent message severity levels
     * @param summary the summary message to be added
     */
    public static void setFacesMessage(FacesContext context,FacesMessage.Severity severity,String summary)
    {
    	String viewId = context.getViewRoot().getViewId();
    	context.addMessage(viewId, new FacesMessage( FacesMessage.SEVERITY_INFO, summary ,summary ) );
    }


}
