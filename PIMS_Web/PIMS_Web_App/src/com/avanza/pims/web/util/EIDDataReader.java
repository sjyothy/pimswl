package com.avanza.pims.web.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.richfaces.json.HTTP;

import com.avanza.core.util.Logger;

public class EIDDataReader 
{
	private static Logger logger = Logger.getLogger(EIDDataReader.class);
	static SystemParameters parameters = SystemParameters.getInstance();
	static String cardReaderWSEndPoint= parameters.getParameter( "CardDataWS" );
    public static String callUrlJSON(String urlMethod,String postInfo) throws RuntimeException, Exception 
    {

        CloseableHttpClient httpclient = HttpClients.createDefault();
//        String callUrl = cardReaderWSEndPoint +urlMethod;
        String callUrl = urlMethod;
        logger.logInfo("Url To Call:" + callUrl);
        HttpPost request = new HttpPost(callUrl);
        request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
        request.addHeader("content-type", "application/json");
        
        StringEntity params = new StringEntity(postInfo);
        request.setEntity(params);

        CloseableHttpResponse response1 = httpclient.execute(request);
        try {

            HttpEntity entity = response1.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, org.apache.http.protocol.HTTP.UTF_8).replace("&lt;","<").replace("&gt;",">");
            }


        } finally {
            response1.close();
        }
        return "";

    }
}

