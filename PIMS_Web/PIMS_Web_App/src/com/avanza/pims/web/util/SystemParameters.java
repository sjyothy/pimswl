package com.avanza.pims.web.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.avanza.core.jsf.appbase.ApplicationException;
import com.avanza.core.util.Logger;

/**
 * 
 * @author Kamran Shahin
 */

public class SystemParameters
{
    private static Logger logger = Logger.getLogger( SystemParameters.class );
    private static String PARAMETERS_FILE = "\\WEB-INF\\AMAF-PIMS.properties";

    private static FileConfiguration config;

    private static SystemParameters me;

//    static
//    {
//        loadParametersFromFile();
//    }

    /** Creates a new instance of SystemParameters */
    private SystemParameters( )
    {
    }

    public static SystemParameters getInstance()
    {
        if ( me == null )
        {
            me = new SystemParameters();
        }

        return me;
    }

    private static void loadParametersFromFile()
    {
        logger.logInfo( " About to load parameters from File: " + PARAMETERS_FILE );
        try
        {
            config = new PropertiesConfiguration( PARAMETERS_FILE );
            config.setEncoding( "UTF-8" );
            config.setAutoSave( true );
        } catch ( ConfigurationException e )
        {
            throw new ApplicationException( e );
        }
        logger.logInfo( " Parameters loaded from File successfully. " );
    }
    public static void loadParametersFromFile(String realPath)
    {
        logger.logInfo( " About to load parameters from File: %s" ,realPath+PARAMETERS_FILE );
        try
        {
            config = new PropertiesConfiguration( realPath+PARAMETERS_FILE );
            config.setEncoding( "UTF-8" );
            config.setAutoSave( true );
        } catch ( ConfigurationException e )
        {
            throw new ApplicationException( e );
        }
        logger.logInfo( " Parameters loaded from File successfully. " );
    }

    public String getParameter( String aParamName )
    {
        return config.getString( aParamName );
    }

    public boolean getBoolean( String parameter )
    {
        return config.getBoolean( parameter );
    }
    
    public int getInteger(String parameter)
    {
        return config.getInt( parameter );
    }

    public void setParameter( String aParamName, Object value )
    {
        config.setProperty( aParamName, value );
    }

    public String[] getParamterValues( String parameter )
    {
        return config.getStringArray( parameter );
    }

    public String getAllParamterValues( String parameter, String separator )
    {
        StringBuffer stringBuffer = new StringBuffer();
        String[] values = config.getStringArray( parameter );
        String result;
        for ( String value : values )
        {
            stringBuffer.append( value );
            stringBuffer.append( separator );
        }
        result = stringBuffer.toString();
        return result.substring( 0, result.length() - 1 );
    }

    public Map<String, String> getAllKeyValues()
    {
        Map<String, String> keyValue = new HashMap<String, String>();
        Iterator iterator = config.getKeys();

        while ( iterator.hasNext() )
        {

            String key = (String) iterator.next();
            
            keyValue.put( key, config.getString( key ) );
            
            if(key.equalsIgnoreCase( "RUN_BACKUP_SERVICE_DAYS" ) || key.equalsIgnoreCase( "BACKUP_FILE_LOCATION" )){
                keyValue.remove( key );
            }

        }

        return keyValue;
    }
    
    public Hashtable<String, String> getKeyValuePair()
    {
        Hashtable<String, String> keyValue = new Hashtable<String, String>();
        Iterator iterator = config.getKeys();

        while ( iterator.hasNext() )
        {

            String key = (String) iterator.next();
            
            if(key.equals( "FILE_UPLOAD_ALLOWED_EXTENSTION" )){
                keyValue.put( key.toLowerCase(), this.getAllParamterValues( key, "," ) );   
            }
            else{
                keyValue.put( key.toLowerCase(), config.getString( key ) );    
            }
            

        }

        return keyValue;
    }


}