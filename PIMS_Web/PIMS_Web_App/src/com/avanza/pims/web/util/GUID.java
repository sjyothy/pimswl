package com.avanza.pims.web.util;

import java.util.Random;

public class GUID {
    private static GUID GUIDFactory = new GUID();
    static Random random = new Random();

    /**
     * Allow global replacement of the GUID generator.  Applications
     * wishing to install their own GUID generators should sub-class
     * GUID, override the getGUID() method, and use this method to
     *  install their generator.
     */

    public static void
    setGUIDImpl(GUID factory) {
	GUIDFactory = factory;
    }

    /**
     * Return a GUID as a string.
     */

    public static String getString() {
	return GUIDFactory.getGUIDString();
    }

    /**
     * Return a GUID as a string.  This is completely arbitrary, and
     * returns the hexification of a random value followed by a
     * timestamp.
     */

    protected String getGUIDString() {
       long rand = (random.nextLong() & 0x7FFFFFFFFFFFFFFFL) |
      	 0x4000000000000000L;
       return Long.toString(rand, 32) +
	      Long.toString(System.currentTimeMillis()&0xFFFFFFFFFFFFFL, 32);
    }
}

