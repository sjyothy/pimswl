package com.avanza.pims.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.jsf.appbase.AbstractPageBean;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.modes.Control;
import com.avanza.pims.web.modes.Mode;
import com.avanza.pims.web.modes.Page;
import com.avanza.pims.web.modes.PagesModesConfigManager;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.JSFUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;

public class AbstractController extends AbstractPageBean implements Serializable{

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap = context.getExternalContext().getSessionMap();
	protected Map viewMap = getFacesContext().getViewRoot().getAttributes();
	private static Logger logger = Logger.getLogger(AbstractController.class);
	protected HtmlCommandButton btnCollectPayment=new HtmlCommandButton();  
	protected HtmlOutputLink linkPrintEjariCertificate = new HtmlOutputLink();
	protected HtmlOutputLink linkPrintEjariContract= new HtmlOutputLink();
	//For server side paging and sorting
    public final int getTotalRows() {
    	int totalRows = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_TOTAL_ROWS"))
    		totalRows =  (Integer) viewMap.get("SERVER_SIDE_PAGING_TOTAL_ROWS");
		return totalRows;
	}

	public final void setTotalRows(int totalRows) {
		viewMap.put("SERVER_SIDE_PAGING_TOTAL_ROWS",totalRows);
	}

	public final int getRowsPerPage() {
		int rowsPerPage = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_ROWS_PER_PAGE"))
    		rowsPerPage =  (Integer) viewMap.get("SERVER_SIDE_PAGING_ROWS_PER_PAGE");
		return rowsPerPage;
	}

	public final void setRowsPerPage(int rowsPerPage) {
		viewMap.put("SERVER_SIDE_PAGING_ROWS_PER_PAGE",rowsPerPage);
	}

	public final int getTotalPages() {
		int totalPages = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_TOTAL_PAGES"))
    		totalPages =  (Integer) viewMap.get("SERVER_SIDE_PAGING_TOTAL_PAGES");
		return totalPages;		
	}

	public final void setTotalPages(int totalPages) {
		viewMap.put("SERVER_SIDE_PAGING_TOTAL_PAGES",totalPages);
	}

	public final int getPageRange() {
		int pageRange = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_PAGE_RANGE"))
    		pageRange =  (Integer) viewMap.get("SERVER_SIDE_PAGING_PAGE_RANGE");
		return pageRange;		
	}

	public final void setPageRange(int pageRange) {
		viewMap.put("SERVER_SIDE_PAGING_PAGE_RANGE",pageRange);
	}

	public final Integer[] getPages() {
		Integer[] pages = new Integer[0];
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_PAGES"))
    		pages =  (Integer[]) viewMap.get("SERVER_SIDE_PAGING_PAGES");
		return pages;	
	}

	public final void setPages(Integer[] pages) {
		viewMap.put("SERVER_SIDE_PAGING_PAGES",pages);
	}

	public final String getSortField() {
		String sortField = "";
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_SORT_FIELD"))
    		sortField =  (String) viewMap.get("SERVER_SIDE_PAGING_SORT_FIELD");
		return sortField;
	}

	public final void setSortField(String sortField) {
		viewMap.put("SERVER_SIDE_PAGING_SORT_FIELD",sortField);		
	}

	public final boolean isSortItemListAscending() {
		boolean sortAscending = false;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_SORT_ORDER"))
    		sortAscending =  (Boolean) viewMap.get("SERVER_SIDE_PAGING_SORT_ORDER");
		return sortAscending;		
	}

	public final void setSortItemListAscending(boolean sortAscending) {
		viewMap.put("SERVER_SIDE_PAGING_SORT_ORDER",sortAscending);
	}

	public final int getFirstRow() {
		int firstRow = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_FIRST_ROW"))
    		firstRow =  (Integer) viewMap.get("SERVER_SIDE_PAGING_FIRST_ROW");
		return firstRow;
	}

	public final void setFirstRow(int firstRow) {
		viewMap.put("SERVER_SIDE_PAGING_FIRST_ROW",firstRow);		
	}

	public final int getCurrentPage() {
		int currentPage = 0;
    	if(viewMap.containsKey("SERVER_SIDE_PAGING_CURRENT_PAGE"))
    		currentPage =  (Integer) viewMap.get("SERVER_SIDE_PAGING_CURRENT_PAGE");
		return currentPage;
	}

	public final void setCurrentPage(int currentPage) {
		viewMap.put("SERVER_SIDE_PAGING_CURRENT_PAGE",currentPage);		
	}
	
	public final void pageFirst() {
        page(0);
    }

    public final void pageNext() {
        page(getFirstRow() + getRowsPerPage());
    }

    public final void pagePrevious() {
        page(getFirstRow() - getRowsPerPage());
    }

    public final void pageLast() {
        page(getTotalRows() - ((getTotalRows() % getRowsPerPage() != 0) ? getTotalRows() % getRowsPerPage() : getRowsPerPage()));
    }

    public final void page(ActionEvent event) {
        page(((Integer) ((UICommand) event.getComponent()).getValue() - 1) * getRowsPerPage());
    }

    private final void page(int firstRow) 
    {
    	setFirstRow(firstRow);
    	doSearchItemList(); // Load requested page.
    }
    
    public final void doPagingComputations()
    {
    	// Set currentPage, totalPages and pages.
        int currentPage = (getTotalRows() / getRowsPerPage()) - ((getTotalRows() - getFirstRow()) / getRowsPerPage()) + 1;
        setCurrentPage(currentPage);
        
        int totalPages = (getTotalRows() / getRowsPerPage()) + ((getTotalRows() % getRowsPerPage() != 0) ? 1 : 0);
        setTotalPages(totalPages);
        
        int pagesLength = Math.min(getPageRange(), getTotalPages());
        Integer[] pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, getCurrentPage() - (getPageRange() / 2)), getTotalPages() - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
        setPages(pages);
    }
    public final void sort(ActionEvent event) 
    {
        String sortFieldAttribute = (String) event.getComponent().getAttributes().get("sortField");        
        // If the same field is sorted, then reverse order, else sort the new field ascending.
        if (getSortField().equals(sortFieldAttribute))
        {
        	setSortItemListAscending(!isSortItemListAscending());
        } else 
        {
            setSortField(sortFieldAttribute);
            setSortItemListAscending(true);
        }         
        pageFirst(); // Go to first page and load requested page.
    }
    public void doSearchItemList()
    {
    	// this method is to be implemented by the backing beans. Override this method in backing beans
    }
	
    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    public SessionScope getSessionScope()
    {
        return (SessionScope) getBean("SessionScope");
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    public ApplicationScope getApplicationScope()
    {
        return (ApplicationScope) getBean("ApplicationScope");
    }
    
    /**
     * sets the value from the current request
     * @param key the key that points to a writable property
     * @param value New value for the property pointed at by <code>Key</code>
     */
    public void setRequestParam(String key,Object value)
    {     
        setValue("#{requestScope."+key+"}",value);
    }
    
    /**
     * gets the value from the current request
     * @param key  the key that points to a writable property
     * @return the value for the property pointed at by <code>Key</code>
     */
    public Object getRequestParam(String key)
    {
        return getValue("#{requestScope."+key+"}");
    }
  
    public void setErrorMessage(String key)
    {
    	String message = getSessionScope().getLocalizedMessage(key);
    	JSFUtil.setFacesMessage(getFacesContext(), FacesMessage.SEVERITY_ERROR, message);
    }
    
    public void setWarningMessage(String key)
    {
    	String message = getSessionScope().getLocalizedMessage(key);
    	JSFUtil.setFacesMessage(getFacesContext(), FacesMessage.SEVERITY_WARN, message);
    }
 
    public void setInfoMessage(String key)
    {
    	String message = getSessionScope().getLocalizedMessage(key);
    	JSFUtil.setFacesMessage(getFacesContext(), FacesMessage.SEVERITY_INFO, message);
    }
	public String getCurrentLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getShortDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }	
	public String getLoggedInUserId()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = "";
		if(user!=null)
			loginId = user.getLoginId();
		return loginId;
	}
	public User getLoggedInUserObj()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		return webContext.getAttribute(CoreConstants.CurrentUser);
		
		
	}
	public void updatePageForMode( String modeId ) throws Exception {		
		
		Page page = PagesModesConfigManager.getPagesModesConfig().getPage( this.getClass().getCanonicalName() );
		
		if ( page != null ) 
		{
			Mode mode = page.getMode(modeId);
			
			if ( mode != null ) 
			{			
				for ( Control control : mode.getControlsMap().values() )
				{					
					UIComponent component = (UIComponent) this.getClass().getField( control.getControlId() ).get(this);
					component.setRendered( control.getRendered() );
					
					Map<String, Object> attributeMap  =  component.getAttributes();					
					
					if ( attributeMap.get("readonly") != null && control.getReadOnly() != null )
						attributeMap.put("readonly", control.getReadOnly());
					
					if ( attributeMap.get("disabled") != null && control.getDisabled() != null )
						attributeMap.put("disabled", control.getDisabled());
						
				}
			}
		}
	}
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		final String METHOD_NAME = "generateNotification()";
		boolean success = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
		
		return success;
	}
	@SuppressWarnings("unchecked")
	public List<ContactInfo> getContactInfoList(PersonView personView) 
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		if(personView.getContactInfoViewSet() == null || personView.getContactInfoViewSet().size()<=0)
    		iter = new UtilityService().getContractInfos(personView).iterator();
    	
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(personView.getPersonId().toString(), personView.getCellNumber(), null, ciView.getEmail()));    			
            }
    	}
    	return emailList;
	}
	public TimeZone getLocalTimeZone()

	{
	       return TimeZone.getDefault();
	}
	@SuppressWarnings("unchecked")
	public void onAdultFromMinor() 
	{
		try
		{
			if(viewMap.get(WebConstants.PERSON_ID) != null)
			{
				Long personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID).toString() ); 
				CommonUtil.onAdultFromMinor( personId, getLoggedInUserId() );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onAdultFromMinor|Error Occured ", ex);
//			errorMessages = new ArrayList<String>();
//			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
	
		}
	}

	@SuppressWarnings("unchecked")
	public void onMinorFromAdult() 
	{
		try
		{
			if(viewMap.get(WebConstants.PERSON_ID) != null)
			{
				Long personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID).toString() ); 
				CommonUtil.onMinorFromAdult( personId, getLoggedInUserId() );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onMinorFromAdult|Error Occured ", ex);
//			errorMessages = new ArrayList<String>();
//			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
	
		}
	}

	public void onProcessComplete()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText="javaScript:onProcessComplete();";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		
	}

	public boolean isTaskAvailable()
	{
		if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
			return true;
		else
			return false;
	}

	public HtmlCommandButton getBtnCollectPayment() {
		return btnCollectPayment;
	}

	public void setBtnCollectPayment(HtmlCommandButton btnCollectPayment) {
		this.btnCollectPayment = btnCollectPayment;
	}
	public void prerender()
	{
		super.prerender();
		if( sessionMap.containsKey( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION ) )
			btnCollectPayment.setDisabled( true );
		else if ( viewMap.containsKey("ENABLE_COLLECT_PAYMENT")  )
			btnCollectPayment.setDisabled( false );
		else
			btnCollectPayment.setDisabled( false );
	}
	@SuppressWarnings( "unchecked" )
	public void enableCollectPayment( )
	{
		viewMap.put("ENABLE_COLLECT_PAYMENT", true );
	}

	public HtmlOutputLink getLinkPrintEjariCertificate() {
		return linkPrintEjariCertificate;
	}

	public void setLinkPrintEjariCertificate(
			HtmlOutputLink linkPrintEjariCertificate) {
		this.linkPrintEjariCertificate = linkPrintEjariCertificate;
	}

	public HtmlOutputLink getLinkPrintEjariContract() {
		return linkPrintEjariContract;
	}

	public void setLinkPrintEjariContract(HtmlOutputLink linkPrintEjariContract) {
		this.linkPrintEjariContract = linkPrintEjariContract;
	}
	
	
	 
}
