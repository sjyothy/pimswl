package com.avanza.pims.web.controller;

import com.avanza.core.jsf.appbase.AbstractSessionBean;
import com.avanza.core.security.User;   
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.JSFUtil;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class SessionScope  extends AbstractSessionBean{

    private User loggedInUser;
    private Locale currentLocale = new Locale( "ar" );
    private Map<String,Integer> userStatusItems; 
    public SessionScope() {
    }


    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
        
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }


    public void setCurrentLocale(Locale currentLocale) {
        this.currentLocale = currentLocale;
    }

    public Locale getCurrentLocale() {
        return currentLocale;
    }
    
    /**
     * Get a Message from the resource bundle for the key specified. 
     * The value returned is according to the current locale in use
     * @param key the key
     * @return the message against the key
     */
    public String getLocalizedMessage( String key )
        {   
            return JSFUtil.getLocalizedMessage(key,getFacesContext().getApplication().getMessageBundle(),currentLocale);
            
        }
        
    public boolean isEnglishLocale()
    {
        if(currentLocale.equals(Locale.ENGLISH))
            return true;
        else
            return false;
    }
    
    
    private void setUserStatusItemsOnStartup()
    {
    	userStatusItems = new HashMap<String,Integer>();
                
        String label = getLocalizedMessage("commons.user.status."+WebConstants.ACTIVE_STATUS);                  
        userStatusItems.put(label,WebConstants.ACTIVE_STATUS_ID);
        
        label = getLocalizedMessage("commons.user.status."+WebConstants.IN_ACTIVE_STATUS);
        userStatusItems.put(label,WebConstants.IN_ACTIVE_STATUS_ID);
        
        label = getLocalizedMessage("commons.user.status."+WebConstants.DELETE_STATUS);
        userStatusItems.put(label,WebConstants.DELETE_STATUS_ID	);
    }


	/**
	 * @return the userStatusItems
	 */
	public Map<String,Integer> getUserStatusItems() {
		setUserStatusItemsOnStartup();
		return userStatusItems;
	}
    
}
