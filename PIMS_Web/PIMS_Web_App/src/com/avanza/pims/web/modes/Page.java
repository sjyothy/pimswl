package com.avanza.pims.web.modes;

import java.util.HashMap;
import java.util.Map;

public class Page {
	
	private String pageId;
	private String pageName;
	private String pageDesc;
	private String pageClass;	
	private Map<String, Mode> modesMap;
	
	public Page() {
		modesMap = new HashMap<String, Mode>(0);
	}
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageDesc() {
		return pageDesc;
	}
	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}
	public String getPageClass() {
		return pageClass;
	}
	public void setPageClass(String pageClass) {
		this.pageClass = pageClass;
	}
	public Map<String, Mode> getModesMap() {
		return modesMap;
	}
	public void setModesMap(Map<String, Mode> modesMap) {
		this.modesMap = modesMap;
	}
	
	public void addMode(Mode mode) {
		modesMap.put(mode.getModeId(), mode);
	}	
	
	public Mode getMode(String modeId) {
		return modesMap.get(modeId);
	}
	
}
