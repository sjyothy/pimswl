package com.avanza.pims.web.modes;

import java.io.File;

import org.apache.commons.digester.Digester;

public class PagesModesConfigManager {
	
	private static PagesModesConfig pagesModesConfig;
	private static Digester digester;
	
	static {
		pagesModesConfig = new PagesModesConfig();
		digester = new Digester();		
	}
	
	public static void loadPagesModesConfig(String configFilePath) throws Exception {
		
		PagesModesConfigManager.configureDigester();
		PagesModesConfigManager.configureParsingRules();
		PagesModesConfigManager.parseFile(configFilePath);				
	}
	
	private static void configureDigester() {
		digester.setValidating( false );
	}
	
	private static void configureParsingRules() {
		digester.addObjectCreate("pages-modes-config", PagesModesConfig.class);
		
		digester.addObjectCreate("pages-modes-config/page", Page.class);
		digester.addSetProperties("pages-modes-config/page", "pageId", "pageId");
		digester.addSetProperties("pages-modes-config/page", "pageName", "pageName");
		digester.addSetProperties("pages-modes-config/page", "pageDesc", "pageDesc");
		digester.addSetProperties("pages-modes-config/page", "pageClass", "pageClass");		
		digester.addSetNext("pages-modes-config/page", "addPage");
		
		digester.addObjectCreate("pages-modes-config/page/mode", Mode.class);
		digester.addSetProperties("pages-modes-config/page/mode", "modeId", "modeId");
		digester.addSetProperties("pages-modes-config/page/mode", "modeName", "modeName");
		digester.addSetProperties("pages-modes-config/page/mode", "modeDesc", "modeDesc");
		digester.addSetNext("pages-modes-config/page/mode", "addMode");
		
		digester.addObjectCreate("pages-modes-config/page/mode/control", Control.class);
		digester.addSetProperties("pages-modes-config/page/mode/control", "controlId", "controlId");
		digester.addSetProperties("pages-modes-config/page/mode/control", "controlName", "controlName");
		digester.addSetProperties("pages-modes-config/page/mode/control", "controlDesc", "controlDesc");
		digester.addSetProperties("pages-modes-config/page/mode/control", "rendered", "rendered");
		digester.addSetProperties("pages-modes-config/page/mode/control", "readOnly", "readOnly");
		digester.addSetProperties("pages-modes-config/page/mode/control", "disabled", "disabled");
		digester.addSetNext("pages-modes-config/page/mode/control", "addControl");
	}
	
	private static void parseFile(String configFilePath) throws Exception {
		File file = new File(configFilePath);
		PagesModesConfigManager.setPagesModesConfig( (PagesModesConfig) digester.parse(file) );		
	}

	public static PagesModesConfig getPagesModesConfig() {
		return pagesModesConfig;
	}

	public static void setPagesModesConfig(PagesModesConfig pagesModesConfig) {
		PagesModesConfigManager.pagesModesConfig = pagesModesConfig;
	}

	public static Digester getDigester() {
		return digester;
	}

	public static void setDigester(Digester digester) {
		PagesModesConfigManager.digester = digester;
	}

}
