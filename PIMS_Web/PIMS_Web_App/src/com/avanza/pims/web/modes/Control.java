package com.avanza.pims.web.modes;

public class Control {
	
	private String controlId;
	private String controlName;
	private String controlDesc;	
	private Boolean rendered;
	private Boolean readOnly;
	private Boolean disabled;
	
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlDesc() {
		return controlDesc;
	}
	public void setControlDesc(String controlDesc) {
		this.controlDesc = controlDesc;
	}
	public Boolean getRendered() {
		return rendered;
	}
	public void setRendered(Boolean rendered) {
		this.rendered = rendered;
	}
	public Boolean getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}	
	
}
