package com.avanza.pims.web.modes;

import java.util.HashMap;
import java.util.Map;

public class PagesModesConfig {
	
	private Map<String, Page> pagesMap;
	
	public PagesModesConfig() {
		pagesMap = new HashMap<String, Page>(0);
	}

	public Map<String, Page> getPagesMap() {
		return pagesMap;
	}

	public void setPagesMap(Map<String, Page> pagesMap) {
		this.pagesMap = pagesMap;
	}
	
	public void addPage(Page page) {
		pagesMap.put(page.getPageClass(), page);
	}
	
	public Page getPage(String pageClass) {
		return pagesMap.get(pageClass);
	} 
 
}
