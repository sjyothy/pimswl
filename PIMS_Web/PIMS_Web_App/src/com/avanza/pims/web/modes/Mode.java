package com.avanza.pims.web.modes;

import java.util.HashMap;
import java.util.Map;

public class Mode {

	private String modeId;
	private String modeName;
	private String modeDesc;
	private Map<String, Control> controlsMap;
	
	public Mode() {
		controlsMap = new HashMap<String, Control>(0);
	}
	
	public String getModeId() {
		return modeId;
	}
	public void setModeId(String modeId) {
		this.modeId = modeId;
	}
	public String getModeName() {
		return modeName;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	public String getModeDesc() {
		return modeDesc;
	}
	public void setModeDesc(String modeDesc) {
		this.modeDesc = modeDesc;
	}
	public Map<String, Control> getControlsMap() {
		return controlsMap;
	}
	public void setControlsMap(Map<String, Control> controlsMap) {
		this.controlsMap = controlsMap;
	}
	
	public void addControl( Control control ) {
		controlsMap.put(control.getControlId(), control);
	}
	
	public Control getControl( String controlId ) {
		return controlsMap.get(controlId);
	}
}
