/**
 * 
 */
package com.avanza.pims.web.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;

/**
 * @author Hammad Afridi
 *
 */
public class SecurityTag extends TagSupport
{
    private static final Logger logger = Logger
                            .getLogger(SecurityTag.class);

    private String action=null;
    private String secondAction=null;
    private String thirdAction=null;
    private String screen=null;

    
    public void setAction(String action)
    {
    this.action = action;
    }
    
    public String getAction()
    {
    return this.action;
    }

    public void setScreen(String screen)
    {
    this.screen = screen;
    }
    
    public String getScreen()
    {
    return this.screen;
    }

    
    @Override
    public int doEndTag() throws JspException
    {
         
        return TagSupport.SKIP_BODY;
    }

    @Override
    public int doStartTag() throws JspException
    {
    
        
        //logger.logInfo("In Security Tag");
        HttpServletRequest request=(HttpServletRequest) pageContext.getRequest();
        HttpSession httpSession=request.getSession();
        try
        {
        HttpServletResponse response=(HttpServletResponse) pageContext.getResponse();
        String contextPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
        

        UserDbImpl user = (UserDbImpl)pageContext.getSession().getAttribute(WebConstants.USER_IN_SESSION);

         
        if (user!=null && user.getPermission(this.screen)!=null)
        {
        	if (this.action.trim().equalsIgnoreCase("view") && user.getPermission(this.screen)!=null)
        	{//for view 
       		return TagSupport.EVAL_BODY_INCLUDE;
        	}	
        	else if (this.action.trim().equalsIgnoreCase("create") && user.getPermission(this.screen).getCanCreate())
            {
            return TagSupport.EVAL_BODY_INCLUDE;
            }
            else if (this.action.trim().equalsIgnoreCase("delete") && user.getPermission(this.screen).getCanDelete())
                return TagSupport.EVAL_BODY_INCLUDE;
            else if (this.action.trim().equalsIgnoreCase("update") && user.getPermission(this.screen).getCanUpdate())
                return TagSupport.EVAL_BODY_INCLUDE;
            
            
        return TagSupport.SKIP_BODY;
        }
        
        
        
        }
        catch(Exception ioe)
        {
        logger.logError("Error occured in Security Tag");
        }

    return SKIP_BODY;
    }

  
    

}
