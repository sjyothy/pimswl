/**
 * 
 */
package com.avanza.pims.web.tags;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.menu.MenuBuilder;
import com.avanza.core.menu.MenuItem;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.pims.web.WebConstants;



/**
 * @author Hammad Afridi
 *
 */
public class SecureMenuDisplayTag extends TagSupport {

	private static final int MAX_MENU_ITEMS =13;

	private String action=null;
    private String secondAction=null;
    private String thirdAction=null;
    
    private static final Logger logger = Logger
                            .getLogger(SecureMenuDisplayTag.class);
    @Override
    public int doEndTag() throws JspException
    {
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	boolean isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");

        List<MenuItem> menuList = MenuBuilder.getMenu("PIMS").getMenuItems();
    
        HttpServletResponse response=(HttpServletResponse) pageContext.getResponse();
        String contextPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
        
        UserDbImpl user = (UserDbImpl)pageContext.getSession().getAttribute(WebConstants.USER_IN_SESSION);
        StringBuffer menuHtml = new StringBuffer("");
        short menuSize = (short) menuList.size();
        int count  = 0;
        
        if (isEnglishLocale ) {

        	//Adding the scroll Up button
        	menuHtml.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
            menuHtml.append("<tr valign=\"top\">");
            menuHtml.append("<td width=\"93%\">");
            menuHtml.append("<div "+
            		//"onclick=\"javascript:do_scroll_Up('menudiv',5,450,14)\"" +
            		"onmousedown=\"javascript:startScrollUp('menudiv')\" "+
            		"onmouseup=\"javascript:stopScroll()\""+
            		"class=\"MENU_TOP_SCROLL\">&nbsp;</div>");
            menuHtml.append("</td></tr>");
        	menuHtml.append("</table>");
        	menuHtml.append("<div id=\"menudiv\" style=\"width:100%;overflow:hidden;height:450px;\">");
        	//Ended Scroll Up Button
        } else  {
        	menuHtml.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
            menuHtml.append("<tr valign=\"top\">");
            menuHtml.append("<td width=\"93%\">");
            menuHtml.append("<div "+
            		//"onclick=\"javascript:do_scroll_Up('menudiv',5,450,14)\" "+
            		"onmousedown=\"javascript:startScrollUp('menudiv')\" "+
            		"onmouseup=\"javascript:stopScroll()\""+
            		"class=\"MENU_TOP_SCROLL\">&nbsp;</div>");
            menuHtml.append("</td></tr>");
        	menuHtml.append("</table>");
        	menuHtml.append("<div id=\"menudiv\" style=\"margin-right:5px;width:100%;overflow:hidden;height:450px;\">");
        }
        for (MenuItem mainMenu : menuList) {
           
            Collection<MenuItem> subMenus = mainMenu.getSubMenusList();   
            count++;
            
            if(isEnglishLocale) {
            	 menuHtml.append("<table width=\"15%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
	             menuHtml.append("<!--Menu Item " +(count) + " -->");
	             menuHtml.append("<tr valign=\"top\">");
	             menuHtml.append("<td width=\"93%\"><table width=\"49%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td onClick=\"showhide("+(count)+","+menuSize+")\" width=\"82%\" background=\""+contextPath+"/resources/images/Panel/btn_top.png\" style=\"cursor:pointer;background-repeat:no-repeat;#	background: none;#	filter:progid:DXimageTransform.Microsoft.AlphaimageLoader(src='../resources/images/Panel/btn_top.png', sizingMethod='image');\">");
	             menuHtml.append("<table width=\"178pt\" height=\"auto\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td width=\"24\" align=\"right\"><img style=\"vertical-align: bottom;margin-top:0px;margin-left:7px;#margin-left:10px;\" src=\""+contextPath+ mainMenu.getIcon() + "\" ></td>");
	             String str = "";
	             String str1 = ""; 
	             str = mainMenu.getPrimaryName();
	             if (str.length()>11) {
	            	 str1 = str.substring(0, 11)+"."; 	 
	                 menuHtml.append("<td width=\"79\"><span class=\"style1\" title=\""+str+"\" style=\"font-weight:bold;font-size:9px;display:block;vertical-align: bottom;margin-top:4px;padding-left:3px;\">"+str1+"</span></td>");
	             } else {
	                 menuHtml.append("<td width=\"79\"><span class=\"style1\" title=\""+str+"\" style=\"font-weight:bold;font-size:9px;display:block;vertical-align: bottom;margin-top:4px;padding-left:3px;\">"+str+"</span></td>");
	             }
	             menuHtml.append("<td width=\"21\">"); 
	             menuHtml.append("<div id=\"up_"+(count)+"\" style=\"display:block;padding-right:7px;\" border=\"0\">");
	             menuHtml.append("<a href=\"#\"><img src=\""+contextPath+"/resources/images/Expand.jpg\" style=\"vertical-align: bottom;width:11pt;height:11pt;\" border=\"0\"></a>");
	             menuHtml.append("</div>");
	             menuHtml.append("<div id=\"down_"+(count)+"\" style=\"display:none;padding-right:7px;\" border=\"0\">");
	             menuHtml.append("<a href=\"#\">");
	             menuHtml.append("<img src=\""+contextPath+"/resources/images/Collapse-state.jpg\" style=\"vertical-align: bottom;width:11pt;height:11pt;\" border=\"0\">");
	             menuHtml.append("</a>");
	             menuHtml.append("</div>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");        
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("<tr id=\"div_"+(count)+ "\" valign=\"top\">");
	           	 menuHtml.append("<td colspan=\"2\" width=\"178px\">");
	             menuHtml.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:1px;width:92%;\">");
	             int subMenuCount = 0;
	             for (MenuItem subMenu : subMenus) {
	            	 subMenuCount++;
	                 String url = contextPath + subMenu.getActionUri();	                 
	                 if ( url.contains("?") )
	                	 url = url.concat("&selectedMenu="+count+"_"+subMenuCount);
	                 else
	                	 url = url.concat("?selectedMenu="+count+"_"+subMenuCount);	                	 
	                 menuHtml.append("<tr border=\"0\">");
	                 menuHtml.append("<td cellspacing=\"0\" cellpadding=\"0\" background=\""+contextPath+"/resources/images/Left%20Panel/Center.jpg\" style=\"background-repeat:repeat-y\" align=\"left\"><img style=\"margin-left:13px;\" src=\""+contextPath+"/resources/images/tree/join.gif\"><img style=\"vertical-align:top;\" src=\""+contextPath+"/resources/images/folder.gif\"><a id=\""+count+"_"+subMenuCount+"\" href=\""+url+"\" style=\"vertical-align:5px;margin-left:4px;text-decoration:none;font-color:#ffffff\" class=\"style1\" >"+subMenu.getPrimaryName()+"</a></td>");
	                 menuHtml.append("</tr>");
	             } 
	             menuHtml.append("<tr>");
	             menuHtml.append("<td  cellspacing=\"0\" cellpadding=\"0\" ><div style=\"margin-left:-1px;\" class=\"MENU_BOTTOM_PART\" height=\"14\">&nbsp;</div></td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td>");
	             menuHtml.append("<span id=\"bottom_"+(count)+"\"><div class=\"MENU_BOTTOM_PART\" width=\"164\" height=\"14\">&nbsp;</div></div>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
             } else { //Arabic Locale  
            	 
            	 menuHtml.append("<table width=\"15%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
	             menuHtml.append("<!--Menu Item " +(count) + " -->");
	             menuHtml.append("<tr valign=\"top\">");
	             menuHtml.append("<td width=\"93%\"><table width=\"49%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td onClick=\"showhide("+(count)+","+menuSize+")\" width=\"82%\" height=\"15%\"  background=\""+contextPath+"/resources/images/Panel/btn_top.png\" style=\"cursor:pointer;background-repeat:no-repeat;#	background: none;#	filter:progid:DXimageTransform.Microsoft.AlphaimageLoader(src='../resources/images/Panel/btn_top.png', sizingMethod='image');\">");
	             menuHtml.append("<table height=\"auto\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:170px;#width:170px;\">");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td width=\"24\" align=\"left\"><img style=\"vertical-align: top;margin-right:5px;margin-top:-1px;height:15px;\" src=\""+contextPath+ mainMenu.getIcon() + "\" ></td>");
	             String str = "";
	             String str1 = ""; 
	             str = mainMenu.getSecName();
	        //     if (str.length()>11) {
	       //     	 str1 = str.substring(0, 11)+"."; 	 
	      //          menuHtml.append("<td width=\"79\"><span class=\"style1\" title=\""+str+"\" style=\"text-align:right;font-weight:bold;font-size:10px;display:block;vertical-align: bottom;padding-right:3px;font-family:Tahoma;\">"+str1+"</span></td>");
	       //      } else {
	                menuHtml.append("<td width=\"79\"><span class=\"style1\" title=\""+str+"\" style=\"text-align:right;font-weight:bold;font-size:10px;display:block;vertical-align: bottom;padding-right:3px;font-family:Tahoma;white-space:nowrap;\">"+str+"</span></td>");
	       //      }
	             menuHtml.append("<td width=\"21\">"); 
	             menuHtml.append("<div id=\"up_"+(count)+"\" style=\"display:block;padding-left:7px;#margin-top:-2px;\" border=\"0\">");
	             menuHtml.append("<a href=\"#\"><img src=\""+contextPath+"/resources/images/Expand.jpg\" style=\"vertical-align: bottom;width:11pt;height:11pt;margin:0px;\" border=\"0\"></a>");
	             menuHtml.append("</div>");
	             menuHtml.append("<div id=\"down_"+(count)+"\" style=\"display:none;#margin-top:-2px;\" border=\"0\">");
	             menuHtml.append("<a href=\"#\">");
	             menuHtml.append("<img src=\""+contextPath+"/resources/images/Collapse-state.jpg\" style=\"vertical-align: bottom;width:11pt;height:11pt;margin-left:7px;\" border=\"0\">");
	             menuHtml.append("</a>");
	             menuHtml.append("</div>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");        
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("<tr id=\"div_"+(count)+ "\" valign=\"top\">");
	           	 menuHtml.append("<td colspan=\"2\" width=\"178px\">");
	             menuHtml.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-right:6px;width:96%;#margin-right:0px;#margin-left:1px;#width:93%;\">");
	             int subMenuCount = 0;
	             for (MenuItem subMenu : subMenus) {
	            	 subMenuCount++;
	                 String url = contextPath + subMenu.getActionUri();
	                 if ( url.contains("?") )
	                	 url = url.concat("&selectedMenu="+count+"_"+subMenuCount);
	                 else
	                	 url = url.concat("?selectedMenu="+count+"_"+subMenuCount);
	                 menuHtml.append("<tr border=\"0\">");
	                 try{
	                	 menuHtml.append("<td cellspacing=\"0\" cellpadding=\"0\" background=\""+contextPath+"/resources/images/Left%20Panel/Center.jpg\" style=\"background-repeat:repeat-y\" align=\"right\"><img style=\"margin-right:6px;#margin-right:-1px;\" src=\""+contextPath+"/resources/images/ar/tree/join_ar.GIF\"><img style=\"vertical-align:top;\" src=\""+contextPath+"/resources/images/folder.gif\"><a id=\""+count+"_"+subMenuCount+"\" href=\""+url+"\" style=\"vertical-align:5px;margin-right:6px;text-decoration:none;font-color:#ffffff;#padding-left:6px;font-family:Arial;font-size:12px\" class=\"style1\" >"+subMenu.getSecName()+"</a></td>");
	                 }catch(Exception e){}
	                 menuHtml.append("</tr>");
	             } 
	             menuHtml.append("<tr>");
	             menuHtml.append("<td  cellspacing=\"0\" cellpadding=\"0\" ><div style=\"margin-right:-1px;width:101%;#width:100%;#margin-left:-1px;#margin-right:0px;\" class=\"MENU_BOTTOM_PART\" height=\"14\">&nbsp;</div></td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("<tr>");
	             menuHtml.append("<td>");
	             menuHtml.append("<span id=\"bottom_"+(count)+"\"><div class=\"MENU_BOTTOM_PART\" width=\"164\" height=\"14\">&nbsp;</div></div>");
	             menuHtml.append("</td>");
	             menuHtml.append("</tr>");
	             menuHtml.append("</table>");
             }//else for arabic	 
        }
        
        if (isEnglishLocale ) {
        	//Adding the scroll Down button
	        menuHtml.append("</div>");
        	menuHtml.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
            menuHtml.append("<tr valign=\"top\">");
            menuHtml.append("<td width=\"93%\">");
            menuHtml.append("<div class=\"MENU_BOTTOM_SCROLL\" " +
            		//"onclick=\"javascript:do_scroll_Down('menudiv',5,450,44)\""+
            		"onmousedown=\"javascript:startScrollDown('menudiv')\" "+
            		"onmouseup=\"javascript:stopScroll()\""+
            		">&nbsp;</div>");
            menuHtml.append("</td></tr>");
        	menuHtml.append("</table>");
        	//Ended Scroll Down Button
        } else  {
        	//Adding the scroll Down button
	        menuHtml.append("</div>");
        	menuHtml.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:1px;\">");
            menuHtml.append("<tr valign=\"top\">");
            menuHtml.append("<td width=\"93%\">");
            menuHtml.append("<div id=\"MENU_BOTTOM_SCROLL\" class=\"MENU_BOTTOM_SCROLL\" "+
            		//"onclick=\"javascript:do_scroll_Down('menudiv',5,450,44)\"" +
            		"onmousedown=\"javascript:startScrollDown('menudiv')\" "+
            		"onmouseup=\"javascript:stopScroll()\""+
            		">&nbsp;</div>");
            menuHtml.append("</td></tr>");
        	menuHtml.append("</table>");
        	//Ended Scroll Down Button
        }
        menuHtml.append("<SCRIPT language=javascript src=\""+contextPath+"/resources/jscripts/leftmenu.js\"></SCRIPT>");
        menuHtml.append("<script type=\"text/javascript\">showhide(1,"+menuSize+");</script>");
        
        if ( pageContext.getSession().getAttribute("selectedMenu") != null )
        	menuHtml.append("<script type=\"text/javascript\">showhide("+ ((String) pageContext.getSession().getAttribute("selectedMenu")).split("_")[0] + "," + menuSize + "); selectMenuItem('"+(String) pageContext.getSession().getAttribute("selectedMenu")+"');</script>");
        
        try {
        	response.getWriter().print(menuHtml);
        } catch(IOException ioe){
        	System.out.print("ioe");
        }
        return TagSupport.EVAL_PAGE;
    }

    @Override
    public int doStartTag() throws JspException
    {
        HttpServletRequest request=(HttpServletRequest) pageContext.getRequest();
        HttpSession httpSession=request.getSession();
        
        if(ApplicationContext.getContext().get(WebContext.class) == null)
        	ApplicationContext.getContext().add(WebContext.class.getName(), new WebContextImpl(httpSession));
    
    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<in the tag>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");    
    return EVAL_PAGE;
    }

  
    

}
