package com.avanza.pims.web.tags;


import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.webapp.UIComponentTag;


public class JSFPagingTag extends UIComponentTag {
    private String showpages;
    private String dataTableId;
    private String styleClass;
    private String selectedStyleClass;
    
    public String getRendererType() { return "com.avanza.pims.web.tags.JSFPagingTag"; }
    public String getComponentType() { return "javax.faces.Output"; }
    
    public void setShowpages(String newValue) { showpages = newValue; }
    public void setDataTableId(String newValue) { dataTableId = newValue; }
    public void setStyleClass(String newValue) { styleClass = newValue; }
    public void setSelectedStyleClass(String newValue) {
        selectedStyleClass = newValue;
    }
    
    public void setProperties(UIComponent component) {
        super.setProperties(component);
        if (component == null) return;
        
        if (isValueReference(showpages))
        {
          FacesContext context = FacesContext.getCurrentInstance();
          Application app = context.getApplication();
          ValueBinding vb = app.createValueBinding(showpages);
          component.setValueBinding("showpages", vb);                  
        }
        else 
          component.getAttributes().put("showpages", showpages);


        if (isValueReference(dataTableId))
        {
          FacesContext context = FacesContext.getCurrentInstance();
          Application app = context.getApplication();
          ValueBinding vb = app.createValueBinding(dataTableId);
          component.setValueBinding("dataTableId", vb);                  
        }
        else 
          component.getAttributes().put("dataTableId", dataTableId);
        if (isValueReference(styleClass))
        {
          FacesContext context = FacesContext.getCurrentInstance();
          Application app = context.getApplication();
          ValueBinding vb = app.createValueBinding(styleClass);
          component.setValueBinding("styleClass", vb);                  
        }
        else 
          component.getAttributes().put("styleClass", styleClass);
        if (isValueReference(selectedStyleClass))
        {
          FacesContext context = FacesContext.getCurrentInstance();
          Application app = context.getApplication();
          ValueBinding vb = app.createValueBinding(selectedStyleClass);
          component.setValueBinding("selectedStyleClass", vb);                  
        }
        else 
          component.getAttributes().put("selectedStyleClass", selectedStyleClass);

     
    //    component.setValueExpression("styleClass", styleClass);
    
    }
    
    public void release() {
        super.release();
        showpages = null;
        dataTableId = null;
        styleClass = null;
        selectedStyleClass = null;
    }
}