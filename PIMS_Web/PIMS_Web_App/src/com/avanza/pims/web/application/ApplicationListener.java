package com.avanza.pims.web.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.config.ReportConfig;
import com.avanza.pims.report.manager.AbstractReportManager;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.scheduler.SchedulerManager;
import com.avanza.pims.scheduler.TaskNotificationJob;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.modes.PagesModesConfigManager;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;

    import java.sql.Connection;
    import java.sql.DriverManager;
import java.sql.SQLException;


public class ApplicationListener implements ServletContextListener{
	Logger logger = Logger.getLogger(ApplicationListener.class);
	
    public ApplicationListener() {
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try
        {
            String realPath = servletContextEvent.getServletContext().getRealPath("/") ;
            logger.logInfo("realPath :%s",realPath );
//          String avConfFilePath=realPath+ "//WEB-INF//av-conf.xml";
            String avConfFilePath=realPath+ "WEB-INF\\av-conf.xml";
            
            logger.logInfo("avConfFilePath:%s",avConfFilePath);
        
//          String messageResourcePathEn = realPath+ "//WEB-INF//Messages.properties";        
            String messageResourcePathEn = realPath+"WEB-INF\\classes\\com\\avanza\\pims\\web\\messageresource\\Messages.properties";
            logger.logInfo("messageResourcePathEn:%s",messageResourcePathEn);
//          String messageResourcePathAr = realPath+ "//WEB-INF//Messages_ar.properties";
            String messageResourcePathAr =realPath +"WEB-INF\\classes\\com\\avanza\\pims\\web\\messageresource\\Messages_ar.properties";
            logger.logInfo("messageResourcePathAr:%s",messageResourcePathAr);
            
//          String reportConfigFilePath = realPath + "//WEB-INF//reports-config.properties";
            String reportConfigFilePath = realPath+ "WEB-INF\\reports-config.properties";
            logger.logInfo("reportConfigFilePath:%s",reportConfigFilePath);
            
//          String pickListMappingPropertiesFilePath= realPath+ "//WEB-INF//picklist-mapping.properties"
            String pickListMappingPropertiesFilePath= realPath+"WEB-INF\\picklist-mapping.properties";
            logger.logInfo("pickListMappingPropertiesFilePath:%s",pickListMappingPropertiesFilePath);
            
//          String pagesModesConfigFilePath = realPath+ "//WEB-INF//pages-modes-config.xml";
            String pagesModesConfigFilePath = realPath+ "WEB-INF\\pages-modes-config.xml";
            logger.logInfo("pagesModesConfigFilePath:%s",pagesModesConfigFilePath);
        
//           String soaConfigPath=servletContextEvent.getServletContext().getRealPath("/")+ "//WEB-INF//config.properties";    
            //for windowds
            String soaConfigPath =realPath+ "WEB-INF\\config.properties";
            logger.logInfo("soaConfigPath:%s",soaConfigPath);
            
            loadSystemParameters(realPath);
            ApplicationLoader.load(avConfFilePath);   
            loadApplicationData(servletContextEvent);
            loadReportConfigurations(servletContextEvent,reportConfigFilePath );
            loadPickList(servletContextEvent,pickListMappingPropertiesFilePath);
            loadPagesMode(servletContextEvent, pagesModesConfigFilePath);
            loadReportMessageResrources(messageResourcePathAr,messageResourcePathEn);
    //      loadQuartzSchedules();
            loadBPMWorkListClient(servletContextEvent,messageResourcePathAr,messageResourcePathEn,soaConfigPath);
        
        } catch (Exception exp) {
            logger.LogException("Failed to initialize Context:", exp);
        }

    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationLoader.destroy();
        try{
        	SchedulerManager.stopScheduler();
	}
        catch (Exception e){
			logger.LogException(" Exception occurred in SchedulerManager ---> ", e);
        }
    }
    
    private void loadApplicationData(ServletContextEvent event)throws Exception {
    	
    	
    		UtilityServiceAgent serviceAgent = new UtilityServiceAgent();
    		List<DomainTypeView> list = serviceAgent.getAllDomainTypeData();
    		ServletContext context = event.getServletContext();

    		context.setAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST, list);
    		
    		List<Long> paymentScheduleBouncedReplacedExemptedStatusList = new ArrayList<Long>();  
    		
    		List<DomainDataView>paymentStatusList = serviceAgent.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_STATUS);
    		
    		for (DomainDataView domainDataView : paymentStatusList) {
    			if (
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_RETURN) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_REPLACED) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_COLLECTED_RETURN) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED_APPROVAL) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED_CANCEL) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED_WAITING_FOR_APPROVAL) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_RETURNED) ||
    				domainDataView.getDataValue().equals(WebConstants.PAYMENT_SCHEDULE_STATUS_RECEIVED_FROM_BANK)
    				) {
    				paymentScheduleBouncedReplacedExemptedStatusList.add(domainDataView.getDomainDataId());
    			}
    			
    		}
    		
    		// Added bounced payment schedule list
    		context.setAttribute(WebConstants.ServletContext.PAYMENT_SCHEDULE_BOUNCED_REPLACED_ExEMPTED_STATUS_LIST, paymentScheduleBouncedReplacedExemptedStatusList);
    		
    }
    
    
    private void loadReportConfigurations(ServletContextEvent servletContextEvent,String reportConfigFilePath) throws Exception{
    
            logger.logInfo("About to load Reports Configurations");
            
            //                AbstractReportManager.reportBaseFolderName = servletContextEvent.getServletContext().getRealPath("/") + "//reports";
            //                AbstractReportManager.reportBaseFolderName = servletContextEvent.getServletContext().getRealPath("/") + "reports";
            logger.logInfo("report file path: %s" , reportConfigFilePath);
            //                logger.logInfo("report folder path: "+ AbstractReportManager.reportBaseFolderName);
            ReportConfig.load(reportConfigFilePath);
            logger.logInfo("Reports Configurations Loaded Successfully");
    
    }

    private void loadSystemParameters(String realPath) throws Exception {
            logger.logInfo("--loading system paramters Real Path:%s--",realPath );
            SystemParameters systemParameters = SystemParameters.getInstance();
            systemParameters.loadParametersFromFile(realPath);
            logger.logInfo("--loading system paramters loaded Path:%s--" ,realPath);
        
    }

    private void loadPagesMode(ServletContextEvent servletContextEvent, String pagesModesConfigFilePath) {
        try {
            logger.logInfo("About to load Pages Modes Configurations");
            
            PagesModesConfigManager.loadPagesModesConfig(pagesModesConfigFilePath);
            logger.logInfo("Pages Modes Configurations Loaded Successfully");
        } catch (Exception e) {
            logger.LogException(" Exception occurred while loading Pages Modes Configurations ", e);
        }
    }

    private void loadPickList(ServletContextEvent servletContextEvent, String pickListMappingPropertiesFilePath) throws Exception{

            File file =new File(pickListMappingPropertiesFilePath);
            logger.logInfo("<<<<<<<<<<<<<<<< File Created :" + file + " >>>>>>>>>>>>>>>>>>>>" + file.getPath());
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            logger.logInfo("<<<<<<<<<<<<<<<< File InputStream Created :" + file + " >>>>>>>>>>>>>>>>>>>>");
            Properties picklistMapping = new Properties();
            PickListManager.load();
            picklistMapping.load(fis);
            servletContextEvent.getServletContext().setAttribute("picklistMapping", picklistMapping);
            logger.logInfo("<<<<<<<<<<<<<<<< Properties Loaded :" + file + " >>>>>>>>>>>>>>>>>>>>" + file.getPath());

    }

    private void loadReportMessageResrources(String messageResourcePathAr,String messageResourcePathEn) throws Exception{
        
            logger.logInfo("About to load Reports Message Resource");
            //			String reportMessageResourceEnFilePath = servletContextEvent.getServletContext().getRealPath("/") + "WEB-INF\\classes\\com\\avanza\\pims\\web\\messageresource\\Messages.properties";
            //			String reportMessageResourceArFilePath = servletContextEvent.getServletContext().getRealPath("/") + "WEB-INF\\classes\\com\\avanza\\pims\\web\\messageresource\\Messages_ar.properties";
            ReportMessageResource.load(messageResourcePathEn, messageResourcePathAr);
            logger.logInfo("Reports Message Resource Loaded Successfully");
        
    }

    private void loadQuartzSchedules() {
        try {
            SchedulerManager.startScheduler();
            SchedulerManager.loadJobSchedule();
            SchedulerManager.scheduleJobs();
        } catch (Exception e) {
            logger.LogException(" Exception occurred in SchedulerManager ---> ", e);
        }
    }

    private void loadBPMWorkListClient(ServletContextEvent servletContextEvent,String  messageResourcePathAr,String messageResourcePathEn,String soaConfigPath) {
        try {
            logger.logInfo("About to load bpmWorkListClient");
            
            ApplicationContext.getContext().add("SOA_CONFIG_PATH", soaConfigPath);
            soaConfigPath = (String) ApplicationContext.getContext().get("SOA_CONFIG_PATH");
            TaskNotificationJob.bpmWorkListClient = new BPMWorklistClient(soaConfigPath, messageResourcePathEn, messageResourcePathAr);
        } catch (Exception exp) {
            logger.LogException("Failed to intialize bpmWorkListClient  ", exp);
        }
    }
}
