package com.avanza.pims.web.application.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.avanza.core.CoreException;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.pims.web.WebConstants;

/**
 * Add the request parameter cache=false for disabling cached based headers.
 * @author 
 *
 */
public class PimsFilter implements Filter {
	private static final Logger logger = Logger.getLogger(PimsFilter.class);
	private HashSet<String> insecurePages = null;
	private String logout = "";

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		//logger.logDebug("||||| On request filter |||||");
		
		try{
			applyFilters(request, response, filterChain);
		} catch(Throwable t){
			logger.LogException("An Unhandled Exception caught at PIMS Filter.", t);
			if (request instanceof HttpServletRequest) 
				((HttpServletRequest)request).getSession().setAttribute("exceptionMessage", t);
			throw new CoreException("An Unhandled Exception caught at PIMS Filter.",t);
		}
	}

	private void applyFilters(ServletRequest request, ServletResponse response,	FilterChain filterChain) throws ServletException, IOException {
		
		ApplicationContext applicationContext = ApplicationContext.getContext();
		
		if (request instanceof HttpServletRequest) {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;

			req.setCharacterEncoding("UTF-8");
			req.getSession().removeAttribute("exceptionMessage");
			
//			logger.logDebug("Request received: " + req.getRequestURI() + " from user with session id: " + req.getRequestedSessionId());

			String uri = req.getRequestURI();
			String path = req.getContextPath();
			String targetURI = uri.substring(path.length(), uri.length());
			String target = targetURI.toLowerCase();
			
			if (req.getSession().getAttribute(WebConstants.USER_IN_SESSION) == null) {
				logger.logInfo("PIMSFILter:User with session id: "	+ req.getRequestedSessionId() + " not authenticated yet.");

				if (!insecurePages.contains(targetURI)) {
					forwardToLogin(req, res);
					return;
				}
			} 
			else if (logout.equals(targetURI)) 
			{
				req.getSession().invalidate();
				logger.logDebug("PIMSFILter:session out - need to login again");
				forwardToLogin(req, res);
				return;
			} else {
				User sessionUser = (UserDbImpl) req.getSession().getAttribute(WebConstants.USER_IN_SESSION);
				if (sessionUser == null) {
		             logger.logDebug("PIMSFILter:User is not found in session, Login Required Again!!");
					forwardToLogin(req, res);
					return;
				}
				
				WebContext ctx = new WebContextImpl(req.getSession());
				RequestContext reqctx = new RequestContextImpl(req);
				ApplicationContext.getContext().add(WebContext.class.getName(), ctx);
				ApplicationContext.getContext().add(RequestContext.class.getName(), reqctx);
				ApplicationContext.getContext().add(WebConstants.USER_IN_SESSION, sessionUser);
				LocaleInfo locale = (LocaleInfo)ctx.getAttribute(CoreConstants.CurrentLocale);
				if (locale != null)
					req.setCharacterEncoding(locale.getEncoding());

			//	logger.logDebug("Processing request for user with login id: " + sessionUser.getLoginId());
			}

			processRequest(req, res, filterChain);
		}
		
		applicationContext.release();
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		
		addCustomHeaders(request, response);
		updateMenuState( request, response );
		filterChain.doFilter(request, response);
		
	}
	
	 
	
    private void updateMenuState(HttpServletRequest request, HttpServletResponse response) {
    	try 
    	{
	    	if ( request.getQueryString() != null ) 
	    	{
	    		String[] queryStrings = request.getQueryString().split("&");
	    		String lastQueryString = queryStrings[ queryStrings.length -1 ];
	    		String lastQueryStringKey = lastQueryString.split("=")[0];
	    		String lastQueryStringValue = lastQueryString.split("=")[1];
	    		if ( lastQueryStringKey.equalsIgnoreCase("selectedMenu") )    		
	    			request.getSession().setAttribute("selectedMenu", lastQueryStringValue);    		
	    	}
    	}
    	catch (Exception e) 
    	{
			 
		}
	}

	private void addCustomHeaders(HttpServletRequest servRequest, HttpServletResponse httpServletResponse) {
        LocaleInfo locale = (LocaleInfo) servRequest.getSession().getAttribute(CoreConstants.CurrentLocale);
        
        if (locale != null) {
            httpServletResponse.setCharacterEncoding(locale.getEncoding());
            httpServletResponse.setLocale(locale.getLocale());
           	//disabled by Kazim as javascript back probably not working bcuz of it.
//            String cache = (String) servRequest.getParameter("cache");
            /*if ("false".equalsIgnoreCase(cache))*/{
                httpServletResponse.setHeader("pragma", "no-cache");
                httpServletResponse.setHeader("cache-control", "no-cache");
                httpServletResponse.setHeader("cache-control", "no-store");
                httpServletResponse.setHeader("Cache-Control", "private"); // HTTP 1.1
                httpServletResponse.setHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 
                httpServletResponse.setHeader("expires", "0");
            }
        }
    }

	private void forwardToLogin(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		RequestDispatcher rd = req.getRequestDispatcher("/pages/index.jsf");
		rd.forward(req, res);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		logger.logDebug("Initializing PIMS Filter....");
		
		initConfig(filterConfig);
		logger.logInfo("Filter Started.");
	}

	private void initConfig(FilterConfig filterConfig) {
		
		String strInsecurePages = filterConfig.getInitParameter("insecurePages");
		logout = filterConfig.getInitParameter("logout");
		
		StringTokenizer tokenizer = new StringTokenizer(strInsecurePages, ",",false);
		insecurePages = new HashSet<String>(tokenizer.countTokens());

		while (tokenizer.hasMoreTokens()) {
			insecurePages.add(tokenizer.nextToken().trim());
		}
	}
}
