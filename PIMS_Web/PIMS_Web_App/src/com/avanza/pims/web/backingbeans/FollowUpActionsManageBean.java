package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.FollowUpActionsView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.ui.util.ResourceUtil;

public class FollowUpActionsManageBean extends AbstractController
{
	
	
	FacesContext context ;
	Map<String, Object> sessionMap;
	Map<String,Object> viewMap ;
	private static Logger logger = Logger.getLogger(FollowUpActionsManageBean.class);;
	
	private HtmlInputText txt_followUpActionDescEn;
	private HtmlInputText txt_followUpActionDescAr;
	private javax.faces.component.html.HtmlSelectOneMenu cmb_followUpStatus;
	private ArrayList<String> errorMessages;
	private ArrayList<String> successMessages;
	private FollowUpActionsView followUpActionsView ;
	
	@SuppressWarnings("unchecked")
	public FollowUpActionsManageBean()
	{
		
		context = FacesContext.getCurrentInstance();
		sessionMap = context.getExternalContext().getSessionMap();
		viewMap = (Map<String,Object>)FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		
		txt_followUpActionDescEn = new HtmlInputText();
		txt_followUpActionDescAr = new HtmlInputText();
		cmb_followUpStatus = new javax.faces.component.html.HtmlSelectOneMenu();
		
		
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		followUpActionsView =new FollowUpActionsView();
	}
	
	@Override
	public void init()
	{
		super.init();
		if(!isPostBack())
		{
			if(sessionMap.containsKey(WebConstants.FollowUp.FOLLOW_UP_VIEW) && sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW)!=null)
			{
				followUpActionsView =(FollowUpActionsView) sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
				sessionMap.remove(WebConstants.FollowUp.FOLLOW_UP_VIEW);
				viewMap.put(WebConstants.FollowUp.FOLLOW_UP_VIEW, followUpActionsView);
				beforeApplyRequestValues();
			}
		}
		
	}
	@Override 
	public void     beforeApplyRequestValues() 
	{
	 super.beforeApplyRequestValues();
	 if(viewMap.containsKey(WebConstants.FollowUp.FOLLOW_UP_VIEW))
		{
			followUpActionsView = (FollowUpActionsView)viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
			txt_followUpActionDescEn.setValue(followUpActionsView.getDescriptionEn());
			txt_followUpActionDescAr.setValue(followUpActionsView.getDescriptionAr());
			if(followUpActionsView.getFollowUpStatus()!=null)
			 cmb_followUpStatus.setValue(followUpActionsView.getFollowUpStatus().toString());
			if(followUpActionsView.getIsSystem()!=null && followUpActionsView.getIsSystem().compareTo(1L)==0)
				cmb_followUpStatus.setDisabled(true);
		}
	}
	@Override
	public void prerender()
	{
		super.prerender();
	}
	public boolean getIsArabicLocale()
	{
		return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	private boolean hasErrors()
	{
		boolean hasErrors=false;
		if(txt_followUpActionDescEn.getValue()==null || txt_followUpActionDescEn.getValue().toString().trim().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("followupaction.msg.followUpActionEnRequired"));
			hasErrors=true;
		}
		if(txt_followUpActionDescAr.getValue()==null || txt_followUpActionDescAr.getValue().toString().trim().length()<=0)
		{
		    errorMessages.add(ResourceUtil.getInstance().getProperty("followupaction.msg.followUpActionArRequired"));
		    hasErrors = true;
		}
		return hasErrors;
	}
	public void reset()
	{
		if(viewMap.containsKey(WebConstants.FollowUp.FOLLOW_UP_VIEW))
		{
			followUpActionsView = (FollowUpActionsView)viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
			txt_followUpActionDescEn.setValue(followUpActionsView.getDescriptionEn());
			txt_followUpActionDescAr.setValue(followUpActionsView.getDescriptionAr());
			if(followUpActionsView.getFollowUpStatus()!=null)
			 cmb_followUpStatus.setValue(followUpActionsView.getFollowUpStatus().toString());
		}
		else
		{
			txt_followUpActionDescEn.setValue("");
			txt_followUpActionDescAr.setValue("");
		}
		
	}
	public void btnSave_Click()
	{
		String methodName= "btnSave_Click";
		
		
		try 
		{
			
			logger.logInfo(methodName+"|Start");
			if(viewMap.containsKey(WebConstants.FollowUp.FOLLOW_UP_VIEW))
			  followUpActionsView = (FollowUpActionsView)viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
            if(!hasErrors())
            {
            	
				followUpActionsView.setDescriptionEn(txt_followUpActionDescEn.getValue().toString().trim());
				followUpActionsView.setDescriptionAr(txt_followUpActionDescAr.getValue().toString().trim());
				followUpActionsView.setFollowUpStatus(new Long(cmb_followUpStatus.getValue().toString()));    
			
				if(followUpActionsView.getFollowUpActionsId()== null)
				{
				 followUpActionsView.setCreatedBy(getLoggedInUserId());
				 followUpActionsView.setCreatedOn(new Date());
				 followUpActionsView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				 followUpActionsView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
				}
				followUpActionsView.setUpdatedOn(new Date());
				followUpActionsView.setUpdatedBy(getLoggedInUserId());
				FollowUpServiceAgent fsa = new FollowUpServiceAgent(); 
				fsa.persistFollowUpActions(followUpActionsView);
				viewMap.put(WebConstants.FollowUp.FOLLOW_UP_VIEW, followUpActionsView );
			    String javaScriptText="javaScript:sendResponse();";
		        AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
		        addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
            }
			logger.logInfo(methodName+"|Finish");
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
	}

	public HtmlInputText getTxt_followUpActionDescEn() {
		return txt_followUpActionDescEn;
	}


	public void setTxt_followUpActionDescEn(HtmlInputText txt_followUpActionDescEn) {
		this.txt_followUpActionDescEn = txt_followUpActionDescEn;
	}


	public HtmlInputText getTxt_followUpActionDescAr() {
		return txt_followUpActionDescAr;
	}


	public void setTxt_followUpActionDescAr(HtmlInputText txt_followUpActionDescAr) {
		this.txt_followUpActionDescAr = txt_followUpActionDescAr;
	}


	public javax.faces.component.html.HtmlSelectOneMenu getCmb_followUpStatus() {
		return cmb_followUpStatus;
	}


	public void setCmb_followUpStatus(javax.faces.component.html.HtmlSelectOneMenu cmb_followUpStatus) {
		this.cmb_followUpStatus = cmb_followUpStatus;
	}



	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}


	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}



}
