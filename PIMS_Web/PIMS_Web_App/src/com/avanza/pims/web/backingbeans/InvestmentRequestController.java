package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.memsinvestmentrequestbpel.proxy.MEMSInvestmentRequestBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.DisBenInhBenAssociationView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("unchecked")
public class InvestmentRequestController extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;
	private static final Long INVALID_STATUS = -1L;
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnRequestId;
	private static final String BENEF_NO_COST_CENTER_MAP			= "BENEF_NO_COST_CENTER_MAP";
	private static final String DELETED_DISBURSEMENT 		= "DELETED";
	private static final String DISBURSEMENTS 				= "DISB_DETAILS";
	private static final String BENENEFICIARY_LIST 			= "BENENEFICIARY_LIST";
	private static final String PROCEDURE_TYPE 				= "procedureType";
	private static final String EXTERNAL_ID 				= "externalId";
	private static final String CSS_READONLY 				= "READONLY";
	private static final String IS_UPDATE 					= "IS_UPDATE";
	private static final String IS_FILE_ASSOCIATED 			= "IsFileAssociated";
	private static final String ALL_USER_GROUPS 			= "AllUserGroups";
	private static final String USER_IN_GROUP 				= "UserInGroup";
	private static final String TAB_ID_INVEST_TAB 			= "investTab";
	private static final String TAB_ID_FINANCE				= "financeTab";
	private static final String TAB_ID_REVIEW				= "reviewTab";
	
	private static final String CMB_DEFAULT_VAL				= "-1";
	private static final String PERSON_BALANCE_MAP			= "personBalance";
	private static final String PERSON_BLOCKING_MAP			= "personBlocking";
	

	private HtmlSelectOneMenu cmbInvestTypes = new HtmlSelectOneMenu();
	private HtmlInputText txtTotalAmount = new HtmlInputText();
	private HtmlInputTextarea txtInvestmentDetails = new HtmlInputTextarea();
	private HtmlSelectOneMenu cmbBeneficiaryList = new HtmlSelectOneMenu();
	private HtmlInputText txtAmount = new HtmlInputText();
	private HtmlInputText txtDescription = new HtmlInputText();
	private HtmlDataTable disbursementTable = new HtmlDataTable();
	private HtmlInputTextarea txtReviewComments = new HtmlInputTextarea();
	private HtmlSelectOneMenu cmbUserGroups = new HtmlSelectOneMenu();
	private HtmlTabPanel richPanel = new HtmlTabPanel();
	private HtmlInputText txtBalance = new HtmlInputText();
	private HtmlInputText blockingBalance = new HtmlInputText();
	private HtmlInputText totalAmount = new HtmlInputText();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlFileType = new HtmlInputText();
	private HtmlInputText htmlFileStatus  = new HtmlInputText();
	private HtmlInputText htmlFileDate = new HtmlInputText();
	
	
	

	public HtmlTabPanel getRichPanel() {
		return richPanel;
	}

	public void setRichPanel(HtmlTabPanel richPanel) {
		this.richPanel = richPanel;
	}

	public HtmlInputTextarea getTxtReviewComments() {
		return txtReviewComments;
	}

	public void setTxtReviewComments(HtmlInputTextarea txtReviewComments) {
		this.txtReviewComments = txtReviewComments;
	}

	public HtmlSelectOneMenu getCmbUserGroups() {
		return cmbUserGroups;
	}

	public void setCmbUserGroups(HtmlSelectOneMenu cmbUserGroups) {
		this.cmbUserGroups = cmbUserGroups;
	}

	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}

	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}

	public HtmlSelectOneMenu getCmbBeneficiaryList() {
		return cmbBeneficiaryList;
	}

	public HtmlInputText getTxtAmount() {
		return txtAmount;
	}

	public void setTxtAmount(HtmlInputText txtAmount) {
		this.txtAmount = txtAmount;
	}

	public HtmlInputText getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(HtmlInputText txtDescription) {
		this.txtDescription = txtDescription;
	}

	public void setCmbBeneficiaryList(HtmlSelectOneMenu cmbBeneficiaryList) {
		this.cmbBeneficiaryList = cmbBeneficiaryList;
	}

	public HtmlInputTextarea getTxtInvestmentDetails() {
		return txtInvestmentDetails;
	}

	public void setTxtInvestmentDetails(HtmlInputTextarea txtInvestmentDetails) {
		this.txtInvestmentDetails = txtInvestmentDetails;
	}

	public HtmlInputText getTxtTotalAmount() {
		return txtTotalAmount;
	}

	public void setTxtTotalAmount(HtmlInputText txtTotalAmount) {
		this.txtTotalAmount = txtTotalAmount;
	}

	public HtmlSelectOneMenu getCmbInvestTypes() {
		return cmbInvestTypes;
	}

	public void setCmbInvestTypes(HtmlSelectOneMenu cmbInvestTypes) {
		this.cmbInvestTypes = cmbInvestTypes;
	}

	@Override
	public void init() {
		logger.logInfo("[init() .. Starts]");
		super.init();
		try {
			viewMap.put(PROCEDURE_TYPE,
					WebConstants.PROCEDURE_TYPE_MEMS_INVESTMENT);
			viewMap.put(EXTERNAL_ID,
					WebConstants.Attachment.EXTERNAL_ID_MEMS_INVESTMENT);

			if (!isPostBack()) {

				ApplicationBean application = new ApplicationBean();

				List<DomainDataView> ddvList = CommonUtil
						.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);

				viewMap.put(WebConstants.REQUEST_STATUS_NEW,
						CommonUtil.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_NEW)
								.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED,
						CommonUtil.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)
								.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_APPROVED, CommonUtil
						.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_APPROVED)
						.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_REJECTED, CommonUtil
						.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_REJECTED)
						.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil
						.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_COMPLETE)
						.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED,
						CommonUtil.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_REVIEW_REQUIRED)
								.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_REVIEW_DONE, CommonUtil
						.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_REVIEW_DONE)
						.getDomainDataId());

				viewMap.put(WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil
						.getIdFromType(ddvList,
								WebConstants.REQUEST_STATUS_COMPLETE)
						.getDomainDataId());

				viewMap
						.put(
								WebConstants.MemsInvestmentTypes.DOMAIN_KEY,
								application
										.getDomainDataList(WebConstants.MemsInvestmentTypes.DOMAIN_KEY));

				List<DisbursementDetailsView> toDeleteList = new ArrayList<DisbursementDetailsView>();
				viewMap.put(DELETED_DISBURSEMENT, toDeleteList);

				RequestView reqView = (RequestView) getRequestMap().get( WebConstants.REQUEST_VIEW);
						
				InheritanceFileView inheritanceFile = (InheritanceFileView) getRequestMap()
						.get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);

				BeneficiaryGridView beneficiaryView = (BeneficiaryGridView) getRequestMap()
						.get( WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
								
				UserTask userTask = (UserTask) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK);
						
				
				if( null!=userTask ) {
					sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK);
				}

				if (null != reqView) {
					initFromRequestSearch(reqView);
				} else if (null != inheritanceFile) {
					initFromInheritanceFileSearch(inheritanceFile);
				} else if (null != beneficiaryView) {
					initFromBeneficiarySearch(beneficiaryView);
				} else if (null != userTask) {
					initFromUserTask(userTask);
				}

				handleFinanceTabVisibility();
				loadUserGroupsInViewRoot();
				setUserInGroup();
			}
		} catch (Exception ex) {

			logger.LogException("[Exception occured in init()]", ex);
			ex.printStackTrace();
		}
		logger.logInfo("[init() .. Ends]");
	}

	private void initFromUserTask(UserTask userTask) throws Exception {
		logger.logInfo("[initFromUserTask(UserTask) .. Starts]");
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		Map taskAttributes = userTask.getTaskAttributes();
		if (null != taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)) {
			Long reqId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));					
			RequestService reqSrv = new RequestService();
			RequestView requestView = reqSrv.getRequestById(reqId);
			initFromRequestSearch(requestView);
			loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
		}
		logger.logInfo("[initFromUserTask(UserTask) .. Ends]");
	}

	private void initFromBeneficiarySearch(BeneficiaryGridView beneficiaryView) throws Exception {
		logger.logInfo("[initFromBeneficiarySearch() .. Starts]");
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();
		
		setTotalRows(disbDetails.size());

		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date());
		reqView.setCreatedOn(new Date());
		reqView.setRequestDate(new Date());

		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.INVESTMENT_REQUEST_ID);
				
		reqView.setStatusId(INVALID_STATUS);
		reqView.setDisubrsementDetailsView(disbDetails);

		viewMap.put(DISBURSEMENTS, disbDetails);
		setRequestView(reqView);
		cmbBeneficiaryList.setValue(beneficiaryView.getBeneficiaryPersonId().toString());
	    storeBeneficiaryBalanceAmountInMap( beneficiaryView.getBeneficiaryPersonId() );
	    storeBeneficiaryBlockingAmountInMap(beneficiaryView.getBeneficiaryPersonId() );
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		loadBeneficiaryList(beneficiaryView);
		logger.logInfo("[initFromBeneficiarySearch() .. Ends]");
	}
	private void storeBeneficiaryBlockingAmountInMap( Long beneficiaryId ) throws Exception
	   {
		   if(  !getBlockingMap().containsKey(beneficiaryId  ) )
		   {
		    
			Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
			Double amount =  BlockingAmountService.getBlockingAmount(  beneficiaryId , null, null, null, null);
			personBlockingMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
			handleBeneficiaryChange();				
		   }
		   
	   }
   private void storeBeneficiaryBalanceAmountInMap( Long beneficiaryId ) throws Exception
   {
	   if( ! getBalanceMap().containsKey( beneficiaryId  ) )
	   {
	    PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryId,getRequestView().getInheritanceFileId());
//		Double amount =  accountWS.getAmountByPersonId( beneficiaryId  );
		personBalanceMap.put( beneficiaryId , amount);
	    viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		handleBeneficiaryChange();				
	   }
	   
   }
	private void initFromInheritanceFileSearch(InheritanceFileView inheritanceFileView) throws Exception 
	{
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();
		setTotalRows(disbDetails.size());
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date());
		reqView.setCreatedOn(new Date());
		reqView.setRequestDate(new Date());
		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.INVESTMENT_REQUEST_ID);
		reqView.setStatusId(INVALID_STATUS);
		reqView.setDisubrsementDetailsView(disbDetails);
		viewMap.put(DISBURSEMENTS, disbDetails);
		Long fileId = inheritanceFileView.getInheritanceFileId();
		reqView.setInheritanceFileId(fileId);
		reqView.setInheritanceFileView(inheritanceFileView);
		setRequestView(reqView);
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		loadBeneficiaryList(fileId);
		loadInhFileParamTextBoxes(fileId);
		viewMap.put(IS_FILE_ASSOCIATED, "TRUE");
	}

	
	private void loadInhFileParamTextBoxes(Long fileId) {
		String methodName = "loadInhFileParams|";
		logger.logInfo(methodName);
		try {
			if (fileId != null) 
			{
				InheritanceFileView inhFile = new UtilityService().getInheritanceFileById(fileId);
				htmlFileNumber.setValue(inhFile.getFileNumber());
				DomainDataView domainData = new UtilityService().getDomainDataByDomainDataId(inhFile.getStatusId() );
				DomainDataView domainDataFileType = new UtilityService().getDomainDataByDomainDataId(new Long(	inhFile.getFileTypeId() ));
				
				PropertyService service = new PropertyService();
				PersonView personView=service.getPersonById(new Long( inhFile.getFilePersonId() ));
				
				@SuppressWarnings("unused")
				String completeName = (personView.getFirstName()!=null  ? personView.getFirstName():"" )
										+(personView.getMiddleName()!=null  ? personView.getMiddleName():"" )
										 + (personView.getLastName()!=null  ? personView.getLastName():"");
				if (CommonUtil.getIsEnglishLocale()) {
					htmlFileStatus.setValue(domainData.getDataDescEn());
					htmlFileType.setValue(domainDataFileType.getDataDescEn());
					htmlFileDate.setValue(inhFile.getCreatedOn());
//					htmlFilePerson.setValue(completeName); 
					
				} else {
					htmlFileStatus.setValue(domainData.getDataDescAr());
					htmlFileType.setValue(domainDataFileType.getDataDescAr());
					htmlFileDate.setValue(inhFile.getCreatedOn());
					
					
//					htmlFilePerson.setValue(completeName); 
				}

			}
		} catch (Exception ex) {
			logger.LogException(methodName + "Error Occured:", ex);
			
		}
		
	}

	private void initFromRequestSearch(RequestView reqView) throws Exception {
		logger.logInfo("[initFromRequestSearch() .. Starts]");
		setRequestView(reqView);
		Long inhFileId = reqView.getInheritanceFileId();
		refreshTabs(reqView);
		// TODO: if its not from request search populate with the beneficiary
		// combo box
		if (null != inhFileId) {
			loadBeneficiaryList(inhFileId);
			viewMap.put(IS_FILE_ASSOCIATED, "TRUE");
		} else {
			// the request is not associated with the inheritance file
			// there should be only one disbursement and one request beneficiary
			List<DisbursementDetailsView> view = getDisbursementDetails();
			if (null != view && 1 == view.size()) {
				List<DisbursementRequestBeneficiaryView> reqBenef = view.get(0).getRequestBeneficiaries();						
				DisbursementRequestBeneficiaryView next = reqBenef.get(0);
				BeneficiaryGridView gridView = new BeneficiaryGridView();
				gridView.setPersonName(next.getName());
				gridView.setBeneficiaryPersonId(next.getPersonId());
				loadBeneficiaryList(gridView);
			}
		}
		loadAttachmentsAndComments(reqView.getRequestId());
		loadInhFileParamTextBoxes(reqView.getInheritanceFileId()!=null?reqView.getInheritanceFileId():null);
		showTab(getRequestView().getStatusId());
		logger.logInfo("[initFromRequestSearch() .. Ends]");
	}
	
	private void showTab(final Long statusId) {
		if( statusId.equals( getStatusApprovalRequiredId()) || statusId.equals( getStatusReviewedId()) ) {
			richPanel.setSelectedTab( TAB_ID_INVEST_TAB);
		} else if( statusId.equals( getStatusApprovedId()) ) {
			richPanel.setSelectedTab( TAB_ID_FINANCE);
		} else if ( statusId.equals( getStatusReviewRequiredId()) ) {
			richPanel.setSelectedTab( TAB_ID_REVIEW);
		}
	}

	private void refreshTabs(RequestView reqView) {
		if (null == reqView && null == reqView.getApplicantView()) {
			throw new IllegalArgumentException();
		}

		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, reqView
				.getRequestDate());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, reqView
				.getRequestNumber());

		PersonView applicant = reqView.getApplicantView();
		viewMap.put(WebConstants.LOCAL_PERSON_ID, applicant.getPersonId());

		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,
				applicant.getPersonId());

		// TODO: I am getting it null when saving
		// FIXME proper fix required
		if (null != applicant.getPersonFullName()) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					applicant.getPersonFullName());
		}

		if (null != applicant.getCellNumber()) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					applicant.getCellNumber());
		}

		String desc = reqView.getDescription();

		if (null != desc && 0 != desc.trim().length()) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,
					reqView.getDescription());
		}

		if (CommonUtil.getIsEnglishLocale()) {
			// viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
			// applicant.getPersonTypeEn());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,
					reqView.getStatusEn());
		} else if (CommonUtil.getIsArabicLocale()) {
			// viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
			// applicant.getPersonTypeAr());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,
					reqView.getStatusAr());
		}

		if (!getIsNewStatus()) {
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}

		List<DisbursementDetailsView> detailsView = reqView.getDisubrsementDetailsView();
				
		if (null == detailsView) {
			detailsView = new ArrayList<DisbursementDetailsView>();
		}
		
		txtInvestmentDetails.setValue(reqView.getInvestmentDetails());
		cmbInvestTypes.setValue(reqView.getInvestmentType().toString());
		viewMap.put(DISBURSEMENTS, detailsView);
		
		if( getIsApproved() ) {
			populateFinanceData();
		}
		handleAttachmentCommentTabStatus(reqView);
	}

	@Override
	public void prerender() {
		super.prerender();
		readParams();

		if (viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS)) {				
			List<String> sucMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);					
			successMessages.addAll(sucMsgs);
			viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);

		}
		if (viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS)) {				
			List<String> errMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);					
			errorMessages.addAll(errMsgs);
			viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);

		
		
		}
		if(viewMap.containsKey(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES))
		{
			errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES) );
			viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES);
			
		}
		if (  getIsReviewRequired() && getIsUserInGroup() )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
		}
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}

	private boolean validateCostCenter(String personId) throws Exception 
	{
		Map<Long, String> map  =  (HashMap<Long, String>) viewMap.get( BENEF_NO_COST_CENTER_MAP );
		if ( map.containsKey( new Long(personId) ) )
		{
			String message = map.get( new Long(personId) );
			errorMessages.add( 
								java.text.MessageFormat.format(
																CommonUtil.getBundleMessage("disburseFinance.msg.associateCostCenter"),
																 (" "+message)
															  )
							    
							 );
			return false;
		}
		return true;
	}

	private boolean validateComplete() {
		logger.logInfo("[validateComplete() .. Starts]");
		if (!viewMap.containsKey(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE)) {
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.ERR_COMPLETE));
			return false;
		}
		logger.logInfo("[validateComplete() .. Ends]");
		return true;
	}
	public void handleBeneficiaryChange() 
	{
		try
		  {
			 if( cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL) || 
				 !validateCostCenter(cmbBeneficiaryList.getValue().toString()) 	 
			   ) 
			 {
					txtBalance.setValue( "" );			
					txtAmount.setValue( "" );
					blockingBalance.setValue( "" );
					availableBalance.setValue("");
					requestedAmount.setValue("");
					return;
			 }
			 else if( !cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL) 
			        ) 
			 {
				Map<Long, Double> balanceMap = getBalanceMap();
				Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
				Double amount = 0.00d;
				if( balanceMap.containsKey( beneficiaryId) ) 
				{
					amount = balanceMap.get( beneficiaryId);
					txtBalance.setValue( amount.toString());
				}
				

				
				 Map<Long, Double> blockingMap = getBlockingMap();
				 Double blockingMapAmount = 0.00d ;
				 if( blockingMap.containsKey( beneficiaryId ) ) 
				 {
					blockingMapAmount = blockingMap.get( beneficiaryId);
					blockingBalance.setValue( blockingMapAmount.toString());
				 }
				 
				 Double requestAmount = DisbursementService.getRequestedAmount(
						 														beneficiaryId, 
						 														getRequestView().getInheritanceFileId(), 
						 														getRequestView().getRequestId() 
						 														,null
						 													   );
				 requestedAmount.setValue( requestAmount );
					
				 DecimalFormat oneDForm = new DecimalFormat("#.00");
				 availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
					
			 } 
	}
	  catch(Exception ex) 
	  {
		 logger.LogException("[Exception occured in handleBeneficiaryChange()]", ex);
	  }
	}
	private Map<Long, Double> getBalanceMap() {
		if( viewMap.containsKey( PERSON_BALANCE_MAP)) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BALANCE_MAP);
		}
		return new HashMap<Long, Double>();
	}

	private Map<Long, Double> getBlockingMap() {
		if( viewMap.containsKey( PERSON_BLOCKING_MAP )) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BLOCKING_MAP  );
		}
		return new HashMap<Long, Double>();
	}
	public boolean validateSubmitQuery() {
		logger.logInfo("[validateSubmit() .. Starts]");
		boolean bValid = true;
		if (0 == getDisbursementDetails().size()) {
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_ONE_BENEF_REQ));												
			bValid = false;
		}
		logger.logInfo("[validateSubmit() .. Ends]");
		return bValid;
	}

	private void loadUserGroupsInViewRoot() throws Exception {
		if (!viewMap.containsKey(ALL_USER_GROUPS)) {
			List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
			List<SelectItem> listUserGrps = new ArrayList<SelectItem>(
					allUserGrps.size());
			for (UserGroup singleGrp : allUserGrps) {
				listUserGrps.add(new SelectItem(singleGrp.getUserGroupId(),
						isEnglishLocale()? singleGrp.getPrimaryName(): singleGrp.getSecondaryName()));
			}
			Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
			viewMap.put(ALL_USER_GROUPS, listUserGrps);
		}
	}

	public List<SelectItem> getAllUserGroups() {
		if (viewMap.containsKey(ALL_USER_GROUPS)) {
			return (List<SelectItem>) viewMap.get(ALL_USER_GROUPS);
		}
		return new ArrayList<SelectItem>(0);
	}

	public List<SelectItem> getInvestmentTypes() {
		if (viewMap.containsKey(WebConstants.MemsInvestmentTypes.DOMAIN_KEY)) {
			return (List<SelectItem>) viewMap
					.get(WebConstants.MemsInvestmentTypes.DOMAIN_KEY);
		}
		return new ArrayList<SelectItem>(0);
	}

	private boolean validateBeneficiariesInput()throws Exception 
	{

		// this check will only be invoked when the status of the request is
		// approval required
		// and use clicks save without selecting any beneficiary


		if (null == getIsUpdate() && (getIsApprovalRequired() || getIsReviewed()) ) 
		{			
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_SEL_BENEFICIARY));
			
			return false;
		}

		boolean bValid = true;
		if ( null == cmbBeneficiaryList.getValue() || 
			 cmbBeneficiaryList.getValue().toString().equals("-1")
		   ) 
	    {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_BENF_REQ));
			bValid = false;
		}
		else if(!validateCostCenter(cmbBeneficiaryList.getValue().toString()))
		{
			return false;
		}

		if (null == txtAmount.getValue()
				|| 0 == txtAmount.getValue().toString().trim().length()) {
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_AMNT_REQ));
			bValid = false;
		} 
		else 
		{
			try 
			{
				double amount = Double.parseDouble(txtAmount.getValue().toString().trim());
			} 
			catch (NumberFormatException ex) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_AMNT_NUMERIC));
				bValid = false;
			}
			Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
			int validateBalanceCode = validateBalance(
					 									 getBalanceMap().get( beneficiaryId ) ,
														 getBlockingMap().get( beneficiaryId ),
														 DisbursementService.getRequestedAmount(beneficiaryId , getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
														 Double.parseDouble(txtAmount.getValue().toString().trim()),
														 beneficiaryId,
														 null,
														 getRequestView().getInheritanceFileId()
			   										  ) ;

			 if ( validateBalanceCode != 0 )
			 {
				 return false;
			 }
			
		}

		if (null == getIsUpdate()) {
			List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
			for (DisbursementDetailsView singleDetailView : dDetailsView) {
				if (singleDetailView.getPersonId().toString().equals(
						cmbBeneficiaryList.getValue().toString())) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_ALREADY_EXISTS));
					bValid = false;
					break;
				}
			}
		}
		logger.logInfo("[validateBeneficiariesInput .. Ends]");
		return bValid;
	}

	public void save() {
		logger.logInfo("[save() .. Starts]");
		try {
			Long statusId = getRequestView().getStatusId();
			if (doSaveApplication(getStatusNewId())) 
			{
				if (statusId.equals(INVALID_STATUS)) 
				{
					CommonUtil.saveSystemComments(WebConstants.REQUEST,
							MessageConstants.RequestEvents.REQUEST_CREATED,
							getRequestView().getRequestId());
				}
				refreshTabs(getRequestView());
				successMessages
						.add(CommonUtil
								.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SAVE));
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in save()]", ex);
			ex.printStackTrace();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[save() .. Ends]");
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+getRequestView().getInheritanceFileId()+"');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			Long requestId = -1l;
			if( getRequestView().getRequestId() != null )
			{
				requestId  = getRequestView().getRequestId();
			}
			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+
								  														getRequestView().getInheritanceFileId()+"','"+ 
								  														requestId+
								  												 "');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onPrint()
    {
    	try
    	{
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	public void submitQuery() {
		logger.logInfo("[submitQuery() .. Starts]");
		try {
			Long statusId = getRequestView().getStatusId();
			if (validateSubmitQuery() && doSaveApplication(getStatusApprovalRequiredId())) {					
				if (statusId.equals(INVALID_STATUS)) {
					CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_CREATED,
																								getRequestView().getRequestId());													
				}
				invokeBpel(getRequestView().getRequestId());
				refreshTabs(getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST,
												MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL,
												getRequestView().getRequestId());
				CommonUtil.printMemsReport( this.getRequestView().getRequestId() );																																				
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SUBMIT));														
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in submitQuery()]", ex);
			ex.printStackTrace();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[submitQuery() .. Ends]");
	}

	public void complete() {
		logger.logInfo("[complete() .. Starts]");
		try {
			if ( validateComplete() ) {
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				refreshTabs(getRequestView());
				completeTask(TaskOutcome.OK);
				doSaveApplication(getStatusCompleteId());
				CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED,
						getRequestView().getRequestId());														
				successMessages.add(CommonUtil
						.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_COMPLETE));						
								
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in complete()]", ex);
			ex.printStackTrace();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

		logger.logInfo("[complete() .. Ends]");
	}

	public void approve() {
		logger.logInfo("[approve() .. Starts]");
		try {
			if( validateApprove() ) 
			{
				Long disbRequired = CommonUtil.getDomainDataId(WebConstants.DisbursementStatus.DOMAIN_KEY,
																				WebConstants.DisbursementStatus.DIS_REQ);											
				List<DisbursementDetailsView> details = getDisbursementDetails();
				for (DisbursementDetailsView singleDetail : details) 
				{
					singleDetail.setStatus(disbRequired);
				}
				doSaveApplication( getStatusApprovedId() );
				completeTask(TaskOutcome.APPROVE);
				refreshTabs(getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_APPROVED,
																				getRequestView().getRequestId());
												
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_APPROVE));																								
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in approve]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));															
		}
		logger.logInfo("[approve() .. Ends]");
	}

	public void reject() {
		logger.logInfo("[reject() .. Starts]");
		try {
			List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
			Long disRejected = CommonUtil.getDomainDataId(
					WebConstants.DisbursementStatus.DOMAIN_KEY,
					WebConstants.DisbursementStatus.REJECTED);
			for (DisbursementDetailsView singleDetail : disbursementDetails) {
				singleDetail.setStatus(disRejected);
			}
			if (doSaveApplication(getStatusRejectedId())) {
				completeTask(TaskOutcome.REJECT);
				refreshTabs(getRequestView());
				CommonUtil.saveSystemComments(WebConstants.REQUEST,
						MessageConstants.RequestEvents.REQUEST_REJECTED,
						getRequestView().getRequestId());
				successMessages
						.add(CommonUtil
								.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REJECT));
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in approve]", ex);
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[reject() .. Ends]");
	}

	public void done() {
		try {
			doSaveApplication(getStatusReviewedId());
			ReviewRequestView reviewRequestView = (ReviewRequestView) viewMap
					.get(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
			new UtilityService().persistReviewRequest(reviewRequestView);
			completeTask(TaskOutcome.APPROVE);
			refreshTabs(getRequestView());
			CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.MemsNOL.MSG_REQ_REVIWED, 
								getRequestView().getRequestId());
		} catch (Exception ex) {
			logger.LogException("[Exception occured in done()]", ex);
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void review() {
		try {
			if (validateSendReviewInput()) {
				RequestView reqView = getRequestView();
				reqView.setMemsNolReviewGrpId(cmbUserGroups.getValue()
						.toString());
				RequestService reqWS = new RequestService();
				reqWS.saveOrUpdate(getReviewRequestVO());
				if (doSaveApplication(getStatusReviewRequiredId())) {
					refreshTabs(getRequestView());
					CommonUtil.saveSystemComments(WebConstants.REQUEST,
														MessageConstants.MemsNOL.MSG_REQ_SENT_FOR_REVIEW,
														getRequestView().getRequestId());
					setUserInGroup();
					completeTask(TaskOutcome.OK);
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST);
				}
				successMessages
						.add(CommonUtil
								.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REVIEW));
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			ex.printStackTrace();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void saveBeneficiary() 
	{
		try 
		{
			if (validateBeneficiariesInput()) 
			{
				DisbursementDetailsView reqBenef = getIsUpdate();
				if (null != reqBenef) 
				{
					Iterator<DisbursementRequestBeneficiaryView> iterator = reqBenef.getRequestBeneficiaries().iterator();
					DisbursementRequestBeneficiaryView next = iterator.next();
					double amnt = Double.parseDouble(txtAmount.getValue().toString());
					next.setAmount(amnt);
					reqBenef.setAmount(amnt);
					reqBenef.setOriginalAmount(amnt);
					reqBenef.setDescription(txtDescription.getValue().toString());
					if( getRequestView()!= null && getRequestView().getInheritanceFileId() != null )
					{
						
						List<DisBenInhBenAssociationView> fileAssociationList = new ArrayList<DisBenInhBenAssociationView>();
						DisbursementRequestBeneficiaryView benfView =  (DisbursementRequestBeneficiaryView) reqBenef.getRequestBeneficiaries().get(0);
						DisBenInhBenAssociationView fileAssoView = ( DisBenInhBenAssociationView )benfView.getFileAssociations().get(0) ;
						fileAssoView.setAmount( amnt );
						fileAssociationList.add( fileAssoView );
						benfView.setFileAssociations(fileAssociationList);
					}
					viewMap.remove(IS_UPDATE);
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_ADD_SUCC));

				}
				else 
				{
					String beneficiaryName = "";
					List<SelectItem> beneficiaryList = this.getBeneficiaryList();
					for (SelectItem singleItem : beneficiaryList) 
					{
						if (singleItem.getValue().toString().equals(cmbBeneficiaryList.getValue().toString())) 
						{
							beneficiaryName = singleItem.getLabel();
							break;
						}
					}
					List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
					DisbursementDetailsView defView = getDefaultDisbDetailView();
					double amnt = Double.parseDouble( txtAmount.getValue().toString() );
					defView.setAmount( amnt );
					defView.setOriginalAmount( amnt );
					defView.setDescription( txtDescription.getValue().toString().trim()  );
					List<DisbursementRequestBeneficiaryView> requestBeneficiaries = defView.getRequestBeneficiaries();
							
					if ( null == requestBeneficiaries ) 
					{
						requestBeneficiaries = new ArrayList<DisbursementRequestBeneficiaryView>();
						defView.setRequestBeneficiaries(requestBeneficiaries);
					}
					DisbursementRequestBeneficiaryView benfView = new DisbursementRequestBeneficiaryView();
					//If request was created from File Search.
					if( getRequestView()!= null && getRequestView().getInheritanceFileId() != null )
					{
						List<DisBenInhBenAssociationView> fileAssociationList = new ArrayList<DisBenInhBenAssociationView>();
						DisBenInhBenAssociationView fileAssoView = new DisBenInhBenAssociationView();
						benfView.setFileAssociations(fileAssociationList);
						fileAssoView.setAmount( amnt );
						fileAssoView.setPersonId( Long.parseLong( cmbBeneficiaryList.getValue().toString() ) );
						fileAssoView.setInheritanceFileId( getRequestView().getInheritanceFileId().toString()  );
						fileAssociationList.add( fileAssoView ); 
					}
					benfView.setName(beneficiaryName);
					benfView.setAmount(amnt);
					benfView.setPersonId(Long.parseLong(cmbBeneficiaryList.getValue().toString()));
					benfView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
					
					requestBeneficiaries.add(benfView);
					dDetailsView.add(defView);
					setTotalAmount(dDetailsView);
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_UPDATE_SUCC));
				}
				setTotalRows(getDisbursementDetails().size());
				resetNonMaturedSharesViewToDetault();
			}
		} catch (Exception ex) 
		{
			logger.LogException("[Exception occured in saveBeneficiary()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void editBeneficiary() 
	{
		try
		{
			DisbursementDetailsView selectedRow = (DisbursementDetailsView) disbursementTable.getRowData();
			if (null != selectedRow) 
			{
				txtAmount.setValue(selectedRow.getAmount().toString());
				txtDescription.setValue(selectedRow.getDescription());
				cmbBeneficiaryList.setValue(selectedRow.getPersonId().toString());
				cmbBeneficiaryList.setDisabled(true);
				storeBeneficiaryBalanceAmountInMap(selectedRow.getPersonId());
				storeBeneficiaryBlockingAmountInMap(selectedRow.getPersonId());
				handleBeneficiaryChange();
				viewMap.put(IS_UPDATE, selectedRow);
			}
		}
		catch( Exception e )
		{
    		errorMessages = new ArrayList<String>(0);
    		logger.LogException("[editBeneficiary() .. |Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}

	}

	public void editPaymentDetails() {
		logger.logInfo("[editPaymentDetails() .. Starts]");
		DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();				
		List<DisbursementRequestBeneficiaryView> reqSet = detailData.getRequestBeneficiaries();
		DisbursementRequestBeneficiaryView next = reqSet.get( 0);				
		sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, detailData);
//		String javaScriptText = " var screen_width = 800;"
//				+ "var screen_height = 500;"
//				+ "var popup_width = screen_width-200;"
//				+ "var popup_height = screen_height;"
//				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
//				+ "window.open('paymentDetailsPopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP&"
//				+ WebConstants.PERSON_ID
//				+ "="
//				+ next.getPersonId().toString()
//				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
		String javaScriptText = " javaScript:openPaymentDetailsPopUp( 'pageMode=MODE_SELECT_MANY_POPUP&"
			+ WebConstants.PERSON_ID+ "="+ next.getPersonId().toString()+"');";		

		sendToParent(javaScriptText);
		logger.logInfo("[editPaymentDetails() .. Starts]");
	}
	
	public void showInheritanceFilePopup() {
		logger.logInfo("[showInheritanceFilePopup() .. Starts]");
		try	 {			
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, getRequestView().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			String javaScriptText ="var screen_width = 900;"+
            	"var screen_height =600;"+
            	"window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    sendToParent( javaScriptText);		    		    
		}
		catch (Exception ex)  {
			logger.LogException("[Exception occured in showInheritanceFilePopup()]", ex);
			ex.printStackTrace();
		}
		logger.logInfo("[showInheritanceFilePopup() .. Ends]");
	}

	private String sendToParent(String javaScript) {
		final String viewId = "/SearchAssets.jsp";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	public void onMessageFromAssociateCostCenter()
	{
		try
		{
			logger.logInfo("[onMessageFromAssociateCostCenter .. Starts]");
			DisbursementDetailsView message = (DisbursementDetailsView) sessionMap.get(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);				
			sessionMap.remove(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);
			List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
			for (DisbursementDetailsView dtView : disbursementDetails) 
			{
				if(dtView.getId() == message.getId() )
				{
				  logger.logInfo("[onMessageFromAssociateCostCenter .. Id matched]");
				  disbursementDetails.remove( dtView  );
				  disbursementDetails.add( message );
				  successMessages.add(CommonUtil.getBundleMessage( "commons.associateCostCenterDoneSuccessfully" ));
				  break;
				}
                								
				
			}
			setDisbursementDetails( disbursementDetails );
			logger.logInfo("[onMessageFromAssociateCostCenter .. Ends]");
		}
		catch( Exception e)
		{
			logger.LogException( " [onMessageFromAssociateCostCenter --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}	
	public void loadPaymentDetails(ActionEvent evt) 
	{
		try
		{
				logger.logInfo("[loadPaymentDetails .. Starts]");
				DisbursementDetailsView view = (DisbursementDetailsView) sessionMap.get(WebConstants.LOAD_PAYMENT_DETAILS);				
				sessionMap.remove(WebConstants.LOAD_PAYMENT_DETAILS);
				boolean bFinanceTab = false;
				if( sessionMap.containsKey( WebConstants.FinanceTabPublishKeys.IS_FINANCE)) {
					sessionMap.remove( WebConstants.FinanceTabPublishKeys.IS_FINANCE);
					bFinanceTab = true;
				}
				if( bFinanceTab ) {																						
					FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
					if( null!=tabFinanceController ) {
						tabFinanceController.loadPaymentDetails( view);
					}
				}
				PaymentCriteriaView criteriaView = view.getPaymentCriteriaView();
				List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
				for (DisbursementDetailsView dtView : disbursementDetails) {
					if (dtView.getDisDetId().equals(view.getDisDetId())) {
						List<PaymentDetailsView> listPaymentDetailsView = dtView.getPaymentDetails();
						if( null==listPaymentDetailsView ) {
							listPaymentDetailsView = new ArrayList<PaymentDetailsView>();
							dtView.setPaymentDetails( listPaymentDetailsView);
						}
						
						PaymentDetailsView paymentView = null;
						if( 0<listPaymentDetailsView.size() ) {
							paymentView = listPaymentDetailsView.get( 0);
						} else {
							paymentView = new PaymentDetailsView();
							paymentView.setStatus( WebConstants.NOT_DISBURSED);
							paymentView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
						}
						
						paymentView.setAccountNo(criteriaView.getAccountNumber());
						paymentView.setAmount(criteriaView.getAmount());
						paymentView.setBankId(criteriaView.getBankId());
						if (null != criteriaView.getPaymentMode()) {
							paymentView.setPaymentMethod(new Long(criteriaView
									.getPaymentMode()));
						}
						if( null!=criteriaView.getPaidTo()) {
							paymentView.setPaidTo( new Long( criteriaView.getPaidTo()));
							paymentView.setOthers( null );
						}
						else if ( criteriaView.getOther() != null && criteriaView.getOther().trim().length() >0 )
						{
							paymentView.setOthers(criteriaView.getOther().trim() );
							paymentView.setPaidTo(  null );
						}
						paymentView.setAmount(dtView.getAmount());
						paymentView.setRefDate(criteriaView.getDate());
						paymentView.setReferenceNo(criteriaView.getReferenceNumber());
						if( 0==listPaymentDetailsView.size() ) {
							listPaymentDetailsView.add( paymentView);
						}
						successMessages.add(CommonUtil						
											.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
					}
				}
				logger.logInfo("[loadPaymentDetails .. Ends]");
		}
		catch( Exception exception) 
		{		
			 errorMessages.clear();
			 logger.LogException("loadPaymentDetails() crashed ", exception);
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} 
	}

	public void deleteBeneficiary() {
		logger.logInfo("[deleteBeneficiary() .. Starts]");
		try {
			DisbursementDetailsView detailView = (DisbursementDetailsView) disbursementTable
					.getRowData();
			if (null != detailView) {
				if (null != detailView.getDisDetId()) {
					detailView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					getToDeleteList().add(detailView);
				}
				if (getDisbursementDetails().remove(detailView)) {
					successMessages
							.add(CommonUtil
									.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_DELETE_SUCC));
				}
			}
		} catch (Exception ex) {
			logger.logInfo("[Exception occured in deleteBeneficiary()]", ex);
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[deleteBeneficiary() .. Ends]");
	}

	private boolean doSaveApplication(final Long statusId) {
		logger.logInfo("[doSaveApplication .. Starts]");
		try 
		{
			if (validateInput()) 
			{
				RequestView reqView = getRequestView();
				reqView.setStatusId(statusId);
				reqView.setInvestmentDetails( txtInvestmentDetails.getValue().toString() );
				reqView.setInvestmentType( new Long( cmbInvestTypes.getValue().toString() ) );
				int totalSize = getToDeleteList().size()+ getDisbursementDetails().size();
				List<DisbursementDetailsView> totalDisbursement = new ArrayList<DisbursementDetailsView>(totalSize);
				totalDisbursement.addAll( getDisbursementDetails() );
				totalDisbursement.addAll( getToDeleteList() );
				reqView.setDisubrsementDetailsView( totalDisbursement );
				RequestView updatedView = saveOrUpdate( reqView );
				setRequestView( updatedView );
				List<DisbursementDetailsView> dDetailsView = updatedView.getDisubrsementDetailsView();
				if (null == dDetailsView) 
				{
					dDetailsView = new ArrayList<DisbursementDetailsView>();
				}
				setDisbursementDetails(dDetailsView);
				setTotalRows(dDetailsView.size());
				saveAttachmentsAndComments(updatedView.getRequestId());
				logger.logInfo("[doSaveApplication .. Ends]");
				return true;
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in saveApplication()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

		
		return false;
	}

	private void setTotalAmount(List<DisbursementDetailsView> detailsView) {
		Double totalGridAmount=new Double(0);
		for (DisbursementDetailsView disbursementDetailsView : detailsView) {
			totalGridAmount+=disbursementDetailsView.getAmount();
		}
		this.totalAmount.setValue(totalGridAmount);
		// TODO Auto-generated method stub
		
	}

	private boolean validateSendReviewInput() {
		Object comments = txtReviewComments.getValue();
		Object userGroup = cmbUserGroups.getValue();
		errorMessages  = new ArrayList<String>();
		boolean bValid = true;
		if (null == comments || 0 == comments.toString().trim().length()) {
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_COMMENTS_REQ));
			bValid = false;
		}
		if (null == userGroup || userGroup.toString().equals("-1")) {
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_SEL_GROU));
			bValid = false;
		}
		return bValid;
	}

	private boolean validateApprove()throws Exception 
	{
		errorMessages  = new ArrayList<String>();
		List<DisbursementDetailsView> ddView = getDisbursementDetails();
		boolean hasBalanceError = false;
		for (DisbursementDetailsView singleView : ddView) 
		{
			if( null == singleView.getPaymentDetails() || 0 == singleView.getPaymentDetails().size() ) 
			{			
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_PAYMENT_DT_REQ));
				richPanel.setSelectedTab( TAB_ID_INVEST_TAB);
				return false;
			}
			for(DisbursementRequestBeneficiaryView singleBeneficiary : singleView.getRequestBeneficiaries()) 
			{
			 if( !validateCostCenter( singleBeneficiary.getPersonId().toString() ) )
			 {
				 return false;
			 }
				
        	 int validateBalanceCode = validateBalance(  getBalanceMap().get( singleBeneficiary.getPersonId() ) ,
														 getBlockingMap().get( singleBeneficiary.getPersonId() ),
														 DisbursementService.getRequestedAmount(singleBeneficiary.getPersonId() , getRequestView().getInheritanceFileId(), getRequestView().getRequestId(),null ),
														 singleBeneficiary.getAmount(),
														 singleBeneficiary.getPersonId(),
														 singleView.getBeneficiariesName(),
														 getRequestView().getInheritanceFileId()
			   										  );
			  if( !hasBalanceError )
			  {
				hasBalanceError = validateBalanceCode > 0 ;
			  }
			}
			if ( hasBalanceError )
			{
				richPanel.setSelectedTab( TAB_ID_INVEST_TAB);
				return false;
			}
		}
		return true;
	}

	private boolean validateInput() {
		boolean bValid = true;
		errorMessages  = new ArrayList<String>();

		if (!viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID)) {				
			errorMessages.add(CommonUtil
								.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			richPanel.setSelectedTab("applicationTab");
			return false;
		}

		if (null == cmbInvestTypes.getValue() || cmbInvestTypes.getValue().toString().equals("-1")) {				
			errorMessages.add(CommonUtil
								.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_INV_TYPE_REQ));											
			bValid = false;
		}

		if (null == txtInvestmentDetails.getValue() || 0 == txtInvestmentDetails.getValue().toString().trim().length()) {								
			errorMessages.add(CommonUtil
								.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_INV_DESC_REQ));												
			bValid = false;
		}
		return bValid;
	}

	private RequestView saveOrUpdate(RequestView reqView)throws Exception 
	{
		RequestService reqWS = new RequestService();
		return reqWS.saveOrUpdate(reqView);
	}

	private DisbursementDetailsView getIsUpdate() {
		return (DisbursementDetailsView) viewMap.get(IS_UPDATE);
	}

	private void readParams() {
		logger.logInfo("[readParams .. Starts]");
		if (hdnPersonId != null && !hdnPersonId.equals("")) {
			RequestView reqView = getRequestView();
			PersonView applicant = reqView.getApplicantView();
			if (null != applicant) {
				applicant.setPersonId(Long.parseLong(hdnPersonId));
			} else {
				applicant = new PersonView();
				applicant.setPersonId(Long.parseLong(hdnPersonId));
				reqView.setApplicantView(applicant);
			}
			viewMap.put(WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,
					hdnPersonId);
		}

		if (hdnPersonName != null && !hdnPersonName.equals("")) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					hdnPersonName);
		}

		if (null != hdnCellNo && !hdnCellNo.equals("")) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					hdnCellNo);
		}

		if (null != hdnPersonType && !hdnPersonType.equals("")) {
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
					hdnPersonType);
		}
		logger.logInfo("[readParams() .. Ends]");
	}

	public Boolean getReadOnlyMode() {
		return !getIsNewStatus() && !getIsReviewed()
				&& !getIsApprovalRequired();
	}

	public String getReadOnlyClass() {
		if (getReadOnlyMode()) {
			return CSS_READONLY;
		}
		return "";
	}

	public String getBeneficiaryCmbCss() {
		if (!getIsNewStatus()) {
			return CSS_READONLY;
		}
		return "";
	}

	public List<SelectItem> getBeneficiaryList() {
		if (viewMap.containsKey(BENENEFICIARY_LIST)) {
			return (List<SelectItem>) viewMap.get(BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>(0);
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}

	public List<DisbursementDetailsView> getDisbursementDetails() {
		if (viewMap.containsKey(DISBURSEMENTS)) {
			return (List<DisbursementDetailsView>) viewMap.get(DISBURSEMENTS);
		}
		return new ArrayList<DisbursementDetailsView>();
	}

	public void setDisbursementDetails(List<DisbursementDetailsView> list) {
		@SuppressWarnings("unused")
		List<DisbursementDetailsView> oldView = (List<DisbursementDetailsView>) viewMap
				.remove(DISBURSEMENTS);
		oldView = null;
		viewMap.put(DISBURSEMENTS, list);
	}

	public List<DisbursementDetailsView> getToDeleteList() {
		if (viewMap.containsKey(DELETED_DISBURSEMENT)) {
			return (List<DisbursementDetailsView>) viewMap
					.get(DELETED_DISBURSEMENT);
		}
		return new ArrayList<DisbursementDetailsView>(0);
	}

	public Boolean getIsFileAssociated() {
		if (viewMap.containsKey(IS_FILE_ASSOCIATED)) {
			return ((String) viewMap.get(IS_FILE_ASSOCIATED)).equals("TRUE");
		}
		return false;
	}

	private void saveAttachmentsAndComments(Long reqId)
			throws PimsBusinessException {
		loadAttachmentsAndComments(reqId);
		CommonUtil.saveAttachments(reqId);
		String noteOwner = (String) viewMap.get("noteowner");
		CommonUtil.saveComments(reqId, noteOwner);
	}

	public void loadAttachmentsAndComments(Long requestId) {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(
				PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = viewMap.get(EXTERNAL_ID).toString();
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);

		if (null != requestId) {
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	private void handleAttachmentCommentTabStatus(RequestView view) {
		if (view.getStatusId().equals(getStatusCompleteId()) || view.getStatusId().equals( getStatusRejectedId())) {
			viewMap.put("canAddAttachment", false);
			viewMap.put("canAddNote", false);
		} else {
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
		}
	}

	private void resetNonMaturedSharesViewToDetault() {
		txtDescription.setValue("");
		txtAmount.setValue("");
		cmbBeneficiaryList.setDisabled(false);
		if (getIsFileAssociated()) {
			cmbBeneficiaryList.setValue("-1");
		}
	}

	private ReviewRequestView getReviewRequestVO() {
		ReviewRequestView reviewReqVO = new ReviewRequestView();
		reviewReqVO.setRequestId(getRequestView().getRequestId());
		reviewReqVO.setCreatedBy(CommonUtil.getLoggedInUser());
		reviewReqVO.setCreatedOn(new Date());
		reviewReqVO.setRfc(txtReviewComments.getValue().toString());
		reviewReqVO.setGroupId(cmbUserGroups.getValue().toString());
		return reviewReqVO;
	}

	private DisbursementDetailsView getDefaultDisbDetailView()
	{
		DisbursementDetailsView ddView = new DisbursementDetailsView();
		ddView.setCreatedBy(CommonUtil.getLoggedInUser());
		ddView.setCreatedOn(new Date());
		ddView.setUpdatedBy(CommonUtil.getLoggedInUser());
		ddView.setUpdatedOn(new Date());
		ddView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		ddView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		ddView.setHasInstallment( WebConstants.HAS_INSTALLMENT_DEAFULT);
		ddView.setIsDisbursed(0L);
		DomainDataView ddStatus = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.DisbursementStatus.DOMAIN_KEY),
				WebConstants.DisbursementStatus.NEW);
		ddView.setStatus( ddStatus.getDomainDataId() );
		ddView.setStatusEn( ddStatus.getDataDescEn() );
		ddView.setStatusAr( ddStatus.getDataDescAr() );
		Long srcTypeid = CommonUtil.getDomainDataId( WebConstants.PaymentSrcType.DOMAIN_KEY, WebConstants.PaymentSrcType.SELF_ACCOUNT );
		ddView.setSrcType(srcTypeid);
		DomainDataView ddv = CommonUtil.getDomainDataView( WebConstants.PaymentSrcType.DOMAIN_KEY, srcTypeid );
		ddView.setSrcTypeAr( ddv.getDataDescAr() );
		ddView.setSrcTypeEn( ddv.getDataDescEn() );

		return ddView;
	}

	private void loadBeneficiaryList(BeneficiaryGridView beneficiaryGridView) throws Exception
	{
		Map<Long, String> benefNoCostCenterMap = new HashMap<Long, String>();
		ArrayList<SelectItem> beneficiary = new ArrayList<SelectItem>();
		beneficiary.add(new SelectItem(beneficiaryGridView.getBeneficiaryPersonId().toString(), beneficiaryGridView.getPersonName()));
//		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Double amount =0.0d;
		if( beneficiaryGridView.getCostCenter() != null &&  beneficiaryGridView.getCostCenter().trim().length() >0  )
		{
			amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryGridView.getBeneficiaryPersonId(),getRequestView().getInheritanceFileId());
	//		Double amount = accountWS.getAmountByPersonId( beneficiaryGridView.getBeneficiaryPersonId());
			personBalanceMap.put( beneficiaryGridView.getBeneficiaryPersonId(), amount);
	//		accountWS = null;
			txtBalance.setValue( amount.toString());
		}
		else
		{
				benefNoCostCenterMap.put(beneficiaryGridView.getBeneficiaryPersonId(),beneficiaryGridView.getBeneficiaryName() ); 		
		}
		Map<Long, Double> personBlockBalanceMap = new HashMap<Long, Double>();
		
		Double personBlockBalanceAmount = BlockingAmountService.getBlockingAmount( beneficiaryGridView.getBeneficiaryPersonId(), null, null, null, null );
		personBlockBalanceMap.put( beneficiaryGridView.getBeneficiaryPersonId(), personBlockBalanceAmount);
		blockingBalance.setValue( personBlockBalanceAmount.toString()  );
		
		Double requestAmount = DisbursementService.getRequestedAmount(
																		beneficiaryGridView.getBeneficiaryPersonId(), 
																		getRequestView().getInheritanceFileId(), 
																		getRequestView().getRequestId()
																		,null
																	   );
		requestedAmount.setValue( requestAmount );
		
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		availableBalance.setValue(   oneDForm.format( amount- (personBlockBalanceAmount+requestAmount)) );
		viewMap.put( PERSON_BLOCKING_MAP, personBlockBalanceMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiary);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
	}


	private void loadBeneficiaryList(final Long fileId) throws Exception 
	{
		InheritedAssetService inheritedAssetWS = new InheritedAssetService();
		List<BeneficiariesPickListView> beneficiaresPickList = inheritedAssetWS.getBeneficiariesView(fileId);

		ArrayList<SelectItem> beneficiaries = new ArrayList<SelectItem>();
		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Map<Long, String> benefNoCostCenterMap = new HashMap<Long, String>();
		Map<Long, Double> personBlockBalanceMap = new HashMap<Long, Double>();
		for (BeneficiariesPickListView singleBeneficiary : beneficiaresPickList) 
		{
			SelectItem selItem = new SelectItem(singleBeneficiary.getPersonId().toString(), singleBeneficiary.getFullName());
			beneficiaries.add(selItem);
			if(	singleBeneficiary.getCostCenter() == null )
			{
				benefNoCostCenterMap.put(singleBeneficiary.getPersonId(),singleBeneficiary.getFullName() ); 		
			}
			else
			{
				Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(singleBeneficiary.getPersonId(),getRequestView().getInheritanceFileId());
	//			Double amount = accountWS.getAmountByPersonId( singleBeneficiary.getPersonId());
				personBalanceMap.put( singleBeneficiary.getPersonId(), amount);
				
				Double personBlockBalanceAmount = BlockingAmountService.getBlockingAmount( singleBeneficiary.getPersonId(), null ,null, null, null);
				personBlockBalanceMap.put( singleBeneficiary.getPersonId(), personBlockBalanceAmount);
			}
			

		}
		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
		accountWS = null;
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put( PERSON_BLOCKING_MAP, personBlockBalanceMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiaries);
	}

	private void handleFinanceTabVisibility() {
		logger.logInfo("[handleFinanceTableVisibility .. Starts]");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_PAYMENT_TYPE, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_REASON, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_ITEMS, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_DIS_SRC, "");
		logger.logInfo("[handleFinanceTableVisibility .. Ends]");
	}

	public String getInvestmentTypeCss() {
		if (getIsNewStatus()) {
			return StringHelper.EMPTY;
		}
		return CSS_READONLY;
	}

	public void setRequestView(RequestView reqView) {
		@SuppressWarnings("unused")
		RequestView oldView = (RequestView) viewMap.remove(WebConstants.REQUEST_VIEW);
		oldView = null;
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
	}

	private RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}

	public Boolean getIsInvalidStatus() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(INVALID_STATUS);
	}

	public Boolean getIsReviewed() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusReviewedId());
	}

	public Boolean getIsReviewRequired() {
		Long reqId = this.getRequestView().getStatusId();
		
		
		
		return reqId.equals(getStatusReviewRequiredId());
	}

	public Boolean getIsApproved() {
		Long statusId = this.getRequestView().getStatusId();
		return statusId.equals(getStatusApprovedId());
	}

	public Boolean getIsApprovalRequired() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusApprovalRequiredId());
	}

	public Boolean getIsNewStatus() {
		Long reqId = this.getRequestView().getStatusId();
		return (reqId.equals(INVALID_STATUS))
				|| (reqId.equals(getStatusNewId()));
	}

	public Boolean getIsUserInGroup() {
		String userInGrp = ((String) viewMap.get(USER_IN_GROUP));
		if (null == userInGrp) {
			return false;
		}
		return userInGrp.equals("TRUE");
	}

	public void setUserInGroup() {
		if (getIsReviewRequired()) {
			User loggedInUser = getLoggedInUserObj();
			if (loggedInUser.hasUserGroup(getRequestView()
					.getMemsNolReviewGrpId())) {
				viewMap.put(USER_IN_GROUP, "TRUE");
			}
		}
	}

	private Long getStatusNewId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_NEW)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_NEW);
	}

	private Long getStatusCompleteId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_COMPLETE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
	}

	private Long getStatusApprovedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVED);
	}

	private Long getStatusRejectedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED);
	}

	private Long getStatusApprovalRequiredId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap
				.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
	}

	private Long getStatusReviewRequiredId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED);
	}

	private Long getStatusReviewedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_DONE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_DONE);
	}

	@SuppressWarnings("unchecked")
	private Boolean invokeBpel(Long requestId) {
		 logger.logInfo("invok" + "eBpel() started...");		 		
		 Boolean success = true;
		 try {
			 MEMSInvestmentRequestBPELPortClient bpelClient = new
			 MEMSInvestmentRequestBPELPortClient();
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint =
			 parameters.getParameter(WebConstants.MemsBPELEndPoints.MEMS_INVESTMENT_END_POINT);
			 bpelClient.setEndpoint( endPoint);
			 String userId = CommonUtil.getLoggedInUser();
			 bpelClient.initiate(userId, requestId.toString(), null, null);
			 logger.logInfo("invokeBpel() completed successfully!!!");
		 } catch(PIMSWorkListException exception) {		
			 success = false;
			 logger.LogException("invokeBpel() crashed ", exception);
		 } catch (Exception exception) {		 
			 success = false;
			 logger.LogException("invokeBpel() crashed ", exception);
		 } finally {		 
			 if(!success) {
				 errorMessages.clear();
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 }
		 }
		return true;
	}

	@SuppressWarnings("unchecked")
	private Boolean completeTask(TaskOutcome taskOutcome) throws Exception {
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext)
		getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");		 
		logger.logInfo("Contextpath is: " + contextPath);
		String loggedInUser = CommonUtil.getLoggedInUser();
		if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		} else {
			userTask = getIncompleteRequestTasks();
		}
		 
		 BPMWorklistClient client = new BPMWorklistClient(contextPath);
		 logger.logInfo("UserTask is: " + userTask.getTaskType());
		 if(userTask!=null) {
			 client.completeTask(userTask, loggedInUser, taskOutcome);
		 }
		 logger.logInfo("completeTask() completed successfully!!!");
		 return success;
	}
	
	public void populateFinanceData() 
	{
		try
		{
			FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
			if( null!=tabFinanceController ) {
				tabFinanceController.handleTabChange();
			}
		}
		catch (Exception ex) {
				logger.LogException( "[Exception occured in populateFinanceData()]", ex);
		}
	}

	public void getRequestHistory() {
		logger.logInfo("[getRequestHistory() .. Starts]");
		try {
			Long reqId = getRequestView().getRequestId();
			if (null != reqId && !reqId.equals(INVALID_STATUS)) {
				RequestHistoryController rhc = new RequestHistoryController();
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, reqId
						.toString());
			}
		} catch (Exception ex) {
			logger.logInfo("[Exception occured in getRequestHistory()]", ex);
			ex.printStackTrace();
		}
		logger.logInfo("[getRequestHistory() .. Ends]");
	}
	
	public void getReviewHistory() {
		logger.logInfo("[getReviewHistory() .. Starts]");

		logger.logInfo("[getReviewHistory() .. Ends]");
	}
	
	public UserTask getIncompleteRequestTasks() {
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";		 
		try {
	   		Long requestId = getRequestView().getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();	   		
	   		if( null!=taskId ) {	   		
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);				 
				taskType = userTask.getTaskType();
				logger.logInfo("Task Type is:" + taskType);
				return userTask;
	   		} else {	   		
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		} catch(PIMSWorkListException ex) {				
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {			
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC) {			
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else {			
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		} catch (Exception exception) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		return null;
	}

	public HtmlInputText getTxtBalance() {
		return txtBalance;
	}

	public void setTxtBalance(HtmlInputText txtBalance) {
		this.txtBalance = txtBalance;
	}

	public HtmlInputText getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(HtmlInputText totalAmount) {
		this.totalAmount = totalAmount;
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlFileType() {
		return htmlFileType;
	}

	public void setHtmlFileType(HtmlInputText htmlFileType) {
		this.htmlFileType = htmlFileType;
	}

	public HtmlInputText getHtmlFileStatus() {
		return htmlFileStatus;
	}

	public void setHtmlFileStatus(HtmlInputText htmlFileStatus) {
		this.htmlFileStatus = htmlFileStatus;
	}

	public HtmlInputText getHtmlFileDate() {
		return htmlFileDate;
	}

	public void setHtmlFileDate(HtmlInputText htmlFileDate) {
		this.htmlFileDate = htmlFileDate;
	}			
	
	public void openAvailableBankTransfersPopUp(javax.faces.event.ActionEvent event)
	{
		if( !cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL)) 
		{
			Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
			
			String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 250;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('AvailableBankTransfers.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.PERSON_ID
				+ "="
				+ beneficiaryId
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
			sendToParent(javaScriptText);
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.AVAILABLE_BALANCE_ERROR_BENEF_MISSING));
		}
		
	}
	public String executeJavascript(String javaScript) {
		String methodName = "executeJavascript";
		 logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
    
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) {
    	
    	DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
		String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 550;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.PERSON_ID+ "="+ gridViewRow.getPersonId()
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
		sendToParent(javaScriptText);

	}
    public void openAssociateCostCenters() 
    {
    	final String methodName = "openAssociateCostCenters";
    	try
    	{
    	
    	DisbursementDetailsView gridViewRow = ( DisbursementDetailsView ) disbursementTable.getRowData();
		String javaScriptText = "javaScript:openAssociateCostCenters();";
        sessionMap.put( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW, gridViewRow );
        if( gridViewRow.getRequestBeneficiaries() != null &&  gridViewRow.getRequestBeneficiaries().size() > 0 )
        {
        	
        	sessionMap.put( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW, gridViewRow.getRequestBeneficiaries().get( 0 ) );
        	
        }
		sendToParent( javaScriptText );
    	}
    	catch( Exception e )
    	{
    		logger.LogException( methodName +"Error Occured:", e );
    		errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

    	}
	}

	public HtmlInputText getBlockingBalance() {
		return blockingBalance;
	}

	public void setBlockingBalance(HtmlInputText blockingBalance) {
		this.blockingBalance = blockingBalance;
	}
	


}
