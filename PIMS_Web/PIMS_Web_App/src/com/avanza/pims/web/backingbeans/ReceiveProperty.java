package com.avanza.pims.web.backingbeans;


import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.bpel.proxy.PIMSRecivePropertyBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.CostCenterFields;
import com.avanza.pims.dao.CostCenterGenerator;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.ProjectDetailsView;
import com.avanza.pims.ws.vo.PropertyFacilityView;
import com.avanza.pims.ws.vo.PropertyOwnerView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.ReceivePropertyFloorView;
import com.avanza.pims.ws.vo.ReceivePropertyPropertyView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.UnitTypeDescView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class ReceiveProperty extends AbstractController {
	private String endowmentId;
	//Base Screen
	private final String PROPERTY_STATUS_UNDER_RECEIVING = "PROPERTY_STATUS_UNDER_RECEIVING";
	private final String PROPERTY_STATUS_UNDER_EVALUATION = "PROPERTY_STATUS_UNDER_EVALUATION";
	private final String PROPERTY_STATUS_RECEIVED = "PROPERTY_STATUS_RECEIVED";
	private List<String> messages = new ArrayList<String>();
	private List<String> errorMessages=  new ArrayList<String>();
	private HtmlInputHidden hiddenPropertyValue = new HtmlInputHidden();
	private HtmlInputText propertyReferenceNumber = new HtmlInputText();
	private HtmlInputText financialAccountNumber = new HtmlInputText();
	private HtmlInputText endowedName  = new HtmlInputText();
	private HtmlInputText propertyNameEn  = new HtmlInputText();
	private HtmlInputText commercialName =new HtmlInputText();
	private HtmlGraphicImage projectImage=new HtmlGraphicImage();
	private HtmlInputText landNumber = new HtmlInputText();
	private HtmlInputText departmentCode = new HtmlInputText();
	private HtmlInputText municipalityPlanNumber = new HtmlInputText();
	private HtmlInputText projectNumber = new HtmlInputText();
	private HtmlInputText landArea = new HtmlInputText();
	private HtmlInputText builtInArea = new HtmlInputText();
	private HtmlInputText statusText = new HtmlInputText();
	private HtmlInputText numberOfLifts = new HtmlInputText();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton saveAfterReceivedButton = new HtmlCommandButton();
	private HtmlCommandButton submitButton = new HtmlCommandButton();
	private HtmlCommandButton generateFloorButton = new HtmlCommandButton();
	private HtmlCommandButton generateUnitButton = new HtmlCommandButton();
	private HtmlInputHidden floorHiddenId = new HtmlInputHidden();
	private HtmlInputHidden unitHiddenId = new HtmlInputHidden();
	private HtmlInputHidden unitStatusId = new HtmlInputHidden();
	private HtmlInputText accountNumber = new HtmlInputText();
	private HtmlInputText endowedMinorNo = new HtmlInputText();
	private HtmlInputText ewUtilityNo = new HtmlInputText();
	private HtmlInputText buildingCoordinates = new HtmlInputText();
//	private Date contractDate = new Date();
	private HtmlInputText totalNoOfFlats = new HtmlInputText();
	private HtmlInputText totalNoOfUnits = new HtmlInputText();
	private HtmlInputText totalNoOfFloors = new HtmlInputText();
	private HtmlInputText totalNoOfParkingFloors = new HtmlInputText();
	private HtmlInputText totalAnnualRent = new HtmlInputText();

	private HtmlInputText totalMezzanineFloors = new HtmlInputText();
	private HtmlSelectOneMenu ownershipType = new HtmlSelectOneMenu();
	private List<SelectItem> ownershipItems = new ArrayList<SelectItem>();
	private boolean showChangeRentTab;
	DomainDataView floorTypeGround = new DomainDataView();
	DomainDataView floorTypeBasement = new DomainDataView();
	DomainDataView floorTypeRoofTop = new DomainDataView();
	DomainDataView floorTypeMezzanine = new DomainDataView();
	DomainDataView floorTypeTop = new DomainDataView();



	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu cityMenu = new HtmlSelectOneMenu();
	
	private String selectedCountryId;
	private String stateId;
	private String cityId;
	private String countryId;
	private Boolean showTabs;
	private HtmlInputHidden showTabsHidden =new HtmlInputHidden();

	private Boolean showDocNComTabs;
	private List<SelectItem> CRStateList = new ArrayList<SelectItem>();
	private List<SelectItem> CRCityList = new ArrayList<SelectItem>();
	String CRCountryId;
	String CRStateId;
	String CRCityId;

	private Boolean deleteAction ;
	private HtmlSelectOneMenu propertyType = new HtmlSelectOneMenu();
	private List<SelectItem> propertyItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu propertyCategory  = new HtmlSelectOneMenu();
	private List<SelectItem> categoryItem = new ArrayList<SelectItem>();
	ContactInfoView location = new ContactInfoView();
	private HtmlSelectOneMenu propertyUsage = new HtmlSelectOneMenu();
	private List<SelectItem> usageItems  = new ArrayList<SelectItem>();



	private HtmlSelectOneMenu proeprtyInvestmentPurpose = new HtmlSelectOneMenu();
	private List<SelectItem> investmentPurposes = new ArrayList<SelectItem>();
	//Comment Tab
	private HtmlCommandButton saveCommentButton = new HtmlCommandButton();

	//Attachment Tab
	private HtmlCommandButton saveAttachmentsButton = new HtmlCommandButton();


	//Location Screen
	
	private boolean onRentValue = true;

	private HtmlInputText addressLineOne = new HtmlInputText();
	private HtmlInputHidden contactInfoId = new HtmlInputHidden();

	private HtmlInputText addressLineTwo = new HtmlInputText();
	private HtmlInputText homePhone = new HtmlInputText();
	private HtmlInputText postalCode = new HtmlInputText();
	private HtmlInputText streetInformation = new HtmlInputText();



	private HtmlCommandButton locationSaveButton = new HtmlCommandButton();

	//Owners Screen
	private HtmlInputText ownerReferenceNo = new HtmlInputText();
	private String ownerRefNo;
	private String representativeRefNo;
	private HtmlInputText ownerRepresentative = new HtmlInputText();
	private HtmlInputHidden ownerHiddenId = new HtmlInputHidden();
	private HtmlInputHidden representativeHiddenId = new HtmlInputHidden();

	private HtmlInputText ownerPercentage = new HtmlInputText();
	private HtmlDataTable ownerDataGrid = new HtmlDataTable();
	private List<PropertyOwnerView> ownerDataList;
	private HtmlCommandButton ownerSaveButton = new HtmlCommandButton();
	private HtmlGraphicImage ownerSearchButton = new HtmlGraphicImage();
	private HtmlGraphicImage representativeSearchButton = new HtmlGraphicImage();
	//Property Floor N Unit TabScreen
	private boolean showRentTabs = false;
	//Property Tab Screen
	private HtmlInputText rentValue = new HtmlInputText();
	private HtmlCommandButton propertyRentSaveButton = new HtmlCommandButton();
	private HtmlOutputText propertyTabCategory = new HtmlOutputText();
	private HtmlOutputText propertyDepositAmount = new HtmlOutputText();
	private HtmlOutputText propertyRentProcess = new HtmlOutputText();
	private HtmlOutputText propertyGracePeriod = new HtmlOutputText();
	private HtmlOutputText propertyGracePeriodType = new HtmlOutputText();
	private HtmlOutputText propertyAuctionOpeningPrice  = new HtmlOutputText();
	private HtmlSelectOneMenu propertyGracePeriodTypeSelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> propertyGracePeriodTypeItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu propertyCategorySelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> propertyCategoryItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu propertyRentProcessSelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> propertyRentProcessItems = new ArrayList<SelectItem>();
	private HtmlInputText propertyDepositAmountText = new HtmlInputText();
	private HtmlInputText propertyGracePeriodText = new HtmlInputText();
	private HtmlInputText propertyAuctionOpeningPriceText = new HtmlInputText();

	//Facilities Tab

	private HtmlInputText charges = new HtmlInputText();

	private String facilityId;
	private String facilityTypeId;
	private List<SelectItem> facilityTypeList = new ArrayList<SelectItem>();
	private List<SelectItem> facilityList = new ArrayList<SelectItem>();
	private HtmlInputText facilityDescription = new HtmlInputText();
	private HtmlDataTable facilityGrid = new HtmlDataTable();


	private HtmlGraphicImage searchFacilityButton = new HtmlGraphicImage();
	private HtmlCommandButton saveFacilityButton = new HtmlCommandButton();
	private List<PropertyFacilityView> facilityDataList ;

	//Floor Tab Screen
	private HtmlSelectOneMenu afterFloorMenu = new HtmlSelectOneMenu();
	private HtmlOutputText floorCategory = new HtmlOutputText();
	private HtmlOutputText floorDepositAmount = new HtmlOutputText();
	private HtmlOutputText floorRentProcess = new HtmlOutputText();
	private HtmlOutputText floorGracePeriod = new HtmlOutputText();
	private HtmlOutputText floorAuctionOpeningPrice  = new HtmlOutputText();
	private HtmlOutputText floorGracePeriodType = new HtmlOutputText();
	private HtmlSelectOneMenu floorCategorySelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> floorCategoryItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu floorTypeSelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> floorTypeItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu floorRentProcessSelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> floorRentProcessItems = new ArrayList<SelectItem>();
	private HtmlInputText floorDepositAmountText = new HtmlInputText();
	private HtmlInputText floorGracePeriodText = new HtmlInputText();
	private HtmlInputText floorAuctionOpeningPriceText = new HtmlInputText();
	private HtmlSelectOneMenu floorGracePeriodTypeSelectOneMenu = new HtmlSelectOneMenu();
	private List<SelectItem> floorGracePeriodItems = new ArrayList<SelectItem>();
	private HtmlInputText floorPrefix = new HtmlInputText();
	private HtmlInputTextarea floorDescription = new HtmlInputTextarea();
	private HtmlInputText floorSeperator = new HtmlInputText();
	private HtmlInputText floorFrom = new HtmlInputText();
	private HtmlInputText floorTo = new HtmlInputText();
	private HtmlCommandButton addFloorButton = new HtmlCommandButton();
	private HtmlCommandButton duplicateSelectedFloor = new HtmlCommandButton();

	private HtmlInputText floorNumber = new HtmlInputText();
	private HtmlInputText rentValueForFloors = new HtmlInputText();
	private HtmlCommandButton floorRentSaveButton = new HtmlCommandButton();
	private HtmlCommandButton floorDeleteButton = new HtmlCommandButton();
	private HtmlCommandButton unitDeleteButton = new HtmlCommandButton();
	private HtmlDataTable floorTable = new HtmlDataTable();
	private List<ReceivePropertyFloorView> floorsDataList = new ArrayList<ReceivePropertyFloorView>();



	//Unit Tab Screen
	private HtmlSelectOneMenu floorNumberMenu = new HtmlSelectOneMenu();
	
	private List<SelectItem> floorsList = new ArrayList<SelectItem>();
	private HtmlInputText unitSeperator = new HtmlInputText();
	private HtmlInputText unitNumber = new HtmlInputText();
	private HtmlDataTable unitTable = new HtmlDataTable();
	private List<ReceivePropertyUnitView> unitDataList = new ArrayList<ReceivePropertyUnitView>();
	private HtmlSelectOneMenu unitSideMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitSideList = new ArrayList<SelectItem>();

	private HtmlInputText unitStatus= new HtmlInputText();
	private HtmlSelectOneMenu unitInvestmentTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitInvestmentTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu unitTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu unitUsageTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitUsageTypeList = new ArrayList<SelectItem>();
	private HtmlInputText unitNo = new HtmlInputText();
	private HtmlInputText unitPrefix = new HtmlInputText();

	private HtmlInputText unitDescription = new HtmlInputText();
	private HtmlInputText bedRooms = new HtmlInputText();
	private HtmlInputText bathRooms = new HtmlInputText();
	private HtmlInputText areaInSquare = new HtmlInputText();
	private HtmlInputText livingRooms = new HtmlInputText();
	private HtmlInputText unitEWUtilityNumber = new HtmlInputText();
	private HtmlInputText rentValueForUnits = new HtmlInputText();
	private HtmlInputTextarea unitRemarks= new HtmlInputTextarea();
	private HtmlSelectBooleanCheckbox includeFloorNumber = new HtmlSelectBooleanCheckbox();
	private Boolean includeFloorNumberVar = new Boolean(false);
	private HtmlCommandButton addUnitButton = new HtmlCommandButton();


	//Receiving Team
	private HtmlDataTable receivingTeamGrid = new HtmlDataTable();
	private List<UserView> receivingDataList = new ArrayList<UserView>();
	private HtmlCommandButton addReceiveingTeam = new HtmlCommandButton();
	private HtmlCommandButton addEvaluationTeam = new HtmlCommandButton();


	List<SelectItem> receivingTeamList = new ArrayList<SelectItem>(); 
	List selectedReceivingTeamList = new ArrayList();
	List<SelectItem> usersReceivingList = new ArrayList<SelectItem>(); 
	List selectedUsersReceivingList = new ArrayList();
	private HtmlCommandButton saveReceivingTeamButton = new HtmlCommandButton();
	//Evaluation Team
	List<SelectItem> evaluationTeamList = new ArrayList<SelectItem>(); 
	List selectedEvaluationTeamList = new ArrayList();
	List<SelectItem> usersEvaluationList = new ArrayList<SelectItem>(); 
	List selectedUsersEvaluationList = new ArrayList();
	private HtmlCommandButton saveEvaluationTeamButton = new HtmlCommandButton();
	private HtmlDataTable evaluationTeamGrid = new HtmlDataTable();
	private List<UserView> evaluationDataList = new ArrayList<UserView>();

	//Tabs
	private HtmlTabPanel richTabPanel = new HtmlTabPanel();

	//Calendar
//	private HtmlCalendar lastContractDate = new HtmlCalendar();
	//Modes
	private HtmlCommandButton submitAfterEvaluation = new HtmlCommandButton();
	private HtmlCommandButton approveEvaluation = new HtmlCommandButton();
	private HtmlCommandButton reviewApproveEvaluation = new HtmlCommandButton();
	private HtmlCommandButton rejectEvaluation = new HtmlCommandButton();
	private HtmlCommandButton approveRentCommittee = new HtmlCommandButton();
	private HtmlCommandButton btnUpdateUnitAtApproval = new HtmlCommandButton();
	Map viewRootMap=getFacesContext().getViewRoot().getAttributes();

	public static final Double minimumFloorSequence = new Double(1);
	public static final Double maximumFloorSequence = new Double(100);

	//Commons
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

	ResourceBundle bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());
	SystemParameters parameters = SystemParameters.getInstance();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(ReceiveProperty.class);
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	ApplicationBean applicationBean = new ApplicationBean();
	
	
	private HtmlInputHidden hiddenCounter= new HtmlInputHidden();

	private String teamId;
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	DomainDataView domainDataView = new DomainDataView();
	ContactInfoView contactInfoView = new ContactInfoView();
	ReceivePropertyFloorView receivePropertyFloorView =new ReceivePropertyFloorView();
	ReceivePropertyUnitView receivePropertyUnitView = new ReceivePropertyUnitView();

	PropertyView propertyView = new PropertyView();
	ReceivePropertyPropertyView propertyViewWithFloorsAndUnits = new ReceivePropertyPropertyView();
	private String  hdnprojectNumber;
	private Long hdnprojectId;
	 Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private List<DomainDataView> ddPropertyStatusList = new ArrayList<DomainDataView>(0);
	private HtmlInputText txtEndowmentRef = new HtmlInputText();
	private HtmlInputText txtAssetNumber = new HtmlInputText();
	private HtmlInputText txtAssetNameEn = new HtmlInputText();
	private HtmlInputText txtAssetNameAr = new HtmlInputText();
	private HtmlInputText txtAssetDesc = new HtmlInputText();
	private boolean isShowAssetControls;
	 private boolean isUnderReceiving;
	 private boolean isUnderEvaluation;
	 private boolean isAllowedToUpdate;
	 private boolean isPropertyReceived;
	 private boolean isApprovalTask;
	 private HtmlGraphicImage imgSearchAsset=new HtmlGraphicImage();
	 private HtmlGraphicImage imgSearchEndowment=new HtmlGraphicImage();
	 private String newCC;
	 private String oldCC;
	 private interface FloorType{
		 String  DD_FLOOR_TYPE_BASEMENT = "DD_FLOOR_TYPE_BASEMENT";
		 String  DD_FLOOR_TYPE_GROUND = "DD_FLOOR_TYPE_GROUND";
		 String  DD_FLOOR_TYPE_MEZZANINE = "DD_FLOOR_TYPE_MEZZANINE";
		 String  DD_FLOOR_TYPE_TOP_FLOOR = "DD_FLOOR_TYPE_TOP_FLOOR";
		 String  DD_FLOOR_TYPE_ROOF_TOP = "DD_FLOOR_ROOF_TOP";
	 }
	 
	 
	 private enum FloorPrefix {
		 DEFAULT(0), BASEMENT(1), GROUND(2), MEZZANINE(3), TOP(4), ROOF(5);

		 private int prefix;

		 private FloorPrefix(int px) {
		   prefix = px;
		 }

		 public int getPrefix() {
		   return prefix;
		 }
	 }
	 
	@SuppressWarnings("unchecked")
	public Integer getUnitRecordSize(){

		if(viewRootMap.containsKey("UNITS_DATA_LIST"))
			unitDataList = (ArrayList<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST");

		return unitDataList.size();
	}
	@SuppressWarnings("unchecked")
	
	public Integer getFloorRecordSize(){

		if(viewRootMap.containsKey("FLOORS_DATA_LIST"))
			floorsDataList = (ArrayList<ReceivePropertyFloorView>)viewRootMap.get("FLOORS_DATA_LIST");

		return floorsDataList.size();
	}
	public Integer getOwnerRecordSize(){

		return ownerDataList.size();
	}
	public Integer getFacilityRecordSize(){

		return facilityDataList.size();
	}
	public Integer getReceivingTeamRecordSize(){

		return receivingDataList.size();
	}
	public Integer getEvaluationTeamRecordSize(){

		return evaluationDataList.size();
	}

	@SuppressWarnings("unchecked")
	private void getEndowmentDetailsFromSessionMap() throws Exception
	{
		Endowment endowment = (Endowment) sessionMap.remove(Constant.Endowments.ENDOWMENT);
		viewMap.put( Constant.EndowmentFile.ENDOWMENT_FILE_ID, sessionMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ID) );
		setEndowmentId( endowment.getEndowmentId().toString() );
		ownershipType.setDisabled(true);
		if( sessionMap.get( "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			viewMap.put(  
						   "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ,
						   sessionMap.remove(  "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ) 
			           );
		}
		if( endowment.getPropertyId() != null )
		{
			  PropertyView propertyView =  new PropertyService().getPropertyByID( endowment.getPropertyId() );
			  sessionMap.put(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY,propertyView);
			  return;
		}
		else
		{
			loadCountry();
			countryId = WebConstants.Region.UAE;
			loadState(WebConstants.Region.UAE_ID);
			stateId = WebConstants.Region.DUBAI;
			loadCity( WebConstants.Region.DUBAI_ID );
		}
		
		txtEndowmentRef.setValue( endowment.getEndowmentNum() );
		endowedName.setValue(endowment.getEndowmentName());
		ownershipType.setValue( String.valueOf( WebConstants.OwnerShipType.ENDOWED_ID ) );
		if( endowment.getCostCenter() != null && endowment.getCostCenter().trim().length() > 0  )
		{
			newCC = endowment.getCostCenter();
		}
		if( endowment.getFielDetials1() != null && endowment.getFielDetials1().trim().length() > 0  )
		{
			landNumber.setValue( endowment.getFielDetials1() );
		}
		if( endowment.getFielDetials3() != null && endowment.getFielDetials3().trim().length() > 0  )
		{
			addressLineOne.setValue( endowment.getFielDetials3() );
		}
		if( endowment.getFielDetials4() != null && endowment.getFielDetials4().trim().length() > 0  )
		{
			landArea.setValue( endowment.getFielDetials4() );
		}
	}
	
	@SuppressWarnings("unchecked")
	public String onBack()
	{
		
		try	
		{	
			 if( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ID ) != null )
			 {
				 sessionMap.put(
						 		Constant.EndowmentFile.ENDOWMENT_FILE_ID , 
						  		viewMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ID ) 
						  	   );
				 if( viewMap.get( "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
				 { 
						sessionMap.put(  
									   WebConstants.TASK_LIST_SELECTED_USER_TASK ,
									   viewMap.get(  "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ) 
						              );
				 }
				 return "endowment";
			 }
			 
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_SAVE_ERROR ) );
			logger.LogException( "onBack --- CRASHED --- ", exception);			
		}
		
		return "";
	}
	@SuppressWarnings("unchecked")
	public Boolean getShowBackButton()
	{
		Boolean show = false;
		
		if ( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ID)!= null )
		{
			show = true;
		}
			
		
		return show;
	}
	
   @SuppressWarnings("unchecked")
	public void init()
   {
		try 
		{
			addUnitButton.setValue(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_ADD_VALUE));
			setRequestParam("receivePropertyHeader", bundle.getString("receiveProperty.title"));
			receivingDataList = new ArrayList<UserView>();
			evaluationDataList = new ArrayList<UserView>();

			if(viewRootMap.containsKey("new")&& viewRootMap.get("new").toString().equals("true"))
			   inventoryMode();			
			
			if (!isPostBack()) 
			{
				
				if( sessionMap.get( Constant.Endowments.ENDOWMENT ) != null )
				{
					getEndowmentDetailsFromSessionMap();
				}
				List<DomainDataView> ddPropertyStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_STATUS);
				this.setDdPropertyStatusList(ddPropertyStatusList);
				
				DomainDataView ddvUnderReceiving = new DomainDataView();
				DomainDataView ddvUnderEvaluation = new DomainDataView();
				DomainDataView ddvReceived = new DomainDataView();
				ddvUnderReceiving = CommonUtil.getIdFromType(ddPropertyStatusList, WebConstants.Property.DRAFT);
				ddvUnderEvaluation = CommonUtil.getIdFromType(ddPropertyStatusList, WebConstants.Property.UNDER_EVALUATION);
				ddvReceived = CommonUtil.getIdFromType(ddPropertyStatusList, WebConstants.Property.PROPERTY_STATUS_RECEIVED);
				
				if(ddvUnderEvaluation != null)
				{
					viewMap.put(PROPERTY_STATUS_UNDER_EVALUATION, ddvUnderEvaluation);
				}
				if(ddvUnderReceiving != null)
				{
					viewMap.put(PROPERTY_STATUS_UNDER_RECEIVING, ddvUnderReceiving);
				}
				if(ddvReceived != null)
				{
					viewMap.put(PROPERTY_STATUS_RECEIVED, ddvReceived);
				}
				viewMap.put("MINOR_DD_ID", WebConstants.OwnerShipType.MINOR_ID);
				removeSessionValues();
				HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
				
				if(request.getParameter("new")!=null)
					inventoryMode();
				
				if(sessionMap.get("ADD_RENT_AMOUNT_MODE") != null )
				{
					sessionMap.get("ADD_RENT_AMOUNT_MODE");
					viewMap.put("ADD_RENT_AMOUNT_MODE", sessionMap.get("ADD_RENT_AMOUNT_MODE"));
					sessionMap.remove("ADD_RENT_AMOUNT_MODE");
				}
				if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null  &&  sessionMap.get(WebConstants.FROM_TASK_LIST)==null)
				{
					sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,null);
					showTabsHidden.setValue(true);
				}
				normalMode();
				sessionMap.put(WebConstants.FROM_TASK_LIST,null);
            	if(sessionMap.get(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY)!=null)
            	{
    				getDataFromEditMode();
    				
            	}
            	else if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
    			{
    				fromTaskList();
    			}
            	
				if(propertyView.getPropertyId()!=null)
				{
					propertyViewWithFloorsAndUnits = propertyServiceAgent.getPropertyByIdWithFloorsAndUnits(propertyView.getPropertyId());
			        sessionMap.put(WebConstants.Property.PROPERTY_VIEW_WITH_FLOOR_AND_UNITS,propertyViewWithFloorsAndUnits);
				}
				getFloorTypeDetails();
            	loadAttachmentsAndComments(propertyView.getPropertyId());
			}

			loadTabsData();
			loadCombos();
			
			 
		} 
		catch (Exception e) 
		{
			logger.LogException( "init|Error Occured ",e);
		}
	}
 
    @SuppressWarnings("unchecked")
	private void loadTabsData()throws Exception
	{
		loadPropertyFacilityDataList(false);
		loadPropertyOwnersDataList(false);
		loadPropertyReceivingTeamDataList();
		loadPropertyEvaluationTeamDataList();
		loadPropertyUnitsDataList(true);
		loadPropertyFloorsDataList(false);
	}
	
   	@SuppressWarnings("unchecked")
	private void getFloorTypeDetails() throws Exception 
   	{
		floorTypeGround = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FLOOR_TYPE),WebConstants.FloorType.GROUND_FLOOR);
		floorTypeBasement = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FLOOR_TYPE),WebConstants.FloorType.BASEMENT_FLOOR);
		floorTypeMezzanine = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FLOOR_TYPE),WebConstants.FloorType.MEZZANINE_FLOOR);
		floorTypeRoofTop = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FLOOR_TYPE),WebConstants.FloorType.ROOF_TOP_FLOOR);
		floorTypeTop = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FLOOR_TYPE),WebConstants.FloorType.TOP_FLOOR);
		
		if(floorTypeGround != null )
			viewMap.put(FloorType.DD_FLOOR_TYPE_GROUND, floorTypeGround);
		
		if(floorTypeBasement != null )
			viewMap.put(FloorType.DD_FLOOR_TYPE_BASEMENT, floorTypeBasement);
		
		if(floorTypeMezzanine != null )
			viewMap.put(FloorType.DD_FLOOR_TYPE_MEZZANINE, floorTypeMezzanine);
		
		if(floorTypeTop != null )
			viewMap.put(FloorType.DD_FLOOR_TYPE_TOP_FLOOR, floorTypeTop);
		
		if(floorTypeRoofTop != null )
			viewMap.put(FloorType.DD_FLOOR_TYPE_ROOF_TOP, floorTypeRoofTop);
	}

   	@SuppressWarnings("unchecked")
	private void fromTaskList() throws PimsBusinessException 
	{
		showTabsHidden.setValue(true);
		UserTask task = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,task);
		setModeBasedOnTaskType(task);
		Map taskAttributes =  task.getTaskAttributes();
		long propertyId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.PROPERTY_ID));
		propertyView = propertyServiceAgent.getPropertyByID(propertyId);
		if(propertyView.getAssetId() != null)
		{
			viewMap.put("ASSET_ID", propertyView.getAssetId());
		}
		if(propertyView.getEndowmentId() != null)
		{
			setEndowmentId(propertyView.getEndowmentId().toString());
		}
		//sessionMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
		viewMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
		List<ReceivePropertyUnitView> unitList = new ArrayList<ReceivePropertyUnitView>();
		unitList = propertyServiceAgent.getPropertyUnitList(propertyId);
		if(unitList != null && unitList.size() > 0 && propertyView.getNoOfUnits() <= 0)
			propertyView.setNoOfUnits(Long.parseLong(Integer.toString(unitList.size())));
		//sessionMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
		propertyViewWithFloorsAndUnits = propertyServiceAgent.getPropertyByIdWithFloorsAndUnits(propertyId);
		fillValues(propertyView);
		viewRootMap.put(WebConstants.GenerateUnits.PROPERTY_VIEW,propertyView);
	}
   @SuppressWarnings("unchecked")
   private void getDataFromEditMode() throws Exception 
   {
	   String methodName = "getDataFromEditMode";
	   logger.logInfo(methodName+"|Start");
		draftMode();
		showTabsHidden.setValue(true);
		
		propertyView = (PropertyView) sessionMap.remove(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY);
		if(propertyView.getAssetId() != null)
		{
			viewMap.put("ASSET_ID", propertyView.getAssetId());
		}
		if(propertyView.getEndowmentId() != null)
		{
			setEndowmentId( propertyView.getEndowmentId().toString() );
		}
		List<ReceivePropertyUnitView> unitList = new ArrayList<ReceivePropertyUnitView>();
		
		unitList = propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());
		if(unitList != null && unitList.size() > 0 &&( propertyView.getNoOfUnits() == null || propertyView.getNoOfUnits() <= 0))
			propertyView.setNoOfUnits(Long.parseLong(Integer.toString(unitList.size())));
		viewMap.put("PROPERTY_ID_FOR_FETCHING_UNIT_TYPE", propertyView.getPropertyId());
		
		DomainDataView ddView = CommonUtil.getDomainDataFromId(this.getDdPropertyStatusList(), propertyView.getStatusId());
		if(
			!ddView.getDataValue().equals(WebConstants.Property.DRAFT) &&
			!ddView.getDataValue().equals(WebConstants.Property.NEW)
		  )
		{
			actionPerformedMode();
			showDocNComTabs = false;
		}
		if(!ddView.getDataValue().equals(WebConstants.Property.NEW))
		{
			showDocNComTabs = true;
		}
		viewMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
		
	
		fillValues(propertyView);
		
		logger.logInfo(methodName+"|Finish");
    
   }
   @SuppressWarnings("unchecked")
	private void setModeBasedOnTaskType(UserTask task) 
	{
		if(task!=null && task.getTaskType().equals(WebConstants.Property.EVALUATE_PROPERTY))
		{
			evaluationMode();
		}
		else if(task!=null && task.getTaskType().equals(WebConstants.Property.APPROVE_EVALUATION))
		{
			approvalMode();
		}
		else if(task!=null && task.getTaskType().equals(WebConstants.Property.REVIEW_APPROVE_EVALUATION))
		{
			approvalReviewMode();
		}
		else if(task!=null && task.getTaskType().equals(WebConstants.Property.CATEGORIZE_PROPERTY))
		{
			rentCommitteeMode();
		}
		
	}
	@SuppressWarnings("unchecked")
	private void removeSessionValues() 
	{
//		if(viewRootMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
//			sessionMap.put(WebConstants.Property.PROPERTY_VIEW,(PropertyView)viewRootMap.get(WebConstants.Property.PROPERTY_VIEW));
		if(sessionMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
			sessionMap.remove(WebConstants.Property.PROPERTY_VIEW);
		if(sessionMap.containsKey("SESSION_HASHMAP_RECEIVING_TEAM_USERS"))
			sessionMap.remove("SESSION_HASHMAP_RECEIVING_TEAM_USERS");
		if(sessionMap.containsKey("SESSION_USER_RECEIVING"))
			sessionMap.remove("SESSION_USER_RECEIVING");
		if(sessionMap.containsKey("SESSION_RECEIVING_TEAM_USERS"))
			sessionMap.remove("SESSION_RECEIVING_TEAM_USERS");
		if(sessionMap.containsKey("SESSION_HASHMAP_EVALUATION_TEAM_USERS"))
			sessionMap.remove("SESSION_HASHMAP_EVALUATION_TEAM_USERS");
		if(sessionMap.containsKey("SESSION_USER_EVALUATION"))
			sessionMap.remove("SESSION_USER_EVALUATION");
		if(sessionMap.containsKey("SESSION_EVALUATION_TEAM_USERS"))
			sessionMap.remove("SESSION_EVALUATION_TEAM_USERS");
		if(sessionMap.containsKey(WebConstants.Property.PROPERTY_VIEW_WITH_FLOOR_AND_UNITS))
			sessionMap.remove(WebConstants.Property.PROPERTY_VIEW_WITH_FLOOR_AND_UNITS);
	}
	public void loadFloors(Long propertyId)  {

		String methodName="loadFloors"; 
		logger.logInfo(methodName+"|"+"Start");
		try 
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <ReceivePropertyFloorView> floorViewList = psa.getPropertyFloorList(propertyId);
            floorsList = new ArrayList<SelectItem>(0);
			for(int i=0;i<floorViewList.size();i++)
			{
				ReceivePropertyFloorView rcvPropertyFloorVO=(ReceivePropertyFloorView)floorViewList.get(i);
				SelectItem item;
				item = new SelectItem(rcvPropertyFloorVO.getFloorId().toString()+"%%"+rcvPropertyFloorVO.getFloorNumber().toString(), rcvPropertyFloorVO.getFloorNumber());			  
				floorsList.add(item);
			}
            this.setFloorsList(floorsList);
			this.setCountryList(countryList);
			logger.logInfo(methodName+"|"+"Finish");
		}
		catch (Exception e)
		{
			logger.LogException(methodName+"|Error Occured ",e);
		}
	}
	public void loadFacility()  
	{

		String methodName="loadFacility"; 
		logger.logInfo(methodName+"|"+"Start");
		try 
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			FacilityView fView =new FacilityView();
			//Only unblocked facilities should be shown
			fView.setRecordStatus(1L);
			List <FacilityView> facilityViewList = psa.getAllFacility(fView);


			for(int i=0;i<facilityViewList.size();i++)
			{
				FacilityView facilityVO=(FacilityView)facilityViewList.get(i);
				
				String name = "";
				String Id = "";
				SelectItem item;
				if (getIsEnglishLocale())
				{
					// English is not nullable field
					Id = facilityVO.getFacilityId().toString();
					name =  facilityVO.getFacilityName();
				}
				else 
				{
					Id = facilityVO.getFacilityId().toString();
					name =  facilityVO.getFacilityNameAr();	  
					
					// Arabic is nullable field
					if(name == null)
						name =  facilityVO.getFacilityName();
					
				}

				item = new SelectItem(Id, name);
				
				if(this.getFacilityList()!=null)
				{
					this.getFacilityList().add(item);
				}
				else
				{
					this.setFacilityList(new ArrayList<SelectItem>());
					this.getFacilityList().add(item);
				}

			}

			this.setCountryList(countryList);
			logger.logInfo(methodName+"|"+"Finish");
		}
		catch (Exception e)
		{
			logger.LogException(methodName+"|Error Occured ",e);
		}
	}
	@SuppressWarnings("unchecked")
	public void loadFacilityCharges()
	{
		String methodName="loadFacility"; 
		logger.logInfo(methodName+"|"+"Start");
		try 
		{
			List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
			if(!viewMap.containsKey(WebConstants.FACILITY_TYPE))
			{
				ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FACILITY_TYPE);
				viewMap.put(WebConstants.FACILITY_TYPE,ddvList);
			}
			else
				ddvList = (ArrayList<DomainDataView>)viewMap.get(WebConstants.FACILITY_TYPE);
			
			DomainDataView ddv = CommonUtil.getDomainDataFromId(ddvList, Long.valueOf(facilityTypeId.toString()));
			PropertyServiceAgent psa = new PropertyServiceAgent();
			FacilityView facilityView = psa.getFacilityById(Long.valueOf(facilityId.toString()));
			this.getFacilityDescription().setValue(facilityView.getDescription());
			if(facilityView.getRent()!=null )
			    this.getCharges().setValue(facilityView.getRent());
			else
				this.getCharges().setValue(0D);
			if(ddv!=null&&ddv.getDataValue().equals(WebConstants.FACILITY_FACILITY_TYPE_FREE))
			{
				this.getCharges().setValue(0d);
				this.getCharges().setDisabled(true);
			}
			else if(Double.valueOf(this.getCharges().getValue().toString())<=0&&Long.valueOf(facilityTypeId.toString())!=-1)
				this.getCharges().setDisabled(false);

			
			logger.logInfo(methodName+"|"+"Finish");
		}
		catch (Exception e)
		{
			logger.LogException(methodName+"|Error Occured ",e);
		}

	}
	public void updateFacilityCharges()
	{
		String methodName="updateFacilityCharges"; 
		logger.logInfo(methodName+"|"+"Start");
		try 
		{
			List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
			if(!viewMap.containsKey(WebConstants.FACILITY_TYPE))
			{
				ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FACILITY_TYPE);
				viewMap.put(WebConstants.FACILITY_TYPE,ddvList);
			}
			else
				ddvList = (ArrayList<DomainDataView>)viewMap.get(WebConstants.FACILITY_TYPE);
			DomainDataView ddv = CommonUtil.getDomainDataFromId(ddvList, Long.valueOf(facilityTypeId.toString()));
			if(ddv!=null&&ddv.getDataValue().equals(WebConstants.FACILITY_FACILITY_TYPE_FREE))
			{
				this.getCharges().setValue(0d);
				this.getCharges().setDisabled(true);
			}
			else if(Double.valueOf(this.getCharges().getValue().toString())<=0&&Long.valueOf(facilityTypeId.toString())!=-1)
			{
				loadFacilityCharges();
				this.getCharges().setDisabled(false);
			}
			logger.logInfo(methodName+"|"+"Finish");
		}
		catch (Exception e)
		{
			logger.LogException(methodName+"|Error Occured ",e);
		}

	}
	@SuppressWarnings("unchecked")
	public void loadCountry() 
	{

		try 
		{
			if(!viewRootMap.containsKey("countryList"))
			{
				
				PropertyServiceAgent psa = new PropertyServiceAgent();
				List <RegionView> regionViewList = psa.getCountry();
	
	
				for(int i=0;i<regionViewList.size();i++)
				{
					RegionView rV=(RegionView)regionViewList.get(i);
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
					else 
						item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
	
	
					this.getCountryList().add(item);
	
				}
				Collections.sort(countryList,ListComparator.LIST_COMPARE);
				viewMap.put("countryList", countryList);
			}
		}
		catch (Exception e)
		{
			logger.LogException( "loadCountry|Error Occured ",e);

		}
	}
	@SuppressWarnings("unchecked")
	public void loadState()  
	{

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			if(viewRootMap.containsKey("stateList")){
				viewRootMap.remove("stateList");
				stateList.clear();
			}

			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(this.getCountryId())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	

			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());


			for(int i=0;i<regionViewList.size();i++)
			{
				RegionView rV=(RegionView)regionViewList.get(i);
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				else 
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				this.getStateList().add(item);


			}
			Collections.sort(stateList,ListComparator.LIST_COMPARE);
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
		}catch (Exception e){

			logger.LogException( "loadState|Error Occured ",e);

		}
	}
	@SuppressWarnings("unchecked")
	public void  loadCity()
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();

		if(viewRootMap.containsKey("cityList")){
			viewRootMap.remove("cityList");
			cityList.clear();
		}

		try 
		{

			Set<RegionView> regionViewList = null;
			if(this.getStateId()!=null && new Long(this.getStateId()).compareTo(new Long(-1))!=0)
				regionViewList =  psa.getCity(new Long(this.getStateId()));
			sessionMap.put(WebConstants.SESSION_CITY,regionViewList);

			if(regionViewList != null && regionViewList.size() > 0){
				Iterator itrator = regionViewList.iterator();

				for(int i=0;i<regionViewList.size();i++)
				{
					RegionView rV=(RegionView)itrator.next();
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
					}
					else 
						item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
					this.getCityList().add(item);


				}
				Collections.sort(cityList,ListComparator.LIST_COMPARE);
				viewMap.put("cityList", cityList);

			}
		}
		catch(Exception ex)
		{
			logger.LogException("loadCity|Error Occured ",ex);

		}

	}
	public void loadState(Long countryId2)  {

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			if(viewRootMap.containsKey("stateList")){
				viewRootMap.remove("stateList");
				stateList.clear();
			}

			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(countryId2)==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	

			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());


			for(int i=0;i<regionViewList.size();i++)
			{
				RegionView rV=(RegionView)regionViewList.get(i);
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				else 
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				this.getStateList().add(item);


			}
			Collections.sort(stateList,ListComparator.LIST_COMPARE);
			viewMap.put("stateList", stateList);
		}catch (Exception e){

			logger.LogException( "loadState(Long countryId2) |Error Occured ",e);

		}
	}
	@SuppressWarnings("unchecked")
	public void  loadCity(Long stateId2){
		PropertyServiceAgent psa = new PropertyServiceAgent();

		if(viewRootMap.containsKey("cityList")){
			viewRootMap.remove("cityList");
			cityList.clear();
		}

		try 
		{

			Set<RegionView> regionViewList = null;
			if(stateId2!=null && new Long(stateId2).compareTo(new Long(-1))!=0)
				regionViewList =  psa.getCity(new Long(stateId2));
			sessionMap.put(WebConstants.SESSION_CITY,regionViewList);


			Iterator itrator = regionViewList.iterator();

			for(int i=0;i<regionViewList.size();i++)
			{
				RegionView rV=(RegionView)itrator.next();
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				else 
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				this.getCityList().add(item);


			}
			Collections.sort(cityList,ListComparator.LIST_COMPARE);
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
		}
		catch(Exception ex)
		{
			logger.LogException( "loadCity(Long stateId2)|Error Occured ",ex);

		}

	}

	public Long getGroundFloorTypeID()
	{
		DomainDataView floorTypeGround = null;
		
		try
		{
			if(viewMap.containsKey("DD_VIEW_FLOOR_TYPE_GROUND"))
			{
				floorTypeGround = (DomainDataView) viewMap.get("DD_VIEW_FLOOR_TYPE_GROUND");
				
			}
			else
			{
				floorTypeGround = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.DomainTypes.FLOOR_TYPE), WebConstants.FloorType.GROUND_FLOOR);
				viewMap.get("DD_VIEW_FLOOR_TYPE_GROUND");
			}
		}
		catch(Exception exp)
		{
			logger.LogException("getGroundFloorTypeID crashed ", exp);
		}
		
		if(floorTypeGround != null)
			return floorTypeGround.getDomainDataId();
		else
			return new Long(0);
			
	}
	private HashMap<String, String> getExcludedFloorTypes()
	{
		HashMap<String, String> excludeFloorTypes = new HashMap<String, String>();
		
		try
		{				
			String groundFloorTypeId = getGroundFloorTypeID().toString();;
			excludeFloorTypes.put(groundFloorTypeId, groundFloorTypeId);
		}
		catch(Exception exp)
		{
			logger.LogException("Failed to fetch the excluded floor type list ", exp);
		}
		
		
		return excludeFloorTypes;
	}
    @SuppressWarnings("unchecked")
	private void loadCombos() throws Exception
	{
		categoryItem = applicationBean.getPropertyCategoryList();
		usageItems = applicationBean.getPropertyUsageList();
		propertyItems = applicationBean.getPropertyTypeList();
		ownershipItems = applicationBean.getPropertyOwnershipType();
		floorCategoryItems= applicationBean.getUnitUsageTypeList();
		
		
		floorTypeItems = applicationBean.getFloorTypesList(getExcludedFloorTypes());

		unitTypeList = applicationBean.getUnitType();
		unitInvestmentTypeList = applicationBean.getUnitInvestmentTypeList();
		unitUsageTypeList= applicationBean.getUnitUsageTypeList();
		unitSideList =applicationBean.getUnitSideType();
		propertyCategoryItems=applicationBean.getUnitUsageTypeList();
		floorGracePeriodItems = applicationBean.getGracePeriodTypeList();

		propertyGracePeriodTypeItems= applicationBean.getGracePeriodTypeList();
		floorRentProcessItems = applicationBean.getRentProcessList();

		propertyRentProcessItems = applicationBean.getRentProcessList();
		facilityTypeList = applicationBean.getFacilityType();
		investmentPurposes = applicationBean.getInvestmentPurposeList();
		loadCountry();
		loadFacility();
		if(viewMap.containsKey(WebConstants.Property.PROPERTY_VIEW)&& viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
			propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
		if(propertyView!=null&&propertyView.getPropertyId()!=null)
			loadFloors(propertyView.getPropertyId());
		
		
		if( (sessionMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW)&& sessionMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW)!=null) 
				||viewRootMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW))
		{
			if(sessionMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW)&&sessionMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW)!=null)
				propertyView = (PropertyView)sessionMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);
			else
				propertyView = (PropertyView)viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);
			
				viewRootMap.put(WebConstants.GenerateUnits.PROPERTY_VIEW, propertyView);
			sessionMap.remove(WebConstants.GenerateUnits.PROPERTY_VIEW);
			try 
			{
				propertyViewWithFloorsAndUnits = propertyServiceAgent.getPropertyByIdWithFloorsAndUnits(propertyView.getPropertyId());
				 fillUnits(propertyViewWithFloorsAndUnits);
				
			} catch (PimsBusinessException e) 
			{
				// 
				logger.LogException("init |Error Occured ",e);
				
			}
		}
		
		UnitView unitView = new UnitView();
		if((sessionMap.containsKey(WebConstants.GenerateUnits.UNIT_VIEW)&& sessionMap.get(WebConstants.GenerateUnits.UNIT_VIEW)!=null)
				|| viewRootMap.containsKey(WebConstants.GenerateUnits.UNIT_VIEW))
		{
			
			if(sessionMap.containsKey(WebConstants.GenerateUnits.UNIT_VIEW)&&sessionMap.get(WebConstants.GenerateUnits.UNIT_VIEW)!=null)
				unitView = (UnitView)sessionMap.get(WebConstants.GenerateUnits.UNIT_VIEW);
			else
				unitView = (UnitView)viewRootMap.get(WebConstants.GenerateUnits.UNIT_VIEW);
				viewRootMap.put(WebConstants.GenerateUnits.UNIT_VIEW, unitView);
			sessionMap.remove(WebConstants.GenerateUnits.UNIT_VIEW);
			
		}
		if(unitView!=null&&unitView.getPropertyId()!=null)
			editUnitSearchUnit(unitView);
		if (!isPostBack())
			viewMap.put("stateList",stateList);
	}
	public HtmlInputText getAddressLineOne() {
		return addressLineOne;
	}
	public void setAddressLineOne(HtmlInputText addressLineOne) {
		this.addressLineOne = addressLineOne;
	}
	public HtmlInputText getAddressLineTwo() {
		return addressLineTwo;
	}
	public void setAddressLineTwo(HtmlInputText addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public HtmlInputText getBuiltInArea() {
		return builtInArea;
	}
	public void setBuiltInArea(HtmlInputText builtInArea) {
		this.builtInArea = builtInArea;
	}
	public List<SelectItem> getCategoryItem() {
		return categoryItem;
	}
	public void setCategoryItem(List<SelectItem> categoryItem) {
		this.categoryItem = categoryItem;
	}

	public HtmlInputText getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(HtmlInputText departmentCode) {
		this.departmentCode = departmentCode;
	}

	public HtmlInputText getEndowedName() {
		return endowedName;
	}
	public void setEndowedName(HtmlInputText endowedName) {
		this.endowedName = endowedName;
	}
	public HtmlInputText getFinancialAccountNumber() {
		return financialAccountNumber;
	}
	public void setFinancialAccountNumber(HtmlInputText financialAccountNumber) {
		this.financialAccountNumber = financialAccountNumber;
	}
	public HtmlInputText getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(HtmlInputText floorNumber) {
		this.floorNumber = floorNumber;
	}
	public HtmlCommandButton getFloorRentSaveButton() {
		return floorRentSaveButton;
	}
	public void setFloorRentSaveButton(HtmlCommandButton floorRentSaveButton) {
		this.floorRentSaveButton = floorRentSaveButton;
	}
	public HtmlDataTable getFloorTable() {
		return floorTable;
	}
	public void setFloorTable(HtmlDataTable floorTable) {
		this.floorTable = floorTable;
	}
	public HtmlCommandButton getGenerateFloorButton() {
		return generateFloorButton;
	}
	public void setGenerateFloorButton(HtmlCommandButton generateFloorButton) {
		this.generateFloorButton = generateFloorButton;
	}
	public HtmlCommandButton getGenerateUnitButton() {
		return generateUnitButton;
	}
	public void setGenerateUnitButton(HtmlCommandButton generateUnitButton) {
		this.generateUnitButton = generateUnitButton;
	}
	public HtmlInputText getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(HtmlInputText homePhone) {
		this.homePhone = homePhone;
	}
	public List<SelectItem> getInvestmentPurposes() {
		return investmentPurposes;
	}
	public void setInvestmentPurposes(List<SelectItem> investmentPurposes) {
		this.investmentPurposes = investmentPurposes;
	}
	public HtmlInputText getLandArea() {
		return landArea;
	}
	public void setLandArea(HtmlInputText landArea) {
		this.landArea = landArea;
	}
	public HtmlInputText getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(HtmlInputText landNumber) {
		this.landNumber = landNumber;
	}
	public HtmlCommandButton getLocationSaveButton() {
		return locationSaveButton;
	}
	public void setLocationSaveButton(HtmlCommandButton locationSaveButton) {
		this.locationSaveButton = locationSaveButton;
	}
	public HtmlInputText getMunicipalityPlanNumber() {
		return municipalityPlanNumber;
	}
	public void setMunicipalityPlanNumber(HtmlInputText municipalityPlanNumber) {
		this.municipalityPlanNumber = municipalityPlanNumber;
	}
	public HtmlInputText getNumberOfLifts() {
		return numberOfLifts;
	}
	public void setNumberOfLifts(HtmlInputText numberOfLifts) {
		this.numberOfLifts = numberOfLifts;
	}
	public HtmlDataTable getOwnerDataGrid() {
		return ownerDataGrid;
	}
	public void setOwnerDataGrid(HtmlDataTable ownerDataGrid) {
		this.ownerDataGrid = ownerDataGrid;
	}
	public HtmlInputText getOwnerPercentage() {
		return ownerPercentage;
	}
	public void setOwnerPercentage(HtmlInputText ownerPercentage) {
		this.ownerPercentage = ownerPercentage;
	}
	public HtmlInputText getOwnerReferenceNo() {
		return ownerReferenceNo;
	}
	public void setOwnerReferenceNo(HtmlInputText ownerReferenceNo) {

		this.ownerReferenceNo = ownerReferenceNo;


	}
	public HtmlInputText getOwnerRepresentative() {
		return ownerRepresentative;
	}
	public void setOwnerRepresentative(HtmlInputText ownerRepresentative) {
		this.ownerRepresentative = ownerRepresentative;
	}
	public HtmlCommandButton getOwnerSaveButton() {
		return ownerSaveButton;
	}
	public void setOwnerSaveButton(HtmlCommandButton ownerSaveButton) {
		this.ownerSaveButton = ownerSaveButton;
	}
	public HtmlInputText getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(HtmlInputText postalCode) {
		this.postalCode = postalCode;
	}
	public HtmlSelectOneMenu getProeprtyInvestmentPurpose() {
		return proeprtyInvestmentPurpose;
	}
	public void setProeprtyInvestmentPurpose(
			HtmlSelectOneMenu proeprtyInvestmentPurpose) {
		this.proeprtyInvestmentPurpose = proeprtyInvestmentPurpose;
	}
	public HtmlInputText getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(HtmlInputText projectNumber) {
		this.projectNumber = projectNumber;
	}
	public HtmlSelectOneMenu getPropertyCategory() {
		return propertyCategory;
	}
	public void setPropertyCategory(HtmlSelectOneMenu propertyCategory) {
		this.propertyCategory = propertyCategory;
	}
	public HtmlInputText getPropertyReferenceNumber() {
		return propertyReferenceNumber;
	}
	public void setPropertyReferenceNumber(HtmlInputText propertyReferenceNumber) {
		this.propertyReferenceNumber = propertyReferenceNumber;
	}
	public HtmlCommandButton getPropertyRentSaveButton() {
		return propertyRentSaveButton;
	}
	public void setPropertyRentSaveButton(HtmlCommandButton propertyRentSaveButton) {
		this.propertyRentSaveButton = propertyRentSaveButton;
	}

	public HtmlSelectOneMenu getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(HtmlSelectOneMenu propertyType) {
		this.propertyType = propertyType;
	}
	public HtmlSelectOneMenu getPropertyUsage() {
		return propertyUsage;
	}
	public void setPropertyUsage(HtmlSelectOneMenu propertyUsage) {
		this.propertyUsage = propertyUsage;
	}

	public HtmlInputText getRentValue() {
		return rentValue;
	}
	public void setRentValue(HtmlInputText rentValue) {
		this.rentValue = rentValue;
	}
	public HtmlInputText getRentValueForFloors() {
		return rentValueForFloors;
	}
	public void setRentValueForFloors(HtmlInputText rentValueForFloors) {
		this.rentValueForFloors = rentValueForFloors;
	}
	public HtmlInputText getRentValueForUnits() {
		return rentValueForUnits;
	}
	public void setRentValueForUnits(HtmlInputText rentValueForUnits) {
		this.rentValueForUnits = rentValueForUnits;
	}
	public HtmlTabPanel getRichTabPanel() {
		return richTabPanel;
	}
	public void setRichTabPanel(HtmlTabPanel richTabPanel) {
		this.richTabPanel = richTabPanel;
	}
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}

	public HtmlInputText getStreetInformation() {
		return streetInformation;
	}
	public void setStreetInformation(HtmlInputText streetInformation) {
		this.streetInformation = streetInformation;
	}
	public HtmlCommandButton getSubmitButton() {
		return submitButton;
	}
	public void setSubmitButton(HtmlCommandButton submitButton) {
		this.submitButton = submitButton;
	}
	public HtmlInputText getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(HtmlInputText unitNumber) {
		this.unitNumber = unitNumber;
	}

	public HtmlDataTable getUnitTable() {
		return unitTable;
	}
	public void setUnitTable(HtmlDataTable unitTable) {
		this.unitTable = unitTable;
	}
	public List<SelectItem> getUsageItems() {
		return usageItems;
	}
	public void setUsageItems(List<SelectItem> usageItems) {
		this.usageItems = usageItems;
	}
	@SuppressWarnings("unchecked")
	public void preprocess(){
		super.preprocess();
		logger.logInfo("preprocess start");
		try {
			PersonView ownerView = new PersonView() ;
			PersonView representativeView = new PersonView();
			//propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
			propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

			if(ownerHiddenId.getValue()!=null&&ownerHiddenId.getValue().toString().trim().length()>0)
				ownerView = propertyServiceAgent.getPersonInformation(Long.valueOf(ownerHiddenId.getValue().toString()));
			if(representativeHiddenId.getValue()!=null&&representativeHiddenId.getValue().toString().trim().length()>0)
				representativeView = propertyServiceAgent.getPersonInformation(Long.valueOf(representativeHiddenId.getValue().toString()));
			if(ownerView.getPersonId()!=null)
				viewRootMap.put(WebConstants.Person.PERSON_OWNER_VIEW,ownerView);
			if(representativeView.getPersonId()!=null)
				viewRootMap.put(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW,representativeView);
			if(sessionMap.containsKey("FLOORS_DATA_LIST_FROM_POPUP") && sessionMap.get("FLOORS_DATA_LIST_FROM_POPUP")!=null)
			{
				sessionMap.remove("FLOORS_DATA_LIST_FROM_POPUP");				
				floorsDataList=propertyServiceAgent.getPropertyFloorList(propertyView.getPropertyId());
				unitDataList=propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());
				viewRootMap.put("UNITS_DATA_LIST",unitDataList);
				viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
				calculateBuildingHeight(floorsDataList);
			}
			if(sessionMap.containsKey("UNIT_DATA_LIST_FROM_POPUP")&& sessionMap.get("UNIT_DATA_LIST_FROM_POPUP")!=null)
			{
				sessionMap.remove("UNIT_DATA_LIST_FROM_POPUP");
				unitDataList=propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());
				 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_GENERATED_SUCCESSFULLY));
				viewRootMap.put("UNITS_DATA_LIST",unitDataList);
			}
			if(  (!viewRootMap.containsKey("UNITS_DATA_LIST")||viewRootMap.get("UNITS_DATA_LIST")==null||
					((List<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST")).size()<=0)&&(propertyView!=null||
							viewRootMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW)&&
									 viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW)!=null))
			{
				if(viewRootMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW))
					 propertyView = (PropertyView)viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);
				
				fillValues(propertyView);
			}
			

		} catch (NumberFormatException e) {

			logger.LogException("preprocess |Error Occured ",e);

		} catch (PimsBusinessException e) {

			logger.LogException("preprocess |Error Occured ",e);

		}
	}

	


	private boolean validateEvaluation(){
		boolean isValidated = true;
		if(!viewRootMap.containsKey("RECEIVING_TEAM_DATA_LIST")||viewRootMap.get("RECEIVING_TEAM_DATA_LIST")==null||
				((List<UserView>)viewRootMap.get("RECEIVING_TEAM_DATA_LIST")).size()<=0){
			
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.RECEIVING_TEAM_MUST_BE_MADE));
			richTabPanel.setSelectedTab("receiveTeamTab");
			return false;
		}
		
		if(!viewRootMap.containsKey("UNITS_DATA_LIST")||viewRootMap.get("UNITS_DATA_LIST")==null||
				((List<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST")).size()<=0){
			
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.THERE_MUST_BE_ONE_UNIT));
			richTabPanel.setSelectedTab("unitTab");
			return false;
		}
		
		return isValidated;
	}
	private boolean validateApproval(){
		boolean isValidated = true;
		if(!viewRootMap.containsKey("EVALUATION_TEAM_DATA_LIST")||viewRootMap.get("EVALUATION_TEAM_DATA_LIST")==null||
				((List<UserView>)viewRootMap.get("EVALUATION_TEAM_DATA_LIST")).size()<=0){
			isValidated = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.EVALUATION_TEAM_MUST_BE_MADE));
			richTabPanel.setSelectedTab("evaluationTeamTab");
		}
		if(!viewRootMap.containsKey("UNITS_DATA_LIST")||viewRootMap.get("UNITS_DATA_LIST")==null||
				((List<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST")).size()<=0){
			
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.THERE_MUST_BE_ONE_UNIT));
			richTabPanel.setSelectedTab("unitTab");
			return false;
		}
		return isValidated;
	}

	@SuppressWarnings("unchecked")
	public String sendForEvaluation()
	{
        errorMessages = new ArrayList<String>(0);
		try
		{
//			if(validateEvaluation() )
			{

				if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
					propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
				// for saving or updating evaluation team
				String saved = saveProperty();
				  if(StringHelper.isEmpty(saved) || (StringHelper.isNotEmpty(saved) &&  saved.compareTo("failure") != 0))
				  {
					  List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_STATUS);
						DomainDataView ddv = CommonUtil.getDomainDataFromId(ddvList, propertyView.getStatusId());
						if(ddv!=null&&(ddv.getDataValue().equals(WebConstants.Property.DRAFT)||ddv.getDataValue().equals(WebConstants.Property.NEW)))
						{
							PIMSRecivePropertyBPELPortClient pimsRecivePropertyBPELPortClient = new PIMSRecivePropertyBPELPortClient();
							
							String endPoint = parameters.getParameter(WebConstants.Property.RECEIVE_PROPERTY_BPEL_END_POINT);
							pimsRecivePropertyBPELPortClient.setEndpoint(endPoint);
							pimsRecivePropertyBPELPortClient.initiate(propertyView.getPropertyId(),getLoggedInUser(), null, null);
							
							//propertyView = propertyServiceAgent.getPropertyByID(propertyView.getPropertyId());
							ddv = CommonUtil.getIdFromType(ddvList, WebConstants.Property.UNDER_EVALUATION);
							propertyView.setStatusId(ddv.getDomainDataId());
							fillValues(propertyView);
							//sessionMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
							 viewRootMap.put(WebConstants.Property.PROPERTY_VIEW, propertyView);
							messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_SEND_FOR_EVALUATION_SUCCESSFULLY));
							actionPerformedMode();
						}

						else
						{
							errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_IS_NOT_AVAILABLE));
						}
				  }
				
			}

			return "";
		}
		
		catch (Exception e) 
		{
			logger.LogException(  "sendForEvaluation|crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

			return "fail";

		}


	}
	@SuppressWarnings("unchecked")
	protected Long setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    	Map taskAttributes =  userTask.getTaskAttributes();
			String loggedInUser=getLoggedInUser();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			sessionMap.put(WebConstants.FROM_TASK_LIST,null);
    	    return Convert.toLong(taskAttributes.get(WebConstants.UserTasks.PROPERTY_ID));
    	
    }

		
	private boolean validateOwnerTab(){
		String method = "ValidateOwner";
		logger.logInfo(method + " started...");
		boolean isValid = true;	

		NumberFormat nf = new DecimalFormat(getNumberFormat());
		PersonView personView ;
		
		String strPersonName="";
		String strCompanyName="";
		if(!viewRootMap.containsKey(WebConstants.Person.PERSON_OWNER_VIEW)){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_OWNER_REQUIRED));
			isValid = false;

		}
		
		else{
			personView = (PersonView)viewRootMap.get(WebConstants.Person.PERSON_OWNER_VIEW);
			 strPersonName="";
			 if(personView.getFirstName()!=null)
				 strPersonName= strPersonName+personView.getFirstName();
			 if(personView.getMiddleName()!=null)
				 strPersonName= strPersonName+personView.getMiddleName();
			 if(personView.getLastName()!=null)
				 strPersonName= strPersonName+personView.getLastName();
			 if(strPersonName.trim().length()<=0)
				strCompanyName=personView.getCompanyName();

		}
		if((ownerRepresentative.getValue()!=null&&ownerRepresentative.getValue().toString().trim().length()>0)&&
				(ownerRepresentative.getValue().toString().equals(strPersonName)||ownerRepresentative.getValue().toString().equals(strCompanyName))){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_REPRESENTATIVE_AND_OWNER_CANT_SAME));
			isValid = false;

		}

		
		try{
			if(Double.valueOf(ownerPercentage.getValue().toString())>100){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PERCENTAGE_INCORRECT));
				
				isValid = false;
			}
			Double percentage=0D;
			for(PropertyOwnerView propertyOwnerView:ownerDataList){
				
				percentage=propertyOwnerView.getOwnerSharePercent()+percentage+Double.valueOf(ownerPercentage.getValue().toString());
				if(propertyOwnerView.getOwnerName().equals(strCompanyName)||
						propertyOwnerView.getOwnerName().equals(strPersonName)){
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_OWNER_ALREADY_ADDED));
					isValid = false;
				}

			}
			if(Double.valueOf(percentage.toString())>100){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PERCENTAGE_INCORRECT));
				isValid = false;
			}
			if(ownerPercentage.getValue()!=null&&ownerPercentage.getValue().toString().trim().length()>0)
				Double.parseDouble(ownerPercentage.getValue().toString());
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_SHARE_PERCENT));

		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_SHARE_PERCENT));

		}
		return isValid;
	}
	private boolean validateFacilityTabl(){
		String method = "ValidateFacilit";
		logger.logInfo(method + " started...");
		boolean isValid = true;	

		NumberFormat nf = new DecimalFormat(getNumberFormat());

		if(Long.valueOf(facilityId)==-1){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_FACILITY_REQUIRED));
			isValid = false;

		}
		if(Long.valueOf(facilityTypeId)==-1){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_FACILITY_TYPE_REQUIRED));
			isValid = false;

		}
		if(charges.getValue()==null||charges.getValue().toString().trim().length()==0){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_CHARGES_REQUIRED));
			isValid = false;

		}

		try{
			if(charges.getValue()!=null&&charges.getValue().toString().trim().length()>0)
				Double.parseDouble(charges.getValue().toString());
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_INVALID_CHARGES));
		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_INVALID_CHARGES));
		}
		return isValid;
	}
	private boolean validateBaseScreen() {

		String method = "ValidateBase";
		logger.logInfo(method + " started...");
		boolean isValid = true;	

		NumberFormat nf = new DecimalFormat(getNumberFormat());
		if(commercialName.getValue()==null||commercialName.getValue().toString().trim().length()<=0){

			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_COMMERCIAL_NAME_REQUIRED));
			isValid = false;
		}
		if(propertyNameEn.getValue()==null||propertyNameEn.getValue().toString().trim().length()<=0){

			errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.msg.nameEnrequired"));
			isValid = false;
		}
		
		if(ownershipType.getValue()==null||ownershipType.getValue().toString().equals("-1")){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_OWNERSHIP_TYPE_REQUIRED));
			isValid = false;
		}
		if(proeprtyInvestmentPurpose.getValue()==null||proeprtyInvestmentPurpose.getValue().toString().equals("-1")){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVESTMENT_PURPOSE_REQUIRED));
			isValid = false;
		}
	
		
		if(propertyUsage.getValue()==null||propertyUsage.getValue().toString().equals("-1")){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_USAGE_REQUIRED));
			isValid = false;
		}
		if(propertyType.getValue()==null||propertyType.getValue().toString().equals("-1")){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_TYPE_REQUIRED));
			isValid = false;
		}


		if(addressLineOne.getValue()==null||addressLineOne.getValue().toString().trim().length()==0){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_ADDRESS_REQUIRED));
			isValid = false;
		}

		try{
			if(landArea.getValue()!=null && landArea.getValue().toString().trim().length()>0)
				Double.parseDouble( landArea.getValue().toString());
			if(landArea.getValue()!=null&&landArea.getValue().toString().trim().length() > 0&&
					Math.round(Double.parseDouble(landArea.getValue().toString()))>99999999){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LAND_AREA));
			}
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LAND_AREA));
		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LAND_AREA));
		}
		
		try{
			if(totalAnnualRent.getValue()!=null && totalAnnualRent.getValue().toString().trim().length()>0)
				Double.parseDouble(totalAnnualRent.getValue().toString());
			if(totalAnnualRent.getValue()!=null&&totalAnnualRent.getValue().toString().trim().length() > 0&&
					Math.round(Double.parseDouble(totalAnnualRent.getValue().toString()))>99999999){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_TOTAL_RENT));
			}
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_TOTAL_RENT));
		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_TOTAL_RENT));
		}
		
		try{
			if(builtInArea.getValue() !=null && builtInArea.getValue().toString().trim().length()>0)
				Double.parseDouble(builtInArea.getValue().toString());
			if(builtInArea.getValue()!=null&&builtInArea.getValue().toString().trim().length() > 0&&
					Math.round(Double.parseDouble(builtInArea.getValue().toString()))>99999999){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BUILT_IN_AREA));
			}
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BUILT_IN_AREA));
		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BUILT_IN_AREA));
		}
		try{
			if(numberOfLifts.getValue()!=null&&numberOfLifts.getValue().toString().trim().length() > 0)
				Integer.parseInt(numberOfLifts.getValue().toString());
			if(numberOfLifts.getValue()!=null&&numberOfLifts.getValue().toString().trim().length() > 0&&
					Math.round(Integer.parseInt(numberOfLifts.getValue().toString()))>99999999){
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_NO_OF_LIFTS));
			}
			
				
			//nf.parse(getUnitArea());
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_NO_OF_LIFTS));
		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_NO_OF_LIFTS));
		}

		if(landNumber.getValue()==null||landNumber.getValue().toString().trim().length()<=0){

			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_LAND_NUMBER_REQUIRED));
			isValid = false;
		}
		
		if(isMinorSelected() && (txtAssetNumber.getValue() == null || StringHelper.isEmpty(txtAssetNumber.getValue().toString())))
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.receiveProeprty.assetRequired"));
			isValid = false;
		}
		if(!isValid)
			richTabPanel.setSelectedTab("propertyBaseTab");
		return isValid;
	}


	public String getNumberFormat(){
		String methodName ="getNumberFormat";
		logger.logInfo(methodName+ " started");
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		logger.logInfo(methodName+" compelted");
		return localeInfo.getNumberFormat();
	}
	public String editFloor(){
		String methodName = "editFloor";
		logger.logInfo(methodName+ " |startedl");
		ReceivePropertyFloorView recievePropertyFloorView  =  (ReceivePropertyFloorView) floorTable.getRowData();
		floorHiddenId.setValue(recievePropertyFloorView.getFloorId());
		floorNumber.setValue(recievePropertyFloorView.getFloorNumber());
		rentValueForFloors.setValue("");
		sessionMap.put("FLOOR_VIEW_FOR_EDIT",recievePropertyFloorView);
		logger.logInfo(methodName+ "|compelted");


		return "";
	}
	private void updateUnitStatusToAvailable() throws PimsBusinessException
	{
		ReceivePropertyUnitView unitView=null;

		Iterator it = unitDataList.iterator();
		while(it.hasNext()){

			unitView= (ReceivePropertyUnitView )it.next();
			HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.UNIT_STATUS),WebConstants.UNIT_VACANT_STATUS);
			if(hMap.containsKey("returnId"));
			{
				PropertyServiceAgent psa=new PropertyServiceAgent();
				psa.changeUnitStatus(unitView.getUnitId(), new Long(hMap.get("returnId").toString()),getLoggedInUser());
			}
		}
	}

	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	 {
		 String methodName="getIdFromType";
		 logger.logInfo(methodName+"|"+"Start...");
		 logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		 Long returnId =new Long(0);
		 HashMap hMap=new HashMap();
		 for(int i=0;i<ddv.size();i++)
		 {
			 DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			 if(domainDataView.getDataValue().equals(type))
			 {
				 logger.logInfo(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
				 hMap.put("returnId",domainDataView.getDomainDataId());
				 logger.logInfo(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
				 hMap.put("IdEn",domainDataView.getDataDescEn());
				 hMap.put("IdAr",domainDataView.getDataDescAr());

			 }

		 }
		 logger.logInfo(methodName+"|"+"Finish...");
		 return hMap;

	 }
	 private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	 {
		 String method="getDomainDataListForDomainType";
		 logger.logInfo(method+"|"+"Start...");
		 List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		 List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		 Iterator<DomainTypeView> itr = list.iterator();
		 //Iterates for all the domain Types
		 while( itr.hasNext() ) 
		 {
			 DomainTypeView dtv = itr.next();
			 if( dtv.getTypeName().compareTo(domainType)==0 ) 
			 {
				 Set<DomainDataView> dd = dtv.getDomainDatas();
				 //Iterates over all the domain datas
				 for (DomainDataView ddv : dd) 
				 {
					 ddvList.add(ddv);	
				 }
				 break;
			 }
		 }
		 logger.logInfo(method+"|"+"Finsih...");
		 return ddvList;
	 }
	 public void editUnit(){
		 String methodName = "editUnit";
		 logger.logInfo(methodName+"| started");
		 ReceivePropertyUnitView recievePropertyUnitView  =  (ReceivePropertyUnitView) unitTable.getRowData();
		 floorNumberMenu.setReadonly(true);
		 unitPrefix.setReadonly(true);
		 unitNo.setReadonly(true);
		 if(isTaskTypeApproveEvaluation()){
			 addUnitButton.setRendered(true);
			 btnUpdateUnitAtApproval.setRendered(true);
		 }
		 else
			 addUnitButton.setValue(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_UPDATE_VALUE));
		 
		 accountNumber.setValue(recievePropertyUnitView.getAccountNumber());
		 unitHiddenId.setValue(recievePropertyUnitView.getUnitId());
		 
		 unitNumber.setValue(recievePropertyUnitView.getUnitNumber());
		 
		 rentValueForUnits.setValue(recievePropertyUnitView.getRentValue());
		 
		 unitPrefix.setValue(recievePropertyUnitView.getUnitPrefix());
		 if(recievePropertyUnitView.getUnitNo()!=null)
			 unitNo.setValue(recievePropertyUnitView.getUnitNo().longValue());
		 
		 if(isEnglishLocale)
			 unitStatus.setValue(recievePropertyUnitView.getStatusEn());
		 else
			 unitStatus.setValue(recievePropertyUnitView.getStatusAr());
		 
		 unitStatusId.setValue(recievePropertyUnitView.getStatusId());
		 unitEWUtilityNumber.setValue(recievePropertyUnitView.getEwUtilityNumber());
		 unitRemarks.setValue(recievePropertyUnitView.getUnitRemarks());
		 unitTypeMenu.setValue(recievePropertyUnitView.getUnitTypeId().toString());
		 if(recievePropertyUnitView.getFloorId()!=null)
			 floorNumberMenu.setValue(recievePropertyUnitView.getFloorId().toString()+"%%"+recievePropertyUnitView.getFloorNumber());
		 if(recievePropertyUnitView.getUnitSideTypeId()!=null)
			 unitSideMenu.setValue(recievePropertyUnitView.getUnitSideTypeId().toString());
		 if(recievePropertyUnitView.getUsageTypeId()!=null)
			 unitUsageTypeMenu.setValue(recievePropertyUnitView.getUsageTypeId().toString());
		 if(recievePropertyUnitView.getInvestmentTypeId()!=null)
		 	unitInvestmentTypeMenu.setValue(recievePropertyUnitView.getInvestmentTypeId().toString());
		 
		 unitDescription.setValue(recievePropertyUnitView.getUnitDesc());
		 
		 areaInSquare.setValue(recievePropertyUnitView.getUnitArea());
		 
		 bedRooms.setValue(recievePropertyUnitView.getNoOfBed());
		 
		 livingRooms.setValue(recievePropertyUnitView.getNoOfLiving());
		 
		 bathRooms.setValue(recievePropertyUnitView.getNoOfBath());
		 if(recievePropertyUnitView.getIncludeFloorNumber()!=null&&
				 recievePropertyUnitView.getIncludeFloorNumber().toString().equals("1")){
			 includeFloorNumberVar =true;
		 }
		 else
			 includeFloorNumberVar=false;
		 
//		 rentValueForUnits.setValue(receivePropertyUnitView.getRentValue());
		 
//		 sessionMap.put("UNIT_VIEW_FOR_EDIT",recievePropertyUnitView);
		 logger.logInfo(methodName+" |end");
		

	 } public void editUnitSearchUnit(UnitView unitView){
		 String methodName = "editUnitSearchUnit";
		 logger.logInfo(methodName+"| started");
		 
		 unitHiddenId.setValue(unitView.getUnitId());
		 
		 unitNumber.setValue(unitView.getUnitNumber());
		 
		 rentValueForUnits.setValue(unitView.getRentValue());
		 
		 unitPrefix.setValue(unitView.getUnitPrefix());
		 if(unitView.getUnitNo()!=null)
			 unitNo.setValue(unitView.getUnitNo().longValue());
		 
		 if(isEnglishLocale)
			 unitStatus.setValue(unitView.getStatusEn());
		 else
			 unitStatus.setValue(unitView.getStatusAr());
		 
		 unitStatusId.setValue(unitView.getStatusId());
//		 unitEWUtilityNumber.setValue(unitView.getEwUtilityNumber());
		 unitRemarks.setValue(unitView.getUnitRemarks());
		 unitTypeMenu.setValue(unitView.getUnitTypeId().toString());
		 if(unitView.getFloorId()!=null)
			 floorNumberMenu.setValue(unitView.getFloorId().toString()+"%%"+unitView.getFloorNumber());
		 if(unitView.getUnitSideTypeId()!=null)
			 unitSideMenu.setValue(unitView.getUnitSideTypeId().toString());
		 if(unitView.getUsageTypeId()!=null)
			 unitUsageTypeMenu.setValue(unitView.getUsageTypeId().toString());
		 if(unitView.getInvestmentTypeId()!=null)
		 	unitInvestmentTypeMenu.setValue(unitView.getInvestmentTypeId().toString());
		 
		 unitDescription.setValue(unitView.getUnitDesc());
		 
		 areaInSquare.setValue(unitView.getUnitArea());
		 
		 bedRooms.setValue(unitView.getNoOfBed());
		 
		 livingRooms.setValue(unitView.getNoOfLiving());
		 
		 bathRooms.setValue(unitView.getNoOfBath());
		 if(unitView.getIncludeFloorNumber()!=null&&
				 unitView.getIncludeFloorNumber().toString().equals("1")){
			 includeFloorNumberVar =true;
		 }
		 else
			 includeFloorNumberVar=false;
		 
//		 rentValueForUnits.setValue(receivePropertyUnitView.getRentValue());
		 
//		 sessionMap.put("UNIT_VIEW_FOR_EDIT",recievePropertyUnitView);
		 logger.logInfo(methodName+" |end");
		

	 }
	 @SuppressWarnings("unchecked")
		public String sendAfterEvaluation(){

			String  METHOD_NAME = "sendAfterEvaluation|";
			logger.logInfo(METHOD_NAME+" |start");
			try 
			{
				
				 if( validateApproval())
				 {
					    persistEvaluationTeam();   
					    long propertyId = setTaskOutCome(TaskOutcome.OK);			
						propertyView = propertyServiceAgent.getPropertyByID(propertyId);
					    
					    saveCommentsAttachments(propertyId);
						viewRootMap.remove("EVALUATION_TEAM_DATA_LIST");
						fillValues(propertyView);
						actionPerformedMode();
						messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_SEND_FOR_APPROVAL_SUCCESSFULLY));
						
						
				 }
				 logger.logInfo(METHOD_NAME+" completed successfully...");
			} 
			catch(PIMSWorkListException ex)
			{
				logger.LogException("Failed to approve due to:",ex);
				if(errorMessages == null)
					errorMessages = new ArrayList<String>();
				errorMessages.add(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE);
				


			}
			catch (Exception e) 
			{
				logger.LogException(METHOD_NAME + "crashed...", e);
				if(errorMessages == null)
					errorMessages = new ArrayList<String>();

				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));


			}
			return "";

		}

	 @SuppressWarnings("unchecked")
	 public String approveEvaluation()
	 {

		 String  METHOD_NAME = "approveEvaluation|";
		 try 
		 {
			    long propertyId = setTaskOutCome(TaskOutcome.APPROVE);
				 saveCommentsAttachments(propertyId);			
				 propertyView = propertyServiceAgent.getPropertyByID(propertyId);
				 persistEvaluationTeam();
				 viewRootMap.remove("EVALUATION_TEAM_DATA_LIST");
				 fillValues(propertyView);
				 actionPerformedMode();
				 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_EVALUATION_APPROVED_SUCCESSFULLY));
				 btnUpdateUnitAtApproval.setRendered(false);
			 
		 } 
		 catch(PIMSWorkListException ex)
		 {
			 logger.LogException("Failed to approve due to:",ex);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE);
			 


		 }
		 catch (Exception e) 
		 {
			 logger.LogException(METHOD_NAME + "crashed...", e);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));


		 }
		 return "";

	 }
	 @SuppressWarnings("unchecked")
	 public void receiveProperty()
	 {
		 try 
		 {
			 errorMessages = new ArrayList<String>();
			 
			 if(!validateApproval()){return;}
			 long propertyId = ((PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW)).getPropertyId();
			 try
				
			 {
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				propertyView = propertyServiceAgent.getPropertyByID(propertyId);
				propertyView.setUpdatedBy( getLoggedInUser() );
				fillValues(propertyView);
				actionPerformedMode();
				updateUnitStatusToAvailable();
				//No need to generate cost center for Zawaya.
				if( propertyView.getCategoryId() .compareTo(WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) !=0)
				{
					CostCenterFields costCenterFields = 
														new CostCenterFields.
																			 Builder( WebConstants.CostCenterRequestSource.PROPERTY ).
																			 propertyId( propertyId ).
																			 updatedBy( getLoggedInUser()).
																			 build();
					new CostCenterGenerator().generateAndPersistCostCenter(costCenterFields);
				}
				ApplicationContext.getContext().getTxnContext().commit();
				propertyId = setTaskOutCome(TaskOutcome.OK);
				saveCommentsAttachments(propertyId);
				messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_REVIEWED_SUCCESSFULLY));
				}
				catch (Exception e) 
				{
					ApplicationContext.getContext().getTxnContext().rollback();
					throw e;
				}
				finally
				{
					ApplicationContext.getContext().getTxnContext().release();
				}
				 
					
		 } 
		 
		 catch (Exception e) 
		 {
			 logger.LogException("receiveProperty|crashed...", e);
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }

	 }
     @SuppressWarnings("unchecked")
	 public String rejectEvaluation(){



		 String  METHOD_NAME = "rejectEvaluation|";
		 logger.logInfo(METHOD_NAME+"starts");
		 try 
		 {
			long propertyId = setTaskOutCome(TaskOutcome.REJECT);
			saveCommentsAttachments(propertyId);			
			 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_EVALUATION_REJECTED_SUCCESSFULLY));
			 actionPerformedMode();
			 logger.logInfo(METHOD_NAME+"end");
		 } 
		 catch(PIMSWorkListException ex)
		 {
			 logger.LogException("Failed to approve due to:",ex);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE);
		 }
		 catch (Exception e) 
		 {
			 logger.LogException(METHOD_NAME + "crashed...", e);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		 return "";

	 }
     @SuppressWarnings("unchecked")
	 public String approveAndUpdate()
	 {

		 String  METHOD_NAME = "approveAndUpdate|";
		 logger.logInfo(METHOD_NAME+"start");
		 try 
		 {
			long propertyId = setTaskOutCome(TaskOutcome.APPROVE);
			saveCommentsAttachments(propertyId);			
			updateUnitStatusToAvailable();
			actionPerformedMode();
			messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_APPROVED_AND_UPDATED_SUCCESSFULLY));
			logger.logInfo(METHOD_NAME+" completed successfully...");
		 } 
		 catch(PIMSWorkListException ex)
		 {
			 logger.LogException("Failed to approve due to:",ex);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE);
			 sessionMap.put(WebConstants.Contract.CONTRACT_VIEW,null);
		 }
		 catch (Exception e) 
		 {
			 logger.LogException(METHOD_NAME + "crashed...", e);
			 if(errorMessages == null)
				 errorMessages = new ArrayList<String>();
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 sessionMap.put(WebConstants.Contract.CONTRACT_VIEW,null);
		 }
		 return "";
	 }
     @SuppressWarnings("unchecked")
	 public String saveOwner()
	 {
		 String methodName = "saveOwner";
		 if(validateOwnerTab() )
		 {
			 logger.logInfo(methodName+"|starts");
			// if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
			 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
			 {
				 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
				 PropertyOwnerView propertyOwnerView = new PropertyOwnerView();
				 propertyOwnerView.setProperty(propertyView);
				 PersonView ownerView = (PersonView) viewRootMap.get(WebConstants.Person.PERSON_OWNER_VIEW);
				 viewRootMap.remove(WebConstants.Person.PERSON_OWNER_VIEW);
				 PersonView representativeView =null;
				 if(viewRootMap.containsKey(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW))
				 {
				   representativeView = (PersonView) viewRootMap.get(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW);
				   viewRootMap.remove(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW);
				 }
				 PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
				 Set<PropertyOwnerView> propertyOwnerViewSet = new HashSet<PropertyOwnerView>();
				 propertyOwnerView.setCreatedOn(new Date());
				 propertyOwnerView.setOwnerSharePercent(Double.valueOf(ownerPercentage.getValue().toString()));
				 propertyOwnerView.setCreatedBy(getLoggedInUser());
				 propertyOwnerView.setUpdatedBy(getLoggedInUser());
				 propertyOwnerView.setUpdatedOn(new Date());
				 propertyOwnerView.setIsDeleted(new Long(0));
				 propertyOwnerView.setRecordStatus(new Long(1));
				 propertyOwnerView.setPersonByOwnerId(ownerView);
				 propertyOwnerView.setOwnershipTypeId(propertyView.getCategoryId());
				 propertyOwnerView.setPersonByRepresentativeId(representativeView);
				 propertyOwnerViewSet.add(propertyOwnerView);
				 try 
				 {
					 propertyServiceAgent.persistPropertyOwner(propertyOwnerViewSet);
					 ownerPercentage.setValue("");
					 ownerRepresentative.setValue("");
					 ownerReferenceNo.setValue("");
					 ownerHiddenId.setValue("");
					 representativeHiddenId.setValue("");
					 if(viewRootMap.containsKey("ownerReferenceNo"))
						 viewRootMap.put("ownerReferenceNo", "");
					 if(viewRootMap.containsKey("representativeRefNo"))
						 viewRootMap.put("representativeRefNo", "");
					 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_OWNER_SAVED_SUCCESSFULLY));
					 logger.logInfo(methodName+"|Completed");
				 } 
				 catch (PimsBusinessException e)
				 {
					 logger.LogException(methodName+"|Error Occured ",e);
				 }
				 loadPropertyOwnersDataList(true);
			 }
		 }
		 return "";

	 }
	 public void deleteUnit(){
		 ReceivePropertyUnitView unitView = (ReceivePropertyUnitView)unitTable.getRowData();
		 String methodName = "deleteUnit";
		 logger.logInfo(methodName+" |started");
		 try {
			 propertyServiceAgent.deleteUnitById(unitView.getUnitId());
			 unitDataList=propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());
			 viewRootMap.put("UNITS_DATA_LIST",unitDataList);
			 getTotalNoOfUnits();
			 propertyView.setNoOfUnits((long)unitDataList.size());
			 logger.logInfo(methodName+" |compelted");
		 } catch (PimsBusinessException e) {
			 // TODO Auto-generated catch block
			 logger.LogException(methodName+"|Error Occured ",e);
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_UNIT_CANNOT_BE_DELETED));
			 
		 }
	 }
	 public void deleteFloor(){
		 ReceivePropertyFloorView floorView = (ReceivePropertyFloorView)floorTable.getRowData();
		 String methodName = "deleteFloor";
		 logger.logInfo(methodName+" |started");
		 try 
		 {
			 if(viewMap.get("BUILDING_HEIGHT") != null)
				 viewMap.remove("BUILDING_HEIGHT") ;
			 propertyServiceAgent.deleteFloorById(floorView.getFloorId());
			 floorsDataList=propertyServiceAgent.getPropertyFloorList(propertyView.getPropertyId());
		     loadFloors(propertyView.getPropertyId());
			 viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
			 logger.logInfo(methodName+" |compelted");
		 } 
		 catch (PimsBusinessException e)
		 {
			 logger.LogException(methodName+"|Error Occured ",e);
			 
		 }
	 }
	 public String saveReceivingTeam(List<UserView> listUserView)throws Exception
	 {
		 	if(propertyView!=null)
			{
				try 
				{
		
					TeamView teamView = new TeamView();
					teamView=getReceivingTeamView(listUserView);
					teamView = propertyServiceAgent.addTeam(teamView);
					List<TeamMemberView> teamMemberViewList = 	getTeamMemberList( getReceivingDataList() );
					propertyServiceAgent.addUpdateTeamMembers(teamMemberViewList, teamView);
					propertyView.setTeamViewByReceivingTeamId(teamView);
					propertyServiceAgent.updatePropertyForReceiveProperty(propertyView);
					
				}
				catch (PimsBusinessException e) 
				{
					if(errorMessages == null)
						errorMessages = new ArrayList<String>();
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

				}
			}
			return "";

		}
		 
		 
	
	 private void persistEvaluationTeam() throws PimsBusinessException,Exception 
	 {
			if(
					propertyView.getTeamByEvaluationTeamId() !=null &&
					propertyView.getTeamByEvaluationTeamId().getTeamId() != null
			  )
			{
			 updateEvaluationTeam();
			}
			 else
			 {
			  // in case of add evaluation team 
				 saveEvaluationTeam(getEvaluationDataList());
//				 updateEvaluationTeam();
			 }
	}
	 public String saveEvaluationTeam(List<UserView> listUserView)throws PimsBusinessException,Exception
	 {
		 if(listUserView == null || listUserView.size() <= 0) return "";
		 
	    TeamView teamView = new TeamView();
		teamView=getEvaluationTeamView(listUserView);
		teamView = propertyServiceAgent.addTeam(teamView);
		 
		List<TeamMemberView> teamMemberViewList = 	getTeamMemberList( getEvaluationDataList() );
		propertyServiceAgent.addUpdateTeamMembers(teamMemberViewList, teamView);
		propertyView.setTeamByEvaluationTeamId(teamView);
		propertyServiceAgent.updatePropertyForReceiveProperty(propertyView);
		
		return "";
	 }
	
	 private void updateEvaluationTeam() throws PimsBusinessException 
	 {
		 List<UserView> listUserView = getEvaluationDataList() ;
		 if(listUserView == null || listUserView.size() <= 0) return ;
		 List<TeamMemberView> teamMemberViewList = 	getTeamMemberList( listUserView );
		 TeamView teamView = new TeamView();
		 teamView.setTeamId(propertyView.getTeamByEvaluationTeamId().getTeamId());
		 propertyServiceAgent.addUpdateTeamMembers(teamMemberViewList, teamView);
	 }
	 
	 
	 private TeamView getReceivingTeamView(List<UserView> listUserView)throws PimsBusinessException
	 {
		 String methodName = "getTeamView";
		 logger.logInfo(methodName+"|"+"Start..");
		 
		 TeamView teamView =new TeamView();
			Set<TeamMemberView> teamMemberSet=new HashSet<TeamMemberView>(0);
			TeamMemberView teamMemberView=new TeamMemberView();

			logger.logInfo(methodName+"|"+"teamId.."+teamId);
			if(teamId!=null && teamId.length()>0)
			{
				teamView.setTeamId(new Long(teamId));	

			}
		
			teamView.setTeamName("Receiving Team");	
			teamView.setCreatedOn(new Date());
			teamView.setCreatedBy(getLoggedInUser());
			teamView.setUpdatedOn(new Date());
			teamView.setUpdatedBy(getLoggedInUser());
			teamView.setIsDeleted(new Long(0));
			teamView.setRecordStatus(new Long(1));

			
			//adding members
			for (UserView selectItem : listUserView) 
			{
				teamMemberView=new TeamMemberView();
				logger.logInfo(methodName+"|"+"loginId.."+selectItem.getUserName().toString());
				teamMemberView.setLoginId(selectItem.getUserName().toString());
				teamMemberView.setIsDeleted(new Long(0));
				teamMemberView.setRecordStatus(new Long(1));
				teamMemberView.setUpdatedOn(new Date());
				teamMemberView.setCreatedOn(new Date());
				teamMemberView.setCreatedBy(getLoggedInUser());
				teamMemberView.setUpdatedBy(getLoggedInUser());
				teamMemberSet.add(teamMemberView);

			}
			teamView.setTeamMembersView(teamMemberSet);
			logger.logInfo(methodName+"|"+"Finish..");
		 
		 return teamView;
	 }

	 private TeamView getEvaluationTeamView(List<UserView> listUserView)throws Exception
	 {
		 String methodName = "getTeamView";
		 logger.logInfo(methodName+"|"+"Start..");
		 
		 TeamView teamView =new TeamView();
			Set<TeamMemberView> teamMemberSet=new HashSet<TeamMemberView>(0);
			TeamMemberView teamMemberView=new TeamMemberView();

			logger.logInfo(methodName+"|"+"teamId.."+teamId);
			if(teamId!=null && teamId.length()>0)
			{
				teamView.setTeamId(new Long(teamId));	

			}
		
			teamView.setTeamName("Evaluation Team");	
			teamView.setCreatedOn(new Date());
			teamView.setCreatedBy(getLoggedInUser());
			teamView.setUpdatedOn(new Date());
			teamView.setUpdatedBy(getLoggedInUser());
			teamView.setIsDeleted(new Long(0));
			teamView.setRecordStatus(new Long(1));

			
			//adding members
			for (UserView selectItem : listUserView) 
			{
				teamMemberView=new TeamMemberView();
				logger.logInfo(methodName+"|"+"loginId.."+selectItem.getUserName().toString());
				teamMemberView.setLoginId(selectItem.getUserName().toString());
				teamMemberView.setIsDeleted(new Long(0));
				teamMemberView.setRecordStatus(new Long(1));
				teamMemberView.setUpdatedOn(new Date());
				teamMemberView.setCreatedOn(new Date());
				teamMemberView.setCreatedBy(getLoggedInUser());
				teamMemberView.setUpdatedBy(getLoggedInUser());
				teamMemberSet.add(teamMemberView);

			}
			teamView.setTeamMembersView(teamMemberSet);
			logger.logInfo(methodName+"|"+"Finish..");
		 
		 return teamView;
	 }
	 private void clearFloorFields()
     {
    	 floorPrefix.setValue("");
    	 floorSeperator.setValue("");
         floorFrom.setValue("");
         floorTo.setValue("");
         floorTypeSelectOneMenu.setValue("-1");
         afterFloorMenu.setValue("-1");
         floorDescription.setValue("");
    	 
     }

     @SuppressWarnings("unchecked")
	 public String generatePropertyFloors()
	 {
		 String method = "generatePropertyFloors";
		 logger.logInfo(method + " started...");

		 try
		 {
				 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
				 if(validateFloorScreen())
				 {
				 logger.logInfo("validation has been done");
				 String loggedInUser = getLoggedInUser();
				 Long propertyId = Convert.toLong(propertyView.getPropertyId());
				 
				 int floorNumberFrom = Convert.toInteger(floorFrom.getValue());
				 int floorNumberTo = Convert.toInteger(floorTo.getValue());
				 
				 String floorNumberPrefix = floorPrefix.getValue().toString().trim();
				 Long floorTypeId = Convert.toLong(floorTypeSelectOneMenu.getValue().toString());			
				 PropertyServiceAgent psAgent = new PropertyServiceAgent();
				 String floorId =null;
				 
				 Double currentFloorSequence=0D;
				 Double dividerFloorSequence = 0D;
				 Double nextFloorSequence=0D;
				 
				 
				 Double minSeq= 0D;
				 Double maxSeq= 0D;
				 
				 
				 ArrayList<FloorView> propertyFloorViews = new ArrayList<FloorView>();
				 
				 if(afterFloorMenu.getValue()!=null && !afterFloorMenu.getValue().toString().equals("-1") &&
						 afterFloorMenu.getValue().toString().trim().length()>0 &&
						 afterFloorMenu.getValue().toString().split("%%").length>0 &&
						 afterFloorMenu.getValue().toString().split("%%")[0] != null )
				 {
					 floorId= afterFloorMenu.getValue().toString().split("%%")[0];
					 currentFloorSequence = psAgent.getFloorSequenceByID(Long.valueOf(floorId),propertyView.getPropertyId(),true,0D);
					 minSeq = currentFloorSequence;
					 
					 maxSeq = psAgent.getMinimumFloorSequence(propertyId,currentFloorSequence);
					 if(maxSeq == null || maxSeq.compareTo(0D) <= 1)
						 maxSeq = maximumFloorSequence;
					 
					 //nextFloorSequence=psAgent.getFloorSequenceByID(Long.valueOf(floorId),propertyView.getPropertyId(),false,currentFloorSequence);
					 //dividerFloorSequence=currentFloorSequence;
				 }
				 
				 else
				 {
					 maxSeq = psAgent.getMinimumFloorSequence(propertyId,null);
					 minSeq= minimumFloorSequence;
				 }
				 
				 Double steps = (maxSeq - minSeq) / (floorNumberTo -floorNumberFrom + 2);
				 for(int index = floorNumberFrom; index <= floorNumberTo; index ++ ) 
				 {
					 FloorView propertyFloorView = new FloorView();
					 propertyFloorView.setCreatedBy(loggedInUser);
					 propertyFloorView.setCreatedOn(new Date());
					 if(floorNumberPrefix.trim().length()>0)
					 {
						 propertyFloorView.setFloorNumber(floorNumberPrefix+floorSeperator.getValue().toString().trim()+index);
						 propertyFloorView.setFloorName  (floorNumberPrefix+floorSeperator.getValue().toString().trim()+index);
					 }
//					 else if(index<=0 && floorNumberPrefix.trim().length()>0)
//					 { 
//						 propertyFloorView.setFloorNumber(floorNumberPrefix);
//						 propertyFloorView.setFloorName  (floorNumberPrefix);
//					 }
					 else
					 {
						 propertyFloorView.setFloorNumber(String.valueOf(index));
						 propertyFloorView.setFloorName(String.valueOf(index));
					 }

					 propertyFloorView.setFloorNo(Double.valueOf(index));
					 propertyFloorView.setFloorSeperator(floorSeperator.getValue().toString().trim());
					 propertyFloorView.setFloorPrefix(floorPrefix.getValue().toString().trim());
					 propertyFloorView.setIsDeleted(new Long(0));
					 propertyFloorView.setRecordStatus(new Long(1));
					 propertyFloorView.setPropertyId(propertyId);
					 propertyFloorView.setTypeId(floorTypeId);
					 propertyFloorView.setUpdatedBy(loggedInUser);
					 propertyFloorView.setUpdatedOn(new Date());
					
					 
					 Double floorSequence = minSeq + steps;
					 propertyFloorView.setFloorSequence(floorSequence);
					 minSeq = floorSequence;
//					 if(currentFloorSequence==0D||nextFloorSequence==0D)
//					 {
//						 currentFloorSequence=currentFloorSequence.doubleValue()+1D;
//						 dividerFloorSequence=currentFloorSequence;
//						 propertyFloorView.setFloorSequence(currentFloorSequence);
//					 }
//					 else if(currentFloorSequence==1D||dividerFloorSequence==nextFloorSequence)
//					 {
//						 dividerFloorSequence=nextFloorSequence;
//						 currentFloorSequence = (currentFloorSequence+nextFloorSequence)/2;
//						 propertyFloorView.setFloorSequence(currentFloorSequence);
//					 }
//					 else
//					 {
//						 currentFloorSequence = (currentFloorSequence+nextFloorSequence)/2;
//						 propertyFloorView.setFloorSequence(currentFloorSequence);
//					 }
					 
					 
					 
					 propertyFloorViews.add(propertyFloorView);
				 }
				 propertyFloorViews = psAgent.generateFloors(propertyId,propertyFloorViews);
				 List<ReceivePropertyFloorView> listPropertyFloorView = new ArrayList<ReceivePropertyFloorView>();
				 floorsDataList=psAgent.getPropertyFloorList(propertyId);
				 viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
				 loadFloors(propertyView.getPropertyId());
				 if(propertyFloorViews.size()  >0)
				 {
					 errorMessages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ALREADY_EXISTS));                
					 for(FloorView floorView: propertyFloorViews) 
					 {
						 {
							 errorMessages.add(floorView.getFloorNumber()+" ; "+floorView.getFloorName());
							 logger.logInfo("Floor Number: "+floorView.getFloorNumber()+" Floor Name: "+floorView.getFloorName() );
						 }
					 }
				 }
				 else 
				 {
					 floorsDataList=psAgent.getPropertyFloorList(propertyId);
					 viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
					 messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_SUCCESSFULLY));
				 }
				 
				 
			     clearFloorFields();	 
			 }
			

		 }
		 catch(Exception ex)
		 {
			 logger.logError("Crashed while generating floors:",ex);
			 errorMessages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ERROR));
		 }
		 
		 return "";

	 }
     
	 public void onGenerateUnitCostCenter()throws Exception
	 {
		 try{
			 errorMessages=new ArrayList<String>();
			 messages = new ArrayList<String>();
			 boolean hasSelected = false;
			 if(viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW)!=null )
			 propertyView = (PropertyView)viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);
			 com.avanza.pims.entity.Property property = EntityManager.getBroker().findById(com.avanza.pims.entity.Property.class, propertyView.getPropertyId());
			 for (ReceivePropertyUnitView unitVO: unitDataList )
			 {
				 if( 
					unitVO.getSelected()== null || !unitVO.getSelected()  
				   ) continue;
				 
				 hasSelected =true;
				 Unit unit = EntityManager.getBroker().findById(Unit.class,unitVO.getUnitId());

	            	if( 
	            		unit.getAccountNumber() == null || unit.getAccountNumber().trim().length()<= 0 
	            	   ) 
	            	{
	            		CostCenterFields costCenterFields = 
	            											new CostCenterFields.
																				 Builder( Constant.CostCenterRequestSource.PROPERTY_UNIT ).
																				 propertyId( property.getPropertyId() ).
																				 updatedBy( getLoggedInUserId()).
																				 unitId(unit.getUnitId() ).
																				 
																				 build();
	            		costCenterFields.setProperty(property);
	            		costCenterFields.setUnit(unit);
	            		costCenterFields.setOwnershipTypeId(property.getCategoryId());
	            		new CostCenterGenerator().generateAndPersistCostCenter(costCenterFields);
	            		unit.setAccountNumber( costCenterFields.getGeneratedCostCenter() );
	            		
	            		EntityManager.getBroker().update(unit);
	            		unitVO.setAccountNumber(unit.getAccountNumber());
	            	}
	         }
			 if(!hasSelected)
			 {
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_ROW));
				
			 }
			 else
			 {
				 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_UPDATED_SUCCESSFULLY));
				 viewRootMap.put("UNITS_DATA_LIST",unitDataList);
				 getUnitDataList();
			 }
		 }
		 catch(Exception e)
		 {
			 logger.LogException("Error Occured:", e);
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		 
	 }
	 public String generatePropertyUnits()
	 {
		 String method = "generatePropertyUnits";
		 logger.logInfo(method + " started...");

		 try
		 {
			 if(validateUnitScreen() ){
				 //propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
				 String uPrefix="";
				 if(unitPrefix.getValue()!= null && unitPrefix.getValue().toString().trim().length()>0)
					 uPrefix = unitPrefix.getValue().toString().trim();
				 //propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
				 if(viewRootMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW)&&
						 viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW)!=null)
					 propertyView = (PropertyView)viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);

				 String loggedInUser = getLoggedInUser();
				 Long propertyId = Convert.toLong(propertyView.getPropertyId());
				 int noOfUnits = 1;

//				 String floorNamePrefix = inputTextFloorNamePrefix.getValue().toString();
				 Boolean editMode = false;
				 Long unitTypeId = Convert.toLong(unitTypeMenu.getValue().toString());			
				 Long unitSideId = Convert.toLong(unitSideMenu.getValue().toString());
				 Long usageTypeId= Convert.toLong(unitUsageTypeMenu.getValue().toString());
				 Long investmentTypeId= Convert.toLong(unitInvestmentTypeMenu.getValue().toString());
				 PropertyServiceAgent psAgent = new PropertyServiceAgent();

				 ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();
				 UnitView propertyUnitView = new UnitView();
				 propertyUnitView.setCreatedBy(loggedInUser);
				 propertyUnitView.setCreatedOn(new Date());
				 if(includeFloorNumberVar)
				 {
					 propertyUnitView.setIncludeFloorNumber(1L);
					 propertyUnitView.setUnitNumber(floorNumberMenu.getValue().toString().split("%%")[1]+uPrefix+unitNo.getValue().toString());
				 }
				else
				{
					 propertyUnitView.setIncludeFloorNumber(0L);
					 propertyUnitView.setUnitNumber(uPrefix+unitNo.getValue().toString());
				}
				 
				 propertyUnitView.setUnitName(uPrefix+unitNo.getValue().toString());
				 propertyUnitView.setUnitPrefix(uPrefix);
				 propertyUnitView.setUnitNo(Double.valueOf(unitNo.getValue().toString()));
				 propertyUnitView.setIsDeleted(new Long(0));
				 propertyUnitView.setRecordStatus(new Long(1));
				 propertyUnitView.setPropertyId(propertyId);
				 propertyUnitView.setFloorId(Long.valueOf(floorNumberMenu.getValue().toString().split("%%")[0]));
				 propertyUnitView.setUnitTypeId(unitTypeId);
				 propertyUnitView.setUnitSideTypeId(unitSideId);
				 propertyUnitView.setUsageTypeId(usageTypeId);
				 propertyUnitView.setInvestmentTypeId(investmentTypeId);
				 propertyUnitView.setUnitDesc(unitDescription.getValue().toString());
				 if(areaInSquare.getValue()!=null)
					 propertyUnitView.setUnitArea(Double.valueOf(areaInSquare.getValue().toString()));
				 if(bathRooms.getValue()!=null)
					 propertyUnitView.setNoOfBath(Long.valueOf(bathRooms.getValue().toString()));
				 if(bedRooms.getValue()!=null)
					 propertyUnitView.setNoOfBed(Long.valueOf(bedRooms.getValue().toString()));
				 if(livingRooms.getValue()!=null)
					 propertyUnitView.setNoOfLiving(Long.valueOf(livingRooms.getValue().toString()));
				 if(unitEWUtilityNumber!=null&&unitEWUtilityNumber.getValue().toString().trim().length()>0)
					 propertyUnitView.setEWUtilitNo(Long.valueOf(unitEWUtilityNumber.getValue().toString()));
				 if(rentValueForUnits.getValue()!=null){
					 propertyUnitView.setRentValue(Double.valueOf(rentValueForUnits.getValue().toString()));
				 
					 propertyUnitView.setRequiredPrice(Double.valueOf(rentValueForUnits.getValue().toString()));
				 }
				 if(accountNumber.getValue() != null){
					 propertyUnitView.setAccountNumber(accountNumber.getValue().toString());
				 }
				 propertyUnitView.setUnitRemarks(unitRemarks.getValue().toString());
				 propertyUnitView.setUpdatedBy(loggedInUser);
				 propertyUnitView.setUpdatedOn(new Date());
				 if(unitHiddenId.getValue()!=null&&
						 unitHiddenId.getValue().toString().trim().length()>0){
					 propertyUnitView.setUnitId(Long.valueOf(unitHiddenId.getValue().toString()));
					 editMode=true;
				 }
				 if(isPropertyReceived())
					 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.UNIT_STATUS,WebConstants.Statuses.UNIT_VACANT_STATUS);
				 else if(viewRootMap.containsKey("new")&&
							viewRootMap.get("new").toString().equals("true"))
				 	 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.UNIT_STATUS,WebConstants.UNIT_UNDER_CONSTRUCTION);
				 else 
					 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.UNIT_STATUS,WebConstants.UNIT_UNDER_RECEIVING);
				 if(unitStatusId.getValue()!=null&&unitStatusId.getValue().toString().trim().length()>0)
					 propertyUnitView.setStatusId(Long.valueOf(unitStatusId.getValue().toString()));
				 else
					 propertyUnitView.setStatusId(domainDataView.getDomainDataId());
				 propertyUnitView.setStatusAr(domainDataView.getDataDescAr());
				 propertyUnitView.setStatusEn(domainDataView.getDataDescEn());
				 propertyUnitViews.add(propertyUnitView);

		
					 propertyUnitViews = psAgent.generateUnits(propertyId,Long.valueOf(floorNumberMenu.getValue().toString().split("%%")[0]),propertyUnitViews);
				 List<ReceivePropertyUnitView> listPropertyUnitView = new ArrayList<ReceivePropertyUnitView>();
			
				   unitDataList= psAgent.getPropertyUnitList(propertyId);
				 
				 viewRootMap.put("UNITS_DATA_LIST",unitDataList);
				 getTotalNoOfUnits();
				 propertyView.setNoOfUnits((long)unitDataList.size());

				 if(propertyUnitViews.size()  >0&&propertyUnitView.getUnitId()==null) {
					 errorMessages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ALREADY_EXISTS));                
					 for(UnitView unitView: propertyUnitViews) {
						 {
							 errorMessages.add(unitView.getUnitNumber()+" ; "+unitView.getFloorView().getFloorNumber());

						 }
					 }
				 }
				 else {
					 if(!editMode)
					 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_GENERATED_SUCCESSFULLY));
					 else
						 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_UPDATED_SUCCESSFULLY));
					 clearControls();
				 }

				 btnUpdateUnitAtApproval.setRendered(false);
			 }
		 }

		 catch(Exception ex)
		 {
			 addUnitButton.setValue(ResourceUtil.getInstance().getProperty("commons.Add"));
			 logger.logError("Crashed while generating units:",ex);
			 ex.printStackTrace();
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }


		 return "";

	 }
	 // This method seems to be written on to generate the default ground floor
	 private void generateFloor(Long propertyId) throws PimsBusinessException{
		 String methodName = "generateFloor";
		 logger.logInfo(methodName + " started");
		 UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
		 DomainDataView ddv = utilityServiceAgent.getDomainDataByValue(WebConstants.FLOOR_TYPE, WebConstants.FloorType.GROUND_FLOOR);
		 int noOfFloors = 1;
		 int floorNumberFrom = 1;
		 int floorNumberTo = floorNumberFrom + noOfFloors - 1;
		 String floorNamePrefix = SystemParameters.getInstance().getParameter("FLOOR_NAME_PREFIX");
		 String floorNumberPrefix = SystemParameters.getInstance().getParameter("FLOOR_NO_PREFIX");
		 String floorSeperator= SystemParameters.getInstance().getParameter("FLOOR_SEPERATOR");
		 Long floorTypeId = ddv.getDomainDataId();			

		 PropertyServiceAgent psAgent = new PropertyServiceAgent();
		 
		 

		 ArrayList<FloorView> propertyFloorViews = new ArrayList<FloorView>();
		 for(int index = floorNumberFrom; index <= floorNumberTo; index ++ ) 
		 {
			 FloorView propertyFloorView = new FloorView();
			 propertyFloorView.setCreatedBy(getLoggedInUser());
			 propertyFloorView.setCreatedOn(new Date());
			 propertyFloorView.setFloorNumber(floorNumberPrefix+floorSeperator+index);
			 propertyFloorView.setFloorName(floorNamePrefix+floorSeperator+index);
			 propertyFloorView.setFloorNo(Double.valueOf(index));
			 propertyFloorView.setFloorPrefix(floorNamePrefix);
			 propertyFloorView.setFloorSeperator(floorSeperator);
			 propertyFloorView.setIsDeleted(new Long(0));
			 propertyFloorView.setRecordStatus(new Long(1));
			 propertyFloorView.setPropertyId(propertyId);
			 propertyFloorView.setTypeId(floorTypeId);
			 propertyFloorView.setUpdatedBy(getLoggedInUser());
			 propertyFloorView.setUpdatedOn(new Date());
			 
			 Double floorSequence =  ( minimumFloorSequence + maximumFloorSequence ) / (floorNumberFrom + noOfFloors);
			 
			 propertyFloorView.setFloorSequence(floorSequence);
			 propertyFloorView.setFloorNo(Convert.toDouble(index));
			 propertyFloorView.setFloorPrefix(floorNumberPrefix);
			 propertyFloorView.setFloorSeperator(floorSeperator);
			 
			 propertyFloorViews.add(propertyFloorView);
		 }

		 propertyFloorViews = psAgent.generateFloors(propertyId,propertyFloorViews);
		 loadPropertyFloorsDataList(true);
		 logger.logInfo(methodName+ "completed");
	 }
	 public String saveProperty()
	 {
		 String methodName="saveProperty";
		 errorMessages = new ArrayList<String>();
		 logger.logInfo(methodName + "started...");
			try
			 {

				 messages = new ArrayList<String>();	
				 if(viewRootMap.containsKey("PROPERTY_INVENTORY_MODE")&&
						 viewRootMap.get("PROPERTY_INVENTORY_MODE")!=null)
				 {
					 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.PROPERTY_STATUS,WebConstants.Property.NEW);

				 }
				 else
					 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.PROPERTY_STATUS,WebConstants.Property.DRAFT);
				 if(validateBaseScreen())
				 {
					 PropertyView propertyView ;
					// propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
					 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
					 if(propertyView==null)
					   propertyView = new PropertyView();
					//put the projectId in property before saving it.
					 if(viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY")!=null)
					 {
						ProjectDetailsView projectDetails=new ProjectDetailsView();
						projectDetails=(ProjectDetailsView) viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY");
						propertyView.setProjectId(projectDetails.getProjectId());
					 }
					 else if(getHdnprojectId()!=null)
						 propertyView.setProjectId(getHdnprojectId());
					 propertyView.setCommercialName(commercialName.getValue().toString());
					 propertyView.setLandNumber(landNumber.getValue().toString());
					 propertyView.setAddressLineOne(addressLineOne.getValue().toString());
					 if( newCC != null && newCC.trim().length() > 0 )
					 {
						 propertyView.setCostCenter( newCC.trim() );
					 }
					 if(
						 ( viewMap.get("PARENT_SCREEN")==null || StringHelper.isEmpty( viewMap.get("PARENT_SCREEN").toString() ) )&&
						  (
							propertyView.getStatusId() ==null ||
							propertyView.getStatusId().compareTo(10017L)!=0
						  )
					   )
					 {
						 propertyView.setStatusId(domainDataView.getDomainDataId());
					 }
					 propertyView.setUpdatedBy(getLoggedInUser());
					 propertyView.setUpdatedOn(new Date());
					 propertyView.setInvestmentPurposeId(Long.valueOf(proeprtyInvestmentPurpose.getValue().toString()));
					 if(floorsDataList!=null)
					 {
					
						 propertyView.setNoFloors(getTotalFloorExceptGround(floorsDataList));	 
						 DomainDataView domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.FLOOR_TYPE,WebConstants.FloorType.MEZZANINE_FLOOR);
						 propertyView.setNoOfMezzanineFloors(Long.valueOf(getNoOfFloors(domainDataView.getDomainDataId())));
						 domainDataView = utilityServiceAgent.getDomainDataByValue(WebConstants.FLOOR_TYPE,WebConstants.FloorType.PARKING_FLOOR);
						 propertyView.setNoOfParkingFloors(Long.valueOf(getNoOfFloors(domainDataView.getDomainDataId())));
					 }
					 if(endowedName.getValue()!=null)
						  propertyView.setEndowedName(endowedName.getValue().toString());
					 if(propertyNameEn.getValue()!=null)
						  propertyView.setPropertyNameEn( propertyNameEn.getValue().toString().trim() );
					 
					 if(endowedMinorNo.getValue()!=null)
						 propertyView.setEndowedMinorNo(endowedMinorNo.getValue().toString());
					 if(ewUtilityNo.getValue()!=null)
						 propertyView.setEWUtilityNo(ewUtilityNo.getValue().toString());
					 if(projectNumber.getValue()!=null)
						 propertyView.setProjectNo(projectNumber.getValue().toString());
					 if(streetInformation.getValue()!=null )
						   propertyView.setStreetInformation(streetInformation.getValue().toString());
					 if(unitDataList!=null)
					    propertyView.setNoOfUnits(Long.valueOf(unitDataList.size()));
					 if(landArea.getValue()!=null && landArea.getValue().toString().trim().length()>0)
						 propertyView.setLandArea(Double.valueOf(landArea.getValue().toString()));
					 if(builtInArea.getValue()!=null && builtInArea.getValue().toString().trim().length()>0)
						 propertyView.setBuiltInArea(Double.valueOf(builtInArea.getValue().toString()));
					 if(numberOfLifts.getValue()!=null && numberOfLifts.getValue().toString().trim().length()>0)
						 propertyView.setNoOfLifts(Long.valueOf(numberOfLifts.getValue().toString()));
					 if(numberOfLifts.getValue()!=null && numberOfLifts.getValue().toString().trim().length()>0)
						 propertyView.setNoOfLifts(Long.valueOf(numberOfLifts.getValue().toString()));

					 // Added by Majid these fields were ReadOnly before and we need them in XLS reports reportLandBuildingTemplate
					 if(totalNoOfParkingFloors.getValue()!=null && totalNoOfParkingFloors.getValue().toString().trim().length()>0)
						 propertyView.setNoOfParkingFloors(Long.valueOf(totalNoOfParkingFloors.getValue().toString()));

					// Added by Majid these fields were ReadOnly before and we need them in XLS reports reportLandBuildingTemplate
					 if(totalMezzanineFloors.getValue()!=null && totalMezzanineFloors.getValue().toString().trim().length()>0)
						 propertyView.setNoOfMezzanineFloors(Long.valueOf(totalMezzanineFloors.getValue().toString()));



					 if(buildingCoordinates.getValue()!=null && buildingCoordinates.getValue().toString().trim().length()>0)
						 propertyView.setBuildingCoordinates(buildingCoordinates.getValue().toString());
					 if(totalAnnualRent.getValue()!=null && totalAnnualRent.getValue().toString().trim().length()>0)
						 propertyView.setTotalAnnualRent(Double.valueOf(totalAnnualRent.getValue().toString()));
					 if(!propertyType.getValue().toString().equals("-1"))
						 propertyView.setTypeId(Long.valueOf(propertyType.getValue().toString()));
					 if(!propertyUsage.getValue().toString().equals("-1"))
						 propertyView.setUsageTypeId(Long.valueOf(propertyUsage.getValue().toString()));
					 if(!ownershipType.getValue().toString().equals("-1"))
						 propertyView.setOwnershipTypeId(Long.valueOf(ownershipType.getValue().toString()));
					 if(!proeprtyInvestmentPurpose.getValue().toString().equals("-1"))
						 propertyView.setInvestmentPurposeId(Long.valueOf(proeprtyInvestmentPurpose.getValue().toString()));
					 
					 if(viewMap.get("ASSET_ID") != null)
					 {
						propertyView.setAssetId((Long) viewMap.get("ASSET_ID"));
					 }
					 else
						 propertyView.setAssetId(null);
					 if( getEndowmentId() != null && getEndowmentId().trim().length() > 0 )
					 {
						propertyView.setEndowmentId( Long.valueOf( getEndowmentId() ) );
					 }
					 else
						 propertyView.setEndowmentId(  null );
					 //if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)==null)
					 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)==null)
					 {
						 propertyView.setCreatedBy(getLoggedInUser());
						 propertyView.setCreatedOn(new Date());
						 propertyView.setIsDeleted(new Long(0));
						 propertyView.setRecordStatus(new Long(1));

						 propertyView = propertyServiceAgent.addProperty(propertyView);
						 if(getIsArabicLocale())
							 statusText.setValue(propertyView.getStatusTypeAr());
						 else
							 statusText.setValue(propertyView.getStatusTypeEn());	
						 propertyReferenceNumber.setValue(propertyView.getPropertyNumber());
						 generateFloor(propertyView.getPropertyId());
						 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_SAVED_SUCCESSFULLY));
					 }
					 else
					 {

						 propertyView=propertyServiceAgent.updatePropertyForReceiveProperty(propertyView);
						 
						 // condition for save and update receiving and evaluation team 
						 if(propertyView!=null )
						 {
							 // in case of update receiving team 
							 if(
								  propertyView.getTeamViewByReceivingTeamId()!=null && 
								  propertyView.getTeamViewByReceivingTeamId().getTeamId() !=null
							   )
							 {
								 List<TeamMemberView> teamMemberViewList = 	getTeamMemberList( getReceivingDataList() );
								 TeamView teamView = new TeamView();
								 teamView.setTeamId( propertyView.getTeamViewByReceivingTeamId().getTeamId() );
								 propertyServiceAgent.addUpdateTeamMembers(teamMemberViewList, teamView);
							 }else if(getReceivingDataList().size()>0)
							 { 
								 // in case of add receiving team 
								 saveReceivingTeam(getReceivingDataList());
							 }
//							 persistEvaluationTeam();
							 viewRootMap.remove("RECEIVING_TEAM_DATA_LIST");
							 viewRootMap.remove("EVALUATION_TEAM_DATA_LIST");
						 }
						 
						 
						 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UPDATED_SUCCESSFULLY));
						 // for refrashing team tabs
						 propertyView = propertyServiceAgent.getPropertyByID(propertyView.getPropertyId());
						 viewRootMap.put(WebConstants.Property.PROPERTY_VIEW, propertyView);
						 fillReceivingTeamInfo(propertyView);
						 fillEvaluationTeamInfo(propertyView);
					 }


					 //sessionMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
					 viewRootMap.put(WebConstants.Property.PROPERTY_VIEW, propertyView);
					 saveLocation();
					 loadAttachmentsAndComments(propertyView.getPropertyId());
					 saveCommentsAttachments(propertyView.getPropertyId());

					 if((viewRootMap.containsKey("PROPERTY_INVENTORY_MODE") && viewRootMap.get("PROPERTY_INVENTORY_MODE")!=null)||
							 (showTabsHidden.getValue()!=null&&showTabsHidden.getValue().equals("inventory")))
					 {
						 savedModeInventory();
						 showTabsHidden.setValue("inventory");
					 }
					 else
					 {
						 //if property is saved after received then keep the layoyt as it is
						 if(propertyView.getStatusId()!=null && propertyView.getStatusId().compareTo(10017L)!=0 )
						 {
								//This code is written to hide the submit button
								//because we don't need submit button while we have already received the property.
								 
								 if(
										 viewMap.get("PARENT_SCREEN")!=null && 
									     viewMap.get("PARENT_SCREEN").toString().compareTo("PROJECT")==0 
									)
									
								 {
									 	boolean isLand=false; 
									 	if(viewMap.get("PROPERTY_TYPE_LAND")!=null)
									 		isLand=(Boolean) viewMap.get("PROPERTY_TYPE_LAND");
									 	 if(!isLand)
									 	 { draftMode();
									 	    submitButton.setRendered(false);
									 	 }
									 	 else
									 	 {
									 		 normalMode();
									 		 saveButton.setRendered(false);
									 		
									 	 }
								}
								 else
									 savedMode();
								 showTabsHidden.setValue(true);
						 }
					 }

				 }
				 else
				 {
				 	normalMode();
				 	return "failure";
				 }
				 logger.logInfo(methodName + "completed...");
			 }
			 catch(Exception e)
			 {
				 logger.LogException(methodName + "crashed...",e);
				 if(errorMessages!=null)
					   errorMessages = new ArrayList<String>(0);
					 
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				 try
				 {
				 normalMode();
				 }
				 catch(Exception ez)
				 {
					 logger.LogException(methodName + "crashed...",ez);
				 }
				 return "failure";
				 
			 }
		 return "";
	 }
	 
	 private Long getTotalFloorExceptGround(List<ReceivePropertyFloorView> floorList) {
		 String methodName ="getTotalFloorExceptGround";
			logger.logInfo(methodName+"|"+"Start..");
			Long totalFloors = 0L;
				
				floorTypeGround = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_GROUND);	
				floorTypeBasement = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_BASEMENT);	
			
			try	
	    	{
				for(ReceivePropertyFloorView floor : floorList)
				{
					if(floorTypeGround.getDomainDataId().compareTo(floor.getTypeId()) != 0 && floorTypeBasement.getDomainDataId().compareTo(floor.getTypeId()) != 0)
						totalFloors++;
				}
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");
		return totalFloors;
	}
	public List<TeamMemberView> getTeamMemberList (List<UserView> listUserView)
		{
			List<TeamMemberView> teamMemberViewList = new ArrayList<TeamMemberView>();
			TeamMemberView teamMemberView=new TeamMemberView();
			for (UserView selectItem : listUserView) 
			{
				teamMemberView=new TeamMemberView();
				logger.logInfo("|"+"loginId.."+selectItem.getUserName().toString());
				// in case of update 
				if(selectItem.getTeamMemberId()!=null)
				{
					teamMemberView.setTeamMemberId(selectItem.getTeamMemberId());
					teamMemberView.setLoginId(selectItem.getUserName().toString());
					teamMemberView.setIsDeleted(selectItem.getTeamMemberIsDeleted());
					teamMemberView.setRecordStatus(selectItem.getTeamMemberRecordStatus());
					teamMemberView.setUpdatedOn(new Date());
					teamMemberView.setCreatedOn(selectItem.getTeamMemberCreatedOn());
					teamMemberView.setCreatedBy(selectItem.getTeamMemberCreatedBy());
					teamMemberView.setUpdatedBy(getLoggedInUser());
					teamMemberViewList.add(teamMemberView);
				}else{// in case of Add 
					teamMemberView.setLoginId(selectItem.getUserName().toString());
					teamMemberView.setIsDeleted(new Long(0));
					teamMemberView.setRecordStatus(new Long(1));
					teamMemberView.setUpdatedOn(new Date());
					teamMemberView.setCreatedOn(new Date());
					teamMemberView.setCreatedBy(getLoggedInUser());
					teamMemberView.setUpdatedBy(getLoggedInUser());
					teamMemberViewList.add(teamMemberView);
				}
		
			}
		  
			return teamMemberViewList;	
		}
	 

	 public String saveLocation(){
		 String msg = "saveLocation";
		 logger.logInfo(msg+"started");
		 try{
			 //propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
			 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
			 contactInfoView.setAddress1(addressLineOne.getValue().toString().trim());
			 if(contactInfoId.getValue()!=null && contactInfoId.getValue().toString().trim().length()>0)
				 contactInfoView.setContactInfoId(Long.valueOf(contactInfoId.getValue().toString()));
			 if(streetInformation.getValue()!=null && streetInformation.getValue().toString().trim().length()>0)
			 contactInfoView.setStreet(streetInformation.getValue().toString());
			 if(this.getCountryId()!=null&&this.getCountryId().trim().length()>0&&Long.valueOf(this.getCountryId())!=-1)
				 contactInfoView.setCountryId(Long.valueOf(this.getCountryId()));
			 if(this.getStateId()!=null&& this.getStateId().trim().length()>0&&Long.valueOf(this.getStateId())!=-1)
				 contactInfoView.setStateId(Long.valueOf(this.getStateId()));
			 if(this.getCityId()!=null&&Long.valueOf(this.getCityId())!=-1)
				 contactInfoView.setCityId(Long.valueOf(this.getCityId()));
			 contactInfoView.setCreatedOn(new Date());
			 contactInfoView.setCreatedBy(getLoggedInUser());
			 contactInfoView.setUpdatedBy(getLoggedInUser());
			 contactInfoView.setUpdatedOn(new Date());
			 contactInfoView.setIsDeleted(new Long(0));
			 contactInfoView.setRecordStatus(new Long(1));
			 contactInfoId.setValue(propertyServiceAgent.persistPropertyContactInfo(contactInfoView,propertyView).toString());
			 logger.logInfo(msg+"completed");
		 }
		 catch(Exception e)
		 {
			 logger.LogException(msg+"|Error Occured ",e);
		 }
		 return "";

	 }

	 @SuppressWarnings("unchecked")
	private void fillValues(PropertyView propertyView){
		 String msg = "fillValues";
		 logger.logInfo(msg+"started");
		 propertyReferenceNumber.setValue(propertyView.getPropertyNumber());
		 commercialName.setValue(propertyView.getCommercialName());
		 
		 if(getIsArabicLocale())
			 statusText.setValue(propertyView.getStatusTypeAr());
		 else
			 statusText.setValue(propertyView.getStatusTypeEn());
		 projectNumber.setValue(propertyView.getProjectNo());
		 endowedName.setValue(propertyView.getEndowedName());
		 propertyNameEn.setValue( propertyView.getPropertyNameEn() );
		 endowedMinorNo.setValue(propertyView.getEndowedMinorNo());
		 if(propertyView.getOwnershipTypeId()!=null)
			 ownershipType.setValue(propertyView.getOwnershipTypeId().toString());
		 proeprtyInvestmentPurpose.setValue(propertyView.getInvestmentPurposeId().toString());
		 propertyUsage.setValue(propertyView.getUsageTypeId().toString());
		 propertyType.setValue(propertyView.getTypeId().toString());
		 ewUtilityNo.setValue(propertyView.getEWUtilityNo());
		 buildingCoordinates.setValue(propertyView.getBuildingCoordinates());
		 landArea.setValue(propertyView.getLandArea());
		 builtInArea.setValue(propertyView.getBuiltInArea());
		 totalNoOfFloors.setValue(propertyView.getNoFloors());
		 totalMezzanineFloors.setValue(propertyView.getNoOfMezzanineFloors());
		 totalNoOfUnits.setValue(propertyView.getNoOfUnits());
		 totalNoOfParkingFloors.setValue(propertyView.getNoOfParkingFloors());
		 numberOfLifts.setValue(propertyView.getNoOfLifts());
		 totalAnnualRent.setValue(propertyView.getTotalAnnualRent());
		 areaInSquare.setValue("0");
		 bedRooms.setValue("0");
		 livingRooms.setValue("0");
		 bathRooms.setValue("0");
		 landNumber.setValue(propertyView.getLandNumber()) ;
		 if (StringHelper.isNotEmpty(propertyView.getCostCenter())) {
			 viewMap.put("NEW_CC", propertyView.getCostCenter().toString());
		}
		if (StringHelper.isNotEmpty(propertyView.getOldCostCenter())) {
			viewMap.put("OLD_CC", propertyView.getOldCostCenter().toString());
		}
		 if(propertyView.getCategoryId()!=null)
			 ownershipType.setValue(propertyView.getCategoryId().toString());
		 try 
		 {
		  if(contactInfoView == null || contactInfoView.getContactInfoId()== null) 
		    fillContactInfo(propertyView.getPropertyId());
		 fillOwnersInfo(propertyView.getPropertyId());
		 fillFacilitiesInfo(propertyView.getPropertyId());
		 fillFloors();
		 if(propertyViewWithFloorsAndUnits.getFloorView().size()==0)
			 propertyViewWithFloorsAndUnits = propertyServiceAgent.getPropertyByIdWithFloorsAndUnits(propertyView.getPropertyId());
		 
		
			 fillUnits(propertyViewWithFloorsAndUnits);
		 if(viewRootMap.get("RECEIVING_TEAM_DATA_LIST")==null)
		 fillReceivingTeamInfo(propertyView);
		 if(viewRootMap.get("EVALUATION_TEAM_DATA_LIST")==null)
		 fillEvaluationTeamInfo(propertyView);
		 if( viewMap.get("MINOR_DD_ID")!= null )
			{
				String minorId= viewMap.get("MINOR_DD_ID").toString();
				if( ownershipType.getValue() != null && ownershipType.getValue().toString().compareTo("-1") != 0 && ownershipType.getValue().toString().compareTo(minorId) == 0) 
				{
					viewMap.put("IS_MINOR", true);
				}
			}
		  if(propertyView.getAssetId() != null)
		  {
			  AssetMemsView assetMems = new InheritanceFileService().getAssetMemsById(propertyView.getAssetId());
			  txtAssetNumber.setValue(assetMems.getAssetNumber());
			  txtAssetNameEn.setValue(assetMems.getAssetNameEn());
			  txtAssetNameAr.setValue(assetMems.getAssetNameAr());
			  txtAssetDesc.setValue(assetMems.getDescription());
			  if( viewMap.get("MINOR_DD_ID")!= null )
				{
					String minorId= viewMap.get("MINOR_DD_ID").toString();
					if( ownershipType.getValue() != null && ownershipType.getValue().toString().compareTo("-1") != 0 && ownershipType.getValue().toString().compareTo(minorId) == 0) 
					{
						viewMap.put("IS_MINOR", true);
					}
				}
			  ownershipType.setDisabled(true);
		  }
		  if( propertyView.getEndowmentId() != null )
		  {
			  setEndowmentId( propertyView.getEndowmentId().toString() );
			  txtEndowmentRef.setValue( propertyView.getEndowmentReference() );
			  ownershipType.setDisabled(true);
		  }
			 

			 logger.logInfo(msg+"completed");
		 }
		 catch (PimsBusinessException e) 
		 {
			 
			 logger.LogException(msg+"|Error Occured ",e);
			 
		 }
		 catch (Exception e) 
		 {
			 
			 logger.LogException(msg+"|Error Occured ",e);
			 
		 }
	  
	 }
	 private void fillContactInfo(Long propertyId){
		 String msg = "fillContactInfo";
		 logger.logInfo(msg+"started");
		 try {
			 
			 
			 
			 contactInfoView = propertyServiceAgent.getPropertyContactInfo(propertyId);
			 if(contactInfoView!=null)
			 {

				 contactInfoId.setValue(contactInfoView.getContactInfoId());
				 addressLineOne.setValue(contactInfoView.getAddress1());
				 addressLineTwo.setValue(contactInfoView.getAddress2());
				 streetInformation.setValue(contactInfoView.getStreet());
				 if(contactInfoView.getCountryId()!=null)
					 this.setCountryId(contactInfoView.getCountryId().toString());
				 if(contactInfoView.getStateId()!=null)
					 this.setStateId(contactInfoView.getStateId().toString());
				 if(contactInfoView.getCityId()!=null)
					 this.setCityId(contactInfoView.getCityId().toString());
				 if(this.getCountryId()!=null)
				 {
					 loadCountry();
					 loadState();
				 }

				 if(this.getStateId()!=null)
					 loadCity();
				 

			 }
			 logger.logInfo(msg+" completed");
		 } 
		 catch (PimsBusinessException e) 
		 {
			 logger.LogException(msg+"|Error Occured ",e);
		 }

	 }
	 private void fillReceivingTeamInfo(PropertyView propertyView){
		 String msg = "fillReceivingTeamInfo";
		 logger.logInfo(msg+" started");
		 List<UserView> userViewList = new ArrayList<UserView>();
		 UserView userView = null;
		 TeamView teamView =propertyView.getTeamViewByReceivingTeamId();
		 TeamMemberView tmv =null;
		 if(teamView!=null)	{	
			 Iterator it = teamView.getTeamMembersView().iterator();
			 while(it.hasNext()){
				 userView = new UserView();
				 tmv = (TeamMemberView)it.next();

				 UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
				 userView.setUserName(tmv.getLoginId());

				 try {
					 List<UserView> tempList = new ArrayList<UserView>();
					 tempList = utilityServiceAgent.getUsers(userView);
					 if(tempList.size()>0)
					 {
						 if(tmv.getIsDeleted().compareTo(1L)==0)
							 continue;
						 userView =tempList.get(0);
						 userView.setTeamMemberId(tmv.getTeamMemberId());
						 userView.setTeamMemberIsDeleted(tmv.getIsDeleted());
						 userView.setTeamMemberRecordStatus(tmv.getRecordStatus());
						 userView.setTeamMemberCreatedBy(tmv.getCreatedBy());
						 userView.setTeamMemberCreatedOn(tmv.getCreatedOn());
						 userView.setTeamMemberUpdatedBy(tmv.getUpdatedBy());
						 userView.setTeamMemberUpdatedOn(tmv.getUpdatedOn());
					 }
					 
				 } catch (PimsBusinessException e) {
					 // TODO Auto-generated catch block
					 e.printStackTrace();
				 }
				 userViewList.add(userView);
				 viewRootMap.put("RECEIVING_TEAM_DATA_LIST",userViewList);
			 }
		 }
		 logger.logInfo(msg+" completed");
	 }
	 private void fillEvaluationTeamInfo(PropertyView propertyView){
		 String msg = "fillEvaluationTeamInfo";
		 logger.logInfo(msg+" started");
		 List<UserView> userViewList = new ArrayList<UserView>();
		 UserView userView = null;
		 TeamView teamView =propertyView.getTeamByEvaluationTeamId();
		 TeamMemberView tmv =null;
		 if(teamView!=null){
			 Iterator it = teamView.getTeamMembersView().iterator();
			 while(it.hasNext()){
				 userView = new UserView();
				 tmv = (TeamMemberView)it.next();

				 UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
				 userView.setUserName(tmv.getLoginId());
				 try {
					 List<UserView> tempList = new ArrayList<UserView>();
					 tempList = utilityServiceAgent.getUsers(userView);
					 if(tempList.size()>0)
					 {
						 if(tmv.getIsDeleted().compareTo(1L)==0)
						 continue;
						 
						 userView =tempList.get(0);
						 userView.setTeamMemberId(tmv.getTeamMemberId());
						 userView.setTeamMemberIsDeleted(tmv.getIsDeleted());
						 userView.setTeamMemberRecordStatus(tmv.getRecordStatus());
						 userView.setTeamMemberCreatedBy(tmv.getCreatedBy());
						 userView.setTeamMemberCreatedOn(tmv.getCreatedOn());
						 userView.setTeamMemberUpdatedBy(tmv.getUpdatedBy());
						 userView.setTeamMemberUpdatedOn(tmv.getUpdatedOn());
					 }


				 } catch (PimsBusinessException e) {
					 // TODO Auto-generated catch block
					 logger.LogException(msg+"|Error Occured ",e);
					 e.printStackTrace();
				 }
				 userViewList.add(userView);
				 viewRootMap.put("EVALUATION_TEAM_DATA_LIST",userViewList);
			 }
		 }
		 logger.logInfo(msg+"completed");

	 }
	 private void fillOwnersInfo(Long propertyId){
		 String msg ="fillOwnersInfo";
		 logger.logInfo(msg+" started");
		 try {
			 Set<PropertyOwnerView> ownerView = new HashSet<PropertyOwnerView>();
			 ownerView = propertyServiceAgent.getPropertyOwners(propertyView.getPropertyId());
			 if(ownerView!=null){
				 loadPropertyOwnersDataList(ownerView);
			 }
			 logger.logInfo(msg+"completed");
		 }
		 catch (PimsBusinessException e) 
		 {
			 logger.LogException(msg+"|Error Occured ",e);
		 }

	 }
	 private void fillFacilitiesInfo(Long propertyId){
		 String msg = "fillFacilitiesInfo";

		 try {
			 logger.logInfo(msg+" Started");
			 Set<PropertyFacilityView> facilityView = new HashSet<PropertyFacilityView>();
			 facilityView = propertyServiceAgent.getPropertyFacilities(propertyView.getPropertyId());
			 if(facilityView!=null){
				 loadPropertyFacilityDataList(facilityView);
			 }
			 logger.logInfo(msg+" completed");
		 } catch (PimsBusinessException e) {
			 // TODO Auto-generated catch block
			 logger.LogException(msg+"|Error Occured ",e);
			 e.printStackTrace();
		 }

	 }
	 private void fillFloors(){



		 loadPropertyFloorsDataList(true);


	 }
	 private void fillUnits(ReceivePropertyPropertyView propertyViewWithFloorsAndUnits){



		 loadPropertyUnitDataList(propertyViewWithFloorsAndUnits);


	 }
	 @SuppressWarnings("unchecked")
	 public String saveRentForProperty(){
		 String msg = "saveRentForProperty";
		 logger.logInfo(msg+ "started");

		 try{

			 Iterator itFloor = propertyViewWithFloorsAndUnits.getFloorView().iterator();
			 while(itFloor.hasNext()){
				 receivePropertyFloorView = (ReceivePropertyFloorView)itFloor.next();
				 Iterator itUnits = receivePropertyFloorView.getUnitView().iterator();
				 while(itUnits.hasNext()){

					 receivePropertyUnitView =(ReceivePropertyUnitView)itUnits.next();

					 receivePropertyUnitView.setRentValue(Double.valueOf(rentValue.getValue().toString()));
					 if(propertyAuctionOpeningPriceText.getValue()!=null)
						 receivePropertyUnitView.setRequiredPrice(Double.valueOf(propertyAuctionOpeningPriceText.getValue().toString()));
					 if(propertyCategory.getValue()!=null)
						 receivePropertyUnitView.setUnitTypeId(Long.valueOf(propertyCategory.getValue().toString()));
					 if(propertyRentProcessSelectOneMenu.getValue()!=null)
						 receivePropertyUnitView.setRentProcessId(Long.valueOf(propertyRentProcessSelectOneMenu.getValue().toString()));
					 if(propertyGracePeriodTypeSelectOneMenu.getValue()!=null)
						 receivePropertyUnitView.setGracePeriodTypeId(Long.valueOf(propertyGracePeriodTypeSelectOneMenu.getValue().toString()));
					 if(propertyGracePeriodText.getValue()!=null)
						 receivePropertyUnitView.setGracePeriod(Long.valueOf(propertyGracePeriodText.getValue().toString()));
					 if(propertyDepositAmountText.getValue()!=null)
						 receivePropertyUnitView.setDepositAmount(Double.valueOf(propertyDepositAmountText.getValue().toString()));

					 propertyServiceAgent.updateUnitForReceiveProperty(receivePropertyUnitView);

				 }
			 }


			 messages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReceiveProperty.MSG_RENT_FOR_PROPERTY_SAVED_SUCCESSFULLY));
			 logger.logInfo(msg+"completed");
		 }
		 catch(Exception e){
			 
			 logger.LogException(msg+"|Error Occured ",e);
		 }
		 return "";

	 }

	 public String saveRentForFloor(){
		 String msg = "saveRentForFloor";
		 logger.logInfo(msg+" started");
		 try{
			 Iterator itUnits ;
			 if(floorHiddenId.getValue()==null){
				 itUnits = receivePropertyFloorView.getUnitView().iterator();
			 }
			 else{
				 receivePropertyFloorView = (ReceivePropertyFloorView)sessionMap.get("FLOOR_VIEW_FOR_EDIT");
				 itUnits = receivePropertyFloorView.getUnitView().iterator();
			 }
			 while(itUnits.hasNext()){

				 receivePropertyUnitView =(ReceivePropertyUnitView)itUnits.next();
				 if(rentValueForUnits.getValue()!=null)
					 receivePropertyUnitView.setRentValue(Double.valueOf(rentValueForUnits.getValue().toString()));

				 propertyServiceAgent.updateUnitForReceiveProperty(receivePropertyUnitView);		
			 }



			 messages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReceiveProperty.MSG_RENT_FOR_FLOOR_SAVED_SUCCESSFULLY));
			 logger.logInfo(msg+" completed");
		 }
		 catch(Exception e){
			 logger.LogException(msg+"|Error Occured ",e);
			 
		 }
		 return "";

	 }

	 public String saveRentForUnit(){
		 String msg = "saveRentForUnit";
		 try{
			 logger.logInfo(msg+" started");
			 if(unitHiddenId.getValue()!=null)
				 receivePropertyUnitView = (ReceivePropertyUnitView)sessionMap.get("UNIT_VIEW_FOR_EDIT");

			 if(rentValueForUnits.getValue()!=null)
				 receivePropertyUnitView.setRentValue(Double.valueOf(rentValueForUnits.getValue().toString()));

			 propertyServiceAgent.updateUnitForReceiveProperty(receivePropertyUnitView);		



			 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_RENT_FOR_UNIT_SAVED_SUCCESSFULLY));
			 logger.logInfo(msg+" completed");
		 }
		 catch(Exception e){
			 
			 logger.LogException(msg+"|Error Occured ",e);
		 }
		 return "";

	 }


	 private String getLoggedInUser() 
	 {
		 String msg = "getLoggedInUser";
		 logger.logInfo(msg+"started");
		 FacesContext context = FacesContext.getCurrentInstance();
		 HttpSession session = (HttpSession) context.getExternalContext()
		 .getSession(true);
		 String loggedInUser = "";

		 if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			 UserDbImpl user = (UserDbImpl) session
			 .getAttribute(WebConstants.USER_IN_SESSION);
			 loggedInUser = user.getLoginId();
		 }
		 logger.logInfo(msg+" completed");
		 return loggedInUser;
	 }
	 private void loadPropertyFacilityDataList(boolean isUpdated)
	 {

		 String sMethod="loadPropertyFacilityDataList";
		 logger.logInfo(sMethod+" started");
		 facilityDataList = new ArrayList<PropertyFacilityView>();
		 Set<PropertyFacilityView> propertyFacilityViewSet;

		 PropertyFacilityView propertyFacilityView = new PropertyFacilityView();
//		 if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null&&!isUpdated)
//			 propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
		 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null&&!isUpdated)
			 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
		 try
		 {
			 if(propertyView.getPropertyId()!=null)
			 {
				 propertyFacilityViewSet = propertyServiceAgent.getPropertyFacilities(propertyView.getPropertyId());
				 Iterator<PropertyFacilityView> it = propertyFacilityViewSet.iterator();
				 while(it.hasNext())
				 {
					 propertyFacilityView = it.next();
					 if(propertyFacilityView.getIsDeleted()==0)	
						 facilityDataList.add(propertyFacilityView);
	
				 }
				 viewRootMap.put("FACILITY_DATA_LIST",facilityDataList);
			 }
			 logger.logInfo(sMethod + "completed");
		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
		 }


	 }
	 @SuppressWarnings("unchecked")
	 private void loadPropertyFacilityDataList(Set propertyFacilityViewSet)
	 {

		 String sMethod="loadPropertyFacilitDataList";
		 logger.logInfo(sMethod+"staerted");
		 facilityDataList = new ArrayList<PropertyFacilityView>();


		 PropertyFacilityView propertyFacilityView = new PropertyFacilityView();
		 try
		 {

			 Iterator it = propertyFacilityViewSet.iterator();
			 while(it.hasNext()){
				 propertyFacilityView=(PropertyFacilityView)it.next();
				 if(propertyFacilityView.getIsDeleted()==0)
					 facilityDataList.add(propertyFacilityView);

			 }
			 viewRootMap.put("FACILITY_DATA_LIST",facilityDataList);
			 logger.logInfo(sMethod+"finished");

		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
			 ex.printStackTrace();
		 }


	 }

	 private void loadPropertyOwnersDataList(boolean isUpdated)
	 {

		 String sMethod="loadOwnersDataList";
		 logger.logInfo(sMethod+" started");
		 ownerDataList = new ArrayList<PropertyOwnerView>();
		 Set<PropertyOwnerView> propertyOwnerViewSet;
		 PropertyOwnerView propertyOwnerView = new PropertyOwnerView();


//		 if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null && !isUpdated)
//			 propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
		 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null && !isUpdated)
			 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
		 try
		 {
			 propertyOwnerViewSet = propertyServiceAgent.getPropertyOwners(propertyView.getPropertyId());
			 Iterator it = propertyOwnerViewSet.iterator();


			 while(it.hasNext()){
				 propertyOwnerView=(PropertyOwnerView)it.next();
				 if(propertyOwnerView.getIsDeleted()==0)
					 ownerDataList.add(propertyOwnerView);

			 }
			 viewRootMap.put("OWNER_DATA_LIST",ownerDataList);
			 logger.logInfo(sMethod+" completed");
		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
			 
		 }


	 }
	 private void loadPropertyOwnersDataList(Set propertyOwnerViewSet)
	 {

		 String sMethod="loadOwnersDataList";
		 ownerDataList = new ArrayList<PropertyOwnerView>();

		 PropertyOwnerView propertyOwnerView = new PropertyOwnerView();


		 try
		 {

			 Iterator it = propertyOwnerViewSet.iterator();
			 while(it.hasNext()){
				 propertyOwnerView=(PropertyOwnerView)it.next();
				 if(propertyOwnerView.getIsDeleted()==0)
					 ownerDataList.add(propertyOwnerView);

			 }
			 viewRootMap.put("OWNER_DATA_LIST",ownerDataList);
			 logger.logInfo(sMethod+"completed");

		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
			 ex.printStackTrace();
		 }


	 }
	 @SuppressWarnings("unchecked")
	 private void loadPropertyReceivingTeamDataList()
	 {

		 String sMethod="loadReceivingTeamDataList";
		 logger.logInfo(sMethod+" started");
		 Boolean isAdd = true;
		 
		 if(sessionMap.get(WebConstants.Property.SEARCH_RECEIVING)!=null && sessionMap.get(WebConstants.TEAM_DATA_LIST)!=null)
			{
			 sessionMap.remove(WebConstants.Property.SEARCH_RECEIVING);
				// check duplication 
			   if(getReceivingDataList()!=null && getReceivingDataList().size()>0)
			   {
				   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
				   tempReceivingTeam = (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST);
				   sessionMap.remove(WebConstants.TEAM_DATA_LIST);
				   for(UserView uV:tempReceivingTeam)
				   {
					   for(UserView uV1: getReceivingDataList())
					   {
						   isAdd = true;
						   if(uV.getUserName().equals(uV1.getUserName()))
						   {
							 isAdd = false;
							 break;
						   }
					   }
					
					  if(isAdd)
					  {
						  uV.setTeamMemberIsDeleted(0L);
						  getReceivingDataList().add(uV);
					  }
				   }
				   viewRootMap.put("RECEIVING_TEAM_DATA_LIST",getReceivingDataList());
				   
			   } // for the first time when no member present 
			   else{
				   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
					for(UserView uV: (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST))
					{
						 uV.setTeamMemberIsDeleted(0L);
						  tempReceivingTeam.add(uV);
					}
				    sessionMap.remove(WebConstants.TEAM_DATA_LIST);	
				    setReceivingDataList(tempReceivingTeam);
			   }
			}
	
		 logger.logInfo(sMethod+"completed");


	 }
	 @SuppressWarnings("unchecked")
	 private void loadPropertyEvaluationTeamDataList()
	 {

		 String sMethod="loadEvaluationTeamDataList";
		 logger.logInfo(sMethod+"staerted");
         Boolean isAdd = true;
		 
		 if(sessionMap.get(WebConstants.Property.SEARCH_EVALUATION)!=null && sessionMap.get(WebConstants.TEAM_DATA_LIST)!=null)
			{
			 sessionMap.remove(WebConstants.Property.SEARCH_EVALUATION);
				// check duplication 
			   if(getEvaluationDataList()!=null && getEvaluationDataList().size()>0)
			   {
				   List<UserView> tempEvaluationTeam = new ArrayList<UserView>();
				   tempEvaluationTeam = (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST);
				   sessionMap.remove(WebConstants.TEAM_DATA_LIST);
				   for(UserView uV:tempEvaluationTeam)
				   {
					   for(UserView uV1: getEvaluationDataList())
					   {
						   isAdd = true;
						   if(uV.getUserName().equals(uV1.getUserName()))
						   {
							 isAdd = false;
							 break;
						   }
					   }
					
					  if(isAdd)
					  {
						  uV.setTeamMemberIsDeleted(0L);
						  getEvaluationDataList().add(uV);
					  }
				   }
				   viewRootMap.put("EVALUATION_TEAM_DATA_LIST",getEvaluationDataList());
				   
			   } // for the first time when no member present 
			   else
			   {
				   List<UserView> tempEvaluationTeam = new ArrayList<UserView>();
					for(UserView uV: (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST))
					{
						 uV.setTeamMemberIsDeleted(0L);
						 tempEvaluationTeam.add(uV);
					}
				    sessionMap.remove(WebConstants.TEAM_DATA_LIST);	
				    setEvaluationDataList(tempEvaluationTeam);
			   }
			}
	
		 logger.logInfo(sMethod+"completed");
		 
	 }

	 @SuppressWarnings("unchecked")
	 private void loadPropertyUnitDataList(ReceivePropertyPropertyView propertyViewWithFloorsAndUnits)
	 {

		 String sMethod="loadPropertyUnitDataList";
		 logger.logInfo(sMethod+ "started");
		 unitDataList = new ArrayList<ReceivePropertyUnitView>();
		 Set<ReceivePropertyUnitView> propertyUnitViewSet;
		 ReceivePropertyFloorView propertyFloorView = new ReceivePropertyFloorView();
		 ReceivePropertyUnitView propertyUnitView = new ReceivePropertyUnitView();

		 try
		 {


			 Iterator it = propertyViewWithFloorsAndUnits.getFloorView().iterator();
			 while(it.hasNext()){
				 propertyFloorView=(ReceivePropertyFloorView)it.next();
				 Iterator itUnit = propertyFloorView.getUnitView().iterator();
				 while(itUnit.hasNext()){
					 propertyUnitView = (ReceivePropertyUnitView)itUnit.next();
					 if(getIsEnglishLocale()){
						 propertyUnitView.setUnitType(propertyUnitView.getUnitTypeEn());
						 propertyUnitView.setStatus(propertyUnitView.getStatusEn());
					 }
					 if(getIsArabicLocale()){
						 propertyUnitView.setUnitType(propertyUnitView.getUnitTypeAr());
						 propertyUnitView.setStatus(propertyUnitView.getStatusAr());


					 }
					 if(propertyUnitView.getIncludeFloorNumber()!=null&&
							 propertyUnitView.getIncludeFloorNumber().toString().equals("1"))
							 {
						 propertyUnitView.setUnitNumber(propertyUnitView.getFloorNumber()+
					                 ((propertyUnitView.getUnitPrefix()!=null && propertyUnitView.getUnitPrefix().trim().length()>0)?propertyUnitView.getUnitPrefix().trim():"")+
					                 String.valueOf(propertyUnitView.getUnitNo().longValue()));
	
//						 propertyUnitView.setUnitNumber(propertyUnitView.getFloorNumber()+propertyUnitView.getUnitPrefix()+String.valueOf(propertyUnitView.getUnitNo().intValue()));
							 }
					 unitDataList.add(propertyUnitView);

				 }

			 }
			 viewRootMap.put("UNITS_DATA_LIST",unitDataList);
			 logger.logInfo(sMethod+"completed");

		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
			 ex.printStackTrace();
		 }


	 }
	 @SuppressWarnings("unchecked")
	 private void loadPropertyFloorsDataList(Boolean update)
	 {
	 String msg ="loadPropertyFloorsDataList";
		 logger.logInfo(msg+" started");

	
		 if(viewRootMap.containsKey(WebConstants.GenerateUnits.PROPERTY_VIEW))
			 propertyView  = (PropertyView)viewRootMap.get(WebConstants.GenerateUnits.PROPERTY_VIEW);
//		 else if(sessionMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
//			 propertyView= (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
		 else if(viewMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
			 propertyView= (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
		 if(viewRootMap.get("FLOORS_DATA_LIST")!=null&&!update){
			 floorsDataList = (ArrayList<ReceivePropertyFloorView>)viewRootMap.get("FLOORS_DATA_LIST");
		 }
		 else
		 {
			 try 
			 {
				 if(propertyView!=null&&propertyView.getPropertyId()!=null)
				 {
					 
					 PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
					 floorsDataList =propertyServiceAgent.getPropertyFloorList(propertyView.getPropertyId());
					 viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
				 }
			 } catch (PimsBusinessException e) 
			 {
				 // TODO Auto-generated catch block
				 logger.LogException(msg+"|Error Occured ",e);
			 } 
		 }
		 calculateBuildingHeight(floorsDataList);

	 }
	 @SuppressWarnings("unchecked")
	 private void loadPropertyUnitsDataList(Boolean update)
	 {
		 String msg ="loadPropertyUnitsDataList";
		 logger.logInfo(msg+" started");
		 PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		 PropertyView propertyView = null;
//		 if(sessionMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
//			 propertyView= (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
		 if(viewMap.containsKey(WebConstants.Property.PROPERTY_VIEW))
			 propertyView= (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);

		 if(viewRootMap.get("UNITS_DATA_LIST")!=null&&!update){
			 unitDataList = (ArrayList<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST");
		 }
		 else{
			 try {
				 List<ReceivePropertyUnitView> tempUnitDataList = new ArrayList<ReceivePropertyUnitView>();
				 if(propertyView!=null&&propertyView.getPropertyId()!=null){
					 unitDataList =propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());


					 for(ReceivePropertyUnitView propertyUnitView :unitDataList){

						 if(getIsEnglishLocale()){
							 propertyUnitView.setUnitType(propertyUnitView.getUnitTypeEn());
							 propertyUnitView.setStatus(propertyUnitView.getStatusEn());
						 }
						 if(getIsArabicLocale()){
							 propertyUnitView.setUnitType(propertyUnitView.getUnitTypeAr());
							 propertyUnitView.setStatus(propertyUnitView.getStatusAr());


						 }

						 tempUnitDataList.add(propertyUnitView);

					 }

					 viewRootMap.put("UNITS_DATA_LIST",tempUnitDataList);
				 }
			 } catch (PimsBusinessException e) {
				 // TODO Auto-generated catch block
				 logger.LogException(msg+"|Error Occured ",e);
				 e.printStackTrace();
			 } 
		 }
		 logger.logInfo(msg+"finished");

	 }
	 @SuppressWarnings("unchecked")
	 public void loadAttachmentsAndComments(Long propertyId) throws Exception {
		 
		 
		 String methodName="loadAttachmentsAndComments|";
		 try
		 {
			 logger.logInfo(methodName + "started...");			
	
			 String repositoryId = "pimsDb";
			 viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.Property.PROCEDURE_TYPE_RECEIVE_PROPERTY);
             String externalId = WebConstants.Attachment.EXTERNAL_ID_PROPERTY;
			 viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
			 viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
			 viewMap.put("noteowner", WebConstants.PROPERTY);
			 if(propertyId!= null)
			 {
				 String entityId = propertyId.toString();
				 viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
				 viewMap.put("entityId", entityId);
			 }
			 logger.logInfo(methodName + "completed successfully!!!");
		 }
		 catch (Exception exception) 
		 {
			 logger.LogException(methodName + "crashed ", exception);
			 throw exception;
		 }
	 }
	    private void saveCommentsAttachments(Long referenceId)throws Exception
	    {
	    	
	    	saveComments(referenceId);
	    	saveAttachments(referenceId);
	    }
		private void saveComments(Long propertyId) throws Exception
		{
			String methodName="saveComments|";
			try{
				logger.logInfo(methodName + "started...");

				String notesOwner = WebConstants.PROPERTY;
				NotesController.saveNotes(notesOwner, propertyId);

				logger.logInfo(methodName + "completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException(methodName + "crashed ", exception);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				throw exception;
			}
		}
		public Boolean saveAttachments(Long referenceId)throws Exception
		 {
			 Boolean success = false;
			 try{
				 logger.logInfo("saveAtttachments started...");
				 if(referenceId!=null){
					 success = CommonUtil.updateDocuments();
				 }
				 logger.logInfo("saveAtttachments completed successfully!!!");
			 }
			 catch (Exception e) 
			 {
				 success = false;
				 logger.LogException("saveAtttachments crashed ", e);
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				 throw e;
			 }

			 return success;
		 }

	 

	 private void loadPropertyFloorsDataList(Set<ReceivePropertyFloorView> propertyFloorViewSet)
	 {

		 String sMethod="loadFloorsDataList";
		 logger.logInfo(sMethod+"started");
		 floorsDataList = new ArrayList<ReceivePropertyFloorView>();

		 ReceivePropertyFloorView propertyFloorView = new ReceivePropertyFloorView();



		 try
		 {

			 Iterator it = propertyFloorViewSet.iterator();
			 while(it.hasNext()){
				 propertyFloorView=(ReceivePropertyFloorView)it.next();
				 if(getIsArabicLocale())
					 propertyFloorView.setFloorStatus(propertyFloorView.getFloorStatusAr());
				 if(getIsEnglishLocale())
					 propertyFloorView.setFloorStatus(propertyFloorView.getFloorStatusEn());

				 floorsDataList.add(propertyFloorView);

			 }
			 viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
			 logger.logInfo(sMethod+" completed");

		 }
		 catch(Exception ex)
		 {
			 logger.LogException(sMethod+"|Error Occured ",ex);
			 ex.printStackTrace();
		 }


	 }

	 public static boolean isLoggedInUserCreatedRecord(String createdByUserId) throws Exception
		{
			boolean result = false;
			logger.logDebug( "isLoggedInUserCreatedRecord|createdByUserId:%s|",createdByUserId);
			User loggedInUser = SecurityManager.getUser(CommonUtil.getLoggedInUser() );
			User createdByUser = SecurityManager.getUser(createdByUserId);
			
			List<UserGroup> loggedInUserGroups =new ArrayList<UserGroup>(0);
			List<UserGroup> createdByUserGroups = new ArrayList<UserGroup>(0);

			loggedInUserGroups = loggedInUser.getUserGroups();
			createdByUserGroups = createdByUser.getUserGroups();
			
			if( loggedInUserGroups != null && loggedInUserGroups.size() > 0  && createdByUserGroups != null && createdByUserGroups.size() > 0 )
			{
				LoggedInLoop:	
				for(UserGroup loggedInGroup : loggedInUserGroups)
				{
							for(UserGroup createdByGroup : createdByUserGroups)
							{
								if( loggedInGroup == null  || createdByGroup == null ) continue;
								if(loggedInGroup.getUserGroupId().compareTo(createdByGroup.getUserGroupId()) == 0)
								{
									result = true;
									break LoggedInLoop;
								}
							}
				}
			}
			return result;
		}
	 
	 @Override
	 @SuppressWarnings("unchecked")
	 public void prerender()
	 {

		 super.prerender();
		 try
		 {
					 propertyView = (PropertyView) viewMap.get(WebConstants.Property.PROPERTY_VIEW);
					 DomainDataView domainDataView = new DomainDataView();
					 propertyNameEn.setReadonly(false);
					 getUnitDataList();
					 if(
						 propertyView!=null&&
						 propertyView.getStatusId()!=null&& 
						 propertyView.getStatusId().toString().trim().length()>0
					   )
					 {
						 try
						 {
							 domainDataView = CommonUtil.getDomainDataFromId(this.getDdPropertyStatusList(), propertyView.getStatusId());
			
						 } catch (Exception e) 
						 {
							 logger.LogException("prerender|Error Occured ",e);
						 }	
					 }
					 else if( propertyView == null && !isPostBack())
					 {
						 setCountryId( "19" );
						 loadState( 19L);
						 setStateId( "20" );
						 loadCity(20L);
						 
						 
					 }
			
					 if(
						 showTabsHidden.getValue()!=null&&
						 showTabsHidden.getValue().equals(false)
					   )
					 {
						 normalMode();
					 }
					 if(domainDataView.getDataValue()!=null && domainDataView.getDataValue().toString().equals(WebConstants.Property.DRAFT))
						 draftMode();
					 else if(domainDataView.getDataValue()!=null && domainDataView.getDataValue().toString().equals(WebConstants.Property.NEW))
						 inventoryMode();
					 else if(domainDataView.getDataValue()!=null && domainDataView.getDataValue().toString().equals(WebConstants.Property.RECEIVED ))
					 {
						 saveAfterReceivedButton.setRendered(true);
					 }
					 PersonView personView;
					 String strPersonName="";
					 if(viewRootMap.get(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW)!=null)
					 {
						 personView = (PersonView)viewRootMap.get(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW);
						 if(personView.getFirstName()!=null)
							 strPersonName= strPersonName+personView.getFirstName();
						 if(personView.getMiddleName()!=null)
							 strPersonName= strPersonName+personView.getMiddleName();
						 if(personView.getLastName()!=null)
							 strPersonName= strPersonName+personView.getLastName();
						 if(strPersonName.trim().length()>0 &&!viewRootMap.containsKey("representativeRefNo"))
							 ownerRepresentative.setValue(strPersonName);
						 else if(personView.getCompanyName()!=null && !viewRootMap.containsKey("representativeRefNo"))
							 ownerRepresentative.setValue(personView.getCompanyName());
			
						 
					 }
					 if(viewRootMap.get(WebConstants.Person.PERSON_OWNER_VIEW)!=null)
					 {
						 personView = (PersonView)viewRootMap.get(WebConstants.Person.PERSON_OWNER_VIEW);
						 strPersonName="";
						 if(personView.getFirstName()!=null)
							 strPersonName= strPersonName+personView.getFirstName();
						 if(personView.getMiddleName()!=null)
							 strPersonName= strPersonName+personView.getMiddleName();
						 if(personView.getLastName()!=null)
							 strPersonName= strPersonName+personView.getLastName();
						 if(strPersonName.trim().length()>0 && !viewRootMap.containsKey("ownerReferenceNo"))
							 ownerReferenceNo.setValue(strPersonName);
						 else if(personView.getCompanyName()!=null && !viewRootMap.containsKey("ownerReferenceNo"))
							 ownerReferenceNo.setValue(personView.getCompanyName());
			
					 }
					 //if the project data is shifted from the project screen
					 if(sessionMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY")!=null)
						{
							ProjectDetailsView projectDetails;
							projectDetails=(ProjectDetailsView) sessionMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY");
							sessionMap.remove("PROJECT_VIEW_FOR_RECEIVING_PROPERTY");
							setHdnprojectNumber(projectDetails.getProjectNumber());
							//This hndProjectNumber will initialize the projectNumber field in next step.
							viewMap.put("PROJECT_VIEW_FOR_RECEIVING_PROPERTY", projectDetails);
						}
					 if(getHdnprojectNumber()!=null && !StringHelper.isEmpty(getHdnprojectNumber()))
						 {
						 	viewMap.put("HIDDEN_PROJECT_NUMBER", getHdnprojectNumber());
						 	projectNumber.setValue(getHdnprojectNumber());
						 }
					//if the project is selected then set the normalMode, previously normalMode was only being set in !ispostback()
					//if we don't set the normalMode then all  tabs will be appear in first step of receiving property
					 if(getHdnprojectNumber()!=null && !StringHelper.isEmpty(getHdnprojectNumber()))
						normalMode();
					//If recieveProperty screen is being open from project then hide the send button, only save the info.
				    //user should complete the procedure by navigating throughreceiveProperty screen
					//Allow the user to edit, thats why we set here draft mode. 
					 if(sessionMap.get("PARENT_SCREEN")!=null)
					 {
						 String parentScreen;
						 boolean isLand=false;
						 if(sessionMap.get("PROPERTY_TYPE_LAND")!=null)
							 isLand=(Boolean) sessionMap.get("PROPERTY_TYPE_LAND");
						 parentScreen=sessionMap.get("PARENT_SCREEN").toString();
						 sessionMap.remove("PARENT_SCREEN");
						 sessionMap.remove("PROPERTY_TYPE_LAND");
						 viewMap.put("PARENT_SCREEN",parentScreen);
						 viewMap.put("PROPERTY_TYPE_LAND",isLand);
						 logger.logDebug( "prerender| if(!isLand)");
						 if(!isLand)
						 {
						 	draftMode();
						 	submitButton.setRendered(false);
						 }
						 else
						 {
							 normalMode();
							 saveButton.setRendered(false);
						 }
						
					 }
					 
					 //check for errors
					 if(viewMap.get("ERROR_MSG_PROVIDE_VALID_FORM_AREA") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_VALID_FORM_AREA"))
					 {
					 	errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvedCorrectFromArea"));
					 	viewMap.remove("ERROR_MSG_PROVIDE_VALID_FORM_AREA");
					 }
					 
					 if(viewMap.get("ERROR_MSG_SELECT_UNIT_TYPE") != null && (Boolean)viewMap.get("ERROR_MSG_SELECT_UNIT_TYPE"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseSelectUnitType"));
						 viewMap.remove("ERROR_MSG_SELECT_UNIT_TYPE");
					 }
					 
					 if(viewMap.get("ERROR_MSG_PROVIDE_VALID_TO_AREA") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_VALID_TO_AREA"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvideCorrectToArea"));
						 viewMap.remove("ERROR_MSG_PROVIDE_VALID_TO_AREA");
					 }
					 
					 if(viewMap.get("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvideCorrectLimit"));
						 viewMap.remove("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT");
					 }
					 
					 if(viewMap.get("ERROR_MSG_PROVIDE_AMOUNT") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_AMOUNT"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvideAmount"));
						 viewMap.remove("ERROR_MSG_PROVIDE_AMOUNT");
					 }
					 
					 if(viewMap.get("ERROR_MSG_PROVIDE_UNIT_TYPE_AND_AMOUNT") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_UNIT_TYPE_AND_AMOUNT"))
					  {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvideUnitTypeAndAmount"));
						 viewMap.remove("ERROR_MSG_PROVIDE_UNIT_TYPE_AND_AMOUNT");
					  }
					 
					 if(viewMap.get("ERROR_MSG_PROVIDE_CORRECT_AMOUNT") != null && (Boolean)viewMap.get("ERROR_MSG_PROVIDE_CORRECT_AMOUNT"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.pleaseProvideCorrectAmount"));
						 viewMap.remove("ERROR_MSG_PROVIDE_CORRECT_AMOUNT");
					 }
					 
					 if(viewMap.get("SHOW_CURRENT_GRID") != null && (Boolean)viewMap.get("SHOW_CURRENT_GRID"))
					 {
						 errorMessages.add(CommonUtil.getBundleMessage("receiveProperty.errMsg.currentGridUpdateClearIt"));
						 viewMap.remove("SHOW_CURRENT_GRID");
					 }
					 
					 //success message
					 if(viewMap.get("SUCCESS_MSG_RECORDS_UPDATED") != null)
					 {
						 messages.add(viewMap.get("SUCCESS_MSG_RECORDS_UPDATED").toString()+ " " +CommonUtil.getBundleMessage("receiveProperty.successMsg.recordsUpdated"));
						 viewMap.remove("SUCCESS_MSG_RECORDS_UPDATED");
					 }
					 
					 showRentTabs = true;
					 if( viewMap.get(WebConstants.Property.PROPERTY_VIEW) != null)
					 {
						 propertyView = (PropertyView) viewMap.get(WebConstants.Property.PROPERTY_VIEW);
						  logger.logDebug( "propertyView.getCreatedBy():%s|",propertyView.getCreatedBy());
						 if(isUnderReceiving() && !isLoggedInUserCreatedRecord(propertyView.getCreatedBy()))
						 {
							 disableControlsForOtherGroup();
							 errorMessages =  new ArrayList<String>();
							 String formattedMsg = "";
							 if(viewMap.get("FORMATTED_AUTHORIZATION_MSG_TO_UPDATE") != null)
							 {
								 formattedMsg = viewMap.get("FORMATTED_AUTHORIZATION_MSG_TO_UPDATE").toString();
							 }
							 else{
								 formattedMsg = MessageFormat.format(CommonUtil.getBundleMessage("errMsg.receiveProperty"),CommonUtil.getCSVGroupsOfUserById(propertyView.getCreatedBy()));
								 viewMap.put("FORMATTED_AUTHORIZATION_MSG_TO_UPDATE",formattedMsg);
							 }
							 errorMessages.add(formattedMsg);
						 }
					 }
			 } catch (Exception e) 
			 {
				 logger.LogException("prerender|Error Occured ",e);
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 }
			 
		 
	 }
	 private void disableControlsForOtherGroup()throws Exception {
		 saveAfterReceivedButton.setRendered(false);
		 saveButton.setRendered(false);
		 submitButton.setRendered(false);
		 //For endowment because basic details will be added by someone else and unit details by other user.Need to verify if 
		 //apply only for endowment.
//		 addUnitButton.setRendered(false);
//		 addFloorButton.setRendered(false);
		 ownerSaveButton.setRendered(false);
		 saveFacilityButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);
		 addEvaluationTeam.setRendered(false);
		 addReceiveingTeam.setRendered(false);
		 if(viewRootMap.containsKey("DELETE_ACTION"))
			 viewRootMap.remove("DELETE_ACTION");
	}
	public List<PropertyFacilityView> getFacilityDataList()
	 {
		 String msg = "getFacilityDataList";
		 logger.logInfo(msg+" started");
		 if(facilityDataList!=null)
			 viewRootMap.put("FACILITY_DATA_LIST",facilityDataList);

		 facilityDataList = (ArrayList<PropertyFacilityView>)viewRootMap.get("FACILITY_DATA_LIST");
		 logger.logInfo(msg+ "completed");
		 return facilityDataList;
	 }
	 public List<PropertyOwnerView> getOwnerDataList()
	 {	
		 String msg = "getOwnerDataList";
		 logger.logInfo(msg+ "started");
		 if(ownerDataList!=null)
			 viewRootMap.put("OWNER_DATA_LIST",ownerDataList);

		 ownerDataList = (ArrayList<PropertyOwnerView>)viewRootMap.get("OWNER_DATA_LIST");
		 logger.logInfo(msg+" completed");
		 return ownerDataList;
	 }
	 @SuppressWarnings("unchecked")
	 public List<ReceivePropertyFloorView> getFloorsDataList()
	 {
		 if(viewRootMap.containsKey("FLOORS_DATA_LIST"))
			 floorsDataList = (ArrayList<ReceivePropertyFloorView>)viewRootMap.get("FLOORS_DATA_LIST");
		 return floorsDataList;
	 }
	 @SuppressWarnings("unchecked")
	 public List<ReceivePropertyUnitView> getUnitDataList()
	 {
		 if(viewRootMap.containsKey("UNITS_DATA_LIST"))
			 unitDataList = (ArrayList<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST");
		 
		 return unitDataList;
	 }

	 public boolean getIsArabicLocale() {
		 isArabicLocale = !getIsEnglishLocale();
		 return isArabicLocale;
	 }

	 public boolean getIsEnglishLocale() {

		 
		 WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		 LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		 isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	
		 return isEnglishLocale;
	 }



	 public String btnRemoveEvaluationItems_Click() 
	 {
		 String methodName = "btnRemoveEvaluationItems_Click";
		 logger.logInfo(methodName+" started");
		 try 
		 {
			 HashMap selectedUsersMap=new HashMap();
			 for(int i =0;i<selectedUsersEvaluationList.size();i++)
			 {
				 selectedUsersMap.put(selectedUsersEvaluationList.get(i).toString(), selectedUsersEvaluationList.get(i).toString());

			 }
			 removeItemsFromList(selectedUsersMap,"Evaluation");


			 logger.logInfo(methodName+ "finished");
		 } catch (Exception e) 
		 {
			 logger.logError(methodName + "|" + "Exception Occured::" + e);
		 }
		 return "";
	 }

	 public String btnAddEvaluationItems_Click() 
	 {
		 try 
		 {
			 addItemsToList("Evaluation");
		 } catch (Exception e) 
		 {
			 logger.logError("btnAddEvaluationItems_Click|Exception Occured::" + e);
		 }
		 return "";
	 }	
	 public String btnRemoveReceivingItems_Click() 
	 {
		 String methodName = "btnRemoveReceivingItems_Click";
		 logger.logInfo(methodName + "started");
		 try 
		 {
			 HashMap selectedUsersMap=new HashMap();
			 for(int i =0;i<selectedUsersReceivingList.size();i++)
			 {
				 selectedUsersMap.put(selectedUsersReceivingList.get(i).toString(), selectedUsersReceivingList.get(i).toString());

			 }
			 removeItemsFromList(selectedUsersMap,"Receiving");

			 logger.logInfo(methodName + "completed");


		 } catch (Exception e) 
		 {
			 logger.logError(methodName + "|" + "Exception Occured::" + e);
		 }
		 return "";
	 }

	 public String btnAddReceivingItems_Click() 
	 {
		 String methodName = "btnAddReceivingItems_Click";
		 logger.logInfo(methodName + "started");
		 try 
		 {
			 addItemsToList("Receiving");

			 logger.logInfo(methodName + "completed");
		 } catch (Exception e) 
		 {
			 logger.logError(methodName + "|" + "Exception Occured::" + e);
		 }
		 return "";
	 }
	 public List getSelectedUsersReceivingList() {
		 return selectedUsersReceivingList;
	 }

	 public void setSelectedUsersReceivingList(List selectedUsersReceivingList) {
		 this.selectedUsersReceivingList = selectedUsersReceivingList;
	 }
	 public List getSelectedUsersEvaluationList() {
		 return selectedUsersEvaluationList;
	 }

	 public void setSelectedUsersEvaluationList(List selectedUsersEvaluationList) {
		 this.selectedUsersEvaluationList = selectedUsersEvaluationList;
	 }

	 private void populateReceivingList(Set<User> usersSet)
	 {
		 String methodName="populateReceivingList";
		 logger.logInfo("|"+"Start..");
		 Iterator iter= usersSet.iterator();
		 while(iter.hasNext())
		 {
			 User user=(User)iter.next();  
			 SelectItem item=null; 
			 item =new SelectItem(user.getLoginId(),user.getFullName());
			 receivingTeamList.add(item);
			 sessionMap.put("SESSION_RECEIVING_TEAM_USERS",receivingTeamList);

		 }


		 logger.logInfo("|"+"Finish..");

	 }
	 private void removeItemsFromList(HashMap selectedUsersMap,String strType)
	 {
		 String msg = "removeItemsFromList";
		 logger.logInfo(msg + "started");
		 if(strType.equals("Receiving")){
			 if(selectedUsersReceivingList.size()>0 )
			 {
				 if( sessionMap.containsKey("SESSION_USER_RECEIVING"))
					 sessionMap.remove("SESSION_USER_RECEIVING");
				 //usersInspectionList.clear();
			 }
			 else
			 {
				 if(sessionMap.containsKey("SESSION_USER_RECEIVING"))
					 usersReceivingList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_RECEIVING");
			 }
			 List<SelectItem> tempList=new ArrayList<SelectItem>();
			 for(int j=0;j<usersReceivingList.size();j++)
			 {
				 SelectItem useritem=(SelectItem)usersReceivingList.get(j);
				 if(!selectedUsersMap.containsKey(useritem.getValue()))
				 {
					 tempList.add(useritem);
				 }

			 }
			 usersReceivingList= tempList;
			 sessionMap.put("SESSION_USER_RECEIVING",usersReceivingList);
		 }

		 if(strType.equals("Evaluation")){
			 if(selectedUsersEvaluationList.size()>0 )
			 {
				 if( sessionMap.containsKey("SESSION_USER_EVALUATION"))
					 sessionMap.remove("SESSION_USER_EVALUATION");
				 //usersInspectionList.clear();
			 }
			 else
			 {
				 if(sessionMap.containsKey("SESSION_USER_EVALUATION"))
					 usersEvaluationList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_EVALUATION");
			 }
			 List<SelectItem> tempList=new ArrayList<SelectItem>();
			 for(int j=0;j<usersEvaluationList.size();j++)
			 {
				 SelectItem useritem=(SelectItem)usersEvaluationList.get(j);
				 if(!selectedUsersMap.containsKey(useritem.getValue()))
				 {
					 tempList.add(useritem);
				 }

			 }
			 usersEvaluationList= tempList;
			 sessionMap.put("SESSION_USER_EVALUATION",usersEvaluationList);
		 }


		 logger.logInfo(msg+ "completed");
	 }
	 private void addItemsToList(String strType)
	 {
		 String msg = "addItemsToList";
		 logger.logInfo(msg+ "Started");
		 HashMap hMap=new HashMap();
		 if(strType.equals("Receiving")){    		
			 if(sessionMap.containsKey("SESSION_HASHMAP_RECEIVING_TEAM_USERS"))
				 hMap=(HashMap)sessionMap.get("SESSION_HASHMAP_RECEIVING_TEAM_USERS");
			 if(sessionMap.containsKey("SESSION_USER_RECEIVING"))
				 usersReceivingList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_RECEIVING");

			 for(int i =0;i<selectedReceivingTeamList.size();i++)
			 {
				 if(hMap.containsKey(selectedReceivingTeamList.get(i).toString()))
				 {
					 User user=(User) hMap.get(selectedReceivingTeamList.get(i).toString());
					 SelectItem item=null; 
					 item =new SelectItem(user.getLoginId(),user.getFullName());

					 if(!isItemExists(user,strType))
					 {
						 usersReceivingList.add(item);

					 }

				 }
			 }
			 sessionMap.put("USER_RECEIVING",usersReceivingList);
			 sessionMap.put("SESSION_USER_RECEIVING",usersReceivingList);
		 }

		 if(strType.equals("Evaluation")){    		
			 if(sessionMap.containsKey("SESSION_HASHMAP_EVALUATION_TEAM_USERS"))
				 hMap=(HashMap)sessionMap.get("SESSION_HASHMAP_EVALUATION_TEAM_USERS");
			 if(sessionMap.containsKey("SESSION_USER_EVALUATION"))
				 usersEvaluationList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_EVALUATION");

			 for(int i =0;i<selectedEvaluationTeamList.size();i++)
			 {
				 if(hMap.containsKey(selectedEvaluationTeamList.get(i).toString()))
				 {
					 User user=(User) hMap.get(selectedEvaluationTeamList.get(i).toString());
					 SelectItem item=null; 
					 item =new SelectItem(user.getLoginId(),user.getFullName());

					 if(!isItemExists(user,strType))
					 {
						 usersEvaluationList.add(item);

					 }

				 }
			 }
			 sessionMap.put("USER_EVALUATION",usersEvaluationList);
			 sessionMap.put("SESSION_USER_EVALUATION",usersEvaluationList);
		 }
		 logger.logInfo(msg+ "completed");
	 }
	 private boolean isItemExists(User user,String strType)
	 {
		 String methodName="isItemExists";
		 boolean isExist=false;
		 logger.logInfo("|"+"Start..");
		 if(strType.endsWith("Receiving")){


			 List<SelectItem>itemList= (List<SelectItem>)sessionMap.get("SESSION_USER_RECEIVING");
			 if(itemList!=null)
			 {
				 for(int i=0;i<itemList.size();i++)
				 {
					 SelectItem item=(SelectItem)itemList.get(i);
					 if(item.getValue().equals(user.getLoginId()))
					 {
						 isExist=true;
						 break;
					 }
				 }
			 }
		 }
		 logger.logInfo("|"+"Finish..");
		 return isExist;
	 }
	 public HtmlInputText getCommercialName() {
		 return commercialName;
	 }
	 public void setCommercialName(HtmlInputText commercialName) {
		 this.commercialName = commercialName;
	 }
	 public List<SelectItem> getPropertyItems() {
		 return propertyItems;
	 }
	 public void setPropertyItems(List<SelectItem> propertyItems) {
		 this.propertyItems = propertyItems;
	 }
	 public List<SelectItem> getReceivingTeamList() 
	 {
		 String msg = "getReceivingTeamList";
		 logger.logInfo(msg+" started");
		 Set<User> usersSet=new HashSet<User>(0);
		 if(sessionMap.containsKey("SESSION_RECEIVING_TEAM_USERS"))
			 usersSet=(Set<User>)sessionMap.get("SESSION_RECEIVING_TEAM_USERS");
		 Iterator iter= usersSet.iterator();
		 receivingTeamList.clear();
		 while(iter.hasNext())
		 {
			 User user=(User)iter.next();  
			 SelectItem item=null; 
			 item =new SelectItem(user.getLoginId(),user.getFullName());
			 receivingTeamList.add(item);


		 }

		 logger.logInfo(msg+ " completed");
		 return receivingTeamList;
	 }

	 public void setReceivingTeamList(List<SelectItem> receivingTeamList) {
		 this.receivingTeamList = receivingTeamList;
	 }

	 public List getSelectedReceivingTeamList() {
		 return selectedReceivingTeamList;
	 }

	 public void setSelectedReceivingTeamList(
			 List selectedReceivingTeamList) {
		 this.selectedReceivingTeamList = selectedReceivingTeamList;
	 }

	 public List<SelectItem> getUsersReceivingList() {

		 String msg = "getUserReceivingList";
		 logger.logInfo(msg + "started");
		 if(sessionMap.containsKey("SESSION_USER_RECEIVING"))
		 {
			 List<SelectItem> itemsList =(List<SelectItem>) sessionMap.get("SESSION_USER_RECEIVING");
			 usersReceivingList=			itemsList;
		 }


		 logger.logInfo(msg+"finished");
		 return usersReceivingList;
	 }

	 public void setUsersReceivingList(List<SelectItem> usersReceivingList) {
		 this.usersReceivingList = usersReceivingList;
	 }

	/* public String tabReceivingTeam_Click()
	 {

		 String methodName="tabReceivingTeam_Click";
		 logger.logInfo("|"+"Start..");
		 String eventOutcome="";
		 		Set<User> usersSet;
		//if(!sessionMap.containsKey("SESSION_INSPECTION_TEAM_USERS"))
		{
			UserGroup userGroup  = (UserGroup) SecurityManager.getGroup("AMAF Inspection Team");
			usersSet=     userGroup.getUsers();

			Iterator iter= usersSet.iterator();
			HashMap hMap =new HashMap();
			while(iter.hasNext())
			{
				User user=(User)iter.next();  

				hMap.put(user.getLoginId(), user);

			}
			sessionMap.put("SESSION_HASHMAP_RECEIVING_TEAM_USERS",hMap);
			sessionMap.put("SESSION_RECEIVING_TEAM_USERS",usersSet);

		}

		 //populateInspectionsList(usersSet);
		 loadPropertyReceivingTeamDataList();
		 logger.logInfo("|"+"Finish..");
		 return eventOutcome;

	 }

*/

	 public List<SelectItem> getEvaluationTeamList() 
	 {
		 String msg = "getEvaluationTeamList";
		 logger.logInfo(msg+" started");
		 Set<User> usersSet=new HashSet<User>(0);
		 if(sessionMap.containsKey("SESSION_EVALUATION_TEAM_USERS"))
			 usersSet=(Set<User>)sessionMap.get("SESSION_EVALUATION_TEAM_USERS");
		 Iterator iter= usersSet.iterator();
		 evaluationTeamList.clear();
		 while(iter.hasNext())
		 {
			 User user=(User)iter.next();  
			 SelectItem item=null; 
			 item =new SelectItem(user.getLoginId(),user.getFullName());
			 evaluationTeamList.add(item);


		 }

		 logger.logInfo(msg+ "completed");
		 return evaluationTeamList;
	 }

	 public void setEvaluationTeamList(List<SelectItem> evaluationTeamList) {
		 this.evaluationTeamList = evaluationTeamList;
	 }

	 public List getSelectedEvaluationTeamList() {
		 return selectedEvaluationTeamList;
	 }

	 public void setSelectedEvaluationTeamList(
			 List selectedEvaluationTeamList) {
		 this.selectedEvaluationTeamList = selectedEvaluationTeamList;
	 }

	 public List<SelectItem> getUsersEvaluationList() {


		 if(sessionMap.containsKey("SESSION_USER_EVALUATION"))
		 {
			 List<SelectItem> itemsList =(List<SelectItem>) sessionMap.get("SESSION_USER_EVALUATION");
			 usersEvaluationList=			itemsList;
		 }



		 return usersEvaluationList;
	 }

	 public void setUsersEvaluationList(List<SelectItem> usersEvaluationList) {
		 this.usersEvaluationList = usersEvaluationList;
	 }


	/* public String tabEvaluationTeam_Click()
	 {
		 String methodName="tabEvaluationTeam_Click";
		 logger.logInfo("|"+"Start..");
		 String eventOutcome="";
		 Set<User> usersSet;
		//if(!sessionMap.containsKey("SESSION_INSPECTION_TEAM_USERS"))
		{
			UserGroup userGroup  = (UserGroup) SecurityManager.getGroup("AMAF Inspection Team");
			usersSet=     userGroup.getUsers();

			Iterator iter= usersSet.iterator();
			HashMap hMap =new HashMap();
			while(iter.hasNext())
			{
				User user=(User)iter.next();  

				hMap.put(user.getLoginId(), user);

			}
			sessionMap.put("SESSION_HASHMAP_EVALUATION_TEAM_USERS",hMap);
			sessionMap.put("SESSION_EVALUATION_TEAM_USERS",usersSet);

		}

		  		
		 //populateInspectionsList(usersSet);
		 loadPropertyEvaluationTeamDataList();
		 logger.logInfo("|"+"Finish..");
		 return eventOutcome;

	 }*/


	 private void normalMode()throws Exception{
		 viewRootMap.put("canAddAttachment",false);
		 viewRootMap.put("canAddNote",false);

		 if(showTabsHidden.getValue()==null
				 ||!Boolean.valueOf(showTabsHidden.getValue().toString())){
			 showTabs=false;
			 showDocNComTabs = false;
		 }
		 else{
			 viewRootMap.put("canAddAttachment",true);

			 viewRootMap.put("canAddNote", true);
			 showTabs=true;
			 showDocNComTabs = true;
		 }
		 if(showTabsHidden.getValue()!=null&&showTabsHidden.getValue().equals("inventory")){
			 viewRootMap.put("canAddAttachment",true);

			 viewRootMap.put("canAddNote", true);
			 showDocNComTabs=true;
			 showTabs=false;
		 }


		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(false);
		 endowedName.setReadonly(false);
		 propertyType.setReadonly(false);
		 propertyCategory.setReadonly(false);
		 commercialName.setReadonly(false);
		 //propertyNameEn.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 showRentTabs = false;
		 landNumber.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 departmentCode.setReadonly(false);
		 municipalityPlanNumber.setReadonly(false);
		 projectNumber.setReadonly(false);
		 landArea.setReadonly(false);
		 builtInArea.setReadonly(false);
		 numberOfLifts.setReadonly(false);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(true);
		 saveAfterReceivedButton.setRendered(false);

		 proeprtyInvestmentPurpose.setReadonly(false);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);

		 addressLineOne.setReadonly(false);
		 addressLineTwo.setReadonly(true);
		 homePhone.setReadonly(true);
		 postalCode.setReadonly(true);
		 streetInformation.setReadonly(false);
//		 need change
		 /*city.setReadonly(true);
		emirate.setReadonly(true);
		countryMenu.setReadonly(true);*/
		 locationSaveButton.setRendered(false);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(true);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);

		 showRentTabs = false;

		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(true);

		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);

		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);





		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);


		 saveReceivingTeamButton.setRendered(false);
		 saveEvaluationTeamButton.setRendered(false);

		 addFloorButton.setRendered(false);
		 viewRootMap.put("DELETE_ACTION",false);

		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(false);
		 addReceiveingTeam.setRendered(false);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);

		 onRentValue=false;
		 btnUpdateUnitAtApproval.setRendered(false);
	 }

	 private void draftMode(){
		 String msg="draftMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);
		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(false);
		 endowedName.setReadonly(false);
		 propertyType.setReadonly(false);
		 propertyCategory.setReadonly(false);
		 commercialName.setReadonly(false);
		 //propertyNameEn.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 showRentTabs = false;
		 landNumber.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 departmentCode.setReadonly(false);
		 municipalityPlanNumber.setReadonly(false);
		 projectNumber.setReadonly(false);
		 landArea.setReadonly(false);
		 builtInArea.setReadonly(false);
		 numberOfLifts.setReadonly(false);
		 generateFloorButton.setRendered(true);
		 generateUnitButton.setRendered(true);
		 submitButton.setRendered(true);
		 saveButton.setRendered(true);
		 saveAfterReceivedButton.setRendered(false);
		 proeprtyInvestmentPurpose.setReadonly(false);
		 saveCommentButton.setRendered(true);
		 saveAttachmentsButton.setRendered(true);
		 addressLineOne.setReadonly(false);
		 addressLineTwo.setReadonly(false);
		 homePhone.setReadonly(false);
		 postalCode.setReadonly(false);
		 streetInformation.setReadonly(false);
		 locationSaveButton.setRendered(true);
		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(false);
		 ownerSaveButton.setRendered(true);
		 ownerSearchButton.setRendered(true);
		 representativeSearchButton.setRendered(true);
		 showRentTabs = false;
		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);
		 charges.setReadonly(false);
		 searchFacilityButton.setRendered(true);
		 saveFacilityButton.setRendered(true);
		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);
		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 saveReceivingTeamButton.setRendered(true);
		 saveEvaluationTeamButton.setRendered(true);
		 addFloorButton.setRendered(true);
		 viewRootMap.put("DELETE_ACTION",true);
		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(false);
		 addReceiveingTeam.setRendered(false);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);
		 onRentValue=false;
		 rentValueForUnits.setValue(0L);
		 rentValueForUnits.setReadonly(true);
		 logger.logInfo(msg+"completed");
	 }


	 private void savedMode(){
		 String msg="savedMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);

		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(false);
		 endowedName.setReadonly(false);
		 propertyType.setReadonly(false);
		 propertyCategory.setReadonly(false);
		 commercialName.setReadonly(false);
		 //propertyNameEn.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 showRentTabs = false;
		 landNumber.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 departmentCode.setReadonly(false);
		 municipalityPlanNumber.setReadonly(false);
		 projectNumber.setReadonly(false);
		 landArea.setReadonly(false);
		 builtInArea.setReadonly(false);
		 numberOfLifts.setReadonly(false);
		 generateFloorButton.setRendered(true);
		 generateUnitButton.setRendered(true);
		 submitButton.setRendered(true);
		 saveButton.setRendered(true);
		 saveAfterReceivedButton.setRendered(false);

		 proeprtyInvestmentPurpose.setReadonly(false);
		 saveCommentButton.setRendered(true);
		 saveAttachmentsButton.setRendered(true);

		 addressLineOne.setReadonly(false);
		 addressLineTwo.setReadonly(false);
		 homePhone.setReadonly(false);
		 postalCode.setReadonly(false);
		 streetInformation.setReadonly(false);
//		 need change
		 /*city.setReadonly(false);
		emirate.setReadonly(false);
		countryMenu.setReadonly(false);*/
		 locationSaveButton.setRendered(true);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(false);
		 ownerSaveButton.setRendered(true);
		 ownerSearchButton.setRendered(true);
		 representativeSearchButton.setRendered(true);

		 showRentTabs = false;

		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);





		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);

//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(false);

		 searchFacilityButton.setRendered(true);
		 saveFacilityButton.setRendered(true);




		 saveReceivingTeamButton.setRendered(true);
		 saveEvaluationTeamButton.setRendered(true);
		 addFloorButton.setRendered(true);

		 addUnitButton.setRendered(true);
		 viewRootMap.put("DELETE_ACTION",true);
		 addEvaluationTeam.setRendered(true);
		 addReceiveingTeam.setRendered(true);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);
		 onRentValue=false;
		 rentValueForUnits.setValue(0L);
		 rentValueForUnits.setReadonly(true);
		 logger.logInfo(msg+"compelted");

	 }
	 private void savedModeInventory(){
		 String msg="savedModeInventory";
		 logger.logInfo(msg+"started");
		 showTabs=false;
		 viewRootMap.put("canAddAttachment",true);

		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(false);
		 endowedName.setReadonly(false);
		 propertyType.setReadonly(false);
		 propertyCategory.setReadonly(false);
		 commercialName.setReadonly(false);
		 //propertyNameEn.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 showRentTabs = false;
		 landNumber.setReadonly(false);
		 propertyUsage.setReadonly(false);
		 departmentCode.setReadonly(false);
		 municipalityPlanNumber.setReadonly(false);
		 projectNumber.setReadonly(false);
		 landArea.setReadonly(false);
		 builtInArea.setReadonly(false);
		 numberOfLifts.setReadonly(false);


		 submitButton.setRendered(false);
		 saveButton.setRendered(true);
		 saveAfterReceivedButton.setRendered(false);

		 proeprtyInvestmentPurpose.setReadonly(false);
		 saveCommentButton.setRendered(true);
		 saveAttachmentsButton.setRendered(true);

		 addressLineOne.setReadonly(false);
		 addressLineTwo.setReadonly(false);
		 homePhone.setReadonly(false);
		 postalCode.setReadonly(false);
		 streetInformation.setReadonly(false);
//		 need change
		 /*city.setReadonly(false);
		emirate.setReadonly(false);
		countryMenu.setReadonly(false);*/
		 locationSaveButton.setRendered(true);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(false);
		 ownerSaveButton.setRendered(true);
		 ownerSearchButton.setRendered(true);
		 representativeSearchButton.setRendered(true);

		 showRentTabs = false;

		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);





		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);

//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(false);

		 searchFacilityButton.setRendered(true);
		 saveFacilityButton.setRendered(true);
		 saveReceivingTeamButton.setRendered(true);
		 saveEvaluationTeamButton.setRendered(true);
		 addFloorButton.setRendered(true);
		 addUnitButton.setRendered(true);
		 viewRootMap.put("DELETE_ACTION",true);
		 addEvaluationTeam.setRendered(true);
		 addReceiveingTeam.setRendered(true);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);
		 onRentValue=false;
		 logger.logInfo(msg+"completed");

	 }
	 private void actionPerformedMode(){
		 String msg="actionPerformedMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);

		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(true);
		 endowedName.setReadonly(true);
		 propertyType.setReadonly(true);
		 propertyCategory.setReadonly(true);
		 commercialName.setReadonly(true);
		 //propertyNameEn.setReadonly(true);
		 projectImage.setRendered(false);
		 propertyUsage.setReadonly(true);
		 showRentTabs = false;
		 landNumber.setReadonly(true);
		 propertyUsage.setReadonly(true);
		 departmentCode.setReadonly(true);
		 municipalityPlanNumber.setReadonly(true);
		 projectNumber.setReadonly(true);
		 landArea.setReadonly(true);
		 builtInArea.setReadonly(true);
		 numberOfLifts.setReadonly(true);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(false);
		 saveAfterReceivedButton.setRendered(false);
		 proeprtyInvestmentPurpose.setReadonly(false);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);

		 addressLineOne.setReadonly(false);
		 addressLineTwo.setReadonly(false);
		 homePhone.setReadonly(false);
		 postalCode.setReadonly(false);
		 streetInformation.setReadonly(false);

		 locationSaveButton.setRendered(false);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(false);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);

		 showRentTabs = false;

		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);


		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);

//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(false);

		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);




		 saveReceivingTeamButton.setRendered(false);
		 saveEvaluationTeamButton.setRendered(false);
		 addFloorButton.setRendered(false);

		 viewRootMap.put("DELETE_ACTION",false);
		 addUnitButton.setRendered(false);
		 addEvaluationTeam.setRendered(false);
		 addReceiveingTeam.setRendered(false);

		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);

		 onRentValue=false;
	 }

	 private void inventoryMode(){
		 String msg="inventoryMode";
		 logger.logInfo(msg+"started");
		 showTabs = false;
		 viewRootMap.put("new",true);
		 viewRootMap.put("canAddAttachment",false);
		 viewRootMap.put("canAddNote",false);
		 showDocNComTabs=true;
		 viewRootMap.put("PROPERTY_INVENTORY_MODE", true);
		 onRentValue=false;
		 logger.logInfo(msg+" completed");

	 }
	 private void inventoryModeSave(){
		 String msg="inventoryModeSave";
		 logger.logInfo(msg+"started");
		 showTabs = false;
		 viewRootMap.put("new",true);
		 viewRootMap.put("canAddAttachment",true);

		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 viewRootMap.put("PROPERTY_INVENTORY_MODE", true);
		 onRentValue=false;
		 logger.logInfo(msg+"completed");
	 }
	 private void evaluationMode(){
		 String msg="evaluationMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);
		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(true);
		 endowedName.setReadonly(true);
		 propertyType.setReadonly(true);
		 propertyCategory.setReadonly(true);
		 commercialName.setReadonly(true);
		 //propertyNameEn.setReadonly(true);
		 projectImage.setRendered(false);
		 propertyUsage.setReadonly(true);
		 showRentTabs = true;
		 landNumber.setReadonly(true);
		 propertyUsage.setReadonly(true);
		 departmentCode.setReadonly(true);
		 municipalityPlanNumber.setReadonly(true);
		 projectNumber.setReadonly(true);
		 landArea.setReadonly(true);
		 builtInArea.setReadonly(true);
		 numberOfLifts.setReadonly(true);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(false);
		 saveAfterReceivedButton.setRendered(false);

		 proeprtyInvestmentPurpose.setReadonly(true);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);

		 addressLineOne.setReadonly(true);
		 addressLineTwo.setReadonly(true);
		 homePhone.setReadonly(true);
		 postalCode.setReadonly(true);
		 streetInformation.setReadonly(true);
//		 need change
		 /*city.setReadonly(true);
		emirate.setReadonly(true);
		countryMenu.setReadonly(true);*/
		 locationSaveButton.setRendered(false);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(true);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);

		 showRentTabs = true;

		 rentValue.setReadonly(false);
		 propertyRentSaveButton.setRendered(true);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(false);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(true);





		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(false);



//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(true);

		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);



		 saveReceivingTeamButton.setRendered(true);
		 saveEvaluationTeamButton.setRendered(true);
		 addFloorButton.setRendered(true);
		 viewRootMap.put("DELETE_ACTION",false);
		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(true);
		 addReceiveingTeam.setRendered(true);
		 submitAfterEvaluation.setRendered(true);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);
		 onRentValue=true;
		 imgSearchAsset.setRendered(false);
		 imgSearchEndowment.setRendered(false);
		 ownershipType.setReadonly(true);
		 logger.logInfo(msg+"completed");
	 }

	 private void approvalMode(){
		 String msg="approvalMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);
		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(true);
		 endowedName.setReadonly(true);
		 propertyType.setReadonly(true);
		 propertyCategory.setReadonly(true);
		 commercialName.setReadonly(true);
		 //propertyNameEn.setReadonly(true);
		 projectImage.setRendered(false);
		 propertyUsage.setReadonly(true);
		 showRentTabs = true;
		 landNumber.setReadonly(true);
		 propertyUsage.setReadonly(true);
		 departmentCode.setReadonly(true);
		 municipalityPlanNumber.setReadonly(true);
		 projectNumber.setReadonly(true);
		 landArea.setReadonly(true);
		 builtInArea.setReadonly(true);
		 numberOfLifts.setReadonly(true);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(false);
		 saveAfterReceivedButton.setRendered(false);
		 proeprtyInvestmentPurpose.setReadonly(true);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);

		 addressLineOne.setReadonly(true);
		 addressLineTwo.setReadonly(true);
		 homePhone.setReadonly(true);
		 postalCode.setReadonly(true);
		 streetInformation.setReadonly(true);
//		 need change
		 /*city.setReadonly(true);
		emirate.setReadonly(true);
		countryMenu.setReadonly(true);*/
		 locationSaveButton.setRendered(false);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(true);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);

		 showRentTabs = true;


//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(true);

		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);

		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);





		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);

//		 facilityReferenceNumber.setReadonly(true);

		 saveReceivingTeamButton.setRendered(false);
		 saveEvaluationTeamButton.setRendered(false);
		 addFloorButton.setRendered(false);
		 viewRootMap.put("DELETE_ACTION",false);
		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(true);
		 addReceiveingTeam.setRendered(true);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(true);
		 rejectEvaluation.setRendered(true);
		 approveRentCommittee.setRendered(false);
		 onRentValue=true;
		 imgSearchAsset.setRendered(false);
		 imgSearchEndowment.setRendered(false);
		 ownershipType.setReadonly(true);
		 logger.logInfo(msg+"completed");
	 }
	 private void approvalReviewMode()
	 {
		 showTabs=true;
		 viewRootMap.put("canAddAttachment",true);
		 viewRootMap.put("canAddNote", true);
		 showDocNComTabs=true;
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(true);
		 endowedName.setReadonly(true);
		 propertyType.setReadonly(true);
		 propertyCategory.setReadonly(true);
		 commercialName.setReadonly(true);
		 projectImage.setRendered(false);
		 propertyUsage.setReadonly(true);
		 showRentTabs = true;
		 landNumber.setReadonly(true);
		 propertyUsage.setReadonly(true);
		 departmentCode.setReadonly(true);
		 municipalityPlanNumber.setReadonly(true);
		 projectNumber.setReadonly(true);
		 landArea.setReadonly(true);
		 builtInArea.setReadonly(true);
		 numberOfLifts.setReadonly(true);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(false);
		 saveAfterReceivedButton.setRendered(false);
		 proeprtyInvestmentPurpose.setReadonly(true);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);
		 addressLineOne.setReadonly(true);
		 addressLineTwo.setReadonly(true);
		 homePhone.setReadonly(true);
		 postalCode.setReadonly(true);
		 streetInformation.setReadonly(true);
		 locationSaveButton.setRendered(false);
		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(true);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);
		 showRentTabs = true;
		 charges.setReadonly(true);
		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);
		 rentValue.setReadonly(true);
		 propertyRentSaveButton.setRendered(false);
		 propertyTabCategory.setRendered(false);
		 propertyDepositAmount.setRendered(false);
		 propertyRentProcess.setRendered(false);
		 propertyGracePeriod.setRendered(false);
		 propertyGracePeriodType.setRendered(false);
		 propertyAuctionOpeningPrice.setRendered(false);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(false);
		 propertyCategorySelectOneMenu.setRendered(false);
		 propertyRentProcessSelectOneMenu.setRendered(false);
		 propertyDepositAmountText.setRendered(false);
		 propertyGracePeriodText.setRendered(false);
		 propertyAuctionOpeningPriceText.setRendered(false);


		 floorNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 floorCategory.setRendered(false);
		 floorDepositAmount.setRendered(false);
		 floorRentProcess.setRendered(false);
		 floorGracePeriod.setRendered(false);
		 floorAuctionOpeningPrice.setRendered(false);
		 floorGracePeriodType.setRendered(false);
		 floorCategorySelectOneMenu.setRendered(false);
		 floorRentProcessSelectOneMenu.setRendered(false);
		 floorDepositAmountText.setRendered(false);
		 floorRentSaveButton.setRendered(false);
		 floorGracePeriodText.setRendered(false);
		 floorGracePeriodTypeSelectOneMenu.setRendered(false);
		 floorAuctionOpeningPriceText.setRendered(false);
		 unitNumber.setReadonly(true);
		 rentValueForFloors.setReadonly(true);
		 saveReceivingTeamButton.setRendered(true);
		 saveEvaluationTeamButton.setRendered(true);
		 addFloorButton.setRendered(true);
		 viewRootMap.put("DELETE_ACTION",false);
		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(true);
		 addReceiveingTeam.setRendered(true);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(false);
		 reviewApproveEvaluation.setRendered(true);
		 onRentValue=false;
		 imgSearchAsset.setRendered(false);
		 imgSearchEndowment.setRendered(false);
		 ownershipType.setReadonly(true);

	 }


	 private void rentCommitteeMode(){
		 onRentValue=true;
		 String msg="rentCommitteeMode";
		 logger.logInfo(msg+"started");
		 showTabs=true;
		 showDocNComTabs=true;
		 viewRootMap.put("canAddAttachment",true);
		 viewRootMap.put("canAddNote", true);
		 propertyReferenceNumber.setReadonly(true);
		 financialAccountNumber.setReadonly(true);
		 endowedName.setReadonly(true);
		 propertyType.setReadonly(true);
		 propertyCategory.setReadonly(true);
		 commercialName.setReadonly(true);
		 //propertyNameEn.setReadonly(true);
		 projectImage.setRendered(false);
		 propertyUsage.setReadonly(true);
		 showRentTabs = true;
		 landNumber.setReadonly(true);
		 propertyUsage.setReadonly(true);
		 departmentCode.setReadonly(true);
		 municipalityPlanNumber.setReadonly(true);
		 projectNumber.setReadonly(true);
		 landArea.setReadonly(true);
		 builtInArea.setReadonly(true);
		 numberOfLifts.setReadonly(true);
		 generateFloorButton.setRendered(false);
		 generateUnitButton.setRendered(false);
		 submitButton.setRendered(false);
		 saveButton.setRendered(false);
		 saveAfterReceivedButton.setRendered(false);

		 proeprtyInvestmentPurpose.setReadonly(true);
		 saveCommentButton.setRendered(false);
		 saveAttachmentsButton.setRendered(false);

		 addressLineOne.setReadonly(true);
		 addressLineTwo.setReadonly(true);
		 homePhone.setReadonly(true);
		 postalCode.setReadonly(true);
		 streetInformation.setReadonly(true);
//		 need change
		 /*city.setReadonly(true);
		emirate.setReadonly(true);
		countryMenu.setReadonly(true);*/
		 locationSaveButton.setRendered(false);

		 ownerReferenceNo.setReadonly(true);
		 ownerRepresentative.setReadonly(true);
		 ownerPercentage.setReadonly(true);
		 ownerSaveButton.setRendered(false);
		 ownerSearchButton.setRendered(false);
		 representativeSearchButton.setRendered(false);

		 showRentTabs = true;



//		 facilityReferenceNumber.setReadonly(true);
		 charges.setReadonly(true);

		 searchFacilityButton.setRendered(false);
		 saveFacilityButton.setRendered(false);

		 rentValue.setReadonly(false);
		 propertyRentSaveButton.setRendered(true);
		 propertyTabCategory.setRendered(true);
		 propertyDepositAmount.setRendered(true);
		 propertyRentProcess.setRendered(true);
		 propertyGracePeriod.setRendered(true);
		 propertyGracePeriodType.setRendered(true);
		 propertyAuctionOpeningPrice.setRendered(true);
		 propertyGracePeriodTypeSelectOneMenu.setRendered(true);
		 propertyCategorySelectOneMenu.setRendered(true);
		 propertyRentProcessSelectOneMenu.setRendered(true);
		 propertyDepositAmountText.setRendered(true);
		 propertyGracePeriodText.setRendered(true);
		 propertyAuctionOpeningPriceText.setRendered(true);


		 floorNumber.setReadonly(false);
		 rentValueForFloors.setReadonly(false);
		 floorCategory.setRendered(true);
		 floorDepositAmount.setRendered(true);
		 floorRentProcess.setRendered(true);
		 floorGracePeriod.setRendered(true);
		 floorAuctionOpeningPrice.setRendered(true);
		 floorGracePeriodType.setRendered(true);
		 floorCategorySelectOneMenu.setRendered(true);
		 floorRentProcessSelectOneMenu.setRendered(true);
		 floorDepositAmountText.setRendered(true);
		 floorRentSaveButton.setRendered(true);
		 floorGracePeriodText.setRendered(true);
		 floorGracePeriodTypeSelectOneMenu.setRendered(true);
		 floorAuctionOpeningPriceText.setRendered(true);





		 unitNumber.setReadonly(false);
		 rentValueForFloors.setReadonly(false);


		 saveReceivingTeamButton.setRendered(false);
		 saveEvaluationTeamButton.setRendered(false);
		 addFloorButton.setRendered(false);
		 viewRootMap.put("DELETE_ACTION",false);
		 addUnitButton.setRendered(true);
		 addEvaluationTeam.setRendered(false);
		 addReceiveingTeam.setRendered(false);
		 submitAfterEvaluation.setRendered(false);
		 approveEvaluation.setRendered(false);
		 rejectEvaluation.setRendered(false);
		 reviewApproveEvaluation.setRendered(false);
		 approveRentCommittee.setRendered(true);
		 imgSearchAsset.setRendered(false);
		 imgSearchEndowment.setRendered(false);
		 ownershipType.setReadonly(true);
		 logger.logInfo(msg+" completed");
	 }



	 public ContactInfoView getLocation() {
		 return location;
	 }
	 public void setLocation(ContactInfoView location) {
		 this.location = location;
	 }
	 public HtmlDataTable getFacilityGrid() {
		 return facilityGrid;
	 }
	 public void setFacilityGrid(HtmlDataTable facilityGrid) {
		 this.facilityGrid = facilityGrid;
	 }

	 public HtmlCommandButton getSaveFacilityButton() {
		 return saveFacilityButton;
	 }
	 public void setSaveFacilityButton(HtmlCommandButton saveFacilityButton) {
		 this.saveFacilityButton = saveFacilityButton;
	 }
	 public HtmlGraphicImage getSearchFacilityButton() {
		 return searchFacilityButton;
	 }
	 public void setSearchFacilityButton(HtmlGraphicImage searchFacilityButton) {
		 this.searchFacilityButton = searchFacilityButton;
	 }
	 public HtmlInputText getCharges() {
		 return charges;
	 }
	 public void setCharges(HtmlInputText charges) {
		 this.charges = charges;
	 }
	 public void showFacilitiesSearchPopup(ActionEvent event){		

		 String msg="showFacilitiesSearchPopup";
		 logger.logInfo(msg+"started");
		 setRequestParam(WebConstants.SEARCH_FACILITY, "true");


		 final String viewId = "/ReceiveProperty.jsf"; 

		 FacesContext facesContext = FacesContext.getCurrentInstance();

		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		 String javaScriptText = "javascript:FacilitiesPopup();";

		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(msg+"completed");
	 }
	 public void showDuplicateFloorsSearchPopup(){		
		 String msg="showDuplicateFloorsSearchPopup";
		 logger.logInfo(msg+"started");

		 ReceivePropertyFloorView floorView = (ReceivePropertyFloorView)floorTable.getRowData();
		 sessionMap.put("FLOOR_VIEW_AS_PARAM",floorView);



		 final String viewId = "/ReceiveProperty.jsf"; 

		 FacesContext facesContext = FacesContext.getCurrentInstance();

		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		 String javaScriptText = "javascript:DuplicateFloorPopup();";

		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(msg+"completed");
	 }

	 public void showDuplicateUnitsSearchPopup(){		
		 String msg = "showDuplicateUnitsSearchPopup";

		 logger.logInfo(msg+"started");

		 ReceivePropertyUnitView unitView = (ReceivePropertyUnitView)unitTable.getRowData();
		 sessionMap.put("UNIT_VIEW_AS_PARAM",unitView);
		 sessionMap.put("PROPERTY_ID_DUPLICATE_UNITS",propertyView.getPropertyId());
		 viewRootMap.put(WebConstants.Property.PROPERTY_VIEW,propertyView);
		 final String viewId = "/ReceiveProperty.jsf"; 
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		 String javaScriptText = "javascript:DuplicateUnitPopup();";
		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(msg+"|completed");
	 }

	 public void showRentValuePopup(){		
		 String msg="showRentValuePopup";
		 logger.logInfo(msg+"started");

		 ReceivePropertyUnitView unitView = (ReceivePropertyUnitView)unitTable.getRowData();
		 UserTask userTask ;
		 sessionMap.put("UNIT_VIEW_AS_PARAM",unitView);
		 if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
		 {
			 userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			 Map taskAttributes =  userTask.getTaskAttributes();
			 long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.PROPERTY_ID));
			 sessionMap.put("PROPERTY_ID_DUPLICATE_UNITS",requestId);
		 }
		 if(propertyView!=null&&propertyView.getPropertyId()!=null){
			 sessionMap.put("PROPERTY_ID_DUPLICATE_UNITS",propertyView.getPropertyId());
		 }



		 final String viewId = "/ReceiveProperty.jsf"; 

		 FacesContext facesContext = FacesContext.getCurrentInstance();

		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();

		 String javaScriptText = "javascript:ApplyRentValuePopup();";

		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

		 logger.logInfo(msg+"completed");
	 }

	 public void showOwnersSearchPopup(ActionEvent event){		
		 String msg="showOwnersSearchMode";
		 logger.logInfo(msg+"started");

		 sessionMap.put(WebConstants.Person.SEARCH_OWNER,true);


		 final String viewId = "/ReceiveProperty.jsf"; 

		 FacesContext facesContext = FacesContext.getCurrentInstance();

		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		 String javaScriptText = "javascript:OwnerSearchPopup();";

		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

		 logger.logInfo(msg+"completed");
	 }	
	 public void showRepresenatativesSearchPopup(ActionEvent event){		
		 String msg="showRepresentatvieSearchPopup";
		 logger.logInfo(msg+"started");

		 sessionMap.put(WebConstants.Person.SEARCH_REPRESENTATIVE,true);

		 final String viewId = "/ReceiveProperty.jsf"; 

		 FacesContext facesContext = FacesContext.getCurrentInstance();

		 // This is the proper way to get the view's url
		 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		 String javaScriptText = "javascript:RepSearchPopup();";

		 // Add the Javascript to the rendered page's header for immediate execution
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(msg+"completed");
	 }	
	 public void showEvaluationSearchPopup() {
		String msg = "showEvaluationSearchPopup";
		logger.logInfo(msg + "started");
			sessionMap.put(WebConstants.Property.SEARCH_EVALUATION, true);

			final String viewId = "/ReceiveProperty.jsf";

			FacesContext facesContext = FacesContext.getCurrentInstance();

			// This is the proper way to get the view's url
			ViewHandler viewHandler = facesContext.getApplication()
					.getViewHandler();
			String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			String javaScriptText = "javascript:SearchMemberPopup();";

			// Add the Javascript to the rendered page's header for immediate
			// execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);

		logger.logInfo(msg + " completed");
	}	
	 public void showReceivingSearchPopup(){		
		 String msg="showReceiveingSearchPopup";
		 logger.logInfo(msg+"started");
			 sessionMap.put(WebConstants.Property.SEARCH_RECEIVING,true);

			 final String viewId = "/ReceiveProperty.jsf"; 

			 FacesContext facesContext = FacesContext.getCurrentInstance();

			 // This is the proper way to get the view's url
			 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
			 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			 String javaScriptText = "javascript:SearchMemberPopup();";

			 // Add the Javascript to the rendered page's header for immediate execution
			 AddResource addResource = AddResourceFactory.getInstance(facesContext);
			 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 logger.logInfo(msg+" completed");
	 }



	 public void showGenerateFloorsPopup(ActionEvent event){		

		 String msg="showGenerateFloorsPopup";
		 logger.logInfo(msg+"started");
		 //if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
		 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
		 {
			 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);

			 sessionMap.put(WebConstants.GenerateFloors.PROPERTY_VIEW,propertyView);

			 final String viewId = "/ReceiveProperty.jsf"; 

			 FacesContext facesContext = FacesContext.getCurrentInstance();

			 // This is the proper way to get the view's url
			 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
			 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			 String javaScriptText = "javascript:GenerateFloorsPopup();";

			 // Add the Javascript to the rendered page's header for immediate execution
			 AddResource addResource = AddResourceFactory.getInstance(facesContext);
			 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		 }
		 logger.logInfo(msg+" completed");
	 }	

	 public void showGenerateUnitsPopup(ActionEvent event) {
		 String msg="showGenerateUnitsPopup";
		 logger.logInfo(msg+"started");
		 //if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
		 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
		 {
			 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);

			 sessionMap.put(WebConstants.GenerateUnits.PROPERTY_VIEW, propertyView);

			 final String viewId = "/ReceiveProperty.jsf";

			 FacesContext facesContext = FacesContext.getCurrentInstance();

			 // This is the proper way to get the view's url
			 ViewHandler viewHandler = facesContext.getApplication()
			 .getViewHandler();
			 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			 String javaScriptText = "javascript:GenerateUnitsPopup();";

			 // Add the Javascript to the rendered page's header for immediate
			 // execution
			 AddResource addResource = AddResourceFactory.getInstance(facesContext);
			 addResource.addInlineScriptAtPosition(facesContext,
					 AddResource.HEADER_BEGIN, javaScriptText);
		 }
		 logger.logInfo(msg+"completed");
	 }
	 public String cmdFacilityDelete_Click()

	 {
		 String msg="cmdFacilityDelete_click";
		 logger.logInfo(msg+"started");
		 //Write your server side code here

		 //fromDelete = true;
			 PropertyFacilityView facilityView=(PropertyFacilityView)facilityGrid.getRowData();

			 try

			 {
				 facilityView.setIsDeleted(1L);
				 propertyServiceAgent.deletePropertyFacility(facilityView);
				 loadPropertyFacilityDataList(true);
			 }catch (Exception e) {
				 logger.LogException(msg+"|Error Occured ",e);
				 System.out.println("Exception "+e);
			 }
		 return "delete";
	 }
	 public String cmdReceivingTeamMemberDelete_Click(){
		 String msg="cmdReceivingTeamMemberDelete_click";
		 logger.logInfo(msg+"started");
			 UserView receivingTeamView=(UserView)receivingTeamGrid.getRowData();
				List<UserView> tempReceivingDataList  = new ArrayList<UserView>();
				try
				{
					if(!(receivingTeamView.getTeamMemberId()!=null))
					{
						getReceivingDataList().remove(receivingTeamView);
						return "";
					}else{
					
						for(UserView receivingTeamDataListView : getReceivingDataList()){
							if(receivingTeamDataListView.getUserName().equals(receivingTeamView.getUserName()))
							{
								receivingTeamDataListView.setTeamMemberIsDeleted(1L); 
								tempReceivingDataList.add(receivingTeamDataListView);
							}else
							{
								tempReceivingDataList.add(receivingTeamDataListView);
							}
						}
					  }
					setReceivingDataList(tempReceivingDataList);
				}catch (Exception e) {
					System.out.println("Exception "+e);
				}
			return "";
	 }
	 public String cmdEvaluationTeamMemberDelete_Click(){
		 String msg="cmdEvaluationTeamMemberDelete_click";
		 logger.logInfo(msg+"started");

		 
		 UserView evaluationTeamView=(UserView)evaluationTeamGrid.getRowData();
			List<UserView> tempEvaluationDataList  = new ArrayList<UserView>();
			try
			{
				if(!(evaluationTeamView.getTeamMemberId()!=null))
				{
					getEvaluationDataList().remove(evaluationTeamView);
					return "";
				}else{
				
					for(UserView evaluationTeamDataListView : getEvaluationDataList()){
						if(evaluationTeamDataListView.getUserName().equals(evaluationTeamView.getUserName()))
						{
							evaluationTeamDataListView.setTeamMemberIsDeleted(1L); 
							tempEvaluationDataList.add(evaluationTeamDataListView);
						}else
						{
							tempEvaluationDataList.add(evaluationTeamDataListView);
						}
					}
				  }
				setEvaluationDataList(tempEvaluationDataList);
			}catch (Exception e) {
				System.out.println("Exception "+e);
			}
			return "";

	 }
	 public String cmdOwnerDelete_Click()

	 {


		 String msg="cmdOwnerDelete_click";
		 logger.logInfo(msg+"started");

		 PropertyOwnerView ownerView=(PropertyOwnerView)ownerDataGrid.getRowData();

		 try

		 {



			 ownerView.setIsDeleted(1L);

			 propertyServiceAgent.deletePropertyOwner(ownerView);



			 loadPropertyOwnersDataList(true);



		 }catch (Exception e) {


			 logger.LogException(msg+"crashed", e);
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		 logger.logInfo(msg+" finished");
		 return "delete";

	 }
	 public String saveFacility(){
		 if(validateFacilityTabl()){
			 String msg="saveFacility";
			 logger.logInfo(msg+"started");

			 //if(sessionMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
			 if(viewMap.get(WebConstants.Property.PROPERTY_VIEW)!=null)
			 {
				 //propertyView = (PropertyView)sessionMap.get(WebConstants.Property.PROPERTY_VIEW);
				 propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
				 FacilityView facilityView =null;
				 try {
					 facilityView = propertyServiceAgent.getFacilityById(Long.valueOf(facilityId));


					 facilityView.setFacilityTypeId(Long.valueOf(facilityTypeId));
				 } catch (PimsBusinessException e2) {
					 // TODO Auto-generated catch block
					 logger.LogException(msg+"crashed",e2);
					 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				 }


				 PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
				 PropertyFacilityView propertyFacilityView = new PropertyFacilityView();
				 Set<PropertyFacilityView> propertyFacilitiesViewSet = new HashSet<PropertyFacilityView>();

				 propertyFacilityView.setProperty(propertyView);
				 propertyFacilityView.setFacility(facilityView);

				 propertyFacilityView.setRent(Double.valueOf(charges.getValue().toString()));

				 propertyFacilityView.setFacilityName(facilityView.getFacilityName());
				 propertyFacilityView.setFacilityNameAr(facilityView.getFacilityNameAr());
				 propertyFacilityView.setFacilityTypeId(Long.valueOf(facilityTypeId));
				 propertyFacilityView.setDescription(facilityDescription.getValue().toString());

				 propertyFacilityView.setCreatedOn(new Date());
				 propertyFacilityView.setCreatedBy(getLoggedInUser());
				 propertyFacilityView.setUpdatedBy(getLoggedInUser());
				 propertyFacilityView.setUpdatedOn(new Date());
				 propertyFacilityView.setIsDeleted(new Long(0));
				 propertyFacilityView.setRecordStatus(new Long(1));

				 propertyFacilitiesViewSet.add(propertyFacilityView);


				 try {
					 propertyServiceAgent.persistPropertyFacilities(propertyFacilitiesViewSet);
					 sessionMap.put(WebConstants.FACILITY_VIEW,null);
					 charges.setValue("");
					 facilityId="-1";
					 facilityTypeId="-1";
					 facilityDescription.setValue("");
//					 facilityReferenceNumber.setValue("");
					 messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_FACILITY_SAVED_SUCCESSFULLY));
					 loadPropertyFacilityDataList(true);
				 } catch (PimsBusinessException e) {
					 // TODO Auto-generated catch blockl
					 logger.LogException(msg+"crashed", e);
					 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
					 e.printStackTrace();
				 }

			 }

			 logger.logInfo(msg+"completed");
		 }
		 return "";

	 }




	 public String getMessages() {
		 return CommonUtil.getErrorMessages(messages);
	 }


	 public void setMessages(List<String> messages) {
		 this.messages = messages;
	 }

	 public String getErrorMessages() {
		 return CommonUtil.getErrorMessages(errorMessages);
	 }


	 public void setErrorMessages(List<String> errorMessages) {
		 this.errorMessages = errorMessages;
	 }

	 public HtmlCommandButton getSaveEvaluationTeamButton() {
		 return saveEvaluationTeamButton;
	 }


	 public void setSaveEvaluationTeamButton(HtmlCommandButton saveEvaluationTeamButton) {
		 this.saveEvaluationTeamButton = saveEvaluationTeamButton;
	 }


	 public HtmlCommandButton getSaveReceivingTeamButton() {
		 return saveReceivingTeamButton;
	 }


	 public void setSaveReceivingTeamButton(HtmlCommandButton saveReceivingTeamButton) {
		 this.saveReceivingTeamButton = saveReceivingTeamButton;
	 }


	 public HtmlInputHidden getHiddenPropertyValue() {
		 return hiddenPropertyValue;
	 }


	 public void setHiddenPropertyValue(HtmlInputHidden hiddenPropertyValue) {
		 this.hiddenPropertyValue = hiddenPropertyValue;
	 }





	 public String getTeamId() {
		 return teamId;
	 }


	 public void setTeamId(String teamId) {
		 this.teamId = teamId;
	 }


	 public HtmlCommandButton getSaveAttachmentsButton() {
		 return saveAttachmentsButton;
	 }


	 public void setSaveAttachmentsButton(HtmlCommandButton saveAttachmentsButton) {
		 this.saveAttachmentsButton = saveAttachmentsButton;
	 }


	 public HtmlCommandButton getSaveCommentButton() {
		 return saveCommentButton;
	 }


	 public void setSaveCommentButton(HtmlCommandButton saveCommentButton) {
		 this.saveCommentButton = saveCommentButton;
	 }


	 public HtmlGraphicImage getOwnerSearchButton() {
		 return ownerSearchButton;
	 }


	 public void setOwnerSearchButton(HtmlGraphicImage ownerSearchButton) {
		 this.ownerSearchButton = ownerSearchButton;
	 }


	 public HtmlGraphicImage getRepresentativeSearchButton() {
		 return representativeSearchButton;
	 }


	 public void setRepresentativeSearchButton(
			 HtmlGraphicImage representativeSearchButton) {
		 this.representativeSearchButton = representativeSearchButton;
	 }









	 public void setOwnerDataList(List<PropertyOwnerView> ownerDataList) {
		 this.ownerDataList = ownerDataList;
	 }







	 public void setFacilityDataList(List<PropertyFacilityView> facilityDataList) {
		 this.facilityDataList = facilityDataList;
	 }



	 public boolean isShowRentTabs() {
		 return showRentTabs;
	 }



	 public void setShowRentTabs(boolean showRentTabs) {
		 this.showRentTabs = showRentTabs;
	 }



	 public HtmlCommandButton getApproveEvaluation() {
		 return approveEvaluation;
	 }



	 public void setApproveEvaluation(HtmlCommandButton approveEvaluation) {
		 this.approveEvaluation = approveEvaluation;
	 }



	 public HtmlCommandButton getApproveRentCommittee() {
		 return approveRentCommittee;
	 }



	 public void setApproveRentCommittee(HtmlCommandButton approveRentCommittee) {
		 this.approveRentCommittee = approveRentCommittee;
	 }



	 public HtmlCommandButton getRejectEvaluation() {
		 return rejectEvaluation;
	 }



	 public void setRejectEvaluation(HtmlCommandButton rejectEvaluation) {
		 this.rejectEvaluation = rejectEvaluation;
	 }



	 public HtmlCommandButton getSubmitAfterEvaluation() {
		 return submitAfterEvaluation;
	 }



	 public void setSubmitAfterEvaluation(HtmlCommandButton submitAfterEvaluation) {
		 this.submitAfterEvaluation = submitAfterEvaluation;
	 }





	 public void setFloorsDataList(List<ReceivePropertyFloorView> floorsDataList) {
		 this.floorsDataList = floorsDataList;
	 }






	 public void setUnitDataList(List<ReceivePropertyUnitView> unitDataList) {
		 this.unitDataList = unitDataList;
	 }



	 public HtmlOutputText getFloorAuctionOpeningPrice() {
		 return floorAuctionOpeningPrice;
	 }



	 public void setFloorAuctionOpeningPrice(HtmlOutputText floorAuctionOpeningPrice) {
		 this.floorAuctionOpeningPrice = floorAuctionOpeningPrice;
	 }



	 public HtmlInputText getFloorAuctionOpeningPriceText() {
		 return floorAuctionOpeningPriceText;
	 }



	 public void setFloorAuctionOpeningPriceText(
			 HtmlInputText floorAuctionOpeningPriceText) {
		 this.floorAuctionOpeningPriceText = floorAuctionOpeningPriceText;
	 }



	 public HtmlOutputText getFloorCategory() {
		 return floorCategory;
	 }



	 public void setFloorCategory(HtmlOutputText floorCategory) {
		 this.floorCategory = floorCategory;
	 }



	 public List<SelectItem> getFloorCategoryItems() {
		 return floorCategoryItems;
	 }



	 public void setFloorCategoryItems(List<SelectItem> floorCategoryItems) {
		 this.floorCategoryItems = floorCategoryItems;
	 }



	 public HtmlSelectOneMenu getFloorCategorySelectOneMenu() {
		 return floorCategorySelectOneMenu;
	 }



	 public void setFloorCategorySelectOneMenu(
			 HtmlSelectOneMenu floorCategorySelectOneMenu) {
		 this.floorCategorySelectOneMenu = floorCategorySelectOneMenu;
	 }



	 public HtmlOutputText getFloorDepositAmount() {
		 return floorDepositAmount;
	 }



	 public void setFloorDepositAmount(HtmlOutputText floorDepositAmount) {
		 this.floorDepositAmount = floorDepositAmount;
	 }



	 public HtmlInputText getFloorDepositAmountText() {
		 return floorDepositAmountText;
	 }



	 public void setFloorDepositAmountText(HtmlInputText floorDepositAmountText) {
		 this.floorDepositAmountText = floorDepositAmountText;
	 }



	 public HtmlOutputText getFloorGracePeriod() {
		 return floorGracePeriod;
	 }



	 public void setFloorGracePeriod(HtmlOutputText floorGracePeriod) {
		 this.floorGracePeriod = floorGracePeriod;
	 }



	 public List<SelectItem> getFloorGracePeriodItems() {
		 return floorGracePeriodItems;
	 }



	 public void setFloorGracePeriodItems(List<SelectItem> floorGracePeriodItems) {
		 this.floorGracePeriodItems = floorGracePeriodItems;
	 }



	 public HtmlInputText getFloorGracePeriodText() {
		 return floorGracePeriodText;
	 }



	 public void setFloorGracePeriodText(HtmlInputText floorGracePeriodText) {
		 this.floorGracePeriodText = floorGracePeriodText;
	 }



	 public HtmlOutputText getFloorGracePeriodType() {
		 return floorGracePeriodType;
	 }



	 public void setFloorGracePeriodType(HtmlOutputText floorGracePeriodType) {
		 this.floorGracePeriodType = floorGracePeriodType;
	 }



	 public HtmlSelectOneMenu getFloorGracePeriodTypeSelectOneMenu() {
		 return floorGracePeriodTypeSelectOneMenu;
	 }



	 public void setFloorGracePeriodTypeSelectOneMenu(
			 HtmlSelectOneMenu floorGracePeriodTypeSelectOneMenu) {
		 this.floorGracePeriodTypeSelectOneMenu = floorGracePeriodTypeSelectOneMenu;
	 }



	 public HtmlOutputText getFloorRentProcess() {
		 return floorRentProcess;
	 }



	 public void setFloorRentProcess(HtmlOutputText floorRentProcess) {
		 this.floorRentProcess = floorRentProcess;
	 }



	 public List<SelectItem> getFloorRentProcessItems() {
		 return floorRentProcessItems;
	 }



	 public void setFloorRentProcessItems(List<SelectItem> floorRentProcessItems) {
		 this.floorRentProcessItems = floorRentProcessItems;
	 }



	 public HtmlSelectOneMenu getFloorRentProcessSelectOneMenu() {
		 return floorRentProcessSelectOneMenu;
	 }



	 public void setFloorRentProcessSelectOneMenu(
			 HtmlSelectOneMenu floorRentProcessSelectOneMenu) {
		 this.floorRentProcessSelectOneMenu = floorRentProcessSelectOneMenu;
	 }






	 public HtmlOutputText getPropertyAuctionOpeningPrice() {
		 return propertyAuctionOpeningPrice;
	 }



	 public void setPropertyAuctionOpeningPrice(
			 HtmlOutputText propertyAuctionOpeningPrice) {
		 this.propertyAuctionOpeningPrice = propertyAuctionOpeningPrice;
	 }



	 public HtmlInputText getPropertyAuctionOpeningPriceText() {
		 return propertyAuctionOpeningPriceText;
	 }



	 public void setPropertyAuctionOpeningPriceText(
			 HtmlInputText propertyAuctionOpeningPriceText) {
		 this.propertyAuctionOpeningPriceText = propertyAuctionOpeningPriceText;
	 }



	 public List<SelectItem> getPropertyCategoryItems() {
		 return propertyCategoryItems;
	 }



	 public void setPropertyCategoryItems(List<SelectItem> propertyCategoryItems) {
		 this.propertyCategoryItems = propertyCategoryItems;
	 }



	 public HtmlSelectOneMenu getPropertyCategorySelectOneMenu() {
		 return propertyCategorySelectOneMenu;
	 }



	 public void setPropertyCategorySelectOneMenu(
			 HtmlSelectOneMenu propertyCategorySelectOneMenu) {
		 this.propertyCategorySelectOneMenu = propertyCategorySelectOneMenu;
	 }



	 public HtmlOutputText getPropertyDepositAmount() {
		 return propertyDepositAmount;
	 }



	 public void setPropertyDepositAmount(HtmlOutputText propertyDepositAmount) {
		 this.propertyDepositAmount = propertyDepositAmount;
	 }



	 public HtmlInputText getPropertyDepositAmountText() {
		 return propertyDepositAmountText;
	 }



	 public void setPropertyDepositAmountText(HtmlInputText propertyDepositAmountText) {
		 this.propertyDepositAmountText = propertyDepositAmountText;
	 }



	 public HtmlOutputText getPropertyGracePeriod() {
		 return propertyGracePeriod;
	 }



	 public void setPropertyGracePeriod(HtmlOutputText propertyGracePeriod) {
		 this.propertyGracePeriod = propertyGracePeriod;
	 }



	 public HtmlInputText getPropertyGracePeriodText() {
		 return propertyGracePeriodText;
	 }



	 public void setPropertyGracePeriodText(HtmlInputText propertyGracePeriodText) {
		 this.propertyGracePeriodText = propertyGracePeriodText;
	 }






	 public List<SelectItem> getPropertyGracePeriodTypeItems() {
		 return propertyGracePeriodTypeItems;
	 }



	 public void setPropertyGracePeriodTypeItems(
			 List<SelectItem> propertyGracePeriodTypeItems) {
		 this.propertyGracePeriodTypeItems = propertyGracePeriodTypeItems;
	 }



	 public HtmlOutputText getPropertyRentProcess() {
		 return propertyRentProcess;
	 }



	 public void setPropertyRentProcess(HtmlOutputText propertyRentProcess) {
		 this.propertyRentProcess = propertyRentProcess;
	 }



	 public List<SelectItem> getPropertyRentProcessItems() {
		 return propertyRentProcessItems;
	 }



	 public void setPropertyRentProcessItems(
			 List<SelectItem> propertyRentProcessItems) {
		 this.propertyRentProcessItems = propertyRentProcessItems;
	 }



	 public HtmlSelectOneMenu getPropertyRentProcessSelectOneMenu() {
		 return propertyRentProcessSelectOneMenu;
	 }



	 public void setPropertyRentProcessSelectOneMenu(
			 HtmlSelectOneMenu propertyRentProcessSelectOneMenu) {
		 this.propertyRentProcessSelectOneMenu = propertyRentProcessSelectOneMenu;
	 }



	 public HtmlOutputText getPropertyTabCategory() {
		 return propertyTabCategory;
	 }



	 public void setPropertyTabCategory(HtmlOutputText propertyTabCategory) {
		 this.propertyTabCategory = propertyTabCategory;
	 }




	 public HtmlSelectOneMenu getPropertyGracePeriodTypeSelectOneMenu() {
		 return propertyGracePeriodTypeSelectOneMenu;
	 }



	 public void setPropertyGracePeriodTypeSelectOneMenu(
			 HtmlSelectOneMenu propertyGracePeriodTypeSelectOneMenu) {
		 this.propertyGracePeriodTypeSelectOneMenu = propertyGracePeriodTypeSelectOneMenu;
	 }






	 public void setPropertyGracePeriodType(HtmlOutputText propertyGracePeriodType) {
		 this.propertyGracePeriodType = propertyGracePeriodType;
	 }









	 public HtmlOutputText getPropertyGracePeriodType() {
		 return propertyGracePeriodType;
	 }


	 public HtmlInputHidden getFloorHiddenId() {
		 return floorHiddenId;
	 }
	 public void setFloorHiddenId(HtmlInputHidden floorHiddenId) {
		 this.floorHiddenId = floorHiddenId;
	 }
	 public HtmlInputHidden getUnitHiddenId() {
		 return unitHiddenId;
	 }
	 public void setUnitHiddenId(HtmlInputHidden unitHiddenId) {
		 this.unitHiddenId = unitHiddenId;
	 }
	 public HtmlInputHidden getHiddenCounter() {
		 return hiddenCounter;
	 }
	 public void setHiddenCounter(HtmlInputHidden hiddenCounter) {
		 this.hiddenCounter = hiddenCounter;
	 }
	 public HtmlInputHidden getContactInfoId() {
		 return contactInfoId;
	 }
	 public void setContactInfoId(HtmlInputHidden contactInfoId) {
		 this.contactInfoId = contactInfoId;
	 }
	 public HtmlInputText getStatusText() {
		 return statusText;
	 }
	 public void setStatusText(HtmlInputText statusText) {
		 this.statusText = statusText;
	 }

	 public HtmlInputText getBuildingCoordinates() {
		 return buildingCoordinates;
	 }
	 public void setBuildingCoordinates(HtmlInputText buildingCoordinates) {
		 this.buildingCoordinates = buildingCoordinates;
	 }
	 public HtmlInputText getEndowedMinorNo() {
		 return endowedMinorNo;
	 }
	 public void setEndowedMinorNo(HtmlInputText endowedMinorNo) {
		 this.endowedMinorNo = endowedMinorNo;
	 }
	 public HtmlInputText getEwUtilityNo() {
		 return ewUtilityNo;
	 }
	 public void setEwUtilityNo(HtmlInputText ewUtilityNo) {
		 this.ewUtilityNo = ewUtilityNo;
	 }

	 public List<SelectItem> getOwnershipItems() {
		 return ownershipItems;
	 }
	 public void setOwnershipItems(List<SelectItem> ownershipItems) {
		 this.ownershipItems = ownershipItems;
	 }
	 public HtmlSelectOneMenu getOwnershipType() {
		 return ownershipType;
	 }
	 public void setOwnershipType(HtmlSelectOneMenu ownershipType) {
		 this.ownershipType = ownershipType;
	 }
	 public HtmlInputText getTotalAnnualRent() {
		 return totalAnnualRent;
	 }
	 public void setTotalAnnualRent(HtmlInputText totalAnnualRent) {
		 this.totalAnnualRent = totalAnnualRent;
	 }
	 public HtmlInputText getTotalMezzanineFloors() {
		 return totalMezzanineFloors;
	 }
	 public void setTotalMezzanineFloors(HtmlInputText totalMezzanineFloors) {
		 this.totalMezzanineFloors = totalMezzanineFloors;
	 }
	 public HtmlInputText getTotalNoOfFlats() {
		 return totalNoOfFlats;
	 }
	 public void setTotalNoOfFlats(HtmlInputText totalNoOfFlats) {
		 this.totalNoOfFlats = totalNoOfFlats;
	 }
	 public HtmlInputText getTotalNoOfParkingFloors() {
		 return totalNoOfParkingFloors;
	 }
	 public void setTotalNoOfParkingFloors(HtmlInputText totalNoOfParkingFloors) {
		 this.totalNoOfParkingFloors = totalNoOfParkingFloors;
	 }
	 public HtmlInputText getTotalNoOfUnits() 
	 {
		 if(viewRootMap.containsKey("UNITS_DATA_LIST"))
		 {
			 unitDataList = (ArrayList<ReceivePropertyUnitView>)viewRootMap.get("UNITS_DATA_LIST");
			 totalNoOfUnits.setValue(unitDataList.size());
		 }
		 return totalNoOfUnits;
	 }
	 public void setTotalNoOfUnits(HtmlInputText totalNoOfUnits) {
		 this.totalNoOfUnits = totalNoOfUnits;
	 }
	 public HtmlInputText getTotalNoOfFloors() {
		 return totalNoOfFloors;
	 }
	 public void setTotalNoOfFloors(HtmlInputText totalNoOfFloors) {
		 this.totalNoOfFloors = totalNoOfFloors;
	 }
	 public String getCityId() {
		 if(viewRootMap.containsKey("cityId"))
			 cityId=viewRootMap.get("cityId").toString();
		 return cityId;
	 }
	 public void setCityId(String cityId) {
		 this.cityId = cityId;
		 if(this.cityId != null)
			 viewRootMap.put("cityId",this.cityId );
	 }

	 public String getCountryId() {
		 if(viewRootMap.containsKey("countryId"))
			 countryId=viewRootMap.get("countryId").toString();
		 return countryId;
	 }
	 public void setCountryId(String countryId) {
		 this.countryId = countryId;
		 if(this.countryId!= null)
			 viewRootMap.put("countryId",this.countryId);

	 }

	 public String getStateId() {
		 if(viewRootMap.containsKey("stateId"))
			 stateId=viewRootMap.get("stateId").toString();
		 return stateId;
	 }
	 public void setStateId(String stateId) {
		 this.stateId = stateId;
		 if(this.stateId!= null)
			 viewRootMap.put("stateId",this.stateId);
	 }

	 public String getCRCountryId() {
		 if(viewRootMap.containsKey("CRCountryId"))
			 CRCountryId=viewRootMap.get("CRCountryId").toString();
		 return CRCountryId;
	 }

	 public void setCRCountryId(String countryId) {
		 CRCountryId = countryId;
		 if(this.CRCountryId!= null)
			 viewRootMap.put("CRCountryId",this.CRCountryId);
	 }

	 public String getCRStateId() {
		 if(viewRootMap.containsKey("CRStateId"))
			 CRStateId=viewRootMap.get("CRStateId").toString();
		 return CRStateId;
	 }

	 public void setCRStateId(String stateId) {
		 CRStateId = stateId;
		 if(this.CRStateId!= null)
			 viewRootMap.put("CRStateId",this.CRStateId);
	 }

	 public String getCRCityId() {
		 if(viewRootMap.containsKey("CRCityId"))
			 CRCityId=viewRootMap.get("CRCityId").toString();
		 return CRCityId;
	 }

	 public void setCRCityId(String cityId) {
		 CRCityId = cityId;
		 if(this.CRCityId != null)
			 viewRootMap.put("CRCityId",this.CRCityId );
	 }
	 public List<SelectItem> getCRCityList() {
		 return CRCityList;
	 }
	 public void setCRCityList(List<SelectItem> cityList) {
		 CRCityList = cityList;
	 }
	 public List<SelectItem> getCRStateList() {
		 return CRStateList;
	 }
	 public void setCRStateList(List<SelectItem> stateList) {
		 CRStateList = stateList;
	 }
	 public String getSelectedCountryId() {
		 return selectedCountryId;
	 }

	 public void setSelectedCountryId(String selectedCountryId) {

		 this.selectedCountryId = selectedCountryId;
		 if(this.selectedCountryId!=null)
			 viewRootMap.put("selectedCountryId",this.selectedCountryId); 
	 }
	 public Date getLastContractDate() {

		 Date temp = (Date)viewRootMap.get("lastContractDate");
		 return temp;




	 }
	 public void setLastContractDate(Date lastContractDate) {
		 if(lastContractDate!=null)
			 viewRootMap.put("lastContractDate" , lastContractDate);

	 }
	 public HtmlDataTable getReceivingTeamGrid() {
		 return receivingTeamGrid;
	 }
	 public void setReceivingTeamGrid(HtmlDataTable receivingTeamGrid) {
		 this.receivingTeamGrid = receivingTeamGrid;
	 }
		public List<UserView> getReceivingDataList() {
			
			if(viewRootMap.containsKey("RECEIVING_TEAM_DATA_LIST"))
				receivingDataList = (ArrayList<UserView>)viewRootMap.get("RECEIVING_TEAM_DATA_LIST");

			return receivingDataList;
		}
		public void setReceivingDataList(List<UserView> receivingDataList) {
			this.receivingDataList = receivingDataList;
			
			if(this.receivingDataList!=null && this.receivingDataList.size()>0)
				viewRootMap.put("RECEIVING_TEAM_DATA_LIST",this.receivingDataList);
		}
	 public HtmlDataTable getEvaluationTeamGrid() {
		 return evaluationTeamGrid;
	 }
	 public void setEvaluationTeamGrid(HtmlDataTable evaluationTeamGrid) {
		 this.evaluationTeamGrid = evaluationTeamGrid;
	 }
	 public List<UserView> getEvaluationDataList() {
		 if(viewRootMap.containsKey("EVALUATION_TEAM_DATA_LIST"))
			 evaluationDataList = (ArrayList<UserView>)viewRootMap.get("EVALUATION_TEAM_DATA_LIST");

		 return evaluationDataList;
	 }
	 public void setEvaluationDataList(List<UserView> evaluationDataList) {
		 this.evaluationDataList = evaluationDataList;
		 
		 if(this.evaluationDataList!=null && this.evaluationDataList.size()>0)
				viewRootMap.put("EVALUATION_TEAM_DATA_LIST",this.evaluationDataList);
	 }
	 public HtmlCommandButton getAddEvaluationTeam() {
		 return addEvaluationTeam;
	 }
	 public void setAddEvaluationTeam(HtmlCommandButton addEvaluationTeam) {
		 this.addEvaluationTeam = addEvaluationTeam;
	 }
	 public HtmlCommandButton getAddReceiveingTeam() {
		 return addReceiveingTeam;
	 }
	 public void setAddReceiveingTeam(HtmlCommandButton addReceiveingTeam) {
		 this.addReceiveingTeam = addReceiveingTeam;
	 }
	 public String getFacilityId() {
		 return facilityId;
	 }
	 public void setFacilityId(String facilityId) {
		 this.facilityId = facilityId;
	 }
	 public List<SelectItem> getFacilityList() {
		 return facilityList;
	 }
	 public void setFacilityList(List<SelectItem> facilityList) {
		 this.facilityList = facilityList;
	 }
	 public String getFacilityTypeId() {
		 return facilityTypeId;
	 }
	 public void setFacilityTypeId(String facilityTypeId) {
		 this.facilityTypeId = facilityTypeId;
	 }
	 public List<SelectItem> getFacilityTypeList() {
		 return facilityTypeList;
	 }
	 public void setFacilityTypeList(List<SelectItem> facilityTypeList) {
		 this.facilityTypeList = facilityTypeList;
	 }
	 public HtmlInputText getFacilityDescription() {
		 return facilityDescription;
	 }
	 public void setFacilityDescription(HtmlInputText facilityDescription) {
		 this.facilityDescription = facilityDescription;
	 }
	 public HtmlInputHidden getOwnerHiddenId() {
		 return ownerHiddenId;
	 }
	 public void setOwnerHiddenId(HtmlInputHidden ownerHiddenId) {
		 this.ownerHiddenId = ownerHiddenId;
	 }
	 public HtmlInputHidden getRepresentativeHiddenId() {
		 return representativeHiddenId;
	 }
	 public void setRepresentativeHiddenId(HtmlInputHidden representativeHiddenId) {
		 this.representativeHiddenId = representativeHiddenId;
	 }
	 public Boolean getShowTabs() {
		 return showTabs;
	 }
	 public void setShowTabs(Boolean showTabs) {
		 this.showTabs = showTabs;
	 }
	 public HtmlSelectOneMenu getCityMenu() {
		 return cityMenu;
	 }
	 public void setCityMenu(HtmlSelectOneMenu cityMenu) {
		 this.cityMenu = cityMenu;
	 }
	 public Boolean getShowDocNComTabs() {
		 return showDocNComTabs;
	 }
	 public void setShowDocNComTabs(Boolean showDocNComTabs) {
		 this.showDocNComTabs = showDocNComTabs;
	 }
	 public HtmlInputHidden getShowTabsHidden() {
		 return showTabsHidden;
	 }
	 public void setShowTabsHidden(HtmlInputHidden showTabsHidden) {
		 this.showTabsHidden = showTabsHidden;
	 }
	 public HtmlCommandButton getAddFloorButton() {
		 return addFloorButton;
	 }
	 public void setAddFloorButton(HtmlCommandButton addFloorButton) {
		 this.addFloorButton = addFloorButton;
	 }
	 public HtmlCommandButton getDuplicateSelectedFloor() {
		 return duplicateSelectedFloor;
	 }
	 public void setDuplicateSelectedFloor(HtmlCommandButton duplicateSelectedFloor) {
		 this.duplicateSelectedFloor = duplicateSelectedFloor;
	 }
	 public HtmlInputTextarea getFloorDescription() {
		 return floorDescription;
	 }
	 public void setFloorDescription(HtmlInputTextarea floorDescription) {
		 this.floorDescription = floorDescription;
	 }
	 public HtmlInputText getFloorFrom() {
		 return floorFrom;
	 }
	 public void setFloorFrom(HtmlInputText floorFrom) {
		 this.floorFrom = floorFrom;
	 }
	 public HtmlInputText getFloorPrefix() {
		 return floorPrefix;
	 }
	 public void setFloorPrefix(HtmlInputText floorPrefix) {
		 this.floorPrefix = floorPrefix;
	 }
	 public HtmlInputText getFloorSeperator() {
		 return floorSeperator;
	 }
	 public void setFloorSeperator(HtmlInputText floorSeperator) {
		 this.floorSeperator = floorSeperator;
	 }
	 public HtmlInputText getFloorTo() {
		 return floorTo;
	 }
	 public void setFloorTo(HtmlInputText floorTo) {
		 this.floorTo = floorTo;
	 }
	 public List<SelectItem> getFloorTypeItems() {
		 return floorTypeItems;
	 }
	 public void setFloorTypeItems(List<SelectItem> floorTypeItems) {
		 this.floorTypeItems = floorTypeItems;
	 }
	 public HtmlSelectOneMenu getFloorTypeSelectOneMenu() {
		 return floorTypeSelectOneMenu;
	 }
	 public void setFloorTypeSelectOneMenu(HtmlSelectOneMenu floorTypeSelectOneMenu) {
		 this.floorTypeSelectOneMenu = floorTypeSelectOneMenu;
	 }
	 /*	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}*/
	 public HtmlCommandButton getAddUnitButton() {
		 return addUnitButton;
	 }
	 public void setAddUnitButton(HtmlCommandButton addUnitButton) {
		 this.addUnitButton = addUnitButton;
	 }
	 public HtmlInputText getAreaInSquare() {
		 return areaInSquare;
	 }
	 public void setAreaInSquare(HtmlInputText areaInSquare) {
		 this.areaInSquare = areaInSquare;
	 }
	 public HtmlInputText getBathRooms() {
		 return bathRooms;
	 }
	 public void setBathRooms(HtmlInputText bathRooms) {
		 this.bathRooms = bathRooms;
	 }
	 public HtmlInputText getBedRooms() {
		 return bedRooms;
	 }
	 public void setBedRooms(HtmlInputText bedRooms) {
		 this.bedRooms = bedRooms;
	 }
	 public HtmlSelectOneMenu getFloorNumberMenu() {
		 return floorNumberMenu;
	 }
	 public void setFloorNumberMenu(HtmlSelectOneMenu floorNumberMenu) {
		 this.floorNumberMenu = floorNumberMenu;
	 }
	 public List<SelectItem> getFloorsList() {
		 return floorsList;
	 }
	 public void setFloorsList(List<SelectItem> floorsList) {
		 this.floorsList = floorsList;
	 }
	 public HtmlSelectBooleanCheckbox getIncludeFloorNumber() {
		 return includeFloorNumber;
	 }
	 public void setIncludeFloorNumber(HtmlSelectBooleanCheckbox includeFloorNumber) {
		 this.includeFloorNumber = includeFloorNumber;
	 }
	 public HtmlInputText getLivingRooms() {
		 return livingRooms;
	 }
	 public void setLivingRooms(HtmlInputText livingRooms) {
		 this.livingRooms = livingRooms;
	 }
	 public HtmlInputText getUnitDescription() {
		 return unitDescription;
	 }
	 public void setUnitDescription(HtmlInputText unitDescription) {
		 this.unitDescription = unitDescription;
	 }
	 public HtmlInputText getUnitEWUtilityNumber() {
		 return unitEWUtilityNumber;
	 }
	 public void setUnitEWUtilityNumber(HtmlInputText unitEWUtilityNumber) {
		 this.unitEWUtilityNumber = unitEWUtilityNumber;
	 }
	 public List<SelectItem> getUnitInvestmentTypeList() {
		 return unitInvestmentTypeList;
	 }
	 public void setUnitInvestmentTypeList(List<SelectItem> unitInvestmentTypeList) {
		 this.unitInvestmentTypeList = unitInvestmentTypeList;
	 }
	 public HtmlSelectOneMenu getUnitInvestmentTypeMenu() {
		 return unitInvestmentTypeMenu;
	 }
	 public void setUnitInvestmentTypeMenu(HtmlSelectOneMenu unitInvestmentTypeMenu) {
		 this.unitInvestmentTypeMenu = unitInvestmentTypeMenu;
	 }
	 public HtmlInputText getUnitSeperator() {
		 return unitSeperator;
	 }
	 public void setUnitSeperator(HtmlInputText unitSeperator) {
		 this.unitSeperator = unitSeperator;
	 }
	 public List<SelectItem> getUnitSideList() {
		 return unitSideList;
	 }
	 public void setUnitSideList(List<SelectItem> unitSideList) {
		 this.unitSideList = unitSideList;
	 }
	 public HtmlSelectOneMenu getUnitSideMenu() {
		 return unitSideMenu;
	 }
	 public void setUnitSideMenu(HtmlSelectOneMenu unitSideMenu) {
		 this.unitSideMenu = unitSideMenu;
	 }

	 public List<SelectItem> getUnitTypeList() {
		 return unitTypeList;
	 }
	 public void setUnitTypeList(List<SelectItem> unitTypeList) {
		 this.unitTypeList = unitTypeList;
	 }
	 public HtmlSelectOneMenu getUnitTypeMenu() {
		 return unitTypeMenu;
	 }
	 public void setUnitTypeMenu(HtmlSelectOneMenu unitTypeMenu) {
		 this.unitTypeMenu = unitTypeMenu;
	 }
	 public List<SelectItem> getUnitUsageTypeList() {
		 return unitUsageTypeList;
	 }
	 public void setUnitUsageTypeList(List<SelectItem> unitUsageTypeList) {
		 this.unitUsageTypeList = unitUsageTypeList;
	 }
	 public HtmlSelectOneMenu getUnitUsageTypeMenu() {
		 return unitUsageTypeMenu;
	 }
	 public void setUnitUsageTypeMenu(HtmlSelectOneMenu unitUsageTypeMenu) {
		 this.unitUsageTypeMenu = unitUsageTypeMenu;
	 }
	 public HtmlInputText getUnitStatus() {
		 return unitStatus;
	 }
	 public void setUnitStatus(HtmlInputText unitStatus) {
		 this.unitStatus = unitStatus;
	 }
	 public HtmlInputTextarea getUnitRemarks() {
		 return unitRemarks;
	 }
	 public void setUnitRemarks(HtmlInputTextarea unitRemarks) {
		 this.unitRemarks = unitRemarks;
	 }
	 public HtmlInputText getUnitNo() {
		 return unitNo;
	 }
	 public void setUnitNo(HtmlInputText unitNo) {
		 this.unitNo = unitNo;
	 }
	 public HtmlInputText getUnitPrefix() {
		 return unitPrefix;
	 }
	 public void setUnitPrefix(HtmlInputText unitPrefix) {
		 this.unitPrefix = unitPrefix;
	 }

	 public String getOwnerRefNo() {
		 if(viewRootMap.containsKey("ownerReferenceNo"))
			 ownerRefNo=viewRootMap.get("ownerReferenceNo").toString();
		 return ownerRefNo;
	 }
	 public void setOwnerRefNo(String ownerRefNo) {
		 this.ownerRefNo = ownerRefNo;
		 if(this.ownerRefNo!=null)
			 viewRootMap.put("ownerReferenceNo", this.ownerRefNo);

	 }
	 public void setRepresentativeRefNo(String representativeRefNo) {
		 this.representativeRefNo = representativeRefNo;

		 if(this.representativeRefNo!=null)
			 viewRootMap.put("representativeRefNo", this.representativeRefNo);

	 }
	 public String getRepresentativeRefNo() {
		 if(viewRootMap.containsKey("representativeRefNo") && viewRootMap.get("representativeRefNo")!=null)
			 representativeRefNo=viewRootMap.get("representativeRefNo").toString();
		 return representativeRefNo;
	 }
	 public List<SelectItem> getCountryList() {
		 if(viewRootMap.containsKey("countryList")){
			 //countryList.clear();
			 countryList= (List<SelectItem>) (viewRootMap.get("countryList"));
		 }
		 return countryList;

	 }

	 public void setCountryList(List<SelectItem> countryList) {
		 this.countryList = countryList;
	 }

	 public List<SelectItem> getStateList() {
		 if(viewRootMap.containsKey("stateList")){
			 //stateList.clear();
			 stateList= (List<SelectItem>) (viewRootMap.get("stateList"));
		 }
		 return stateList;	


	 }

	 public void setStateList(List<SelectItem> stateList) {
		 this.stateList = stateList;

	 }

	 public List<SelectItem> getCityList() {
		 if(viewRootMap.containsKey("cityList")){
			 //cityList.clear();
			 cityList= (List<SelectItem>) (viewRootMap.get("cityList"));

		 }
		 return cityList;	

	 }

	 public void setCityList(List<SelectItem> cityList) {
		 this.cityList = cityList;

	 }

	 public String getDateFormat(){
		 return CommonUtil.getDateFormat();
	 }
	 public Boolean getIncludeFloorNumberVar() {
		 return includeFloorNumberVar;
	 }
	 public void setIncludeFloorNumberVar(Boolean includeFloorNumberVar) {
		 this.includeFloorNumberVar = includeFloorNumberVar;
	 }
	 public HtmlCommandButton getReviewApproveEvaluation() {
		 return reviewApproveEvaluation;
	 }
	 public void setReviewApproveEvaluation(HtmlCommandButton reviewApproveEvaluation) {
		 this.reviewApproveEvaluation = reviewApproveEvaluation;
	 }
	 public Boolean getDeleteAction() {
		 if(viewRootMap.containsKey("DELETE_ACTION")&&
				 (Boolean)viewRootMap.get("DELETE_ACTION"))
			 return true;
		 else
			 return false;
	 }
	 public void setDeleteAction(Boolean deleteAction) {
		 this.deleteAction = deleteAction;
	 }
	 public boolean isOnRentValue() {
		 return onRentValue;
	 }
	 public void setOnRentValue(boolean onRentValue) {
		 this.onRentValue = onRentValue;
	 }
	 private boolean validateUnitScreen() {

		 String method = "ValidateUnitScreen";
		 logger.logInfo(method + " started...");
		 boolean isValid = true;	

		 NumberFormat nf = new DecimalFormat(getNumberFormat());
		 if(floorNumberMenu.getValue()==null||floorNumberMenu.getValue().toString().equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_REQUIRED));
			 isValid = false;
		 }
		 try{
			 if(unitNo.getValue()!=null&&unitNo.getValue().toString().trim().length()>0)
				 Double.parseDouble(unitNo.getValue().toString());
			 else{
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_NO_REQUIRED));
			 }

			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_UNIT_NO));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_UNIT_NO));
		 }
		 if(unitTypeMenu.getValue()==null||unitTypeMenu.getValue().toString().equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_TYPE_REQUIRED));
			 isValid = false;
		 }
		 if(unitSideMenu.getValue()==null||unitSideMenu.getValue().toString().equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_SIDE_REQUIRED));
			 isValid = false;
		 }
		 if(unitUsageTypeMenu.getValue()==null||unitUsageTypeMenu.getValue().toString().equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_USAGE_TYPE_REQUIRED));
			 isValid = false;
		 }
		 if(unitInvestmentTypeMenu.getValue()==null||unitInvestmentTypeMenu.getValue().toString().equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_INVESTMENT_TYPE_REQUIRED));
			 isValid = false;
		 }




		 try{
			 if(areaInSquare.getValue()!=null&&areaInSquare.getValue().toString().trim().length()>0)
				 Double.parseDouble(areaInSquare.getValue().toString());
			 else{
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_AREA_REQUIRED));
			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_AREA));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_AREA));
		 }
		 try{
			 if(bedRooms.getValue()!=null&&bedRooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(bedRooms.getValue().toString());
			 else{
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_BEDROOMS_REQUIRED));

			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BEDROOMS));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BEDROOMS));
		 }
		 try{
			 if(livingRooms.getValue()!=null&&livingRooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(livingRooms.getValue().toString());
			 else{
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_LIVINGROOMS_REQUIRED));

			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LIVINGROOMS));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LIVINGROOMS));
		 }
		 try{
			 if(bathRooms.getValue()!=null&&bathRooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(bathRooms.getValue().toString());
			 else{
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_BATHROOMS_REQUIRED));

			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BATHROOMS));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BATHROOMS));
		 }
		 try{
			 if(isTaskTypeApproveEvaluation() )
			 {
				 if(
					 rentValueForUnits.getValue()!=null &&
					 rentValueForUnits.getValue().toString().trim().length()>0
				   )
				 {
					 Double.parseDouble(rentValueForUnits.getValue().toString());
				 }
				 else
				 {
					 isValid = false;
					 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_RENT_REQUIRED));

				 }
			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
		 }
		 try{
			 if(unitEWUtilityNumber.getValue()!=null&&unitEWUtilityNumber.getValue().toString().trim().length()>0)
				 Double.parseDouble(unitEWUtilityNumber.getValue().toString());
		
			 
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_EW_UTILITY_NUMBER));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_EW_UTILITY_NUMBER));
		 }
		 return isValid;
	 }
	 
	 
	 
	 
	 private boolean validateFloorScreen() 
	 {

		 String method = "ValidateFloorScreen";
		 logger.logInfo(method + " started...");
		 boolean isValid = true;	

		 NumberFormat nf = new DecimalFormat(getNumberFormat());
		 boolean floorFromToValidated=true;
		 try
		 {
			 if(floorFrom.getValue()==null || floorFrom.getValue().toString().trim().length()<=0)
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_NO_FROM_REQUIRED));
				 floorFromToValidated=false;
			 }
			 else
			 {
				 Integer.parseInt(floorFrom.getValue().toString());
				 
			 }
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_FROM));
			 floorFromToValidated=false;
		 }
		 catch (IllegalArgumentException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_FROM));
			 floorFromToValidated=false;
		 }
		 
		 try
		 {
			 if(floorTo.getValue()==null || floorTo.getValue().toString().trim().length()<=0)
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_NO_TO_REQUIRED));
				 floorFromToValidated=false;
			 }
			 else
				 Integer.parseInt(floorTo.getValue().toString());
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_TO));
			 floorFromToValidated=false;
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_TO));
			 floorFromToValidated=false;
		 }
		if(floorFromToValidated &&   Integer.parseInt(floorFrom.getValue().toString())>Integer.parseInt(floorTo.getValue().toString()))
		{
			isValid=false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.FLOORS_FROM_SMALLER_FLOOR_TO));
			
		}
		 
		 return isValid;
	 }
	public HtmlInputHidden getUnitStatusId() {
		return unitStatusId;
	}
	public void setUnitStatusId(HtmlInputHidden unitStatusId) {
		this.unitStatusId = unitStatusId;
	}
	 private void clearControls(){
		 floorNumberMenu.setReadonly(false);
		 unitPrefix.setReadonly(false);
		 unitNo.setReadonly(false);
		 
		 addUnitButton.setValue(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_ADD_VALUE));
		 floorNumberMenu.setValue(null);
		 unitHiddenId.setValue(null);
		 unitStatusId.setValue(null);
		 unitNumber.setValue(null);
		 unitNo.setValue(null);
		 unitPrefix.setValue(null);
		 unitTypeMenu.setValue(null);
		 unitSideMenu.setValue(null);
		 unitUsageTypeMenu.setValue(null);
		 unitInvestmentTypeMenu.setValue(null);
		 unitDescription.setValue(null);
		 areaInSquare.setValue(null);
		 bedRooms.setValue(0);
		 livingRooms.setValue(0);
		 bathRooms.setValue(0);
		 unitEWUtilityNumber.setValue(null);
		rentValueForUnits.setValue(null);
		unitRemarks.setValue(null);
		includeFloorNumberVar=false;
		accountNumber.setValue(null);
		 
		unitEWUtilityNumber.setValue(null); 
		 
	 }
	public HtmlCommandButton getFloorDeleteButton() {
		return floorDeleteButton;
	}
	public void setFloorDeleteButton(HtmlCommandButton floorDeleteButton) {
		this.floorDeleteButton = floorDeleteButton;
	}
	public HtmlCommandButton getUnitDeleteButton() {
		return unitDeleteButton;
	}
	public void setUnitDeleteButton(HtmlCommandButton unitDeleteButton) {
		this.unitDeleteButton = unitDeleteButton;
	}
	public void deleteMultipleFloors(){
		String methodName = "deleteMultipleFloors";
		 logger.logInfo(methodName+" |started");
			 try {
			for(ReceivePropertyFloorView receivePropertyFloorView: floorsDataList){
				if(receivePropertyFloorView.getIsSelected()!=null && receivePropertyFloorView.getIsSelected()){
					 propertyServiceAgent.deleteFloorById(receivePropertyFloorView.getFloorId());
					 
						
				}
			}
			floorsDataList=propertyServiceAgent.getPropertyFloorList(propertyView.getPropertyId());
			viewRootMap.put("FLOORS_DATA_LIST",floorsDataList);
			 logger.logInfo(methodName+" |compelted");
			 } catch (PimsBusinessException e) {
				 // TODO Auto-generated catch block
				 logger.LogException(methodName+"|Error Occured ",e);
			 }
		 
	}
	public void deleteMultipleUnits(){
		
		 String methodName = "deleteMultipleUnits";
		 logger.logInfo(methodName+" |started");
		 try {
					for(ReceivePropertyUnitView receivePropertyUnitView: unitDataList)
					{
						if(receivePropertyUnitView.getSelected()!=null && receivePropertyUnitView.getSelected())
						{
				           propertyServiceAgent.deleteUnitById(receivePropertyUnitView.getUnitId());
						}
					}
					 unitDataList=propertyServiceAgent.getPropertyUnitList(propertyView.getPropertyId());
					 viewRootMap.put("UNITS_DATA_LIST",unitDataList);
				 logger.logInfo(methodName+" |compelted");

		} catch (PimsBusinessException e) {
			 // TODO Auto-generated catch block
			 logger.LogException(methodName+"|Error Occured ",e);
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_UNIT_CANNOT_BE_DELETED));
		 }
	}
	private int getNoOfFloors(Long floorTypeId){
		int counter=0;
		if(floorTypeId!=null){
			for(ReceivePropertyFloorView floorView:floorsDataList){
				if(floorView.getTypeId().compareTo(floorTypeId)==0){
					counter++;
				}
			}
		}
		return counter;
	}
	public HtmlSelectOneMenu getAfterFloorMenu() {
		return afterFloorMenu;
	}
	public void setAfterFloorMenu(HtmlSelectOneMenu afterFloorMenu) {
		this.afterFloorMenu = afterFloorMenu;
	}
	public List<DomainDataView> getDdPropertyStatusList() {
		if(viewMap.containsKey("ddPropertyStatusList"))
			ddPropertyStatusList = (ArrayList<DomainDataView>)viewMap.get("ddPropertyStatusList");
		else
		{
			ddPropertyStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_STATUS);
			setDdPropertyStatusList(ddPropertyStatusList);
		}
		return ddPropertyStatusList;
	}
	public void setDdPropertyStatusList(List<DomainDataView> ddPropertyStatusList) 
	{
		this.ddPropertyStatusList = ddPropertyStatusList;
		if(this.ddPropertyStatusList!=null)
			viewMap.put("ddPropertyStatusList" , this.ddPropertyStatusList);
	}
	public String getHdnprojectNumber() {
		return hdnprojectNumber;
	}
	public void setHdnprojectNumber(String hdnprojectNumber) {
		this.hdnprojectNumber = hdnprojectNumber;
	}
	public Long getHdnprojectId() {
		return hdnprojectId;
	}
	public void setHdnprojectId(Long hdnprojectId) {
		this.hdnprojectId = hdnprojectId;
	}
	public HtmlGraphicImage getProjectImage() {
		return projectImage;
	}
	public void setProjectImage(HtmlGraphicImage projectImage) {
		this.projectImage = projectImage;
	}
	public void getPropertyUnitTypeDesc(long propertyId) throws PimsBusinessException
	{
		List<UnitTypeDescView> unitTypeDescList = new ArrayList<UnitTypeDescView>();
		unitTypeDescList = new PropertyService().getUnitTypesByPropertyId(propertyId);
		List<SelectItem> unitTypeItems = new ArrayList<SelectItem>();
		HashMap<Long ,String> addedItems = new HashMap<Long, String>();
		
		if(unitTypeDescList != null && unitTypeDescList.size() > 0)
		{
			SelectItem item = null;
			for(UnitTypeDescView unitType : unitTypeDescList)
			{
				if( ! addedItems.containsKey(unitType.getUnitTypeId()))
				{
					if(isEnglishLocale)
						{
							item = new SelectItem(unitType.getDescEn(), unitType.getDescEn());
							addedItems.put(unitType.getUnitTypeId(), unitType.getDescEn());
						}
					else
						{
							item = new SelectItem(unitType.getDescAr(), unitType.getDescAr());
							addedItems.put(unitType.getUnitTypeId(), unitType.getDescAr());
						}
					unitTypeItems.add(item);
				}
			}
			viewMap.put("UNIT_TYPE_VIEW_ITEMS", unitTypeItems);
		}
		
	}
	public void unitTypeAmountEditTabClick() 
	{
		if(viewMap.get("UNIT_TYPE_LIST_BY_PROPERTY") != null )
		return ;
		else if(viewMap.get("PROPERTY_ID_FOR_FETCHING_UNIT_TYPE") != null)
		{
			try 
			{
			long propertyId = (Long) viewMap.get("PROPERTY_ID_FOR_FETCHING_UNIT_TYPE");
			getPropertyUnitTypeDesc(propertyId);
			} 
			catch (PimsBusinessException e) 
			{
				logger.LogException("unitTypeAmountEditTabClick crashed |", e);
			}
		}
	}
	public boolean isShowChangeRentTab() 
	{
		if(viewMap.get("ADD_RENT_AMOUNT_MODE") != null && (Boolean)viewMap.get("ADD_RENT_AMOUNT_MODE"))
			return true;
		else
			return false;
	}
	public void setShowChangeRentTab(boolean showChangeRentTab) 
	{
		this.showChangeRentTab = showChangeRentTab;
	}
	public void ownershipTypeChanged(ValueChangeEvent event)
	{
			String minorId = null;
			if( viewMap.get("MINOR_DD_ID")!= null )
			{
				minorId= viewMap.get("MINOR_DD_ID").toString();
				if( ownershipType.getValue() != null && ownershipType.getValue().toString().compareTo("-1") != 0) 
				{
					
					if(minorId.compareTo(ownershipType.getValue().toString()) == 0)
					{
						viewMap.put("IS_MINOR", true);
						messages.add(CommonUtil.getBundleMessage("infoMsg.receiveProperty.plzAddAsset"));
					}
					else
						viewMap.put("IS_MINOR", false);
					
					
				}
				if(event.getOldValue() != null && event.getOldValue().toString().compareTo(minorId) == 0)
				{
					errorMessages.add(CommonUtil.getBundleMessage("errMsg.receiveProeprty.changesLost"));
					resetAssetFields();
				}
			}
			
			else
				viewMap.put("IS_MINOR", false);
		showTabs = false;
		showDocNComTabs = false;
	}
	public boolean isShowAssetControls()
	{
		boolean show = false;
		if(viewMap.get("IS_MINOR") != null && (Boolean)viewMap.get("IS_MINOR"))
			show = true;
		return show;
	}
	public boolean isMinorSelected()
	{
		boolean isMinor = false;
		if(viewMap.get("IS_MINOR") != null && (Boolean)viewMap.get("IS_MINOR"))
			isMinor = true;
		return isMinor;
	}
	public HtmlInputText getTxtAssetNumber() {
		return txtAssetNumber;
	}
	public void setTxtAssetNumber(HtmlInputText txtAssetNumber) {
		this.txtAssetNumber = txtAssetNumber;
	}
	public HtmlInputText getTxtAssetNameEn() {
		return txtAssetNameEn;
	}
	public void setTxtAssetNameEn(HtmlInputText txtAssetNameEn) {
		this.txtAssetNameEn = txtAssetNameEn;
	}
	public HtmlInputText getTxtAssetNameAr() {
		return txtAssetNameAr;
	}
	public void setTxtAssetNameAr(HtmlInputText txtAssetNameAr) {
		this.txtAssetNameAr = txtAssetNameAr;
	}
	public HtmlInputText getTxtAssetDesc() {
		return txtAssetDesc;
	}
	public void setTxtAssetDesc(HtmlInputText txtAssetDesc) {
		this.txtAssetDesc = txtAssetDesc;
	}
	public void setShowAssetControls(boolean isShowAssetControls) {
		this.isShowAssetControls = isShowAssetControls;
	}
	public HtmlGraphicImage getImgSearchAsset() {
		return imgSearchAsset;
	}
	public void setImgSearchAsset(HtmlGraphicImage imgSearchAsset) {
		this.imgSearchAsset = imgSearchAsset;
	}
	public void receiveAsset()
	{
		if(sessionMap.get(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET) != null)
		{
			AssetsGridView asset = ((AssetsGridView) sessionMap.get(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET));
			sessionMap.remove(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET);
			if(StringHelper.isNotEmpty(asset.getAssetNumber()))
				txtAssetNumber.setValue(asset.getAssetNumber());
			
			if(StringHelper.isNotEmpty(asset.getAssetDesc()))
				txtAssetDesc.setValue(asset.getAssetDesc());
			
			if(StringHelper.isNotEmpty(asset.getAssetNameEn()))
				txtAssetNameEn.setValue(asset.getAssetNameEn());
			
			if(StringHelper.isNotEmpty(asset.getAssetNameAr()))
				txtAssetNameAr.setValue(asset.getAssetNameAr());
			
			viewMap.put("ASSET_ID", asset.getAssetId());
		}
	}
	public void resetAssetFields()
	{
		txtAssetNumber.setValue(null);
		txtAssetDesc.setValue(null);
		txtAssetNameEn.setValue(null);
		txtAssetNameAr.setValue(null);
		if(viewMap.get("ASSET_ID") != null)
			viewMap.remove("ASSET_ID");
	}
	public void openManageAsset()
	{
		
		if(viewMap.get("ASSET_ID") != null)
		{
			Long assetId = (Long) viewMap.get("ASSET_ID"); 
			String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 550;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.ASSET_ID+"="+assetId
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

			executeJavascript(javaScriptText);
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzSelectAsset"));
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	private void calculateBuildingHeight(
			List<ReceivePropertyFloorView> floorList) {
		String methodName = "calculateBuildingHeight";
		logger.logInfo(methodName + "|" + "Start..");
		String buildingHeight = "";

		byte basement = 0;
		byte ground = 0;
		byte mezzanine = 0;
		byte top = 0;
		byte RoofTop = 0;
//		if (viewMap.get("BUILDING_HEIGHT") == null) {
			try {
				for (ReceivePropertyFloorView floor : floorList) {
					switch (getPrefixCode(floor)) {
					case 1:
						basement++;
						break;
					case 2:
						ground++;
						break;
					case 3:
						mezzanine++;
						break;
					case 4:
						top++;
						break;
					case 5:
						RoofTop++;
						break;
					}
				}
				buildingHeight = (basement != 0 ? basement + "B+" : "")
						+ (ground != 0 ? (ground>1?ground:"") + "G+" : "")
						+ (mezzanine != 0 ? mezzanine + "M+" : "")
						+ (top != 0 ? top + "F+" : "")
						+ (RoofTop != 0 ? RoofTop + "R" : "");
				buildingHeight = buildingHeight.endsWith("+") ? buildingHeight
						.substring(0, buildingHeight.length() - 1)
						: buildingHeight;
				if(StringHelper.isNotEmpty(buildingHeight))		
				viewMap.put("BUILDING_HEIGHT", buildingHeight);
			} catch (Exception ex) {
				logger.LogException(methodName + "|" + "Error Occured..", ex);
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
//		}
		logger.logInfo(methodName + "|" + "Finish..");
	}
	private int getPrefixCode(ReceivePropertyFloorView floor) {
		FloorPrefix floorEnum = FloorPrefix.DEFAULT;

		if (isBasement(floor)) {
			floorEnum = FloorPrefix.BASEMENT;
		} else if (isGround(floor)) {
			floorEnum = FloorPrefix.GROUND;
		} else if (isMezzanin(floor)) {
			floorEnum = FloorPrefix.MEZZANINE;
		} else if (isTopFloor(floor)) {
			floorEnum = FloorPrefix.TOP;
		} else if (isRoofTop(floor)) {
			floorEnum = FloorPrefix.ROOF;
		}
		return floorEnum.getPrefix();
	}
	private boolean isBasement(ReceivePropertyFloorView floor) {
		floorTypeBasement = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_BASEMENT);
		if(floorTypeBasement != null && floorTypeBasement.getDomainDataId() != null){
			if(floor.getTypeId().compareTo(floorTypeBasement.getDomainDataId()) == 0){
				return true;
			}
		}
		return false;
	}
	
	private boolean isGround(ReceivePropertyFloorView floor) {
		floorTypeGround = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_GROUND);
		if(floorTypeGround != null && floorTypeGround.getDomainDataId() != null){
			if(floor.getTypeId().compareTo(floorTypeGround.getDomainDataId()) == 0){
				return true;
			}
		}
		return false;
	}
	
	private boolean isMezzanin(ReceivePropertyFloorView floor) {
		floorTypeMezzanine = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_MEZZANINE);
		if(floorTypeMezzanine != null && floorTypeMezzanine.getDomainDataId() != null){
			if(floor.getTypeId().compareTo(floorTypeMezzanine.getDomainDataId()) == 0){
				return true;
			}
		}
		return false;
	}
	
	private boolean isTopFloor(ReceivePropertyFloorView floor) {
		floorTypeTop = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_TOP_FLOOR);
		if(floorTypeTop != null && floorTypeTop.getDomainDataId() != null){
			if(floor.getTypeId().compareTo(floorTypeTop.getDomainDataId()) == 0){
				return true;
			}
		}
		return false;
	}
	
	private boolean isRoofTop(ReceivePropertyFloorView floor) {
		floorTypeRoofTop = (DomainDataView) viewMap.get(FloorType.DD_FLOOR_TYPE_ROOF_TOP);
		if(floorTypeRoofTop != null && floorTypeRoofTop.getDomainDataId() != null){
			if(floor.getTypeId().compareTo(floorTypeRoofTop.getDomainDataId()) == 0){
				return true;
			}
		}
		return false;
	}
	public String getBuildingHeight()
	{
		if(viewMap.get("BUILDING_HEIGHT") != null ){
			return viewMap.get("BUILDING_HEIGHT").toString();
		}
		return "";
	}
	public boolean isUnderReceiving()throws Exception {
 
		if(viewMap.get(PROPERTY_STATUS_UNDER_RECEIVING) != null && viewMap.get(WebConstants.Property.PROPERTY_VIEW) != null)
		{
			PropertyView propertyView = (PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW);
			logger.logDebug( "propertyView.getStatusId():%s",propertyView.getStatusId() );
			Long underReceiving=1001L;
			if(	underReceiving.compareTo( propertyView.getStatusId() ) == 0)
			{
				logger.logDebug( "mathc");
				return true;
			}
			else
			{
				logger.logDebug( " not mathc");
			}
		}
		return false;
	}
	public void setUnderReceiving(boolean isUnderReceiving) {
		this.isUnderReceiving = isUnderReceiving;
	}
	public boolean isUnderEvaluation() {
		if(viewMap.get(PROPERTY_STATUS_UNDER_EVALUATION) != null && viewMap.get(WebConstants.Property.PROPERTY_VIEW) != null)
		{
			if(
					((DomainDataView)viewMap.get(PROPERTY_STATUS_UNDER_EVALUATION)).getDomainDataId().compareTo
					(((PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW)).getStatusId()) == 0)
				return true;
		}
		return false;
	}
	public void setUnderEvaluation(boolean isUnderEvaluation) {
		this.isUnderEvaluation = isUnderEvaluation;
	}
	public boolean isAllowedToUpdate()throws Exception {
		if(viewMap.get("IS_ALLOWED") != null && (Boolean)viewMap.get("IS_ALLOWED")){
			return true;
		}
		else if(viewMap.get(WebConstants.Property.PROPERTY_VIEW) != null){
			propertyView = (PropertyView) viewMap.get(WebConstants.Property.PROPERTY_VIEW);
			if((isUnderReceiving() || isTaskTypePropertyEvaluation()) && isLoggedInUserCreatedRecord(propertyView.getCreatedBy()))
			{
				viewMap.put("IS_ALLOWED", true);
				return true;
			}
		}
		else if(viewMap.get(WebConstants.Property.PROPERTY_VIEW) == null){
			return true;
		}
		return false;
	}
	public void setAllowedToUpdate(boolean isAllowedToUpdate) {
		this.isAllowedToUpdate = isAllowedToUpdate;
	}
//	private boolean hasError(){
//		if(isAllowedToUpdate())
//			return false;
//		else{
//			errorMessages.add(CommonUtil.getBundleMessage("errMsg.propertyIsCreatedByDifferentGroupUCantUpdate"));
//			return true;
//		}
//	}
	private boolean isTaskTypePropertyEvaluation(){
		if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
			UserTask task = new UserTask();
			task = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			if(task!=null && task.getTaskType().equals(WebConstants.Property.EVALUATE_PROPERTY))
				return true;
		}
		return false;
	}
	private boolean isTaskTypeApproveEvaluation(){
		if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
			UserTask task = new UserTask();
			task = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			if(task!=null && task.getTaskType().equals(WebConstants.Property.APPROVE_EVALUATION))
				return true;
		}
		return false;
	}
	public boolean isPropertyReceived() {
		if(viewMap.get(PROPERTY_STATUS_RECEIVED) != null && viewMap.get(WebConstants.Property.PROPERTY_VIEW) != null)
		{
			if(
					((DomainDataView)viewMap.get(PROPERTY_STATUS_RECEIVED)).getDomainDataId().compareTo
					(((PropertyView)viewMap.get(WebConstants.Property.PROPERTY_VIEW)).getStatusId()) == 0)
				return true;
		}
		return false;
	}
	public void setPropertyReceived(boolean isPropertyReceived) {
		this.isPropertyReceived = isPropertyReceived;
	}
	public void generatePropertyFloorsAfterReceived(){
		viewMap.put("IS_ALLOWED", true);
		generatePropertyFloors();
	}
	public void generatePropertyUnitsAfterReceived(){
		viewMap.put("IS_ALLOWED", true);
		generatePropertyUnits();
	}
	public void editUnitAtApproval(){
		editUnit();
		unitTypeMenu.setReadonly(true);
		unitSideMenu.setReadonly(true);
		areaInSquare.setReadonly(true);
	}
	public boolean isApprovalTask() {
		if(isTaskTypeApproveEvaluation())
			return true;
		else
			return false;
	}
	public void setApprovalTask(boolean isApprovalTask) {
		this.isApprovalTask = isApprovalTask;
	}
	public void updateUnitAtApproval(){
		generatePropertyUnits();
	}
	public HtmlCommandButton getBtnUpdateUnitAtApproval() {
		return btnUpdateUnitAtApproval;
	}
	public void setBtnUpdateUnitAtApproval(HtmlCommandButton btnUpdateUnitAtApproval) {
		this.btnUpdateUnitAtApproval = btnUpdateUnitAtApproval;
	}
	public String getNewCC() {
		if(viewMap.get("NEW_CC") != null){
			newCC = viewMap.get("NEW_CC").toString();
		}
		return newCC;
	}
	public void setNewCC(String newCC) {
		this.newCC = newCC;
	}
	public String getOldCC() {
		if(viewMap.get("OLD_CC") != null){
			oldCC = viewMap.get("OLD_CC").toString();
		}
		return oldCC;
	}
	public void setOldCC(String oldCC) {
		this.oldCC = oldCC;
	}
	public HtmlInputText getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(HtmlInputText accountNumber) {
		this.accountNumber = accountNumber;
	}
	public HtmlInputText getTxtEndowmentRef() {
		return txtEndowmentRef;
	}
	public void setTxtEndowmentRef(HtmlInputText txtEndowmentRef) {
		this.txtEndowmentRef = txtEndowmentRef;
	}
	public HtmlGraphicImage getImgSearchEndowment() {
		return imgSearchEndowment;
	}
	public void setImgSearchEndowment(HtmlGraphicImage imgSearchEndowment) {
		this.imgSearchEndowment = imgSearchEndowment;
	}
	public String getEndowmentId() {
		if( viewMap.get("endowmentId")!= null )
		{
			endowmentId = viewMap.get("endowmentId").toString();
		}
		return endowmentId;
	}
	public void setEndowmentId(String endowmentId) {
		this.endowmentId = endowmentId;
		if( this.endowmentId != null )
		{
			viewMap.put( "endowmentId",this.endowmentId);
		}
	}
	public HtmlCommandButton getSaveAfterReceivedButton() {
		return saveAfterReceivedButton;
	}
	public void setSaveAfterReceivedButton(HtmlCommandButton saveAfterReceivedButton) {
		this.saveAfterReceivedButton = saveAfterReceivedButton;
	}
	public HtmlInputText getPropertyNameEn() {
		return propertyNameEn;
	}
	public void setPropertyNameEn(HtmlInputText propertyNameEn) {
		this.propertyNameEn = propertyNameEn;
	}
}
