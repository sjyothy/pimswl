package com.avanza.pims.web.backingbeans;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneListbox;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.PropertyView;

public class Property extends AbstractController {

	Logger logger = Logger.getLogger(Property.class);
	
	private static final long serialVersionUID = 8832446416083043942L;
	
	private HtmlSelectOneListbox ddlTypeId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlStatusId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlCategoryId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlUsageTypeId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlInvestmentPurposeId = new HtmlSelectOneListbox();
    
	private HtmlInputHidden hdnPropertyId = new HtmlInputHidden();
	
	private HtmlInputText txtPropertyNumber = new HtmlInputText();
	private HtmlInputText txtFinancialAccountNumber = new HtmlInputText();
    private HtmlInputText txtNoOfUnits = new HtmlInputText();
    private HtmlInputText txtEndowedName = new HtmlInputText();
    private HtmlInputText txtCommercialName = new HtmlInputText();
    private HtmlInputText txtLandNumber = new HtmlInputText();
    private HtmlInputText txtLandDeptCode = new HtmlInputText();
    private HtmlInputText txtMunicipalityPlanNo = new HtmlInputText();
    private HtmlInputText txtProjectNo = new HtmlInputText();
    private HtmlInputText txtLandArea = new HtmlInputText();
    private HtmlInputText txtBuiltInArea = new HtmlInputText();
    private HtmlInputText txtNoFloors = new HtmlInputText();
    private HtmlInputText txtNoOfMezzanineFloors = new HtmlInputText();
    private HtmlInputText txtNoOfParkingFloors = new HtmlInputText();
    private HtmlInputText txtNoOfFlats = new HtmlInputText();
    private HtmlInputText txtNoOfLifts = new HtmlInputText();
    private HtmlInputText txtDewaNumber = new HtmlInputText();
   
	private PropertyView propertyBean = new PropertyView(); 

	public PropertyView getPropertyBean() {
		return propertyBean;
	}

	public void setPropertyBean(PropertyView propertyBean) {
		this.propertyBean = propertyBean;
	}

	public HtmlSelectOneListbox getDdlTypeId() {
		return ddlTypeId;
	}

	public void setDdlTypeId(HtmlSelectOneListbox ddlTypeId) {
		this.ddlTypeId = ddlTypeId;
	}

	public HtmlSelectOneListbox getDdlStatusId() {
		return ddlStatusId;
	}

	public void setDdlStatusId(HtmlSelectOneListbox ddlStatusId) {
		this.ddlStatusId = ddlStatusId;
	}

	public HtmlSelectOneListbox getDdlCategoryId() {
		return ddlCategoryId;
	}

	public void setDdlCategoryId(HtmlSelectOneListbox ddlCategoryId) {
		this.ddlCategoryId = ddlCategoryId;
	}

	public HtmlSelectOneListbox getDdlUsageTypeId() {
		return ddlUsageTypeId;
	}

	public void setDdlUsageTypeId(HtmlSelectOneListbox ddlUsageTypeId) {
		this.ddlUsageTypeId = ddlUsageTypeId;
	}

	public HtmlSelectOneListbox getDdlInvestmentPurposeId() {
		return ddlInvestmentPurposeId;
	}

	public void setDdlInvestmentPurposeId(
			HtmlSelectOneListbox ddlInvestmentPurposeId) {
		this.ddlInvestmentPurposeId = ddlInvestmentPurposeId;
	}

	public HtmlInputHidden getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(HtmlInputHidden hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlInputText getTxtPropertyNumber() {
		return txtPropertyNumber;
	}

	public void setTxtPropertyNumber(HtmlInputText txtPropertyNumber) {
		this.txtPropertyNumber = txtPropertyNumber;
	}

	public HtmlInputText getTxtNoOfUnits() {
		return txtNoOfUnits;
	}

	public void setTxtNoOfUnits(HtmlInputText txtNoOfUnits) {
		this.txtNoOfUnits = txtNoOfUnits;
	}

	public HtmlInputText getTxtEndowedName() {
		return txtEndowedName;
	}

	public void setTxtEndowedName(HtmlInputText txtEndowedName) {
		this.txtEndowedName = txtEndowedName;
	}

	public HtmlInputText getTxtCommercialName() {
		return txtCommercialName;
	}

	public void setTxtCommercialName(HtmlInputText txtCommercialName) {
		this.txtCommercialName = txtCommercialName;
	}

	public HtmlInputText getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(HtmlInputText txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public HtmlInputText getTxtLandDeptCode() {
		return txtLandDeptCode;
	}

	public void setTxtLandDeptCode(HtmlInputText txtLandDeptCode) {
		this.txtLandDeptCode = txtLandDeptCode;
	}

	public HtmlInputText getTxtMunicipalityPlanNo() {
		return txtMunicipalityPlanNo;
	}

	public void setTxtMunicipalityPlanNo(HtmlInputText txtMunicipalityPlanNo) {
		this.txtMunicipalityPlanNo = txtMunicipalityPlanNo;
	}

	public HtmlInputText getTxtProjectNo() {
		return txtProjectNo;
	}

	public void setTxtProjectNo(HtmlInputText txtProjectNo) {
		this.txtProjectNo = txtProjectNo;
	}

	public HtmlInputText getTxtLandArea() {
		return txtLandArea;
	}

	public void setTxtLandArea(HtmlInputText txtLandArea) {
		this.txtLandArea = txtLandArea;
	}

	public HtmlInputText getTxtBuiltInArea() {
		return txtBuiltInArea;
	}

	public void setTxtBuiltInArea(HtmlInputText txtBuiltInArea) {
		this.txtBuiltInArea = txtBuiltInArea;
	}

	public HtmlInputText getTxtNoFloors() {
		return txtNoFloors;
	}

	public void setTxtNoFloors(HtmlInputText txtNoFloors) {
		this.txtNoFloors = txtNoFloors;
	}

	public HtmlInputText getTxtNoOfMezzanineFloors() {
		return txtNoOfMezzanineFloors;
	}

	public void setTxtNoOfMezzanineFloors(HtmlInputText txtNoOfMezzanineFloors) {
		this.txtNoOfMezzanineFloors = txtNoOfMezzanineFloors;
	}

	public HtmlInputText getTxtNoOfParkingFloors() {
		return txtNoOfParkingFloors;
	}

	public void setTxtNoOfParkingFloors(HtmlInputText txtNoOfParkingFloors) {
		this.txtNoOfParkingFloors = txtNoOfParkingFloors;
	}

	public HtmlInputText getTxtNoOfFlats() {
		return txtNoOfFlats;
	}

	public void setTxtNoOfFlats(HtmlInputText txtNoOfFlats) {
		this.txtNoOfFlats = txtNoOfFlats;
	}

	public HtmlInputText getTxtNoOfLifts() {
		return txtNoOfLifts;
	}

	public void setTxtNoOfLifts(HtmlInputText txtNoOfLifts) {
		this.txtNoOfLifts = txtNoOfLifts;
	}

	public HtmlInputText getTxtDewaNumber() {
		return txtDewaNumber;
	}

	public void setTxtDewaNumber(HtmlInputText txtDewaNumber) {
		this.txtDewaNumber = txtDewaNumber;
	}
	
	public String btnSaveAction() {
		logger.logInfo("btnSaveAction start ...");
//		try {
//			PIMSPropertyWSSoapHttpPortClient proxy = new PIMSPropertyWSSoapHttpPortClient();
//			propertyBean.setCreatedBy("Saquib");
//			propertyBean.setCreatedOn(Calendar.getInstance());
//			propertyBean.setUpdatedBy("Saquib");
//			propertyBean.setUpdatedOn(Calendar.getInstance());
//			propertyBean.setIsDeleted(0L);
//			propertyBean.setRecordStatus(1L);
//			if(propertyBean.getPropertyId()==null) {
//				propertyBean = proxy.addProperty(propertyBean);
//			} else {
//				propertyBean = proxy.updateProperty(propertyBean);
//			}
//		} catch(Exception e){
//			logger.LogException(e.getMessage(), e);
//		} finally {
//			logger.logInfo("btnSaveAction ends ...");
//		}
		return "success";
	}
	
	public String btnCancelAction() {
		return "success";
	}

	public HtmlInputText getTxtFinancialAccountNumber() {
		return txtFinancialAccountNumber;
	}

	public void setTxtFinancialAccountNumber(HtmlInputText txtFinancialAccountNumber) {
		this.txtFinancialAccountNumber = txtFinancialAccountNumber;
	}
	
	
	

}
