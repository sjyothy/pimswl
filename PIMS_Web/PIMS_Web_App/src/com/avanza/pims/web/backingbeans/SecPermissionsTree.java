package com.avanza.pims.web.backingbeans;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;

import org.apache.myfaces.custom.tree2.TreeNode;
import org.apache.myfaces.custom.tree2.TreeNodeBase;
import org.hibernate.criterion.Restrictions;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.jsf.appbase.dao.security.GroupManager;
import com.avanza.core.security.Permission;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.PermissionType;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupPermissionBinding;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.JSFUtil;




/**
 * @author Nauman Bashir
 * @version 1.0
 */
public class SecPermissionsTree extends AbstractController{
    
    /**
     * 
     */
    
    private static final long serialVersionUID = 1L;
    
    Search search;
    
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false; 
    private List<String> permissionList;
    
    private boolean checkAdd;
    private TreeNode permissionsTree ;    
    private UserGroupDbImpl currentUserGroup;
    private HashMap<String, PermissionBinding> currentUserGroupPermissions;
    private String userName = "";
    
    private String userGroupId = "";
    private String USERNAME = "";
    
    private String LOGINID = "";
    
    
    private String primaryName = "";
    
    private String secondaryName = "";
    
    private List<String> errorMessages = new ArrayList<String>();
    
    private PermissionBinding permissionBinding;

    private GroupManager groupManager = new GroupManager();
    private HtmlCommandButton cmdSavePerm;
    private HtmlInputHidden hdnGrpId = new HtmlInputHidden();


    public SecPermissionsTree() {
   
       
    }
    
    public void init()
    {
        String groupId = "Admin Group";      
        
        if(!this.isPostBack())
            groupId = JSFUtil.getRequestParamAsString("View_Permission");
        else
            groupId = JSFUtil.getValueAsString(hdnGrpId);
              
        if(StringHelper.isNotEmpty(groupId))
        {    
            try 
            {            	
            	currentUserGroup =  (UserGroupDbImpl) SecurityManager.getGroup(groupId);
            	List<PermissionBinding> permissionBindings = currentUserGroup.getPermissions();    
            	
            	if(permissionBindings != null && permissionBindings.size() >0)
            	{
            		currentUserGroupPermissions = new HashMap<String, PermissionBinding>(0);
            		
	            	for(PermissionBinding pb: permissionBindings)
	            	{
	            		if(currentUserGroupPermissions.containsKey(pb.getLinkPermission().getPermissionId()))
	            			currentUserGroupPermissions.put(pb.getLinkPermission().getPermissionId(), pb);
	            	}
            	}
            	
            	BuildPermissionTree();
         
            } catch (Exception e) 
            {
               e.printStackTrace();
            }
        }               
     }  
    
    
    private void BuildPermissionTree()
    {
    	List<Permission> secPermissions = SecurityManager.findPermission(null);   
        if(secPermissions != null && secPermissions.size() > 0)
        {                	
        	Permission rootPermission = getRootPermission(secPermissions); 
        	permissionsTree = new SecPermissions(rootPermission, "None");                
            setPermissionsTree(rootPermission.getPermissions(), (TreeNodeBase)permissionsTree);
        }
    }
    
    private Permission getRootPermission(List<Permission> secPermissions)
    {
    	Permission rootPermission = null;
    	for(Permission permission: secPermissions)
    	{
    		if (permission.getType() == PermissionType.None && permission.getParent() == null) 
            {
    			rootPermission = permission;
    			break;
            }
            
    	}
    	return rootPermission;
    }
    
    public void preprocess()
    {
        String groupId = JSFUtil.getValueAsString(hdnGrpId);
        
//        if(StringHelper.isNotEmpty(groupId))
//        {
//       
//            
//            hdnGrpId.setValue(groupId);
//            currentUserGroup = groupManager.findById( UserGroupDbImpl.class, groupId.toString() ) ;
//            
//            PermissionBinding permBind = groupManager.PrepareBindings();
//            if (currentUserGroup.getPermissions().size() > 0)
//                copyListValues(permBind, currentUserGroup.getPermissions());
//            setPermissionsTree(permBind.getPermissions(), null);
//       
//        }
        
    }
    
    
    public UserGroup getCurrentUserGroup() {
        return currentUserGroup;
    }
    
    public void setCurrentUserGroup(UserGroupDbImpl currentUserGroup) {
        this.currentUserGroup = currentUserGroup;
    }
    
    public String getUserName() {
        return (userName);
    }
    
    public void setUserName(String userName) {
        USERNAME = userName;
        this.userName = userName;
    }
    
    public String getUserGroupId() {
        return (userGroupId);
    }
    
    public void setUserGroupId(String userGroupId) {
        LOGINID = userGroupId;
        this.userGroupId = userGroupId;
    }
    
 
    public String getPrimaryName() {
        return primaryName;
    }
    
    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }
    
    public String getSecondaryName() {
        return secondaryName;
    }
    
    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }
    
    public String getErrorMessages() {
        String messageList;
        if ((errorMessages == null) || (errorMessages.size() == 0)) {
            messageList = "";
        }
        else {
            messageList = "<FONT COLOR=RED><B><UL>\n";
            for (String message : errorMessages) {
                messageList = messageList + "<LI>" + message + "\n";
            }
            messageList = messageList + "</UL></B></FONT>\n";
        }
        return (messageList);
    }
    
    private void MakePermissionsList(List list, List<PermissionBinding> list2, Boolean add) {
//        SecPermissions perm;
//        
//        for (Object obj : list) {
//            perm = (SecPermissions) obj;
//            PermissionBinding permissionBinding = perm.permissionBinding;
//            if (permissionBinding.getType() == PermissionType.None) {
//                if (getCanViewPermission(perm))
//                    list2.add(permissionBinding);
//                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
//                    currentUserGroup.removePermission(permissionBinding);
//                
//                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
//            }
//            else if (permissionBinding.getType() == PermissionType.Module) {
//                if (getCanViewPermission(perm))
//                    list2.add(permissionBinding);
//                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
//                    currentUserGroup.removePermission(permissionBinding);
//                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
//            }
//           // else if (permissionBinding.getType() == PermissionType.Action) {
//           //     if (getCanViewPermission(perm))
//           //         list2.add(permissionBinding);
//           //     else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
//            //        currentUserGroup.removePermission(permissionBinding);
//                
//           // }
//            else if (permissionBinding.getType() == PermissionType.Transaction) {
//                if (getCanViewPermission(perm))
//                    list2.add(permissionBinding);
//                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
//                    currentUserGroup.removePermission(permissionBinding);
//                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
//            }
//            
//        }
        
    }
    
    private Boolean isAddedPermission(String permissionId) {
        Iterator<GroupPermissionBinding> iterator = currentUserGroup.getGroupPermissionBindings().iterator();
        while (iterator.hasNext()) {
            GroupPermissionBinding groupPermissionBinding =  iterator.next();
            if (groupPermissionBinding.getPermissionId().equals(permissionId)) { return true; }
        }
        return false;
    }
    
    private Boolean getCanViewPermission(SecPermissions permissions) {
        if (permissions.isCanCreate() || permissions.isCanDelete() || permissions.isCanUpdate() || permissions.isCanView())
            return true;
        else
            return false;
    }
    
  
    private void copyListValues(PermissionBinding permBind, List<PermissionBinding> specificList) {
        specificList.get(0).selectBindings(permBind);
    }
    
  

    public void setCmdSavePerm(HtmlCommandButton commandButton1) {
        this.cmdSavePerm = commandButton1;
    }

    public HtmlCommandButton getCmdSavePerm() {
        return cmdSavePerm;
    }

    public void setHdnGrpId(HtmlInputHidden inputHidden1) {
        this.hdnGrpId = inputHidden1;
    }

    public HtmlInputHidden getHdnGrpId() {
        return hdnGrpId;
    }

    public String cmdSavePerm_action() {
        System.out.println("On Permission");
        List<PermissionBinding> permissionList = new ArrayList<PermissionBinding>();
        List list = new ArrayList();
        list.add(permissionsTree);
        
        MakePermissionsList(list, permissionList, false);
        currentUserGroup.addPermission(permissionList);
      
        groupManager.updatePermissions(currentUserGroup);
        return "success";
    }

    
    
    /*
     * public void setTreeData(List<PermissionBinding> list, DefaultMutableTreeNode treeNodeBase) { for (PermissionBinding root : list) { if
     * (root.getType() == PermissionType.None) { treeData = new DefaultMutableTreeNode(new Permissions(root .getPermissionId(), root,
     * root.getCanCreate(), root .getCanUpdate(), root.getCanDelete(), "foo-folder")); moduleId.add(root.getCanCreate());
     * setTreeData(root.getPermissions(), null); } else if (root.getType() == PermissionType.Module) { DefaultMutableTreeNode personNode = new
     * DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(), root.getCanUpdate(), root .getCanDelete(),
     * "person")); moduleId.add(root.getCanCreate()); setTreeData(root.getPermissions(), personNode); if (treeNodeBase != null)
     * treeNodeBase.insert(personNode); else treeData.insert(personNode); } else if (root.getType() == PermissionType.Transaction) {
     * DefaultMutableTreeNode folderNode = new DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(),
     * root.getCanUpdate(), root .getCanDelete(), "bar-folder")); moduleId.add(root.getCanCreate()); setTreeData(root.getPermissions(), folderNode);
     * if (treeNodeBase != null) treeNodeBase.insert(folderNode); } else if (root.getType() == PermissionType.Action) { DefaultMutableTreeNode
     * documentNode = new DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(), root.getCanUpdate(), root
     * .getCanDelete(), "document")); moduleId.add(root.getCanCreate()); if (treeNodeBase != null) treeNodeBase.insert(documentNode); } } }
     */

    /**
     * NOTE: This is just to show an alternative way of supplying tree data. You can supply either a TreeModel or TreeNode.
     * 
     * @return TreeModel
     */
    /*
     * public org.apache.myfaces.custom.tree2.TreeModel getExpandedTreeData() { return new TreeModelBase( ((org.apache.myfaces.custom.tree2.TreeNode)
     * getTreeDate1())); }
     */

    public org.apache.myfaces.custom.tree2.TreeNode getPermissionsTree() 
    {
        
        return permissionsTree;
    }
    
    public void setPermissionsTree(List<Permission> list, TreeNodeBase treeNodeBase) {
       
        for (Permission permission : list)
        {	            
           if (permission.getType() == PermissionType.Module) 
            {
                TreeNodeBase moduleNode =new SecPermissions(permission, "Module");
                setPermissionsTree(permission.getPermissions(), moduleNode);
                
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(moduleNode);              
                
            }
            else if (permission.getType() == PermissionType.Transaction) 
            {
                TreeNodeBase transactionNode =new SecPermissions(permission,"Transaction");
                setPermissionsTree(permission.getPermissions(), transactionNode);
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(transactionNode);
                
            }
            else if (permission.getType() == PermissionType.Action) 
            {
                TreeNodeBase actionNode = new SecPermissions(permission,  "Action");
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(actionNode);
                
            }            
        }
        
    }
    
    private Boolean checkPermissionGroupRelation(PermissionBinding permissionBinding, UserGroupDbImpl userGroup) {
        
        if(userGroup==null)
            return  false;
        
        for (GroupPermissionBinding groupPermissionBinding : userGroup.getGroupPermissionBindings()) {
            if (groupPermissionBinding.getPermissionId().equals(permissionBinding.getPermissionId())) { return true; }
        }
        return false;
    }
        
    
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
    
    private String getLocalWisePermissionName(Permission permission) {
        
        if (getIsEnglishLocale()) {
        	return permission.getPrimaryName();
        }
        
        else {
            return permission.getSecondaryName();
        }
    }
    
    private String getLocalWiseGroupName(UserGroup userGroup) {
        
        if (getIsEnglishLocale()) {
            return userGroup.getPrimaryName();
        }
        else {
            return userGroup.getSecondaryName();
        }
    }
    
    public boolean isCheckAdd() {
        return checkAdd;
    }
    
    public void setCheckAdd(boolean checkAdd) {
        this.checkAdd = checkAdd;
    }
    
    public boolean getCheckAdd() {
        return this.checkAdd;
    }
    
    public List getPermissionList() {
        return permissionList;
    }
    
    public void setPermissionList(List permissionList) {
        this.permissionList = permissionList;
    }
    
    public Boolean getCanCreate() {
        return this.permissionBinding.getCanCreate();
    }
    
    public Boolean getCanDelete() {
        return this.permissionBinding.getCanDelete();
    }
    
    public Boolean getCanUpdate() {
        return this.permissionBinding.getCanUpdate();
    }
    
   
    
    
    
    
public class SecPermissions extends TreeNodeBase implements Serializable 
{
        
        private String permissionName;    
        private String permissionId ;
        private Permission permission;        
        private boolean canCreate;        
        private boolean linkCanCreate;        
        private boolean canUpdate;        
        private boolean linkCanUpdate;        
        private boolean canDelete;        
        private boolean linkCanDelete;        
        private boolean canView;
        
        
        public SecPermissions(Permission permission, String node) 
        {	
            super(node, getLocalWisePermissionName(permission),permission.getPermissionId(), false);
            
            permissionName =  getLocalWisePermissionName(permission);
        	permissionId = permission.getPermissionId();
        	this.permission = permission;
            canCreate = permission.getCanCreate();            
            canDelete = permission.getCanDelete();
            canUpdate = permission.getCanUpdate();
            
        }


		public String getPermissionName() {
			return permissionName;
		}


		public void setPermissionName(String permissionName) {
			this.permissionName = permissionName;
		}


		public String getPermissionId() {
			return permissionId;
		}


		public void setPermissionId(String permissionId) {
			this.permissionId = permissionId;
		}


		public Permission getPermission() {
			return permission;
		}


		public void setPermission(Permission permission) {
			this.permission = permission;
		}


		public boolean isCanCreate() {
			return canCreate;
		}


		public void setCanCreate(boolean canCreate) {
			this.canCreate = canCreate;
		}


		public boolean isLinkCanCreate() {
			return linkCanCreate;
		}


		public void setLinkCanCreate(boolean linkCanCreate) {
			this.linkCanCreate = linkCanCreate;
		}


		public boolean isCanUpdate() {
			return canUpdate;
		}


		public void setCanUpdate(boolean canUpdate) {
			this.canUpdate = canUpdate;
		}


		public boolean isLinkCanUpdate() {
			return linkCanUpdate;
		}


		public void setLinkCanUpdate(boolean linkCanUpdate) {
			this.linkCanUpdate = linkCanUpdate;
		}


		public boolean isCanDelete() {
			return canDelete;
		}


		public void setCanDelete(boolean canDelete) {
			this.canDelete = canDelete;
		}


		public boolean isLinkCanDelete() {
			return linkCanDelete;
		}


		public void setLinkCanDelete(boolean linkCanDelete) {
			this.linkCanDelete = linkCanDelete;
		}


		public boolean isCanView() {
			return canView;
		}


		public void setCanView(boolean canView) {
			this.canView = canView;
		}  

        
    }   
}
