package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class ContractListCriteriaBackingBean extends AbstractController {
	
	
	private String dateFormat;
	private String unitRefNum;
	private String contractType;
	private String contractStatus;
	private String contractNumber;
	private String startDate;
	private String endDate;
	private String tenantNumber;
	private String tenantName;
	private String tenantType;
	private String propertyComercialName;
	private String propertyEndowedName;
	private String propertyUsage;
	private String propertyType;
	private String countries;
	private String states;
	private String city;
	private String street;
	private String fromContractDate;
	private String toContractDate;
	private String fromContractStartDate;
	private String toContractStartDate;
	private String fromContractExpDate;
	private String toContractExpDate;
	private String userName;
	private String endowedNo;
	private Double fromRentAmount;
	private Double toRentAmount;
	private String hdnTenantId;
	private String unitCostCenter;
	private String unitDesc;
	private ContractView contractView = new ContractView();
	CommonUtil commonUtil = new CommonUtil();
	private String allContractStatus;
	private List<ContractView> contractList;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	List<SelectItem> statusList = new ArrayList<SelectItem> ();
	List<SelectItem> typeList = new ArrayList<SelectItem> ();
	List<SelectItem> tenantTypeList = new ArrayList<SelectItem> ();
	List<SelectItem> propertyTypeList = new ArrayList<SelectItem> ();
	List<SelectItem> propertyUsageList = new ArrayList<SelectItem> ();
	List<SelectItem> countryList = new ArrayList<SelectItem> ();
	List<SelectItem> stateList = new ArrayList<SelectItem> ();
	List<SelectItem> cityList = new ArrayList<SelectItem> ();
	private Boolean printedBeforeActive = false;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String TENANT_INFO="TENANTINFO";
	
	
	
	 private HtmlDataTable dataTable;
	 
	 public Date fromContractDateRich;
	 public Date toContractDateRich;
	 public Date fromContractExpDateRich;
	 public Date toContractExpDateRich;
	 public Date fromContractStartDateRich;
	 public Date toContractStartDateRich;
	
	 private static Logger logger = Logger.getLogger(ContractListCriteriaBackingBean.class);
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 HttpServletRequest requestParam = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	 Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() {
		
		logger.logInfo("Stepped into the init method");
		// TODO Auto-generated method stub	
		try{
		super.init();	
		this.getStates();
		if (!isPostBack()){
			
			
			
			
			getIsEnglishLocale();
			getIsArabicLocale();
			unitRefNum = "";

			
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList",new ArrayList<SelectItem>());
			
			loadCombos();
		if(requestParam.getParameter("clStatus")!=null){
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("clStatus",requestParam.getParameter("clStatus"));
			requestParam.removeAttribute("clStatus");
		}
	 		
			
		}
		}catch(Exception e){
			logger.logError("init crashed due to:"+e.getStackTrace());
		}
		logger.logInfo("init method completed Succesfully");
	}
	
	
	public String getDateFormat()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	@SuppressWarnings( "unchecked" )
	private void loadCombos()
	{
	
		String METHOD_NAME = "loadCombos()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		// TODO Auto-generated method stub	
		try{
		
		List<DomainDataView> domainDataListStatus =loadContractStatus();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.CONTRACT_STATUS, domainDataListStatus);
		
		List<DomainDataView> domainDataListTypes = loadContractTypes();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.CONTRACT_TYPE, domainDataListTypes);

		List<DomainDataView> domainDataTenantListTypes = loadTenantType();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.TENANT_TYPE, domainDataTenantListTypes);
		
		List<DomainDataView> domainDataPropertyTypes = loadPropertyType();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.PROPERTY_TYPE, domainDataPropertyTypes);
		
		List<DomainDataView> domainDataPropertyUsages = loadPropertyUsage();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.PROPERTY_USAGE, domainDataPropertyUsages);
		
//		List<RegionView> reginCountries = loadCountry();
//		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.SESSION_COUNTRY, reginCountries);
		
		loadCountry();
		
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
	}
	
	public void processValueChange(ValueChangeEvent vce)
	{

		
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
			}	
			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
			for(int i=0;i<regionViewList.size();i++)
			{
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      this.getStateList().add(item);
			 }
			
			}
		    catch (Exception e)
			{
				logger.LogException( "processValueChange|Error Occured:", e );
				
			}
		}
		
		
	
	
	
	  public void loadCountry()  {
			
			
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
			      this.getCountryList().add(item);
			     
			  }
			Collections.sort(countryList,ListComparator.LIST_COMPARE);
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("countryList", countryList);
			 
			}catch (Exception e){
				logger.LogException("Exception Occured - In loadCountry()",e);
				
			}
		}
	 
	 public void loadState(ValueChangeEvent event)
	 {
		try
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			Long selectedCountry = new Long((String) event.getNewValue());
			if(selectedCountry != -1)
			{
				
				setStateList(new ArrayList());
				setStates("");
				setCityList(new ArrayList());
				setCity("");
				setStateList(getStateList(selectedCountry));
				 			
			}

		
			 
		}catch (Exception e){
			
			logger.logDebug("Error Occuired - In loadCountry() of Contract search List");
			
		}
		}
	 
	 public void loadCities(ValueChangeEvent event)
	 {
		try
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			Long selectedState = new Long((String) event.getNewValue());
			if(selectedState != -1)
			{
				setCityList(new ArrayList());
				setCity("");
				setCityList(getCityList(selectedState));
				 			
			}

		
			 
		}catch (Exception e){
			
			logger.logDebug("Error Occuired - In loadCountry() of Contract search List");
			
		}
		}
	
	
	 
	 
	public List<DomainDataView> loadContractStatus() {
		
		String METHOD_NAME = "loadContractStatus()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.CONTRACT_STATUS);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}
	
public List<DomainDataView> loadPropertyType() {
		
		String METHOD_NAME = "loadPropertyType()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_TYPE);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}
	

	public List<DomainDataView> loadPropertyUsage() {
	
	String METHOD_NAME = "loadPropertyUsage()";
	logger.logInfo("Stepped into the "+METHOD_NAME+" method");
	
	List<DomainDataView> domainDataList =null;
	try {
		
		domainDataList = new ArrayList<DomainDataView>();
		domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);
		
	//	domainDataList.get(0);
		
	} catch (PimsBusinessException e) {
		// TODO Auto-generated catch block
		logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
	}
	logger.logInfo(METHOD_NAME+" method completed Succesfully");
	 return domainDataList;
	}
	
public List<DomainDataView> loadTenantType() {
		
		String METHOD_NAME = "loadContractStatus()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.TENANT_TYPE);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}
	
public List<DomainDataView> loadContractTypes() {
	
		String METHOD_NAME = "loadContractTypes()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.CONTRACT_TYPE);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}
   

    public List<SelectItem> getStatusList(){
    	
    	String METHOD_NAME = "getStatusList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {
                statusList.clear();
                
                List<DomainDataView> statusComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.CONTRACT_STATUS);
                for(int i=0;i< statusComboList.size();i++)
                {
                	DomainDataView ddv=(DomainDataView)statusComboList.get(i);
                	SelectItem item=null;
                	if (isEnglishLocale)
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
                	else
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
                	
                	
                statusList.add(item);
             
                }
                Collections.sort(statusList,ListComparator.LIST_COMPARE);  
			}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return statusList;
          }
    
    public List<SelectItem> getTypeList()

    {
    	String METHOD_NAME = "getTypeList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {

          typeList.clear();
          
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.CONTRACT_TYPE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


              typeList.add(item);
              
          }
          Collections.sort(typeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
          return typeList;

    }

	public boolean getIsArabicLocale()
	{
		
		
		isArabicLocale = !getIsEnglishLocale();
		
		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public String btnCmdLink_Click()
	{
		String eventOutCome="failure";
		ContractView contractViewRow=(ContractView)dataTable.getRowData();
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 //setRequestParam("contractId", contractView.getContractId());
		facesContext.getExternalContext().getSessionMap().put("contractId", contractViewRow.getContractId());
		String contractId=facesContext.getExternalContext().getSessionMap().get("contractId").toString();
		eventOutCome="success";
		return eventOutCome;
	}
	@SuppressWarnings("unchecked")	
	public String searchContracts(){
		
		String METHOD_NAME = "searchContracts()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
	boolean b = getIsArabicLocale();
	System.out.println(b);
		
		try {
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
			contractView.setContractNumber(contractNumber);
			contractView.setContractTypeEn(contractType);
			
					
			HashMap dateHashMap = new HashMap();
			HashMap contractHashMap = new HashMap();
			
			
			clearSessionMap();
			DateFormat dfForRich = new SimpleDateFormat("dd/MM/yyyy");
			
			sessionMap.put("contractNumber", contractNumber);
			sessionMap.put("contractType", contractType);
			
				if(fromContractDateRich!=null )
				{
				
					dateHashMap.put("fromContractDate", dfForRich.format(fromContractDateRich));
					contractHashMap.put("fromContractDate", dfForRich.format(fromContractDateRich));
					sessionMap.put("fromContractDate", fromContractDateRich);
				
				}
				
				if(toContractDateRich!=null )
				{
				
					dateHashMap.put("toContractDate", dfForRich.format(toContractDateRich));
					contractHashMap.put("toContractDate", dfForRich.format(toContractDateRich));
					sessionMap.put("toContractDate", toContractDateRich);
				
				}
				if(fromContractExpDateRich!=null )
				{
				
					dateHashMap.put("fromContractExpDate",  dfForRich.format(fromContractExpDateRich));
					contractHashMap.put("fromContractExpDate", dfForRich.format(fromContractExpDateRich));
					sessionMap.put("fromContractExpDateRich", fromContractExpDateRich);
				
				}
				if(toContractExpDateRich!=null )
				{
				
					dateHashMap.put("toContractExpDate", dfForRich.format(toContractExpDateRich));
					contractHashMap.put("toContractExpDate", dfForRich.format(toContractExpDateRich));
					sessionMap.put("toContractExpDateRich", toContractExpDateRich);
				
				}
				if(fromContractStartDateRich!=null )
				{
				
					dateHashMap.put("fromContractStartDate",  dfForRich.format(fromContractStartDateRich));
					contractHashMap.put("fromContractStartDate", dfForRich.format(fromContractStartDateRich));
					sessionMap.put("fromContractStartDate", fromContractStartDateRich);
					
				}
				if(toContractStartDateRich!=null )
				{
				
					dateHashMap.put("toContractStartDate", dfForRich.format(toContractStartDateRich));
					contractHashMap.put("toContractStartDate", dfForRich.format(toContractStartDateRich));
					sessionMap.put("toContractStartDate", toContractStartDateRich);
								
				}
				
				if(userName != null && !userName.equals(""))
				{
					contractHashMap.put("userName", userName);
					sessionMap.put("userName", userName);
				}
				if(endowedNo != null && !endowedNo.equals(""))
				{
					contractHashMap.put("endowedNo", endowedNo);
					sessionMap.put("endowedNo", endowedNo);
				}
				
			
			if(startDate!=null && !startDate.equals(""))
			{
			
				Date date =new Date();
			
				date = df.parse(startDate);
			
				contractView.setStartDate(date);
			}
			
			if(endDate!=null && !endDate.equals(""))
			{
			
				Date date =new Date();
			
				date = df.parse(endDate);
			
				contractView.setEndDate(date);
			}
			if(contractStatus!=null && !contractStatus.equals("") && !contractStatus.equals("-1"))
			{
				contractView.setStatus(new Long(contractStatus));
				sessionMap.put("contractStatus", contractStatus);
				
				
			}
			
			if(contractType!=null && !contractType.equals("") && !contractType.equals("-1"))
			{
				contractView.setContractTypeId(new Long(contractType));
				sessionMap.put("contractType", contractType);
				
			}
			if(tenantNumber!=null && !tenantNumber.equals(""))
			{
			
				contractHashMap.put("tenantNumber", tenantNumber);
				sessionMap.put("tenantNumber", tenantNumber);
			
			}
			if(tenantName!=null && !tenantName.equals(""))
			{
			
				contractHashMap.put("tenantName", tenantName);
				sessionMap.put("tenantName", tenantName);
			
			}
			if(tenantType != null && !tenantType.equals("-1"))
			{
			
				contractHashMap.put("tenantTypeId", tenantType);
				sessionMap.put("tenantType", tenantType);
			
			}
			if(propertyComercialName!=null && !propertyComercialName.equals(""))
			{
			
				contractHashMap.put("propertyComercialName", propertyComercialName);
				sessionMap.put("propertyComercialName", propertyComercialName);
			
			}
			if(propertyEndowedName !=null && !propertyEndowedName.equals(""))
			{
			
				contractHashMap.put("propertyEndowedName", propertyEndowedName);
				sessionMap.put("propertyEndowedName", propertyEndowedName);
			
			}
			if(propertyType != null && !propertyType.equals("-1"))
			{
			
				contractHashMap.put("propertyTypeId", propertyType);
				sessionMap.put("propertyType", propertyType);
			
			}
			if(propertyUsage != null && !propertyUsage.equals("-1"))
			{
			
				contractHashMap.put("propertyUsageId", propertyUsage);
				sessionMap.put("propertyUsage", propertyUsage);
			
			}
			if(fromRentAmount!=null && fromRentAmount.doubleValue() > 0)
			{
			
				contractHashMap.put("fromRentAmount", fromRentAmount.toString());
				sessionMap.put("fromRentAmount", fromRentAmount);
			
			}
			if(toRentAmount!=null && toRentAmount.doubleValue() > 0)
			{
			
				contractHashMap.put("toRentAmount", toRentAmount.toString());
				sessionMap.put("toRentAmount", toRentAmount);
			
			}
			if(countries != null && !countries.equals("-1"))
			{
			
				contractHashMap.put("countryId", countries);
				sessionMap.put("countries", countries);
			
			}
			if(states != null && !states.equals("-1"))
			{
			
				contractHashMap.put("stateId", states);
				sessionMap.put("states", states);
			
			}
			if(city != null && !city.equals("-1"))
			{
				contractHashMap.put("cityId", city);
				sessionMap.put("city", city);
			
			}
			
			if(street != null && !street.equals(""))
			{
				contractHashMap.put("street",street);
				sessionMap.put("street",street);
			}
			
			contractHashMap.put("contractView", contractView);
			contractHashMap.put("unitRefNum", unitRefNum);
			sessionMap.put("unitRefNum", unitRefNum);
			contractHashMap.put( "unitAccNo", unitCostCenter.trim() );
			sessionMap.put( "unitAccNo", unitCostCenter.trim() );
			contractHashMap.put( "unitDesc", unitDesc.trim() );
			sessionMap.put( "unitDesc", unitDesc.trim() );
			
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("unitNo", unitRefNum);
			contractHashMap.put("dateFormat", getDateFormat());
			
			//contractList = psa.getAllContracts(contractView, unitRefNum, dateHashMap,"dd-MMM-yyyy");
			contractList = psa.getAllContractsForContractList(contractHashMap);
			
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("contractList", contractList);
			recordSize = contractList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("contractList", contractList);
			
			if(contractList.isEmpty())
				clearSessionMap();
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		return "";
	}


	public String getUnitRefNum() {
		
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("unitNo"))
			unitRefNum = (String) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("unitNo");
					
		return unitRefNum;
	}


	public void setUnitRefNum(String unitRefNum) {
		this.unitRefNum = unitRefNum;
		
	}


	public ContractView getContractView() {
		return contractView;
	}


	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}


	public List<ContractView> getContractList() {
		
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("contractList"))
			contractList = (List<ContractView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("contractList");
		
		return contractList;
	}


	public void setContractList(List<ContractView> contractList) {
		this.contractList = contractList;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}


	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public String getContractStatus() {
		return contractStatus;
	}


	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}


	public String getContractNumber() {
		if(viewRootMap.get("ContractNumber")!=null)
			contractNumber = viewRootMap.get("ContractNumber").toString();
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
		if(this.contractNumber!=null)
			viewRootMap.put("ContractNumber", this.contractNumber);
			
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFromContractDate() {
		return fromContractDate;
	}

	public void setFromContractDate(String fromContractDate) {
		this.fromContractDate = fromContractDate;
	}

	public String getToContractDate() {
		return toContractDate;
	}

	public void setToContractDate(String toContractDate) {
		this.toContractDate = toContractDate;
	}

	public String getAllContractStatus() {
		return allContractStatus;
	}

	public void setAllContractStatus(String allContractStatus) {
		this.allContractStatus = allContractStatus;
	}
	
	public Long getClearanceLetterStatus() {
		Long clearanceLetterStatus = new Long(0);
		Object obj =ApplicationContext.getContext().get(WebContext.class).
						getAttribute(WebConstants.CONTRACT_STATUS_EXPIRED_ID);
		if(obj != null){
			clearanceLetterStatus = Long.parseLong(obj.toString());
		}
		else{			
			try {
				clearanceLetterStatus = getContractStatusExpiredId();
				ApplicationContext.getContext().get(WebContext.class).
					setAttribute(WebConstants.CONTRACT_STATUS_EXPIRED_ID, clearanceLetterStatus);
			} catch (PimsBusinessException e) {
				logger.LogException("getCLStatus crashed...", e);
			}
		}
		return clearanceLetterStatus;
	}
	

	private Long getContractStatusExpiredId() throws PimsBusinessException {
		Long cLExpiredId = new Long(0);
		HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
   		       WebConstants.CONTRACT_STATUS_TERMINATED);
  		if(hMap.containsKey("returnId")){
  			cLExpiredId = Long.parseLong(hMap.get("returnId").toString());
  			if(cLExpiredId.equals(null))
  				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
  		}
		return cLExpiredId;
	}
	
	public Long getNOLStatus() {
		Long nOLStatus = new Long(0);
		Object obj =ApplicationContext.getContext().get(WebContext.class).
						getAttribute(WebConstants.CONTRACT_STATUS_APPROVED_ID);
		if(obj != null){
			nOLStatus = Long.parseLong(obj.toString());
		}
		else{			
			try {
				nOLStatus = getNOLId();
				ApplicationContext.getContext().get(WebContext.class).
					setAttribute(WebConstants.CONTRACT_STATUS_APPROVED_ID, nOLStatus);
			} catch (PimsBusinessException e) {
				logger.LogException("getCLStatus crashed...", e);
			}
		}
		return nOLStatus;
	}
	


	private Long getNOLId() throws PimsBusinessException {
		Long nOLId = new Long(0);
		HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
   		       WebConstants.CONTRACT_STATUS_ACTIVE);
  		if(hMap.containsKey("returnId")){
  			nOLId = Long.parseLong(hMap.get("returnId").toString());
  			if(nOLId.equals(null))
  				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
  		}
		return nOLId;
	}

	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logInfo(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	logger.logInfo(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return hMap;
		
	}
	
    private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
				  ddvList.add(ddv);	
				}
				break;
			}
		}
    	
    	return ddvList;
    }

	

	
			

	
	
	
	public String issueCLCmdLink_Click()
	{
		String  METHOD_NAME = "issueCLCmdLink_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.ISSUE_CLEARANCE_LETTER;		
	}

	public String cmdLinkCancelContract_Click(){
		String  METHOD_NAME = "cmdLinkCancelContract_Click|";		
        logger.logDebug(METHOD_NAME+"started...");
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.CANCEL_CONTRACT;
	}
	
	public String cmdLinkSettleContract_Click(){
		String  METHOD_NAME = "cmdLinkSettleContract_Click|";		
        logger.logDebug(METHOD_NAME+"started...");
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
        	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SETTLE_PAGE_MODE","readOnly");
        	webContext = ApplicationContext.getContext().get(WebContext.class);
        	webContext.setAttribute("parentPage", "contractList");
        	logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		
		return WebConstants.ContractListNavigation.SETTLE_CONTRACT;
	}

	public String issueNOLCmdLink_Click(){
		String  METHOD_NAME = "issueNOLmdLink_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.ISSUE_NO_OBJECTION_LETTER;
	}
	
	public String contractViolationCmdLink_Click(){
		String  METHOD_NAME = "contractViolationCmdLink_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.CONTRACT_VIOLATION;
	}
	public String cmdChangeTenant_Click(){
		String  METHOD_NAME = "cmdChangeTenant_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{

        	ContractView contractView=(ContractView)dataTable.getRowData();
        	
        	PropertyServiceAgent psa=new PropertyServiceAgent();
        	RequestView requestView =new RequestView();
        	requestView.setContractView(contractView);//Id(contractView.getContractId());
        	RequestTypeView requestTypeView =new RequestTypeView();
        	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);
        	
        	List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null,null);
        	CommonUtil commonUtil =new CommonUtil();
        	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
        			                                    WebConstants.REQUEST_STATUS_COMPLETE);
        	//if there is some request for change tenant name against this 
        	//contract and its status is not completed then display that request
        	//otherwise create new screen
        	requestView=null;
        	for (RequestView requestView2 : requestViewList) {
				
        		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
        		{
        			requestView =requestView2;
        		   break;
        		}
			}
        	if(requestView!=null)
        	{
        		
        		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
        	}
        	else 
        	{
        	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
        	   putContractInRequestMap((ContractView)dataTable.getRowData());
        	}
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.CHANGE_TENANT_NAME;
	}
	
	public String cmdAmendLeaseContract_Click(){
		String  METHOD_NAME = "cmdAmendLeaseContract_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{

        	ContractView contractView=(ContractView)dataTable.getRowData();
        	CommonUtil commonUtil = new CommonUtil();
        	PropertyServiceAgent psa=new PropertyServiceAgent();
        	RequestServiceAgent rsa=new RequestServiceAgent();
        	RequestView requestView =new RequestView();
        	List<RequestView> requestViewList =new ArrayList<RequestView>(0);
        	//requestView.setContractView(contractView);//Id(contractView.getContractId());
        	RequestTypeView requestTypeView =new RequestTypeView();
        	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
        	RequestFieldDetailView rfdv=new RequestFieldDetailView();
        	RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);        	
        	requestViewList= 
        		              rsa.getRequestFromRequestField(contractView.getContractId().toString(), 
        		            		                         requestView.getRequestTypeId().toString(),
        		            		                         requestKeyView.getRequestKeyId().toString(),
        		            		                         null 
        		                                            );
        	
        	
        	
    		
        	//List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null);
        	
        	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
        			                                    WebConstants.REQUEST_STATUS_COMPLETE);
        	DomainDataView ddvRejected= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
					WebConstants.REQUEST_STATUS_REJECTED);
	        DomainDataView ddvCancelled= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
					WebConstants.REQUEST_STATUS_CANCELED);
        	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
        	//if there is some request for Amend against this 
        	//contract and its status is not completed/rejected/cancelled then display that request
        	//otherwise create new screen
        	requestView=null;
        	for (RequestView requestView2 : requestViewList) {
				
        		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0 &&
        		   requestView2.getStatusId().compareTo(ddvCancelled.getDomainDataId())!=0 &&
        		   requestView2.getStatusId().compareTo(ddvRejected.getDomainDataId())!=0
        		)
        		{
        			requestView =requestView2;
        		   break;
        		}
			}
        	if(requestView!=null)
        	{
        		
        		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
        		setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
        	}
        	else 
        	{
        	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
        	   putContractInRequestMap((ContractView)dataTable.getRowData());
        	   setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
        	}
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.AMEND_CONTRACT;
	}
	

	

	
	public String cmdTransferContract_Click(){
		String  METHOD_NAME = "cmdTransferContract_Click|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{
        	
        	setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
        	
        	
//        	ContractView contractView=(ContractView)dataTable.getRowData();
//        	
//        	PropertyServiceAgent psa=new PropertyServiceAgent();
//        	RequestView requestView =new RequestView();
//        	requestView.setContractView(contractView);//Id(contractView.getContractId());
//        	RequestTypeView requestTypeView =new RequestTypeView();
//        	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);
//        	
//        	List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null);
//        	CommonUtil commonUtil =new CommonUtil();
//        	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
//        			                                    WebConstants.REQUEST_STATUS_COMPLETE);
//        	//if there is some request for change tenant name against this 
//        	//contract and its status is not completed then display that request
//        	//otherwise create new screen
//        	requestView=null;
//        	for (RequestView requestView2 : requestViewList) {
//				
//        		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
//        		{
//        			requestView =requestView2;
//        		   break;
//        		}
//			}
//        	if(requestView!=null)
//        	{
//        		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
//        	}
//        	else 
//        	{
//        	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
//        	   putContractInRequestMap((ContractView)dataTable.getRowData());
//        	}
//        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
		return WebConstants.ContractListNavigation.TRANSFER_CONTRACT;
	}

	
	private void putContractInRequestMap(ContractView contractView) {
		String  METHOD_NAME = "putContractInSession|";		
        logger.logInfo(METHOD_NAME+"started...");
        try{
        	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}
	}

	public String getTenantNumber() {
		return tenantNumber;
	}

	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}

	
	public String getTenantName() {
		if(viewRootMap.containsKey("tenantName") && viewRootMap.get("tenantName")!=null)
			tenantName = viewRootMap.get("tenantName").toString();
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
		viewRootMap.put("tenantName", tenantName);
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public List<SelectItem> getTenantTypeList() {
		
		String METHOD_NAME = "getTenantTypeList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {

			tenantTypeList.clear();
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) commonUtil.getDomainDataListForDomainType(WebConstants.PERSON_KIND);
          for(int i=0;i< typeComboList.size();i++)
          {
        	 Long isCompany=0L; 
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
          	if(ddv.getDataValue().compareTo(WebConstants.PERSON_KIND_COMPANY)==0)
          		isCompany=1L;
        	if (isEnglishLocale)
        		item = new SelectItem(isCompany.toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(isCompany.toString(), ddv.getDataDescAr());


        	tenantTypeList.add(item);
          }
          Collections.sort(tenantTypeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
        		
		return tenantTypeList;
	}

	public void setTenantTypeList(List<SelectItem> tenantTypeList) {
		this.tenantTypeList = tenantTypeList;
	}

	public String getPropertyComercialName() {
		return propertyComercialName;
	}

	public void setPropertyComercialName(String propertyComercialName) {
		this.propertyComercialName = propertyComercialName;
	}

	public String getPropertyEndowedName() {
		return propertyEndowedName;
	}

	public void setPropertyEndowedName(String propertyEndowedName) {
		this.propertyEndowedName = propertyEndowedName;
	}

	public String getPropertyUsage() {
		return propertyUsage;
	}

	public void setPropertyUsage(String propertyUsage) {
		this.propertyUsage = propertyUsage;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public List<SelectItem> getPropertyTypeList() {
		
		String METHOD_NAME = "getPropertyTypeList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {

			propertyTypeList.clear();
          
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.PROPERTY_TYPE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


        	propertyTypeList.add(item);
          }
          Collections.sort(propertyTypeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		
		return propertyTypeList;
	}

	public void setPropertyTypeList(List<SelectItem> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}

	public List<SelectItem> getPropertyUsageList() {
		
		String METHOD_NAME = "getPropertyUsageList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {

			propertyUsageList.clear();
          
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.PROPERTY_USAGE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


        	propertyUsageList.add(item);
          }
          
          Collections.sort(propertyUsageList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
         

		
		return propertyUsageList;
	}

	public void setPropertyUsageList(List<SelectItem> propertyUsageList) {
		this.propertyUsageList = propertyUsageList;
	}

	public String getFromContractExpDate() {
		return fromContractExpDate;
	}

	public void setFromContractExpDate(String fromContractExpDate) {
		this.fromContractExpDate = fromContractExpDate;
	}

	public String getToContractExpDate() {
		return toContractExpDate;
	}

	public void setToContractExpDate(String toContractExpDate) {
		this.toContractExpDate = toContractExpDate;
	}

	public Double getFromRentAmount() {
		return fromRentAmount;
	}

	public void setFromRentAmount(Double fromRentAmount) {
		this.fromRentAmount = fromRentAmount;
	}

	public Double getToRentAmount() {
		return toRentAmount;
	}

	public void setToRentAmount(Double toRentAmount) {
		this.toRentAmount = toRentAmount;
	}

	public String getCountries() {
		return countries;
	}

	public void setCountries(String countries) {
		this.countries = countries;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
   @SuppressWarnings( "unchecked" )
	public List<SelectItem> getCountryList() {
		
		String METHOD_NAME = "getCountryList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {

			countryList.clear();
          
          List<RegionView> countryComboList=(ArrayList<RegionView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.SESSION_COUNTRY);
          
          for(int i=0;i< countryComboList.size();i++)
          {
        	  RegionView rV=(RegionView)countryComboList.get(i);
          	
          	SelectItem item=null;
        	
          	if (isEnglishLocale)
        		item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn().toString());
        	else
        		item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr().toString());


        	countryList.add(item);
          }
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");

		
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public List<SelectItem> getCityList() {
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}


	public Date getToContractExpDateRich() {
		return toContractExpDateRich;
	}


	public void setToContractExpDateRich(Date toContractExpDateRich) {
		this.toContractExpDateRich = toContractExpDateRich;
	}


	public Date getFromContractDateRich() {
		return fromContractDateRich;
	}


	public void setFromContractDateRich(Date fromContractDateRich) {
		this.fromContractDateRich = fromContractDateRich;
	}


	public Date getToContractDateRich() {
		return toContractDateRich;
	}


	public void setToContractDateRich(Date toContractDateRich) {
		this.toContractDateRich = toContractDateRich;
	}


	public Date getFromContractExpDateRich() {
		return fromContractExpDateRich;
	}


	public void setFromContractExpDateRich(Date fromContractExpDateRich) {
		this.fromContractExpDateRich = fromContractExpDateRich;
	}


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public String reset()
	{
		  unitRefNum = "";
		  contractType = "";
		  contractStatus = "";
		  contractNumber = "";
		  startDate = "";
		  endDate = "";
		  tenantNumber = "";
		  tenantName = "";
		  tenantType = "";
		  propertyComercialName = "";
		  propertyEndowedName = "";
		  propertyUsage = "";
		  propertyType = "";
		  countries = "";
		  states = "";
		  city = "";
		  street = "";
		  fromContractDate = "";
		  toContractDate = "";
		  fromContractExpDate = "";
		  toContractExpDate = "";
		  fromRentAmount = null;
		  toRentAmount = null;
		  
		  fromContractDateRich = null;
		  toContractDateRich = null;
		  fromContractExpDateRich = null;
		  toContractExpDateRich = null;
		  
		  setStateList(new ArrayList());
		  setCityList(new ArrayList());
			
		
		return "";
	}






	public String renewContract()
	{
		String methodName = "renewContract";
		logger.logInfo(methodName+"|Start");
		ContractView selectedContract = (ContractView) dataTable.getRowData();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try
		{
			RequestView requestView = null;
			Long lastContractId = psa.getLastContractIdByOldContractId(selectedContract.getContractId());
			if(lastContractId !=null)
    			requestView = new CommonUtil().getIncompleteRequestForRequestType(lastContractId ,WebConstants.REQUEST_TYPE_RENEW_CONTRACT);
			if(requestView !=null)
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
			else
			{
			    setRequestParam("contractId", selectedContract.getContractId());
				setRequestParam("suggestedRentAmount", 0.0);
				setRequestParam("oldRentValue", selectedContract.getRentAmount());
				setRequestParam("fromRenewList", "true");
			}
				logger.logInfo(methodName+"|Finish");
				return "renew";
		}
		catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured::",e);
		}
		return "";
	}

	private void retrieveSearchCriteria()
	{
		
		
		if(sessionMap.containsKey("contractNumber") && sessionMap.get("contractNumber") != null)
		{
			setContractNumber((String)sessionMap.get("contractNumber"));
			sessionMap.remove("contractNumber");
		}
		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{
			setContractType((String)sessionMap.get("contractType"));
			sessionMap.remove("contractType");
		}
		
		if(sessionMap.containsKey("fromContractDate") && sessionMap.get("fromContractDate") != null)
			{
				setFromContractDateRich((Date)sessionMap.get("fromContractDate"));
				sessionMap.remove("fromContractDate");
			}
		
		if(sessionMap.containsKey("toContractDate") && sessionMap.get("toContractDate") != null)
			{
				setToContractDateRich((Date)sessionMap.get("toContractDate"));
				sessionMap.remove("toContractDate");
			}

		
		if(sessionMap.containsKey("fromContractExpDateRich") && sessionMap.get("fromContractExpDateRich") != null)
			{
				setFromContractExpDateRich((Date)sessionMap.get("fromContractExpDateRich") );
				sessionMap.remove("fromContractExpDateRich");
			}
			
		
		if(sessionMap.containsKey("toContractExpDateRich") && sessionMap.get("toContractExpDateRich") != null)
			{
				setToContractExpDateRich((Date)sessionMap.get("toContractExpDateRich"));
				sessionMap.remove("toContractExpDateRich");
			}

		
		if(sessionMap.containsKey("contractStatus") && sessionMap.get("contractStatus") != null)
			{
				setContractStatus((String)sessionMap.get("contractStatus"));
				sessionMap.remove("contractStatus");
			}

		
		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
			{
				setContractType((String)sessionMap.get("contractType"));
				sessionMap.remove("contractType");
			}

		
		if(sessionMap.containsKey("tenantNumber") && sessionMap.get("tenantNumber") != null)
			{
				setTenantNumber((String)sessionMap.get("tenantNumber"));
				sessionMap.remove("tenantNumber");
			}
		
		if(sessionMap.containsKey("tenantName") && sessionMap.get("tenantName") != null)
			{
				setTenantName((String)sessionMap.get("tenantName"));
				sessionMap.remove("tenantName");
			}

		
		if(sessionMap.containsKey("tenantType") && sessionMap.get("tenantType") != null)
			{
				setTenantType((String)sessionMap.get("tenantType"));
				sessionMap.remove("tenantType");
			}

		
		if(sessionMap.containsKey("propertyComercialName") && sessionMap.get("propertyComercialName") != null)
			{
				setPropertyComercialName((String)sessionMap.get("tenantType"));
				sessionMap.remove("propertyComercialName");
			}

		
		if(sessionMap.containsKey("propertyEndowedName") && sessionMap.get("propertyEndowedName") != null)
			{
				setPropertyEndowedName((String)sessionMap.get("propertyEndowedName"));
				sessionMap.remove("propertyEndowedName");
			}

		
		if(sessionMap.containsKey("propertyType") && sessionMap.get("propertyType") != null)
			{
				setPropertyType((String)sessionMap.get("propertyType"));
				sessionMap.remove("propertyType");
			}

		
		if(sessionMap.containsKey("propertyUsage") && sessionMap.get("propertyUsage") != null)
			{
				setPropertyUsage((String)sessionMap.get("propertyUsage"));
				sessionMap.remove("propertyUsage");
			}
		
		if(sessionMap.containsKey("fromRentAmount") && sessionMap.get("fromRentAmount") != null)
			{
				setFromRentAmount((Double)sessionMap.get("fromRentAmount"));
				sessionMap.remove("fromRentAmount");
			}
			
		if(sessionMap.containsKey("toRentAmount") && sessionMap.get("toRentAmount") != null)
			{
				setToRentAmount((Double)sessionMap.get("toRentAmount"));
				sessionMap.remove("toRentAmount");
			}
				
		if(sessionMap.containsKey("countries") && sessionMap.get("countries") != null)
			{
				setCountries((String)sessionMap.get("countries"));
				sessionMap.remove("countries");
			}
					
		if(sessionMap.containsKey("states") && sessionMap.get("states") != null)
			{
				setStates((String)sessionMap.get("states"));
				sessionMap.remove("states");
			}
		if(sessionMap.containsKey("city") && sessionMap.get("city") != null)
		{
			setCity((String)sessionMap.get("city"));
			sessionMap.remove("city");
		}
		
		if(sessionMap.containsKey("unitRefNum") && sessionMap.get("unitRefNum") != null)
		{
			setUnitRefNum((String)sessionMap.get("unitRefNum"));
			sessionMap.remove("unitRefNum");
		}
		
		
		if(sessionMap.containsKey("fromContractStartDate") && sessionMap.get("fromContractStartDate") != null)
		{
			setFromContractStartDateRich((Date)sessionMap.get("fromContractStartDate"));
			sessionMap.remove("fromContractStartDate");
		}
	
		if(sessionMap.containsKey("toContractStartDate") && sessionMap.get("toContractStartDate") != null)
		{
			setToContractStartDateRich((Date)sessionMap.get("toContractStartDate"));
			sessionMap.remove("toContractStartDate");
		}
		
		if(sessionMap.containsKey("userName") && sessionMap.get("userName") != null)
		{
			setUserName((String)sessionMap.get("userName"));
			sessionMap.remove("userName");
		}
		
		if(sessionMap.containsKey("endowedNo") && sessionMap.get("endowedNo") != null)
		{
			setEndowedNo((String)sessionMap.get("endowedNo"));
			sessionMap.remove("endowedNo");
		}
		
		if(sessionMap.containsKey("street") && sessionMap.get("street") != null)
		{
			setStreet((String) sessionMap.get("street"));
			sessionMap.remove("street");
		}
		
		
		
/*		if(!this.countries.equals("-1"))
		{
			stateList=getStateList(new Long(countries));
			this.setStateList(stateList);
		}
		
		if(!this.states.equals("-1"))
		{
			cityList=getCityList(new Long(states));
			this.setCityList(cityList);
		}*/
		
		sessionMap.remove("preserveSearchCriteria");
		
		searchContracts();		
	}
	
	public List<SelectItem> getCityList(Long stateId) {
		
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			Set<RegionView> regionViewList = pSAgent.getCity(stateId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				cityList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("", e);
		}
		return cityList;
	}
	
	public List<SelectItem> getStateList(Long countryId) {
		
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			Set<RegionView> regionViewList = pSAgent.getState(countryId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				stateList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("", e);
		}
		return stateList;
	}
	
	public void clearSessionMap()
	{
		
		if(sessionMap.containsKey("unitRefNum") && sessionMap.get("unitRefNum") != null)
		{
			sessionMap.remove("unitRefNum");
		}
		
		if(sessionMap.containsKey("contractNumber") && sessionMap.get("contractNumber") != null)
		{
			sessionMap.remove("contractNumber");
		}
		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{
			sessionMap.remove("contractType");
		}
		if(sessionMap.containsKey("fromContractDate") && sessionMap.get("fromContractDate") != null)
		{
			sessionMap.remove("fromContractDate");
		}
	
		if(sessionMap.containsKey("toContractDate") && sessionMap.get("toContractDate") != null)
		{
			
			sessionMap.remove("toContractDate");
		}

	
	if(sessionMap.containsKey("fromContractExpDateRich") && sessionMap.get("fromContractExpDateRich") != null)
		{
			
			sessionMap.remove("fromContractExpDateRich");
		}
		
	
	if(sessionMap.containsKey("toContractExpDateRich") && sessionMap.get("toContractExpDateRich") != null)
		{
			
			sessionMap.remove("toContractExpDateRich");
		}

	
	if(sessionMap.containsKey("contractStatus") && sessionMap.get("contractStatus") != null)
		{
			
			sessionMap.remove("contractStatus");
		}

	
	if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{
			
			sessionMap.remove("contractType");
		}

	
	if(sessionMap.containsKey("tenantNumber") && sessionMap.get("tenantNumber") != null)
		{
			
			sessionMap.remove("tenantNumber");
		}
	
	if(sessionMap.containsKey("tenantName") && sessionMap.get("tenantName") != null)
		{
			
			sessionMap.remove("tenantName");
		}

	
	if(sessionMap.containsKey("tenantType") && sessionMap.get("tenantType") != null)
		{
			sessionMap.remove("tenantType");
		}

	
	if(sessionMap.containsKey("propertyComercialName") && sessionMap.get("propertyComercialName") != null)
		{
			
			sessionMap.remove("propertyComercialName");
		}

	
	if(sessionMap.containsKey("propertyEndowedName") && sessionMap.get("propertyEndowedName") != null)
		{
			
			sessionMap.remove("propertyEndowedName");
		}

	
	if(sessionMap.containsKey("propertyType") && sessionMap.get("propertyType") != null)
		{
			
			sessionMap.remove("propertyType");
		}

	
	if(sessionMap.containsKey("propertyUsage") && sessionMap.get("propertyUsage") != null)
		{
			
			sessionMap.remove("propertyUsage");
		}
	
	if(sessionMap.containsKey("fromRentAmount") && sessionMap.get("fromRentAmount") != null)
		{
			
			sessionMap.remove("fromRentAmount");
		}
		
	if(sessionMap.containsKey("toRentAmount") && sessionMap.get("toRentAmount") != null)
		{
			
			sessionMap.remove("toRentAmount");
		}
			
	if(sessionMap.containsKey("countries") && sessionMap.get("countries") != null)
		{
			
			sessionMap.remove("countries");
		}
				
	if(sessionMap.containsKey("states") && sessionMap.get("states") != null)
		{
			
			sessionMap.remove("states");
		}	
	if(sessionMap.containsKey("city") && sessionMap.get("city") != null)
	{
		sessionMap.remove("city");
	}
	
	
	if(sessionMap.containsKey("fromContractStartDate") && sessionMap.get("fromContractStartDate") != null)
	{
		sessionMap.remove("fromContractStartDate");
	}
	if(sessionMap.containsKey("toContractStartDate") && sessionMap.get("toContractStartDate") != null)
	{
		sessionMap.remove("toContractStartDate");
	}
	if(sessionMap.containsKey("userName") && sessionMap.get("userName") != null)
	{
		sessionMap.remove("userName");
	}
	if(sessionMap.containsKey("endowedNo") && sessionMap.get("endowedNo") != null)
	{
		sessionMap.remove("endowedNo");
	}
	
	if(sessionMap.containsKey("street") && sessionMap.get("street") != null)
	{
		sessionMap.remove("street");
	}	
	
	
	}
	
	public String getFromContractStartDate() {
		return fromContractStartDate;
	}


	public void setFromContractStartDate(String fromContractStartDate) {
		this.fromContractStartDate = fromContractStartDate;
	}


	public String getToContractStartDate() {
		return toContractStartDate;
	}


	public void setToContractStartDate(String toContractStartDate) {
		this.toContractStartDate = toContractStartDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEndowedNo() {
		return endowedNo;
	}


	public void setEndowedNo(String endowedNo) {
		this.endowedNo = endowedNo;
	}


	public Date getFromContractStartDateRich() {
		return fromContractStartDateRich;
	}


	public void setFromContractStartDateRich(Date fromContractStartDateRich) {
		this.fromContractStartDateRich = fromContractStartDateRich;
	}


	public Date getToContractStartDateRich() {
		return toContractStartDateRich;
	}


	public void setToContractStartDateRich(Date toContractStartDateRich) {
		this.toContractStartDateRich = toContractStartDateRich;
	}


	public String getUnitCostCenter() {
		return unitCostCenter;
	}


	public void setUnitCostCenter(String unitCostCenter) {
		this.unitCostCenter = unitCostCenter;
	}


	public String getUnitDesc() {
		return unitDesc;
	}


	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}


	public Boolean getPrintedBeforeActive() {
		return printedBeforeActive;
	}


	public void setPrintedBeforeActive(Boolean printedBeforeActive) {
		this.printedBeforeActive = printedBeforeActive;
	}
public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}
}

