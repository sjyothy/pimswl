package com.avanza.pims.web.backingbeans;


import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.avanza.core.util.Logger;
import com.avanza.pims.dao.AuctionManager;
import com.avanza.pims.entity.Auction;
import com.avanza.pims.entity.AuctionAdvertisement;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionAdvertisementView;
import com.avanza.ui.util.ResourceUtil;


public class AuctionAdvertisementBackingBean extends AbstractController
{
    /**
	 * 
	 */
	
	public AuctionAdvertisementBackingBean(){
		System.out.println("AuctionAdvertisementBackingBean Constructor");
	}
	transient Logger logger = Logger.getLogger(AuctionAdvertisementBackingBean.class);
	
	private static final long serialVersionUID = 1L;

	private String message;
	
	private AuctionAdvertisementView advertisementView;
	

	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
    public String save()
    {
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        auctionNumber="Cancel";        
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
        return "save";
    }
    

    public String cancel()
    {
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        auctionNumber="Cancel";        
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
        return "cancel";
    }
    
    public String complete()
    {
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        auctionNumber="Proceed";        
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
        return "complete";
    }

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	
}
