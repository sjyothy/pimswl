package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import com.avanza.core.util.Logger;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisburseSrcService;
import com.avanza.pims.ws.vo.mems.DisbursementSourceView;
import com.avanza.ui.util.ResourceUtil;


/** The class is a backing bean for SearchDibursementSrouces.js
 * @author Farhan M. Amin
 *
 */
@SuppressWarnings("unchecked")
public class SearchDisbursementSourceController extends AbstractController {
	
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(SearchDisbursementSourceController.class);
	
	private static final String PAGE_RES 				= "PAGE_RESULT";
	private static final String EDIT_ENTITY 			= "entity";
	private static final String DEFAULT_SORT_FIELD 		= "grpAccNo";
	private static final String ADD_SRC					= "add";
	private static final String EDIT_SRC				= "edit";
	
				  
	protected HtmlInputText sourceNameEn = new HtmlInputText();
	protected HtmlInputText sourceNameAr = new HtmlInputText();
	protected HtmlInputText grpNum = new HtmlInputText();
	protected HtmlInputText acctsPayCombCode = new HtmlInputText();
	protected HtmlDataTable searchResultsTable = new HtmlDataTable();

	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> messages = new ArrayList<String>();
	
	Map viewRootMap = getFacesContext().getViewRoot().getAttributes();	
	FacesContext context=FacesContext.getCurrentInstance();  

	@Override 	
	public void init() {
		
		if(!isPostBack()) {
			setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			setSortField(DEFAULT_SORT_FIELD);
			setSortItemListAscending(true);			
			setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);		
		}
				
	}
	

	/** The function is registered as an action listener of the Search button.
	 * The function calls the doSearchItemList() for the required user search.
	 * @see com.avanza.pims.web.controller.SearchDisbursementSourceController#doSearchItemList()
	 * @author Farhan Muhammad Amin
	 * 
	 */
	public void doSearch() {
		logger.logInfo("[doSearch Starts]");
		doSearchItemList();					
		logger.logInfo("[doSearch Ends]");				
	}
		
	/** The function overrides the Search. The function is called by the AbstractController
	 * whenever the user navigate results that are returned from the search. The flow of the
	 * function is as follows
	 * <p> The function creates a view object passing it the search parameters provided by user. </p>
	 * <p> The function gets the total count of the results that the search will return. <p>
	 * <p> The function sets the total rows and performs the pages calculations. </p>
	 * <p> the function passes the view object to the service which returns the list of items found based
	 * on the criteria. If no records are found the function adds an appropriate error message. If one or more
	 * records are found the functions updates the search results in the view root</p>
	 *  
	 * @see com.avanza.pims.web.controller.AbstractController#doSearchItemList()
	 * @author Farhan Muhammad Amin
	 */
	@Override
	public void doSearchItemList() {
					
		try {			
			DisburseSrcService service = new DisburseSrcService();			
			DisbursementSourceView view = new DisbursementSourceView();
			if( grpNum.getValue() != null && grpNum.getValue().toString().trim().length() > 0 )
			{
				view.setGrpNum(this.grpNum.getValue().toString());
			}
			view.setSrcNameEn(this.sourceNameEn.getValue().toString());
			view.setSrcNameAr(this.sourceNameAr.getValue().toString());
			if( acctsPayCombCode.getValue() != null && acctsPayCombCode.getValue().toString().trim().length() > 0 )
			{
				view.setAcctsPayCombCode( acctsPayCombCode.getValue().toString() );
			}
			int totalRows = service.getTotalDisbursementSourcesByCriteria(view);
			setTotalRows(totalRows);
			doPagingComputations();
						
			if ( 0 < totalRows ) {																						
				List<DisbursementSourceView> currentView = service.getDisbursementSourceByView(view, getRowsPerPage(), getCurrentPage(), 
						getSortField(), isSortItemListAscending());
			
				List<DisbursementSourceView> temp = getPageResult();
				temp.clear();
				temp= null;
				viewRootMap.remove(PAGE_RES);
				viewRootMap.put(PAGE_RES, currentView);

			} else {
				getPageResult().clear();
	        	errorMessages = new ArrayList<String>();	        	
				errorMessages.add(CommonUtil.getBundleMessage(
										WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));				
				this.setCurrentPage(0);
			}
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("doSearchLimit CRASHED",ex);
	        
			
		}
	}
	
	/** The function is registered as an action to edit the SEARCHED records. The function is 
	 * invoked when user wants to edit a particular record. The functions first get the 
	 * relevant records and puts it in the request attribute.
	 * <b> Appropriate navigation rules are defined in the faces-config.xml upon "edit" the
	 * user is navigated to ManageDisbursementSrc.jsp</b> ManageDisbursementSrc.jsp is opened in the
	 * EDIT mode with pre-populated with the desired dibursement source.
	 * @return
	 * 		The function returns "edit" when relevant records is found and is successfully 
	 * inserted into the request attributes else it returns an empty string.
	 */
	public String editDisbursementSrc() {
		
		DisbursementSourceView srcView = (DisbursementSourceView) searchResultsTable.getRowData();		
		if ( null != srcView ) {						
			getSessionMap().put(EDIT_ENTITY, srcView);
			return EDIT_SRC;
		}	
		return "";				
	}
	
	/** The function simply returns "add" and registered as an Action with Add button.
	 * <b> Please note appropriate navigation rules are defined for this page. When JSF Navigation
	 * Handler encounters "add" it REDIRECTS the user to ManageDisbursementSrc.jsp </b>
	 * @return
	 * 		The function returns "add" string which navigation handler uses for appropriate navigations.
	 */
	public String addDisbursementSource() {
		return ADD_SRC;
	}
	
	public void clearAll() {
		this.sourceNameAr.setValue("");
		this.sourceNameEn.setValue("");
		this.grpNum.setValue("");
		
	}
	
	/* accessors and mutators */

	public HtmlInputText getSourceNameEn() {
		return sourceNameEn;
	}

	public void setSourceNameEn(HtmlInputText sourceNameEn) {
		this.sourceNameEn = sourceNameEn;
	}

	public HtmlInputText getSourceNameAr() {
		return sourceNameAr;
	}

	public void setSourceNameAr(HtmlInputText sourceNameAr) {
		this.sourceNameAr = sourceNameAr;
	}

	public HtmlInputText getGrpNum() {
		return grpNum;
	}

	public void setGrpNum(HtmlInputText grpNum) {
		this.grpNum = grpNum;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;	
	}
	
	public String getMessages() {
		return CommonUtil.getErrorMessages(messages);
	}
	
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public HtmlDataTable getSearchResultsTable() {
		return searchResultsTable;
	}

	public void setSearchResultsTable(HtmlDataTable searchResultsTable) {
		this.searchResultsTable = searchResultsTable;
	}

	public List<DisbursementSourceView> getPageResult() {
		if (viewRootMap.containsKey(PAGE_RES)) {
			return (List<DisbursementSourceView>) viewRootMap.get(PAGE_RES);
		}
		return new ArrayList<DisbursementSourceView>();
	}


	public HtmlInputText getAcctsPayCombCode() {
		return acctsPayCombCode;
	}


	public void setAcctsPayCombCode(HtmlInputText acctsPayCombCode) {
		this.acctsPayCombCode = acctsPayCombCode;
	}
}
