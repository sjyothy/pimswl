package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.RequestDetailsReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.services.TaskServices;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TaskListSearchCriteria;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.ui.util.ResourceUtil;


public class RequestSearch extends AbstractController
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(RequestSearch.class);
	private TaskServices taskServices = new TaskServices();
	PropertyService propertyService = new PropertyService();
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	HttpServletRequest request;
	private Map<String,Long> requestStatusMap = new HashMap<String,Long>();
	private Map<String,Long> requestTypeMap = new HashMap<String,Long>();
	private RequestDetailsReportCriteria requestDetailsReportCriteria;
	
	private RequestView requestView = new RequestView();
	private Date toDate = null;
	private Date fromDate = null;
	static DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private String requestTypeId = "";
	private String statusId = "";
	
	
	
	private RequestView dataItem = new RequestView();
	private List<RequestView> dataList = new ArrayList<RequestView>();
	
	private HtmlCalendar htmlCalendarFromDate = new HtmlCalendar();
	private HtmlCalendar htmlCalendarToDate = new HtmlCalendar();
	
	private HtmlCalendar lastTrxDateFrom = new HtmlCalendar();
	private HtmlCalendar lastTrxDateTo = new HtmlCalendar();
	
	
	private HtmlSelectOneMenu requestTypeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu requestTypeModuleCombo= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu requestStatusCombo = new HtmlSelectOneMenu();
	
	List<RequestTypeView> requestTypeList = new ArrayList<RequestTypeView>();
	private List<SelectItem> requestStatuses = new ArrayList<SelectItem>();
	private List<SelectItem> requestTypes = new ArrayList<SelectItem>();
	
	private List<SelectItem> countries = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>();
	private List<SelectItem> cities = new ArrayList<SelectItem>();
	
	private Map<Long,RegionView> countryMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> stateMap = new HashMap<Long,RegionView>();
	
	private List<String> errorMessages;
	private String infoMessage = "";
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private Boolean sortAscending = true;
	

	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	
	
	// hashMap field From Shiraz 
	
	
	private String unitRefNo;
	private String inheritanceFileRefNo;
	private String fileOwner;
	private String propertyRefNo;
	private String masrafName;
	private String complaintNum;
	private String endowmentName;
	private String endowmentNum;
	private String endowmentFileName;
	private String endowmentFileNum;
	private String selectOnedonationCollectedBy;
	private String selectOneTenantType;
	private String selectOneDisbursementReasonId;
	private String selectOneDisbursementReasonTypeId;
	private String selectOneContractType;
	private String selectOneState;
	private String selectOneCity;
	private String selectOnePropertyUsage;
	private String  DEFAULT_SORT_FIELD = "requestNumber";
	private String applicantName;
	private String requestOrigination;
	private List<String>requestOriginationList;
	private String requestCreatedBy;
	private String requestProcessedBy;
	
	
	private String hdnTenantId;
	private String hdnApplicantId;
	
	
	
	public String getComplaintNum() {
		return complaintNum;
	}


	public void setComplaintNum(String complaintNum) {
		this.complaintNum = complaintNum;
	}


	@SuppressWarnings("unchecked")
	public String getHdnTenantId() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		if(viewMap.containsKey("hdnTenantId") && viewMap.get("hdnTenantId")!=null)
			hdnTenantId = viewMap.get("hdnTenantId").toString();
		return hdnTenantId;
		
	}


	public String getHdnApplicantId() {
		
		
		return hdnApplicantId;
	}

	@SuppressWarnings("unchecked")
	public void setHdnTenantId(String hdnTenantId) {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		this.hdnTenantId = hdnTenantId;
		if(this.hdnTenantId!=null)
			viewMap.put("hdnTenantId", this.hdnTenantId);
	}


	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}


	public String getApplicantName() {
		return applicantName;
	}


	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}


	public RequestSearch(){
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	

	/*
	 * Methods
	 */
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = "";
		if(user!=null)
			loginId = user.getLoginId();
		return loginId;
	}
	
	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			sessionMap.remove(WebConstants.REQUEST_TYPE_LIST);
			sessionMap.remove(WebConstants.REQUEST_TYPE_MAP);
			sessionMap.remove(WebConstants.REQUEST_STATUS_MAP);
			sessionMap.remove(WebConstants.REQUEST_TYPE_COMBO_EN);
			sessionMap.remove(WebConstants.REQUEST_TYPE_COMBO_AR);
			
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public String approvalRequiredAction(){
		String METHOD_NAME = "approvalRequiredAction()";
		logger.logInfo(METHOD_NAME + " started...");
		String taskType = "";
		try{
			
			clearSessionMap();
	   		RequestView reqView=(RequestView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		/*WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	   		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);*/

			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return taskType;
//		return "home";
	}
	
	@SuppressWarnings("unchecked")
	public String fetchHumanTask() 
	{		
		errorMessages = new ArrayList<String>();
		String returnValue="";
		try{
			
			returnValue = fetchSOATask();
			returnValue = fetchWfTask();
			
		}
		catch(Exception exp)
		{
			logger.LogException("Failed to fetchHumanTask", exp);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.generalError"));
		}
		return returnValue;
	}
	@SuppressWarnings("unchecked")
	public String fetchWfTask() 
	{		
		errorMessages = new ArrayList<String>();
		String requestType = "";
		RequestView reqView = (RequestView)dataTable.getRowData();
		List<TaskListVO> taskList;
		try
		{			
			setRequestParam(WebConstants.REQUEST_VIEW,reqView);
			TaskListSearchCriteria criteria = new TaskListSearchCriteria();
	   		criteria.setRequestNumber(reqView.getRequestNumber());
	   		if(reqView.getTaskAcquiredBy() != null && !reqView.getTaskAcquiredBy().equals("") && !getLoggedInUser().equals(reqView.getTaskAcquiredBy())){
	   			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.userNotAuthentic"));
	   			throw new PIMSWorkListException(ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK);
	   		}
	   		else{
	   			taskList = taskServices.getTaskList(criteria, 1, 1, getLoggedInUserId());
	   		}
	   		if(getLoggedInUser().equals(reqView.getTaskAcquiredBy())){
	   			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,taskList.get(0));
	   			
	   			return reqView.getRequestTypeId().toString();
	   		}
	   		else if(taskList.size() <= 0 ){
	   			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.noTaskFound"));
	   			throw new PIMSWorkListException(ExceptionCodes.FAILED_TO_GET_USER_TASKS);
	   		}
	   		else{
		   		for (TaskListVO taskListVO : taskList) {
		   		 
		   			WFClient wfClient = new WFClient();
		   			logger.logInfo("result : %s" + wfClient.claimTask(taskListVO.getTaskId(), getLoggedInUser()));
		   			
		   			reqView.setTaskAcquiredByNameEn(getLoggedInUser());
		   			reqView.setTaskAcquiredByNameAr(getLoggedInUser());
		   			setDataList(dataList);
		   			errorMessages.clear();
		   			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,taskListVO);
		   			infoMessage =  getBundleMessage("TaskList.messages.claimSuccess");
					
				}
	   		}
		   		
			}
			
			catch(Exception exp)
			{
				logger.LogException("Failed to fetch user task", exp);
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.generalError"));
			}
			
		return reqView.getRequestTypeId().toString();



	}

	@SuppressWarnings("unchecked")
	public String fetchSOATask() 
	{		
		String requestType = "";
		try
		{			
			RequestView reqView=(RequestView)dataTable.getRowData();
			Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);				
				UserTask userTask = bpmWorkListClient.getTaskForWFAdmin(taskId);         
				boolean hasPermission = true;
				try
				{
					if(userTask != null && userTask.getTaskAttributes()!=null && userTask.getTaskAttributes().containsKey("TASK_ASSIGNEE_GROUPS") )
					{
						hasPermission = false;					
						String groups = userTask.getTaskAttributes().get("TASK_ASSIGNEE_GROUPS");
						if(groups != null)
						{
							String[] taskGroups =  groups.split(",");
							
							if(taskGroups !=null && taskGroups.length >0 )
							{
								List<UserGroup> usereGroups =  getLoggedInUserObj().getUserGroups();
								
								if(usereGroups!=null && usereGroups.size() >0)
								{
									for(UserGroup ug:  usereGroups)
									{
										for(String taskGrp: taskGroups)
										{
											if(ug.getUserGroupId().trim().toLowerCase().equals(taskGrp.trim().toLowerCase()))
											{
												hasPermission = true;
												break;
											}
										}
									}
								}
							}
						}					
					}
				}
				catch(Exception exp)
				{
					logger.LogException("Failed to check task group level security", exp);
				}
				if(!hasPermission)
				{
					throw new PIMSWorkListException(ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK);
				}
				try
				{		
					
					bpmWorkListClient.accquireTask(userTask, user);
					CommonUtil.updateRequestTaskAcquiredBy( userTask,CommonUtil.getLoggedInUser()  );
					reqView.setTaskAcquiredByNameAr( getLoggedInUserObj().getSecondaryFullName() );
					reqView.setTaskAcquiredByNameEn( getLoggedInUserObj().getFullName() );
				}
				catch(PIMSWorkListException ex)
				{
					if(ex.getExceptionCode() == ExceptionCodes.TASK_ALREADY_ACQUIRED)
					{
						if(!ex.getUserIdAlreadyPerformedAction().toLowerCase().equals(user.toLowerCase()))
							throw ex;
					}
				}
				
				requestType = reqView.getRequestTypeId().toString();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
			//	logger.logInfo("Request Type is: " + requestType);
				return requestType;
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getParamBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND,reqView.getRequestNumber()));
//				requestTypeId = "";
//				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW, reqView);
	
	   		}
		}
		catch(PIMSWorkListException exp)
		{
			errorMessages = new ArrayList<String>();
			
			if(exp.getExceptionCode() == ExceptionCodes.TASK_ALREADY_ACQUIRED)
			{			
				String msg = java.text.MessageFormat.format(
															 CommonUtil.getParamBundleMessage("TaskList.messages.claimTaskFailureAlreadyClaimed"),
															 exp.getUserIdAlreadyPerformedAction()
														   );
														
				errorMessages.add(	msg );
			}
			else if(exp.getExceptionCode() == ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.LogException("user not authentic", exp);
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.userNotAuthentic"));
			}
			else if(exp.getExceptionCode() == ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.LogException("user not authorized", exp);
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.userNotAuthorize"));
			}
			else if(exp.getExceptionCode() == ExceptionCodes.FAILED_TO_GET_USER_TASKS)
			{
				logger.LogException("Failed to fetch user task", exp);
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.generalError"));
			}		
		}
		catch(Exception exp)
		{
			logger.LogException("Failed to fetch user task", exp);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.generalError"));
		}
   		
		return requestType;
	}

	@SuppressWarnings( "unchecked" )
	public String detailsAction()
	{
		String requestTypeId = null;
		RequestView reqView=null;
		try
		{
			reqView=(RequestView)dataTable.getRowData();
			if( reqView.getRequestTypeId() != null )
			{
				requestTypeId = reqView.getRequestTypeId().toString().trim();
				if(     requestTypeId.equals(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_PAY_BOUNCE_CHEQUE.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_REPLACE_CHEQUE.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_LEASE_CONTRACT.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_RENEW_CONTRACT.toString()) ||
						requestTypeId.equals(WebConstants.NOL.REQUEST_TYPE_NOL_ID.toString()) ||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_MAINTENANCE_APPLICATION.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_CLEARANCE_LETTER.toString()) ||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_INSPECTION.toString()) ||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_CONSTRUCTION_TENDER_PURCHASE.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.PERIODIC_DISBURSEMENTS_ID.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.PAYMENT_REQUEST_ID.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.INVESTMENT_REQUEST_ID.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID.toString()) ||					
						requestTypeId.equals(WebConstants.MemsRequestType.PERIODIC_DISBURSEMENTS_ID.toString())||
						requestTypeId.equals(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.PAY_VENDOR_REQUEST_ID.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.NOL_REQUEST_ID.toString()) ||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST.toString())||
						requestTypeId.equals(WebConstants.MemsRequestType.ENDOWMENT_FILE.toString()) ||
						requestTypeId.equals(WebConstants.MemsRequestType.DONATION_REQUEST.toString() ) ||
						requestTypeId.equals(WebConstants.MemsRequestType.THIRD_PARTY_REVENUE_REQUEST.toString() ) ||
						requestTypeId.equals( WebConstants.MemsRequestType.ENDOWMENT_FILE.toString()) ||
						requestTypeId.equals( WebConstants.MemsRequestType.ENDOWMENT_DISBURSEMENT_REQUEST.toString() )||
						requestTypeId.equals( WebConstants.MemsRequestType.MASRAF_DISBURSEMENT.toString() )||
						requestTypeId.equals( WebConstants.MemsRequestType.ENDOWMENT_EXPENSE_REQUEST.toString()	)  ||
						requestTypeId.equals( WebConstants.MemsRequestType.BLOCKING_REQUEST.toString()) ||
						requestTypeId.equals( WebConstants.MemsRequestType.UNBLOCKING_REQUEST.toString()) ||
						requestTypeId.equals( WebConstants.MemsRequestType.GENERAL_COLLECTION.toString())||
						requestTypeId.equals( WebConstants.MemsRequestType.OPEN_BOX.toString()) ||
						requestTypeId.equals( WebConstants.MemsRequestType.ZAKAT_DEDUCTION.toString() ) ||
						requestTypeId.equals( WebConstants.MemsRequestType.MODIFY_ESTATE_LIMITATION.toString())||
						requestTypeId.equals( WebConstants.MemsRequestType.MINOR_STATUS_CHANGE.toString())||
						requestTypeId.equals( WebConstants.MemsRequestType.ENDOWMENT_NOL_REQUEST_ID.toString())||
						requestTypeId.equals( WebConstants.REQUEST_TYPE_COMPLAINT_ID.toString()) ||
						requestTypeId.equals( WebConstants.MemsRequestType.HOUSING_REQUEST_ID.toString()) ||	
						requestTypeId.equals( WebConstants.MemsRequestType.TERMINATE_HOUSING_REQUEST_ID.toString()) ||
						requestTypeId.equals( Constant.MemsRequestType.FAMILY_VILLAGE_FILE.toString())||
						requestTypeId.equals( WebConstants.REQUEST_TYPE_COMPLAINT_ID.toString())		
				  ) 					
					FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW, reqView);
				else
					ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW, reqView);
			}
			if(requestTypeId.equals(WebConstants.REQUEST_TYPE_TERMINATE_CONTRACT.toString()))
				setRequestParam(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH, "true");
			if(requestTypeId.equals(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString()))
			    setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
			else if(requestTypeId.equals(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT.toString()))
			{
				setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
			    setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
			}

			else if (
					requestTypeId.equals(WebConstants.REQUEST_TYPE_TENDER_INQUIRY.toString())
					||requestTypeId.equals(WebConstants.REQUEST_TYPE_PREPARE_CONSTRUCTION_CONTRACT.toString())
					||requestTypeId.equals(WebConstants.TENDER_INQUIRY_REPLY.toString()))
					
			{
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
			}
		}
		catch (Exception e) {
			logger.LogException("detailsAction| crashed ", e);
		}		
		finally
		{
			reqView= null;
		}
		if( requestTypeId == null )
			return "";
		else
			return requestTypeId;
	}

	public String getBundleMessage(String key){

    	return ResourceUtil.getInstance().getProperty(key);
    }
	
	@SuppressWarnings("unchecked")
	public String technicalCommentsRequiredAction(){
		String METHOD_NAME = "technicalCommentsRequiredAction()";
		logger.logInfo(METHOD_NAME + " started...");
		String taskType = "";
		try{
			
			clearSessionMap();
	   		RequestView reqView=(RequestView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is: " + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		/*WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	   		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);*/

			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return taskType;
//		return "home";
	}
	
	public void doSearchItemList()
	{
		try 
		{
			if(validateFields())
    		{
    			setValues();
    			loadRequestList();
    		}
			
		} 
		catch (Exception e) {
			logger.LogException("doSearchItemList| crashed ", e);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadRequestList() throws PimsBusinessException
	{
		HashMap requestMap = new HashMap ();
    	
    	try
    	{
    		// setValues(); 
    		 loadRequestStatusMap();
			 dataList = new ArrayList<RequestView>();
			 
			 String toDateString = "";
			 String fromDateString = "";
			 if(
					 	requestTypeModuleCombo.getValue() != null && 
					 	!requestTypeModuleCombo.getValue().equals("0") &&
					 	requestTypeId.equals("0")
			   )
			 {
				 
				   requestMap.put("requestTypeModule",requestTypeModuleCombo.getValue() );
				 
			 }
			 else
			 {
				 requestView.setRequestTypeId(Convert.toLong(requestTypeId));
			 }
			 if(htmlCalendarToDate.getValue() !=null)
				toDateString = formatter.format(htmlCalendarToDate.getValue());
			 if(htmlCalendarFromDate.getValue()!=null)
				 fromDateString = formatter.format(htmlCalendarFromDate.getValue());
			 if( lastTrxDateFrom.getValue() != null )
			 {
				 requestMap.put("lastTrxDateFrom",formatter.format( lastTrxDateFrom.getValue() ) );
			 }
			 if( lastTrxDateTo.getValue() != null )
			 {
				 requestMap.put("lastTrxDateTo", formatter.format( lastTrxDateTo.getValue() ) );
			 }
			 if (selectOneContractType!=null && !selectOneContractType.equals("")&& !selectOneContractType.equals("0")){
			    requestMap.put("ContractType",new Long(selectOneContractType));	
			 }
			 if (selectOnedonationCollectedBy!=null && !selectOnedonationCollectedBy.equals("-1") )
			 {
				    requestMap.put("donationCollectedBy", selectOnedonationCollectedBy);	
			 }
			 
			 if (selectOneDisbursementReasonId!=null && !selectOneDisbursementReasonId.equals("")&& !selectOneDisbursementReasonId.equals("-1")){
				    requestMap.put("disbursmentReason",new Long(selectOneDisbursementReasonId));	
			 }
			 if (selectOneDisbursementReasonTypeId!=null && !selectOneDisbursementReasonTypeId.equals("")&& !selectOneDisbursementReasonTypeId.equals("-1")){
				    requestMap.put("disbursmentReasonType",new Long(selectOneDisbursementReasonTypeId));	
			 }
			 if (selectOneTenantType!=null && !selectOneTenantType.equals("")&& !selectOneTenantType.equals("0")){
				    requestMap.put("TenantType",new Long(selectOneTenantType));	
			 }
			 if (selectOneState!=null && !selectOneState.equals("")&& !selectOneState.equals("0")){
				    requestMap.put("State",new Long(selectOneState));	
			}
			 if (selectOneCity!=null && !selectOneCity.equals("")&& !selectOneCity.equals("0")){
				    requestMap.put("City",new Long(selectOneCity));	
			}
			 
			 if (requestCreatedBy!=null && !requestCreatedBy.equals("")&& !requestCreatedBy.equals("-1")){
				    requestMap.put("requestCreatedBy",requestCreatedBy);	
			}
			 if(requestProcessedBy !=null && requestProcessedBy.length() > 0  && !requestProcessedBy.equals("-1") )
			 {
					    requestMap.put("requestProcessedBy",requestProcessedBy);	
			}
			 if (selectOnePropertyUsage!=null && !selectOnePropertyUsage.equals("")&& !selectOnePropertyUsage.equals("0")){
				    requestMap.put("PropertyUsage",new Long(selectOnePropertyUsage));	
			}
			 if (propertyRefNo!=null && !propertyRefNo.equals("")){
				    requestMap.put("PropertyRefNo",propertyRefNo);	
			}
			 if (unitRefNo!=null && !unitRefNo.equals("")){
				    requestMap.put("UnitRefNo",unitRefNo);	
			}
			 if(requestView.getContractView().getContractNumber() != null && requestView.getContractView().getContractNumber().trim().length()>0)
			 {
				    requestMap.put("ContractNumber",requestView.getContractView().getContractNumber().trim());
			 }
			if (requestOrigination!=null && !requestOrigination.equals("")&& !requestOrigination.equals("0")){
				    requestMap.put("RequestOrigination",new Long(requestOrigination));	
			}
			if (requestOriginationList!=null && requestOriginationList.size()>0  ){
				
				if(
					!(
						requestOriginationList.size()==1 && 
						requestOriginationList.get(0).equals( "0")
					 )  
				  )
				{
					List<Long> roList = new ArrayList<Long>();
					for (String obj : requestOriginationList) {
						roList.add(Long.valueOf(obj));
					}
				    requestMap.put("RequestOriginationList",roList);
				}
			}
			if (inheritanceFileRefNo!=null && !inheritanceFileRefNo.trim().equals("") )
			{
				    requestMap.put("inheritanceFileNumber",inheritanceFileRefNo.trim().toLowerCase() );	
			}
			if (fileOwner!=null && !fileOwner.trim().equals("") )
			{
				    requestMap.put("fileOwner",fileOwner.trim().toLowerCase() );	
			}
			 
			
		 	if(applicantName != null && applicantName.trim().length()>0)
			 {
				    requestMap.put("ApplicantName",applicantName.trim());
			 }	
		 
		 	if ( masrafName !=null && masrafName .trim().length()> 0)
		 	{
			    requestMap.put("masrafName",masrafName.trim());	
		    }
		 	if ( complaintNum !=null && complaintNum.trim().length()> 0)
		 	{
			    requestMap.put("complaintNum",complaintNum.trim());	
		    }
		 	if ( endowmentFileName !=null && endowmentFileName.trim().length()> 0)
		 	{
			    requestMap.put("endowmentFileName",endowmentFileName.trim());	
		    }
		 	if ( endowmentFileNum !=null && endowmentFileNum.trim().length()> 0)
		 	{
			    requestMap.put("endowmentFileNum",endowmentFileNum.trim());	
		    }
		 	if ( endowmentName !=null && endowmentName.trim().length()> 0)
		 	{
			    requestMap.put("endowmentName",endowmentName.trim());	
		    }
		 	if ( endowmentNum !=null && endowmentNum.trim().length()> 0)
		 	{
			    requestMap.put("endowmentNum",endowmentNum.trim());	
		    }
				//This is done because, if contract id is not provided but contract number is 
				//provided then it will not come to this transformation therfore we have to define contract id as -1 in request search.java 
				//and with this it will come to transformtaion and will pick other contract related fields
			 requestView.getContractView().setContractId(-1L);	
				
				/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
				int totalRows =  propertyService.searchRequestGetTotalNumberOfRecords(requestView, fromDateString, toDateString,requestMap);
				setTotalRows(totalRows);
				doPagingComputations();
		        //////////////////////////////////////////////////////////////////////////////////////////////
			
			List<RequestView> requestViewList = propertyService.getAllRequests(requestView, fromDateString, toDateString,
					                                                                requestMap,getRowsPerPage(),getCurrentPage(),
					                                                                getSortField(),isSortItemListAscending());
			viewMap.put("requestList", requestViewList);
			recordSize = totalRows;
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			if(!requestViewList.isEmpty())
			{
			 dataList = requestViewList ;
			}
			else
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
		}
		
		catch(Exception exception)
		{
			logger.LogException("loadRequestList() crashed ",exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}	
	
	
	private void setValues()throws Exception
	{
			
			requestView.setStatusId(Convert.toLong(statusId));
	}
	
	public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		try{

			
		}
		catch (Exception exception) {
		}

		return validated;
	}
			
	@Override
	public void init() {
		super.init();
		try
		{
			loadCountries();
			loadStateMap();

			if(!isPostBack())
			{
				//requestCreatedBy = getLoggedInUserId();
				if(
						request.getParameter("source")!=null &&
						request.getParameter("source").equals("newonlinerequestcount")
						
				  )
				{
					requestOriginationList = new ArrayList<String>();
					requestOriginationList.add(String.valueOf(Constant.REQUEST_ORIGINATION_INTERNET_ID));
					requestOriginationList.add(String.valueOf(Constant.REQUEST_ORIGINATION_MOBILE_ID ));
					this.statusId=String.valueOf(WebConstants.REQUEST_STATUS_NEW_ID);
				}
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
		        setSortItemListAscending(true);
				clearSessionMap();
//				loadRequestTypeCombo();
			}
		}
		catch (Exception exception) {
			logger.LogException("init| crashed ", exception);
		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {

		super.prerender();
	}

	@SuppressWarnings("unchecked")
	private void loadRequestTypeList(){
		try
		{
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			String moduleId="0";
			if(requestTypeModuleCombo.getValue()!= null)
			{
				moduleId= requestTypeModuleCombo.getValue().toString();
			}
			requestTypeList =UtilityService.getRequestTypesByModules(moduleId );
//			if(sessionMap.containsKey(WebConstants.REQUEST_TYPE_LIST))
//			{
//				requestTypeList = (List<RequestTypeView>)sessionMap.get(WebConstants.REQUEST_TYPE_LIST);
//			}
//			else
//			{
//				requestTypeList = propertyService.getAllRequestTypes();
//				
//				sessionMap.put(WebConstants.REQUEST_TYPE_LIST,requestTypeList);
//			}
		}
		catch(PimsBusinessException exception)
		{
			logger.LogException("loadRequestTypeList| crashed ",exception);
    	}
    	catch(Exception exception)
    	{
    		logger.LogException( "loadRequestTypeList| crashed ",exception);
    	}
	}
	

	@SuppressWarnings("unchecked")
	public void loadRequestStatusMap() 
	{
		try
		{
			if(viewMap.containsKey(WebConstants.REQUEST_STATUS_MAP))
			{
				requestStatusMap = (HashMap<String,Long>)viewMap.get(WebConstants.REQUEST_STATUS_MAP);
			}
			else
			{
				List<DomainDataView> requestStatusList = propertyService.getDomainDataByDomainTypeName(WebConstants.REQUEST_STATUS);
				if(requestStatusList!=null) 
				{
					for (DomainDataView domainDataView : requestStatusList) 
					{
						requestStatusMap.put(domainDataView.getDataValue(), domainDataView.getDomainDataId());
					}
					viewMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
				}
			}
		}
    	catch(Exception exception){
    		logger.LogException("loadRequestStatusMap() crashed ",exception);
    	}
	}
	public void requestTypeModuleChange(ValueChangeEvent vce) throws Exception
	{
		try{
			dataList=new ArrayList<RequestView>();
			loadRequestTypeList();
			getRequestTypes();
		}
    	catch(Exception exception){
    		logger.LogException("requestTypeModuleChange() crashed ",exception);
    	}	
	}
	@SuppressWarnings("unchecked")
	private void loadRequestTypeCombo(){
		try{
			requestTypes=new ArrayList<SelectItem>();
			if((requestTypeList.size()==0) || (requestTypeList==null))
				loadRequestTypeList();
			if(getIsEnglishLocale())
			{
//				if(viewMap.containsKey(WebConstants.REQUEST_TYPE_COMBO_EN))
//					requestTypes = (List<SelectItem>)viewMap.get(WebConstants.REQUEST_TYPE_COMBO_EN);
//				else
				{
					for(RequestTypeView reqTypeView: requestTypeList)
					{
						SelectItem item = null;
						item = new SelectItem(reqTypeView.getRequestTypeId().toString(),reqTypeView.getRequestTypeEn());
					    requestTypes.add(item);
					}
					Collections.sort( requestTypes, ListComparator.LIST_COMPARE );
					viewMap.put(WebConstants.REQUEST_TYPE_COMBO_EN,requestTypes);
				}
			}
			else
			{
//				if(viewMap.containsKey(WebConstants.REQUEST_TYPE_COMBO_AR))
//					requestTypes = (List<SelectItem>)viewMap.get(WebConstants.REQUEST_TYPE_COMBO_AR);
//				else
				{
					for(RequestTypeView reqTypeView: requestTypeList)
					{
						SelectItem item = null;
						item = new SelectItem(reqTypeView.getRequestTypeId().toString(),reqTypeView.getRequestTypeAr());
					    requestTypes.add(item);
					}
					Collections.sort( requestTypes, ListComparator.LIST_COMPARE );
					viewMap.put(WebConstants.REQUEST_TYPE_COMBO_AR,requestTypes);
				}
			}
    	}
    	catch(Exception exception){
    		logger.LogException("loadRequestTypeCombo| crashed ",exception);
    	}
	}
	
	public void clear(javax.faces.event.ActionEvent event){
    	logger.logInfo("clear() started...");
    	try{
    		fromDate=null;
    		toDate=null;
    		requestView.getTenantsView().setFirstName("");
    		requestView.getContractView().setContractNumber("");
    		/*requestTypeCombo.resetValue();
    		requestStatusCombo.resetValue();*/
    		logger.logInfo("clear() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("clear() crashed ",exception);
    	}
    }
	
		
    public String searchRequests()
    {
    	try{
    		if(validateFields())
    		{
    			setValues();
    			loadRequestList();
    		}
    			
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
        return "searchRequests";
    }
	
    
    public void cancel(ActionEvent event) {
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        clearSessionMap();
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }
    
    	
	/*
	 * Setters / Getters
	 */
    
	/**
	 * @return the requestView
	 */
	public RequestView getRequestView() {
		return requestView;
	}

	/**
	 * @param requestView the requestView to set
	 */
	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	/**
	 * @return the dataItem
	 */
	public RequestView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(RequestView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	@SuppressWarnings("unchecked")
	public List<RequestView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<RequestView>)viewMap.get("requestList");
		if(dataList==null)
			dataList = new ArrayList<RequestView>();
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<RequestView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}



	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the requestStatuses
	 */
	public List<SelectItem> getRequestStatuses() {
		return requestStatuses;
	}

	/**
	 * @param requestStatuses the requestStatuses to set
	 */
	public void setRequestStatuses(List<SelectItem> requestStatuses) {
		this.requestStatuses = requestStatuses;
	}

		/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the requestStatusMap
	 */
	public Map<String, Long> getRequestStatusMap() {
		return requestStatusMap;
	}

	/**
	 * @param requestStatusMap the requestStatusMap to set
	 */
	public void setRequestStatusMap(Map<String, Long> requestStatusMap) {
		this.requestStatusMap = requestStatusMap;
	}

	/**
	 * @return the requestTypeMap
	 */
	public Map<String, Long> getRequestTypeMap() {
		return requestTypeMap;
	}

	/**
	 * @param requestTypeMap the requestTypeMap to set
	 */
	public void setRequestTypeMap(Map<String, Long> requestTypeMap) {
		this.requestTypeMap = requestTypeMap;
	}

	
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}


	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	/**
	 * @return the requestTypes
	 */
	public List<SelectItem> getRequestTypes() {
		loadRequestTypeCombo();
		return requestTypes;
	}

	/**
	 * @param requestTypes the requestTypes to set
	 */
	public void setRequestTypes(List<SelectItem> requestTypes) {
		this.requestTypes = requestTypes;
	}

	/**
	 * @return the requestTypeId
	 */
	public String getRequestTypeId() {
		return requestTypeId;
	}

	/**
	 * @param requestTypeId the requestTypeId to set
	 */
	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the requestTypeList
	 */
	public List<RequestTypeView> getRequestTypeList() {
		return requestTypeList;
	}

	/**
	 * @param requestTypeList the requestTypeList to set
	 */
	public void setRequestTypeList(List<RequestTypeView> requestTypeList) {
		this.requestTypeList = requestTypeList;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message;;
			}
			messageList = messageList + "</UL>";
		}
		return (messageList);

	}
	
	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}


	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}


	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}


	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}


	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	/**
	 * @return the requestTypeCombo
	 */
	public HtmlSelectOneMenu getRequestTypeCombo() {
		return requestTypeCombo;
	}


	/**
	 * @param requestTypeCombo the requestTypeCombo to set
	 */
	public void setRequestTypeCombo(HtmlSelectOneMenu requestTypeCombo) {
		this.requestTypeCombo = requestTypeCombo;
	}


	/**
	 * @return the requestStatusCombo
	 */
	public HtmlSelectOneMenu getRequestStatusCombo() {
		return requestStatusCombo;
	}


	/**
	 * @param requestStatusCombo the requestStatusCombo to set
	 */
	public void setRequestStatusCombo(HtmlSelectOneMenu requestStatusCombo) {
		this.requestStatusCombo = requestStatusCombo;
	}


	/**
	 * @return the paginatorMaxPages
	 */
	@SuppressWarnings("unchecked")
	public Integer getPaginatorMaxPages() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}


	public String getUnitRefNo() {
		return unitRefNo;
	}


	public void setUnitRefNo(String unitRefNo) {
		this.unitRefNo = unitRefNo;
	}


	public String getPropertyRefNo() {
		return propertyRefNo;
	}


	public void setPropertyRefNo(String propertyRefNo) {
		this.propertyRefNo = propertyRefNo;
	}


	public String getSelectOneTenantType() {
		return selectOneTenantType;
	}


	public void setSelectOneTenantType(String selectOneTenantType) {
		this.selectOneTenantType = selectOneTenantType;
	}


	public String getSelectOneContractType() {
		return selectOneContractType;
	}


	public void setSelectOneContractType(String selectOneContractType) {
		this.selectOneContractType = selectOneContractType;
	}


	public String getSelectOneState() {
		return selectOneState;
	}


	public void setSelectOneState(String selectOneState) {
		this.selectOneState = selectOneState;
	}


	public String getSelectOneCity() {
		return selectOneCity;
	}


	public void setSelectOneCity(String selectOneCity) {
		this.selectOneCity = selectOneCity;
	}


	public String getSelectOnePropertyUsage() {
		return selectOnePropertyUsage;
	}


	public void setSelectOnePropertyUsage(String selectOnePropertyUsage) {
		this.selectOnePropertyUsage = selectOnePropertyUsage;
	}


	public List<SelectItem> getCountries() {
		return countries;
	}


	public void setCountries(List<SelectItem> countries) {
		this.countries = countries;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getStates() {
		
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		states = (List<SelectItem>) sessionMap.get("states");
		if(states==null)
			states = new ArrayList<SelectItem>();
		return states;
	}


	public void setStates(List<SelectItem> states) {
		this.states = states;
	}


	public Map<Long, RegionView> getCountryMap() {
		return countryMap;
	}


	public void setCountryMap(Map<Long, RegionView> countryMap) {
		this.countryMap = countryMap;
	}

	@SuppressWarnings("unchecked")
	public Map<Long, RegionView> getStateMap() {
		
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		stateMap = (Map<Long, RegionView>) sessionMap.get("stateMap");
		if(stateMap==null)
			stateMap = new HashMap<Long, RegionView>();
		return stateMap;
	}


	public void setStateMap(Map<Long, RegionView> stateMap) {
		this.stateMap = stateMap;
	}
	
    @SuppressWarnings("unchecked")
	private void loadCountries() 
	{
		try {
			List <RegionView> regionViewList = propertyService.getCountry();
			countries = new ArrayList();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(sessionMap.containsKey("countryMap"))
			{
				countryMap = (HashMap<Long, RegionView>) sessionMap.get("countryMap");
			}
			else{
				countryMap = new HashMap<Long, RegionView>();
				for(RegionView regView :regionViewList)
				{
					countryMap.put(regView.getRegionId(), regView);
				}
				sessionMap.put("countryMap",countryMap);
			}
			if(sessionMap.containsKey("countries"))
			{
				countries = (List<SelectItem>) sessionMap.get("countries");
			}
			else{	 
				for(RegionView regionView :regionViewList)
				{
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
				  countries.add(item);
				}
				sessionMap.put("countries",countries);
			}
		}catch(Exception exception){
    		logger.LogException("loadCountries() crashed ",exception);
    	}
		
		
	}
    @SuppressWarnings("unchecked")	
	public void loadStateMap(){
		try {
			stateMap = new HashMap<Long, RegionView>();
			
			List <RegionView> statesList = propertyService.getAllStates();
			for(RegionView regView :statesList)
			{
				stateMap.put(regView.getRegionId(), regView);
			}
			viewMap.put("stateMap",stateMap);
		}catch(Exception exception){
			logger.LogException("loadStateMap() crashed ",exception);
		}
	}
    @SuppressWarnings("unchecked")
	public void loadStates(ValueChangeEvent vce)  {
		try {
			states = new ArrayList<SelectItem>();
			String regionName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)countryMap.get(regionId);
			if(temp!=null)
			{
				regionName = temp.getRegionName();
				List <RegionView> statesList = propertyService.getCountryStates(regionName,getIsArabicLocale());
				
				for(RegionView stateView:statesList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
					else 
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
					states.add(item);
				}
				viewMap.put("states",states);
			}
			else{
				viewMap.remove("states");
			}
				
		}catch(Exception exception){
    		logger.LogException("loadStates() crashed ",exception);
    	}
	}
    @SuppressWarnings("unchecked")	
	public void loadCities(ValueChangeEvent vce)  {
		try {
			cities = new ArrayList<SelectItem>();
			String selectedStateName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)stateMap.get(regionId);
			if(temp!=null)
			{
				selectedStateName = temp.getRegionName();


				List <RegionView> cityList = propertyService.getCountryStates(selectedStateName,getIsArabicLocale());

			
			
				for(RegionView cityView:cityList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionEn());			  }
					else 
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionAr());	  
					cities.add(item);
				}
			}
		}catch(Exception exception){
    		logger.LogException("loadCities() crashed ",exception);
    	}
	}


	public String getRequestOrigination() {
		return requestOrigination;
	}


	public void setRequestOrigination(String requestOrigination) {
		this.requestOrigination = requestOrigination;
	}
	public String printReport()
	{
		RequestView reqView=null;
		try 
		{
			reqView=(RequestView)dataTable.getRowData();
			if( reqView.getRequestTypeId().compareTo( Constant.RENEW_CONTRACT ) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_LEASE_CONTRACT ) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_CANCEL_CONTRACT_ID  ) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_TRANSFER_CONTRACT) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_CHANGE_TENANT_NAME) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_REPLACE_CHEQUE) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE ) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_NOL) == 0 ||
				reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_MAINTENANCE_REQUEST) == 0 
				 
			  )
			{
				CommonUtil.printZawayaReport( reqView.getRequestId() );
				return null;
			}
			else if(reqView.getRequestTypeId().compareTo( Constant.REQUEST_TYPE_AUCTION_REG) == 0){
				
				CommonUtil.printZawayaReportAuction( reqView.getRequestId() );
				return null;
			}
			if(reqView.getRequestTypeId().compareTo(Constant.MemsRequestType.MASRAF_DISBURSEMENT)==0  		
			  )
				
			{
				CommonUtil.printMasrafDisbursementReport( reqView.getRequestId() );
				return null;
			}
			if(
				reqView.getRequestTypeId().compareTo(Constant.MemsRequestType.ENDOWMENT_DISBURSEMENT_REQUEST)==0 		
			  )
						
			{
				CommonUtil.printEndowmentDisbursementReport( reqView.getRequestId() );
				return null;
			}
			
			if(reqView.getRequestTypeId().compareTo(28l)>0)
			{
				CommonUtil.printMemsReport(reqView.getRequestId());
				return null;
			}
			requestDetailsReportCriteria=new RequestDetailsReportCriteria(ReportConstant.Report.REQUEST_DETAILS_REPORT_AR, 
			ReportConstant.Processor.REQUEST_DETAILS_REPORT_PROCESSOR,CommonUtil.getLoggedInUser());
			
			if(reqView.getRequestId()!=null)
				requestDetailsReportCriteria.setRequestId(reqView.getRequestId().toString());
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, requestDetailsReportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		 } 
		catch (Exception exception) 
		{
			logger.LogException("openPopup() crashed ", exception);
		}
		finally
		{
			reqView = null;
		}
		return null;
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		 } 
		catch (Exception exception) 
		{
			logger.LogException("openPopup() crashed ", exception);
		}
	}


	public HtmlCalendar getHtmlCalendarFromDate() {
		return htmlCalendarFromDate;
	}


	public void setHtmlCalendarFromDate(HtmlCalendar htmlCalendarFromDate) {
		this.htmlCalendarFromDate = htmlCalendarFromDate;
	}


	public HtmlCalendar getHtmlCalendarToDate() {
		return htmlCalendarToDate;
	}


	public void setHtmlCalendarToDate(HtmlCalendar htmlCalendarToDate) {
		this.htmlCalendarToDate = htmlCalendarToDate;
	}
public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}


public String getRequestCreatedBy() {
	return requestCreatedBy;
}


public void setRequestCreatedBy(String requestCreatedBy) {
	this.requestCreatedBy = requestCreatedBy;
}


public String getRequestProcessedBy() {
	return requestProcessedBy;
}


public void setRequestProcessedBy(String requestProcessedBy) {
	this.requestProcessedBy = requestProcessedBy;
}


public HtmlCalendar getLastTrxDateFrom() {
	return lastTrxDateFrom;
}


public void setLastTrxDateFrom(HtmlCalendar lastTrxDateFrom) {
	this.lastTrxDateFrom = lastTrxDateFrom;
}


public HtmlCalendar getLastTrxDateTo() {
	return lastTrxDateTo;
}


public void setLastTrxDateTo(HtmlCalendar lastTrxDateTo) {
	this.lastTrxDateTo = lastTrxDateTo;
}


public String getInheritanceFileRefNo() {
	return inheritanceFileRefNo;
}


public void setInheritanceFileRefNo(String inheritanceFileRefNo) {
	this.inheritanceFileRefNo = inheritanceFileRefNo;
}


public String getFileOwner() {
	return fileOwner;
}


public void setFileOwner(String fileOwner) {
	this.fileOwner = fileOwner;
}


public String getSelectOneDisbursementReasonId() {
	return selectOneDisbursementReasonId;
}


public void setSelectOneDisbursementReasonId(
		String selectOneDisbursementReasonId) {
	this.selectOneDisbursementReasonId = selectOneDisbursementReasonId;
}


public String getSelectOneDisbursementReasonTypeId() {
	return selectOneDisbursementReasonTypeId;
}


public void setSelectOneDisbursementReasonTypeId(
		String selectOneDisbursementReasonTypeId) {
	this.selectOneDisbursementReasonTypeId = selectOneDisbursementReasonTypeId;
}


public String getSelectOnedonationCollectedBy() {
	return selectOnedonationCollectedBy;
}


public void setSelectOnedonationCollectedBy(String selectOnedonationCollectedBy) {
	this.selectOnedonationCollectedBy = selectOnedonationCollectedBy;
}


public String getMasrafName() {
	return masrafName;
}


public void setMasrafName(String masrafName) {
	this.masrafName = masrafName;
}


public String getEndowmentName() {
	return endowmentName;
}


public void setEndowmentName(String endowmentName) {
	this.endowmentName = endowmentName;
}


public String getEndowmentNum() {
	return endowmentNum;
}


public void setEndowmentNum(String endowmentNum) {
	this.endowmentNum = endowmentNum;
}


public String getEndowmentFileName() {
	return endowmentFileName;
}


public void setEndowmentFileName(String endowmentFileName) {
	this.endowmentFileName = endowmentFileName;
}


public String getEndowmentFileNum() {
	return endowmentFileNum;
}


public void setEndowmentFileNum(String endowmentFileNum) {
	this.endowmentFileNum = endowmentFileNum;
}


public HtmlSelectOneMenu getRequestTypeModuleCombo() {
	return requestTypeModuleCombo;
}


public void setRequestTypeModuleCombo(HtmlSelectOneMenu requestTypeModuleCombo) {
	this.requestTypeModuleCombo = requestTypeModuleCombo;
}


public List<String> getRequestOriginationList() {
	return requestOriginationList;
}


public void setRequestOriginationList(List<String> requestOriginationList) {
	this.requestOriginationList = requestOriginationList;
}


}

