package com.avanza.pims.web.backingbeans.Investment.Opportunities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.InvestmentOpportunityView;

public class InvestmentOpportunitySearchBacking extends AbstractController 
{	
	private static final long serialVersionUID = 1243101210652238837L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent;
	
	private String pageMode;
	private InvestmentOpportunityView investmentOpportunityView;
	private List<InvestmentOpportunityView> investmentOpportunityViewList;
	
	private HtmlDataTable dataTable;
	
	@SuppressWarnings("unchecked")
	public InvestmentOpportunitySearchBacking() 
	{
		logger = Logger.getLogger( this.getClass() );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
		
		pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_SEARCH;
		investmentOpportunityView = new InvestmentOpportunityView();
		investmentOpportunityViewList = new ArrayList<InvestmentOpportunityView>(0);
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}		
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY );
		}
		else if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY );
		}
		
		
		// INVESTMENT OPPORTUNITY VIEW
		if ( sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW ) != null )
		{
			investmentOpportunityView = (InvestmentOpportunityView) sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );			
			sessionMap.remove( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );			
		}
		else if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW ) != null )
		{
			investmentOpportunityView = (InvestmentOpportunityView) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );
		}
		
		// INVESTMENT OPPORTUNITY VIEW LIST
		if( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW_LIST ) != null )
		{
			investmentOpportunityViewList = (List<InvestmentOpportunityView>) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW_LIST );
		}		
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_KEY, pageMode );
		}
		
		
		// INVESTMENT OPPORTUNITY VIEW
		if ( investmentOpportunityView != null )
		{
			 viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW, investmentOpportunityView );
		}
		
		
		// INVESTMENT OPPORTUNITY VIEW LIST
		if ( investmentOpportunityViewList != null )
		{
			viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW_LIST, investmentOpportunityViewList );
		}
	}
	
	public String onSearch() 
	{
		String METHOD_NAME = "onSearch()";		
		String path = null;
		
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			investmentOpportunityView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			investmentOpportunityView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );			
			investmentOpportunityViewList = investmentOpportunityServiceAgent.getInvestmentOpportunityViewList( investmentOpportunityView );
			
			updateValuesToMaps();
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onAdd() 
	{
		sessionMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY , WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_ADD );
		return "investmentOpportunityManage";
	}
	
	public String onView()
	{
		InvestmentOpportunityView selectedInvestmentOpportunityView = (InvestmentOpportunityView) dataTable.getRowData();
		sessionMap.put(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW, selectedInvestmentOpportunityView);
		sessionMap.put(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY, WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_VIEW);
		return "investmentOpportunityManage";
	}
	
	public String onEdit()
	{
		InvestmentOpportunityView selectedInvestmentOpportunityView = (InvestmentOpportunityView) dataTable.getRowData();
		sessionMap.put(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW, selectedInvestmentOpportunityView);
		sessionMap.put(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY, WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EDIT);
		return "investmentOpportunityManage";
	}
	
	public String onDelete()
	{
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		Boolean isDeleted = false;
		
		InvestmentOpportunityView selectedInvestmentOpportunityView = (InvestmentOpportunityView) dataTable.getRowData();
		selectedInvestmentOpportunityView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedInvestmentOpportunityView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		try
		{
			isDeleted = investmentOpportunityServiceAgent.updateInvestmentOpportunity( selectedInvestmentOpportunityView );
			
			if ( isDeleted )
			{
				investmentOpportunityViewList.remove( selectedInvestmentOpportunityView );
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_DELETE_SUCCESS ) );
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
		}
		catch (Exception exception) 
		{	
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_DELETE_ERROR ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;
	}
	
	public String onSingleSelect()
	{
		return null;
	}
	
	public String onMultiSelect()
	{
		return null;
	}
	
	public Boolean getIsSearchMode()
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_SEARCH_PAGE_MODE_SEARCH ) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Boolean getIsMultiSelectPopupMode()
	{
		Boolean isMultiSelectPopupMode = false;
		
		return isMultiSelectPopupMode;		
	}
	
	public Boolean getIsSingleSelectPopupMode() 
	{
		Boolean isSingleSelectPopupMode = false;
		
		return isSingleSelectPopupMode;
	}
	
	public Integer getRecordSize() 
	{
		return investmentOpportunityViewList.size();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public Integer getPaginatorRows() 
	{		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() 
	{
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() 
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public InvestmentOpportunityView getInvestmentOpportunityView() {
		return investmentOpportunityView;
	}

	public void setInvestmentOpportunityView(
			InvestmentOpportunityView investmentOpportunityView) {
		this.investmentOpportunityView = investmentOpportunityView;
	}

	public InvestmentOpportunityServiceAgent getInvestmentOpportunityServiceAgent() {
		return investmentOpportunityServiceAgent;
	}

	public void setInvestmentOpportunityServiceAgent(
			InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent) {
		this.investmentOpportunityServiceAgent = investmentOpportunityServiceAgent;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public List<InvestmentOpportunityView> getInvestmentOpportunityViewList() {
		return investmentOpportunityViewList;
	}

	public void setInvestmentOpportunityViewList(
			List<InvestmentOpportunityView> investmentOpportunityViewList) {
		this.investmentOpportunityViewList = investmentOpportunityViewList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
}
