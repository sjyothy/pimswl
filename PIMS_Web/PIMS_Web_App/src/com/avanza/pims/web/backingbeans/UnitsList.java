package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FeeConfigurationDetailView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;

public class UnitsList extends AbstractController
{
	private static Logger logger = Logger.getLogger( UnitsList.class );
	
	private HtmlDataTable dataTable;
	private Double unitAreaMin = null;
	private Double unitAreaMax = null;
	private Long noOfLivingMin = null;
	private Long noOfLivingMax = null;


	private Long noOfBathMin = null;
	private Long noOfBathMax = null;


	private String floorNumber ;

	private String hdnSelectedUnit;
	//private Calendar occupiedTillDate = new Calendar();
	
	private Date inquiryDate = new Date();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String unitId;
	private String createdBy;
	private String createdOn;
	
	
	
	private HtmlSelectOneMenu unitTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitUsageSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitInvestmentSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitSideSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu inquiryMethodSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu prioritySelectMenu= new HtmlSelectOneMenu();


	String selectOneUnitSide;
	
	
	
	List<SelectItem> unitType= new ArrayList();
	List<SelectItem> unitUsage= new ArrayList();
	List<SelectItem> unitInvestment= new ArrayList();
	List<SelectItem> unitSide= new ArrayList();
	
	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectBooleanCheckbox  checkBox=new HtmlSelectBooleanCheckbox();
	InquiryView inquiryView = new InquiryView();
	List<SelectItem> city= new ArrayList();
	List<SelectItem> state= new ArrayList();
	List<SelectItem> country= new ArrayList();
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	
	private String selectOneUnitType;
	private String selectOneUnitUsage;
	private String unitNumber;
	private String rentValueMin;
	private String rentValueMax;
	private String noOfBedsMax;
	private String noOfBedsMin;
	private String countryId;
	private String stateId;
	private String cityId;

	    
	
	 FacesContext context=FacesContext.getCurrentInstance();
	 Map sessionMap= context.getExternalContext().getSessionMap();
	 Map viewRootMap=context.getViewRoot().getAttributes();

    CommonUtil commonUtil = new CommonUtil();
    
    
	
     @Override 
	 public void init() 
     {
    	
    	 super.init();
    	 try
    	 {
    
     sessionMap=context.getExternalContext().getSessionMap();
     loadCountry();
     
     if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_TYPE,sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_TYPE);
		}

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_USAGE,sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_USAGE);
		}	

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_NUMBER,sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_NUMBER);
		}	

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN,sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN);
		}

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX,sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX);
		}	

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX,sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX);
		}

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN,sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN);
		}	

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.COUNTRY_ID,sessionMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.COUNTRY_ID);
		}	

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.STATE_ID,sessionMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.STATE_ID);
		}

		if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID)){
			viewRootMap.put(WebConstants.INQUIRY_UNITS.CITY_ID,sessionMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString());
			sessionMap.remove(WebConstants.INQUIRY_UNITS.CITY_ID);
		}

     
     if(!isPostBack()){
    	 
    	 if (dataList == null || dataList.size()==0)
    		{
    
    			//dataList= loadDataList(); 
    		}
                   }
    		 
    	 }
    	 
    	 
    	 catch(Exception es)
    	 {
    		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		 System.out.println(es);
    		 
    	 }
	        
	 }
     @Override 
     public void prerender(){
    	 super.prerender();
    	 
    	    	 
    		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID)){
    			countryId = viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString();
    			if(countryId!=null && !countryId.equals("")&& !countryId.equals("-1")){
    			loadCountry();
    			loadState();}
     	        }
        		
        		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID)){
        			
    			  stateId=	viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString();
    			  if(stateId!=null && !stateId.equals("")&& !stateId.equals("-1")){
    			  loadCity();
    			  }
        		}
     }
	 
     private void loadCombos() throws PimsBusinessException
     {
    	 
    	 if(!sessionMap.containsKey(WebConstants.UNIT_TYPE))
    	   loadUnitTypeList();
    	 if(!sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
      	   loadUnitSideTypeList();
    	 if(!sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
    	   loadUnitUsageList(); 
    	 
     }
	 
     private void loadUnitTypeList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_TYPE);
    	 sessionMap.put(WebConstants.UNIT_TYPE, ddl);
    	 
    	 
     }
     private void loadUnitSideTypeList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_SIDE_TYPE);
    	 sessionMap.put(WebConstants.UNIT_SIDE_TYPE, ddl);
    	 
     }
     private void  loadUnitUsageList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);
    	 sessionMap.put(WebConstants.PROPERTY_USAGE, ddl);
    	 
     }  
     
	 
		//We have to look for the list population for facilities
	private List<String> errorMessages = new ArrayList<String>();
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private UnitView dataItem = new UnitView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<UnitView> dataList = new ArrayList<UnitView>();
  
	//	public Unit dataItem = new Unit();
	
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
	public List<UnitView> getPropertyInquiryDataList()
	{
		 Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		if (dataList == null || dataList.size()==0)
		{
			if(viewMap.containsKey("UNIT_LIST"))	
				  dataList= (ArrayList)viewMap.get("UNIT_LIST");
			//else
			//	loadDataList();
		}
		return dataList;
	}

	
	
	public String doBid() {
//		errorMessages = new ArrayList<String>();
/*		if (getCustomerId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
			
*/ 
		return "";
		}
	
	public void doSearch (){
				logger.logInfo("doSearch Start:::::::::::::::::::::");
				try {
				if ( noOfBedsMin!=null && ! noOfBedsMin.equals(""))
				{
				inquiryView.setNoOfBedsMin(Long.parseLong(noOfBedsMin));
				}
				if ( noOfBedsMax !=null && !noOfBedsMax.equals(""))
				{
				inquiryView.setNoOfBedsMax(Long.parseLong(noOfBedsMax));
				}
				if (rentValueMin!=null && !rentValueMin.equals(""))
				{
				inquiryView.setRentValueMin(Double.parseDouble(rentValueMin));
				}
				if (rentValueMax!=null && !rentValueMax.equals(""))
				{
				inquiryView.setRentValueMax(Double.parseDouble(rentValueMax));
				}
				if(selectOneUnitType!=null && !selectOneUnitType.equals("")&& !selectOneUnitType.equals("-1") )
				{
				inquiryView.setUnitType(new Long(selectOneUnitType));
				}
				if(selectOneUnitUsage !=null && !selectOneUnitUsage.equals("")&& !selectOneUnitUsage.equals("-1") ){
				inquiryView.setUnitUsageTypeId(new Long(selectOneUnitUsage));
				}
				if (unitNumber!=null && !unitNumber.equals("")){
				inquiryView.setUnitNumber(unitNumber);
				}
				if (countryId !=null && ! countryId.equals("")&& ! countryId.equals("-1")){
				inquiryView.setCountryId(Long.parseLong(countryId));
				}
				if (stateId!=null && !stateId.equals("")&& !stateId.equals("-1")){
				inquiryView.setStateId(Long.parseLong(stateId));
				}
				if (cityId!=null && !cityId.equals("")&& !cityId.equals("-1")){
				inquiryView.setCityId(Long.parseLong(cityId));
				}
				dataList.clear();
				loadDataList();
				logger.logInfo("doSearch Ends:::::::::::::::::::::");

                    }
            catch (Exception e){
            	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
            	logger.logInfo("doSearch Crashed:::::::::::::::::::::");
                System.out.println("Exception doSearch"+e);       
           }



        



}

	
	public List<UnitView> loadDataList() 
	{
		 String methodName="loadDataList";
		 List<UnitView> list = new ArrayList();
		 
		 logger.logInfo(methodName+"Starts:::::::::::");
		
		try
		{
			PropertyServiceAgent myPort = new PropertyServiceAgent();

	            
	           list =  myPort.getPropertyInquiryUnit(inquiryView);
	           
	           
	           
	           Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				viewMap.put("UNIT_LIST", list);
				recordSize = list.size();
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
	           

		        dataList=(ArrayList<UnitView>) list;

		        logger.logInfo("Record Found In Load Data List : %s",dataList.size());
		        
		        if (dataList.size()==0){
		        	errorMessages = new ArrayList<String>();
					errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
		        	
		        }
	           
     
		        logger.logInfo(methodName+"Ends:::::::::::");
	    
	    }
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logInfo(methodName+"Crashed:::::::::::");
	        ex.printStackTrace();
	    }
		return list;
		
	}
	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }

	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Double getUnitAreaMin() {
		return unitAreaMin;
	}



	public void setUnitAreaMin(Double unitAreaMin) {
		this.unitAreaMin = unitAreaMin;
	}



	public Double getUnitAreaMax() {
		return unitAreaMax;
	}



	public void setUnitAreaMax(Double unitAreaMax) {
		this.unitAreaMax = unitAreaMax;
	}



	public Long getNoOfLivingMin() {
		return noOfLivingMin;
	}



	public void setNoOfLivingMin(Long noOfLivingMin) {
		this.noOfLivingMin = noOfLivingMin;
	}



	public Long getNoOfLivingMax() {
		return noOfLivingMax;
	}



	public void setNoOfLivingMax(Long noOfLivingMax) {
		this.noOfLivingMax = noOfLivingMax;
	}



	



	public Long getNoOfBathMin() {
		return noOfBathMin;
	}



	public void setNoOfBathMin(Long noOfBathMin) {
		this.noOfBathMin = noOfBathMin;
	}



	public Long getNoOfBathMax() {
		return noOfBathMax;
	}



	public void setNoOfBathMax(Long noOfBathMax) {
		this.noOfBathMax = noOfBathMax;
	}



	/*public Double getRentValueMin() {
		return rentValueMin;
	}



	public void setRentValueMin(Double rentValueMin) {
		this.rentValueMin = rentValueMin;
	}



	public Double getRentValueMax() {
		return rentValueMax;
	}



	public void setRentValueMax(Double rentValueMax) {
		this.rentValueMax = rentValueMax;
	}
*/






	public HtmlSelectOneMenu getUnitTypeSelectMenu() {
		return unitTypeSelectMenu;
	}



	public void setUnitTypeSelectMenu(HtmlSelectOneMenu unitTypeSelectMenu) {
		this.unitTypeSelectMenu = unitTypeSelectMenu;
	}



	public HtmlSelectOneMenu getUnitUsageSelectMenu() {
		return unitUsageSelectMenu;
	}



	public void setUnitUsageSelectMenu(HtmlSelectOneMenu unitUsageSelectMenu) {
		this.unitUsageSelectMenu = unitUsageSelectMenu;
	}



	public HtmlSelectOneMenu getUnitInvestmentSelectMenu() {
		return unitInvestmentSelectMenu;
	}



	public void setUnitInvestmentSelectMenu(
			HtmlSelectOneMenu unitInvestmentSelectMenu) {
		this.unitInvestmentSelectMenu = unitInvestmentSelectMenu;
	}



	public HtmlSelectOneMenu getUnitSideSelectMenu() {
		return unitSideSelectMenu;
	}



	public void setUnitSideSelectMenu(HtmlSelectOneMenu unitSideSelectMenu) {
		this.unitSideSelectMenu = unitSideSelectMenu;
	}



	public HtmlSelectOneMenu getInquiryMethodSelectMenu() {
		return inquiryMethodSelectMenu;
	}



	public void setInquiryMethodSelectMenu(HtmlSelectOneMenu inquiryMethodSelectMenu) {
		this.inquiryMethodSelectMenu = inquiryMethodSelectMenu;
	}



	public HtmlSelectOneMenu getPrioritySelectMenu() {
		return prioritySelectMenu;
	}



	public void setPrioritySelectMenu(HtmlSelectOneMenu prioritySelectMenu) {
		this.prioritySelectMenu = prioritySelectMenu;
	}



	public List<SelectItem> getUnitType() {
		unitType.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitType.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR)); 
		 }
		
		
		return unitType;
	}



	public void setUnitType(List<SelectItem> unitType) {
		this.unitType = unitType;
	}



	public List<SelectItem> getUnitUsage() {
		unitUsage.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.PROPERTY_USAGE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitUsage.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR)); 
		 }



		return unitUsage;
	}



	public void setUnitUsage(List<SelectItem> unitUsage) {
		this.unitUsage = unitUsage;
	}



	public List<SelectItem> getUnitInvestment() {
		return unitInvestment;
	}



	public void setUnitInvestment(List<SelectItem> unitInvestment) {
		this.unitInvestment = unitInvestment;
	}



	public List<SelectItem> getUnitSide() {
		unitSide.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_SIDE_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitSide.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR)); 
		 }

		return unitSide;
	}



	public void setUnitSide(List<SelectItem> unitSide) {
		this.unitSide = unitSide;
	}




	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}



	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}



	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}



	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}



	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}



	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}



	public List<SelectItem> getCity() {
		return city;
	}



	public void setCity(List<SelectItem> city) {
		this.city = city;
	}



	public List<SelectItem> getState() {
		return state;
	}



	public void setState(List<SelectItem> state) {
		this.state = state;
	}



	public List<SelectItem> getCountry() {
		return country;
	}



	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}


    public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public UnitView getDataItem() {
		return dataItem;
	}
	
	



	public void setDataItem(UnitView dataItem) {
		this.dataItem = dataItem;
	}



	public List<UnitView> getDataList() {
		return dataList;
	}



	public void setDataList(List<UnitView> dataList) {
		this.dataList = dataList;
	}



	

/*
	public Date getOccupiedTillDate() {
		return occupiedTillDate;
	}



	public void setOccupiedTillDate(Date occupiedTillDate) {
		this.occupiedTillDate = occupiedTillDate;
	}

	*/

	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;

	private boolean sortAscending = true;

	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}


	public void sortDataList(ActionEvent event) {


		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters -----------------------------------------------------------------------------------



	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------



	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}



	public HtmlSelectBooleanCheckbox getCheckBox() {
		return checkBox;
	}



	public void setCheckBox(HtmlSelectBooleanCheckbox checkBox) {
		this.checkBox = checkBox;
	}


	public String getSelectOneUnitSide() {
		return selectOneUnitSide;
	}

	public void setSelectOneUnitSide(String selectOneUnitSide) {
		this.selectOneUnitSide = selectOneUnitSide;
	}

	public String getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}



	public String getHdnSelectedUnit() {
		return hdnSelectedUnit;
	}

	public void setHdnSelectedUnit(String hdnSelectedUnit) {
		this.hdnSelectedUnit = hdnSelectedUnit;
	}

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = 10;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public String  btnSelect_Click()
    {
    	String methodName ="btnSelect_Click";
    	List<UnitView> unitViewListNew=new ArrayList<UnitView>(0);
    	Long paySchId;
    	Long statusId;
    	String chequeNo;
    	hdnSelectedUnit = null;
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	logger.logInfo(methodName+"|"+"Start...");
    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    	List<UnitView> unitViewList=new ArrayList<UnitView>(0);
    	if(viewMap.containsKey("UNIT_LIST")){
    		unitViewList=(ArrayList<UnitView>)viewMap.get("UNIT_LIST"); 
    	for (UnitView uVL : unitViewList) 
    	{
    	if(uVL.getSelected()!=null && !uVL.equals(""))
    	 {
    		if(uVL.getSelected())
    		{
    			if (hdnSelectedUnit==null){ 
    			
    			hdnSelectedUnit = uVL.getUnitId()+"_"+uVL.getUnitNumber();}
    			else {
    				hdnSelectedUnit = hdnSelectedUnit +","+ uVL.getUnitId()+"_"+uVL.getUnitNumber();
    				
    				
    			}
    			
    			unitViewListNew.add(uVL);
    		}
    	  }
			
		}
    	
    	if(unitViewListNew.size()>0){
    		sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST,unitViewListNew);
    	}

    	if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE))
	        sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_TYPE+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE))
			sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_USAGE+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER))
			sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_NUMBER+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN))
			sessionMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX))
			sessionMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX))
			sessionMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN))
			sessionMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID))
			sessionMap.put(WebConstants.INQUIRY_UNITS.COUNTRY_ID+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID))
			sessionMap.put(WebConstants.INQUIRY_UNITS.STATE_ID+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString());

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID))
			sessionMap.put(WebConstants.INQUIRY_UNITS.CITY_ID+"_2",viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString());

    	
        final String viewId = "/UnitList.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        
        String javaScriptText ="window.opener.populateSelectManyUsers("+
                                                                       (selectOneUnitType==null?"''":"'"+selectOneUnitType+"'")+","+
                                                                       (selectOneUnitUsage==null?"''":"'"+selectOneUnitUsage+"'")+","+
                                                                       (unitNumber==null?"''":"'"+unitNumber+"'")+","+
                                                                       (rentValueMin==null?"''":"'"+rentValueMin+"'")+","+
                                                                       (rentValueMax==null?"''":"'"+rentValueMax+"'")+","+
                                                                       (noOfBedsMax==null?"''":"'"+noOfBedsMax+"'")+","+
                                                                       (noOfBedsMin==null?"''":"'"+noOfBedsMin+"'")+","+
                                                                       (countryId==null?"''":"'"+countryId+"'")+","+
                                                                       (stateId==null?"''":"'"+stateId+"'")+","+
                                                                       (cityId==null?"''":"'"+cityId+"'")
        	                                                          +");window.opener.document.forms[0].submit();window.close();";
        			
                       
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
    }	                                 
    	logger.logInfo(methodName+"|"+"End...");
    	viewMap.clear();
    	//loadDataList();
       return "btnSelect_Click";
    	
   }


	  public void loadCountry()  {
			
		  String methodName="loadCountry"; 
			logger.logInfo(methodName+"|"+"Start");
			try {
				if(viewRootMap.containsKey("countryList")){
					viewRootMap.remove("countryList");
					countryList.clear();
				}
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
			      this.getCountryList().add(item);
			     
			  }
			
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("countryList", countryList);
			 logger.logInfo(methodName+"|"+"Finish");
			}catch (Exception e){
				logger.LogException(methodName+"|Error Occured ",e);
				
			}
		}
	  
		public void loadState()  {
			
			String methodName="loadState"; 
			logger.logInfo(methodName+"|"+"Start");
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			if(viewRootMap.containsKey("stateList")){
				viewRootMap.remove("stateList");
				stateList.clear();
			}
			
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(countryId)==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	
			
			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      this.getStateList().add(item);
			      
			      
			  }
			
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
			 logger.logInfo(methodName+"|"+"Finish");
			}catch (Exception e){
				
				logger.LogException(methodName+"|Error Occured ",e);
				
			}
		}
		
		public void  loadCity(){
			String methodName="loadCity"; 
			logger.logInfo(methodName+"|"+"Start"); 
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			if(viewRootMap.containsKey("cityList")){
				viewRootMap.remove("cityList");
				cityList.clear();
			}
				
				try 
				{
					
					 Set<RegionView> regionViewList = null;
					 if(stateId!=null && new Long(stateId).compareTo(new Long(-1))!=0)
					 regionViewList =  psa.getCity(new Long(stateId));
				          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
			  	    
					
					Iterator itrator = regionViewList.iterator();

					for(int i=0;i<regionViewList.size();i++)
					  {
						  RegionView rV=(RegionView)itrator.next();
						  SelectItem item;
						  if (getIsEnglishLocale())
						  {
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
						  else 
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
					      this.getCityList().add(item);
					      
					      
					  }
				
					 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
					 logger.logInfo(methodName+"|"+"Finish");
				}
				catch(Exception ex)
				{
					logger.LogException(methodName+"|Error Occured ",ex);
					
				}
			   
		   }

		

		public void setArabicLocale(boolean isArabicLocale) {
			this.isArabicLocale = isArabicLocale;
		}


		public void setEnglishLocale(boolean isEnglishLocale) {
			this.isEnglishLocale = isEnglishLocale;
		}
	
		public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
		public boolean getIsEnglishLocale()
		{

			WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
			return isEnglishLocale;
		}

		public List<SelectItem> getCountryList() {
			if(viewRootMap.containsKey("countryList")){
				//countryList.clear();
				countryList= (List<SelectItem>) (viewRootMap.get("countryList"));
			}
			return countryList;
			
		}

		public void setCountryList(List<SelectItem> countryList) {
			this.countryList = countryList;
		}

		public List<SelectItem> getStateList() {
			if(viewRootMap.containsKey("stateList")){
				//stateList.clear();
				stateList= (List<SelectItem>) (viewRootMap.get("stateList"));
			}
			return stateList;	
			
			
		}

		public void setStateList(List<SelectItem> stateList) {
			this.stateList = stateList;
			
		}

		public List<SelectItem> getCityList() {
			if(viewRootMap.containsKey("cityList")){
				//cityList.clear();
				cityList= (List<SelectItem>) (viewRootMap.get("cityList"));
				
			}
			return cityList;	
			
		}

		public void setCityList(List<SelectItem> cityList) {
			this.cityList = cityList;
			
		}


		public String getSelectOneUnitType() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE))
				selectOneUnitType=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString();
			return selectOneUnitType;
		}

		public void setSelectOneUnitType(String selectOneUnitType) {
			this.selectOneUnitType = selectOneUnitType;
			if(this.selectOneUnitType !=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_TYPE,this.selectOneUnitType );
		}

		public String getSelectOneUnitUsage() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE))
				selectOneUnitUsage=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString();
			return selectOneUnitUsage;
			
		}

		public void setSelectOneUnitUsage(String selectOneUnitUsage) {
			this.selectOneUnitUsage = selectOneUnitUsage;
			if(this.selectOneUnitUsage!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_USAGE,this.selectOneUnitUsage );
		}

		public String getUnitNumber() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER))
				unitNumber=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString();
			return unitNumber;
		}

		public void setUnitNumber(String unitNumber) {
			this.unitNumber = unitNumber;
			if(this.unitNumber!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_NUMBER,this.unitNumber );
			
		}

		public String getRentValueMin() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN))
				rentValueMin=viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString();
			return rentValueMin;
			
		}

		public void setRentValueMin(String rentValueMin) {
			this.rentValueMin = rentValueMin;
			if(this.rentValueMin!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN,this.rentValueMin );
			
		}

		public String getRentValueMax() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX))
				rentValueMax=viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString();
			return rentValueMax;
		}

		public void setRentValueMax(String rentValueMax) {
			this.rentValueMax = rentValueMax;
			if(this.rentValueMax!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX,this.rentValueMax );
		}

		public String getNoOfBedsMax() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX))
				noOfBedsMax=viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString();
			return noOfBedsMax;
		}

		public void setNoOfBedsMax(String noOfBedsMax) {
			this.noOfBedsMax = noOfBedsMax;
			if(this.noOfBedsMax!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX,this.noOfBedsMax );
		}

		public String getNoOfBedsMin() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN))
				noOfBedsMin=viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString();
			return noOfBedsMin;
		}

		public void setNoOfBedsMin(String noOfBedsMin) {
			this.noOfBedsMin = noOfBedsMin;
			if(this.noOfBedsMin!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN,this.noOfBedsMin );
		}

		public String getCountryId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID))
				countryId=viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString();
			return countryId;
		}

		public void setCountryId(String countryId) {
			this.countryId = countryId;
			if(this.countryId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.COUNTRY_ID,this.countryId);
		}

		public String getStateId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID))
				stateId=viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString();
			return stateId;
		}

		public void setStateId(String stateId) {
			this.stateId = stateId;
			if(this.stateId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.STATE_ID,this.stateId);
		}

		public String getCityId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID))
				cityId=viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString();
			return cityId;
		}

		public void setCityId(String cityId) {
			this.cityId = cityId;
			if(this.cityId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.CITY_ID,this.cityId);
		}
	
}
