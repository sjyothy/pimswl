package com.avanza.pims.web.backingbeans;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlInputText;
import org.apache.myfaces.component.html.ext.HtmlInputTextarea;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.DocumentManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DocumentFolderView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.ui.util.ResourceUtil;


public class UploadFileBean extends AbstractController {

	private static Logger logger = Logger.getLogger(UploadFileBean.class);

	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	private HtmlPanelGrid uploadPanel = new HtmlPanelGrid();
	private HtmlSelectOneMenu docTypeCombo = new HtmlSelectOneMenu();
	private HtmlInputText documentDescriptionField = new HtmlInputText();
	private HtmlInputTextarea commentsField = new HtmlInputTextarea();
	private String fileNetPath;
	DocumentManager documentManager = new DocumentManager();

	private UploadedFile selectedFile;

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	private final Long FILE_MAX_SIZE = 20971520L;
										
	private final String FILE_MAX_SIZE_STR = "20MB";
	private final Long FILE_MIN_SIZE = 1L;
	public Boolean scannerImageSelected;

	public UploadFileBean (){

	}

	@Override
	public void init()
    {
		super.init();
   	 	if(!isPostBack())
   	 	{
   	 	    sessionMap.remove("SCANNED_IMAGE");
   	 		setMode();
   	 		setValues();
   	 	    this.setScannerImageSelected(false);
   	 	}
   	 	else
   	 	{
   	 	   if(sessionMap.containsKey("SCANNED_IMAGE"))
	 	    this.setScannerImageSelected(true);
   	 	}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
    @SuppressWarnings("unchecked")
	public void setMode()
    {
		try 
		{
			infoMessage = (String)sessionMap.remove("infoMessage");
			if(infoMessage!=null)
			{
				viewMap.put("infoMessage", infoMessage);
			}
			
			
			Boolean isProcDoc = ((Boolean)sessionMap.get("isProcDoc")== null )?false:(Boolean)sessionMap.remove("isProcDoc");
			viewMap.put("isProcDoc", isProcDoc);
			if(isProcDoc)
			{
				DocumentView selectedDoc = (DocumentView) sessionMap.remove("selectedDoc");
				if(selectedDoc!=null)
				{
					viewMap.put("selectedDoc", selectedDoc);
				}

				int procDocIndex = ((Integer)sessionMap.get("procDocIndex")==null)? -1 : ((Integer)sessionMap.remove("procDocIndex")).intValue();
				viewMap.put("procDocIndex", procDocIndex);
			}

			String repositoryId = (String)sessionMap.remove("repositoryId");
			if(repositoryId!=null)
			{
				viewMap.put("repositoryId", repositoryId);
			}
			
			String externalId = (String)sessionMap.remove("externalId");
			if(externalId!=null)
			{
				viewMap.put("externalId", externalId);
			}
		
			String associatedObjectId = (String)sessionMap.remove("associatedObjectId");
			if(associatedObjectId!=null)
			{
				viewMap.put("associatedObjectId", associatedObjectId);

			}
		}
		catch (Exception ex) 
		{
			logger.LogException("setMode crashed... ", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    @SuppressWarnings("unchecked")
	public void openScanPopup(javax.faces.event.ActionEvent event){
		try
		{

			String repositoryId = (String) viewMap.get("repositoryId");
			String externalId = (String) viewMap.get("externalId");
			String associatedObjectId = (String) viewMap.get("associatedObjectId");
			sessionMap.put("repositoryId", repositoryId);
			sessionMap.put("externalId", externalId);
			sessionMap.put("associatedObjectId", associatedObjectId);
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showScanPopup();";
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
		catch (Exception exception) {
			logger.LogException("openUploadPopup() crashed ", exception);
		}
    }

	public void setValues(){
		try {
			Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");

			if(!isProcDoc){

			}
			else{

			}

		} catch (Exception ex) {
			logger.LogException("setValues crashed... ", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}


	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	@SuppressWarnings("unchecked")
	private void applyMode()
	{
		try
		{
			fileUploadCtrl.setUploadedFile(getSelectedFile());
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void cancel(ActionEvent event) {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";

        sessionMap.remove("success");
		sessionMap.remove("documentView");
		sessionMap.remove("isProcDoc");
		sessionMap.remove("updateObjectIdRequired");
		sessionMap.remove("selectedDoc");
		sessionMap.remove("procDocIndex");

        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

    }
	@SuppressWarnings("unchecked")
	private Boolean validateForUpload()
	{
	    Boolean validated = true;
    	try
 		{
    		if(!getIsProcDoc()){
    			if(docTypeCombo.getValue().toString().equals("0")){
    				errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Attachment.DOC_TYPE));
    				validated = false;
    			}
    			String name = (String) documentDescriptionField.getValue();
    			if(StringHelper.isEmpty(name))
    			{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Attachment.DOC_NAME));
					validated = false;
				}
    		}

//    		UploadedFile file = fileUploadCtrl.getUploadedFile();
    		UploadedFile file = getSelectedFile();
    		FileItem fileItem=null;
    		if(sessionMap.containsKey("SCANNED_IMAGE"))
    		{
    			logger.logInfo( "Session map has key : SCANNED_IMAGE" );
    			fileItem =(FileItem)sessionMap.get("SCANNED_IMAGE");
    		}

    		if(file!=null)
    		{
    			logger.logInfo("file!=null");
    			Long size = file.getSize();
    			String fileName = file.getName();
    			File temp = new File(fileName);
    			logger.logInfo(temp.getName() + " exists : " + temp.exists());
    			if(size>FILE_MAX_SIZE)
    			{
    				errorMessages.add(
    									java.text.MessageFormat.format(
    																	ResourceUtil.getInstance().getProperty( MessageConstants.Attachment.MSG_FILE_SIZE_TOO_LARGE ), 
    																	FILE_MAX_SIZE_STR
    																  )
    								  );
					validated = false;
					viewMap.remove("selectedFile");
    			}
    			else if(size<FILE_MIN_SIZE)
    			{
	    				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_FILE_SIZE_TOO_SMALL));
						validated = false;
						viewMap.remove("selectedFile");
    			}

    		}
    		else if(fileItem!=null)
    		{
    			logger.logInfo("fileItem!=null");
				Long size = fileItem.getSize();
				String fileName = fileItem.getName();
				File temp = new File(fileName);
				logger.logInfo(temp.getName() + " exists : " + temp.exists());

    			if(size>FILE_MAX_SIZE)
    			{
    				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.Attachment.MSG_FILE_SIZE_TOO_LARGE, FILE_MAX_SIZE_STR));
					validated = false;
					viewMap.remove("selectedFile");
    			}
    			else if(size<FILE_MIN_SIZE)
    			{
	    				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_FILE_SIZE_TOO_SMALL));
						validated = false;
						viewMap.remove("selectedFile");
    			}

    		}
    		else{
    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
    			validated = false;
    		}

    		if(StringUtils.isNotBlank(fileNetPath)){
    			
    		}
    		
    		logger.logInfo("File Name: %s | File Type: %s | File Size: %s | Procedure Document = %s", file!=null?file.getName():"<File Not Selected>", file!=null?file.getContentType():"<File Not Selected>", file!=null?file.getSize():"<File Not Selected>", getIsProcDoc()?"Yes":"No");
		}
		catch (Exception exception) {
			logger.LogException("validateForUpload() crashed ", exception);
		}
		return validated;
    }

	@SuppressWarnings("unchecked")
	public void uploadFile(javax.faces.event.ActionEvent event)
	{
		try
 		{
			viewMap.remove("infoMessage");
			if(!validateForUpload()){return;}
			
			Long documentId = upload();
			logger.logInfo(" document id " + documentId);
			if(documentId==null)
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
				return;
			}
			sessionMap.put("success", true);
			
			if(viewMap.containsKey("updateObjectIdRequired"))
			{
				sessionMap.put("updateObjectIdRequired",true);
			}

			int procDocIndex = ((Integer)viewMap.get("procDocIndex")==null)? -1 : ((Integer)viewMap.get("procDocIndex")).intValue();
			sessionMap.put("procDocIndex", procDocIndex);
			sessionMap.put("reloadOpener", true);
			
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:closeWindowSubmit();";

	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
		catch (Exception exception) 
		{
			logger.LogException("uploadFile(ActionEvent) crashed ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	public void uploadFileSave(javax.faces.event.ActionEvent event)
	{
		logger.logInfo("uploadFile(ActionEvent) started...");
		try
 		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        //String javaScriptText = "javascript:UploadClick();";

			viewMap.remove("infoMessage");
			if(validateForUpload())
			{
				Long documentId = upload();
				if(documentId!=null)
				{
					sessionMap.put("success", true);
					if(viewMap.containsKey("updateObjectIdRequired"))
						sessionMap.put("updateObjectIdRequired",true);

					int procDocIndex = ((Integer)viewMap.get("procDocIndex")==null)? -1 : ((Integer)viewMap.get("procDocIndex")).intValue();
					sessionMap.put("procDocIndex", procDocIndex);

					String javaScriptText = "javascript:closeWindowSubmit();";
					   // Add the Javascript to the rendered page's header for immediate execution
			        AddResource addResource = AddResourceFactory.getInstance(facesContext);
			        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

				}

				else
				{
	    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
				}

			}
			logger.logInfo("uploadFile(ActionEvent) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("uploadFile(ActionEvent) crashed ", exception);
		}

	}
	@SuppressWarnings("unchecked")
	public Long upload()
	{
		Long documentId = null;
		byte[] fileByteArray = null;
		byte[] encodedFileByteArray = null;
		String base64EncodedString = null;
		DocumentView documentView = null;
		try
		{

			UploadedFile file =  getSelectedFile();

			if(file != null)
			{
				Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");
				documentView = new DocumentView();
				if(isProcDoc)
					documentView = (DocumentView)viewMap.get("selectedDoc");
				String updatedBy = CommonUtil.getLoggedInUser();
				String createdBy = CommonUtil.getLoggedInUser();
				Date updatedOn = new Date();
				Date createdOn = new Date();
				String repositoryId = (String) viewMap.get("repositoryId");
				String externalId = (String) viewMap.get("externalId");
				String associatedObjectId = (String) viewMap.get("associatedObjectId");
				logger.logInfo("repositoryId : %s | externalId : %s | associatedObjectId : %s ", repositoryId, externalId, associatedObjectId);

				if(StringHelper.isEmpty(associatedObjectId))
				{
					viewMap.put("updateObjectIdRequired", true);
				}
				String documentFileName = getFileNameForPath(file.getName());
				String documentPath = file.getName();
				String documentTitle = getDocumentDescription();
				String comments = getComments();
				String contentType = file.getContentType();
				Long sizeInBytes = file.getSize();
				documentId = documentView.getDocumentId();

				logger.logInfo("documentFileName : %s | contentType : %s | sizeInBytes : %s ", documentFileName, contentType, sizeInBytes);

				documentView.setUpdatedBy(updatedBy);
				documentView.setUpdatedOn(updatedOn);
				documentView.setCreatedOn(createdOn);
				documentView.setCreatedBy(createdBy);
				
				documentView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				documentView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

				documentView.setDocumentId(documentId);
				documentView.setAssociatedObjectId(associatedObjectId);
				documentView.setDocumentFileName(documentFileName);
				documentView.setDocumentPath(documentPath);
				documentView.setContentType(contentType);
				documentView.setComments(comments);
				documentView.setDocTypeId(Convert.toLong(getDocTypeId()));
				documentView.setExternalId(externalId);
				documentView.setRepositoryId(repositoryId);
				documentView.setSizeInBytes(sizeInBytes);
				documentView.setIsProcDoc(isProcDoc);
				if(!isProcDoc)
				{
					documentView.setDocumentTitle(documentTitle);
				}
				UtilityServiceAgent usa = new UtilityServiceAgent();
				if(file.getInputStream().available()!=0)
				{
					 
					fileByteArray = file.getBytes();
					encodedFileByteArray = Base64.encodeBase64(fileByteArray);
					base64EncodedString = new String(encodedFileByteArray);
					documentView = usa.addAttachment(documentView, base64EncodedString);

					sessionMap.put("documentView", documentView);
					sessionMap.put("isProcDoc", isProcDoc);
				}
				documentId = documentView.getDocumentId();
			 }
			else if(sessionMap.containsKey("SCANNED_IMAGE"))
			{
				logger.logInfo(" scanned image is to be uploaded ");
				FileItem fileItem=null;

				fileItem =(FileItem)sessionMap.get("SCANNED_IMAGE");

				Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");
				documentView = new DocumentView();
				if(isProcDoc)
					documentView = (DocumentView)viewMap.get("selectedDoc");



				String updatedBy = CommonUtil.getLoggedInUser();
				String createdBy = CommonUtil.getLoggedInUser();
				Date updatedOn = new Date();
				Date createdOn = new Date();

				String repositoryId = (String) viewMap.get("repositoryId");
				String externalId = (String) viewMap.get("externalId");
				String associatedObjectId = (String) viewMap.get("associatedObjectId");

				logger.logInfo("repositoryId : %s | externalId : %s | associatedObjectId : %s ", repositoryId, externalId, associatedObjectId);

				if(StringHelper.isEmpty(associatedObjectId))
					viewMap.put("updateObjectIdRequired", true);

				String documentFileName = getFileNameForPath(fileItem.getName());
				String documentPath = fileItem.getName();
				String documentTitle = getDocumentDescription();
				String comments = getComments();
				String contentType = fileItem.getContentType();
				Long sizeInBytes = fileItem.getSize();
				documentId = documentView.getDocumentId();

				logger.logInfo("documentFileName : %s | contentType : %s | sizeInBytes : %s ", documentFileName, contentType, sizeInBytes);

				documentView.setUpdatedBy(updatedBy);
				documentView.setUpdatedOn(updatedOn);
				documentView.setCreatedOn(createdOn);
				documentView.setCreatedBy(createdBy);
				documentView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				documentView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

				documentView.setDocumentId(documentId);
				documentView.setAssociatedObjectId(associatedObjectId);
				documentView.setDocumentFileName(documentFileName);
				documentView.setDocumentPath(documentPath);
				documentView.setContentType(contentType);
				documentView.setComments(comments);
				documentView.setDocTypeId(Convert.toLong(getDocTypeId()));
				documentView.setExternalId(externalId);
				documentView.setRepositoryId(repositoryId);
				documentView.setSizeInBytes(sizeInBytes);
				documentView.setIsProcDoc(isProcDoc);
				if(!isProcDoc)
					documentView.setDocumentTitle(documentTitle);

				UtilityServiceAgent usa = new UtilityServiceAgent();
				if(fileItem.getInputStream().available()!=0)
				{
					//InputStream inputStream  =  file.getInputStream();
					//BufferedInputStream bis = new BufferedInputStream(inputStream);

					fileByteArray = fileItem.get();
					encodedFileByteArray = Base64.encodeBase64(fileByteArray);
					base64EncodedString = new String(encodedFileByteArray);
					documentView = usa.addAttachment(documentView, base64EncodedString);

					sessionMap.put("documentView", documentView);
					sessionMap.put("isProcDoc", isProcDoc);
				}
				documentId = documentView.getDocumentId();
				
			}
			else
				logger.logInfo("no file selected for upload!!!");

			if( documentId != null &&  viewMap.get("associatedObjectId") != null )
			{
				
				DocumentFolderView documentFolderView = new DocumentFolderView();
				documentFolderView.setExternalId((String) viewMap.get("externalId"));
				documentFolderView.setAssociatedObjectId((String) viewMap.get("associatedObjectId"));
				documentFolderView.setDocumentId(documentId);
				
				if((documentManager.getFolderName(documentFolderView)))
					documentManager.uploadeFileNetDoc(documentId );
			}
			logger.logInfo("upload completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("upload crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAILURE));
		}
		finally
		{
			sessionMap.remove("SCANNED_IMAGE");
			fileByteArray = null;
			encodedFileByteArray = null;
			base64EncodedString = null;
			documentView = null;
		}
		return documentId;
	}


	private String getFileNameForPath (String path) {

        if ( path == null || path.equalsIgnoreCase("") ) {
            return path;
         }
        return path.substring( path.lastIndexOf("\\") + 1, path.length() );
    }

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat(){
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(StringHelper.isNotEmpty(infoMessage))
			temp.add(infoMessage);
//		if(!getIsProcDoc())
//			return null;
		return CommonUtil.getErrorMessages(temp);
	}

	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}

	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}

	public HtmlPanelGrid getUploadPanel() {
		return uploadPanel;
	}

	public void setUploadPanel(HtmlPanelGrid uploadPanel) {
		this.uploadPanel = uploadPanel;
	}

	public HtmlSelectOneMenu getDocTypeCombo() {
		return docTypeCombo;
	}

	public void setDocTypeCombo(HtmlSelectOneMenu docTypeCombo) {
		this.docTypeCombo = docTypeCombo;
	}

	public HtmlInputText getDocumentDescriptionField() {
		return documentDescriptionField;
	}

	public void setDocumentDescriptionField(HtmlInputText documentDescriptionField) {
		this.documentDescriptionField = documentDescriptionField;
	}

	public HtmlInputTextarea getCommentsField() {
		return commentsField;
	}

	public void setCommentsField(HtmlInputTextarea commentsField) {
		this.commentsField = commentsField;
	}


	public Boolean getIsProcDoc(){
		Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");
		if(isProcDoc==null)
			isProcDoc = false;
		return isProcDoc;
	}

	@SuppressWarnings("unchecked")
	public String getDocumentDescription() {
		String temp = (String)viewMap.get("documentDescription");
		if(temp==null)
			temp = "";
		return temp.trim();
	}

	@SuppressWarnings("unchecked")
	public void setDocumentDescription(String documentDescription) {
		if(documentDescription!=null)
			viewMap.put("documentDescription" , documentDescription);
	}
	@SuppressWarnings("unchecked")
	public String getComments() {
		String temp = (String)viewMap.get("comments");
		return temp;
	}
	@SuppressWarnings("unchecked")
	public void setComments(String comments) {
		if(comments!=null)
			viewMap.put("comments" , comments);
	}

	@SuppressWarnings("unchecked")
	public String getDocTypeId() {
		String docTypeId = (String)viewMap.get("docTypeId");
		if(docTypeId==null)
			docTypeId = "0";
		return docTypeId;
	}
	@SuppressWarnings("unchecked")
	public void setDocTypeId(String docTypeId) {
		if(docTypeId!=null)
			viewMap.put("docTypeId",docTypeId);
	}

	public UploadedFile getSelectedFile() {
		UploadedFile selectedFile = (UploadedFile)viewMap.get("selectedFile");
		return selectedFile;
	}
	@SuppressWarnings("unchecked")
	public void setSelectedFile(UploadedFile selectedFile) {
		if(selectedFile!=null){
			viewMap.put("selectedFile", selectedFile);
		}
	}
	@SuppressWarnings("unchecked")
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	@SuppressWarnings("unchecked")
	public Boolean getScannerImageSelected() {
		if(viewMap.containsKey("SCANNER_IMAGE_SELECTED"))
			scannerImageSelected = (Boolean)viewMap.get("SCANNER_IMAGE_SELECTED");
		return scannerImageSelected;
	}
	@SuppressWarnings("unchecked")
	public void setScannerImageSelected(Boolean scannerImageSelected) {
		this.scannerImageSelected = scannerImageSelected;
		if(this.scannerImageSelected!=null)
			viewMap.put("SCANNER_IMAGE_SELECTED",this.scannerImageSelected);
	}

	/**
	 * @return the fileNetPath
	 */
	public String getFileNetPath() {
		return fileNetPath;
	}

	/**
	 * @param fileNetPath the fileNetPath to set
	 */
	public void setFileNetPath(String fileNetPath) {
		this.fileNetPath = fileNetPath;
	}

}
