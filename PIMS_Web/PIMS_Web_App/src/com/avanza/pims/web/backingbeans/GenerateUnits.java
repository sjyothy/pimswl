package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class GenerateUnits extends AbstractController {

	public class Messages {
		public static final String REQUIRED_NO_OF_UNITS = "generateUnits.messages.NoOfUnitsRequired";
		public static final String REQUIRED_UNIT_NUM_FROM = "generateUnits.messages.UnitNumFromRequired";
		public static final String REQUIRED_PROPERTY_FLOOR = "generateUnits.messages.floorRequired";	
		public static final String REQUIRED_UNIT_NUMBER_PREFIX = "generateUnits.messages.UnitNumberPrefixRequired";
		public static final String NO_OF_UNITS_POSITIVE_WHOLE_NUM = "generateUnits.messages.NoOfUnitsPositiveWholeNum";
		public static final String UNIT_NUM_FROM_POSITIVE_WHOLE_NUM = "generateUnits.messages.UnitNumFromPositiveWholeNum";
		public static final String UNITS_GENERATED_SUCCESSFULLY = "generateUnits.messages.UnitsGeneratedSuccess";
		public static final String UNITS_UPDATED_SUCCESSFULLY = "generateUnits.messages.UnitsUpdatedSuccess";
		
		public static final String UNITS_GENERATED_GENERAL_ERROR = "generateUnits.messages.generalError";
		public static final String UNITS_UPDATE_VALUE="commons.Update";
		public static final String UNITS_ADD_VALUE="commons.Add";
		
		public static final String UNITS_GENERATED_GENERAL_ALREADY_EXISTS = "generateUnits.messages.unitsAlreadyExists";
	}

	// Fields

	private transient Logger logger = Logger.getLogger(SelectWinner.class);

	private HtmlInputText inputTextPropertyName = new HtmlInputText();
	private HtmlInputText inputTextNumberOfUnits = new HtmlInputText();	
	private HtmlInputText inputTextUnitNumberPrefix = new HtmlInputText();
	private HtmlInputText inputTextUnitNumberFrom = new HtmlInputText();
	private HtmlInputText inputTextUnitNumberTo = new HtmlInputText();
	private HtmlInputHidden hdnTextUnitNumberTo = new HtmlInputHidden();
	private HtmlInputHidden hdnTextPropertyId = new HtmlInputHidden();
	private HtmlInputHidden hdnTextPropertyName = new HtmlInputHidden();
	private HtmlInputHidden hdnTextTypeId = new HtmlInputHidden();
	private HtmlInputHidden hdnTextInvestmentPurposeId = new HtmlInputHidden();
	private HtmlInputHidden hdnTextUsageTypeId = new HtmlInputHidden();
	private HtmlSelectOneMenu selectOneMenu = new HtmlSelectOneMenu();
	private String unitSideTypeId;
	private String floorId;
	private HtmlCommandButton commandButtonSearch;
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;

	private List<String> messages;
	private List<String> error_Messages;
	private List<String> error_Messages_ub;
	private List<SelectItem> propertyFloors;
	
	// Constructor
	/** default constructor */
	public GenerateUnits() {
	}

	// Methods
	public List<SelectItem> getPropertyFloors() {
		
		logger.logInfo("in getPropertyFloors");
		if (propertyFloors == null || propertyFloors.size() == 0) {
			propertyFloors = new ArrayList<SelectItem>();
			Long propertyId = Convert.toLong(hdnTextPropertyId.getValue());
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			try {
				logger.logInfo("trying to get property floors");
				
				ArrayList<FloorView> floors = psAgent
						.getPropertyFloors(propertyId);
				
				logger.logInfo("property floors got");
				
				if (floors != null) {
					for (FloorView fView : floors) {
						SelectItem item = new SelectItem(fView.getFloorId()
								.toString(), fView.getFloorNumber());
						propertyFloors.add(item);
					}
				}
			} catch (PimsBusinessException e) {

				logger.logError("getPropertyFloors crashed",e);				
			}

		}
		logger.logInfo("getPropertyFloors completed successfully");
		return propertyFloors;
	}

	public void setPropertyFloors(List<SelectItem> propertyFloors) {
		this.propertyFloors = propertyFloors;
	}
	
	private String getLoggedInUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public boolean isValidPositiveLong(String val) {
		boolean isValidPositiveLong = true;

		try {
			Long lVal = Long.parseLong(val);
			if (lVal < 0)
				isValidPositiveLong = false;
		} catch (Exception exp) {
			isValidPositiveLong = false;
		}

		return isValidPositiveLong;
	}

	public boolean ValidateScreen() {
		
		String method = "ValidateScreen";
		logger.logInfo(method + " started...");
		boolean isValid = true;

		try
		{
		Object noOfUnits = inputTextNumberOfUnits.getValue();
		Object unitNumberFrom = inputTextUnitNumberFrom.getValue();
		Object unitNumberTo = hdnTextUnitNumberTo.getValue();		
		Object unitNumberPrefix = inputTextUnitNumberPrefix.getValue()
				.toString();
		
		
		
		Object floor = getFloorId();
		
		error_Messages = new ArrayList<String>();

		if (noOfUnits == null || noOfUnits.toString().equals("")) {
			error_Messages.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.REQUIRED_NO_OF_UNITS));
			isValid = false;
		} else {
			if (!(isValidPositiveLong(noOfUnits.toString()))) {
				error_Messages
						.add(ResourceUtil
								.getInstance()
								.getProperty(
										GenerateUnits.Messages.NO_OF_UNITS_POSITIVE_WHOLE_NUM));
				isValid = false;
			}
		}
		if (unitNumberFrom == null || unitNumberFrom.toString().equals("")) {
			error_Messages.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.REQUIRED_UNIT_NUM_FROM));
			isValid = false;
		} else {
			if (!(isValidPositiveLong(unitNumberFrom.toString()))) {
				error_Messages
						.add(ResourceUtil
								.getInstance()
								.getProperty(
										GenerateUnits.Messages.UNIT_NUM_FROM_POSITIVE_WHOLE_NUM));
			}
		}

		if (unitNumberPrefix == null
				|| unitNumberPrefix.toString().equals("")) {
			error_Messages.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.REQUIRED_UNIT_NUMBER_PREFIX));
			isValid = false;
		}
		if (floor == null
				|| floor.toString().equals("")) {
			error_Messages.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.REQUIRED_PROPERTY_FLOOR));
			isValid = false;
		}
		logger.logInfo(method + " completed...");		
		}
		catch(Exception ex)
		{
			logger.logError("Crashed while validating:", ex);			
			error_Messages_ub.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ERROR));
			
			isValid = false;
		}		
		
		return isValid;
	}

	private Long getEquivalentDomainDataIdForUnit(Long id,String domainTypeProperty,String domainTypeUnit)
	{
		Long unitId = null;
		
		if(id != null)
		{
			ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			List<DomainTypeView> list = (List<DomainTypeView>) context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
			Iterator<DomainTypeView> itr = list.iterator();
			DomainTypeView unitDomainType = null;
			DomainTypeView propertyDomainType = null;
			while(itr.hasNext())
			{
				DomainTypeView dtv = itr.next();
				if( dtv.getTypeName().compareTo(domainTypeProperty)==0 ) {
				
					propertyDomainType = dtv;
				}
				else if(dtv.getTypeName().compareTo(domainTypeUnit)==0 )
				{
					unitDomainType = dtv;
				}
				
				if(unitDomainType != null && propertyDomainType != null)
					break;
					
			}
			
			if(unitDomainType != null && propertyDomainType != null)
			{
				Set<DomainDataView> propertyDomainDatas  = propertyDomainType.getDomainDatas();
				for(DomainDataView ddvProperty:propertyDomainDatas)
				{
					if(ddvProperty.getDomainDataId().longValue() == id.longValue())
					{
						String propertyDataVal = ddvProperty.getDataValue();						
						Set<DomainDataView> unitDomainDatas  = unitDomainType.getDomainDatas();
						
						for(DomainDataView ddvunit:unitDomainDatas)
						{
							if(ddvunit.getDataValue().equals(propertyDataVal))
							{
								unitId = ddvunit.getDomainDataId();
								break;
							}
						}
						
						break;
					}
				}
			}
			
				
		}
		
		return unitId;
	}
	public String generatePropertyUnits() {
		
		String method = "generatePropertyUnits";
		logger.logInfo(method + " started...");
		try {
			if (ValidateScreen()) {		
				logger.logInfo("validation has been done");
				String loggedInUser = getLoggedInUser();
				Long propertyId = Convert.toLong(hdnTextPropertyId.getValue());
				int noOfUnits = Convert.toInteger(inputTextNumberOfUnits
						.getValue());
				int unitNumberFrom = Convert.toInteger(inputTextUnitNumberFrom
						.getValue());
				int unitNumberTo = noOfUnits + unitNumberFrom - 1;
				String unitNumberPrefix = inputTextUnitNumberPrefix.getValue()
						.toString();
				Long floorId = Convert.toLong(getFloorId()) ;
				Long unitSideTypeId = Convert.toLong(getUnitSideTypeId());
				
				Long propertyInvestmentTypeId = Convert.toLong(hdnTextInvestmentPurposeId.getValue());
				Long propertyTypeId = Convert.toLong(hdnTextTypeId.getValue());
				Long propertyUsageTypeId = Convert.toLong(hdnTextUsageTypeId.getValue());
				
				Long unitInvestmentTypeId = getEquivalentDomainDataIdForUnit(propertyInvestmentTypeId,WebConstants.INVESTMENT_POURPOSE,WebConstants.UNIT_INVESTMENT_TYPE);
				Long unitTypeId = getEquivalentDomainDataIdForUnit(propertyTypeId,WebConstants.PROPERTY_TYPE,WebConstants.UNIT_TYPE);
				Long unitUsageTypeId = getEquivalentDomainDataIdForUnit(propertyUsageTypeId,WebConstants.PROPERTY_USAGE,WebConstants.UNIT_USAGE_TYPE);

				PropertyServiceAgent psAgent = new PropertyServiceAgent();

				ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();

				for (int index = unitNumberFrom; index <= unitNumberTo; index++) {

					UnitView propertyUnitView = new UnitView();
					propertyUnitView.setUnitNo(new Double(index));
					propertyUnitView.setCreatedBy(loggedInUser);
					propertyUnitView.setCreatedOn(new Date());
					propertyUnitView.setUpdatedBy(loggedInUser);
					propertyUnitView.setUpdatedOn(new Date());
					propertyUnitView.setIsDeleted(new Long(0));
					propertyUnitView.setRecordStatus(new Long(1));
					propertyUnitView.setUnitSideTypeId(unitSideTypeId);
					propertyUnitView.setUnitNumber(unitNumberPrefix + "-"
							+ index);
					propertyUnitView
							.setInvestmentTypeId(unitInvestmentTypeId);
					propertyUnitView.setUnitTypeId(unitTypeId);
					propertyUnitView.setUsageTypeId(unitUsageTypeId);
					propertyUnitView.setFloorId(floorId);
					propertyUnitView.setRequiredPrice(new Double(0));
					propertyUnitView.setRentValue(new Double(0));
					propertyUnitView.setUnitArea(new Double(0));
					propertyUnitView.setNoOfBed(new Long(0));
					propertyUnitView.setNoOfBath(new Long(0));
					propertyUnitView.setNoOfLiving(new Long(0));

					propertyUnitViews.add(propertyUnitView);
				}

				propertyUnitViews = psAgent.generateUnits(propertyId, floorId,
						propertyUnitViews);
				messages = new ArrayList<String>();
				error_Messages_ub = new ArrayList<String>();

				if (propertyUnitViews.size() > 0) {
					error_Messages_ub
							.add(ResourceUtil
									.getInstance()
									.getProperty(
											GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ALREADY_EXISTS));
					for (UnitView unitView : propertyUnitViews) {
						{
							error_Messages_ub.add(unitView.getUnitNumber());
						}
					}
				} else {
					messages
							.add(ResourceUtil
									.getInstance()
									.getProperty(
											GenerateUnits.Messages.UNITS_GENERATED_SUCCESSFULLY));
				}

				inputTextUnitNumberTo.setValue(unitNumberTo);
			}
			
			logger.logInfo(method+"completed");
		} catch (Exception ex) {
			logger.logError("Crashed while generating units:", ex);
			ex.printStackTrace();
			error_Messages_ub.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ERROR));
		}
		return "";
	}

	private void closeWindow() {
		final String viewId = "/GenerateUnits.jsp";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		// Your Java script code here
		String javaScriptText = "javascript:closeWindow();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

	}

	private void setInfo() {
		String methodName = "setInfo";
		logger.logInfo(methodName + " started...");
		try {
			Object obj = getFacesContext().getExternalContext().getSessionMap()
					.get(WebConstants.GenerateUnits.PROPERTY_VIEW);

			if (obj != null) {
				PropertyView property = (PropertyView) obj;
				hdnTextPropertyId.setValue(property.getPropertyId());
				hdnTextPropertyName.setValue(property.getCommercialName());
				hdnTextTypeId.setValue(property.getTypeId());
				hdnTextUsageTypeId.setValue(property.getUsageTypeId());
				hdnTextInvestmentPurposeId.setValue(property.getInvestmentPurposeId());
				inputTextPropertyName.setValue(property.getCommercialName());
			}
			logger.logInfo(methodName + " comleted successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed...", exception);
		}
	}

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if (!isPostBack())
			setInfo();
	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	// Getters and Setters

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}

	/**
	 * @param isEnglishLocale
	 *            the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale
	 *            the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	// This is codo for testing my popup
	public void showGenerateUnitsPopup(ActionEvent event) {

		PropertyView property = new PropertyView();
		property.setPropertyId(new Long(4));
		property.setTypeId(new Long(22002));
		property.setInvestmentPurposeId(new Long(8002));
		property.setUsageTypeId(new Long(6001));
		property.setCommercialName("This is a test property");

		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.GenerateUnits.PROPERTY_VIEW, property);

		final String viewId = "/home.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:GenerateUnitsPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getMessages() {
		String messageList;
		if ((messages == null) || (messages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<B><UL>\n";
			for (String message : messages) {
				messageList = messageList + message + "<BR> \n";
			}
			messageList = messageList + "</UL></B>\n";
		}
		return (messageList);
	}

	public String getErrorMessages() {

		return CommonUtil.getErrorMessages(error_Messages);

	}

	public HtmlInputText getInputTextPropertyName() {
		return inputTextPropertyName;
	}

	public void setInputTextPropertyName(HtmlInputText inputTextPropertyName) {
		this.inputTextPropertyName = inputTextPropertyName;
	}

	public HtmlInputText getInputTextNumberOfUnits() {
		return inputTextNumberOfUnits;
	}

	public void setInputTextNumberOfUnits(HtmlInputText inputTextNumberOfUnits) {
		this.inputTextNumberOfUnits = inputTextNumberOfUnits;
	}

	public HtmlInputText getInputTextUnitNumberPrefix() {
		return inputTextUnitNumberPrefix;
	}

	public void setInputTextUnitNumberPrefix(
			HtmlInputText inputTextUnitNumberPrefix) {
		this.inputTextUnitNumberPrefix = inputTextUnitNumberPrefix;
	}

	public HtmlInputText getInputTextUnitNumberFrom() {
		return inputTextUnitNumberFrom;
	}

	public void setInputTextUnitNumberFrom(HtmlInputText inputTextUnitNumberFrom) {
		this.inputTextUnitNumberFrom = inputTextUnitNumberFrom;
	}

	public HtmlInputText getInputTextUnitNumberTo() {
		return inputTextUnitNumberTo;
	}

	public void setInputTextUnitNumberTo(HtmlInputText inputTextUnitNumberTo) {
		this.inputTextUnitNumberTo = inputTextUnitNumberTo;
	}

	public HtmlInputHidden getHdnTextUnitNumberTo() {
		return hdnTextUnitNumberTo;
	}

	public void setHdnTextUnitNumberTo(HtmlInputHidden hdnTextUnitNumberTo) {
		this.hdnTextUnitNumberTo = hdnTextUnitNumberTo;
	}

	public HtmlSelectOneMenu getSelectOneMenu() {
		return selectOneMenu;
	}

	public void setSelectOneMenu(HtmlSelectOneMenu selectOneMenu) {
		this.selectOneMenu = selectOneMenu;
	}

	public String getUnitSideTypeId() {
		return unitSideTypeId;
	}

	public void setUnitSideTypeId(String unitSideTypeId) {
		this.unitSideTypeId = unitSideTypeId;
	}

	public HtmlInputHidden getHdnTextPropertyId() {
		return hdnTextPropertyId;
	}

	public void setHdnTextPropertyId(HtmlInputHidden hdnTextPropertyId) {
		this.hdnTextPropertyId = hdnTextPropertyId;
	}

	public HtmlInputHidden getHdnTextPropertyName() {
		return hdnTextPropertyName;
	}

	public void setHdnTextPropertyName(HtmlInputHidden hdnTextPropertyName) {
		this.hdnTextPropertyName = hdnTextPropertyName;
	}

	public HtmlInputHidden getHdnTextTypeId() {
		return hdnTextTypeId;
	}

	public void setHdnTextTypeId(HtmlInputHidden hdnTextTypeId) {
		this.hdnTextTypeId = hdnTextTypeId;
	}

	public HtmlInputHidden getHdnTextInvestmentPurposeId() {
		return hdnTextInvestmentPurposeId;
	}

	public void setHdnTextInvestmentPurposeId(
			HtmlInputHidden hdnTextInvestmentPurposeId) {
		this.hdnTextInvestmentPurposeId = hdnTextInvestmentPurposeId;
	}

	public HtmlInputHidden getHdnTextUsageTypeId() {
		return hdnTextUsageTypeId;
	}

	public void setHdnTextUsageTypeId(HtmlInputHidden hdnTextUsageTypeId) {
		this.hdnTextUsageTypeId = hdnTextUsageTypeId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	

}
