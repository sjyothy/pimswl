package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.property.ComplaintService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.MaintenanceRequestView;


@SuppressWarnings("serial")
public class MaintenanceRequestHistoryPopUp  extends AbstractController
{
	public ResourceBundle bundle;
	String dateformat="";
	private HtmlDataTable requestHistoryDataTable;
	private List<MaintenanceRequestView> requestHistoryDataList = new ArrayList<MaintenanceRequestView>();
	private MaintenanceRequestView requestHistoryDataItem = new MaintenanceRequestView();
	
	private static Logger logger = Logger.getLogger( MaintenanceRequestHistoryPopUp.class );
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("rawtypes")
	Map sessionMap;
	@SuppressWarnings("rawtypes")
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	boolean isArabicLocale;
	boolean isEnglishLocale; 
	
	HttpServletRequest request ;
	String  MAINTENANCE_REQUEST_HISTORY_FOR_CONTRACT = "MAINTENANCE_REQUEST_HISTORY_FOR_CONTRACT";   		

	    /**
	     * <p>Callback method that is called whenever a page is navigated to,
	     * either directly via a URL, or indirectly via page navigation.
	     * Override this method to acquire resources that will be needed
	     * for event handlers and lifecycle methods, whether or not this
	     * page is performing post back processing.</p>
	     *
	     * <p>The default implementation does nothing.</p>
	     */
	    @SuppressWarnings("unchecked")
		public void init() 
	    {
	    	
	    	super.init();
	    	request =(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	    	sessionMap =context.getExternalContext().getSessionMap();
	    	viewRootMap.put("recordSize","0");
	    	
	    	try
	    	{
		    	
		    	if( request.getParameter("contractId") != null )
		    	{
		    		logger.logInfo("init|contractId"+request.getParameter("contractId").toString());
		    		requestHistoryDataList= getMaintenanceRequestsForContract(request.getParameter("contractId").toString());
		    	}
		    	else if(request.getParameter("complaintId")!= null)
		    	{
		    		logger.logInfo("init|complaintId"+request.getParameter("complaintId").toString());
		    		requestHistoryDataList= getMaintenanceRequestsForComplaint(request.getParameter("complaintId").toString());
		    	}
		    	viewRootMap.put("recordSize",requestHistoryDataList.size());
		    	viewRootMap.put(MAINTENANCE_REQUEST_HISTORY_FOR_CONTRACT, requestHistoryDataList);
	    	
	    	}	    	
	    	catch(Exception ex)
	    	{
	    		logger.LogException("init", ex);
	    	}
	   }
	    


	    /**
	     * <p>Callback method that is called after the component tree has been
	     * restored, but before any event processing takes place.  This method
	     * will <strong>only</strong> be called on a "post back" request that
	     * is processing a form submit.  Override this method to allocate
	     * resources that will be required in your event handlers.</p>
	     *
	     * <p>The default implementation does nothing.</p>
	     */
	    @Override
	    public void preprocess() {
	    	
	    	
	    }


	    /**
	     * <p>Callback method that is called just before rendering takes place.
	     * This method will <strong>only</strong> be called for the page that
	     * will actually be rendered (and not, for example, on a page that
	     * handled a post back and then navigated to a different page).  Override
	     * this method to allocate resources that will be required for rendering
	     * this page.</p>
	     *
	     * <p>The default implementation does nothing.</p>
	     */
	    public void prerender() {
	    	
	    	
			   
	    	
	    }


	    /**
	     * <p>Callback method that is called after rendering is completed for
	     * this request, if <code>init()</code> was called, regardless of whether
	     * or not this was the page that was actually rendered.  Override this
	     * method to release resources acquired in the <code>init()</code>,
	     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	     * acquired during execution of an event handler).</p>
	     *
	     * <p>The default implementation does nothing.</p>
	     */
	    public void destroy() {
	        ;
	    }



	
	// Constructors

	/** default constructor */
	public MaintenanceRequestHistoryPopUp() 
	{
		
	}
	private ArrayList<MaintenanceRequestView> getMaintenanceRequestsForContract(String requestContractId)throws Exception
	{
		ArrayList<MaintenanceRequestView> requestViewList=new ArrayList<MaintenanceRequestView>(0);
	    if(requestContractId!=null && requestContractId.length()>0)
	    {
	    	requestViewList=new PropertyService().getMaintenanceRequestsForContract(Long.valueOf(requestContractId));
	    
	    }
	    return requestViewList;
	}
	
	private List<MaintenanceRequestView> getMaintenanceRequestsForComplaint(String complaintId)throws Exception
	{
		List<MaintenanceRequestView> requestViewList=new ArrayList<MaintenanceRequestView>(0);
	    if(complaintId!=null && complaintId.length()>0)
	    {
	    	requestViewList=ComplaintService.getMaintenanceRequestsByComplaintId( Long.valueOf(complaintId) );
	    
	    }
	    return requestViewList;
	}
	public Integer getRecordSize() {
		viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object size = viewRootMap.get("recordSize");
		Integer recordSize = null;
		if(size != null && !size.toString().equals("0")){
			recordSize = (Integer)size;
		}
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public HtmlDataTable getRequestHistoryDataTable() {
		return requestHistoryDataTable;
	}


	public void setRequestHistoryDataTable(HtmlDataTable requestHistoryDataTable) {
		this.requestHistoryDataTable = requestHistoryDataTable;
	}


	@SuppressWarnings("unchecked")
	public List<MaintenanceRequestView> getRequestHistoryDataList() {
		if(viewRootMap.containsKey(MAINTENANCE_REQUEST_HISTORY_FOR_CONTRACT))
			requestHistoryDataList=(ArrayList<MaintenanceRequestView>)viewRootMap.get(MAINTENANCE_REQUEST_HISTORY_FOR_CONTRACT);
		return requestHistoryDataList;
	}


	public void setRequestHistoryDataList(List<MaintenanceRequestView> requestHistoryDataList) {
		this.requestHistoryDataList = requestHistoryDataList;
	}


	public MaintenanceRequestView getRequestHistoryDataItem() {
		return requestHistoryDataItem;
	}


	public void setRequestHistoryDataItem(MaintenanceRequestView requestHistoryDataItem) {
		this.requestHistoryDataItem = requestHistoryDataItem;
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	
		
	}
	
	
	@SuppressWarnings("unused")
	private String getLoggedInUser() 
	{
	        context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    
	public ResourceBundle getBundle() {

        if(getIsArabicLocale())

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

        else

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

        return bundle;

  }


  public void setBundle(ResourceBundle bundle) {

        this.bundle = bundle;

  }

    
	
	public String getLocale(){
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode();
		}

	public String getDateformat() {
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		dateformat= localeInfo.getDateFormat();
		return dateformat;
	}


	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}

	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}



	
	
	

}