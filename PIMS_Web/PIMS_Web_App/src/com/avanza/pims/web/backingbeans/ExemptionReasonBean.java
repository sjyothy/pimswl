package com.avanza.pims.web.backingbeans;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.core.web.context.impl.ViewContextImpl;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.controller.AbstractController;

public class ExemptionReasonBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1980791238989860656L;
	private static final String EXEMPTION_REASON_NOTES = "EXEMPTION_REASON";
	
	private static Logger logger = Logger.getLogger(ExemptionReasonBean.class);
	
	@Override
	public void init() {
		super.init();
		initContext();
	}

	@Override
	public void preprocess() {
		super.preprocess();
		initContext();
	}
	
	private void initContext() {
		webContext = ApplicationContext.getContext().get(WebContext.class);
    	requestContext = ApplicationContext.getContext().get(RequestContext.class);
    	viewContext = ApplicationContext.getContext().get(ViewContext.class);

    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	webContext = new WebContextImpl(req.getSession());
    	requestContext = new RequestContextImpl(req);
    	if (getFacesContext().getViewRoot() != null) {
    		viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
    		ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
    	}
    	ApplicationContext.getContext().add(WebContext.class.getName(), webContext);
    	ApplicationContext.getContext().add(RequestContext.class.getName(), requestContext);
	}
	
	public void saveExemptionReason(ActionEvent event){

		logger.logDebug("saveExemptionReason() - action event called.");
		Long requestId = Long.parseLong((String) viewContext.getAttribute("entityId"));
		try {
			NotesController.saveNotes(EXEMPTION_REASON_NOTES, requestId);
			
			   AddResource addResource = AddResourceFactory.getInstance( getFacesContext() );
	           addResource.addInlineScriptAtPosition( getFacesContext(), 
	        		                                  AddResource.HEADER_BEGIN, 
	        		                                  "window.close();"
	        		                                 );
		} catch (PimsBusinessException e) {
			logger.LogException("Exception while adding the Exeption Reason while Settlement.", e);
			throw new CoreException("Exception while adding the Exeption Reason while Settlement.", e);
		}
	}
}
