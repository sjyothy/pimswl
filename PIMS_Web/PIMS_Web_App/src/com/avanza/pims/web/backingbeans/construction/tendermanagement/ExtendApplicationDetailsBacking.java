package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.richfaces.component.html.HtmlTabPanel;

import COM.rsa.asn1.en;

import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.bpel.proxy.pimsextendrequest.PIMSExtendTenderRequestBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.ExtendRequestDetailsView;
import com.avanza.pims.ws.vo.NOLTypeView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;


public class ExtendApplicationDetailsBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ExtendApplicationDetailsBacking.class);

	private transient RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	private transient ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private transient PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	
	private final String TAB_REQUEST_DETAILS = "requestDetailsTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private final String notesOwner = WebConstants.ExtendApplication.EXTEND_REQUEST;
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	String hiddenContractorId = "";
	String hiddenTenderId = "";
	
	Boolean showSave = true;
	String remarks = "";
	
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_DRAFT ="PAGE_MODE_DRAFT";
	private final String PAGE_MODE_DRAFT_DONE ="PAGE_MODE_DRAFT_DONE";
	private final String PAGE_MODE_FOLLOW_UP ="PAGE_MODE_FOLLOW_UP";
	private final String PAGE_MODE_FOLLOW_UP_DONE ="PAGE_MODE_FOLLOW_UP_DONE";
	private final String PAGE_MODE_APPROVE ="PAGE_MODE_APPROVE";
	private final String PAGE_MODE_APPROVE_DONE ="PAGE_MODE_APPROVE_DONE";
	private final String PAGE_MODE_PUBLISH ="PAGE_MODE_PUBLISH";
	private final String PAGE_MODE_PUBLISH_DONE ="PAGE_MODE_PUBLISH_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private String pageMode="default";
	
	private final String NOTIFICATION_EVENT_PUBLISH = "Event.ExtendApplication.Publish";
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_NEW;
	
	private String message="";
	private String heading="";
	

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();

	
	
	@SuppressWarnings("unchecked")
	private Boolean generateNotification(String eventName){
    	String methodName ="generateNotification";
    	Boolean success = true;
		try
		{
			logger.logInfo(methodName+"|Start");
			ExtendRequestDetailsView extendRequestDetailsView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
			List<ContactInfo> contractorsEmailList = extendRequestDetailsView.getTenderContractorsContactInfoList();
			NotificationFactory nsfactory = NotificationFactory.getInstance();
			NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
			Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();	
			if(StringHelper.isNotEmpty(extendRequestDetailsView.getTenderId()))
	    	{
			    if(contractorsEmailList!=null && !contractorsEmailList.isEmpty()){
			    	for(ContactInfo contractorContactInfo : contractorsEmailList){
				    	getNotificationPlaceHolder(event, contractorContactInfo.getUserId());			    	
				    	notifier.fireEvent(event, contractorsEmailList);
			    	}
			    }
	    	}
			logger.logInfo(methodName+"|Finish");
		}
		catch(Exception ex)
		{
			success  = false;
			logger.LogException(methodName+"|Finish", ex);
		}
		return success;
    }
	
	private void  getNotificationPlaceHolder(Event placeHolderMap, String userId)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		
		ExtendRequestDetailsView extendRequestDetailsView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
		
		placeHolderMap.setValue("CONTRACTOR_NAME", userId);
		placeHolderMap.setValue("TENDER_NO", extendRequestDetailsView.getTenderNumber());
		placeHolderMap.setValue("CLOSURE_DATE", extendRequestDetailsView.getTenderEndDate());
		
		logger.logDebug(methodName+"|Finish");
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	
	public String getInvalidMessage(String field){
		return CommonUtil.getInvalidMessage(field);
	}
	
    public String getFieldRequiredMessage(String field){
		return CommonUtil.getFieldRequiredMessage(field);
	}

    public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }
    
    @SuppressWarnings("unchecked")
	public Boolean validForApproval() {
	    logger.logInfo("validForApproval() started...");
	    Boolean validated = true;
	    try
		{
	    	if(getNewClosureDate()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ExtendApplication.NEW_CLOSURE_DATE));
				return validated;
	        }
	    	
	    	if(viewMap.get("tenderEndDate")!=null){
		    	Date tenderEndDate = (Date) viewMap.get("tenderEndDateVal");
		    	if(tenderEndDate.after(getNewClosureDate()))
		        {
		    		validated = false;
		        	errorMessages.clear();
		        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_NEW_CLOSURE_DATE_INVALID));
					return validated;
		        }
	    	}
	    	
	    	if(StringHelper.isEmpty(remarks))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				return validated;
	        }

			logger.logInfo("validForApproval() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validForApproval() crashed ", exception);
		}
		return validated;
	}
    
    @SuppressWarnings("unchecked")
	public Boolean validateRemarks() {
	    logger.logInfo("validateRemarks() started...");
	    Boolean validated = true;
	    try
		{
	    	if(StringHelper.isEmpty(remarks))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				return validated;
	        }

			logger.logInfo("validateRemarks() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateRemarks() crashed ", exception);
		}
		return validated;
	}
    
    @SuppressWarnings("unchecked")
	public Boolean validated() {
	    logger.logInfo("validated() started...");
	    Boolean validated = true;
	    try
		{
	    	String applicationSourceId = (String)viewMap.get("applicationSourceId");
	    	String reasons = (String)viewMap.get("reasons");
	    	
	    	logger.logInfo("applicationSourceId : %s | hiddenContractorId : %s | hiddenTenderId : %s | reasons : %s", applicationSourceId, hiddenContractorId, hiddenTenderId, reasons);
	    	
	    	if(StringHelper.isEmpty(applicationSourceId) || applicationSourceId.equals("0"))
	        {
	    		validated = false;
	        	//errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Application.APPLICATION_SOURCE));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				//return validated;
	        }
//	    	hiddenContractorId = "15";
	    	if(StringHelper.isEmpty(hiddenContractorId))
	        {
	    		validated = false;
	        	//errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_CONTRACTOR));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				//return validated;
	        }
//	    	hiddenTenderId = "1";
	    	if(StringHelper.isEmpty(hiddenTenderId))
	        {
	    		validated = false;
	        	//errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_TENDER));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				//return validated;
	        }
	    	
	    	/*if(requestAlreadyExists()){
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_TENDER));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				return validated;
	    	}*/
	    	
	    	if(StringHelper.isEmpty(reasons))
	        {
	    		validated = false;
	        	//errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REASONS));
	        	tabPanel.setSelectedTab(TAB_REQUEST_DETAILS);
				//return validated;
	        }
	    	
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		//errorMessages.clear();
	    		errorMessages.add(getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;
				//return validated;
	    	}

			logger.logInfo("validated() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validated() crashed ", exception);
		}
		return validated;
	}
	
    private Boolean requestAlreadyExists(){
    	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
    	Boolean exists = true;
		try{
			logger.logInfo("requestAlreadyExists started...");
			String existingRequestNumber = "";
			if(StringHelper.isNotEmpty(hiddenContractorId) && StringHelper.isNotEmpty(hiddenTenderId)){
				existingRequestNumber = requestServiceAgent.requestAlreadyExists(Long.valueOf(hiddenContractorId), Long.valueOf(hiddenTenderId));
				if(existingRequestNumber.equals("null"))
					exists = false;
				else
					viewMap.put("existingRequestNumber", existingRequestNumber);
			}
			logger.logInfo("requestAlreadyExists completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestAlreadyExists crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		return exists;
    }
    
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			if(sessionMap.containsKey("FROM_CONSTRUCTION_TENDER"))
			{
				viewMap.put("FROM_CONSTRUCTION_TENDER", sessionMap.get("FROM_CONSTRUCTION_TENDER"));
				//sessionMap.remove("FROM_CONSTRUCTION_TENDER");
			}	
			
			Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
			if(requestId!=null)
				loadAttachmentsAndComments(requestId + "");
			ExtendRequestDetailsView requestDetailsTabView = requestServiceAgent.getExtendRequestById(requestId, getArgMap());	
			
			viewMap.put(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
			if(pageMode.equals(PAGE_MODE_PUBLISH))
				setNewClosureDate(requestDetailsTabView.getTenderEndDateVal());
			else if(pageMode.equals(PAGE_MODE_FOLLOW_UP))
				setFollowUpDate(new Date());
			populateRequestDetailsTab();
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		return CommonUtil.getLoggedInUser();
	}
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
			if(!isPostBack())
			{
				loadAttachmentsAndComments();
				setMode();
				setValues();
			}

			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
	
	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			// from extend application search screen
			if(sessionMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE)!=null)
			{
				String extendRequestMode = (String) sessionMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE);
				sessionMap.remove(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE);
				Long requestId = (Long)sessionMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				sessionMap.remove(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				if(requestId!=null)
					viewMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
				
				canAddAttachmentsAndComments(true);
				viewMap.put("extendRequestDetailsReadonlyMode", true);
				viewMap.put("asterisk", "");
				
				if(extendRequestMode.equals(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_NEW)){
					pageMode = PAGE_MODE_NEW;
					viewMap.put("extendRequestDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_NEW);
				}
				else if(extendRequestMode.equals(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_DRAFT)){
					pageMode = PAGE_MODE_DRAFT;
					viewMap.put("extendRequestDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_DRAFT);
				}
				else if(extendRequestMode.equals(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_PUBLISH)){
					pageMode = PAGE_MODE_PUBLISH;
					viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_PUBLISH);
				}
				else if(extendRequestMode.equals(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_FOLLOW_UP)){
					pageMode = PAGE_MODE_FOLLOW_UP;
					viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_FOLLOW_UP);
				}
				else if(extendRequestMode.equals(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_VIEW)){
					pageMode = PAGE_MODE_VIEW;
					viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_VIEW);
					canAddAttachmentsAndComments(false);
				}
				
				viewMap.put(PAGE_MODE, pageMode);
				
			}
			// FROM TASK LIST
			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
				checkTask();
			}

			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public Number getNumberValue(String doubleStr, String pattern){
		return new CommonUtil().getNumberValue(doubleStr, pattern);
	}
	
	public String getStringFromDate(Date dateVal){
		return CommonUtil.getStringFromDate(dateVal);
	}
	
	public Date getDateFromString(String dateStr){
		return CommonUtil.getDateFromString(dateStr);
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		return CommonUtil.getFormattedDoubleString(val, pattern);
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	
	
	public void loadAttachmentsAndComments(String associatedObjectId){
		if(StringHelper.isNotEmpty(associatedObjectId)){
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, associatedObjectId);
			viewMap.put("entityId", associatedObjectId);
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_EXTEND_REQUEST);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_EXTEND_REQUEST;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.ExtendApplication.EXTEND_REQUEST);
	}

	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long requestId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.REQUEST_ID)!=null)
				{
					requestId = Convert.toLong(taskAttributes.get(WebConstants.REQUEST_ID));
					viewMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
				}
				
				pageMode = PAGE_MODE_APPROVE;
				viewMap.put(HEADING, WebConstants.ExtendApplication.Headings.EXTEND_REQUEST_MODE_APPROVE);
				canAddAttachmentsAndComments(true);
				viewMap.put("extendRequestDetailsReadonlyMode", true);
				viewMap.put(PAGE_MODE, pageMode);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_VIEW;
			
			logger.logInfo("pageMode : %s", pageMode);
			
			heading = getHeading();
			
			// for extendRequestTab start
			loadContractor();
			loadTender();
			// for extendRequestTab end
				
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadContractor(){
		try{
			logger.logInfo("loadContractor started...");
			
			Boolean contractorLoaded = (Boolean) viewMap.get("contractorLoaded");
			contractorLoaded = (contractorLoaded==null)?false:contractorLoaded;
			if(StringHelper.isNotEmpty(hiddenContractorId) && !contractorLoaded){
				viewMap.put("hiddenContractorId", hiddenContractorId);
				ContractorView contractorView = constructionServiceAgent.getContractorByIdForExtendRequest(Long.valueOf(hiddenContractorId));
				String contractorName = getIsEnglishLocale()?contractorView.getContractorNameEn():contractorView.getContractorNameAr();
				viewMap.put("contractorName", contractorName);
				String contractorId = contractorView.getPersonId().toString();
				viewMap.put("contractorId", contractorId);
				String contractorType = getIsEnglishLocale()?contractorView.getContractorTypeEn():contractorView.getContractorTypeAr();
				viewMap.put("contractorType", contractorType);
				
				viewMap.put("contractorLoaded", true);
			}
			logger.logInfo("loadContractor completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadContractor crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadTender(){
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		try{
			logger.logInfo("loadTender started...");
//			viewMap.put("tenderLoaded", false);
			Boolean tenderLoaded = (Boolean) viewMap.get("tenderLoaded");
			tenderLoaded = (tenderLoaded==null)?false:tenderLoaded;
			if(StringHelper.isNotEmpty(hiddenTenderId) && !tenderLoaded){
				viewMap.put("hiddenTenderId", hiddenTenderId);
				TenderView tenderView = csa.getTenderById(Long.parseLong(hiddenTenderId));
				String tenderNumber = tenderView.getTenderNumber();
				viewMap.put("tenderNumber", tenderNumber);
				String tenderId = tenderView.getTenderId().toString();
				viewMap.put("tenderId", tenderId);
				String tenderType = getIsEnglishLocale()?tenderView.getTenderTypeView().getDescriptionEn():tenderView.getTenderTypeView().getDescriptionAr();
				viewMap.put("tenderType", tenderType);
				String tenderStartDate = getStringFromDate(tenderView.getIssueDate());
				viewMap.put("tenderStartDate" , tenderStartDate);
				String tenderEndDate = getStringFromDate(tenderView.getEndDate());
				viewMap.put("tenderEndDate" , tenderEndDate);
				
				viewMap.put("tenderLoaded", true);
			}
			logger.logInfo("loadTender completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadTender crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	/*
	 * Button Click Action Handlers
	 */

	
	private Boolean closeRelatedApplications(){
		logger.logInfo("closeRelatedApplications() started...");
		Boolean success = true;
		try {
			 String tenderId = (String)viewMap.get("tenderId");
			 Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
			 
			 if(StringHelper.isNotEmpty(tenderId) && requestId!=null)
				 success = requestServiceAgent.closeRelatedExtendApplications(Long.valueOf(tenderId), requestId, getLoggedInUser());
			logger.logInfo("closeRelatedApplications() completed successfully!!!");
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("closeRelatedApplications() crashed ", exception);
		}
		return success;
	}
	
	private Boolean sendNotifications(){
		logger.logInfo("sendNotifications() started...");
		Boolean success = true;
		try {
			success = generateNotification(NOTIFICATION_EVENT_PUBLISH);
			logger.logInfo("sendNotifications() completed successfully!!!");
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("sendNotifications() crashed ", exception);
		}
		return success;
	}
	
	public String completeFollowUp(){
		logger.logInfo("completeFollowUp() started...");
		try {
			 if(validateRemarks()){
			 	 Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				 NotesVO remarksVO = CommonUtil.createRemarksNote(notesOwner, requestId, remarks);
				 Boolean success = false;
				 if(requestId!=null && remarksVO!=null){
					 success = requestServiceAgent.followUpExtendRequest(requestId, remarksVO, getLoggedInUser());
				     if(success){
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_FOLLOW_UP_DONE);
						errorMessages.clear();
						infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_FOLLOW_UP_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_FOLLOW_UP_DONE);
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
				     }
				 }
			 }
			 logger.logInfo("completeFollowUp() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("completeFollowUp() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_FOLLOW_UP_FAILURE));
		}
		return "followUpDone";
	}
	
	private Boolean publishExtendRequest(){
		logger.logInfo("publishExtendRequest() started...");
		Boolean success = true;
		try {
			 Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
			 NotesVO remarksVO = CommonUtil.createRemarksNote(notesOwner, requestId, remarks);
			 
			 if(requestId!=null && remarksVO!=null)
				 success = requestServiceAgent.publishExtendRequest(requestId, remarksVO, getLoggedInUser());

			 logger.logInfo("publishExtendRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("publishExtendRequest() crashed ", exception);
		}
		return success;
	}
	
	private Boolean updateTenderEndDate(){
		logger.logInfo("updateTenderEndDate() started...");
		Boolean success = true;
		try {
			 String tenderId = (String)viewMap.get("tenderId");
			 Date newClosureDate = getNewClosureDate();
			 Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
			 NotesVO remarksVO = CommonUtil.createRemarksNote(notesOwner, requestId, remarks);
			 
			 if(StringHelper.isNotEmpty(tenderId))
			 {
				 success = constructionServiceAgent.updateTenderEndDate(Long.valueOf(tenderId), newClosureDate, remarksVO, getArgMap());
				 if(success)
					 viewMap.put("tenderEndDate", getStringFromDate(newClosureDate));
			 }
			logger.logInfo("updateTenderEndDate() completed successfully!!!");
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("updateTenderEndDate() crashed ", exception);
		}
		return success;
	}
	
	@SuppressWarnings("unchecked")
	public String approveRequest() {
		logger.logInfo("approveRequest() started...");
		UserTask userTask = null;
		String success = "false";
		Boolean published = false;
		Boolean relatedClosed = false;
		Long requestId = 0L;
		try {
				if(validForApproval()){
					if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
						requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
					Boolean attachSuccess = saveAttachments(requestId);
					Boolean commentSuccess = saveComments(requestId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
			        
					Boolean updated = updateTenderEndDate();
					if(updated){
						if(getPublish())
							published = sendNotifications();
						else
							logger.logError("Publish Failed...");
						if(getCloseRelated())
							relatedClosed = closeRelatedApplications();
						else
							logger.logError("Close Related Applications Failed...");
	
						String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
				        logger.logInfo("Contextpath is: " + contextPath);
				        String loggedInUser = getLoggedInUser();
				        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
							userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
						BPMWorklistClient client = new BPMWorklistClient(contextPath);
						logger.logInfo("UserTask is: " + userTask.getTaskType());
						if(userTask!=null){
							client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
							notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_ExtendTender_Request_Approved);
						}
						
						errorMessages.clear();
						infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_APPROVE_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_APPROVE_DONE);
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
					}
				}
			logger.logInfo("approveRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("approveRequest() crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_APPROVE_FAILURE));
		}
		return "approveRequest";
	}

	@SuppressWarnings("all")
	public String rejectRequest() {
		logger.logInfo("rejectRequest() started...");
		UserTask userTask = null;
		String success = "false";
		Long requestId = 0L;
		try {
			if(validateRemarks()){
				if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
					requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				Boolean attachSuccess = saveAttachments(requestId);
				Boolean commentSuccess = saveComments(requestId);
				
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				 logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					CommonUtil.saveRemarksAsComments(requestId,remarks,notesOwner);
					client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_REJECTED);
				}
				errorMessages.clear();
				infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_REJECT_SUCCESS);
				viewMap.put(PAGE_MODE, PAGE_MODE_APPROVE_DONE);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
			}
			logger.logInfo("rejectRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("rejectRequest() crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_REJECT_FAILURE));
		}
		return "rejectRequest";
	}

	@SuppressWarnings("all")
	public String cancelRequest() {
		logger.logInfo("cancelRequest() started...");
		UserTask userTask = null;
		String success = "false";
		Long requestId = 0L;
		try {
			if(validateRemarks()){
				if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
					requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				Boolean attachSuccess = saveAttachments(requestId);
				Boolean commentSuccess = saveComments(requestId);
				
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				 logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					CommonUtil.saveRemarksAsComments(requestId,remarks,notesOwner);
					client.completeTask(userTask, loggedInUser, TaskOutcome.CANCEL);
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_CANCELED);
				}
				errorMessages.clear();
				infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_CANCEL_SUCCESS);
				viewMap.put(PAGE_MODE, PAGE_MODE_APPROVE_DONE);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
			}
			logger.logInfo("cancelRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("cancelRequest() crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_CANCEL_FAILURE));
		}
		return "cancelRequest";
	}
	
	@SuppressWarnings("unchecked")
	public String publishRequest() {
		logger.logInfo("publishRequest() started...");
		Long requestId=0L;
		String success = "false";
		try {
				if(validateRemarks()){
					if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
						requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
					Boolean attachSuccess = saveAttachments(requestId);
					Boolean commentSuccess = saveComments(requestId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
			        
					Boolean statusUpdatedRemarksSaved = publishExtendRequest();
					Boolean notify = false;
					if(statusUpdatedRemarksSaved){
						notify = sendNotifications();
						if(!notify)
							logger.logError("Publish Notification Failed...");
					}
					else
						logger.logError("Status Update and Save Remarks Failed...");
					if(getCloseRelated()){
						Boolean relatedClosed = closeRelatedApplications();
						if(!relatedClosed)
							logger.logError("Close Related Applications Failed...");
					}
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_PUBLISHED);
					errorMessages.clear();
					infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_PUBLISH_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_PUBLISH_DONE);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
				}				
			logger.logInfo("publishRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("publishRequest() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_PUBLISH_FAILURE));
		}
		return "publishRequest";
	}
	
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	
	public String saveExtendRequest() {
		return saveRequest(false);
	}
	
	@SuppressWarnings("unchecked")
	public String saveRequest(Boolean fromSubmitButton) {
		logger.logInfo("saveRequest() started...");
		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
		Boolean firstTime = false;
		String success = "false";
		Boolean crashed = false;
		try {
//			fillExtendRequestDetailsView();
			if(validated()){
				ExtendRequestDetailsView extendRequestDetailsView = new ExtendRequestDetailsView();
				Long requestId = (Long)viewMap.get("requestId");
				 
				if(requestId!=null){
					extendRequestDetailsView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
					extendRequestDetailsView.setRequestId(requestId);
					
				}
					 
				 String applicationSourceId = (String)viewMap.get("applicationSourceId");
				 extendRequestDetailsView.setApplicationSourceId(applicationSourceId);
				 
				 String contractorId = (String)viewMap.get("hiddenContractorId");
				 extendRequestDetailsView.setContractorId(contractorId);
				 
				 String tenderId = (String)viewMap.get("hiddenTenderId");
				 extendRequestDetailsView.setTenderId(tenderId);
				 
				 String reasons = (String)viewMap.get("reasons");
				 extendRequestDetailsView.setReasons(reasons);
				 
				 
				 if(extendRequestDetailsView.getRequestId()==null)
					 firstTime = true;
				 
				 if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
					 extendRequestDetailsView.setFromConstruction(true);
				 else
					 extendRequestDetailsView.setFromConstruction(false);
				 extendRequestDetailsView = requestServiceAgent.saveExtendRequest(extendRequestDetailsView, getArgMap());
				 viewMap.put(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW, extendRequestDetailsView);
				 populateRequestDetailsTab();
				 
				 if(extendRequestDetailsView.getRequestId()!=null){
					requestId = extendRequestDetailsView.getRequestId();
					viewMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
					
					loadAttachmentsAndComments(requestId.toString());
					
					Boolean attachSuccess = saveAttachments(requestId);
					Boolean commentSuccess = saveComments(requestId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
					
					if(!fromSubmitButton){
						errorMessages.clear();
						infoMessage="";
						String msg="";
						if(firstTime){
							msg = CommonUtil.getParamBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_CREATE_SUCCESS, extendRequestDetailsView.getApplicationNumber());
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_CREATED);
						}
						else{
							msg = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_SAVE_SUCCESS);
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_SAVED);
						}
						infoMessage=msg;
					}
			   }
			}
			else
				success = "validation";
			logger.logInfo("saveRequest() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("saveRequest() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("saveRequest() crashed ", exception);
		}
		finally{
			if(crashed)
				errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_SAVE_FAILURE));
		}
		return success;
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen==null)
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }

	public String done() {
		logger.logInfo("done() started...");
		Long requestId=0L;
		Boolean crashed = false;
		String success = "false";
		try {
				success = saveRequest(true);
				if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
					requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
				if(success.equals("true")){
					// TODO change required
					PIMSExtendTenderRequestBPELPortClient bpelPortClient= new PIMSExtendTenderRequestBPELPortClient();
					SystemParameters parameters = SystemParameters.getInstance();
					String endPoint = parameters.getParameter(WebConstants.ExtendApplication.EXTEND_APPLICATION_BPEL_END_POINT);
					logger.logDebug("end point : %s", endPoint);
					String userId = getLoggedInUser();
					bpelPortClient.setEndpoint(endPoint);
					String tenderIdStr = (String)viewMap.get("hiddenTenderId");
					logger.logInfo("tenderId : %s | requestId : %s ", tenderIdStr, requestId);
					Long tenderId = 0L;
					if(StringHelper.isNotEmpty(tenderIdStr))
						tenderId = Long.valueOf(tenderIdStr);
					if(tenderId!=0L && requestId!=0L){
						bpelPortClient.initiate(tenderId, requestId, userId, null, null);
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);
						infoMessage = CommonUtil.getParamBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_SUBMIT_SUCCESS, viewMap.get("applicationNumber").toString());
						
						notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_ExtendTender_Request_Received);
						viewMap.put(PAGE_MODE,PAGE_MODE_DRAFT_DONE);
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
						viewMap.put("extendRequestDetailsReadonlyMode", true);
						viewMap.put("asterisk", "");
						
						CommonUtil.printReport(requestId);

					}
					else{
						logger.logError("done() - tenderId or requestId is null!!!");    
					}
				}
				else if(success.equals("false")){
					errorMessages.clear();
					errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_SUBMIT_FAILURE));
				}
			logger.logInfo("done() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			crashed = true;
			logger.LogException("done() crashed ", exception);    
		}
		catch (Exception exception) {
			crashed = true;
			logger.LogException("done() crashed ", exception);
		}
		finally{
			if(crashed)
			{
				errorMessages.clear();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.Messages.TASK_CRASH_MSG));
			}
		}
		return "bpelInvoked";
	}

	@SuppressWarnings("unchecked")
	private void populateRequestDetailsTab(){
		ExtendRequestDetailsView requestDetailsTabView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
		if(requestDetailsTabView!=null){
			if(requestDetailsTabView.getApplicationDate()!=null)
				viewMap.put("applicationDate", requestDetailsTabView.getApplicationDate());
			if(requestDetailsTabView.getApplicationNumber()!=null)
				viewMap.put("applicationNumber", requestDetailsTabView.getApplicationNumber());
			if(requestDetailsTabView.getApplicationSource()!=null)
				viewMap.put("applicationSource", requestDetailsTabView.getApplicationSource());
			if(requestDetailsTabView.getApplicationSourceId()!=null)
				viewMap.put("applicationSourceId", requestDetailsTabView.getApplicationSourceId());
			if(requestDetailsTabView.getApplicationStatus()!=null)
				viewMap.put("applicationStatus", requestDetailsTabView.getApplicationStatus());
			if(requestDetailsTabView.getApplicationStatusId()!=null)
				viewMap.put("applicationStatusId", requestDetailsTabView.getApplicationStatusId());
			if(requestDetailsTabView.getContractorId()!=null)
				{
					viewMap.put("contractorId", requestDetailsTabView.getContractorId());
					hiddenContractorId = requestDetailsTabView.getContractorId();
				}
			if(requestDetailsTabView.getContractorName()!=null)
				viewMap.put("contractorName", requestDetailsTabView.getContractorName());
			if(requestDetailsTabView.getContractorType()!=null)
				viewMap.put("contractorType", requestDetailsTabView.getContractorType());
			if(requestDetailsTabView.getContractorTypeId()!=null)
				viewMap.put("contractorTypeId", requestDetailsTabView.getContractorTypeId());
			if(requestDetailsTabView.getReasons()!=null)
				viewMap.put("reasons", requestDetailsTabView.getReasons());
			if(requestDetailsTabView.getRequestId()!=null)
				viewMap.put("requestId", requestDetailsTabView.getRequestId());
			if(requestDetailsTabView.getTenderEndDate()!=null)
				viewMap.put("tenderEndDate", requestDetailsTabView.getTenderEndDate());
			if(requestDetailsTabView.getTenderEndDateVal()!=null)
				viewMap.put("tenderEndDateVal", requestDetailsTabView.getTenderEndDateVal());
			if(requestDetailsTabView.getTenderId()!=null)
				{
					viewMap.put("tenderId", requestDetailsTabView.getTenderId());
					hiddenTenderId = requestDetailsTabView.getTenderId();
				}
			if(requestDetailsTabView.getTenderNumber()!=null)
				viewMap.put("tenderNumber", requestDetailsTabView.getTenderNumber());
			if(requestDetailsTabView.getTenderStartDate()!=null)
				viewMap.put("tenderStartDate", requestDetailsTabView.getTenderStartDate());
			if(requestDetailsTabView.getTenderType()!=null)
				viewMap.put("tenderType", requestDetailsTabView.getTenderType());
			if(requestDetailsTabView.getTenderTypeId()!=null)
				viewMap.put("tenderTypeId", requestDetailsTabView.getTenderTypeId());
			
			
				
		}
		
	}
	
	public Boolean saveSystemComments(String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	
	    	Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
	    	
	    	if(requestId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(notesOwner, sysNote, requestId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	
	public void requestHistoryTabClick()
	{
		logger.logInfo("requestHistoryTabClick started...");
		try	
		{
			RequestHistoryController rhc=new RequestHistoryController();
			Long requestId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
			if(requestId!=null && requestId!=0L)
				rhc.getAllRequestTasksForRequest(notesOwner, requestId.toString());
			
			logger.logInfo("requestHistoryTabClick completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestHistoryTabClick crashed...",ex);
		}
	}
	
    public void doPublish(ValueChangeEvent event){
    	setPublish((Boolean)event.getNewValue());
    }

    public void doCloseRelated(ValueChangeEvent event){
    	setCloseRelated((Boolean)event.getNewValue());
    }

    /*
	 * Setters / Getters
	 */

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = getBundleMessage(DEFAULT_HEADING);
		else
			heading = getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public Boolean getShowActionPanel(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT_DONE))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			return false;
		return true;
	}
	
	public Boolean getShowHistory(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		return true;
	}

	
	public Boolean getShowCancel(){
		return true;
	}
	
	public Boolean getShowFollowUpSave() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP))
			return true;
		return false;
	}
	
	public Boolean getShowSave()  {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
				return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
				return true;
		
		return false;
	}
	
	public Boolean getNewClosureDateReadonlyMode() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE))
			return false;
		return true;
	}
	
	public Boolean getFollowUpDateReadonlyMode() {
		return true;
	}
	
	public Boolean getRemarksReadonlyMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_PUBLISH_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			return false;
		return false;
	}
	
	public Boolean getShowRemarks() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT_DONE))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			return false;
		return true;
	}
	
	public Boolean getShowClosureDate(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_PUBLISH))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_PUBLISH_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowFollowUpDate(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowSubmit(){
		try
		{
			if (pageMode.equalsIgnoreCase("default"))
			return true;
			if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
				return true;
			if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
				return true;
			if(pageMode.equalsIgnoreCase(PAGE_MODE_VIEW) && ifRequestStatusNew())
				return true;
		}
		catch(Exception ex)
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return false;
	}
	
	public Boolean getShowApprove(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE))
			return true;
		return false;
	}
	
	public Boolean getShowReject(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE))
			return true;
		return false;
	}
	
	public Boolean getShowCancelRequest(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVE))
			return true;
		return false;
	}
	
	public Boolean getShowPublish(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_PUBLISH))
			return true;
		return false;
	}
	
	public Boolean getReadonlyMode(){
		Boolean extendRequestDetailsReadonlyMode = (Boolean)viewMap.get("extendRequestDetailsReadonlyMode");
		if(extendRequestDetailsReadonlyMode==null)
			extendRequestDetailsReadonlyMode = false;
		return extendRequestDetailsReadonlyMode;
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public String getHiddenContractorId() {
		return hiddenContractorId;
	}

	public void setHiddenContractorId(String hiddenContractorId) {
		this.hiddenContractorId = hiddenContractorId;
	}

	public String getHiddenTenderId() {
		return hiddenTenderId;
	}

	public void setHiddenTenderId(String hiddenTenderId) {
		this.hiddenTenderId = hiddenTenderId;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public void setShowSave(Boolean showSave) {
		this.showSave = showSave;
	}
	
	public Date getNewClosureDate() {
		Date temp = (Date)viewMap.get("newClosureDate");
		return temp;
	}
	
	public void setNewClosureDate(Date newClosureDate) {
		if(newClosureDate!=null)
			viewMap.put("newClosureDate", newClosureDate);
	}
	
	public Date getFollowUpDate() {
		Date temp = (Date)viewMap.get("followUpDate");
		return temp;
	}
	
	public void setFollowUpDate(Date followUpDate) {
		if(followUpDate!=null)
			viewMap.put("followUpDate", followUpDate);
	}

/*	public String getRemarks() {
		String temp = (String)viewMap.get("remarks");
		return temp;
	}
	
	public void setRemarks(String remarks) {
		if(remarks!=null)
			viewMap.put("remarks", remarks);
	}
	*/
	public Boolean getPublish() {
		Boolean temp = (Boolean)viewMap.get("publish");
		return temp;
	}
	
	public void setPublish(Boolean publish) {
		if(publish!=null)
			viewMap.put("publish", publish);
	}
	
	public Boolean getCloseRelated() {
		Boolean temp = (Boolean)viewMap.get("closeRelated");
		return temp;
	}
	
	public void setCloseRelated(Boolean closeRelated) {
		if(closeRelated!=null)
			viewMap.put("closeRelated", closeRelated);
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	private Boolean ifRequestStatusNew() throws PimsBusinessException {
		Long requestStatusId = null;
		String  METHOD_NAME = "NOLRequestStatus|";		
		logger.logInfo(METHOD_NAME+"started...");		
		try
		{
			HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
				WebConstants.REQUEST_STATUS_NEW);
			if(hMap.containsKey("returnId")){
				requestStatusId = Long.parseLong(hMap.get("returnId").toString());
				if(requestStatusId.equals(null))
					throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
				else
				{
					String temp = (String)viewMap.get("applicationStatusId");
					if(temp.compareTo(requestStatusId.toString()) == 0)
						return true;
				}
		}
		}
		catch(PimsBusinessException ex)
		{
			logger.logDebug("get status id for new request crashed", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		logger.logInfo(METHOD_NAME+"ended...");
		return false;
	}

	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logInfo

				(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
				hMap.put("returnId",domainDataView.getDomainDataId());
				logger.logInfo(methodName+"|"+"DomainDataDesc En..."+
						domainDataView.getDataDescEn());
				hMap.put("IdEn",domainDataView.getDataDescEn());
				hMap.put("IdAr",domainDataView.getDataDescAr());
			}

		}
		logger.logInfo(methodName+"|"+"Finish...");
		return hMap;

	}
	
	
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		ServletContext servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	
	private void notifyRequestStatus(String notificationType)
	{
		Long reqId = 0L;
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		RequestView requestView = null;
		ExtendRequestDetailsView requestDetailsTabView = null;
				
		if(viewMap.containsKey(WebConstants.ExtendApplication.EXTEND_REQUEST_ID))			
			reqId = (Long)viewMap.get(WebConstants.ExtendApplication.EXTEND_REQUEST_ID);
		
		if(viewMap.containsKey(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW))
			requestDetailsTabView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
		try
		{
			requestView = psAgent.getRequestById(reqId);
			if(requestDetailsTabView != null && requestView != null)
			{
				PersonView contractor = requestView.getContactorView();	
				
				if(contractor!=null)
					{
						List<ContactInfo> contactInfoList = getContactInfoList(contractor);
												
						eventAttributesValueMap.put("REQUEST_NUMBER", requestDetailsTabView.getApplicationNumber());
						eventAttributesValueMap.put("TENDER_NUMBER", requestDetailsTabView.getTenderNumber());
						eventAttributesValueMap.put("START_DATE", requestDetailsTabView.getTenderStartDate());
						eventAttributesValueMap.put("END_DATE", requestDetailsTabView.getTenderEndDate());
						eventAttributesValueMap.put("REASON", requestDetailsTabView.getReasons());
						eventAttributesValueMap.put("APPLICANT_NAME", requestDetailsTabView.getContractorName());
										
						generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
						
					}
				
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("notify request status crashed",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	
}

