package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.PaymentSchedule;
import com.avanza.pims.web.backingbeans.PaymentSchedule.Keys;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContract.Page_Mode;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;

public class ServiceContractPaymentSchTab extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ServiceContractPaymentSchTab.class);
	FacesContext context;
    Map sessionMap;
    Map viewRootMap;
    Map requestMap;
    HttpServletRequest request;
    HttpServletRequest requestFromConsultant = (HttpServletRequest) FacesContext
	.getCurrentInstance().getExternalContext().getRequest();
    
    public interface ViewRootKeys {
		
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String PAYMENT_DUE_ON_MULTIPLE="PAYMENT_DUE_ON_MULTIPLE";
		public static final String PROJECT_ID="PROJECT_ID";
		public static final String  DIRECTION_TYPE_INBOUND="directionTypeInbound";
		public static final String  OWNERSHIP_TYPE="ownerShipTypeId";
		public static final String  PS_STATUS_COLLECTED="psCollected";
		public static final String  ERROR_TOTAL_VALUE_NOT_PRESENT ="ERROR_TOTAL_VALUE_NOT_PRESENT";
		
	}
    private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
    private HtmlGraphicImage imgDelPayments = new HtmlGraphicImage();
    private HtmlGraphicImage imgEditPayments = new HtmlGraphicImage();
    private HtmlCommandButton btnAddPayments = new HtmlCommandButton();
    private HtmlCommandButton btnGenPayments = new HtmlCommandButton();

    private HtmlDataTable paymentScheduleDataTable;
    
    private String selectSubType;
	private HtmlSelectOneMenu cmbSelectSubType = new HtmlSelectOneMenu();
	
	private HtmlOutputText cmbSelectSubTypeOutput = new HtmlOutputText();
	
    private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
	public ServiceContractPaymentSchTab()
	{
		  context=FacesContext.getCurrentInstance();
		  sessionMap=context.getExternalContext().getSessionMap();
		  requestMap= context.getExternalContext().getRequestMap();
		  viewRootMap=context.getViewRoot().getAttributes();
		  request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
		  
	}
	 @Override 
	 public void init() 
     {
		 if(!isPostBack())
		 {
			 viewRootMap.put( ViewRootKeys.PS_STATUS_COLLECTED, CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),WebConstants.PAYMENT_SCHEDULE_COLLECTED));
			 
			if(sessionMap.containsKey("fromConsultantContract"))
			{
			 viewRootMap.put(WebConstants.FROM_CONSULTANT_CONTRACT, sessionMap.get("fromConsultantContract"));
			 sessionMap.remove("fromConsultantContract");
			}
			if(sessionMap.containsKey("fromConstructionContract"))
			{
			 viewRootMap.put(WebConstants.FROM_CONSTRUCTION_CONTRACT, sessionMap.get("fromConstructionContract"));
			 sessionMap.remove("fromConstructionContract");
			}
			
			
			
			renderedPaymentScheduleSubTypeCmb();
		 }
		 
		 if(sessionMap.containsKey("PUT_OLD_LIST_OF_PAYMENT"))
    	 {
    		 sessionMap.remove("PUT_OLD_LIST_OF_PAYMENT");
    		 
    		 viewRootMap.put("PUT_OLD_LIST_OF_PAYMENT", (List<PaymentScheduleView>)viewRootMap.get(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT) );
    		 
    		 viewRootMap.remove(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT);
    	 }
    	 
	 }
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}
     
     public void prerender(){
    	 super.prerender();
    	 
    	 
     }

    public  void enableDisableControls(String pageMode)
    {
    	btnCollectPayment.setRendered(false);
    	if(pageMode.equals(Page_Mode.ACTIVE) || pageMode.equals(Page_Mode.COMPLETE) 
 				|| pageMode.equals(Page_Mode.LEGAL_REVIEW_REQUIRED) || pageMode.equals(ViewRootKeys.VIEW))
    	{
    		btnAddPayments.setRendered(false);
    		btnGenPayments.setRendered(false);
    		imgEditPayments.setRendered(false);
    		imgDelPayments.setRendered(false);
    	}
    	
    }
     
    public void renderedPaymentScheduleSubTypeCmb()
    {
    	if(viewRootMap.containsKey(WebConstants.FROM_CONSULTANT_CONTRACT))
    	{
    	cmbSelectSubType.setRendered(true);
    	cmbSelectSubTypeOutput.setRendered(true);
    	}
    	else
    	{
    	cmbSelectSubType.setRendered(false);
    	cmbSelectSubTypeOutput.setRendered(false);
    	}
    }
    
    public void removePaymentSubTypeCombo()
    {
    	
    	cmbSelectSubType.setRendered(false);
    	cmbSelectSubTypeOutput.setRendered(false);
    	
    }
    
    public Boolean getPaymentSubType ()
    {
      Boolean fromConsultantContract = false;
      if(viewRootMap.containsKey(WebConstants.FROM_CONSULTANT_CONTRACT))
      fromConsultantContract = true;
      
      return fromConsultantContract;
    }
 	
 	public Boolean getIsArabicLocale()
	{
    	
		return CommonUtil.getIsArabicLocale();
	}
 	public Boolean getIsEnglishLocale()
	{
    	
		return CommonUtil.getIsEnglishLocale();
	}
		public String getDateFormat()
		{
	    	
			return CommonUtil.getDateFormat();
		}
		public TimeZone getTimeZone()
		{
			 return CommonUtil.getTimeZone();
			
		}

		public void btnGenPayments_Click()
		{
			String methodName ="btnGenPayments_Click";
			logger.logInfo(methodName+"|"+"Start..");
			String amount = "";
			List<PaymentScheduleView> tempPaymentSchList = new ArrayList<PaymentScheduleView>();
			
			try	
	    	{
				
				Date startDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
				Date endDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);
				
				if(viewRootMap.containsKey(WebConstants.FROM_CONSULTANT_CONTRACT))
   			    {
   			    
					if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN))
   			    		viewRootMap.remove(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
   			    	if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION))
   			    		viewRootMap.remove(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
   			    	
   			    	DomainDataView ddvPaymentSubtypeDesign = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
   			    	DomainDataView ddvPaymentSubtypeSupervision = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
   			    	
   			    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
   			    	{
   			    		if(viewRootMap.containsKey(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT))
   			    		   viewRootMap.remove(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT);
   			    		  			    							
						
						for(PaymentScheduleView psv:(List<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
						{
							PaymentScheduleView cpyPsv = (PaymentScheduleView) BeanUtils.cloneBean(psv);
							
							if( cpyPsv.getPaymentScheduleSubTypeId().toString().equals(selectSubType) 
									&& cpyPsv.getPaymentScheduleId() != null )
							{	
								cpyPsv.setIsDeleted( WebConstants.IS_DELETED_TRUE );
								tempPaymentSchList.add( cpyPsv );
								
							}else if(! cpyPsv.getPaymentScheduleSubTypeId().toString().equals(selectSubType))
							{
								tempPaymentSchList.add( cpyPsv );
							}
							
						}
   			    		if(tempPaymentSchList!=null && tempPaymentSchList.size()>0)
   			    		{
   			    		viewRootMap.put(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT,tempPaymentSchList );
   			    		sessionMap.put("OLD_LIST_OF_PAYMENT", true);
   			    		}
   			    	}
   			    	
   			    	if(ddvPaymentSubtypeDesign.getDomainDataId().toString().equals(selectSubType))
   			    	{
   			    		viewRootMap.put(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN, ddvPaymentSubtypeDesign);	
	   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.DESIGN_AMOUNT))
	   	   			    	amount =viewRootMap.get(WebConstants.ContractDetailsTab.DESIGN_AMOUNT).toString();
   			    	
   			    	}else if(ddvPaymentSubtypeSupervision.getDomainDataId().toString().equals(selectSubType))
   			    	{
   			    	    viewRootMap.put(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION, ddvPaymentSubtypeSupervision);	
	   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.SUPERVISION_AMOUNT))
	   	   			    	amount =viewRootMap.get(WebConstants.ContractDetailsTab.SUPERVISION_AMOUNT).toString();
   			    	
   			    	}
   			    }
   			    else{
   			    	   			    	
   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT))
   	   			    	amount =viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString();
   			        }
				
				    if(viewRootMap.containsKey(WebConstants.FROM_CONSTRUCTION_CONTRACT))
				    {
				    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
	   			    	{
	   			    		if(viewRootMap.containsKey(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT))
	   			    		   viewRootMap.remove(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT);
	   			    		  			    							
							
							for(PaymentScheduleView psv:(List<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
							{
								PaymentScheduleView cpyPsv = (PaymentScheduleView) BeanUtils.cloneBean(psv);
								
								if(cpyPsv.getPaymentScheduleId() != null )
								{	
									cpyPsv.setIsDeleted( WebConstants.IS_DELETED_TRUE );
									tempPaymentSchList.add( cpyPsv );
								}
								
							}
	   			    		if(tempPaymentSchList!=null && tempPaymentSchList.size()>0)
	   			    		{
	   			    		viewRootMap.put(WebConstants.GENERATE_PAYMENT_FOR_CONSULTANT_CONTRACT,tempPaymentSchList );
	   			    		sessionMap.put("OLD_LIST_OF_PAYMENT", true);
	   			    		}
	   			    	}
				    }
				
				
				if(amount !=null && amount.trim().length()>0 && startDate!=null && endDate!=null)
				{
					String ownerShipTypeId="";
					if(viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE))
						   ownerShipTypeId=viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE).toString();
					DateFormat df = new SimpleDateFormat(getDateFormat());
					String javaScriptText ="var screen_width = 1024;"+
		            "var screen_height = 450;"+
		            "window.open('generateServiceContractPayments.jsf?" +
	                GeneratePayments.Keys.GRP_ACCOUNT_ID+"=0"+"&"+  
	                GeneratePayments.Keys.CONTRACT_START_DATE+"="+df.format(startDate)+"&"+
	                GeneratePayments.Keys.CONTRACT_END_DATE+"="+df.format(endDate)+"&"+
	                GeneratePayments.Keys.TOTAL_CONTRACT_VALUE+"="+amount+"&"+
	                GeneratePayments.Keys.PAYMENT_TYPE+"="+WebConstants.PAYMENT_TYPE_RENT_ID+"&"+
	                GeneratePayments.Keys.OWNER_SHIP_TYPE+"="+ownerShipTypeId+
	                "','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=100,top=150,scrollbars=yes,status=yes');";
					AddResource addResource = AddResourceFactory.getInstance(context);
			        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScriptText);
	    	   }
		        else
		        	viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.ERROR_TOTAL_VALUE_NOT_PRESENT,true);
		        
		        logger.logInfo(methodName+"|"+"Finish..");  
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);
	    	
	    	}
	    	
			
		}
		public void btnPrintPaymentSchedule_Click()
	    {
	    	String methodName = "btnPrintPaymentSchedule_Click";
	    	logger.logInfo(methodName+"|"+" Start...");
	    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
	    	//CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),viewRootMap.get("procedureType").toString());
	    	logger.logInfo(methodName+"|"+" Finish...");
	    }
		public boolean getIsPrintPaymentReceiptShow()
		{
			boolean isPrintPaymentReceiptShow = true;
			try
			{
				PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
				DomainDataView ddvCollected = (DomainDataView)viewRootMap.get(ViewRootKeys.PS_STATUS_COLLECTED);
				if(ddvCollected.getDomainDataId().compareTo(psv.getStatusId())==0)
					isPrintPaymentReceiptShow =true;
				else 
					isPrintPaymentReceiptShow =false;
			}
			catch(Exception ex)
			{
				isPrintPaymentReceiptShow = false;				
			}
			return isPrintPaymentReceiptShow ;
		}
		public void btnAddPayments_Click() 
		{
			String methodName ="btnAddPayments_Click";
			logger.logInfo(methodName+"|"+"Start..");

			try	
	    	{
				
				String paymentDueOnMultipleQueryString="";
				String directionTypeInbound="0";
				String ownerShipTypeId="";
				String totalContractValue="";
				if( viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE)!=null && viewRootMap.get(Keys.PROJECT_ID)!=null)
					paymentDueOnMultipleQueryString="&"+PaymentSchedule.Keys.MUTIPLE_PAYMENT_DUE_ON+"=true&"+PaymentSchedule.Keys.PROJECT_ID+"="+viewRootMap.get(Keys.PROJECT_ID);
				if(viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.PROJECT_ID)!=null)
					viewRootMap.put(PaymentSchedule.Keys.PROJECT_ID, viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.PROJECT_ID));
				if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) &&viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)!=null)
			    	sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST));
				if(viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND))
					directionTypeInbound = viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND).toString();
   			    if(viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE))
				   ownerShipTypeId="&ownerShipTypeId="+viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE).toString();
   			    
   			    
   			    
   			    if(viewRootMap.containsKey(WebConstants.FROM_CONSULTANT_CONTRACT))
   			    {
   			    	if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN))
   			    		viewRootMap.remove(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
   			    	if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION))
   			    		viewRootMap.remove(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
   			    
   			    	DomainDataView ddvPaymentSubtypeDesign = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
   			    	DomainDataView ddvPaymentSubtypeSupervision = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
   			    	
   			    	if(ddvPaymentSubtypeDesign.getDomainDataId().toString().equals(selectSubType))
   			    	{
   			    		viewRootMap.put(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN, ddvPaymentSubtypeDesign);	
	   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.DESIGN_AMOUNT))
	   	   			    	totalContractValue ="totalContractValue="+viewRootMap.get(WebConstants.ContractDetailsTab.DESIGN_AMOUNT).toString();
   			    	
   			    	}else if(ddvPaymentSubtypeSupervision.getDomainDataId().toString().equals(selectSubType))
   			    	{
   			    	    viewRootMap.put(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION, ddvPaymentSubtypeSupervision);	
	   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.SUPERVISION_AMOUNT))
	   	   			    	totalContractValue ="totalContractValue="+viewRootMap.get(WebConstants.ContractDetailsTab.SUPERVISION_AMOUNT).toString();
   			    	
   			    	}
   			    }
   			    else{
   			    	
   			    	
   			    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT))
   	   			    	totalContractValue ="totalContractValue="+viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString();
   			    }
   			    
   			    
				String javaScript ="var screen_width = 1024;"+ 
					"var screen_height = 450;"+
	            "window.open('PaymentSchedule.jsf?" +
	            totalContractValue+
	            paymentDueOnMultipleQueryString+ownerShipTypeId+
	            "&directionType="+directionTypeInbound+"','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
				AddResource addResource = AddResourceFactory.getInstance(context);
		        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScript);
				
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);

	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");

		}
		public void btnEditPaymentSchedule_Click(){
			
			String methodName ="btnEditPaymentSchedule_Click";
			logger.logInfo(methodName+"|"+"Start..");

			try	
	    	{
				String paymentDueOnMultipleQueryString="";
				if( viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE)!=null &&
					viewRootMap.get(Keys.PROJECT_ID)!=null		
				)
				paymentDueOnMultipleQueryString=PaymentSchedule.Keys.MUTIPLE_PAYMENT_DUE_ON+"=true&"
				+PaymentSchedule.Keys.PROJECT_ID+"="+viewRootMap.get(Keys.PROJECT_ID);
			
				PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
		    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) &&
		 			   viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)!=null)
		    	     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
		    				viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST));
		    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
		    	
		    	String javaScript="var screen_width = 1024;"+
		         "var screen_height = 450;"+
		        "window.open('EditPaymentSchedule.jsf?"+paymentDueOnMultipleQueryString+
		        "','_blank','width='+(screen_width-500)+',height='+(screen_height-200)+',left=300,top=250,scrollbars=yes,status=yes');";
		    	AddResource addResource = AddResourceFactory.getInstance(context);
		        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScript);
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);

	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");
			

			
		}
		public void btnDelPaymentSchedule_Click() {
		    String methodName = "btnDelPaymentSchedule_Click";
	    	logger.logInfo(methodName+"|"+" Start...");
	    	try{
	    	PaymentScheduleView selectedPaymentScheduleView =(PaymentScheduleView)paymentScheduleDataTable.getRowData();
	    	PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
	    	List<PaymentScheduleView> paymentList = new  ArrayList<PaymentScheduleView>(0);
	    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
		      paymentList = (ArrayList<PaymentScheduleView>) viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
		    
	    	if(viewRootMap.get(ViewRootKeys.PAGE_MODE).toString().equals(ViewRootKeys.ADD))
	    	{
	    		paymentList.remove(paymentScheduleDataTable.getRowIndex());
	    	}
	    	else
	    	{
	    	  psv.setIsDeleted(1L);
	    	  paymentList.set(paymentScheduleDataTable.getRowIndex(),psv);
	    	  
	    	}
	    	 viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,paymentList);
	    	
	    	 logger.logInfo(methodName+"|"+" Finish...");
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
	    		
	    	}

		}
		public List<PaymentScheduleView> getPaymentScheduleDataList() {
			try
			{
				List<PaymentScheduleView> tempPaymentSchList = new ArrayList<PaymentScheduleView>();
				DomainDataView ddvDesign = new DomainDataView();
				DomainDataView ddvSupervision = new DomainDataView();

				//int rowId = 0;
				if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
				{
					List<PaymentScheduleView> tempPaymentScheduleDataList =
						(ArrayList<PaymentScheduleView>) viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
					
					paymentScheduleDataList = tempPaymentScheduleDataList ;
					
					if(viewRootMap.containsKey("PUT_OLD_LIST_OF_PAYMENT"))
					{
						paymentScheduleDataList.addAll((List<PaymentScheduleView>)viewRootMap.get("PUT_OLD_LIST_OF_PAYMENT"));
						viewRootMap.remove("PUT_OLD_LIST_OF_PAYMENT");
						
					}
					
					for (PaymentScheduleView paymentScheduleView : tempPaymentScheduleDataList) 
				    {
						if(paymentScheduleView.getIsDeleted()==null )
						{
							  paymentScheduleView.setIsDeleted(0L);
						      paymentScheduleDataList.add(paymentScheduleView);
					    }
				    }
					
					if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN))
					{
						ddvDesign = (DomainDataView)viewRootMap.get(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
												
					}
					if(viewRootMap.containsKey(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION))
					{
						ddvSupervision = (DomainDataView)viewRootMap.get(WebConstants.DOMAIN_DATA_PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
						
					}
					
					
					if((ddvSupervision !=null && ddvSupervision.getDomainDataId()!=null)||
							(ddvDesign !=null && ddvDesign.getDomainDataId()!=null)	)
					{	
						for(PaymentScheduleView psv:paymentScheduleDataList)
						{
							if(psv.getPaymentScheduleSubTypeId()==null && ddvSupervision !=null && ddvSupervision.getDomainDataId()!=null)
							{
								psv.setPaymentScheduleSubTypeId(ddvSupervision.getDomainDataId());
								psv.setPaymentScheduleSubTypeEn(ddvSupervision.getDataDescEn());
								psv.setPaymentScheduleSubTypeAr(ddvSupervision.getDataDescAr());
								
							}
							else if(psv.getPaymentScheduleSubTypeId()==null && ddvDesign !=null && ddvDesign.getDomainDataId()!=null)
							{
								psv.setPaymentScheduleSubTypeId(ddvDesign.getDomainDataId());
								psv.setPaymentScheduleSubTypeEn(ddvDesign.getDataDescEn());
								psv.setPaymentScheduleSubTypeAr(ddvDesign.getDataDescAr());
								
							}	
							tempPaymentSchList.add(psv);
						}
						
						paymentScheduleDataList = tempPaymentSchList;
						
					}
					
				}
			}
			catch(Exception e)
			{
				logger.LogException("Error", e);
			}
			return paymentScheduleDataList;
		}
		public String openReceivePaymentsPopUp()
		{
			String methodName="openReceivePaymentsPopUp";
			logger.logInfo(methodName+"|"+"Start..");
			        FacesContext facesContext = FacesContext.getCurrentInstance();
			        if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) && viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)!=null)
			    	{
	    		        List<PaymentScheduleView> paymentSchedules = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
	    		        List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
	    				for (int i=0; i<paymentSchedules.size(); i++)
	    				{
	    					logger.logInfo(methodName+"|"+"i.."+i);
	    					if (paymentSchedules.get(i).getSelected() !=null && paymentSchedules.get(i).getSelected())
	    						paymentsForCollection.add(paymentSchedules.get(i));
	    				}
	    				if(paymentsForCollection != null && paymentsForCollection.size() > 0)
	    					sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
	    				else
	    					{
	    						this.btnCollectPayment.setDisabled(false);
	    						viewRootMap.put("SELECT_PAYMENT_TO_COLLECT",true);
	    						return null;
	    					}
			    	}
			        String extrajavaScriptText ="var screen_width = 850;"+
	                "var screen_height = 450;"+
	                "window.open('receivePayment.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=100,top=200,scrollbars=no,status=no');";
			        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
			        this.btnCollectPayment.setDisabled(true);
			        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
			        AddResource addResource = AddResourceFactory.getInstance(facesContext);
			        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, extrajavaScriptText);
			logger.logInfo(methodName+"|"+"Finish..");
			return "";
		}
		public boolean getIsDirectionTypeInbound()
		{
			boolean isDirInbound = true;
			if(!viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND) || 
					viewRootMap.get(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND).toString()=="0")
			     isDirInbound = false;
//			else
//				isDirInbound = false;
			return isDirInbound ;
		}
		public Long getPaymentSchedulePendingStausId()
	    {
	    	DomainDataView ddv = new DomainDataView();
	    
	    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
	    	{
	    	
	    	 ddv=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_PENDING);
	    	}
	    	else
	    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_PENDING) ;
	    	return ddv.getDomainDataId();
	    	
	    }
		public void setPaymentScheduleDataList(
				List<PaymentScheduleView> paymentScheduleDataList) {
			this.paymentScheduleDataList = paymentScheduleDataList;
			
		}

		public HtmlCommandButton getBtnAddPayments() {
			return btnAddPayments;
		}

		public void setBtnAddPayments(HtmlCommandButton btnAddPayments) {
			this.btnAddPayments = btnAddPayments;
		}

		public HtmlCommandButton getBtnGenPayments() {
			return btnGenPayments;
		}

		public void setBtnGenPayments(HtmlCommandButton btnGenPayments) {
			this.btnGenPayments = btnGenPayments;
		}

		
		public HtmlDataTable getPaymentScheduleDataTable() {
			return paymentScheduleDataTable;
		}
		public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
			this.paymentScheduleDataTable = paymentScheduleDataTable;
		}
		public HtmlGraphicImage getImgDelPayments() {
			return imgDelPayments;
		}
		public void setImgDelPayments(HtmlGraphicImage imgDelPayments) {
			this.imgDelPayments = imgDelPayments;
		}
		public HtmlGraphicImage getImgEditPayments() {
			return imgEditPayments;
		}
		public void setImgEditPayments(HtmlGraphicImage imgEditPayments) {
			this.imgEditPayments = imgEditPayments;
		}
		public HtmlCommandButton getBtnCollectPayment() {
			return btnCollectPayment;
		}
		public void setBtnCollectPayment(HtmlCommandButton btnCollectPayment) {
			this.btnCollectPayment = btnCollectPayment;
		}
		public Integer getPaginatorMaxPages() {

			return WebConstants.SEARCH_RESULTS_MAX_PAGES;

			}

		public void setPaginatorMaxPages(Integer paginatorMaxPages) {
			this.paginatorMaxPages = paginatorMaxPages;
		}

		/**
		 * @return the paginatorRows
		 */
		public Integer getPaginatorRows() {
			return WebConstants.RECORDS_PER_PAGE;
		}

		public void setPaginatorRows(Integer paginatorRows) {
			this.paginatorRows = paginatorRows;
		}
		public String getSelectSubType() {
			if(viewRootMap.containsKey("SUB_TYPE"))
				selectSubType  = viewRootMap.get("SUB_TYPE").toString();
			return selectSubType;
		}
		public void setSelectSubType(String selectSubType) {
			this.selectSubType = selectSubType;
			if(this.selectSubType!=null)
				viewRootMap.put("SUB_TYPE", this.selectSubType);
		}
		public HtmlSelectOneMenu getCmbSelectSubType() {
			return cmbSelectSubType;
		}
		public void setCmbSelectSubType(HtmlSelectOneMenu cmbSelectSubType) {
			this.cmbSelectSubType = cmbSelectSubType;
		}
		public HtmlOutputText getCmbSelectSubTypeOutput() {
			return cmbSelectSubTypeOutput;
		}
		public void setCmbSelectSubTypeOutput(HtmlOutputText cmbSelectSubTypeOutput) {
			this.cmbSelectSubTypeOutput = cmbSelectSubTypeOutput;
		}
		
		
		
	    @SuppressWarnings( "unchecked" )
	    public void btnViewPaymentDetails_Click() {
	    	String methodName = "btnViewPaymentDetails_Click";
	    	logger.logInfo(methodName+"|"+" Start...");
	    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
	    	CommonUtil.viewPaymentDetails(psv);
	 
	    	logger.logInfo(methodName+"|"+" Finish...");
	    }

}
