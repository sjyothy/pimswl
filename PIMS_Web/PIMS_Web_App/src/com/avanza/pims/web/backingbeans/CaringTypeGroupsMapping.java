package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.hsqldb.lib.HashMap;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.CaringType;
import com.avanza.pims.entity.CaringTypeUserGroupMapping;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ProcedureTask;
import com.avanza.pims.entity.ProcedureTaskGroup;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.ui.util.ResourceUtil;
import com.crystaldecisions.reports.queryengine.ca;

//TODO::

public class CaringTypeGroupsMapping extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private HtmlDataTable dataTable;
	private HtmlSelectOneMenu selectOneGroup = new HtmlSelectOneMenu();
	private HtmlCommandButton btnAddGroup = new HtmlCommandButton();

	private ArrayList<CaringTypeView> dataList = new ArrayList();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private List<SelectItem> selectUserGroups = new ArrayList<SelectItem>();
	private String selectOneUserGroup;

	private List<UserGroup> caringTypeGroups = new ArrayList<UserGroup>();

	@Override
	public void init() {
		super.init();

		if (!isPostBack()) {
			getUserGroups();
			// fillDataTable();

		}
	}

	private void fillDataTable() {
		setCaringTypeGroups(getSecUserGroups());

	}

	public void prerender() {
		super.prerender();
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlSelectOneMenu getSelectOneGroup() {
		return selectOneGroup;
	}

	public void setSelectOneGroup(HtmlSelectOneMenu selectOneGroup) {
		this.selectOneGroup = selectOneGroup;
	}

	public ArrayList<CaringTypeView> getDataList() {
		if (viewMap.get("CARING_TYPE_LIST") != null)
			return (ArrayList<CaringTypeView>) viewMap.get("CARING_TYPE_LIST");

		return dataList;
	}

	public void setDataList(ArrayList<CaringTypeView> dataList) {

		this.dataList = dataList;
		if (this.dataList != null)
			viewMap.put("CARING_TYPE_LIST", this.dataList);

	}

	private void getUserGroups() {
		logger.logInfo("getUserGroups() started...");

		try {
			CaringTypeView selectedCaringTypeView = (CaringTypeView) sessionMap
					.get("selectedCaringTypeView");

			selectUserGroups.clear();
			caringTypeGroups.clear();
			
			Set caringTypeUserGroupMappingSet = selectedCaringTypeView
					.getCaringTypeUserGroupMappings();

			HashMap tempHashmap = new HashMap();

			for (Object object : caringTypeUserGroupMappingSet) {

				CaringTypeUserGroupMapping caringTypeUserGroupMapping = (CaringTypeUserGroupMapping) object;
				tempHashmap.put(caringTypeUserGroupMapping.getSecUserGroup(),
						caringTypeUserGroupMapping);
			}

			for (UserGroup userGroup : getSecUserGroups()) {

				if (tempHashmap.containsKey(userGroup.getUserGroupId())) {
					caringTypeGroups.add(userGroup);
				} else {
					selectUserGroups.add(new SelectItem(userGroup
							.getUserGroupId(), userGroup.getPrimaryName()));
				}

			}
			viewMap.put("caringTypeGroups", caringTypeGroups);
			viewMap.put("selectUserGroups", selectUserGroups);

			if (this.dataList != null)
				this.setDataList(this.dataList);
			
			recordSize = caringTypeGroups.size();
			viewMap.put("recordSize", recordSize);

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getUserGroups() crashed ", e);
		}
	}

	public void addGroup() {

		String value = selectOneUserGroup;

		CaringTypeView selectedCaringTypeView = (CaringTypeView) sessionMap
				.get("selectedCaringTypeView");

		CaringType caringType = EntityManager.getBroker().findById(
				CaringType.class, selectedCaringTypeView.getCaringTypeId());

		CaringTypeUserGroupMapping newGroup = new CaringTypeUserGroupMapping();
		newGroup.setCreatedBy("admin");
		newGroup.setCreatedOn(new Date());
		newGroup.setCaringType(caringType);
		newGroup.setUpdatedBy("admin");
		newGroup.setUpdatedOn(new Date());
		newGroup.setSecUserGroup(value);

		if(value!=null)
			EntityManager.getBroker().add(newGroup);

		reload();

	}

	public void deleteGroup() {
		logger.logInfo("deleteGroup() started...");

		CaringTypeView selectedCaringTypeView = (CaringTypeView) sessionMap
				.get("selectedCaringTypeView");

		CaringType caringType = EntityManager.getBroker().findById(
				CaringType.class, selectedCaringTypeView.getCaringTypeId());
		
		UserGroup selectedUserGroup = (UserGroup) dataTable.getRowData();
		
		CaringTypeUserGroupMapping caringTypeUserGroupMapping = null;
		
		for(Object obj: selectedCaringTypeView.getCaringTypeUserGroupMappings()){
			caringTypeUserGroupMapping = (CaringTypeUserGroupMapping) obj;
			if(caringTypeUserGroupMapping.getCaringType().getCaringTypeId()==selectedCaringTypeView.getCaringTypeId() && caringTypeUserGroupMapping.getSecUserGroup().equalsIgnoreCase(selectedUserGroup.getUserGroupId())){
				break;
			}
		}
		
		EntityManager.getBroker().delete(caringTypeUserGroupMapping);
		
		reload();
	}

	public void reload() {

		CaringTypeView selectedCaringTypeView = (CaringTypeView) sessionMap
				.get("selectedCaringTypeView");

		CaringType caringType = EntityManager.getBroker().findById(
				CaringType.class, selectedCaringTypeView.getCaringTypeId());
		selectedCaringTypeView.setCaringTypeUserGroupMappings(caringType
				.getCaringTypeUserGroupMappings());

		sessionMap.put("selectedCaringTypeView", selectedCaringTypeView);

		getUserGroups();
	}

	private List<UserGroup> getSecUserGroups() {
		logger.logInfo("getSecUserGroups() started...");

		List<UserGroup> userGroups = null;
		try {
			Search query = new Search();
			query.clear();
			query.addFrom(UserGroupDbImpl.class);
			userGroups = SecurityManager.findGroup(query);
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getSecUserGroups() crashed ", e);
		}

		return userGroups;
	}
	
	public void closeWindowAndSubmit() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();


		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:closeWindowSubmit();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	public List<SelectItem> getSelectUserGroups() {
		if (viewMap.get("selectUserGroups") != null)
			selectUserGroups = (List<SelectItem>) viewMap
					.get("selectUserGroups");
		return selectUserGroups;
	}

	public void setSelectUserGroups(List<SelectItem> selectUserGroups) {
		this.selectUserGroups = selectUserGroups;
	}

	public String getSelectOneUserGroup() {
		return selectOneUserGroup;
	}

	public void setSelectOneUserGroup(String selectOneUserGroup) {
		this.selectOneUserGroup = selectOneUserGroup;
	}

	public List<UserGroup> getCaringTypeGroups() {
		if(viewMap.get("caringTypeGroups") != null)
			caringTypeGroups = (List<UserGroup>) viewMap.get("caringTypeGroups");
		return caringTypeGroups;
	}

	public void setCaringTypeGroups(List<UserGroup> caringTypeGroups) {
		this.caringTypeGroups = caringTypeGroups;
	}

	public HtmlCommandButton getBtnAddGroup() {
		return btnAddGroup;
	}

	public void setBtnAddGroup(HtmlCommandButton btnAddGroup) {
		this.btnAddGroup = btnAddGroup;
	}

	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(viewMap.get("recordSize") != null)
			recordSize = (Integer) viewMap.get("recordSize");
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	
}
