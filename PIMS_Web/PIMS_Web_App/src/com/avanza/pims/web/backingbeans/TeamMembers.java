package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.web.controller.AbstractController;

public class TeamMembers extends AbstractController{
	private HtmlDataTable dataTable;
	private List<UserDbImpl> dataList = new ArrayList();
	
	public List<UserDbImpl> getSecUserDataList() {
		if (dataList == null || dataList.size()>0) {
			loadDataList(); // Preload by lazy loading.
		}
		return dataList;
	}


	private void loadDataList() {
		try{
		UtilityManager objUtilityManager = new UtilityManager();
		List<UserDbImpl> userList =objUtilityManager.getUsers();
		for(int i = 0;i<userList.size();i++){

			dataList.add(userList.get(i));
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if(dataList==null||dataList.size()==0)
			loadDataList();


	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

}
