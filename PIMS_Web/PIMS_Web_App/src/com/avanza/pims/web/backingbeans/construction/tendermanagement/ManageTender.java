package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

import com.avanza.pims.ws.vo.ConstructionTenderDetailsView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExtendApplicationFilterView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderPropertyView;
import com.avanza.pims.ws.vo.TenderProposalView;
import com.avanza.pims.ws.vo.TenderView;

import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;

public class ManageTender extends AbstractController {
	//Common Initialization
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	//Common Initialization
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private transient Logger logger = Logger.getLogger(ManageTender.class);
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil = new CommonUtil();
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	//Contractors Tab
	private HtmlCommandButton addContractor = new HtmlCommandButton();
	//Tender Details Tab
	private HtmlInputText tenderNumber = new HtmlInputText();
	private HtmlInputHidden hdnTenderId = new HtmlInputHidden();
	private HtmlInputText tenderStatue =new HtmlInputText();
	private HtmlInputText tenderDescription = new HtmlInputText();
	private HtmlInputTextarea actionRemarks=new HtmlInputTextarea();
	private Date endDate = new Date();
	private Date startDate = new Date();
	private HtmlSelectOneMenu tenderType = new HtmlSelectOneMenu();
	private List<SelectItem> tenderTypeList = new ArrayList<SelectItem>();
	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton sendButton = new HtmlCommandButton();

	//Property Details Tab
	private HtmlDataTable propertyGrid = new HtmlDataTable();
	private List<PropertyView> propertyDataList;
	private HtmlCommandButton addUnit = new HtmlCommandButton();
	private HtmlCommandButton addProperty = new HtmlCommandButton();
	//Scope of Work Tab
	private HtmlSelectOneMenu workTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> workTypeItems = new ArrayList<SelectItem>();
	private HtmlInputTextarea scopeWorkDescription = new HtmlInputTextarea();
	private HtmlDataTable scopeOfWorkDataTable = new HtmlDataTable();
//	private List<WorkScopeView> scopeOfWorkList= new ArrayList<WorkScopeView>();
	private HtmlCommandButton saveScopeOfWork = new HtmlCommandButton();


	//Proposals View Tab
	private HtmlDataTable proposalDataTable = new HtmlDataTable();
	private List<TenderProposalView> proposalDataList = new ArrayList<TenderProposalView>();

	//Inquries App Tab
	private HtmlDataTable inquiryDataTable = new HtmlDataTable();
	private List<TenderInquiryView> tenderInquiryList = new ArrayList<TenderInquiryView>();
	private HtmlCommandButton replyToSelected = new HtmlCommandButton();
	//Extended App Tab
	private HtmlDataTable extendedDataTable = new HtmlDataTable();
	private List<ExtendApplicationFilterView> extendedDataList = new ArrayList<ExtendApplicationFilterView>();
	private HtmlCommandButton extend= new HtmlCommandButton();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private Boolean renderAction;

	private final String noteOwner = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_SERVICE_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if(!isPostBack())
			{
				Long tenderID=null;
			 	CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				if( sessionMap.get(WebConstants.Tender.TENDER_ID)!=null )
				{
				    tenderID= Long.valueOf(sessionMap.remove(WebConstants.Tender.TENDER_ID).toString());
				    viewMap.put(WebConstants.Tender.TENDER_ID,tenderID);
				    CommonUtil.loadAttachmentsAndComments(tenderID.toString());
					ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
					setProposalDataList( constructionServiceAgent.getTenderProposalList(tenderID) );

					sessionMap.remove("reloadOpener");
					for(TenderProposalView tenderProposalView:proposalDataList)
					{
						if(tenderProposalView.getIsWinner())
						{
							viewMap.put("HIDE_ACTION", true);
							break;
						}
					}
				}
				modeView();
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.LogException("init|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
			Long tenderID=null;
			if(viewMap.get(WebConstants.Tender.TENDER_ID)!=null)
			{
			    tenderID= Long.valueOf(viewMap.get(WebConstants.Tender.TENDER_ID).toString());
				if(!viewMap.containsKey("requestList"))
				{
					loadRequestList(tenderID);
				}
				if(!viewMap.containsKey("TENDER_VIEW"))
				{
				    populateTabs(tenderID);
				}
				if ( viewMap.get("InquiryList") != null )
				{
					tenderInquiryList = (List<TenderInquiryView>) viewMap.get("InquiryList");
				}
				else
				{
		            HashMap searchMap = new HashMap ();
		            searchMap.put("tenderId", tenderID);
		            ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
		            tenderInquiryList = constructionServiceAgent.getAllTenderInquiries(searchMap);
		        	if ( tenderInquiryList != null )
		        	{
		    			viewMap.put("InquiryList",tenderInquiryList);
		        	}
				}
			}
		}
		catch ( Exception e)
		{
			logger.LogException("prerender|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	private void saveAttachmentComments(String sysNote,long id, String... placeHolderVals) throws Exception
	{
		CommonUtil.saveAttachments(id);
		CommonUtil.saveComments(id, noteOwner);
		if(sysNote != null && sysNote.length()>0)
		{
		 CommonUtil.saveSystemComments(noteOwner, sysNote, id ,placeHolderVals);
		}

	}
	public void requestHistoryTabClick()
	{
		TenderView view =(TenderView)viewMap.get("TENDER_VIEW");
		if( view != null && view.getTenderId() != null )
		{
			try
			{
			  RequestHistoryController rhc=new RequestHistoryController();
			  rhc.getAllRequestTasksForRequest(noteOwner,view.getTenderId().toString());
			}
			catch(Exception ex)
			{
				logger.LogException( "requestHistoryTabClick|Error Occured..",ex);
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadRequestList(Long tenderId) throws Exception
	{
		logger.logInfo("loadRequestList() started...");
			extendedDataList= new ArrayList<ExtendApplicationFilterView>();
			Map argMap = getArgMap();

			List<ExtendApplicationFilterView> requestViewList = requestServiceAgent.getExtendApplicationRequestsForTender(tenderId,WebConstants.REQUEST_TYPE_TENDER_EXTEND_APPLICATION, getArgMap());
			viewMap.put("requestList", requestViewList);
			viewMap.put("recordSize", requestViewList.size());
//			if(requestViewList.isEmpty())
//			{
//				errorMessages = new ArrayList<String>();
//				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
//			}
			logger.logInfo("loadRequestList() completed successfully!!!");
	}
	@SuppressWarnings( "unchecked" )
	private Map getArgMap()throws Exception
	{
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	@SuppressWarnings( "unchecked" )
	private void populateTabs(Long tenderId) throws Exception
	{
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		TenderView tenderView = csa.getTenderByIdForTender(tenderId);
		viewMap.put("TENDER_VIEW", tenderView);
		tenderNumber.setValue(tenderView.getTenderNumber());
		if(getIsArabicLocale())
			tenderStatue.setValue(tenderView.getStatusAr());
		else
			tenderStatue.setValue(tenderView.getStatusEn());

		startDate = tenderView.getIssueDate();
		viewMap.put("START_DATE", startDate);
		hdnTenderId.setValue(String.valueOf(tenderId));
		endDate = tenderView.getEndDate();
		viewMap.put("END_DATE", endDate);

		tenderDescription.setValue(tenderView.getRemarks());
		tenderType.setValue(tenderView.getTenderTypeView().getTenderTypeId().toString());

		PropertyView propertyView;
		if(propertyDataList==null)
			propertyDataList = new ArrayList<PropertyView>();
		for(TenderPropertyView tenderPropertyView : tenderView.getPropertyViewList())
		{
			propertyView = new PropertyView();
			propertyView.setTenderPropertyId(tenderPropertyView.getTenderPropertyId());
			propertyView.setTenderPropertyIsDeleted(tenderPropertyView.getIsDeleted());
			propertyView.setPropertyNumber(tenderPropertyView.getPropertyNumber());
			propertyView.setPropertyId(tenderPropertyView.getPropertyId());
			propertyView.setCommercialName(tenderPropertyView.getCommercialName());
			propertyView.setPropertyTypeAr(tenderPropertyView.getPropertyTypeAr());
			propertyView.setPropertyTypeEn(tenderPropertyView.getPropertyTypeEn());
			propertyView.setTypeId(tenderPropertyView.getTypeId());
			propertyView.setUnitRefNo(tenderPropertyView.getUnitRefNo());
			propertyView.setUnitId(tenderPropertyView.getUnitId());
			propertyView.setEmirateNameAr(tenderPropertyView.getEmirateNameAr());
			propertyView.setEmirateNameEn(tenderPropertyView.getEmirateNameEn());
			propertyDataList.add(propertyView);

		}
		viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);
		viewMap.put(WebConstants.Tender.CONTRACTORS_LIST,tenderView.getContractorsViewList());
		viewMap.put(WebConstants.Tender.SCOPE_OF_WORK_LIST,tenderView.getTenderWorkScopeViewList());
		sessionMap.put(WebConstants.Tender.TENDER_VIEW,tenderView);

	}
	@SuppressWarnings("unchecked")
	public String onDelete()
	{
		String taskType = "";
		try{


	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();

	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);

				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is: " + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		/*WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	   		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);*/

		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch (Exception exception) {
			logger.LogException( "onDelete| crashed ", exception);
		}
		return taskType;
	}

	public String onDeselectWinners(){
		String METHOD_NAME = "onDeselectWinners()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{

			TenderView tenderDetailsView = (TenderView) getTenderView();

			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.TENDER_STATUS_APPROVED);
			csa.setContractAsWinner( Long.valueOf(tenderDetailsView.getTenderId()),
					                 null, ddv.getDomainDataId(),getLoggedInUser()
					                );
			this.unmarkAllProposalsFirst(tenderDetailsView.getTenderId());
			successMessages.add(ResourceUtil.getInstance().getProperty("tenderProposal.DeselectWinnersMessage") );
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");

		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return path;
	}
	@SuppressWarnings( "unchecked" )
	public void onWinnerSelected()
	{
		try
		{
			//TenderView tenderView =(TenderView)viewMap.get("TENDER_VIEW");
			TenderProposalView row = (TenderProposalView) proposalDataTable.getRowData();
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.WINNER_SELECTED);
			csa.setContractAsWinner(
					                 Long.valueOf(row.getTenderId()),
					                 Long.valueOf(row.getContractorId()), ddv.getDomainDataId(),getLoggedInUser()
					                );
			row.setIsWinner(true);
			successMessages.clear();
			viewMap.put("TENDER_ID",row.getTenderId());
			sendNotification(row);

			this.unmarkAllProposalsFirst(Long.valueOf(row.getTenderId()));
			this.changeProposalStatus(WebConstants.TENDER_PROPOSAL_STATUS_WINNER);

			String winner =   row.getProposalNumber()+"/"+
			                 (
			                		 ( row.getContractorNameAr()!= null && row.getContractorNameAr().length() > 0 )?row.getContractorNameAr():
			                			                                                                            row.getContractorNameEn()
			                  );

			saveAttachmentComments( "tender.event.winnerSelected", Long.valueOf( row.getTenderId() ), winner );
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MARK_AS_WINNER));
			viewMap.put("MARK_WINNER_DONE",true);
			viewMap.put("HIDE_ACTION", true);
		}
        catch ( Exception e)
        {
        	logger.LogException("onWinnerSelected|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private Boolean unmarkAllProposalsFirst(Long tenderId){
		try
		{
			ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
			List<TenderProposalView> proposalViews = constructionServiceAgent.getTenderProposalList(tenderId);
			for(TenderProposalView proposalView : proposalViews){
				this.changeProposalStatusByProposal(proposalView, Constant.TENDER_PROPOSAL_STATUS_OPENED);
			}
			return true;
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposal() --- exception --- ", e);
			return false;
		}
	}
	private void changeProposalStatusByProposal(TenderProposalView selectedTenderProposalView, String status) throws Exception
	{
		DomainData domainData = new UtilityManager().getDomainDataByValue( status );
		selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
		selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
		selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
		ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
		selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
		constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
		if(viewMap.get("TENDER_ID")!=null)
		{
			setProposalDataList(
					             constructionServiceAgent.getTenderProposalList(
					            		                                        Long.parseLong(  ( viewMap.get("TENDER_ID").toString() ) )
					            		                                       )
					           );
		}

	}


	@SuppressWarnings( "unchecked" )
	public void viewLink_action()
	{
		try
		{
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			String javaScriptText = "javascript:showViewTenderProposalPopup();";
			sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
			openPopup(javaScriptText);

		}
		catch (Exception e)
		{
			logger.LogException("viewLink_action failed",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	@SuppressWarnings( "unchecked" )
	private void sendNotification(TenderProposalView selectedTenderProposalView)
	{
		List<TenderProposalView> proposalList = this.getProposalDataList();
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		PersonView contractor = new PersonView();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		TenderView tender = new TenderView();

		try
		{
		if(viewMap.containsKey("TENDER_VIEW"))
				tender = (TenderView)viewMap.get("TENDER_VIEW");

		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		eventAttributesValueMap.put("TENDER_NUMBER", tender.getTenderNumber());
		eventAttributesValueMap.put("TENDER_TYPE",tender.getTenderTypeView().getDescriptionEn());


		for(TenderProposalView proposalView : proposalList)
		{
			eventAttributesValueMap.put("PROPOSAL_NUMBER", proposalView.getProposalNumber());
			contractor = psa.getPersonInformation(new Long(proposalView.getContractorId()));
			eventAttributesValueMap.put("CONTRACTOR_NAME", proposalView.getContractorNameEn());

			if(proposalView.getTenderProposalId().compareTo(selectedTenderProposalView.getTenderProposalId()) == 0)
			{
				eventAttributesValueMap.put("PROPOSAL_ACTION", "accepted");
			}
			else
				eventAttributesValueMap.put("PROPOSAL_ACTION", "rejected");

			contactInfoList = getContactInfoList(contractor);
			generateNotification(WebConstants.Notification_MetaEvents.Event_Service_Tender_Status_Notification,contactInfoList, eventAttributesValueMap,null);
		}
		}
		catch (PimsBusinessException ex)
		{
			logger.LogException("notification failed",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}

	
//	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
//	{
//		final String METHOD_NAME = "generateNotification()";
//		boolean success = false;
//		try
//		{
//			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
//			if ( recipientList != null && recipientList.size() > 0 )
//			{
//				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
//				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
//				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
//					event.setValue( eventAttributesValueMap );
//				if ( notificationType != null )
//					notificationProvider.fireEvent(event, recipientList, notificationType);
//				else
//					notificationProvider.fireEvent(event, recipientList);
//
//			}
//			success = true;
//			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
//		}
//		catch (Exception exception)
//		{
//			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
//		}
//
//		return success;
//	}
	
	@SuppressWarnings( "unchecked" )
	public List<ContactInfo> getContactInfoList(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();

    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(personView.getPersonId().toString(), personView.getCellNumber(), null, ciView.getEmail()));
            }
    	}
    	return emailList;
	}

	@SuppressWarnings( "unchecked" )
	private void changeProposalStatus(String status) throws Exception
	{
		DomainData domainData = new UtilityManager().getDomainDataByValue( status );
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
		selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
		selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
		ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
		selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
		constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
		if(viewMap.get("TENDER_ID")!=null)
		{
			setProposalDataList(
					             constructionServiceAgent.getTenderProposalList(
					            		                                        Long.parseLong(  ( viewMap.get("TENDER_ID").toString() ) )
					            		                                       )
					           );
		}

	}

	@SuppressWarnings( "unchecked" )
	private void openPopup(String javaScriptText)throws Exception
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
	}

	private String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsEnglishLocale() {


		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	public HtmlInputHidden getHdnTenderId() {
		return hdnTenderId;
	}
	public void setHdnTenderId(HtmlInputHidden hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	@SuppressWarnings( "unchecked" )
	public List<TenderProposalView> getProposalDataList()
	{
		if( viewMap.get( WebConstants.Tender.PROPOSAL_VIEW_LIST ) != null )
		{
			proposalDataList  = (List<TenderProposalView>)viewMap.get( WebConstants.Tender.PROPOSAL_VIEW_LIST);
		}
		return proposalDataList;
	}
	@SuppressWarnings( "unchecked" )
	public void setProposalDataList(List<TenderProposalView> proposalDataList)
	{
		this.proposalDataList = proposalDataList;
		if(this.proposalDataList != null )
		{
			viewMap.put(WebConstants.Tender.PROPOSAL_VIEW_LIST, this.proposalDataList );
		}
	}
	public HtmlDataTable getProposalDataTable() {
		return proposalDataTable;
	}
	public void setProposalDataTable(HtmlDataTable proposalDataTable) {
		this.proposalDataTable = proposalDataTable;
	}




	public String completeRequest()
	{
		generateNotification(WebConstants.Notification_MetaEvents.Event_TENDER_WONTENDER);
		return "";
	}
	public void generateNotification(String eventType)
	{
	    try
	    {
	
	    	PersonView personView = new PersonView();
	    	ContractView contractView = new ContractView();
	        List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
	
	    	if (
	    			viewMap.containsKey("APPLICANT_VIEW_FOR_NOTIFICATION") &&
	    			viewMap.containsKey("Contract_VIEW_FOR_PAYMENT")
	    	   ) 
	    	{
	    		personView = (PersonView)viewMap.get("APPLICANT_VIEW_FOR_NOTIFICATION");
	    		contractView = (ContractView)viewMap.get("Contract_VIEW_FOR_PAYMENT");
	    		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);	
	    		eventAttributesValueMap.put("PERSON_NAME",personView.getPersonFullName());
	    		eventAttributesValueMap.put("CONTRACT_NUMBER",contractView.getContractNumber());

//	    		getNotificationPlaceHolder(event,contractView,personView);
	    		personsEmailList = getEmailContactInfos((PersonView)personView);
	    		generateNotification( eventType, personsEmailList , eventAttributesValueMap, null);
	            
	
			}
	    }
	    catch(Exception ex)
	    {
	          logger.LogException( "generateNotification|Finish", ex);
	    }

	}
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
//	private void  getNotificationPlaceHolder(Event placeHolderMap,ContractView contractView,PersonView perView)
//	{
//		
//		placeHolderMap.setValue("PERSON_NAME",perView.getPersonFullName());
//		placeHolderMap.setValue("CONTRACT_NUMBER",contractView.getContractNumber());
//
//
//	}
	public Integer getInquiryAppRecordSize() {
		return tenderInquiryList.size();
	}
	  public String btnDeleteInquiry_Click(){

			TenderInquiryView tIV = (TenderInquiryView) inquiryDataTable.getRowData();
			Integer index =  inquiryDataTable.getRowIndex();
			viewMap.put("delete", index);

			Boolean flage = false;
			successMessages = new ArrayList<String>();
			try {
				flage = new ConstructionServiceAgent().setTenderInquiryToDeleted(tIV.getRequestId());

				if(flage){
					index = (Integer) viewMap.get("delete");
					List<TenderInquiryView> tempList = getTenderInquiryList();
					tempList.remove(index.intValue());
					setTenderInquiryList(tempList);
					successMessages.add(CommonUtil.getBundleMessage("tenderInquiryReplies.msg.DetailRequired"));
				}

			} catch (PimsBusinessException e) {
				logger.LogException("Exception in btnDeleteViolation_Click::::::::::::::", e);
			}

			return "EditViolation";


		}
	  public TimeZone getTimeZone()
		{
			 return TimeZone.getDefault();

		}
	public Boolean getRenderAction()
	{
		renderAction=true;
		if( viewMap.get("HIDE_ACTION")!=null && Boolean.valueOf( viewMap.get("HIDE_ACTION").toString() )  )
		{
			renderAction =false;
		}
		return renderAction;
	}
	public void setRenderAction(Boolean renderAction) {

		this.renderAction = renderAction;
	}
	@SuppressWarnings( "unchecked" )
	private void modeView()
	{
		tenderNumber.setReadonly(true);
		tenderType.setReadonly(true);
		tenderDescription.setReadonly(true);
		sendButton.setRendered(false);
		addContractor.setRendered(false);
		addProperty.setRendered(false);
		addUnit.setRendered(false);
		cancelButton.setRendered(false);
		saveButton.setRendered(false);
		viewMap.put("canAddAttachment",false);
		viewMap.put("canAddWorkScope",false);
		viewMap.put("modeAction",true);
		viewMap.put("canAddNote",false);
		viewMap.put("CAN_DELETE_TENDER_PROPERTY",false);
		viewMap.put("CAN_DELETE_TENDER_CONTRACTOR",false);
		viewMap.put("CAN_DELETE_TENDER_SCOPE_OF_WORK",false);
		viewMap.put("CAN_ADD_TENDER_SCOPE_OF_WORK",false);




	}
	public Boolean getCanDeleteTenderProperty()
	{
        if (viewMap.get("CAN_DELETE_TENDER_PROPERTY")!=null)
        	return false;
        else
        	return true;

	}

	public String  cancel(){

		return "SearchServiceTender";

	}
	public HtmlInputTextarea getActionRemarks() {
		return actionRemarks;
	}
	public void setActionRemarks(HtmlInputTextarea actionRemarks) {
		this.actionRemarks = actionRemarks;
	}


	public HtmlCommandButton getAddProperty() {
		return addProperty;
	}
	public void setAddProperty(HtmlCommandButton addProperty) {
		this.addProperty = addProperty;
	}
	public HtmlCommandButton getAddUnit() {
		return addUnit;
	}
	public void setAddUnit(HtmlCommandButton addUnit) {
		this.addUnit = addUnit;
	}
	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}



	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public HtmlCommandButton getExtend() {
		return extend;
	}
	public void setExtend(HtmlCommandButton extend) {
		this.extend = extend;
	}
	public List<ExtendApplicationFilterView> getExtendedDataList() {
		if(viewMap.containsKey("requestList"))
			extendedDataList = (List<ExtendApplicationFilterView>) viewMap.get("requestList");

		if(extendedDataList == null)
			extendedDataList = new ArrayList<ExtendApplicationFilterView>();


		return extendedDataList;
	}
	public void setExtendedDataList(List<ExtendApplicationFilterView> extendedDataList) {
		this.extendedDataList = extendedDataList;
	}
	public HtmlDataTable getExtendedDataTable() {
		return extendedDataTable;
	}
	public void setExtendedDataTable(HtmlDataTable extendedDataTable) {
		this.extendedDataTable = extendedDataTable;
	}

	public List<TenderInquiryView> getTenderInquiryList() {
		if(viewMap.containsKey("TENDER_INQUIRY_LIST"))
			tenderInquiryList = (List<TenderInquiryView>)viewMap.get("TENDER_INQUIRY_LIST");
		return tenderInquiryList;
	}

	public void setTenderInquiryList(List<TenderInquiryView> tenderInquiryList) {
		this.tenderInquiryList = tenderInquiryList;

		if(this.tenderInquiryList!=null)
			viewMap.put("TENDER_INQUIRY_LIST",this.tenderInquiryList);
	}

	public HtmlDataTable getInquiryDataTable() {
		return inquiryDataTable;
	}
	public void setInquiryDataTable(HtmlDataTable inquiryDataTable) {
		this.inquiryDataTable = inquiryDataTable;
	}

	public List<PropertyView> getPropertyDataList() {
		if(viewMap.containsKey(WebConstants.Contract.CONTRACT_UNIT_LIST))
			propertyDataList = (List<PropertyView>) viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);

		if(propertyDataList == null)
			propertyDataList = new ArrayList<PropertyView>();


		return propertyDataList;
	}
	public void setPropertyDataList(List<PropertyView> propertyDataList) {
		this.propertyDataList = propertyDataList;
	}
	public HtmlDataTable getPropertyGrid() {
		return propertyGrid;
	}
	public void setPropertyGrid(HtmlDataTable propertyGrid) {
		this.propertyGrid = propertyGrid;
	}


	public HtmlCommandButton getReplyToSelected() {
		return replyToSelected;
	}
	public void setReplyToSelected(HtmlCommandButton replyToSelected) {
		this.replyToSelected = replyToSelected;
	}
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	public HtmlCommandButton getSaveScopeOfWork() {
		return saveScopeOfWork;
	}
	public void setSaveScopeOfWork(HtmlCommandButton saveScopeOfWork) {
		this.saveScopeOfWork = saveScopeOfWork;
	}
	public HtmlDataTable getScopeOfWorkDataTable() {
		return scopeOfWorkDataTable;
	}
	public void setScopeOfWorkDataTable(HtmlDataTable scopeOfWorkDataTable) {
		this.scopeOfWorkDataTable = scopeOfWorkDataTable;
	}
	public HtmlInputTextarea getScopeWorkDescription() {
		return scopeWorkDescription;
	}
	public void setScopeWorkDescription(HtmlInputTextarea scopeWorkDescription) {
		this.scopeWorkDescription = scopeWorkDescription;
	}
	public HtmlCommandButton getSendButton() {
		return sendButton;
	}
	public void setSendButton(HtmlCommandButton sendButton) {
		this.sendButton = sendButton;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public HtmlInputText getTenderDescription() {
		return tenderDescription;
	}
	public void setTenderDescription(HtmlInputText tenderDescription) {
		this.tenderDescription = tenderDescription;
	}

	public HtmlInputText getTenderNumber() {
		return tenderNumber;
	}
	public void setTenderNumber(HtmlInputText tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public HtmlInputText getTenderStatue() {
		return tenderStatue;
	}
	public void setTenderStatue(HtmlInputText tenderStatue) {
		this.tenderStatue = tenderStatue;
	}
	public HtmlSelectOneMenu getTenderType() {
		return tenderType;
	}
	public void setTenderType(HtmlSelectOneMenu tenderType) {
		this.tenderType = tenderType;
	}
	public List<SelectItem> getTenderTypeList() {
		return tenderTypeList;
	}
	public void setTenderTypeList(List<SelectItem> tenderTypeList) {
		this.tenderTypeList = tenderTypeList;
	}
	public List<SelectItem> getWorkTypeItems() {
		return workTypeItems;
	}
	public void setWorkTypeItems(List<SelectItem> workTypeItems) {
		this.workTypeItems = workTypeItems;
	}
	public HtmlSelectOneMenu getWorkTypeMenu() {
		return workTypeMenu;
	}
	public void setWorkTypeMenu(HtmlSelectOneMenu workTypeMenu) {
		this.workTypeMenu = workTypeMenu;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getLocale(){

		return new CommonUtil().getLocale();
	}

	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}


	//Property Tab Methods
	public void cmdPropertyDelete_Click(){

	}
	public void showUnitsPopup(){

	}
	public void showPropertyPopup(){

	}
	public Integer getPropertyRecordSize(){

		if(getFacesContext().getViewRoot().getAttributes().containsKey("PROPERTY_DATA_LIST"))
					propertyDataList = (ArrayList<PropertyView>)viewMap.get("PROPERTY_DATA_LIST");

		return propertyDataList.size();
	}
	public HtmlCommandButton getAddContractor() {
		return addContractor;
	}
	public void setAddContractor(HtmlCommandButton addContractor) {
		this.addContractor = addContractor;
	}


	public String approveButton_action() {
		return null;
	}

	public String rejectButton_action() {
		return null;
	}
	public Boolean getMarkWinnerLink()
	{
		if(viewMap.containsKey("MARK_WINNER_DONE"))
		return false;
		else
		return true;
	}

	public TenderView getTenderView(){
		TenderView view =(TenderView)viewMap.get("TENDER_VIEW");
		return view != null? view : new TenderView();
	}
}
