package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PortFolioDetailsCriteria;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class SummaryInvestmentEvaluationReport extends AbstractController {
	
	

	private static Logger logger = Logger.getLogger("SummaryInvestmentEvaluationReport");
	
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	
	PortFolioDetailsCriteria portfolioDetailsCriteria;
	
	private List<String> errorMessages;
	private List<String> successMessages;

	public SummaryInvestmentEvaluationReport() {
		if (CommonUtil.getIsEnglishLocale()) { // for english
			portfolioDetailsCriteria = new PortFolioDetailsCriteria(ReportConstant.Report.SUMMARY_INVESTMENT_EVALUATION_REPORT_AR,
					ReportConstant.Processor.SUMMARY_INVESTMENT_EVALUATION_REPORT,CommonUtil.getLoggedInUser());
		}
		else { // for arabic
			portfolioDetailsCriteria = new PortFolioDetailsCriteria(ReportConstant.Report.SUMMARY_INVESTMENT_EVALUATION_REPORT_AR,
					ReportConstant.Processor.SUMMARY_INVESTMENT_EVALUATION_REPORT,CommonUtil.getLoggedInUser());
		}
		
	}
	
	public void init() {

	}

	public void prerender() {
		if (portfolioDetailsCriteria.getYear() == null|| portfolioDetailsCriteria.getYear().length() <= 0) {
			Calendar cal = new GregorianCalendar();
			this.portfolioDetailsCriteria.setYear(Integer.toString(cal.get(Calendar.YEAR)));
		}
	}

	public String cmdView_Click() {

		if (validate()) {
			HttpServletRequest request = (HttpServletRequest) getFacesContext()
					.getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY,portfolioDetailsCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		return null;
	}

	private Boolean validate() {
		Boolean flage = true;
		errorMessages = new ArrayList<String>();
		return flage;
	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,	AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	public PortFolioDetailsCriteria getPortfolioDetailsCriteria() {
		return portfolioDetailsCriteria;
	}

	public void setPortfolioDetailsCriteria(
			PortFolioDetailsCriteria portfolioDetailsCriteria) {
		this.portfolioDetailsCriteria = portfolioDetailsCriteria;
	}

}
