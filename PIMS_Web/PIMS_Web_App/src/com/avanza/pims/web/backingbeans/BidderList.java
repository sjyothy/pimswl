package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.ui.util.ResourceUtil;

public class BidderList extends AbstractController {

	// Fields

	private transient Logger logger = Logger.getLogger(BidderList.class);

	private HtmlInputText inputTextSocialSecNo;

	private HtmlInputText inputTextFirstName;

	private HtmlInputText inputTextPassport;

	private HtmlInputText inputTextLastName;

	private HtmlCommandButton commandButtonSearch;

	private HtmlDataTable dataTable;

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private List<String> errorMessages;

	private boolean fromDelete = false;

	private BidderView dataItem = new BidderView();

	private BidderView bidderViewDelete = new BidderView();

	BidderView bidderView = new BidderView();

	private List<BidderView> dataList = new ArrayList();

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	CommonUtil commonUtil = new CommonUtil();

	// Constructor
	/** default constructor */
	public BidderList() {
	}

	// Methods
	public void init() {
		super.init();
		if (!isPostBack()) {

			// loadDataList();

		}
	}

	public List<BidderView> getBidderDataList() {
		if (dataList == null || dataList.size() == 0) {
			if (viewMap.containsKey("BIDDER_LIST"))
				dataList = (ArrayList) viewMap.get("BIDDER_LIST");
			// else
			// loadDataList();
		}
		return dataList;
	}

	private void loadDataList() {
		String methodName = "loadDataList";
		logger.logInfo(methodName + " started...");

		try {

			if (inputTextFirstName != null
					&& inputTextFirstName.getValue() != null
					&& !inputTextFirstName.getValue().toString().equals(""))
				bidderView.setFirstName(inputTextFirstName.getValue()
						.toString());
			if (inputTextLastName != null
					&& inputTextLastName.getValue() != null
					&& !inputTextLastName.getValue().toString().equals(""))
				bidderView.setLastName(inputTextLastName.getValue().toString());
			if (inputTextSocialSecNo != null
					&& inputTextSocialSecNo.getValue() != null
					&& !inputTextSocialSecNo.getValue().toString().equals(""))
				bidderView.setSocialSecNumber(inputTextSocialSecNo.getValue()
						.toString());
			if (inputTextPassport != null
					&& inputTextPassport.getValue() != null
					&& !inputTextPassport.getValue().toString().equals(""))
				bidderView.setPassportNumber(inputTextPassport.getValue()
						.toString());

			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			List<BidderView> bidders = null;// pSAgent.getBidderLike(bidderView);
			dataList = bidders;

			logger.logInfo("Record Found In Load Data List : %s", dataList
					.size());

			if (dataList.size() == 0) {
				errorMessages = new ArrayList<String>();
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));

			}

			viewMap.put("BIDDER_LIST", bidders);
			recordSize = bidders.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();

			fromDelete = false;
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName + " crashed ...", exception);
			exception.printStackTrace();
		}
		logger.logInfo(methodName + " ended...");
	}

	public String search() {
		String methodName = "search";
		logger.logInfo(methodName + " started...");
		if (dataList != null) {
			loadDataList();
		}
		logger.logInfo(methodName + " ended...");
		return "";
	}

	//	
	// public String selectDataItem(){
	// System.out.println("INSIDE selectDataItem");
	// //dataItem = (BidderView) dataTable.getRowData();
	//
	// FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("bidder",
	// dataItem);
	//		
	//		
	// return "";
	//		
	// }
	public String theCall() {
		System.out.println("the Call is called.......");
		return "";
	}

	// Getters and Setters
	public HtmlInputText getInputTextSocialSecNo() {
		return inputTextSocialSecNo;
	}

	public void setInputTextSocialSecNo(HtmlInputText inputSocialSecNo) {
		this.inputTextSocialSecNo = inputSocialSecNo;
	}

	public HtmlInputText getInputTextFirstName() {
		return inputTextFirstName;
	}

	public void setInputTextFirstName(HtmlInputText inputTextFirstName) {
		this.inputTextFirstName = inputTextFirstName;
	}

	public HtmlInputText getInputTextPassport() {
		return inputTextPassport;
	}

	public void setInputTextPassport(HtmlInputText inputTextPassport) {
		this.inputTextPassport = inputTextPassport;
	}

	public HtmlInputText getInputTextLastName() {
		return inputTextLastName;
	}

	public void setInputTextLastName(HtmlInputText inputTextLastName) {
		this.inputTextLastName = inputTextLastName;
	}

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public BidderView getDataItem() {
		return dataItem;
	}

	public void setDataItem(BidderView dataItem) {
		this.dataItem = dataItem;
	}

	public List<BidderView> getDataList() {
		return dataList;
	}

	public void setDataList(List<BidderView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	public String cmdBidderName_Click() {

		BidderView bidderView = (BidderView) dataTable.getRowData();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			facesContext.getExternalContext().getRequestMap().put("bidderId",
					bidderView.getBidderId());
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}

		return "success";

	}

	public void cmdDelete_Click(javax.faces.event.ActionEvent event) {
		// Write your server side code here
		// fromDelete = true;
		final String viewId = "/BidderList.jsp";
		BidderView bidderView = (BidderView) dataTable.getRowData();
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			bidderView.setIsDeleted(1L);
			//psa.updateBidder(bidderView);

			loadDataList();

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}
	}

	public void cmdStatus_Click(javax.faces.event.ActionEvent event) {
		// Write your server side code here
		// fromDelete = true;
		final String viewId = "/BidderList.jsp";
		BidderView bidderView = (BidderView) dataTable.getRowData();
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			if (bidderView.getRecordStatus() == 1) {
				bidderView.setRecordStatus(0L);
			} else {
				bidderView.setRecordStatus(1L);
			}
			//psa.updateBidder(bidderView);

			loadDataList();

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}

	}

	public String btnAddBidder_Click() {
		return "AddBidder";

	}

	public BidderView getBidderView() {
		return bidderView;
	}

	public void setBidderView(BidderView bidderView) {
		this.bidderView = bidderView;
	}

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = 10;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}

	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}

}
