package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.Region;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.EIDDataReader;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.CardData;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ReadEIDData extends AbstractMemsBean
{

	private static final long serialVersionUID = 1L;
	
	HtmlSelectOneMenu cmbNationality = new HtmlSelectOneMenu();
	HtmlCommandButton btnDone = new HtmlCommandButton();
	HtmlSelectBooleanCheckbox chkPassportIssuePlace = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkPassportNumber = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkCellNumber = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkNationality = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkGender = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkDateOfBirth = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkFullNameEn = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkLastName = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkFirstName = new HtmlSelectBooleanCheckbox();
	HtmlSelectBooleanCheckbox chkSocialSecNumber = new HtmlSelectBooleanCheckbox();
	
	
	private String ef_idn_cn; 
	private String ef_mod_data;
	private String ef_non_mod_data;
	private String ef_sign_image;
	private String ef_home_address;
	private String ef_work_address;
	HttpServletRequest request =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	private PersonView person =new PersonView();
	protected static SystemParameters parameters = SystemParameters.getInstance();
	static String endPoint= parameters.getParameter("CardDataWS");
	public HtmlSelectBooleanCheckbox getChkPassportIssuePlace() {
		return chkPassportIssuePlace;
	}

	public void setChkPassportIssuePlace(
			HtmlSelectBooleanCheckbox chkPassportIssuePlace) {
		this.chkPassportIssuePlace = chkPassportIssuePlace;
	}

	public HtmlSelectBooleanCheckbox getChkPassportNumber() {
		return chkPassportNumber;
	}

	public void setChkPassportNumber(HtmlSelectBooleanCheckbox chkPassportNumber) {
		this.chkPassportNumber = chkPassportNumber;
	}

	public HtmlSelectBooleanCheckbox getChkCellNumber() {
		return chkCellNumber;
	}

	public void setChkCellNumber(HtmlSelectBooleanCheckbox chkCellNumber) {
		this.chkCellNumber = chkCellNumber;
	}

	public HtmlSelectBooleanCheckbox getChkNationality() {
		return chkNationality;
	}

	public void setChkNationality(HtmlSelectBooleanCheckbox chkNationality) {
		this.chkNationality = chkNationality;
	}

	public HtmlSelectBooleanCheckbox getChkGender() {
		return chkGender;
	}

	public void setChkGender(HtmlSelectBooleanCheckbox chkGender) {
		this.chkGender = chkGender;
	}

	public HtmlSelectBooleanCheckbox getChkDateOfBirth() {
		return chkDateOfBirth;
	}

	public void setChkDateOfBirth(HtmlSelectBooleanCheckbox chkDateOfBirth) {
		this.chkDateOfBirth = chkDateOfBirth;
	}

	public HtmlSelectBooleanCheckbox getChkFullNameEn() {
		return chkFullNameEn;
	}

	public void setChkFullNameEn(HtmlSelectBooleanCheckbox chkFullNameEn) {
		this.chkFullNameEn = chkFullNameEn;
	}

	public HtmlSelectBooleanCheckbox getChkLastName() {
		return chkLastName;
	}

	public void setChkLastName(HtmlSelectBooleanCheckbox chkLastName) {
		this.chkLastName = chkLastName;
	}

	public HtmlSelectBooleanCheckbox getChkFirstName() {
		return chkFirstName;
	}

	public void setChkFirstName(HtmlSelectBooleanCheckbox chkFirstName) {
		this.chkFirstName = chkFirstName;
	}

	public HtmlSelectBooleanCheckbox getChkSocialSecNumber() {
		return chkSocialSecNumber;
	}

	public void setChkSocialSecNumber(HtmlSelectBooleanCheckbox chkSocialSecNumber) {
		this.chkSocialSecNumber = chkSocialSecNumber;
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

	public PersonView getPerson() {
		if( viewMap.get("person")!= null )
		{
			this.person = (PersonView)viewMap.get("person");
		}
		return person;
	}
	
    @SuppressWarnings("unchecked")
	public void setPerson(PersonView person) 
    {
		this.person = person;
		if(this.person != null)
		{
			viewMap.put("person", person);
		}
	}

	public String getEf_mod_data() {
		return ef_mod_data;
	}

	public void setEf_mod_data(String ef_mod_data) {
		this.ef_mod_data = ef_mod_data;
	}

	public String getEf_non_mod_data() {
		return ef_non_mod_data;
	}

	public void setEf_non_mod_data(String ef_non_mod_data) {
		this.ef_non_mod_data = ef_non_mod_data;
	}

	public String getEf_sign_image() {
		return ef_sign_image;
	}

	public void setEf_sign_image(String ef_sign_image) {
		this.ef_sign_image = ef_sign_image;
	}

	public String getEf_home_address() {
		return ef_home_address;
	}

	public void setEf_home_address(String ef_home_address) {
		this.ef_home_address = ef_home_address;
	}

	public String getEf_work_address() {
		return ef_work_address;
	}

	public void setEf_work_address(String ef_work_address) {
		this.ef_work_address = ef_work_address;
	}

	public ReadEIDData(){
	}
	
	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		
			try 
			{
				if( !isPostBack()  )
				{
					initData();
					setScreen();
		
				}
			}
			catch (Exception e) 
			{
			 logger.LogException("init|Error Occured:", e);
			}
	}
	@SuppressWarnings("unchecked")
	private void setScreen()
	{
		btnDone.setRendered(false);
		checkUncheck(true);
		
		
	}

	/**
	 * 
	 */
	private void checkUncheck( boolean check ) 
	
	{
		chkSocialSecNumber.setRendered(false);
		chkCellNumber.setSelected(check);
		chkDateOfBirth.setSelected(check);
		chkFirstName.setSelected(check);
		chkFullNameEn.setSelected(check);
		chkGender.setSelected(check);
		chkLastName.setSelected(check);
		chkNationality.setSelected(check);
		chkPassportIssuePlace.setSelected(check);
		chkPassportNumber.setSelected(check);
		chkSocialSecNumber.setSelected(check);
	}
	@SuppressWarnings("unchecked")
	public void initData()throws Exception 
	{
		if( request.getParameter("context") !=  null )
		{
			viewMap.put("context", request.getParameter("context").toString());
		}
		//default context is addperson
		else
		{
			viewMap.put("context", "addperson");
		}
		
		
	}


	@SuppressWarnings("unchecked")
	public void onScanEmiratesCard()
	{
		String dataFromReader="" ;
		try 
		{
			logger.logDebug("onScanEmiratesCard");
			person = new PersonView();
			
			String postInfo = getPostInfo();
			dataFromReader = EIDDataReader.callUrlJSON(endPoint, postInfo);
			
			logger.logInfo(dataFromReader);
	        Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();    
	        CardData input = jsonObj.fromJson(dataFromReader, CardData.class);
	        
	        person = input.getPersonDetailsFromCardData();
	        if(	person.getEidNationalityEn()	!= null	)
	        {
	        	setNationalityFromEIDCountryCode( person.getEidNationalityEn()  );
	        }
	        if(person.getContactInfoViewSet() != null && person.getContactInfoViewSet().size() > 0 )
	        {
	        	List<ContactInfoView> contactInfoViewList=new ArrayList<ContactInfoView>(0);
	        	contactInfoViewList.addAll( person.getContactInfoViewSet() );
	        }
	        btnDone.setRendered(true);
	        this.setPerson(person);
	        
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
		
	}
	
	public void setNationalityFromEIDCountryCode( String eidCountryCode ) throws Exception
	{
		Region  region = UtilityService.getRegionByEidCountryCode(eidCountryCode );
		if (region == null )
		{
			cmbNationality.setDisabled(false);
			cmbNationality.setValue("-1");
			return ;
		}
		cmbNationality.setValue( region.getRegionId().toString() );
//		person.setNationalityId(region.getRegionId());
	}

	
	@SuppressWarnings("unchecked")
	public void onPassportPlaceChanged(ValueChangeEvent  event)
	{
		try 
		{
			person = this.getPerson();
			if(event.getNewValue() == null)return;
			String regionId = event.getNewValue().toString();
			if( regionId.equals( "-1" ) )return;
			UtilityService.persistEidCountryCodeForRegionId(Long.valueOf( regionId ), person.getEidPassportIssuePlace() , getLoggedInUserId() );
			if( person.getEidPassportIssuePlace().equals(person.getEidNationalityEn()) )
			{
				person.setNationalId(regionId);
				person.setPassportIssuePlaceId( Long.valueOf( regionId ) ); 
			}
	        
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onNationalityChanged|Error Occured:", e);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onNationalityChanged(ValueChangeEvent  event)
	{
		try 
		{
			person = this.getPerson();
			if(event.getNewValue() == null)return;
			String regionId = event.getNewValue().toString();
			if( regionId.equals( "-1" ) )return;
			UtilityService.persistEidCountryCodeForRegionId(Long.valueOf( regionId ), person.getEidNationalityEn() , getLoggedInUserId() );
			if( person.getEidPassportIssuePlace().equals(person.getEidNationalityEn()) )
			{
				person.setPassportIssuePlaceId( Long.valueOf( regionId ) );
			}
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onNationalityChanged|Error Occured:", e);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSelectAll()
	{
		checkUncheck(true);
	}
	
	@SuppressWarnings("unchecked")
	public void onDeSelectAll()
	{
		checkUncheck(false);
	}
	
	@SuppressWarnings("unchecked")
	public void onSendBack()
	{
		try 
		{

		 if(  !((Boolean)chkFirstName.getValue() ) )
		 {
			 person.setFirstName(null);
		 }
		 if(  !((Boolean)chkLastName.getValue() ) )
		 {
			 person.setLastName(null);
		 }
		 if(  !((Boolean)chkFullNameEn.getValue() ) )
		 {
			 person.setFullNameEn( null );
		 }
		 if(  !((Boolean)chkCellNumber.getValue() ) )
		 {
			 person.setCellNumber( null );
		 }
		 if(  !((Boolean)chkNationality.getValue() ) )
		 {
			 person.setNationalId(  null );
		 }
		 if(  !((Boolean)chkDateOfBirth.getValue() ) )
		 {
			 person.setDateOfBirth(  null );
		 }
		 if(  !((Boolean)chkGender.getValue() ) )
		 {
			 person.setGender(  null );
		 }
		 if(  !((Boolean)chkPassportNumber.getValue() ) )
		 {
			 person.setPassportNumber(   null );
		 }
		 if(  !((Boolean)chkPassportIssuePlace.getValue() ) )
		 {
			 person.setPassportIssuePlaceId(    null );
			 person.setPassportIssuePlaceIdString(     null );
		 }
	     sessionMap.put(WebConstants.Person.EID_CARD_PERSON_DETAILS, this.getPerson() );
	     executeJavascript("javaScript:sendToParent('"+viewMap.get("context").toString()+"')"); 
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onSendBack|Error Occured:", e);
		}
		
	}
	
	private String getPostInfo() throws Exception
	{
		
		String postInfo = "{ \"ef_idn_cn\":\""+ getEf_idn_cn()+ "\", "+
				  "\"ef_mod_data\":\""+ getEf_mod_data()+"\","+
				  "\"ef_non_mod_data\":\""+ getEf_non_mod_data()+"\","+
				  "\"ef_home_address\":\""+ getEf_home_address()+"\","+
				  "\"ef_work_address\":\""+ getEf_work_address()+"\"}";
		return postInfo;
	}
	
	private void executeJavascript(String javascript) throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}

	public String getEf_idn_cn() {
		return ef_idn_cn;
	}

	public void setEf_idn_cn(String ef_idn_cn) {
		this.ef_idn_cn = ef_idn_cn;
	}

	public HtmlSelectOneMenu getCmbNationality() {
		return cmbNationality;
	}

	public void setCmbNationality(HtmlSelectOneMenu cmbNationality) {
		this.cmbNationality = cmbNationality;
	}

	
}
