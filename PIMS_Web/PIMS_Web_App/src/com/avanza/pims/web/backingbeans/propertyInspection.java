package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.Floor;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.UnitView;

public class propertyInspection extends AbstractController  {
	private HtmlInputText propertyReferenceNumber = new HtmlInputText();
	private HtmlInputText financialAccofuntNumber = new HtmlInputText();
	private HtmlInputText propertyRentValue = new HtmlInputText();
	


	private HtmlInputText departmentCode = new HtmlInputText();

	private HtmlInputText numberOfUnits = new HtmlInputText();
	private HtmlInputText projectNumber = new HtmlInputText();
	private HtmlInputText numberOfFloors =new HtmlInputText();
	private HtmlInputText landArea =new HtmlInputText();
	private HtmlInputText builtInArea =new HtmlInputText();
	private HtmlInputText coOrdinates = new HtmlInputText();
	private HtmlInputText locationChart =new HtmlInputText();

	private HtmlInputText propertyType = new HtmlInputText();
	private HtmlInputText recommendedPropertyRent = new HtmlInputText();
	private HtmlInputText recommendedUnitRent = new HtmlInputText();

	private HtmlInputText propertyCategory = new HtmlInputText();
	private HtmlInputText  propertyUsageList = new HtmlInputText();
	private HtmlInputText propertyStatus= new HtmlInputText();
	private HtmlInputText investmentPurpose = new HtmlInputText();


	private List<SelectItem> propertyUsageItems = new ArrayList();

	private HtmlInputTextarea inspectionComments =new HtmlInputTextarea();


	private List<FloorView> dataList = new ArrayList();
	private List<UnitView> unitsDataList = new ArrayList();

	private UploadedFile _upFile;
	private HtmlDataTable dataTable;
	private HtmlDataTable unitsDataTable;


	PropertyView objPropertyView;
	PropertyServiceAgent objPropertyServiceAgent = new PropertyServiceAgent();

	private int index;
	private static final int FLOORS_INDEX = 0;
	private static final int UNITS_INDEX = 1; 
	private String[] tabs = { "FloorsTabText", "UnitsTabText", };

	private String[] tabTooltips = { "FloorsTooltip", "UnitsTooltip" };	


	@Override

	public void destroy() {
		super.destroy();
	}

	@Override

	public void init() {
		// TODO Auto-generated method stub
		super.init();
		try{
			if(!isPostBack()){
				if(financialAccofuntNumber.getValue()==null || financialAccofuntNumber.getValue().toString().trim().length()<=0){
		//			objPropertyView = objPropertyServiceAgent.getPropertyByIdWithFloorsAndUnits(3L);

					builtInArea.setValue(String.valueOf(objPropertyView.getBuiltInArea()));
					coOrdinates.setValue(objPropertyView.getCoordinates());
					departmentCode.setValue(objPropertyView.getLandDeptCode());
					financialAccofuntNumber.setValue(objPropertyView.getFinancialAccountNo());
					landArea.setValue(objPropertyView.getLandArea());
					numberOfFloors.setValue(String.valueOf(objPropertyView.getNoFloors()));
					numberOfUnits.setValue(String.valueOf(objPropertyView.getNoOfUnits()));
					projectNumber.setValue(String.valueOf(objPropertyView.getProjectNo()));
					propertyCategory.setValue(objPropertyView.getCategoryTypeEn());
					propertyStatus.setValue(objPropertyView.getStatusTypeEn());
					propertyReferenceNumber.setValue(objPropertyView.getPropertyNumber());
					propertyType.setValue(objPropertyView.getPropertyTypeEn());
					investmentPurpose.setValue(objPropertyView.getInvestmentPurposeEn());
					propertyUsageList.setValue(objPropertyView.getUsageTypeEn());
					Iterator it = objPropertyView.getFloorView().iterator();
					Iterator unitIterator;
					FloorView objFloorView ;
					UnitView objUnitView ;
					while(it.hasNext()){
						objFloorView = (FloorView)it.next();
						
						dataList.add(objFloorView);
						//unitIterator = objFloorView.getUnits().iterator();
						
//						while(unitIterator.hasNext()){
//							objUnitView = (UnitView)unitIterator.next();
//							unitsDataList.add(objUnitView);
//						}
					}
					
					getFacesContext().getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.UNITS_LIST_PROPERTY_INSPECTION, unitsDataList);
					getFacesContext().getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.FLOORS_LIST_PROPERTY_INSPECTION, dataList);
				}
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}
	


//	CSS styles

	public String getFloorsStyle() { return getCSS(FLOORS_INDEX);  }
	public String getUnitsStyle() { return getCSS(UNITS_INDEX);  }


	private String getCSS(int forIndex) {
		return forIndex == index ? "tabbedPaneTextSelected" : "tabbedPaneText"; 
	}


//	methods for determining the current tab

	public boolean isFloorCurrent() { return index == FLOORS_INDEX;  }
	public boolean isUnitsCurrent() { return index == UNITS_INDEX;  }


	@Override
	public void preprocess() {
		try{
			index = FLOORS_INDEX;


//			PIMSPropertyWSSoapHttpPortClient objPIMSPropertyWSSoapHttpPortClient = new PIMSPropertyWSSoapHttpPortClient();
//			objPropertyView = objPIMSPropertyWSSoapHttpPortClient.getPropertyByID(new Long(3));
			if(financialAccofuntNumber.getValue()==null || financialAccofuntNumber.getValue().toString().trim().length()<=0){
			//	objPropertyView = objPropertyServiceAgent.getPropertyByIdWithFloorsAndUnits(3L);

				builtInArea.setValue(String.valueOf(objPropertyView.getBuiltInArea()));
				coOrdinates.setValue(objPropertyView.getCoordinates());
				departmentCode.setValue(objPropertyView.getLandDeptCode());
				financialAccofuntNumber.setValue(objPropertyView.getFinancialAccountNo());
				landArea.setValue(objPropertyView.getLandArea());
				numberOfFloors.setValue(String.valueOf(objPropertyView.getNoFloors()));
				numberOfUnits.setValue(String.valueOf(objPropertyView.getNoOfUnits()));
				projectNumber.setValue(String.valueOf(objPropertyView.getProjectNo()));
				propertyCategory.setValue(objPropertyView.getCategoryTypeEn());
				propertyStatus.setValue(objPropertyView.getStatusTypeEn());
				propertyReferenceNumber.setValue(objPropertyView.getPropertyNumber());
				propertyType.setValue(objPropertyView.getPropertyTypeEn());
				investmentPurpose.setValue(objPropertyView.getInvestmentPurposeEn());
				propertyUsageList.setValue(objPropertyView.getUsageTypeEn());
				
				
				Iterator it = objPropertyView.getFloorView().iterator();
				FloorView objFloorView ;
				UnitView objUnitView ;
				while(it.hasNext()){
					objFloorView = (FloorView)it.next();
					dataList.add(objFloorView);
				}
				while(it.hasNext()){
					objUnitView = (UnitView)it.next();
					unitsDataList.add(objUnitView);
					
				}
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	public String upload() throws IOException
	{

		getFacesContext().getExternalContext().getApplicationMap().put("fileupload_bytes", _upFile.getBytes());
		getFacesContext().getExternalContext().getApplicationMap().put("fileupload_type", _upFile.getContentType());
		getFacesContext().getExternalContext().getApplicationMap().put("fileupload_name", _upFile.getName());
		return "ok";
	}


	public List<FloorView> getFloorList() {
				return (List<FloorView>)getFacesContext().getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.FLOORS_LIST_PROPERTY_INSPECTION);
	}

	public List<UnitView> getUnitsList() {
				return (List<UnitView>)getFacesContext().getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.UNITS_LIST_PROPERTY_INSPECTION);
	}

	private void loadUnitsDataList(){

	}
	private void loadDataList() {

		/*Search query = new Search();

		query.clear();

		query.addFrom(PropertyDbImpl.class);

		List<Property> groupList = SecurityManager.findGroup(query);
		for(int i = 0;i<groupList.size();i++){

			dataList.add(groupList.get(i));
		}
		 */

	}


	public UploadedFile get_upFile() {
		return _upFile;
	}

	public void set_upFile(UploadedFile file) {
		_upFile = file;
	}



	public HtmlInputText getBuiltInArea() {
		return builtInArea;
	}

	public void setBuiltInArea(HtmlInputText builtInArea) {
		this.builtInArea = builtInArea;
	}


	public HtmlInputText getCoOrdinates() {
		return coOrdinates;
	}

	public void setCoOrdinates(HtmlInputText coOrdinates) {
		this.coOrdinates = coOrdinates;
	}

	public HtmlInputText getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(HtmlInputText departmentCode) {
		this.departmentCode = departmentCode;
	}



	public HtmlInputText getFinancialAccofuntNumber() {
		return financialAccofuntNumber;
	}

	public void setFinancialAccofuntNumber(HtmlInputText financialAccofuntNumber) {
		this.financialAccofuntNumber = financialAccofuntNumber;
	}

	public HtmlInputTextarea getInspectionComments() {
		return inspectionComments;
	}

	public void setInspectionComments(HtmlInputTextarea inspectionComments) {
		this.inspectionComments = inspectionComments;
	}





	public HtmlInputText getLandArea() {
		return landArea;
	}

	public void setLandArea(HtmlInputText landArea) {
		this.landArea = landArea;
	}



	public HtmlInputText getLocationChart() {
		return locationChart;
	}

	public void setLocationChart(HtmlInputText locationChart) {
		this.locationChart = locationChart;
	}



	public HtmlInputText getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(HtmlInputText numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}


	public HtmlInputText getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(HtmlInputText numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}



	public HtmlInputText getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(HtmlInputText projectNumber) {
		this.projectNumber = projectNumber;
	}


	public HtmlInputText getPropertyReferenceNumber() {
		return propertyReferenceNumber;
	}

	public void setPropertyReferenceNumber(HtmlInputText propertyReferenceNumber) {
		this.propertyReferenceNumber = propertyReferenceNumber;
	}



	public HtmlInputText getInvestmentPurpose() {
		return investmentPurpose;
	}

	public void setInvestmentPurpose(HtmlInputText investmentPurpose) {
		this.investmentPurpose = investmentPurpose;
	}

	public HtmlInputText getPropertyCategory() {
		return propertyCategory;
	}

	public void setPropertyCategory(HtmlInputText propertyCategory) {
		this.propertyCategory = propertyCategory;
	}

	public HtmlInputText getPropertyStatus() {
		return propertyStatus;
	}

	public void setPropertyStatus(HtmlInputText propertyStatus) {
		this.propertyStatus = propertyStatus;
	}

	public HtmlInputText getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(HtmlInputText propertyType) {
		this.propertyType = propertyType;
	}

	public List<UnitView> getUnitsDataList() {
		return unitsDataList;
	}

	public void setUnitsDataList(List<UnitView> unitsDataList) {
		this.unitsDataList = unitsDataList;
	}

	public List<SelectItem> getPropertyUsageItems() {
		return propertyUsageItems;
	}

	public void setPropertyUsageItems(List<SelectItem> propertyUsageItems) {
		this.propertyUsageItems = propertyUsageItems;
	}

	public HtmlInputText getPropertyUsageList() {
		return propertyUsageList;
	}

	public void setPropertyUsageList(HtmlInputText propertyUsageList) {
		this.propertyUsageList = propertyUsageList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlDataTable getUnitsDataTable() {
		return unitsDataTable;
	}

	public void setUnitsDataTable(HtmlDataTable unitsDataTable) {
		this.unitsDataTable = unitsDataTable;
	}
	public String editFloor(){
		Floor dataItem = (Floor) dataTable.getRowData();
		setRequestParam(WebConstants.EDIT_PARAM,dataItem.getFloorId());       // Store the ID of the data item in hidden input element.
		setRequestParam("groupObj",dataItem);   
		return "edit";
	}
	public String save(){
		try{
		for(int i = 0;i<getUnitsList().size();i++)
			getUnitsList().get(i).setRentValue(Double.parseDouble(propertyRentValue.getSubmittedValue().toString()));
		objPropertyServiceAgent.persistPropertyRentValue(getUnitsList());
		}
		catch(Exception e){
			e.printStackTrace();
		}
				
			
		return "Saved";
	}
	public String inspected(){
		try{
			PropertyView objPropertyView=  new PropertyView();
			objPropertyView.setPropertyId(new Long(3));
			objPropertyView.setUsageTypeId(new Long(1));

//			if(objPropertyServiceAgent.setPropertyAsInspected(3L))
//				System.out.println("Successfully Inspected");
			/*	PIMSPropertyWSSoapHttpPortClient objPIMSPropertyWSSoapHttpPortClient = new PIMSPropertyWSSoapHttpPortClient();
			objPIMSPropertyWSSoapHttpPortClient.updateProperty(objPropertyView);*/
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "inspected";

	}

	public void unitsAct(){
		index = UNITS_INDEX;


	}
	public void floorsAct() { 
		index = FLOORS_INDEX;

	}

	public void setPropertyRentValue(HtmlInputText propertyRentValue) {
		this.propertyRentValue = propertyRentValue;
	}

	public HtmlInputText getPropertyRentValue() {
		return propertyRentValue;
	}


}
