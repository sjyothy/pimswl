package com.avanza.pims.web.backingbeans.istisnaacontract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.bpel.proxy.PIMSIstisnaContractBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionContractAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.construction.plugins.ProjectDetailsTabBOTBacking;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ConstructionContractView;
import com.avanza.pims.ws.vo.ContractAdditionalInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class IstisnaaContract extends AbstractController {
	
	private transient Logger logger = Logger.getLogger(IstisnaaContract.class);
	public interface Page_Mode {
		
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String EDIT="PAGE_MODE_EDIT";
		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
		public static final String COMPLETE="PAGE_MODE_COMPLETE";
		public static final String POPUP="PAGE_MODE_POPUP";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String ACTIVE="PAGE_MODE_ACTIVE";
		
	}
    public interface TAB_ID {
		
		public static final String ContractDetails="tabContractDetails";
		public static final String Project="tabProject";
		public static final String PaymentSchedule="tabPaymentSchedule";
		public static final String Attachment="attachmentTab";
		
		
	}
	protected  String pageMode="pageMode";
	protected  String tenderTypeConstruction; 
	HttpServletRequest request ;
	
	protected SystemParameters parameters = SystemParameters.getInstance();
	Map viewRootMap ;
	Map sessionMap ;
	FacesContext context ;
	protected List<String> errorMessages = new ArrayList<String>();
	protected List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil ;
	protected DomainDataView ddContractType;
	protected String CONTRACT_TYPE="CONTRACT_TYPE";
	protected String DD_CONTRACT_TYPE="DD_CONTRACT_TYPE";
	protected String DD_CONTRACT_STATUS_LIST ="DD_CONTRACT_STATUS_LIST";
	protected String PROCEDURE_TYPE;
	protected String EXTERNAL_ID;
	
	protected String contractCreatedOn;
	protected String contractCreatedBy;
	protected PersonView investor= new PersonView();
	protected String INVESTOR_INFO  ="INVESTOR_INFO";
	protected String CONTRACT_VIEW ="CONTRACT_VIEW";
	protected String CONTRACT_ADDITIONAL_INFO_VIEW ="CONTRACT_ADDITIONAL_INFO_VIEW";
	protected String REQUEST_VIEW ="REQUEST_VIEW";
	protected String PROJECT_VIEW ="PROJECT_VIEW";
	protected String PROPERTY_VIEW ="PROPERTY_VIEW";
	private ProjectView projectView;
	PropertyView propertyView ;
	protected ConstructionContractView constructionContractView = new ConstructionContractView();
	protected RequestView requestView = new RequestView();
	public String contractId;
	public String pageTitle;
	protected org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabProjectDetails=new org.richfaces.component.html.HtmlTab();
	protected HtmlCommandButton btnApprove = new HtmlCommandButton();
    protected HtmlCommandButton btnReject = new HtmlCommandButton();
    protected HtmlCommandButton btnReview = new HtmlCommandButton();
    protected HtmlCommandButton btnComplete = new HtmlCommandButton();
    protected HtmlCommandButton btnPrint = new HtmlCommandButton();
    protected HtmlCommandButton btnReviewReq = new HtmlCommandButton();
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnSend_For_Approval= new HtmlCommandButton();
    protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    protected String txtRemarks;    
    protected String hdnOwnerId;
    protected String hdnProjectId;
    
	protected String requestId;
	protected String hdnPropertyId;
	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	
	public IstisnaaContract(){
		request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getFacesContext().getExternalContext().getSessionMap();
		context = FacesContext.getCurrentInstance();
		commonUtil = new CommonUtil();
		PROCEDURE_TYPE ="procedureType";
		EXTERNAL_ID ="externalId";
	}
	
	@Override
    public void init() 
    {
    	
    	String methodName="init";
    	try{
	    	logger.logInfo(methodName+"|"+"Start");
	    	
		       if(!isPostBack())
		       {
		    	   
		    	   viewRootMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_TYPE_ISTISNAA_CONTRACT);
		    	   viewRootMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_ISTISNAA_CONTRACT);
		    	   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ADD);
		    	   setContractTypeAsConstruction();
		    	   logger.logDebug(methodName+"|"+"Is not postback");
		    	   loadAttachmentsAndComments(null);
		    	   		    	   
		    	   
			    	if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				       getDataFromTaskList();
//			    	
//			    	if(viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
//						getPageModeFromTaskList();
			    	
			    	else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
			    	 {
			    		 requestView=(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
					     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
					         this.contractId=requestView.getContractView().getContractId().toString();
					}
			    	else if (getRequestParam(WebConstants.CONSTRUCTION_CONTRACT_ID)!=null)
			    		this.contractId = getRequestParam(WebConstants.CONSTRUCTION_CONTRACT_ID).toString();
			    	if((this.contractId!=null && this.contractId.trim().length()>0) )
			    	{
			    		getContractById(this.contractId );
			    		
			    	}
		       }	
		       else
		       {
			       if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
			       {
			    	   getPaymentScheduleFromSession();
			       }
			       if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
			    	  &&  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null
				      && (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
				    )
			 	   {
	    		       sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
	    		       CollectPayments();
			 	   }
		       }
	    	logger.logInfo(methodName+"|"+"Finish");
    	}
    	catch(Exception ex)
    	{
    		errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    }

	private void CollectPayments()
	{
		String methodName = "CollectPayments";
		logger.logInfo(methodName+"|Start");
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		try
		{
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     prv = psa.collectPayments(prv);
		     getPaymentScheduleByContractId();
		     saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PAYMENT_COLLECTED);
		     //ContractView contractView = (ContractView)viewRootMap.get(CONTRACT_VIEW);
		     //CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext());
		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
		     logger.logInfo(methodName + "|" + "Finish...");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + "|" + "Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	

	public void prerender()
	{
		String methodName ="prerender";
		super.prerender();
		try
		{
			setPageTitleBasedOnProcedureType();
			
			//viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE,true);
			viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND,"1");
			 enableDisableControls();
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException(methodName+"|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	public void enableDisableControls()
	{
		String methodName = "enableDisableControls";
		logger.logInfo(methodName+"|Start");
		constructionContractView = getConstructionContractFromViewRoot();
		btnSend_For_Approval.setRendered(false);
		IstisnaaContractDetailsTab scdt= (IstisnaaContractDetailsTab )getBean("pages$IstisnaaContractDetailsTab");
		ProjectDetailsTabBOTBacking pdtb =(ProjectDetailsTabBOTBacking )getBean("pages$projectDetailsTabBOT");
		pdtb.getImgSearchProject().setRendered(false);
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);
        //For Istisnaa Contract No Generate Payment will be used
		scpt.getBtnGenPayments().setRendered(false);
		
         
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
		{
			pdtb.getImgSearchProject().setRendered(true);
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			PageModeAdd(scdt);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{
			PageModeEdit(scdt);
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			btnSend_For_Approval.setRendered(true);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
		{
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
			PageModeApprovalRequired(scdt);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.LEGAL_REVIEW_REQUIRED))
		{
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
			PageModeLegalReviewRequired(scdt);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
		{
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);

			PageModeComplete(scdt);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ACTIVE))
		{
			viewRootMap.put("canAddAttachment", false);
			viewRootMap.put("canAddNote", false);

			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		    PageModeActive(scdt);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
		{
			viewRootMap.put("canAddAttachment", false);
			viewRootMap.put("canAddNote", false);
			scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
			PageModeView(scdt);
		}
		logger.logInfo(methodName+"|Finish");
		
	}
	
	@SuppressWarnings("unchecked")
	private void setContractTypeAsConstruction() {
		ddContractType = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE) , WebConstants.ISTISNAA_CONTRACT);
         if(ddContractType !=null)
         {
		   viewRootMap.put(DD_CONTRACT_TYPE, ddContractType );
		   if(getIsEnglishLocale())
		     viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescEn());
		   else if(getIsArabicLocale())
			   viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescAr());
         }
	}

    private boolean hasContractDetailsErrors()throws Exception
    {
    	boolean hasErrors = false;
    	Date contractStartDate =null;
        Date contractEndDate =null;
        Date contractDate =null;
        
    	if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_START_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE)!=null)
            contractStartDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
         if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_END_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE)!=null)
            contractEndDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);
         if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_DATE)!=null)
             contractDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_DATE);
      
         if(hdnOwnerId== null || hdnOwnerId.trim().length()<=0)
         {
        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.IstisnaaContract.MSG_REQ_OWNER));
        	hasErrors = true;
    	   }
       if(contractDate==null)
       {
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACT_DATE));
			hasErrors=true;
    	   
       }
       if(!viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM) || viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM).toString().length()<=0)
       {
    	   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACT_PERIOD));
		   hasErrors=true;
       }
       else 
       {
    	   try {
    		   new Integer(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM).toString());
		   } catch (Exception e) {
			  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_CONTRACT_PERIOD));
			   hasErrors=true;
	       }
    	   
    	   
       }
       if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM) || viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM).toString().length()<=0)
       {
    	   try {
    		   new Integer(viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM).toString());
		   } catch (Exception e) {
			  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_GRACE_PERIOD));
			   hasErrors=true;
	       }
       }

       if(contractStartDate!=null && !contractStartDate.equals("") && contractEndDate!=null && !contractEndDate.equals("") )
		{
		    DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
		    if(contractDate!= null && dateFormat.parse(dateFormat.format(contractDate)).compareTo(dateFormat.parse(dateFormat.format(contractStartDate)))>0)
		    {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_CONTRACT_DATE));
				hasErrors=true;
			}
		    if (dateFormat.parse(dateFormat.format(contractStartDate)).compareTo(dateFormat.parse(dateFormat.format(contractEndDate)))>0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_EXP));
				hasErrors=true;
			}
		}
	   	else
	   	{
	   		if(contractStartDate ==null)
	   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_START_DATE));
	   		if(contractEndDate ==null)
	   	      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_EXPIRY_DATE));
	   		hasErrors=true;
	   	}
       String istisnaaMethod = viewRootMap.get(WebConstants.ContractDetailsTab.ISTISNAA_METHOD).toString();
//       //Fixed
       if(istisnaaMethod.compareTo("0")==0)
       {
    	   if(!viewRootMap.containsKey(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE) ||
    			   viewRootMap.get(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE).toString().trim().length()<=0 )
    	   {
    	    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.IstisnaaContract.MSG_REQ_SECOND_PARTY_SHARE));
	   		hasErrors=true;
    	   }
       }
   	   //Percentage
   	   else
   	   {
   		if(!viewRootMap.containsKey(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE) ||
 			viewRootMap.get(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE).toString().trim().length()<=0 || 
 		   !viewRootMap.containsKey(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE) || 
 			viewRootMap.get(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE).toString().trim().length()<=0 	   
   		  )
   		{
 	       errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.IstisnaaContract.MSG_REQ_AMAF_OR_SEC_PARTY_SHARE));
	   		hasErrors=true;
   		}
   		   
   	   }   		   
   		   
   		   
//	     if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null
//	     	&& viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().trim().length()>0)
//	     {
//	     	try
//	     	{
//	     		Double chkAmountValidity =new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());
//	     	}
//	     	catch(NumberFormatException nfe)
//	     	{
//	     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_CONTRACT_VALUE));
//	     	    hasErrors=true;
//	     	}
//	     } 
//	     else 
//	     {
//	 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACT_AMOUNT));
//	     	hasErrors=true;
//	     }
       return hasErrors;

    }
	private boolean hasError()
	{
	 	boolean hasErrors =false;
	 	errorMessages = new ArrayList<String>(0);
	 
	 	try
	 	{
	 		
	 		/////Validate Contract Details Tab Start///////////////
	 	    if(hasContractDetailsErrors())
	 	    {
	 	    	
	 	    	tabPanel.setSelectedTab(TAB_ID.ContractDetails);
	 	    	return true;
	 	    }
	 		
	 		/////Validate Contract Details Tab Finish///////////////
	 	    
	 	    
	 	    
	 		/////Validate Project Tab Start///////////////
	        if((hdnProjectId== null || hdnProjectId.trim().length()<=0) && (hdnPropertyId==null || hdnPropertyId.trim().length()<=0))
	        {
	        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.IstisnaaContract.MSG_REQ_PROJECT_OR_PROPERTY));
	        	hasErrors = true;
	        	tabPanel.setSelectedTab(TAB_ID.Project);
	        	return true;
	    	}
	        /////Validate Project Tab Finish///////////////
	        
//	        //Validating Payment Schedules Start
//	        if(!getIsContractValueandRentAmountEqual())
//	        {
//	        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
//	        	tabPanel.setSelectedTab(TAB_ID.PaymentSchedule);
//	        	return true;
//	        }
//	        //Validating Payment Schedules Finish

	        
	        
	        
    	    /////Validate Attachment Tab Start///////////////
	        if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		hasErrors = true;
	    		tabPanel.setSelectedTab(TAB_ID.Attachment);
	        	return true;
	    	}
    	    /////Validate Attachment Tab Finish///////////////
	 	}
	 	catch(Exception e )
	 	{
	 		logger.LogException("hasError|Error Occured", e);
	 	}
   	


	 	
	 	return hasErrors;
		
	}
	private boolean getIsContractValueandRentAmountEqual()
    {
    	String methodName="getIsContractValueandRentAmountEqual";
    	logger.logInfo(methodName+"|"+"Start");
    	boolean isEqual=false;
    	
    	
    	  Double contractValue=new Double( viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());	
    		if(contractValue.compareTo(getSumofAllRentAmounts())==0)
    			isEqual=true;
    	
    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
    	logger.logInfo(methodName+"|"+"Finish");
    	return isEqual;
    }
	private Double getSumofAllRentAmounts()
    {
    	String methodName="getSumofAllRentAmounts";
    	logger.logInfo(methodName+"|"+"Start");
    	Double rentAmount=new Double(0);
    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
    	
    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				//If payment Schedule is for the rent 
				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&& 
						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0)
				   rentAmount+=paymentScheduleView.getAmount();
			}
		    
        }
    	logger.logInfo(methodName+"|"+"Finish");
    	return Math.ceil(rentAmount);
    	
    }
	private void getContractDetailsFromContractDetailsTab()throws Exception
	{

		
		//Set Contract Number if present
		if(contractId !=null && contractId.trim().length()>0 )
		{
			constructionContractView =getConstructionContractFromViewRoot();
			constructionContractView.setContractId(new Long(contractId));

		}
		
		//Set Total Contract Value
		//For Now putting zero in the contract amount field because there is no contract amount in Istisnaa
		constructionContractView.setRentAmount(new Double(0));//viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString()));
		

		//Set Contract Date
	    constructionContractView.setContractDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_DATE));

	    //Set Contract Hejree Date
	    if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE))
	      constructionContractView.setContractHejreeDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE));
	    
	    //Set Contract period Frequency
	    constructionContractView.setContractPeriodFrequency(new Long( viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_FREQ).toString()));
	    
	    //Set Contract period Num
	    constructionContractView.setContractPeriodNum(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM).toString()));
	    
	    //Set GRACE period Frequency
	    constructionContractView.setGracePeriodFrequency(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_FREQ).toString()));
	    
	    //Set Grace period Num
	    constructionContractView.setGracePeriodNum(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM).toString()));
	    
//	    //Set Rent Type
//	    constructionContractView.setRentType(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_RENT_TYPE).toString()));
//	    
	    //Set Contract Start Date
	    constructionContractView.setStartDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE));

	    //Set Contract End Date
	    constructionContractView.setEndDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE));
	    
	    //Set Contract Type
	    ddContractType = (DomainDataView)viewRootMap.get(DD_CONTRACT_TYPE);
	    constructionContractView.setContractTypeId(ddContractType.getDomainDataId());
	    
	    //Set Contract Additional Info
	    constructionContractView.setContractAdditionalInfos(setContractAdditionalInfoView());

	    if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
	    {
	    	
	    	DomainDataView ddContractStatus = commonUtil.getIdFromType(getContractStatusList(),
	    			                                                        WebConstants.CONTRACT_STATUS_DRAFT);
	      constructionContractView.setStatus(ddContractStatus.getDomainDataId());
	      
	    }
	    //Set CreatedOn CreatedBy UpdatedOn UpdateBy IsDeleted RecordStatus
	    constructionContractView.setIsDeleted(new Long(0));
	    constructionContractView.setRecordStatus(new Long(1));
	    constructionContractView.setUpdatedBy(getLoggedInUser());
        constructionContractView.setUpdatedOn(new Date());
        if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
        	constructionContractView.setCreatedOn(new Date());
        	constructionContractView.setCreatedBy(getLoggedInUser());
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
        }
	    
	}
	private Set<ContractAdditionalInfoView> setContractAdditionalInfoView()
	{
		Set<ContractAdditionalInfoView> setContractAdditionalInfo = new HashSet<ContractAdditionalInfoView>(0);
		ContractAdditionalInfoView contractAdditionalInfo = null;
		if(!viewRootMap.containsKey(CONTRACT_ADDITIONAL_INFO_VIEW))
			contractAdditionalInfo = new ContractAdditionalInfoView();  
		else
			contractAdditionalInfo = (ContractAdditionalInfoView)viewRootMap.get(CONTRACT_ADDITIONAL_INFO_VIEW);
		
		contractAdditionalInfo.setIstisnaaMethod(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.ISTISNAA_METHOD).toString()));
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE) && 
		   viewRootMap.get(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE).toString().trim().length()>0)
		       contractAdditionalInfo.setFirstPartyShare(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE).toString()));
		else
			   contractAdditionalInfo.setFirstPartyShare(null);
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE)&& 
		   viewRootMap.get(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE).toString().trim().length()>0)
		    contractAdditionalInfo.setSecondPartyShare(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE).toString()));
		else
			contractAdditionalInfo.setSecondPartyShare(null);
		setContractAdditionalInfo.add(contractAdditionalInfo);
		return setContractAdditionalInfo;
	}
	private void setRequestView()
	{
		Set<RequestView> rvSet = new HashSet<RequestView>(0);
		if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
            requestView.setIsDeleted(new Long(0));
            requestView.setRecordStatus(new Long(1));
            DomainDataView ddRequestStatus = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);
  	        requestView.setStatusId(ddRequestStatus.getDomainDataId());  
            requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_CONSTRUCTION_CONTRACT);
            requestView.setRequestDate(new Date());
        }
		else
		   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		if(hdnOwnerId!=null && hdnOwnerId.trim().length()>0)
		{
	      PersonView applicantView =new PersonView();
	      applicantView.setPersonId(new Long(hdnOwnerId));
	      requestView.setApplicantView(applicantView);
		}
		
		requestView.setUpdatedBy(getLoggedInUser());
        requestView.setUpdatedOn(new Date());
        rvSet.add(requestView);
        constructionContractView.setRequestsView(rvSet);
	}
	private void putControlValuesinViews() throws Exception
	{
		String methodName = "putControlValuesinViews";
		logger.logInfo(methodName +"|Start");
         getContractDetailsFromContractDetailsTab();
        //Set Owner
 		if(hdnOwnerId!=null && hdnOwnerId.trim().length()>0)
 		{
 			PersonView pv =new PersonView();
 			pv.setPersonId(new Long(hdnOwnerId));
 		    constructionContractView.setInvestor(pv);
 		}
 		//Set Project
		if(hdnProjectId!=null && hdnProjectId.trim().length()>0)
		{
			ProjectView pv =new ProjectView();
			pv.setProjectId(new Long(hdnProjectId));
			constructionContractView.setProjectView(pv);
		
		}
		//Set Property
		if(hdnPropertyId!=null && hdnPropertyId.length()>0)
	    {
	    	PropertyView property = new PropertyView();
	    	property.setPropertyId(new Long(hdnPropertyId));
	    	constructionContractView.setPropertyView(property);
	    }
         //Set Request View 
         setRequestView();
         //Set Payment Schedule         
         if(viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)!=null)
         {
		  List<PaymentScheduleView> paymentScheduleDataList =(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST); 
		  Set<PaymentScheduleView> paymentScheduleViewViewSet = new HashSet<PaymentScheduleView>(0);
		  paymentScheduleViewViewSet.addAll(paymentScheduleDataList);
		  constructionContractView.setPaymentScheduleView(paymentScheduleViewViewSet);
         }
		logger.logInfo(methodName +"|Finish");
	}
	
	public void btnSendForApproval_Click()
	{
	
		String methodName = "btnSendForApproval_Click";
		logger.logInfo(methodName +"|Start");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		ConstructionContractAgent cca = new ConstructionContractAgent();
	   try {
		        String endPoint= parameters.getParameter(WebConstants.ISTISNAA_CONTRACT_BPEL_ENDPOINT);
		        PIMSIstisnaContractBPELPortClient port=new PIMSIstisnaContractBPELPortClient();
		   	    port.setEndpoint(endPoint);
		   	    logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
		   		if(this.contractId!=null && this.contractId.trim().length()>0)
		   		{
			   	 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			   	 cca.setIstisnaaContractAsApprovalRequired(new Long(this.contractId), requestView.getRequestId(), getLoggedInUser());
			   	 port.initiate(new Long(contractId),requestView.getRequestId(),getLoggedInUser(), null, null);
			   	 if(viewRootMap.containsKey(CONTRACT_VIEW) )
			   	    constructionContractView = getConstructionContractFromViewRoot();
			   	  saveCommensAttachment(MessageConstants.ContractEvents.APROVAL_REQUIRED);
				  viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
		   	      
	   	          successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
		   		}
			   
			   logger.logInfo(methodName+"|"+" Finish...");
	    	}
	    	catch(Exception ex)
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
	    	}
	}
	public void btnSave_Click()
	{
		String methodName = "btnSave_Click";
		logger.logInfo(methodName +"|Start");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		ConstructionContractAgent sca = new ConstructionContractAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasError())
			{
				 putControlValuesinViews();
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_CREATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_CREATED_SUCCESSFULLY); 
				     tabPanel.setSelectedTab(TAB_ID.ContractDetails);
				 }
				 else 
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_UPDATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY); 
				 }
				 
				 constructionContractView = sca.persistConstructionContract(constructionContractView);
				 this.contractId = constructionContractView.getContractId().toString(); 
				 viewRootMap.put(CONTRACT_VIEW,constructionContractView);
				 refresh();
				 logger.logInfo(methodName +"|Contract Added with id ::"+constructionContractView );
				 logger.logInfo(methodName+"|"+" Saving Notes...Start");
				 saveCommensAttachment(eventDesc);
				 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				 successMessages.add(successMsg);			
				 logger.logInfo(methodName +"|Finish");
			}
		}
		catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void getPaymentScheduleFromSession() 
	{
		String methodName ="getPaymentScheduleFromSession";
		logger.logInfo(methodName+"|Start");
		List<PaymentScheduleView> tempPaySch=new ArrayList<PaymentScheduleView>(0);
		int rowId=0;
		try 
		{
			if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD) || 
					   viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
					{
					   for (PaymentScheduleView paymentScheduleView : 
						               (ArrayList<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)) 
					    {
							if(paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
							{
								  paymentScheduleView.setRowId(rowId);
								  rowId = rowId + 1 ;
								  tempPaySch.add(paymentScheduleView);
						    }
					    }
					}
					else  
					{
					  constructionContractView = getConstructionContractFromViewRoot();
					  tempPaySch =(ArrayList<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
					  ConstructionContractAgent cca = new ConstructionContractAgent();
					  cca.persistPaymentsAfterSave(tempPaySch, constructionContractView.getContractId(), getLoggedInUser());
					}
					viewRootMap.put( WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,tempPaySch);	   
					sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
					logger.logInfo(methodName+"|Finish");
			
		} catch (Exception e) 
		{
			logger.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			ConstructionContractView cView =(ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
			RequestView rv =(RequestView)viewRootMap.get(REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		
		
		
	}
	
	private void setPageModeBasedOnContractStatus()
	{
		DomainDataView ddContractStatus = commonUtil.getDomainDataFromId( getContractStatusList(),
                constructionContractView.getStatus());
		if(ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_DRAFT))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.COMPLETE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_ACTIVE))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ACTIVE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_REVIEW_REQUIRED_LEGAL))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.LEGAL_REVIEW_REQUIRED);
			return;
		}

	}

	protected List<DomainDataView>  getContractStatusList() {
		List<DomainDataView> ddvContractStatusList = null;
		if(!viewRootMap.containsKey(DD_CONTRACT_STATUS_LIST))
		{
		ddvContractStatusList = commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
		viewRootMap.put(DD_CONTRACT_STATUS_LIST, ddvContractStatusList );
		}
		else
			ddvContractStatusList =(ArrayList<DomainDataView>)viewRootMap.get(DD_CONTRACT_STATUS_LIST);
		return ddvContractStatusList ;
	}
	private void getRequestForContract()throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		ContractView cv= new ContractView();
		cv.setContractId(new Long(this.contractId));
		
		rv.setContractView(cv);
		
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0); 
			viewRootMap.put(REQUEST_VIEW,requestView );
			 
		}
    }
	
	
	protected void setPageTitleBasedOnProcedureType()
	{
		
		if(viewRootMap.get(PROCEDURE_TYPE).toString().trim().equals(WebConstants.PROCEDURE_TYPE_ISTISNAA_CONTRACT))
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.IstisnaaContract.TITLE_CONSTRUCTION_CONTRACT));
	}
	public void populateInvestorInfoInTab()
	{
		try
		{
			IstisnaaContractDetailsTab cdt= (IstisnaaContractDetailsTab )getBean("pages$IstisnaaContractDetailsTab");
		    investor = getInvestorInfo();
			cdt.populateOwner(investor);
		}
		catch(PimsBusinessException e)
		{
			logger.LogException("populateInvestorInfoInTab|Error Occured::", e);
		}
		
	}
	protected PersonView getInvestorInfo()throws PimsBusinessException
    {
    	
    	
	    if(viewRootMap.containsKey(INVESTOR_INFO))
    	{
    		investor=(PersonView)viewRootMap.get(INVESTOR_INFO);
	    	if(hdnOwnerId!=null && hdnOwnerId.trim().length()>0 && 
	    			investor.getPersonId()!=null && investor.getPersonId().compareTo(new Long(hdnOwnerId))!=0)
	    	{
	    		getInvestor();
	    	
	    			
	    	}
	     
    	}
    	else if(hdnOwnerId!=null && hdnOwnerId.trim().length()>0 )
    	{
    		getInvestor();
    	    
    	 
    			
    	}
    	else 
    		return null;
	    
	    return investor;	    
    }

	private void getInvestor() throws PimsBusinessException {
		PropertyServiceAgent psa=new PropertyServiceAgent();
		PersonView pv=new PersonView();
		pv.setPersonId(new Long(this.getHdnOwnerId()));
		List<PersonView> investorsList =  psa.getPersonInformation(pv);
		//contractor =  csa.getContractorById(new Long(hdnOwnerId), null);
		if(investorsList.size()>0)
		{
			viewRootMap.put(INVESTOR_INFO, investorsList.get(0) );
		    investor =  (PersonView)investorsList.get(0) ;
		}
	}

	protected ProjectView getProjectInfo()throws PimsBusinessException
    {
	    if(viewRootMap.containsKey(PROJECT_VIEW))
    	{
    		projectView=(ProjectView)viewRootMap.get(PROJECT_VIEW);
	    	if(hdnProjectId!=null && hdnProjectId.trim().length()>0 && 
	    			projectView.getProjectId()!=null && projectView.getProjectId().compareTo(new Long(hdnProjectId))!=0)
	    	{
	    		getProject();
	    		hdnPropertyId = projectView.getPropertyView().getPropertyId().toString();
	    		propertyView = getPropertyInfo();
	    	}
    	}
    	else if(hdnProjectId!=null && hdnProjectId.trim().length()>0 )
    	{
    		    getProject();
    		    hdnPropertyId = projectView.getPropertyView().getPropertyId().toString();
    		    propertyView = getPropertyInfo();
    	}
    	else 
    		    return null;
	    
	    return  projectView;
	    	    
    }

	private void getProject()
			throws PimsBusinessException {
		ProjectServiceAgent csa=new ProjectServiceAgent();
		projectView =  csa.getProjectById(new Long(hdnProjectId));
		viewRootMap.put(PROJECT_VIEW, projectView);
	}
	protected PropertyView getPropertyInfo()throws PimsBusinessException
    {
    	PropertyServiceAgent csa=new PropertyServiceAgent();
	    if(viewRootMap.containsKey(PROPERTY_VIEW))
    	{
    		propertyView =(PropertyView)viewRootMap.get(PROPERTY_VIEW);
	    	if(hdnPropertyId!=null && hdnPropertyId.trim().length()>0 && 
	    			propertyView.getPropertyId()!=null && propertyView.getPropertyId().compareTo(new Long(hdnPropertyId))!=0)
	    	{
	    		propertyView =  csa.getPropertyByID(new Long(hdnPropertyId));
	    	    viewRootMap.put(PROPERTY_VIEW , propertyView);
	    		viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE,propertyView.getCategoryId());
	    	}
	        return propertyView;
    	}
    	else if(hdnPropertyId!=null && hdnPropertyId.trim().length()>0 )
    	{
    		propertyView =  csa.getPropertyByID(new Long(hdnPropertyId));
    	    viewRootMap.put(PROPERTY_VIEW , propertyView);
    	    viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.OWNERSHIP_TYPE,propertyView.getCategoryId());
    	    return propertyView;
    			
    	}
    	else 
    		return null;
	    	    
    }
	
	protected Boolean generateNotification(String eventName)
    {
    	   String methodName ="generateNotification";
    	   Boolean success = false;
            try
            {
                  logger.logInfo(methodName+"|Start");
                  HashMap placeHolderMap = new HashMap();
                  constructionContractView= getConstructionContractFromViewRoot();
                  List<ContactInfo> contractorEmailList    = new ArrayList<ContactInfo>(0);
                  
                  NotificationFactory nsfactory = NotificationFactory.getInstance();
                  NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
                  Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
                  getNotificationPlaceHolder(event);
                  if(viewRootMap.containsKey(INVESTOR_INFO))
                  {
                        contractorEmailList = CommonUtil.getEmailContactInfos((PersonView)viewRootMap.get(INVESTOR_INFO));
                        if(contractorEmailList.size()>0)
                              notifier.fireEvent(event, contractorEmailList);    
                  }     
                  
                  success  = true;
                  logger.logInfo(methodName+"|Finish");
            }
    	   catch(Exception ex)
    	   {
    	          logger.LogException(methodName+"|Finish", ex);
    	   }
    	   return success;
    }
    
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		
		logger.logDebug(methodName+"|Finish");
		
	}
	
	protected void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present ");
			try
			{
				ConstructionContractAgent sca =new ConstructionContractAgent ();
				constructionContractView= sca.getConstructionContractById(new Long(contractId));
				viewRootMap.put(CONTRACT_VIEW ,constructionContractView);
				this.contractId =contractId;
				refresh();
				
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|"+"Error occured:",ex);
				throw ex;
				
			}
	}

	private void refresh() throws PimsBusinessException {
		
		getRequestForContract();
		populateContractDetailsTabFromContractView();
		if(constructionContractView.getProjectView()!=null && constructionContractView.getProjectView().getProjectId()!=null)
		{
			hdnProjectId=constructionContractView.getProjectView().getProjectId().toString();
			getProjectInfo();
		}
		else
			hdnPropertyId = constructionContractView.getPropertyView().getPropertyId().toString();
		getPropertyInfo();
		getPaymentScheduleByContractId();
		setPageModeBasedOnContractStatus();
	}
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String methodName="setTaskOutCome";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			String loggedInUser=getLoggedInUser();
			updateStatusBasedOnTaskOutCome(taskOutCome) ;
			logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId()+"| TaskOutCome..."+taskOutCome+"|loggedInUser..."+loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
	private void updateStatusBasedOnTaskOutCome(TaskOutcome taskOutCome) throws PimsBusinessException
	{
		String methodName ="updateStatusBasedOnTaskOutCome";
		logger.logInfo(methodName+"|Start");
		ConstructionContractAgent cca = new ConstructionContractAgent();
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		constructionContractView = (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
		logger.logInfo(methodName + "contractId..."+constructionContractView.getContractId());
		if(taskOutCome.compareTo(TaskOutcome.APPROVE)==0)
			cca.setIstisnaaContractApproved(constructionContractView.getContractId(), requestView.getRequestId(),getLoggedInUser());
		else if(taskOutCome.compareTo(TaskOutcome.REJECT)==0)
			cca.setIstisnaaContractAsRejected(constructionContractView.getContractId(), requestView.getRequestId(),getLoggedInUser());
		else if(taskOutCome.compareTo(TaskOutcome.LEGAL_DEPT_REVIEW)==0)
			cca.setIstisnaaContractAsLegalReviewRequired(constructionContractView.getContractId(), requestView.getRequestId(),getLoggedInUser());
		else if(taskOutCome.compareTo(TaskOutcome.COMPLETE)==0)
			cca.setIstisnaaContractAsActivated(constructionContractView.getContractId(), requestView.getRequestId(),getLoggedInUser());
		//On review of contract
		else if(taskOutCome.compareTo(TaskOutcome.OK)==0)
			cca.setIstisnaaContractAsApprovalRequired(constructionContractView.getContractId(), requestView.getRequestId(),getLoggedInUser());
			
			
		
		logger.logInfo(methodName+"|Finish");
	}
    public String getErrorMessages()
	{

    	return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
    public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.CONTRACT;
    		
    		constructionContractView = (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
    		logger.logInfo(methodName + "contractId..."+constructionContractView.getContractId());
    		Long contractsId = new Long(constructionContractView.getContractId() );
    		  logger.logInfo(methodName + "notesOwner..."+notesOwner);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, contractsId );
	    	  logger.logInfo(methodName + "completed successfully!!!");
    		
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.CONTRACT;
	    	if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	  CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId){
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewRootMap.get(PROCEDURE_TYPE).toString());
    		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
        	String externalId = viewRootMap.get(EXTERNAL_ID).toString();
        	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
    		viewMap.put("noteowner", WebConstants.CONTRACT);
    		if(requestId!= null){
    	    	String entityId = requestId.toString();
    			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
    			viewMap.put("entityId", entityId);
    		}
    	}	
    public void tabAttachmentsComments_Click()
    {
    	String methodName ="saveCommensAttachment";
    	constructionContractView  = (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
		 if(constructionContractView!= null && constructionContractView.getContractId()!=null)
		 {
			 loadAttachmentsAndComments(constructionContractView.getContractId());
		 }
    	
    }
	
	protected void getDataFromTaskList()throws Exception
    {
    	    	   String methodName="getDataFromTaskList";
    	    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
    		      viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
    		      if(userTask.getTaskAttributes().get("CONTRACT_ID")!=null)
    			       this.contractId= userTask.getTaskAttributes().get("CONTRACT_ID").toString();
    		       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    }
	
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
		    return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
	protected  String getLoggedInUser() 
	{
	        return CommonUtil.getLoggedInUser();
	}
	public String getContractId() {
		return contractId;
	}
    @SuppressWarnings("unchecked")
	protected void populateContractDetailsTabFromContractView()throws PimsBusinessException
	{
		String methodName ="populateContractDetailsTabFromContractView";
		logger.logInfo(methodName+"|"+"Start..");
	    //if contract has been saved
		if(contractId != null && contractId.trim().length()>0)
		{		
			
			    constructionContractView = getConstructionContractFromViewRoot();
				viewRootMap.put(INVESTOR_INFO ,constructionContractView.getInvestor());
				investor= constructionContractView.getInvestor();
				hdnOwnerId  = constructionContractView.getInvestor().getPersonId().toString();
				DateFormat dateFormat =new SimpleDateFormat(getDateFormat());
				contractCreatedOn = dateFormat.format(constructionContractView.getCreatedOn());
				contractCreatedBy = constructionContractView.getCreatedBy();
				IstisnaaContractDetailsTab cdt= (IstisnaaContractDetailsTab )getBean("pages$IstisnaaContractDetailsTab");	
				cdt.populateContractDetails(constructionContractView,CONTRACT_ADDITIONAL_INFO_VIEW);
				populateInvestorInfoInTab(); 
				loadAttachmentsAndComments(constructionContractView.getContractId());
		}
		logger.logInfo(methodName+"|"+"Finish..");
	}

	
	
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public void tabProjectDetails_Click()
	{
		String methodName ="tabProjectDetails_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
    	    projectView  = getProjectInfo();
//    	    if(projectView != null)
//    	    {
//    	      
//    		  viewRootMap.put(PROPERTY_VIEW , projectView.getPropertyView());
//    	    //TODO:Commented because the conversion is causing erros when generating payments
////	    	 if(hdnTenderId== null || hdnTenderId.trim().length()<=0)
////	  		 {
////	  				DecimalFormat df = new DecimalFormat();
////	  				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,df.format(projectView.getProjectEstimatedCost()));
////	  		 }
//    	    }
			if(projectView!=null && projectView.getProjectId()!=null)
			{   
				ProjectDetailsTabBOTBacking pdtb= (ProjectDetailsTabBOTBacking)getBean("pages$projectDetailsTabBOT");
			    pdtb.populateProjectDetails(projectView,null,getPropertyInfo());
			}
			else if(hdnPropertyId != null) 
			{
				propertyView = getPropertyInfo();
				ProjectDetailsTabBOTBacking pdtb= (ProjectDetailsTabBOTBacking)getBean("pages$projectDetailsTabBOT");
			    pdtb.populateProjectDetails(null,null,propertyView);
			}
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void tabPaymentTerms_Click()
	{
		String methodName ="tabPaymentTerms_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(this.contractId!=null && this.contractId.trim().length()>0 &&
					!viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
			{
				getPaymentScheduleByContractId();
			}
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	private void getPaymentScheduleByContractId() throws PimsBusinessException {
		PropertyServiceAgent psa= new PropertyServiceAgent();
		if(this.getContractId()!=null)
		{
		 List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(new Long(this.getContractId()),null);
		 viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,paymentScheduleViewList);
		}
	}
	
	public PersonView getInvestorFromViewRoot()
	{
		
		if(viewRootMap.get(INVESTOR_INFO)!=null)
			return (PersonView)viewRootMap.get(INVESTOR_INFO);
		else
			return null;
		
	}
	public ConstructionContractView getConstructionContractFromViewRoot()
	{
		if(viewRootMap.get(CONTRACT_VIEW)!=null)
			return (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
		else
			return null;
		
	}
	public RequestView getRequestViewFromViewRoot()
	{
		if(viewRootMap.get(REQUEST_VIEW)!=null)
			return (RequestView)viewRootMap.get(REQUEST_VIEW);
		else
			return null;
		
	}
	public void tabAuditTrail_Click()
	{
		String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	errorMessages = new ArrayList<String>(0);
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(contractId!=null && contractId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
		
	}
	
	public void btnReviewReq_Click()
	{
		String methodName ="btnReviewReq_Click";
		logger.logInfo(methodName+"|"+"Start..");
		successMessages= new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			 
			if(txtRemarks!=null && txtRemarks.trim().length()>0)
			{
				if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();
				setTaskOutCome(TaskOutcome.LEGAL_DEPT_REVIEW);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				saveCommensAttachment(MessageConstants.ContractEvents.REVIEW_REQUIRED_LEGAL);
				txtRemarks ="";
				getContractById(this.contractId);
				viewRootMap.put(Page_Mode.PAGE_MODE , Page_Mode.LEGAL_REVIEW_REQUIRED);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_SENT_FOR_REVIEW));
			}
			else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnApprove_Click()
	{
		String methodName ="btnApprove_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.APPROVE);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_APPROVED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.COMPLETE);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_APPROVED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReject_Click()
	{
		String methodName ="btnReject_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.REJECT);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_REJECTED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REJECED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReview_Click()
	{
		String methodName ="btnReview_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			   if(txtRemarks!=null && txtRemarks.trim().length()>0)
			   {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					 getIncompleteRequestTasks();
				   setTaskOutCome(TaskOutcome.OK);
				   viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				   saveCommensAttachment(MessageConstants.ContractEvents.REVIEWED_LEGAL);
				   txtRemarks ="";
				   //getContractById(this.contractId);
				   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.APPROVAL_REQUIRED);
				   //viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
				   successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REVIEWED_SUCCESSFULLY));
			   }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	  
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	
	public void btnComplete_Click()
	{
		String methodName ="btnComplete_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	//if(CommonUtil.isAllPaymentsCollected((ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)))
			    	{
				    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
							  getIncompleteRequestTasks();
						setTaskOutCome(TaskOutcome.COMPLETE);
						viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_ACTIVATED);
				    	txtRemarks ="";
				    	getContractById(this.contractId);
				    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ACTIVE);
				    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_COMPLETED_SUCCESSFULLY));
			    	}
//			    	else
//			    		errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.msg.collectAllPayments"));
			    		
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
	
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	
	public void btnPrint_Click()
	{
		String methodName ="btnPrint_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_PRINTED);
			        txtRemarks ="";
			        successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_PRINTED_SUCCESSFULLY));
			    }
				else
				  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}

	private void saveCommensAttachment(String eventDesc) throws Exception {
		String methodName ="saveCommensAttachment";
		 constructionContractView  = (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
		 if(constructionContractView != null && constructionContractView.getContractId()!=null)
		 {
		     saveComments(constructionContractView.getContractId());
			 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
			 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
			 saveAttachments(constructionContractView.getContractId().toString());
			 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		
		 }
		saveSystemComments(eventDesc);
	}

	
	public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
		return tabPaymentTerms;
	}

	public void setTabPaymentTerms(
			org.richfaces.component.html.HtmlTab tabPaymentTerms) {
		this.tabPaymentTerms = tabPaymentTerms;
	}
	public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
		return tabAuditTrail;
	}
	
	
	public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
		this.tabAuditTrail = tabAuditTrail;
	}
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;
		
		return isViewModePopup;
	}

	public String getPageTitle() {
		if(viewRootMap.containsKey("pageTitle") && viewRootMap.get("pageTitle")!=null)
			pageTitle = viewRootMap.get("pageTitle").toString();
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
		if(this.pageTitle !=null)
			viewRootMap.put("pageTitle",this.pageTitle );
			
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public DomainDataView getDdContractType() {
		return ddContractType;
	}

	public void setDdContractType(DomainDataView ddContractType) {
		this.ddContractType = ddContractType;
	}

	public String getPageMode() {
		if(viewRootMap.get(Page_Mode.PAGE_MODE)!=null)
			pageMode =viewRootMap.get(Page_Mode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getContractCreatedOn() {
		return contractCreatedOn;
	}

	public void setContractCreatedOn(String contractCreatedOn) {
		this.contractCreatedOn = contractCreatedOn;
	}

	public String getContractCreatedBy() {
		return contractCreatedBy;
	}

	public void setContractCreatedBy(String contractCreatedBy) {
		this.contractCreatedBy = contractCreatedBy;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnReview() {
		return btnReview;
	}

	public void setBtnReview(HtmlCommandButton btnReview) {
		this.btnReview = btnReview;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlCommandButton getBtnReviewReq() {
		return btnReviewReq;
	}

	public void setBtnReviewReq(HtmlCommandButton btnReviewReq) {
		this.btnReviewReq = btnReviewReq;
	}
	
	protected void PageModeAdd(IstisnaaContractDetailsTab scdt)
	{
		scdt.enableDisableControls("");
		btnSend_For_Approval.setRendered(false);
		tbl_Action.setRendered(false);
		
	}
	protected void PageModeEdit(IstisnaaContractDetailsTab scdt)
	{
		
		
		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.EDIT);
		tbl_Action.setRendered(false);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(true);
		
	}
	protected void PageModeApprovalRequired(IstisnaaContractDetailsTab scdt)
	{
		

		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.APPROVAL_REQUIRED);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		btnReviewReq.setRendered(true);
		btnReview.setRendered(false);
		tbl_Action.setRendered(true);
		btnSave.setRendered(false);
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.getBtnAddPayments().setRendered(true);
		scpt.getImgEditPayments().setRendered(true);
		scpt.getImgDelPayments().setRendered(true);
		
	}
	protected void PageModeLegalReviewRequired(IstisnaaContractDetailsTab scdt)
	{
		
		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.LEGAL_REVIEW_REQUIRED);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(true);
		tbl_Action.setRendered(true);
		btnSave.setRendered(false);
	}
	protected void PageModeComplete(IstisnaaContractDetailsTab scdt)
	{
		
		
		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.COMPLETE);
		tbl_Action.setRendered(true);
		tbl_Action.setRendered(true);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(false);
		btnComplete.setRendered(true);
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.getBtnAddPayments().setRendered(true);
		scpt.getImgEditPayments().setRendered(true);
		scpt.getImgDelPayments().setRendered(true);
	}
	protected void PageModeActive(IstisnaaContractDetailsTab scdt)
	{
		
		
		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.COMPLETE);
		btnComplete.setRendered(false);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(false);
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.getBtnAddPayments().setRendered(true);
		scpt.getImgEditPayments().setRendered(true);
		scpt.getImgDelPayments().setRendered(true);
		
	}
	protected void PageModeView(IstisnaaContractDetailsTab scdt)
	{
		
		
		scdt.enableDisableControls(IstisnaaContractDetailsTab.Page_Mode.VIEW);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(false);
		
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnSend_For_Approval() {
		return btnSend_For_Approval;
	}

	public void setBtnSend_For_Approval(HtmlCommandButton btnSend_For_Approval) {
		this.btnSend_For_Approval = btnSend_For_Approval;
	}

	public String getTenderTypeSecurity()
	{
		return WebConstants.Tender.TENDER_TYPE_SECURITY;
		
	}

	public org.richfaces.component.html.HtmlTab getTabProjectDetails() {
		return tabProjectDetails;
	}

	public void setTabProjectDetails(
			org.richfaces.component.html.HtmlTab tabProjectDetails) {
		this.tabProjectDetails = tabProjectDetails;
	}

	public String getHdnProjectId() {
		return hdnProjectId;
	}

	public void setHdnProjectId(String hdnProjectId) {
		this.hdnProjectId = hdnProjectId;
	}
	
	public String getTenderTypeConstruction() {
		tenderTypeConstruction = WebConstants.Tender.TENDER_TYPE_CONSTRUCTION;
		return tenderTypeConstruction ;
	}

	public void setTenderTypeConstruction(String tenderTypeConstruction) {
		this.tenderTypeConstruction = tenderTypeConstruction;
	}

	public PropertyView getPropertyView() {
		return propertyView;
	}

	public void setPropertyView(PropertyView propertyView) {
		this.propertyView = propertyView;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnOwnerId() {
		return hdnOwnerId;
	}

	public void setHdnOwnerId(String hdnOwnerId) {
		this.hdnOwnerId = hdnOwnerId;
	}
	


	
}
