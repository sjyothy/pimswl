package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.FinanceServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.util.list.ListComparator;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.*;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.finance.FinanceService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.PaidFacilitiesView;
import com.avanza.pims.ws.vo.PaymentDetailsView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.pims.ws.vo.RequestView;



/**
 * SecUserGroup generated by MyEclipse Persistence Tools
 */

public class PaymentsDetails  extends AbstractController
{
	Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private final String PAYMENT_SCHEDULE_VIEW="PAYMENT_SCHEDULE_VIEW";
	private final String PAYMENT_RECEIPT_DETAILS_VIEW="PAYMENT_RECEIPT_DETAILS_VIEW";
	private String dateFormat;
	private PaymentScheduleView paymentScheduleView=new PaymentScheduleView();
	private static Logger logger = Logger.getLogger( PaymentsDetails.class );
	private List<PaymentReceiptDetailView> paymentReceiptDetailsViewList=new ArrayList<PaymentReceiptDetailView>();
	private PaymentReceiptView prv=new PaymentReceiptView();
	private PaymentDetailsView paymentsDetailsView=new PaymentDetailsView();
	private String paymentCategory="";
	private PaymentReceiptDetailView paymentReceiptDetailsView=new PaymentReceiptDetailView();
	private boolean paymentCollected=false;
	private DomainDataView ddvForPaymentStatus=new DomainDataView();
	private List<DomainDataView> ddvForPaymentCategoryList=new ArrayList<DomainDataView>(); 
	PropertyServiceAgent psa=new PropertyServiceAgent();
	
	private boolean isEnglishLocale = false;
	
	@SuppressWarnings( "unchecked" )
     public void init()
     {
    	 super.init();
    	try
    	{
    	 if(sessionMap.get(PAYMENT_SCHEDULE_VIEW)!=null)
    	 {
    		 paymentScheduleView=(PaymentScheduleView) sessionMap.get(PAYMENT_SCHEDULE_VIEW);
    		 sessionMap.remove(PAYMENT_SCHEDULE_VIEW);
    		 viewRootMap.put(PAYMENT_SCHEDULE_VIEW, paymentScheduleView);
    		 if( paymentScheduleView.getPaymentReceipts()!=null && paymentScheduleView.getPaymentReceipts().size()>0 )
    		 {
	    		 prv=paymentScheduleView.getPaymentReceipts().get(0);
	    		 paymentsDetailsView=psa.getPaymentDetailViewByScheduleId(paymentScheduleView.getPaymentScheduleId());
	    		 paymentReceiptDetailsView=paymentsDetailsView.getPaymentReceiptDetailView();
	    		 viewRootMap.put(PAYMENT_RECEIPT_DETAILS_VIEW, paymentReceiptDetailsView);
    		 }
    	 }
    	}
    	catch(Exception e)
    	{
    		logger.LogException("init() Crashed : ", e);
    	}
     }
     public String getDateFormat()
 	{
     	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
     	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
     	
     	dateFormat= localeInfo.getDateFormat();
     	
 		return dateFormat;
 	}
     public PaymentsDetails()
     {
    	 
     }
	public PaymentScheduleView getPaymentScheduleView() 
	{
		if(viewRootMap.get(PAYMENT_SCHEDULE_VIEW)!=null)
			paymentScheduleView=(PaymentScheduleView) viewRootMap.get(PAYMENT_SCHEDULE_VIEW);
		return paymentScheduleView;
	}
	public void setPaymentScheduleView(PaymentScheduleView paymentScheduleView) 
	{
		this.paymentScheduleView = paymentScheduleView;
	}
	public PaymentReceiptView getPrv() {
		return prv;
	}
	public void setPrv(PaymentReceiptView prv) {
		this.prv = prv;
	}
	public List<PaymentReceiptDetailView> getPaymentReceiptDetailsViewList() {
		return paymentReceiptDetailsViewList;
	}
	public void setPaymentReceiptDetailsViewList(
			List<PaymentReceiptDetailView> paymentReceiptDetailsViewList) {
		this.paymentReceiptDetailsViewList = paymentReceiptDetailsViewList;
	}
	public PaymentDetailsView getPaymentsDetailsView() 
	{
		return paymentsDetailsView;
	}
	public void setPaymentsDetailsView(PaymentDetailsView paymentsDetailsView) {
		this.paymentsDetailsView = paymentsDetailsView;
	}
	public PaymentReceiptDetailView getPaymentReceiptDetailsView() 
	{
		if(viewRootMap.get(PAYMENT_RECEIPT_DETAILS_VIEW)!=null)
			paymentReceiptDetailsView=(PaymentReceiptDetailView) viewRootMap.get(PAYMENT_RECEIPT_DETAILS_VIEW);
		return paymentReceiptDetailsView;
	}
	public void setPaymentReceiptDetailsView(
			PaymentReceiptDetailView paymentReceiptDetailsView) {
		this.paymentReceiptDetailsView = paymentReceiptDetailsView;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public boolean isPaymentCollected() 
	{
		{
			if(viewRootMap.get(PAYMENT_SCHEDULE_VIEW)!=null)
			paymentScheduleView=(PaymentScheduleView) viewRootMap.get(PAYMENT_SCHEDULE_VIEW);
			ddvForPaymentStatus= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_COLLECTED);
			DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	                WebConstants.REALIZED);
			if( ddvForPaymentStatus.getDomainDataId().compareTo(paymentScheduleView.getStatusId())==0  || 
			    ddvRealized.getDomainDataId().compareTo(paymentScheduleView.getStatusId())==0	
			  )
				paymentCollected=true;
		}
		return paymentCollected;
	}
	public void setPaymentCollected(boolean paymentCollected) {
		this.paymentCollected = paymentCollected;
	}
	public String getPaymentCategory() 
	{
		if(viewRootMap.get(PAYMENT_SCHEDULE_VIEW)!=null)
		{
			paymentScheduleView=(PaymentScheduleView) viewRootMap.get(PAYMENT_SCHEDULE_VIEW);
			ddvForPaymentCategoryList= CommonUtil.getDomainDataListForDomainType(WebConstants.PaymentCategories.PAYMENT_CATEGORY);
			DomainDataView ddvPC=new DomainDataView();
			for(DomainDataView ddv : ddvForPaymentCategoryList)
			{
				if(ddv.getDomainDataId().compareTo(paymentScheduleView.getPaymentCategory())==0)
					{
						ddvPC=ddv;
						if(CommonUtil.getIsEnglishLocale())
							paymentCategory=ddvPC.getDataDescEn();
						else
							paymentCategory=ddvPC.getDataDescAr();
						break;
					}
			}
		}
		return paymentCategory;
	}
	public void setPaymentCategory(String paymentCategory) {
		this.paymentCategory = paymentCategory;
	}
	public DomainDataView getDdvForPaymentStatus() {
		return ddvForPaymentStatus;
	}
	public void setDdvForPaymentStatus(DomainDataView ddvForPaymentStatus) {
		this.ddvForPaymentStatus = ddvForPaymentStatus;
	}
	public List<DomainDataView> getDdvForPaymentCategoryList() {
		return ddvForPaymentCategoryList;
	}
	public void setDdvForPaymentCategoryList(
			List<DomainDataView> ddvForPaymentCategoryList) {
		this.ddvForPaymentCategoryList = ddvForPaymentCategoryList;
	}
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

}