package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContractorTypeView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.StudyDetailView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;

public class TenderInquirySearch extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(TenderInquirySearch.class);

	public interface Keys {
		public static final String CONTRACTORS_LIST = "CONTRACTORS_LIST";
		public static final String RECORD_SIZE = "RECORD_SIZE";
		public static final String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static final String CONTRACTOR_TYPE_LIST = "CONTRACTOR_TYPE_LIST";
		public static final String LICENSE_SOURCE_LIST = "LICENSE_SOURCE_LIST";
		public static final String COUNTRY_LIST = "COUNTRY_LIST";
		public static final String STATE_LIST = "STATE_LIST";
		public static final String CITY_LIST = "CITY_LIST";
		public static final String VIEW_MODE = "viewMode";
		public static final String POPUP_MODE = "popup";
		public static final String TENDER_INQUIRY_LIST = "TENDER_INQUIRY_LIST";
		public static final String TENDER_INQUIRY_LIST_TO_SEND = "TENDER_INQUIRY_LIST_TO_SEND";
	}

	private ServletContext context1 = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private Map<String,List<SelectItem>> listMap = new HashMap<String,List<SelectItem>>();
	private List<SelectItem> emptyList = new ArrayList<SelectItem>();
	HttpServletRequest request = (HttpServletRequest) FacesContext
			.getCurrentInstance().getExternalContext().getRequest();
	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<String> errorMessages;
	private List<String> infoMessages;

	// UI component attributes
	private HtmlDataTable dataTable ;

	// configurable values of data table
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	// attributes binded with UI Components to save their values entered by the
	// user
	private String requestNumber;
	private String requestStatusId;
	private Date   requestDate;
	private String sourceTypeId;
	private String contractorNameAr;
	private String contractorNameEn;
	private String contractorType;
	private String tenderNumber;
	private String tenderDescription;
	

	// lists of UI components goes here
	private Map<String, String> contractorTypeList;
	private Map<String, String> licenseSourceList;
	private Map<String, String> countryList;
	private Map<String, String> stateList;
	private Map<String, String> cityList;
	private List<TenderInquiryView> tenderInquiryViewList;
    private HashMap searchMap = new HashMap();
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		try {
			if (!isPostBack()) {
				
				
				if(request.getParameter("ConstructionTender")!=null)
					viewMap.put("FROM_CONSTRUCTION_TENDER", true);
				if (request.getParameter(Keys.VIEW_MODE) != null)
					viewMap.put(Keys.VIEW_MODE, request.getParameter(
							Keys.VIEW_MODE).toString());

				loadMapValues();
				
				// FROM RECEIVE PROPOSAL SCREEN AS POPUP - Hammad
				String tenderId = (String) sessionMap.get(WebConstants.ReceiveTenderProposals.TENDER_ID_FOR_CONTRACTOR_SEARCH);
				if(StringHelper.isNotEmpty(tenderId))
					viewMap.put(WebConstants.Tender.TENDER_ID, tenderId);
				//
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}

	// fills the maps that is populated in the UI components
	@SuppressWarnings("unchecked")
	private void loadMapValues() {
		loadContractorTypes();
		viewMap.put(Keys.CONTRACTOR_TYPE_LIST, contractorTypeList);

	}



	// loads the country list
	private final void loadContractorTypes() {
		String methodName = "loadContractorTypes()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<ContractorTypeView> contractTypeViewList = csa
					.getAllContractorType();

			if (contractorTypeList == null)
				contractorTypeList = new HashMap<String, String>();

			for (ContractorTypeView contractTypeView : contractTypeViewList)
				getContractorTypeList().put(
						getIsEnglishLocale() ? contractTypeView
								.getDescriptionEn() : contractTypeView
								.getDescriptionAr(),
						contractTypeView.getContractorTypeId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}






	// searches the tender according to the criteria specified by the user
	@SuppressWarnings("unchecked")
	public void searchTenders() {
		errorMessages  = new ArrayList<String>();
		String METHOD_NAME = "searchTenders()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			
			if(requestNumber!=null && !requestNumber.equals(""))
			searchMap.put("requestNumber", requestNumber);
			if(requestStatusId!=null && !requestStatusId.equals("")&& !requestStatusId.equals("-1"))
			searchMap.put("requestStatus", requestStatusId);
			if(requestDate!=null && !requestDate.equals(""))
			searchMap.put("requestDate", requestDate);
			if(sourceTypeId!=null && !sourceTypeId.equals("")&& !sourceTypeId.equals("-1"))
			searchMap.put("requestSource", sourceTypeId);
			if(contractorNameEn!=null && !contractorNameEn.equals(""))
			searchMap.put("contractorNameEn", contractorNameEn);
			if(contractorNameAr!=null && !contractorNameAr.equals(""))
			searchMap.put("contractorNameAr", contractorNameAr);
			if(contractorType!=null && !contractorType.equals("")&& !contractorType.equals("-1"))
			searchMap.put("contractorType", contractorType);
			if(tenderNumber!=null && !tenderNumber.equals(""))
			searchMap.put("tenderNumber", tenderNumber);
			if(tenderDescription!=null && !tenderDescription.equals(""))
			searchMap.put("tenderDescription", tenderDescription);
			if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
			searchMap.put("FROM_CONSTRUCTION_TENDER", true);	
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			tenderInquiryViewList = csa.getAllTenderInquiries(searchMap);
 					

			recordSize = tenderInquiryViewList.size();
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			
			if(recordSize ==0){
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}

			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;

			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

			viewMap.put(Keys.RECORD_SIZE, recordSize);
			viewMap.put(Keys.PAGINATOR_MAX_PAGES, paginatorMaxPages);
			viewMap.put(Keys.TENDER_INQUIRY_LIST, tenderInquiryViewList);

			logger.logInfo(METHOD_NAME + " method completed Succesfully");
		} catch (PimsBusinessException e) {
			logger.LogException(METHOD_NAME + " crashed due to:", e);
		}
	}

	public String replayAll(){
		
		String METHOD_NAME = "replayAll()";
		List<TenderInquiryView> tenderInquiryViewList= new ArrayList<TenderInquiryView>();
		List<TenderInquiryView> tenderInquiryViewListToSend= new ArrayList<TenderInquiryView>();
		
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");
		
		if (viewMap.containsKey(Keys.TENDER_INQUIRY_LIST)) {
			tenderInquiryViewList = (ArrayList<TenderInquiryView>) viewMap
					.get(Keys.TENDER_INQUIRY_LIST);
			for (TenderInquiryView tIV : tenderInquiryViewList) {
				if (tIV.getSelected() != null && !tIV.equals("")) {
					if (tIV.getSelected()) {
						tenderInquiryViewListToSend.add(tIV);
					}
				}

			}
			
			if(tenderInquiryViewListToSend.size()>0){
				sessionMap.put(Keys.TENDER_INQUIRY_LIST_TO_SEND, tenderInquiryViewListToSend);
			}
		}
		logger.logInfo(METHOD_NAME + "|" + "End...");
		

		final String viewId = "/tenderInquirySearch.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showTenderInquiryRepliesAll();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

		

		
		return "Replay_All_NEW";
	}
	
	// Accessors starts from here
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}



	public String getContractorNameAr() {
		return contractorNameAr;
	}

	public void setContractorNameAr(String contractorNameAr) {
		this.contractorNameAr = contractorNameAr;
	}

	public String getContractorNameEn() {
		return contractorNameEn;
	}

	public void setContractorNameEn(String contractorNameEn) {
		this.contractorNameEn = contractorNameEn;
	}

	public String getContractorType() {
		return contractorType;
	}

	public void setContractorType(String contractorType) {
		this.contractorType = contractorType;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	@SuppressWarnings("unchecked")
	public List<TenderInquiryView> getTenderInquiryViewList() {
		if (viewMap.get(Keys.TENDER_INQUIRY_LIST) != null)
			tenderInquiryViewList = (List<TenderInquiryView>) viewMap
					.get(Keys.TENDER_INQUIRY_LIST);

		return tenderInquiryViewList;
	}

	public void setTenderInquiryViewList(List<TenderInquiryView> tenderInquiryViewList) {
		this.tenderInquiryViewList = tenderInquiryViewList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getContractorTypeList() {
		if (viewMap.get(Keys.CONTRACTOR_TYPE_LIST) != null)
			contractorTypeList = (Map<String, String>) viewMap
					.get(Keys.CONTRACTOR_TYPE_LIST);

		return contractorTypeList;
	}

	public void setContractorTypeList(Map<String, String> contractorTypeList) {
		this.contractorTypeList = contractorTypeList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getLicenseSourceList() {
		if (viewMap.get(Keys.LICENSE_SOURCE_LIST) != null)
			licenseSourceList = (Map<String, String>) viewMap
					.get(Keys.LICENSE_SOURCE_LIST);

		return licenseSourceList;
	}

	public void setLicenseSourceList(Map<String, String> licenseSourceList) {
		this.licenseSourceList = licenseSourceList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCityList() {
		if (viewMap.get(Keys.CITY_LIST) != null)
			cityList = (Map<String, String>) viewMap.get(Keys.CITY_LIST);

		return cityList;
	}

	public void setCityList(Map<String, String> cityList) {
		this.cityList = cityList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getStateList() {
		if (viewMap.get(Keys.STATE_LIST) != null)
			stateList = (Map<String, String>) viewMap.get(Keys.STATE_LIST);

		return stateList;
	}

	public void setStateList(Map<String, String> stateList) {
		this.stateList = stateList;
	}

	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(getTenderInquiryViewList()!=null &&getTenderInquiryViewList().size()>0){
			return getTenderInquiryViewList().size();	
		}else{
			return 0;
		}
		
	}




	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestStatusId() {
		return requestStatusId;
	}

	public void setRequestStatusId(String requestStatusId) {
		this.requestStatusId = requestStatusId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(String sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	public String getTenderNumber() {
		return tenderNumber;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public String getTenderDescription() {
		return tenderDescription;
	}

	public void setTenderDescription(String tenderDescription) {
		this.tenderDescription = tenderDescription;
	}
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }

	public List<SelectItem> getRequestStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();
		
		if(isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_EN);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_OPEN)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_REPLIED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_REJECTED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_AR);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_OPEN)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_REPLIED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_REJECTED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
		
	}
	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	
	public String btnAddInquiry_Click()
	{
		if(viewMap.get("FROM_CONSTRUCTION_TENDER")!=null)	
	       sessionMap.put("FROM_CONSTRUCTION_TENDER", true);
		
		return "TenderInquiryAdd";
	}
	
	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}
	
	public String btnManage_Click(){
		
		TenderInquiryView tT = (TenderInquiryView) dataTable.getRowData();
		
		sessionMap.put("FROM_INQUIRY_GRID", tT);
		
		
		final String viewId = "/tenderInquirySearch.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showTenderInquiryReplies();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	
		return "btnManage_Click";
	}
	
	public String btnDeleteInquiry_Click(){
	
		
		
		TenderInquiryView tIV = (TenderInquiryView) dataTable.getRowData();
		Integer index =  dataTable.getRowIndex();
		viewMap.put("delete", index);
		
		Boolean flage = false;
		infoMessages = new ArrayList<String>();
		try {
			flage = new ConstructionServiceAgent().setTenderInquiryToDeleted(tIV.getRequestId());
			
			if(flage){
				index = (Integer) viewMap.get("delete");
				List<TenderInquiryView> tempList = getTenderInquiryViewList();
				tempList.remove(index.intValue());
				setTenderInquiryViewList(tempList);
				infoMessages.add(CommonUtil.getBundleMessage("tenderInquiryReplies.msg.DetailRequired"));
			}
			
		} catch (PimsBusinessException e) {
			logger.LogException("Exception in btnDeleteViolation_Click::::::::::::::", e);
		}	
			
		return "EditViolation";
	}
	
	public String btnViewInquiry_Click(){
		
		TenderInquiryView tT = (TenderInquiryView) dataTable.getRowData();
		
		sessionMap.put("FROM_INQUIRY_GRID", tT);
		
		
		final String viewId = "/tenderInquirySearch.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showTanderInquiryAddPopUp();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	
		return "btnManage_Click";
	}
	
	/*public DomainDataView getDomainDataForRepliedRequest ()
	{
		
		if(!viewMap.containsKey("REQUEST_REPLIED"))
		{
		 DomainDataView ddv = new DomainDataView();
		 ddv =  CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),WebConstants.REQUEST_STATUS_REPLIED) ;
		 viewMap.put("REQUEST_REPLIED", ddv);
			return ddv;
		}else
		return (DomainDataView)viewMap.get("REQUEST_REPLIED");
	    
	
	}
	
	public DomainDataView getDomainDataForRejectedRequest ()
	{
		
		if(!viewMap.containsKey("REQUEST_REJECTED"))
		{
		 DomainDataView ddv = new DomainDataView();
		 ddv =  CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),WebConstants.REQUEST_STATUS_REJECTED);
		 viewMap.put("REQUEST_REJECTED", ddv);
			return ddv;
		}else
		return (DomainDataView)viewMap.get("REQUEST_REJECTED");
	    
	
	}
	
	public boolean getEditingEnable()
	{  
		
		boolean showEditIcons = true;
		if(getTenderInquiryViewList()!=null && getTenderInquiryViewList().size()>0)
		{
			TenderInquiryView tIV = (TenderInquiryView) dataTable.getRowData();
			DomainDataView ddV = getDomainDataForRepliedRequest();
			DomainDataView ddV2 = getDomainDataForRejectedRequest();
			
			if(!(ddV.getDomainDataId().equals(tIV.getStatusId())||ddV2.getDomainDataId().equals(tIV.getStatusId())))
			{
				showEditIcons = false;
			}
		
		}
		return showEditIcons;
	}*/
}
