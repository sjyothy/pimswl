package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import COM.rsa.asn1.el;
import COM.rsa.jsafe.v;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.AuctionVenue;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionVenueView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.ui.util.ResourceUtil;


public class AuctionVenueBackingBean extends AbstractController
{

	/**
	 * 
	 */
	
	public AuctionVenueBackingBean(){
		
	}
	private transient Logger logger = Logger.getLogger(AuctionVenueBackingBean.class);
//	
//	private static final long serialVersionUID = 1L;
//
	private String message;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private AuctionVenueView dataItem = new AuctionVenueView();
	private AuctionVenueView venue = new AuctionVenueView();
	
	private List<AuctionVenueView> dataList = new ArrayList<AuctionVenueView>();
	
	private List<SelectItem> countries = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>();
	private List<SelectItem> cities = new ArrayList<SelectItem>();
	
	private Map<Long,RegionView> countryMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> stateMap = new HashMap<Long,RegionView>();
	
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private boolean sortAscending = true;
	
	private List<SelectItem> auctionStatuses = new ArrayList<SelectItem>();
	
	private List<String> errorMessages;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	CommonUtil	commonUtil = new CommonUtil();


	/*
	 * Methods
	 */
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

    		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public void cancel(ActionEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";
        
        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);         
    }
	
    public String cancel(){
    	
    	return "cancel";
    }
    
    public String reset(){
    	    	
    	return "reset";
    }
    
   /* public String searchVenue(){
    	this.message = "Search Results";
    	loadVenueList();
        return "searchVenue";
    }*/
    
    public Boolean atleastOneCriterionEntered() {
		if(StringHelper.isNotEmpty(venue.getVenueName()))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getCountryIdString())  && !venue.getLocation().getCountryIdString().equals("0"))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getStateIdString())  && !venue.getLocation().getStateIdString().equals("0"))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getStreet()))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getPostCode()))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getAddress1()))
			return true;
		if(StringHelper.isNotEmpty(venue.getLocation().getAddress2()))
			return true;
		return false;
	}
    
    public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		if(!atleastOneCriterionEntered()){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			validated = false;
			return validated;
		}
		/*if (auctionView.getAuctionNumber().lastIndexOf(" ")>=0) {
		 errorMessages.add(bundle.getString(WebConstants.PropertyKeys.Commons.Validation.SHOULD_NOT_HAVE_SPACE));
		 validated = false;
		}
		if (auctionView.getAuctionDate().equals("")){
			if( (!auctionView.getAuctionStartTime().equals("")) || (!auctionView.getAuctionEndTime().equals(""))) 
			{
			 errorMessages.add(bundle.getString(WebConstants.PropertyKeys.Auction.Validation.SHOULD_HAVE_AUCTION_DATE));
			 validated = false;
			}
		}*/

		return validated;
	}
	
	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}
    
    public String searchVenue()
    {
    	logger.logInfo("searchVenue() started...");
    	try{
    		if(validateFields())
    			loadVenueList();
    		logger.logInfo("searchVenue() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("searchAuctions() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("searchVenue() crashed ",exception);
    	}
        return "searchVenue";
    }
	
    private void loadVenueList() {
		logger.logInfo("loadVenueList() started...");
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		
    	try{		
    		dataList = propertyServiceAgent.getAuctionVenues(venue,"and");
    		
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("venueList", dataList);
			recordSize = dataList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			
    		if(dataList.isEmpty() || dataList==null)
    		{
				errorMessages.clear();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
    		}
    		else{
    			for(AuctionVenueView temp: dataList){
    				ContactInfoView location =  temp.getLocation();
    				RegionView stateView = stateMap.get(location.getStateId());
    				RegionView countryView = countryMap.get(location.getCountryId());
    				String state = "";
    				String country = "";
    				if (getIsEnglishLocale()){
    					if (stateView!=null){
    						if(!stateView.equals("")){
		    					state = stateView.getDescriptionEn();
		    					country = countryView.getDescriptionEn();
		    					}
    					  }
    				}
    				else{
    					if (stateView!=null){
    						if(!stateView.equals("")){
		    					state = stateView.getDescriptionAr();
		    					country = countryView.getDescriptionAr();
		    					}
    					   }
    				}
    				location.setState(state);
    				location.setCountry(country);
    			}
    		}
			logger.logInfo("loadVenueList() completed successfully!!!");
		}
		/*catch(PimsBusinessException exception){
			logger.LogException("loadAuctionList() crashed ",exception);
		}*/
		catch(Exception exception){
			logger.LogException("loadVenueList() crashed ",exception);
		}
	}	
    
    
    public String createVenue(){
    	this.message = "New Venue Created";
    	return "createVenue";
    }
    // Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	
	
	public List<AuctionVenueView> getVenueDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<AuctionVenueView>)viewMap.get("venueList");
		if(dataList==null)
			dataList = new ArrayList<AuctionVenueView>();
		return dataList;
	}

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	private void loadCountries() {
		try {
			logger.logInfo("loadCountries() started...");
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			countries = new ArrayList();
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			if(viewMap.containsKey("countryMap"))
			{
				countryMap = (HashMap<Long, RegionView>) viewMap.get("countryMap");
			}
			else{
				countryMap = new HashMap<Long, RegionView>();
				for(RegionView regView :regionViewList)
				{
					countryMap.put(regView.getRegionId(), regView);
				}
				viewMap.put("countryMap",countryMap);
			}
			if(viewMap.containsKey("countries"))
			{
				countries = (List<SelectItem>) viewMap.get("countries");
			}
			else{	 
				for(RegionView regionView :regionViewList)
				{
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
				  countries.add(item);
				}
				viewMap.put("countries",countries);
			}
			logger.logInfo("loadCountries() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadCountries() crashed ",exception);
    	}
		
		

//		for (int i = 0; i < countries.size(); i++) {
//			getPropertyTypeSelectMenu().setValue(new SelectItem(countries.get(i)));
//		}
	}
	
	public void loadStateMap(){
		try {
			logger.logInfo("loadStateMap() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			stateMap = new HashMap<Long, RegionView>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> statesList = psa.getAllStates();
			for(RegionView regView :statesList)
			{
				stateMap.put(regView.getRegionId(), regView);
			}
			sessionMap.put("stateMap",stateMap);
			logger.logInfo("loadStateMap() completed successfully!!!");
		}catch(Exception exception){
			logger.LogException("loadStateMap() crashed ",exception);
		}
	}
	
	public void loadStates(ValueChangeEvent vce)  {
		try {
			logger.logInfo("loadStates() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			states = new ArrayList<SelectItem>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			String regionName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)countryMap.get(regionId);
			if(temp!=null)
			{
				if(getIsEnglishLocale())
				   regionName = temp.getDescriptionEn();
				if(getIsArabicLocale())
					regionName = temp.getDescriptionAr();
				List <RegionView> statesList = psa.getCountryStates(regionName,getIsArabicLocale());
				
				for(RegionView stateView:statesList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
					else 
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
					states.add(item);
				}
				sessionMap.put("states",states);
			}
			else{
				sessionMap.remove("states");
			}
				
//			loadCities(vce);
			logger.logInfo("loadStates() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadStates() crashed ",exception);
    	}
	}
	
	public void loadCities(ValueChangeEvent vce)  {
		try {
			logger.logInfo("loadCities() started...");
			cities = new ArrayList<SelectItem>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			String selectedStateName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)stateMap.get(regionId);
			if(temp!=null)
			{
				selectedStateName = temp.getRegionName();
				List <RegionView> cityList = psa.getCountryStates(selectedStateName,getIsArabicLocale());
			
			
				for(RegionView cityView:cityList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionEn());			  }
					else 
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionAr());	  
					cities.add(item);
				}
			}
			logger.logInfo("loadCities() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadCities() crashed ",exception);
    	}
	}
	
	/*private void loadVenueList() {
		dataList = new ArrayList();
		AuctionVenueView venue;
		
		for (int i = 0; i < 40; i++) {
			venue = new AuctionVenueView();
			venue.setVenueName("Venue " + i);
			venue.getLocation().setAddress1("address1 No. " + i);
			venue.getLocation().setAddress2("address2 No. " + i);
			venue.getLocation().setCityId(new Long(1+i));
			venue.getLocation().setCountryId(new Long(2+i));
			venue.getLocation().setStateId(new Long(2+i));
			venue.getLocation().setPostCode("7530" + i);
			venue.getLocation().setStreet("Street No. "+i);
			dataList.add(venue);
			addCount.setValue("15");
		}
//		if (sortField != null) {
//			Collections.sort(dataList, new ListComparator(sortField,
//					sortAscending));
//		}

	}*/
	
	public void selectVenue(javax.faces.event.ActionEvent event)
    {
    	logger.logInfo("selectVenue() started...");
    	try{
    		 AuctionVenueView aucVenueView=(AuctionVenueView)dataTable.getRowData();
    		 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    		 sessionMap.put(WebConstants.AUCTION_VENUE_ID,aucVenueView.getVenueId());
    		 sessionMap.put(WebConstants.AUCTION_VENUE_NAME,aucVenueView.getVenueName());
            
    		 FacesContext facesContext = FacesContext.getCurrentInstance();
 	         ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
 	//        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
 	         String javaScriptText = "javascript:closeWindowSubmit();";
 	
 	        // Add the Javascript to the rendered page's header for immediate execution
 	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
 	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
    		logger.logInfo("selectVenue() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsEditMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("selectVenue() crashed ",exception);
    	}
    }
	
	
	public AuctionVenueView getDataItem() {
		return dataItem;
	}

	public void setDataItem(AuctionVenueView dataItem) {
		this.dataItem = dataItem;
	}
			
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}
	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

		/**
	 * @return the auctionVenue List
	 */
	public List<AuctionVenueView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the AuctionVenue list to set
	 */
	public void setDataList(List<AuctionVenueView> dataList) {
		this.dataList = dataList;
	}


	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	

	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		
		loadCountries();
		loadStateMap();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	/**
	 * @return the venue
	 */
	public AuctionVenueView getVenue() {
		return venue;
	}

	/**
	 * @param venue the venue to set
	 */
	public void setVenue(AuctionVenueView venue) {
		this.venue = venue;
	}

	/**
	 * @return the countries
	 */
	public List<SelectItem> getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(List<SelectItem> countries) {
		this.countries = countries;
	}

	/**
	 * @return the states
	 */
	public List<SelectItem> getStates() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		states = (List<SelectItem>) sessionMap.get("states");
		if(states==null)
			states = new ArrayList<SelectItem>();
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public void setStates(List<SelectItem> states) {
		this.states = states;
	}

	/**
	 * @return the cities
	 */
	public List<SelectItem> getCities() {
		return cities;
	}

	/**
	 * @param cities the cities to set
	 */
	public void setCities(List<SelectItem> cities) {
		this.cities = cities;
	}

	/**
	 * @return the countrySelectMenu
	 */
	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}

	/**
	 * @param countrySelectMenu the countrySelectMenu to set
	 */
	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}

	/**
	 * @return the stateSelectMenu
	 */
	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}

	/**
	 * @param stateSelectMenu the stateSelectMenu to set
	 */
	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}

	/**
	 * @return the citySelectMenu
	 */
	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}

	/**
	 * @param citySelectMenu the citySelectMenu to set
	 */
	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the auctionStatuses
	 */
	public List<SelectItem> getAuctionStatuses() {
		return auctionStatuses;
	}

	/**
	 * @param auctionStatuses the auctionStatuses to set
	 */
	public void setAuctionStatuses(List<SelectItem> auctionStatuses) {
		this.auctionStatuses = auctionStatuses;
	}


	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	/**
	 * @return the countryMap
	 */
	public Map<Long, RegionView> getCountryMap() {
		return countryMap;
	}
	/**
	 * @param countryMap the countryMap to set
	 */
	public void setCountryMap(Map<Long, RegionView> countryMap) {
		this.countryMap = countryMap;
	}
	/**
	 * @return the stateMap
	 */
	public Map<Long, RegionView> getStateMap() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		stateMap = (Map<Long, RegionView>) sessionMap.get("stateMap");
		if(stateMap==null)
			stateMap = new HashMap<Long, RegionView>();
		return stateMap;
	}
	/**
	 * @param stateMap the stateMap to set
	 */
	public void setStateMap(Map<Long, RegionView> stateMap) {
		this.stateMap = stateMap;
	}
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
//		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
////		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
//		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		paginatorMaxPages=WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}
	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;

	}
	
	public String cmdDelete_Click()
	{
        //Write your server side code here
		//fromDelete = true;
		HashMap venueMap = new HashMap();
		Long deleteStatus;
		Long auctionVenueId;
		
		AuctionVenueView auctionVenueView=(AuctionVenueView)dataTable.getRowData();
       try
       {
        	PropertyServiceAgent psa  =new PropertyServiceAgent();	
        	
        	deleteStatus = 1L;
        	auctionVenueId = auctionVenueView.getVenueId();
        	
        	venueMap.put("Delete", deleteStatus);
        	venueMap.put("VenueId", auctionVenueId);
        	
        	psa.addVenueStatus(venueMap);
			
			loadVenueList();
        	
        }catch (Exception e) {
			
        	System.out.println("Exception "+e);
		}
        return "delete";
	}
    public String  cmdStatus_Click()
    	{
            //Write your server side code here
    		//fromDelete = true;
    	HashMap venueMap = new HashMap();
		Long status;
		Long auctionVenueId;
            
		 
        AuctionVenueView auctionVenueView=(AuctionVenueView)dataTable.getRowData();
        
        try
           {
            	PropertyServiceAgent psa  = new PropertyServiceAgent();	
     
            if (auctionVenueView.getRecordStatus()==1){
            	status = 0L;
             	}
            else {
            	status = 1L;
                 }
            auctionVenueId = auctionVenueView.getVenueId();
            
            venueMap.put("Status", status);
        	venueMap.put("VenueId", auctionVenueId);
        	
            psa.addVenueStatus(venueMap);
			
			loadVenueList();
            	
            }catch (Exception e) {
    			
            	System.out.println("Exception "+e);
    		}
            
            return "Status";
     }

    public String  cmdVenueName_Click()
	{
       
    	AuctionVenueView auctionVenueView=(AuctionVenueView)dataTable.getRowData();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try{
            facesContext.getExternalContext().getRequestMap().put("venueId", auctionVenueView.getVenueId());
        }catch (Exception e) {
			
        	System.out.println("Exception "+e);
		}

          return "AddAuctionVenue";

    }
    public String btnAdd_Click()
	{
		return "AddAuctionVenue";
		
	}

}