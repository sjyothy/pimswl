package com.avanza.pims.web.backingbeans;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ViolationCategoryView;
import com.avanza.pims.ws.vo.ViolationTypeView;

public class ViolationCategorySearchBean  extends AbstractController
{
	private static final long serialVersionUID = 9143085774222568923L;
	/**
     * @author Kamran Ahmed
     */
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private static Logger logger = Logger.getLogger(ViolationCategorySearchBean.class);
	private List<SelectItem> ViolationCategoryList;
	private List<ViolationTypeView> violationTypeForCategory; 
	private ViolationCategoryView violationCategoryView;
	private String selectedCategory;
	private String descriptionEn;
	private String descriptionAr;
	private HtmlDataTable dataTable;
	Long categoryId;
	private HtmlOutputLabel lblDescEn = new HtmlOutputLabel();;
	private HtmlOutputLabel lblDescAr=new HtmlOutputLabel();
	private HtmlInputText txtDescEn= new HtmlInputText();
	private HtmlInputText txtDescAr= new HtmlInputText();
	private HtmlCommandButton btnAdd;
	private HtmlCommandButton btnSave;
	private List<String> errorMessages;
	private List<String> successMessages;
	private Long selectedViolationId;
	private boolean render;
	private boolean comboChanged = false;
	@Override
    public void init() 
    {
    	ViolationCategoryList=new ArrayList<SelectItem>();
    	dataTable=new HtmlDataTable();
    	violationTypeForCategory=new ArrayList<ViolationTypeView>();
    	
    	 btnAdd = new HtmlCommandButton();
    	 btnSave = new HtmlCommandButton();
    	 lblDescEn.setRendered(false);
    	 lblDescAr.setRendered(false);
    	 txtDescEn.setRendered(false);
    	 txtDescAr.setRendered(false);
    	 btnSave.setRendered(false);
    	 violationCategoryView =new ViolationCategoryView();
    }
	@Override
    public void prerender()
	{
		if (this.comboChanged)
		{
			renderLabelAndTextBox(false);
			
		}
		System.out.println("Rendered ="+ render);
		
	}
    public void checkTheSelectedCategory(ValueChangeEvent event)
    {
	try {
			
		logger.logInfo("checkTheSelectedCategory( )|Start :");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
			setDescriptionAr("");
			setDescriptionEn("");
//			this.setRender(false);
			renderLabelAndTextBox(false);
			UtilityServiceAgent usa = new UtilityServiceAgent();
			categoryId = Long.parseLong(event.getNewValue().toString());
			if (categoryId.compareTo(-1L)!=0) 
			{
				violationTypeForCategory = usa.getViolationTypeListByCategory(categoryId);
				violationCategoryView=usa.getViolationCategoryById(categoryId);
				if(violationTypeForCategory!=null)
					viewRootMap.put("VIOLATION_TYPE_LIST", violationTypeForCategory);
				if(violationCategoryView!=null)
					viewRootMap.put("VIOLATION_CATEGORY", violationCategoryView);
			}
			else
			{
				violationTypeForCategory.clear();
				viewRootMap.put("VIOLATION_TYPE_LIST", violationTypeForCategory);
			}
				
			logger.logInfo("checkTheSelectedCategory( )|Finish:");
		} 
    	catch (PimsBusinessException e) 
    	{
    		
    		logger.LogException("Method : checkTheSelectedCategory( ) Crashed due to :", e);
		}
    }
    public void searchViolationCategory()
    {
    	try
    	{
    		
    	}
    	catch (Exception e)
    	{
			// TODO: handle exception
		}
    }
    public void editViolationType()
    {
    	try
    	{
    		ViolationTypeView violationTypeView ;
    		violationTypeView = (ViolationTypeView) dataTable.getRowData();
    		viewRootMap.put("SELECTED_RECORD", violationTypeView);
    		selectedViolationId = violationTypeView.getViolationTypeId();
    		viewRootMap.put("VIOLATION_TYPE_ID", selectedViolationId);
    		this.setDescriptionEn(violationTypeView.getDescriptionEn());
    		this.setDescriptionAr(violationTypeView.getDescriptionAr());
    		renderLabelAndTextBox(true);
    		btnSave.setRendered(true);
    	}
    	catch (Exception e)
    	{
			logger.LogException("editViolationType|Error Occured", e);
		}
    }
    public void addViolationCategory()
    {
    	 errorMessages=new ArrayList<String>(0);
    try
    {
		if (!selectedCategory.equals("-1"))
		{
			lblDescEn.setRendered(true);
			lblDescAr.setRendered(true);
			txtDescEn.setRendered(true);
			txtDescAr.setRendered(true);
			btnSave.setRendered(true);
			descriptionEn="";
			descriptionAr="";
			viewRootMap.put("VIOLATION_TYPE_ID",-1L);
		}
		else 
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY ));
	}
	catch (Exception e) 
	{
		logger.LogException("addViolationCategory () crashed :", e);
	}
			
    }
    public void saveViolationType()
    {
    	successMessages=new ArrayList<String>(0);
	    errorMessages=new ArrayList<String>(0);
		try {
				
				if (!selectedCategory.equals("-1")) 
				{
					if(viewRootMap.get("VIOLATION_TYPE_ID")!=null)
						selectedViolationId = Convert.toLong(viewRootMap.get("VIOLATION_TYPE_ID"));
					if (selectedViolationId!=null && selectedViolationId.compareTo(-1L)==1 ) 
					{
						for (ViolationTypeView vtv : violationTypeForCategory)
						{
							if (viewRootMap.get("VIOLATION_TYPE_ID") != null)
							{
								if (vtv.getViolationTypeId().longValue() == selectedViolationId.longValue()) 
								{
									if(descriptionMessages())
										break;
									vtv.setDescriptionEn(this.descriptionEn);
									vtv.setDescriptionAr(this.descriptionAr);
									PropertyServiceAgent psa = new PropertyServiceAgent();
									vtv=psa.addViolationType(vtv);
									selectedViolationId=-1L;
									viewRootMap.put("VIOLATION_TYPE_ID",selectedViolationId);
									successMessages.add(getBundle().getString(	MessageConstants.CommonMessage.RECORD_UPDATED));
									renderLabelAndTextBox(false);
									btnSave.setRendered(false);
									break;
								}
							}
							
						}
					}
				  else
					{
						if ( descriptionEn!=null &&  descriptionAr!=null && descriptionEn.trim().length()>0 && descriptionAr.trim().length()>0 )
						{
							PropertyServiceAgent psa = new PropertyServiceAgent();
							ViolationTypeView violationTypeView = new ViolationTypeView();
							violationTypeView.setCreatedBy(getLoggedInUser());
							violationTypeView.setUpdatedBy(getLoggedInUser());
							violationTypeView.setCreatedOn(new Date());
							violationTypeView.setUpdatedOn(new Date());
							violationTypeView.setDescriptionEn(descriptionEn);
							violationTypeView.setDescriptionAr(descriptionAr);
							violationTypeView.setIsDeleted(0L);
							violationTypeView.setRecordStatus(1L);
							violationTypeView.setViolationTypeId(null);
							violationTypeView.setViolationCategory(this.getViolationCategoryView());
							violationTypeView=psa.addViolationType(violationTypeView);
							renderLabelAndTextBox(false);
							btnSave.setRendered(false);
							violationTypeForCategory.add(violationTypeView);
							successMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_VIOLATION_TYPE_ADDED));
						}
						else 
							descriptionMessages();
					}
				}
				else 
					errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY ));
				viewRootMap.put("VIOLATION_TYPE_LIST", violationTypeForCategory);
			}
		catch (Exception e) {
				logger.LogException(" saveViolationType() Crashed :", e);
				errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
    }
    public ResourceBundle getBundle() {

		 ResourceBundle bundle;
		             bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());
        return bundle;

  }
    public boolean descriptionMessages()
    {
    	if(descriptionEn==null || descriptionEn.trim().length()<=0)
    		{
    			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_VIOLATION_DESC_EN_REQ ));
    			if(descriptionAr==null || descriptionAr.trim().length()<=0)
        		{
        			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_VIOLATION_DESC_AR_REQ ));
        			return true;
        		}
    			return true;
			}
    	if(descriptionAr==null || descriptionAr.trim().length()<=0)
    		{
    			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_VIOLATION_DESC_AR_REQ ));
    			if(descriptionEn==null || descriptionEn.trim().length()<=0)
        		{
        			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.ViolationDetails.MSG_VIOLATION_DESC_EN_REQ ));
        			return true;
    			}
    			return true;
    		}
    	else 
    		return false;
    }
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    public void renderLabelAndTextBox(boolean value)
    {
    	lblDescEn.setRendered(value);
		lblDescAr.setRendered(value);
		txtDescEn.setRendered(value);
		txtDescAr.setRendered(value);
    }
    /**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {		 
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public int getRecordSize() {
		return violationTypeForCategory.size();
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public List<SelectItem> getViolationCategoryList() {
		return ViolationCategoryList;
	}
	public void setViolationCategoryList(List<SelectItem> violationCategoryList) {
		ViolationCategoryList = violationCategoryList;
	}
	public List<ViolationTypeView> getViolationTypeForCategory()
	{
		if(viewRootMap.get("VIOLATION_TYPE_LIST")!=null)
			violationTypeForCategory=(List<ViolationTypeView>) viewRootMap.get("VIOLATION_TYPE_LIST");
		return violationTypeForCategory;
	}
	public void setViolationTypeForCategory(
			List<ViolationTypeView> violationTypeForCategory) {
		this.violationTypeForCategory = violationTypeForCategory;
	}
	public String getSelectedCategory() {
		return selectedCategory;
	}
	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
	public String getDescriptionEn() {
		return descriptionEn;
	}
	public void setDescriptionEn(String descriptionEn)
	{
		this.descriptionEn=descriptionEn;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr=descriptionAr;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlOutputLabel getLblDescEn() {
		return lblDescEn;
	}
	public void setLblDescEn(HtmlOutputLabel lblDescEn) {
		this.lblDescEn = lblDescEn;
	}
	public HtmlOutputLabel getLblDescAr() {
		return lblDescAr;
	}
	public void setLblDescAr(HtmlOutputLabel lblDescAr) {
		this.lblDescAr = lblDescAr;
	}
	public HtmlInputText getTxtDescEn() {
		return txtDescEn;
	}
	public void setTxtDescEn(HtmlInputText txtDescEn) {
		this.txtDescEn = txtDescEn;
	}
	public HtmlInputText getTxtDescAr() {
		return txtDescAr;
	}
	public void setTxtDescAr(HtmlInputText txtDescAr) {
		this.txtDescAr = txtDescAr;
	}
	public HtmlCommandButton getBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(HtmlCommandButton btnAdd) {
		this.btnAdd = btnAdd;
	}
	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}
	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}
	public Long getSelectedViolationId() {
		return selectedViolationId;
	}
	public void setSelectedViolationId(Long selectedViolationId) {
		this.selectedViolationId = selectedViolationId;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages()	{
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public ViolationCategoryView getViolationCategoryView()
	{
		if(viewRootMap.get("VIOLATION_CATEGORY")!=null)
			violationCategoryView=(ViolationCategoryView) viewRootMap.get("VIOLATION_CATEGORY");
		return violationCategoryView;
	}
	public void setViolationCategoryView(ViolationCategoryView violationCategoryView) {
		this.violationCategoryView = violationCategoryView;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Boolean getRender() 
	{
		if(viewRootMap.get("IS_RENDER")!=null)
			render=Convert.toBoolean(viewRootMap.get("IS_RENDER"));
		return render;
	}
	public void setRender(boolean isRender) {
		viewRootMap.put("IS_RENDER",isRender);
		this.render=isRender;
	}
	public boolean isRender()
	{
		if(viewRootMap.get("IS_RENDER")!=null)
			render=Convert.toBoolean(viewRootMap.get("IS_RENDER"));
		return render;
	}
}