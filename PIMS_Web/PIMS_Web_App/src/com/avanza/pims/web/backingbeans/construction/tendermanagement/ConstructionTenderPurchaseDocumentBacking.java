package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.component.html.ext.HtmlCommandButton;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ConstructionTenderServiceAgent;
import com.avanza.pims.business.services.GRPCustomerServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.PaymentConfigurationManager;
import com.avanza.pims.entity.PaymentType;
import com.avanza.pims.entity.Person;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.GRPCustomersService;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TenderContractorView;
import com.avanza.pims.ws.vo.TenderDetailView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class ConstructionTenderPurchaseDocumentBacking extends AbstractController {	
	
	private static final long serialVersionUID = 7793998854649305357L;
	
	private Logger logger = Logger.getLogger(ConstructionTenderPurchaseDocumentBacking.class);
	private CommonUtil commonUtil = new CommonUtil();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();

	private Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	public HtmlTabPanel tabPanel = new HtmlTabPanel();
	public HtmlTab purchaseRequestTab = new HtmlTab();
	public HtmlInputText txtVendorName = new HtmlInputText();
	public HtmlSelectOneMenu ddnVendorType = new HtmlSelectOneMenu();
	public HtmlInputText txtTenderNumber = new HtmlInputText();
	public HtmlInputText txtTenderDescription = new HtmlInputText();
	public HtmlCalendar calPaymentDate = new HtmlCalendar();
	public HtmlInputText txtTenderPrice = new HtmlInputText();
	public HtmlCommandButton btnCollect = new HtmlCommandButton();
	public HtmlCommandButton btnCancel = new HtmlCommandButton();
	public HtmlCommandButton btnSave = new HtmlCommandButton();
	public HtmlCommandButton btnAddNew = new HtmlCommandButton();
	
	
	
	private ConstructionTenderServiceAgent constructionTenderServiceAgent = new ConstructionTenderServiceAgent();
	private ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private ProjectServiceAgent projectServiceAgent ;
	private PropertyServiceAgent propertyServiceAgent ;
	private ProjectView projectView;
	private List<PaymentScheduleView> paymentScheduleViewList;
	HashMap conditionsMap;
	PaymentConfigurationManager paymentConfigurationManager;
	PaymentType paymentType;
	private HtmlDataTable dataTablePaymentSchedule;
	private boolean isArabicLocale;
	private boolean isEnglishLocale;
	private String pageMode = WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_NEW;
	private Long constructionTenderId = null;
	private TenderDetailView tenderDetailView = new TenderDetailView();
	private ContractorView contractorView = new ContractorView() ;
	private RequestView requestView ;
	private PaymentScheduleView paymentScheduleView;
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;
	
	
	public ConstructionTenderPurchaseDocumentBacking() 
	{
		requestView = new RequestView();
		projectServiceAgent = new ProjectServiceAgent();
		propertyServiceAgent = new PropertyServiceAgent();
		projectView = new ProjectView();
		paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
		conditionsMap  = new HashMap();
		paymentConfigurationManager = new PaymentConfigurationManager();
		paymentType = new PaymentType();
		paymentScheduleView = new PaymentScheduleView();
		
		isArabicLocale = false;
		isEnglishLocale = false;
	}
	
	public void init() {
		String METHOD_NAME = "init()";
		logger.logInfo(METHOD_NAME + " --- started --- ");
		super.init();
		if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
	     		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
	        )
	        {
			   viewMap.put( WebConstants.Tender.PAYMENT_SCHEDULE_LIST,(List<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	     	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	     	 
	        }   
		if(!isPostBack())
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);	

		try	{			
			updateValuesFromMap();
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);

			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
		
	public void preprocess() {
		super.preprocess();
	}
	
	public void prerender() {		
		super.prerender();	
		if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
		{
			boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
			sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
			if(isPaymentCollected)		       
				CollectPayment();			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void CollectPayment() {
		String methodName="CollectPayment"; 
		logger.logInfo(methodName + "|" + "Start...");
        infoMessages = new ArrayList<String>(0);
        try
		{
        	PropertyServiceAgent psa = new PropertyServiceAgent();
        	GRPCustomersService grpCustomerService = new GRPCustomersService ();
        	// Adding the actual payment now
        	if ( sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW) != null )
        	{
	        	PaymentReceiptView prv = (PaymentReceiptView) sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			    sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			    if(contractorView!=null && contractorView.getPersonId()!=null )
				{ 
			     prv.setPaidById( contractorView.getPersonId() );	
			     String address = (projectView.getPropertyView().getAddressLineOne()!=null ?projectView.getPropertyView().getAddressLineOne():projectView.getPropertyView().getCommercialName());
			     
			     grpCustomerService.sendTenderPurchasePersonLocationToGRP( requestView.getRequestId(),contractorView.getPersonId(), address ,WebConstants.COST_CENTER_PURCHASE_DOC );
				}
			    prv=psa.collectPayments(prv);
			    
			    CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			    CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.TENDER_PURCHASE_DOC);
			    
        	}
				infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.PurchaseTenderDocs.MSG_TENDER_PURCHASED_SUCCESSFULLY));
				//infoMessages.add("Purchase Tender Document Request Saved Successfully");
				btnCollect.setRendered(false);
				btnSave.setRendered(false);
				//for changing request status
				requestView.setStatusId(getConstructionTenderRequestStatusAcceptedCollected());
				new RequestServiceAgent().addRequest(requestView);
				loadScreenData();
		    logger.logInfo(methodName + "|" + "Finish...");
		}
        catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured..."+ex);

			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	
	private void updateValuesFromMap() throws Exception {		
		// Page Mode
		if ( viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
		}
		else if ( sessionMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
			sessionMap.remove( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );			
		}
		
		// Construction Tender Id
		if ( viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID ) != null )
		{
			constructionTenderId = (Long) viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
		}
		else if ( sessionMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID ) != null )
		{
			constructionTenderId = (Long) sessionMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
			sessionMap.remove( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
		}
		
		// Tender Detail View
		if ( viewMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW ) != null )
		{
			tenderDetailView = (TenderDetailView) viewMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW );
		}
		else if ( constructionTenderId != null )
		{
			tenderDetailView = constructionServiceAgent.getTenderDetailViewById( constructionTenderId );
			tenderDetailView.setTenderPresentingDate( new Date() );
			CommonUtil.loadAttachmentsAndComments(constructionTenderId.toString());
			
		}
		
		// Project View
		if ( viewMap.get( WebConstants.Tender.PROJECT_DETAIL_VIEW ) != null )
		{
			projectView = (ProjectView) viewMap.get( WebConstants.Tender.PROJECT_DETAIL_VIEW );
		}
		else if ( tenderDetailView != null && tenderDetailView.getProjectId()!=null )
		{
			projectView = projectServiceAgent.getProjectById(tenderDetailView.getProjectId());
			
		}
		
		//Load List of Payment Schedules
		DomainDataView ddvOwner = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.OWNER_TYPE), "AMAF");
		if ( viewMap.get( WebConstants.Tender.PAYMENT_SCHEDULE_LIST ) != null )
		{
			paymentScheduleViewList = (List<PaymentScheduleView>) viewMap.get( WebConstants.Tender.PAYMENT_SCHEDULE_LIST );
		}
		else if ( projectView != null && projectView.getPropertyView()!=null&& ddvOwner.getDomainDataId() != null )
		{
			paymentType = paymentConfigurationManager.getPaymentTypeById(WebConstants.PAYMENT_TYPE_PURCHASE_TENDER_DOC);
			paymentScheduleViewList = propertyServiceAgent.getPaymentScheduleFromPaymentConfiguration(WebConstants.PROCEDURE_TYPE_TENDERING_CONSTRUCTION_CONTRACT , 
					                                                                                  paymentType.getPaymentTypeId(), conditionsMap, null, 
					                                                                                  CommonUtil.getIsEnglishLocale(),
					                                                                                  ddvOwner.getDomainDataId());
			
			if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0)
			{
				paymentScheduleViewList = updatedPaymentScheduleList();
			}
			
			paymentScheduleView =  tenderPurchasePaymentSchedule();
			paymentScheduleViewList.add(paymentScheduleView);
			btnCollect.setRendered(false);
		}
		
		
		// Contractor View
		if ( viewMap.get( WebConstants.ConstructionTender.CONTRACTOR_VIEW ) != null )
		{
			contractorView = (ContractorView) viewMap.get( WebConstants.ConstructionTender.CONTRACTOR_VIEW );
		}
		
		// Request View
		if ( viewMap.get( WebConstants.Tender.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.Tender.REQUEST_VIEW );
			//loadScreenData();
		}
		// Comming Form Request Search 
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().containsKey(WebConstants.REQUEST_VIEW))
		{
			requestView  = (RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().remove(WebConstants.REQUEST_VIEW);
			loadScreenData();
		}
		
		changePageMode(pageMode);
	}
	
	private void changePageMode( String pageMode ) throws Exception {
		this.pageMode = pageMode;
		updateValuesToMap();
		updatePageForMode(this.pageMode);
	}
	
	private void updateValuesToMap() {		
		// Page Mode
		if ( pageMode != null )
			viewMap.put( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, pageMode );
		
		// Construction Tender Id
		if ( constructionTenderId != null )
			viewMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, constructionTenderId );
		
		// Tender Detail View
		if ( tenderDetailView != null )
			viewMap.put( WebConstants.Tender.TENDER_DETAIL_VIEW, tenderDetailView );
		
		// Project View
		if ( projectView != null )
			viewMap.put( WebConstants.Tender.PROJECT_DETAIL_VIEW, projectView );
		
		if(paymentScheduleViewList !=null)
			viewMap.put( WebConstants.Tender.PAYMENT_SCHEDULE_LIST, paymentScheduleViewList );
		
		// Contractor View
		if ( contractorView != null )
			viewMap.put( WebConstants.ConstructionTender.CONTRACTOR_VIEW, contractorView );
		
		// Request VIew
		if ( requestView != null )
			viewMap.put( WebConstants.Tender.REQUEST_VIEW, requestView );
		
		
	}
	
	private void openPopup(String javaScriptText) throws Exception 
	{
		logger.logInfo("openPopup() started...");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
	}
	
	private List<PaymentScheduleView> updatedPaymentScheduleList()
	{
		List<PaymentScheduleView> tempPaymentScheduleViewList = new ArrayList<PaymentScheduleView>();
		
		for(PaymentScheduleView paymentScheduleView :paymentScheduleViewList)
		{
			DomainDataView ddvMode= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE), WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			DomainDataView ddvStatus = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);			

			paymentScheduleView.setPaymentModeId( ddvMode.getDomainDataId() );
			paymentScheduleView.setPaymentModeEn( ddvMode.getDataDescEn() );
			paymentScheduleView.setPaymentModeAr( ddvMode.getDataDescAr() );

			paymentScheduleView.setStatusId( ddvStatus.getDomainDataId() );
			paymentScheduleView.setStatusEn( ddvStatus.getDataDescEn() );
			paymentScheduleView.setStatusAr( ddvStatus.getDataDescAr() );
			
			tempPaymentScheduleViewList.add(paymentScheduleView);

		}
		
		
		return tempPaymentScheduleViewList;
	}
	
	private PaymentScheduleView tenderPurchasePaymentSchedule()  throws Exception{		
		String METHOD_NAME = "tenderPurchasePaymentSchedule()";
		String path = null;
		PropertyServiceAgent psa = new PropertyServiceAgent();	
		RequestServiceAgent rsa = new RequestServiceAgent();	
		RequestView requestView = new RequestView();
		TenderView tenderView = new TenderView();
		PersonView applicantView = new PersonView();
		PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
		long requestId = 0L;
		
		logger.logInfo(METHOD_NAME + " --- started --- ");
				DomainDataView ddvMode= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE), WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
				DomainDataView ddvStatus = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);			
				DomainDataView ddvOwner = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.OWNER_TYPE), "AMAF");			
				PaymentConfigurationManager paymentConfigurationManager = new PaymentConfigurationManager();
				PaymentType paymentType = paymentConfigurationManager.getPaymentTypeById(WebConstants.PAYMENT_TYPE_PURCHASE_TENDER_DOC );
				
				
				//logger.logInfo(METHOD_NAME + " --- setting payment amount--- ");
				paymentScheduleView.setAmount( tenderDetailView.getTenderPrice() );
				paymentScheduleView.setCreatedBy( CommonUtil.getLoggedInUser() );
				paymentScheduleView.setDescription("Tender Price");
				//logger.logInfo(METHOD_NAME + " --- setting payment mode --- ");
				paymentScheduleView.setPaymentModeId( ddvMode.getDomainDataId() );
				paymentScheduleView.setPaymentModeEn( ddvMode.getDataDescEn() );
				paymentScheduleView.setPaymentModeAr( ddvMode.getDataDescAr() );
				//logger.logInfo(METHOD_NAME + " --- setting payment status--- ");
				paymentScheduleView.setStatusId( ddvStatus.getDomainDataId() );
				paymentScheduleView.setStatusEn( ddvStatus.getDataDescEn() );
				paymentScheduleView.setStatusAr( ddvStatus.getDataDescAr() );
				//logger.logInfo(METHOD_NAME + " --- setting payment type--- ");
				paymentScheduleView.setTypeId( paymentType.getPaymentTypeId() );
				paymentScheduleView.setTypeEn( paymentType.getDescriptionEn() );
				paymentScheduleView.setTypeAr( paymentType.getDescriptionAr() );
				paymentScheduleView.setIsReceived("N");
				paymentScheduleView.setRowId(paymentScheduleView.hashCode());
				logger.logInfo(METHOD_NAME + " --- setting tender presenting date--- ");
				paymentScheduleView.setPaymentDueOn( tenderDetailView.getTenderPresentingDate() );
				logger.logInfo(METHOD_NAME + " --- setting selected to true--- ");
				paymentScheduleView.setSelected(true);
				logger.logInfo(METHOD_NAME + " --- setting owner ship type--- ");
				paymentScheduleView.setOwnerShipTypeId( ddvOwner.getDomainDataId() );			
				logger.logInfo(METHOD_NAME + " --- setting grp number--- ");
				
						
				logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			
		
		return paymentScheduleView;
	}
	
	public void loadScreenData() throws Exception
	{
		
		requestView = loadRequest(requestView.getRequestId());
		
		tenderDetailView = constructionServiceAgent.getTenderDetailViewById( requestView.getTenderView().getTenderId() );
		tenderDetailView.setTenderPresentingDate( requestView.getRequestDate() );
		CommonUtil.loadAttachmentsAndComments(tenderDetailView.getTenderId().toString());
		
		projectView = projectServiceAgent.getProjectById(tenderDetailView.getProjectId());
		paymentScheduleViewList = (List<PaymentScheduleView>) propertyServiceAgent.getPaymentScheduleByRequestID(requestView.getRequestId());
		contractorView = new ConstructionServiceAgent().getContractorByIdForExtendRequest(requestView.getApplicantView().getPersonId());
		
		if(paymentScheduleViewList!=null && paymentScheduleViewList.size()>0)
		{
			paymentScheduleView =  paymentScheduleViewList.get(0);
			DomainDataView ddvStatusCollected = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_COLLECTED);
			DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	                WebConstants.REALIZED);
			if( paymentScheduleView.getStatusId().equals(ddvStatusCollected.getDomainDataId()) ||
				paymentScheduleView.getStatusId().equals(ddvRealized.getDomainDataId())
			  )
			{
				btnCollect.setRendered(false);
				btnSave.setRendered(false);
			}
		}
		
		updateValuesToMap();
		
	}
	
	public void btnEditPaymentSchedule_Click()
	{
		String methodName = "btnEditPaymentSchedule_Click";
		try
		{
		logger.logInfo(methodName+"|"+" Start...");
		PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();

		sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);
		openPopup("javascript:openPopupEditPaymentSchedule();");


		logger.logInfo(methodName+"|"+" Finish...");
		}
		catch (Exception e )
		{
			logger.LogException(methodName + " --- crashed --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.TENDER_PURCHASE_DOC);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	public String onCollect() {		
		String METHOD_NAME = "onCollect()";
		String path = null;
		PropertyServiceAgent psa = new PropertyServiceAgent();	
		RequestServiceAgent rsa = new RequestServiceAgent();	
		RequestView requestView = new RequestView();
		TenderView tenderView = new TenderView();
		PersonView applicantView = new PersonView();
		long requestId = 0L;
		
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
		
			if(paymentScheduleViewList!=null && paymentScheduleViewList.size()>0)
			{
				
				PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
							
				logger.logInfo(METHOD_NAME + " --- putting values in session--- ");
				sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentScheduleViewList);
				sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
				
				openPopup("javascript:openPopupReceivePayment();");			
	
						
				logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			}
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
		return path;
	}
	
	public String onAddNew()
	{
		
			String path = null;		
			String METHOD_NAME="onAddNew";
			path = "assetClassManage";
			try {
				
			 if ( viewMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW ) != null )
			 {
				sessionMap.put( WebConstants.Tender.TENDER_DETAIL_VIEW_SESSION,viewMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW ) );
			 }	
				
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
			response.sendRedirect("constructionTenderPurchaseDocument.jsf");
			
			 if ( sessionMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW_SESSION ) != null )
			 {
				viewMap.put( WebConstants.Tender.TENDER_DETAIL_VIEW,sessionMap.get( WebConstants.Tender.TENDER_DETAIL_VIEW_SESSION) );
				sessionMap.remove( WebConstants.Tender.TENDER_DETAIL_VIEW_SESSION );
				updateValuesFromMap();
			 }
			
			} catch (Exception e) {
				logger.LogException(METHOD_NAME + " --- crashed --- ", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			
			return "";
		
	}
	
	
	public String onSave() {		
		String METHOD_NAME = "onSave()";
		String path = null;
			
		RequestServiceAgent rsa = new RequestServiceAgent();	
		TenderView tenderView = new TenderView();
		PersonView applicantView = new PersonView();
		long requestId = 0L;
		Boolean isUpdate = false;
		
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
		
			if(contractorView!=null && contractorView.getPersonId()!=null)
			{
	
					tenderView.setTenderId(tenderDetailView.getTenderId());
					applicantView.setPersonId(contractorView.getPersonId());
					requestView.setTenderView(tenderView);
					requestView.setApplicantView(applicantView);
					requestView.setStatusId(getConstructionTenderRequestStatusNew());
					requestView.setRequestTypeId(getTenderPurchaseRequestType());
				
				// when request Update
				if(requestView.getRequestId()!=null)
				{
					requestView.setUpdatedBy(CommonUtil.getLoggedInUser());
					requestView.setUpdatedOn(new Date());
					isUpdate = true;
				}
				//when request Add
				else
				{
					requestView.setCreatedBy(CommonUtil.getLoggedInUser());
					requestView.setCreatedOn(new Date());
					requestView.setUpdatedBy(CommonUtil.getLoggedInUser());
					requestView.setUpdatedOn(new Date());
					requestView.setRecordStatus(new Long(1));
					requestView.setIsDeleted(new Long(0));
					requestView.setRequestDate(new Date());
				}
				
					
	            // adding request in DB  
				requestId = rsa.addRequest(requestView);
				// load full request View 
				requestView = loadRequest(requestId);
				// adding PaymentSchedule in DB
				addPaymentSchedule();
				//load list of paymentSchedule
				paymentScheduleViewList = loadPaymentSchedule();
				
				btnCollect.setRendered(true);
				loadScreenData();
				
				if(isUpdate)
				infoMessages.add(CommonUtil.getBundleMessage("purchaseTender.msg.requestUpdated"));
				else
				infoMessages.add(CommonUtil.getBundleMessage("purchaseTender.msg.requestCreated"));
				
				CommonUtil.printReport(requestView.getRequestId());
				
						
				logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			}else{
				errorMessages.add(CommonUtil.getBundleMessage("purchaseTender.msg.contractorSelected"));
			}
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
		return path;
	}
	
	
	public String addPaymentSchedule() {		
		String METHOD_NAME = "addPaymentSchedule()";
		String path = null;
		PaymentConfigurationManager paymentConfigurationManager = new PaymentConfigurationManager();	
		
		
		
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
		
			if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0 )
			{
							
				for(PaymentScheduleView paymentScheduleView:paymentScheduleViewList)
				{
					// For update paymentSchedule 
					if(paymentScheduleView!=null && paymentScheduleView.getPaymentScheduleId()!=null)
					{
						paymentScheduleView.setUpdatedBy(CommonUtil.getLoggedInUser());
						paymentScheduleView.setUpdatedOn(new Date());
						
					}// For Add paymentSchedule 
					else
					{
						DomainDataView ddvOwner = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.OWNER_TYPE), "AMAF");
						paymentScheduleView.setCreatedBy( CommonUtil.getLoggedInUser() );
						paymentScheduleView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
						paymentScheduleView.setIsReceived("N");
						paymentScheduleView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
						paymentScheduleView.setCreatedOn( new Date() );
						paymentScheduleView.setUpdatedOn( new Date() );
						paymentScheduleView.setUpdatedBy( CommonUtil.getLoggedInUser() );
						paymentScheduleView.setSelected(true);
						paymentScheduleView.setOwnerShipTypeId( ddvOwner.getDomainDataId()  );			
						paymentScheduleView.setGrpAccountNumber( contractorView.getGrpNumber() );
						paymentScheduleView.setRequestId(requestView.getRequestId());
					}
					
					logger.logInfo(METHOD_NAME + " --- adding payment schedule to db--- ");
		        	propertyServiceAgent.addPaymentScheduleByPaymentSchedule( paymentScheduleView );			
		        	logger.logInfo(METHOD_NAME + " --- adding in the list--- ");
	        	
				}
	        			
	
						
				logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			}
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		return path;
	}
	
	
	private RequestView loadRequest (Long requestId)
	{
	 
		String METHOD_NAME="loadRequest";
	 try {
		    	
			requestView.setRequestId(requestId);
			List<RequestView> requestViewList = propertyServiceAgent.getAllRequests(requestView, null, null, null);
			requestView = requestViewList.get(0);
		
		} catch (PimsBusinessException e) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		return requestView;
	}
	
	private List<PaymentScheduleView> loadPaymentSchedule ()throws Exception
	{
	 
	 		paymentScheduleViewList = propertyServiceAgent.getPaymentScheduleByRequestID(requestView.getRequestId());
		
		
		return paymentScheduleViewList;
	}
	
	private Long getConstructionTenderRequestStatusNew() throws PimsBusinessException {
		Long domainDataId = null;
		String  METHOD_NAME = "getConstructionTenderRequestStatus|";		
		logger.logInfo(METHOD_NAME+"started...");		
		
		DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);  
	    domainDataId = ddv.getDomainDataId();
		
		return domainDataId;
	}
	
	private Long getConstructionTenderRequestStatusAcceptedCollected() throws PimsBusinessException {
		Long domainDataId = null;
		String  METHOD_NAME = "getConstructionTenderRequestStatus|";		
		logger.logInfo(METHOD_NAME+"started...");		
		
		DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_ACCEPTED_COLLECTED);  
	    domainDataId = ddv.getDomainDataId();
		
		return domainDataId;
	}
	
	private Long getTenderPurchaseRequestType() throws PimsBusinessException {
		Long nolReqTypeId = null;
		String  METHOD_NAME = "getNOlRequestType|";		
		logger.logInfo(METHOD_NAME+"started...");
		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
		RequestTypeView requestTypeView = requestServiceAgent.getRequestType(WebConstants.REQUEST_TYPE_TENDER_PURCHACE_REQUEST);


		nolReqTypeId= requestTypeView.getRequestTypeId();
		logger.logInfo(METHOD_NAME+"ended...");
		return nolReqTypeId;
	}
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	public String getDateFormatForDataTable()
	{
		return CommonUtil.getDateFormat();
	}
	
	public String onCancel() {		
		String METHOD_NAME = "onCancel()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
			path = "constructionTenderSearch";
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		return path;
	}
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}

	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public Long getConstructionTenderId() {
		return constructionTenderId;
	}

	public void setConstructionTenderId(Long constructionTenderId) {
		this.constructionTenderId = constructionTenderId;
	}

	public HtmlCommandButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(HtmlCommandButton btnCancel) {
		this.btnCancel = btnCancel;
	}		

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}
	
	public HtmlTab getPurchaseRequestTab() {
		return purchaseRequestTab;
	}

	public void setPurchaseRequestTab(HtmlTab purchaseRequestTab) {
		this.purchaseRequestTab = purchaseRequestTab;
	}

	public HtmlInputText getTxtVendorName() {
		return txtVendorName;
	}

	public void setTxtVendorName(HtmlInputText txtVendorName) {
		this.txtVendorName = txtVendorName;
	}

	public HtmlSelectOneMenu getDdnVendorType() {
		return ddnVendorType;
	}

	public void setDdnVendorType(HtmlSelectOneMenu ddnVendorType) {
		this.ddnVendorType = ddnVendorType;
	}

	public HtmlInputText getTxtTenderNumber() {
		return txtTenderNumber;
	}

	public void setTxtTenderNumber(HtmlInputText txtTenderNumber) {
		this.txtTenderNumber = txtTenderNumber;
	}

	public HtmlInputText getTxtTenderDescription() {
		return txtTenderDescription;
	}

	public void setTxtTenderDescription(HtmlInputText txtTenderDescription) {
		this.txtTenderDescription = txtTenderDescription;
	}

	public HtmlCalendar getCalPaymentDate() {
		return calPaymentDate;
	}

	public void setCalPaymentDate(HtmlCalendar calPaymentDate) {
		this.calPaymentDate = calPaymentDate;
	}

	public HtmlInputText getTxtTenderPrice() {
		return txtTenderPrice;
	}

	public void setTxtTenderPrice(HtmlInputText txtTenderPrice) {
		this.txtTenderPrice = txtTenderPrice;
	}

	public HtmlCommandButton getBtnCollect() {
		return btnCollect;
	}

	public void setBtnCollect(HtmlCommandButton btnCollect) {
		this.btnCollect = btnCollect;
	}

	public TenderDetailView getTenderDetailView() {
		return tenderDetailView;
	}

	public void setTenderDetailView(TenderDetailView tenderDetailView) {
		this.tenderDetailView = tenderDetailView;
	}

	public ConstructionServiceAgent getConstructionServiceAgent() {
		return constructionServiceAgent;
	}

	public void setConstructionServiceAgent(
			ConstructionServiceAgent constructionServiceAgent) {
		this.constructionServiceAgent = constructionServiceAgent;
	}

	public ContractorView getContractorView() {
		return contractorView;
	}

	public void setContractorView(ContractorView contractorView) {
		this.contractorView = contractorView;
	}

	public ConstructionTenderServiceAgent getConstructionTenderServiceAgent() {
		return constructionTenderServiceAgent;
	}

	public void setConstructionTenderServiceAgent(
			ConstructionTenderServiceAgent constructionTenderServiceAgent) {
		this.constructionTenderServiceAgent = constructionTenderServiceAgent;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public ProjectServiceAgent getProjectServiceAgent() {
		return projectServiceAgent;
	}

	public void setProjectServiceAgent(ProjectServiceAgent projectServiceAgent) {
		this.projectServiceAgent = projectServiceAgent;
	}

	public ProjectView getProjectView() {
		return projectView;
	}

	public void setProjectView(ProjectView projectView) {
		this.projectView = projectView;
	}

	public List<PaymentScheduleView> getPaymentScheduleViewList() {
		return paymentScheduleViewList;
	}

	public void setPaymentScheduleViewList(
			List<PaymentScheduleView> paymentScheduleViewList) {
		this.paymentScheduleViewList = paymentScheduleViewList;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public PaymentConfigurationManager getPaymentConfigurationManager() {
		return paymentConfigurationManager;
	}

	public void setPaymentConfigurationManager(
			PaymentConfigurationManager paymentConfigurationManager) {
		this.paymentConfigurationManager = paymentConfigurationManager;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentScheduleView getPaymentScheduleView() {
		return paymentScheduleView;
	}

	public void setPaymentScheduleView(PaymentScheduleView paymentScheduleView) {
		this.paymentScheduleView = paymentScheduleView;
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public HtmlCommandButton getBtnAddNew() {
		return btnAddNew;
	}

	public void setBtnAddNew(HtmlCommandButton btnAddNew) {
		this.btnAddNew = btnAddNew;
	}
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click() {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }

}

