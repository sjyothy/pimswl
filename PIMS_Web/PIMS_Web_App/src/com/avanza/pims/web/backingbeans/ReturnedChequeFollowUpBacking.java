package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.FollowUpService;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FollowUpProgressView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class ReturnedChequeFollowUpBacking extends AbstractController {
	
	private static final long serialVersionUID = 5677934797520360701L;
	private final String USER_GROUPS = "USER_GROUPS";
	private final String ALLOWED_USER_GROUP_FOR_OPEN = "ALLOWED_USER_GROUP_FOR_OPEN";
	private final String ALLOWED_USER_GROUP_FOR_LEGAL = "ALLOWED_USER_GROUP_FOR_LEGAL";
	private final String IS_ALLOWED = "IS_ALLOWED";
	// Commons
	private Logger logger = Logger.getLogger(ReturnedChequeFollowUpBacking.class);
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	private Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private Map requestMap = getFacesContext().getExternalContext().getRequestMap();
	private CommonUtil commonUtil = new CommonUtil();
	private PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private FollowUpServiceAgent followUpServiceAgent = new FollowUpServiceAgent();
	
	// Follow Up View
	FollowUpView followUpView = new FollowUpView();
	
	// Contract Details Tab
	private ContractView contractView = new ContractView();
	private String txtpropertyName = "";
	private String txtpropertyType = "";
	private String txtTenantName = "";
	private String txtunitRefNum = "";
	private String txtUnitRentValue = "";
	private String txtUnitType = "";
	private String txtTenantId = "";
	private String stopReplaceCheque="0";
	// Court Results Tab
	private List<FollowUpProgressView> courtResultsList = new ArrayList<FollowUpProgressView>(0);
	
	// Actions History Tab
	private List<FollowUpProgressView> actionsHistoryList = new ArrayList<FollowUpProgressView>(0);
	
	
	private List<DomainDataView> ddfollowUpStatus = new ArrayList<DomainDataView>(0);
	private DomainDataView ddfollowUpStatusClose = new DomainDataView();
	private DomainDataView ddfollowUpStatusForlegalDeptt = new DomainDataView();
	private DomainDataView ddfollowUpStatusOpen = new DomainDataView();
	private Boolean isFollowUpClosed=false ;
	String notesOwner = WebConstants.FollowUp.NOTES_OWNER_FOLLOW_UP;
	private String PROCEDURE_TYPE="procedureType";
	private String EXTERNAL_ID ="externalId";
	private boolean isShowBtnSendToLegal;
	private boolean isShowBtnSave;
	private boolean isShowBtnFollowup;
	@Override
	public void init() {
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- started --- ");
			super.init();
			updateValuesFromMap();
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
	}
	
	@Override
	public void preprocess() {		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
		canAddAttachmentNotes(true);
		if(followUpView!=null && followUpView.getStatusId()!=null && 
				followUpView.getStatusId().compareTo(ddfollowUpStatusClose.getDomainDataId())==0)
		{
			canAddAttachmentNotes(false);
			this.setIsFollowUpClosed(true);
			this.getIsFollowUpClosed();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void canAddAttachmentNotes(boolean canAdd)
	{
		viewMap.put("canAddAttachment", canAdd); 
	    viewMap.put("canAddNote", canAdd);
	    
		
	}    
	public Boolean getIsFollowUpClosed()
	{
		return isFollowUpClosed;
		
	}
	public void setIsFollowUpClosed(Boolean isFollowUpClosed)
	{
		this.isFollowUpClosed = isFollowUpClosed;
		
		
	}
	@SuppressWarnings("unchecked")
	private void updateValuesFromMap() throws Exception {
		
		// Follow Up View 
		if ( sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) != null )
		{
			followUpView = (FollowUpView) sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
			if(followUpView.getPaymentReceiptDetailView() !=null && followUpView.getPaymentReceiptDetailView().getPaymentReceiptDetailId()!=null)
				viewMap.put("PAYMENT_RECEIPT_DETAIL_ID", followUpView.getPaymentReceiptDetailView().getPaymentReceiptDetailId());
			if(followUpView.getPaymentReceiptDetailView().getStopReplaceCheque()!=null)
			this.setStopReplaceCheque(followUpView.getPaymentReceiptDetailView().getStopReplaceCheque().toString());
		}
		else if ( viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) != null )
		{
			followUpView = (FollowUpView) viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW);
		}
		if(!isPostBack())
		{
			viewMap.put(PROCEDURE_TYPE,WebConstants.FollowUp.PROCEDURE_TYPE_FOLLOW_UP);
	    	viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_FOLLOW_UP);
	    	loadAttachmentsAndComments(followUpView.getFollowUpId());
			ddfollowUpStatus  = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS);
			viewMap.put(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS, ddfollowUpStatus );
			ddfollowUpStatusClose = (DomainDataView)CommonUtil.getIdFromType(ddfollowUpStatus , WebConstants.FollowUpStatus.CLOSE);
			viewMap.put(WebConstants.FollowUpStatus.CLOSE, ddfollowUpStatusClose );
			
			ddfollowUpStatusForlegalDeptt = (DomainDataView)CommonUtil.getIdFromType(ddfollowUpStatus , WebConstants.FollowUpStatus.FOR_LEGAL_DEPTT);
			viewMap.put(WebConstants.FollowUpStatus.FOR_LEGAL_DEPTT, ddfollowUpStatusForlegalDeptt );
			
			ddfollowUpStatusOpen = (DomainDataView)CommonUtil.getIdFromType(ddfollowUpStatus , WebConstants.FollowUpStatus.OPEN);
			viewMap.put(WebConstants.FollowUpStatus.OPEN, ddfollowUpStatusOpen );
			
			
			User user = SecurityManager.getUser(CommonUtil.getLoggedInUser());
			List<UserGroup> userGroups = user.getUserGroups();
			
			if(userGroups != null && userGroups.size() > 0){
				viewMap.put(USER_GROUPS, userGroups);
			}
			PropertyServiceAgent psa = new PropertyServiceAgent();
			String allowedGroupsCSVForOpen = psa.getSystemConfigurationValueByKey(WebConstants.FOLLOW_UP_GROUP_FOR_OPEN_STATUS);
			
			if(allowedGroupsCSVForOpen != null && allowedGroupsCSVForOpen.length() > 0){
				viewMap.put(ALLOWED_USER_GROUP_FOR_OPEN, allowedGroupsCSVForOpen);
			}
			
			String allowedGroupsCSVForLegal = psa.getSystemConfigurationValueByKey(WebConstants.FOLLOW_UP_GROUP_FOR_LEGAL_STATUS);
			
			if(allowedGroupsCSVForLegal != null && allowedGroupsCSVForLegal.length() > 0){
				viewMap.put(ALLOWED_USER_GROUP_FOR_LEGAL, allowedGroupsCSVForLegal);
			}
			
//			String allowedGroup[] = allowedGroupsCSV.split(",") ;
//			boolean isAllowed = false;
//			if(userGroups != null && userGroups.size() > 0 && allowedGroup.length > 0){
//				
//			Assgined : for(UserGroup assignedGroup : userGroups ){
//						for(String allowed : allowedGroup ){
//						if(assignedGroup.getUserGroupId().compareTo(allowed) == 0){
//							isAllowed = true;
//							break Assgined;
//						}
//					}
//				}
//			}
//			viewMap.put("IS_ALLOWED", isAllowed);
		}
		else
		{
			ddfollowUpStatus = (ArrayList<DomainDataView>)viewMap.get(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS);
			ddfollowUpStatusClose =(DomainDataView)viewMap.get(WebConstants.FollowUpStatus.CLOSE);
			ddfollowUpStatusForlegalDeptt =(DomainDataView)viewMap.get(WebConstants.FollowUpStatus.FOR_LEGAL_DEPTT);
			ddfollowUpStatusOpen = (DomainDataView)viewMap.get(WebConstants.FollowUpStatus.OPEN);
				
		}	
		// Contract View		
		if ( viewMap.get(WebConstants.Contract.CONTRACT_VIEW) != null )
		{
			contractView = (ContractView) viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
			setOtherContractRelatedValues();
		}
		else
		{
			if(followUpView.getContractView()!= null && followUpView.getContractView().getContractId()!=null)
			getContractById( String.valueOf( followUpView.getContractView().getContractId() ) );
		}
		
		// Court Result List 		
		if ( sessionMap.get(WebConstants.FollowUp.RE_FETCH) != null ) 
		{
			DomainDataView domainDataView = CommonUtil.getIdFromType(
					                         CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
					                         WebConstants.FollowUp.DOMAIN_DATA_COURT_RESULT);
			courtResultsList = followUpServiceAgent.getFollowUpProgressList( new FollowUpProgressView(null, followUpView.getFollowUpId(), domainDataView.getDomainDataId(), Constant.DEFAULT_IS_DELETED) );			
		}
		else if ( viewMap.get(WebConstants.FollowUp.COURT_RESULT_LIST) != null )
		{
			courtResultsList = (List<FollowUpProgressView>) viewMap.get(WebConstants.FollowUp.COURT_RESULT_LIST);
		}
		else
		{			
			DomainDataView domainDataView = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
					                                 WebConstants.FollowUp.DOMAIN_DATA_COURT_RESULT);
			courtResultsList = followUpServiceAgent.getFollowUpProgressList( new FollowUpProgressView(null, followUpView.getFollowUpId(), domainDataView.getDomainDataId(), Constant.DEFAULT_IS_DELETED) );
		}
		
		// Actions History List
		if ( sessionMap.get(WebConstants.FollowUp.RE_FETCH) != null )
		{
			DomainDataView domainDataView = CommonUtil.getIdFromType(
					                                CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
					                                WebConstants.FollowUp.DOMAIN_DATA_ACTIONS_HISTORY);
			actionsHistoryList = followUpServiceAgent.getFollowUpProgressList( new FollowUpProgressView(null, followUpView.getFollowUpId(), domainDataView.getDomainDataId(), Constant.DEFAULT_IS_DELETED) );
		}
		else if ( viewMap.get(WebConstants.FollowUp.ACTION_HISTORY_LIST) != null )
		{
			actionsHistoryList = (List<FollowUpProgressView>) viewMap.get(WebConstants.FollowUp.ACTION_HISTORY_LIST);
		}
		else
		{
			DomainDataView domainDataView = CommonUtil.getIdFromType(
					                                 CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
					                                 WebConstants.FollowUp.DOMAIN_DATA_ACTIONS_HISTORY);
			actionsHistoryList = followUpServiceAgent.getFollowUpProgressList( new FollowUpProgressView(null, followUpView.getFollowUpId(), domainDataView.getDomainDataId(), Constant.DEFAULT_IS_DELETED) );
		} 
		updateValuesToMap();
	}
	
	private void updateValuesToMap() {
		if ( contractView != null )
			viewMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
		if ( courtResultsList != null )
			viewMap.put(WebConstants.FollowUp.COURT_RESULT_LIST, courtResultsList);
		if ( actionsHistoryList != null )
			viewMap.put(WebConstants.FollowUp.ACTION_HISTORY_LIST, actionsHistoryList);
		if ( followUpView != null )
			viewMap.put(WebConstants.FollowUp.FOLLOW_UP_VIEW, followUpView);
		if ( sessionMap.get(WebConstants.FollowUp.RE_FETCH) != null ) 
			sessionMap.remove(WebConstants.FollowUp.RE_FETCH);
		if ( sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) != null )
			sessionMap.remove(WebConstants.FollowUp.FOLLOW_UP_VIEW);
	}
	
	private void getContractById(String contractId) throws Exception {
		
		ContractView contract = new ContractView();
		contract.setContractId(new Long(contractId));		
		
		HashMap<String, Object> contractHashMap = new HashMap<String, Object>(0);
		contractHashMap.put("contractView",contract);		
		contractHashMap.put("dateFormat",getDateFormat());
		
		List<ContractView>contractViewList = propertyServiceAgent.getAllContracts(contractHashMap);
		
		if( contractViewList != null && contractViewList.size() > 0 )
			contractView = (ContractView) contractViewList.get(0);
		
		setOtherContractRelatedValues();		
	}
	
	private void setOtherContractRelatedValues() {
		if ( contractView != null && contractView.getContractUnitView() != null && contractView.getContractUnitView().size() > 0 )
		{
			List<ContractUnitView> contractUnitViewList = new ArrayList<ContractUnitView>();
			contractUnitViewList.addAll(contractView.getContractUnitView());
			ContractUnitView contractUnitView = contractUnitViewList.get(0);
			
			txtpropertyName = contractUnitView.getUnitView().getPropertyCommercialName();			
			txtpropertyType = getIsEnglishLocale() ? contractUnitView.getUnitView().getPropertyTypeEn() : contractUnitView.getUnitView().getPropertyTypeAr();			
			txtunitRefNum = contractUnitView.getUnitView().getUnitNumber();
			txtUnitRentValue = contractUnitView.getUnitView().getRentValue() != null ? contractUnitView.getUnitView().getRentValue().toString() : "";
			txtUnitType = getIsEnglishLocale() ? contractUnitView.getUnitView().getUnitTypeEn() : contractUnitView.getUnitView().getUnitTypeAr();
		}
		
		if ( contractView != null && contractView.getTenantView() != null ) 
		{
			txtTenantName = contractView.getTenantView().getPersonFullName().equals("") ? contractView.getTenantView().getCompanyName() : contractView.getTenantView().getPersonFullName();
			txtTenantId = contractView.getTenantView().getPersonId() != null ? contractView.getTenantView().getPersonId().toString() : "";
		}
	}
	public void btnSave_Click()
	{
		String methodName = "btnSave_Click";
		logger.logInfo(methodName + "|" + "Start..");
		try
		{
			saveCommensAttachment();
			Long stopReplaceCheque = new Long(this.getStopReplaceCheque());//followUpView.getPaymentReceiptDetailView().getStopReplaceCheque();
			new FollowUpServiceAgent().stopAllowReplaceCheque(followUpView, stopReplaceCheque);
			successMessages.add(ResourceUtil.getInstance().getProperty("followUp.msg.updatedSuccesfully"));
			logger.logInfo(methodName + "|" + "Finish..");

		}
		catch (Exception e)
		{
			logger.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long associatedObjectId){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = viewMap.get(EXTERNAL_ID).toString();
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", notesOwner);
		if(associatedObjectId!= null)
		{
	    	String entityId = associatedObjectId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	private void saveCommensAttachment() throws Exception 
	{
		String methodName ="saveCommensAttachment";
		 
		 if(followUpView != null && followUpView.getFollowUpId()!=null)
		 {
		     saveComments(followUpView.getFollowUpId());
			 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
			 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
			 saveAttachments(followUpView.getFollowUpId().toString());
			 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		
		 }
		
	}
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try
    	{
	    	logger.logInfo(methodName + "started...");
    		logger.logInfo(methodName + "OpportunityId..."+ followUpView.getFollowUpId());
    		NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, followUpView.getFollowUpId());
    		logger.logInfo(methodName + "completed successfully!!!");
    		
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try
    	{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	@SuppressWarnings("unchecked")
	public String btnTenant_Click() {

		String methodName = "btnTenant_Click";
		logger.logInfo(methodName + "|" + "Start..");
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("tenantId",txtTenantId);
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+txtTenantId+");";
		openPopUp("", javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	@SuppressWarnings("unchecked")
	public String btnContract_Click() {
		String methodName = "btnContract_Click";
		logger.logInfo(methodName + "|" + "Start..");
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId",
				contractView.getContractId().toString());
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(
				WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,
				WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);

		String javaScriptText = "var screen_width = 1024;"
				+ "var screen_height = 450;"
				+ "window.open('LeaseContract.jsf?"
				+ WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW
				+ "="
				+ WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW
				+ "&"
				+ WebConstants.VIEW_MODE
				+ "="
				+ WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP
				+ "','_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=120,top=150,scrollbars=no,status=yes');";
		
		openPopUp("LeaseContract.jsf", javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");

		return "";
	}
	
	public String openPopUp(String URLtoOpen, String extraJavaScript) {
		String methodName = "openPopUp";		 
		logger.logInfo(methodName + "|" + "Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();		
		String javaScriptText = "var screen_width = screen.width;"
				+ "var screen_height = screen.height;"
				+ "window.open('"
				+ URLtoOpen
				+ "','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
		if (extraJavaScript != null && extraJavaScript.length() > 0)
			javaScriptText = extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	
	public String btnFollowUp_Click() {
		FollowUpProgressView followUpProgressView = new FollowUpProgressView();
		DomainDataView domainDataView = CommonUtil.getIdFromType(
				                                    CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
				                                    WebConstants.FollowUp.DOMAIN_DATA_ACTIONS_HISTORY);
		followUpProgressView.setFollowUpProgressTypeId( domainDataView.getDomainDataId() );
		followUpProgressView.setFollowUpId( followUpView.getFollowUpId() );
		followUpProgressView.setFollowUpProgressDate( new Date() );
		followUpProgressView.setStatusId( followUpView.getStatusId() );
		followUpProgressView.setStatusEn( followUpView.getStatusEn() );
		followUpProgressView.setStatusAr( followUpView.getStatusAr() );
		
		sessionMap.put(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW, followUpProgressView); 
		String javaScriptText = "javascript:showAddFollowUpProgressPopup();";
		openPopup( javaScriptText );
		return null;
	}
	
	public String btnCancel_Click() {
		return "followupSearch";
	}
	
	public String btnAddResult_Click() {
		FollowUpProgressView followUpProgressView = new FollowUpProgressView();
		DomainDataView domainDataView = CommonUtil.getIdFromType(
				                        CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
				                        WebConstants.FollowUp.DOMAIN_DATA_COURT_RESULT);
		followUpProgressView.setFollowUpProgressTypeId( domainDataView.getDomainDataId() );
		followUpProgressView.setFollowUpId( followUpView.getFollowUpId() );
		followUpProgressView.setFollowUpProgressDate( new Date() );
		followUpProgressView.setStatusId( followUpView.getStatusId() );
		followUpProgressView.setStatusEn( followUpView.getStatusEn() );
		followUpProgressView.setStatusAr( followUpView.getStatusAr() );
		sessionMap.put(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW, followUpProgressView); 
		String javaScriptText = "javascript:showAddFollowUpProgressPopup();";
		openPopup( javaScriptText );
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public Integer getCourtResultsRecordSize() {
		return courtResultsList.size();
	}
	
	public Integer getActionsHistoryRecordSize() {
		return actionsHistoryList.size();
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public ContractView getContractView() {
		return contractView;
	}

	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}

	public String getTxtpropertyName() {
		return txtpropertyName;
	}

	public void setTxtpropertyName(String txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}

	public String getTxtpropertyType() {
		return txtpropertyType;
	}

	public void setTxtpropertyType(String txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public String getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}

	public String getTxtUnitRentValue() {
		return txtUnitRentValue;
	}

	public void setTxtUnitRentValue(String txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}

	public String getTxtUnitType() {
		return txtUnitType;
	}

	public void setTxtUnitType(String txtUnitType) {
		this.txtUnitType = txtUnitType;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public String getTxtTenantId() {
		return txtTenantId;
	}

	public void setTxtTenantId(String txtTenantId) {
		this.txtTenantId = txtTenantId;
	}

	public List<FollowUpProgressView> getCourtResultsList() {
		return courtResultsList;
	}

	public void setCourtResultsList(List<FollowUpProgressView> courtResultsList) {
		this.courtResultsList = courtResultsList;
	}

	public List<FollowUpProgressView> getActionsHistoryList() {
		return actionsHistoryList;
	}

	public void setActionsHistoryList(List<FollowUpProgressView> actionsHistoryList) {
		this.actionsHistoryList = actionsHistoryList;
	}

	public FollowUpServiceAgent getFollowUpServiceAgent() {
		return followUpServiceAgent;
	}

	public void setFollowUpServiceAgent(FollowUpServiceAgent followUpServiceAgent) {
		this.followUpServiceAgent = followUpServiceAgent;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public FollowUpView getFollowUpView() {
		return followUpView;
	}

	public void setFollowUpView(FollowUpView followUpView) {
		this.followUpView = followUpView;
	}

	public String getStopReplaceCheque() {
		if(viewMap.containsKey("stopReplaceCheque"))
		   stopReplaceCheque =  viewMap.get("stopReplaceCheque").toString();
		return stopReplaceCheque;
	}

	public void setStopReplaceCheque(String stopReplaceCheque) {
		this.stopReplaceCheque = stopReplaceCheque;
		if(this.stopReplaceCheque!=null)
			viewMap.put("stopReplaceCheque",this.stopReplaceCheque);
	}
	public void showReplaceChequeRequest()
	{
		RequestView requestView = new RequestView();
		try{
		if(viewMap.get("PAYMENT_RECEIPT_DETAIL_ID") !=null )
			{
				Long paymentReceiptDetailId;
				paymentReceiptDetailId=(Long) viewMap.get("PAYMENT_RECEIPT_DETAIL_ID");
				requestView= new RequestServiceAgent().getReplaceChequeRequestAgainstFollowUp(paymentReceiptDetailId);
			}
		if(requestView!=null && requestView.getRequestId()!=null)
			{
				sessionMap.put(WebConstants.REQUEST_VIEW,requestView);
				executeJavaScript("openReplaceChequePopup();");
			}
		else
			{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage("followup.errorMessage.noRelaceChequeRequestFound"));
			}
		}
		catch (Exception e) 
		{
			logger.LogException("showReplaceChequeRequest() crashed |", e);
		}
		
	}
	public void executeJavaScript(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	public void sendToLegalDeptt()
	{
		String methodName ="sendToLegalDeptt";
		logger.logInfo(methodName+"|"+"Start..");
		try	
    	{
			if ( viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) != null ){
				followUpView = (FollowUpView) viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) ;
				followUpView = new FollowUpService().setFollowupStatusAsForLegalDeptt(followUpView.getFollowUpId(), getLoggedInUserId());
				successMessages.add(CommonUtil.getBundleMessage("successMsg.sendToLegalDept"));
				if(viewMap.get(IS_ALLOWED) != null )
					viewMap.remove(IS_ALLOWED);
				updateValuesToMap();
			}				
    	}
    	catch(PimsBusinessException ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		if(ex.getExceptionCode().compareTo(ExceptionCodes.STATUS_UNABLE_TO_CHANGE) == 0)
    			errorMessages.add(CommonUtil.getBundleMessage("errMsg.statusUnableToChangeToLegalDeptt"));
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	public boolean isShowBtnSendToLegal() {
		boolean isShow = false;
		if ( viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) != null ){
			followUpView = (FollowUpView) viewMap.get(WebConstants.FollowUp.FOLLOW_UP_VIEW) ;
			if(followUpView.getStatusId().compareTo(ddfollowUpStatusOpen.getDomainDataId()) == 0){
				if(viewMap.get("IS_ALLOWED") != null){
					isShow = (Boolean) viewMap.get("IS_ALLOWED");
				}
			}
		}
		return isShow;
	}

	public void setShowBtnSendToLegal(boolean isShowBtnSendToLegal) {
		this.isShowBtnSendToLegal = isShowBtnSendToLegal;
	}

	public DomainDataView getDdfollowUpStatusForlegalDeptt() {
		return ddfollowUpStatusForlegalDeptt;
	}

	public void setDdfollowUpStatusForlegalDeptt(
			DomainDataView ddfollowUpStatusForlegalDeptt) {
		this.ddfollowUpStatusForlegalDeptt = ddfollowUpStatusForlegalDeptt;
	}

	public boolean isShowBtnSave() {
		return isUserAllowedToSaveAndAddFollowup();
	}

	public void setShowBtnSave(boolean isShowBtnSave) {
		this.isShowBtnSave = isShowBtnSave;
	}

	public boolean isShowBtnFollowup() {
		return isUserAllowedToSaveAndAddFollowup();
	}

	public void setShowBtnFollowup(boolean isShowBtnFollowup) {
		this.isShowBtnFollowup = isShowBtnFollowup;
	}
	private boolean isUserAllowedToSaveAndAddFollowup(){
		
		String methodName ="isUserAllowedToSaveAndAddFollowup";
		logger.logInfo(methodName+"|"+"Start..");
		
		boolean isAllowed = false;
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			//if 'open' and finance dept
			//else if 'for legal' and legal dept
			if(isOpenStatus()){
				return isAllowed(ALLOWED_USER_GROUP_FOR_OPEN);
			}
			else if(isLegalStatus()){
				return isAllowed(ALLOWED_USER_GROUP_FOR_LEGAL);
			}
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		return isAllowed; 
	}
	@SuppressWarnings("unchecked")
	private boolean isAllowed(String followUpGroupForStatus) {
		boolean isAllowed = false;
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{

			if(viewMap.get(IS_ALLOWED) != null && (Boolean) viewMap.get(IS_ALLOWED)){
				isAllowed = true;
			}
			else if(viewMap.get(USER_GROUPS) != null ){
				List<UserGroup> userGroups = (List<UserGroup>) viewMap.get(USER_GROUPS);
				String allowedGroupsCSV =(String) viewMap.get(followUpGroupForStatus);
				if(allowedGroupsCSV != null && allowedGroupsCSV.length() > 0){
					String allowedGroup[] = allowedGroupsCSV.split(",") ;
					if(userGroups != null && userGroups.size() > 0 && allowedGroup.length > 0){
						
					Assgined : for(UserGroup assignedGroup : userGroups ){
								for(String allowed : allowedGroup ){
								if(assignedGroup.getUserGroupId().compareTo(allowed) == 0){
									isAllowed = true;
									viewMap.put(IS_ALLOWED, true);
									break Assgined;
								}
							}
						}
				}
				}
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("isAllowed|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		return isAllowed;
	}

	private boolean isOpenStatus(){
		if(followUpView.getStatusId().compareTo(ddfollowUpStatusOpen.getDomainDataId()) == 0){
			return true;
		}
		return false;
	}
	
	private boolean isCloseStatus(){
		if(
		   followUpView.getStatusId().compareTo(ddfollowUpStatusClose.getDomainDataId()) == 0 
		    
		  )
		{
			return true;
		}
		return false;
	}
	
	private boolean isLegalStatus(){
		if(
				followUpView.getStatusId().compareTo(ddfollowUpStatusForlegalDeptt.getDomainDataId()) == 0 ||
				followUpView.getStatusId().compareTo(63004l) == 0 ||
				followUpView.getStatusId().compareTo(63005l) == 0
				
		  )
		{
			return true;
		}
		return false;
	}
	
	
	private boolean isFinanceGroupUser(){
		boolean isFinance =  false;
		
		return isFinance;
	}
}
