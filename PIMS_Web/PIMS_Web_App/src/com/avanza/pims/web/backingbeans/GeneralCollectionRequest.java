package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.PIMSGeneralCollectionRequestBPELPortClient;
import com.avanza.pims.entity.GeneralCollectionFixedCostCenter;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;


public class GeneralCollectionRequest extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(GeneralCollectionRequest.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_COLLECTION_REQUIRED = "COLLECTION_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_PAYMENT_DETAILS   = "paymentsTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String hdnPropertyId;
    private String hdnUnitId;
    private String txtRemarks;
    private Boolean chkCostCenterFixed;
    private String selectOneCostCenter;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlInputText txtCostCenter = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneFixCostCenter = new HtmlSelectOneMenu();
	private RequestView requestView ;
    private List<PaymentScheduleView> paymentList = new ArrayList<PaymentScheduleView>();	
    private PaymentScheduleView paymentView = new PaymentScheduleView();
    private HtmlDataTable dataTable;
    private HtmlSelectOneMenu cmbOwnershipType = new HtmlSelectOneMenu();
	public GeneralCollectionRequest(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				if (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) != null )
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					if(isPaymentCollected)
				    {
							onMessageFromReceivePayments();
							
				    }
				}
				
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		
		viewMap.put(PROCEDURE_TYPE,WebConstants.GeneralCollectionRequest.PROCEDURE_TYPE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.GeneralCollectionRequest.PROCEDURE_TYPE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender(){
	
		if ( getPageMode().equals(PAGE_MODE_COLLECTION_REQUIRED) && 
				 viewMap.get("collectionCompleted" ) != null 
			   )
		{
				setPageMode(PAGE_MODE_VIEW);
		}
		if(chkCostCenterFixed != null)
		{
			htmlSelectOneFixCostCenter.setRendered(chkCostCenterFixed);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW);
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.GENERAL_COLLECTION );
		

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if( getStatusCollectionRequired() )
		{
			setPageMode( PAGE_MODE_COLLECTION_REQUIRED);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if (getStatusCompleted() )
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}	
		else if( 
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_CANCELED ) == 0  || 
			 	this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED )  == 0 
		       )
		{
			setPageMode(PAGE_MODE_VIEW);
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusCollectionRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COLLECTION_REQ ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get(  WebConstants.GeneralCollectionRequest.PaymentList) != null)
		{
			paymentList = (	ArrayList<PaymentScheduleView> )viewMap.get(  WebConstants.GeneralCollectionRequest.PaymentList ); 
		}
		if( viewMap.get(  WebConstants.GeneralCollectionRequest.PaymentView) != null)
		{
			paymentView = (	PaymentScheduleView )viewMap.get(  WebConstants.GeneralCollectionRequest.PaymentView ); 
		}
		if( viewMap.get("chkCostCenterFixed")!= null )
		{
			chkCostCenterFixed=(Boolean)viewMap.get("chkCostCenterFixed"); 
		}
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if( paymentList != null )
		{
			viewMap.put(  WebConstants.GeneralCollectionRequest.PaymentList, paymentList);
		}
		if( paymentView != null )
		{
			viewMap.put(  WebConstants.GeneralCollectionRequest.PaymentView,paymentView );
		}
		else
		{
			viewMap.put(  WebConstants.GeneralCollectionRequest.PaymentView, new PaymentScheduleView() );
		}
		if( viewMap.get("chkCostCenterFixed") != null )
		{
		viewMap.put("chkCostCenterFixed",chkCostCenterFixed);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getGeneralCollectionRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getGeneralCollectionRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getGeneralCollectionRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getGeneralCollectionRequestById( id );
		if(requestView.getPaymentScheduleView() != null )
		{
			paymentList = requestView.getPaymentScheduleView();
//			HashMap dominDataMap = (HashMap) new UtilityManager().getDomainDataMap();
//			for (PaymentScheduleView paymentSchedule : paymentList) 
//			{
//				if( paymentSchedule.getPropertyId()  != null )
//				{
//					try
//					{
//						paymentSchedule.setProperty(
//														 PropertyTransformUtil.transformToPropertyView(paymentSchedule.getPropertyId() , dominDataMap)
//												   );
//						paymentSchedule.setName( paymentSchedule.getProperty().getCommercialName() );
//					}
//					catch(Exception e) { }
//				}
//				if( paymentSchedule.getUnitId()  != null )
//				{
//					try
//					{
//						UnitView unitView  = PropertyTransformUtil.transformToUnitView(paymentSchedule.getUnitId() , dominDataMap);
//						paymentSchedule.setUnit( unitView );
//						paymentSchedule.setName( unitView.getUnitNumber() );
//					}
//					catch(Exception e) { }
//				}
//			}
		}
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
    public List<SelectItem> getFixedCostCenters() throws Exception 
    {
    	final String key = "FixedCostCenters"; 
    	if(viewMap.get( key  )!= null)
    	{ 
    		return (List<SelectItem>)viewMap.get( key );
    	}
    	
    	
		List<GeneralCollectionFixedCostCenter> list = UtilityService.getGeneralCollectionFixedCostCenters();
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) 
		{
			for (GeneralCollectionFixedCostCenter obj : list) 
			{

				SelectItem item = new SelectItem(obj.getCcAr() , obj.getCcAr() );
				itemList.add(item);
			}
		}
		viewMap.put( key , itemList );
		return itemList;
	}

	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			EndowmentView endowment = (EndowmentView)sessionMap.remove(WebConstants.SELECTED_ROW);
			if( endowment.getCostCenter() != null && endowment.getCostCenter() .trim().length() >0  )
			{
				paymentView.setCostCenter( endowment.getCostCenter()  );
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.endowmentNotCostCenter"));
				return;
			}
			chkCostCenterFixed=false;
			paymentView.setEndowmentView( endowment  );
			paymentView.setProperty(new PropertyView());
			paymentView.setPropertyId( null );
			paymentView.setUnit(new UnitView() );
			paymentView.setInheritanceFile( new InheritanceFileView ());
			paymentView.setInheritanceFileId( null );
			paymentView.setInheritanceBeneficiary( new InheritanceBeneficiaryView() );
			paymentView.setInheritanceBeneficiaryId( null );
		  	hdnPropertyId = "";
		  	hdnUnitId = "";
		  	paymentView.setOwnerShipTypeIdStr( String.valueOf( WebConstants.OwnerShipType.ENDOWED_ID ) );
		  	cmbOwnershipType.setDisabled(true);
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchEndowments|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchUnit()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			
			updateValuesFromMap();
			
		 
				UnitView unit= (UnitView)sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);
				UnitView uView= new UnitView();
				hdnUnitId = unit.getUnitId().toString();
				if( unit.getAccountNumber() != null && unit.getAccountNumber().trim().length() >0  )
				{
					paymentView.setCostCenter( unit.getAccountNumber() );
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.unitNotCostCenter"));
					return;
				}
				
				paymentView.setUnitId( unit.getUnitId() );
				uView.setAccountNumber( unit.getAccountNumber() );
				uView.setUnitNumber( unit.getUnitNumber() );
				
				paymentView.setUnit( uView );
				
				chkCostCenterFixed=false;
				paymentView.setProperty(new PropertyView());
				paymentView.setPropertyId( null );
				paymentView.setEndowmentView( new EndowmentView() );
				paymentView.setInheritanceFile( new InheritanceFileView ());
				paymentView.setInheritanceFileId( null );
				paymentView.setInheritanceBeneficiary( new InheritanceBeneficiaryView() );
				paymentView.setInheritanceBeneficiaryId( null );
			  	hdnPropertyId = "";
			  	if( unit.getPropertyCategoryId() != null )
			  	{
			  		paymentView.setOwnerShipTypeIdStr( String.valueOf( unit.getPropertyCategoryId() ) );
			  	}
			  	cmbOwnershipType.setDisabled(false);
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchUnit|Error Occured..",ex);
			
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchBeneficiary()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			if( sessionMap.get(WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_ONE_BENEFICIARY ) != null )
			{
				
				BeneficiaryGridView beneficiaryGridView = (BeneficiaryGridView)sessionMap.remove( WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_ONE_BENEFICIARY  );
				if( beneficiaryGridView.getCostCenter()  != null && beneficiaryGridView.getCostCenter().trim().length() >0  )
				{
					paymentView.setCostCenter( beneficiaryGridView.getCostCenter() );
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.fieldLabel.benCostCenter"));
					return;
				}
				
				
				InheritanceBeneficiaryView bView = new InheritanceBeneficiaryView();
				
				bView.setInheritanceBeneficiaryId(beneficiaryGridView.getInheritanceBeneficiaryId());
				bView.setBeneficiaryId(beneficiaryGridView.getBeneficiaryPersonId() );
				
				PersonView beneficiary = new PersonView();
				beneficiary.setPersonFullName( beneficiaryGridView.getPersonName());
				bView.setBeneficiary( beneficiary );
				
				paymentView.setInheritanceBeneficiary(bView); 
				chkCostCenterFixed=false;
				paymentView.setInheritanceBeneficiaryId( bView.getInheritanceBeneficiaryId()  );
				paymentView.setOwnerShipTypeIdStr( String.valueOf( WebConstants.OwnerShipType.MINOR_ID ) );
				
				paymentView.setProperty( new PropertyView() );
				paymentView.setPropertyId( null ); 
				paymentView.setUnit(new UnitView());
				paymentView.setUnitId( null );
				paymentView.setEndowmentView( new EndowmentView() );
				paymentView.setInheritanceFile( new InheritanceFileView() );
				paymentView.setInheritanceFileId( null ); 
				cmbOwnershipType.setDisabled(true);
				hdnPropertyId = "";
			  	hdnUnitId = "";
				
			}
		}
		catch(Exception e)
		{
			logger.LogException("onMessageFromSearchFile|Error Occured..",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchFile()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			if( sessionMap.get(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_ONE_FILE ) != null )
			{
				
				InheritanceFileView fileView = (InheritanceFileView)sessionMap.remove(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_ONE_FILE );
				if( fileView.getCostCenter() != null && fileView.getCostCenter().trim().length() >0  )
				{
					paymentView.setCostCenter(fileView.getCostCenter());
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.fileNotCostCenter"));
					return;
				}
				
				
				
				paymentView.setInheritanceFile( 
												fileView 
											  ); 
				
				paymentView.setInheritanceFileId( fileView.getInheritanceFileId() );
				paymentView.setOwnerShipTypeIdStr( String.valueOf( WebConstants.OwnerShipType.MINOR_ID ) );
				chkCostCenterFixed=false;
				paymentView.setProperty( new PropertyView() );
				paymentView.setPropertyId( null ); 
				paymentView.setUnit(new UnitView());
				paymentView.setUnitId( null );
				paymentView.setEndowmentView( new EndowmentView() );
				paymentView.setInheritanceBeneficiary( new InheritanceBeneficiaryView() );
				paymentView.setInheritanceBeneficiaryId( null ); 
				cmbOwnershipType.setDisabled(true);
				
				hdnPropertyId = "";
			  	hdnUnitId = "";
			}
		}
		catch(Exception e)
		{
			logger.LogException("onMessageFromSearchFile|Error Occured..",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchProperty()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			
			updateValuesFromMap();
			if(hdnPropertyId!= null )
			{
				
				PropertyService service = new PropertyService();
				PropertyView property = service.getPropertyByID( new Long ( hdnPropertyId ) );
				if( property.getCostCenter() != null && property.getCostCenter().trim().length() >0  &&
					!property.getCostCenter().equals("null") 
				)
				{
					paymentView.setCostCenter( property.getCostCenter() );
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.propertyNotCostCenter"));
					return;
				}
				PropertyView propView = new PropertyView();
				chkCostCenterFixed=false;
				propView.setCostCenter(property.getCostCenter());
				propView.setCommercialName(property.getCommercialName());
				propView.setPropertyId( property.getPropertyId() );
				paymentView.setPropertyId(property.getPropertyId());
				paymentView.setProperty( propView );
				//paymentView.setName( )
				
				paymentView.setUnit(new UnitView());
				paymentView.setUnitId( null );
				paymentView.setEndowmentView( new EndowmentView() );
				paymentView.setInheritanceFile( new InheritanceFileView ());
				paymentView.setInheritanceFileId( null );
				paymentView.setInheritanceBeneficiary( new InheritanceBeneficiaryView() );
				paymentView.setInheritanceBeneficiaryId( null );
				if( property.getCategoryId() != null )
			  	{
			  		paymentView.setOwnerShipTypeIdStr( String.valueOf( property.getCategoryId() ) );
			  	}		
				hdnUnitId= "";
				cmbOwnershipType.setDisabled(false);
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchProperty|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( paymentList==null || paymentList.size() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("thirdPartRevenue.msg.revenueRequired"));
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
			return true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}


	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			persistRequestInTransaction();
			getGeneralCollectionRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty("generalCollection.msg.saved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void persistRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
	
		if(status!=null) requestView.setStatusId(status);
		requestView.setPaymentScheduleView(paymentList);
		requestView = requestService.persistGeneralCollectionRequest(requestView);
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			if( hasSaveErrors() ){ return; }
			 updateValuesFromMap();
			 
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "PIMSCollectionBPEL" );
			 PIMSGeneralCollectionRequestBPELPortClient port=new PIMSGeneralCollectionRequestBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( status );
			 getGeneralCollectionRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getGeneralCollectionRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.OK);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onSendBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getGeneralCollectionRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBack--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
		//	 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getGeneralCollectionRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean hasCollectionErrors() throws Exception
	{
		boolean hasCollectionErrors=false;
		errorMessages = new ArrayList<String>();

		return hasCollectionErrors;
		
	}
	

	@SuppressWarnings( "unchecked" )
	public void onPrintReceipt()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			CommonUtil.printMiscellaneousPaymentReceipt(requestView.getRequestId().toString(), null, FacesContext.getCurrentInstance());
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPrintReceipt--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCollect()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasCollectionErrors() ){ return; }
			
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentList);
	        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	        executeJavaScript("javascript:openPopupReceivePayment();");
			
//			saveDonationRequestInTransaction();
//			getDonationRequestDetails( requestView.getRequestId() );
//			setTaskOutCome(TaskOutcome.OK);
			updateValuesToMap();
			btnCollectPayment.setRendered(false);
			//successMessages.add( ResourceUtil.getInstance().getProperty("endowmentProgram.msg.savedSuccessfully"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCollect--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromReceivePayments()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			if( hasCollectionErrors() ){ return; }

			Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
			requestView.setStatusId(status);
			prv.setPaidById(requestView.getApplicantView().getPersonId());
			prv = requestService.collectRequestsPayments(requestView, prv);
			
			setTaskOutCome(TaskOutcome.APPROVE);
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			getGeneralCollectionRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_COLLECT);
			viewMap.put("collectionCompleted", true);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("generalCollection.msg.collection"));
			this.setPageMode( PAGE_MODE_VIEW );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromReceivePayments--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}


	private boolean hasAddErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
	
		
		if( paymentView.getTypeId() ==null || paymentView.getTypeId().compareTo( 0l ) <= 0  )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("generalCollection.msg.paymentTypeReq"));
		}
		if( !chkCostCenterFixed && 
			( paymentView.getCostCenter()  ==null || paymentView.getCostCenter().trim().length() <= 0 ) 
		   )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("generalCollection.msg.costCenterRequired"));
		}
		else if( chkCostCenterFixed && 
				(selectOneCostCenter == null || selectOneCostCenter.equals("-1")) 
		   	   )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("generalCollection.msg.costCenterRequired"));
		}
		if( paymentView.getAmount()==null || paymentView.getAmount().compareTo(0d)<=0)
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("mems.payment.err.msgs.amntReq"));
		}
		if( paymentView.getOwnerShipTypeId() == null || paymentView.getOwnerShipTypeId().compareTo( 0l ) <= 0  )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("generalCollection.msg.OwnershiptReq"));
		}
		
		if( hasSaveErrors )return true;
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onCheckFixCostCenter()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
		    updateValuesFromMap();
		    txtCostCenter.setRendered( !chkCostCenterFixed );
		    htmlSelectOneFixCostCenter.setRendered( chkCostCenterFixed );
		    if(!chkCostCenterFixed)
		    {
		    	paymentView.setCostCenter( "" );
		    }
		    else
		    {
		    	paymentView.setCostCenter( selectOneCostCenter );
		    }
			paymentView.setUnit(new UnitView());
			paymentView.setUnitId( null );
			paymentView.setProperty(new PropertyView());
			paymentView.setPropertyId( null );
			paymentView.setEndowmentView( new EndowmentView() );
			paymentView.setInheritanceFile( new InheritanceFileView ());
			paymentView.setInheritanceFileId( null );
			paymentView.setInheritanceBeneficiary( new InheritanceBeneficiaryView() );
			paymentView.setInheritanceBeneficiaryId( null );
			cmbOwnershipType.setDisabled(false);
		  	hdnPropertyId = "";
		  	hdnUnitId = "";
		}
		catch(Exception ex)
		{
			logger.LogException("onCheckFixCostCenter|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasAddErrors() ){ return; }
			PaymentScheduleView clonedView = (PaymentScheduleView)BeanUtils.cloneBean(paymentView);
			if( chkCostCenterFixed )
			{
				clonedView.setCostCenter( selectOneCostCenter );
			}
			paymentList.add( 0, clonedView);
			persistRequestInTransaction();
			getGeneralCollectionRequestDetails( requestView.getRequestId() );
			paymentView = new PaymentScheduleView();
			cmbOwnershipType.setDisabled(false);
			hdnPropertyId ="";
			hdnUnitId = "";
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.added"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAdd --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( requestView.getRequestId() != null && paymentList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.deleteError"));
        }
		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
	        paymentView = ( PaymentScheduleView )dataTable.getRowData();
			if( paymentView.getPropertyId()  != null )
			{
				hdnPropertyId = paymentView.getPropertyId().toString(); 
			}
			else if( paymentView.getUnitId()  != null )
			{
				hdnUnitId = paymentView.getUnitId().toString(); 
			}
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onEdit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			PaymentScheduleView row = ( PaymentScheduleView )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getPaymentScheduleId() != null)
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	requestService.deleteEndowmentRelatedPayment(row);
	        }
	        paymentList.remove( row );	
			
			successMessages.add( ResourceUtil.getInstance().getProperty("generalCollection.msg.deleted"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("generalCollection.lbl.title"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCollectButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_COLLECTION_REQUIRED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintButton()
	{
		if(  getStatusCompleted() )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<PaymentScheduleView> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<PaymentScheduleView> paymentList) {
		this.paymentList = paymentList;
	}

	public PaymentScheduleView getPaymentView() {
		return paymentView;
	}

	public void setPaymentView(PaymentScheduleView paymentView) {
		this.paymentView = paymentView;
	}

	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public String getHdnUnitId() {
		return hdnUnitId;
	}

	public void setHdnUnitId(String hdnUnitId) {
		this.hdnUnitId = hdnUnitId;
	}

	public HtmlSelectOneMenu getCmbOwnershipType() {
		return cmbOwnershipType;
	}

	public void setCmbOwnershipType(HtmlSelectOneMenu cmbOwnershipType) {
		this.cmbOwnershipType = cmbOwnershipType;
	}

	public String getSelectOneCostCenter() {
		return selectOneCostCenter;
	}

	public void setSelectOneCostCenter(String selectOneCostCenter) {
		this.selectOneCostCenter = selectOneCostCenter;
	}

	@SuppressWarnings("unchecked")
	public Boolean getChkCostCenterFixed() {
		if( viewMap.get("chkCostCenterFixed") != null  )
		{
			chkCostCenterFixed  = (Boolean)viewMap.get("chkCostCenterFixed");
		}
		return chkCostCenterFixed;
	}

	@SuppressWarnings("unchecked")
	public void setChkCostCenterFixed(Boolean chkCostCenterFixed) {
		this.chkCostCenterFixed = chkCostCenterFixed;
		if( this.chkCostCenterFixed != null )
		{
			viewMap.put("chkCostCenterFixed",this.chkCostCenterFixed );
		}
	}

	public HtmlInputText getTxtCostCenter() {
		return txtCostCenter;
	}

	public void setTxtCostCenter(HtmlInputText txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}

	public HtmlSelectOneMenu getHtmlSelectOneFixCostCenter() {
		return htmlSelectOneFixCostCenter;
	}

	public void setHtmlSelectOneFixCostCenter(
			HtmlSelectOneMenu htmlSelectOneFixCostCenter) {
		this.htmlSelectOneFixCostCenter = htmlSelectOneFixCostCenter;
	}

	
	

}