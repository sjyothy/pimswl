package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DocumentType;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.NOLType;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ProcedureDocuments.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.NOLTypeView;

public class ProcedureDocument extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(ProcedureDocument.class);

	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<String> errorMessages;
	private List<String> infoMessages;

	private com.avanza.pims.entity.ProcedureDocument procedureDocument;
	private String procedureTypeId;
	private String procedureType;
	private String selectOneDocumentType;
	private String selectOneNOLType;
	private String documentDescription;
	private HtmlSelectOneMenu cmbNolType= new HtmlSelectOneMenu();
	private List<SelectItem> nolTypeItems = new ArrayList();
	private HtmlSelectOneMenu cmbFileType= new HtmlSelectOneMenu();
	private Boolean isUpdateMode;

	public void init() {
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		if (isUpdateMode() == null)
			setUpdateMode(false);

		if (!isPostBack())
			getAttributesFromSession();
		
		if( viewMap.get(WebConstants.NOL.NOL_TYPE_LIST)!=null){
			nolTypeItems = (List<SelectItem>) viewMap.get(WebConstants.NOL.NOL_TYPE_LIST);

		}		
		else
		{
			getNolTypes(WebConstants.NO_OBJECTION_LETTER_REQUEST_TYPE);
		}
		
		if(!getIsSelectedProcedureTypeIsNOL())
		{
			cmbNolType.setRendered(false);
		}
	}

	public boolean getIsSelectedProcedureTypeIsNOL()
	{
		boolean flag = false;
		
		try
		{
			if(procedureTypeId != null && !procedureTypeId.trim().equals(""))
			{
				Long _procedureTypeId = Long.parseLong(procedureTypeId);
				UtilityManager utilityManager = new UtilityManager();
				DomainData domainDataProcedureType_NOL = null;
				if(viewMap.containsKey("DD_PROCEDURE_TYPE_NOL"))
				{
					domainDataProcedureType_NOL = (DomainData) viewMap.get("DD_PROCEDURE_TYPE_NOL");
				}					
				else
				{
					domainDataProcedureType_NOL = utilityManager.getDomainDataByValue(WebConstants.PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_NO_OBJECTION_LETTER);
					viewMap.put("DD_PROCEDURE_TYPE_NOL", domainDataProcedureType_NOL);
				}
									
				if(domainDataProcedureType_NOL.getDomainDataId().compareTo(_procedureTypeId) == 0)
					flag = true;
			}		
		}
		catch(Exception ex)
		{}
		return flag;
	}
	
	private void getNolTypes(String domainType)
	{
		
		String methodName="getNolTypes"; 
		logger.logInfo(methodName+"|"+"Start");
		if(!viewMap.containsKey(WebConstants.NOL.NOL_TYPE_LIST))
		{
			try 
			{
				
				UtilityServiceAgent usa = new UtilityServiceAgent();
				List <NOLTypeView> nolTypeViewList = usa.getAllNOLTypes();
				
				
				for(int i=0;i<nolTypeViewList.size();i++)
				  {
					  NOLTypeView rTV=(NOLTypeView)nolTypeViewList.get(i);
					  SelectItem item;
					  if (getIsEnglishLocale())
						 {
						 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionEn()));			  
						 }
						 else 
						 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionAr()));
				  }
				logger.logInfo(methodName+"|"+"Finish");
				}
			catch (Exception e){
				logger.LogException(methodName+"|Error Occured ",e);
				
			}
	
			viewMap.put(WebConstants.NOL.NOL_TYPE_LIST, nolTypeItems);
		}
	}
	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}
	// gets the attributes passed to this page in request scope
	@SuppressWarnings("unchecked")
	private void getAttributesFromSession() {
		logger.logInfo("getAttributesFromRequest() started...");

		try {
			Object procedureDocument = sessionMap
					.get(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_OBJ);

			if (procedureDocument != null) {
				this.procedureDocument = (com.avanza.pims.entity.ProcedureDocument) procedureDocument;
				viewMap.put("procedureDocument", this.procedureDocument);
				sessionMap
						.remove(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_OBJ);
				setUpdateMode(true);
			}

			Object procedureTypeId = sessionMap
					.get(ProcedureDocuments.Keys.PROCEDURE_TYPE_ID);

			if (procedureTypeId != null) {
				this.procedureTypeId = (String) procedureTypeId;
				viewMap.put("procedureTypeId", this.procedureTypeId);
				sessionMap.remove(ProcedureDocuments.Keys.PROCEDURE_TYPE_ID);
			}

			Object procedureType = sessionMap
					.get(ProcedureDocuments.Keys.PROCEDURE_TYPE);

			if (procedureType != null) {
				this.procedureType = (String) procedureType;
				viewMap.put("procedureType", this.procedureType);
				sessionMap.remove(ProcedureDocuments.Keys.PROCEDURE_TYPE);
			}

			Object documentType = sessionMap
					.get(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_TYPE);
			if (documentType != null) {
				this.selectOneDocumentType = (String) documentType;
				viewMap
						.put("selectOneDocumentType",
								this.selectOneDocumentType);
				sessionMap
						.remove(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_TYPE);
			}

			Object documentDescription = sessionMap
					.get(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_DESCRIPTION);
			if (documentDescription != null) {
				this.documentDescription = (String) documentDescription;
				viewMap.put("documentDescription", this.documentDescription);
				sessionMap
						.remove(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_DESCRIPTION);
			}
			
			Object nolType = sessionMap.get("NOL_TYPE_ID");
			if (nolType != null)
			{
				cmbNolType.setValue(nolType.toString());
				sessionMap.remove("NOL_TYPE_ID");
			}
			Object fileType = sessionMap.get("FILE_TYPE_ID");
			if (fileType != null)
			{
				cmbFileType.setValue(fileType.toString());
				sessionMap.remove("FILE_TYPE_ID");
			}
			
			Object procedureDocumentId = sessionMap.get("PROCEDURE_DOCUMENT_ID");
			if (procedureDocumentId != null)
			{
				viewMap.put("PROCEDURE_DOCUMENT_ID", procedureDocumentId);
				sessionMap.remove("PROCEDURE_DOCUMENT_ID");
			}
			
			
			logger.logInfo("getAttributesFromRequest() finished...");
		} catch (Exception e) {
			logger.LogException("getAttributesFromRequest() crashed ", e);
		}
	}

	// saves the documents of the selected procedure
	public void saveDocument() {
		logger.logInfo("saveDocument() started...");

		try {
			
			if (viewMap.containsKey("PROCEDURE_DOCUMENT_ID")) 
			{
				com.avanza.pims.entity.ProcedureDocument procedureDocument = EntityManager.getBroker().findById(com.avanza.pims.entity.ProcedureDocument.class, (Long)viewMap.get("PROCEDURE_DOCUMENT_ID"));
				DocumentType documentType = new DocumentType();
				documentType.setDocumentTypeId(Long
						.parseLong(getSelectOneDocumentType()));
				procedureDocument.setDescription(getDocumentDescription());
				procedureDocument.setDocumentType(documentType);

				Object nolTypeVal =  cmbNolType.getValue();
				if(nolTypeVal != null && !nolTypeVal.toString().trim().equals(""))
				{
					if(nolTypeVal.toString().trim().equals("-1"))
						procedureDocument.setNolType(null);
					else
					{
						NOLType nolType = new NOLType();
						nolType.setNolTypeId(Long.parseLong(nolTypeVal.toString()));
						procedureDocument.setNolType(nolType);
					}
				}
				else{
					if(cmbFileType != null && cmbFileType.getValue() != null && cmbFileType.getValue().toString().compareTo("-1") != 0){
						procedureDocument.setSubProcedureId(Long.valueOf(cmbFileType.getValue().toString()));
					}
				}
				
				
				EntityManager.getBroker().update(procedureDocument);
			} else {
				DocumentType documentType = new DocumentType();
				documentType.setDocumentTypeId(Long
						.parseLong(getSelectOneDocumentType()));

				com.avanza.pims.entity.ProcedureDocument newProcedureDocument = new com.avanza.pims.entity.ProcedureDocument();
				newProcedureDocument.setProcedureTypeId(Long
						.parseLong(getProcedureTypeId()));
				newProcedureDocument.setDocumentType(documentType);
				newProcedureDocument.setDescription(getDocumentDescription());
				newProcedureDocument.setCreatedBy("admin");
				newProcedureDocument.setCreatedOn(new Date());
				newProcedureDocument.setUpdatedBy("admin");
				newProcedureDocument.setUpdatedOn(new Date());
				newProcedureDocument.setIsDeleted(0l);
				newProcedureDocument.setRecordStatus(1l);
				
				Object nolTypeVal =  cmbNolType.getValue();
				if(nolTypeVal != null && !nolTypeVal.toString().trim().equals(""))
				{
					if(nolTypeVal.toString().trim().equals("-1"))
						newProcedureDocument.setNolType(null);
					else
					{
					NOLType nolType = new NOLType();
					nolType.setNolTypeId(Long.parseLong(nolTypeVal.toString()));
					newProcedureDocument.setNolType(nolType);
					}
				}
				else{
					if(cmbFileType != null && cmbFileType.getValue() != null && cmbFileType.getValue().toString().compareTo("-1") != 0){
						newProcedureDocument.setSubProcedureId(Long.valueOf(cmbFileType.getValue().toString()));
					}
				}

				EntityManager.getBroker().add(newProcedureDocument);
				
				viewMap.put("procedureDocument", newProcedureDocument);
			}

			closeWindowAndSubmit();
			logger.logInfo("saveDocument() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("saveDocument() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	private void closeWindowAndSubmit() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		sessionMap.put("reloadProcedureDocuments", true);
		sessionMap.put("procedureDocument", getProcedureDocument());
		sessionMap.put(Keys.PROCEDURE_TYPE_ID, getProcedureTypeId());

		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:closeWindowSubmit();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	// Attribute Accessors
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public String getProcedureType() {
		if (viewMap.get("procedureType") != null)
			procedureType = (String) viewMap.get("procedureType");

		return procedureType;
	}

	public void setProcedureType(String procedureType) {
		this.procedureType = procedureType;
	}

	public String getSelectOneDocumentType() {
		return selectOneDocumentType;
	}

	public void setSelectOneDocumentType(String selectOneDocumentType) {
		this.selectOneDocumentType = selectOneDocumentType;
	}

	public String getDocumentDescription() {
		return documentDescription;
	}

	public void setDocumentDescription(String documentDescription) {
		this.documentDescription = documentDescription;
	}

	public com.avanza.pims.entity.ProcedureDocument getProcedureDocument() {
		if (viewMap.get("procedureDocument") != null)
			procedureDocument = (com.avanza.pims.entity.ProcedureDocument) viewMap
					.get("procedureDocument");

		return procedureDocument;
	}

	public void setProcedureDocument(
			com.avanza.pims.entity.ProcedureDocument procedureDocument) {
		this.procedureDocument = procedureDocument;
	}

	public Boolean isUpdateMode() {
		if (viewMap.get("isUpdateMode") != null)
			isUpdateMode = (Boolean) viewMap.get("isUpdateMode");

		return isUpdateMode;
	}

	@SuppressWarnings("unchecked")
	public void setUpdateMode(Boolean isUpdateMode) {
		if (isUpdateMode != null)
			viewMap.put("isUpdateMode", isUpdateMode);

		this.isUpdateMode = isUpdateMode;
	}

	public String getProcedureTypeId() {
		if (viewMap.get("procedureTypeId") != null)
			procedureTypeId = (String) viewMap.get("procedureTypeId");

		return procedureTypeId;
	}

	public void setProcedureTypeId(String procedureTypeId) {
		this.procedureTypeId = procedureTypeId;
	}

	public List<SelectItem> getNolTypeItems() {
		return nolTypeItems;
	}

	public void setNolTypeItems(List<SelectItem> nolTypeItems) {
		this.nolTypeItems = nolTypeItems;
	}

	public String getSelectOneNOLType() {
		return selectOneNOLType;
	}

	public void setSelectOneNOLType(String selectOneNOLType) {
		this.selectOneNOLType = selectOneNOLType;
	}

	public HtmlSelectOneMenu getCmbNolType() {
		return cmbNolType;
	}

	public void setCmbNolType(HtmlSelectOneMenu cmbNolType) {
		this.cmbNolType = cmbNolType;
	}

	public HtmlSelectOneMenu getCmbFileType() {
		return cmbFileType;
	}

	public void setCmbFileType(HtmlSelectOneMenu cmbFileType) {
		this.cmbFileType = cmbFileType;
	}
	public boolean isSelectedProcedureTypeIsOpenFile() {
		boolean flag = false;

		try {
			if (procedureTypeId != null && !procedureTypeId.trim().equals("")) {
				Long _procedureTypeId = Long.valueOf(procedureTypeId);
				UtilityManager utilityManager = new UtilityManager();
				DomainData domainDataProcedureType_OpenFile = null;
				if (viewMap.containsKey("DD_PROCEDURE_TYPE_OPEN_FILE")) {
					domainDataProcedureType_OpenFile = (DomainData) viewMap
							.get("DD_PROCEDURE_TYPE_OPEN_FILE");
				} else {
					domainDataProcedureType_OpenFile = utilityManager
							.getDomainDataByValue(
									WebConstants.PROCEDURE_TYPE,
									WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
					viewMap.put("DD_PROCEDURE_TYPE_OPEN_FILE",
							domainDataProcedureType_OpenFile);
				}

				if (domainDataProcedureType_OpenFile.getDomainDataId().compareTo(
						_procedureTypeId) == 0)
					flag = true;
			}
		} catch (Exception ex) {
		}
		return flag;
	}
}
