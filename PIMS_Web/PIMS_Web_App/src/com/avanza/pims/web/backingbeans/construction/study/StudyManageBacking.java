package com.avanza.pims.web.backingbeans.construction.study;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.bpel.proxy.pimsmanagestudies.proxy.PIMSManageStudiesBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RecommendationServiceAgent;
import com.avanza.pims.business.services.StudyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.StrategicPlan;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.Investment.Opportunities.StrategicPlanSearch;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RecommendationView;
import com.avanza.pims.ws.vo.RelatedStudiesView;
import com.avanza.pims.ws.vo.StrategicPlanStudyView;
import com.avanza.pims.ws.vo.StrategicPlanView;
import com.avanza.pims.ws.vo.StudyDetailView;
import com.avanza.pims.ws.vo.StudyParticipantView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;

public class StudyManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	
	private InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent;
	private RecommendationServiceAgent recommendationServiceAgent;
	private List<RecommendationView> recommendationViewList;
	private HtmlDataTable recommendationDataTable;
	private String pageMode;
	private StudyServiceAgent studyServiceAgent;
	private StudyDetailView studyDetailView;
	private List<RelatedStudiesView> relatedStudiesViewList;
	private HtmlDataTable relatedStudiesDataTable;
	private List<UserView> userViewList;
	private List<PersonView> personViewList;
	private List<StudyParticipantView> studyParticipantViewList;
	private HtmlDataTable studyParticipantDataTable;
	private SystemParameters parameters;
	private UserTask userTask;
	private final String noteOwner = "STUDY_MANAGE";
	private final String externalId = "Study Manage";
	private final String procedureTypeKey = "STUDY_MANAGE";
	public String remarks;
	private StrategicPlanStudyView strategicPlanStudyView;
		
	public StudyManageBacking() 
	{
		investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
		recommendationServiceAgent = new RecommendationServiceAgent();
		recommendationViewList = new ArrayList<RecommendationView>(0);
		recommendationDataTable = new HtmlDataTable();
		logger = Logger.getLogger(StudyManageBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		studyDetailView = new StudyDetailView();
		relatedStudiesViewList = new ArrayList<RelatedStudiesView>(0);
		studyParticipantViewList = new ArrayList<StudyParticipantView>(0);
		userViewList = new ArrayList<UserView>(0);
		personViewList = new ArrayList<PersonView>(0);
		relatedStudiesDataTable = new HtmlDataTable();
		studyParticipantDataTable = new HtmlDataTable();
		studyServiceAgent = new StudyServiceAgent();
		commonUtil = new CommonUtil();
		pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_ADD_STUDY;
		parameters = SystemParameters.getInstance();
		userTask = new UserTask();
		strategicPlanStudyView = new StrategicPlanStudyView();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			super.init();
			DomainDataView ddvStudyStatusNew = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Study.STUDY_STATUS), WebConstants.Study.STUDY_STATUS_NEW);
			studyDetailView.setStatusId( String.valueOf( ddvStudyStatusNew.getDomainDataId() ) );
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}
		else if ( viewMap.get( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}		
		
		// STUDY DETAIL VIEW
		if ( sessionMap.get( WebConstants.Study.STUDY_DETAIL_VIEW ) != null )
		{
			studyDetailView = (StudyDetailView) sessionMap.get( WebConstants.Study.STUDY_DETAIL_VIEW );
			sessionMap.remove( WebConstants.Study.STUDY_DETAIL_VIEW );
			if(studyDetailView.getStudyId()!=null)
			CommonUtil.loadAttachmentsAndComments(studyDetailView.getStudyId().toString());
		}
		else if ( viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW ) != null )
		{
			studyDetailView = (StudyDetailView) viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW );
			if(studyDetailView.getStudyId()!=null)
			CommonUtil.loadAttachmentsAndComments(studyDetailView.getStudyId().toString());
		}
		
		// USER TASK
		if ( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		}
		else if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			studyDetailView = studyServiceAgent.getStudyById( new Long( userTask.getTaskAttributes().get( WebConstants.Study.TASK_ATTRIBUTE_STUDY_ID ) ) );
			
			if ( userTask.getTaskAttributes().get( WebConstants.Study.TASK_ATTRIBUTE_TASK_TYPE ) != null 
					&& ! userTask.getTaskAttributes().get( WebConstants.Study.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase("")
					&& userTask.getTaskAttributes().get( WebConstants.Study.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.Study.TASK_TYPE_APPROVE_REJECT_STUDY ) )
				pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_APPROVE_REJECT_STUDY;
			else
				pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_COMPLETE_STUDY;
		}
		
		// RECOMMENDATIONS LIST
		if ( viewMap.get( WebConstants.Study.RECOMMENDATIONS_LIST ) != null  )
		{
			recommendationViewList = (List<RecommendationView>) viewMap.get( WebConstants.Study.RECOMMENDATIONS_LIST );
		}
		else if ( studyDetailView != null && studyDetailView.getStudyId() != null )
		{
			RecommendationView criteriaRecommendationView = new RecommendationView();
			criteriaRecommendationView.setStudyDetailView( studyDetailView );
			criteriaRecommendationView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			criteriaRecommendationView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
			recommendationViewList = recommendationServiceAgent.getRecommendationList( criteriaRecommendationView );
		}
		
		// RELATED STUDIES LIST
		if ( viewMap.get( WebConstants.Study.RELATED_STUDIES_LIST ) != null  )
		{
			relatedStudiesViewList = (List<RelatedStudiesView>) viewMap.get( WebConstants.Study.RELATED_STUDIES_LIST );
		}
		else if ( studyDetailView != null && studyDetailView.getStudyId() != null )
		{
			RelatedStudiesView relatedStudiesView = new RelatedStudiesView();
			relatedStudiesView.setStudyByStudyId( studyDetailView );
			relatedStudiesView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			relatedStudiesView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
			relatedStudiesViewList = studyServiceAgent.getRelatedStudiesList(relatedStudiesView);
		}
		
		// STRATEGIC PLAN STUDY VIEW
		if ( sessionMap.get( WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW ) != null )
		{
			strategicPlanStudyView.setStrategicPlanView( ( StrategicPlanView ) sessionMap.get( WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW ) );
			sessionMap.remove( WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW );
		}
		else if ( viewMap.get( WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW ) != null )
		{
			strategicPlanStudyView = (StrategicPlanStudyView) viewMap.get( WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW );
		}	
		
		
		if(sessionMap.containsKey(WebConstants.TEAM_DATA_LIST)){
			populateInternalParticipantList(); 
		}
		else if(sessionMap.containsKey(WebConstants.SELECTED_PERSONS_LIST)){
			populateExternalParticipantList(); 
		}
		else if ( viewMap.get( "STUDY_PARTICIPANT_LIST")!=null 
				&&((List<StudyParticipantView>)viewMap.get( "STUDY_PARTICIPANT_LIST")).size()>0	)
		{
			studyParticipantViewList = (List<StudyParticipantView>) viewMap.get( "STUDY_PARTICIPANT_LIST");
		}
		else if ( studyDetailView != null && studyDetailView.getStudyId() != null )
		{
			studyParticipantViewList  = studyServiceAgent.getStudiesParticipantList(studyDetailView);			
		}
			
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		
		// STUDY DETAIL VIEW
		if ( studyDetailView != null )
		{
			viewMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailView );
		}
		
		// RECOMMENDATIONS LIST
		if ( recommendationViewList != null )
		{
			viewMap.put( WebConstants.Study.RECOMMENDATIONS_LIST, recommendationViewList );
		}
		
		// RELATED STUDIES LIST
		if ( relatedStudiesViewList != null  )
		{
			viewMap.put( WebConstants.Study.RELATED_STUDIES_LIST, relatedStudiesViewList );
		}
		if(studyParticipantViewList !=null){
			viewMap.put( "STUDY_PARTICIPANT_LIST", studyParticipantViewList );
		}
		
		// USER TASK
		if ( userTask != null )
		{
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		}
		
		// STRATEGIC PLAN STUDY VIEW
		if ( strategicPlanStudyView != null )
		{
			viewMap.put( WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW, strategicPlanStudyView );
		}
		
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "studySearch";
		return path;
	}
	
	public String onSend() 
	{
		String METHOD_NAME = "onSend()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{	
			PIMSManageStudiesBPELPortClient portClient = new PIMSManageStudiesBPELPortClient();
			portClient.setEndpoint( parameters.getParameter( WebConstants.Study.MANAGE_STUDIES_BPEL_END_POINT ) );
			portClient.initiate(studyDetailView.getStudyId(), CommonUtil.getLoggedInUser(), null, null);
			afterBPELActions();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Study.STUDY_REQUEST_SENT_SUCCESS ) );
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskApprove()
	{
		String METHOD_NAME = "onTaskApprove()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			 if(remarks!=null && remarks.trim().length()>0)
			{	
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCompleteTask(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
			afterBPELActions();
			CommonUtil.saveRemarksAsComments(studyDetailView.getStudyId(), remarks, noteOwner);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Study.STUDY_REQUEST_APPROVE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
			else
			  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskReject()
	{
		String METHOD_NAME = "onTaskReject()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			 if(remarks!=null && remarks.trim().length()>0)
			{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCompleteTask(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
			afterBPELActions();
			CommonUtil.saveRemarksAsComments(studyDetailView.getStudyId(), remarks, noteOwner);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Study.STUDY_REQUEST_REJECT_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	
	public String onTaskComplete()
	{
		String METHOD_NAME = "onTaskComplete()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			
			if(remarks !=null && remarks.trim().length()>0)
			{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCompleteTask(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
			afterBPELActions();
			CommonUtil.saveRemarksAsComments(studyDetailView.getStudyId(), remarks, noteOwner);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Study.STUDY_REQUEST_COMPLETE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		    }else
		     errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	private void approveRejectCompleteTask(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception 
	{
		String METHOD_NAME = "ApproveRejectTask()";		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");				
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
	}
	
	public String onTaskCancel() 
	{
		String path = null;
		path = "taskList";
		return path;
	}
	
	private void afterBPELActions() throws PimsBusinessException
	{
		studyDetailView = studyServiceAgent.getStudyById( studyDetailView.getStudyId() );
		pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_VIEW_STUDY;
		updateValuesToMaps();
	}
	
	private Boolean validate() 
	{
		Boolean isValid = true;
		
		if ( studyDetailView.getStartDate() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_STUDY_DATE ) );			
		}
		
		if ( studyDetailView.getEndDate() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_COMPLETION_DATE ) );
		}
		
		if ( studyDetailView.getStudyTypeId() == null || studyDetailView.getStudyTypeId().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_STUDY_TYPE ) );
		}
		
		if ( studyDetailView.getStudySubjectId() == null || studyDetailView.getStudySubjectId().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_STUDY_SUBJECT ) );
		}
		
		if ( studyDetailView.getStudyTitle() == null || studyDetailView.getStudyTitle().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_STUDY_TITLE ) );
		}
		
		if ( studyDetailView.getStudyPurpose() == null || studyDetailView.getStudyPurpose().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_STUDY_PURPOSE ) );
		}
		
		if ( studyDetailView.getRequestedBy() == null || studyDetailView.getRequestedBy().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_REQUESTED_BY ) );
		}
		
		if ( studyDetailView.getDescription() == null || studyDetailView.getDescription().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( MessageConstants.Study.LBL_DESCRIPTION ) );
		}
		
		return isValid;
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validate() )
			{
				isSuccess = saveStudyDetailView(studyDetailView);
				isSuccess = saveRelatedStudiesViewList(relatedStudiesViewList);
				isSuccess = saveRecommendationViewList(recommendationViewList);
				isSuccess = saveStudiesParticipantsViewList(studyParticipantViewList);
				
				if (isSuccess)
				{
					pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_UPDATE_STUDY;
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_SAVE_SUCCESS) );
					
					CommonUtil.loadAttachmentsAndComments(studyDetailView.getStudyId().toString());     
					Boolean attachSuccess = CommonUtil.saveAttachments(studyDetailView.getStudyId());
					Boolean commentSuccess = CommonUtil.saveComments(studyDetailView.getStudyId(), noteOwner);
					
					logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
				}
				
			}
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_SAVE_ERROR) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveStudyDetailView( StudyDetailView studyDetailView ) throws PimsBusinessException
	{
		Boolean isSuccess = false;		
		
		if ( studyDetailView.getStudyId() == null )
		{
			studyDetailView.setCreatedBy( CommonUtil.getLoggedInUser() );
			studyDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = studyServiceAgent.addStudy( studyDetailView );
		}
		else
		{
			studyDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );			
			isSuccess = studyServiceAgent.updateStudy( studyDetailView );			
		}
		
		return isSuccess;
	}
	
	private Boolean saveRelatedStudiesViewList( List<RelatedStudiesView> relatedStudiesViewList ) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( relatedStudiesViewList != null && relatedStudiesViewList.size() > 0 ) 
		{	
			for ( RelatedStudiesView currentRelatedStudiesView : relatedStudiesViewList )
			{
				if ( currentRelatedStudiesView.getRelatedStudiesId() == null )
				{
					// Add
					currentRelatedStudiesView.setCreatedBy( CommonUtil.getLoggedInUser() );
					currentRelatedStudiesView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					currentRelatedStudiesView.setStudyByStudyId( studyDetailView );
					isSuccess = studyServiceAgent.addRelatedStudies( currentRelatedStudiesView );
				}
				else if ( currentRelatedStudiesView.getIsDeleted().equals(1L) )
				{	
					// Deleted
					currentRelatedStudiesView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = studyServiceAgent.updateRelatedStudies( currentRelatedStudiesView );
				}
				else
				{
					// No change
					isSuccess = true;
				}
			}
		}
		else
		{
			isSuccess = true;
		}
		
		return isSuccess;
	}
	
	private Boolean saveRecommendationViewList( List<RecommendationView> recommendationViewList ) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( recommendationViewList != null && recommendationViewList.size() > 0 )
		{
			for ( RecommendationView currentRecommendationView : recommendationViewList )
			{
				if ( currentRecommendationView.getRecommendationId() == null )
				{
					// Add
					currentRecommendationView.setCreatedBy( CommonUtil.getLoggedInUser() );
					currentRecommendationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					currentRecommendationView.setStudyDetailView( studyDetailView );					
					isSuccess = recommendationServiceAgent.addRecommendation( currentRecommendationView );
				}
				else 
				{
					// Update
					currentRecommendationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = recommendationServiceAgent.updateRecommendation( currentRecommendationView );
				}				
			}
		}
		else
		{
			isSuccess = true;
		}
		
		return isSuccess;
	}
	
	public String onAddToInvesmentPlan()
	{
		String METHOD_NAME = "onAddToInvesmentPlan()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			strategicPlanStudyView.setStudyDetailView( studyDetailView );
			strategicPlanStudyView.setCreatedBy( CommonUtil.getLoggedInUser() );
			strategicPlanStudyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			
			isSuccess = investmentOpportunityServiceAgent.addStrategicPlanStudyView( strategicPlanStudyView );
				
			if (isSuccess)
			{	
				pageMode = WebConstants.Study.STUDY_MANAGE_PAGE_MODE_VIEW_STUDY; 
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Study.STUDY_SAVE_SUCCESS ) );
				
				CommonUtil.loadAttachmentsAndComments(studyDetailView.getStudyId().toString());     
				Boolean attachSuccess = CommonUtil.saveAttachments(studyDetailView.getStudyId());
				Boolean commentSuccess = CommonUtil.saveComments(studyDetailView.getStudyId(), noteOwner);
				
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}				
			
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}	
	
	public String onAddRecommendations() 
	{
		String path = null;
		openPopup("openRecommendationManagePopup();");
		return path;
	}
	
	public String onEditRecommendation()
	{
		String path = null;
		RecommendationView selectedRecommendationView = (RecommendationView) recommendationDataTable.getRowData();		
		selectedRecommendationView.setIsForUpdate( true );
		selectedRecommendationView.setStoredHashCode( selectedRecommendationView.hashCode() );
		sessionMap.put( WebConstants.Study.RECOMMENDATION, selectedRecommendationView );
		openPopup("openRecommendationManagePopup();");
		updateValuesToMaps();
		return path;
	}
	
	public String receiveRecommendation()
	{	
		if ( sessionMap.get( WebConstants.Study.RECOMMENDATION ) != null )
		{
			RecommendationView receivedRecommendationView = (RecommendationView) sessionMap.get( WebConstants.Study.RECOMMENDATION );			
			
			sessionMap.remove( WebConstants.Study.RECOMMENDATION );
			
			if ( receivedRecommendationView.getIsForUpdate() )
			{
				for ( RecommendationView currentRecommendationView : recommendationViewList  )
				{
					if ( currentRecommendationView.getStoredHashCode() != null &&  currentRecommendationView.getStoredHashCode().equals( receivedRecommendationView.getStoredHashCode() ) )
					{	
						// Already in the list
						currentRecommendationView.setRecommendationDate( receivedRecommendationView.getRecommendationDate() );
						currentRecommendationView.setRemark( receivedRecommendationView.getRemark() );
						currentRecommendationView.setRecommendationUser( CommonUtil.getLoggedInUser() );
						currentRecommendationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
						currentRecommendationView.setUpdatedOn( new Date() );
						currentRecommendationView.setIsForUpdate( false );
						currentRecommendationView.setStoredHashCode( null );
					}
				}
			}
			else
			{
				// Not in the list
				receivedRecommendationView.setStudyDetailView( studyDetailView );
				receivedRecommendationView.setRecommendationUser( CommonUtil.getLoggedInUser() );
				receivedRecommendationView.setCreatedBy( CommonUtil.getLoggedInUser() );
				receivedRecommendationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				receivedRecommendationView.setCreatedOn( new Date() );
				receivedRecommendationView.setUpdatedOn( new Date() );
				receivedRecommendationView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
				receivedRecommendationView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
				recommendationViewList.add( receivedRecommendationView );
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String onDeleteRecommendation() 
	{	
		String path = null;
		
		RecommendationView selectedRecommendationView = (RecommendationView) recommendationDataTable.getRowData();
		
		if ( selectedRecommendationView.getRecommendationId() == null )
			recommendationViewList.remove( selectedRecommendationView );
		else
			selectedRecommendationView.setIsDeleted(1L);
		
		updateValuesToMaps();
		
		return path;
	}
	
	public Integer getRecommendationsRecordSize()
	{
		int size = 0;
		
		for ( RecommendationView currentRecommendationView :  recommendationViewList )
		{
			if ( currentRecommendationView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				size++;
		}
		return size;
	}
	
	public String onAddStudies() 
	{
		String path = null;
		sessionMap.put( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY, WebConstants.Study.STUDY_SEARCH_PAGE_MODE_POPUP_MULTI );
		String javascript = "openStudySearchPopup();";
		openPopup(javascript);
		return path;
	}
	
	@SuppressWarnings("unchecked")
	public String receiveSelectedStudies()
	{
		if ( sessionMap.get( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST ) != null )
		{
			// Selected studies from search studies popup
			List<StudyDetailView> selectedStudyDetailView = (List<StudyDetailView>) sessionMap.get( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST );
			sessionMap.remove( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST );			
			
			Map<Long, StudyDetailView> alreadyContainedStudyMap = new HashMap(0);		

			// All those studies which already exits in the list should not be added again
			for ( RelatedStudiesView currentRelatedStudiesView : relatedStudiesViewList )
			{
				if ( currentRelatedStudiesView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				{
					alreadyContainedStudyMap.put( currentRelatedStudiesView.getStudyByChildStudyId().getStudyId(), currentRelatedStudiesView.getStudyByChildStudyId() );
				}
			}
			
			// If a study tries to add itself in the related study it should not be able to do so
			if( studyDetailView != null && studyDetailView.getStudyId() != null )
			{
				alreadyContainedStudyMap.put(studyDetailView.getStudyId(), studyDetailView);
			}
			
			for ( StudyDetailView currentStudyDetailView : selectedStudyDetailView )
			{
				if ( alreadyContainedStudyMap.get( currentStudyDetailView.getStudyId() ) == null )
				{
					RelatedStudiesView newRelatedStudiesView = new RelatedStudiesView();
					newRelatedStudiesView.setStudyByChildStudyId( currentStudyDetailView );
					newRelatedStudiesView.setStudyByStudyId( studyDetailView );
					newRelatedStudiesView.setCreatedBy( CommonUtil.getLoggedInUser() );
					newRelatedStudiesView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					newRelatedStudiesView.setCreatedOn( new Date() );
					newRelatedStudiesView.setUpdatedOn( new Date() );
					newRelatedStudiesView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
					newRelatedStudiesView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
					relatedStudiesViewList.add( newRelatedStudiesView );
				}
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String onStudyView() 
	{
		String path = null;
		RelatedStudiesView selectedRelatedStudiesView = (RelatedStudiesView) relatedStudiesDataTable.getRowData();
		sessionMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, selectedRelatedStudiesView.getStudyByChildStudyId() );
		sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY );
		String javascript = "openStudyViewPopup();";
		openPopup(javascript);
		return path;
	}
	
	public String onStudyDelete() 
	{
		String path = null;
		RelatedStudiesView selectedRelatedStudiesView = (RelatedStudiesView) relatedStudiesDataTable.getRowData();
		if ( selectedRelatedStudiesView.getRelatedStudiesId() == null )
			relatedStudiesViewList.remove( selectedRelatedStudiesView );	
		else
			selectedRelatedStudiesView.setIsDeleted(1L);		
		updateValuesToMaps();		
		return path;
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Integer getRelatedStudiesRecordSize() 
	{
		int size = 0;
		
		for ( RelatedStudiesView currentRelatedStudiesView :  relatedStudiesViewList )
		{
			if ( currentRelatedStudiesView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				size++;
		}
		return size;
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_UPDATE_STUDY ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsAddToInvestmentPlanMode() 
	{ 
		Boolean isAddToInvestmentPlanMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_ADD_TO_INVESTMENT_PLAN ) )
			isAddToInvestmentPlanMode = true;
		
		return isAddToInvestmentPlanMode;
	}
	
	public Boolean getIsApproveRejectStudyMode()
	{
		Boolean isApproveRejectStudyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_APPROVE_REJECT_STUDY ) )
			isApproveRejectStudyMode = true;
		 
		return isApproveRejectStudyMode;
	}
	
	public Boolean getIsCompleteStudyMode() 
	{
		Boolean isCompleteStudyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_COMPLETE_STUDY ) )
			isCompleteStudyMode = true;
		
		return isCompleteStudyMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_VIEW_STUDY ) )
			isViewMode = true;
		
		return isViewMode;
	}
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY ) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public void showHideAttachmentAndCommentsBtn()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_ADD_STUDY ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_UPDATE_STUDY ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_APPROVE_REJECT_STUDY ) )
			canAddAttachmentsAndComments(false);
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_COMPLETE_STUDY ) )
			canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_VIEW_STUDY ) )	
	    	canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY ) )	
	    	canAddAttachmentsAndComments(false);
		
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public StudyDetailView getStudyDetailView() {
		return studyDetailView;
	}

	public void setStudyDetailView(StudyDetailView studyDetailView) {
		this.studyDetailView = studyDetailView;
	}

	public StudyServiceAgent getStudyServiceAgent() {
		return studyServiceAgent;
	}

	public void setStudyServiceAgent(StudyServiceAgent studyServiceAgent) {
		this.studyServiceAgent = studyServiceAgent;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public List<RelatedStudiesView> getRelatedStudiesList() {
		return relatedStudiesViewList;
	}

	public void setRelatedStudiesList(List<RelatedStudiesView> relatedStudiesList) {
		this.relatedStudiesViewList = relatedStudiesList;
	}

	public List<RelatedStudiesView> getRelatedStudiesViewList() {
		return relatedStudiesViewList;
	}

	public void setRelatedStudiesViewList(
			List<RelatedStudiesView> relatedStudiesViewList) {
		this.relatedStudiesViewList = relatedStudiesViewList;
	}

	public HtmlDataTable getRelatedStudiesDataTable() {
		return relatedStudiesDataTable;
	}

	public void setRelatedStudiesDataTable(HtmlDataTable relatedStudiesDataTable) {
		this.relatedStudiesDataTable = relatedStudiesDataTable;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public List<RecommendationView> getRecommendationViewList() {
		return recommendationViewList;
	}

	public void setRecommendationViewList(
			List<RecommendationView> recommendationViewList) {
		this.recommendationViewList = recommendationViewList;
	}

	public HtmlDataTable getRecommendationDataTable() {
		return recommendationDataTable;
	}

	public void setRecommendationDataTable(HtmlDataTable recommendationDataTable) {
		this.recommendationDataTable = recommendationDataTable;
	}

	public RecommendationServiceAgent getRecommendationServiceAgent() {
		return recommendationServiceAgent;
	}

	public void setRecommendationServiceAgent(
			RecommendationServiceAgent recommendationServiceAgent) {
		this.recommendationServiceAgent = recommendationServiceAgent;
	}
	
	// Shiraz Code Start Here 
	
	public void onAddMember(){
	
	final String viewId = "/studyManage.jsf";

	FacesContext facesContext = FacesContext.getCurrentInstance();

	// This is the proper way to get the view's url
	ViewHandler viewHandler = facesContext.getApplication()
			.getViewHandler();
	@SuppressWarnings("unused")
	String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	String javaScriptText = "javascript:SearchMemberPopup();";

	// Add the Javascript to the rendered page's header for immediate
	// execution
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,
			AddResource.HEADER_BEGIN, javaScriptText);
	}

	public List<StudyParticipantView> getStudyParticipantViewList() {
		return studyParticipantViewList;
	}

	public void setStudyParticipantViewList(List<StudyParticipantView> studyParticipantViewList) {
		this.studyParticipantViewList = studyParticipantViewList;

	}

	public HtmlDataTable getStudyParticipantDataTable() {
		return studyParticipantDataTable;
	}

	public void setStudyParticipantDataTable(HtmlDataTable studyParticipantDataTable) {
		this.studyParticipantDataTable = studyParticipantDataTable;
	}
	
	@SuppressWarnings("unchecked")
	private void populateInternalParticipantList(){
		StudyParticipantView studyParticipantView = new StudyParticipantView();
		Map<String, StudyParticipantView> alreadyContainedParticipantMap = new HashMap<String, StudyParticipantView>(0);
	
		if(sessionMap.containsKey(WebConstants.TEAM_DATA_LIST)){
		
			userViewList  = (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST);
			sessionMap.remove(WebConstants.TEAM_DATA_LIST);
			
			
				if(viewMap.containsKey( "STUDY_PARTICIPANT_LIST")){
					studyParticipantViewList = (List<StudyParticipantView>)viewMap.get( "STUDY_PARTICIPANT_LIST");
					for(StudyParticipantView sPV:studyParticipantViewList){
						if(sPV.getLoginId()!=null && sPV.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ))
						alreadyContainedParticipantMap.put(sPV.getLoginId(), sPV);
					}
				}
	          
			    for(UserView userView: userViewList){
			    	
			       if(alreadyContainedParticipantMap.get(userView.getUserName())==null){
						 studyParticipantView = new StudyParticipantView();
						 studyParticipantView.setPersonFullName(userView.getFullNameSecondary());
						 studyParticipantView.setEmail(userView.getEmailURL());
						 studyParticipantView.setLoginId(userView.getUserName());
						 studyParticipantView.setIsDeleted(0L);
						 studyParticipantViewList.add(studyParticipantView);
			       }
			     }
	     }
	   updateValuesToMaps();
    }
	

	@SuppressWarnings("unchecked")
	private void populateExternalParticipantList(){
		StudyParticipantView studyParticipantView = new StudyParticipantView();
		Map<Long, StudyParticipantView> alreadyContainedParticipantMap = new HashMap(0);
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		try {
	
	  if(sessionMap.containsKey(WebConstants.SELECTED_PERSONS_LIST)){
		
		  personViewList  = (List<PersonView>)sessionMap.get(WebConstants.SELECTED_PERSONS_LIST);
			sessionMap.remove(WebConstants.SELECTED_PERSONS_LIST);
			
			
				if(viewMap.containsKey( "STUDY_PARTICIPANT_LIST")){
					studyParticipantViewList = (List<StudyParticipantView>)viewMap.get( "STUDY_PARTICIPANT_LIST");
					for(StudyParticipantView sPV:studyParticipantViewList){
						if(sPV.getPersonId()!=null && sPV.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ))
						alreadyContainedParticipantMap.put(sPV.getPersonId(), sPV);	
					}
				}
	          
			    for(PersonView personView: personViewList){
			    	
			       if(alreadyContainedParticipantMap.get(personView.getPersonId())==null){
						 psa.getPersonInformation(personView.getPersonId());
						 emailList = getEmailContactInfos(personView);
						 studyParticipantView = new StudyParticipantView();
						 studyParticipantView.setPersonFullName(personView.getPersonFullName());
						 studyParticipantView.setPersonId(personView.getPersonId());
						 studyParticipantView.setIsDeleted(0L);
						 if(emailList.size()>0)
						 studyParticipantView.setEmail(emailList.get(0).getEmail());
						 studyParticipantViewList.add(studyParticipantView);
			       }
			     }
	  
    }
	
     } catch (PimsBusinessException e) {
	       logger.LogException("Exception Occure::::::::::::", e);
	       
			
		}
	 
	 updateValuesToMaps();
	}	
	

	
	@SuppressWarnings("unchecked")
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}
	public int getParticipantsGridRecordSize() {  	  
	  
	  int size = 0;
		
		for ( StudyParticipantView current :  studyParticipantViewList )
		{
			if ( current.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				size++;
		}
		return size;
	 }
	
	
	
	@SuppressWarnings("unchecked")
	private Boolean saveStudiesParticipantsViewList( List<StudyParticipantView> studyPartView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		if(viewMap.containsKey( "STUDY_PARTICIPANT_LIST")){
		studyParticipantViewList = (List<StudyParticipantView>)viewMap.get( "STUDY_PARTICIPANT_LIST");	
		 if ( studyParticipantViewList != null && studyParticipantViewList.size() > 0 ) 
		  {	
			for ( StudyParticipantView studyPartyView : studyParticipantViewList )
			{
				if ( studyPartyView.getStudyPartcipantId() == null )
				{
					studyPartyView.setCreatedBy( CommonUtil.getLoggedInUser() );
					studyPartyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					studyPartyView.setStudyView( studyDetailView );
					isSuccess = studyServiceAgent.addStudiesParticipants( studyPartyView );
				}
				else if ( studyPartyView.getIsDeleted().equals(1L) )
				{	
					studyPartyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = studyServiceAgent.updateStudiesParticipants( studyPartyView );
				}
				else
				{
					isSuccess = true;
				}
			}
		}
		else
		{
			isSuccess = true;
		}
	 }	
		return isSuccess;
	}
	
	public String onDeleteParticipant() 
	{
		String path = null;
		
		StudyParticipantView selectedStudyParticipantView = (StudyParticipantView) studyParticipantDataTable.getRowData();		
		
		if ( selectedStudyParticipantView.getStudyPartcipantId() == null )
			studyParticipantViewList.remove( selectedStudyParticipantView );
		else
			selectedStudyParticipantView.setIsDeleted(1L);		
		
		updateValuesToMaps();
		
		return path;
	}

	public List<UserView> getUserViewList() {
		return userViewList;
	}

	public void setUserViewList(List<UserView> userViewList) {
		this.userViewList = userViewList;
	}

	public List<PersonView> getPersonViewList() {
		return personViewList;
	}

	public void setPersonViewList(List<PersonView> personViewList) {
		this.personViewList = personViewList;
	}

	public SystemParameters getParameters() {
		return parameters;
	}

	public void setParameters(SystemParameters parameters) {
		this.parameters = parameters;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public StrategicPlanStudyView getStrategicPlanStudyView() {
		return strategicPlanStudyView;
	}

	public void setStrategicPlanStudyView(
			StrategicPlanStudyView strategicPlanStudyView) {
		this.strategicPlanStudyView = strategicPlanStudyView;
	}

	public String getNoteOwner() {
		return noteOwner;
	}

	public String getExternalId() {
		return externalId;
	}

	public String getProcedureTypeKey() {
		return procedureTypeKey;
	}

	public InvestmentOpportunityServiceAgent getInvestmentOpportunityServiceAgent() {
		return investmentOpportunityServiceAgent;
	}

	public void setInvestmentOpportunityServiceAgent(
			InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent) {
		this.investmentOpportunityServiceAgent = investmentOpportunityServiceAgent;
	}
	
	// Shiraz Code End Here
}
