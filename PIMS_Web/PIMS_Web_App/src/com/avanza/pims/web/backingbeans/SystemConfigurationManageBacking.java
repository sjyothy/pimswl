package com.avanza.pims.web.backingbeans;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.SystemConfiguration;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.Investment.Asset.AssetSearchBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.MessageResourceView;
import com.avanza.pims.ws.vo.SystemConfigurationView;
import com.avanza.ui.util.ResourceUtil;


public class SystemConfigurationManageBacking extends AbstractController {

	
	SystemConfigurationView systemConfigurationView;
	List<SystemConfigurationView> systemConfigurationViewList;
	private HtmlDataTable tbl_systemConfiguration;
	PropertyServiceAgent propertyServiceAgent;
	private HashMap searchMap;
	private static Logger logger ;
	private Map<String,Object> viewMap;
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	private List<String> errorMessages ;
	private List<String> infoMessages ;
	
	public SystemConfigurationManageBacking() 
	{
		systemConfigurationView = new SystemConfigurationView();
		systemConfigurationViewList = new ArrayList<SystemConfigurationView>();
		tbl_systemConfiguration = new HtmlDataTable();
		propertyServiceAgent =new PropertyServiceAgent();
		searchMap = new HashMap();
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		logger = Logger.getLogger(SystemConfigurationManageBacking.class);
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();
	}
	
	
	
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		String METHOD_NAME = "init()";
		try
		{
		 updateValuesFromMaps();
		}catch (Exception e) {
		logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
		}
	}
	
	
	private void populateSystemConfiguration()
	{
		String METHOD_NAME = "populateSystemConfiguration()";
		
	  try{	
		
			systemConfigurationViewList =  propertyServiceAgent.getAllSystemConfiguration(searchMap);
			
			if(systemConfigurationViewList !=null && systemConfigurationViewList.size()>0 )
			{
				viewMap.put("recordSize", systemConfigurationViewList.size());
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize/paginatorRows;
				if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
				if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewMap.put("paginatorMaxPages", paginatorMaxPages);
			}
		
	   }catch (Exception e) 
	    {
			  logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);  
		}
		
	}

	private void updateValuesFromMaps() throws Exception
	{
		
		// System Configuration List
		if ( viewMap.get( WebConstants.SYSTEM_CONFIGURATION.SYSTEM_CONFIGURATION_LIST ) != null )
		{
		  systemConfigurationViewList = (List<SystemConfigurationView>) viewMap.get( WebConstants.SYSTEM_CONFIGURATION.SYSTEM_CONFIGURATION_LIST);
		 			
		}//for the first time only 
		else
		{
			populateSystemConfiguration();	
		}
		
		updateValuesToMaps();
	}
	
	private void updateValuesToMaps() 
	{
		// System Configuration List
		if ( systemConfigurationViewList != null )
		{
			viewMap.put( WebConstants.SYSTEM_CONFIGURATION.SYSTEM_CONFIGURATION_LIST, systemConfigurationViewList );
		}
		
	}
	
	public String onApplyChanges ()
	{
		String METHOD_NAME = "onApplyChanges()";		
		ApplicationContext applicationContext = ApplicationContext.getContext();
		List<SystemConfiguration> list = applicationContext.get( Constant.SYSTEM_CONFIGURATION_LIST );
		Boolean isChangesApplied = false;
	try{	
		
	   if(!hasError())
	   {   
			for(SystemConfigurationView sCV : systemConfigurationViewList)
			{
				if(sCV.getIsChange()!=null && sCV.getIsChange().equalsIgnoreCase("1"))
				{
					sCV.setConfigvalue(sCV.getConfigvalue().trim());
				    propertyServiceAgent.updateSystemConfiguration(sCV);
					sCV.setIsChange("0");
					isChangesApplied = true;
				}
			}
			if(isChangesApplied)
			{
			 infoMessages.add(ResourceUtil.getInstance().getProperty("systemConfigurationManage.msg.changesApplied"));
		     applicationContext.add(Constant.SYSTEM_CONFIGURATION_LIST, new ArrayList<SystemConfiguration>());
			}
		
	   }	
		
		
	  }catch (Exception e) 
	    {
		  logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);  
	   }
	  
	  return "";
	}
	public Boolean hasError()
	{
		Boolean flage = false;
		
		for(SystemConfigurationView sCV : systemConfigurationViewList)
		{
			if(sCV.getIsChange()!=null && sCV.getIsChange().equalsIgnoreCase("1"))
			{
			    if(sCV.getConfigvalue().trim().length()>500)
			    {
                 errorMessages.add(java.text.MessageFormat.format(ResourceUtil.getInstance().getProperty("systemConfigurationManage.msg.valueLenght"),sCV.getConfigkey()));
                 flage = true;
			    }
				
			}
		}
		
		
		return flage; 
	}
	public String onChange()
	{

		SystemConfigurationView sCV = (SystemConfigurationView)tbl_systemConfiguration.getRowData();
		sCV.setIsChange("1");
		return "";
	}
	
	public String onSearch()
	{ 
		String METHOD_NAME = "onSearch()";
		
	  try
	  {
		if(systemConfigurationView.getConfigkey()!=null &&  !systemConfigurationView.getConfigkey().equals(""))
		{
			searchMap.put(WebConstants.SYSTEM_CONFIGURATION.SYSTEM_CONFIGURATION_KEY, systemConfigurationView.getConfigkey());
		}
		if(systemConfigurationView.getConfigvalue()!=null &&  !systemConfigurationView.getConfigvalue().equals(""))
		{
			searchMap.put(WebConstants.SYSTEM_CONFIGURATION.SYSTEM_CONFIGURATION_VALUE, systemConfigurationView.getConfigvalue());
		}
		   
		systemConfigurationViewList =  propertyServiceAgent.getAllSystemConfiguration(searchMap);

		  	
		viewMap.put("recordSize", systemConfigurationViewList.size());
		updateValuesToMaps();
		
	  }catch (Exception e) 
	    {
		  logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
	    }
		return "";
		
	}


	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}



	public SystemConfigurationView getSystemConfigurationView() {
		return systemConfigurationView;
	}



	public void setSystemConfigurationView(
			SystemConfigurationView systemConfigurationView) {
		this.systemConfigurationView = systemConfigurationView;
	}



	public List<SystemConfigurationView> getSystemConfigurationViewList() {
		return systemConfigurationViewList;
	}



	public void setSystemConfigurationViewList(
			List<SystemConfigurationView> systemConfigurationViewList) {
		this.systemConfigurationViewList = systemConfigurationViewList;
	}



	public HtmlDataTable getTbl_systemConfiguration() {
		return tbl_systemConfiguration;
	}



	public void setTbl_systemConfiguration(HtmlDataTable tbl_systemConfiguration) {
		this.tbl_systemConfiguration = tbl_systemConfiguration;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	





}
