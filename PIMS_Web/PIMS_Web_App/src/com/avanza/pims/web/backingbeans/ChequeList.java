package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.ReadChequeStatusService;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.ui.util.ResourceUtil;


public class ChequeList extends AbstractController 
{

	private static final long serialVersionUID = 7597939644073192139L;
	// Fields
	private static Logger logger = Logger.getLogger(ChequeList.class);

	private HtmlDataTable dataTable;
	private List<SelectItem> paymentStatusList = new ArrayList<SelectItem>();
	private List<SelectItem> paymentMethodList = new ArrayList<SelectItem>();
	
	private BounceChequeView dataItem = new BounceChequeView();
	private List<BounceChequeView> dataList = new ArrayList<BounceChequeView>();
	private HtmlInputHidden tenantIdHidden = new HtmlInputHidden();
	private HtmlInputHidden hdnModeSelectOnePopUp = new HtmlInputHidden();
	private HtmlInputHidden hdnModeSetup = new HtmlInputHidden();
	private HtmlSelectOneMenu paymentMethodMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu paymentStatusMenu = new HtmlSelectOneMenu();
	
	private String hdnMode;
    private String invokingContext;
	private String name;
	private String passportNumber;
	private String residenceVisaNumber;
	private String licenseNumber;
	private String personalSecurityCardNo;
	private String selectTenantType;
	private String visaNumber;
	private String facilityId;
	private String personTenant;
	private String hdnTenantId;
	private String TENANT_INFO="TENANTINFO";
    private String inheritanceFileNumber;
    private String inheritanceFileOwner;
	private Date dueOnFrom;
	private Date dueOnTo;
	private Date dateTo;
	private Date dateFrom;
	private String receiptNumber;
	
	private Long bankId;
	private String methodRefNo;
	private String paymentNumber;
	private Long tenantId;
	private String tenantNumber;
	private String contractNumber;
	private String grpNumber;
	private String selectOneBank;
	private String hdnContractNumber;
	private String hdnIsFromEditCheque;
	private List<String> errorMessages = new ArrayList<String>();
	// private Boolean isEnglishLocale;

	List<SelectItem> bank = new ArrayList<SelectItem>();

	private String rentValue;
	private String selectOneFacilityType;

	// private String mode;
	private String modeSetup = "Mode_Setup";
	private String modeSelectOnePopUp = "Mode_Select_One_Pop_Up";

	private Boolean isModeSetup;
	private Boolean isModeSelectOnePopUp;

	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton realizedButton = new HtmlCommandButton();
	private HtmlCommandButton bouncedButton = new HtmlCommandButton();
	private HtmlInputText txtFileNumber = new HtmlInputText();
	private HtmlInputText txtTenantNumber = new HtmlInputText();
	private HtmlInputText txtTenantName = new HtmlInputText();
	private HtmlInputText txtTenantPassportNumber = new HtmlInputText();
	private HtmlInputText txtTenantVisaNumber = new HtmlInputText();
	private HtmlInputHidden addCount = new HtmlInputHidden();
	boolean isArabicLocale;
	boolean isEnglishLocale;
	TenantView tenantsView = new TenantView();
	FacilityView facilityView = new FacilityView();
	BounceChequeView paySchView = new BounceChequeView();
	HashMap searchMap = new HashMap();
	private Boolean replaceCheque = new Boolean(false);
	private Boolean notReplaceCheque = new Boolean(true);
	private HtmlInputHidden replaceChequeHidden = new HtmlInputHidden();
	private HtmlInputHidden notReplaceChequeHidden = new HtmlInputHidden();

	private DomainDataView ddvChequeStatusCollected=new DomainDataView();
	private DomainDataView ddvPaymentMethodCheque=new DomainDataView();
	private DomainDataView ddvGrpStatusPosted=new DomainDataView();
	CommonUtil comUtil = new CommonUtil();
	CommonUtil commonUtil = new CommonUtil();
	DomainDataView domianData = new DomainDataView();
	Map sessionMap;
	FacesContext context = getFacesContext();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	HttpServletRequest requestParam = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

	private String sortField = null;
	private boolean sortAscending = true;
	
	List<SelectItem> paymentTypeList = new ArrayList();
	private String selectOnePaymentType;
	private String selectOnePaymentStatus;
	private String selectOnePaymentMethod;
	private String selectOneOwnershipType;
	private Double paymentAmount;
	private Date transactionDate;
	private Date transactionDateTo;
	private Date chequeDate;
	private String propertyName;
	private String unitNumber;
	private String tenantName;
	private String  DEFAULT_SORT_FIELD = "paymentNumber";
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	private UploadedFile selectedFile = null;


	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		try
		{
			sessionMap = context.getExternalContext().getSessionMap();
			if(this.hdnIsFromEditCheque!=null && this.hdnIsFromEditCheque.trim().length()>0 && this.hdnIsFromEditCheque.compareTo("1")==0)
			{
				hdnIsFromEditCheque="0";
				loadDataList();
			}
			if ( !isPostBack() ) 
			{
				ddvChequeStatusCollected=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_COLLECTED);
				ddvPaymentMethodCheque=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE), WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
				ddvGrpStatusPosted=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.GRP.GRP_TRANSACTION_STATUS), WebConstants.GRP.GRP_POSTED);
				viewMap.put(WebConstants.PAYMENT_SCHEDULE_COLLECTED, ddvChequeStatusCollected);
				viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE, ddvPaymentMethodCheque);
				viewMap.put(WebConstants.GRP.GRP_POSTED, ddvGrpStatusPosted);
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
		        setSortItemListAscending(true);
				// errorMessages.clear();
		        
				if (requestParam.getParameter("modeReplaceCheque") != null)
				{
					modeReplaceCheque();
				}
				getDataFromQueryString();
				loadCombos();
			}
		}
		catch(Exception e)
		{
			errorMessages = new ArrayList<String>();
			logger.LogException("init|Exception Occured :",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	/**
	 * @throws Exception
	 */
	private void loadCombos() throws Exception 
	{
		loadPaymentScheduleStatusList();
		loadPaymentMethodList();
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void getDataFromQueryString() 
	{
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		if (request.getParameter("modeSelectOnePopUp") != null)
		{
			setHdnMode(modeSelectOnePopUp);
		}
		else
		{
			setHdnMode(modeSetup);
			setInvokingContext("LEFTMENU");
		}
		if (request.getParameter("context") != null)
		{
			setInvokingContext( request.getParameter("context")  );
		}
		else
		{
			setInvokingContext("LEFTMENU");
		}
		
		if (request.getParameter("fileId") != null)
		{
			InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, Long.valueOf(request.getParameter("fileId").toString() ));
			viewMap.put("fileNumber", file.getFileNumber());
		}
		

		
	}

	@SuppressWarnings("unchecked")
	public void loadPaymentScheduleStatusList() throws Exception 
	{
		
	    List<DomainDataView> ddList = new  UtilityService().getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_STATUS );
	    for (DomainDataView ddv : ddList) 
	    {
	    	boolean add=false;
	    	if(
	    			getInvokingContext().equals("MINOR_COLLECTIONS") && 
	    			(
	    					ddv.getDomainDataId().compareTo(WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_ID )== 0 ||
	    					ddv.getDomainDataId().compareTo(WebConstants.PAYMENT_SCHEDULE_COLLECTED_ID )== 0 
	    	  		)
	    	)
	    	{
	    		add = true;
	    	}
	    	else if (getInvokingContext().equals("LEFTMENU") )
	    	{
	    		add = true;
	    		
	    	}
	    	if( add )
	    	{
			SelectItem item = new SelectItem(
												ddv.getDomainDataId().toString(), 
												getIsEnglishLocale()? ddv.getDataDescEn():ddv.getDataDescAr()
											);
			paymentStatusList.add(item);
	    	}
		}
		Collections.sort(paymentStatusList,ListComparator.LIST_COMPARE);
		this.setPaymentStatusList( paymentStatusList);
			
	}
	
	@SuppressWarnings("unchecked")
	public void loadPaymentMethodList() throws Exception 
	{
		
	    List<DomainDataView> ddList = new  UtilityService().getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_MODE );
	    for (DomainDataView ddv : ddList) 
	    {
	    	boolean add=false;
	    	if(
	    			getInvokingContext().equals("MINOR_COLLECTIONS") && 
	    			(
	    			  ddv.getDataValue().compareTo(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE  )== 0 
	    	  		)
	    	)
	    	{
	    		add = true;
	    	}
	    	else if (getInvokingContext().equals("LEFTMENU") )
	    	{
	    		add = true;
	    		
	    	}
	    	if( add )
	    	{
			SelectItem item = new SelectItem(
												ddv.getDomainDataId().toString(), 
												isEnglishLocale? ddv.getDataDescEn():ddv.getDataDescAr()
											);
			paymentMethodList.add(item);
	    	}
		}
		Collections.sort(paymentMethodList,ListComparator.LIST_COMPARE);
		this.setPaymentMethodList( paymentMethodList ) ;
			
	}


	@SuppressWarnings("unchecked")
	public Boolean validateForReplaceCheuqe( BounceChequeView vo ) throws Exception
	{
	  Boolean hasError = false;
	  if( dataItem.getStopReplaceCheque().compareTo( 1L ) == 0 )
	  {
		  errorMessages.add(ResourceUtil.getInstance().getProperty("chequeList.msg.StopReplaceChequeTransactionDueToFollowUp"));
		  hasError = true;
	  }
	  return hasError;
	}
	@SuppressWarnings("unchecked")
	public String showReplaceCheque() 
	{
		try
		{
			errorMessages = new ArrayList<String>();
			dataItem = (BounceChequeView) dataTable.getRowData();
			if( !validateForReplaceCheuqe( dataItem  ) )
			{
			  FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW, dataItem);
			  if( dataItem.getRequestView() != null && 
				  dataItem.getRequestView().getRequestTypeId().compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT) ==  0 
				)
			  {
				  CollectionProcedureView collection = CollectionProcedureService.getNewCollectionsRequestForReplacementOfPaymentScheduleId( dataItem.getPaymentScheduleId() );
				  if(collection != null)
				  {
					  String msg = java.text.MessageFormat.format(
																   getBundleMessage("replaceCheque.msg.collectionRequestPresent"), 
																   collection.getRequestNumber()
																 );
					  errorMessages.add(msg);
					  return "";
				  }
				  return "replaceMinorsCollection";
			  }
			  else 
			  {
				  return "replaceCheque";  
			  }
			}
		}
		catch ( Exception e)
		{
			  logger.LogException("showReplaceCheque|Error Occured:", e );
			  errorMessages.add( getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		return "";
	}
	@SuppressWarnings("unchecked")
	public void cmdLnkRedepositCheque_Click()
	{
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put(WebConstants.BOUNCE_CHEQUE_REDEPOSIT , (BounceChequeView) dataTable.getRowData());
		
		String javaScriptText ="javaScript:showRedepositCheque();";
        
		AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
		addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
	
		
	}

	private void modeReplaceCheque() 
	{
		replaceCheque = true;
		notReplaceCheque = false;
		replaceChequeHidden.setValue("notnull");
		notReplaceChequeHidden.setValue("notnull");
		cancelButton.setRendered(false);
		bouncedButton.setRendered(false);
		realizedButton.setRendered(false);
	}

	public void preprocess() 
	{

		if (replaceChequeHidden.getValue() != null
				&& replaceChequeHidden.getValue().toString().trim().length() > 0) 
		{
			modeReplaceCheque();
		}
		getIsModeSelectOnePopUp();
	}

	public void prerender() {

		getIsModeSelectOnePopUp();
		
		if(hdnContractNumber!=null && !hdnContractNumber.equals("")){
			
			contractNumber = hdnContractNumber;
			hdnContractNumber = null;
		}
		if(this.hdnIsFromEditCheque!=null && this.hdnIsFromEditCheque.trim().length()>0 && this.hdnIsFromEditCheque.compareTo("1")==0)
		{
			hdnIsFromEditCheque="0";
			loadDataList();
		}
		
		try
    	{
		    	FillTenantInfo();
		    	if(viewMap.containsKey(TENANT_INFO))
			    {
				   PersonView tenantViewRow=(PersonView)viewMap.get(TENANT_INFO);
			       String tenantNames="";
			        if(tenantViewRow.getPersonFullName()!=null)
			        tenantNames=tenantViewRow.getPersonFullName();
			        if(tenantNames.trim().length()<=0)
			        	 tenantNames=tenantViewRow.getCompanyName();
			        txtTenantName.setValue(tenantNames);
			       
			    }
		    	   
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "prerender|Error Occured:",ex);		
    	}
    	if(	getInvokingContext().equals("MINOR_COLLECTIONS") )
    	{
//    		paymentMethodMenu.setReadonly(true);
//    		paymentStatusMenu.setReadonly(true);
    		txtFileNumber.setReadonly(true);
    		txtFileNumber.setStyleClass("READONLY");
    		txtFileNumber.setValue(  viewMap.get("fileNumber").toString() );
    	}
    					
	}

	private void FillTenantInfo() throws PimsBusinessException{
    	PersonView tenantView;
    	 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(this.getHdnTenantId()));
    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
    		if(tenantsList.size()>0)
    			viewMap.put(TENANT_INFO,tenantsList.get(0));
    			
    	}
    	
		
	}

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	public boolean getSortAscending() {
		return sortAscending;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors
	/** default constructor */
	public ChequeList() {

	}


	public HtmlDataTable getDataTable() {

		return dataTable;
	}

	public List<BounceChequeView> getGridDataList() {
		
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<BounceChequeView>)viewMap.get("ChequeList");
		if(dataList==null)
			dataList = new ArrayList<BounceChequeView>();
		

		return dataList;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}


	/*
	 * public String btnAdd_Click() { String sMethod="btnAdd_Click"; String
	 * eventOutCome="success"; return eventOutCome; }
	 */
	public String btnSearch_Click() {
		String eventOutCome = "failure";
		pageFirst();
		return eventOutCome;

	}
	
	@SuppressWarnings( "unchecked" )
    public void onSendToParent()
    {
		try
		{
			BounceChequeView dataItem = (BounceChequeView)dataTable.getRowData(); 
			CollectionProcedureView collection = CollectionProcedureService.getNewCollectionsRequestForReplacementOfPaymentScheduleId( dataItem.getPaymentScheduleId() );
			
			 if(collection != null)
			 {
				  String msg = java.text.MessageFormat.format(
															   getBundleMessage("replaceCheque.msg.collectionRequestPresent"), 
															   collection.getRequestNumber()
															 );
				  errorMessages.add(msg);
				  return;
			 }
			sessionMap.put( WebConstants.SELECTED_ROW, dataItem);
			String javaScript = "javaScript:sendToParentAfterSelection();";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript );
			
		}
		catch (Exception exception) {
			logger.LogException("onSendToParent() crashed ", exception);
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}
	

    }
	
    @SuppressWarnings( "unchecked" )
    public void onViewPaymentDetails()
    {

		try
		{
		BounceChequeView data = ( BounceChequeView ) dataTable.getRowData();
		PaymentScheduleView psv = new PropertyService().getPaymentScheduleViewById(data.getPaymentScheduleId());
    	CommonUtil.viewPaymentDetails(psv);
		}
		catch (Exception exception) {
			logger.LogException("onViewPaymentDetails() crashed ", exception);
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}
	

    }
	public void updateSearch()
	{
		loadDataList();
	}

	@SuppressWarnings("unchecked")
	public void btnBounce_Click(ActionEvent event) 
	{
		try
		{
			List<Long> selectedList = getSelectedReceiptDetailIds();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put("SELECTED_BOUNCED_CHEQUES", selectedList);
	        String javaScriptText = "javascript:showBouncedChequesPopup();";
	        AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
	        addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);
			viewMap.clear();
			loadDataList();
		}
		catch (Exception exception) 
		{
			logger.LogException("btnBounce_Click() crashed ", exception);
		}
	}
	 
	@SuppressWarnings("unchecked")
	public String btnCancelled_Click() 
	{
		try
		{
				Long paySchId;
				Long statusId;
				String chequeNo;
				PropertyServiceAgent psa = new PropertyServiceAgent();
				List<BounceChequeView> bouceChequeViewList = new ArrayList<BounceChequeView>(0);
				if (viewMap.containsKey("ChequeList")) 
				{
					bouceChequeViewList = (ArrayList<BounceChequeView>) viewMap.get("ChequeList");
					for (BounceChequeView bCV : bouceChequeViewList) 
					{
						if (bCV.getSelected() != null && !bCV.equals("")) 
						{
							if (bCV.getSelected()) 
							{
								paySchId = bCV.getPaymentScheduleId();
								chequeNo = bCV.getChequeNumber();
								DomainDataView ddv = getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),WebConstants.CANCELED);
								statusId = ddv.getDomainDataId();
		
								if (psa.addChequeStatus(paySchId, statusId, chequeNo)) 
								{
									errorMessages = new ArrayList<String>();
									errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeCancel"));
		
								} else 
								{
									errorMessages = new ArrayList<String>();
									errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeStatus"));
								}
		
							}
						}
		
					}
				}
				viewMap.clear();
				loadDataList();
				
		}
		catch (Exception exception) 
		{
			logger.LogException("btnCancelled_Click crashed ", exception);
		}

		return "btnBounce_Click";

	}

	
	public void doSearchItemList()
	{
		loadDataList();
	}

	@SuppressWarnings("unchecked")
	private void loadDataList() 
	{
		logger.logInfo("LoadDataList Started SuccessFully");

		dataList = new ArrayList<BounceChequeView>();

		List<BounceChequeView> list = new ArrayList<BounceChequeView>();
		boolean isSucceed = false;
		try 
		{
			if(!selectOneOwnershipType.equals("-1"))
			{
				searchMap.put("ownershiptype", selectOneOwnershipType);
			}
			if (dateTo != null && !dateTo.equals(""))
				searchMap.put("dateTo", dateTo);
			if (dateFrom != null && !dateFrom.equals(""))
				searchMap.put("dateFrom", dateFrom);
			if (dueOnTo != null && !dueOnTo.equals(""))
				searchMap.put("dueOnTo", dueOnTo);
			if (dueOnFrom != null && !dueOnFrom.equals(""))
				searchMap.put("dueOnFrom", dueOnFrom);
			
			if (paymentNumber != null && !paymentNumber.equals(""))
				searchMap.put("paymentNumber", paymentNumber);
			if (tenantId != null && !tenantId.equals(""))
				searchMap.put("tenantId", tenantId);
			if (tenantNumber != null && !tenantNumber.equals(""))
				searchMap.put("tenantNumber", tenantNumber);
			if (contractNumber != null && !contractNumber.equals(""))
				searchMap.put("contractNumber", contractNumber);
			if (receiptNumber != null && !receiptNumber.equals(""))
				searchMap.put("receiptNumber", receiptNumber);
			if (transactionDate != null && !transactionDate.equals(""))
				searchMap.put("transactionDate", transactionDate);
			if (transactionDateTo != null && !transactionDateTo.equals(""))
				searchMap.put("transactionDateTo", transactionDateTo);
			if (selectOneBank != null && !selectOneBank.equals("")
					&& !selectOneBank.equals("-1")) {
				bankId = new Long(selectOneBank);
				searchMap.put("bankId", bankId);
			}
			if (methodRefNo != null && !methodRefNo.equals(""))
				searchMap.put("methodRefNo", methodRefNo);
			
			if (selectOnePaymentType != null && !selectOnePaymentType.equals("")&& !selectOnePaymentType.equals("-1"))
				searchMap.put("paymentType", selectOnePaymentType);
			if (selectOnePaymentStatus != null && !selectOnePaymentStatus.equals("")&& !selectOnePaymentStatus.equals("-1"))
				searchMap.put("paymentStatus", selectOnePaymentStatus);
			else if (selectOnePaymentStatus.equals("-1") && getInvokingContext().equals("MINOR_COLLECTIONS") )
			{
				
				List<Long> listStatus = new ArrayList<Long>();
				listStatus.add(WebConstants.PAYMENT_SCHEDULE_COLLECTED_ID);
				listStatus.add(WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_ID);
				searchMap.put("paymentStatusIn",listStatus);
			}
			
			if (selectOnePaymentMethod != null && !selectOnePaymentMethod.equals("")&& !selectOnePaymentMethod.equals("-1"))
			{
				searchMap.put("paymentMethod", selectOnePaymentMethod);
			}
			else if (selectOnePaymentMethod.equals("-1") && getInvokingContext().equals("MINOR_COLLECTIONS") )
			{
				List<Long> listMethod = new ArrayList<Long>();
				listMethod.add(WebConstants.PAYMENT_SCHEDULE_MODE_CASH_ID);
				listMethod.add(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE_ID);
				searchMap.put("paymentMethodIn",listMethod);
			}
			if (paymentAmount != null && !paymentAmount.equals(""))
				searchMap.put("paymentAmount", paymentAmount);
			if (chequeDate != null && !chequeDate.equals(""))
				searchMap.put("chequeDate", chequeDate);
			if (propertyName != null && !propertyName.equals(""))
				searchMap.put("propertyName", propertyName);
			if (unitNumber != null && !unitNumber.equals(""))
				searchMap.put("unitNumber", unitNumber);
			
			tenantName = getTxtTenantName().getValue().toString().trim(); 
				
			
			if (tenantName != null && !tenantName.equals(""))
				searchMap.put("tenantName", tenantName);
			if (grpNumber != null && !grpNumber.equals(""))
				searchMap.put("grpNumber", grpNumber);
			if( inheritanceFileNumber != null && inheritanceFileNumber.trim().length() > 0  )
			{
				searchMap.put("inheritanceFileNumber", inheritanceFileNumber);
			}
			if( inheritanceFileOwner!= null && inheritanceFileOwner.trim().length() > 0  )
			{
				searchMap.put("inheritanceFileOwner", inheritanceFileOwner);
			}

            PropertyService psa = new PropertyService();
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  psa.searchChequeGetTotalNumberOfRecords(searchMap);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			
			
			list = psa.getAllCheck(searchMap,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
			dataList = (ArrayList<BounceChequeView>) list;

			logger.logInfo("Record Found In Load Data List : %s", dataList.size());

			if (dataList.size() == 0) 
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));

			}

			viewMap.put("ChequeList", dataList);
			recordSize = totalRows;
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			

			isSucceed = true;
			logger.logInfo("LoadDataList Ends SuccessFully");
		} 
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("LoadDataList Crashed",ex);
			
		}
		// return isSucceed;

	}

	private String rectifyValues(String input) {
		String output = input;
		output = output.replace("'", "''");

		return output;
	}


	// Setters
	// -----------------------------------------------------------------------------------

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	// Public Methods


	public String reset() {
		return "success";
	}

	// Private Methods

	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public String getSelectTenantType() {
		return selectTenantType;
	}

	public void setSelectTenantType(String selectTenantType) {
		this.selectTenantType = selectTenantType;
	}

	public HtmlInputHidden getTenantIdHidden() {
		return tenantIdHidden;
	}

	public void setTenantIdHidden(HtmlInputHidden tenantIdHidden) {
		this.tenantIdHidden = tenantIdHidden;
	}

	public String getTenantNumber() {
		return tenantNumber;
	}

	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getResidenceVisaNumber() {
		return residenceVisaNumber;
	}

	public void setResidenceVisaNumber(String residenceVisaNumber) {
		this.residenceVisaNumber = residenceVisaNumber;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getPersonalSecurityCardNo() {
		return personalSecurityCardNo;
	}

	public void setPersonalSecurityCardNo(String personalSecurityCardNo) {
		this.personalSecurityCardNo = personalSecurityCardNo;
	}

	public HtmlInputText getTxtTenantNumber() {
		return txtTenantNumber;
	}

	public void setTxtTenantNumber(HtmlInputText txtTenantNumber) {
		this.txtTenantNumber = txtTenantNumber;
	}

	public HtmlInputText getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(HtmlInputText txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public HtmlInputText getTxtTenantPassportNumber() {
		return txtTenantPassportNumber;
	}

	public void setTxtTenantPassportNumber(HtmlInputText txtTenantPassportNumber) {
		this.txtTenantPassportNumber = txtTenantPassportNumber;
	}

	public HtmlInputText getTxtTenantVisaNumber() {
		return txtTenantVisaNumber;
	}

	public void setTxtTenantVisaNumber(HtmlInputText txtTenantVisaNumber) {
		this.txtTenantVisaNumber = txtTenantVisaNumber;
	}

	public String getVisaNumber() {
		return visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}

	public BounceChequeView getDataItem() {
		return dataItem;
	}

	public void setDataItem(BounceChequeView dataItem) {
		this.dataItem = dataItem;
	}

	public HtmlInputHidden getHdnModeSelectOnePopUp() {
		return hdnModeSelectOnePopUp;
	}

	public void setHdnModeSelectOnePopUp(HtmlInputHidden hdnModeSelectOnePopUp) {
		this.hdnModeSelectOnePopUp = hdnModeSelectOnePopUp;
	}

	public HtmlInputHidden getHdnModeSetup() {
		return hdnModeSetup;
	}

	public void setHdnModeSetup(HtmlInputHidden hdnModeSetup) {
		this.hdnModeSetup = hdnModeSetup;
	}

	

	
	public String onUpdateStatusFromGRP()
	{
		errorMessages = new ArrayList<String>();
		try 
		{
		  ReadChequeStatusService service = ReadChequeStatusService.getInstance();
          logger.logInfo("onUpdateStatusFromGRP|Start|PaymentReceiptDetailId:%s");
          BounceChequeView data = ( BounceChequeView ) dataTable.getRowData();
          int result = service.processJobFromPaymentSearch( data );
          switch(result)
          {
	           case WebConstants.ResultFromGRPUpdate.BOUNCED: 
	           {
	        	   
	        	   loadDataList(); 	   
	        	   errorMessages.add(
	        			             MessageFormat.format( ResourceUtil.getInstance().getProperty("chequeList.updateChequeStatusFromGRP.resultBounced"),
	        			            		 data.getChequeNumber())
	        	                     );
	        	   break;
	           }
	           case WebConstants.ResultFromGRPUpdate.REALIZED:
	           {
	        	   loadDataList();
	        	   errorMessages.add(
				             MessageFormat.format( ResourceUtil.getInstance().getProperty("chequeList.updateChequeStatusFromGRP.resultRealized"),
				            		 data.getChequeNumber())
		                     );
	        	   break;
	        	   
	          }
	           case WebConstants.ResultFromGRPUpdate.TRX_NOT_POSTED_TO_GRP:
	           {
	        	   errorMessages.add(
				             MessageFormat.format( ResourceUtil.getInstance().getProperty("chequeList.updateChequeStatusFromGRP.resultTrxNotPostedToGrp"),
				            		 data.getChequeNumber())
		                     );
	        	   break;
	        	   
	          }
	          case WebConstants.ResultFromGRPUpdate.TRX_NOT_PRESENT:
	          {
	        	   errorMessages.add(
				             MessageFormat.format( ResourceUtil.getInstance().getProperty("chequeList.updateChequeStatusFromGRP.resultTrxNotCreated"),
				            		 data.getChequeNumber())
		                     );
	        	   break;
	        	   
	          }

	           default:
	          {
	        	  
	        	  errorMessages.add(
				             MessageFormat.format( ResourceUtil.getInstance().getProperty("chequeList.updateChequeStatusFromGRP.resultNotUpdated"),
				            		 data.getChequeNumber())
		                     );
	        	  break;
	          }
           }
          logger.logInfo("onUpdateStatusFromGRP|Finish");
		} 
		catch (Exception e) 
		{
			logger.LogException("onUpdateStatusFromGRP|Error Occured:", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

		return "Status";
	}


	public void setIsModeSelectOnePopUp(Boolean isModeSelectOnePopUp) {
		this.isModeSelectOnePopUp = isModeSelectOnePopUp;
	}

	@SuppressWarnings("unchecked")
	public String getHdnMode() {
		if(	viewMap.get("hdnMode")!= null )
		{
		  hdnMode = viewMap.get("hdnMode" ).toString();
		}
		return hdnMode;
	}

	@SuppressWarnings("unchecked")
	public void setHdnMode(String hdnMode) 
	{
		this.hdnMode = hdnMode;
		viewMap.put("hdnMode",this.hdnMode);
	}

	public String btnAddFacility_Click() {
		return "AddFacility";

	}

	public String getRentValue() {
		return rentValue;
	}

	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}

	public String getSelectOneFacilityType() {
		return selectOneFacilityType;
	}

	public void setSelectOneFacilityType(String selectOneFacilityType) {
		this.selectOneFacilityType = selectOneFacilityType;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public String getMethodRefNo() {
		return methodRefNo;
	}

	public void setMethodRefNo(String methodRefNo) {
		this.methodRefNo = methodRefNo;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getContractNumber() {
		
		return contractNumber;
	}
    @SuppressWarnings( "unchecked" )
	public void setContractNumber(String contractNumber) {
		
		this.contractNumber = contractNumber;
		
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public Date getTransactionDateTo() {
		return transactionDateTo;
	}

	public void setTransactionDateTo(Date transactionDateTo) {
		this.transactionDateTo = transactionDateTo;
	}

	public String getSelectOneBank() {
		return selectOneBank;
	}

	public void setSelectOneBank(String selectOneBank) {
		this.selectOneBank = selectOneBank;
	}

	public List<SelectItem> getBank() {
		bank.clear();

		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<BankView> bViewList = new ArrayList<BankView>();
		SelectItem item = new SelectItem();
		try {
			bViewList = psa.getAllBank();

			for (BankView bV : bViewList) {

				if (getIsEnglishLocale()) {
					item = new SelectItem(bV.getBankId().toString(), bV
							.getBankEn());
					bank.add(item);
				} else {
					item = new SelectItem(bV.getBankId().toString(), bV
							.getBankAr());
					bank.add(item);
				}
			}

		} catch (PimsBusinessException e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			e.printStackTrace();
		}

		return bank;
	}

	public void setBank(List<SelectItem> bank) {
		this.bank = bank;
	}

	public DomainDataView getTypeFromId(List<DomainDataView> ddv, String id) {
		String methodName = "getIdFromType";
		logger.logInfo(methodName + "|" + "Start...");
		logger.logInfo(methodName + "|" + "DomainDataType To search..." + id);
		Long returnId = new Long(0);
		HashMap hMap = new HashMap();
		DomainDataView returnDomainDataView = new DomainDataView();
		for (int i = 0; i < ddv.size(); i++) {
			DomainDataView domainDataView = (DomainDataView) ddv.get(i);
			if (domainDataView.getDomainDataId().toString().equals(id)) {
				returnDomainDataView = domainDataView;
				return returnDomainDataView;
			}

		}
		logger.logInfo(methodName + "|" + "Finish...");
		return returnDomainDataView;

	}

	private List<DomainDataView> getDomainDataListForDomainType(
			String domainType) {
		List<DomainDataView> ddvList = new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		// Iterates for all the domain Types
		while (itr.hasNext()) {
			DomainTypeView dtv = itr.next();
			if (dtv.getTypeName().compareTo(domainType) == 0) {
				Set<DomainDataView> dd = dtv.getDomainDatas();
				// Iterates over all the domain datas
				for (DomainDataView ddv : dd) {
					ddvList.add(ddv);
				}
				break;
			}
		}

		return ddvList;
	}

	public DomainDataView getIdFromType(List<DomainDataView> ddv, String type) {
		String methodName = "getIdFromType";
		logger.logInfo(methodName + "|" + "Start...");
		logger.logInfo(methodName + "|" + "DomainDataType To search..." + type);
		Long returnId = new Long(0);
		HashMap hMap = new HashMap();
		DomainDataView returnDomainDataView = new DomainDataView();
		for (int i = 0; i < ddv.size(); i++) {
			DomainDataView domainDataView = (DomainDataView) ddv.get(i);
			if (domainDataView.getDataValue().equals(type)) {
				returnDomainDataView = domainDataView;
				return returnDomainDataView;
			}

		}
		logger.logInfo(methodName + "|" + "Finish...");
		return returnDomainDataView;

	}

	public Boolean getReplaceCheque() {
		return replaceCheque;
	}

	public void setReplaceCheque(Boolean replaceCheque) {
		this.replaceCheque = replaceCheque;
	}

	public HtmlCommandButton getBouncedButton() {
		return bouncedButton;
	}

	public void setBouncedButton(HtmlCommandButton bouncedButton) {
		this.bouncedButton = bouncedButton;
	}

	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	public HtmlCommandButton getRealizedButton() {
		return realizedButton;
	}

	public void setRealizedButton(HtmlCommandButton realizedButton) {
		this.realizedButton = realizedButton;
	}

	public Boolean getNotReplaceCheque() {
		return notReplaceCheque;
	}

	public void setNotReplaceCheque(Boolean notReplaceCheque) {
		this.notReplaceCheque = notReplaceCheque;
	}

	public HtmlInputHidden getNotReplaceChequeHidden() {
		return notReplaceChequeHidden;
	}

	public void setNotReplaceChequeHidden(HtmlInputHidden notReplaceChequeHidden) {
		this.notReplaceChequeHidden = notReplaceChequeHidden;
	}

	public HtmlInputHidden getReplaceChequeHidden() {
		return replaceChequeHidden;
	}

	public void setReplaceChequeHidden(HtmlInputHidden replaceChequeHidden) {
		this.replaceChequeHidden = replaceChequeHidden;
	}
	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}

	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}
	
	private List<Long> getSelectedReceiptDetailIds(){
		List<Long> selectedList = new ArrayList<Long>();
		List<BounceChequeView> bouceChequeViewList = new ArrayList<BounceChequeView>(0);
		if (viewMap.containsKey("ChequeList")) {
			bouceChequeViewList = (ArrayList<BounceChequeView>) viewMap.get("ChequeList");
			for (BounceChequeView bCV : bouceChequeViewList) {
				if (bCV.getSelected() != null && !bCV.equals("")) {
					if (bCV.getSelected()) {
						selectedList.add(bCV.getPaymentReceiptDetailId());
					}
				}
			}
		}
		return selectedList;
	}

	public String getSelectOnePaymentMethod() {
		return selectOnePaymentMethod;
	}

	public void setSelectOnePaymentMethod(String selectOnePaymentMethod) {
		this.selectOnePaymentMethod = selectOnePaymentMethod;
	}
	
	
	public List<SelectItem> getPaymentTypeList() 
	{
	
	    paymentTypeList.clear();
	    if(!viewMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE) || viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE)==null)
	       loadPaymentType();
	    List<PaymentTypeView> ptvList = (ArrayList<PaymentTypeView>)viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE);
	    List<SelectItem> typeList = new ArrayList<SelectItem>(0);
	    for (PaymentTypeView paymentTypeView : ptvList) {
	    	SelectItem item =null; 
	         if(getIsEnglishLocale())
	          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn());
	         else 
		          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr());
		    typeList.add(item);   
	    }
	       paymentTypeList= typeList;
		   return paymentTypeList;
	}

	private void loadPaymentType()
	{
	  String methodName="loadPaymentType";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  UtilityServiceAgent usa =new UtilityServiceAgent();
		  if(!viewMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE))
		  {
		    List<PaymentTypeView>ddv= (ArrayList<PaymentTypeView>)usa.getAllPaymentTypes();
		    viewMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE, ddv);
		  }
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}

	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}

	public String getSelectOnePaymentType() {
		return selectOnePaymentType;
	}

	public void setSelectOnePaymentType(String selectOnePaymentType) {
		this.selectOnePaymentType = selectOnePaymentType;
	}

	public String getSelectOnePaymentStatus() {
		return selectOnePaymentStatus;
	}

	public void setSelectOnePaymentStatus(String selectOnePaymentStatus) {
		this.selectOnePaymentStatus = selectOnePaymentStatus;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}

	public String getHdnContractNumber() {
		return hdnContractNumber;
	}

	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}
	
	private boolean hasOnRealizedErrors(BounceChequeView row)throws Exception
	{
		boolean hasError = false;
		
		if( row.getOwnerShipTypeId()== null || row.getOwnerShipTypeId().compareTo(WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) != 0 )
		{
			hasError=true;
			String errorMesg= java.text.MessageFormat.format( 
																getBundleMessage("checklist.msg.relaizedNotAllowedForNonZawaya"),
																row.getPaymentNumber()
																
															);
			errorMessages.add(errorMesg);
			
		}
		
		return hasError;
	}
	@SuppressWarnings("unchecked")
	public void onRealized() 
	{
		errorMessages = new ArrayList<String>();
		
		try
		{
			logger.logInfo( "onRealized|Start...");
			List<BounceChequeView > listToProcess = new ArrayList<BounceChequeView>();
			if(viewMap.get("ChequeList")== null) return;
			List<BounceChequeView > bouceChequeViewList = (ArrayList<BounceChequeView>) viewMap.get("ChequeList");
			
			for (BounceChequeView rowToAdd : bouceChequeViewList) 
			{
				if (rowToAdd.getCollectedChequeSelected() != null && rowToAdd.getCollectedChequeSelected())
				{
					if (  hasOnRealizedErrors(rowToAdd) )return;
					listToProcess.add(rowToAdd);
				}
			 }
			
			
			for (BounceChequeView row : listToProcess) 
			{
				Boolean isMarkedRealized = new PropertyServiceAgent().markChequeStatus(
																						row.getPaymentReceiptDetailId(),
																						WebConstants.REALIZED_ID, 
																						row.getChequeNumber(), 
																						getLoggedInUserId(),
																						row.getPaymentDueOn() 
																					   );
				if ( isMarkedRealized )
				{
					errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeRealize"));
	
				} else 
				{
					errorMessages.add(getBundleMessage("bouncedCheque.msg.notChequeStatus"));
				}
			}
			loadDataList();
			logger.logInfo("onRealized|End...");
		}
		catch (Exception e) 
		{
			logger.LogException("onRealized|Error Occured:", e);
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	private boolean hasBounceErrors(BounceChequeView row)throws Exception
	{
		boolean hasError = false;
		
		if( row.getOwnerShipTypeId()== null || row.getOwnerShipTypeId().compareTo(WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) != 0 )
		{
			hasError=true;
			String errorMesg= java.text.MessageFormat.format( 
																getBundleMessage("checklist.msg.bouncedNotAllowedForNonZawaya"),
																row.getPaymentNumber()
																
															);
			errorMessages.add(errorMesg);
			
		}
		
		return hasError;
	}
	@SuppressWarnings("unchecked")
	public String btnBouncePayment_Click()
	{
		logger.logInfo("btnBouncePayment_Click| Start...");
		try
		{
			List<BounceChequeView> bouceChequeViewList = new ArrayList<BounceChequeView>(0);
			List<BounceChequeView> bouceChequeViewToSendList = new ArrayList<BounceChequeView>(0);
			errorMessages = new ArrayList<String>();
			int count=0;
			Long id = 0L;
			Boolean flage= false;
			
			if (viewMap.containsKey("ChequeList")) 
			{
				bouceChequeViewList = (ArrayList<BounceChequeView>) viewMap.get("ChequeList");
				
				for (BounceChequeView bCV : bouceChequeViewList) 
				{
					if (bCV.getCollectedChequeSelected() != null && bCV.getCollectedChequeSelected())
					{
						bouceChequeViewToSendList.add(bCV);
						if(count==0)
						{
							id = bCV.getStatusId(); 
						}
						if(count>0 && !id.equals(bCV.getStatusId()))
						{
							flage = true;
							break;
					    }
						count++;
					}
				 }
				
				if(flage)
				{
					errorMessages.add(getBundleMessage("bouncedCheque.msg.selectSameStatus"));
					return "btnBouncePayment_Click";
				}
			}
		
		
		  if(bouceChequeViewToSendList !=null && bouceChequeViewToSendList.size()>0)
		  {
	        sessionMap.put("CHEQUE_LIST",bouceChequeViewToSendList);
			String extrajavaScriptText ="var screen_width = 1024;var screen_height = 450;"+
		     							"window.open('returnCheque.jsf','_blank','width='+(screen_width-100)+',height='+(screen_height-120)+'," +
		     							"left=150,top=250,scrollbars=yes,status=yes');";
		     openPopUp("EditPaymentSchedule.jsf", extrajavaScriptText);
		  }
		  logger.logInfo( "btnBouncePayment_Click| Finish...");
		}
		catch(Exception e )
		{
			logger.LogException("btnBouncePayment_Click|Error Occured", e );
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}
		return "btnBouncePayment_Click";
	}
	
	private void openPopUp(String URLtoOpen,String extraJavaScript)
	{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText=extraJavaScript;
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
	
	}

	public List<BounceChequeView> getDataList() {
		return dataList;
	}

	public void setDataList(List<BounceChequeView> dataList) {
		this.dataList = dataList;
	}

	public String getGrpNumber() {
		return grpNumber;
	}

	public void setGrpNumber(String grpNumber) {
		this.grpNumber = grpNumber;
	}

	public String showReceiptDetail()
	{
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		
		dataItem = (BounceChequeView) dataTable.getRowData();
		
		request.setAttribute(WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL, dataItem);
		return "receiptDetail";
	}
	
	public boolean getIsModeSelectOnePopUp() 
	{
		if (getHdnMode() != null && getHdnMode().equals(modeSelectOnePopUp))
			return true;
		else
			return false;
	}
	public boolean getIsShowDetailIcon()
	{
		dataItem = (BounceChequeView) dataTable.getRowData();
		if(dataItem.getReceiptNumber() != null && !dataItem.getReceiptNumber().trim().equals(""))
			return true;
		else
			return false;
	}
	public boolean getIsShowReportIcon()
	{
		
			return true;
	}

	public Date getDueOnFrom() {
		return dueOnFrom;
	}

	public void setDueOnFrom(Date dueOnFrom) {
		this.dueOnFrom = dueOnFrom;
	}

	public Date getDueOnTo() {
		return dueOnTo;
	}

	public void setDueOnTo(Date dueOnTo) {
		this.dueOnTo = dueOnTo;
	}

	public DomainDataView getDdvChequeStatusCollected() 
	{
		if(viewMap.get(WebConstants.PAYMENT_SCHEDULE_COLLECTED)!=null)
			ddvChequeStatusCollected=(DomainDataView) viewMap.get(WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		return ddvChequeStatusCollected;
	}

	public void setDdvChequeStatusCollected(DomainDataView ddvChequeStatusCollected) {
		this.ddvChequeStatusCollected = ddvChequeStatusCollected;
	}

	public DomainDataView getDdvPaymentMethodCheque() 
	{
		if(viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE)!=null)
			ddvPaymentMethodCheque=(DomainDataView) viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
		return ddvPaymentMethodCheque;
	}

	public void setDdvPaymentMethodCheque(DomainDataView ddvPaymentMethodCheque) {
		this.ddvPaymentMethodCheque = ddvPaymentMethodCheque;
	}
	public void openEditChequePopup()
	{
		BounceChequeView selectedRow= (BounceChequeView) dataTable.getRowData();
		sessionMap.put("SELECTED_RECORD", selectedRow);
		if(selectedRow.getContractNumber() != null){
			PropertyServiceAgent psa = new PropertyServiceAgent();
			try {
				ContractView contract = psa.getContract(selectedRow.getContractNumber(), getDateFormat());
				if(contract != null){
			    		sessionMap.put("CONTRACT_END_DATE",contract.getEndDate());
				}
			} catch (PimsBusinessException e) {
				e.printStackTrace();
			}
		}
		openPopup("openEditChequePopup();");
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	/**
	 * @author kamranahmed
	 * @param event
	 * Upload file file content and invokes Cheque Realization/Bounce mechanism
	 */
	public void uploadFile(javax.faces.event.ActionEvent event) 
	{
			errorMessages = new ArrayList<String>(0);
			try 
			{
					int numOfChequeProcessed = 0 ;
					UploadedFile uploadedFile = getSelectedFile();
					if (uploadedFile == null ) 
					{
						 errorMessages.add( CommonUtil.getBundleMessage("contract.payment.nofileUploaded.msg") ) ;
						
					}
					else if (
								!(uploadedFile.getName().endsWith("xlsx") || uploadedFile.getName().endsWith("xls"))
							
							) 
					{
						errorMessages.add(getBundleMessage("contract.payment.invalidFile"));
					}

					
					boolean isXlsx = uploadedFile.getName().endsWith("xlsx");
					if (isXlsx) 
					{
						numOfChequeProcessed  = processXLSXFile(uploadedFile);
							
					} 
					else 
					{
						numOfChequeProcessed  = processXLSFile(uploadedFile);
					}
					if( numOfChequeProcessed  > 0 )
					{
						String msg = java.text.MessageFormat.format(
																	 CommonUtil.getBundleMessage("contract.payment.cheque.realized.count"),
																	 numOfChequeProcessed
																	);
						errorMessages.add(msg);
					}
			}
			catch (Exception e) 
			{
				
				logger.LogException( "Error Occred:", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}

	}

	/**
	 * @param uploadedFile
	 * @throws IOException
	 */
	private int processXLSXFile(UploadedFile uploadedFile) throws Exception 
	{
//		InputStream input = null;
//		XSSFWorkbook xlsxWorkbook = null;
		int realizedRecords = 0;
//		boolean hasFailedCheques = false;
//		
//		try
//		{
//			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//			input = uploadedFile.getInputStream();
//			
//			int totalRecords = 0;
//			xlsxWorkbook = new XSSFWorkbook(input);
//			if(xlsxWorkbook == null || xlsxWorkbook.getNumberOfSheets() <= 0 ){return -1;}
//
//			String failedCheques="";
//			for(int index = 0; index < xlsxWorkbook.getNumberOfSheets() ; index++)
//			{
//				  XSSFSheet sheet= xlsxWorkbook.getSheetAt(index);
//				  Iterator rows=sheet.rowIterator();
//				  //Ignore Header
//				  rows.next();
//			      while (rows.hasNext()) 
//			      {
//			        totalRecords++;
//			    	XSSFRow row=(XSSFRow)rows.next();
//			        Iterator cells=row.cellIterator();
//			        //Assuming Column-1:Cheque Number, Column-2:Cheque Date, Column-3: Bank Name, Column-4:Contract Number, Column-5: Status [1=Realized, 2=Bounced]
//			        String chequeNumber = cells.hasNext() ? ((XSSFCell)cells.next()).getStringCellValue():null;
//			        Date chequeDate =  null;
//			        if(cells.hasNext())
//			        { 
//			        	XSSFCell hssfcell=  (XSSFCell)cells.next();
//			        	chequeDate= formatter.parse(hssfcell.getStringCellValue());
//			        }
//			        String bankName = cells.hasNext() ? ((XSSFCell)cells.next()).getStringCellValue():null;
//			        String contractNumber =null;
//			        if(cells.hasNext())
//			        { 
//			        	XSSFCell hssfcell=  (XSSFCell)cells.next();
//			        	contractNumber = hssfcell.getStringCellValue();
//			        }
//			        Double statusType = cells.hasNext() ? ((XSSFCell)cells.next()).getNumericCellValue():null;
//			        if(chequeNumber != null && chequeDate != null && bankName != null && contractNumber != null)
//			        {
//			        	try{
//			        		if(statusType.intValue() == 1)
//			        		{
//			        			if (markChequeRealized(chequeNumber, contractNumber, chequeDate, bankName))
//			        			{
//					        		realizedRecords++;
//					        	}
//			        			else
//					        	{
//					        		failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
//					        		hasFailedCheques=true;
//					        	}	
//			        		}else
//			        		{
//			        			if (markChequeBounced(chequeNumber, contractNumber, chequeDate, bankName))
//			        			{
//					        		realizedRecords++;
//					        	}
//			        			else
//			        			{
//					        		failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
//					        		hasFailedCheques = true;
//					        	}
//			        		}
//				        	
//				        }catch(Exception ex){
//				        	logger.logError("Cheque#: "+chequeNumber +" of Bank:"+bankName+" can't be realized...");
//				        	failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
//				        	hasFailedCheques =true;
//				        }
//			        }
//			   }
////			errorMessages.add(ResourceUtil.getInstance().getProperty("contract.payment.cheque.error.msg"));
//			if(hasFailedCheques)
//			{
//				errorMessages.add(failedCheques);
//			}
//		}
//		}catch(Exception e)
//		{
//			throw e;
//		}
//		finally
//		{
//			input = null;
//			xlsxWorkbook = null; 
//		}
		return realizedRecords;
	}

	/**
	 * @param uploadedFile
	 * @throws IOException
	 */
	private int processXLSFile(UploadedFile uploadedFile) throws Exception 
	{
		InputStream input = null;
		HSSFWorkbook xlsWorkbook = null;
		boolean hasFailedCheques = false;
		int realizedRecords = 0;
		try
		{
		
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			input = uploadedFile.getInputStream();
			
			int totalRecords = 0;
			xlsWorkbook = new HSSFWorkbook(input);
			String failedCheques="";
			if(xlsWorkbook == null && xlsWorkbook.getNumberOfSheets() <= 0)
			{ return -1;}
//			for(int index = 0; index < xlsWorkbook.getNumberOfSheets() ; index++)
//			{
				HSSFSheet sheet= xlsWorkbook.getSheetAt(0);
				Iterator rows=sheet.rowIterator();
				//Ignore Header
				rows.next();
			      while (rows.hasNext()) 
			      {
			        totalRecords++;
			        HSSFRow row=(HSSFRow)rows.next();
			        Iterator cells=row.cellIterator();
			        //Assuming Column-1:Cheque Number, Column-2:Cheque Date, Column-3: Bank Name, Column-4:Contract Number, Column-5: Status [1=Realized, 2=Bounced]
			        String chequeNumber = cells.hasNext() ? ((HSSFCell)cells.next()).getStringCellValue():null;
			        Date chequeDate =  null;
			        if(cells.hasNext())
			        { 
			        	HSSFCell hssfcell=  (HSSFCell)cells.next();
//			        	chequeDate= formatter.parse(hssfcell.getStringCellValue());
			        	chequeDate=  hssfcell.getDateCellValue();
			        }
			        String bankName = cells.hasNext() ? ((HSSFCell)cells.next()).getStringCellValue():null;
			        String contractNumber =null;
			        if(cells.hasNext())
			        { 
			        	HSSFCell hssfcell=  (HSSFCell)cells.next();
			        	contractNumber = hssfcell.getStringCellValue();
			        }
			        Double statusType = cells.hasNext() ? ((HSSFCell)cells.next()).getNumericCellValue():null;
			        
			        if(chequeNumber != null && chequeDate != null && bankName != null && contractNumber != null)
			        {
			        	try
			        	{
			        		if(statusType.intValue() == 1)
			        		{
			        			if (markChequeRealized(chequeNumber, contractNumber, chequeDate, bankName, chequeDate))
			        			{
					        		realizedRecords++;
					        	}
			        			else
			        			{
					        		failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
					        		hasFailedCheques = true;
					        	}	
			        		}
			        		else
			        		{
			        			if (markChequeBounced(chequeNumber, contractNumber, chequeDate, bankName))
			        			{
					        		realizedRecords++;
					        	}
			        			else
			        			{
					        		failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
					        		hasFailedCheques = true;
					        	}
			        		}
				        }
			        	catch(Exception ex)
			        	{
				        	logger.LogException("Cheque#: "+chequeNumber +" of Bank:"+bankName+" can't be realized...",ex);
				        	failedCheques+=" Cheque#:"+chequeNumber+" Bank:"+bankName+",";
				        	hasFailedCheques = true;   	
				        }
			        }
			      }
//			      errorMessages.add(ResourceUtil.getInstance().getProperty("contract.payment.cheque.error.msg"));
			      if( hasFailedCheques  )
			      {
			    	  errorMessages.add(failedCheques);
			      }
//			}
		
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			input = null;
			xlsWorkbook = null; 
		}
		return realizedRecords;
	}
	public DomainDataView getDdvGrpStatusPosted()
	{
		if(viewMap.get(WebConstants.GRP.GRP_POSTED)!=null)
			ddvGrpStatusPosted=(DomainDataView) viewMap.get(WebConstants.GRP.GRP_POSTED);
		return ddvGrpStatusPosted;
	}

	public void setDdvGrpStatusPosted(DomainDataView ddvGrpStatusPosted) {
		this.ddvGrpStatusPosted = ddvGrpStatusPosted;
	}

	public String getHdnIsFromEditCheque() {
		return hdnIsFromEditCheque;
	}

	public void setHdnIsFromEditCheque(String hdnIsFromEditCheque) {
		this.hdnIsFromEditCheque = hdnIsFromEditCheque;
	}
	 @SuppressWarnings( "unchecked" )
	 public void btnPrintPaymentSchedule_Click()
	 {
		 try
		 {
	    	
	    	BounceChequeView psv = (BounceChequeView)dataTable.getRowData();
	    	if( psv.getContractId() ==null && psv.getRequestView().getRequestTypeId().equals(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT ) )
	    	{
	    		CommonUtil.printMEMSPaymentReceipt( psv.getRequestId().toString(),
	    				                           "",
	    				                           getFacesContext()
	    				                           );
	    	}
	    	
	    	else if( psv.getContractId() != null  )
	    	{
	    		CommonUtil.printPaymentReceipt(psv.getContractId().toString(), psv.getPaymentScheduleId().toString(), "", 
	    			                       getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.NEW_LEASE_CONTRACT);
	    	}
	    	else if( psv.getRequestId() != null )
	    	{
	    		CommonUtil.printMiscellaneousPaymentReceipt( psv.getRequestId().toString() , psv.getPaymentScheduleId().toString() ,getFacesContext()); 
	    												
	    	}
	    } 
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("btnPrintPaymentSchedule_Click Crashed",ex);
			
		}
	 }
	 /**
	  * @author kamranahmed
	  * @param chequeNumber
	  * @param contractNumber
	  * @param chequeDate
	  * @param bankName
	  * @return
	  * Mark Payment against given cheque realized
	  */
	 public boolean markChequeRealized(String chequeNumber, String contractNumber, Date chequeDate, String bankName,Date matDate)  
	 {
			Long payDetailId;
			
			PropertyServiceAgent psa = new PropertyServiceAgent();
			try
			{
				payDetailId = psa.getPaymentDetailIdByChequeAndContract(chequeNumber, contractNumber, chequeDate, bankName);
				if(payDetailId == null)
				{
					errorMessages.add(getBundleMessage("contract.payment.cheque.realized.msg"));
					return false;
				}
				if ( psa.markChequeStatus(payDetailId, WebConstants.REALIZED_ID, chequeNumber, getLoggedInUserId(),matDate ) )
				{
				  errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeRealize"));
	
				} else 
				{
					errorMessages.add(getBundleMessage("bouncedCheque.msg.notChequeStatus"));
					return false;
				}
			}
			catch (Exception e) 
			{
			  logger.LogException("Error Occured:", e);
			  errorMessages.add(getBundleMessage("commons.ErrorMessage"));
			}
			return true;
		}
	 /**
	  * @author kamranahmed
	  * @param chequeNumber
	  * @param contractNumber
	  * @param chequeDate
	  * @param bankName
	  * @return
	  * Mark Payment against given cheque Bounced
	  */
	 public boolean markChequeBounced(String chequeNumber, String contractNumber, Date chequeDate, String bankName) {
			String methodName = "markChequeBounced";
			Long payDetailId;
			Long statusId;
			PropertyServiceAgent psa = new PropertyServiceAgent();
			errorMessages = new ArrayList<String>();
			logger.logInfo(methodName + "|" + "Start...");
			
			

			try{
							payDetailId = psa.getPaymentDetailIdByChequeAndContract(chequeNumber, contractNumber, chequeDate, bankName);
							if(payDetailId == null){
								errorMessages.add(getBundleMessage("contract.payment.cheque.realized.msg"));
								return false;
							}
							DomainDataView ddv = getIdFromType(
									getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
									Constant.PAYMENT_SCHEDULE_STATUS_BOUNCED);
							statusId = ddv.getDomainDataId();
							
							if ( psa.markChequeStatus(payDetailId, statusId, chequeNumber, getLoggedInUserId() ,chequeDate) )
							{
								errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeBounced"));

							} else 
							{
								errorMessages.add(getBundleMessage("bouncedCheque.msg.notChequeStatus"));
								return false;
							}

							logger.logInfo("Payment Schedule ID:::::" + payDetailId);
							logger.logInfo("Status ID:::::::::" + statusId);
							logger.logInfo("Cheque No::::::::" + chequeNumber);

			
			logger.logInfo(methodName + "|" + "End...");
			}catch (Exception e) {
				errorMessages.add(getBundleMessage("commons.ErrorMessage"));
			}
			return true;

	 }
	 public String getPersonTenant() {
			
			return WebConstants.PERSON_TYPE_TENANT;
		}
		public String getHdnTenantId() {
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			if(viewMap.containsKey("hdnTenantId") && viewMap.get("hdnTenantId")!=null)
				hdnTenantId = viewMap.get("hdnTenantId").toString();
			return hdnTenantId;
		}

		@SuppressWarnings( "unchecked" )
		public void setHdnTenantId(String hdnTenantId) {
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			this.hdnTenantId = hdnTenantId;
			if(this.hdnTenantId!=null)
				viewMap.put("hdnTenantId", this.hdnTenantId);
		}
		public String getInheritanceFileNumber() {
			return inheritanceFileNumber;
		}
		public void setInheritanceFileNumber(String inheritanceFileNumber) {
			this.inheritanceFileNumber = inheritanceFileNumber;
		}
		public String getInheritanceFileOwner() {
			return inheritanceFileOwner;
		}
		public void setInheritanceFileOwner(String inheritanceFileOwner) {
			this.inheritanceFileOwner = inheritanceFileOwner;
		}

		@SuppressWarnings("unchecked")
		public List<SelectItem> getPaymentStatusList() {
			if( viewMap.get("paymentStatusList") != null )
			{
				paymentStatusList =  (ArrayList<SelectItem>) viewMap.get("paymentStatusList") ;
			}
			return paymentStatusList;
		}
		@SuppressWarnings("unchecked")
		public void setPaymentStatusList(List<SelectItem> paymentStatusList) {
			this.paymentStatusList = paymentStatusList;
			if( paymentStatusList != null )
			{
				viewMap.put("paymentStatusList",this.paymentStatusList) ;
			}
		}
		@SuppressWarnings("unchecked")
		public String getInvokingContext() {
			if( viewMap.get("invokingContext") != null )
			{
				invokingContext = viewMap.get("invokingContext").toString();
			}
			return invokingContext;
		}
		@SuppressWarnings("unchecked")
		public void setInvokingContext(String invokingContext) {
			this.invokingContext = invokingContext;
			if( invokingContext != null )
			{
				viewMap.put("invokingContext",invokingContext );
			}
		}

		@SuppressWarnings("unchecked")
		public List<SelectItem> getPaymentMethodList() {
			if( viewMap.get("paymentMethodList") != null )
			{
				paymentMethodList = (ArrayList<SelectItem>) viewMap.get("paymentMethodList");
			}
			return paymentMethodList;
		}

		@SuppressWarnings("unchecked")
		public void setPaymentMethodList(List<SelectItem> paymentMethodList) 
		{
			this.paymentMethodList = paymentMethodList;
			if( this.paymentMethodList  != null )
			{
				viewMap.put("paymentMethodList",paymentMethodList  );
			}
		}

		public HtmlSelectOneMenu getPaymentMethodMenu() {
			return paymentMethodMenu;
		}

		public void setPaymentMethodMenu(HtmlSelectOneMenu paymentMethodMenu) {
			this.paymentMethodMenu = paymentMethodMenu;
		}

		public HtmlSelectOneMenu getPaymentStatusMenu() {
			return paymentStatusMenu;
		}

		public void setPaymentStatusMenu(HtmlSelectOneMenu paymentStatusMenu) {
			this.paymentStatusMenu = paymentStatusMenu;
		}

		public HtmlInputText getTxtFileNumber() {
			return txtFileNumber;
		}

		public void setTxtFileNumber(HtmlInputText txtFileNumber) {
			this.txtFileNumber = txtFileNumber;
		}
		public UploadedFile getSelectedFile() 
		{
			if(viewMap.get("selectedFile") != null )
			{
				selectedFile = (UploadedFile)viewMap.get("selectedFile");
			}
			return selectedFile;
		}

		@SuppressWarnings("unchecked")
		public void setSelectedFile(UploadedFile selectedFile) 
		{
			if(selectedFile!=null)
				viewMap.put("selectedFile", selectedFile);
		}

		public HtmlInputFileUpload getFileUploadCtrl() {
			return fileUploadCtrl;
		}

		public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
			this.fileUploadCtrl = fileUploadCtrl;
		}

		public String getSelectOneOwnershipType() {
			return selectOneOwnershipType;
		}

		public void setSelectOneOwnershipType(String selectOneOwnershipType) {
			this.selectOneOwnershipType = selectOneOwnershipType;
		}
		
}
