package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.pimsevaluaterequest.PIMSPropertyEvaluationRequestBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Region;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;
import com.avanza.pims.ws.vo.EvaluationView;
import com.avanza.pims.ws.vo.RegionDetailsView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.SiteVisitResultsView;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;


public class RegionDetailsAddBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(RegionDetailsAddBacking.class);

	private transient UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	
	private RegionDetailsView region = new RegionDetailsView(); 
	
	HtmlInputText countryEnText = new HtmlInputText();
	HtmlInputText countryArText = new HtmlInputText();
	HtmlInputText cityEnText = new HtmlInputText();
	HtmlInputText cityArText = new HtmlInputText();
	HtmlInputText areaEnText = new HtmlInputText();
	HtmlInputText areaArText = new HtmlInputText();
	HtmlSelectOneMenu grpCountryMenu = new HtmlSelectOneMenu();
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_COUNTRY = WebConstants.RegionDetails.PAGE_MODE_COUNTRY; // initial mode where only country fields will be visible
	private final String PAGE_MODE_CITY = WebConstants.RegionDetails.PAGE_MODE_CITY; // mode where country and city fields will be visible
	private final String PAGE_MODE_AREA = WebConstants.RegionDetails.PAGE_MODE_AREA; // mode where country, city and area fields will be visible
	private String pageMode="default";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.RegionDetails.Headings.MAIN;
	
	private String heading="";

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private HtmlDataTable regionDataTable = new HtmlDataTable();
	private List<RegionDetailsView> regionList = new ArrayList<RegionDetailsView>();
	private Integer regionRecordSize = 0;

	
	// Methods
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
//			if(!isPostBack())
//			{
				setMode();
				setValues();
//			}
//			else{
//
//			}

			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			///
//			sessionMap.put(PAGE_MODE,PAGE_MODE_CITY);
//			sessionMap.remove(POPUP_MODE);
//			sessionMap.remove(PAGE_MODE);
//			sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_ID, 693L);
//	   		sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_MODE, WebConstants.RegionDetails.EVALUATION_REQUEST_MODE_EVALUATE);
//	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
			///
			
			// if popup mode i.e. add country, city or area
			if(sessionMap.get(PAGE_MODE)!=null)
				viewMap.put(PAGE_MODE, sessionMap.get(PAGE_MODE));
			sessionMap.remove(PAGE_MODE);
			if(sessionMap.containsKey(WebConstants.RegionDetails.REGION_VIEW)){
				region = (RegionDetailsView) sessionMap.get(WebConstants.RegionDetails.REGION_VIEW);
				viewMap.put(WebConstants.RegionDetails.REGION_VIEW, region);
				sessionMap.remove(WebConstants.RegionDetails.REGION_VIEW);
			}
			
			if(sessionMap.get("editMode")!=null)
				viewMap.put("editMode", sessionMap.get("editMode"));
			sessionMap.remove("editMode");
			
			/*List<RegionDetailsView> countryList = (List<RegionDetailsView>) sessionMap.get("countryList");
			sessionMap.remove("countryList");
			if(countryList!=null)
				viewMap.put("countryList", countryList);*/
			
			//
				
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_COUNTRY; 
			
			viewMap.put("asterisk", "*");
			
			if(!getIsViewModePopUp()){
				viewMap.put(HEADING, WebConstants.RegionDetails.Headings.MAIN);
				viewMap.put("asterisk", "");
			}
			else{
				if(pageMode.equals(PAGE_MODE_COUNTRY)){
					if(getIsEditMode())
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_EDIT_COUNTRY);
					else
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_COUNTRY);
				}	
				else if(pageMode.equals(PAGE_MODE_CITY)){
					if(getIsEditMode())
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_EDIT_CITY);
					else
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_CITY);
				}
				else if(pageMode.equals(PAGE_MODE_AREA)){
					if(getIsEditMode())
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_EDIT_AREA);
					else
						viewMap.put(HEADING, WebConstants.RegionDetails.Headings.PAGE_MODE_AREA);
				}
			}
			
			viewMap.put(PAGE_MODE, pageMode);	
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	@SuppressWarnings("unchecked")
	public Boolean validateForSave() throws Exception{
	    logger.logInfo("validateForSave() started...");
	    Boolean validated = true;
	    try
		{
	    	Long regionId = null;
			if(viewMap.containsKey("regionId")){
				regionId = (Long) viewMap.get("regionId");
			}
			
			RegionDetailsView temp = (RegionDetailsView) viewMap.get(WebConstants.RegionDetails.REGION_VIEW);
			
			if(temp==null)
				temp = new RegionDetailsView();
			
	    	if(getIsCountryMode())
	    	{
		    	if(StringHelper.isEmpty(region.getCountryEn()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.COUNTRY_EN));
		    		validated = false;
		    	}
		    	else
		    		temp.setCountryEn(region.getCountryEn());
		    	
		    	if(StringHelper.isEmpty(region.getCountryAr()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.COUNTRY_AR));
		    		validated = false;
		    	}
		    	else
		    		temp.setCountryAr(region.getCountryAr());
		    
		    	if(region.getGrpCountryCode() == null || region.getGrpCountryCode().trim().length()<=0 || region.getGrpCountryCode().equals("-1")  )
		    	{
		    		errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.RegionDetails.Fields.GRP_COUNTRY));
		    		validated = false;
		    	}
		    	else
		    		temp.setGrpCountryCode(region.getGrpCountryCode());
		    
		    	
		    	region.setCountryId(regionId);
		    	
	    	}
	    	else if(getIsCityMode()){
		    	if(StringHelper.isEmpty(region.getCityEn()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.CITY_EN));
		    		validated = false;
		    	}
		    	else
		    		temp.setCityEn(region.getCityEn());
		    	
		    	if(StringHelper.isEmpty(region.getCityAr()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.CITY_AR));
		    		validated = false;
		    	}
		    	else
		    		temp.setCityAr(region.getCityAr());
		    	
		    	region.setCityId(regionId);
	    	}
	    	else if(getIsAreaMode()){
		    	if(StringHelper.isEmpty(region.getAreaEn()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.AREA_EN));
		    		validated = false;
		    	}
		    	else
		    		temp.setAreaEn(region.getAreaEn());
		    	
		    	if(StringHelper.isEmpty(region.getAreaAr()))
		    	{
		    		errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.RegionDetails.Fields.AREA_AR));
		    		validated = false;
		    	}
		    	else
		    		temp.setAreaAr(region.getAreaAr());
		    	region.setAreaId(regionId);
	    	}
	    	
	    	region = temp;
	    	
	    	DomainTypeView domainType = utilityServiceAgent.getDomainTypeByTypeName(WebConstants.REGION_TYPE);
			List<DomainDataView> domainDataList = new ArrayList<DomainDataView>();
			if(domainType.getDomainDatas()!=null){
				domainDataList.addAll(domainType.getDomainDatas());
				for(DomainDataView ddv: domainDataList){
					if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_COUNTRY))
						region.setCountryDomainDataId(ddv.getDomainDataId());
					else if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_CITY))
						region.setCityDomainDataId(ddv.getDomainDataId());
					else if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_AREA))
						region.setAreaDomainDataId(ddv.getDomainDataId());
				}
			}
			
			
	    	
			logger.logInfo("validateForSave() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForSave() crashed ", exception);
			throw exception;
		}
		return validated;
	}
    @SuppressWarnings("unchecked")
	public String saveRegion()
    {
    	logger.logInfo("saveRegion() started...");
    	try{
    		if(validateForSave())
    		{
    			Integer currentLevel = getCurrentRegionLevel();
    			region.setRegionLevel(currentLevel);
    			Long regionId = null;
    			try{
    				regionId = utilityServiceAgent.saveRegion(region, getArgMap());
    				List<RegionDetailsView> regionList = getRegionList();
    				if(getIsCountryMode())
    					region.setCountryId(regionId);
    				else if(getIsCityMode())
    					region.setCityId(regionId);
    				else if(getIsAreaMode())
    					region.setAreaId(regionId);
    				regionList.add(region);
    				setRegionList(regionList);
    				viewMap.put("showAddMore", true);
    				viewMap.put("regionId", regionId);
    				sessionMap.put("modifiedRegion", region);
    				sessionMap.put("saveSuccess", true);
    			}
    			catch (PimsBusinessException pimsException) {
    				logger.LogException("saveRegion() crashed ", pimsException);
    			}
    			if(regionId!=null){
    	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_SAVE_SUCCESS);
    	   		}
    	   		else
    	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_SAVE_FAILURE));
    		}
    			
    		logger.logInfo("saveRegion() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("saveRegion() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("saveRegion() crashed ",exception);
    	}
        return "saveRegion";
    }
	
	public String addMore()
    {
    	logger.logInfo("addMore() started...");
    	try{
			if(getIsCountryMode()){
				region.setCountryId(null);
				region.setCountryEn(null);
				region.setCountryAr(null);
			}
			else if(getIsCityMode()){
				region.setCityId(null);
				region.setCityEn(null);
				region.setCityAr(null);
    		}
			else if(getIsAreaMode()){
				region.setAreaId(null);
				region.setAreaEn(null);
				region.setAreaAr(null);
			}
   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.RegionDetails.ENTER_DETAILS);
   			viewMap.remove("showAddMore");
   			viewMap.remove("region");
    		logger.logInfo("addMore() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("addMore() crashed ",exception);
    	}
        return "addMore";
    }
	
	private Integer getCurrentRegionLevel(){
		pageMode = (String) viewMap.get(PAGE_MODE);
		if(pageMode!=null)
			if(pageMode.equals(PAGE_MODE_COUNTRY))
				return WebConstants.RegionLevels.COUNTRY_LEVEL;
			else if(pageMode.equals(PAGE_MODE_CITY))
				return WebConstants.RegionLevels.CITY_LEVEL;
			else if(pageMode.equals(PAGE_MODE_AREA))
				return WebConstants.RegionLevels.AREA_LEVEL;
		return null;
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_COUNTRY;
			
			logger.logInfo("pageMode : %s", pageMode);
			heading = getHeading();
			
			Boolean readonly = getShowAddMore();
			countryEnText.setReadonly(readonly);
			countryArText.setReadonly(readonly);
			cityEnText.setReadonly(readonly);
			cityArText.setReadonly(readonly);
			areaEnText.setReadonly(readonly);
			areaArText.setReadonly(readonly);
			
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		argMap.put("editMode", getIsEditMode());
		return argMap;
	}
	

    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	
	public Boolean getShowAddMore() {
		if(getIsEditMode())
			return false;
		return viewMap.get("showAddMore")==null?false:(Boolean)viewMap.get("showAddMore");
	}
	
	public Boolean getIsEditMode() {
		return viewMap.get("editMode")==null?false:(Boolean)viewMap.get("editMode");
	}
	
	public Boolean getShowCountry() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getShowCity() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getShowArea() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}
	
	public Boolean getIsCountryMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return true;
		return false;
	}

	
	public Boolean getIsCityMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		return false;
	}

	public Boolean getIsAreaMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getCountryReadonly() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return false;
		return true;
	}
	
	public Boolean getCityReadonly() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return false;
		return true;
	}
	
	public Boolean getAreaReadonly() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return false;
		return true;
	}

	public Boolean getIsViewModePopUp(){
		return true;
	}

	public RegionDetailsView getRegion() {
		return region;
	}

	public void setRegion(RegionDetailsView region) {
		this.region = region;
	}

	public HtmlDataTable getRegionDataTable() {
		return regionDataTable;
	}

	public void setRegionDataTable(HtmlDataTable regionDataTable) {
		this.regionDataTable = regionDataTable;
	}

	public List<RegionDetailsView> getRegionList() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			regionList = (List<RegionDetailsView>)viewMap.get("countryList");
		else if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			regionList = (List<RegionDetailsView>)viewMap.get("cityList");
		else if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			regionList = (List<RegionDetailsView>)viewMap.get("areaList");
		
		if(regionList==null)
			regionList = new ArrayList<RegionDetailsView>();
		return regionList;
	}

	public void setRegionList(List<RegionDetailsView> regionList) {
		if(regionList==null){
			regionList = new ArrayList<RegionDetailsView>();
		}
		else{
			this.regionList = regionList;
			if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
				viewMap.put("countryList", this.regionList);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
				viewMap.put("cityList", this.regionList);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
				viewMap.put("areaList", this.regionList);
		}
	}

	public Integer getRegionRecordSize() {
		return regionRecordSize;
	}

	public void setRegionRecordSize(Integer regionRecordSize) {
		this.regionRecordSize = regionRecordSize;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public HtmlInputText getCountryEnText() {
		return countryEnText;
	}

	public void setCountryEnText(HtmlInputText countryEnText) {
		this.countryEnText = countryEnText;
	}

	public HtmlInputText getCountryArText() {
		return countryArText;
	}

	public void setCountryArText(HtmlInputText countryArText) {
		this.countryArText = countryArText;
	}

	public HtmlInputText getCityEnText() {
		return cityEnText;
	}

	public void setCityEnText(HtmlInputText cityEnText) {
		this.cityEnText = cityEnText;
	}

	public HtmlInputText getCityArText() {
		return cityArText;
	}

	public void setCityArText(HtmlInputText cityArText) {
		this.cityArText = cityArText;
	}

	public HtmlInputText getAreaEnText() {
		return areaEnText;
	}

	public void setAreaEnText(HtmlInputText areaEnText) {
		this.areaEnText = areaEnText;
	}

	public HtmlInputText getAreaArText() {
		return areaArText;
	}

	public void setAreaArText(HtmlInputText areaArText) {
		this.areaArText = areaArText;
	}

	public HtmlSelectOneMenu getGrpCountryMenu() {
		return grpCountryMenu;
	}

	public void setGrpCountryMenu(HtmlSelectOneMenu grpCountryMenu) {
		this.grpCountryMenu = grpCountryMenu;
	}
	
}

