package com.avanza.pims.web.backingbeans;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.bpel.mempayvendorbpel.proxy.MEMSPayVendorBPELPortClient;
import com.avanza.pims.bpel.proxy.MEMSBlockingBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.entity.Request;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("unchecked")
public class PayVendorRequestController extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;
	DecimalFormat oneDForm = new DecimalFormat("#.00");
	private Boolean markUnMarkAll;
	private static final String PROCEDURE_TYPE 				= "procedureType";
	private static final String EXTERNAL_ID 				= "externalId";
	private static final String VENDOR_ID					= "vendorId";
	private static final String VENDOR_NAME					= "vendorName";
	private static final String DISBURSEMENT_IDS_LIST		= "disbursementIds";	
	private static final String VENDOR						= "vendor";
	private static final String TAB_ID_DETAILS= "disbursementTab";
	private static final Long INVALID_STATUS = -1L;
	private HtmlInputTextarea htmlRFC = new HtmlInputTextarea();
	private boolean pageModeView =false;
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
	private HtmlInputText txtVendor = new HtmlInputText();
	private HtmlGraphicImage imgVendor = new HtmlGraphicImage();
	private HtmlTabPanel richPanel = new HtmlTabPanel();
	private HtmlDataTable disbursementTable = new HtmlDataTable();
	
	/* html hidden query paramter fields */
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnRequestId;
	private String hdnVendorId;
	private String hdnVendorName;
	String sumDisbursements = "0.0";
	@Override
	public void init() {
		try {	
			viewMap.put( PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_PAY_VENDOR);				
			viewMap.put( EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_MEMS_PAYVENDOR);					
			if( !isPostBack() ) 
			{
				
				List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(
						WebConstants.REQUEST_STATUS);
				
				viewMap.put( WebConstants.REQUEST_STATUS_NEW, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_NEW).getDomainDataId());
																
				viewMap.put( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED).getDomainDataId());
						
				viewMap.put( WebConstants.REQUEST_STATUS_APPROVED, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_APPROVED).getDomainDataId());
																																	
				viewMap.put( WebConstants.REQUEST_STATUS_REJECTED, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_REJECTED).getDomainDataId());
																				
				viewMap.put( WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId());
																				
				viewMap.put( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_REVIEW_REQUIRED).getDomainDataId());
																						
				viewMap.put( WebConstants.REQUEST_STATUS_REVIEW_DONE, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_REVIEW_DONE).getDomainDataId());
																				
				viewMap.put( WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId());
				
				viewMap.put( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ, CommonUtil.getIdFromType(ddvList,
						WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ).getDomainDataId());
				
				
											
				RequestView reqView = (RequestView) getRequestMap().get( WebConstants.REQUEST_VIEW);
				UserTask userTask = (UserTask) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK);
				
				if( null!=reqView ) {
					initFromRequestSearch( reqView);
				} else if ( null!=userTask ) {
					sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK);
					initFromUserTask( userTask);
				} else {
					RequestView request = new RequestView();
					request.setStatusId( INVALID_STATUS);
					request.setCreatedBy( CommonUtil.getLoggedInUser());
					request.setUpdatedBy( CommonUtil.getLoggedInUser());
					request.setUpdatedOn( DateUtil.getCurrentDate());
					request.setCreatedOn( DateUtil.getCurrentDate());
					request.setRequestDate( DateUtil.getCurrentDate());
					request.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
					request.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
					request.setRequestTypeId( WebConstants.MemsRequestType.PAY_VENDOR_REQUEST_ID);							
					setRequestView( request);
					handleAttachmentCommentTabStatus( request);
					loadAttachmentsAndComments( null);
				}						
			}		
		} catch (Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void save() 
	{
		try 
		{
				if( !validateInput() ) 
				{return;}
				Long statusId = getRequestView().getStatusId();
				if ( doSaveApplication(getStatusNewId())) 
				{
					if ( statusId.equals(INVALID_STATUS)) 
					{
						CommonUtil.saveSystemComments(
														WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_CREATED,
														getRequestView().getRequestId()
													  );							
					}
					pageModeView=true;
					refreshTabs(getRequestView());
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SAVE));														
				}	
						
		} catch (Exception ex) {
			logger.LogException( "[Exception occured in save()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private void doSubmit() throws Exception 
	{
		if( !validateSubmitQuery() ) 
		{return;}
		doSaveApplication( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
		invokeBpel( getRequestView().getRequestId());
		initFromRequestSearch( getRequestView());
		CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL,getRequestView().getRequestId());					
	}
	
	@SuppressWarnings( "unchecked" )
	private boolean validateApprove()throws Exception 
	{
		boolean hasBalanceError=false;
		
		List<Long> disDetIdNotIn  = new ArrayList<Long>();
		for(DisbursementDetailsView singleView : getDisbursementDetails() ) 
		{
			disDetIdNotIn.add(singleView.getDisDetId());
		}
		for(DisbursementDetailsView singleView : getDisbursementDetails() ) 
		{
			
			if( singleView.getSrcType().longValue() == WebConstants.PaymentSrcType.AID_ID ) continue;
			for(DisbursementRequestBeneficiaryView singleBeneficiary : singleView.getRequestBeneficiaries()) 
			{
				 int validateBalanceCode = validateBalance(  getBeneficiaryBalanceAmount( singleBeneficiary.getPersonId() , singleView.getFileId()) ,
															 getBeneficiaryBlockingAmount( singleBeneficiary.getPersonId() ),
															 DisbursementService.getRequestedAmount(
																	 								 singleBeneficiary.getPersonId() , 
																	 								 singleView.getFileId(), 
																	 								 getRequestView().getRequestId() ,
																	 								 disDetIdNotIn
																	 								),
						 									 singleBeneficiary.getAmount(),
															 singleBeneficiary.getPersonId(),
															 singleView.getBeneficiariesName(),
															 singleView.getFileId()
				 										   ) ;
				 if( !hasBalanceError )
				 {
				  hasBalanceError = validateBalanceCode > 0 ;
				 }
				
			}
			 if( singleView.getPaymentDetails() == null || singleView.getPaymentDetails().size() <= 0 )
			 {
				String msg =  java.text.MessageFormat.format(
																CommonUtil.getBundleMessage( "mems.payment.err.msg.paymentDetailsReqName"),
																singleView.getRefNum(),
																singleView.getBeneficiariesName()
															); 
				errorMessages.add(msg);
				richPanel.setSelectedTab(   TAB_ID_DETAILS );
				return false;
			 }
		}
		if ( hasBalanceError )
		{
			richPanel.setSelectedTab(   TAB_ID_DETAILS );
			return false;
		}
		return true;
	}
	
	private Double getBeneficiaryBalanceAmount( Long beneficiaryId,Long fileId ) throws Exception
	   {
//		    PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
			return PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(
																				 beneficiaryId,
																				 fileId
																			    );
//			return  PersonalAccountTrxService .getAmount( beneficiaryId  );
		   
	   }
	
	private Double getBeneficiaryBlockingAmount( Long beneficiaryId ) throws Exception
	   {
			return BlockingAmountService.getBlockingAmount(  beneficiaryId , null, null , null, null);
		   
	   }
	
	@SuppressWarnings("unchecked")
	private boolean hasUnblockError ( List<DisbursementDetailsView> selectedDetails ) throws Exception
	{
		boolean hasError = false;
		if( selectedDetails == null || selectedDetails.size()  <=  0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "disbursement.msg.selectAny" )	);
			hasError = true;
//			tabPanel.setSelectedTab(TAB_DETAILS);
		}
		else
		{
			for (DisbursementDetailsView disbursementDetailsView : selectedDetails) 
			{
				Request  request = BlockingAmountService.getActiveUnBlockingRequest( disbursementDetailsView );
				if( request != null )
				{
					String msg = java.text.MessageFormat.format( 
																  CommonUtil.getBundleMessage( "UnBlockingRequest.msg.selectedDisbursementAlreadyBindedWithRequet" ),	
																  disbursementDetailsView.getRefNum(),
																  request.getRequestNumber()
																);
					errorMessages.add( msg );
					hasError = true;
//					tabPanel.setSelectedTab(TAB_DETAILS);
					break;
				}
			}
			
		}
		
		return hasError;
	}
	
	
	@SuppressWarnings("unchecked")
	public void onMarkUnMarkChanged()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			for (DisbursementDetailsView dtView : getDisbursementDetails()) 
			{
				dtView.setSelected(markUnMarkAll != null && markUnMarkAll );
			}
			
	        
		}
		catch( Exception e)
		{
			logger.LogException( " onMarkUnMarkChanged --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(	ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );

		}
	}	


	@SuppressWarnings("unchecked")
	public void onUnblockSelected()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			List<DisbursementDetailsView> selectedDetails = new ArrayList<DisbursementDetailsView>(); 
			for (DisbursementDetailsView dtView : getDisbursementDetails()) 
			{
              if( dtView.getSelected() == null || !dtView.getSelected() ) { continue; }
              selectedDetails.add( dtView );	 
			}
			if( hasUnblockError( selectedDetails  ) ) return;
			
	        ApplicationContext.getContext().getTxnContext().beginTransaction();
	        
	        
			BlockingAmountService blockService = new BlockingAmountService();
			RequestView reqView = getRequestView();
			
			Request unblockingRequest = blockService.populateUnBlockingRequestFromDisbursementView(
																									 reqView, 
																									 selectedDetails,
																									 getLoggedInUserId()
																								  );
			invokeUnblockBPEL( unblockingRequest ); 			
            String msg =  ResourceUtil.getInstance().getProperty( "disbursement.msg.unblockingRequestSaved" );			
            msg =  java.text.MessageFormat.format( 
		            								msg, 
		            								unblockingRequest.getRequestNumber()
		            							  );	
            
			successMessages.add( msg );
	        ApplicationContext.getContext().getTxnContext().commit();
		}
		catch( Exception e)
		{
			logger.LogException( " onUnblockSelected --- EXCEPTION --- ", e );
	        ApplicationContext.getContext().getTxnContext().rollback();
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(	ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );

		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}	

	@SuppressWarnings("unchecked")
	public void onOpenPayVendorNotificationPopup()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			sessionMap.put(WebConstants.REQUEST , getRequestView() );
			sessionMap.put("Disbursements", getDisbursementDetails());
			executeJavaScript("javaScript:openPayVendorNotificationPopup();");
		}
		catch( Exception e)
		{
			logger.LogException( " onOpenPayVendorNotificationPopup--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(	ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}

	}	


	
	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeUnblockBPEL(Request request) throws Exception, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSBlockingBPEL" );
		 MEMSBlockingBPELPortClient port=new MEMSBlockingBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		Integer.parseInt( request.getRequestId().toString()),
				 		CommonUtil.getLoggedInUser(),
				 		"0", 
				 		null, 
				 		null
				 	   );
	}
	
	@SuppressWarnings( "unchecked" )
	public void editPaymentDetails() throws Exception
	{
			DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();
			String fetchPersonsById = WebConstants.PERSON_ID+ "="+ detailData.getVendorId().toString();
			sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, detailData);
			if( detailData.getIsMultiple()!= null && detailData.getIsMultiple().longValue() == 1 )
			{
				sessionMap.put( WebConstants.MULTIPLE_BENEFICIARIES, "1" );
				sessionMap.put( WebConstants.DISBURSEMENT_DETAIL_ID, detailData.getDisDetId() );
			}
			String javaScriptText = " javaScript:openPaymentDetailsPopUp( '"+fetchPersonsById+"');";
			executeJavaScript( javaScriptText	);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenFilePopup() 
	{
		try	 
		{
			DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, gridViewRow.getFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
		    executeJavaScript("openFile()");		    		    
		}
		catch (Exception ex)  
		{
			logger.LogException("[Exception occured in onOpenFilePopup()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp()
   {
    	try
    	{
    		DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
    		if(	gridViewRow.getPersonId()>0 && 
    			(
					gridViewRow.getIsMultiple()== null ||	
					gridViewRow.getIsMultiple().compareTo(0L)==0
    			)
    		  )
    		{
    			executeJavaScript("javaScript:openBeneficiaryPopup("+ gridViewRow.getPersonId()+");");
    		}
    		else if( gridViewRow.getBeneficiaryCount() > 1 )
    		{
    		  openMultipleBeneficiaryPopup(gridViewRow, true);
    		}
    		else
    		{
    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_BENEFICIARY));
    		}
	   } catch (Exception ex) {
			logger.LogException("[Exception occured in openManageBeneficiaryPopUp()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void openMultipleBeneficiaryPopup( DisbursementDetailsView disbursement,boolean readOnly )throws Exception
	{
		sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, disbursement);
		sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, disbursement.getFileId());
		if(readOnly)
		{
			sessionMap.put( WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
		}
		executeJavaScript( "javaScript:openAddMultipleBenefificiaryForDisbursementPopup();");
	}
	private boolean hasRejectionError()
	{
		if( htmlRFC.getValue()== null || htmlRFC.getValue().toString().trim().length()  <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "socialResearchRecommendation.msg.rejectionReasonRequired" ));
			return true;
		}
		return false;
	}
	
	private boolean hasRejectFinanceError()throws Exception
	{
	

		FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
		
		List<DisbursementDetailsView> ddView = tabFinanceController.getDisbDetails();
		
		for(DisbursementDetailsView singleView : ddView) 
		{
			if(  singleView.getIsDisbursed().longValue()==1l  || !tabFinanceController.getRenderDisburse()) 
			{
				errorMessages.add(CommonUtil.getBundleMessage("disburseFinance.msg.rejectionError"));
				richPanel.setSelectedTab("financeTab"	);
				return true;				
			}
		}
		if( hasRejectionError() )
		{
			return true;
		}	
		return false;
	}
	
	public void rejectFinance() 
	{
		try 
		{
			errorMessages = new ArrayList<String>();
			if( hasRejectFinanceError() )
				return; 
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				if(	doSaveApplication( WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID )	)
				{
					completeTask( TaskOutcome.REJECT);
					pageModeView=true;
					refreshTabs(getRequestView());
					CommonUtil.saveSystemComments( WebConstants.REQUEST,"disbursementRequest.event.financeRejected", getRequestView().getRequestId());
					successMessages.add(CommonUtil.getBundleMessage("disbursementRequest.msg.financeRejected" ));
				}
            
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in rejectFinance()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onApproved() 
	{
		try 
		{
			errorMessages = new ArrayList<String>();
			if( validateApprove() ) 
			{
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				refreshTabs( reqView);
				doSaveApplication( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID );
				completeTask( TaskOutcome.APPROVE);
				pageModeView=true;
				refreshTabs( getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_APPROVED,
						                       getRequestView().getRequestId());
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_APPROVE));
			}
		} catch (Exception ex) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in onApproved()]", ex);
		}
	}
	
	public boolean hasOnRejectError () throws Exception
	{
		if( htmlRFC.getValue() == null || StringHelper.isEmpty( htmlRFC.getValue().toString().trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onReject() 
	{
		try 
		{
			    if( hasOnRejectError() ) return;
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				refreshTabs( reqView);
				doSaveApplication( WebConstants.REQUEST_STATUS_REJECTED_ID );
				completeTask( TaskOutcome.REJECT);
				pageModeView=true;
				refreshTabs( getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_REJECTED,
						                       getRequestView().getRequestId());
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REJECT));
		} catch (Exception ex) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in onReject()]", ex);
		}
	}
	@SuppressWarnings( "unchecked" )
	public boolean hasRemoveDisbursementError() throws Exception
	{
		
		List<DisbursementDetailsView > list = getDisbursementDetails();
		 
		
		if( list.size() <= 1 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("payVendor.msg.disRequired"));
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{

			DisbursementDetailsView ddView =  ( DisbursementDetailsView )disbursementTable.getRowData();
			String csvPersonId = ",";
			for(DisbursementRequestBeneficiaryView singleBeneficiary : ddView.getRequestBeneficiaries()) 
			{
				csvPersonId += singleBeneficiary.getPersonId().toString()+",";  
			}
			Long requestId = -1l;
			if( getRequestView().getRequestId() != null )
			{
				requestId  = getRequestView().getRequestId();
			}
			List<Long> disDetIdNotIn  = new ArrayList<Long>();
			for(DisbursementDetailsView singleView : getDisbursementDetails() ) 
			{
				disDetIdNotIn.add(singleView.getDisDetId());
			}
			sessionMap.put("disDetIdNotIn", disDetIdNotIn);
		
			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+csvPersonId+"','"+
								  														ddView.getFileId()+"','"+ 
								  														requestId+
								  												 "');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			DisbursementDetailsView ddView =  ( DisbursementDetailsView )disbursementTable.getRowData();
			String csvPersonId = ",";
			for(DisbursementRequestBeneficiaryView singleBeneficiary : ddView.getRequestBeneficiaries()) 
			{
				csvPersonId += singleBeneficiary.getPersonId().toString()+",";  
			}
			executeJavaScript(
					  "javaScript:openBlockingDetailsPopup('"+csvPersonId+"','"+ddView.getFileId()+"');"
					 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}


	@SuppressWarnings( "unchecked" )
	public void onRemoveDisbursement() 
	{
		try
		{
				if( hasRemoveDisbursementError() ) 
				{return; }
				DisbursementDetailsView ddView =  ( DisbursementDetailsView )disbursementTable.getRowData();
				DisbursementService service = new DisbursementService();
				RequestView request = getRequestView();
				service.removeDisbursementFromPaymentVendor( request , ddView, getLoggedInUserId() );
				CommonUtil.saveSystemComments( 
											   WebConstants.REQUEST, 
											   "payVendor.msg.removeDisbursement",
											   request.getRequestId(),
						                       ddView.getRefNum(),
						                       ddView.getAmount().toString()
				                            );
				refreshTabs( request );
				List<DisbursementDetailsView > list = getDisbursementDetails();
				list.remove(ddView);
				saveAttachmentsAndComments(request.getRequestId());
				successMessages.add( CommonUtil.getBundleMessage( "payVendor.msg.removeSuccessfully"  ) );
		} 
		catch (Exception ex) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in onRemoveDisbursement]", ex);
		}
	}
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( cmbReviewGroup.getValue() == null || StringHelper.isEmpty( cmbReviewGroup.getValue().toString().trim() ) || 
			this.cmbReviewGroup.getValue().toString().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( htmlRFC.getValue() == null || StringHelper.isEmpty( htmlRFC.getValue().toString().trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	public void review() 
	{
		try 
		{
			if( !isReviewValidated() )
				return ;
			UtilityService utilityService = new UtilityService();
			if( doSaveApplication( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID  )) 
			{
				refreshTabs(getRequestView());
				ReviewRequestView reviewRequestView = new ReviewRequestView();
				reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
				reviewRequestView.setCreatedOn( new Date() );
				reviewRequestView.setRfc( htmlRFC.getValue().toString().trim() );
				reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
				reviewRequestView.setRequestId( getRequestView().getRequestId() );
				utilityService.persistReviewRequest( reviewRequestView );				
		
				completeTask(TaskOutcome.OK);
				pageModeView = true;
				htmlRFC.setValue("");
				cmbReviewGroup.setValue("-1");
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW, 
						getRequestView().getRequestId());
				
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
										MemsPaymentDisbursementMsgs.REVIEW_REQUIRED));
			}
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void reviewDone() 
	{
		try 
		{
			UtilityService utilityService = new UtilityService();
			ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			utilityService.persistReviewRequest( reviewRequestView );
			
			
			if( doSaveApplication( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID )  ) 
			{
				completeTask(TaskOutcome.OK);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_REVIEWED, 
						getRequestView().getRequestId());
				pageModeView = true;
				refreshTabs(getRequestView());
			}
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
											MemsPaymentDisbursementMsgs.REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean validateComplete() 
	{
		if (!viewMap.containsKey(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE)) {
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.ERR_COMPLETE));
			return false;
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	public void complete() {
		try 
		{
			if( !validateComplete() ) 
			{return;}
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				refreshTabs( reqView);
				completeTask( TaskOutcome.APPROVE);
				doSaveApplication( getStatusCompleteId());
				pageModeView=true;
				refreshTabs( getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED,
						                       getRequestView().getRequestId());
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_COMPLETE));
		} 
		catch (Exception ex) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in complete()]", ex);
		}
	}
	@SuppressWarnings("unchecked")
	public void onPrint()
    {
    	try
    	{
    			CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	
	public void submitQuery() 
	{
		try 
		{
				if(! validateInput() ) 
				{return;}
				Long status = getRequestView().getStatusId();
				/* always save the request before submitting it */
				if( doSaveApplication( getStatusNewId())) 
				{
					if( status.equals( INVALID_STATUS)) 
					{
						CommonUtil.saveSystemComments( WebConstants.REQUEST,MessageConstants.RequestEvents.REQUEST_CREATED,
														getRequestView().getRequestId()
													  );
					}
					doSubmit();
					successMessages.add(CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SUBMIT));
					pageModeView=true;
					CommonUtil.printMemsReport( this.getRequestView().getRequestId()  );
				}	
			
		} catch (Exception ex) {
			logger.LogException( "[Exception occured in submitQuery()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void showRequestPopup() {
	}
	
	@Override
	public void prerender() 
	{
		try 
		{
			readParams();		
			if (viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS)) {				
				List<String> sucMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);					
				successMessages.addAll(sucMsgs);
				viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);
			}
			
			if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS)) {
				List<String> errMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS); 
				errorMessages.addAll( errMsgs);
				viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);
			}
			if(viewMap.containsKey(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES))
			{
				errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES) );
				viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES);
				
			}
			if(viewMap.containsKey(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES))
			{
				errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES) );
				viewMap.remove(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES);
				
			}
			if (  getIsReviewRequired()  )
			{
				viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			}
			if( 	!isTaskAvailable() && 
					( 
					  getIsApprovalRequired() || getIsReviewRequired() || 
					  getIsReviewed() || getIsFinanceRejected() || getIsDisbursementRequired()  
			 
					)
			 )
			{
				if( viewMap.remove("fetchTaskNotified") == null  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
					viewMap.put("fetchTaskNotified", true);
				}
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in prerender()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}
	
	public void getRequestHistory() {
		try {
			Long reqId = getRequestView().getRequestId();
			if ( null!=reqId && !reqId.equals(INVALID_STATUS)) {
				RequestHistoryController rhc = new RequestHistoryController();
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, reqId.toString());						
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in getRequestHistory()]", ex);
		}
	}
	
	public List<DisbursementDetailsView> getDisbursementDetails() {
		if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS)) {
			return (List<DisbursementDetailsView>) viewMap.get( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS);
		}
		return new ArrayList<DisbursementDetailsView>();
	}
	
	private void readParams() throws Exception 
	{
		if( StringHelper.isNotEmpty( hdnVendorId) ) 
		{
			viewMap.put( VENDOR_ID, hdnVendorId);
			viewMap.put( VENDOR_NAME, hdnVendorName);			
			txtVendor.setValue( hdnVendorName);											
			List<DisbursementDetailsView> list = new DisbursementService().getDisbursementDetails( new Long( hdnVendorId));
			if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS)) {
				viewMap.remove( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS);
			}
			double sumDisbursement = 0d;
			for (DisbursementDetailsView disbursementDetailsView : list) 
			{
				if( disbursementDetailsView.getIsDeleted().compareTo( 1L ) == 0 )
					continue;
				sumDisbursement += disbursementDetailsView.getAmount(); 
			}
			this.setSumDisbursements(oneDForm.format(sumDisbursement)); 
			PersonView vendor = new PersonView();
			vendor.setPersonId( new Long( hdnVendorId));
			getRequestView().setContactorView( vendor);
			viewMap.put( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS, list);
			setTotalRows( list.size());
			hdnVendorId = null;
			hdnVendorName = null;
		} else {
			if( hdnPersonId!=null && !hdnPersonId.equals("")) {						
				RequestView reqView = getRequestView();
				PersonView applicant = reqView.getApplicantView();			
				if( null!=applicant) {
					applicant.setPersonId(Long.parseLong(hdnPersonId));
				} else {
					applicant = new PersonView();
					applicant.setPersonId(Long.parseLong(hdnPersonId));
					reqView.setApplicantView(applicant); 				
				}					
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}
			
			if( hdnPersonName!=null && !hdnPersonName.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			}
			
			if( null!=hdnCellNo && !hdnCellNo.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, hdnCellNo);
			}
			
			if( null!=hdnPersonType && !hdnPersonType.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, hdnPersonType);
			}
		}
	}

	
	private void refreshTabs(RequestView reqView) {
		if (null == reqView && null == reqView.getApplicantView()) {
			throw new IllegalArgumentException();
		}

		viewMap.put( WebConstants.ApplicationDetails.APPLICATION_DATE, reqView.getRequestDate());
		viewMap.put( WebConstants.ApplicationDetails.APPLICATION_NUMBER, reqView.getRequestNumber());
				
		PersonView applicant = reqView.getApplicantView();
		
		viewMap.put(WebConstants.LOCAL_PERSON_ID, applicant.getPersonId());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,applicant.getPersonId());
				
		if ( null!=applicant.getPersonFullName() ) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					applicant.getPersonFullName());										
		}

		if ( null!=applicant.getCellNumber() ) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					applicant.getCellNumber());
		}

		String desc = reqView.getDescription();

		if ( null != desc && 0 != desc.trim().length() ) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,
					reqView.getDescription());				
		}

		if (CommonUtil.getIsEnglishLocale()) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusEn());					
		} else if (CommonUtil.getIsArabicLocale()) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusAr());
					
		}
		
		if (!getIsNewStatus()) {
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
			imgVendor.setRendered( false);
		}
		
		if( !getIsInvalidStatus() ) {
			imgVendor.setRendered( false);
		}

		if( getIsDisbursementRequired() ) {
			populateFinanceData();
		}
		
		if( viewMap.containsKey( VENDOR)) {
			PersonView vendor = (PersonView) viewMap.get( VENDOR);
			txtVendor.setValue( vendor.getPersonFullName());
		}
				
		handleAttachmentCommentTabStatus(reqView);
	}

	private boolean validateSubmitQuery() throws Exception 
	{
		if( null==getRequestView().getRequestDetailView() || 0==getRequestView().getRequestDetailView().size()) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPayVendorMsgs.ERR_NO_RECORD_SELECTED));
			return false;
		}
		return true;
	}
	
	private boolean validateInput()throws Exception 
	{
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			return false;
		}
		if( null==getRequestView().getContactorView() ) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPayVendorMsgs.ERR_SELECT_VENDOR));
			return false;
		}
		
		if(getDisbursementDetails() == null && getDisbursementDetails().size() <= 0)
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPayVendorMsgs.ERR_NO_RECORD_SELECTED));
			return false;
		}
		else 
		{
			List<DisbursementDetailsView> disDtlList = null;
			disDtlList = getDisbursementDetails();
			boolean isSelected = false;
			for(DisbursementDetailsView disbursement : disDtlList)
			{
				if(disbursement.getSelected())
				{
					isSelected = true;
					break;
				}
			}
			if( !isSelected )
			{
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPayVendorMsgs.ERR_NO_RECORD_SELECTED));
				return false;
			}
		}
		return true;
	}
	
	private void populateFinanceData() {
			
		try
		{
			FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
			if( null!=tabFinanceController ) {
				tabFinanceController.handleTabChange();
			}
		}
		catch (Exception ex) {
				logger.LogException( "[Exception occured in populateFinanceData()]", ex);
		}
	}
	
	private boolean doSaveApplication(final Long statusId) throws Exception 
	{
			RequestService reqWS = new RequestService();
						
			RequestView reqView = getRequestView();
			reqView.setStatusId(statusId);
			Long requestId = reqWS.addRequest( reqView);
			
			RequestView updated = reqWS.getRequestById( requestId);
			setRequestView( updated);
			
			List<DisbursementDetailsView> disbursements = getDisbursementDetails();

			if(statusId.equals( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID) ) 
			{
				for (DisbursementDetailsView disbursementDetailsView : disbursements) 
				{
					reqWS.saveOrUpdate( disbursementDetailsView );
				}
			}	
			/* if the request is in new status 
			 * 			or request is being rejected from approval step 
			 * the user can change his mind about the disbursement that could be
			 * sent to the finance. this check will never be executed once the request is submitted 
			 */
			
			if( statusId.equals( getStatusNewId()) ||
				statusId.equals( WebConstants.REQUEST_STATUS_REJECTED_ID ) 	
			  ) 
			{
				
				
				/* get the existing request details and mark them deleted */
				List<RequestDetailView> existingDetails = reqWS.getRequestDetailByRequestId( reqView.getRequestId());				
				if( null!=existingDetails && 0<existingDetails.size() ) 
				{
					for(RequestDetailView eachRequestDetailView : existingDetails) 
					{
						eachRequestDetailView.setIsDeleted( WebConstants.IS_DELETED_TRUE);
						eachRequestDetailView.setRequestId( getRequestView().getRequestId());
					}
					//reqWS.updateRequestDetails( existingDetails, getRequestView());
					RequestView request = getRequestView();
					request.setUpdatedBy( getLoggedInUserId() );
					reqWS.updateRequestDetails(request , existingDetails);
					
				}
				
				if( statusId.equals( getStatusNewId() ) )
				{
						/* add newly added request details in the database */
						UtilityService service = new UtilityService();
						DomainDataView ddBinded = service.getDomainDataByValue( WebConstants.DisbursementStatus.DOMAIN_KEY,
								WebConstants.DisbursementStatus.BINDED_TO_REQUEST);
						DomainDataView ddOutStandingVendor = service.getDomainDataByValue( WebConstants.DisbursementStatus.DOMAIN_KEY,
								WebConstants.DisbursementStatus.OUTSTAND_VENDOR);
								
						Set<RequestDetailView> reqDetailsSet = new HashSet<RequestDetailView>();			
						for(DisbursementDetailsView eachDisbursement : disbursements) 
						{
							/* reset the status of each disbursement to outstanding vendor */
							eachDisbursement.setStatus( ddOutStandingVendor.getDomainDataId());
							eachDisbursement.setStatusEn( ddOutStandingVendor.getDataDescEn());
							eachDisbursement.setStatusAr( ddOutStandingVendor.getDataDescAr());
							eachDisbursement.setIsDisbursed( 0l );
							/* insert the selected the records and update the disbursement status accordingly */
							if(
								eachDisbursement.getSelected()  != null && 
								eachDisbursement.getSelected() 
							  ) 
							{
								RequestDetailView reqDetailView = new RequestDetailView();
								reqDetailView.setUpdatedBy( getLoggedInUserId());
								reqDetailView.setCreatedBy( getLoggedInUserId());
								reqDetailView.setUpdatedOn( DateUtil.getCurrentDate());
								reqDetailView.setCreatedOn( DateUtil.getCurrentDate());
								reqDetailView.setDisbursementDetailsId( eachDisbursement.getDisDetId()); 
								reqDetailView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
								reqDetailView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
								reqDetailView.setRequestId( getRequestView().getRequestId());
								eachDisbursement.setStatus( ddBinded.getDomainDataId());
								eachDisbursement.setStatusEn( ddBinded.getDataDescEn());
								eachDisbursement.setStatusAr( ddBinded.getDataDescAr());
																	
								reqDetailsSet.add( reqDetailView );
							}
						}			
						reqWS.addRequestDetailsUpdateDisbursements( reqDetailsSet );
						getRequestView().setRequestDetailView( reqDetailsSet );
				}
				
			}
			saveAttachmentsAndComments( getRequestView().getRequestId());
		return true;
	}

	
	
	private void initFromRequestSearch(RequestView reqView) throws Exception 
	{
		setRequestView( reqView);
		List<Long> idsList = null;
		List<DisbursementDetailsView> disbursements = null;
		Set<RequestDetailView> details = reqView.getRequestDetailView();
		idsList = new ArrayList<Long>( details.size());
		for(RequestDetailView view : details) 
		{
			if( view.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED)) 
			{
				idsList.add( view.getDisbursementDetailsId());
			}
		}
		if( getIsNewStatus() ) 
		{
			DisbursementService service = new DisbursementService();
			List<DisbursementDetailsView> savedList = service.getDisbursementDetails( idsList);
			disbursements = service.getDisbursementDetails( reqView.getContactorView().getPersonId());
			disbursements.addAll( savedList);
			for(Long singleId : idsList ) 
			{
				for(DisbursementDetailsView singleDisbursement : disbursements) 
				{
					if( singleDisbursement.getDisDetId().equals( singleId)) 
					{
						singleDisbursement.setSelected( true);
					}
				}
			}
		}
		else 
		{
			DisbursementService service = new DisbursementService();
			disbursements = service.getDisbursementDetails( idsList);						
		}		
		
		if( null==txtVendor.getValue() || 0==txtVendor.getValue().toString().trim().length()) 
		{
			Long personId = reqView.getContactorView().getPersonId();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			PersonView vendor = psa.getPersonInformation( personId);
			viewMap.put( VENDOR, vendor);
			txtVendor.setValue( vendor.getPersonFullName());
		}
		
		viewMap.put( DISBURSEMENT_IDS_LIST, idsList);
		viewMap.put( WebConstants.FinanceTabPublishKeys.DISBURSEMENTS, disbursements);
		double sumDisbursement = 0d;
		for (DisbursementDetailsView disbursementDetailsView : disbursements) 
		{
			if( disbursementDetailsView.getIsDeleted().compareTo( 1L ) == 0 )
				continue;
			sumDisbursement += disbursementDetailsView.getAmount(); 
		}
		this.setSumDisbursements(oneDForm.format(sumDisbursement)); 
		setTotalRows( disbursements.size());
		refreshTabs( getRequestView());
	}
	
	private void initFromUserTask(UserTask userTask) throws Exception {
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		Map taskAttributes = userTask.getTaskAttributes();
		if (null != taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)) {
			Long reqId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));					
			RequestService reqSrv = new RequestService();
			RequestView requestView = reqSrv.getRequestById(reqId);
			initFromRequestSearch(requestView);
		}
	}
	
	private void saveAttachmentsAndComments(Long reqId) throws PimsBusinessException {	
		loadAttachmentsAndComments(reqId);
		CommonUtil.saveAttachments(reqId);
		String noteOwner = (String) viewMap.get("noteowner");
		CommonUtil.saveComments(reqId, noteOwner);
		if( htmlRFC.getValue() != null && htmlRFC.getValue().toString().trim().length() > 0)
		{
			CommonUtil.saveRemarksAsComments(reqId, htmlRFC.getValue().toString().trim(), noteOwner) ;
		}
	}
	
	private void loadAttachmentsAndComments(Long requestId) {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(
				PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = viewMap.get(EXTERNAL_ID).toString();
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		if (null != requestId) {
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
	private void handleAttachmentCommentTabStatus(RequestView view) {
		if (view.getStatusId().equals(getStatusCompleteId()) || view.getStatusId().equals( getStatusRejectedId())) {
			viewMap.put("canAddAttachment", false);
			viewMap.put("canAddNote", false);
		} else {
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
		}
	}

	private boolean areDisbursementDetailSame(DisbursementDetailsView disbursements,DisbursementDetailsView loopedDisbursement) 
	{
		//If looped disbursement doesnt have DisDetId then compare the hashCodes
		return loopedDisbursement.getDisDetId()== null ?loopedDisbursement.getId() == disbursements.getId():
														loopedDisbursement.getDisDetId().equals( disbursements.getDisDetId() ) ;
	}
	
	public void loadPaymentDetails(ActionEvent evt) 
	{
		
		errorMessages = new ArrayList<String>();
		try
		{
					DisbursementDetailsView view = (DisbursementDetailsView) sessionMap.remove(WebConstants.LOAD_PAYMENT_DETAILS);				
					if( sessionMap.containsKey( WebConstants.FinanceTabPublishKeys.IS_FINANCE)) 
					{
						sessionMap.remove( WebConstants.FinanceTabPublishKeys.IS_FINANCE);
						FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
						if( null!=tabFinanceController ) 
						{
							tabFinanceController.loadPaymentDetails( view);
						}
					}
					else 
					{
						PaymentCriteriaView criteriaView = view.getPaymentCriteriaView();
						List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
						for (DisbursementDetailsView dtView : disbursementDetails) 
						{
							if (areDisbursementDetailSame(view, dtView)) 
							{
								List<PaymentDetailsView> listPaymentDetailsView = dtView.getPaymentDetails();
								if( null==listPaymentDetailsView ) 
								{
									listPaymentDetailsView = new ArrayList<PaymentDetailsView>();
									dtView.setPaymentDetails( listPaymentDetailsView);
								}
								PaymentDetailsView paymentView = null;
								if( 0<listPaymentDetailsView.size() ) 
								{
									paymentView = listPaymentDetailsView.get( 0);
								} 
								else 
								{
									paymentView = new PaymentDetailsView();
									paymentView.setMyHashCode(paymentView.getMyHashCode()); 
									paymentView.setStatus( WebConstants.NOT_DISBURSED);
									paymentView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
								}
								
								paymentView.setAccountNo(criteriaView.getAccountNumber());
								paymentView.setAmount(criteriaView.getAmount());
								paymentView.setBankId(criteriaView.getBankId());
								if (null != criteriaView.getPaymentMode()) 
								{
									paymentView.setPaymentMethod(new Long(criteriaView.getPaymentMode()));
								}
								if( null!=criteriaView.getPaidTo()) 
								{
									paymentView.setPaidTo( new Long( criteriaView.getPaidTo()));
									paymentView.setOthers( null );
								}
								else if ( criteriaView.getOther() != null && criteriaView.getOther().trim().length() >0 )
								{
									paymentView.setOthers(criteriaView.getOther().trim() );
									paymentView.setPaidTo(  null );
								}
								paymentView.setAmount(dtView.getAmount());
								paymentView.setRefDate(criteriaView.getDate());
								paymentView.setReferenceNo(criteriaView.getReferenceNumber());
								if( 0==listPaymentDetailsView.size() ) 
								{
									listPaymentDetailsView.add( paymentView);
								}
								successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
							}
						}
				}
		}
		catch( Exception exception) 
		{		
			 errorMessages.clear();
			 logger.LogException("loadPaymentDetails() crashed ", exception);
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} 
	}
	
	@SuppressWarnings("unchecked")
	private Boolean invokeBpel(Long requestId) 
	{
		 Boolean success = true;
		 try 
		 {
			 MEMSPayVendorBPELPortClient bpelClient = new MEMSPayVendorBPELPortClient();
			 SystemParameters parameters = SystemParameters.getInstance();			 			 			 		
			 String endPoint = parameters.getParameter( WebConstants.MemsBPELEndPoints.MEMS_PAY_VENDORBPEL_END_POINT);			 
			 bpelClient.setEndpoint( endPoint);
			 String userId = CommonUtil.getLoggedInUser();
			 bpelClient.initiate( requestId, userId, null, null);			 
		 } 
		 catch(PIMSWorkListException exception) {		
			 success = false;
			 logger.LogException("invokeBpel() crashed ", exception);
		 } catch (Exception exception) {		 
			 success = false;
			 logger.LogException("invokeBpel() crashed ", exception);
		 } finally {		 
			 if(!success) {
				 errorMessages.clear();
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 }
		 }
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private Boolean completeTask(TaskOutcome taskOutcome) throws Exception 
	{
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext)
		getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");		 
		String loggedInUser = CommonUtil.getLoggedInUser();
		if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		} else {
			userTask = getIncompleteRequestTasks();
		}
		 
		 BPMWorklistClient client = new BPMWorklistClient(contextPath);
		 if(userTask!=null) {
			 client.completeTask(userTask, loggedInUser, taskOutcome);
		 }
		 return success;		
	}
	
	private UserTask getIncompleteRequestTasks() {
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";		 
		try {
	   		Long requestId = getRequestView().getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();	   		
	   		if( null!=taskId ) {	   		
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);				 
				taskType = userTask.getTaskType();
				logger.logInfo("Task Type is:" + taskType);
				return userTask;
	   		} else {	   		
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		} catch(PIMSWorkListException ex) {				
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {			
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC) {			
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else {			
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		} catch (Exception exception) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		return null;
	}				


	
	private void setRequestView(RequestView reqView) {		
		@SuppressWarnings("unused")
		RequestView oldView = (RequestView) viewMap.remove(WebConstants.REQUEST_VIEW);				
		oldView = null;
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
	}
	
	public String getVendorId() {
		if( viewMap.containsKey( VENDOR_ID)) {
			return (String) viewMap.get( VENDOR_ID);
		}
		return StringHelper.EMPTY;
	}
	
	public String getVendorName() {
		if ( viewMap.containsKey( VENDOR_NAME)) {
			return (String) viewMap.get(VENDOR_NAME);
		}
		return StringHelper.EMPTY;
	}
	
	private RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}
	
	public Boolean getIsInvalidStatus() {	
		return getRequestView().getStatusId().equals( INVALID_STATUS);
	}

	public Boolean getIsReviewed() {
		return !pageModeView &&  getRequestView().getStatusId().equals( getStatusReviewedId());		
	}

	public Boolean getIsReviewRequired() {
		return !pageModeView && getRequestView().getStatusId().equals( getStatusReviewRequiredId());
	}
	
	public Boolean getIsDisbursementRequired() {
		return !pageModeView && getRequestView().getStatusId().equals( getStatusDisbursementReqId());
	}

	public Boolean getIsApproved() {
		return !pageModeView && getRequestView().getStatusId().equals( getStatusApprovedId());		
	}

	public Boolean getIsFinanceRejected() {
		return !pageModeView && getRequestView().getStatusId().equals( WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID );		
	}
	public Boolean getIsApprovalRequired() {
		return !pageModeView && getRequestView().getStatusId().equals( getStatusApprovalRequiredId());		
	}

	public Boolean getIsNewStatus() {
		Long statusId = this.getRequestView().getStatusId();
		return ( !pageModeView && ( statusId.equals( INVALID_STATUS)) || (statusId.equals( getStatusNewId()) ));		
	}

	
	private Long getStatusDisbursementReqId() {
		if( !viewMap.containsKey( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ);
	}
	
	private Long getStatusNewId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_NEW)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_NEW);
	}

	private Long getStatusCompleteId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_COMPLETE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
	}

	private Long getStatusApprovedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVED);
	}

	private Long getStatusRejectedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED);
	}

	private Long getStatusApprovalRequiredId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap
				.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
	}

	private Long getStatusReviewRequiredId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED);
	}

	private Long getStatusReviewedId() {
		if (!viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_DONE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_DONE);
	}

	public HtmlInputText getTxtVendor() {
		return txtVendor;
	}

	public void setTxtVendor(HtmlInputText txtVendor) {
		this.txtVendor = txtVendor;
	}

	public HtmlGraphicImage getImgVendor() {
		return imgVendor;
	}

	public void setImgVendor(HtmlGraphicImage imgVendor) {
		this.imgVendor = imgVendor;
	}

	public HtmlTabPanel getRichPanel() {
		return richPanel;
	}

	public void setRichPanel(HtmlTabPanel richPanel) {
		this.richPanel = richPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}

	public String getHdnVendorId() {
		return hdnVendorId;
	}

	public void setHdnVendorId(String hdnVendorId) {
		this.hdnVendorId = hdnVendorId;
	}

	public String getHdnVendorName() {
		return hdnVendorName;
	}

	public void setHdnVendorName(String hdnVendorName) {
		this.hdnVendorName = hdnVendorName;
	}

	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}

	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}

	public HtmlInputTextarea getHtmlRFC() {
		return htmlRFC;
	}

	public void setHtmlRFC(HtmlInputTextarea htmlRFC) {
		this.htmlRFC = htmlRFC;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public Boolean getMarkUnMarkAll() {
		return markUnMarkAll;
	}

	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
	}

	@SuppressWarnings("unchecked")
	public String getSumDisbursements() {
		if(viewMap.get("sumDisbursements") != null )
		{
			sumDisbursements  =  viewMap.get("sumDisbursements").toString() ; 
		}
		return sumDisbursements;
	}

	@SuppressWarnings("unchecked")
	public void setSumDisbursements(String sumDisbursements) {
		
		this.sumDisbursements = sumDisbursements;
		if(this.sumDisbursements != null )
		{
			 viewMap.put("sumDisbursements", this.sumDisbursements);
		}
	}

	
	

}
