package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.DocumentManager;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.TenderDetailView;
import com.avanza.pims.ws.vo.TenderProposalView;
import com.avanza.pims.ws.vo.TenderSessionView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;

public class ConstructionTenderOpenProposalBacking extends AbstractController {

	//Common Initialization
	private static final long serialVersionUID = -4343161065231391963L;
	private Logger logger = Logger.getLogger(ConstructionTenderOpenProposalBacking.class);
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	private Map<String, Object> viewMap = getFacesContext().getViewRoot().getAttributes();
	private Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	private CommonUtil commonUtil = new CommonUtil();
	private UtilityManager utilityManager = new UtilityManager();
	private final String notesOwner = WebConstants.Tender.TENDER;

	private ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

	private Long tenderId = 0L;
	private TenderView tenderView = new TenderView();
	private TenderDetailView tenderDetailView = new TenderDetailView();
	private TenderSessionView tenderSessionView = new TenderSessionView();



	private HtmlTabPanel tabPanel = new HtmlTabPanel();

	//Tender Details Tab
	private HtmlInputText tenderNumber = new HtmlInputText();
	private HtmlInputText tenderStatue =new HtmlInputText();
	private HtmlInputText tenderDescription = new HtmlInputText();
	private HtmlInputText estimatedCost = new HtmlInputText();
	private HtmlInputText tenderDocSellingPrice = new HtmlInputText();
	private Date endDate = new Date();
	private Date issueDate = new Date();
	private String hhEnd = "";
	private String mmEnd = "";
	private String hhStart = "";
	private String mmStart = "";
	public  HtmlSelectOneMenu ddnConstructionServiceType = new HtmlSelectOneMenu();
	public HtmlInputText txtProjectNumber = new HtmlInputText();
	public HtmlInputText txtProjectName = new HtmlInputText();
	public HtmlInputText txtProjectEstimatedCost = new HtmlInputText();
	private HtmlSelectOneMenu tenderType = new HtmlSelectOneMenu();
	private List<SelectItem> tenderTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu startDateHoursMenu =new HtmlSelectOneMenu();
	private HtmlSelectOneMenu endDateHoursMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu startDateMinsMenu =new HtmlSelectOneMenu();
	private HtmlSelectOneMenu endDateMinsMenu = new HtmlSelectOneMenu();
	private List<SelectItem> hoursList = new ArrayList<SelectItem>();
	private List<SelectItem> minsList = new ArrayList<SelectItem>();
	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton sendButton = new HtmlCommandButton();

		//Session Details Tab
	private Date sessionDate = new Date();
	private HtmlSelectOneMenu sessionTimeHourMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu sessionTimeMinsMenu = new HtmlSelectOneMenu();
	private HtmlInputText sessionLocation = new HtmlInputText();
	private HtmlInputTextarea sessionRemarks = new HtmlInputTextarea();
	private String hhSessionTime = "";
	private String mmSessionTime = "";

	//	Receiving Team
	private String teamId;
	private HtmlDataTable receivingTeamGrid = new HtmlDataTable();
	private List<UserView> receivingDataList = new ArrayList<UserView>();
	private HtmlCommandButton addReceiveingTeam = new HtmlCommandButton();
	private String externalUserName;

	//Committe Memebers Tab
	private HtmlDataTable committeMemebersDataTable = new HtmlDataTable();
	private HtmlCommandButton addCommittee = new HtmlCommandButton();
	private List<UserView> committeMemebersDataList = new ArrayList<UserView>();

	//Proposals View Tab
	private HtmlDataTable proposalDataTable = new HtmlDataTable();
	private List<TenderProposalView> proposalDataList = new ArrayList<TenderProposalView>();


	private final String noteOwner = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;
	private Boolean canAttachFile;



	public ConstructionTenderOpenProposalBacking() {
		//loadScreenData();
	}

	private void loadScreenData()
	{
		try
		{
			updateListsFromViewRoot();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void updateListsFromViewRoot() throws Exception
	{
		// Tender Id
		if ( viewMap.get(WebConstants.Tender.TENDER_ID) != null )
			tenderId = (Long) viewMap.get(WebConstants.Tender.TENDER_ID);
		else
		{
			tenderId = (Long) sessionMap.get(WebConstants.Tender.TENDER_ID);
			sessionMap.remove(WebConstants.Tender.TENDER_ID);

		}

		// Tender Proposals
		if( viewMap.get(WebConstants.Tender.PROPOSAL_VIEW_LIST) != null && sessionMap.get("reloadOpener") == null )
			proposalDataList = (List<TenderProposalView>) viewMap.get(WebConstants.Tender.PROPOSAL_VIEW_LIST);
		else
		{
			proposalDataList = constructionServiceAgent.getTenderProposalList(tenderId);
			sessionMap.remove("reloadOpener");
		}

		// Tender Detail View
		if( viewMap.get(WebConstants.Tender.TENDER_DETAIL_VIEW) != null )
			tenderDetailView = (TenderDetailView) viewMap.get(WebConstants.Tender.TENDER_DETAIL_VIEW);
		else
			tenderDetailView = constructionServiceAgent.getTenderDetailViewById(tenderId);

		// Tender View
		if ( viewMap.get(WebConstants.Tender.TENDER_VIEW) != null )
			tenderView = (TenderView) viewMap.get(WebConstants.Tender.TENDER_VIEW);
		else
			tenderView = constructionServiceAgent.getTenderByIdForTender(tenderId);

		// for loading tender team
		if(viewMap.get("RECEIVING_TEAM_DATA_LIST")==null && tenderView.getTeamMemberViewList()!=null && tenderView.getTeamMemberViewList().size()>0)
		{
			fillReceivingTeamInfo(tenderView.getTeamMemberViewList());
		}

		// Tender Session
		if( viewMap.get(WebConstants.Tender.TENDER_SESSION_VIEW) != null )
			tenderSessionView = (TenderSessionView) viewMap.get(WebConstants.Tender.TENDER_SESSION_VIEW);
		else
			tenderSessionView = constructionServiceAgent.getTenderSessionViewById(tenderId);


		CommonUtil.loadAttachmentsAndComments(tenderId.toString());
		updateListsToViewRoot();
	}

	private void updateListsToViewRoot()
	{
		viewMap.put("canAddAttachment",true);
		viewMap.put("canAddNote", true);

		if(tenderId!=null && tenderId.toString().trim().length()>0)
		viewMap.put(WebConstants.Tender.TENDER_ID, tenderId);

		if(proposalDataList!=null && proposalDataList.size()>0)
		viewMap.put(WebConstants.Tender.PROPOSAL_VIEW_LIST, proposalDataList);

		if(tenderDetailView !=null )
		viewMap.put(WebConstants.Tender.TENDER_DETAIL_VIEW, tenderDetailView);

		if(tenderView !=null )
		viewMap.put(WebConstants.Tender.TENDER_VIEW, tenderView);

		if(tenderSessionView !=null )
		viewMap.put(WebConstants.Tender.TENDER_SESSION_VIEW, tenderSessionView);
	}
	private void fillReceivingTeamInfo(List<TeamMemberView> tMVList)
	 {
		 String msg = "fillReceivingTeamInfo";
		 logger.logInfo(msg+" started");
		 List<UserView> userViewList = new ArrayList<UserView>();
		 try {
				 for(TeamMemberView tMV:tMVList)
				 {
					 UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
					 UserView userView = new UserView ();

					 if(tMV.getLoginId() != null) {
						 userView.setUserName(tMV.getLoginId());
						 List<UserView> tempList = new ArrayList<UserView>();
						 tempList = utilityServiceAgent.getUsers(userView);

						 if(tempList.size() > 0)
							 userView = tempList.get(0);
					 }
					 else {
						userView.setFullNamePrimary(tMV.getExternalUserName());
						userView.setUserName(null);
					 }

					 userView.setTeamMemberId(tMV.getTeamMemberId());
					 userView.setTeamMemberIsDeleted(tMV.getIsDeleted());
					 userView.setTeamMemberRecordStatus(tMV.getRecordStatus());
					 userView.setTeamMemberCreatedBy(tMV.getCreatedBy());
					 userView.setTeamMemberCreatedOn(tMV.getCreatedOn());
					 userView.setTeamMemberUpdatedBy(tMV.getUpdatedBy());
					 userView.setTeamMemberUpdatedOn(tMV.getUpdatedOn());

					 userViewList.add(userView);

				 }
				 setReceivingDataList(userViewList);
			     logger.logInfo(msg+" completed");

			 } catch (PimsBusinessException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			 }
	 }
	public void init(){
		super.init();
		loadScreenData();
		loadTenderCommitteeTeamDataList();
		decideCanAttach();


		if(!isPostBack())
		CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
	}
	public void preprocess(){
		super.preprocess();
	}
	public void prerender(){
		super.prerender();
	}

	public HtmlCommandButton getAddCommittee() {
		return addCommittee;
	}
	public void setAddCommittee(HtmlCommandButton addCommittee) {
		this.addCommittee = addCommittee;
	}


	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	public List<UserView> getCommitteMemebersDataList() {
		return committeMemebersDataList;
	}
	public void setCommitteMemebersDataList(List<UserView> committeMemebersDataList) {
		this.committeMemebersDataList = committeMemebersDataList;
	}
	public HtmlDataTable getCommitteMemebersDataTable() {
		return committeMemebersDataTable;
	}
	public void setCommitteMemebersDataTable(HtmlDataTable committeMemebersDataTable) {
		this.committeMemebersDataTable = committeMemebersDataTable;
	}


	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public HtmlSelectOneMenu getEndDateHoursMenu() {
		return endDateHoursMenu;
	}
	public void setEndDateHoursMenu(HtmlSelectOneMenu endDateHoursMenu) {
		this.endDateHoursMenu = endDateHoursMenu;
	}
	public HtmlSelectOneMenu getEndDateMinsMenu() {
		return endDateMinsMenu;
	}
	public void setEndDateMinsMenu(HtmlSelectOneMenu endDateMinsMenu) {
		this.endDateMinsMenu = endDateMinsMenu;
	}

	public List<SelectItem> getHoursList() {
		return hoursList;
	}
	public void setHoursList(List<SelectItem> hoursList) {
		this.hoursList = hoursList;
	}

	public List<SelectItem> getMinsList() {
		return minsList;
	}
	public void setMinsList(List<SelectItem> minsList) {
		this.minsList = minsList;
	}

	public HtmlDataTable getProposalDataTable() {
		return proposalDataTable;
	}
	public void setProposalDataTable(HtmlDataTable proposalDataTable) {
		this.proposalDataTable = proposalDataTable;
	}

	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}

	public HtmlCommandButton getSendButton() {
		return sendButton;
	}
	public void setSendButton(HtmlCommandButton sendButton) {
		this.sendButton = sendButton;
	}
	public Date getSessionDate() {
		return sessionDate;
	}
	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}
	public HtmlInputText getSessionLocation() {
		return sessionLocation;
	}
	public void setSessionLocation(HtmlInputText sessionLocation) {
		this.sessionLocation = sessionLocation;
	}
	public HtmlInputTextarea getSessionRemarks() {
		return sessionRemarks;
	}
	public void setSessionRemarks(HtmlInputTextarea sessionRemarks) {
		this.sessionRemarks = sessionRemarks;
	}
	public HtmlSelectOneMenu getSessionTimeHourMenu() {
		return sessionTimeHourMenu;
	}
	public void setSessionTimeHourMenu(HtmlSelectOneMenu sessionTimeHourMenu) {
		this.sessionTimeHourMenu = sessionTimeHourMenu;
	}

	public HtmlSelectOneMenu getStartDateHoursMenu() {
		return startDateHoursMenu;
	}
	public void setStartDateHoursMenu(HtmlSelectOneMenu startDateHoursMenu) {
		this.startDateHoursMenu = startDateHoursMenu;
	}
	public HtmlSelectOneMenu getStartDateMinsMenu() {
		return startDateMinsMenu;
	}
	public void setStartDateMinsMenu(HtmlSelectOneMenu startDateMinsMenu) {
		this.startDateMinsMenu = startDateMinsMenu;
	}

	public HtmlInputText getTenderDescription() {
		return tenderDescription;
	}
	public void setTenderDescription(HtmlInputText tenderDescription) {
		this.tenderDescription = tenderDescription;
	}
	public HtmlInputText getTenderDocSellingPrice() {
		return tenderDocSellingPrice;
	}
	public void setTenderDocSellingPrice(HtmlInputText tenderDocSellingPrice) {
		this.tenderDocSellingPrice = tenderDocSellingPrice;
	}
	public HtmlInputText getTenderNumber() {
		return tenderNumber;
	}
	public void setTenderNumber(HtmlInputText tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public HtmlInputText getTenderStatue() {
		return tenderStatue;
	}
	public void setTenderStatue(HtmlInputText tenderStatue) {
		this.tenderStatue = tenderStatue;
	}
	public HtmlSelectOneMenu getTenderType() {
		return tenderType;
	}
	public void setTenderType(HtmlSelectOneMenu tenderType) {
		this.tenderType = tenderType;
	}
	public List<SelectItem> getTenderTypeList() {
		return tenderTypeList;
	}
	public void setTenderTypeList(List<SelectItem> tenderTypeList) {
		this.tenderTypeList = tenderTypeList;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public HtmlInputText getEstimatedCost() {
		return estimatedCost;
	}
	public void setEstimatedCost(HtmlInputText estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getLocale(){

		return new CommonUtil().getLocale();
	}

	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}


	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public String getHhEnd() {
		return hhEnd;
	}
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}
	public String getMmEnd() {
		return mmEnd;
	}
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}
	public String getHhStart() {
		return hhStart;
	}
	public void setHhStart(String hhStart) {
		this.hhStart = hhStart;
	}
	public String getMmStart() {
		return mmStart;
	}
	public void setMmStart(String mmStart) {
		this.mmStart = mmStart;
	}
	public HtmlSelectOneMenu getSessionTimeMinsMenu() {
		return sessionTimeMinsMenu;
	}
	public void setSessionTimeMinsMenu(HtmlSelectOneMenu sessionTimeMinsMenu) {
		this.sessionTimeMinsMenu = sessionTimeMinsMenu;
	}
	public String getHhSessionTime() {
		return hhSessionTime;
	}
	public void setHhSessionTime(String hhSessionTime) {
		this.hhSessionTime = hhSessionTime;
	}
	public String getMmSessionTime() {
		return mmSessionTime;
	}
	public void setMmSessionTime(String mmSessionTime) {
		this.mmSessionTime = mmSessionTime;
	}
	public List<TenderProposalView> getProposalDataList() {
		return proposalDataList;
	}
	public void setProposalDataList(List<TenderProposalView> proposalDataList) {
		this.proposalDataList = proposalDataList;
	}


	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Integer getRecordSize() {
		return proposalDataList.size();
	}
	public ConstructionServiceAgent getConstructionServiceAgent() {
		return constructionServiceAgent;
	}
	public void setConstructionServiceAgent(
			ConstructionServiceAgent constructionServiceAgent) {
		this.constructionServiceAgent = constructionServiceAgent;
	}
	public String editLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showEditTenderProposalPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	public String viewLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showViewTenderProposalPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}


	public Integer getReceivingTeamRecordSize(){

		return receivingDataList.size();
	}
	public void showReceivingSearchPopup(){

		final String viewId = "/OpenTenderProposals.jsf";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:SearchMemberPopup();";

		// Add the Javascript to the rendered page's header for immediate execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}
	private void loadTenderCommitteeTeamDataList()
	{
		String sMethod="loadReceivingTeamDataList";
		Boolean isAdd = true;

			if(sessionMap.get(WebConstants.TEAM_DATA_LIST)!=null)
			{
				// check duplication
			   if(getReceivingDataList()!=null && getReceivingDataList().size()>0)
			   {
				   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
				   tempReceivingTeam = (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST);
				   sessionMap.remove(WebConstants.TEAM_DATA_LIST);
				   for(UserView uV:tempReceivingTeam)
				   {
					   for(UserView uV1: getReceivingDataList())
					   {
						   isAdd = true;
						   if(uV.getUserName()!= null && uV.getUserName().equals(uV1.getUserName()))
						   {
							 isAdd = false;
							 break;
						   }
					   }

					  if(isAdd)
					  {
						  uV.setTeamMemberIsDeleted(0L);
						  getReceivingDataList().add(uV);
					  }
				   }
				   viewMap.put("RECEIVING_TEAM_DATA_LIST",getReceivingDataList());

			   } // for the first time when no member present
			   else{
				    List<UserView> tempReceivingTeam = new ArrayList<UserView>();
					for(UserView uV: (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST))
					{
						 uV.setTeamMemberIsDeleted(0L);
						  tempReceivingTeam.add(uV);
					}
				    sessionMap.remove(WebConstants.TEAM_DATA_LIST);
				    setReceivingDataList(tempReceivingTeam);
			    }
			}
  	   }
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	private TeamView getReceivingTeamView(List<UserView> listUserView)throws PimsBusinessException
	{
		String methodName = "getTeamView";
		logger.logInfo(methodName+"|"+"Start..");
		TeamView teamView =new TeamView();
		Set<TeamMemberView> teamMemberSet=new HashSet<TeamMemberView>(0);
		TeamMemberView teamMemberView=new TeamMemberView();

		logger.logInfo(methodName+"|"+"teamId.."+teamId);
		if(teamId!=null && teamId.length()>0)
		{
			teamView.setTeamId(new Long(teamId));

		}

		teamView.setTeamName("TENDER PROPOSAL TEAM");
		teamView.setCreatedOn(new Date());
		teamView.setCreatedBy(getLoggedInUser());
		teamView.setUpdatedOn(new Date());
		teamView.setUpdatedBy(getLoggedInUser());
		teamView.setIsDeleted(new Long(0));
		teamView.setRecordStatus(new Long(1));


		//adding members
		for (UserView selectItem : listUserView)
		{
			teamMemberView=new TeamMemberView();
			logger.logInfo(methodName+"|"+"loginId.."+selectItem.getUserName().toString());
			teamMemberView.setLoginId(selectItem.getUserName().toString());
			teamMemberView.setIsDeleted(new Long(0));
			teamMemberView.setRecordStatus(new Long(1));
			teamMemberView.setUpdatedOn(new Date());
			teamMemberView.setCreatedOn(new Date());
			teamMemberView.setCreatedBy(getLoggedInUser());
			teamMemberView.setUpdatedBy(getLoggedInUser());
			teamMemberSet.add(teamMemberView);

		}
		teamView.setTeamMembersView(teamMemberSet);
		logger.logInfo(methodName+"|"+"Finish..");
		return teamView;
	}

	public String saveReceivingTeam(List<UserView> listUserView){
		if(viewMap.get(WebConstants.Tender.TENDER_VIEW)!=null){
			tenderView = (TenderView)viewMap.get(WebConstants.Tender.TENDER_VIEW);
			try {
				successMessages = new ArrayList<String>();
				errorMessages=new ArrayList<String>();
				TeamView teamView = new TeamView();

				teamView=getReceivingTeamView(listUserView);

				teamView = propertyServiceAgent.addTeam(teamView);
				tenderView.setTeamViewByCommitteeTeamId(teamView);
				constructionServiceAgent.updateTenderForCommittee(tenderView);



				//successMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_RECEIVING_TEAM_SAVED_SUCCESSFULLY));
			} catch (PimsBusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return "success";

	}
	/*public String tabCommitteeTeam_Click()
	{

		String methodName="tabReceivingTeam_Click";
		logger.logInfo("|"+"Start..");
		String eventOutcome="";

		loadTenderCommitteeTeamDataList(false);

		logger.logInfo("|"+"Finish..");
		return eventOutcome;

	}*/
	public String cmdReceivingTeamMemberDelete_Click(){
		UserView receivingTeamView=(UserView)receivingTeamGrid.getRowData();
		List<UserView> tempReceivingDataList  = new ArrayList<UserView>();
		try
		{
			if(!(receivingTeamView.getTeamMemberId()!=null))
			{
				getReceivingDataList().remove(receivingTeamView);
				return "success";
			}else{

				for(UserView receivingTeamDataListView : getReceivingDataList()){
					if((receivingTeamDataListView.getUserName()!= null && receivingTeamDataListView.getUserName().equals(receivingTeamView.getUserName())) ||
							receivingTeamDataListView.getUserName()== null &&
							receivingTeamDataListView.getFullNamePrimary() != null &&
							receivingTeamDataListView.getFullNamePrimary().equals(receivingTeamView.getFullNamePrimary()))
					{
						receivingTeamDataListView.setTeamMemberIsDeleted(1L);
						tempReceivingDataList.add(receivingTeamDataListView);
					}else
					{
						tempReceivingDataList.add(receivingTeamDataListView);
					}
				}
			  }
			setReceivingDataList(tempReceivingDataList);
		}catch (Exception e) {
			System.out.println("Exception "+e);
		}
		return "delete";
	}
	public List<UserView> getReceivingDataList() {
		if(viewMap.containsKey("RECEIVING_TEAM_DATA_LIST"))
			receivingDataList = (ArrayList<UserView>)viewMap.get("RECEIVING_TEAM_DATA_LIST");
		return receivingDataList;
	}
	public void setReceivingDataList(List<UserView> receivingDataList) {
        this.receivingDataList = receivingDataList;

		if(this.receivingDataList!=null && this.receivingDataList.size()>0)
			viewMap.put("RECEIVING_TEAM_DATA_LIST",this.receivingDataList);
	}
	public HtmlCommandButton getAddReceiveingTeam() {
		return addReceiveingTeam;
	}
	public void setAddReceiveingTeam(HtmlCommandButton addReceiveingTeam) {
		this.addReceiveingTeam = addReceiveingTeam;
	}
	public HtmlDataTable getReceivingTeamGrid() {
		return receivingTeamGrid;
	}
	public void setReceivingTeamGrid(HtmlDataTable receivingTeamGrid) {
		this.receivingTeamGrid = receivingTeamGrid;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}


	public String addLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showAddProposalRecommendationPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public TenderView getTenderView() {
		return tenderView;
	}

	public void setTenderView(TenderView tenderView) {
		this.tenderView = tenderView;
	}

	public TenderDetailView getTenderDetailView() {
		return tenderDetailView;
	}

	public void setTenderDetailView(TenderDetailView tenderDetailView) {
		this.tenderDetailView = tenderDetailView;
	}

	public Long getTenderId() {
		return tenderId;
	}

	public void setTenderId(Long tenderId) {
		this.tenderId = tenderId;
	}

	public TenderSessionView getTenderSessionView() {
		return tenderSessionView;
	}

	public void setTenderSessionView(TenderSessionView tenderSessionView) {
		this.tenderSessionView = tenderSessionView;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }

	public String cancelLink_action()
	{
		changeProposalStatus( Constant.TENDER_PROPOSAL_STATUS_CANCEL );
		updateTenderProposal();
		successMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_CANCEL_SUCCESS ) );
		return null;
	}

	public String deleteLink_action()
	{
		changeProposalIsDeleted("1");
		updateTenderProposal();
		successMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_DELETE_SUCCESS ) );
		return null;
	}

	public String rejectLink_action()
	{
		changeProposalStatus( Constant.TENDER_PROPOSAL_STATUS_REJECT );
		updateTenderProposal();
		successMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_REJECT_SUCCESS ) );
		return null;
	}

	private void changeProposalIsDeleted(String isDeleted)
	{
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		selectedTenderProposalView.setIsDeleted(isDeleted);
	}

	private void changeProposalStatus(String status)
	{
		logger.logInfo("changeProposalStatus --- started");
		try
		{
			DomainData domainData = utilityManager.getDomainDataByValue( status );
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
			selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
			selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
			logger.logInfo("changeProposalStatus --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("changeProposalStatus --- crashed --- ", e);
		}
	}

	private void updateTenderProposal()
	{
		try
		{
			logger.logInfo("updateTenderProposal() --- started");
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
			constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
			proposalDataList = constructionServiceAgent.getTenderProposalList(tenderId);
			updateListsToViewRoot();
			logger.logInfo("updateTenderProposal() --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposal() --- exception --- ", e);
		}
	}

	public UtilityManager getUtilityManager() {
		return utilityManager;
	}

	public void setUtilityManager(UtilityManager utilityManager) {
		this.utilityManager = utilityManager;
	}

	public String saveAction()
	{
		try
		{


			logger.logInfo("saveAction() --- started");
			Boolean isUpdate = false;

			if ( tenderSessionView.getTenderSessionId() != null )
			{
				tenderSessionView.setTenderSessionUpdatedOn( new Date() );
				tenderSessionView.setTenderSessionUpdatedBy( CommonUtil.getLoggedInUser() );
				constructionServiceAgent.updateTenderSession( tenderSessionView );
				isUpdate = true;
			}
			else
			{
				tenderSessionView.setTenderId( tenderId );
				tenderSessionView.setTenderSessionCreatedOn( new Date() );
				tenderSessionView.setTenderSessionCreatedBy( CommonUtil.getLoggedInUser() );
				tenderSessionView.setTenderSessionUpdatedOn( new Date() );
				tenderSessionView.setTenderSessionUpdatedBy( CommonUtil.getLoggedInUser() );
				tenderSessionView.setTenderSessionIsDeleted( Constant.DEFAULT_IS_DELETED );
				tenderSessionView.setTenderSessionRecordStatus( Constant.DEFAULT_RECORD_STATUS );
				constructionServiceAgent.addTenderSession( tenderSessionView );
			}
			if(viewMap.get(WebConstants.Tender.TENDER_VIEW)!=null)
			{
				tenderView = (TenderView)viewMap.get(WebConstants.Tender.TENDER_VIEW);

				if(tenderView.getTeamViewByCommitteeTeamId()!=null && tenderView.getTeamViewByCommitteeTeamId().getTeamId() !=null)
				{
				 List<TeamMemberView> teamMemberViewList = 	getTeamMemberList( getReceivingDataList() );
				 TeamView teamView = new TeamView();
				 teamView.setTeamId(tenderView.getTeamViewByCommitteeTeamId().getTeamId());
				 propertyServiceAgent.addUpdateTeamMembers(teamMemberViewList, teamView);

				 isUpdate = true;
				}
				else{
				   saveReceivingTeam( getReceivingDataList() );
				}

				viewMap.remove("RECEIVING_TEAM_DATA_LIST");
				tenderView =  constructionServiceAgent.getTenderByIdForTender(tenderView.getTenderId());
				viewMap.put(WebConstants.Tender.TENDER_VIEW,tenderView);
			}


			CommonUtil.loadAttachmentsAndComments(tenderId.toString());
			Boolean attachSuccess = CommonUtil.saveAttachments(tenderId);
			Boolean commentSuccess = CommonUtil.saveComments(tenderId, noteOwner);

			tenderSessionView = constructionServiceAgent.getTenderSessionViewById(tenderId);
			updateListsToViewRoot();

			if(isUpdate)
			successMessages.add( getBundleMessage( "openTenderProposals.updateDone" ) );
			else
			successMessages.add( getBundleMessage( "openTenderProposals.saveDone" ) );

			logger.logInfo("saveAction() --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("saveAction() --- exception occured --- ", e);
		}

		return null;
	}

	public List<TeamMemberView> getTeamMemberList (List<UserView> listUserView)
	{
		List<TeamMemberView> teamMemberViewList = new ArrayList<TeamMemberView>();
		TeamMemberView teamMemberView=new TeamMemberView();
		for (UserView selectItem : listUserView)
		{
			teamMemberView=new TeamMemberView();
			logger.logInfo("|"+"loginId.."+selectItem.getUserName());
			// in case of update
			if(selectItem.getTeamMemberId()!=null)
			{
				teamMemberView.setTeamMemberId(selectItem.getTeamMemberId());
				if(selectItem.getUserName() != null){
					teamMemberView.setLoginId(selectItem.getUserName().toString());
				}
				else {
					teamMemberView.setLoginId(null);
					teamMemberView.setExternalUserName(selectItem.getFullNamePrimary());
				}
				teamMemberView.setIsDeleted(selectItem.getTeamMemberIsDeleted());
				teamMemberView.setRecordStatus(selectItem.getTeamMemberRecordStatus());
				teamMemberView.setUpdatedOn(new Date());
				teamMemberView.setCreatedOn(selectItem.getTeamMemberCreatedOn());
				teamMemberView.setCreatedBy(selectItem.getTeamMemberCreatedBy());
				teamMemberView.setUpdatedBy(getLoggedInUser());
				teamMemberViewList.add(teamMemberView);
			}else{// in case of Add

				if(selectItem.getUserName() != null){
					teamMemberView.setLoginId(selectItem.getUserName().toString());
				}
				else {
					teamMemberView.setLoginId(null);
					teamMemberView.setExternalUserName(selectItem.getFullNamePrimary());
				}
				teamMemberView.setIsDeleted(new Long(0));
				teamMemberView.setRecordStatus(new Long(1));
				teamMemberView.setUpdatedOn(new Date());
				teamMemberView.setCreatedOn(new Date());
				teamMemberView.setCreatedBy(getLoggedInUser());
				teamMemberView.setUpdatedBy(getLoggedInUser());
				teamMemberViewList.add(teamMemberView);
			}

		}

		return teamMemberViewList;
	}

	public String cancelAction()
	{
		String METHOD_NAME = "onCancel()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
			path = "constructionTenderSearch";
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
		return path;
	}

	public HtmlSelectOneMenu getDdnConstructionServiceType() {
		return ddnConstructionServiceType;
	}

	public void setDdnConstructionServiceType(
			HtmlSelectOneMenu ddnConstructionServiceType) {
		this.ddnConstructionServiceType = ddnConstructionServiceType;
	}

	public String getNotesOwner() {
		return notesOwner;
	}

	public HtmlInputText getTxtProjectNumber() {
		return txtProjectNumber;
	}

	public void setTxtProjectNumber(HtmlInputText txtProjectNumber) {
		this.txtProjectNumber = txtProjectNumber;
	}

	public HtmlInputText getTxtProjectName() {
		return txtProjectName;
	}

	public void setTxtProjectName(HtmlInputText txtProjectName) {
		this.txtProjectName = txtProjectName;
	}

	public HtmlInputText getTxtProjectEstimatedCost() {
		return txtProjectEstimatedCost;
	}

	public void setTxtProjectEstimatedCost(HtmlInputText txtProjectEstimatedCost) {
		this.txtProjectEstimatedCost = txtProjectEstimatedCost;
	}

	@SuppressWarnings("unchecked")
	public void openUploadPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openUploadPopup() started...");
		try
		{

			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

			String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
	    	String externalId = WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_PROPOSAL;
			String noteOwner = WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_PROPOSAL;
	    	String associatedObjectId = selectedTenderProposalView.getTenderProposalId();

			sessionMap.put("repositoryId", repositoryId);
			sessionMap.put("externalId", externalId);
			sessionMap.put("associatedObjectId", associatedObjectId);
			sessionMap.put("noteowner", noteOwner);

	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showUploadPopup();";

	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openUploadPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openUploadPopup() crashed ", exception);
		}
    }

	public void removeAttachment(ActionEvent evt){
		logger.logInfo("removeAttachment() started...");
		try {
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			Boolean success = utilityServiceAgent.deleteDocument(Long.valueOf(selectedTenderProposalView.getDocumentID()));
			if(success){
				sessionMap.put("reloadOpener", true);
				proposalDataList = constructionServiceAgent.getTenderProposalList(tenderId);
				String infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_DOC_DELETE_SUCCESS);
				successMessages.add(infoMessage);
			}
			else
				throw new Exception();
			logger.logInfo("removeAttachment() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("removeAttachment() crashed ", exception);
			String errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_DOC_DELETE_FAILURE);
			errorMessages.add(errorMessage);
		}
	}

	public String download()
	{
		try {
				TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
				String documentId = selectedTenderProposalView.getDocumentID();
				sessionMap.put("documentId", documentId);

	     } catch (Exception e) {
	    	 e.printStackTrace();
		}
		return "DownloadFile";
	}

	public Boolean getCanAttachFile() {
		return canAttachFile;
	}

	public void setCanAttachFile(Boolean canAttachFile) {
		this.canAttachFile = canAttachFile;
	}
	private void decideCanAttach(){
    	UtilityManager utilityManager2 = new UtilityManager();
		HashMap map = null;
		try {

			map = utilityManager2.getDomainDataMap();
	    	DomainData domainData2 = (DomainData) map.get( Long.valueOf(this.getTenderDetailView().getTenderStatusId()) );
	    	if(domainData2!=null){
	    		if(domainData2.getDataValue().equals(Constant.TENDER_STATUS_APPROVED))
	    			this.setCanAttachFile(true);
	    		else
	    			this.setCanAttachFile(false);
	    	}

		} catch (PimsBusinessException e) {
			this.setCanAttachFile(false);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addExternalTeamMember()
	{
		logger.logInfo("addExternalTeamMember() started...");
		try {
			if(this.getExternalUserName() != null && !this.getExternalUserName().equals("")) {
				List<UserView> userViewList=new ArrayList<UserView>();
				UserView newUserView = new UserView();
				newUserView.setUserName(null);
				newUserView.setFullNamePrimary(this.getExternalUserName());
				userViewList.add(newUserView);
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TEAM_DATA_LIST, userViewList);
				this.loadTenderCommitteeTeamDataList();
				logger.logInfo("addExternalTeamMember() completed successfully!!!");
			}
			else {
				String errorMessage = CommonUtil.getBundleMessage("teamMember.add.NoNameSpecified");
				errorMessages.add(errorMessage);
			}
		}
		catch (Exception exception) {
			logger.LogException("addExternalTeamMember() crashed ", exception);
			String errorMessage = CommonUtil.getBundleMessage("teamMember.add.error");
			errorMessages.add(errorMessage);
		}
	}

	public String getExternalUserName() {
		return externalUserName;
	}

	public void setExternalUserName(String externalUserName) {
		this.externalUserName = externalUserName;
	}
}
