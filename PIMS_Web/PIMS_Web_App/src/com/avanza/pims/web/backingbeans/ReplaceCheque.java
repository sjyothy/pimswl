package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.bpel.pimsbpelreplacecheque.proxy.PIMSReplaceChequeBPELPortClient;
import com.avanza.pims.bpel.proxy.pimsbouncecheque.PIMSBounceChequeBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ChequeWithdrawlReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.finance.FinanceService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.RequiredDocumentsView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.ui.util.ResourceUtil;




public class ReplaceCheque extends AbstractController {
      
      /**
      * 
       */
      private static final long serialVersionUID = 2432979760361254947L;
      //Replace Cheque Base Data
      //                      Text Fields
      private HtmlInputText contractNumberText = new HtmlInputText();
      private HtmlInputText statusText = new HtmlInputText();
      private HtmlInputText contractStartDateText = new HtmlInputText();
	private HtmlInputText contractExpiryDateText = new HtmlInputText();
	private HtmlInputText tenantReferenceNoText = new HtmlInputText();
	private HtmlInputText tenantNameText = new HtmlInputText();
	private HtmlInputText contractTypeText = new HtmlInputText();
	private HtmlInputText totalContractValue = new HtmlInputText();
	private HtmlInputText unitRefNoText = new HtmlInputText();
	private HtmlInputText unitRentValueText = new HtmlInputText();
	private HtmlInputText propertyNameText = new HtmlInputText();
	private HtmlInputText unitTypeText = new HtmlInputText();
	private Integer noOfBounceChqToReplace = 0;
	private Integer noOfCollectedChqToReplace = 0;
	private Double toatlFeesForBounceChqToReplace;
	private Double toatlFeesForCollectedChqToReplace;
	private boolean pageModePopup;
	private boolean showProcedureType;
	private ChequeWithdrawlReportCriteria withDrawlCriteria ;
	private boolean isShowWithdrawPrintButton;
	
	//				Select One Menu
	private HtmlSelectOneMenu bankStatusMenu = new HtmlSelectOneMenu();
	private List<SelectItem> bankStatuses = new ArrayList<SelectItem>();
	
	//				Command Buttons
	private HtmlCommandButton closeButton = new HtmlCommandButton();
	private HtmlCommandButton paymentButton = new HtmlCommandButton();
	private HtmlCommandButton completeButton = new HtmlCommandButton();
	private HtmlCommandButton submitButton = new HtmlCommandButton();
	private HtmlCommandButton finalSubmitButton = new HtmlCommandButton();
	private HtmlCommandButton tenantButton = new HtmlCommandButton();
	private HtmlCommandButton contractButton = new HtmlCommandButton();
	private HtmlCommandButton attachmentButton = new HtmlCommandButton();
	private HtmlCommandButton commentButton = new HtmlCommandButton();
	private HtmlCommandButton reviewBtn = new HtmlCommandButton();
	
	// property form shiraz 
	private HtmlCommandButton replacePaymentButton = new HtmlCommandButton();
	private Boolean selectChequeDetail;
	private Boolean paymentNumderCol;
	
	private HtmlCommandButton sendBtn = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton approvedBtn = new HtmlCommandButton();
	private HtmlCommandButton rejectBtn = new HtmlCommandButton();
	private HtmlCommandButton completeBtn = new HtmlCommandButton();
	private HtmlCommandButton cancelBtn = new HtmlCommandButton();
	private HtmlCommandButton replacedBtn = new HtmlCommandButton();
	private HtmlCommandButton returedChequeBtn = new HtmlCommandButton();
	private HtmlCommandButton returedMarkBtn = new HtmlCommandButton();
	private HtmlCommandButton withdrawlReportBtn = new HtmlCommandButton();
	
	
	
	
	//				Output labels
	private HtmlOutputText bankStatusLabel = new HtmlOutputText();
	
	
	// Old Cheque Details Tab
	//				Text Fields
	private HtmlInputText oldChequeNumberText = new HtmlInputText();
	private HtmlInputText oldChequeBankNameText = new HtmlInputText();
	private HtmlInputText oldChequeAmountText = new HtmlInputText();
	private HtmlInputText oldChequeDate = new HtmlInputText();
	//				Command Buttons
	private HtmlCommandButton replaceButton = new HtmlCommandButton();
	
	
	//Require Docs Tab
	private HtmlDataTable requiredDocumentsDataTable = new HtmlDataTable();
	private List<RequiredDocumentsView> listRequiredDocuments = new ArrayList<RequiredDocumentsView>();
	RequiredDocumentsView requiredDocumentItem = new RequiredDocumentsView();
	
	
	//Fee Tab
	private HtmlDataTable feeDataTable = new HtmlDataTable();
	PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
	
	
//	Commons
	
	SystemParameters parameters = SystemParameters.getInstance();
	DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	ServletContext servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();

	private HtmlTabPanel replaceChequeTabPanel = new HtmlTabPanel();
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private String requestId;
	Long paymentScheduleId;
	private static Logger logger = Logger.getLogger(ReplaceCheque.class);
	ContractView contractView = null;
	RequestView requestView = new RequestView();
	TenantView tenantView = null;
	private List<String> errorMessages=new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	CommonUtil commonUtil=new CommonUtil();
	
	UtilityServiceAgent utilityServiceAgent=new UtilityServiceAgent();
	PropertyServiceAgent propertyServiceAgent =new PropertyServiceAgent();
	BounceChequeView bounceChequeView = new BounceChequeView();
	ApplicationBean applicationBean = new ApplicationBean();
	ResourceBundle bundle = 
		ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), 
				getFacesContext().getViewRoot().getLocale());
	
	private HtmlInputText contractNoText = new HtmlInputText();
	private HtmlInputText contractEndDateText = new HtmlInputText();
	private HtmlInputText tenantNumberType = new HtmlInputText();
	private HtmlInputText contractStatusText = new HtmlInputText();
	private HtmlInputText totalContractValText = new HtmlInputText();
	private HtmlInputText txtUnitType = new HtmlInputText();
	private HtmlInputText txtpropertyName = new HtmlInputText();
	private HtmlInputText txtunitRefNum = new HtmlInputText();
	private HtmlInputText txtUnitRentValue = new HtmlInputText();
	private HtmlInputText txtpropertyType = new HtmlInputText();
	private HtmlCommandLink populateContract = new HtmlCommandLink();
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private Boolean editPaymentEnable;
	CommonUtil commUtil  = new CommonUtil();
	
	private List<PaymentScheduleView> paymentSchedules = new ArrayList<PaymentScheduleView>();
	private List<BounceChequeView> bounceChequeList  = new ArrayList<BounceChequeView>();
	private HtmlDataTable dataTablePaymentSchedule;
	private HtmlDataTable dataTableChequeDetail;
	public boolean statusIsNotSentForApprovalAndNotApproved;
	private String dateFormatForDataTable;
	
	@SuppressWarnings( "unchecked" )
	Map sessionMap;
	@SuppressWarnings( "unchecked" )
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	@SuppressWarnings( "unchecked" )
	Map requestMap = getFacesContext().getExternalContext().getRequestMap();
	FacesContext context = FacesContext.getCurrentInstance();
	List<RequestKeyView> requestKeys;
	@SuppressWarnings( "unchecked" )
	HashMap searchMap = new HashMap();
	PropertyServiceAgent psa = new PropertyServiceAgent();
	private HtmlTabPanel richTabPanel = new HtmlTabPanel();
	private String APPLICATION_TAB =  "applicationTab";
	private String CONTRACT_DETAIL_TAB =  "contractDetailTab";
	private String CHEQUE_DETAIL_TAB = "chequeDetailTab";
	private String PAYMENT_TAB =  "PaymentTab";
	private String BOUNCED_CHEQUES_PRD_IDS =  "BOUNCED_CHEQUES_PRD_IDS";
	
	private String selectedProcedureType;
	DomainDataView ddvRequestStatusChequeWithdrawn;
	DomainDataView ddvRequestStatusRecievedFromBank;
	private boolean isWithdrawn;
	private boolean isReceivedFromBank;
	private boolean isAcceptedCollected;
	private boolean isAcceptedSettled;
	private boolean isShowReturnOldChequeButton;
	private boolean isShowReturnCheque;
	
	//Overridden Methods
	@SuppressWarnings("unchecked")
	public void init()
	{
		
		super.init();
		try
		{
		sessionMap = context.getExternalContext().getSessionMap();
		sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
		if(getLoggedInDBImplUser().getSecondaryFullName()!= null && getLoggedInDBImplUser().getSecondaryFullName().length()>0)
			withDrawlCriteria = new ChequeWithdrawlReportCriteria(ReportConstant.Report.CHEQUE_WITHDRAWL_REPORT, ReportConstant.Processor.CHEQUE_WITHDRAWL_REPORT, getLoggedInDBImplUser().getSecondaryFullName());
		else
			withDrawlCriteria = new ChequeWithdrawlReportCriteria(ReportConstant.Report.CHEQUE_WITHDRAWL_REPORT, ReportConstant.Processor.CHEQUE_WITHDRAWL_REPORT, getLoggedInDBImplUser().getFullName());
		
		
		fromEditAddPaymentSchedule();   
		if(!isPostBack())
		{
				initializeContext();
				loadAttachmentsAndComments(null);
		}
		if(sessionMap.containsKey("PAYMENT_SCHDULE_SPLIT_DONE"))
		{
				
				Long reqId = (Long) sessionMap.remove("PAYMENT_SCHDULE_SPLIT_DONE");
				paymentSchedules = psa.getContractPaymentSchedule( null, reqId );
				this.setPaymentSchedules(paymentSchedules);						
		
			
		}
		}
		catch(Exception exp)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException( "init|Error Occured", exp);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));	
		}
	}
	@SuppressWarnings("unchecked")
	private void initializeContext() throws Exception
	{
			loadAttachmentsAndComments(null);
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			viewMap.put("selectedTab",APPLICATION_TAB);
		    requestKeys = new RequestServiceAgent().getRequestKeys( WebConstants.REPLACE_CHEQUE );
			//when comming from payment search with cheque to be replace  
			if( requestMap.get(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW)!=null)
				fromPaymentSearch();
			
			// when comming from request search 
			else if( requestMap.containsKey(WebConstants.REQUEST_VIEW))
				fromRequestSearch();
			
			//when we are comming from request search with no task pending 
			else if( requestMap.get( WebConstants.REQUEST_VIEW ) != null )
				fromRequestSearchWithNoPendingTasks();
			
			 //when comming from request search with task or from task list 
			else if(sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
				fromTaskList();
			else if(sessionMap.get(WebConstants.REQUEST_VIEW)!=null)
				fromFollowup();
			
			DomainDataView ddvRequestStatusAcceptedCollected = CommonUtil.getIdFromType( 
                    CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
                    WebConstants.REQUEST_STATUS_ACCEPTED_COLLECTED
                   );
			
			DomainDataView ddvRequestStatusAcceptedSettled = CommonUtil.getIdFromType( 
                    CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
                    WebConstants.REQUEST_STATUS_ACCEPTED_SETTLED
                   );
			
			ddvRequestStatusChequeWithdrawn = CommonUtil.getIdFromType( 
                    CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
                    WebConstants.REQUEST_STATUS_CHEQUE_WITHDRAWN
                   );
			
			ddvRequestStatusRecievedFromBank = CommonUtil.getIdFromType( 
                    CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
                    WebConstants.REQUEST_STATUS_CHEQUE_RECEIVED_FROM_BANK
                   );
			
			if(ddvRequestStatusChequeWithdrawn != null){
				viewMap.put(WebConstants.REQUEST_STATUS_CHEQUE_WITHDRAWN, ddvRequestStatusChequeWithdrawn);
			}
			
			if(ddvRequestStatusRecievedFromBank != null){
				viewMap.put(WebConstants.REQUEST_STATUS_CHEQUE_RECEIVED_FROM_BANK, ddvRequestStatusRecievedFromBank);
			}
			
			if(ddvRequestStatusAcceptedCollected != null){
				viewMap.put(WebConstants.REQUEST_STATUS_ACCEPTED_COLLECTED, ddvRequestStatusAcceptedCollected);
			}
			
			if(ddvRequestStatusAcceptedSettled != null){
				viewMap.put(WebConstants.REQUEST_STATUS_ACCEPTED_SETTLED, ddvRequestStatusAcceptedSettled);
			}

	}
	@SuppressWarnings("unchecked")
	private void fromRequestSearchWithNoPendingTasks() throws PimsBusinessException, Exception 
	{
		RequestFieldDetailView requestFieldDetailViewNew;
		RequestFieldDetailView requestFieldDetailViewDetailId;
		setEditPaymentEnable(false);
		requestKeys = new RequestServiceAgent().getRequestKeys(WebConstants.REPLACE_CHEQUE);
		RequestView requestView = (RequestView)requestMap.get(WebConstants.REQUEST_VIEW);
		
		this.requestId = requestView.getRequestId().toString();
		modeDisplay();
		
		requestFieldDetailViewDetailId= new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.ReplaceCheque.PAYMENT_RECEIPT_DETAIL_ID));
		
		
		viewMap.put("PAYMENT_RECEIPT_DETAIL_STATUS_IDS",requestFieldDetailViewDetailId.getRequestKeyValue());
		refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
		
		requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.OLD_PAYMENT_SCHEDULE_ID));	
		
		String contractNumber = requestFieldDetailViewNew.getRequestKeyValue().toString();
		
		
		
		//searchMap.put("paymentScheduleId",paymentScheudleId);
		loadAttachmentsAndComments(Long.parseLong(requestId));
		populateApplicationDataTab(requestId.toString());
		//bounceChequeList = propertyServiceAgent.getAllCheck(searchMap);
		
		//bounceChequeView = bounceChequeList.get(0);
		fillValues(contractNumber);
		

		
		paymentSchedules = psa.getContractPaymentSchedule(null,Long.parseLong(requestId));
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		getPaymentSchedules();
	}
	@SuppressWarnings("unchecked")
	private void fromEditAddPaymentSchedule() 
	{
		if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
     		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
        )
        {
     	   viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
     	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
     	   
     	  List<PaymentScheduleView> paymentSecList = (List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
   	      if(paymentSecList.get(0).getPaymentScheduleId()!=null)
   	    	viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSecList);
   	      else
   	    	  viewMap.put( "DEFAULT_PAYMENT_EDITED",paymentSecList  );
        }
	}
	@SuppressWarnings("unchecked")
	private void fromTaskList() throws PimsBusinessException, Exception 
	{
		if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
		{

		    UserTask task = ( UserTask ) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	        viewMap.put( WebConstants.TASK_LIST_SELECTED_USER_TASK,sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) );
	        sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			Map taskAttributes =  task.getTaskAttributes();
			Long requestId = Convert.toLong( taskAttributes.get( WebConstants.UserTasks.REQUEST_ID ) );
			requestView = new RequestService().getRequestById(requestId);
			viewMap.put("REQUEST_ID_FOR_WITHDRAWL",requestView.getRequestId());
			viewMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
//			requestView.setRequestId( requestId );
			this.requestId = requestId.toString();
			loadAttachmentsAndComments( requestId );
			populateApplicationDataTab( requestId.toString() );
			setEditPaymentEnable(false);
			viewMap.put( "canAddAttachment", false);
			viewMap.put( "canAddNote", false );
			//for Approval
			
			if(task.getTaskType().equals( WebConstants.ReplaceCheque.EXCHANGE_REPLACE_CHEQUE ))
				viewMap.put("SHOW_WITHDRAWL_LETTER", true);
			if(task.getTaskType().equals( WebConstants.ReplaceCheque.APPROVED_REPLACE_CHEQUE )||
			   task.getTaskType().equals( WebConstants.ReplaceCheque.COLLECT_REPLACE_CHEQUE )||
			   task.getTaskType().equals( WebConstants.ReplaceCheque.EXCHANGE_REPLACE_CHEQUE )||
			   task.getTaskType().equals( WebConstants.ReplaceCheque.COMPLETE_REPLACE_CHEQUE ))
				
					viewMap.put("PROCEDURE_TYPE", "COLLECTED");	
				
			else if(task.getTaskType().equals( WebConstants.ReplaceCheque.APPROVED_BOUNCED_CHEQUE )||
					   task.getTaskType().equals( WebConstants.ReplaceCheque.COLLECT_BOUNCED_REPLACE_CHEQUE )||
					   task.getTaskType().equals( WebConstants.ReplaceCheque.LEGAL_DEPARTMENT_REVIEW )||
					   task.getTaskType().equals( WebConstants.ReplaceCheque.RETURNED_BOUNCED_CHEQUE_AND_COMPLETE_REQUEST ))
					
					viewMap.put("PROCEDURE_TYPE", "BOUNCED");
			else
				viewMap.put("HIDE_PROCEDURE", true);
			
			if( task.getTaskType().equals( WebConstants.ReplaceCheque.APPROVED_REPLACE_CHEQUE ) || task.getTaskType().equals( WebConstants.ReplaceCheque.APPROVED_BOUNCED_CHEQUE ))
					fromApproveReplaceChequeTask( requestId );
			
			//for collect replace cheque and payment
			else if( task.getTaskType().equals( WebConstants.ReplaceCheque.COLLECT_REPLACE_CHEQUE ) || task.getTaskType().equals( WebConstants.ReplaceCheque.COLLECT_BOUNCED_REPLACE_CHEQUE ))
				fromCollectReplaceCheque( requestId );

			//for return old cheque 
			else if( task.getTaskType().equals( WebConstants.ReplaceCheque.EXCHANGE_REPLACE_CHEQUE ) ||  task.getTaskType().equals( WebConstants.ReplaceCheque.RETURNED_BOUNCED_CHEQUE_AND_COMPLETE_REQUEST ))
				fromExchangeReplaceCheque( requestId );
			
			//for Complete replace cheque request 
			else if( task.getTaskType().equals( WebConstants.ReplaceCheque.COMPLETE_REPLACE_CHEQUE )  )
				fromCompleteReplaceCheque(  requestId );
			else if( task.getTaskType().equals( WebConstants.ReplaceCheque.LEGAL_DEPARTMENT_REVIEW )  )
			{
				fromCompleteReplaceCheque(  requestId );
				reviewBtn.setRendered(true);
				completeBtn.setRendered(false);
			}
				
		}
	}
	@SuppressWarnings( "unchecked" )
	private void fromCompleteReplaceCheque( Long requestId )throws PimsBusinessException, Exception 
	{
		viewMap.put("selectedTab",CONTRACT_DETAIL_TAB);
		modeComplete();
		fetchFromRequestFieldDetailForTasks();			
	}
	@SuppressWarnings( "unchecked" )
	private void fromExchangeReplaceCheque( Long requestId ) throws PimsBusinessException, Exception 
	{
		viewMap.put( "selectedTab", CHEQUE_DETAIL_TAB );
		DomainDataView ddv = CommonUtil.getIdFromType(
				                                       this.getPaymentScheduleStatus(),
				                                       WebConstants.PAYMENT_SCHEDULE_STATUS_RETURNED 
				                                      );
		modeChequeReturn();
		fetchFromRequestFieldDetailForTasks();
		if( ddv.getDomainDataId().equals( bounceChequeView.getStatusId() ) )
		{
			returedChequeBtn.setRendered(true);
			withdrawlReportBtn.setRendered(true);
			returedMarkBtn.setRendered(false);
			viewMap.put(WebConstants.RETURN_CHEQUE_DONE,true);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void fromCollectReplaceCheque( Long requestId )throws PimsBusinessException, Exception 
	{
		setEditPaymentEnable( true );
		viewMap.put("selectedTab",PAYMENT_TAB);
		DomainDataView ddv= CommonUtil.getIdFromType(
				                                      this.getPaymentScheduleStatus(),
			                                          WebConstants.PAYMENT_SCHEDULE_PENDING
			                                         );
		Boolean flage = true;
		modeCollectReplaceCheque();
		fetchFromRequestFieldDetailForTasks();
		for(PaymentScheduleView pSV:paymentSchedules)
		{
			if( ( ddv.getDomainDataId().equals( pSV.getStatusId() ) ) )
			{
				flage = false;
			    break;
			}
			
		}
		if(flage)
		{
			cancelBtn.setRendered(false);
			btnCollectPayment.setRendered(false);
			setPaymentNumderCol(true);
			replacedBtn.setRendered(false);
			viewMap.put(  WebConstants.REPLACE_CHEQUE_DONE, true );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void fromApproveReplaceChequeTask( Long requestId )throws PimsBusinessException, Exception 
	{
		
		viewMap.put("selectedTab",CHEQUE_DETAIL_TAB);
		modeApproval();
		fetchDataFromReqestKeyDetail();
		paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID( requestId );
		if( this.getPaymentSchedules() !=null && this.getPaymentSchedules().size()>0 )
		{
			this.setPaymentSchedules(paymentSchedules);
		    viewMap.put(WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules);
		}
		getPaymentSchedules();
		getBounceChequeList();
	}
	
	@SuppressWarnings("unchecked")
	private void fromRequestSearch() throws PimsBusinessException, Exception
	{
		//calling mode for initiate replace cheque procedure 
		modeSend();
		requestView = 	( RequestView )requestMap.get( WebConstants.REQUEST_VIEW );
		viewMap.put("REQUEST_ID_FOR_WITHDRAWL",requestView.getRequestId());
			   if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_PAY_BOUNCE_CHEQUE)==0)
				   viewMap.put("PROCEDURE_TYPE", "BOUNCED");
			   else if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_REPLACE_CHEQUE)==0)
				   viewMap.put("PROCEDURE_TYPE", "COLLECTED");
		this.requestId=requestView.getRequestId().toString();
		populateApplicationDataTab( this.requestId );
		List<RequestView> requestList = new PropertyServiceAgent().getAllRequests( requestView, "", "", null );
		if(requestList.size()>0)
		{
			requestView = ( RequestView )requestList.iterator().next();
			viewMap.put( "REQUEST_VIEW_FOR_EDIT", requestView );
			fetchDataFromReqestKeyDetail();
			paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID( Long.parseLong( requestId ) );
			if( paymentSchedules!=null && paymentSchedules.size() > 0 )
			{
			   viewMap.put( "PAYMENT_SCH_FOR_EDIT", paymentSchedules );
			   viewMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, paymentSchedules );
			   viewMap.put( WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules );
			}
			else
			   viewMap.put( "NO_PAYMENT_SCH_FOR_EDIT", true );
			getPaymentSchedules();
			getBounceChequeList();
			
		}
		ApplicationContext.getContext().get( WebContext.class ).setAttribute( WebConstants.REQUEST_VIEW, requestView );
		CommonUtil.loadAttachmentsAndComments( requestId );
		if( requestView.getStatusId()!=null )
		{
			DomainDataView ddv = CommonUtil.getIdFromType( 
					                                       CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
					                                       WebConstants.REQUEST_STATUS_NEW
					                                      );
			if(!requestView.getStatusId().equals(ddv.getDomainDataId()))
			{
				sendBtn.setRendered(false);
				saveButton.setRendered(false);
				setEditPaymentEnable(false);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);		
			}
		}
//		fromApproveReplaceChequeTask( new Long (requestId ));
//		fromCollectReplaceCheque( new Long (requestId ));
	}
	@SuppressWarnings("unchecked")
	private void fromPaymentSearch() throws Exception, PimsBusinessException 
	{
	   setEditPaymentEnable( false );
	   viewMap.put( "selectedTab",CONTRACT_DETAIL_TAB );
	   viewMap.put( WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW, requestMap.get( WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW ) );
	   //extract status here and decide for further action
	   sessionMap.remove( WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW );
	   modeInitiate();
	   DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
	   BounceChequeView bounceChequeView = ( BounceChequeView )requestMap.get( WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW );
	   fillValues( bounceChequeView.getNumber() );
	   if(  bounceChequeView.getPaymentReceiptDetailId() != null  )
		  viewMap.put( "CHEQUE_NUMBER", bounceChequeView.getPaymentReceiptDetailId() );
	   viewMap.put( "CONTRACT_NUMBER_ACTION", bounceChequeView.getNumber() );
	   List<PaymentReceiptView> paymentReceiptViewList = 	psa.getCollectedChequeOfContract( bounceChequeView.getNumber() );
	   BounceChequeView boucCheViewForList = new BounceChequeView();
	   DomainDataView ddvCollected =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_COLLECTED );
	   DomainDataView ddvRealized  =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.REALIZED );
	   DomainDataView ddvBounced   =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED );
	   if(bounceChequeView!=null && bounceChequeView.getStatusId()!=null)
	   {
		   if(bounceChequeView.getStatusId().compareTo(ddvBounced.getDomainDataId())==0)
			   viewMap.put("PROCEDURE_TYPE", "BOUNCED");
		   else if(bounceChequeView.getStatusId().compareTo(ddvCollected.getDomainDataId())==0)
			   viewMap.put("PROCEDURE_TYPE", "COLLECTED");
	   }
	   String conditionId="";
	   for( PaymentReceiptView payRecpView : paymentReceiptViewList )
	   {
			 Set<PaymentReceiptDetailView> payRecDetViewSet =   payRecpView.getPaymentReceiptDetails();
			 Iterator<PaymentReceiptDetailView> iterPaymentRecpDet = payRecDetViewSet.iterator();
			 while ( iterPaymentRecpDet.hasNext() ) 
			 {
				 PaymentReceiptDetailView payRecDet = iterPaymentRecpDet.next();
				 logger.logInfo(  payRecDet.getPaymentReceiptDetailId().toString() );
				 boolean isCollectedRealizedBounced = ( payRecDet.getMethodStatusId()==null ? false :
					                                    ( 
					                                	 payRecDet.getMethodStatusId().longValue() == this.getPaymentScheduleBouncedDomainDataId() ||
				                                         payRecDet.getMethodStatusId().compareTo( ddvRealized.getDomainDataId() ) ==0  || 
				                                         payRecDet.getMethodStatusId().longValue() == this.getPaymentScheduleCollectedDomainDataId()
				                                        )
				                                      ); 
			     if( (  payRecDet.getMethodStatusId() == null || isCollectedRealizedBounced ) && 
			    	 (  payRecDet.getPaymentMethod()  != null && payRecDet.getPaymentMethod().getPaymentMethodId() == 1L )
			       )
			     {
			    	 boucCheViewForList = new BounceChequeView();
			    	 
			    	 if (payRecDet.getPaymentNumber()!=null) 
			    	 {
			    		 boucCheViewForList.setPaymentNumber(payRecDet.getPaymentNumber());
			    	 }
			    	 
			    	 boucCheViewForList.setPaymentReceiptDetailId(payRecDet.getPaymentReceiptDetailId());
			    	 boucCheViewForList.setReceiptNumber(payRecpView.getReceiptNumber());
			    	 boucCheViewForList.setChequeNumber(payRecDet.getMethodRefNo());
			    	 boucCheViewForList.setPaymentDetailAmount(payRecDet.getAmount());
			    	 boucCheViewForList.setAccountNumber(payRecDet.getAccountNo());
			    	 boucCheViewForList.setPaymentMethodDescription(payRecDet.getPaymentMethod().getDescription());
			    	 
			    	 if (payRecDet.getMethodRefDate()!=null) {
			    		 boucCheViewForList.setPaymentDueOn(payRecDet.getMethodRefDate());
			    	 }
			    	 
			    	 if( payRecDet.getMethodStatusId() == null )
			    	 {
			        	  boucCheViewForList.setPaymentDetailStatusEn(ddvCollected.getDataDescEn());
			        	  boucCheViewForList.setPaymentDetailStatusAr(ddvCollected.getDataDescAr());
			        	  boucCheViewForList.setPaymentDetailStatusId(ddvCollected.getDomainDataId());
			    	 }
			    	 else
			    	 {
			    		 if( payRecDet.getMethodStatusId().equals( ddvCollected.getDomainDataId() ) ) 
			    		 {
			    			 boucCheViewForList.setPaymentDetailStatusEn(ddvCollected.getDataDescEn());
			            	 boucCheViewForList.setPaymentDetailStatusAr(ddvCollected.getDataDescAr());
			            	 boucCheViewForList.setPaymentDetailStatusId(ddvCollected.getDomainDataId());
			    		 }
			    		 else if( payRecDet.getMethodStatusId().equals( ddvRealized.getDomainDataId() ) )
			    		 {
			    			 boucCheViewForList.setPaymentDetailStatusEn(ddvRealized.getDataDescEn());
			            	 boucCheViewForList.setPaymentDetailStatusAr(ddvRealized.getDataDescAr());
			            	 boucCheViewForList.setPaymentDetailStatusId(ddvRealized.getDomainDataId());
			    		 }
			    		 else if( payRecDet.getMethodStatusId().equals( ddvBounced.getDomainDataId() ) )
			    		 {
			    			 boucCheViewForList.setPaymentDetailStatusEn(ddvBounced.getDataDescEn());
			            	 boucCheViewForList.setPaymentDetailStatusAr(ddvBounced.getDataDescAr());
			            	 boucCheViewForList.setPaymentDetailStatusId(ddvBounced.getDomainDataId());
			    		 }
			    	 }
			    	boolean chequeNumberEqual = boucCheViewForList.getPaymentReceiptDetailId().compareTo(  new Long ( viewMap.get( "CHEQUE_NUMBER" ).toString() ) )==0 ;
			    	boucCheViewForList.setRenderedCheckBox( false );
			    	if( chequeNumberEqual )
			    	{
			    	      boucCheViewForList.setEnableDisableCheckBox( true );
			    	      boucCheViewForList.setRenderedCheckBox( true );
			    		  boucCheViewForList.setSelected( true );
			    		  if( payRecDet.getMethodStatusId() == null || payRecDet.getMethodStatusId().equals( ddvCollected.getDomainDataId() ) )
			    		  {
			    			  conditionId = WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE;
			    			  this.setNoOfCollectedChqToReplace( this.getNoOfCollectedChqToReplace() + 1 );
			    		  }
			    		  else if( payRecDet.getMethodStatusId().equals( ddvBounced.getDomainDataId() ) )
			    		  {
			    			  conditionId = WebConstants.CONDITION_PAYMENT_CONFIG_BOUNCE_CHEQUE;
			    			  this.setNoOfBounceChqToReplace( this.getNoOfBounceChqToReplace() + 1 );
			    		  }
			    	}
			    	else
			    	{
			    	  
			    		if( 
			    		 ( boucCheViewForList.getPaymentDetailStatusId().equals( ddvCollected.getDomainDataId() ) ||
			    		   boucCheViewForList.getPaymentDetailStatusId().equals( ddvBounced.getDomainDataId() ) )
			         	 )
			    		  boucCheViewForList.setRenderedCheckBox( true );	
			    	  if( payRecDet.getMethodStatusId() == null || payRecDet.getMethodStatusId().equals( ddvCollected.getDomainDataId() ) )  
					   { 
			    		   Date todate = new Date();
						   Calendar paymentDueOn = GregorianCalendar.getInstance();
						   paymentDueOn.setTime( payRecDet.getMethodRefDate() ); 
						   Calendar now = GregorianCalendar.getInstance();
						   int daysDiff = DateUtil.getDaysBetween( paymentDueOn, now );
						   if( df.parse( df.format( todate ) ).compareTo( df.parse( df.format( payRecDet.getMethodRefDate() ) ) ) >= 0 ) 
							   boucCheViewForList.setShowRealized( false );
						   if(  getSysConfigDaysLimit() >=0 && daysDiff <  getSysConfigDaysLimit() )
							   boucCheViewForList.setIsExceedMaxDaysForWithDrawal( true );
					    }
			    	
			    	}
			    	//if the selected cheque is received then allow the user select all received cheques
			    	//else if selected cheque is bounced then user would be able to select only bounced cheques.
			    	if( (getSelectedProcedureType().compareTo("1")==0 && (payRecDet.getMethodStatusId()==null || (payRecDet.getMethodStatusId()!=null && payRecDet.getMethodStatusId().compareTo(ddvCollected.getDomainDataId())==0))) || 
			    		(getSelectedProcedureType().compareTo("2")==0 && payRecDet.getMethodStatusId() !=null && payRecDet.getMethodStatusId().compareTo(ddvBounced.getDomainDataId())==0))
			    		bounceChequeList.add( boucCheViewForList );
			    	else 
			    		{
			    			boucCheViewForList.setReadOnly(true);
			    			bounceChequeList.add( boucCheViewForList );
			    		}
			     }
			   }
	   }
      viewMap.put( WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceChequeList );
      viewMap.put( WebConstants.OLD_PAYMENT_SCHEDULE_ID_FOR_REPLACE_CHEQUE, bounceChequeView.getPaymentScheduleId() );
      
      getBounceChequeList();
      long contractId = Long.parseLong( viewMap.get( WebConstants.Contract.LOCAL_CONTRACT_ID ).toString() );
      this.setCollectedChequePaymentConfiguration(
    		                                      psa.getDefaultPaymentsForReplaceCheque(contractId, getIsEnglishLocale(), 
    		                                    		                                 WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE
    		                                    		                                 )
    		                                     );
      this.setBouncedChequePaymentConfiguration( 
    		                                      psa.getDefaultPaymentsForReplaceBounceCheque( contractId, getIsEnglishLocale(), 
    		                                    		                                WebConstants.CONDITION_PAYMENT_CONFIG_BOUNCE_CHEQUE
    		                                    		                                )
    		                                      );

      if(conditionId.compareTo(WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE)==0)
    	  paymentSchedules = psa.getDefaultPaymentsForReplaceCheque( contractId, getIsEnglishLocale(), conditionId );
      else 
    	  paymentSchedules = psa.getDefaultPaymentsForReplaceBounceCheque(contractId, getIsEnglishLocale(), conditionId );
	  if( paymentSchedules != null && paymentSchedules.size()>0 )
	  {
	   this.setPaymentSchedules( paymentSchedules );
	   viewMap.put( WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules );
	  }
	  getContractById( viewMap.get( WebConstants.Contract.LOCAL_CONTRACT_ID ).toString() );	
	  getPaymentSchedules();
	}
	@SuppressWarnings( "unchecked" )
	private void fetchFromRequestFieldDetailForTasks() throws PimsBusinessException, Exception 
	{
		
		RequestFieldDetailView requestFieldDetailViewNew;
		RequestFieldDetailView requestFieldDetailViewDetailId;
		requestFieldDetailViewDetailId= new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue
		                                                       (
				                                                 requestView.getRequestId(),
				                                                 getRequestKeyViewId(
				                                                                       requestKeys,
				                                                                       WebConstants.ReplaceCheque.PAYMENT_RECEIPT_DETAIL_ID
				                                                                      )
				                                               );
		viewMap.put("PAYMENT_RECEIPT_DETAIL_STATUS_IDS",requestFieldDetailViewDetailId.getRequestKeyValue());
		refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
		requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.OLD_PAYMENT_SCHEDULE_ID));	
		String contractNumber = requestFieldDetailViewNew.getRequestKeyValue().toString();
		fillValues(contractNumber);
		paymentSchedules = psa.getContractPaymentSchedule( null, new Long( requestId ) );
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		getPaymentSchedules();
	}
	@SuppressWarnings("unchecked")
	private void fetchDataFromReqestKeyDetail()throws PimsBusinessException, Exception
	{
		RequestFieldDetailView requestFieldDetailViewNew;
		requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue
		                                           (
				                                     requestView.getRequestId(),
				                                     getRequestKeyViewId( requestKeys, WebConstants.ReplaceCheque.PAYMENT_RECEIPT_DETAIL_ID )
				                                    );
		viewMap.put("PAYMENT_RECEIPT_DETAIL_STATUS_IDS",requestFieldDetailViewNew.getRequestKeyValue());
		refrashChequeDetail( viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString() );
		viewMap.put("PAYMENT_DETAIL_STATUS_FOR_EDIT", requestFieldDetailViewNew);
		requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(
				                                       requestView.getRequestId(),
				                                       getRequestKeyViewId( requestKeys,WebConstants.OLD_PAYMENT_SCHEDULE_ID )
				                                       );
		String contractNumber = requestFieldDetailViewNew.getRequestKeyValue().toString();
		viewMap.put("PAYMENT_SCHEDULE_ID_FOR_EDIT", requestFieldDetailViewNew);
	    fillValues( contractNumber );
	    
	    contractView = propertyServiceAgent.getContract(contractNumber, "dd/MM/yyyy");
		getContractById( contractView.getContractId().toString() );
		
	}
    @SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId)
    {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.REPLACE_CHEQUE);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.REPLACE_CHEQUE;
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		if(requestId!=null)
		{
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		
		 try 
		 
		 {
			 viewMap.put(WebConstants.SELECT_BLACK_LIST,true);
		
			if(hdnPersonId!=null && !hdnPersonId.equals(""))
			{
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}
	
				if(hdnPersonName!=null && !hdnPersonName.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
				
				
				if(hdnCellNo!=null && !hdnCellNo.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
				
				if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
				{
					DomainDataView ddv = new DomainDataView();
					if(Long.parseLong(hdnIsCompany)==1L)
					{
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);
					        
					}
					else
					{
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);
					
					}
					
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}
				
			
				if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
					if(isPaymentCollected)
				    {
					    
							CollectPayment();
							if(requestId != null)
							{
		  					    viewMap.put(WebConstants.REPLACE_CHEQUE_DONE, true);
		  					    //when payment Received successfully 
		  					    btnCollectPayment.setRendered(false);
		  					    cancelBtn.setRendered(false);
		  					    setEditPaymentEnable(false);
		  					    //for refrashing paymentSchdule List  
			  					  paymentSchedules = psa.getContractPaymentSchedule(null,Long.parseLong(requestId));
			  	                  viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
			  					  getPaymentSchedules();
								  
					        }
				       }
					
				}
				
				String selectedTab = (String)viewMap.get("selectedTab");
				if(selectedTab!=null)
				{
					richTabPanel.setSelectedTab(selectedTab);
					viewMap.remove("selectedTab");
				}
				if( sessionMap.containsKey( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION ) )
					btnCollectPayment.setDisabled( true );
			
			} 
		    catch (NumberFormatException e) 
			{
				errorMessages = new ArrayList<String>(0);
	    		logger.LogException("prerender|Error Occured", e);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			} catch (PimsBusinessException e) 
			{
				errorMessages = new ArrayList<String>(0);
	    		logger.LogException("prerender|Error Occured", e);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}	
		

	}
	
	private void updateTask() 
	{
		String  METHOD_NAME = "updateTask|";
		errorMessages = new ArrayList<String>(0);
	    successMessages = new ArrayList<String>(0);
	    logger.logInfo(METHOD_NAME+" started...");
	    try
	    {
		    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is:"+contextPath);
		        String loggedInUser = getLoggedInUser();
				BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			
				client.completeTask(userTask, loggedInUser, TaskOutcome.COLLECT);
//				NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_COLLECT, requestId);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_REPLACED));
		    	logger.logInfo(METHOD_NAME+" completed successfully...");
		    	modeCollectReplaceChequeFinish();
		    	viewMap.put("selectedTab",CHEQUE_DETAIL_TAB);
		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException(METHOD_NAME + "crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_REPLACED));
		
		}
	    catch (Exception e) 
	    {
			logger.LogException(METHOD_NAME + "crashed...", e);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_REPLACED));
		}
	}
	private void CollectPayment()
    {
    	String methodName="CollectPayment|"; 
		logger.logInfo(methodName +  "Start...");
        successMessages = new ArrayList<String>(0);
		
        try
		{
	    	 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
		     prv.setPaidById(contractView.getTenantView().getPersonId());
		     prv = psa.collectPayments(prv);
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.REPLACE_CHEQUE);
		     replace();
			 successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
		     logger.logInfo(methodName +  "Finish...");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + "|" + "Exception Occured...",ex);
			
		}
    }
	public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.REPLACE_CHEQUE);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	//Common Methods
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings("unchecked")
	private void fillValues(String contractNumber)throws Exception,PimsBusinessException
	{
		
		if(contractNumber!=null)
		{
			contractView = propertyServiceAgent.getContract(contractNumber, "dd/MM/yyyy");
			if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
	     	{
				viewMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
				viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
				
				//Setting the Contract/Tenant Info
				tenantNameText.setValue( contractView.getTenantView().getPersonFullName() );
				hdnPersonId = contractView.getTenantView().getPersonId().toString();
				viewMap.put( "TENANT_ID", hdnPersonId );
				contractTypeText.setValue(contractView.getContractTypeEn());
				contractNoText.setValue(contractView.getContractNumber());
				contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
				contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
				if(getIsEnglishLocale())
				contractStatusText.setValue(contractView.getStatusEn());
				else
				contractStatusText.setValue(contractView.getStatusAr());
				totalContractValText.setValue(contractView.getRentAmount());
				//Added by shiraz for populate required field in the new screen
				if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
			    { 
					List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
					contractUnitViewList.addAll(contractView.getContractUnitView());
					ContractUnitView contractUnitView =contractUnitViewList.get(0);
					logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
					txtUnitType.setValue( getIsEnglishLocale()?
							                                 contractUnitView.getUnitView().getUsageTypeEn():
							                                 contractUnitView.getUnitView().getUsageTypeAr());
					
					logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
				    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
				    
				    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
				    txtpropertyType.setValue(getIsEnglishLocale()?
				    		                                     contractUnitView.getUnitView().getPropertyTypeEn():
				    		                                     contractUnitView.getUnitView().getPropertyTypeAr());
				    
				    
				    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
				    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
				
				    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
				    txtUnitRentValue.setValue( contractUnitView.getRentValue()!=null ?
				    		                                                          contractUnitView.getRentValue().toString():
				    		                                                          ""
				    		                 );
				
		    	}
		    }
	         
		}		
	}
	@SuppressWarnings("unchecked")
	public String btnContract_Click()
	{
		
		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		if( viewMap.get( "LOCAL_CONTRACT_ID" ) != null )
		getFacesContext().getExternalContext().getSessionMap().put("contractId", viewMap.get("LOCAL_CONTRACT_ID"));
		getFacesContext().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		String javaScriptText = "var screen_width = 1024;var screen_height = 450;"+
                               "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                               WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                               WebConstants.VIEW_MODE+"="+
                               WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                               "','_blank','width='+(screen_width-10)+'," +
                               "height='+(screen_height)+',left=0,top=10,scrollbars=no,status=yes');";
	openPopUp("LeaseContract.jsf",javaScriptText);
		
		logger.logInfo(methodName+"|"+"Finish..");
		return "CONTRACT_VIEW";
	}
	@SuppressWarnings("unchecked")
	public String btnTenant_Click()
	{
		
		String methodName="btnTenant_Click";
		logger.logInfo(methodName+"|"+"Start..");
		if( viewMap.get( "TENANT_ID" )!=null )
		getFacesContext().getExternalContext().getRequestMap().put("tenantId", viewMap.get("TENANT_ID"));
		getFacesContext().getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		String javaScriptText  = "javascript:showPersonReadOnlyPopup("+viewMap.get("TENANT_ID").toString()+");";
		openPopUp("",javaScriptText);
		logger.logInfo(methodName+"|"+"Finish..");
		return "TENANT_VIEW";
	}

	private String getLoggedInUser() {
		
		HttpSession session = (HttpSession) getFacesContext().getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }

	public void saveComments(Long requestId) throws Exception
    {
    	try{
	    	
    		String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, requestId);

    	}
    	catch (Exception exception) {
			logger.LogException("saveComments|crashed ", exception);
			throw exception;
		}
    }
	@SuppressWarnings("unchecked")
	private HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logInfo(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
				hMap.put("returnId",domainDataView.getDomainDataId());
				logger.logInfo(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
				hMap.put("IdEn",domainDataView.getDataDescEn());
				hMap.put("IdAr",domainDataView.getDataDescAr());
			}

		}
		logger.logInfo(methodName+"|"+"Finish...");
		return hMap;

	}
	@SuppressWarnings("unchecked")
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (ArrayList<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	private Long getRequestType() throws PimsBusinessException 
	{
		Long nolReqTypeId = null;
		RequestTypeView requestTypeView =new RequestTypeView();
		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
		if(getSelectedProcedureType().compareTo("1")==0)
			requestTypeView = requestServiceAgent.getRequestType(WebConstants.NOL.REQUEST_TYPE_REPLACE_CHEQUE_ID);
		else if(getSelectedProcedureType().compareTo("2")==0)
			requestTypeView = requestServiceAgent.getRequestType(WebConstants.NOL.REQUEST_TYPE_PAY_BOUNCE_CHEQUE_ID);

		nolReqTypeId= requestTypeView.getRequestTypeId();
		return nolReqTypeId;
	}
	private RequestKeyView getRequestKeysForProcedureKeyName(String procedureType,String keyName)throws PimsBusinessException
	{
	  List<RequestKeyView> rkViewList =commonUtil.getRequestKeysForProcedureType(procedureType);
	  for (RequestKeyView requestKeyView : rkViewList) 
	  {
		  if(requestKeyView.getKeyName().equals(keyName))
		  {
			  return requestKeyView;
			  
		  }
	  }
	  
		  return null;
	
	}
	private void setRequestFieldDetailView(
			RequestFieldDetailView requestFieldDetailView ) throws PimsBusinessException {
		
		
		requestFieldDetailView.setCreatedBy(getLoggedInUser());
		requestFieldDetailView.setCreatedOn(new Date());
		requestFieldDetailView.setUpdatedBy(getLoggedInUser());
		requestFieldDetailView.setUpdatedOn(new Date());
		requestFieldDetailView.setRecordStatus(new Long(1));
		requestFieldDetailView.setIsDeleted(new Long(0));
		requestFieldDetailView.setRequestKeyValue(bankStatusMenu.getValue().toString());
		RequestKeyView  requestKeyView=getRequestKeysForProcedureKeyName(WebConstants.ReplaceCheque.PROCEDURE_TYPE_REPLACE_CHEQUE,WebConstants.CHEQUE_STATUS_AT_BANK);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		
	}
	private void setRequestFieldDetailView(
			RequestFieldDetailView requestFieldDetailView , Long paymentReceiptDetailId) throws PimsBusinessException {
		
		
		requestFieldDetailView.setCreatedBy(getLoggedInUser());
		requestFieldDetailView.setCreatedOn(new Date());
		requestFieldDetailView.setUpdatedBy(getLoggedInUser());
		requestFieldDetailView.setUpdatedOn(new Date());
		requestFieldDetailView.setRecordStatus(new Long(1));
		requestFieldDetailView.setIsDeleted(new Long(0));
		requestFieldDetailView.setRequestKeyValue(paymentReceiptDetailId.toString());
		RequestKeyView  requestKeyView=getRequestKeysForProcedureKeyName(WebConstants.ReplaceCheque.PROCEDURE_TYPE_REPLACE_CHEQUE,WebConstants.PAYMENT_RECEIPT_DETAIL_ID);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		
	}
	
     @SuppressWarnings("unchecked")
	public String sendForAcceptance()
	{
		String  METHOD_NAME = "sendForAcceptance|";		
		try 
		{
			UserTask userTask = (UserTask) getFacesContext().getViewRoot().getAttributes().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
			String loggedInUser = getLoggedInUser();
			Map taskAttributes =  userTask.getTaskAttributes();
			long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			addRequestFieldAndRequestDetail(METHOD_NAME, requestId);
	       	BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException("Failed to approve due to:",ex);
			errorMessages.add(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_VIEW,null);

		}
		catch (PimsBusinessException e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			
		}
		catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + "crashed...", e);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_VIEW,null);

		}
		
		return "success";
	}
     @SuppressWarnings( "unchecked" )
	private void addRequestFieldAndRequestDetail(String METHOD_NAME,
			long requestId)throws PimsBusinessException,Exception 
	{
		RequestKeyView requestKeyView;
		try {
			RequestFieldDetailView requestFieldDetailView = new RequestFieldDetailView();
			RequestDetailView requestDetailView = new RequestDetailView();
			DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CHEQUE_STATUS_AT_BANK),
	                WebConstants.ReplaceCheque.OLD_CHEQUE_BOUNCED);
			//List<FeeConfigurationView> feeViewListTemp	= commonUtil.getFeeConfigByProcedureType(WebConstants.ReplaceCheque.CHEQUE_BOUNCE_FEE);
			if(bankStatusMenu.getValue().toString().equals(ddv.getDomainDataId().toString())){
				//addPaymentSchedule(requestId,feeViewListTemp.get(0).getMaxAmount());	
			}
			ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CHEQUE_STATUS_AT_BANK),
	                WebConstants.ReplaceCheque.OLD_CHEQUE_LIQUIDATED);
			Long oldPaymentScheduleId =null;
			if(paymentScheduleId!=null)
			{
				oldPaymentScheduleId = propertyServiceAgent.getOldPaymentScheduleIdByPaymentScheduleIdID(paymentScheduleId);
				PaymentScheduleView paymentScheduleNewView = new PaymentScheduleView();
				PaymentScheduleView oldPaymentScheduleView = new PaymentScheduleView();
				paymentScheduleNewView.setPaymentScheduleId(paymentScheduleId);
				oldPaymentScheduleView.setPaymentScheduleId(oldPaymentScheduleId);
				if(bankStatusMenu.getValue().toString().equals(ddv.getDomainDataId().toString())){
					updatePaymentSchedule(oldPaymentScheduleView,paymentScheduleNewView);	
			}
			}
	
			List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.PROCEDURE_TYPE);
			ddv=CommonUtil.getIdFromType(ddvList, WebConstants.ReplaceCheque.PROCEDURE_TYPE_REPLACE_CHEQUE);		
			PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
			paymentScheduleView.setPaymentScheduleId(paymentScheduleId);
			
			
			requestDetailView.setPaymentScheduleView(paymentScheduleView);
			requestDetailView.setCreatedBy(getLoggedInUser());
			requestDetailView.setCreatedOn(new Date());
			requestDetailView.setUpdatedBy(getLoggedInUser());
			requestDetailView.setUpdatedOn(new Date());
			requestDetailView.setIsDeleted(0L);
			requestDetailView.setRecordStatus(1L);
			
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.ReplaceCheque.PROCEDURE_TYPE_REPLACE_CHEQUE, WebConstants.ReplaceCheque.REQUEST_KEY_CHEQUE_STATUS_AT_BANK);

		
			
		Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>(0);
		setRequestFieldDetailView(requestFieldDetailView);
		

		requestFieldDetailView.setRequestKeyView(requestKeyView);
		
		requestFieldDetailViewSet.add(requestFieldDetailView);
		
		Long paymentReceiptDetailId = (Long)getFacesContext().getExternalContext().getSessionMap().get("PAYMENT_RECEIPT_DETAIL_ID");
		//requestFieldDetailView = new RequestFieldDetailView();
		if(paymentReceiptDetailId!=null){
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailView(requestFieldDetailView, paymentReceiptDetailId);
		requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		requestView.setRequestFieldDetailsView(requestFieldDetailViewSet);
			propertyServiceAgent.addReplaceChequeRequest(requestFieldDetailViewSet, requestDetailView, requestId);
		
		sessionMap.put(WebConstants.FROM_TASK_LIST,null);

		logger.logInfo(METHOD_NAME+" completed successfully...");
		
		actionPerformedMode();
		successMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReplaceCheque.MSG_APPROVED_SUCCESSFULLY));
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			
		}
	}

	private void actionPerformedMode(){
		contractNumberText.setReadonly(true);
		statusText.setReadonly(true);
		contractStartDateText.setReadonly(true);
		contractExpiryDateText.setReadonly(true);
		tenantReferenceNoText.setReadonly(true);
		tenantNameText.setReadonly(true);
		contractTypeText.setReadonly(true);
		totalContractValue.setReadonly(true);
		unitRefNoText.setReadonly(true);
		unitRentValueText.setReadonly(true);
		propertyNameText.setReadonly(true);
		unitTypeText.setReadonly(true);
		unitRefNoText.setRendered(true);
		unitRentValueText.setRendered(true);
		propertyNameText.setRendered(true);
		unitTypeText.setRendered(true);
		
		oldChequeAmountText.setReadonly(true);
		oldChequeBankNameText.setReadonly(true);
		oldChequeDate.setReadonly(true);
		oldChequeNumberText.setReadonly(true);
		
		closeButton.setRendered(false);
		submitButton.setRendered(false);
		finalSubmitButton.setRendered(false);
		replaceButton.setRendered(false);
		completeButton.setRendered(false);
		paymentButton.setRendered(false);
		
		bankStatusMenu.setDisabled(true);
		
	}
	
	@SuppressWarnings("unchecked")
	public void replaceChequePopup(){
		String methodName="pay|";
		logger.logInfo(methodName+"started..");
		try{
			Map map = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			PaymentReceiptView paymentreceiptView = new PaymentReceiptView();
			paymentreceiptView.setAmount(Double.parseDouble(oldChequeAmountText.getValue().toString()));
			PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
			paymentreceiptView.setPaymentScheduleView(paymentScheduleView);
			if(getFacesContext().getViewRoot().getAttributes().get(WebConstants.ReplaceCheque.PAYMENT_SCHEDULE_ID)!=null)
				paymentScheduleView.setPaymentScheduleId(Long.valueOf(getFacesContext().getViewRoot().getAttributes().get(WebConstants.ReplaceCheque.PAYMENT_SCHEDULE_ID).toString()));
				paymentreceiptView.setPaymentScheduleView(paymentScheduleView);
				
			map.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentreceiptView);
			map.put(WebConstants.ReplaceCheque.CALLER, "REPLACE_CHEQUE");
			
			logger.logInfo(methodName+"|"+"completed successfully..");
	
		
		
		
		openPopUp("receivePayment.jsf", "");
		
	    }catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		
	}
		private void updatePaymentSchedule(PaymentScheduleView oldPaymentScheduleView,PaymentScheduleView paymentScheduleNewView)  throws Exception{
		String  METHOD_NAME = "updatePaymentSchedule|";		
		logger.logInfo(METHOD_NAME+" started...");
		try 
		{
			DomainDataView ddv= CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.REALIZED);
			oldPaymentScheduleView.setStatusId(ddv.getDomainDataId());
			ddv= CommonUtil.getIdFromType( this.getPaymentScheduleStatus(),  WebConstants.CANCELED);
			paymentScheduleNewView.setStatusId(ddv.getDomainDataId());
			oldPaymentScheduleView.setUpdatedBy(getLoggedInUser());
			paymentScheduleNewView.setUpdatedBy(getLoggedInUser());
			logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
	}
	@SuppressWarnings("unchecked")
	public void paymentPopup(){
		String methodName="pay|";
		logger.logInfo(methodName+"started..");
		try{
			Map map = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			List<FeeConfigurationView> feeViewListTemp  = new ArrayList<FeeConfigurationView>();
			List<PaymentScheduleView> paymentSchViewList  = new ArrayList<PaymentScheduleView>();
			Long requestId  =(Long)getFacesContext().getViewRoot().getAttributes().get("REPLACE_CHEQUE_REQUEST_ID");
			paymentSchViewList = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
			Double totalFineAmount = 0.00;
			
      	for(int index = 0;index<feeViewListTemp.size();index++)
      	{
				totalFineAmount = totalFineAmount + feeViewListTemp.get(index).getMaxAmount();
			}
			PaymentReceiptView paymentreceiptView = new PaymentReceiptView();
			paymentreceiptView.setAmount(totalFineAmount);
			PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
		
			
			paymentreceiptView.setPaymentScheduleView(paymentScheduleView);
			if(getFacesContext().getViewRoot().getAttributes().get(WebConstants.ReplaceCheque.PAYMENT_SCHEDULE_ID)!=null)
				paymentScheduleView.setPaymentScheduleId(Long.valueOf(viewMap.get(WebConstants.ReplaceCheque.PAYMENT_SCHEDULE_ID).toString()));
				paymentreceiptView.setPaymentScheduleView(paymentScheduleView);
				
			map.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentreceiptView);
			map.put(WebConstants.ReplaceCheque.CALLER, "Payment");
			
			logger.logInfo(methodName+"|"+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		
		
		openPopUp("receivePayment.jsf", "");		
		
	}
	
	private void openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText=extraJavaScript;
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
	
	}
	
	@SuppressWarnings("unchecked")
	public void tabRequiredDocuments_Click()
	{
		String methodName ="tabRequiredDocuments_Click";
		logger.logInfo(methodName + "|" + "Start");
		try
		{	List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.PROCEDURE_TYPE);
			DomainDataView ddv=CommonUtil.getIdFromType(ddvList, WebConstants.ReplaceCheque.PROCEDURE_TYPE_REPLACE_CHEQUE);		
			UtilityServiceAgent usa=new UtilityServiceAgent();
			List<RequiredDocumentsView> requiredDocumentsViewList= usa.getRequiredDocumentsByProcedureType(ddv.getDomainDataId());
	
	
			viewMap.put("REPLACE_CHEQUE_REQUIRED_DOCUMENTS",requiredDocumentsViewList);
		
		logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Error Occured::"+ex);
			
		}
	}
	
	
	public void tabRequestHistory_Click()
    {
    	String methodName="tabRequestHistory_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  
    	    rhc.getAllRequestTasksForRequest(WebConstants.REPLACE_CHEQUE,this.requestId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }
	
	public List<SelectItem> getBankStatuses() {
		return bankStatuses;
	}
	public void setBankStatuses(List<SelectItem> bankStatuses) {
		this.bankStatuses = bankStatuses;
	}
	public HtmlSelectOneMenu getBankStatusMenu() {
		return bankStatusMenu;
	}
	public void setBankStatusMenu(HtmlSelectOneMenu bankStatusMenu) {
		this.bankStatusMenu = bankStatusMenu;
	}
	public HtmlCommandButton getCloseButton() {
		return closeButton;
	}
	public void setCloseButton(HtmlCommandButton closeButton) {
		this.closeButton = closeButton;
	}
	public HtmlCommandButton getCompleteButton() {
		return completeButton;
	}
	public void setCompleteButton(HtmlCommandButton completeButton) {
		this.completeButton = completeButton;
	}
	public HtmlInputText getContractExpiryDateText() {
		return contractExpiryDateText;
	}
	public void setContractExpiryDateText(HtmlInputText contractExpiryDateText) {
		this.contractExpiryDateText = contractExpiryDateText;
	}
	public HtmlInputText getContractNumberText() {
		return contractNumberText;
	}
	public void setContractNumberText(HtmlInputText contractNumberText) {
		this.contractNumberText = contractNumberText;
	}
	public HtmlInputText getContractStartDateText() {
		return contractStartDateText;
	}
	public void setContractStartDateText(HtmlInputText contractStartDateText) {
		this.contractStartDateText = contractStartDateText;
	}
	public HtmlInputText getContractTypeText() {
		return contractTypeText;
	}
	public void setContractTypeText(HtmlInputText contractTypeText) {
		this.contractTypeText = contractTypeText;
	}
	public HtmlDataTable getFeeDataTable() {
		return feeDataTable;
	}
	public void setFeeDataTable(HtmlDataTable feeDataTable) {
		this.feeDataTable = feeDataTable;
	}
	public HtmlInputText getOldChequeAmountText() {
		return oldChequeAmountText;
	}
	public void setOldChequeAmountText(HtmlInputText oldChequeAmountText) {
		this.oldChequeAmountText = oldChequeAmountText;
	}
	public HtmlInputText getOldChequeBankNameText() {
		return oldChequeBankNameText;
	}
	public void setOldChequeBankNameText(HtmlInputText oldChequeBankNameText) {
		this.oldChequeBankNameText = oldChequeBankNameText;
	}
	public HtmlInputText getOldChequeDate() {
		return oldChequeDate;
	}
	public void setOldChequeDate(HtmlInputText oldChequeDate) {
		this.oldChequeDate = oldChequeDate;
	}
	public HtmlInputText getOldChequeNumberText() {
		return oldChequeNumberText;
	}
	public void setOldChequeNumberText(HtmlInputText oldChequeNumberText) {
		this.oldChequeNumberText = oldChequeNumberText;
	}
	public HtmlCommandButton getPaymentButton() {
		return paymentButton;
	}
	public void setPaymentButton(HtmlCommandButton paymentButton) {
		this.paymentButton = paymentButton;
	}
	public HtmlInputText getPropertyNameText() {
		return propertyNameText;
	}
	public void setPropertyNameText(HtmlInputText propertyNameText) {
		this.propertyNameText = propertyNameText;
	}
	public HtmlCommandButton getReplaceButton() {
		return replaceButton;
	}
	public void setReplaceButton(HtmlCommandButton replaceButton) {
		this.replaceButton = replaceButton;
	}
	public HtmlTabPanel getReplaceChequeTabPanel() {
		return replaceChequeTabPanel;
	}
	public void setReplaceChequeTabPanel(HtmlTabPanel replaceChequeTabPanel) {
		this.replaceChequeTabPanel = replaceChequeTabPanel;
	}
	public HtmlInputText getStatusText() {
		return statusText;
	}
	public void setStatusText(HtmlInputText statusText) {
		this.statusText = statusText;
	}
	public HtmlCommandButton getSubmitButton() {
		return submitButton;
	}
	public void setSubmitButton(HtmlCommandButton submitButton) {
		this.submitButton = submitButton;
	}
	public HtmlInputText getTenantNameText() {
		return tenantNameText;
	}
	public void setTenantNameText(HtmlInputText tenantNameText) {
		this.tenantNameText = tenantNameText;
	}
	public HtmlInputText getTenantReferenceNoText() {
		return tenantReferenceNoText;
	}
	public void setTenantReferenceNoText(HtmlInputText tenantReferenceNoText) {
		this.tenantReferenceNoText = tenantReferenceNoText;
	}
	public HtmlInputText getTotalContractValue() {
		return totalContractValue;
	}
	public void setTotalContractValue(HtmlInputText totalContractValue) {
		this.totalContractValue = totalContractValue;
	}
	public HtmlInputText getUnitRefNoText() {
		return unitRefNoText;
	}
	public void setUnitRefNoText(HtmlInputText unitRefNoText) {
		this.unitRefNoText = unitRefNoText;
	}
	public HtmlInputText getUnitRentValueText() {
		return unitRentValueText;
	}
	public void setUnitRentValueText(HtmlInputText unitRentValueText) {
		this.unitRentValueText = unitRentValueText;
	}
	public HtmlInputText getUnitTypeText() {
		return unitTypeText;
	}
	public void setUnitTypeText(HtmlInputText unitTypeText) {
		this.unitTypeText = unitTypeText;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
		
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public HtmlDataTable getRequiredDocumentsDataTable() {
		return requiredDocumentsDataTable;
	}
	public void setRequiredDocumentsDataTable(
			HtmlDataTable requiredDocumentsDataTable) {
		this.requiredDocumentsDataTable = requiredDocumentsDataTable;
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	@SuppressWarnings( "unchecked" )
	public List<RequiredDocumentsView> getListRequiredDocuments() {
		if(getFacesContext().getViewRoot().getAttributes().get("REPLACE_CHEQUE_REQUIRED_DOCUMENTS")!=null){
			listRequiredDocuments=(ArrayList<RequiredDocumentsView>)getFacesContext().getViewRoot().getAttributes().get("REPLACE_CHEQUE_REQUIRED_DOCUMENTS"); 
		}
		
		return listRequiredDocuments;
	}
	public void setListRequiredDocuments(
			List<RequiredDocumentsView> listRequiredDocuments) {
		this.listRequiredDocuments = listRequiredDocuments;
	}
	public HtmlCommandButton getContractButton() {
		return contractButton;
	}
	public void setContractButton(HtmlCommandButton contractButton) {
		this.contractButton = contractButton;
	}
	public HtmlCommandButton getTenantButton() {
		return tenantButton;
	}
	public void setTenantButton(HtmlCommandButton tenantButton) {
		this.tenantButton = tenantButton;
	}
	public HtmlOutputText getBankStatusLabel() {
		return bankStatusLabel;
	}
	public void setBankStatusLabel(HtmlOutputText bankStatusLabel) {
		this.bankStatusLabel = bankStatusLabel;
	}
	public HtmlCommandButton getAttachmentButton() {
		return attachmentButton;
	}
	public void setAttachmentButton(HtmlCommandButton attachmentButton) {
		this.attachmentButton = attachmentButton;
	}
	public HtmlCommandButton getCommentButton() {
		return commentButton;
	}
	public void setCommentButton(HtmlCommandButton commentButton) {
		this.commentButton = commentButton;
	}
	public HtmlCommandButton getFinalSubmitButton() {
		return finalSubmitButton;
	}
	public void setFinalSubmitButton(HtmlCommandButton finalSubmitButton) {
		this.finalSubmitButton = finalSubmitButton;
	}
	public String close(){
		return "CLOSED";
	}
	public HtmlInputText getContractNoText() {
		return contractNoText;
	}
	public void setContractNoText(HtmlInputText contractNoText) {
		this.contractNoText = contractNoText;
	}
	public HtmlInputText getContractEndDateText() {
		return contractEndDateText;
	}
	public void setContractEndDateText(HtmlInputText contractEndDateText) {
		this.contractEndDateText = contractEndDateText;
	}
	public HtmlInputText getTenantNumberType() {
		return tenantNumberType;
	}
	public void setTenantNumberType(HtmlInputText tenantNumberType) {
		this.tenantNumberType = tenantNumberType;
	}
	public HtmlInputText getContractStatusText() {
		return contractStatusText;
	}
	public void setContractStatusText(HtmlInputText contractStatusText) {
		this.contractStatusText = contractStatusText;
	}
	public HtmlInputText getTotalContractValText() {
		return totalContractValText;
	}
	public void setTotalContractValText(HtmlInputText totalContractValText) {
		this.totalContractValText = totalContractValText;
	}
	public HtmlInputText getTxtUnitType() {
		return txtUnitType;
	}
	public void setTxtUnitType(HtmlInputText txtUnitType) {
		this.txtUnitType = txtUnitType;
	}
	public HtmlInputText getTxtpropertyName() {
		return txtpropertyName;
	}
	public void setTxtpropertyName(HtmlInputText txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}
	public HtmlInputText getTxtunitRefNum() {
		return txtunitRefNum;
	}
	public void setTxtunitRefNum(HtmlInputText txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}
	public HtmlInputText getTxtUnitRentValue() {
		return txtUnitRentValue;
	}
	public void setTxtUnitRentValue(HtmlInputText txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}
	public HtmlInputText getTxtpropertyType() {
		return txtpropertyType;
	}
	public void setTxtpropertyType(HtmlInputText txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
	}
	
	
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	public HtmlCommandLink getPopulateContract() {
		return populateContract;
	}
	public void setPopulateContract(HtmlCommandLink populateContract) {
		this.populateContract = populateContract;
	}
	public String getHdnPersonId() {
		return hdnPersonId;
	}
	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}
	public String getHdnPersonName() {
		return hdnPersonName;
	}
	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}
	public String getHdnPersonType() {
		return hdnPersonType;
	}
	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}
	public String getHdnCellNo() {
		return hdnCellNo;
	}
	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}
	public String getHdnIsCompany() {
		return hdnIsCompany;
	}
	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentSchedules() 
	{
		if (viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
			paymentSchedules = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		return paymentSchedules;
	}
	@SuppressWarnings( "unchecked" )
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) 
	{
		this.paymentSchedules = paymentSchedules;
		if( this.paymentSchedules != null )
		viewMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, this.paymentSchedules );
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	public boolean isStatusIsNotSentForApprovalAndNotApproved() {
		return statusIsNotSentForApprovalAndNotApproved;
	}

	public void setStatusIsNotSentForApprovalAndNotApproved(
			boolean statusIsNotSentForApprovalAndNotApproved) {
		this.statusIsNotSentForApprovalAndNotApproved = statusIsNotSentForApprovalAndNotApproved;
	}
	@SuppressWarnings("unchecked")
	public String openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		viewMap.put("payments", viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		
		DomainDataView ddvExemptedStatus  = CommonUtil.getIdFromType(getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
		List<PaymentScheduleView> paymentSchedule = new ArrayList<PaymentScheduleView>();
		for (PaymentScheduleView payment : paymentSchedules) {
			if( payment.getStatusId().longValue() != ddvExemptedStatus.getDomainDataId().longValue() )
			paymentSchedule.add( payment );
		}
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentSchedule);
        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
        openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings("unchecked")
	public String deletePayment()
	{
		try
		{
			int rowID = dataTablePaymentSchedule.getRowIndex();
			paymentSchedules = getPaymentSchedules();
			paymentSchedules.remove(rowID);
			sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}
	public String getDateFormatForDataTable() {
		return dateFormatForDataTable;
	}

	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}
	@SuppressWarnings( "unchecked" )
	public List<BounceChequeView> getBounceChequeList() {
		if (viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL)!=null)
			bounceChequeList = (List<BounceChequeView>) viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL);
		return bounceChequeList;
	}

	public void setBounceChequeList(List<BounceChequeView> bounceChequeList) {
		this.bounceChequeList = bounceChequeList;
	}

	public HtmlDataTable getDataTableChequeDetail() {
		return dataTableChequeDetail;
	}

	public void setDataTableChequeDetail(HtmlDataTable dataTableChequeDetail) {
		this.dataTableChequeDetail = dataTableChequeDetail;
	}
	
    @SuppressWarnings("unchecked")
	public String replace_Payment()throws Exception,PimsBusinessException
	{
		
		Long statusId;
		
		Long payDetailId = 0L;
	    Boolean isStatusChange = false;
	    String recpNumber = "";
	    String chequeNumber= "";
	    Long paymentSchId = 0L;
	    int count = 0;
	    HashMap map  = new HashMap ();
		List<BounceChequeView> bouceChequeViewList = new ArrayList<BounceChequeView>(0);
		PropertyServiceAgent propertyServiceAgent =  new PropertyServiceAgent();
		String paymentDetailList = new String();
		paymentDetailList = "";
		
		DomainDataView ddv = CommonUtil.getIdFromType(
				                                       this.getPaymentScheduleStatus(),
				                                       WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED_WAITING_FOR_APPROVAL
				                                      );
		statusId= ddv.getDomainDataId();
		
			if(viewMap.containsKey(WebConstants.SESSION_FOR_CHEQUE_DETAIL))
			{
				
				bouceChequeViewList = (ArrayList<BounceChequeView>) viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL);
				
				for (BounceChequeView bCV : bouceChequeViewList) 
				{
					
					if (bCV.getSelected() != null) 
					{
						if (bCV.getSelected()) 
						{
					
					        paymentScheduleId = bCV.getPaymentScheduleId();
							recpNumber =  bCV.getReceiptNumber();
							chequeNumber = bCV.getChequeNumber();
							paymentSchId = bCV.getPaymentScheduleId();
							payDetailId  = bCV.getPaymentReceiptDetailId();
							
							if(count==0)
							paymentDetailList+=bCV.getPaymentReceiptDetailId().toString();
							else
							paymentDetailList+=","+bCV.getPaymentReceiptDetailId().toString();
							
							count++;
				         }
			       }      
	            }			
					ArrayList<Long> paymentDetailids = new ArrayList<Long>() ;
					
					if(paymentDetailList.contains(","))
					{
						String[] stringArray= paymentDetailList.split(",");
						for(int i=0;i<stringArray.length;i++)
						{
							paymentDetailids.add(Long.parseLong(stringArray[i]));
						}
					}
					else
						paymentDetailids.add(Long.parseLong(paymentDetailList));
				
				isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids, statusId);
				if(isStatusChange)
				{
					refrashChequeDetail(paymentDetailList);
					viewMap.put("PAYMENT_RECEIPT_DETAIL_STATUS_IDS",paymentDetailList);
					
					modeSend();
				}
		}
		return "replace_Payment";
	}
	
	//All Modes of Replace Cheque 
	private void modeInitiate(){
		setSelectChequeDetail(true);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(true);
		saveButton.setRendered(true);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(false);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		reviewBtn.setRendered(false);
        setEditPaymentEnable(true);
        
		
	}
	
	private void modeSend(){
		setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(true);
		saveButton.setRendered(true);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		reviewBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(false);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(true);
		
	}
	
   private void modeSendForApproval(){
		setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(false);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
	}

   @SuppressWarnings( "unchecked" )
   private void modeApproval(){
		setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(true);
		reviewBtn.setRendered(false);
		rejectBtn.setRendered(true);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(false);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
		viewMap.put( "canAddAttachment", true );
		viewMap.put( "canAddNote", true );
	}

   private void modeApprovedReject(){
		setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		reviewBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(false);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
	}
   @SuppressWarnings( "unchecked" )
   private void modeCollectReplaceCheque(){
		setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		reviewBtn.setRendered(false);
		cancelBtn.setRendered(true);
		btnCollectPayment.setRendered(true);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(true);
		viewMap.put( "canAddAttachment", true);
		viewMap.put( "canAddNote", true );
	}
   private void modeDisplay(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
	}
   private void modeCollectReplaceChequeFinish(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
		
		
	}
   @SuppressWarnings( "unchecked" )
   private void modeChequeReturn(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		reviewBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(true);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(true);
		setEditPaymentEnable(false);
		viewMap.put( "canAddAttachment", true);
		viewMap.put( "canAddNote", true );
		
	}
   private void modeChequeReturned()throws Exception
   {
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(true);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
		
	}
   private void modeChequeReturnedComplete(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		completeBtn.setRendered(false);
		reviewBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
		
	}
   @SuppressWarnings( "unchecked" )
   private void modeComplete(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(true);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
		viewMap.put( "canAddAttachment", true);
		viewMap.put( "canAddNote", true );
		
	}
   private void modeCompleteCancel(){
		
	    setSelectChequeDetail(false);
		replacePaymentButton.setRendered(false);
		sendBtn.setRendered(false);
		saveButton.setRendered(false);
		approvedBtn.setRendered(false);
		rejectBtn.setRendered(false);
		reviewBtn.setRendered(false);
		completeBtn.setRendered(false);
		cancelBtn.setRendered(false);
		btnCollectPayment.setRendered(false);
		setPaymentNumderCol(true);
		replacedBtn.setRendered(false);
		returedChequeBtn.setRendered(false);
		withdrawlReportBtn.setRendered(false);
		returedMarkBtn.setRendered(false);
		setEditPaymentEnable(false);
	}
   
	public HtmlCommandButton getReplacePaymentButton() {
		return replacePaymentButton;
	}

	public void setReplacePaymentButton(HtmlCommandButton replacePaymentButton) {
		this.replacePaymentButton = replacePaymentButton;
	}

	public Boolean getSelectChequeDetail() {
		if(viewMap.containsKey("CHEQUE_DETAIL_BOX"))
			selectChequeDetail = Boolean.parseBoolean(viewMap.get("CHEQUE_DETAIL_BOX").toString());
		return selectChequeDetail;
	}
	@SuppressWarnings( "unchecked" )
	public void setSelectChequeDetail(Boolean selectChequeDetail) {
		this.selectChequeDetail = selectChequeDetail;
		if(this.selectChequeDetail!=null)
			viewMap.put("CHEQUE_DETAIL_BOX", this.selectChequeDetail);
	}

	public HtmlCommandButton getSendBtn() {
		return sendBtn;
	}

	public void setSendBtn(HtmlCommandButton sendBtn) {
		this.sendBtn = sendBtn;
	}

	public HtmlCommandButton getApprovedBtn() {
		return approvedBtn;
	}

	public void setApprovedBtn(HtmlCommandButton approvedBtn) {
		this.approvedBtn = approvedBtn;
	}

	public HtmlCommandButton getRejectBtn() {
		return rejectBtn;
	}

	public void setRejectBtn(HtmlCommandButton rejectBtn) {
		this.rejectBtn = rejectBtn;
	}

	public HtmlCommandButton getCompleteBtn() {
		return completeBtn;
	}

	public void setCompleteBtn(HtmlCommandButton completeBtn) {
		this.completeBtn = completeBtn;
	}

	public HtmlCommandButton getCancelBtn() {
		return cancelBtn;
	}

	public void setCancelBtn(HtmlCommandButton cancelBtn) {
		this.cancelBtn = cancelBtn;
	}


	//check the procedure type before sending it for approval
	public String send()
	{
		String methodName="send|";
		try 
		{
		
			logger.logInfo(methodName+"Start|requestId:%s", this.requestId );
			errorMessages = new ArrayList<String>(0);
	        successMessages = new ArrayList<String>(0);
			loadAttachmentsAndComments(Long.parseLong(this.requestId));
			saveAttachments(Long.parseLong(this.requestId));
			saveComments(Long.parseLong(this.requestId));
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_CREATED, Long.parseLong(requestId));
			String procedureType="";
			if(getSelectedProcedureType().compareTo("1")==0)
				{
					PIMSReplaceChequeBPELPortClient pimsReplaceChequeBPELPortClient = new PIMSReplaceChequeBPELPortClient();
					String endPoint = parameters.getParameter(WebConstants.ReplaceCheque.REPLACE_CHEQUE_END_POINT);
					pimsReplaceChequeBPELPortClient.setEndpoint(endPoint);
					pimsReplaceChequeBPELPortClient.initiate(requestId.toString(),getLoggedInUser(), null,null);
					
				}
			else if(getSelectedProcedureType().compareTo("2")==0)
				{
					PIMSBounceChequeBPELPortClient pimsBounceChequeBPELPortClient= new PIMSBounceChequeBPELPortClient();
					String endPoint = parameters.getParameter(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_END_POINT);
					pimsBounceChequeBPELPortClient.setEndpoint(endPoint);
					pimsBounceChequeBPELPortClient.initiate(Long.parseLong(requestId),getLoggedInUser(), null,null);
					new RequestService().setRequestAsApprovalReq(Long.parseLong(this.requestId));
				} 
			else
				{
					PIMSReplaceChequeBPELPortClient pimsReplaceChequeBPELPortClient = new PIMSReplaceChequeBPELPortClient();
					String endPoint = parameters.getParameter(WebConstants.ReplaceCheque.REPLACE_CHEQUE_END_POINT);
					pimsReplaceChequeBPELPortClient.setEndpoint(endPoint);
					pimsReplaceChequeBPELPortClient.initiate(requestId.toString(),getLoggedInUser(), null,null);
				}
			CommonUtil.printReport(Long.parseLong(this.requestId));
			
			requestStatusChangeNotification(WebConstants.Notification_MetaEvents.Event_Request_Received);
			modeSendForApproval();
			
			successMessages.add(CommonUtil.getBundleMessage("replaceChaqueRequest.Send"));
			
			logger.logInfo(methodName+"Finish");
		} catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
        
		return "send";
	}
	@SuppressWarnings("unchecked")
	public String saveRequest()
	{
		
		try 
		{
	        successMessages = new ArrayList<String>(0);
			Long requestId = null;
			
			if(hdnPersonId!=null && !hdnPersonId.equals(""))
			{
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				 viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}

			if(hdnPersonName!=null && !hdnPersonName.equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			if(hdnCellNo!=null && !hdnCellNo.equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
			if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
			{
				DomainDataView ddv = new DomainDataView();
				if(Long.parseLong(hdnIsCompany)==1L)
				        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
				else
					ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);
				
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
			
			}
			//if no applicant is selected than we will not allow to save request
			if( validate() )
			{	
						
		           // this method mark status of the as Replace/waiting for approval
				   // bcoz only onces 
				    if(!viewMap.containsKey("REQUEST_VIEW_FOR_EDIT"))
					replace_Payment();
		            requestId =  addRequest();  						
					addPaymentSchedule(requestId);
					loadAttachmentsAndComments(requestId);
					saveAttachments(requestId);
					saveComments(requestId);
					this.requestId = requestId.toString();
					putRequestForEdit(requestId);
					requestView.setRequestId(requestId);
					successMessages.add(CommonUtil.getBundleMessage("replaceChaqueRequest.Save"));
					modeSend();
		     }	
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException("saveRequest|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			
			onProcessComplete();
		}
		
		return "success";
	}
	@SuppressWarnings( "unchecked" )
	 public Boolean	validate()throws Exception
	 {
		 
		    // if no applicant is selected than we will not allow to save request
			if(
				!viewMap.containsKey(WebConstants.LOCAL_PERSON_ID) && 
				!viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID)
			   )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
				viewMap.put("selectedTab",APPLICATION_TAB);
				return false;
			}
		return true;	
	 }	
	@SuppressWarnings( "unchecked" )
	private Long addRequest()throws Exception,PimsBusinessException
	{
		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
		PersonView applicantView  = new PersonView();
		RequestView requestViewForSend = new RequestView();
		HashSet<RequestFieldDetailView> reqFieldDetailsView = new HashSet<RequestFieldDetailView>();
		RequestFieldDetailView reqFieldFee = new RequestFieldDetailView();
		RequestFieldDetailView reqFieldDetailId = new RequestFieldDetailView();
		Long requestId = 0L;
		Long contractId = Long.parseLong(viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).toString());	
		List<RequestKeyView> requestKeys = requestServiceAgent.getRequestKeys(WebConstants.REPLACE_CHEQUE);
		if(viewMap.containsKey("REQUEST_VIEW_FOR_EDIT"))
		{
			RequestView reqViewEdit = 	(RequestView)viewMap.get("REQUEST_VIEW_FOR_EDIT");
			requestViewForSend.setRequestNumber(reqViewEdit.getRequestNumber());
			requestViewForSend.setRequestId(reqViewEdit.getRequestId());
			requestViewForSend.setCreatedBy(reqViewEdit.getCreatedBy());
			requestViewForSend.setCreatedOn(reqViewEdit.getCreatedOn());
			requestViewForSend.setUpdatedBy(getLoggedInUser());
			requestViewForSend.setUpdatedOn(new Date());
			requestViewForSend.setRecordStatus(reqViewEdit.getRecordStatus());
			requestViewForSend.setIsDeleted(reqViewEdit.getIsDeleted());
			requestViewForSend.setRequestDate(reqViewEdit.getRequestDate());
			requestViewForSend.setStatusId(reqViewEdit.getStatusId());
			requestViewForSend.setRequestTypeId(reqViewEdit.getRequestTypeId());
			requestViewForSend.setContractId(reqViewEdit.getContractId());
			requestViewForSend.setApplicantView(reqViewEdit.getApplicantView());
			requestViewForSend.setDescription(reqViewEdit.getDescription());
		}
		else
		{
			requestViewForSend.setCreatedBy(getLoggedInUser());
			requestViewForSend.setCreatedOn(new Date());
			requestViewForSend.setUpdatedBy(getLoggedInUser());
			requestViewForSend.setUpdatedOn(new Date());
			requestViewForSend.setRecordStatus(new Long(1));
			requestViewForSend.setIsDeleted(new Long(0));
			requestViewForSend.setRequestDate(new Date());
			requestViewForSend.setStatusId(getRequestStatus());
			requestViewForSend.setRequestTypeId(getRequestType());
			requestViewForSend.setContractId(contractId);
		    applicantView.setPersonId(Long.parseLong(viewMap.get(WebConstants.LOCAL_PERSON_ID).toString()));
		    requestViewForSend.setApplicantView(applicantView);
		    if(viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
		     requestViewForSend.setDescription((String)viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION));
		}
		if(viewMap.containsKey("PAYMENT_SCHEDULE_ID_FOR_EDIT"))
		{	
			RequestFieldDetailView requestFieldDetailView = (RequestFieldDetailView)viewMap.get("PAYMENT_SCHEDULE_ID_FOR_EDIT");
			reqFieldFee.setRequestFieldDetailViewId(requestFieldDetailView.getRequestFieldDetailViewId());
			reqFieldFee.setCreatedBy(requestFieldDetailView.getCreatedBy());
			reqFieldFee.setCreatedOn(requestFieldDetailView.getCreatedOn());
			reqFieldFee.setIsDeleted(requestFieldDetailView.getIsDeleted());
			reqFieldFee.setRecordStatus(requestFieldDetailView.getRecordStatus());
			reqFieldFee.setRequestKeyValue(requestFieldDetailView.getRequestKeyValue());
			reqFieldFee.setRequestKeyViewId(requestFieldDetailView.getRequestKeyViewId());
			reqFieldFee.setRequestKeyView(requestFieldDetailView.getRequestKeyView());
			reqFieldFee.setUpdatedBy(getLoggedInUser());
			reqFieldFee.setUpdatedOn(new Date());
			reqFieldDetailsView.add(reqFieldFee);
		}
		else
		{
			reqFieldFee.setCreatedBy(getLoggedInUser());
			reqFieldFee.setCreatedOn(new Date());
			reqFieldFee.setIsDeleted(new Long(0));
			reqFieldFee.setRecordStatus(new Long(1));
			reqFieldFee.setRequestKeyValue(viewMap.get("CONTRACT_NUMBER_ACTION").toString());
			reqFieldFee.setRequestKeyViewId(getRequestKeyViewId(requestKeys,WebConstants.OLD_PAYMENT_SCHEDULE_ID));
			reqFieldFee.setUpdatedBy(getLoggedInUser());
			reqFieldFee.setUpdatedOn(new Date());
			reqFieldDetailsView.add(reqFieldFee);
		}
		if(viewMap.containsKey("PAYMENT_DETAIL_STATUS_FOR_EDIT"))
		{	
			RequestFieldDetailView requestFieldDetailView = (RequestFieldDetailView)viewMap.get("PAYMENT_DETAIL_STATUS_FOR_EDIT");
			reqFieldDetailId.setRequestFieldDetailViewId(requestFieldDetailView.getRequestFieldDetailViewId());
			reqFieldDetailId.setCreatedBy(requestFieldDetailView.getCreatedBy());
			reqFieldDetailId.setCreatedOn(requestFieldDetailView.getCreatedOn());
			reqFieldDetailId.setIsDeleted(requestFieldDetailView.getIsDeleted());
			reqFieldDetailId.setRecordStatus(requestFieldDetailView.getRecordStatus());
			reqFieldDetailId.setRequestKeyValue(requestFieldDetailView.getRequestKeyValue());
			reqFieldDetailId.setRequestKeyViewId(requestFieldDetailView.getRequestKeyViewId());
			reqFieldDetailId.setRequestKeyView(requestFieldDetailView.getRequestKeyView());
			reqFieldDetailId.setUpdatedBy(getLoggedInUser());
			reqFieldDetailId.setUpdatedOn(new Date());
			reqFieldDetailsView.add(reqFieldDetailId);
		}
		else
		{
			reqFieldDetailId.setCreatedBy(getLoggedInUser());
			reqFieldDetailId.setCreatedOn(new Date());
			reqFieldDetailId.setIsDeleted(new Long(0));
			reqFieldDetailId.setRecordStatus(new Long(1));
			reqFieldDetailId.setRequestKeyValue(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
			reqFieldDetailId.setRequestKeyViewId(getRequestKeyViewId(requestKeys,WebConstants.ReplaceCheque.PAYMENT_RECEIPT_DETAIL_ID));
			reqFieldDetailId.setUpdatedBy(getLoggedInUser());
			reqFieldDetailId.setUpdatedOn(new Date());
			reqFieldDetailsView.add(reqFieldDetailId);
		}
		requestViewForSend.setRequestFieldDetailsView(reqFieldDetailsView);
		requestId = requestServiceAgent.addRequest(requestViewForSend);
		viewMap.put("requestViewForSend", requestViewForSend);
		return requestId;
	}
	@SuppressWarnings( "unchecked" )
	public void putRequestForEdit( Long requestId)throws Exception,PimsBusinessException
	{
	   
		    RequestFieldDetailView requestFieldDetailViewNew = new RequestFieldDetailView();
 		    RequestView requestView = new RequestView();
			this.requestId=requestId.toString();
			populateApplicationDataTab(this.requestId);
			requestView.setRequestId(requestId);
			List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
			requestKeys = new RequestServiceAgent().getRequestKeys(WebConstants.REPLACE_CHEQUE);
			if(requestList.size()>0)
			{
				requestView = (RequestView)requestList.iterator().next();
				viewMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
			
				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.ReplaceCheque.PAYMENT_RECEIPT_DETAIL_ID));
				viewMap.put("PAYMENT_RECEIPT_DETAIL_STATUS_IDS",requestFieldDetailViewNew.getRequestKeyValue());
				refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
				viewMap.put("PAYMENT_DETAIL_STATUS_FOR_EDIT", requestFieldDetailViewNew);
				
				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.OLD_PAYMENT_SCHEDULE_ID));
				String contractNumber = requestFieldDetailViewNew.getRequestKeyValue().toString();
				viewMap.put("PAYMENT_SCHEDULE_ID_FOR_EDIT", requestFieldDetailViewNew);
	
	            fillValues(contractNumber);
				contractView = propertyServiceAgent.getContract(contractNumber, "dd/MM/yyyy");
				getContractById(contractView.getContractId().toString());
				
				paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
				if(paymentSchedules!=null && paymentSchedules.size()>0)
				{
				 viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
				 viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
				 viewMap.put(WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules);
				}
				else
				 viewMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
				getPaymentSchedules();
				getBounceChequeList();
			}
			ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
		    CommonUtil.loadAttachmentsAndComments(this.requestId);
	}
	@SuppressWarnings( "unchecked" )
	private Long getRequestStatus() throws PimsBusinessException 
	{
		Long cleranceLetterRequestStatusId = null;
		HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
				WebConstants.REQUEST_STATUS_NEW);
		if(hMap.containsKey("returnId")){
			cleranceLetterRequestStatusId = Long.parseLong(hMap.get("returnId").toString());
			if(cleranceLetterRequestStatusId.equals(null))
				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		}
		return cleranceLetterRequestStatusId;
	}
	
	private Long getRequestKeyViewId(List<RequestKeyView> keys, String requestKeyReason) throws PimsBusinessException{
		Long requestKeyViewId = new Long(0);
		String  METHOD_NAME = "getRequestKeyViewId|";		
		logger.logInfo(METHOD_NAME+"started...");
		for(RequestKeyView req: keys)
		{
			if(req.getKeyName().equals(requestKeyReason))
			{
				requestKeyViewId = req.getRequestKeyId();
				break;
		     }
		}
		if( requestKeyViewId.equals( new Long( 0 ) ) )
			throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		logger.logInfo(METHOD_NAME+"ended...");
		return requestKeyViewId;
	}
	@SuppressWarnings( "unchecked" )
	public void populateApplicationDataTab(String requestId)
	{
		
		RequestView rV= new RequestView();
		List<RequestView> rVList = new ArrayList<RequestView>();
		try 
		{
			rV.setRequestId(new Long(requestId));
			PropertyServiceAgent psa = new PropertyServiceAgent();
		    rVList = psa.getAllRequests(rV, null, null, null);
			rV = rVList.get(0);
			if(rV.getRequestNumber()!=null && !rV.getRequestNumber().equals(""))
			   viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, rV.getRequestNumber());
			if(rV.getStatusId().toString()!=null && !rV.getStatusId().toString().equals(""))
			   viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?rV.getStatusEn():rV.getStatusAr());
			if(rV.getRequestDate()!=null)
			   viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, rV.getRequestDate());
			if(rV.getDescription()!=null && !rV.getDescription().equals(""))
			   viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, rV.getDescription());
			if(rV.getRequestId()!=null && !rV.getRequestId().equals(""))
			   viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,rV.getRequestId());
			
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
	
			if(rV.getApplicantView()!=null && !rV.getApplicantView().equals(""))
			{
				viewMap.put("APPLICANT_VIEW_FOR_NOTIFICATION", rV.getApplicantView());
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals(""))
				      viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,rV.getApplicantView().getPersonId());
		
				if(rV.getApplicantView().getFirstName()!=null && !rV.getApplicantView().getFirstName().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
							(rV.getApplicantView().getFirstName()!=null && rV.getApplicantView().getFirstName().length()>0? 
									                                                            rV.getApplicantView().getFirstName():"")+" "+
			                (rV.getApplicantView().getMiddleName()!=null && rV.getApplicantView().getMiddleName().length()>0?
			                		                                                            rV.getApplicantView().getMiddleName():"")+" "+
				            (rV.getApplicantView().getLastName()!=null && rV.getApplicantView().getLastName().length()>0?
				            		                                                            rV.getApplicantView().getLastName():"")
				            		  );
				
				if(rV.getApplicantView().getPersonTypeId()!=null && !rV.getApplicantView().getPersonTypeId().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
							getIsEnglishLocale()?rV.getApplicantView().getPersonTypeEn():rV.getApplicantView().getPersonTypeAr());
				if(rV.getApplicantView().getCellNumber()!=null && !rV.getApplicantView().getCellNumber().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,rV.getApplicantView().getCellNumber());
				
				if(rV.getApplicantView().getIsCompany()!=null && !rV.getApplicantView().getIsCompany().equals(""))
				{
					DomainDataView ddv = new DomainDataView();
					List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
					if(rV.getApplicantView().getIsCompany()==1L)
					        ddv=  CommonUtil.getIdFromType( ddvList , WebConstants.TENANT_TYPE_COMPANY);
					else
					        ddv= CommonUtil.getIdFromType(ddvList , WebConstants.TENANT_TYPE_INDIVIDUAL);
				    viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}
		  }
		
	  } 
		catch (PimsBusinessException e)
	  {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public String approve()
	{
		
		String  METHOD_NAME = "approve|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
        Boolean isStatusChange;
      	try
      	{
        	Map viewRootMap = viewMap;
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			//generateNotification(WebConstants.Event_REPLACECHEQUE_REQUESTAPPROVED);
			this.requestId  = requestId.toString();
//      		Long requestId = new Long(this.requestId);
			addNewPaymentSchedules(requestId);
			RequestServiceAgent rSAgent = new RequestServiceAgent();
			rSAgent.setRequestAsApproved(requestId, getLoggedInUser());
			loadAttachmentsAndComments(requestId);
			saveAttachments(requestId);
			saveComments(requestId);
			DomainDataView ddv = CommonUtil.getIdFromType(
															this.getPaymentScheduleStatus(),
															WebConstants.PAYMENT_SCHEDULE_STATUS_REPLACED_APPROVAL
														 );
			
			ArrayList<Long> paymentDetailids = getPaymentDetailIdsFromView();
			
			isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids, ddv.getDomainDataId());
			
			if(isStatusChange)
				refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());

			client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_APPROVED, requestId);
			
			if(getSelectedProcedureType().compareTo("1")==0 || getSelectedProcedureType().compareTo("0")==0)
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Approved));
			else if(getSelectedProcedureType().compareTo("2")==0)
				successMessages.add(CommonUtil.getBundleMessage("replaceBouncedCheque.successMsg.Approved"));
				
			requestStatusChangeNotification(WebConstants.Notification_MetaEvents.Event_Request_Approved);
			logger.logInfo(METHOD_NAME+" completed successfully...");
        	modeApprovedReject();
		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_APPROVED));
		
		}
        catch (Exception e) 
        {
			logger.LogException(METHOD_NAME + "crashed...", e);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_APPROVED));
		}
        
		return METHOD_NAME;
	}
	@SuppressWarnings( "unchecked" )
	private ArrayList<Long> getPaymentDetailIdsFromView() 
	{
		String paymentDetailList = viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString();

		ArrayList<Long> paymentDetailids = new ArrayList<Long>() ;
						
		if(paymentDetailList.contains(","))
		{
			String[] stringArray= paymentDetailList.split(",");
			for(int i=0;i<stringArray.length;i++)
			{
			 paymentDetailids.add(Long.parseLong(stringArray[i]));
			}
		}
		else
		    paymentDetailids.add(Long.parseLong(paymentDetailList));
		return paymentDetailids;
	}
	
	@SuppressWarnings( "unchecked" )
    public String reject()
	{
		
		String  METHOD_NAME = "reject|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
        Boolean isStatusChange;
        logger.logInfo(METHOD_NAME+" started...");
        try 
        {
        	Map viewRootMap = viewMap;
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			//for sending notification to the Applicant 
			generateNotification(WebConstants.Notification_MetaEvents.Event_REPLACECHEQUE_REQUESTREJECTED);
			this.requestId  = requestId.toString();
			RequestServiceAgent rSAgent = new RequestServiceAgent();
			rSAgent.setRequestAsRejected(requestId, getLoggedInUser());
			new PropertyService().setPaymentScheduleCanceled(requestId);
			loadAttachmentsAndComments(requestId);
			saveAttachments(requestId);
			saveComments(requestId);
			DomainDataView ddv = CommonUtil.getIdFromType(
															this.getPaymentScheduleStatus(),
															WebConstants.PAYMENT_SCHEDULE_COLLECTED
														);
			DomainDataView ddvBounced = CommonUtil.getIdFromType(
					this.getPaymentScheduleStatus(),
					WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
			
			ArrayList<Long> paymentDetailids = getPaymentDetailIdsFromView();
			if(getSelectedProcedureType().compareTo("2")==0)
				isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids,ddvBounced.getDomainDataId());
			else
				isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids,ddv.getDomainDataId());
			if(isStatusChange)
				refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
			client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_REJECTED, requestId);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Reject));
        	logger.logInfo(METHOD_NAME+" completed successfully...");
        	modeApprovedReject();
		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException(METHOD_NAME + "crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_REJECT));
		
		}
        catch (Exception e) 
        {
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_REJECT));
		}
        
		return METHOD_NAME;
	}
   @SuppressWarnings( "unchecked" )
   public String complete()
   {
	
	String  METHOD_NAME = "complete|";
	errorMessages = new ArrayList<String>(0);
    successMessages = new ArrayList<String>(0);
    logger.logInfo(METHOD_NAME+" started...");
    try 
    {
    	Map viewRootMap = viewMap;
    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
        logger.logInfo("Contextpath is:"+contextPath);
        String loggedInUser = getLoggedInUser();
		BPMWorklistClient client = new BPMWorklistClient(contextPath);			
		Map taskAttributes =  userTask.getTaskAttributes();
		Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
		this.requestId  = requestId.toString();
//		RequestServiceAgent rSAgent = new RequestServiceAgent();
//		rSAgent.setRequestAsCompleted(requestId, getLoggedInUser());
		markChequesAsRecieveReturnedAndRequestComplete();
		loadAttachmentsAndComments(requestId);
		saveAttachments(requestId);
		saveComments(requestId);
		client.completeTask(userTask, loggedInUser, TaskOutcome.OK);
		NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Complete));
    	logger.logInfo(METHOD_NAME+" completed successfully...");
    	requestStatusChangeNotification(WebConstants.Notification_MetaEvents.Event_Request_Completed);
    	modeCompleteCancel();
	} 
	catch(PIMSWorkListException ex)
	{
		logger.LogException(METHOD_NAME + "crashed...", ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_COMPLETE));
		ex.printStackTrace();
	
	}
    catch (Exception e) 
    {
		logger.LogException(METHOD_NAME + "crashed...", e);
		
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_COMPLETE));
	}
    
	return METHOD_NAME;
}
	@SuppressWarnings( "unchecked" )
public String replaceCancel(){
	
	String  METHOD_NAME = "cancel|";
	errorMessages = new ArrayList<String>(0);
    successMessages = new ArrayList<String>(0);
    PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
    Boolean isStatusChange;
    logger.logInfo(METHOD_NAME+" started...");
    try {
    	Map viewRootMap = viewMap;
    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
        logger.logInfo("Contextpath is:"+contextPath);
        String loggedInUser = getLoggedInUser();
		BPMWorklistClient client = new BPMWorklistClient(contextPath);			
		Map taskAttributes =  userTask.getTaskAttributes();
		Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
		this.requestId  = requestId.toString();
		RequestServiceAgent rSAgent = new RequestServiceAgent();
		rSAgent.setRequestAsCanceled(requestId, getLoggedInUser());
		DomainDataView ddv = CommonUtil.getIdFromType(
													    this.getPaymentScheduleStatus(),
														WebConstants.PAYMENT_SCHEDULE_COLLECTED
													 );
		DomainDataView ddvBounced = CommonUtil.getIdFromType(
				this.getPaymentScheduleStatus(),
				WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
		
		ArrayList<Long> paymentDetailids = getPaymentDetailIdsFromView();
		if(getSelectedProcedureType().compareTo("2")==0)
			isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids,ddvBounced.getDomainDataId());
		else
			isStatusChange = propertyServiceAgent.changePaymentReceiptDetailStatus(paymentDetailids,ddv.getDomainDataId());
		
	    if(isStatusChange)
				refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
		
	    new PropertyService().setPaymentScheduleCanceled(requestId);
		loadAttachmentsAndComments(requestId);
		saveAttachments(requestId);
		saveComments(requestId);
		client.completeTask(userTask, loggedInUser, TaskOutcome.CANCEL);
		NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_CANCELED, requestId);
		successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Cancel));
    	logger.logInfo(METHOD_NAME+" completed successfully...");
    	modeCollectReplaceChequeFinish();
	} 
	catch(PIMSWorkListException ex)
	{
		logger.LogException(METHOD_NAME + "crashed...", ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_CANCEL));
	
	}
    catch (Exception e) 
    {
		logger.LogException(METHOD_NAME + "crashed...", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_CANCEL));
	}
    
	return METHOD_NAME;
}
@SuppressWarnings( "unchecked" )
public String replace()
{
	
	errorMessages = new ArrayList<String>(0);
    successMessages = new ArrayList<String>(0);
    try
    {
	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			loadAttachmentsAndComments(requestId);
			List<Long> paymentDetailids = getPaymentDetailIdsFromView();

			new PropertyService().replaceCheque(requestId, paymentDetailids, getLoggedInUser());

			client.completeTask(userTask, loggedInUser, TaskOutcome.COLLECT);
			
			refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_COLLECT, requestId);
	    	modeCollectReplaceChequeFinish();
	    	viewMap.put("selectedTab",CHEQUE_DETAIL_TAB);
	} 
    catch (Exception e) 
    {
		logger.LogException( "replace|crashed...", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_REPLACED));
	}
    
	return "replace|";
}
	@SuppressWarnings( "unchecked" )
	public String returned(){
	      
	      String  METHOD_NAME = "returned|";
	      errorMessages = new ArrayList<String>(0);
	    successMessages = new ArrayList<String>(0);
	    logger.logInfo(METHOD_NAME+" started...");
	     
	    
	    try {
	      Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	      
	         cheque_Returned();
	      //if payment received than allow to replace 
	      if(viewRootMap.containsKey(WebConstants.RETURN_CHEQUE_DONE))
	      //if(true)
	      {
	      
	            UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	            String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;           
	              logger.logInfo("Contextpath is:"+contextPath);
	              String loggedInUser = getLoggedInUser();
	                  BPMWorklistClient client = new BPMWorklistClient(contextPath);                
	                  Map taskAttributes =  userTask.getTaskAttributes();
	                  Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
	                  this.requestId  = requestId.toString();
	                  loadAttachmentsAndComments(requestId);
	                  saveAttachments(requestId);
	                  saveComments(requestId);
	                  RequestServiceAgent rSAgent = new RequestServiceAgent();
	                  rSAgent.setRequestAsOldChequeReturned(Long.parseLong(this.requestId), getLoggedInUser());
	                  if( viewMap.get( BOUNCED_CHEQUES_PRD_IDS )!=null )
	                  {
	                        logger.logInfo(METHOD_NAME+" closing followUps...");
	                        new FinanceService().closeFollowUpsFromReplaceCheque( (ArrayList<Long>)viewMap.get( BOUNCED_CHEQUES_PRD_IDS ) , getLoggedInUser(), "");
	                        
	                  }
	                  client.completeTask(userTask, loggedInUser, TaskOutcome.OK);
	                  NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_RETURN, requestId);
	                  if(viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK) != null)
	                	  viewMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK);
	                  successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_RETURNED));
	                  if(getSelectedProcedureType() !=null && getSelectedProcedureType().compareTo("2")==0)
	                  {
	                        NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
	                      successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Complete));
	                        completeBtn.setRendered(false);
	                  }
	                                    
	                  logger.logInfo(METHOD_NAME+" completed successfully...");
	            modeChequeReturnedComplete();
	      }
	      else
	      {
	            errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_REPLACED_NOT_RETURN));
	            return METHOD_NAME;
	      }
	      } 
	      catch(PIMSWorkListException ex)
	      {
	            logger.LogException("Failed to approve due to:",ex);
	            errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_RETURNED));
	      
	      }
	    catch (Exception e) 
	    {
	            logger.LogException(METHOD_NAME + "crashed...", e);
	            errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_FAILED_RETURNED));
	      }
	    
	      return METHOD_NAME;
	}


	@SuppressWarnings( "unchecked" )
public String cheque_Returned()
	{
	
	String methodName="cheque_Returned|";
	logger.logInfo(methodName +"Start");
    Boolean isStatusChange = false;
	PropertyServiceAgent propertyServiceAgent =  new PropertyServiceAgent();
	try
	{
		boolean procTypeBounced =true;
		ArrayList<Long> paymentDetailids = getPaymentDetailIdsFromView();
		
		
		if( viewMap.get("PROCEDURE_TYPE")!= null  )
		{
			logger.logInfo(methodName +"Procedure Type:%s", viewMap.get("PROCEDURE_TYPE").toString());
			if ( viewMap.get("PROCEDURE_TYPE").toString().equals("COLLECTED") )
			procTypeBounced =false;
		}
		
		isStatusChange = propertyServiceAgent.changePaymentScheduleStatusForReplaceCheque(paymentDetailids,procTypeBounced );
		if(isStatusChange)
		{
			modeChequeReturned();
			viewMap.put(WebConstants.RETURN_CHEQUE_DONE, true);
			refrashChequeDetail(viewMap.get("PAYMENT_RECEIPT_DETAIL_STATUS_IDS").toString());
		}
		logger.logInfo(methodName +"Finish");
	} 
	catch (PimsBusinessException e) 
	{
		errorMessages = new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	catch (Exception e) 
	{
		errorMessages = new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	
	
	
	
	
	return "replace_Payment";
}
	@SuppressWarnings( "unchecked" )
	private Boolean addPaymentSchedule(Long requestId)  throws Exception
	{
		String  METHOD_NAME = "addPaymentSchedule|";
		List<PaymentScheduleView> paymentSchViewListForFee = new ArrayList<PaymentScheduleView>(0);
		PaymentScheduleView paymentScheduleViewForEdit = new PaymentScheduleView();
		PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
    	PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		ContractView contView = new ContractView();
		
		logger.logInfo(METHOD_NAME+" started...");
        try 
        {
    		   if(viewMap.containsKey("NO_PAYMENT_SCH_FOR_EDIT"))
    			   return true;
    		   if(viewMap.containsKey( "PAYMENT_SCH_FOR_EDIT" ) )
    		   {
    				    List<PaymentScheduleView> paymentScheduleViewList = (List<PaymentScheduleView>)viewMap.get("PAYMENT_SCH_FOR_EDIT");
    				    if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0)
    				       paymentScheduleViewForEdit = paymentScheduleViewList.get(0);
    				    paymentScheduleView.setPaymentScheduleId(paymentScheduleViewForEdit.getPaymentScheduleId());
    				    paymentScheduleView.setPaymentNumber(paymentScheduleViewForEdit.getPaymentNumber());
    				    paymentScheduleView.setContractId(paymentScheduleViewForEdit.getContractId());
    				    paymentScheduleView.setAmount(paymentScheduleViewForEdit.getAmount());
    				    paymentScheduleView.setIsSystem( paymentScheduleViewForEdit.getIsSystem() );
    					paymentScheduleView.setTypeId(paymentScheduleViewForEdit.getTypeId());
    					paymentScheduleView.setPaymentDueOn(paymentScheduleViewForEdit.getPaymentDueOn());
    					paymentScheduleView.setPaymentModeId(paymentScheduleViewForEdit.getPaymentModeId());
    					paymentScheduleView.setCreatedBy(paymentScheduleViewForEdit.getCreatedBy());
    					paymentScheduleView.setCreatedOn(paymentScheduleViewForEdit.getCreatedOn());
    					paymentScheduleView.setUpdatedBy(getLoggedInUser());
    					paymentScheduleView.setUpdatedOn(new Date());
    					paymentScheduleView.setIsDeleted(paymentScheduleViewForEdit.getIsDeleted());
    					paymentScheduleView.setRecordStatus(paymentScheduleViewForEdit.getRecordStatus());
    					paymentScheduleView.setIsReceived(paymentScheduleViewForEdit.getIsReceived());
    					paymentScheduleView.setComments( paymentScheduleViewForEdit.getComments() );
    					
    					if( viewMap.containsKey( "CANCEL_PAYMENT_SCH" ) )
    					  paymentScheduleView.setStatusId( getPaymentScheduleCancelStatusId() );
    					else
    					  paymentScheduleView.setStatusId(  paymentScheduleViewForEdit.getStatusId() );
    					paymentScheduleView.setRequestId(   paymentScheduleViewForEdit.getRequestId() ) ;
    					paymentScheduleView.setDescription( paymentScheduleViewForEdit.getDescription() );
    					paymentScheduleView.setOwnerShipTypeId( paymentScheduleViewForEdit.getOwnerShipTypeId() );
    					pSAgent.addPaymentScheduleByPaymentSchedule( paymentScheduleView );
    					logger.logInfo(METHOD_NAME+" completed successfully...");
    					return true;
    			}
    			else
    			{
    				
    				//In case befor save the person has changed the default payment status to exempted
//    				if( viewMap.containsKey(  "DEFAULT_PAYMENT_EDITED" ) )
//    				{
//    					List<PaymentScheduleView> paymentScheduleViewList = (List<PaymentScheduleView>)viewMap.get("DEFAULT_PAYMENT_EDITED");
//    				    if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0)
//    				    {
//    				    	paymentScheduleView = paymentScheduleViewList.get(0);
//    				    }
//    				}
//    				else
//    				{
//		        	     paymentScheduleView = new PaymentScheduleView();
//		        	     paymentScheduleView.setStatusId( getPaymentSchedulePendingStatusId() );
//    				}
		        	if( 
		        		viewMap.containsKey( WebConstants.REPLACE_CHEQUE_FEE_AMOUNT ) 
		        	  )
		        	{
//		        		paymentScheduleView = new PaymentScheduleView();
//		        	    paymentScheduleView.setStatusId( getPaymentSchedulePendingStatusId() );
		        		contView = ( ContractView ) viewMap.get( "CONTRACT_VIEW_FOR_PAYMENT" );
			        	paymentSchViewListForFee = (List<PaymentScheduleView>)viewMap.get( WebConstants.REPLACE_CHEQUE_FEE_AMOUNT );
			            Long ownerShipTypeId  =contView.getContractUnitView().iterator().next().getUnitView().getPropertyCategoryId();
			        	for (PaymentScheduleView systemPayment : paymentSchViewListForFee) 
			        	{
			        		systemPayment.setContractId( contView.getContractId() );
			        		systemPayment.setCreatedBy( getLoggedInUser() );
			        		systemPayment.setCreatedOn( new Date() );
			        		systemPayment.setUpdatedBy( getLoggedInUser() );
			        		systemPayment.setUpdatedOn( new Date() );
			        		systemPayment.setIsDeleted( new Long( 0 ) );
			        		systemPayment.setRecordStatus( new Long( 1 ) );
			        		systemPayment.setIsReceived( "N" );
			        		systemPayment.setRequestId( requestId );
			        		systemPayment.setIsSystem( 1L );
			        		systemPayment.setOwnerShipTypeId( ownerShipTypeId );
//				        	paymentScheduleView.setTypeId( systemPayment.getTypeId() );
//				        	paymentScheduleView.setPaymentDueOn( new Date() );
//				        	paymentScheduleView.setPaymentModeId( system.getPaymento );
//				          	paymentScheduleView.setStatusId( getReplaceChequePaymentScheduleModeForFeeId() );
//				        	paymentScheduleView.setAmount( systemPayment.getAmount() );
//				        	paymentScheduleView.setDescription( systemPayment.getDescription() );	
				        	
				        	pSAgent.addPaymentScheduleByPaymentSchedule( systemPayment );
			        	}
		
		            }
		        	logger.logInfo(METHOD_NAME+" completed successfully...");
		        	return true;
        	}
        		
		}
        catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void addNewPaymentSchedules(Long requestId)throws Exception
	{
		List<PaymentScheduleView> paymentSchViewList = new ArrayList<PaymentScheduleView>(0);
    	PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		ContractView contView = new ContractView();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			if(viewMap.containsKey("CONTRACT_VIEW_FOR_PAYMENT"))
	    		contView =(ContractView)viewMap.get("CONTRACT_VIEW_FOR_PAYMENT");
	    	
			ArrayList<Long> paymentDetailids = getPaymentDetailIdsFromView();    	
	    	paymentSchViewList = propertyServiceAgent.getSummedPaymentScheduleByPaymentReceiptDetailIds(paymentDetailids);
	    	
	    	for(PaymentScheduleView paySchView:paymentSchViewList)
	    	{
	    		
	        		if(contView.getContractId()!=null)
	        		   paySchView.setContractId(contView.getContractId());
	        		
	        		paySchView.setTypeId(paySchView.getTypeId());
	        		if( df.parse( df.format( new Date() ) ).compareTo( df.parse( df.format( paySchView.getPaymentDueOn() ) ) ) >=0 )
	        		   paySchView.setPaymentDueOn( df.parse( df.format(  new Date() ) ) );
	        		else
	        			paySchView.setPaymentDueOn( df.parse( df.format( paySchView.getPaymentDueOn() ) ) );
	        		paySchView.setPaymentModeId(getReplaceChequePaymentScheduleModeId());
	        		paySchView.setAmount(paySchView.getAmount());
	        		paySchView.setCreatedBy(getLoggedInUser());
	        		paySchView.setCreatedOn(new Date());
	        		paySchView.setUpdatedBy(getLoggedInUser());
	        		paySchView.setUpdatedOn(new Date());
	        		paySchView.setIsDeleted(new Long(0));
	        		paySchView.setRecordStatus(new Long(1));
	        		paySchView.setIsReceived("N");
	        		paySchView.setStatusId(getPaymentSchedulePendingStatusId());
	        		paySchView.setRequestId(requestId);
	        		paySchView.setDescription(paySchView.getDescription());
	        		paySchView.setOldPaymentScheduleId(paySchView.getPaymentScheduleId().toString());
	        		paySchView.setPaymentScheduleId(null);
	        		paySchView.setMigrated( 0L );
	        		if(contView.getContractUnitView() != null && contView.getContractUnitView().size() > 0 )
	        		{
	        			paySchView.setOwnerShipTypeId(contView.getContractUnitView().iterator().next().getUnitView().getPropertyCategoryId());
	        		}
	        		else if ( contView.getContractTypeId().longValue() == 32008l || 
	        				  contView.getContractTypeId().longValue() == 32009l 
	        				)
	        		{
	        			
	        			Contract contract = EntityManager.getBroker().findById(Contract.class, contView.getContractId() );
	        			paySchView.setOwnerShipTypeId(contract.getProperty().getCategoryId());
	        		}
	        		
	        	    pSAgent.addPaymentScheduleByPaymentSchedule(paySchView);
            }
		
	}
	@SuppressWarnings( "unchecked" )	
	private Long getPaymentScheduleCancelStatusId() 
	{
		Long statusId = new Long(0);
		String methodName="getPaymentSchedulePendingStatusId|";
		logger.logDebug(methodName+"started..");
		try
		{
        	HashMap hMap= getIdFromType(
					        			   this.getPaymentScheduleStatus(),
					        		       WebConstants.CANCELED
					        		    );
       		if(hMap.containsKey("returnId")){
       			statusId = Long.parseLong(hMap.get("returnId").toString());
       			if(statusId.equals(null))
       				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
       		}
			
			logger.logDebug(methodName+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		return statusId;
	}
	@SuppressWarnings( "unchecked" )
	private Long getReplaceChequePaymentScheduleModeId() throws Exception{
		Long typeId = new Long(0);
		String  METHOD_NAME = "getReplaceChequePaymentScheduleModeId|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
      		       WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
     		if(hMap.containsKey("returnId")){
     			typeId = Long.parseLong(hMap.get("returnId").toString());
     			if(typeId.equals(null))
     				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
     		}
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
		return typeId;
	}
	@SuppressWarnings( "unchecked" )
	private Long getReplaceChequePaymentScheduleModeForFeeId() throws Exception{
		Long typeId = new Long(0);
		String  METHOD_NAME = "getReplaceChequePaymentScheduleModeForFeeId|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
      		       WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
     		if(hMap.containsKey("returnId")){
     			typeId = Long.parseLong(hMap.get("returnId").toString());
     			if(typeId.equals(null))
     				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
     		}
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
		return typeId;
	}
	@SuppressWarnings( "unchecked" )
	private Long getPaymentSchedulePendingStatusId() {
		Long statusId = new Long(0);
		String methodName="getPaymentSchedulePendingStatusId|";
		logger.logDebug(methodName+"started..");
		try{
        	HashMap hMap= getIdFromType(
					        			   this.getPaymentScheduleStatus(),
					        		       WebConstants.PAYMENT_SCHEDULE_PENDING
					        		    );
       		if(hMap.containsKey("returnId")){
       			statusId = Long.parseLong(hMap.get("returnId").toString());
       			if(statusId.equals(null))
       				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
       		}
			
			logger.logDebug(methodName+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		return statusId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}



	public Boolean getPaymentNumderCol() {
		if(viewMap.containsKey("PAYMENT_NUMBER"))
			paymentNumderCol=(Boolean)viewMap.get("PAYMENT_NUMBER");
		return paymentNumderCol;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setPaymentNumderCol(Boolean paymentNumderCol) {
		this.paymentNumderCol = paymentNumderCol;
		if(this.paymentNumderCol !=null)
			viewMap.put("PAYMENT_NUMBER",this.paymentNumderCol );
		
	}

	public HtmlCommandButton getReplacedBtn() {
		return replacedBtn;
	}

	public void setReplacedBtn(HtmlCommandButton replacedBtn) {
		this.replacedBtn = replacedBtn;
	}

	public HtmlCommandButton getReturedChequeBtn() {
		return returedChequeBtn;
	}

	public void setReturedChequeBtn(HtmlCommandButton returedChequeBtn) {
		this.returedChequeBtn = returedChequeBtn;
	}

	public HtmlCommandButton getReturedMarkBtn() {
		return returedMarkBtn;
	}

	public void setReturedMarkBtn(HtmlCommandButton returedMarkBtn) {
		this.returedMarkBtn = returedMarkBtn;
	}
	@SuppressWarnings( "unchecked" )
	private void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			ContractView contract=new ContractView();
			contract.setContractId(new Long(contractId));
			HashMap contractHashMap=new HashMap();
			contractHashMap.put("contractView",contract);
			contractHashMap.put("dateFormat",getDateFormat());
			PropertyServiceAgent psa =new PropertyServiceAgent();
			logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
			List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
			logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
			if(contractViewList.size()>0)
			contractView=(ContractView)contractViewList.get(0);
			viewMap.put("CONTRACT_VIEW_FOR_PAYMENT",contractView);
	}
	
	@SuppressWarnings( "unchecked" )
	public void generateNotification(String eventType)
	{
		   String methodName ="generateNotification";
		            try
		            {
		            	logger.logInfo(methodName+"|Start");
		            	PersonView personView = new PersonView();
		            	ContractView contractView = new ContractView();
		                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
		                NotificationFactory nsfactory = NotificationFactory.getInstance();
		                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
		                Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
		        		
		            	if (viewMap.containsKey("APPLICANT_VIEW_FOR_NOTIFICATION")
		            		&&viewMap.containsKey("CONTRACT_VIEW_FOR_PAYMENT")) 
		            	{
		            		personView = (PersonView)viewMap.get("APPLICANT_VIEW_FOR_NOTIFICATION");
		            		contractView = (ContractView)viewMap.get("CONTRACT_VIEW_FOR_PAYMENT");
		            		getNotificationPlaceHolder(event,contractView,personView);
		            		personsEmailList = getEmailContactInfos((PersonView)personView);
	                        if(personsEmailList.size()>0)
	                              notifier.fireEvent(event, personsEmailList);
		        			
		        		}
		                  
		                  logger.logInfo(methodName+"|Finish");
		            }
		            catch(Exception ex)
		            {
		                  logger.LogException(methodName+"|Finish", ex);
		            }
		          
	}
	@SuppressWarnings( "unchecked" )
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator<ContactInfoView> iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	private void  getNotificationPlaceHolder(Event placeHolderMap,ContractView contractView,PersonView perView)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		placeHolderMap.setValue("PERSON_NAME",perView.getPersonFullName());
		placeHolderMap.setValue("CONTRACT_NUMBER",contractView.getContractNumber());
		
		logger.logDebug(methodName+"|Finish");
		
	}

	public HtmlTabPanel getRichTabPanel() {
		return richTabPanel;
	}

	public void setRichTabPanel(HtmlTabPanel richTabPanel) {
		this.richTabPanel = richTabPanel;
	}
	@SuppressWarnings( "unchecked" )
    public void refrashChequeDetail(String PaymentDetailList) throws Exception
	{
            
		try
		{ 
			
			    ArrayList<Long> bouncedPaymentDetailids = new ArrayList<Long>() ;
				ArrayList<Long> paymentDetailids = new ArrayList<Long>() ;
				if(PaymentDetailList.contains(","))
				{
					String[] stringArray= PaymentDetailList.split(",");
					for(int i=0;i<stringArray.length;i++)
						paymentDetailids.add(Long.parseLong(stringArray[i]));
				}
				else
					paymentDetailids.add(Long.parseLong(PaymentDetailList));
				bounceChequeList = new ArrayList<BounceChequeView>();
				List<PaymentReceiptDetailView> paymentReceiptDetailViewList = 	psa.getPaymentReceiptDetailList( paymentDetailids );
				BounceChequeView boucCheViewForList = new BounceChequeView();
				for(PaymentReceiptDetailView payRecpDetailView:paymentReceiptDetailViewList)
				{
		           	 boucCheViewForList = new BounceChequeView();
		           	 
		           	 if (payRecpDetailView.getPaymentNumber()!=null){
		           		boucCheViewForList.setPaymentNumber(payRecpDetailView.getPaymentNumber());
		           	 }
		           	 
		           	 boucCheViewForList.setPaymentReceiptDetailId(payRecpDetailView.getPaymentReceiptDetailId());
		           	 boucCheViewForList.setReceiptNumber(payRecpDetailView.getPaymentReceiptNumber());
		           	 boucCheViewForList.setChequeNumber(payRecpDetailView.getMethodRefNo());
		           	 boucCheViewForList.setPaymentDetailAmount(payRecpDetailView.getAmount());
		           	 boucCheViewForList.setAccountNumber(payRecpDetailView.getAccountNo());
		           	 boucCheViewForList.setPaymentMethodDescription(payRecpDetailView.getPaymentMethod().getDescription());
		           	 boucCheViewForList.setPaymentDetailStatusEn(payRecpDetailView.getMethodStatusEn());
		       	     boucCheViewForList.setPaymentDetailStatusAr(payRecpDetailView.getMethodStatusAr());
		       	     boucCheViewForList.setPaymentDetailStatusId(payRecpDetailView.getMethodStatusId());
		       	     boucCheViewForList.setPaymentDueOn(payRecpDetailView.getMethodRefDate());
		       	     
		           	 bounceChequeList.add(boucCheViewForList);
		           	 if( payRecpDetailView.getMethodStatusId().longValue() == getPaymentScheduleBouncedReturnedStatusId().longValue() )
		           	 {
		           		 
		           		bouncedPaymentDetailids.add( payRecpDetailView.getPaymentReceiptDetailId() );
		           		 
		           	 }
		           	 
			     }
				if( bouncedPaymentDetailids.size()>0 )
				  viewMap.put(BOUNCED_CHEQUES_PRD_IDS, bouncedPaymentDetailids );
				  viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceChequeList);
				  getBounceChequeList();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
   }
	@SuppressWarnings( "unchecked" )	
public void btnEditPaymentSchedule_Click()
{
	String methodName = "btnEditPaymentSchedule_Click";
	logger.logInfo(methodName+"|"+" Start...");
	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
	{
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	}
	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
	if( psv.getPaymentScheduleId() != null &&  psv.getPaymentScheduleId().longValue() > 0   )
	{
		sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
	}
		openPopUp("", "javascript:openPopupEditPaymentSchedule();");


	logger.logInfo(methodName+"|"+" Finish...");
}



public Boolean getEditPaymentEnable() {
	if(viewMap.containsKey("EDIT_ENABLE"))
		editPaymentEnable = (Boolean)viewMap.get("EDIT_ENABLE");
	return editPaymentEnable;
}
@SuppressWarnings( "unchecked" )
public void setEditPaymentEnable(Boolean editPaymentEnable) {
	this.editPaymentEnable = editPaymentEnable;
	if(this.editPaymentEnable !=null)
		viewMap.put("EDIT_ENABLE",this.editPaymentEnable);
}
 public String getSelectBlackListKey()
 {
	 return WebConstants.SELECT_BLACK_LIST;
 }

public HtmlCommandButton getSaveButton() {
	return saveButton;
}

public void setSaveButton(HtmlCommandButton saveButton) {
	this.saveButton = saveButton;
}

	private void notifyReturnOfCheques()
	{
		try
		{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		ContractView contView = null;
		Long reqId = new Long(this.getRequestId());
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		RequestView reqView = psAgent.getRequestById(reqId);
		String contractNo = this.getContractNoText().getValue().toString();
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		
						
		if(reqView != null)
		{
			PersonView applicant = reqView.getApplicantView();
			contView = reqView.getContractView();
			PersonView tenant = null;
			
			if(contView != null)
				tenant = contView.getTenantView();
			else
			{
				contView = psAgent.getContract(contractNo, getDateFormat());
				tenant = contView.getTenantView();
			}
			
			eventAttributesValueMap.put("CONTRACT_NUMBER", contractNo);
			eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum().getValue().toString());
			eventAttributesValueMap.put("NO_OF_CHEQUES", this.getBounceChequeList().size());
			eventAttributesValueMap.put("CHEQUE_NUMBER", getChequeNumbers());
			
					
			if(applicant!=null)
			{
				contactInfoList = getContactInfoList(applicant);
				eventAttributesValueMap.put("TENANT_NAME", applicant.getPersonFullName());
				generateNotification(WebConstants.Notification_MetaEvents.Event_Cheque_Returned,contactInfoList, eventAttributesValueMap,null);
			}
			
			if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
			{
				eventAttributesValueMap.remove("TENANT_NAME");
				eventAttributesValueMap.put("TENANT_NAME", this.getTenantNameText().getValue().toString());
				contactInfoList =  getContactInfoList(tenant);
				generateNotification(WebConstants.Notification_MetaEvents.Event_Cheque_Returned,contactInfoList, eventAttributesValueMap,null);
			}
		}
	}
		catch (PimsBusinessException e)
		{
			logger.logInfo("notifyTenant crashed...");
			e.printStackTrace();
		
		}	
		
	}
	

	private void requestStatusChangeNotification(String notificationType)
	{
		try
		{
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			Long reqId = new Long(this.getRequestId());
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			RequestView reqView = psAgent.getRequestById(reqId);
			ContractView contView = null;
			
			if(reqView != null)
			{
				PersonView applicant = reqView.getApplicantView();
				PersonView tenant = null;
				contView = reqView.getContractView();
				
				if(contView != null)
					tenant = contView.getTenantView();
				else
				{
					String contractNo = this.getContractNoText().getValue().toString();
					contView = psAgent.getContract(contractNo, getDateFormat());
					if(contView.getContractId() != null)
					{
						tenant = contView.getTenantView();
					}
				}
				
						
				if(applicant!=null)
				{
					List<ContactInfo> contactInfoList = getContactInfoList(applicant);
					
					eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
					eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
					eventAttributesValueMap.put("CONTRACT_TYPE", contView.getContractTypeEn());
					eventAttributesValueMap.put("PROPERTY_NAME", this.getTxtpropertyName().getValue().toString());
					eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum().getValue().toString());
					eventAttributesValueMap.put("NO_OF_CHEQUES", this.getBounceChequeList().size());
					eventAttributesValueMap.put("CHEQUE_NUMBER", getChequeNumbers());
					eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
														
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
					
					if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
						{
							contactInfoList = getContactInfoList(tenant);
							eventAttributesValueMap.remove("APPLICANT_NAME");
							eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
							generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
						}
				}
			}
			}
		catch (Exception e)
		{
			logger.logInfo("requestStatusNotification crashed...");
			e.printStackTrace();
		
		}
			
	}
	
	
	private String getChequeNumbers()
	{
		String chequeNumbers;
		StringBuilder builder = new StringBuilder();
		builder.append(" ");
		List<BounceChequeView> list = this.getBounceChequeList();
		if(!list.isEmpty())
		{
			for(BounceChequeView chequeView : list)
				builder.append(chequeView.getChequeNumber()).append("<br/>").append("\t");
		}
		
		chequeNumbers = builder.toString();
		return chequeNumbers;
	}
	@SuppressWarnings( "unchecked" )
	public void chkSelectChq_Changed()
	{
		
		try
		{
			BounceChequeView row = ( BounceChequeView )dataTableChequeDetail.getRowData();
			
			if( row.getSelected() != null && row.getSelected() )
			{
				List<PaymentScheduleView> paymentConfigs = null;
				if( row.getPaymentDetailStatusId().longValue() == this.getPaymentScheduleCollectedDomainDataId() )
					paymentConfigs = this.getCollectedChequePaymentConfiguration() ;
				else if ( row.getPaymentDetailStatusId().longValue() == this.getPaymentScheduleBouncedDomainDataId() )
					paymentConfigs = this.getBouncedChequePaymentConfiguration();
				if( paymentConfigs !=null  )
				{
						if ( this.getPaymentSchedules().size() > 0  )
						{
							for (PaymentScheduleView psv : this.getPaymentSchedules() ) {
								for (PaymentScheduleView paymentConfig : paymentConfigs) {
									
								
									if(psv.getTypeId().compareTo(paymentConfig.getTypeId())==0)
									{
										psv.setAmount( psv.getAmount() + paymentConfig.getAmount() );
									}
								}
							}
						}
						
				}
			}
			else if ( row.getSelected() != null && !row.getSelected() )
			{
				List<PaymentScheduleView> paymentConfigs = null;
				if( row.getPaymentDetailStatusId().longValue() == this.getPaymentScheduleCollectedDomainDataId() )
					paymentConfigs = this.getCollectedChequePaymentConfiguration() ;
				else if ( row.getPaymentDetailStatusId().longValue() == this.getPaymentScheduleBouncedDomainDataId() )
					paymentConfigs = this.getBouncedChequePaymentConfiguration();
				if( paymentConfigs !=null  )
				{
						if ( this.getPaymentSchedules().size() > 0  )
						{
							for (PaymentScheduleView psv : this.getPaymentSchedules() ) {
								for (PaymentScheduleView paymentConfig : paymentConfigs) {
									
								
									if(psv.getTypeId().compareTo(paymentConfig.getTypeId())==0)
									{
										psv.setAmount( psv.getAmount() - paymentConfig.getAmount() );
									}
								}
							}
						}
						
				}
				
			}
			if( paymentSchedules !=null )
			viewMap.put(WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules);
		}
		catch( Exception e )
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("chkSelectChq_Changed|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public long getPaymentScheduleCollectedDomainDataId()
	{
		long id;
		if( viewMap.get( "ddvCollectedId" ) == null )
		{
		 DomainDataView ddvCollected =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_COLLECTED );
		 id = ddvCollected.getDomainDataId();
		 viewMap.put( "ddvCollectedId", id );
		}
		else  
			id = Long.parseLong( viewMap.get( "ddvCollectedId" ).toString() );
		return id;
	}
	@SuppressWarnings( "unchecked" )
	private Long getPaymentScheduleBouncedReturnedStatusId() 
	{
		long id;
		if( viewMap.get( "ddvBouncedReturnedId" ) == null )
		{
		 DomainDataView ddvBouncedReturned =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_RETURN );
		 id = ddvBouncedReturned.getDomainDataId();
		 viewMap.put( "ddvBouncedReturnedId", id );
		}
		else  
			id = Long.parseLong( viewMap.get( "ddvBouncedReturnedId" ).toString() );
		return id;
	}
	@SuppressWarnings( "unchecked" )
	public long getPaymentScheduleBouncedDomainDataId()
	{
		long id;
		if( viewMap.get( "ddvBouncedId" ) == null )
		{
		 DomainDataView ddvCollected =  CommonUtil.getIdFromType( this.getPaymentScheduleStatus(), WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED );
		 id = ddvCollected.getDomainDataId();
		 viewMap.put( "ddvBouncedId", id );
		}
		else  
			id = Long.parseLong( viewMap.get( "ddvBouncedId" ).toString() );
		return id;
	}
	@SuppressWarnings( "unchecked" )
	public List<DomainDataView> getPaymentScheduleStatus () 
	{
		List<DomainDataView> ddvList ; 
		if( viewMap.get( WebConstants.PAYMENT_SCHEDULE_STATUS ) == null )
		 ddvList =  getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
		else
			ddvList =  	( ArrayList<DomainDataView> ) viewMap.get( WebConstants.PAYMENT_SCHEDULE_STATUS ); 
	  return ddvList;
	}
	@SuppressWarnings( "unchecked" )
	public Integer getNoOfBounceChqToReplace() 
	{
		if( viewMap.get( "noOfBounceChqToReplace" ) != null )
			noOfBounceChqToReplace = Integer.parseInt( viewMap.get( "noOfBounceChqToReplace" ).toString() ); 
		return noOfBounceChqToReplace;
	}
	@SuppressWarnings( "unchecked" )
	public void setNoOfBounceChqToReplace(Integer noOfBounceChqToReplace) {
		
		this.noOfBounceChqToReplace = noOfBounceChqToReplace;
		if( this.noOfBounceChqToReplace !=null )
		viewMap.put( "noOfBounceChqToReplace", this.noOfBounceChqToReplace );
	}
	@SuppressWarnings( "unchecked" )
	public Integer getNoOfCollectedChqToReplace() {
		
		if( viewMap.get( "noOfCollectedChqToReplace" ) != null )
			noOfCollectedChqToReplace = Integer.parseInt( viewMap.get( "noOfCollectedChqToReplace" ).toString() );
		return noOfCollectedChqToReplace;
	}
	@SuppressWarnings( "unchecked" )
	public void setNoOfCollectedChqToReplace(Integer noOfCollectedChqToReplace) {
		this.noOfCollectedChqToReplace = noOfCollectedChqToReplace;
		if( this.noOfCollectedChqToReplace  !=null )
			viewMap.put( "noOfCollectedChqToReplace", this.noOfCollectedChqToReplace  );
	}
	@SuppressWarnings( "unchecked" )
	public Double getToatlFeesForBounceChqToReplace() {
		if( viewMap.get( "toatlFeesForBounceChqToReplace" ) != null )
			toatlFeesForBounceChqToReplace = new Double ( viewMap.get( "toatlFeesForBounceChqToReplace" ).toString() );
		return toatlFeesForBounceChqToReplace;
	}
	@SuppressWarnings( "unchecked" )
	public void setToatlFeesForBounceChqToReplace( Double toatlFeesForBounceChqToReplace) 
	{
		this.toatlFeesForBounceChqToReplace = toatlFeesForBounceChqToReplace;
		if( this.toatlFeesForBounceChqToReplace !=null )
			viewMap.put( "toatlFeesForBounceChqToReplace", this.toatlFeesForBounceChqToReplace );
	}
	@SuppressWarnings( "unchecked" )
	public Double getToatlFeesForCollectedChqToReplace() 
	{
		if( viewMap.get( "toatlFeesForCollectedChqToReplace" ) != null )
			toatlFeesForCollectedChqToReplace = new Double ( viewMap.get( "toatlFeesForCollectedChqToReplace" ).toString() );
		return toatlFeesForCollectedChqToReplace;
	}
	@SuppressWarnings( "unchecked" )
	public void setToatlFeesForCollectedChqToReplace( Double toatlFeesForCollectedChqToReplace ) 
	{
		this.toatlFeesForCollectedChqToReplace = toatlFeesForCollectedChqToReplace;
		if( this.toatlFeesForCollectedChqToReplace !=null )
			viewMap.put( "toatlFeesForCollectedChqToReplace", this.toatlFeesForCollectedChqToReplace );
	}
	@SuppressWarnings( "unchecked" )
	private List<PaymentScheduleView>  getCollectedChequePaymentConfiguration()
	{
		if( viewMap.get( WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE ) != null )
		{
			
			return  ( ArrayList<PaymentScheduleView>)viewMap.get( WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE );
		}
		return null;
		
	}
	
	@SuppressWarnings( "unchecked" )
	private  void setCollectedChequePaymentConfiguration( List<PaymentScheduleView> paymentScheduleViewList )
	{
		if( paymentScheduleViewList != null  && paymentScheduleViewList.size() > 0 )
		viewMap.put( WebConstants.CONDITION_PAYMENT_CONFIG_COLLECTED_CHEQUE, paymentScheduleViewList);
		
		
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getBouncedChequePaymentConfiguration()
	{
		if( viewMap.get( WebConstants.CONDITION_PAYMENT_CONFIG_BOUNCE_CHEQUE ) != null )
			return  ( ArrayList<PaymentScheduleView> )viewMap.get( WebConstants.CONDITION_PAYMENT_CONFIG_BOUNCE_CHEQUE );
		
		return null;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public  void setBouncedChequePaymentConfiguration( List<PaymentScheduleView> paymentScheduleViewList )
	{
		if( paymentScheduleViewList != null  && paymentScheduleViewList.size() > 0 )
		viewMap.put( WebConstants.CONDITION_PAYMENT_CONFIG_BOUNCE_CHEQUE, paymentScheduleViewList );
		
		
	}
	@SuppressWarnings("unchecked")
	private void fromFollowup() throws PimsBusinessException, Exception
	{
		//calling mode for initiate replace cheque procedure 
		modeSend();
		requestView = 	( RequestView )sessionMap.get( WebConstants.REQUEST_VIEW );
		viewMap.put( WebConstants.REQUEST_VIEW ,requestView);
		sessionMap.remove( WebConstants.REQUEST_VIEW );
		pageModePopup=true;
		this.requestId=requestView.getRequestId().toString();
		populateApplicationDataTab( this.requestId );
		List<RequestView> requestList = new PropertyServiceAgent().getAllRequests( requestView, "", "", null );
		if(requestList.size()>0)
		{
			requestView = ( RequestView )requestList.iterator().next();
			viewMap.put( "REQUEST_VIEW_FOR_EDIT", requestView );
			fetchDataFromReqestKeyDetail();
			paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID( Long.parseLong( requestId ) );
			if( paymentSchedules!=null && paymentSchedules.size() > 0 )
			{
			   viewMap.put( "PAYMENT_SCH_FOR_EDIT", paymentSchedules );
			   viewMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, paymentSchedules );
			   viewMap.put( WebConstants.REPLACE_CHEQUE_FEE_AMOUNT, paymentSchedules );
			}
			else
			   viewMap.put( "NO_PAYMENT_SCH_FOR_EDIT", true );
			getPaymentSchedules();
			getBounceChequeList();
		}
		ApplicationContext.getContext().get( WebContext.class ).setAttribute( WebConstants.REQUEST_VIEW, requestView );
		CommonUtil.loadAttachmentsAndComments( requestId );
				sendBtn.setRendered(false);
				saveButton.setRendered(false);
				setEditPaymentEnable(false);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				closeButton.setRendered(false);
	}
	public boolean isPageModePopup() 
	{
		if(viewMap.get(WebConstants.REQUEST_VIEW)!=null)
			pageModePopup=true;
		else
			pageModePopup=false;
		return pageModePopup;
	}
	public void setPageModePopup(boolean pageModePopup) {
		this.pageModePopup = pageModePopup;
	}	
	@SuppressWarnings( "unchecked" )
	private int getSysConfigDaysLimit()
	{
		int days = -1;
		try
		{
		 if( viewMap.get( WebConstants.SYS_CONFIG_KEY_DAYS_BEFORE_REPLACE_CHEQUE ) == null  )
		 {
		  String sysConfigDays = new UtilityService().getValueFromSystemConfigration( WebConstants.SYS_CONFIG_KEY_DAYS_BEFORE_REPLACE_CHEQUE );
		  if( sysConfigDays !=null && sysConfigDays.trim().length() > 0 )
			viewMap.put( WebConstants.SYS_CONFIG_KEY_DAYS_BEFORE_REPLACE_CHEQUE, sysConfigDays );
		 }
		  days = Integer.parseInt(  viewMap.get( WebConstants.SYS_CONFIG_KEY_DAYS_BEFORE_REPLACE_CHEQUE ).toString() );
		}
		catch( Exception e )
		{
			logger.LogException( "getSysConfigDaysLimit|Error:",e );
		}
		return days;
	}
	public String getSelectedProcedureType() 
	{
		selectedProcedureType="0";
		if(viewMap.containsKey("PROCEDURE_TYPE") && viewMap.get("PROCEDURE_TYPE")!=null)
		{
			String procedureType=viewMap.get("PROCEDURE_TYPE").toString();
			if(procedureType.equalsIgnoreCase("BOUNCED"))
				selectedProcedureType="2";
			else 
				selectedProcedureType="1";
			
		}
		return selectedProcedureType;
	}
	public void setSelectedProcedureType(String selectedProcedureType) 
	{
		this.selectedProcedureType = selectedProcedureType;
	}
	public boolean isShowProcedureType() 
	{
		if(viewMap.containsKey("HIDE_PROCEDURE") && viewMap.get("HIDE_PROCEDURE") !=null)
			showProcedureType=false;
		else 
			showProcedureType=true;
		return showProcedureType;
	}
	public void setShowProcedureType(boolean showProcedureType) {
		this.showProcedureType = showProcedureType;
	}
	public HtmlCommandButton getReviewBtn() {
		return reviewBtn;
	}
	public void setReviewBtn(HtmlCommandButton reviewBtn) {
		this.reviewBtn = reviewBtn;
	}
	public void review()
	{
		String methodName = "review|";
		try
		{
			logger.logInfo( methodName +"Start|Request Id:%s",this.requestId );
			UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);
			loadAttachmentsAndComments(Long.parseLong(this.requestId));
			saveAttachments(Long.parseLong(this.requestId));
			saveComments(Long.parseLong(this.requestId));
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,"replaceCheq.event.ReviewedLegal", Long.parseLong(requestId));
			client.completeTask(userTask, loggedInUser, TaskOutcome.OK);
			reviewBtn.setRendered(false);
			successMessages.add(CommonUtil.getBundleMessage("replaceCheq.event.ReviewedLegal"));
			logger.logInfo( methodName +"Finish" );
		}
		catch (Exception e) 
		{
			logger.LogException(" review() crashed | ",e);
		}
	}
	public void openPopupToSplitAmount()
	{		
		String methodName = "openPopupToSplitAmount";
		logger.logInfo(methodName+"|"+" Start...");
		PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();		
		sessionMap.put("PAYMENT_SCHDULE_SPLIT",psv);
		executeJavaScript("openSplitingPopup();");
		logger.logInfo(methodName+"|"+" Finish...");		
	}
	public void executeJavaScript(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	public void printWithdrawlLetter()
	{
	try{
		List<PaymentScheduleView> repChqList= new ArrayList<PaymentScheduleView>();
		Long requestId;
		List<String> ids=new ArrayList<String>();
		String queryInCondition="";
		requestId=(Long) viewMap.get("REQUEST_ID_FOR_WITHDRAWL");
		if(isAcceptedCollected()){
			requestView = new RequestService().setRequestStatus(requestId, WebConstants.REQUEST_STATUS_CHEQUE_WITHDRAWN, CommonUtil.getLoggedInUser());
			//requestView is being saved in viewmap due to its requirement for control rendering methods. like isWithdrwan()
			viewMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
			populateApplicationDataTab(requestView.getRequestId().toString());
			successMessages.add(CommonUtil.getBundleMessage("successMsg.withdrawlLetter.printed"));
		}
		ids= new PropertyService().getReplacedPaymentScheduleIdsByRequestId(requestId);
		if(ids !=null && ids.size()>0)
		{
			if(ids.size()>1)
				{queryInCondition=" IN (";
					for(String id : ids)
					queryInCondition = queryInCondition.concat(id+",");
					queryInCondition=queryInCondition.substring(0,queryInCondition.length()-1);
					queryInCondition += ")";
				}
			else
				queryInCondition= " = "+ids.get(0);

			
			withDrawlCriteria.setInCriteria(queryInCondition);
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, withDrawlCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			saveComments(requestId);
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.WITH_DRAWL_LETTER_PRINTED, requestId);
		}
		
		logger.logDebug("printWithdrawlLetter" + "Finish");		
		
	}
	catch (Exception e) 
	{
		logger.LogException("printWithdrawlLetter() crashed |" ,e);
	}
	}
private UserDbImpl getLoggedInDBImplUser() {
	FacesContext context = FacesContext.getCurrentInstance();
	UserDbImpl user=new UserDbImpl();
	HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) 
		 user = (UserDbImpl) session	.getAttribute(WebConstants.USER_IN_SESSION);

	return user;
}
public HtmlCommandButton getWithdrawlReportBtn() {
	return withdrawlReportBtn;
}
public void setWithdrawlReportBtn(HtmlCommandButton withdrawlReportBtn) {
	this.withdrawlReportBtn = withdrawlReportBtn;
}
private void openPopup(String javaScriptText) 
{
	logger.logInfo("openPopup() started...");
	try {
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
		logger.logInfo("openPopup() completed successfully!!!");
	} catch (Exception exception) {
		logger.LogException("openPopup() crashed ", exception);
	}
}
public boolean isShowWithdrawPrintButton()
{
	//Old Code
	//if(viewMap.get("SHOW_WITHDRAWL_LETTER")!=null)
	//New Coe
	if(isAcceptedCollected() || isWithdrawn())
		isShowWithdrawPrintButton=true;
	else
		isShowWithdrawPrintButton=false;
	return isShowWithdrawPrintButton;
}
public void setShowWithdrawPrintButton(boolean isShowWithdrawPrintButton) {
	this.isShowWithdrawPrintButton = isShowWithdrawPrintButton;
}

@SuppressWarnings( "unchecked" )
public void btnViewPaymentDetails_Click() {
	String methodName = "btnViewPaymentDetails_Click";
	logger.logInfo(methodName+"|"+" Start...");
	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	CommonUtil.viewPaymentDetails(psv);

	logger.logInfo(methodName+"|"+" Finish...");
}
public DomainDataView getDdvRequestStatusChequeWithdrawn() {
	if(viewMap.get(WebConstants.REQUEST_STATUS_CHEQUE_WITHDRAWN) != null)
		ddvRequestStatusChequeWithdrawn = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_CHEQUE_WITHDRAWN);
	return ddvRequestStatusChequeWithdrawn;
}
public void setDdvRequestStatusChequeWithdrawn(
		DomainDataView ddvRequestStatusChequeWithdrawn) {
	this.ddvRequestStatusChequeWithdrawn = ddvRequestStatusChequeWithdrawn;
}
public DomainDataView getDdvRequestStatusRecievedFromBank() {
	if(viewMap.get(WebConstants.REQUEST_STATUS_CHEQUE_RECEIVED_FROM_BANK) != null)
		ddvRequestStatusRecievedFromBank = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_CHEQUE_RECEIVED_FROM_BANK);
	return ddvRequestStatusRecievedFromBank;
}
public void setDdvRequestStatusRecievedFromBank(
		DomainDataView ddvRequestStatusRecievedFromBank) {
	this.ddvRequestStatusRecievedFromBank = ddvRequestStatusRecievedFromBank;
}
public boolean isWithdrawn() {
	if(viewMap.get("REQUEST_VIEW_FOR_EDIT") != null){
		requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
		if(requestView.getStatusId().compareTo(getDdvRequestStatusChequeWithdrawn().getDomainDataId()) == 0){
			return true;
		}
	}
	return false;
}
public void setWithdrawn(boolean isWithdrawn) {
	this.isWithdrawn = isWithdrawn;
}
public boolean isReceivedFromBank() {
	if(viewMap.get("REQUEST_VIEW_FOR_EDIT") != null){
		requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
		if(requestView.getStatusId().compareTo(getDdvRequestStatusRecievedFromBank().getDomainDataId()) == 0){
			return true;
		}
	}
	return false;
}
public void setReceivedFromBank(boolean isReceivedFromBank) {
	this.isReceivedFromBank = isReceivedFromBank;
}
public boolean isAcceptedCollected() {
	if(viewMap.get("REQUEST_VIEW_FOR_EDIT") != null){
		requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
		DomainDataView ddv = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_ACCEPTED_COLLECTED);
		if(requestView.getStatusId().compareTo(ddv.getDomainDataId()) == 0){
			return true;
		}
	}
	return false;
}
public void setAcceptedCollected(boolean isAcceptedCollected) {
	this.isAcceptedCollected = isAcceptedCollected;
}
private void receiveChequeFromBankBPEL(Map viewRootMap) throws Exception{
	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
    logger.logInfo("Contextpath is:"+contextPath);
    String loggedInUser = getLoggedInUser();
	BPMWorklistClient client = new BPMWorklistClient(contextPath);			
	Map taskAttributes =  userTask.getTaskAttributes();
	Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
	this.requestId  = requestId.toString();
	loadAttachmentsAndComments(requestId);
	saveAttachments(requestId);
	saveComments(requestId);
//	RequestServiceAgent rSAgent = new RequestServiceAgent();
//	rSAgent.setRequestAsOldChequeReturned(Long.parseLong(this.requestId), getLoggedInUser());
	if( viewMap.get( BOUNCED_CHEQUES_PRD_IDS )!=null )
	{
		logger.logInfo("receiveChequeFromBankBPEL() |"+" closing followUps...");
		new FinanceService().closeFollowUpsFromReplaceCheque( (ArrayList<Long>)viewMap.get( BOUNCED_CHEQUES_PRD_IDS ) , getLoggedInUser(), "");
	}
	client.completeTask(userTask, loggedInUser, TaskOutcome.OK);
//	NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_RETURN, requestId);
//	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_RETURNED));
//	if(getSelectedProcedureType() !=null && getSelectedProcedureType().compareTo("2")==0)
//	{
//		NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
//    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_Complete));
//		completeBtn.setRendered(false);
//	}
				
	logger.logInfo("returnChequeBPELInvoke() |"+" completed successfully...");
	modeChequeReturnedComplete();
}
@SuppressWarnings("unchecked")
public void receiveChequeFromBank(){
	String methodName ="receiveChequeFromBank";
	logger.logInfo(methodName+"|"+"Start..");
	try	
	{
		markChequesAndRequestAsReceivedFromBank();
	}
	catch(Exception ex)
	{
		logger.LogException(methodName+"|"+"Error Occured..",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	logger.logInfo(methodName+"|"+"Finish..");
}

@SuppressWarnings("unchecked")
public void markChequesAndRequestAsReceivedFromBank()
	{
		final String  methodName ="markChequesAndRequestAsReceivedFromBank";
		logger.logInfo(methodName+"|"+"Start..");
		List<BounceChequeView> bounceCheques = new ArrayList<BounceChequeView>();
		bounceCheques  = getBounceChequeList();
		try	
		{
			requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
			if(bounceCheques != null && bounceCheques.size() > 0){
				
				DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_RECEIVED_FROM_BANK);
				List<Long> rcptDtlIds= new ArrayList<Long>();
				for(BounceChequeView bounceCheque : bounceCheques){
					rcptDtlIds.add( bounceCheque.getPaymentReceiptDetailId());
					bounceCheque.setPaymentDetailStatusId(ddv.getDomainDataId());
					bounceCheque.setPaymentDetailStatusEn(ddv.getDataDescEn());
					bounceCheque.setPaymentDetailStatusAr(ddv.getDataDescAr());
				}
				new PropertyService().setChequeAndRequestReceivedFromBank(rcptDtlIds, requestView.getRequestId(),  WebConstants.REQUEST_STATUS_CHEQUE_RECEIVED_FROM_BANK, CommonUtil.getLoggedInUser());
				requestView = new RequestService().getRequestById(requestView.getRequestId()); 
				viewMap.put("REQUEST_VIEW_FOR_EDIT",requestView);
				viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceCheques);
				populateApplicationDataTab(requestView.getRequestId().toString());
//				if(viewMap.containsKey(WebConstants.RETURN_CHEQUE_DONE))
//			    	{
						receiveChequeFromBankBPEL(viewMap);
//			    	}
//			    	else
//			    	{
//			    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReplaceChequeNew.REQUEST_REPLACED_NOT_RETURN));
//			    	}
				successMessages.add(CommonUtil.getBundleMessage("successMsg.chequeHasBeenReceivedFromBank"));
				saveComments(requestView.getRequestId());
				NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.CHEQUE_RECEIVED_FROM_BANK, requestView.getRequestId());
			}
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured..",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceCheques);
		logger.logInfo(methodName+"|"+"Finish..");
	}
public void templateMethodSignature()
{
	String methodName ="templateMethodSignature";
	logger.logInfo(methodName+"|"+"Start..");
	successMessages = new ArrayList<String>(0);
	errorMessages = new ArrayList<String>(0);
	try	
	{
	}
	catch(Exception ex)
	{
		logger.LogException(methodName+"|"+"Error Occured..",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	logger.logInfo(methodName+"|"+"Finish..");
}
public void markChequesAsRecieveReturnedAndRequestComplete()
{
	final String  methodName ="markChequesAsRecieveReturned";
	logger.logInfo(methodName+"|"+"Start..");
	List<BounceChequeView> bounceCheques = new ArrayList<BounceChequeView>();
	bounceCheques  = getBounceChequeList();
	try	
	{
		requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
		if(bounceCheques != null && bounceCheques.size() > 0){
			
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_COLLECTED_RETURN);
			List<Long> rcptDtlIds= new ArrayList<Long>();
			for(BounceChequeView bounceCheque : bounceCheques){
				rcptDtlIds.add( bounceCheque.getPaymentReceiptDetailId());
				bounceCheque.setPaymentDetailStatusId(ddv.getDomainDataId());
				bounceCheque.setPaymentDetailStatusEn(ddv.getDataDescEn());
				bounceCheque.setPaymentDetailStatusAr(ddv.getDataDescAr());
			}
			new PropertyService().setChequeReceivedReturnedAndRequestComplete(rcptDtlIds, requestView.getRequestId(), WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil.getLoggedInUser());
//			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_RETURN, Long.getLong(requestId));
			NotesController.saveSystemNotesForRequest(WebConstants.REPLACE_CHEQUE,MessageConstants.RequestEvents.REQUEST_RETURN, requestView.getRequestId());
			
			requestView = new RequestService().getRequestById(requestView.getRequestId()); 
			viewMap.put("REQUEST_VIEW_FOR_EDIT",requestView);
			viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceCheques);
			populateApplicationDataTab(requestView.getRequestId().toString());
		}
	}
	catch(Exception ex)
	{
		logger.LogException(methodName+"|"+"Error Occured..",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceCheques);
	logger.logInfo(methodName+"|"+"Finish..");
}
public boolean isAcceptedSettled() {
	if(viewMap.get("REQUEST_VIEW_FOR_EDIT") != null){
		requestView = (RequestView) viewMap.get("REQUEST_VIEW_FOR_EDIT");
		DomainDataView ddv = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_ACCEPTED_SETTLED);
		if(requestView.getStatusId().compareTo(ddv.getDomainDataId()) == 0){
			return true;
		}
	}
	return false;
}
public void setAcceptedSettled(boolean isAcceptedSettled) {
	this.isAcceptedSettled = isAcceptedSettled;
}
public boolean isShowReturnOldChequeButton() {
	if(viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
		UserTask task = (UserTask) viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if(task.getTaskType().compareTo(WebConstants.ReplaceCheque.COMPLETE_REPLACE_CHEQUE ) == 0 && (isAcceptedSettled() || isReceivedFromBank()))
			return true;
	}
	return false;
}
public void setShowReturnOldChequeButton(boolean isShowReturnOldChequeButton) {
	this.isShowReturnOldChequeButton = isShowReturnOldChequeButton;
}
public boolean isShowReturnCheque() {
	if(viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
		UserTask task = (UserTask) viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if(task.getTaskType().compareTo( WebConstants.ReplaceCheque.RETURNED_BOUNCED_CHEQUE_AND_COMPLETE_REQUEST ) == 0)
			return true;
	} 
	return false;
}
public void setShowReturnCheque(boolean isShowReturnCheque) {
	this.isShowReturnCheque = isShowReturnCheque;
}
}

