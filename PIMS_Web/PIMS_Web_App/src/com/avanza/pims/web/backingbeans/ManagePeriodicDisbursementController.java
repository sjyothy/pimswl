package com.avanza.pims.web.backingbeans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PeriodicDisbursementGridView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;

/**
 * @author Farhan Muhammad Amin
 */
@SuppressWarnings("unchecked")
public class ManagePeriodicDisbursementController extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;
	private UserTask userTask = null;
	private static final String MAP_ID_COMMENT = "MAP_ID_COMMENT";
	private static final String PRDIC_DISBURSE_ID_LIST = "PeriodicIdsList";
	private static final String PRDIC_DISBURSE_VIEW = "PeriodicPageView";
	private static final String DEFAULT_SORT_FIELD = "endDate";
	private String amount;
	private Date startDate;
	private Date renewedDate;
	private boolean requestStatusCompleted;
	private boolean requestStatusNew;
	protected HtmlDataTable periodicDisbursementTable = new HtmlDataTable();

	@Override
	public void init() {
		super.init();
		
		try {
			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(false);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				
				List<DomainDataView> domain = CommonUtil.getDomainDataListForDomainType(WebConstants.PeriodicListStatus.LIST_STATUS_KEY);
				DomainDataView statusRenewalReq = CommonUtil.getIdFromType(domain, WebConstants.PeriodicListStatus.LIST_STATUS_RENEWAL_REQ);
				DomainDataView statusRenewed = CommonUtil.getIdFromType(domain, WebConstants.PeriodicListStatus.LIST_STATUS_RENEWED);
				DomainDataView statusStopped = CommonUtil.getIdFromType(domain, WebConstants.PeriodicListStatus.LIST_STATUS_STOPPED);
				DomainDataView statusActive = CommonUtil.getIdFromType(domain, WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE);
				
				DomainDataView requestCompleted = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
						                                                WebConstants.REQUEST_STATUS_COMPLETE);
				viewMap.put(WebConstants.REQUEST_STATUS_COMPLETE, requestCompleted);
				viewMap.put(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWAL_REQ, statusRenewalReq.getDomainDataId().toString());
				viewMap.put(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWED, statusRenewed );
				viewMap.put(WebConstants.PeriodicListStatus.LIST_STATUS_STOPPED, statusStopped );
				viewMap.put(WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE, statusActive );
				
				updateValuesFromMap();
				
				
				
				RequestView reqView = (RequestView) getRequestMap().get( WebConstants.REQUEST_VIEW);
				if (null != reqView) {
					initFromRequestView(reqView);
				} else {
					initFromUserTask();
				}				
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
		}
	}

	private void updateValuesFromMap() {
		 
		// User Task
		if ( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask)viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		}
		else if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
			
			
		}
		
		
		
	}

	@SuppressWarnings("unchecked")
	private void initFromRequestView(RequestView reqView) throws Exception
	{
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
		Set<RequestDetailView> details = reqView.getRequestDetailView();
		List<Long> list = new ArrayList<Long>(details.size());
		Map<Long,String> idCommentMap = new HashMap<Long, String>();
		for (RequestDetailView singleDetail : details) 
		{
			list.add( singleDetail.getPeriodicDisburseId() );
			if( singleDetail.getEvaluationRemarks() != null && singleDetail.getEvaluationRemarks().trim().length() > 0  )
			{
				 idCommentMap.put(
						 			singleDetail.getPeriodicDisburseId(),
						 			singleDetail.getEvaluationRemarks().trim()
				 				  );
			}
		}
		viewMap.put(PRDIC_DISBURSE_ID_LIST, list);
		viewMap.put(MAP_ID_COMMENT,idCommentMap);
		setTotalRows(list.size());
		doPagingComputations();
		doSearchItemList();
	}

	@Override
	public void doSearchItemList() 
	{
		try 
		{
			List<Long> idsList = getPeriodicDisbursementIdList();
			Map<Long,String> idCommentMap = new HashMap<Long, String>();
			DisbursementService periodicDisburseWS = new DisbursementService();
			int totalRecords = periodicDisburseWS.getTotalRecords(idsList, Long.parseLong(getStatusRenewalRequiredId()));
			setTotalRows(totalRecords);
		
			doPagingComputations();
			
			List<PeriodicDisbursementGridView> viewList = periodicDisburseWS.getPeriodicDisbursementList(
																										  idsList, getRowsPerPage(),
																										  getCurrentPage(), DEFAULT_SORT_FIELD, 
																										  false, 
																										  Long.parseLong( getStatusRenewalRequiredId() )
																										 );
			idCommentMap  = (HashMap<Long, String>)viewMap.get(MAP_ID_COMMENT );
			for (PeriodicDisbursementGridView obj : viewList) 
			{
				if( idCommentMap.containsKey(obj.getPeriodicDisbursementId() ) )
				{
					obj.setComments( idCommentMap.get(obj.getPeriodicDisbursementId() ) );
				}
			}

			if( null!= viewList && 0<viewList.size() )
			{
				List<PeriodicDisbursementGridView> temp = getPageView();
				viewMap.remove(PRDIC_DISBURSE_VIEW);
				temp.clear();
				temp = null;
				viewMap.put(PRDIC_DISBURSE_VIEW, viewList);
			} 
			else 
			{
				getPageView().clear();
			}

		} catch (Exception ex) {
			logger.logInfo("[Exception occured in doSearchItemList]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initFromUserTask() throws PimsBusinessException, Exception 
	{
		Long requestId=Long.parseLong(userTask.getTaskAttributes().get("REQUEST_ID"));
		RequestService reqWS = new RequestService();
		RequestView requestView = reqWS.getRequestById(requestId);
		initFromRequestView(requestView);
	}

	public void renew() 
	{
		try 
		{
			if( !validateInputs() ) 
			{return;}
				DomainDataView dd= getStatusRenewed();
				List<PeriodicDisbursementGridView> pageList = getPageView();
				boolean bIsAnySelected=false;
				for (PeriodicDisbursementGridView singleRecord : pageList) 
				{
					if (singleRecord.getSelected()) 
					{
						changeStatus(singleRecord, dd);
						if( startDate != null )
						{
							singleRecord.setDateFrom( startDate );
						}
						if( renewedDate != null )
						{
							singleRecord.setRenewedDate(renewedDate);
							singleRecord.setDateTo( renewedDate);
						}
						if( amount != null && amount.trim().length() > 0)
						{
							singleRecord.setAmount(  new Double(amount) );
						}
						singleRecord.setSelected(false);
						bIsAnySelected=true;
					}
				}
				if( !bIsAnySelected ) 
				{
					errorMessages.add(CommonUtil.getBundleMessage(
							MessageConstants.MemsPeriodicDisbursements.ERR_NO_RECORD_IS_SELECTED));
				}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in renew()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void stop() 
	{
		try
		{
			List<PeriodicDisbursementGridView> pageList = getPageView();
			DomainDataView ddStop = getStatusStopped();
			boolean bIsAnySelected=false;
			for (PeriodicDisbursementGridView singleRecord : pageList)
			{
				if (singleRecord.getSelected()) {
					changeStatus(singleRecord, ddStop);
					singleRecord.setSelected(false);
					bIsAnySelected = true;
				}
			}
			if( !bIsAnySelected ) {
				errorMessages.add(CommonUtil.getBundleMessage(
						MessageConstants.MemsPeriodicDisbursements.ERR_NO_RECORD_IS_SELECTED));
			}
		}
		catch( Exception e )
		{
			logger.LogException("[stop() .. Starts]", e );
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		
		}
	}
    private void changeStatus( PeriodicDisbursementGridView obj,DomainDataView dd )throws Exception
    {
       obj.setStatus( dd.getDomainDataId() );
       obj.setStatusEn( dd.getDataDescEn() );
       obj.setStatusAr( dd.getDataDescAr() );
       obj.setStatusDataValue(dd.getDataValue() );
    }
	public void complete() {
		
		try 
		{
			if( validateComplete() ) 
			{
				List<PeriodicDisbursementGridView> list = getPageView();
				RequestView request = getRequestView();
				DisbursementService service = new DisbursementService();
				Double newAmount = null;
				if( amount != null && amount.length() > 0 )
				{
					newAmount = new Double(amount);
				}
				service.completeRenewalOrStopPeriodicDisbursementRequest( 
																		  request.getRequestId(), 
						                                                  list, 
						                                                  CommonUtil.getLoggedInUser() , 
						                                                  getStatusActive().getDomainDataId(), 
						                                                  getStatusRenewed().getDomainDataId(), 
						                                                  getRenewedDate() ,
						                                                  startDate,
						                                                  newAmount 
						                                                  
						                                                  );
				setTaskOutCome(TaskOutcome.OK);
				DomainDataView requestComplete = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
				request.setStatusId( requestComplete.getDomainDataId() );
				request.setStatusEn( requestComplete.getDataDescEn() );
				request.setStatusAr( requestComplete.getDataDescAr() );
				request.setStatusDataValue( requestComplete.getDataValue() );
				
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPeriodicDisbursements.SUCCESS_RCRD_UPDATED));
				doSearchItemList();
			} 
		} catch(Exception ex) {
			logger.LogException("[Exception occured in complete()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	private boolean validateComplete()throws Exception 
	{
		List<PeriodicDisbursementGridView> curPage = getPageView();
		String renewalReqId = getStatusRenewalRequiredId();
		for(PeriodicDisbursementGridView singleRec : curPage) 
		{
			if( singleRec.getStatus().equals(renewalReqId) ) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPeriodicDisbursements.ERR_NO_ALL_RCRDS));
				return false;
			}
		}
		return true;
	}
    @SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) 
    {
    	
    	try
    	{
			PeriodicDisbursementGridView gridViewRow = (PeriodicDisbursementGridView) periodicDisbursementTable.getRowData();
			executeJavaScript( "javaScript:openBeneficiaryPopup("+ gridViewRow.getBeneficiaryId()+");");
    	}
		catch( Exception ex )
    	{
    		logger.LogException("openManageBeneficiaryPopUp|Error Occured...",ex);
    	}
    

	}
	

	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			String loggedInUser=CommonUtil.getLoggedInUser();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }
	private boolean validateInputs()throws Exception 
	{
		boolean allEmpty;
		allEmpty = null==renewedDate && null==amount && amount.trim().length() <=0 ;
		if( allEmpty)
		{
			errorMessages.add( "All emoty");
			return false;
		}
		else if( null!=renewedDate )
		{
			String renewed = new SimpleDateFormat("dd/MM/yyyy").format(renewedDate);
			if( !DateUtil.compareDate(DateUtil.getCurrentDateString(), renewed)) 
			{
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPeriodicDisbursements.ERR_CURRENT_DATE_GREATER));
				return false;
			}	
		}
		return true;
	}

	
	public RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}

	public List<Long> getPeriodicDisbursementIdList() {
		if (viewMap.containsKey(PRDIC_DISBURSE_ID_LIST)) {
			return (List<Long>) viewMap.get(PRDIC_DISBURSE_ID_LIST);
		}
		return new ArrayList<Long>(0);
	}

	public List<PeriodicDisbursementGridView> getPageView() {
		if (viewMap.containsKey(PRDIC_DISBURSE_VIEW)) {
			return (List<PeriodicDisbursementGridView>) viewMap.get(PRDIC_DISBURSE_VIEW);
		}
		return new ArrayList<PeriodicDisbursementGridView>(0);
	}

	public HtmlDataTable getPeriodicDisbursementTable() {
		return periodicDisbursementTable;
	}

	public void setPeriodicDisbursementTable(HtmlDataTable periodicDisbursementTable) {
		this.periodicDisbursementTable = periodicDisbursementTable;
	}

	public Date getRenewedDate() {
		return renewedDate;
	}

	public void setRenewedDate(Date renewedDate) {
		this.renewedDate = renewedDate;
	}
	
	public DomainDataView getStatusStopped() {
		if( viewMap.containsKey(WebConstants.PeriodicListStatus.LIST_STATUS_STOPPED) ) {
			return (DomainDataView) viewMap.get(WebConstants.PeriodicListStatus.LIST_STATUS_STOPPED);
		}
		return null;
	}
	
	public String getStatusRenewalRequiredId() {
		if( viewMap.containsKey(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWAL_REQ) ) {
			return (String) viewMap.get(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWAL_REQ);
		}
		return StringHelper.EMPTY;
	}
	
	public DomainDataView getStatusRenewed() {
		if( viewMap.containsKey(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWED) ) {
			return (DomainDataView) viewMap.get(WebConstants.PeriodicListStatus.LIST_STATUS_RENEWED);
		}
		return null;
	}
	
	public DomainDataView getStatusActive() {
		if( viewMap.containsKey(WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE) ) {
			return (DomainDataView) viewMap.get(WebConstants.PeriodicListStatus.LIST_STATUS_ACTIVE);
		}
		return null;
	}
	
	public Boolean getIsAnyRecords() {
		return 0<getPageView().size();
	}
	

	public void addPeriodicDisbursementRequest(List<PeriodicDisbursementGridView> viewList) throws PimsBusinessException 
	{
		logger.logInfo("[addPeriodicDisbursementRequest(List<PeriodicDisbursementGridView>) .. Starts]");
		List<DomainDataView> domainDataList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView dtView = CommonUtil.getIdFromType(domainDataList, WebConstants.REQUEST_STATUS_NEW);
				
		RequestView reqView = new RequestView();
		reqView.setCreatedBy(getLoggedInUserId());
		reqView.setUpdatedBy(getLoggedInUserId());
		reqView.setCreatedOn(DateUtil.getCurrentDate());
		reqView.setUpdatedOn(DateUtil.getCurrentDate());
		reqView.setRequestDate(DateUtil.getCurrentDate());
		reqView.setRequestTypeId(WebConstants.MemsRequestType.PERIODIC_DISBURSEMENTS_ID);
		reqView.setStatusId(dtView.getDomainDataId());
		reqView.setIsDeleted(0L);
		reqView.setRecordStatus(1L);
		
		RequestService reqSrv = new RequestService();
		Long reqId = reqSrv.addRequest(reqView);

		Set<RequestDetailView> reqDetailViewList = new HashSet<RequestDetailView>();
		
		for(PeriodicDisbursementGridView singleRec : viewList) 
		{
			RequestDetailView reqDetailView = new RequestDetailView();		
			reqDetailView.setUpdatedBy(getLoggedInUserId());
			reqDetailView.setCreatedBy(getLoggedInUserId());
			reqDetailView.setUpdatedOn(DateUtil.getCurrentDate());
			reqDetailView.setCreatedOn(DateUtil.getCurrentDate());
			reqDetailView.setPeriodicDisburseId(singleRec.getPeriodicDisbursementId());
			reqDetailView.setIsDeleted(0L);
			reqDetailView.setRecordStatus(1L);
			reqDetailView.setRequestId(reqId);
			reqDetailViewList.add(reqDetailView);
		}
		reqSrv.addRequestDetails(reqDetailViewList);
		logger.logInfo("[addPeriodicDisbursementRequest(List<PeriodicDisbursementGridView>) .. Ends]");		
	}

	public boolean isRequestStatusCompleted() {
		
		if( viewMap.get(WebConstants.REQUEST_VIEW) != null )
		{
		 if( ((RequestView)viewMap.get(WebConstants.REQUEST_VIEW)).getStatusDataValue().compareTo(WebConstants.REQUEST_STATUS_COMPLETE) == 0)
			 return true;
		 else
			 return false;
		}
		return false;
	}

	public void setRequestStatusCompleted(boolean requestStatusCompleted) {
		this.requestStatusCompleted = requestStatusCompleted;
	}

	public boolean isRequestStatusNew() {
		if( viewMap.get(WebConstants.REQUEST_VIEW) != null )
		{
		 if( ((RequestView)viewMap.get(WebConstants.REQUEST_VIEW)).getStatusDataValue().compareTo(WebConstants.REQUEST_STATUS_NEW) == 0)
			 return true;
		 else
			 return false;
		}
		return true;
	}

	public void setRequestStatusNew(boolean requestStatusNew) {
		this.requestStatusNew = requestStatusNew;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
