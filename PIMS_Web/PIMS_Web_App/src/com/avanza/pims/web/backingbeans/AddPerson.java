package com.avanza.pims.web.backingbeans;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.EmailValidator;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.SecUserUserGroupId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorAdd.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.EIDDataReader;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssignedPersonTypeView;
import com.avanza.pims.ws.vo.BlackListView;
import com.avanza.pims.ws.vo.BusinessActivityView;
import com.avanza.pims.ws.vo.CardData;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContactReferenceView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RolesView;
import com.avanza.pims.ws.vo.TradeLicenseIssuersVO;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
@SuppressWarnings( "unchecked" )
public class AddPerson extends AbstractController
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static SystemParameters parameters = SystemParameters.getInstance();
	public String PAGE_MODE_ADD="ADD";
	public String PAGE_MODE_UPDATE="UPDATE";
	public String PAGE_MODE_VIEW="VIEW";
	public String VIEW_MODE="VIEW_MODE";
	public String READ_ONLY_MODE="READ_ONLY_MODE";
	public String PERSON_VIEW="personView";
	public String pageMode;
//	public String DDV_PERSON_KIND_COMPANY="personKindCompany";
//	public String DDV_PERSON_KIND_INDIVIDUAL="personKindIndividual";
	DateFormat dateFormatter ;
	org.richfaces.component.html.HtmlTab tabPersonIndividual=new  org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabPersonCompany=new  org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabContactReferences=new  org.richfaces.component.html.HtmlTab();
	private HtmlDataTable dataTable = new HtmlDataTable();
	private HtmlDataTable propspectivePersonDataTable;
	private HtmlDataTable rolesDataTable=new HtmlDataTable();
	private HtmlDataTable contactInfoDataTable=new HtmlDataTable();
	HtmlSelectOneMenu cmb_SelectedPersonKind = new HtmlSelectOneMenu();
	private String grpCustomerNo;
	private String statusId;
	
	private String firstName  ;
	private String lastName ;
	private String fullNameEn ;
	private Long personTypeId;
	private Long personId;
	private Long contactInfoId;
	private String nationalityId;
	private String maritialStatusId;
	private String passportNumber;
	private String residenseVisaNumber;
	private String personalSecCardNo;
	private String drivingLicenseNumber;
	private String socialSecNumber;
	private String cellNumber;
	private String secCellNumber;
	private String designation;
	private String titleId;
	private String middleName;
	private String gender;
    private Date dateOfBirth;
    private Boolean isGrpCustomerNumberReadOnly;
    private Date passportIssueDate;
    private String passportIssuePlaceId;
    private String passportIssueCity;
    private Date passportExpiryDate;
    private Date residenseVidaExpDate;
    private Date residenseVisaIssueDate;
    private String residenseVisaIssuePlaceId;
    private String workingCompany;
    //For Contact Info
    private String address1;
    private String address2;
    private String street;
    private String postCode;
    private String cityId;
    private String countryId;
    private String homePhone;
    private String officePhone;
    private String fax;
    private String email;
    private String contactInfoCreatedBy;
    private String contactInfoCreatedOn;
    
    private String msgs;
    
    private String edit;
    
	private boolean showProspectiveGrid;
	//private Calendar occupiedTillDate = new Calendar();
	private PersonView personView = new PersonView();
	String controlName;
	String hdndisplaycontrolName;
	String controlForId;
	String requestForPersonType;
	String displayStylePersonTypeCombo;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    List<SelectItem> personTypeList =new ArrayList<SelectItem>();
    private List<ViolationView> violationsList = new ArrayList<ViolationView>(0);
    private List<BlackListView> blackListViewList = new ArrayList<BlackListView>(0);
    private boolean blackListed=false;
    private String txt_removalReason;
	public String selectManyPersonType;
	String sessionName; 
	//We have to look for the list population for facilities
	private List<String> successMessages;
	private List<String> errorMessages;
	private PersonView dataItem = new PersonView();
	private PersonView prospectiveDataItem = new PersonView();	
	private static Logger logger = Logger.getLogger( AddPerson.class );
	private List<PersonView> dataList = new ArrayList<PersonView>();
	private List<PersonView> prospectivePersonDataList = new ArrayList<PersonView>();
	private List<ContactInfoView> contactInfoList = new ArrayList<ContactInfoView>();
	
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> licenseIssuingEmirateList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	private List<SelectItem> tradeLicenseIssuersList = new ArrayList<SelectItem>();
	private List<SelectItem> tradeLicenseIssuersCity = new ArrayList<SelectItem>();
	private List<SelectItem> titleList = new ArrayList<SelectItem>();
	private Map<String, String> businessActivityList;
	private List<String> businessActivities;
	private List<SelectItem> personSubTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu cmbPersonType=new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbSubPersonType=new HtmlSelectOneMenu();
	private String selectedPersonType;
	private String selectedSubPersonType;
	private String selectedCountryId;
	private String selectedPersonKind;
	private String stateId;
	private String personSubTypeId;
	private String contactInfoRowId;
	private HtmlDataTable contactRefDataTable=new HtmlDataTable();
	private List<ContactReferenceView> contactRefList = new ArrayList<ContactReferenceView>();
	private List<SelectItem> CRStateList = new ArrayList<SelectItem>();
	private List<SelectItem> CRCityList = new ArrayList<SelectItem>();
	List<SelectItem> passportCityList  = new ArrayList<SelectItem>();
	String crContactInfoCreatedBy;
	String crContactInfoCreatedOn;
	String contactRefCreatedBy;
	String contactRefCreatedOn;
	
	String CRTitleId;
	String CRFirstName;
	String CRMiddleName;
	String CRLastName;
	String CRAddress1;
	String CRAddress2;
	String CRStreet;
	String CRPostCode;
	String CRCountryId;
	String CRStateId;
	String CRCityId;
	String CRHomePhone;
	String CROfficePhone;
	String CRFax;
	String CREmail;
	String CRCellNumber;
	
	String txtCompanyName;
	String txtGovtDept;
	String txtLicenseno;
	String licenseIssuePlaceId;
	
	String tradeLicenseIssuersId;
	Date licenseIssueDate;
	Date licenseExpiryDate;
	HttpServletRequest request =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	CommonUtil commonUtil = new CommonUtil();
	//	public Unit dataItem = new Unit();
	Map viewRootMap=getFacesContext().getViewRoot().getAttributes();
	private String ROLE_BEAN_LIST = "roleBeanList";
	private String PERSON_TYPE_LIST = "personTypeList";
	private String PERSON_SUB_TYPE_LIST = "personSubTypeList";
	private String CONTACT_INFO_LIST = "contactInfoList";
	private String CONTACT_INFO_ROW_INDEX = "rowIndex";
	private String SELECTED_CONTACT_INFO_ROW_INDEX= "selectedRowIndex";
	private String CONTACT_REF_LIST = "contactRefList";
	private String CONTACT_REF_ROW_INDEX = "crRowIndex";
	private String SELECTED_CONTACT_REF_ROW_INDEX= "selectedCRRowIndex";
	private String synchedWithEID;
	private String ef_idn_cn; 
	private String ef_mod_data;
	private String ef_non_mod_data;
	private String ef_sign_image;
	private String ef_home_address;
	private String ef_work_address;
	
	public String getSynchedWithEID() {
		return synchedWithEID;
	}
	public void setSynchedWithEID(String synchedWithEID) {
		this.synchedWithEID = synchedWithEID;
	}
	public String getEf_idn_cn() {
		return ef_idn_cn;
	}
	public void setEf_idn_cn(String ef_idn_cn) {
		this.ef_idn_cn = ef_idn_cn;
	}
	public String getEf_mod_data() {
		return ef_mod_data;
	}
	public void setEf_mod_data(String ef_mod_data) {
		this.ef_mod_data = ef_mod_data;
	}
	public String getEf_non_mod_data() {
		return ef_non_mod_data;
	}
	public void setEf_non_mod_data(String ef_non_mod_data) {
		this.ef_non_mod_data = ef_non_mod_data;
	}
	public String getEf_sign_image() {
		return ef_sign_image;
	}
	public void setEf_sign_image(String ef_sign_image) {
		this.ef_sign_image = ef_sign_image;
	}
	public String getEf_home_address() {
		return ef_home_address;
	}
	public void setEf_home_address(String ef_home_address) {
		this.ef_home_address = ef_home_address;
	}
	public String getEf_work_address() {
		return ef_work_address;
	}
	public void setEf_work_address(String ef_work_address) {
		this.ef_work_address = ef_work_address;
	}
	public String getDateFormat()
	{
		
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		return dateFormat;
	}
	public void btnRemoveBlackList_Click()
	{
		try
		{
			if(!hasRemoveBlackListError())
			{
				PersonView personView = (PersonView)viewRootMap.get(PERSON_VIEW);
				PropertyServiceAgent psa = new PropertyServiceAgent();
				psa.removeFromBlackList(personView.getPersonId(), this.getTxt_removalReason(), getLoggedInUser());
				List<BlackListView> blv = this.getBlackListViewList();
				BlackListView blackList = (BlackListView)blv.get(0);
				blackList.setRemovedBy(getLoggedInUser());
				blackList.setRemovedOn(new Date());
				blackList.setRemovingReason(this.getTxt_removalReason());
				this.setBlackListViewList(blv);
				this.setBlackListed(false);
				this.setTxt_removalReason("");
				this.getBlackListViewList();
			}
		}
		catch(Exception e)
		{
			logger.LogException( "btnRemoveBlackList_Click|Error Occured::", e);
			
		}
		
		
	}
	private boolean hasRemoveBlackListError()
	{
		errorMessages = new ArrayList<String>(0);
		if(hasOpenViolations())
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(("person.Msg.closeAllViolations")));
			return true;
		}
		if(this.getTxt_removalReason() == null || this.getTxt_removalReason().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(("person.Msg.removalReaonRequired")));
			return true;
		}
		return false;
	}
	private boolean hasOpenViolations()
	{
		boolean hasOpenViolations = false;
		List<DomainDataView>		ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
		DomainDataView ddViolationClosed = CommonUtil.getIdFromType(ddl, WebConstants.VIOLATION_STATUS_CLOSED);
		DomainDataView ddViolationRemoved    = CommonUtil.getIdFromType(ddl, WebConstants.VIOLATION_STATUS_REMOVED);
		for (ViolationView violationView : this.getViolationsList()) {
			if(violationView.getStatusId().compareTo(ddViolationRemoved.getDomainDataId() )==0 
			  || violationView.getStatusId().compareTo(ddViolationClosed.getDomainDataId() )==0 )
				hasOpenViolations=false;
			else
				return true;
		  	
		}
		
		return hasOpenViolations;
		
	}
	public void populateBlackLists()
	{
		try
		{
			PersonView personView = (PersonView)viewRootMap.get(PERSON_VIEW);
			PropertyServiceAgent psa = new PropertyServiceAgent();
			this.setBlackListViewList(psa.getBlackListViewByPersonId(personView.getPersonId()));
		}
		catch(Exception e)
		{
			logger.LogException("populateBlackLists|Error Occured::", e);
			
		}
			
		
	}
	public void populateViolations()
	{
		try
		{
			
			
			populateBlackLists();
			
			PersonView personView = (PersonView)viewRootMap.get(PERSON_VIEW);
			HashMap<String,Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.TENANT_ID, personView.getPersonId());
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<ViolationView> violationViewList =psa.getAllInspectionViolation(searchCriteria ,"dd/MM/yyyy");
			this.setViolationsList(violationViewList);
		}
		catch(Exception e)
		{
			logger.LogException("populateViolations|Error Occured::", e);
			
		}
			
	}
	private void populatePersonalInfo()
	{
		PersonView personToLoad =new PersonView();
	    if(viewRootMap.containsKey(PERSON_VIEW))
		 personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
	    else if (viewRootMap.get(WebConstants.PERSON_BASIC_INFO)!=null)
				   personToLoad = (PersonView)viewRootMap.get(WebConstants.PERSON_BASIC_INFO);
			
		if(personToLoad.getTitleId() != null)
			this.setTitleId(personToLoad.getTitleId().toString());
	    
		this.setFirstName(personToLoad.getFirstName());
		this.setLastName(personToLoad.getLastName());
		this.setMiddleName(personToLoad.getMiddleName());
		this.setGender(personToLoad.getGender());
		if(personToLoad.getMaritialStatusId()!=null)
		   this.setMaritialStatusId(personToLoad.getMaritialStatusId().toString());
		this.setDateOfBirth(personToLoad.getDateOfBirth());
		if(personToLoad.getNationalityId() != null)  
			this.setNationalityId(personToLoad.getNationalityId().toString());
		this.setPersonalSecCardNo(personToLoad.getPersonalSecCardNo());
		this.setDrivingLicenseNumber(personToLoad.getDrivingLicenseNumber());
		this.setSocialSecNumber(personToLoad.getSocialSecNumber());
		this.setCellNumber(personToLoad.getCellNumber());
		if(personToLoad.getSecCellNumber()!=null && personToLoad.getSecCellNumber().trim().length()>0)
			this.setSecCellNumber(personToLoad.getSecCellNumber());
		if(personToLoad.getPassportIssuePlaceId() != null)
		{
    	  this.setPassportIssuePlaceId(personToLoad.getPassportIssuePlaceId().toString());
    	  onPassportCountryChanged();
		}
		if(personToLoad.getPassportIssueCity() != null)
	    	  this.setPassportIssueCity(personToLoad.getPassportIssueCity().toString() );
      this.setPassportNumber(personToLoad.getPassportNumber());
      this.setPassportIssueDate(personToLoad.getPassportIssueDate());
      this.setPassportExpiryDate(personToLoad.getPassportExpiryDate());

      if(personToLoad.getResidenseVisaIssuePlaceId() != null)
    	  this.setResidenseVisaIssuePlaceId(personToLoad.getResidenseVisaIssuePlaceId().toString());
	  this.setResidenseVisaNumber(personToLoad.getResidenseVisaNumber());
      this.setResidenseVisaIssueDate(personToLoad.getResidenseVisaIssueDate());
      this.setResidenseVidaExpDate(personToLoad.getResidenseVidaExpDate());
      this.setWorkingCompany(personToLoad.getWorkingCompany());
      this.setDesignation(personToLoad.getDesignation());	      
      
      if(personToLoad.getPersonSubTypeId() != null)
    	  this.setPersonSubTypeId(personToLoad.getPersonSubTypeId().toString());
		
	}
	private void populateRoles()
	{
		PersonView personToLoad =new PersonView();
		
	    if(viewRootMap.containsKey(PERSON_VIEW))
		 personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
	    
	    List<RolesView> rvList = new ArrayList<RolesView>(0);
	    if(personToLoad.getAssignedPersonTypeViewSet()!=null && personToLoad.getAssignedPersonTypeViewSet().size()>0)
	    {
	    	
		    Iterator<AssignedPersonTypeView> aptViewIter =personToLoad.getAssignedPersonTypeViewSet().iterator();
		    while(aptViewIter.hasNext())
		    {
		    	RolesView roleView =new RolesView();
		    	AssignedPersonTypeView aptView =(AssignedPersonTypeView)aptViewIter.next();
		    	roleView.setAssignedPersonTypeId(aptView.getAssignedPersonTypeId());
		    	
		    	if(aptView.getPersonTypeId()!=null)
		    	{
		    		String typeDesc="";
		    		if(getIsArabicLocale())
		    			typeDesc=aptView.getPersonTypeAr();
		    		else if(getIsEnglishLocale())
		    			typeDesc=aptView.getPersonTypeEn();
		    		
		    		roleView.setTypeId(aptView.getPersonTypeId().toString());
		    	    roleView.setTypeDesc(typeDesc);
		    	}
		    	if(aptView.getPersonSubTypeId()!=null )
		    	{
		    		String subTypeDesc="";
		    		if(getIsArabicLocale())
		    			subTypeDesc=aptView.getPersonSubTypeAr();
		    		else if(getIsEnglishLocale())
		    			subTypeDesc=aptView.getPersonSubTypeEn();
		    	    
		    		roleView.setSubTypeId(aptView.getPersonSubTypeId().toString());
		    	    roleView.setSubTypeDesc(subTypeDesc);
		    	}
		    	rvList.add(roleView);
		    	
		    }
		    viewRootMap.put(ROLE_BEAN_LIST,rvList);
		    	    
	    }
	}
	public Map<String, String> getBusinessActivityList() {
		if (viewRootMap.get(Keys.BUSINESS_ACTIVITY_LIST) != null)
			businessActivityList = (Map<String, String>) viewRootMap
					.get(Keys.BUSINESS_ACTIVITY_LIST);

		return businessActivityList;
	}

	public void setBusinessActivityList(Map<String, String> businessActivityList) {
		this.businessActivityList = businessActivityList;
	}

	private void populateCompanyInfo()
	{
		PersonView personToLoad =new PersonView();
	    if(viewRootMap.containsKey(PERSON_VIEW))
		 personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
	    else if (viewRootMap.get(WebConstants.PERSON_BASIC_INFO)!=null)
			   personToLoad = (PersonView)viewRootMap.get(WebConstants.PERSON_BASIC_INFO);
		
	    this.setTxtCompanyName(personToLoad.getCompanyName());
	    this.setTxtGovtDept(personToLoad.getGovtDepartment());
	    this.setTxtLicenseno(personToLoad.getLicenseNumber());
	    this.setLicenseExpiryDate(personToLoad.getLicenseExpiryDate());
	    this.setLicenseIssueDate(personToLoad.getLicenseIssueDate());
	    if(personToLoad.getLicenseIssuePlaceId()!=null)
	    {
	    	this.setLicenseIssuePlaceId(personToLoad.getLicenseIssuePlaceId().toString());
	    	onLicenseIssueingEmirateChanged();
	    }
	    if(	
	    		personToLoad.getLicenseIssuingAuthority() !=null
	      )
	    {
		    this.setTradeLicenseIssuersId( personToLoad.getLicenseIssuingAuthority().toString());
	    }
	    this.setBusinessActivities(personToLoad.getBusinessActivities());
	}
	public List<SelectItem> loadTradeLicenseIssuersList(String regionId) throws Exception 
	{

		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		List<TradeLicenseIssuersVO> list = UtilityService.getTradeLicenseIssuers(regionId, getIsEnglishLocale()?"en":"ar");
		for (TradeLicenseIssuersVO type : list) 
		{
				selectItems.add(new SelectItem(
															type.getIssuerId().toString(), 
															getIsEnglishLocale() ? type.getIssuerNameEn(): type.getIssuerNameAr() )
											  );
		}
		setTradeLicenseIssuersList(selectItems);
		return selectItems;

	}

	@SuppressWarnings("unchecked")
	private void populateContactInfo()
	{
		PersonView personToLoad =new PersonView();
		contactInfoList.clear();
	    if(viewRootMap.containsKey(PERSON_VIEW))
		 personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
	    if (personToLoad.getContactInfoViewSet()!= null && personToLoad.getContactInfoViewSet().size()>0)
	    {
	    	  contactInfoList.addAll(personToLoad.getContactInfoViewSet());
	    	  //Done so that row id of new contact info added shud be greater then previous row ids
	    	  viewRootMap.put(CONTACT_INFO_ROW_INDEX,personToLoad.getContactInfoViewSet().size()+1);
	  		  viewRootMap.put(CONTACT_INFO_LIST,contactInfoList);
	    }
	}
	@SuppressWarnings("unchecked")
	private void populateContactReferences()
	{
		PersonView personToLoad =new PersonView();
		contactRefList.clear();
	    if(viewRootMap.containsKey(PERSON_VIEW))
		 personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
	    if (personToLoad.getContactReferenceViewSet()!= null && personToLoad.getContactReferenceViewSet().size()>0)
	    {
	    	  contactRefList.addAll(personToLoad.getContactReferenceViewSet());
	    	  //Done so that row id of new contact reference added shud be greater then previous row ids
	    	  viewRootMap.put(CONTACT_REF_ROW_INDEX,personToLoad.getContactReferenceViewSet().size()+1);
	  		  viewRootMap.put(CONTACT_REF_LIST,contactRefList);
	  		  setContactRefList(contactRefList);
	    }
	}
	@SuppressWarnings("unchecked")
	public void loadEdit()
	{
		try 
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			PersonView personToLoad =new PersonView();
			if (viewRootMap.get(WebConstants.PERSON_BASIC_INFO)!=null)
				   personToLoad = (PersonView)viewRootMap.get(WebConstants.PERSON_BASIC_INFO);
			else if(!viewRootMap.containsKey(PERSON_VIEW))
			{
				if(getRequestParam(WebConstants.PERSON_ID)!=null )
				    this.setPersonId((Long)getRequestParam(WebConstants.PERSON_ID));
				else if(request.getParameter(WebConstants.PERSON_ID)!=null)
					this.setPersonId(new Long(request.getParameter(WebConstants.PERSON_ID).toString()));
				personToLoad = psa.getPersonInformation(personId);
			    viewRootMap.put(PERSON_VIEW, personToLoad);
			}
			else if(viewRootMap.get(PERSON_VIEW)!=null)
   	           personToLoad = (PersonView)viewRootMap.get(PERSON_VIEW);
			
			loadAttachmentsAndComments( personToLoad.getPersonId() );
			if(personToLoad.getGrpCustomerNo()!=null && personToLoad.getGrpCustomerNo().trim().length()>0)
				this.setGrpCustomerNo(personToLoad.getGrpCustomerNo());
			//For some of the migrated data the grp customer number is not present.So this check will make the GRP Customer Number TextBox 
			//editable for the user to enter the grp number for the customer
			else 
			{
				this.setIsGrpCustomerNumberReadOnly( false );
			}
			if(personToLoad.getSynchedWithEID()!=null && personToLoad.getSynchedWithEID().compareTo("1")==0)
				this.setSynchedWithEID("1");
			if(personToLoad.getIsBlacklisted()!=null && personToLoad.getIsBlacklisted().compareTo(1L)==0)
				this.setBlackListed(true);
			if(this.getStatusId()!=null )
				this.setStatusId(this.getStatusId().toString());
			
//		    DomainDataView ddvPersonKindCompany= (DomainDataView )viewRootMap.get(DDV_PERSON_KIND_COMPANY);
//		    DomainDataView ddvPersonKindIndividual= (DomainDataView )viewRootMap.get(DDV_PERSON_KIND_INDIVIDUAL);
		    populateRoles();
		    if(personToLoad.getIsCompany().compareTo(new Long(1))==0)
		    {
		    	populateCompanyInfo();
		    	this.setSelectedPersonKind(WebConstants.PERSON_KIND_COMPANY_ID.toString());
		    }
		    else
		    {
		    	populatePersonalInfo();
		    	this.setSelectedPersonKind(WebConstants.PERSON_KIND_INDIVIDUAL_ID.toString());
		    }
		     populateContactInfo();
		     populateContactReferences();
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("loadEdit|Error Occured",e);
		}
		 catch (Exception e) 
		 {
			logger.LogException("loadEdit|Error Occured",e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadPersonTypeCombos()
	{
		List<DomainDataView> ddList=new ArrayList<DomainDataView>(0);
		if(!viewRootMap.containsKey(WebConstants.PERSON_TYPE))
		{
		  ddList= CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE);
		  viewRootMap.put(WebConstants.PERSON_TYPE,ddList);
		}
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPersonTypeList() 
	{
		personTypeList.clear();
		if(!viewRootMap.containsKey(WebConstants.PERSON_TYPE))
			loadPersonTypeCombos();
		List<DomainDataView> ddvList= (	ArrayList<DomainDataView>)viewRootMap.get(WebConstants.PERSON_TYPE);
		SelectItem item ;

		String dataDesc="";
		    for (DomainDataView domainDataView : ddvList) 
			{
				    dataDesc="";
					if(getIsEnglishLocale())	
					{
						dataDesc = domainDataView.getDataDescEn();
					}
					else if(getIsArabicLocale())
					{
						dataDesc = domainDataView.getDataDescAr();
					}

					{
			    	 		item = new SelectItem(domainDataView.getDomainDataId().toString(), 
			    	 				              dataDesc);
			    	 		personTypeList.add(item);
					}
					
			}
		    Collections.sort(personTypeList,ListComparator.LIST_COMPARE);
			viewRootMap.put(PERSON_TYPE_LIST,personTypeList);
			return personTypeList;
	}		
		
	@SuppressWarnings("unchecked")
	private void loadTenantSubTypes()
	{
		if(!viewRootMap.containsKey(WebConstants.TENANT_TYPE ))
		{
			List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE); 
	    	viewRootMap.put(WebConstants.TENANT_TYPE , ddvList );
		}
	}
	@SuppressWarnings("unchecked")
	private void loadOwnerSubTypes()
	{
		if(!viewRootMap.containsKey(WebConstants.OWNER_TYPE))
		{
			List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.OWNER_TYPE); 
	    	viewRootMap.put(WebConstants.OWNER_TYPE, ddvList );
		}
		
	}
	@SuppressWarnings("unchecked")
	private void loadRepresentativeSubTypes()
	{
		if(!viewRootMap.containsKey(WebConstants.REPRESENTATIVE_TYPE))
		{
			List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REPRESENTATIVE_TYPE); 
	    	viewRootMap.put(WebConstants.REPRESENTATIVE_TYPE, ddvList );
		}
		
	}
	
	@SuppressWarnings("unchecked")	
   public List<RolesView> getRolesList()
   {
	   
	   List<RolesView> rolesList = new ArrayList<RolesView>(0); 
	   if(viewRootMap.containsKey(ROLE_BEAN_LIST))
		   rolesList = (ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST);
      return rolesList;
	   
   }
   public int getSelectedPersonFromList(List<RolesView> roleBeanList,RolesView rv)
   {
		 
		for (int i=0;i<roleBeanList.size();i++ ) 
		{
			RolesView rolesView =roleBeanList.get(i);
		  if(rolesView.getTypeId().equalsIgnoreCase(selectedPersonType))
		  {
			  rv.setAssignedPersonTypeId(rolesView.getAssignedPersonTypeId());
			  return i;
		  }
		}
		return -1;
   }
   @SuppressWarnings("unchecked")
   public void addByDefaultToAllRoles()
   {
		RolesView rolesView =new RolesView();
		List<RolesView> roleBeanList=new ArrayList<RolesView>(0);
		if(viewRootMap.containsKey(ROLE_BEAN_LIST))
		  roleBeanList=(ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST); 
		List<SelectItem> ddPersonType = (ArrayList<SelectItem>)viewRootMap.get(PERSON_TYPE_LIST);
		for (SelectItem item : ddPersonType) 
		{
		  rolesView =new RolesView();
		  rolesView.setTypeDesc(item.getLabel());
    	  rolesView.setTypeId(item.getValue().toString());
    	  rolesView.setSubTypeId("-1");
		  rolesView.setSubTypeDesc("N/A");
		  roleBeanList.add(rolesView);
		}
		viewRootMap.put(ROLE_BEAN_LIST,roleBeanList);
	}
   @SuppressWarnings("unchecked")
   public void addRoleToPerson()
   {
		RolesView rolesView =new RolesView();
		List<RolesView> roleBeanList=new ArrayList<RolesView>(0);
		if(viewRootMap.containsKey(ROLE_BEAN_LIST))
		  roleBeanList=(ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST); 
		if(selectedPersonType!=null && !selectedPersonType.equals("-1"))
		{
			
			rolesView.setTypeId(selectedPersonType);

			List<SelectItem> ddPersonType = (ArrayList<SelectItem>)viewRootMap.get(PERSON_TYPE_LIST);
			
			for (SelectItem item : ddPersonType) 
			{
				if(selectedPersonType.equals(item.getValue().toString()))
				{
			      
			    	  rolesView.setTypeDesc(item.getLabel());
			      
				}
				
			}
			if(selectedSubPersonType!=null && !selectedSubPersonType.equals("-1"))
			{
				rolesView.setSubTypeId(selectedSubPersonType);
				ddPersonType = (ArrayList<SelectItem>)viewRootMap.get(PERSON_SUB_TYPE_LIST);
				
				for (SelectItem item : ddPersonType) 
				{
					if(selectedSubPersonType.equals(item.getValue().toString()))
					{
				      
				    	  rolesView.setSubTypeDesc(item.getLabel());
				      
					}
					
				}
			}
			else
			{
				rolesView.setSubTypeId("-1");
				rolesView.setSubTypeDesc("N/A");
				
			}
			int i =getSelectedPersonFromList(roleBeanList,rolesView);
			if(i>=0)
				roleBeanList.set(i, rolesView);
			else
				roleBeanList.add(rolesView);
			//Collections.sort
			
		}

		//roleBeanList.add(rolesView);
		viewRootMap.put(ROLE_BEAN_LIST,roleBeanList);
	}
   @SuppressWarnings("unchecked")
	public void imgDeleteRemoveRoleFromPerson_Click()
	{
		RolesView roleView = (RolesView)rolesDataTable.getRowData() ;
		List<RolesView> newRoleBeanList= new ArrayList<RolesView>(0);
		List<RolesView> oldRoleBeanList=(ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST);
		for (RolesView rolesView : oldRoleBeanList) {
			if(roleView.getTypeId().compareTo(rolesView.getTypeId())!=0)
				newRoleBeanList.add(rolesView);
		}
		viewRootMap.put(ROLE_BEAN_LIST,newRoleBeanList);

	}
    public Boolean getIsRoleDeleteIconRendered()
    {
		Boolean isRoleDeleteIconRendered=true;
		try
		{
		if(rolesDataTable!=null && rolesDataTable.getRowCount()>0 && rolesDataTable.getRowData()!=null)
    	{
	    	RolesView roleView = (RolesView)rolesDataTable.getRowData() ;
	    	if(roleView.getAssignedPersonTypeId()!=null)
	    		isRoleDeleteIconRendered=false;
    	}
		}
		catch (Exception ex)
		{
		  logger.LogException("getIsRoleDeleteIconRendered|Error Occured..", ex);
		}
    	return isRoleDeleteIconRendered;
    	
    }
    public void imgRemoveContactInfo_Click()
    {
 		ContactInfoView contactInfoView = (ContactInfoView)contactInfoDataTable.getRowData();
 		List<ContactInfoView> newContactInfoViewList=new ArrayList<ContactInfoView>(0);
 		List<ContactInfoView> oldContactInfoViewList=new ArrayList<ContactInfoView>(0);
 		if(viewRootMap.containsKey(CONTACT_INFO_LIST))
 			oldContactInfoViewList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
 		for (ContactInfoView contactInfoView2 : oldContactInfoViewList) 
 		{
			if(contactInfoView2.getRowId().equals(contactInfoView.getRowId()))
				contactInfoView.setIsDeleted(new Long(1));
			else
				newContactInfoViewList.add(contactInfoView2);
 		}
 		viewRootMap.put(CONTACT_INFO_LIST,newContactInfoViewList);
    }
    public void btnAddContactInfo_Click()
    {
 		ContactInfoView contactInfoView = new ContactInfoView();
 		List<ContactInfoView> contactInfoViewList=new ArrayList<ContactInfoView>(0);
 		if(viewRootMap.containsKey(CONTACT_INFO_LIST))
 		  contactInfoViewList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
 		DateFormat df=new SimpleDateFormat(getDateFormat());
 		try
 		{
 		if(!getIsContactInfoHasErrors())
 		{
 			
 			
  		    contactInfoView.setAddress1(this.getAddress1().trim());
 		    contactInfoView.setAddress2(this.getAddress2().trim());
 		    String fullAddress = this.getAddress1().trim();
 		    if(this.getAddress2()!=null && this.getAddress2().trim().length()>0)
 		    	fullAddress += " "+this.getAddress2().trim();
 		   contactInfoView.setFullAddress(fullAddress);
 			if (this.getCountryId() != null && this.getCountryId().length()>0 && !this.getCountryId().equals("-1"))
			contactInfoView.setCountryId(new Long(this.getCountryId()));
			if (this.getStateId() != null && this.getStateId().length()>0 && !this.getStateId().equals("-1"))
			contactInfoView.setStateId(new Long(this.getStateId()));
			if (this.getCityId()!= null && this.getCityId().length()>0 && !this.getCityId().equals("-1"))
			contactInfoView.setCityId(new Long(this.getCityId()));
			if(this.getEmail()!=null && this.getEmail().trim().length()>0)
			contactInfoView.setEmail(this.getEmail().trim());
			if(this.getFax() !=null && this.getFax().trim().length()>0)
			contactInfoView.setFax(this.getFax());
			if(this.getHomePhone() !=null && this.getHomePhone().trim().length()>0)
			contactInfoView.setHomePhone(this.getHomePhone());
			if(this.getOfficePhone() !=null && this.getOfficePhone().trim().length()>0)
			contactInfoView.setOfficePhone(this.getOfficePhone());
			if(this.getPostCode()!=null && this.getPostCode().trim().length()>0)
			contactInfoView.setPostCode(this.getPostCode());
			if(this.getStreet() !=null && this.getStreet().trim().length()>0)
			contactInfoView.setStreet(this.getStreet());
			
			contactInfoView.setUpdatedBy(getLoggedInUser());
			contactInfoView.setUpdatedOn(new Date());
			if(this.getContactInfoCreatedBy()!= null && this.getContactInfoCreatedBy().trim().length()>0)
				contactInfoView.setCreatedBy(this.getContactInfoCreatedBy());
			else
			contactInfoView.setCreatedBy(getLoggedInUser());
			if(this.getContactInfoCreatedOn()!= null && this.getContactInfoCreatedOn().trim().length()>0)
				contactInfoView.setCreatedOn(df.parse(this.getContactInfoCreatedOn()));
			else
				contactInfoView.setCreatedOn(new Date());
			
			contactInfoView.setIsDeleted(0L);
			contactInfoView.setRecordStatus(1L);
			int a=-1; 
			if(viewRootMap.containsKey(SELECTED_CONTACT_INFO_ROW_INDEX) && viewRootMap.get(SELECTED_CONTACT_INFO_ROW_INDEX)!=null)
			 {
			  a=getContactInfoFromListForRowId(contactInfoViewList,new Long(viewRootMap.get(SELECTED_CONTACT_INFO_ROW_INDEX).toString()),contactInfoView);
			  contactInfoView.setRowId(new Long(viewRootMap.get(SELECTED_CONTACT_INFO_ROW_INDEX).toString()));
			  viewRootMap.remove(SELECTED_CONTACT_INFO_ROW_INDEX);
			 }
			if(a>=0)
			{
				
			 contactInfoViewList.set(a,contactInfoView);
			}
			else
			{
				if(viewRootMap.containsKey(CONTACT_INFO_ROW_INDEX) && viewRootMap.get(CONTACT_INFO_ROW_INDEX)!=null)
				{
				 contactInfoView.setRowId(new Long(viewRootMap.get(CONTACT_INFO_ROW_INDEX).toString())+1 );
				}
				else
				  contactInfoView.setRowId(new Long(0));
				viewRootMap.put(CONTACT_INFO_ROW_INDEX,contactInfoView.getRowId());
				 contactInfoViewList.add(contactInfoView);
			}
			viewRootMap.put(CONTACT_INFO_LIST,contactInfoViewList );
			clearContactInfoFields();
 		}
 		
 		}
 		catch(Exception ex)
 		{
 		 logger.LogException("btnAddContactInfo_Click|Error Occured",ex);
		}
 	}
 	public int getContactInfoFromListForRowId(List<ContactInfoView> CIList,Long rowId,ContactInfoView ciView)
 	{
 	
 	for (int i=0;i<CIList.size();i++) {
 	ContactInfoView contactInfoView =(ContactInfoView)CIList.get(i);
		if(contactInfoView.getRowId().compareTo(rowId)==0)
		{
		    ciView.setContactInfoId(contactInfoView.getContactInfoId());
		    ciView.setPersonContactInfoId(contactInfoView.getPersonContactInfoId());
			return i;
		}
		
	}
     return -1;    	
 	}
    private void clearContactInfoFields()
    {
    	 this.setAddress1("");
    	 this.setAddress2("");
    	 this.setCountryId("-1");
    	 this.setStateId( "-1");
    	 this.setCityId("-1");
    	 this.setEmail("");
    	 this.setFax("");
    	 this.setHomePhone("");
    	 this.setOfficePhone("");
    	 this.setPostCode("");
    	 this.setStreet("");
    	 this.setContactInfoCreatedBy("");
    	 this.setContactInfoCreatedOn("");
    	 this.setContactInfoRowId("");
    	
    }
    public void btnPopulateContactInfoFromList_Click()
    {
    	ContactInfoView contactInfoView =(ContactInfoView)contactInfoDataTable.getRowData();
    	viewRootMap.put(SELECTED_CONTACT_INFO_ROW_INDEX, contactInfoView.getRowId());
    			
    	DateFormat df=new SimpleDateFormat(getDateFormat());
    	this.setContactInfoRowId(contactInfoView.getRowId().toString() );
    	 this.setAddress1(contactInfoView.getAddress1());
    	 this.setAddress2(contactInfoView.getAddress2());
		if (contactInfoView.getCountryId()!= null )
		{
		 this.setCountryId(contactInfoView.getCountryId().toString());
		 this.loadContactInfoState();
		}
		if (contactInfoView.getStateId()!= null )
		{
			this.setStateId(contactInfoView.getStateId().toString());
			this.loadContactInfoCity();
		}
		if (contactInfoView.getCityId()!=null)
			this.setCityId(contactInfoView.getCityId().toString());
		if (contactInfoView.getEmail()!= null && contactInfoView.getEmail().length()>0)
			this.setEmail(contactInfoView.getEmail());
		if(contactInfoView.getFax()!=null && contactInfoView.getFax().length()>0)
			this.setFax( contactInfoView.getFax());
		if(contactInfoView.getHomePhone() !=null && contactInfoView.getHomePhone().length()>0)
			this.setHomePhone( contactInfoView.getHomePhone());
		if(contactInfoView.getOfficePhone() !=null && contactInfoView.getOfficePhone().length()>0)
			this.setOfficePhone( contactInfoView.getOfficePhone());
		if(contactInfoView.getPostCode() !=null && contactInfoView.getPostCode().length()>0)
			this.setPostCode(contactInfoView.getPostCode());
		if(contactInfoView.getStreet()!=null && contactInfoView.getStreet().length()>0)
			this.setStreet(contactInfoView.getStreet());
		
		this.setContactInfoCreatedBy(contactInfoView.getCreatedBy());
		this.setContactInfoCreatedOn(df.format(contactInfoView.getCreatedOn()));
		

    	
    }
    public Boolean getIsContactInfoHasErrors()
    {
    	boolean hasErrors=false;
    	EmailValidator emailValidator = EmailValidator.getInstance();
    	errorMessages = new ArrayList<String>(0);
    	if(this.getAddress1()==null || this.getAddress1().trim().length()<=0)
    	{
    		
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_ADDRESS_INFO_REQ)));
    		hasErrors = true;
    	}
    	
    	 	
		if (this.getEmail() == null || this.getEmail().trim().length() <= 0 && viewMap.get(WebConstants.IS_EMAIL_MANDATORY)!=null)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_EMAIL_REQUIRED)));
			hasErrors = true;
		}

    	
    	if(this.getEmail()!=null && this.getEmail().trim().length()>0 &&  !emailValidator.isValid(this.getEmail()))
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_INVALID_EMAIL)));
    		hasErrors = true;
    	}
    	if(
    			this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_COMPANY_ID.toString())==0 && 
    			( this.getPostCode()==null || this.getPostCode().trim().length()<=0)
    			
    	  )
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("person.Msg.CompanyPostalCodeRequired"));
    		hasErrors = true;
    	}
    	return hasErrors;
    }
    public void imgRemoveContactRef_Click()
    {
 		ContactReferenceView contactRefView = (ContactReferenceView)contactRefDataTable.getRowData();
 		List<ContactReferenceView> newContactRefViewList=new ArrayList<ContactReferenceView>(0);
 		List<ContactReferenceView> oldContactREFViewList=new ArrayList<ContactReferenceView>(0);
 		if(viewRootMap.containsKey(CONTACT_REF_LIST))
 			oldContactREFViewList=(List<ContactReferenceView>)viewRootMap.get(CONTACT_REF_LIST);
 		for (ContactReferenceView contactRefView2 : oldContactREFViewList) 
 		{
			if(contactRefView2.getRowId().compareTo(contactRefView.getRowId())==0 )
				contactRefView2.setIsDeleted(new Long(1));
			else
				newContactRefViewList.add(contactRefView2);
 		}
 		viewRootMap.put(CONTACT_REF_LIST,newContactRefViewList);
    }
    public void btnAddContactRef_Click()
    {
 		ContactReferenceView contactReferenceView = new ContactReferenceView();
 		ContactInfoView  contactInfoView= new ContactInfoView();
 		contactReferenceView.setContactInfoView(contactInfoView);
 		List<ContactReferenceView> contactRefViewList=new ArrayList<ContactReferenceView>(0);
 		if(viewRootMap.containsKey(CONTACT_REF_LIST))
 		  contactRefViewList=(List<ContactReferenceView>)viewRootMap.get(CONTACT_REF_LIST);
 		DateFormat df=new SimpleDateFormat(getDateFormat());
 		try
 		{
 		if(!getIsContactRefHasErrors())
 		{
 			if(this.getCRFirstName()!= null && this.getCRFirstName().trim().length()>0)
 				contactReferenceView.setFirstName(this.getCRFirstName().trim());
 			if(this.getCRMiddleName()!= null && this.getCRMiddleName().trim().length()>0)
 				contactReferenceView.setMiddleName(this.getCRMiddleName().trim());
 			if(this.getCRLastName()!= null && this.getCRLastName().trim().length()>0)
 				contactReferenceView.setLastName(this.getCRLastName().trim());
 			if(this.getCRTitleId()!= null && this.getCRTitleId().trim().length()>0)
 				contactReferenceView.setTitle(this.getCRTitleId());
 				//contactReferenceView.setTitleId(new Long(this.getCRTitleId().trim()));
 			
 			
 			contactReferenceView.setFullName((this.getCRFirstName()!= null && this.getCRFirstName().trim().length()>0? this.getCRFirstName().trim():"")+" "+
                    (this.getCRMiddleName()!= null && this.getCRMiddleName().trim().length()>0?this.getCRMiddleName().trim():"")+" "+
		             (this.getCRLastName()!= null && this.getCRLastName().trim().length()>0?this.getCRLastName().trim():"")
                   );
 		    contactReferenceView.getContactInfoView().setAddress1(this.getCRAddress1().trim());
 		    contactReferenceView.getContactInfoView().setAddress2(this.getCRAddress2().trim());
 		    String fullAddress = this.getCRAddress1().trim();
		    if(this.getCRAddress2()!=null && this.getCRAddress2().trim().length()>0)
		    	fullAddress += " "+this.getCRAddress2().trim();
		    
		    contactReferenceView.getContactInfoView().setFullAddress(fullAddress);
		    
 			if (this.getCRCountryId() != null && this.getCRCountryId().length()>0 && !this.getCRCountryId().equals("-1"))
 				contactReferenceView.getContactInfoView().setCountryId(new Long(this.getCRCountryId()));
			if (this.getCRStateId() != null && this.getCRStateId().length()>0 && !this.getCRStateId().equals("-1"))
				contactReferenceView.getContactInfoView().setStateId(new Long(this.getCRStateId()));
			if (this.getCRCityId()!= null && this.getCRCityId().length()>0 && !this.getCRCityId().equals("-1"))
				contactReferenceView.getContactInfoView().setCityId(new Long(this.getCRCityId()));
			if(this.getCREmail()!=null && this.getCREmail().trim().length()>0)
				contactReferenceView.getContactInfoView().setEmail(this.getCREmail().trim());
			if(this.getCRCellNumber()!= null && this.getCRCellNumber().trim().length()>0)
 				contactReferenceView.setCellNumber(this.getCRCellNumber().trim());
			if(this.getCRFax() !=null && this.getCRFax().trim().length()>0)
				contactReferenceView.getContactInfoView().setFax(this.getCRFax());
			if(this.getCRHomePhone() !=null && this.getCRHomePhone().trim().length()>0)
				contactReferenceView.getContactInfoView().setHomePhone(this.getCRHomePhone());
			if(this.getCROfficePhone() !=null && this.getCROfficePhone().trim().length()>0)
				contactReferenceView.getContactInfoView().setOfficePhone(this.getCROfficePhone());
			if(this.getCRPostCode()!=null && this.getCRPostCode().trim().length()>0)
				contactReferenceView.getContactInfoView().setPostCode(this.getCRPostCode());
			if(this.getCRStreet() !=null && this.getCRStreet().trim().length()>0)
				contactReferenceView.getContactInfoView().setStreet(this.getCRStreet());
			
			contactReferenceView.getContactInfoView().setUpdatedBy(getLoggedInUser());
			contactReferenceView.getContactInfoView().setUpdatedOn(new Date());
			if(this.getCrContactInfoCreatedBy()!= null && this.getCrContactInfoCreatedBy().trim().length()>0)
				contactReferenceView.getContactInfoView().setCreatedBy(this.getCrContactInfoCreatedBy());
			else
				contactReferenceView.getContactInfoView().setCreatedBy(getLoggedInUser());
			if(this.getCrContactInfoCreatedOn()!= null && this.getCrContactInfoCreatedOn().trim().length()>0)
				contactReferenceView.getContactInfoView().setCreatedOn(df.parse(this.getCrContactInfoCreatedOn()));
			else
				contactReferenceView.getContactInfoView().setCreatedOn(new Date());
			if(this.getContactRefCreatedBy()!=null && this.getContactRefCreatedBy().trim().length()>0)
				contactReferenceView.setCreatedBy(this.getContactRefCreatedBy());
			else
				contactReferenceView.setCreatedBy(getLoggedInUser());
			if(this.getContactRefCreatedOn()!= null && this.getContactRefCreatedOn().trim().length()>0)
				contactReferenceView.setCreatedOn(df.parse(this.getContactRefCreatedOn()));
			else
				contactReferenceView.setCreatedOn(new Date());
			contactReferenceView.setUpdatedOn(new Date());
			contactReferenceView.setUpdatedBy(getLoggedInUser());
			contactReferenceView.getContactInfoView().setIsDeleted(0L);
			contactReferenceView.getContactInfoView().setRecordStatus(1L);
			contactReferenceView.setIsDeleted(0L);
			contactReferenceView.setRecordStatus(1L);
			int a=-1; 
			if(viewRootMap.containsKey(SELECTED_CONTACT_REF_ROW_INDEX) && viewRootMap.get(SELECTED_CONTACT_REF_ROW_INDEX)!=null)
			 {
			  a=getContactRefFromListForRowId(contactRefViewList,new Long(viewRootMap.get(SELECTED_CONTACT_REF_ROW_INDEX).toString()),contactReferenceView);
			  contactReferenceView.setRowId(new Long(viewRootMap.get(SELECTED_CONTACT_REF_ROW_INDEX).toString()));
			  viewRootMap.remove(SELECTED_CONTACT_REF_ROW_INDEX);
			 }
			if(a>=0)
			{
				
				contactRefViewList.set(a,contactReferenceView);
			}
			else
			{
				if(viewRootMap.containsKey(CONTACT_REF_ROW_INDEX) && viewRootMap.get(CONTACT_REF_ROW_INDEX)!=null)
				{
					contactReferenceView.setRowId(new Long(viewRootMap.get(CONTACT_REF_ROW_INDEX).toString())+1 );
				}
				else
					contactReferenceView.setRowId(new Long(0));
				viewRootMap.put(CONTACT_REF_ROW_INDEX,contactReferenceView.getRowId());
				 contactRefViewList.add(contactReferenceView);
			}
			viewRootMap.put(CONTACT_REF_LIST,contactRefViewList );
			clearContactRefFields();
 		}
 		
 		}
 		catch(Exception ex)
 		{
 		 logger.LogException("btnAddContactRef_Click|Error Occured",ex);
		}
 	}
 	public int getContactRefFromListForRowId(List<ContactReferenceView> CRList,Long rowId,ContactReferenceView crv)
 	{
 	
 	for (int i=0;i<CRList.size();i++) {
 	ContactReferenceView contactRefView =(ContactReferenceView)CRList.get(i);
		if(contactRefView.getRowId().compareTo(rowId)==0)
		{
			crv.setContactReferenceId(contactRefView.getContactReferenceId());
			crv.getContactInfoView().setContactInfoId(contactRefView.getContactInfoView().getContactInfoId());
			return i;
		}
	}
     return -1;    	
 	}
    private void clearContactRefFields()
    {
    	 this.setCRCellNumber("");
         this.setCRFirstName("");
         this.setCRMiddleName("");
         this.setCRLastName("");
    	 this.setCRAddress1("");
    	 this.setCRAddress2("");
    	 this.setCRCountryId("-1");
    	 this.setCRStateId( "-1");
    	 this.setCRCityId("-1");
    	 this.setCREmail("");
    	 this.setCRFax("");
    	 this.setCRHomePhone("");
    	 this.setCROfficePhone("");
    	 this.setCRPostCode("");
    	 this.setCRStreet("");
    	 this.setCrContactInfoCreatedBy((""));
    	 this.setCrContactInfoCreatedOn("");
    	 this.setContactRefCreatedBy("");
    	 this.setContactRefCreatedOn("");
    	 this.setCRTitleId("-1");
    	
    }
    public void btnPopulateContactRefFromList_Click()
    {
    	ContactReferenceView contactRefView =(ContactReferenceView)contactRefDataTable.getRowData();
    	viewRootMap.put(SELECTED_CONTACT_REF_ROW_INDEX, contactRefView.getRowId());
    			
    	DateFormat df=new SimpleDateFormat(getDateFormat());
    	this.setContactInfoRowId(contactRefView.getRowId().toString() );
    	 this.setCRAddress1(contactRefView.getContactInfoView().getAddress1());
    	 this.setCRAddress2(contactRefView.getContactInfoView().getAddress2());
		if (contactRefView.getContactInfoView().getCountryId()!= null )
		{
		 this.setCRCountryId(contactRefView.getContactInfoView().getCountryId().toString());
		 this.loadContactRefState();
		
		}
		if (contactRefView.getContactInfoView().getStateId()!= null )
		{
			this.setCRStateId(contactRefView.getContactInfoView().getStateId().toString());
			this.loadContactRefCity();
		}
		if (contactRefView.getContactInfoView().getCityId()!=null)
			this.setCRCityId(contactRefView.getContactInfoView().getCityId().toString());
		if (contactRefView.getContactInfoView().getEmail()!= null && contactRefView.getContactInfoView().getEmail().length()>0)
			this.setCREmail(contactRefView.getContactInfoView().getEmail());
		if(contactRefView.getContactInfoView().getFax()!=null && contactRefView.getContactInfoView().getFax().length()>0)
			this.setCRFax( contactRefView.getContactInfoView().getFax());
		if(contactRefView.getContactInfoView().getHomePhone() !=null && contactRefView.getContactInfoView().getHomePhone().length()>0)
			this.setCRHomePhone( contactRefView.getContactInfoView().getHomePhone());
		if(contactRefView.getContactInfoView().getOfficePhone() !=null && contactRefView.getContactInfoView().getOfficePhone().length()>0)
			this.setCROfficePhone( contactRefView.getContactInfoView().getOfficePhone());
		if(contactRefView.getContactInfoView().getPostCode() !=null && contactRefView.getContactInfoView().getPostCode().length()>0)
			this.setCRPostCode(contactRefView.getContactInfoView().getPostCode());
		if(contactRefView.getContactInfoView().getStreet()!=null && contactRefView.getContactInfoView().getStreet().length()>0)
			this.setCRStreet(contactRefView.getContactInfoView().getStreet());
		
		this.setCrContactInfoCreatedBy(contactRefView.getContactInfoView().getCreatedBy());
		this.setCrContactInfoCreatedOn(df.format(contactRefView.getContactInfoView().getCreatedOn()));
		this.setContactRefCreatedBy(contactRefView.getCreatedBy());
		this.setContactRefCreatedOn(df.format(contactRefView.getCreatedOn()));
		this.setCRFirstName(contactRefView.getFirstName());
		this.setCRCellNumber(contactRefView.getCellNumber());
		this.setCRMiddleName(contactRefView.getMiddleName());
		this.setCRLastName(contactRefView.getLastName());
		if(contactRefView.getTitleId()!=null)
		this.setCRTitleId(contactRefView.getTitleId().toString());

    	
    }
    public Boolean getIsContactRefHasErrors()
    {
    	boolean hasErrors=false;
    	EmailValidator emailValidator = EmailValidator.getInstance();
    	errorMessages = new ArrayList<String>(0);
    	if(this.getCRTitleId()  ==null || this.getCRTitleId().trim().length()<=0)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_REF_TITLE)));
    		hasErrors = true;
    	}
    	if(this.getCRFirstName()  ==null || this.getCRFirstName().trim().length()<=0)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_REF_FIRST_NAME)));
    		hasErrors = true;
    	}
    	if(this.getCRLastName()  ==null || this.getCRLastName().trim().length()<=0)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_REF_LAST_NAME)));
    		hasErrors = true;
    	}  
    	if(this.getCRAddress1()==null || this.getCRAddress1().trim().length()<=0)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_REF_ADDRESS_INFO_REQ)));
    		hasErrors = true;
    	}
    	
    	if(this.getCREmail()!=null && this.getCREmail().trim().length()>0 &&  !emailValidator.isValid(this.getCREmail()))
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_REF_INVALID_EMAIL)));
    		hasErrors = true;
    	}
    	if(
    		( this.getCRCellNumber() == null || this.getCRCellNumber().trim().length() <=  0  ) &&
    	    ( this.getCRHomePhone()== null || this.getCRHomePhone().trim().length() <=  0     ) &&
    	    ( this.getCROfficePhone()== null || this.getCROfficePhone().trim().length() <=  0     ) 
    	   )
    	{
    		
    		errorMessages.add(ResourceUtil.getInstance().getProperty("common.error.validation.contactRefContactNumRequired"));
    		hasErrors = true;
    	}
    	return hasErrors;
    }
    
    
    @SuppressWarnings("unchecked")
    public void btnGenerateGRPCustomerNumber_Click( )
	{
    	successMessages=new ArrayList<String>(0);
    	try 
    	{
		    PropertyService ps = new PropertyService();
    		this.setGrpCustomerNo( ps.generateGRPCustomerNumber( this.getPersonId() ) );
    		this.setIsGrpCustomerNumberReadOnly( true );
	
    	} 
    	catch ( Exception e) 
    	{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("btnGenerateGRPCustomerNumber_Click| Error Occured..",e);
		}
    	
	}
    
    @SuppressWarnings("unchecked")
	public void onMessageFromReadEIDData()
	{
		try 
		{
			logger.logDebug("onMessageFromReadEIDData");
			
	        PersonView person = (PersonView)sessionMap.remove(WebConstants.Person.EID_CARD_PERSON_DETAILS);
	        
	        this.setFullNameEn(person.getFullNameEn() ); 	
	        this.setSocialSecNumber(person.getSocialSecNumber());
	        this.setCellNumber( person.getCellNumber() );
	        if( person.getFirstName()!= null  )
	        {
	        	this.setFirstName(person.getFirstName());
	        }
	        if( person.getLastName() != null )
	        {
	        	this.setLastName(person.getLastName());
	        }
	        if(person.getDateOfBirth() != null )
	    	{
	    		this.setDateOfBirth(person.getDateOfBirth());
	    	}
	        if( person.getPassportNumber() != null )
	        {
	        	this.setPassportNumber( person.getPassportNumber());
	        }
	        if( person.getNationalId() != null )
	        {
	        	this.setNationalityId(person.getNationalId());
	        }
	        if( person.getGender() != null )
	        {
	        	this.setGender(person.getGender() );
	        }
	        if(person.getContactInfoViewSet() != null && person.getContactInfoViewSet().size() > 0 )
	        {
	        	List<ContactInfoView> contactInfoViewList=new ArrayList<ContactInfoView>(0);
	        	if(viewRootMap.containsKey(CONTACT_INFO_LIST))
	     		{
	     		  contactInfoViewList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
	     		}
	        	contactInfoViewList.addAll( person.getContactInfoViewSet() );
	        	viewRootMap.put(CONTACT_INFO_LIST,contactInfoViewList);
	        }
	        if(this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_INDIVIDUAL_ID.toString())==0)
			{
	        	this.setSelectedPersonKind(WebConstants.PERSON_KIND_INDIVIDUAL_ID.toString()); 
//		    	putIndividualInfoInView(person);
			}
	        this.setSynchedWithEID("1");
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onMessageFromReadEIDData|Error Occured:", e);
		}
		
	}
    
    
    @SuppressWarnings("unchecked")
    public String btnSavePerson_Click(){
    	successMessages=new ArrayList<String>(0);
    	errorMessages = new ArrayList<String>(0);
    	try {
		    if (!hasErrors())
			{
				
		    	PropertyServiceAgent psa = new PropertyServiceAgent();
		    	PersonView personToAdd = new PersonView();
		    	if(this.getPersonId() != null)
		    	personToAdd = psa.getPersonInformation(this.getPersonId());
				
		    	putControlValuesInView(personToAdd);
		    	if(viewRootMap.get("loginId") != null)
		    	{
					personToAdd.setLoginId( viewRootMap.get("loginId").toString() );
		    	}
				personToAdd = psa.addPerson(personToAdd);
				this.setPersonId(personToAdd.getPersonId());
				saveAttachmentsAndNotes(personToAdd.getPersonId());
				viewRootMap.put(PERSON_VIEW,personToAdd);
//				if(viewRootMap.containsKey(WebConstants.PERSON_BASIC_INFO))
//				This code was written for change Tenant				
				boolean isManyPerson=false;
				if(viewRootMap.containsKey("MANY_PERSON") && viewRootMap.get("MANY_PERSON")!=null)
					isManyPerson=(Boolean) viewRootMap.get("MANY_PERSON");
				if(getIsViewModePopUp()&& !isManyPerson)
				{
					sendToParent(personToAdd);
					return "";
				}
				loadEdit();
				if (edit == null)
					clearAll();
				
				if(viewRootMap.containsKey("loginId"))
				{
						successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_ID_GENERATED));
						viewRootMap.remove("loginId");
				}
				else
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_ADDED_SUCESS));
			}
			
		} 
    	catch (PimsBusinessException e) 
    	{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "btnSavePerson_Click| Error Occured..",e);
		}
    	catch (Exception e) 
    	{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "btnSavePerson_Click| Error Occured..",e);
		}
		return "";
	}
    @SuppressWarnings("unchecked")
    private void sendToParent(PersonView person)
    {
    	FacesContext facesContext = FacesContext.getCurrentInstance();
    	String personType="";
    	if( viewRootMap.get("REQUESTED_PERSON_TYPE") != null)
    	{
    		personType=viewRootMap.get("REQUESTED_PERSON_TYPE").toString();
    	}
    	String javaScriptText = "window.opener.populatePerson('"+personType+"','"+person.getPersonFullName()+"','"+person.getPersonId()+"','"+
    	                         person.getPassportNumber()+"','"+person.getCellNumber()+"',''"+",'"+person.getIsCompany()+"');window.close();";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
	    if(viewRootMap.containsKey(WebConstants.PERSON_BASIC_INFO))
	    	sessionMap.put(WebConstants.PERSON_BASIC_INFO, person);
    	
    }
    @SuppressWarnings("unchecked")
	private void putControlValuesInView(PersonView personToAdd) 
    {
//		DomainDataView ddvPersonKindCompany= (DomainDataView)viewRootMap.get(DDV_PERSON_KIND_COMPANY);
//		DomainDataView ddvPersonKindIndividual= (DomainDataView )viewRootMap.get(DDV_PERSON_KIND_INDIVIDUAL);
		
		if( synchedWithEID!= null && synchedWithEID.equals("1"))
		{
			personToAdd.setSynchedWithEID("1");
		}
		else
		{
			personToAdd.setSynchedWithEID("0");
		}
		if(this.getGrpCustomerNo()!=null && this.getGrpCustomerNo().trim().length()>0)
			personToAdd.setGrpCustomerNo(this.getGrpCustomerNo().trim());
		if(this.getStatusId()!=null && !this.getStatusId().equals("-1"))
			personToAdd.setStatusId(new Long(this.getStatusId()));
		if( isBlackListed() )
		{
			personToAdd.setIsBlacklisted(1L);
		}
		else
		{
	    	personToAdd.setIsBlacklisted(0L);
		}
		if(this.getSelectedPersonKind()!=null && this.getSelectedPersonKind().trim().length()>0 )
		{
		    	//Individual Information
		     if(this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_INDIVIDUAL_ID.toString())==0)
			 {
		    	 
		    	    putIndividualInfoInView(personToAdd);
			 }    
		     //Company Information
		     else if(this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_COMPANY_ID.toString())==0)
			 {
		    	 
					putCompanyInfoInView(personToAdd);
					
			 }
		}
		putNotNullFieldsInView(personToAdd);
		putContactInfoListInView(personToAdd);
		putContactReferenceListInView(personToAdd);
		putRolesListInView(personToAdd);
	}
	private void putNotNullFieldsInView(PersonView personToAdd) {
		personToAdd.setUpdatedBy(getLoggedInUser());
		personToAdd.setUpdatedOn(new Date());
		personToAdd.setCreatedBy(getLoggedInUser());
		personToAdd.setCreatedOn(new Date());
		personToAdd.setRecordStatus(1L);
		personToAdd.setIsDeleted(0L);
	}
	private void putContactInfoListInView(PersonView personToAdd) {
		//For Contact Info/////
		
			Set<ContactInfoView> ciViewSet=new HashSet<ContactInfoView>(0);
			List<ContactInfoView> ciList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
			
			ciViewSet.addAll(ciList);
			personToAdd.setContactInfoViewSet(ciViewSet);
	}
	private void putContactReferenceListInView(PersonView personToAdd) {
		if(viewRootMap.containsKey(CONTACT_REF_LIST))
		{
			List<ContactReferenceView> contactRefList=(List<ContactReferenceView>)viewRootMap.get(CONTACT_REF_LIST);
			if(contactRefList!=null && contactRefList.size()>0)
			{
			Set<ContactReferenceView> crViewSet=new HashSet<ContactReferenceView>(0);
			crViewSet.addAll(contactRefList);
		    personToAdd.setContactReferenceViewSet(crViewSet);
			}
		}
	}
	private void putRolesListInView(PersonView personToAdd) {
		List<RolesView> rolesList = new ArrayList<RolesView>(0);
		 Set<AssignedPersonTypeView> aptViewSet =new HashSet<AssignedPersonTypeView>(0);
		 if(viewRootMap.containsKey(ROLE_BEAN_LIST) && viewRootMap.get(ROLE_BEAN_LIST)!=null)
		 {
		    rolesList = (ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST);
		    for (RolesView roleView : rolesList) 
		    {
		    	AssignedPersonTypeView aptView = new AssignedPersonTypeView();
		    	aptView.setAssignedPersonTypeId(roleView.getAssignedPersonTypeId());
		    	if(roleView.getSubTypeId()!=null && roleView.getSubTypeId().trim().length()>0)
			 	   aptView.setPersonSubTypeId(new Long(roleView.getSubTypeId()));
		    	if(roleView.getTypeId()!=null && roleView.getTypeId().trim().length()>0)
			 	   aptView.setPersonTypeId(new Long (roleView.getTypeId()));
		    	aptViewSet.add(aptView);
			}
		    personToAdd.setAssignedPersonTypeViewSet(aptViewSet);
		 }
	}
	private void putCompanyInfoInView(PersonView personToAdd) {
		 
		 personToAdd.setIsCompany(new Long(1));
		if(this.getTxtCompanyName()!=null && this.getTxtCompanyName().trim().length()>0)
		{
		 personToAdd.setCompanyName(this.getTxtCompanyName());
		}
		else if(this.getTxtGovtDept()!=null && this.getTxtGovtDept().trim().length()>0)
		{
		 personToAdd.setGovtDepartment(this.getTxtGovtDept());
		}
		if(this.getTxtLicenseno()!=null && this.getTxtLicenseno().trim().length()>0)
		{
		 personToAdd.setLicenseNumber(this.getTxtLicenseno());
		}
        if(this.getLicenseExpiryDate()!=null)
        {
         personToAdd.setLicenseExpiryDate(this.getLicenseExpiryDate());
        }
        if(this.getLicenseIssueDate()!=null)
        {
         personToAdd.setLicenseIssueDate(this.getLicenseIssueDate());
        }
        if(this.getLicenseIssuePlaceId()!=null && !this.getLicenseIssuePlaceId().trim().equals("-1"))
        {
         personToAdd.setLicenseIssuePlaceId(new Long(this.getLicenseIssuePlaceId()));
         personToAdd.setLicenseIssuePlaceIdString( this.getLicenseIssuePlaceId() );
        }
        if(this.getTradeLicenseIssuersId() !=null && !this.getTradeLicenseIssuersId().trim().equals("-1"))
        {
            personToAdd.setLicenseIssuingAuthority( Long.valueOf( this.getTradeLicenseIssuersId()));
            
        }
         personToAdd.setBusinessActivities(businessActivities);
	}
	private void putIndividualInfoInView(PersonView personToAdd) {
		personToAdd.setIsCompany(new Long(0));
		personToAdd.setDateFormat(getDateFormat());
		personToAdd.setFirstName(this.getFirstName());
		personToAdd.setLastName(this.getLastName());
		personToAdd.setMiddleName(this.getMiddleName());
		personToAdd.setFullNameEn(this.getFullNameEn());
		personToAdd.setDesignation(this.getDesignation());
		personToAdd.setPassportNumber(this.getPassportNumber());
		personToAdd.setResidenseVisaNumber(this.getResidenseVisaNumber());
		personToAdd.setPersonalSecCardNo(this.getPersonalSecCardNo());
		personToAdd.setDrivingLicenseNumber(this.getDrivingLicenseNumber());
		personToAdd.setSocialSecNumber(this.getSocialSecNumber());
		personToAdd.setCellNumber(this.getCellNumber());
		personToAdd.setSecCellNumber(this.getSecCellNumber());
		personToAdd.setGender(this.getGender());
		if (this.getTitleId() != null && !this.getTitleId().equals("-1"))
			personToAdd.setTitleId(new Long(this.getTitleId()));
		if (this.getNationalityId() != null && !this.getNationalityId().equals("-1"))
		{
			personToAdd.setNationalityId(new Long (this.getNationalityId()));
			personToAdd.setNationalityIdString(this.getNationalityId());
		}
		if (this.getDateOfBirth() != null)
		    personToAdd.setDateOfBirth(this.getDateOfBirth());
		if(this.getMaritialStatusId()!=null && !this.getMaritialStatusId().trim().equals("-1"))
			personToAdd.setMaritialStatusId(new Long(this.getMaritialStatusId()));
		if (this.getPassportExpiryDate() != null)
			personToAdd.setPassportExpiryDate(this.getPassportExpiryDate());
		if (this.getPassportIssueDate() != null)
			personToAdd.setPassportIssueDate(this.getPassportIssueDate());
		if (this.getResidenseVisaIssueDate()!= null)
			personToAdd.setResidenseVisaIssueDate(this.getResidenseVisaIssueDate());
		if(this.getWorkingCompany()!=null && this.getWorkingCompany().trim().length()>0)
			personToAdd.setWorkingCompany(this.getWorkingCompany().trim());
		if (this.getResidenseVidaExpDate() != null)
			personToAdd.setResidenseVidaExpDate(this.getResidenseVidaExpDate());
		
		if (this.getPassportIssuePlaceId() != null && !this.getPassportIssuePlaceId().equals("-1"))
			personToAdd.setPassportIssuePlaceId(new Long(this.getPassportIssuePlaceId()));
		
		if (this.getPassportIssueCity() != null && !this.getPassportIssueCity().equals("-1"))
		{
			personToAdd.setPassportIssueCity( new Long(this.getPassportIssueCity()) );
			personToAdd.setPassportIssueCityString( this.getPassportIssueCity().toString() );
		}
		
		
		if (this.getResidenseVisaIssuePlaceId() != null && !this.getResidenseVisaIssuePlaceId().equals("-1"))
			personToAdd.setResidenseVisaIssuePlaceId(new Long(this.getResidenseVisaIssuePlaceId()));
		
		
	}

	
    public void loadAttachmentsAndComments(Long personId){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		//viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_PERSON;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.NOTES_OWNER_PERSON);
		
		if(personId!= null){
			
	    	String entityId = personId.toString();
			
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	public void saveAttachmentsAndNotes(Long entityIdForAttachents) 
    {
    	try{
	    	viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityIdForAttachents.toString());
	    	CommonUtil.updateDocuments();
	    	NotesController.saveNotes(WebConstants.NOTES_OWNER_PERSON, entityIdForAttachents);	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveAttachmentsAndNotes|crashed ", exception);
			
		}
    }
	
	public String clearAll()
	{
		clearCompanyInformation();
		clearPersonInformation();
		clearContactRefFields();
		clearContactInfoFields();
		  return "";
	}
	public void clearPersonInformation()
	{

		  this.setFirstName("");
		  this.setLastName(""); 
		  this.setNationalityId("");
		  this.setPassportNumber("");
		  this.setResidenseVisaNumber("") ;
		  this.setPersonalSecCardNo("");
		  this.setDrivingLicenseNumber("");
		  this.setSocialSecNumber("");
		  this.setCellNumber("");
		  this.setSecCellNumber("");
		  this.setDesignation("");
		  this.setTitleId("");
		  this.setMiddleName("");
		  this.setGender("F");
		  this.setDateOfBirth(null);
		  this.setPassportIssueDate(null);
		  this.setPassportIssuePlaceId("-1");
		  this.setPassportIssueCity( "-1" );
		  this.setPassportExpiryDate(null);
		  this.setResidenseVidaExpDate(null);
		  this.setResidenseVisaIssuePlaceId("-1");
		  this.setSelectedCountryId("-1");
		  //  this.setStateId("-1");
		  this.setPersonSubTypeId("-1");
		
	}
	public void clearCompanyInformation()
	{
		  this.setTxtCompanyName("");
		  this.setTxtGovtDept("");
		  this.setTxtLicenseno("");
	}
	private String getLoggedInUser()
	{
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		return loggedInUser;
	}
	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors()throws Exception
	{
		    boolean isError=false;
		    errorMessages=new ArrayList<String>();
    	    errorMessages.clear();
    		
//	    	    DomainDataView ddvPersonKindCompany= (DomainDataView )viewRootMap.get(DDV_PERSON_KIND_COMPANY);
//			    DomainDataView ddvPersonKindIndividual= (DomainDataView )viewRootMap.get(DDV_PERSON_KIND_INDIVIDUAL);
			    	
				if(this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_COMPANY_ID.toString())==0)
		            isError=getIsCompanyInfoHasErrors();
				else if(this.getSelectedPersonKind().compareTo(WebConstants.PERSON_KIND_INDIVIDUAL_ID.toString())==0)
				    isError=getIsPersonInfoHasErrors();
				
				if(!AttachmentBean.mandatoryDocsValidated())
		    	{
		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
		    		isError = true;
		    	}
	
				if(!viewRootMap.containsKey(CONTACT_INFO_LIST))
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_INFO_REQ)));
		    		isError = true;
				}
		    	else if(viewRootMap.containsKey(CONTACT_INFO_LIST) )
				{
	
		    		List<ContactInfoView> ciList=(ArrayList<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
		    		if(ciList==null || ciList.size()<=0)
		    		{
		    			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_CONTACT_INFO_REQ)));
		    			isError= true;
		    		}
				}
				if( !this.getIsGrpCustomerNumberReadOnly() )
				{
					if( this.getGrpCustomerNo()!= null &&  this.getGrpCustomerNo().trim().length()  > 0)
					{
                        PersonView person  = new PersonView();
                        person.setGrpCustomerNo( this.getGrpCustomerNo() );
                        List<PersonView> list = new PropertyServiceAgent().getPersonInformation(personView);   
		    			if( list.size()>0 )
		    			{
                         errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_GRP_ALREADY_EXISTS)));
		    			 isError= true;
		    			}
		    		}
					else 
					{
			    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_GRP_CUSTOMER_NUMBER_REQ)));
			    		isError= true;
						
					}
					
				}
				
		    return isError;
	}
	public Boolean getIsPersonInfoHasErrors()throws Exception
    {
		Boolean isError=false;
		if (this.getFirstName() == null || this.getFirstName().trim().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_FIRST_NAME)));
			isError = true;
		}

		if (this.getLastName()== null || this.getLastName().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_LAST_NAME)));
			isError = true;
		}
		if (this.getTitleId()==null || this.getTitleId().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_TITLE));
			isError = true;
		}
		
		if (this.getGender()==null || this.getGender().equals("-1") || this.getGender().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_GENDER));
			isError = true;
		}
		
//		if (this.getDateOfBirth()==null)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_DATE_OF_BIRTH));
//			isError = true;
//		}
		if(this.getNationalityId()==null || this.getNationalityId().trim().length()<=0 ||this.getNationalityId().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_NATIONALITY_REQ));
			isError = true;
		}
		if( this.getSocialSecNumber()==null || this.getSocialSecNumber().trim().length() <=0  )
		{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("person.message.idNumRequired"));
    		isError= true;
					
		}
		else
		{
			PersonView person = null;
			Long personIdNot= null;
			if(viewRootMap.get(PERSON_VIEW) != null)
			{
				person = (PersonView)viewRootMap.get(PERSON_VIEW);
				personIdNot  =person.getPersonId();
			}
			String otherPerson = UtilityService.getPersonsBySocialSecNumber(this.getSocialSecNumber(),personIdNot);
		
			if (otherPerson.trim().length()>0)
			{
				errorMessages.add(
						           java.text.MessageFormat.format( 
						        		   							ResourceUtil.getInstance().getProperty("person.message.idNumUniqunessRequired"),
						        		   							otherPerson
						           								  )
						          );
	    		isError= true;
			}
		}

		return isError;
    }
	public Boolean getIsCompanyInfoHasErrors()
    {
    	boolean hasErrors=false;
    	 
    	if((this.getTxtCompanyName()==null || this.getTxtCompanyName().trim().length()<=0 ) &&
    	   (this.getTxtGovtDept()==null || this.getTxtGovtDept().trim().length()<=0)
    	)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_COMPANY_INFO_REQ));
    		hasErrors = true;
    	}
    	if(this.getLicenseIssuePlaceId()==null || this.getLicenseIssuePlaceId().trim().length()<=0 || 
    			this.getLicenseIssuePlaceId().equals("-1"))
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_COMPANY_LICENSE_ISSUE_PLACE_REQ));
    		hasErrors = true;
    	}
    	if(this.getTradeLicenseIssuersId()==null || this.getTradeLicenseIssuersId().trim().length()<=0 || 
    			this.getTradeLicenseIssuersId().equals("-1"))
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("person.Msg.CompanyLicenseIssueAuthorityRequired"));
    		hasErrors = true;
    	}
    	if(this.getLicenseIssueDate() ==null )
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("person.Msg.CompanyLicenseIssueDateRequired"));
    		hasErrors = true;
    	}
    	if(this.getLicenseExpiryDate() ==null )
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("person.Msg.CompanyLicenseExpiryDateRequired"));
    		hasErrors = true;
    	}
    	if (this.getContactRefList() == null || this.getContactRefList().size() <= 0 )
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("contractor.message.contactReferencesRequired") );
    		hasErrors = true;
    	}
    	return hasErrors;
    }
	
	public String getPersonSubTypeId() {
		return personSubTypeId;
	}

	public void setPersonSubTypeId(String personSubTypeId) {
		this.personSubTypeId = personSubTypeId;
	}

	public List<SelectItem> getCountryList() {
		if(viewRootMap.containsKey("countryList"))
			countryList= (List<SelectItem>) (viewRootMap.get("countryList"));
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
		if(this.countryList !=null)
			viewRootMap.put("countryList",this.countryList );
	}

	public List<SelectItem> getStateList() {
		if(viewRootMap.containsKey("stateList"))
			stateList= (List<SelectItem>) (viewRootMap.get("stateList"));

		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
		if(this.stateList!=null)
			viewRootMap.put("stateList",this.stateList );
	}

	public String getSelectManyPersonType()
    {
		return selectManyPersonType;
	}

	public void setSelectManyPersonType(String selectManyPersonType) {
		this.selectManyPersonType = selectManyPersonType;
	}

	@Override 
	@SuppressWarnings("unchecked")
	 public void init() 
     {
    	try{		   
       	     super.init();
       	     if (!isPostBack())
 	 		 {
       	    	if(	sessionMap.get(WebConstants.IS_EMAIL_MANDATORY)	!= null	)
       	    	{
       	    	 viewMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
       	    	 sessionMap.remove(WebConstants.IS_EMAIL_MANDATORY);
       	    	}
       	    	setPassportCityList(passportCityList);
       	    	viewRootMap.put("stateList",stateList);
// 	 		    viewRootMap.put(DDV_PERSON_KIND_COMPANY, getPersonKindCompany());
// 	 		    viewRootMap.put(DDV_PERSON_KIND_INDIVIDUAL , getPersonKindIndividual());
 	 		    loadAttachmentsAndComments(null);
 	 		    this.setIsGrpCustomerNumberReadOnly( true );
 	 		    viewRootMap.put("canAddAttachment", true);
				viewRootMap.put("canAddNote", true);
				//Calling below method here because person types are required in addByDefaultToAllRoles() function
				getPersonTypeList();
				getQueryString();
 	 		 }
       	  
    		    dateFormatter = new SimpleDateFormat(getDateFormat());
    	 	
    	 		loadCombos();
    	 		
     } catch (Exception e) {
			logger.LogException("init| ErrorOccured", e);
		}
	 }
	@SuppressWarnings("unchecked")
	private void getQueryString() 
	{
		if(sessionMap.containsKey("MANY_PERSON")&& sessionMap.get("MANY_PERSON")!=null)
		{
			viewRootMap.put("MANY_PERSON", sessionMap.get("MANY_PERSON"));
			sessionMap.remove("MANY_PERSON");
		}
		if(getRequestParam("PERSON_TYPE")!=null)
		{
			this.requestForPersonType = getRequestParam("PERSON_TYPE").toString();
		    viewRootMap.put("REQUESTED_PERSON_TYPE", requestForPersonType);
		}
		if (getRequestParam(WebConstants.PERSON_ID)!=null  || request.getParameter(WebConstants.PERSON_ID)!=null)
		{
				edit = "edit";
				loadEdit();
				cmb_SelectedPersonKind.setDisabled(true);
		}
		else
			addByDefaultToAllRoles();
		if(sessionMap.containsKey(WebConstants.PERSON_BASIC_INFO) && sessionMap.get(WebConstants.PERSON_BASIC_INFO)!=null)
		{
			viewRootMap.put(WebConstants.PERSON_BASIC_INFO, sessionMap.get(WebConstants.PERSON_BASIC_INFO));
			loadEdit();
			addByDefaultToAllRoles();
			cmb_SelectedPersonKind.setDisabled(true);
			sessionMap.remove(WebConstants.PERSON_BASIC_INFO);
		}
		if (getRequestParam(WebConstants.SELECT_BLACK_LIST)!=null  )
			viewRootMap.put(WebConstants.SELECT_BLACK_LIST, true); 
		else if(request.getParameter(WebConstants.SELECT_BLACK_LIST)!=null)
			viewRootMap.put(WebConstants.SELECT_BLACK_LIST, true); 
		
		if (getRequestParam("viewMode")!=null  )
			viewRootMap.put(VIEW_MODE, getRequestParam("viewMode").toString() ); 
		else if(request.getParameter("viewMode")!=null)
			viewRootMap.put(VIEW_MODE, request.getParameter("viewMode").toString() ); 
		if (getRequestParam("readOnlyMode")!=null  )
		{
			viewRootMap.put(READ_ONLY_MODE, getRequestParam("readOnlyMode").toString()); 
			viewRootMap.put("canAddAttachment", false);
			viewRootMap.put("canAddNote", false);
		}
		else if(request.getParameter("readOnlyMode")!=null)
		{
			viewRootMap.put(READ_ONLY_MODE, request.getParameter("readOnlyMode").toString());
			viewRootMap.put("canAddAttachment", false);
			viewRootMap.put("canAddNote", false);
		}
		
		 if(request.getParameter("selectMode")!=null)
			 viewRootMap.put("SELECT_MODE", request.getParameter("selectMode").toString());
		 else if(getRequestParam("selectMode")!=null)
			 viewRootMap.put("SELECT_MODE", getRequestParam("selectMode").toString());
		
		 if(request.getParameter("displaycontrolname")!=null)
			controlName=request.getParameter("displaycontrolname").toString();
		 if(request.getParameter("hdncontrolname")!=null)
			controlForId=request.getParameter("hdncontrolname").toString();
	}
	public String btnBack_Click()
	{
	   setRequestParam("hdncontrolname", controlForId);
	   setRequestParam("displaycontrolname", controlName);
	   setRequestParam("selectMode", viewRootMap.get("SELECT_MODE"));
	   setRequestParam("viewMode", viewRootMap.get(VIEW_MODE));
	   setRequestParam("persontype", viewRootMap.get("REQUESTED_PERSON_TYPE"));
	   if(viewRootMap.containsKey(WebConstants.SELECT_BLACK_LIST))
     	   setRequestParam(WebConstants.SELECT_BLACK_LIST, true); 
	   //Here we want to inform searchPerson that this popup was opened in multiselect mode. 
	   //This value will be used in SearchPerson.java in getQueryString()
	   if(viewRootMap.containsKey("MANY_PERSON")&& viewRootMap.get("MANY_PERSON")!=null)
			 sessionMap.put("SELECT_MODE",(Boolean)viewRootMap.get("MANY_PERSON")?"many":"single");
	   return "searhPerson";
	   
	}
	public Boolean getIsReadonlyMode(){
		String isReadOnlyMode = "";
		if(viewRootMap.get(READ_ONLY_MODE)!=null){
			isReadOnlyMode = viewRootMap.get(READ_ONLY_MODE).toString();
		}
		return isReadOnlyMode.equals("true");
	}
	
	public boolean getIsViewModePopUp()
	 {
		 if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase("popup"))
			 return true;
		 else 
			 return false;
	 }
	@SuppressWarnings("unchecked")
	private void loadCombos()
	{
		loadCountry();
		
		loadPersonTypeCombos();
		loadTitiles();
		loadBusinessActivityList();
		viewRootMap.put(Keys.BUSINESS_ACTIVITY_LIST, businessActivityList);
		
	}
	// loads the business activity list
	private final void loadBusinessActivityList() {

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<BusinessActivityView> businessActivityViewList = csa
					.getAllBusinessActivities();

			if (businessActivityList == null)
				businessActivityList = new HashMap<String, String>();

			for (BusinessActivityView businessActivityView : businessActivityViewList)
				getBusinessActivityList()
						.put(
								getIsEnglishLocale() ? businessActivityView
										.getDescriptionEn()
										: businessActivityView
												.getDescriptionAr(),
								businessActivityView.getBusinessActivityId()
										.toString());

		} catch (Exception e) {
			logger.LogException("loadBusinessActivityList|Error Occured ", e);
		}
	}
	
	
	 public void loadTitiles()  
	  {
		  ApplicationBean appBean = new ApplicationBean();
		  this.setTitleList((appBean.getTitle()));
	  }
	 @SuppressWarnings("unchecked")
	  public void prerender() 
	  {
		 super.prerender();
         
		 tabPersonIndividual.setRendered(false);
     	 tabPersonCompany.setRendered(false);
     	 tabContactReferences.setRendered(false);

        if(!isPostBack())
        {
        	//Calling this method here so that for the first time subtype load aginst default person type
        	//loadPersonSubType();
        	if( personId==null  && 
        	  (!viewRootMap.containsKey(WebConstants.PERSON_BASIC_INFO) || viewRootMap.get(WebConstants.PERSON_BASIC_INFO)==null))
        	{
        			
        		DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_KIND),
		                 WebConstants.PERSON_KIND_INDIVIDUAL);
        		if(ddv !=null)
        		  this.setSelectedPersonKind(ddv.getDomainDataId().toString());
        	}
        	
        }
        tabContactReferences.setRendered(getIsTabContactReferenceShow());
        selectPersonKind_Changed();
        
	}
	 @SuppressWarnings("unchecked")
	  public boolean getIsTabContactReferenceShow()
	  {
		  boolean isTabContactReferenceShow =true;
		  List<RolesView> listRoles= new ArrayList<RolesView>(0);
		  if(viewRootMap.containsKey(ROLE_BEAN_LIST))
			  listRoles = (ArrayList<RolesView>)viewRootMap.get(ROLE_BEAN_LIST); 
		  if(listRoles.size()>0)
		  {
			  for (RolesView rolesView : listRoles) 
			  {
				  List<DomainDataView> ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.PERSON_TYPE);
				  DomainDataView ddv = CommonUtil.getIdFromType(ddvList, WebConstants.PERSON_TYPE_TENANT);
				  
				if(rolesView.getTypeId().compareTo(ddv.getDomainDataId().toString())==0)
					return true;
				else
					isTabContactReferenceShow=false;
			  }
		  }
		  else
			  isTabContactReferenceShow=false;
	      return isTabContactReferenceShow;	  
	 }
		  
	 
	 public void onLicenseIssueingEmirateChanged()
     {
		 try
		 {
			 if ( this.getLicenseIssuePlaceId() != null && !this.getLicenseIssuePlaceId().equals("-1"))
			 {
				 loadTradeLicenseIssuersList(this.getLicenseIssuePlaceId() );
			 }
			 
		 }
		 catch(Exception ex)
		 {
			 logger.LogException("Error", ex);
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
     }
     public void selectPersonKind_Changed()
     {
    	  if(this.getSelectedPersonKind()!=null && this.getSelectedPersonKind().trim().length()>0 && !this.getSelectedPersonKind().equals("-1"))
          {
          	DomainDataView ddv =getPersonKindCompany();
          	
  	        if(ddv.getDomainDataId().compareTo(new Long (this.getSelectedPersonKind()))==0)
  	        {
  	        	  loadLicenseIssuingEmirate();
  	        	  this.setSynchedWithEID("0");
  	              tabPersonCompany.setRendered(true);
  	              tabPersonIndividual.setRendered(false);
  	              
  	              clearPersonInformation();
  	        }
  	        else 
  	        { 
  	        	  tabPersonIndividual.setRendered(true);
  	        	  tabPersonCompany.setRendered(false);
  	        	  if(
  	        			  ( this.getSynchedWithEID()  == null || this.getSynchedWithEID().equals( "0") ) &&
  	        			  viewRootMap.containsKey( PERSON_VIEW )
  	        	    )
  	        		  
  	        	  {
  	        		  if( errorMessages == null ) errorMessages=new ArrayList<String>();
  	        		  String msg = ResourceUtil.getInstance().getProperty( ( "person.msg.NotSynchedWithEid" ) ); 
  	        		  if( !errorMessages.contains(msg) )
  	        			  	errorMessages.add( msg );
  	        	  }
  	        	  clearCompanyInformation();
  	        }
          }
    	 
    	 
     }
     @SuppressWarnings("unchecked")
	  private DomainDataView getPersonKindCompany()
	  {
		  if(!viewRootMap.containsKey(WebConstants.PERSON_KIND_COMPANY))
		  {
			 
	         DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_KIND), 
	        		                          WebConstants.PERSON_KIND_COMPANY);
	       viewRootMap.put(WebConstants.PERSON_KIND_COMPANY,ddv);
		  }
		  return (DomainDataView)viewRootMap.get(WebConstants.PERSON_KIND_COMPANY);
	  }
     @SuppressWarnings("unchecked")
	  private DomainDataView getPersonKindIndividual()
	  {
		  if(!viewRootMap.containsKey(WebConstants.PERSON_KIND_INDIVIDUAL ))
		  {
			 
	         DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_KIND), 
	        		                          WebConstants.PERSON_KIND_INDIVIDUAL);
	       viewRootMap.put(WebConstants.PERSON_KIND_INDIVIDUAL,ddv);
		  }
		  return (DomainDataView)viewRootMap.get(WebConstants.PERSON_KIND_INDIVIDUAL);
	  }
     @SuppressWarnings("unchecked")
	  public void loadPersonSubType()  
	  {

		  List<DomainDataView> ddvList = (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.PERSON_TYPE); 
		  viewRootMap.remove(WebConstants.PERSON_SUB_TYPE);
		  
		  if(selectedPersonType!=null)
		  {
			  for (DomainDataView domainDataView : ddvList) 
			  {
				  if(selectedPersonType.equals(domainDataView.getDomainDataId().toString() ))
				  {
					  
					  if(domainDataView.getDataValue().equals(WebConstants.PERSON_TYPE_TENANT))
					  {
						  loadTenantSubTypes();
						  viewRootMap.put(WebConstants.PERSON_SUB_TYPE,viewRootMap.get(WebConstants.TENANT_TYPE));
					  }
					  if(domainDataView.getDataValue().equals(WebConstants.PERSON_TYPE_REPRESENTATIVE))
					  {
						  loadRepresentativeSubTypes();
						  viewRootMap.put(WebConstants.PERSON_SUB_TYPE,viewRootMap.get(WebConstants.REPRESENTATIVE_TYPE));
					  }
					  if(domainDataView.getDataValue().equals(WebConstants.PERSON_TYPE_OWNER))
					  {
						  loadOwnerSubTypes();
						  viewRootMap.put(WebConstants.PERSON_SUB_TYPE,viewRootMap.get(WebConstants.OWNER_TYPE));
					  }
					  break;
				  }
				
			   }
			  
			  
		  }
	  }
     @SuppressWarnings("unchecked")
	  public void loadCountry()  {
			
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
				  countryList.add(item);
			     
			  }
			Collections.sort(countryList, ListComparator.LIST_COMPARE);
			 viewRootMap.put("countryList", countryList);
			}catch (Exception e){
				logger.LogException("loadCountry|Error Occured ",e);
				
			}
		}
     @SuppressWarnings("unchecked")
	  public void loadLicenseIssuingEmirate()  {
			
			try 
			{
				licenseIssuingEmirateList=new ArrayList<SelectItem>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			Set<RegionView> regionViewSet = psa.getState(19l);
			
			for (RegionView regionView : regionViewSet) {
				SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
			      
				
				  licenseIssuingEmirateList.add(item);
			}
			
			Collections.sort(licenseIssuingEmirateList, ListComparator.LIST_COMPARE);
			viewRootMap.put("licenseIssuingEmirateList", licenseIssuingEmirateList);
			}catch (Exception e){
				logger.LogException("loadLicenseIssuingEmirate|Error Occured ",e);
				
			}
		}
     
     @SuppressWarnings("unchecked")
	    public void loadContactRefState()
	    {
	     CRStateList =  loadState(new Long(CRCountryId));	
	     FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("CRStateList", CRStateList );
	    }
     @SuppressWarnings("unchecked")
	    public void loadContactInfoState()
	    {
	     stateList = loadState(new Long(countryId));	
	     FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
	    }
     @SuppressWarnings("unchecked")
		public List<SelectItem> loadState(Long selectedCountry)  {
			
			List<SelectItem> cmbStateList = new ArrayList<SelectItem>(0);
			try {
			
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (selectedCountry==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	
			
			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      cmbStateList.add(item);
			      
			      
			  }
			Collections.sort(cmbStateList, ListComparator.LIST_COMPARE);
			 
			}catch (Exception e){
				
				logger.LogException("loadState|Error Occured ",e);
				
			}
			return cmbStateList;
		}
     public void onPassportCountryChanged() 
 	 { 
    	 
 		if( passportIssuePlaceId != null && passportIssuePlaceId.trim().length() > 0  && !passportIssuePlaceId.equals("-1"))
 		{
 			passportCityList= loadCity(Long.parseLong(passportIssuePlaceId));
 			setPassportCityList( passportCityList );
 			getPassportCityList();
 		}
 		
// 		Uncomment if u want to show the city e.r.t country
// 		if(sponsor.getCountryId() != null)
// 			loadCity(Long.parseLong(sponsor.getCountryId()), WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR);
 	 } 
     @SuppressWarnings("unchecked")
		public void loadContactRefCity()
	    {
	    CRCityList = loadCity(new Long(CRStateId ));	
	    FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("CRCityList", CRCityList);
	    }
     @SuppressWarnings("unchecked")
	    public void loadContactInfoCity()
	    {
	     cityList = loadCity(new Long(stateId));
	     FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);	
	    }
     @SuppressWarnings("unchecked")
		public List<SelectItem>  loadCity(Long selectedState){
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
				try 
				{
					
					 Set<RegionView> regionViewList = null;
					 if(selectedState!=null && selectedState.compareTo(new Long(-1))!=0)
					 regionViewList =  psa.getCity(selectedState);
				          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
			  	    
					
					Iterator itrator = regionViewList.iterator();

					for(int i=0;i<regionViewList.size();i++)
					  {
						  RegionView rV=(RegionView)itrator.next();
						  SelectItem item;
						  if (getIsEnglishLocale())
						  {
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
						  }
						  else 
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
					      
						  cmbCityList.add(item);
					  }
					Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);	 
				}
				catch(Exception ex)
				{
					logger.LogException("loadCity|Error Occured ",ex);
					
				}
				
				return cmbCityList;
			   
		   }

	public List<PersonView> getProspectivePersonDataList()
	{
		return prospectivePersonDataList;
	}
	
	public void setProspectivePersonDataList(List <PersonView> pview)
	{
		this.prospectivePersonDataList = pview;
	}

	public boolean getshowProspectiveGrid()
	{
		showProspectiveGrid=false;
		if(sessionName!=null)
		{
		if(sessionMap.containsKey(sessionName) )
			showProspectiveGrid=true;
		}
		
		return showProspectiveGrid;
		
	}

	
	public String doSearch (){

                    try {
                    	
                    
                            if (firstName !=null && !firstName.equals("")){
                            
                            	personView.setFirstName(firstName.trim());                           	
                             	
                            }
                            if (lastName !=null && !lastName.equals("")){
                            	
                            	personView.setLastName(lastName.trim());
                            }
                    
	                        if(personalSecCardNo!=null && !personalSecCardNo.equals(""))
	                        {
	                        	personView.setPersonalSecCardNo(personalSecCardNo);
	                        	
	                        }
	                        
	                        if(passportNumber!=null && !passportNumber.equals(""))
	                        {
	                        	personView.setPassportNumber(passportNumber);
	                        	
	                        }
	                        if(residenseVisaNumber!=null && !residenseVisaNumber.equals(""))
	                        {
	                        	personView.setResidenseVisaNumber(residenseVisaNumber);
	                        	
	                        }
	                        if(drivingLicenseNumber!=null && !drivingLicenseNumber.equals(""))
	                        {
	                        	personView.setDrivingLicenseNumber(drivingLicenseNumber);
	                        	
	                        }
	                        if(socialSecNumber!=null && !socialSecNumber.equals(""))
	                        {
	                        	personView.setSocialSecNumber(socialSecNumber);
	                        	
	                        }
		                    dataList.clear();
		                  
		                    loadDataList();

	                    }

                    catch (Exception e){


                		logger.LogException( "doSearch|Error Occured",e);
       

                    }

                return "";

     }
	
	public String delete()
	{
		 try {
			 
			PersonView selectedPerson = (PersonView) propspectivePersonDataTable.getRowData();
				
			Long personId = selectedPerson.getPersonId();
			
			PropertyServiceAgent psa = new PropertyServiceAgent();
			 
			psa.deletePerson(personId);
			
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
     		logger.LogException( "delete|Error Occured",e);
		}
		
		return "";
	}

	
	public void loadDataList() 
	{
		try
		{
 			   	   PropertyServiceAgent myPort = new PropertyServiceAgent();
 			   	   List<DomainDataView> domainDataList = CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE);
	   			   for(int i=0;i<domainDataList.size();i++)
	   			   {
	   			     DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
	   				 if(domainDataView.getDataValue().equals( this.requestForPersonType)   )
	   				 {
	   					personView.setPersonTypeId(domainDataView.getDomainDataId());
	   					break;
	   				 }
	   			   }
	   			   this.setProspectivePersonDataList(myPort.getPersonInformation(personView));
	    }
		catch (Exception ex) 
		{
			logger.logError( "loadDataList|loadDataList");
	    }
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public String getFirstName() {
		
		if(viewRootMap.containsKey("firstName"))
			firstName=viewRootMap.get("firstName").toString();
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
		if(this.firstName != null)
			viewRootMap.put("firstName",this.firstName);
	}



	public String getLastName() {
		if(viewRootMap.containsKey("lastName"))
			lastName=viewRootMap.get("lastName").toString();
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
		if(this.lastName !=null)
			viewRootMap.put("lastName",this.lastName );
	}



	public Long getPersonTypeId() {
		if(viewRootMap.containsKey("personTypeId"))
			personTypeId=new Long (viewRootMap.get("personTypeId").toString());
		return personTypeId;
	}



	public void setPersonTypeId(Long personTypeId) {
		this.personTypeId = personTypeId;
		if(this.personTypeId!=null)
			viewRootMap.put("personTypeId",this.personTypeId);
	}



	public String getNationalityId() {
		if(viewRootMap.containsKey("nationalityId"))
			nationalityId=viewRootMap.get("nationalityId").toString();
		return nationalityId;
	}



	public void setNationalityId(String nationalityId) {
		this.nationalityId = nationalityId;
		if(this.nationalityId!=null)
			viewRootMap.put("nationalityId",this.nationalityId);
	}



	public String getPassportNumber() {
		if(viewRootMap.containsKey("passportNumber"))
			passportNumber=viewRootMap.get("passportNumber").toString();
		return passportNumber;
	}



	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
		if(this.passportNumber!=null)
			viewRootMap.put("passportNumber",this.passportNumber);
	}



	public String getResidenseVisaNumber() {
		if(viewRootMap.containsKey("residenseVisaNumber"))
			residenseVisaNumber=viewRootMap.get("residenseVisaNumber").toString();
		return residenseVisaNumber;
	}



	public void setResidenseVisaNumber(String residenseVisaNumber) {
		this.residenseVisaNumber = residenseVisaNumber;
		if(this.residenseVisaNumber !=null)
			viewRootMap.put("residenseVisaNumber",this.residenseVisaNumber );
	}



	public String getPersonalSecCardNo() {
		if(viewRootMap.containsKey("personalSecCardNo"))
			personalSecCardNo=viewRootMap.get("personalSecCardNo").toString();

		return personalSecCardNo;
	}



	public void setPersonalSecCardNo(String personalSecCardNo) {
		this.personalSecCardNo = personalSecCardNo;
		if(this.personalSecCardNo!=null)
			viewRootMap.put("personalSecCardNo",this.personalSecCardNo);
	}



	public String getDrivingLicenseNumber() {
		if(viewRootMap.containsKey("drivingLicenseNumber"))
			drivingLicenseNumber=viewRootMap.get("drivingLicenseNumber").toString();
		
		return drivingLicenseNumber;
	}



	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
		if(this.drivingLicenseNumber!=null)
			viewRootMap.put("drivingLicenseNumber",this.drivingLicenseNumber);
	}



	public String getSocialSecNumber() {
		if(viewRootMap.containsKey("socialSecNumber"))
			socialSecNumber=viewRootMap.get("socialSecNumber").toString();
		return socialSecNumber;
	}



	public void setSocialSecNumber(String socialSecNumber) {
		this.socialSecNumber = socialSecNumber;
		if(this.socialSecNumber!=null)
			viewRootMap.put("socialSecNumber",this.socialSecNumber);
	}



	public PersonView getPersonView() {
		return personView;
	}



	public void setPersonView(PersonView personView) {
		this.personView = personView;
	}



	public PersonView getDataItem() {
		return dataItem;
	}



	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}



	public List<PersonView> getDataList() {
		return dataList;
	}



	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getDesignation() {
		if(viewRootMap.containsKey("designation"))
			designation=viewRootMap.get("designation").toString();
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
		if(this.designation!=null)
			viewRootMap.put("designation",this.designation);
	}
	public String getCellNumber() {
		if(viewRootMap.containsKey("cellNumber"))
			cellNumber=viewRootMap.get("cellNumber").toString();
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
		if(this.cellNumber!=null)
			viewRootMap.put("cellNumber",this.cellNumber); 
	}
	public String getSecCellNumber() {
		if(viewRootMap.containsKey("secCellNumber"))
			secCellNumber=viewRootMap.get("secCellNumber").toString();
		return secCellNumber;
	}
	public void setSecCellNumber(String secCellNumber) {
		this.secCellNumber= secCellNumber;
		if(this.secCellNumber!=null)
			viewRootMap.put("secCellNumber",this.secCellNumber); 
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlForId() {
		return controlForId;
	}
	public void setControlForId(String controlForId) {
		this.controlForId = controlForId;
	}
	public String getRequestForPersonType() {
		return requestForPersonType;
	}
	public void setRequestForPersonType(String requestForPersonType) {
		this.requestForPersonType = requestForPersonType;
	}
	public String getDisplayStylePersonTypeCombo() {
		if(requestForPersonType!=null && !requestForPersonType.equals(""))
			displayStylePersonTypeCombo="display:inline";
		else
			displayStylePersonTypeCombo="display:none";
		return displayStylePersonTypeCombo;
	}
	public void setDisplayStylePersonTypeCombo(String displayStylePersonTypeCombo) {
		this.displayStylePersonTypeCombo = displayStylePersonTypeCombo;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	
	public HtmlDataTable getPropspectivePersonDataTable() {
		return propspectivePersonDataTable;
	}

	public void setPropspectivePersonDataTable(
			HtmlDataTable propspectivePersonDataTable) {
		this.propspectivePersonDataTable = propspectivePersonDataTable;
	}

	public PersonView getProspectiveDataItem() {
		return prospectiveDataItem;
	}

	public void setProspectiveDataItem(PersonView prospectiveDataItem) {
		this.prospectiveDataItem = prospectiveDataItem;
	}

	public String getHdndisplaycontrolName() {
		return hdndisplaycontrolName;
	}

	public void setHdndisplaycontrolName(String hdndisplaycontrolName) {
		this.hdndisplaycontrolName = hdndisplaycontrolName;
	}

	public String getSelectedCountryId() {
		if(viewRootMap.containsKey("selectedCountryId"))
			selectedCountryId=viewRootMap.get("selectedCountryId").toString();
		
		return selectedCountryId;
	}
	@SuppressWarnings("unchecked")
	public void setSelectedCountryId(String selectedCountryId) {
		
		this.selectedCountryId = selectedCountryId;
		if(this.selectedCountryId!=null)
			viewRootMap.put("selectedCountryId",this.selectedCountryId); 
	}

	public String getStateId() {
		if(viewRootMap.containsKey("stateId"))
			stateId=viewRootMap.get("stateId").toString();
		return stateId;
	}
	@SuppressWarnings("unchecked")
	public void setStateId(String stateId) {
		this.stateId = stateId;
		if(this.stateId !=null)
			viewRootMap.put("stateId",this.stateId );
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPersonSubTypeList() {
		

		personSubTypeList.clear();
		List<DomainDataView> ddvList= (	ArrayList<DomainDataView>)viewRootMap.get(WebConstants.PERSON_SUB_TYPE);;
		SelectItem item ;
		if (ddvList != null)
		{
			for (DomainDataView domainDataView : ddvList) 
				{
						if(getIsEnglishLocale())	
						{
					        item = new SelectItem(domainDataView.getDomainDataId().toString(), domainDataView.getDataDescEn());
					 	    personSubTypeList.add(item);
						}
					     else if(getIsArabicLocale())
						{
							item = new SelectItem(domainDataView.getDomainDataId().toString(), domainDataView.getDataDescAr());
							personSubTypeList.add(item);
						} 
						
				}
			Collections.sort(personSubTypeList,ListComparator.LIST_COMPARE);
			viewRootMap.put(PERSON_SUB_TYPE_LIST,personSubTypeList);
		}
		else
		{
			item = new SelectItem("-1", "--");
	 	    personSubTypeList.add(item);
			
		}

		
		return personSubTypeList;
	}

	public void setPersonSubTypeList(List<SelectItem> personSubTypeList) {
		this.personSubTypeList = personSubTypeList;
	}

	public String getMiddleName() {
		if(viewRootMap.containsKey("middleName"))
			middleName=viewRootMap.get("middleName").toString();
		return middleName;
	}
	@SuppressWarnings("unchecked")
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
		if(this.middleName !=null)
			viewRootMap.put("middleName",this.middleName );
		
	}

	public String getGender() {
		if(viewRootMap.containsKey("gender"))
			gender=viewRootMap.get("gender").toString();
		return gender;
	}

	@SuppressWarnings("unchecked")
	public void setGender(String gender) {
		this.gender = gender;
		if(this.gender !=null)
			viewRootMap.put("gender",this.gender );
	}

	public Date getDateOfBirth() {
		try
		{
			if(viewRootMap.containsKey("dateOfBirth"))
				//dateOfBirth=dateFormatter.parse(viewRootMap.get("dateOfBirth").toString());
			  dateOfBirth=(Date)viewRootMap.get("dateOfBirth");
		}
		catch(Exception ex)
		{
			logger.LogException("getDateOfBirth|Error Occured", ex);
			
		}
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
		if(this.dateOfBirth !=null)
			viewRootMap.put("dateOfBirth",this.dateOfBirth);
	}

	public Date getPassportIssueDate() {
		try
		{
			if(viewRootMap.containsKey("passportIssueDate"))
				passportIssueDate=(Date)viewRootMap.get("passportIssueDate");
			
		}
		catch(Exception ex)
		{
			logger.LogException("getPassportIssueDate|Error Occured", ex);
			
		}
		return passportIssueDate;
	}

	public void setPassportIssueDate(Date passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
		if(this.passportIssueDate !=null)
		viewRootMap.put("passportIssueDate",this.passportIssueDate );
	}

	public String getPassportIssuePlaceId() {
		if(viewRootMap.containsKey("passportIssuePlaceId"))
			passportIssuePlaceId=viewRootMap.get("passportIssuePlaceId").toString();
		return passportIssuePlaceId;
	}

	public void setPassportIssuePlaceId(String passportIssuePlaceId) {
		this.passportIssuePlaceId = passportIssuePlaceId;
		if(this.passportIssuePlaceId!=null)
			viewRootMap.put("passportIssuePlaceId",this.passportIssuePlaceId);
	}
	

	public String getPassportIssueCity() {
		if(viewRootMap.containsKey("passportIssueCity"))
			passportIssueCity=viewRootMap.get("passportIssueCity").toString();
		return passportIssueCity;
	}

	public void setPassportIssueCity(String passportIssueCity) {
		this.passportIssueCity= passportIssueCity;
		if(this.passportIssueCity!=null)
			viewRootMap.put("passportIssueCity",this.passportIssueCity);
	}
	public Date getPassportExpiryDate() {
		try
		{
			if(viewRootMap.containsKey("passportExpiryDate"))
				passportExpiryDate=(Date)viewRootMap.get("passportExpiryDate");
			
		}
		catch(Exception ex)
		{
			logger.LogException("getPassportExpiryDate|Error Occured", ex);
			
		}

		return passportExpiryDate;
	}

	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
		if(this.passportExpiryDate !=null)
			viewRootMap.put("passportExpiryDate",this.passportExpiryDate);
	}

	public Date getResidenseVidaExpDate() {
		try
		{
			if(viewRootMap.containsKey("residenseVidaExpDate"))
				residenseVidaExpDate=(Date)viewRootMap.get("residenseVidaExpDate");
			
		}
		catch(Exception ex)
		{
			logger.LogException("getPassportExpiryDate|Error Occured", ex);
			
		}
		return residenseVidaExpDate;
	}

	public void setResidenseVidaExpDate(Date residenseVidaExpDate) {
		this.residenseVidaExpDate = residenseVidaExpDate;
		if(this.residenseVidaExpDate!=null)
			viewRootMap.put("residenseVidaExpDate",this.residenseVidaExpDate);
	}

	public String getResidenseVisaIssuePlaceId() {
		if(viewRootMap.containsKey("residenseVisaIssuePlaceId"))
			residenseVisaIssuePlaceId=viewRootMap.get("residenseVisaIssuePlaceId").toString();
		return residenseVisaIssuePlaceId;
	}

	public void setResidenseVisaIssuePlaceId(String residenseVisaIssuePlaceId) {
		this.residenseVisaIssuePlaceId = residenseVisaIssuePlaceId;
		if(this.residenseVisaIssuePlaceId!=null)
			viewRootMap.put("residenseVisaIssuePlaceId",this.residenseVisaIssuePlaceId);
	}

	
	
	public String getErrorMessages() 
	{
    	return	CommonUtil.getErrorMessages(errorMessages);
		
	}
	public String getSuccessMessages() 
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			for (String message : successMessages) 
				{
					messageList += message+ " <br></br>" ;
			    }
			
		}
		return	messageList;
	}


	public List<SelectItem> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<SelectItem> titleList) {
		this.titleList = titleList;
	}

	public String getTitleId() {
		if(viewRootMap.containsKey("titleId"))
			titleId=viewRootMap.get("titleId").toString();
		
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
		if(this.titleId !=null)
			viewRootMap.put("titleId",this.titleId );
	}

	public String getMsgs() {
		return msgs;
	}

	public void setMsgs(String msgs) {
		this.msgs = msgs;
	}

	public String getAddress1() {
		if(viewRootMap.containsKey("address1"))
			address1=viewRootMap.get("address1").toString();
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
		if(this.address1  != null)
			viewRootMap.put("address1",this.address1 );
	
	
	}

	public String getAddress2() {
		if(viewRootMap.containsKey("address2"))
			address2=viewRootMap.get("address2").toString();
		
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
		if(this.address2  != null)
			viewRootMap.put("address2",this.address2 );
	}

	public String getStreet() {
		if(viewRootMap.containsKey("street"))
			street=viewRootMap.get("street").toString();
		
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
		if(this.street != null)
			viewRootMap.put("street",this.street );
	}

	public String getPostCode() {
		if(viewRootMap.containsKey("postCode"))
			postCode=viewRootMap.get("postCode").toString();
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
		if(this.postCode!= null)
			viewRootMap.put("postCode",this.postCode);
	}

	public String getCityId() {
		if(viewRootMap.containsKey("cityId"))
			cityId=viewRootMap.get("cityId").toString();
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
		if(this.cityId != null)
			viewRootMap.put("cityId",this.cityId );
	}

	public String getCountryId() {
		if(viewRootMap.containsKey("countryId"))
			countryId=viewRootMap.get("countryId").toString();
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
		if(this.countryId != null)
			viewRootMap.put("countryId",this.countryId  );
	}

	public String getHomePhone() {
		if(viewRootMap.containsKey("homePhone"))
			homePhone=viewRootMap.get("homePhone").toString();
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
		if(this.homePhone != null)
			viewRootMap.put("homePhone",this.homePhone  );
	}

	public String getOfficePhone() {
		if(viewRootMap.containsKey("officePhone"))
			officePhone=viewRootMap.get("officePhone").toString();
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
		if(this.officePhone != null)
			viewRootMap.put("officePhone",this.officePhone );
	}

	public String getFax() {
		if(viewRootMap.containsKey("fax"))
			fax=viewRootMap.get("fax").toString();
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
		if(this.fax != null)
			viewRootMap.put("fax",this.fax );
	}

	public String getEmail() {
		if(viewRootMap.containsKey("email"))
			email=viewRootMap.get("email").toString();
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		if(this.email != null)
			viewRootMap.put("email",this.email );
	}

	public List<SelectItem> getCityList() {
		
		if(viewRootMap.containsKey("cityList"))
			cityList= (List<SelectItem>) (viewRootMap.get("cityList"));
		
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
		if(this.cityList != null)
			viewRootMap.put("cityList",this.cityList );
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getContactInfoId() {
		return contactInfoId;
	}

	public void setContactInfoId(Long contactInfoId) {
		this.contactInfoId = contactInfoId;
	}

	public String getEdit() {
		return edit;
	}

	public void setEdit(String edit) {
		this.edit = edit;
	}

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getSelectedPersonType() {
		return selectedPersonType;
	}

	public void setSelectedPersonType(String selectedPersonType) {
		this.selectedPersonType = selectedPersonType;
	}

	public String getSelectedSubPersonType() {
		return selectedSubPersonType;
	}

	public void setSelectedSubPersonType(String selectedSubPersonType) {
		this.selectedSubPersonType = selectedSubPersonType;
	}

	public HtmlDataTable getRolesDataTable() {
		return rolesDataTable;
	}

	public void setRolesDataTable(HtmlDataTable rolesDataTable) {
		this.rolesDataTable = rolesDataTable;
	}

	public HtmlSelectOneMenu getCmbPersonType() {
		return cmbPersonType;
	}

	public void setCmbPersonType(HtmlSelectOneMenu cmbPersonType) {
		this.cmbPersonType = cmbPersonType;
	}

	public HtmlSelectOneMenu getCmbSubPersonType() {
		return cmbSubPersonType;
	}

	public void setCmbSubPersonType(HtmlSelectOneMenu cmbSubPersonType) {
		this.cmbSubPersonType = cmbSubPersonType;
	}

	public List<ContactInfoView> getContactInfoList() {
		if(viewRootMap.containsKey(CONTACT_INFO_LIST))
		{

			contactInfoList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
			//List<ContactInfoView> ciList=(List<ContactInfoView>)viewRootMap.get(CONTACT_INFO_LIST);
//		    for (ContactInfoView contactInfoView : ciList) {
//		    	if(contactInfoView.getIsDeleted().compareTo(new Long(0))==0)
//		    		contactInfoList.add(contactInfoView);
//				
			
		}
		return contactInfoList;
	}

	public void setContactInfoList(List<ContactInfoView> contactInfoList) {
		this.contactInfoList = contactInfoList;
	}

	public HtmlDataTable getContactInfoDataTable() {
		return contactInfoDataTable;
	}

	public void setContactInfoDataTable(HtmlDataTable contactInfoDataTable) {
		this.contactInfoDataTable = contactInfoDataTable;
	}

	public String getContactInfoCreatedBy() {
		if(viewRootMap.containsKey("contactInfoCreatedBy"))
			contactInfoCreatedBy=viewRootMap.get("contactInfoCreatedBy").toString();
		return contactInfoCreatedBy;
	}

	public void setContactInfoCreatedBy(String contactInfoCreatedBy) {
		this.contactInfoCreatedBy = contactInfoCreatedBy;
		if(this.contactInfoCreatedBy != null)
			viewRootMap.put("contactInfoCreatedBy",this.contactInfoCreatedBy );
	}

	public String getContactInfoCreatedOn() {
		if(viewRootMap.containsKey("contactInfoCreatedOn"))
			contactInfoCreatedOn=viewRootMap.get("contactInfoCreatedOn").toString();
		return contactInfoCreatedOn;
	}

	public void setContactInfoCreatedOn(String contactInfoCreatedOn) {
		this.contactInfoCreatedOn = contactInfoCreatedOn;
		
		if(this.contactInfoCreatedOn != null)
			viewRootMap.put("contactInfoCreatedOn",this.contactInfoCreatedOn );
		
	}

	public String getContactInfoRowId() {
		
		if(viewRootMap.containsKey("contactInfoRowId"))
			contactInfoRowId=viewRootMap.get("contactInfoRowId").toString();

		return contactInfoRowId;
	}

	public void setContactInfoRowId(String contactInfoRowId) {
		this.contactInfoRowId = contactInfoRowId;

		if(this.contactInfoRowId != null)
			viewRootMap.put("contactInfoRowId",this.contactInfoRowId );
		
	}

	public String getCrContactInfoCreatedBy() {
	if(viewRootMap.containsKey("crContactInfoCreatedBy"))
			crContactInfoCreatedBy=viewRootMap.get("crContactInfoCreatedBy").toString();
		return crContactInfoCreatedBy;
	}

	public void setCrContactInfoCreatedBy(String crContactInfoCreatedBy) {
		this.crContactInfoCreatedBy = crContactInfoCreatedBy;
		if(this.crContactInfoCreatedBy!= null)
			viewRootMap.put("crContactInfoCreatedBy",this.crContactInfoCreatedBy );
	}

	public String getCrContactInfoCreatedOn() {
	if(viewRootMap.containsKey("crContactInfoCreatedOn"))
			crContactInfoCreatedOn=viewRootMap.get("crContactInfoCreatedOn").toString();
		return crContactInfoCreatedOn;
	}

	public void setCrContactInfoCreatedOn(String crContactInfoCreatedOn) {
		this.crContactInfoCreatedOn = crContactInfoCreatedOn;
		if(this.crContactInfoCreatedOn!= null)
			viewRootMap.put("crContactInfoCreatedOn",this.crContactInfoCreatedOn );
	}

	public String getContactRefCreatedBy() {
	if(viewRootMap.containsKey("contactRefCreatedBy"))
			contactRefCreatedBy=viewRootMap.get("contactRefCreatedBy").toString();
		return contactRefCreatedBy;
	}

	public void setContactRefCreatedBy(String contactRefCreatedBy) {
		this.contactRefCreatedBy = contactRefCreatedBy;
		if(this.contactRefCreatedBy!= null)
			viewRootMap.put("contactRefCreatedBy",this.contactRefCreatedBy );
	}

	public String getContactRefCreatedOn() {
	if(viewRootMap.containsKey("contactRefCreatedOn"))
			contactRefCreatedOn=viewRootMap.get("contactRefCreatedOn").toString();
		return contactRefCreatedOn;
	}

	public void setContactRefCreatedOn(String contactRefCreatedOn) {
		this.contactRefCreatedOn = contactRefCreatedOn;
		if(this.contactRefCreatedOn!= null)
			viewRootMap.put("contactRefCreatedOn",this.contactRefCreatedOn );
	}

	public String getCRTitleId() {
	if(viewRootMap.containsKey("CRTitleId"))
			CRTitleId=viewRootMap.get("CRTitleId").toString();
		return CRTitleId;
	}

	public void setCRTitleId(String titleId) {
		CRTitleId = titleId;
		if(this.CRTitleId!= null)
			viewRootMap.put("CRTitleId",this.CRTitleId);
	}

	public String getCRFirstName() {
	if(viewRootMap.containsKey("CRFirstName"))
			CRFirstName=viewRootMap.get("CRFirstName").toString();
		return CRFirstName;
	}

	public void setCRFirstName(String firstName) {
		CRFirstName = firstName;
		if(this.CRFirstName!= null)
			viewRootMap.put("CRFirstName",this.CRFirstName);
	}

	public String getCRMiddleName() {
	if(viewRootMap.containsKey("CRMiddleName"))
			CRMiddleName=viewRootMap.get("CRMiddleName").toString();
		return CRMiddleName;
	}

	public void setCRMiddleName(String middleName) {
		CRMiddleName = middleName;
		if(this.CRMiddleName!= null)
			viewRootMap.put("CRMiddleName",this.CRMiddleName);
	}

	public String getCRLastName() {
	if(viewRootMap.containsKey("CRLastName"))
			CRLastName=viewRootMap.get("CRLastName").toString();
		return CRLastName;
	}

	public void setCRLastName(String lastName) {
		CRLastName = lastName;
		if(this.CRLastName != null)
			viewRootMap.put("CRLastName",this.CRLastName);
	}

	public String getCRAddress1() {
	if(viewRootMap.containsKey("CRAddress1"))
			CRAddress1=viewRootMap.get("CRAddress1").toString();
		return CRAddress1;
	}

	public void setCRAddress1(String address1) {
		CRAddress1 = address1;
		if(this.CRAddress1 != null)
			viewRootMap.put("CRAddress1",this.CRAddress1);
	}

	public String getCRAddress2() {
	if(viewRootMap.containsKey("CRAddress2"))
			CRAddress2=viewRootMap.get("CRAddress2").toString();
		return CRAddress2;
	}

	public void setCRAddress2(String address2) {
		CRAddress2 = address2;
		if(this.CRAddress2 != null)
			viewRootMap.put("CRAddress2",this.CRAddress2);
	}

	public String getCRStreet() {
	if(viewRootMap.containsKey("CRStreet"))
			CRStreet=viewRootMap.get("CRStreet").toString();
		return CRStreet;
	}

	public void setCRStreet(String street) {
		CRStreet = street;
		if(this.CRStreet != null)
			viewRootMap.put("CRStreet",this.CRStreet);
	}

	public String getCRPostCode() {
	if(viewRootMap.containsKey("CRPostCode"))
			CRPostCode=viewRootMap.get("CRPostCode").toString();
		return CRPostCode;
	}

	public void setCRPostCode(String postCode) {
		CRPostCode = postCode;
		if(this.CRPostCode != null)
			viewRootMap.put("CRPostCode",this.CRPostCode);
	}

	public String getCRCountryId() {
	if(viewRootMap.containsKey("CRCountryId"))
			CRCountryId=viewRootMap.get("CRCountryId").toString();
		return CRCountryId;
	}

	public void setCRCountryId(String countryId) {
		CRCountryId = countryId;
		if(this.CRCountryId!= null)
			viewRootMap.put("CRCountryId",this.CRCountryId);
	}

	public String getCRStateId() {
	if(viewRootMap.containsKey("CRStateId"))
			CRStateId=viewRootMap.get("CRStateId").toString();
		return CRStateId;
	}

	public void setCRStateId(String stateId) {
		CRStateId = stateId;
		if(this.CRStateId!= null)
			viewRootMap.put("CRStateId",this.CRStateId);
	}

	public String getCRCityId() {
	if(viewRootMap.containsKey("CRCityId"))
			CRCityId=viewRootMap.get("CRCityId").toString();
		return CRCityId;
	}

	public void setCRCityId(String cityId) {
		CRCityId = cityId;
		if(this.CRCityId != null)
			viewRootMap.put("CRCityId",this.CRCityId );
	}

	public String getCRHomePhone() {
	if(viewRootMap.containsKey("CRHomePhone"))
			CRHomePhone=viewRootMap.get("CRHomePhone").toString();
		return CRHomePhone;
	}

	public void setCRHomePhone(String homePhone) {
		CRHomePhone = homePhone;
		if(this.CRHomePhone!= null)
			viewRootMap.put("CRHomePhone",this.CRHomePhone);
	}

	public String getCROfficePhone() {
	if(viewRootMap.containsKey("CROfficePhone"))
			CROfficePhone=viewRootMap.get("CROfficePhone").toString();
		return CROfficePhone;
	}

	public void setCROfficePhone(String officePhone) {
		CROfficePhone = officePhone;
		if(this.CROfficePhone!= null)
			viewRootMap.put("CROfficePhone",this.CROfficePhone);
	}

	public String getCRFax() {
	if(viewRootMap.containsKey("CRFax"))
			CRFax=viewRootMap.get("CRFax").toString();
		return CRFax;
	}

	public void setCRFax(String fax) {
		CRFax = fax;
		if(this.CRFax!= null)
			viewRootMap.put("CRFax",this.CRFax);
	}

	public String getCREmail() {
	if(viewRootMap.containsKey("CREmail"))
			CREmail=viewRootMap.get("CREmail").toString();
		return CREmail;
	}

	public void setCREmail(String email) {
		CREmail = email;
		if(this.CREmail!= null)
			viewRootMap.put("CREmail",this.CREmail);
	}

	public List<SelectItem> getCRStateList() {
	
	    if(viewRootMap.containsKey("CRStateList"))
	      CRStateList = ( ArrayList<SelectItem> )viewRootMap.get("CRStateList");
		return CRStateList;
	}

	public void setCRStateList(List<SelectItem> stateList) {
		CRStateList = stateList;
	}

	
	public List<SelectItem> getCRCityList() {
	   if(viewRootMap.containsKey("CRCityList"))
	      CRCityList = ( ArrayList<SelectItem> )viewRootMap.get("CRCityList");
		return CRCityList;
	}

	public void setCRCityList(List<SelectItem> cityList) {
		CRCityList = cityList;
	}

	public HtmlDataTable getContactRefDataTable() {
		return contactRefDataTable;
	}

	public void setContactRefDataTable(HtmlDataTable contactRefDataTable) {
		this.contactRefDataTable = contactRefDataTable;
	}

	public List<ContactReferenceView> getContactRefList() {
	if(viewRootMap.containsKey(CONTACT_REF_LIST))
 		  contactRefList=(List<ContactReferenceView>)viewRootMap.get(CONTACT_REF_LIST);
		return contactRefList;
	}

	public void setContactRefList(List<ContactReferenceView> contactRefList) {
		this.contactRefList = contactRefList;
	}

	public String getTxtCompanyName() {
		if(viewRootMap.containsKey("txtCompanyName"))
			txtCompanyName=viewRootMap.get("txtCompanyName").toString();
		return txtCompanyName;
	}

	public void setTxtCompanyName(String txtCompanyName) {
		this.txtCompanyName = txtCompanyName;
		if(this.txtCompanyName!= null)
			viewRootMap.put("txtCompanyName",this.txtCompanyName);
	}

	public String getTxtGovtDept() {
		if(viewRootMap.containsKey("txtGovtDept"))
			txtGovtDept=viewRootMap.get("txtGovtDept").toString();
		return txtGovtDept;
	}

	public void setTxtGovtDept(String txtGovtDept) {
		this.txtGovtDept = txtGovtDept;
		if(this.txtGovtDept != null)
			viewRootMap.put("txtGovtDept",this.txtGovtDept );
	}

	public String getTxtLicenseno() {
		if(viewRootMap.containsKey("txtLicenseno"))
			txtLicenseno=viewRootMap.get("txtLicenseno").toString();
		return txtLicenseno;
	}

	public void setTxtLicenseno(String txtLicenseno) {
		this.txtLicenseno = txtLicenseno;
		if(this.txtLicenseno != null)
			viewRootMap.put("txtLicenseno",this.txtLicenseno );
	}

	public String getLicenseIssuePlaceId() {
		if(viewRootMap.containsKey("licenseIssuePlaceId"))
			licenseIssuePlaceId=viewRootMap.get("licenseIssuePlaceId").toString();
		return licenseIssuePlaceId;
	}

	public void setLicenseIssuePlaceId(String licenseIssuePlaceId) {
		this.licenseIssuePlaceId = licenseIssuePlaceId;
		if(this.licenseIssuePlaceId != null)
			viewRootMap.put("licenseIssuePlaceId",this.licenseIssuePlaceId );
	}

	public Date getLicenseIssueDate() {
		
		try
		{
		if(viewRootMap.containsKey("licenseIssueDate"))
			licenseIssueDate=(Date)viewRootMap.get("licenseIssueDate");
		}
		catch(Exception ex)
		{
			logger.LogException("getLicenseIssueDate|Error Occured", ex);
			
		}
		return licenseIssueDate;
		
	}

	public void setLicenseIssueDate(Date licenseIssueDate) {
		this.licenseIssueDate = licenseIssueDate;
		if(this.licenseIssueDate!= null)
			viewRootMap.put("licenseIssueDate",this.licenseIssueDate);
	}

	public Date getLicenseExpiryDate() {
		try
		{
		if(viewRootMap.containsKey("licenseExpiryDate"))
			licenseExpiryDate=(Date)viewRootMap.get("licenseExpiryDate");
		}
		catch(Exception ex)
		{
			logger.LogException("getLicenseExpiryDate|Error Occured", ex);
			
		}
		
		return licenseExpiryDate;
	}

	public void setLicenseExpiryDate(Date licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
		if(this.licenseExpiryDate!= null)
			viewRootMap.put("licenseExpiryDate",this.licenseExpiryDate);
	}

	public String getSelectedPersonKind() {
		if(viewRootMap.containsKey("selectedPersonKind"))
			selectedPersonKind=viewRootMap.get("selectedPersonKind").toString();
		
		return selectedPersonKind;
	}

	public void setSelectedPersonKind(String selectedPersonKind) {
		this.selectedPersonKind = selectedPersonKind;
		if(this.selectedPersonKind!= null)
			viewRootMap.put("selectedPersonKind",this.selectedPersonKind);
	}

	public org.richfaces.component.html.HtmlTab getTabPersonIndividual() {
		return tabPersonIndividual;
	}

	public void setTabPersonIndividual(
			org.richfaces.component.html.HtmlTab tabPersonIndividual) {
		this.tabPersonIndividual = tabPersonIndividual;
	}

	public org.richfaces.component.html.HtmlTab getTabPersonCompany() {
		return tabPersonCompany;
	}

	public void setTabPersonCompany(
			org.richfaces.component.html.HtmlTab tabPersonCompany) {
		this.tabPersonCompany = tabPersonCompany;
	}

	public org.richfaces.component.html.HtmlTab getTabContactReferences() {
		return tabContactReferences;
	}

	public void setTabContactReferences(
			org.richfaces.component.html.HtmlTab tabContactReferences) {
		this.tabContactReferences = tabContactReferences;
	}

	public String getPageMode() {
		if(viewRootMap.containsKey("pageMode"))
			pageMode=viewRootMap.get("pageMode").toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
		if(this.pageMode!= null)
			viewRootMap.put("pageMode",this.pageMode);
	}
	public HtmlSelectOneMenu getCmb_SelectedPersonKind() {
		return cmb_SelectedPersonKind;
	}
	public void setCmb_SelectedPersonKind(HtmlSelectOneMenu cmb_SelectedPersonKind) {
		this.cmb_SelectedPersonKind = cmb_SelectedPersonKind;
	}
	public String getCRCellNumber() {
		if(viewRootMap.containsKey("CellNumber"))
			CRCellNumber=viewRootMap.get("CellNumber").toString();

		return CRCellNumber;
	}
	public void setCRCellNumber(String cellNumber) {
		CRCellNumber = cellNumber;
		if(this.CRCellNumber !=null)
			viewRootMap.put("CRCellNumber", this.CRCellNumber );
	}
	public List<String> getBusinessActivities() {
		if(viewRootMap.containsKey("businessActivities"))
			businessActivities = (ArrayList<String>)viewRootMap.get("businessActivities");

		return businessActivities;
	}
	public void setBusinessActivities(List<String> businessActivities) {
		this.businessActivities = businessActivities;
		if(this.businessActivities  !=null && this.businessActivities.size()>0)
			viewRootMap.put("businessActivities", this.businessActivities);

	}
	public Date getResidenseVisaIssueDate() {
		try
		{
			if(viewRootMap.containsKey("residenseVisaIssueDate"))
				residenseVisaIssueDate = (Date)viewRootMap.get("residenseVisaIssueDate");
			
		}
		catch(Exception ex)
		{
			logger.LogException("getresidenseVisaIssueDate|Error Occured", ex);
			
		}
		return residenseVisaIssueDate;
	}
	public void setResidenseVisaIssueDate(Date residenseVisaIssueDate) {
		this.residenseVisaIssueDate = residenseVisaIssueDate;
		if(this.residenseVisaIssueDate!=null)
			viewRootMap.put("residenseVisaIssueDate",this.residenseVisaIssueDate);
	}
	public String getWorkingCompany() {
		if(viewRootMap.containsKey("workingCompany"))
			workingCompany = viewRootMap.get("workingCompany").toString();

		return workingCompany;
	}
	public void setWorkingCompany(String workingCompany) {
		this.workingCompany = workingCompany;
		if(this.workingCompany!=null)
			viewRootMap.put("workingCompany",this.workingCompany);
	}
	public String getMaritialStatusId() {
		if(viewRootMap.containsKey("maritialStatusId"))
			maritialStatusId = viewRootMap.get("maritialStatusId").toString();

		return maritialStatusId;
	}
	public void setMaritialStatusId(String maritialStatusId) {
		this.maritialStatusId = maritialStatusId;
		if(this.maritialStatusId!=null)
			viewRootMap.put("maritialStatusId",this.maritialStatusId);
	}
	public String getGrpCustomerNo() {
		if(viewRootMap.containsKey("grpCustomerNo"))
			grpCustomerNo = viewRootMap.get("grpCustomerNo").toString();
		return grpCustomerNo;
	}
	public void setGrpCustomerNo(String grpCustomerNo) {
		this.grpCustomerNo = grpCustomerNo;
		if(this.grpCustomerNo!=null)
			viewRootMap.put("grpCustomerNo",this.grpCustomerNo);
	}
	public String getStatusId() {
		if(viewRootMap.containsKey("statusId"))
			statusId = viewRootMap.get("statusId").toString();
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
		if(this.statusId!=null)
			viewRootMap.put("statusId",this.statusId);
	}
	public List<ViolationView> getViolationsList() {
		if(viewRootMap.containsKey("violationsList"))
			violationsList = (ArrayList<ViolationView>)viewRootMap.get("violationsList");
		return violationsList;
	}
	public void setViolationsList(List<ViolationView> violationsList) {
		this.violationsList = violationsList;
		if(this.violationsList!=null)
		  viewRootMap.put("violationsList",this.violationsList);
	}
	public List<BlackListView> getBlackListViewList() {
		if(viewRootMap.containsKey("blackListViewList"))
			blackListViewList = (ArrayList<BlackListView>)viewRootMap.get("blackListViewList");
		return blackListViewList;
	}
	public void setBlackListViewList(List<BlackListView> blackListViewList) {
		this.blackListViewList = blackListViewList;
		if(this.violationsList!=null)
			  viewRootMap.put("blackListViewList",this.blackListViewList);
	}
	public String getTxt_removalReason() {
		if(viewRootMap.containsKey("txt_removalReason"))
			txt_removalReason = viewRootMap.get("txt_removalReason").toString();
		return txt_removalReason;
	}
	public void setTxt_removalReason(String txt_removalReason) {
		this.txt_removalReason = txt_removalReason;
		if(this.txt_removalReason!=null)
			  viewRootMap.put("txt_removalReason",this.txt_removalReason);
	}
	public boolean isBlackListed() {
		if(viewRootMap.containsKey("blackListed"))
			blackListed = (Boolean)viewRootMap.get("blackListed");
		return blackListed;
	}
	public void setBlackListed(boolean blackListed) {
		this.blackListed = blackListed;
		viewRootMap.put("blackListed",this.blackListed);
	}

	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	
	
	public void generateLoginId() throws PimsBusinessException
	{
		//errorMessages.clear();
		errorMessages = new ArrayList<String>(0);
		PersonView person = (PersonView)viewRootMap.get(PERSON_VIEW);
		try
		{
		
		if(person.getPersonId() != null)
		{
			UserDbImpl user = new UserDbImpl();
			
			user.setLoginId(person.getPersonId().toString());
			user.setFullName( person.getPersonFullName() );
			if( person.getFirstName()!= null && person.getFirstName().trim().length() > 0 )
			{
				user.setFirstName(person.getFirstName().trim());
				if(person.getMiddleName()!= null && !person.getMiddleName().trim().equals("") )
				{
					user.setMiddleName(person.getMiddleName());
				}
				
				if(person.getLastName()!= null && !person.getLastName().trim().equals("") )
				{
					user.setLastName(person.getLastName());
				}
			}
			else 
			{
				user.setFirstName(person.getCompanyName());
			}
			
			user.setPolicyId("admin");
			user.setStatusId("0");
			user.setSystemUser(true);	
			
			Set<ContactInfoView> tempList = new HashSet<ContactInfoView>();
			tempList = person.getContactInfoViewSet();
			String firstEmail = null;
			for(ContactInfoView cInfo : tempList)
			{
				if(cInfo.getEmail() != null)
				{
					firstEmail = cInfo.getEmail();
					break;
				}
				
			}
			
			if(firstEmail == null)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						(MessageConstants.Person.MSG_EMAIL_MANDATORY_FOR_ACTIVATION)));
				return;
			}
			user.setEmailUrl(firstEmail);
			
			Random randomNumer = new Random();
			Integer pin = randomNumer.nextInt(90000)+10000;
			user.setPassword(pin.toString());
			
			SecurityManager.persistUser(user);
			
			//adding user group
			
			SecUserGrp internetUserGrp = getUserGroup();
			if(internetUserGrp != null)
			{
				deleteGroupBinding(user.getLoginId(), internetUserGrp.getUserGroup().getUserGroupId());
				SecUserUserGroupId complexKey = new SecUserUserGroupId();
				GroupUserBinding groupUserBinding = new GroupUserBinding();
				complexKey.setUserGroupId(internetUserGrp.getUserGroup().getUserGroupId());
				complexKey.setLoginId(user.getLoginId());
				groupUserBinding.setUserGroupBinding(complexKey);
				SecurityManager.persistBinding(groupUserBinding);
				
			}
			
			viewRootMap.put("loginId", user.getLoginId());
			person.setLoginId(user.getLoginId());
			btnSavePerson_Click();
			
			//makeEntryInOID();
			sendActivationEmail(user,person);
		} //if(person.getPersonId() != null)
		}//try
		catch(Exception e)
		{	
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
				
	}
	
	private SecUserGrp getUserGroup()
	{
		Search query = new Search();
		query.clear();
		query.addFrom(UserGroup.class);
		List<UserGroup> allUserGroups =  SecurityManager.findGroup(query);
		SecUserGrp secUserGroup = new SecUserGrp();
		for (UserGroup ug : allUserGroups) 
		{
			if(ug.getUserGroupId().compareTo(WebConstants.UserGroups.INTERNET_USERS_GROUP) == 0)
			{
				
				secUserGroup.setUserGroup(ug);
				return secUserGroup;
				
			}
		}
		return null;
		
	}
	
	private void deleteGroupBinding(String loginId, 
			String userGroupId) {

		GroupUserBinding groupUserBinding = null;
		try{	
			groupUserBinding = EntityManager.getBroker().findById(GroupUserBinding.class, new SecUserUserGroupId(userGroupId, loginId));
			if(groupUserBinding!=null)
				EntityManager.getBroker().delete(groupUserBinding);
		}
		catch(Throwable t){
			t.printStackTrace();
		}
	}
	
	private void sendActivationEmail(UserDbImpl user, PersonView person)
	{
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		if (user != null)
			contactInfoList.add(new ContactInfo(person.getPersonId().toString(), person.getCellNumber(), null, user.getEmailUrl()));
		
		UtilityService uService = new UtilityService();
		String link = uService.getActivationLink();
		String type = "&T=";
		String encryptedUID = "?UID="; 
		encryptedUID+= new Cryptographer().encrypt(person.getPersonId().toString());
		link += encryptedUID;
		link += type;
		link += new Cryptographer().encrypt("person");
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		eventAttributesValueMap.put("USER_NAME", user.getFullName());
		eventAttributesValueMap.put("PIN", user.getPassword());
		eventAttributesValueMap.put("ACTIVATION_LINK", link);
		generateNotification(WebConstants.Notification_MetaEvents.Event_Activation_Email, contactInfoList, eventAttributesValueMap,null);
	}
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		boolean success = false;
		
		try
		{
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
		}
		catch (Exception exception)
		{
			logger.LogException( "generateNotification --- Exception Occured --- ", exception);
		}
		
		return success;
	}

	public Boolean getIsGenerateLogin()
	{
		PersonView person = (PersonView)viewRootMap.get(PERSON_VIEW);
		if(person != null)
		{
			if(person.getPersonId() != null && person.getLoginId() == null)
				return true;
		}
		return false;
		
	}
	@SuppressWarnings("unchecked")
	public Boolean getIsGrpCustomerNumberReadOnly() 
	{
		if( viewRootMap.get( "isGrpCustomerNumberReadOnly" ) != null )
			isGrpCustomerNumberReadOnly = (Boolean)viewRootMap.get( "isGrpCustomerNumberReadOnly" ); 
		return isGrpCustomerNumberReadOnly;
	}
	@SuppressWarnings("unchecked")
	public void setIsGrpCustomerNumberReadOnly(Boolean isGrpCustomerNumberReadOnly) 
	{
		this.isGrpCustomerNumberReadOnly = isGrpCustomerNumberReadOnly;
		if(this.isGrpCustomerNumberReadOnly != null)
		viewRootMap.put( "isGrpCustomerNumberReadOnly", this.isGrpCustomerNumberReadOnly );
	}
	public String getReadOnlyStyleClass()
	{
		if( this.getIsGrpCustomerNumberReadOnly() )
		 return "READONLY";
		
		return "";
	}
	public List<SelectItem> getPassportCityList() {
	  if (viewMap.get("passportCityList") != null )
	  {
		  passportCityList = (List<SelectItem>)viewMap.get("passportCityList");
	  }
		return passportCityList;
	}
	public void setPassportCityList(List<SelectItem> passportCityList) {
		this.passportCityList = passportCityList;
		if(this.passportCityList != null)
		{
			viewMap.put("passportCityList" ,this.passportCityList );
		}
	}
	public String getFullNameEn() {
		if(viewRootMap.containsKey("fullNameEn"))
			fullNameEn=viewRootMap.get("fullNameEn").toString();
		return fullNameEn;
	}
	public void setFullNameEn(String fullNameEn) {
		this.fullNameEn = fullNameEn;
		if(this.fullNameEn !=null)
			viewRootMap.put("fullNameEn",this.fullNameEn );
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getTradeLicenseIssuersCity() {
		if(viewRootMap.get("tradeLicenseIssuersCity") != null)
		{
			this.tradeLicenseIssuersCity= (ArrayList<SelectItem>) viewRootMap.get("tradeLicenseIssuersCity");
		}
		return tradeLicenseIssuersCity;
	}
	public void setTradeLicenseIssuersCity(List<SelectItem> tradeLicenseIssuersCity) {
		this.tradeLicenseIssuersCity = tradeLicenseIssuersCity;
		if(this.tradeLicenseIssuersCity  !=null)
			viewRootMap.put("tradeLicenseIssuersCity",this.tradeLicenseIssuersCity  );
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getTradeLicenseIssuersList() {
		if(viewRootMap.get("tradeLicenseIssuersList") != null)
		{
			this.tradeLicenseIssuersList= (ArrayList<SelectItem>) viewRootMap.get("tradeLicenseIssuersList");
		}
		return tradeLicenseIssuersList;
	}

	public void setTradeLicenseIssuersList(List<SelectItem> tradeLicenseIssuersList) {
		this.tradeLicenseIssuersList = tradeLicenseIssuersList;
		if(this.tradeLicenseIssuersList != null )
		{
			viewRootMap.put("tradeLicenseIssuersList",tradeLicenseIssuersList);
		}
	}
	public List<SelectItem> getLicenseIssuingEmirateList() {
		if(viewRootMap.get("licenseIssuingEmirateList") != null)
		{
			this.licenseIssuingEmirateList= (ArrayList<SelectItem>) viewRootMap.get("licenseIssuingEmirateList");
		}
		return licenseIssuingEmirateList;
	}
	public void setLicenseIssuingEmirateList(
			List<SelectItem> licenseIssuingEmirateList) {
		this.licenseIssuingEmirateList = licenseIssuingEmirateList;
		if(this.licenseIssuingEmirateList != null )
		{
			viewRootMap.put("licenseIssuingEmirateList",licenseIssuingEmirateList);
		}
	}
	public String getTradeLicenseIssuersId() {
		if(viewRootMap.containsKey("tradeLicenseIssuersId"))
			tradeLicenseIssuersId=viewRootMap.get("tradeLicenseIssuersId").toString();
		return tradeLicenseIssuersId;
	}
	public void setTradeLicenseIssuersId(String tradeLicenseIssuersId) {
		this.tradeLicenseIssuersId = tradeLicenseIssuersId;
		if(this.tradeLicenseIssuersId != null )
		{
			viewRootMap.put("tradeLicenseIssuersId",tradeLicenseIssuersId);
		}
	}
	
}









