package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyFacilityView;
import com.avanza.pims.ws.vo.ReceivePropertyFloorView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.avanza.pims.ws.vo.UnitView;

public class ApplyRentValuePopup extends AbstractController {
	private HtmlSelectOneMenu floorFromMenu =new HtmlSelectOneMenu();
	private HtmlSelectOneMenu floorToMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitFromMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitToMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitSideMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu investmentTypeMenu = new HtmlSelectOneMenu();
	private HtmlInputText numberOfBedroom = new HtmlInputText();
	private HtmlCommandButton searchButton = new HtmlCommandButton();
	private HtmlDataTable searchResults = new HtmlDataTable();
	private List<ReceivePropertyUnitView> unitDataList = new ArrayList<ReceivePropertyUnitView>();
	private HtmlInputText suggestedRentValue = new HtmlInputText();
	private HtmlInputHidden propertyId = new HtmlInputHidden();
	private List<SelectItem> floorFromList = new ArrayList<SelectItem>();
	private List<SelectItem> floorToList = new ArrayList<SelectItem>();
	private List<SelectItem> unitFromList = new ArrayList<SelectItem>();
	private List<SelectItem> unitToList = new ArrayList<SelectItem>();
	private List<SelectItem> unitSideTypeList = new ArrayList<SelectItem>();
	private List<SelectItem> unitTypeList = new ArrayList<SelectItem>();
	private List<SelectItem> investmentTypeList = new ArrayList<SelectItem>();


	private static Logger logger = Logger.getLogger(DuplicateFloors.class);

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		
		

	}
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub

		super.preprocess();
		if(getFacesContext().getExternalContext().getSessionMap().containsKey("PROPERTY_ID_DUPLICATE_UNITS")){
			propertyId.setValue(getFacesContext().getExternalContext().getSessionMap().get("PROPERTY_ID_DUPLICATE_UNITS").toString());
			getFacesContext().getExternalContext().getSessionMap().remove("PROPERTY_ID_DUPLICATE_UNITS");
		}
		if(propertyId.getValue()!=null&&!(getFacesContext().getViewRoot().getAttributes().containsKey("FLOOR_LIST_IN_VIEWROOT")||
				getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT")==null)){
			loadFloorsFrom(Long.valueOf(propertyId.getValue().toString()));
			
			

		}
		else{
			floorFromList = (List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT");
			
		}
		if(propertyId.getValue()!=null&&(!getFacesContext().getViewRoot().getAttributes().containsKey("UNIT_LIST_IN_VIEWROOT")||
				getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT")==null)){
			loadUnitsFrom(Long.valueOf(propertyId.getValue().toString()));
		}
		else{
			unitFromList = (List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT");
		}
		loadCombos();

	}
	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		if(getFacesContext().getExternalContext().getSessionMap().containsKey("PROPERTY_ID_DUPLICATE_UNITS")){
			propertyId.setValue(getFacesContext().getExternalContext().getSessionMap().get("PROPERTY_ID_DUPLICATE_UNITS").toString());
			getFacesContext().getExternalContext().getSessionMap().remove("PROPERTY_ID_DUPLICATE_UNITS");
			loadFloorsFrom(Long.valueOf(propertyId.getValue().toString()));
			loadUnitsFrom(Long.valueOf(propertyId.getValue().toString()));
		}
		if(propertyId.getValue()!=null&&!(getFacesContext().getViewRoot().getAttributes().containsKey("FLOOR_LIST_IN_VIEWROOT")||
				getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT")==null)){
			loadFloorsFrom(Long.valueOf(propertyId.getValue().toString()));
		}
		else{
			floorFromList = (List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT");
		}
		if(propertyId.getValue()!=null&&(!getFacesContext().getViewRoot().getAttributes().containsKey("UNIT_LIST_IN_VIEWROOT")||
				getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT")==null)){
			loadUnitsFrom(Long.valueOf(propertyId.getValue().toString()));
		}
		else{
			unitFromList = (List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT");
		}
		loadCombos();


	}

	private void loadFloorsFrom(Long propertyId)  {

		String methodName="loadFacility"; 
		logger.logInfo(methodName+"|"+"Start");
		try {
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			List <ReceivePropertyFloorView> floorViewList = propertyServiceAgent.getPropertyFloorList(propertyId);
			floorFromList=new ArrayList<SelectItem>();

			for(int i=0;i<floorViewList.size();i++)
			{
				ReceivePropertyFloorView rcvPropertyFloorVO=(ReceivePropertyFloorView)floorViewList.get(i);


				floorFromList.add(new SelectItem(rcvPropertyFloorVO.getFloorId().toString(), rcvPropertyFloorVO.getFloorName()));
				floorToList.add(new SelectItem(rcvPropertyFloorVO.getFloorId().toString(), rcvPropertyFloorVO.getFloorName()));
				getFacesContext().getViewRoot().getAttributes().put("FLOOR_LIST_IN_VIEWROOT",floorFromList);
			}



			logger.logInfo(methodName+"|"+"Finish");
		}catch (Exception e){
			logger.LogException(methodName+"|Error Occured ",e);

		}
	}

	
	private Boolean validateData(){
		return true;
	}
	public void searchUnits(){
		
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		if(validateData()){
			try {
				Double noOfBeds = 0.00;
				if(numberOfBedroom.getValue().toString().trim().length()>0){
					noOfBeds = Double.valueOf(numberOfBedroom.getValue().toString());
				}
				
				unitDataList = propertyServiceAgent.getPropertyUnitList(Long.valueOf(propertyId.getValue().toString()),
						Long.valueOf(floorFromMenu.getValue().toString()),
						Long.valueOf(floorToMenu.getValue().toString()) ,
						Long.valueOf(unitFromMenu.getValue().toString()),
						Long.valueOf(unitToMenu.getValue().toString()),
						Long.valueOf(unitSideMenu.getValue().toString()),
						Long.valueOf(unitTypeMenu.getValue().toString()),
						Long.valueOf(investmentTypeMenu.getValue().toString()),
						noOfBeds);
				getFacesContext().getViewRoot().getAttributes().put("APPLY_RENT_VALUE_UNIT_DATA_LIST",unitDataList);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PimsBusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	public Integer getUnitListRecordSize(){

		return unitDataList.size();
	}

	private void loadUnitsFrom(Long propertyId)  {

		String methodName="loadFacility"; 
		logger.logInfo(methodName+"|"+"Start");
		try {
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			List <ReceivePropertyUnitView> unitViewList = propertyServiceAgent.getPropertyUnitList(propertyId);
			unitFromList =new ArrayList<SelectItem>();

			for(int i=0;i<unitViewList.size();i++)
			{
				ReceivePropertyUnitView rcvPropertyUnitVO=(ReceivePropertyUnitView)unitViewList.get(i);


				unitFromList.add(new SelectItem(rcvPropertyUnitVO.getUnitId().toString(), rcvPropertyUnitVO.getUnitNumber()));
				unitToList.add(new SelectItem(rcvPropertyUnitVO.getUnitId().toString(), rcvPropertyUnitVO.getUnitNumber()));
			}

			getFacesContext().getViewRoot().getAttributes().put("UNIT_LIST_IN_VIEWROOT",unitFromList);

			logger.logInfo(methodName+"|"+"Finish");
		}catch (Exception e){
			logger.LogException(methodName+"|Error Occured ",e);

		}
	}
	private void loadCombos(){
		ApplicationBean applicationBean = new ApplicationBean();
		unitSideTypeList = applicationBean.getUnitSideType();
		unitTypeList= applicationBean.getUnitTypeList();
		investmentTypeList = applicationBean.getInvestmentPurposeList();

	}
	public void applyRentValue(){
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		try{

			for(ReceivePropertyUnitView receivePropertyUnitView: unitDataList){
				if(receivePropertyUnitView.getSelected()!=null&&
						receivePropertyUnitView.getSelected())
					if(suggestedRentValue.getValue()!=null)
						receivePropertyUnitView.setRentValue(Double.valueOf(suggestedRentValue.getValue().toString()));

				propertyServiceAgent.updateUnitForReceiveProperty(receivePropertyUnitView);
				getFacesContext().getExternalContext().getSessionMap().put("UNIT_DATA_LIST_FROM_POPUP",true);
				String javaScriptText = "javascript:closeRefresh();";
				openPopup(javaScriptText);
			}



		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	private void loadUnitsTo(Long propertyId)  {

		String methodName="loadFacility"; 
		logger.logInfo(methodName+"|"+"Start");
		try {
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			List <ReceivePropertyUnitView> unitViewList = propertyServiceAgent.getPropertyUnitList(propertyId);
			unitToList =new ArrayList<SelectItem>();

			for(int i=0;i<unitViewList.size();i++)
			{
				ReceivePropertyUnitView rcvPropertyUnitVO=(ReceivePropertyUnitView)unitViewList.get(i);


				unitToList.add(new SelectItem(rcvPropertyUnitVO.getUnitId().toString(), rcvPropertyUnitVO.getUnitNumber()));			  
			}




			logger.logInfo(methodName+"|"+"Finish");
		}catch (Exception e){
			logger.LogException(methodName+"|Error Occured ",e);

		}
	}

	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}


	public HtmlSelectOneMenu getFloorFromMenu() {
		return floorFromMenu;
	}
	public void setFloorFromMenu(HtmlSelectOneMenu floorFromMenu) {
		this.floorFromMenu = floorFromMenu;
	}
	public HtmlSelectOneMenu getInvestmentTypeMenu() {
		return investmentTypeMenu;
	}
	public void setInvestmentTypeMenu(HtmlSelectOneMenu investmentTypeMenu) {
		this.investmentTypeMenu = investmentTypeMenu;
	}
	public HtmlInputText getNumberOfBedroom() {
		return numberOfBedroom;
	}
	public void setNumberOfBedroom(HtmlInputText numberOfBedroom) {
		this.numberOfBedroom = numberOfBedroom;
	}
	public HtmlCommandButton getSearchButton() {
		return searchButton;
	}
	public void setSearchButton(HtmlCommandButton searchButton) {
		this.searchButton = searchButton;
	}
	public HtmlDataTable getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(HtmlDataTable searchResults) {
		this.searchResults = searchResults;
	}
	public HtmlInputText getSuggestedRentValue() {
		return suggestedRentValue;
	}
	public void setSuggestedRentValue(HtmlInputText suggestedRentValue) {
		this.suggestedRentValue = suggestedRentValue;
	}
	public List<ReceivePropertyUnitView> getUnitDataList() {
		if(getFacesContext().getViewRoot().getAttributes().containsKey("APPLY_RENT_VALUE_UNIT_DATA_LIST")&&
				getFacesContext().getViewRoot().getAttributes().get("APPLY_RENT_VALUE_UNIT_DATA_LIST")!=null)
			

		unitDataList = (ArrayList<ReceivePropertyUnitView>)getFacesContext().getViewRoot().getAttributes().get("APPLY_RENT_VALUE_UNIT_DATA_LIST");

		return unitDataList;
	}
	public void setUnitDataList(List<ReceivePropertyUnitView> unitDataList) {
		this.unitDataList = unitDataList;
	}
	public HtmlSelectOneMenu getUnitFromMenu() {
		return unitFromMenu;
	}
	public void setUnitFromMenu(HtmlSelectOneMenu unitFromMenu) {
		this.unitFromMenu = unitFromMenu;
	}
	public HtmlSelectOneMenu getUnitSideMenu() {
		return unitSideMenu;
	}
	public void setUnitSideMenu(HtmlSelectOneMenu unitSideMenu) {
		this.unitSideMenu = unitSideMenu;
	}
	public HtmlSelectOneMenu getUnitToMenu() {
		return unitToMenu;
	}
	public void setUnitToMenu(HtmlSelectOneMenu unitToMenu) {
		this.unitToMenu = unitToMenu;
	}
	public HtmlSelectOneMenu getUnitTypeMenu() {
		return unitTypeMenu;
	}
	public void setUnitTypeMenu(HtmlSelectOneMenu unitTypeMenu) {
		this.unitTypeMenu = unitTypeMenu;
	}
	public List<SelectItem> getFloorFromList() {
		if(getFacesContext().getViewRoot().getAttributes().containsKey("FLOOR_LIST_IN_VIEWROOT")
				&&getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT")!=null){
			floorFromList=(List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT");
		}
		
		return floorFromList;
	}
	public void setFloorFromList(List<SelectItem> floorFromList) {
		this.floorFromList = floorFromList;
	}
	public List<SelectItem> getFloorToList() {
		if(getFacesContext().getViewRoot().getAttributes().containsKey("FLOOR_LIST_IN_VIEWROOT")
				&&getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT")!=null){
			floorToList=(List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("FLOOR_LIST_IN_VIEWROOT");
		}
		return floorToList;
	}
	public void setFloorToList(List<SelectItem> floorToList) {
		this.floorToList = floorToList;
	}
	public HtmlSelectOneMenu getFloorToMenu() {
		return floorToMenu;
	}
	public void setFloorToMenu(HtmlSelectOneMenu floorToMenu) {
		this.floorToMenu = floorToMenu;
	}
	public HtmlInputHidden getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(HtmlInputHidden propertyId) {
		this.propertyId = propertyId;
	}
	public List<SelectItem> getUnitFromList() {
		if(getFacesContext().getViewRoot().getAttributes().containsKey("UNIT_LIST_IN_VIEWROOT")
				&&getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT")!=null){
			unitFromList=(List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT");
		}
		return unitFromList;
	}
	public void setUnitFromList(List<SelectItem> unitFromList) {
		this.unitFromList = unitFromList;
	}
	public List<SelectItem> getUnitToList() {
		if(getFacesContext().getViewRoot().getAttributes().containsKey("UNIT_LIST_IN_VIEWROOT")
				&&getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT")!=null){
			unitToList=(List<SelectItem>)getFacesContext().getViewRoot().getAttributes().get("UNIT_LIST_IN_VIEWROOT");
		}
		return unitToList;
	}
	public void setUnitToList(List<SelectItem> unitToList) {
		this.unitToList = unitToList;
	}
	public List<SelectItem> getInvestmentTypeList() {
		return investmentTypeList;
	}
	public void setInvestmentTypeList(List<SelectItem> investmentTypeList) {
		this.investmentTypeList = investmentTypeList;
	}
	public List<SelectItem> getUnitSideTypeList() {
		return unitSideTypeList;
	}
	public void setUnitSideTypeList(List<SelectItem> unitSideTypeList) {
		this.unitSideTypeList = unitSideTypeList;
	}
	public List<SelectItem> getUnitTypeList() {
		return unitTypeList;
	}
	public void setUnitTypeList(List<SelectItem> unitTypeList) {
		this.unitTypeList = unitTypeList;
	}


	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
}
