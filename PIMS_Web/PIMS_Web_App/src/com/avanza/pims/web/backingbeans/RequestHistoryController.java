package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.NotesVO;

public class RequestHistoryController extends AbstractController{

	private static Logger logger = Logger.getLogger(RequestHistoryController.class);
	static Map viewRootMap=FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	static Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();;
	private HtmlDataTable requestHistoryDataTable;
	private List<NotesBean> requestTasksViewDataList; 
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private NotesVO notesDataItem = new NotesVO();

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

     @SuppressWarnings( "unchecked" )
	 public List<NotesBean> getAllRequestTasksForRequest(String notesOwner,String requestId) throws PimsBusinessException,Exception
	    {
			NotesServiceAgent nsa=new NotesServiceAgent();
			if(viewRootMap.containsKey(WebConstants.LIST_REQUEST_HISTORY))
				viewRootMap.remove(WebConstants.LIST_REQUEST_HISTORY);
			if(viewRootMap.containsKey("recordSize"))
			    viewRootMap.remove("recordSize");
			List<NotesBean> requestTasksViewList = new ArrayList<NotesBean>(0);
	        if(requestId!=null && requestId.trim().length()>0)
	        {
		        requestTasksViewList =new NotesController().getSystemNotesListForRequest(notesOwner,requestId.toString());
		        viewRootMap.put(WebConstants.LIST_REQUEST_HISTORY, requestTasksViewList);
		        int recordSize = requestTasksViewList.size();
		        viewRootMap.put("recordSize", recordSize);
	        }
	    	return requestTasksViewList;
	    }
	 @Override
		public void prerender() {
			super.prerender();
		}
	 @Override
		public void init() 
	   {
			super.init();
			if( !isPostBack())
			{
				viewRootMap.remove(WebConstants.LIST_REQUEST_HISTORY);
			}
		}
	


	 public HtmlDataTable getRequestHistoryDataTable() {
		return requestHistoryDataTable;
	}
	public void setRequestHistoryDataTable(HtmlDataTable requestHistoryDataTable) {
		this.requestHistoryDataTable = requestHistoryDataTable;
	}
	public List<NotesBean> getRequestTasksViewDataList() {
		
		if(viewRootMap.containsKey(WebConstants.LIST_REQUEST_HISTORY))
			requestTasksViewDataList=(ArrayList<NotesBean>) viewRootMap.get(WebConstants.LIST_REQUEST_HISTORY);
		
		return requestTasksViewDataList;
	}
	public void setRequestTasksViewDataList(
			List<NotesBean> requestTasksViewDataList) {
		this.requestTasksViewDataList = requestTasksViewDataList;
	}
	public NotesVO getRequestTasksDataItem() {
		return notesDataItem;
	}
	public void setRequestTasksDataItem(NotesVO requestTasksDataItem) {
		this.notesDataItem = requestTasksDataItem;
	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return localeInfo.getDateFormat();
		
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(viewRootMap.containsKey("recordSize"))
		recordSize=Integer.parseInt(viewRootMap.get("recordSize").toString() );
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	
	
}
