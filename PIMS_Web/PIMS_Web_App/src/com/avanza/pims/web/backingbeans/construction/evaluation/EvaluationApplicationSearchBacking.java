/**
 * 
 */
package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PropertyEvaluationCriteria;

import com.avanza.pims.report.criteria.UnitEvaluationCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;
import com.avanza.pims.ws.vo.EvaluationView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;

/**
 * @author hammad.ahmed
 *
 */
public class EvaluationApplicationSearchBacking extends	AbstractController {
	
	private transient Logger logger = Logger.getLogger(EvaluationApplicationSearchBacking.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	
	private Map<String,Long> requestStatusMap = new HashMap<String,Long>();
	
	private EvaluationApplicationFilterView searchFilterView = new EvaluationApplicationFilterView();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	
	private EvaluationApplicationFilterView dataItem = new EvaluationApplicationFilterView();
	private List<EvaluationApplicationFilterView> dataList = new ArrayList<EvaluationApplicationFilterView>();
	
	private HtmlSelectOneMenu applicationStatusCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyTypeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyOwnershipTypeCombo = new HtmlSelectOneMenu();
	
	private List<SelectItem> requestStatuses = new ArrayList<SelectItem>();
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private HtmlDataTable dataTable;
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	/*
	 * Methods
	 */
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		return CommonUtil.getLoggedInUser();
	}
	
	@SuppressWarnings("unchecked")
	public String onEvaluate(){
		logger.logInfo("onEvaluate() started...");
		String taskType = "";
		try{
			/*EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_EVALUATE);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);*/
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
				
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		logger.logInfo("onEvaluate() completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("onEvaluate() crashed ", exception);
		}	
		return "evaluationApplicationDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onSiteVisit(){
		logger.logInfo("onSiteVisit() started...");
		String taskType = "";
		try{
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
				
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		logger.logInfo("onSiteVisit() completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("onSiteVisit() crashed ", exception);
		}	
		return "evaluationApplicationDetails";
	}
	
	public String onEdit(){
		String METHOD_NAME = "onEdit()";
		String requestTypeId = null;
		String taskType = "";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_DRAFT);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "evaluationApplicationDetails";
	}

	public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }
	
/*	@SuppressWarnings("unchecked")
	public String onSiteVisit(){
		String METHOD_NAME = "onSiteVisit()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_SITE_VISIT);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "evaluationApplicationDetails";
	}*/
	
	@SuppressWarnings("unchecked")
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_VIEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "evaluationApplicationDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onDelete(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		Boolean success = requestServiceAgent.deleteRequest(requestId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_DELETE_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   	    	dataList = (List<EvaluationApplicationFilterView>) viewMap.get("requestList");
	   	    	EvaluationApplicationFilterView temp = dataList.remove(index);
	   	    	viewMap.put("requestList",dataList);
	   	    	viewMap.put("recordSize",dataList.size());
	   		}
	   		else
	   			errorMessages.add(getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_DELETE_FAILURE));
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String onCancel(){
		String METHOD_NAME = "onCancel()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		Boolean success = requestServiceAgent.cancelRequest(requestId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_CANCEL_SUCCESS);
	   			loadRequestList();
	   		}
	   		else
	   			errorMessages.add(getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_CANCEL_FAILURE));
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "";
	}
	
	public boolean isPropertyUnitEvaluationReportViewable(){
		
		boolean isPropertyUnitEvaluationReportViewable = false;
		EvaluationApplicationFilterView evaluationApplicationFilterView = (EvaluationApplicationFilterView) dataTable.getRowData();
		Long requestId = evaluationApplicationFilterView.getRequestId();
		
		RequestView requestView = null;
		try {
			requestView = requestServiceAgent.getRequestById(requestId);

			DomainDataView ddRequestCompleted = new DomainDataView();
			if(viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE) != null)
			ddRequestCompleted = (DomainDataView) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
			
			
			if (requestView.getStatusId().equals(ddRequestCompleted.getDomainDataId())) {
				isPropertyUnitEvaluationReportViewable=true;
			}
			
		
		} catch (PimsBusinessException pbe) {
			logger.LogException("Error isPropertyUnitEvaluationReportViewable()", pbe);
		}
		
		return isPropertyUnitEvaluationReportViewable;
	}
	
	@SuppressWarnings( "unchecked" )
	public String viewPropertyUnitEvaluationReport() 
	{
		
		EvaluationApplicationFilterView evaluationApplicationFilterView = (EvaluationApplicationFilterView) dataTable.getRowData();
		Long requestId = evaluationApplicationFilterView.getRequestId();
		
		RequestView requestView = null;
		try {
			requestView = requestServiceAgent.getRequestById(requestId);
			
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			
			if (requestView.getPropertyId()!=null) {
				
				PropertyEvaluationCriteria propertyReportCriteria = null;
				propertyReportCriteria = new PropertyEvaluationCriteria(ReportConstant.Report.PROPERTY_EVALUATION_REPORT_AR, ReportConstant.Processor.PROPERTY_EVALUATION_REPORT, CommonUtil.getLoggedInUser());
				propertyReportCriteria.setRequestId(requestView.getRequestId());
				
				EvaluationView evaluationView = constructionServiceAgent.getEvaluationResults(requestView.getRequestId());
				
				if (evaluationView.getPropertyValue()!=null) { // whole property
					propertyReportCriteria.setWholeProperty(true);
				} else { // by unitType
					propertyReportCriteria.setWholeProperty(false);
				}
				
				
				request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertyReportCriteria);
			}
			else if (requestView.getUnitId()!=null) {
				UnitEvaluationCriteria unitReportCriteria = null;
				unitReportCriteria = new UnitEvaluationCriteria(ReportConstant.Report.UNIT_EVALUATION_REPORT_AR, ReportConstant.Processor.UNIT_EVALUATION_REPORT, CommonUtil.getLoggedInUser());
				unitReportCriteria.setRequestId(requestView.getRequestId());
				request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, unitReportCriteria);
			}
			
			
			
		} catch (PimsBusinessException pbe) {
			logger.LogException("Error viewPropertyUnitEvaluationReport()", pbe);
		}
		 
    	
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	
	private void loadRequestList() throws PimsBusinessException{
		logger.logInfo("loadRequestList() started...");
    	try{
			dataList = new ArrayList<EvaluationApplicationFilterView>();
			Map argMap = getArgMap();
			//TODO: I was here
			List<EvaluationApplicationFilterView> requestViewList = requestServiceAgent.getEvaluationApplicationRequests(searchFilterView, argMap);
			viewMap.put("requestList", requestViewList);
			recordSize = requestViewList.size();
			viewMap.put("recordSize", recordSize);
			if(requestViewList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadRequestList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadRequestList() crashed ",exception);
			throw exception;
		}
		catch(Exception exception){
			logger.LogException("loadRequestList() crashed ",exception);
		}
	}
	
	public Boolean validateFields() {
		if(StringHelper.isNotEmpty(searchFilterView.getRequestNumber()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getApplicationStatusId()) && !searchFilterView.getApplicationStatusId().equals("0"))
			return true;
		if(searchFilterView.getRequestDateFrom()!=null)
			return true;
		if(searchFilterView.getRequestDateTo()!=null)
			return true;			
		if(StringHelper.isNotEmpty(searchFilterView.getPropertyName()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getPropertyTypeId()) && !searchFilterView.getPropertyTypeId().equals("0"))
			return true;		
		if(StringHelper.isNotEmpty(searchFilterView.getPropertyOwnershipTypeId()) && !searchFilterView.getPropertyOwnershipTypeId().equals("0"))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getLandNumber()))
			return true;
		return false;
	}
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				PropertyServiceAgent psa = new PropertyServiceAgent();

				DomainDataView ddRequestCompleted = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
				if(ddRequestCompleted != null)
					viewMap.put(WebConstants.REQUEST_STATUS_COMPLETE, ddRequestCompleted);

			}
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}
	
	public String onAddRequest()
    {
    	logger.logInfo("onAddRequest() started...");
    	try{
    		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_NEW);
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
    		logger.logInfo("onAddRequest() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddRequest() crashed ",exception);
    	}
        return "evaluationApplicationDetails";
    }
	
    public String searchRequests()
    {
    	logger.logInfo("searchRequests() started...");
    	try{
    		errorMessages = new ArrayList<String>();
    		if(validateFields())
    			loadRequestList();
    		else
    			errorMessages.add(getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
    			
    		logger.logInfo("searchRequests() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
        return "searchRequests";
    }
	
    
    public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		        logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }
    
    	
	/*
	 * Setters / Getters
	 */
    

	/**
	 * @return the dataItem
	 */
	public EvaluationApplicationFilterView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(EvaluationApplicationFilterView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<EvaluationApplicationFilterView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<EvaluationApplicationFilterView>)viewMap.get("requestList");
		if(dataList==null)
			dataList = new ArrayList<EvaluationApplicationFilterView>();
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<EvaluationApplicationFilterView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	/**
	 * @return the requestStatuses
	 */
	public List<SelectItem> getRequestStatuses() {
		return requestStatuses;
	}

	/**
	 * @param requestStatuses the requestStatuses to set
	 */
	public void setRequestStatuses(List<SelectItem> requestStatuses) {
		this.requestStatuses = requestStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the requestStatusMap
	 */
	public Map<String, Long> getRequestStatusMap() {
		return requestStatusMap;
	}

	/**
	 * @param requestStatusMap the requestStatusMap to set
	 */
	public void setRequestStatusMap(Map<String, Long> requestStatusMap) {
		this.requestStatusMap = requestStatusMap;
	}


	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public EvaluationApplicationFilterView getSearchFilterView() {
		return searchFilterView;
	}


	public void setSearchFilterView(EvaluationApplicationFilterView searchFilterView) {
		this.searchFilterView = searchFilterView;
	}


	public HtmlSelectOneMenu getApplicationStatusCombo() {
		return applicationStatusCombo;
	}


	public void setApplicationStatusCombo(HtmlSelectOneMenu applicationStatusCombo) {
		this.applicationStatusCombo = applicationStatusCombo;
	}

	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }

	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}

	public HtmlSelectOneMenu getPropertyTypeCombo() {
		return propertyTypeCombo;
	}

	public void setPropertyTypeCombo(HtmlSelectOneMenu propertyTypeCombo) {
		this.propertyTypeCombo = propertyTypeCombo;
	}

	public HtmlSelectOneMenu getPropertyOwnershipTypeCombo() {
		return propertyOwnershipTypeCombo;
	}

	public void setPropertyOwnershipTypeCombo(
			HtmlSelectOneMenu propertyOwnershipTypeCombo) {
		this.propertyOwnershipTypeCombo = propertyOwnershipTypeCombo;
	}
	
}
