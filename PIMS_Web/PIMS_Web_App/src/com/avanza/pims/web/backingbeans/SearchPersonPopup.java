package com.avanza.pims.web.backingbeans;

import java.util.*;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.*;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.PersonView;
public class SearchPersonPopup extends AbstractController
{
	
	private HtmlDataTable dataTable;
	private String firstName  ;
	private String lastName ;
	private Long personTypeId;
	private Long nationalityId;
	private String passportNumber;
	private String residenseVisaNumber;
	private String personalSecCardNo;
	private String drivingLicenseNumber;
	private String socialSecNumber;
	private String cellNumber;
	private String designation;
	//private Calendar occupiedTillDate = new Calendar();
	private PersonView personView = new PersonView();
	String controlName;
	String controlForId;
	String requestForPersonType;
	String displayStylePersonTypeCombo;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    List<SelectItem> personTypeList =new ArrayList<SelectItem>();
	public String selectManyPersonType;
	
	

	public String getSelectManyPersonType() {
		return selectManyPersonType;
	}

	public void setSelectManyPersonType(String selectManyPersonType) {
		this.selectManyPersonType = selectManyPersonType;
	}
	public String editDataItem() {
		dataItem = (PersonView) dataTable.getRowData();
		if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.Person.SEARCH_OWNER)!=null){

			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Person.PERSON_OWNER_VIEW,dataItem);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Person.SEARCH_OWNER,null);
		}
		else if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.Person.SEARCH_REPRESENTATIVE)!=null){
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Person.PERSON_REPRESENTATIVE_VIEW,dataItem);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Person.SEARCH_REPRESENTATIVE,null);
		}
		
        
	    final String viewId = "/SearchPersonPopup.jsf"; 
	    
	    FacesContext facesContext = FacesContext.getCurrentInstance();

	    // This is the proper way to get the view's url
	    ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	    String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	    
	    String javaScriptText = "javascript:closeWindow();";
	    
	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	    return "success";

	}

	@Override 
	 public void init() 
     {
    	
    	 super.init();
    	 try
    	 {
    		 
    		 if(!isPostBack())
    		 {
    			   
    			   HttpServletRequest request =
        			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        		 
        		 if(request.getParameter("persontype")!=null)
        		 {
        			 requestForPersonType=request.getParameter("persontype").toString();
        			 
        			 if(request.getParameter("persontype")!=null)
      			   {
          			this.requestForPersonType=request.getParameter("persontype").toString();
          			
          			PropertyServiceAgent psa = new PropertyServiceAgent();
          			
          			DomainDataView ddv = null;
          			try {
          				
  	        			if (requestForPersonType.equals("OWNER"))
  	        			{
  	        				
  								ddv = psa.getDomainDataByValue(WebConstants.PERSON_TYPE,WebConstants.PERSON_TYPE_OWNER );
  							
  	        			}
  	        			else if (requestForPersonType.equals("REPRESENTATIVE")) 
  	        			{
  	        				ddv = psa.getDomainDataByValue(WebConstants.PERSON_TYPE,WebConstants.PERSON_TYPE_REPRESENTATIVE );
  	        			}
  	    			   
  	        			if (ddv != null && ddv.getDomainDataId() != null)
  	        			{
  	        				personTypeId = ddv.getDomainDataId();
  	        			}
  	        			loadDataList();
          			} catch (PimsBusinessException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					}
      			   }
        			 
        			//Load For Combos;
        			
        			 loadPersonTypeCombo();
        		 }
        		 
        		 if(request.getParameter("displaycontrolname")!=null)
        			controlName=request.getParameter("displaycontrolname").toString();
        		 if(request.getParameter("hdncontrolname")!=null)
         			controlForId=request.getParameter("hdncontrolname").toString();
        		 
    		 }
    		 
    	 }
    	 catch(Exception es)
    	 {
    		 
    		 
    	 }
	        
	 }
	 
	 private void loadPersonTypeCombo() 
	 {
		 String methodName="loadPersonTypeCombo";
		 PropertyServiceAgent psa=new PropertyServiceAgent();
		 try
		 {
			 if(!sessionMap.containsKey(WebConstants.PERSON_TYPE) )
			 {
				 
				 List<DomainDataView> domainDataList=psa.getDomainDataByDomainTypeName(WebConstants.PERSON_TYPE);
				 sessionMap.put(WebConstants.PERSON_TYPE, domainDataList);
				 
			 }
		 }
		 catch(Exception ex)
		 {
			 System.out.println("Error Occured:"+ex);
			 
		 }
		 
	 }
	 
		//We have to look for the list population for facilities
	private List<String> errorMessages;
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private PersonView dataItem = new PersonView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<PersonView> dataList = new ArrayList<PersonView>();
  
	//	public Unit dataItem = new Unit();
	public String btnAdd_Click()
	{
		
		setRequestParam("PERSON_TYPE", WebConstants.INQUIRY);
		return "addPerson";
		
	}
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
	public List<PersonView> getPropertyInquiryDataList()
	{
		
		if (dataList == null || dataList.size()==0)
		{
			dataList= loadDataList(); 
		}
		return dataList;
	}

	
	
	public String doBid() {
//		errorMessages = new ArrayList<String>();
/*		if (getCustomerId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
			
*/ 
		return "";
		}
	
	public String doSearch (){

        

        //InquiryManager inquiryManager = new InquiryManager(); 

                    System.out.println("doSearch ");

                    try {
                    	requestForPersonType=requestForPersonType;
                    
                            if (firstName !=null && !firstName.equals("")){
                            
                            	personView.setFirstName(firstName.trim());                           	
                             	
                            }
                            if (lastName !=null && !lastName.equals("")){
                            	
                            	personView.setLastName(lastName.trim());
                            }
                    
	                        if(personalSecCardNo!=null && !personalSecCardNo.equals(""))
	                        {
	                        	personView.setPersonalSecCardNo(personalSecCardNo);
	                        	
	                        }
	                        
	                        if(passportNumber!=null && !passportNumber.equals(""))
	                        {
	                        	personView.setPassportNumber(passportNumber);
	                        	
	                        }
	                        if(residenseVisaNumber!=null && !residenseVisaNumber.equals(""))
	                        {
	                        	personView.setResidenseVisaNumber(residenseVisaNumber);
	                        	
	                        }
	                        if(drivingLicenseNumber!=null && !drivingLicenseNumber.equals(""))
	                        {
	                        	personView.setDrivingLicenseNumber(drivingLicenseNumber);
	                        	
	                        }
	                        if(socialSecNumber!=null && !socialSecNumber.equals(""))
	                        {
	                        	personView.setSocialSecNumber(socialSecNumber);
	                        	
	                        }
		                    dataList.clear();
		
		                    loadDataList();
	
	                    }

                    catch (Exception e){

                    System.out.println("Exception doSearch"+e);       

                    }

                return "";

     }

	
	public List<PersonView> loadDataList() 
	{
		 String methodName="loadDataList";
		 List<PersonView> list = new ArrayList();
		
		try
		{
			PropertyServiceAgent myPort = new PropertyServiceAgent();
	           //System.out.println("calling " + myPort.getEndpoint());
	            
			personView.setPersonTypeId(personTypeId);
			
	           list =  myPort.getPersonInformation(personView);
	         
	           
               dataList =list;
	    
	    }
		catch (Exception ex) 
		{
	        ex.printStackTrace();
	    }
		return list;
		
	}
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) ||
		(errorMessages.size() == 0)) {
		messageList = "";
		} else {
		messageList = "<FONT COLOR=RED><B><UL>\n";
		for(String message: errorMessages) {
		messageList = messageList + "<LI>" + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
		}



	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Long getPersonTypeId() {
		return personTypeId;
	}



	public void setPersonTypeId(Long personTypeId) {
		this.personTypeId = personTypeId;
	}



	public Long getNationalityId() {
		return nationalityId;
	}



	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}



	public String getPassportNumber() {
		return passportNumber;
	}



	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}



	public String getResidenseVisaNumber() {
		return residenseVisaNumber;
	}



	public void setResidenseVisaNumber(String residenseVisaNumber) {
		this.residenseVisaNumber = residenseVisaNumber;
	}



	public String getPersonalSecCardNo() {
		return personalSecCardNo;
	}



	public void setPersonalSecCardNo(String personalSecCardNo) {
		this.personalSecCardNo = personalSecCardNo;
	}



	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}



	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}



	public String getSocialSecNumber() {
		return socialSecNumber;
	}



	public void setSocialSecNumber(String socialSecNumber) {
		this.socialSecNumber = socialSecNumber;
	}



	public PersonView getPersonView() {
		return personView;
	}



	public void setPersonView(PersonView personView) {
		this.personView = personView;
	}



	public PersonView getDataItem() {
		return dataItem;
	}



	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}



	public List<PersonView> getDataList() {
		return dataList;
	}



	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlForId() {
		return controlForId;
	}
	public void setControlForId(String controlForId) {
		this.controlForId = controlForId;
	}
	public String getRequestForPersonType() {
		return requestForPersonType;
	}
	public void setRequestForPersonType(String requestForPersonType) {
		this.requestForPersonType = requestForPersonType;
	}
	public String getDisplayStylePersonTypeCombo() {
		if(requestForPersonType!=null && !requestForPersonType.equals(""))
			displayStylePersonTypeCombo="display:inline";
		else
			displayStylePersonTypeCombo="display:none";
		return displayStylePersonTypeCombo;
	}
	public void setDisplayStylePersonTypeCombo(String displayStylePersonTypeCombo) {
		this.displayStylePersonTypeCombo = displayStylePersonTypeCombo;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		isEnglishLocale =  view.getLocale().toString().equals("en");
		return isEnglishLocale;
	}
	
	public List<SelectItem> getPersonTypeList() 
	{
		personTypeList.clear();
		if(!sessionMap.containsKey(WebConstants.PERSON_TYPE))
			loadPersonTypeCombo();
		else
		{
			List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
			for(int i=0;i<domainDataList.size();i++)
			{
				SelectItem item =null;
				DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
				System.out.println(domainDataView.getDataValue());
				if(domainDataView.getDataValue().equals( requestForPersonType)   )
				{
					if(getIsEnglishLocale())  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn() );
					else  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr() );
					  personTypeList.add(item);
				}
				
				
			}
		}
		return personTypeList;
	}

	
	


	

	
	
}
