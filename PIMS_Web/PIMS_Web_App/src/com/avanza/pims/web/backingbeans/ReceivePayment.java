package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.CurrencyView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.ui.util.ResourceUtil;

public class ReceivePayment extends AbstractController


{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5160927008167503230L;
	UtilityServiceAgent objUtilityServiceAgent = new UtilityServiceAgent();
	PaymentReceiptView objPaymentReceiptView = new PaymentReceiptView();
	PaymentReceiptDetailView objPaymentReceiptDetailView = new PaymentReceiptDetailView();
	PaymentReceiptDetailView dataItem = new PaymentReceiptDetailView();
	
	private HtmlInputText chequeNo = new HtmlInputText();
	private HtmlInputHidden chequeNoHidden = new HtmlInputHidden();
	private HtmlInputHidden accountNoHidden = new HtmlInputHidden();
	private HtmlInputHidden transactionAmountHidden = new HtmlInputHidden();
	private HtmlInputHidden chequeDateHidden = new HtmlInputHidden();
	private HtmlInputHidden bankIdHidden = new HtmlInputHidden();
	private HtmlInputHidden cashAmountHidden = new HtmlInputHidden();
	SystemParameters parameters = SystemParameters.getInstance();
	  DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));

	private HtmlSelectOneMenu banksMenu = new HtmlSelectOneMenu();
	private String selectOneBank ;
	private String BANK_VIEW_LIST="bankViewList";
	private Double chequeAmountReceived;
	
	private HtmlInputText chequeDate =new HtmlInputText();
	private HtmlInputText transactionAmount = new HtmlInputText();
	private HtmlInputText cashAmount = new HtmlInputText();
	private HtmlInputText totalAmount = new HtmlInputText();
	private HtmlInputText totalAmountReturned = new HtmlInputText();
	private HtmlInputText receiptAmount  = new HtmlInputText();
	private HtmlSelectOneMenu paymentInstrument = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu currencyMenu= new HtmlSelectOneMenu();
	private List<SelectItem> banks = new ArrayList<SelectItem>();
	private List<SelectItem> paymentMode = new ArrayList<SelectItem>();
	private List<SelectItem> currencies = new ArrayList<SelectItem>();
	private HtmlInputText transactionalComment = new HtmlInputText();
	private HtmlInputText exchangeRate = new HtmlInputText();
	private HtmlInputText accountNo = new HtmlInputText();


	private HtmlInputHidden editMode = new HtmlInputHidden();
	private HtmlInputText totalCashAmount = new HtmlInputText();
	private HtmlInputText totalChequeAmount = new HtmlInputText();
	private HtmlInputText totalReceiptAmount = new HtmlInputText();
	private HtmlInputText totalPaidBackAmount = new HtmlInputText();

	
//Used in Tabs
	private int index;
	private  int CASH_INDEX = 0;
	private int CHEQUE_INDEX = 1; 
///End of variables used in tabs

	private HtmlCommandButton buttonSave = new HtmlCommandButton();
	
	private HtmlCommandButton buttonOk = new HtmlCommandButton();
	private ResourceBundle bundle;
	private HtmlCommandButton buttonCancel = new HtmlCommandButton();
	private Double strTotalCashAmount = 0.00;
	private Double strTotalChequeAmvount= 0.00;
	private Double strTotalPaidBackAmount= 0.00;
	private Double strTotalPaidAmount= 0.00;
	private HtmlInputHidden add = new HtmlInputHidden();
	private List<PaymentReceiptDetailView> dataList = new ArrayList<PaymentReceiptDetailView>();
	private List<PaymentReceiptDetailView> dataList2 = new ArrayList<PaymentReceiptDetailView>();
	private HtmlDataTable dataTable;	
	
	private HtmlInputHidden hdnParentVarName = new HtmlInputHidden();
	private transient Logger logger = Logger.getLogger(ReceivePayment.class);
	
	//added for richfaces calendar
	private Date chequeDateDt;
	//added for richfaces calendar
	
	//For Multiple Paymments
	private HtmlDataTable dataTablePaymentSchedule;
	private List<PaymentScheduleView> paymentSchedules = new ArrayList<PaymentScheduleView>();
	////////////////////////////
	
	//Amounts ///////////////////
	private Double totalCashAmountToBePaid;
	private double totalBankTransferAmountToBePaid;
	private double totalChqAmountToBePaid;
	private double totalCashAmountPaid;
	private double totalChqAmountPaid;
	/////////////////////////////
	
	private List<String> errorMessages = new ArrayList<String>();
	
	public String chqType;
	
	private String todayDateString;
	
	private String dateFormatForDataTable;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private Boolean chequeTabRendered ;
	
	private boolean paymentPossible = false;
	
	// Fields for BankTransfer
	private HtmlInputText bankName = new HtmlInputText();
	private String accountNumber = new String();
	private Date transferDate;
	private String transferNumber= new String();
	private String transferAmount = new String();
	
	private HtmlInputText txtAccountNumber = new HtmlInputText();
	private HtmlCalendar clndrTransferDate = new HtmlCalendar();
	private HtmlInputText txtTransferNumber= new HtmlInputText();
	private HtmlInputText  txtTransferAmount = new HtmlInputText();
	private HtmlTab bankTransferTab = new HtmlTab();
	private HtmlCommandButton buttonSaveBankTransfer = new HtmlCommandButton();
	private List<PaymentReceiptDetailView> bankTransferPaymentList = new ArrayList<PaymentReceiptDetailView>();
	private HtmlDataTable bankTransferDataTable;
	private HtmlTab chequeTab = new HtmlTab();
	private HtmlTab cashTab = new HtmlTab();
	private HtmlTabPanel paymentsTabPanel = new HtmlTabPanel();
	// BankTransfer Fields End
	
	
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	Map viewRootMap=context.getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	Map session = getFacesContext().getExternalContext().getSessionMap();
	public String getDateFormatForDataTable() {
		return dateFormatForDataTable;
	}
	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}
	public void tabChanged(ValueChangeEvent vce){
		if(vce.getNewValue().toString().equals("Cheque")){
			chequeAct();
		}
		else if(vce.getNewValue().toString().equals("Cash")){
			cashAct();
		}
		
		
		
	}
	
	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings( "unchecked" )
	public void init(){
		
		super.init();
		try
		{
			Map viewMap = viewRootMap;
			viewMap.put("canAddAttachment", true);
			if((List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID)!=null &&
					((List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID)).size()>0)
				dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
				
			

			session.put(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY, false);
		
			if(!isPostBack())
			{
				setChequeTabRendered(false);
				bankTransferTab.setRendered(false);
				buttonSaveBankTransfer.setRendered(false);
				
				DomainDataView ddvCheques = CommonUtil.getIdFromType(
						                                          CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
				                                                  WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE
				                                                  );
				viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE,ddvCheques);
				DomainDataView ddvCash = CommonUtil.getIdFromType(
						                                          CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
				                                                  WebConstants.PAYMENT_SCHEDULE_MODE_CASH
				                                                  );
				viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CASH,ddvCash);
				
				DomainDataView ddvBankTransfer = CommonUtil.getIdFromType(
											                        CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
											                        WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER
											                        );
				viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER,ddvBankTransfer);	
				
				
				paymentSchedules = (List<PaymentScheduleView>)session.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
				List<PaymentScheduleView> psvNew= new ArrayList<PaymentScheduleView>(0);
				for (PaymentScheduleView psv : paymentSchedules ) 
				{
					if(psv.getPaymentModeId()!=null&&!psv.getPaymentModeId().toString().equals(this.getPaymentModeCashId()))
						psv.setSelected(false);
					psvNew.add(psv);
					if(psv.getPaymentModeId().toString().equals(this.getPaymentModeChequeId())) {
						setChequeTabRendered(true);
						
					}	
					if(psv.getPaymentModeId().toString().equals(this.getPaymentModeBankTransferId()))
						bankTransferTab.setRendered(true);
				}
				
				loadAttachmentsAndComments();
				
				Collections.sort(psvNew, new ListComparator("paymentDueOnString",true));
				viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, psvNew);
				session.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
				//////////////////////////////////////
				
				
				loadBanksList();
				
				
				dataList  = new ArrayList();
				dataList2 = new ArrayList();
				session.put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID, dataList);
				viewRootMap.put("RECEIVE_PAYMENT_BANK_TRANSFER_GRID",dataList2);
				////////////Added by tariq
				HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
				String parentName = "";
				if(request.getParameter("parentHidden") != null)
					parentName = request.getParameter("parentHidden").toString();
				hdnParentVarName.setValue(parentName);
				////////////Added by tariq
				
				//Following Code by Afzal ////
				
				if (!viewMap.containsKey("cashId") && !viewMap.containsKey("chqId"))
				{
					UtilityServiceAgent usa = new UtilityServiceAgent();
					try 
					{
						
						DomainDataView ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
						viewMap.put("cashId", ddv.getDomainDataId());
						
						ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
						viewMap.put("chqId", ddv.getDomainDataId());
						
						ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);
						viewMap.put("bankTransferID", ddv.getDomainDataId());
						
						
					}
					catch (PimsBusinessException e) 
					{
						// TODO Auto-generated catch block
						logger.LogException("init|Error Occured while fetching/setting payment schedule mode", e );
					}
				}
				calculateSummaray();
				todayDateString = df.format(new Date());
				//////////////////////////
			}

			
			buttonOk.setValue(ResourceUtil.getInstance().getProperty("commons.ok"));
			buttonCancel.setValue(ResourceUtil.getInstance().getProperty("commons.cancel"));
			buttonSave.setValue(ResourceUtil.getInstance().getProperty("commons.Add"));
			
			
			if(!isPostBack())
			{
				index = CASH_INDEX;
				if(!session.containsKey(WebConstants.BANKS_LIST) || 
						session.get(WebConstants.BANKS_LIST)==null){
					loadBanksList();
				}
				if(session.get(WebConstants.CURRENCY_LIST)==null){	
					loadCurrenciesList();
				}
				PaymentReceiptView paymentReceiptView;
				
				PaymentScheduleView paymentSchedule = null;

				if (paymentSchedules!=null && paymentSchedules.size()>0) {
					paymentSchedule = paymentSchedules.get(0);
				}

				PropertyService propertyService = new PropertyService();

				if (propertyService.isGrpCustomerNumberExists(paymentSchedule)){
					this.setPaymentPossible(true);
				}


				if(session.get(WebConstants.PAYMENT_RECEIPT_VIEW)!=null){
					paymentReceiptView = (PaymentReceiptView)session.get(WebConstants.PAYMENT_RECEIPT_VIEW);
					
					
										
					
					objPaymentReceiptView = paymentReceiptView;
					if(paymentReceiptView.getPaymentReceiptDetails().size()>0){
						
						Iterator it = paymentReceiptView.getPaymentReceiptDetails().iterator();

						List<PaymentReceiptDetailView> paymentReceiptDetailsViewList = new ArrayList();
						
						boolean isCheckPresent=false;

						while(it.hasNext()){
							PaymentReceiptDetailView paymentReceiptDetailView=  (PaymentReceiptDetailView)it.next();
							paymentReceiptDetailsViewList.add(paymentReceiptDetailView);
							if(paymentReceiptDetailView.getModeOfPaymentEn().equals(WebConstants.PAYMENT_METHOD_CASH_DISPLAY)){
								strTotalCashAmount =strTotalCashAmount + paymentReceiptDetailView.getAmount();
								strTotalPaidAmount = strTotalCashAmount+strTotalChequeAmvount;
								strTotalPaidBackAmount = strTotalCashAmount+strTotalChequeAmvount-objPaymentReceiptView.getAmount();
								
							}
							else{
								strTotalChequeAmvount = strTotalChequeAmvount + paymentReceiptDetailView.getAmount();
								strTotalPaidAmount = strTotalCashAmount+strTotalChequeAmvount;
								strTotalPaidBackAmount = strTotalCashAmount+strTotalChequeAmvount-objPaymentReceiptView.getAmount();
							}
							
							
						}
						totalCashAmount.setValue(strTotalCashAmount);
						totalChequeAmount.setValue(strTotalChequeAmvount);
						totalPaidBackAmount.setValue(strTotalPaidBackAmount);
						totalAmount.setValue(strTotalPaidAmount);
						for(int i = 0;i<paymentReceiptDetailsViewList.size();i++){

							dataList.add(paymentReceiptDetailsViewList.get(i));
						}
						session.put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID, dataList);

					}
					objPaymentReceiptView.setAmount(paymentReceiptView.getAmount());
					if(objPaymentReceiptView.getAmount()!=null)
						receiptAmount.setValue(objPaymentReceiptView.getAmount().toString());
					else
						receiptAmount.setValue("0.00");
				}

			}

			dateFormatForDataTable = getDateFormat();

			
    		

		}
		catch(Exception e){
			logger.LogException("init|Error Occured", e );
		}

	}
	@SuppressWarnings("unchecked")
	public void preprocess()
	{

		super.preprocess();
		try
		{
			
	         //Next line Used in Tab	
            index = CASH_INDEX;
			if((List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID)!=null &&
					((List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID)).size()>0)
				dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
			

			if(isPostBack())
			{
					banks = (List<SelectItem>)session.get(WebConstants.BANKS_LIST);
					currencies= (List<SelectItem>)session.get(WebConstants.CURRENCY_LIST);
			}

		}
		catch(Exception e)
		{
			logger.LogException("preprocess|Error Occured", e );
		}

	}
	//Methods used for Tabs
	@SuppressWarnings("unchecked")
	public void cashAct(){
		index = CASH_INDEX;
		session.put("TAB_INDEX",index);
	}
	@SuppressWarnings("unchecked")
	public void chequeAct() { 
		index = CHEQUE_INDEX;
		session.put("TAB_INDEX",index);

	}
	public String getCashStyle() { return getCSS(CASH_INDEX);  }
	public String getChequeStyle() { return getCSS(CHEQUE_INDEX);  }


	private String getCSS(int forIndex) {
		return forIndex == index ? "tabbedPaneTextSelected" : "tabbedPaneText"; 
	}
	public boolean isCashCurrent() { return index == CASH_INDEX;  }
	public boolean isChequeCurrent() { return index == CHEQUE_INDEX;  }

	@SuppressWarnings("unchecked")
	public String deleteDataItem()
	{
		dataItem =(PaymentReceiptDetailView) dataTable.getRowData();
		if(dataItem.getModeOfPaymentEn().equals(WebConstants.PAYMENT_METHOD_CASH_DISPLAY))
			totalCashAmount.setValue(String.valueOf(Double.parseDouble(getTotalCashAmount().getValue().toString()) - Double.parseDouble(dataItem.getAmount().toString())));
		if(dataItem.getModeOfPaymentEn().equals(WebConstants.PAYMENT_METHOD_CHEQUE_DISPLAY))
			totalChequeAmount.setValue(String.valueOf(Double.parseDouble(getTotalChequeAmount().getValue().toString()) - Double.parseDouble(dataItem.getAmount().toString())));
		totalAmount.setValue(String.valueOf(Double.parseDouble(getTotalCashAmount().getValue().toString())+Double.parseDouble(getTotalChequeAmount().getValue().toString())));
		if(Double.parseDouble(totalAmount.getValue().toString())>Double.parseDouble(getReceiptAmount().getValue().toString()))	
		{
			Double paidBack= Double.parseDouble(totalPaidBackAmount.getValue().toString())
			                 -Double.parseDouble(getReceiptAmount().getValue().toString());
			
		   totalPaidBackAmount.setValue(String.valueOf(paidBack));
		}
		else
			totalPaidBackAmount.setValue("0.00");
		if(dataItem.getCsPaymentScheduleNum()!=null && dataItem.getCsPaymentScheduleNum().trim().length()>0)
		   removeAddedToPaymentReceiptFromPaymentSchedules(dataItem.getCsPaymentScheduleNum());
		dataList =(ArrayList<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
		dataList.remove(dataTable.getRowIndex());
		session.put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID, dataList);

		return "deleted";   


		// Get selected MyData item to be edited.

		// Store the ID of the data item in hidden input element.

	}
	@SuppressWarnings("unchecked")
	public String deleteBankTransferDataItem()
	{
		dataItem =(PaymentReceiptDetailView) bankTransferDataTable.getRowData();

		if(dataItem.getCsPaymentScheduleNum()!=null && dataItem.getCsPaymentScheduleNum().trim().length()>0)
		   removeAddedToPaymentReceiptFromPaymentSchedules(dataItem.getCsPaymentScheduleNum());
		bankTransferPaymentList =(ArrayList<PaymentReceiptDetailView>) viewRootMap.get("RECEIVE_PAYMENT_BANK_TRANSFER_GRID");
		bankTransferPaymentList.remove(bankTransferDataTable.getRowIndex());
		clearBankTransferInputFields();
		viewRootMap.put("RECEIVE_PAYMENT_BANK_TRANSFER_GRID", bankTransferPaymentList);
		
		return "deleted";   


		// Get selected MyData item to be edited.

		// Store the ID of the data item in hidden input element.

	}
	@SuppressWarnings("unchecked")
       private void removeAddedToPaymentReceiptFromPaymentSchedules(String csPaymentNumbers)
       {
    	   
    	   String[] paymentNumber  = csPaymentNumbers.split(",");
    	   HashMap hMap =new HashMap();
    	   for (String payNum : paymentNumber) 
			hMap.put(payNum,payNum);
    	   List<PaymentScheduleView> psvList = getPaymentSchedules();
    	   List<PaymentScheduleView> newPSVList = new ArrayList<PaymentScheduleView>(0);
   		   for (PaymentScheduleView paymentScheduleView : psvList) 
   		   {
			   if(hMap.containsKey(paymentScheduleView.getPaymentNumber()))
				   paymentScheduleView.setAddedToPaymentReceiptDetail(false);
	   		   newPSVList.add(paymentScheduleView);
   		   }
   		viewRootMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, newPSVList );
       }
		String returnVal = "";
		@SuppressWarnings("unchecked")
		public String done()
		{
			logger.logInfo("done|Start");
		
					if ( isPaymentCorrect())
					{
						PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
						//Making a ReceiptDetail view for cash (if any)
						addCashReciptDetailView();
						Set<PaymentReceiptDetailView> paymentReceiptDetailViewSet = new HashSet();
						Iterator dataListIterator= dataList.iterator();
						while(dataListIterator.hasNext())
						{
							PaymentReceiptDetailView objPaymentReceiptDetailView = (PaymentReceiptDetailView)dataListIterator.next();
							paymentReceiptDetailViewSet.add(objPaymentReceiptDetailView);
				
						}
						dataListIterator = bankTransferPaymentList.iterator();
						while(dataListIterator.hasNext())
						{
							paymentReceiptDetailViewSet.add((PaymentReceiptDetailView) dataListIterator.next());
						}
						paymentReceiptView.setPaymentReceiptDetails(paymentReceiptDetailViewSet);
						paymentReceiptView.setCreatedBy(getLoggedInUser());
						paymentReceiptView.setCreatedOn(new Date());
						paymentReceiptView.setTransactionDate(new Date());
						paymentReceiptView.setUpdatedBy(getLoggedInUser());
						paymentReceiptView.setUpdatedOn(new Date());
						paymentReceiptView.setIsDeleted(new Long(0));
						paymentReceiptView.setRecordStatus(new Long(1));
						getFacesContext().getExternalContext().getRequestMap().put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
						session.put(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED, true);
						session.put(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY, true);
						session.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
						session.put(WebConstants.Attachment.ATTACHED_CHEQUE_IDS, 
								viewRootMap.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS));
						returnVal = "success";
				        FacesContext facesContext = FacesContext.getCurrentInstance();        
				        String javaScriptText = "javascript:closeWindow(";
				        String parameter = "";
				        if(hdnParentVarName != null && hdnParentVarName.getValue() != null)
				        {
				        	parameter = hdnParentVarName.getValue().toString();
				        	javaScriptText += "'" +parameter+ "'";
				        }
				        javaScriptText += ");";        
				        AddResource addResource = AddResourceFactory.getInstance(facesContext);
				        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		
		         }
					logger.logInfo("done|Finish");
		         return returnVal;
	}
		@SuppressWarnings("unchecked")
	public String reset()
	{
		session.put(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED, true);
        FacesContext facesContext = FacesContext.getCurrentInstance();        
        if(session.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
        	session.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
        this.enableCollectPayment(); 
        String javaScriptText = "window.opener.document.forms[0].submit();javascript:window.close()";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	@SuppressWarnings("unchecked")
	public List<PaymentReceiptDetailView> getPaymentTransactionList() {
		if ((dataList == null || dataList.size()==0 )&& session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID)==null && !isPostBack()) 
		{
			loadDataList(); // Preload by lazy loading.
			session.put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID, dataList);
		}

		return dataList;
	}
	@SuppressWarnings("unchecked")
	private void loadDataList() 
	{
		PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
		Iterator it = paymentReceiptView.getPaymentReceiptDetails().iterator();
		List<PaymentReceiptDetailView> paymentReceiptDetailsViewList = new ArrayList();
		while(it.hasNext())
		{
			PaymentReceiptDetailView paymentReceiptDetailView=  (PaymentReceiptDetailView)it.next();
			paymentReceiptDetailsViewList.add(paymentReceiptDetailView);
		}
		for(int i = 0;i<paymentReceiptDetailsViewList.size();i++)
		{
			dataList.add(paymentReceiptDetailsViewList.get(i));
		}

	}
	@SuppressWarnings("unchecked")
	private void loadCurrenciesList()
	{
		try
		{
			List<CurrencyView> currencyViewList = new ArrayList<CurrencyView>();
			currencies = new ArrayList();
			currencyViewList = objUtilityServiceAgent.getCurrenciesList();
			for(int index=0;index<currencyViewList.size();index++){
				currencies.add(new SelectItem(currencyViewList.get(index).getCurrencyId(),currencyViewList.get(index).getCurrencyNameEn()));
			}
			session.put(WebConstants.CURRENCY_LIST, currencies);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private void loadBanksList(){
		try{
			List<BankView> bankViewList = new ArrayList();
			banks = new ArrayList();
			bankViewList = objUtilityServiceAgent.getBanksList();
			for(int index=0;index<bankViewList.size();index++){
				banks.add(new SelectItem(bankViewList.get(index).getBankId().toString(),bankViewList.get(index).getBankEn()));
			}
			session.put(WebConstants.BANKS_LIST, banks);
			viewRootMap.put(BANK_VIEW_LIST,bankViewList );
		}
		catch(Exception e){
			logger.LogException("loadBankList|Error Occured", e );
		}
	}
	@SuppressWarnings("unchecked")
  private BankView getBanksViewFromBankId(Long id)
  {
	  
	  List<BankView> bankViewList  = (ArrayList<BankView>)viewRootMap.get(BANK_VIEW_LIST);
	  for (BankView bankView : bankViewList) {
		  if(bankView.getBankId().compareTo(id)==0)
		return bankView ;
	   }
	  return null;
  }
	@SuppressWarnings("unchecked")
  public void loadAttachmentsAndComments(){
	
		if (getChequeTabRendered()) { // payment has checks
			viewRootMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_RECEIVE_PAYMENTS);
		}
		
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
  	    String externalId = WebConstants.Attachment.EXTERNAL_ID_PAYMENT_RECEIPTS;
  	    viewRootMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewRootMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		
	}
	@SuppressWarnings("unchecked")
	public String save()
	{
		try
		{
			add();
		}
		catch(Exception e)
		{
			logger.LogException("save|Error Occured", e );
		}


		return "Saved";
	}
    private boolean validateChequeInput() throws Exception
    {
    	String methodName = "validateChequeInput";
		errorMessages = new ArrayList<String>(0);
		boolean isValidated = true;
		logger.logInfo(methodName +"|Start");
		if(selectOneBank!=null && selectOneBank.trim().length()>0 && selectOneBank.equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReceivePaymentScreen.bank_required));
			isValidated =false;
		}
		if(accountNo.getValue() ==null ||
				( accountNo.getValue() !=null &&   accountNo.getValue().toString().trim().length()<=0 ))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReceivePaymentScreen.AccNo_required));
			isValidated =false;
		}
		if(chequeNo.getValue() ==null ||
				( chequeNo.getValue() !=null &&   chequeNo.getValue().toString().trim().length()<=0 ))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ReceivePaymentScreen.ChqNo_required));
			isValidated =false;
		}
		logger.logInfo(methodName +"|Finish");
    	return isValidated;
    }
    @SuppressWarnings("unchecked")
	public void add()
    {
		String methodName = "add";
		errorMessages = new ArrayList<String>(0);
		try{
			logger.logInfo(methodName +"|Start");
			if ( validateChequeInput() )
			{
				dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
				PaymentReceiptDetailView paymentReceiptDetailView = new PaymentReceiptDetailView();
				paymentReceiptDetailView.setAmount(0.00);
				paymentReceiptDetailView.setExchangeRate(Double.valueOf(exchangeRate.getValue().toString()));
				paymentReceiptDetailView.setModeOfPaymentEn(WebConstants.PAYMENT_METHOD_CHEQUE_DISPLAY);
				paymentReceiptDetailView.setAccountNo(accountNo.getValue().toString().trim());
				PaymentMethodView paymentMethod = new PaymentMethodView();
				paymentMethod.setDescription(WebConstants.PAYMENT_METHOD_CHEQUE_DISPLAY);
				paymentReceiptDetailView.setAmount(Double.valueOf(transactionAmount.getValue().toString()));
				paymentReceiptDetailView.setMethodRefNo(chequeNo.getValue().toString());
				paymentReceiptDetailView.setMethodRefDate(this.getChequeDateDt());
				paymentReceiptDetailView.setBankId(new Long(selectOneBank));
				paymentReceiptDetailView.setChequeType( new Long ( chqType ) );
				BankView bankView =getBanksViewFromBankId(new Long(selectOneBank));
				if(bankView !=null)
				{
					paymentReceiptDetailView.setBankEn(bankView.getBankEn());
					paymentReceiptDetailView.setBankAr(bankView.getBankAr());
					
				}
				paymentMethod.setPaymentMethodId(WebConstants.PAYMENT_METHOD_CHEQUE_ID);
				
				paymentReceiptDetailView.setPaymentMethod(paymentMethod);	
				paymentReceiptDetailView.setCreatedBy(getLoggedInUser());
				paymentReceiptDetailView.setCreatedOn(new Date());
				paymentReceiptDetailView.setUpdatedBy(getLoggedInUser());
				paymentReceiptDetailView.setUpdatedOn(new Date());
				paymentReceiptDetailView.setIsDeleted(new Long(0));
				paymentReceiptDetailView.setRecordStatus(new Long(1));
				paymentReceiptDetailView.setIsReceived("Y");
				paymentReceiptDetailView.setMethodRefNo(chequeNo.getValue().toString());
				setPaymentScheduleForCheque(paymentReceiptDetailView);
				dataList.add(paymentReceiptDetailView);
				totalCashAmount.setValue(String.valueOf(Double.parseDouble(getTotalCashAmount().getValue().toString()) + Double.parseDouble(getCashAmount().getValue().toString())));
				totalChequeAmount.setValue(String.valueOf(Double.parseDouble(getTotalChequeAmount().getValue().toString()) + Double.parseDouble(getTransactionAmount().getValue().toString())));
				totalAmount.setValue(String.valueOf(Double.parseDouble(getTotalCashAmount().getValue().toString())+Double.parseDouble(getTotalChequeAmount().getValue().toString())));
				
				if(Double.parseDouble(totalAmount.getValue().toString())> Double.parseDouble(getReceiptAmount().getValue().toString()))
				{
					Double paidBack=Double.parseDouble(getReceiptAmount().getValue().toString())-Double.parseDouble(totalAmount.getValue().toString());
				    if(paidBack<0)
					 paidBack=paidBack*-1;
				    totalPaidBackAmount.setValue(String.valueOf(paidBack));
				}
				clearChequeInputFields();
				setSelectedPaymentSchAsAddedToPaymentReceipt();
				viewRootMap.put("anyPSSelected",false);
			}
			session.put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID,dataList);	
			logger.logInfo(methodName +"|Finish");
		}
		catch(Exception e)
		{
			errorMessages.clear();
			logger.LogException(methodName +"|Error Occured",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    @SuppressWarnings("unchecked")
	private void clearChequeInputFields() 
	{
		accountNo.setValue("");
		accountNoHidden.setValue("");
		transactionAmount.setValue("0.00");
		transactionAmountHidden.setValue("0.00");
		chequeNo.setValue("");
		chequeNoHidden.setValue("");
		chequeDateHidden.setValue("");
		chequeDate.setValue("");
		this.setChequeDateDt(null);
		selectOneBank="-1";
	}
    @SuppressWarnings("unchecked")
	private void setSelectedPaymentSchAsAddedToPaymentReceipt()
	{
		List<PaymentScheduleView> psvList = getPaymentSchedules();
		List<PaymentScheduleView> newPSVList = new ArrayList<PaymentScheduleView>(0);
		
		for (PaymentScheduleView paymentScheduleView : psvList) {
			
			if(paymentScheduleView.getPaymentModeId().toString().equals(getPaymentModeChequeId())) 
			{
				 if (paymentScheduleView.getSelected())
			     {
				   paymentScheduleView.setAddedToPaymentReceiptDetail(true);
			       paymentScheduleView.setSelected(false);
			     }
			     else
			     {
				   paymentScheduleView.setDisabled(false);
			     }
			}
			else
			if(paymentScheduleView.getPaymentModeId().toString().equals(getPaymentModeBankTransferId())) 
			{
				 if (paymentScheduleView.getSelected())
			     {
				   paymentScheduleView.setAddedToPaymentReceiptDetail(true);
			       paymentScheduleView.setSelected(false);
			     }
			     else
			     {
				   paymentScheduleView.setDisabled(false);
			     }
			}	
			newPSVList.add(paymentScheduleView);
		
		}
		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,newPSVList);
		
	}
	
    @SuppressWarnings("unchecked")
	public void prerender(){
		super.prerender();
		
		if(session.get("TAB_INDEX")!=null)
		index = Integer.parseInt(session.get("TAB_INDEX").toString());
		else
			index = CASH_INDEX;
		if(add.getValue()!=null&&add.getValue().toString().equals("true"))
		{
			if(accountNoHidden.getValue()!=null){
				accountNo.setValue(accountNoHidden.getValue());

			}
			if(cashAmountHidden.getValue()!=null){
				cashAmount.setValue(cashAmountHidden.getValue());

			}
			if(chequeDateHidden.getValue()!=null){
				chequeDate.setValue(chequeDateHidden.getValue());
			}
			if(chequeNoHidden.getValue()!=null){
				chequeNo.setValue(chequeNoHidden.getValue());
			}
			if(transactionAmountHidden.getValue()!=null){
				transactionAmount.setValue(transactionAmountHidden.getValue());
			}
			
			add();
			add.setValue("false");
		}
		//if any cheque payment schedule is selected then add btn is rendered 
		if(viewRootMap.containsKey("anyPSSelected") && 
			viewRootMap.get("anyPSSelected")!=null)
		{
			Boolean anyPSSelected =(Boolean)viewRootMap.get("anyPSSelected");
			buttonSave.setRendered(anyPSSelected);
		}
		else
			buttonSave.setRendered(false);
		if(viewRootMap.containsKey("anyBTSelected") && 
				viewRootMap.get("anyBTSelected")!=null)
		{
			Boolean anyPSSelected =(Boolean)viewRootMap.get("anyBTSelected");
			buttonSaveBankTransfer.setRendered(anyPSSelected);
		}
		else
			buttonSaveBankTransfer.setRendered(false);
		
		
		if (! isPaymentPossible()) 
		{
			if (errorMessages==null) {
				errorMessages = new ArrayList<String>(0);
			}
			
			if (!errorMessages.contains(ResourceUtil.getInstance().getProperty("selectPerson.msg.GRPNumberNotPresent"))) {
				errorMessages.add(ResourceUtil.getInstance().getProperty("selectPerson.msg.GRPNumberNotPresent"));
			}	
			buttonOk.setDisabled(true);
		}
	}
	
    @SuppressWarnings("unchecked")
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    @SuppressWarnings("unchecked")    
	public String getLocale()
    {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	@SuppressWarnings("unchecked")
	public Date getChequeDateDt() {
		if(viewRootMap.containsKey("chequeDateDt") && 
				viewRootMap.get("chequeDateDt")!=null &&
				viewRootMap.get("chequeDateDt").toString().trim().length()>0)
			chequeDateDt=(Date)viewRootMap.get("chequeDateDt");
		else
			chequeDateDt=null;
		return chequeDateDt;
	}
	@SuppressWarnings("unchecked")
	public void setChequeDateDt(Date chequeDateDt) {
		this.chequeDateDt = chequeDateDt;
		if(this.chequeDateDt !=null)
		viewRootMap.put("chequeDateDt",this.chequeDateDt);
		else
			viewRootMap.put("chequeDateDt","");
	}
	///////////added for richfaces calender

	//////////added for showing record size with the paginator
	public Integer getRecordSize(){
		if (dataList != null) {
			return dataList.size();
		}
		else{
			return 0;
		}
	}
	//////////added for showing record size with the paginator
	
	
	public HtmlCommandButton getButtonSave() {
		return buttonSave;
	}
	public void setButtonSave(HtmlCommandButton buttonSave) {
		this.buttonSave = buttonSave;
	}
	public HtmlInputText getCashAmount() {
		if(totalCashAmountToBePaid!=null && totalCashAmountToBePaid >0)
		   cashAmount.setValue(totalCashAmountToBePaid);
		if(cashAmount.getValue()==null||cashAmount.getValue().toString().trim().length()==0)
		   cashAmount.setValue("0.00");
		
		return cashAmount;
	}
	public void setCashAmount(HtmlInputText cashAmount) {
		this.cashAmount = cashAmount;
	}
	public HtmlInputText getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(HtmlInputText chequeDate) {
		this.chequeDate = chequeDate;
	}
	public HtmlInputText getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(HtmlInputText chequeNo) {
		this.chequeNo = chequeNo;
	}
	public List<SelectItem> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<SelectItem> currencies) {
		this.currencies = currencies;
	}
	public List<PaymentReceiptDetailView> getDataList() {
		return dataList;
	}
	public void setDataList(List<PaymentReceiptDetailView> dataList) {
		this.dataList = dataList;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlInputHidden getEditMode() {
		return editMode;
	}
	public void setEditMode(HtmlInputHidden editMode) {
		this.editMode = editMode;
	}
	public HtmlSelectOneMenu getPaymentInstrument() {
		return paymentInstrument;
	}
	public void setPaymentInstrument(HtmlSelectOneMenu paymentInstrument) {
		this.paymentInstrument = paymentInstrument;
	}
	public List<SelectItem> getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(List<SelectItem> paymentMode) {
		this.paymentMode = paymentMode;
	}
	public HtmlInputText getReceiptAmount() {
		if(receiptAmount.getValue()==null||receiptAmount.getValue().toString().trim().length()==0)
			receiptAmount.setValue("0.00");
		return receiptAmount;

	}
	public void setReceiptAmount(HtmlInputText receiptAmount) {
		this.receiptAmount = receiptAmount;
	}


	public HtmlInputText getTotalAmount() {
		if(totalAmount.getValue()==null||totalAmount.getValue().toString().trim().length()==0){

			totalAmount.setValue("0.00");
		}
		return totalAmount;
	}
	public void setTotalAmount(HtmlInputText totalAmount) {
		this.totalAmount = totalAmount;
	}
	public HtmlInputText getTotalAmountReturned() {
		return totalAmountReturned;
	}
	public void setTotalAmountReturned(HtmlInputText totalAmountReturned) {
		this.totalAmountReturned = totalAmountReturned;
	}
	public HtmlInputText getTransactionalComment() {
		return transactionalComment;
	}
	public void setTransactionalComment(HtmlInputText transactionalComment) {
		this.transactionalComment = transactionalComment;
	}
	public HtmlInputText getTransactionAmount() {
		if(transactionAmount.getValue()==null||transactionAmount.getValue().toString().trim().length()==0){

			transactionAmount.setValue("0.00");
		}
		return transactionAmount;
	}
	public void setTransactionAmount(HtmlInputText transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public List<SelectItem> getBanks() {
		return banks;
	}
	public void setBanks(List<SelectItem> banks) {
		this.banks = banks;
	}
	public HtmlSelectOneMenu getBanksMenu() {
		return banksMenu;
	}
	public void setBanksMenu(HtmlSelectOneMenu banksMenu) {
		this.banksMenu = banksMenu;
	}
	public HtmlSelectOneMenu getCurrencyMenu() {
		return currencyMenu;
	}
	public void setCurrencyMenu(HtmlSelectOneMenu currencyMenu) {
		this.currencyMenu = currencyMenu;
	}
	public HtmlInputText getTotalCashAmount() {
		if(totalCashAmount.getValue()==null||totalCashAmount.getValue().toString().trim().length()==0){

			totalCashAmount.setValue("0.00");
		}

		return totalCashAmount;
	}
	public void setTotalCashAmount(HtmlInputText totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}
	public HtmlInputText getTotalChequeAmount() {
		if(totalChequeAmount.getValue()==null||totalChequeAmount.getValue().toString().trim().length()==0){

			totalChequeAmount.setValue("0.00");
		}

		return totalChequeAmount;
	}
	public void setTotalChequeAmount(HtmlInputText totalChequeAmount) {
		this.totalChequeAmount = totalChequeAmount;
	}
	public HtmlInputText getTotalPaidBackAmount() {
		if(totalPaidBackAmount.getValue()==null||totalPaidBackAmount.getValue().toString().trim().length()==0){

			totalPaidBackAmount.setValue("0.00");
		}

		return totalPaidBackAmount;
	}
	public void setTotalPaidBackAmount(HtmlInputText totalPaidBackAmount) {
		this.totalPaidBackAmount = totalPaidBackAmount;
	}
	public HtmlInputText getTotalReceiptAmount() {
		if(totalReceiptAmount.getValue()==null||totalReceiptAmount.getValue().toString().trim().length()==0){

			totalReceiptAmount.setValue("0.00");
		}

		return totalReceiptAmount;
	}
	public void setTotalReceiptAmount(HtmlInputText totalReceiptAmount) {
		this.totalReceiptAmount = totalReceiptAmount;
	}
	public HtmlInputText getExchangeRate() {
		if(exchangeRate.getValue()==null||exchangeRate.getValue().toString().trim().length()==0){

			exchangeRate.setValue("1.00");
		}

		return exchangeRate;
	}
	public void setExchangeRate(HtmlInputText exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public HtmlCommandButton getButtonCancel() {
		return buttonCancel;
	}

	public void setButtonCancel(HtmlCommandButton buttonCancel) {
		this.buttonCancel = buttonCancel;
	}

	public HtmlCommandButton getButtonOk() {
		return buttonOk;
	}

	public void setButtonOk(HtmlCommandButton buttonOk) {
		this.buttonOk = buttonOk;
	}

	public HtmlInputText getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(HtmlInputText accountNo) {
		this.accountNo = accountNo;
	}


	public HtmlInputHidden getAccountNoHidden() {
		return accountNoHidden;
	}
	public void setAccountNoHidden(HtmlInputHidden accountNoHidden) {
		this.accountNoHidden = accountNoHidden;
	}
	public HtmlInputHidden getBankIdHidden() {
		return bankIdHidden;
	}
	public void setBankIdHidden(HtmlInputHidden bankIdHidden) {
		this.bankIdHidden = bankIdHidden;
	}
	public HtmlInputHidden getChequeDateHidden() {
		return chequeDateHidden;
	}
	public void setChequeDateHidden(HtmlInputHidden chequeDateHidden) {
		this.chequeDateHidden = chequeDateHidden;
	}
	public HtmlInputHidden getChequeNoHidden() {
		return chequeNoHidden;
	}
	public void setChequeNoHidden(HtmlInputHidden chequeNoHidden) {
		this.chequeNoHidden = chequeNoHidden;
	}
	public HtmlInputHidden getTransactionAmountHidden() {
		return transactionAmountHidden;
	}
	public void setTransactionAmountHidden(HtmlInputHidden transactionAmountHidden) {
		this.transactionAmountHidden = transactionAmountHidden;
	}
	public HtmlInputHidden getAdd() {
		return add;
	}
	public void setAdd(HtmlInputHidden add) {
		this.add = add;
	}
	public HtmlInputHidden getCashAmountHidden() {
		return cashAmountHidden;
	}
	public void setCashAmountHidden(HtmlInputHidden cashAmountHidden) {
		this.cashAmountHidden = cashAmountHidden;
	}
	public HtmlInputHidden getHdnParentVarName() {
		return hdnParentVarName;
	}
	public void setHdnParentVarName(HtmlInputHidden hdnParentVarName) {
		this.hdnParentVarName = hdnParentVarName;
	}
	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}
	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}
	@SuppressWarnings("unchecked")
	public List<PaymentScheduleView> getPaymentSchedules() {
		
		paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
		return paymentSchedules;
	}
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	public Double getTotalCashAmountToBePaid() {
		return totalCashAmountToBePaid;
	}
	public void setTotalCashAmountToBePaid(Double totalCashAmountToBePaid) {
		this.totalCashAmountToBePaid = totalCashAmountToBePaid;
	}
	public double getTotalChqAmountToBePaid() {
		return totalChqAmountToBePaid;
	}
	public void setTotalChqAmountToBePaid(double totalChqAmountToBePaid) {
		this.totalChqAmountToBePaid = totalChqAmountToBePaid;
	}
	public double getTotalCashAmountPaid() {
		return totalCashAmountPaid;
	}
	public void setTotalCashAmountPaid(double totalCashAmountPaid) {
		this.totalCashAmountPaid = totalCashAmountPaid;
	}
	public double getTotalChqAmountPaid() {
		return totalChqAmountPaid;
	}
	public void setTotalChqAmountPaid(double totalChqAmountPaid) {
		this.totalChqAmountPaid = totalChqAmountPaid;
	}

	@SuppressWarnings("unchecked")
	public void calculateSummaray()
	{
		paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
		
		Long cashId = (Long) viewRootMap.get("cashId");
		Long chqId = (Long) viewRootMap.get("chqId");
		Long bankTransferID = (Long) viewRootMap.get("bankTransferID");
		totalCashAmountToBePaid = 0d;
		
		for (int i=0; i<paymentSchedules.size(); i++)
		{
			
			
			//Checking Mode
			if (paymentSchedules.get(i).getPaymentModeId() != null)
				{
					if (paymentSchedules.get(i).getPaymentModeId().longValue() == cashId.longValue())
					{
						totalCashAmountToBePaid = totalCashAmountToBePaid +  paymentSchedules.get(i).getAmount();
					}
					else if (paymentSchedules.get(i).getPaymentModeId().longValue() == chqId.longValue())
					{
						totalChqAmountToBePaid = totalChqAmountToBePaid + paymentSchedules.get(i).getAmount();
					}
					//cashAmount.setValue(totalCashAmountToBePaid);
					//calculate sum of BankTransfer amount
					else if (paymentSchedules.get(i).getPaymentModeId().longValue() == bankTransferID.longValue())
					{
						totalBankTransferAmountToBePaid += paymentSchedules.get(i).getAmount();
						viewRootMap.put("TOTAL_BANK_TRANSFER_AMOUNT_TO_BE_PAID", totalBankTransferAmountToBePaid);
					}
				}
		}
	}
	public String getTodayDateString() {
		return todayDateString;
	}
	public void setTodayDateString(String todayDateString) {
		this.todayDateString = todayDateString;
	}
	
	public String saveAndExit()
	{
		
		return "saveAndExit";
	}
	@SuppressWarnings("unchecked")
	private boolean isPaymentCorrect()
	{
		boolean correct = true;
		Double chqAmountCollected=0D;
		Double bnkTrsnfrAmountCollected=0D;
		errorMessages = new ArrayList<String>(0);
		dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
		bankTransferPaymentList =(List<PaymentReceiptDetailView>) viewRootMap.get("RECEIVE_PAYMENT_BANK_TRANSFER_GRID");
		for (PaymentReceiptDetailView paymentReceiptDetailView : dataList) 
		{
			chqAmountCollected+= paymentReceiptDetailView.getAmount();
			
		}
		if(new Double(totalChqAmountToBePaid).compareTo(chqAmountCollected)!=0)
		{
			errorMessages.add(getBundle().getString(MessageConstants.ReceivePaymentScreen.chkAmountInvalid));
			correct = false;
		}
		for (PaymentReceiptDetailView paymentReceiptDetailView : bankTransferPaymentList) 
		{
			bnkTrsnfrAmountCollected += paymentReceiptDetailView.getAmount();
			
		}
		if(new Double(getTotalBankTransferAmountToBePaid()).compareTo(bnkTrsnfrAmountCollected)!=0)
		{
			errorMessages.add(getBundle().getString(MessageConstants.ReceivePaymentScreen.bankTransferAmountInvalid));
			correct = false;
		}
		
		
		try {
			if (getChequeTabRendered() && !AttachmentBean.mandatoryDocsValidated()){
				errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
				paymentsTabPanel.setSelectedTab( "attachmentTab" );
				correct = false;
			}
		} catch (Exception e) {
			logger.LogException("", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

		return correct;
	}

	
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}
	
	public String getChqType() {
		return chqType;
	}
	
	public void setChqType(String chqType) {
		this.chqType = chqType;
	}    
	
	public boolean chkDate(Date date)
	{
		boolean correct = false;
		
		List<PaymentScheduleView> paymentsLeft = getPaymentSchedules();
		
		for (int i=0; i<paymentsLeft.size(); i++)
		{
			if (df.format(paymentsLeft.get(i).getPaymentDueOn()).equals(df.format(date)))
				correct = true;
			
			
		}
		
		return correct;
	}
	
	public String getErrorMessages() {
		
		String methodName="getErrorMessages";
    	logger.logInfo(methodName+"|"+" Start");
		
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
				{
					messageList += "<LI>"+ message+ " <br></br>" ;
			    }
			messageList = messageList + "</UL></B></FONT>\n";
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		
		return	messageList;
	}

	public ResourceBundle getBundle() {
	
	String methodName="getBundle";
	logger.logInfo(methodName+"|"+" Start");

    if(getIsArabicLocale())

          bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

    else

          bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

    logger.logInfo(methodName+"|"+" Finish");
    
    return bundle;

}
	@SuppressWarnings("unchecked")
	public void addCashReciptDetailView()
	{
		if(cashAmount.getValue()!=null && cashAmount.getValue().toString().trim().length()!= 0 && !cashAmount.getValue().toString().equals("0.00"))		
		
		{
		
			dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
			PaymentReceiptDetailView paymentReceiptDetailView = new PaymentReceiptDetailView();
	
			paymentReceiptDetailView.setAmount(0.00);
			paymentReceiptDetailView.setExchangeRate(Double.valueOf(exchangeRate.getValue().toString()));


		
			paymentReceiptDetailView.setModeOfPaymentEn(WebConstants.PAYMENT_METHOD_CASH_DISPLAY);
			PaymentMethodView paymentMethod = new PaymentMethodView();
			//paymentMethod.setPaymentMethodId(getPaymentModeId(WebConstants.PAYMENT_MODE_CASH));
			paymentMethod.setDescription(WebConstants.PAYMENT_METHOD_CASH_DISPLAY);
			paymentMethod.setPaymentMethodId(WebConstants.PAYMENT_METHOD_CASH_ID);
			paymentReceiptDetailView.setPaymentMethod(paymentMethod);
			
				
			paymentReceiptDetailView.setAmount(totalCashAmountToBePaid);

			paymentReceiptDetailView.setMethodRefDate(new Date());
			
			//Setting not null values
			paymentReceiptDetailView.setCreatedBy(getLoggedInUser());
			paymentReceiptDetailView.setCreatedOn(new Date());
			paymentReceiptDetailView.setUpdatedBy(getLoggedInUser());
			paymentReceiptDetailView.setUpdatedOn(new Date());
			paymentReceiptDetailView.setIsDeleted(new Long(0));
			paymentReceiptDetailView.setRecordStatus(new Long(1));
			paymentReceiptDetailView.setIsReceived("Y");
			setPaymentScheduleForCash(paymentReceiptDetailView);
			
			
			dataList.add(paymentReceiptDetailView);
		
		}
	}
	@SuppressWarnings("unchecked")
	public void setPaymentScheduleForCheque(PaymentReceiptDetailView payRecDetail){
		
		List<PaymentScheduleView> paySchList = 	getPaymentSchedules();
	    List<PaymentScheduleView> paySchListForAdd = new ArrayList<PaymentScheduleView>();
	    Iterator  paySchListIterator= paySchList.iterator();
	    DomainDataView ddv= CommonUtil.getIdFromType(
	    		                                      CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	                                                  WebConstants.PAYMENT_SCHEDULE_COLLECTED
	                                                 );
	    String csPaymentNumbers="";
	    
	    while (paySchListIterator.hasNext())
	    {
	    	
		    	PaymentScheduleView paySchView =(PaymentScheduleView) paySchListIterator.next();
		    	if(paySchView.getPaymentModeId().toString().equals(getPaymentModeChequeId()) && 
		    	   paySchView.getSelected())
		    	{
		    		
		    		paySchView.setIsReceived("Y");
		    		paySchView.setPaymentDate(new Date());
		    		paySchView.setUpdatedBy(getLoggedInUser());
		    		paySchView.setUpdatedOn(new Date());
				    paySchView.setStatusId(ddv.getDomainDataId());
				    csPaymentNumbers += paySchView.getPaymentNumber()+",";
		    		paySchListForAdd.add(paySchView);
		    	}
	    }
	    
	    if(paySchListForAdd.size()>0){
	    	payRecDetail.setPaymentScheduleViews(paySchListForAdd);
	    	payRecDetail.setCsPaymentScheduleNum(csPaymentNumbers); 
	    	
	       }
	    
		}
	@SuppressWarnings("unchecked")
	public void chkPaySc_Changed()
    {
		errorMessages = new ArrayList<String>(0);
		try
		{
			PaymentScheduleView selectedPSV = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
			boolean isOtherDueOnDateRowItemDisable = selectedPSV.getSelected();
			List<PaymentScheduleView> psvList = getPaymentSchedules();
			List<PaymentScheduleView> newPSVList = new ArrayList<PaymentScheduleView>(0);
			String paymentModeChequeId =getPaymentModeChequeId();
			Double chequeAmount = 0D;
			transactionAmount.setValue( "0.00" );
			boolean anyPSSelected = false;
			boolean anyBTSelected = false;
			
			for (int i=0;i<psvList.size();i++) 
			{
			
				PaymentScheduleView paymentScheduleView = (PaymentScheduleView )psvList.get(i);
				if(paymentScheduleView.getPaymentModeId().toString().equals(paymentModeChequeId))
				{
					if(paymentScheduleView.getSelected())
					{
						chequeAmount += paymentScheduleView.getAmount();
						anyPSSelected = true;
						this.setChequeDateDt(selectedPSV.getPaymentDueOn());
						paymentsTabPanel.setSelectedTab(chequeTab.getId());
					}
					else if(CommonUtil.getDaysBetween(paymentScheduleView.getPaymentDueOn(), selectedPSV.getPaymentDueOn())!=0)
					{
						paymentScheduleView.setDisabled(isOtherDueOnDateRowItemDisable);
						paymentScheduleView.setSelected(false);
					}
					else if(CommonUtil.getDaysBetween(paymentScheduleView.getPaymentDueOn(), selectedPSV.getPaymentDueOn())==0 && !selectedPSV.getPaymentModeId().toString().equals(paymentModeChequeId))
					{
						paymentScheduleView.setDisabled(isOtherDueOnDateRowItemDisable);
						paymentScheduleView.setSelected(false);
					}
				}
				//if Payment_method is Bank_Transfer
				else if(paymentScheduleView.getPaymentModeId().toString().equals(getPaymentModeBankTransferId()))
				{
					if(paymentScheduleView.getSelected())
					{
						anyBTSelected = true;
						viewRootMap.put("TRANSFER_AMOUNT", paymentScheduleView.getAmount());
						viewRootMap.put("TRANSFER_DATE", paymentScheduleView.getPaymentDueOn());
						paymentsTabPanel.setSelectedTab(bankTransferTab.getId());
						buttonSaveBankTransfer.setRendered(true);
					}
					else
					{
						paymentScheduleView.setDisabled(isOtherDueOnDateRowItemDisable);
						paymentScheduleView.setSelected(false);
						if(!anyBTSelected)
						{
							viewRootMap.remove("TRANSFER_AMOUNT");
							viewRootMap.remove("TRANSFER_DATE");
							buttonSaveBankTransfer.setRendered(false);
						}
					}	
				}
				
				newPSVList.add(paymentScheduleView);
			}
			
			if(!anyPSSelected)
			   clearChequeInputFields();
			if(!anyBTSelected)
				clearBankTransferInputFields();
			else
				buttonSaveBankTransfer.setRendered(true);
			
			viewRootMap.put("anyPSSelected",anyPSSelected);
			viewRootMap.put("anyBTSelected",anyBTSelected);
			buttonSave.setRendered(anyPSSelected);
			
			if(chequeAmount>0)
				transactionAmount.setValue(chequeAmount.toString());
			viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,newPSVList);
		}
		catch(Exception ex)
		{
		   logger.LogException("chkPaySc_Changed|Error Occured ..",ex);
		}
    }
	@SuppressWarnings("unchecked")
	private void clearBankTransferInputFields()
	{
		viewRootMap.remove("TRANSFER_DATE");
		viewRootMap.remove("TRANSFER_AMOUNT");
		txtAccountNumber.setValue("");
		txtTransferAmount.setValue("0.00");
		txtTransferNumber.setValue("");
		selectOneBank="-1";
	}
	@SuppressWarnings("unchecked")
    public void setPaymentScheduleForCash(PaymentReceiptDetailView payRecDetail)
	{
		
		    List<PaymentScheduleView> paySchList = 	getPaymentSchedules();
		    List<PaymentScheduleView> paySchListForAdd = new ArrayList<PaymentScheduleView>();
		    Iterator  paySchListIterator= paySchList.iterator();
		    DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
		         WebConstants.REALIZED);
		    DomainDataView ddv2= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
		         WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		    
		    while (paySchListIterator.hasNext())
		    {
			    	PaymentScheduleView paySchView =(PaymentScheduleView) paySchListIterator.next();
			    	if( paySchView.getPaymentModeId().equals( ddv2.getDomainDataId() ) )
			    	{
			    		paySchView.setIsReceived("Y");
			    		paySchView.setPaymentDate(new Date());
			    		paySchView.setUpdatedBy(getLoggedInUser());
			    		paySchView.setUpdatedOn(new Date());
			    		paySchView.setStatusId(ddv.getDomainDataId());
			    		paySchListForAdd.add(paySchView);
			    	}
		    }
		    
		    if(paySchListForAdd.size()>0)
		    	payRecDetail.setPaymentScheduleViews(paySchListForAdd);
    
	}
    public String getPaymentModeCashId() {
                	
        
    	 DomainDataView ddv = (DomainDataView )viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
    	 return ddv.getDomainDataId().toString();
     }
    public String getPaymentModeChequeId() {
                	
        
    	DomainDataView ddv = (DomainDataView )viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
    	return ddv.getDomainDataId().toString();
    }
   public String getPaymentModeBankTransferId() {
        
    	DomainDataView ddv = (DomainDataView )viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);
    	return ddv.getDomainDataId().toString();
    }
	public String getSelectOneBank() {
		return selectOneBank;
	}
	public void setSelectOneBank(String selectOneBank) {
		this.selectOneBank = selectOneBank;
	}
	@SuppressWarnings("unchecked")
	public Double getChequeAmountReceived() 
	{
		chequeAmountReceived =0D;
		dataList =(List<PaymentReceiptDetailView>) session.get(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID);
		for (PaymentReceiptDetailView paymentReceiptDetailView : dataList ) 
		{
			if(paymentReceiptDetailView.getPaymentMethod()!=null && 
					paymentReceiptDetailView.getPaymentMethod().getPaymentMethodId().compareTo(WebConstants.PAYMENT_METHOD_CHEQUE_ID)==0
					)
				chequeAmountReceived+=paymentReceiptDetailView.getAmount();
			
		}
		return chequeAmountReceived;
	}
	public void setChequeAmountReceived(Double chequeAmountReceived) {
		this.chequeAmountReceived = chequeAmountReceived;
	}

	public Boolean getChequeTabRendered() {
		if(viewRootMap.containsKey("CHEQUE_TAB_RENDERED"))
			chequeTabRendered = (Boolean)viewRootMap.get("CHEQUE_TAB_RENDERED"); 
			return chequeTabRendered;
		
	}
	@SuppressWarnings("unchecked")
	public void setChequeTabRendered(Boolean chequeTabRendered) 
	{
		this.chequeTabRendered = chequeTabRendered;
		if(this.chequeTabRendered !=null)
			viewRootMap.put("CHEQUE_TAB_RENDERED",this.chequeTabRendered);
		
	}
	public HtmlInputText getBankName() {
		return bankName;
	}
	public void setBankName(HtmlInputText bankName) {
		this.bankName = bankName;
	}
	public HtmlTab getBankTransferTab() {
		return bankTransferTab;
	}
	public void setBankTransferTab(HtmlTab bankTransferTab) {
		this.bankTransferTab = bankTransferTab;
	}
	public HtmlCommandButton getButtonSaveBankTransfer() {
		return buttonSaveBankTransfer;
	}
	public void setButtonSaveBankTransfer(HtmlCommandButton buttonSaveBankTransfer) {
		this.buttonSaveBankTransfer = buttonSaveBankTransfer;
	}
	@SuppressWarnings("unchecked")
	public void addBankTransfer()
	{

		String methodName = "addBankTransfer";
		errorMessages = new ArrayList<String>(0);
		try{
			logger.logInfo(methodName +"|Start");
			if ( validateBankTransferInput() )
			{
				bankTransferPaymentList =(List<PaymentReceiptDetailView>) viewRootMap.get("RECEIVE_PAYMENT_BANK_TRANSFER_GRID");
				PaymentReceiptDetailView paymentReceiptDetailView = new PaymentReceiptDetailView();
				paymentReceiptDetailView.setModeOfPaymentEn(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_DISPLAY);
				paymentReceiptDetailView.setAccountNo(txtAccountNumber.getValue().toString().trim());
				PaymentMethodView paymentMethod = new PaymentMethodView();
				paymentMethod.setDescription(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_DISPLAY);
				paymentReceiptDetailView.setAmount(Double.parseDouble(txtTransferAmount.getValue().toString()));
				paymentReceiptDetailView.setMethodRefNo(txtTransferNumber.getValue().toString().trim());
				paymentReceiptDetailView.setMethodRefDate(getTransferDate());
				paymentReceiptDetailView.setBankId(new Long(selectOneBank));
				BankView bankView =getBanksViewFromBankId(new Long(selectOneBank));
				if(bankView !=null)
				{
					paymentReceiptDetailView.setBankEn(bankView.getBankEn());
					paymentReceiptDetailView.setBankAr(bankView.getBankAr());
					
				}
				paymentMethod.setPaymentMethodId(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID);
				
				paymentReceiptDetailView.setPaymentMethod(paymentMethod);	
				paymentReceiptDetailView.setCreatedBy(getLoggedInUser());
				paymentReceiptDetailView.setCreatedOn(new Date());
				paymentReceiptDetailView.setUpdatedBy(getLoggedInUser());
				paymentReceiptDetailView.setUpdatedOn(new Date());
				paymentReceiptDetailView.setIsDeleted(new Long(0));
				paymentReceiptDetailView.setRecordStatus(new Long(1));
				paymentReceiptDetailView.setIsReceived("Y");
				setPaymentScheduleForBankTransfer(paymentReceiptDetailView);
				bankTransferPaymentList.add(paymentReceiptDetailView);
				
				clearBankTransferInputFields();
				setSelectedPaymentSchAsAddedToPaymentReceipt();
				viewRootMap.remove("anyBTSelected");
			}
			viewRootMap.put("RECEIVE_PAYMENT_BANK_TRANSFER_GRID",bankTransferPaymentList);
			getBankTransferPaymentList();
			logger.logInfo(methodName +"|Finish");
		}
		catch(Exception e)
		{
			errorMessages.clear();
			logger.LogException(methodName +"|Error Occured",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	private void setPaymentScheduleForBankTransfer(	PaymentReceiptDetailView paymentReceiptDetailView) 
	{
		List<PaymentScheduleView> paySchList = 	getPaymentSchedules();
	    List<PaymentScheduleView> paySchListForAdd = new ArrayList<PaymentScheduleView>();
	    Iterator  paySchListIterator= paySchList.iterator();
	    DomainDataView ddv= CommonUtil.getIdFromType(
	    		                                      CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	                                                  WebConstants.REALIZED
	                                                 );
	    String csPaymentNumbers="";
	    
	    while (paySchListIterator.hasNext())
	    {
	    	
		    	PaymentScheduleView paySchView =(PaymentScheduleView) paySchListIterator.next();
		    	if(paySchView.getPaymentModeId().toString().equals(getPaymentModeBankTransferId()) && 
		    	   paySchView.getSelected())
		    	{
		    		
		    		paySchView.setIsReceived("Y");
		    		paySchView.setPaymentDate(new Date());
		    		paySchView.setUpdatedBy(getLoggedInUser());
		    		paySchView.setUpdatedOn(new Date());
				    paySchView.setStatusId(ddv.getDomainDataId());
				    csPaymentNumbers += paySchView.getPaymentNumber()+",";
		    		paySchListForAdd.add(paySchView);
		    	}
	    }
	    
	    if(paySchListForAdd.size()>0)
	    	{
	    		paymentReceiptDetailView.setPaymentScheduleViews(paySchListForAdd);
	    		paymentReceiptDetailView.setCsPaymentScheduleNum(csPaymentNumbers);
	    	}
	}
	private boolean validateBankTransferInput()
	{
		boolean isValid=false;
			if(txtAccountNumber.getValue()==null ||txtAccountNumber.getValue().toString().trim().equals("")||
			   txtTransferNumber.getValue()==null ||txtTransferNumber.getValue().toString().trim().equals("") ||
			   selectOneBank == null || selectOneBank.endsWith("-1"))
				{	
					isValid = false;
					errorMessages.add(CommonUtil.getBundleMessage("payments.errMsg.provideMandatoryField"));
				}
			else
				isValid = true;
			
		return isValid;
	}
	@SuppressWarnings("unchecked")
	public List<PaymentReceiptDetailView> getBankTransferPaymentList() 
	{
		if(viewRootMap.get("RECEIVE_PAYMENT_BANK_TRANSFER_GRID") !=null )
		  bankTransferPaymentList = (List<PaymentReceiptDetailView>) viewRootMap.get("RECEIVE_PAYMENT_BANK_TRANSFER_GRID");
		return bankTransferPaymentList;
	}
	public void setBankTransferPaymentList(
			List<PaymentReceiptDetailView> bankTransferPaymentList) {
		this.bankTransferPaymentList = bankTransferPaymentList;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Date getTransferDate() 
	{
		if(viewRootMap.get("TRANSFER_DATE")!=null)
			transferDate = (Date) viewRootMap.get("TRANSFER_DATE") ;
		else
			transferDate = null;
		return transferDate;
	}
	public void setTransferDate(Date transferDate)
	{
		if(transferDate != null)
			viewRootMap.put("TRANSFER_DATE", transferDate);
		this.transferDate = transferDate;
	}
	public String getTransferNumber()
	{
		if(viewRootMap.get("TRANSFER_NUMBER")!=null)
			transferNumber = viewRootMap.get("TRANSFER_NUMBER").toString() ;
		return transferNumber;
	}
	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}
	public String getTransferAmount()
	{
		if(viewRootMap.get("TRANSFER_AMOUNT")!=null)
			transferAmount = viewRootMap.get("TRANSFER_AMOUNT").toString() ;
		else
			transferAmount ="0.00";
		return transferAmount;
	}
	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}
	public HtmlInputText getTxtAccountNumber() {
		return txtAccountNumber;
	}
	public void setTxtAccountNumber(HtmlInputText txtAccountNumber) {
		this.txtAccountNumber = txtAccountNumber;
	}
	public HtmlCalendar getClndrTransferDate() {
		return clndrTransferDate;
	}
	public void setClndrTransferDate(HtmlCalendar clndrTransferDate) {
		this.clndrTransferDate = clndrTransferDate;
	}
	public HtmlInputText getTxtTransferNumber() {
		return txtTransferNumber;
	}
	public void setTxtTransferNumber(HtmlInputText txtTransferNumber) {
		this.txtTransferNumber = txtTransferNumber;
	}
	public HtmlInputText getTxtTransferAmount() {
		return txtTransferAmount;
	}
	public void setTxtTransferAmount(HtmlInputText txtTransferAmount) {
		this.txtTransferAmount = txtTransferAmount;
	}
	public List<PaymentReceiptDetailView> getDataList2() {
		return dataList2;
	}
	public void setDataList2(List<PaymentReceiptDetailView> dataList2) {
		this.dataList2 = dataList2;
	}
	public HtmlDataTable getBankTransferDataTable() {
		return bankTransferDataTable;
	}
	public void setBankTransferDataTable(HtmlDataTable bankTransferDataTable) {
		this.bankTransferDataTable = bankTransferDataTable;
	}
	public double getTotalBankTransferAmountToBePaid() 
	{
		if(viewRootMap.get("TOTAL_BANK_TRANSFER_AMOUNT_TO_BE_PAID") != null)
			totalBankTransferAmountToBePaid = (Double) viewRootMap.get("TOTAL_BANK_TRANSFER_AMOUNT_TO_BE_PAID"); 
		return totalBankTransferAmountToBePaid;
	}
	public void setTotalBankTransferAmountToBePaid(
			double totalBankTransferAmountToBePaid) {
		this.totalBankTransferAmountToBePaid = totalBankTransferAmountToBePaid;
	}
	public HtmlTab getChequeTab() {
		return chequeTab;
	}
	public void setChequeTab(HtmlTab chequeTab) {
		this.chequeTab = chequeTab;
	}
	public HtmlTab getCashTab() {
		return cashTab;
	}
	public void setCashTab(HtmlTab cashTab) {
		this.cashTab = cashTab;
	}
	public HtmlTabPanel getPaymentsTabPanel() {
		return paymentsTabPanel;
	}
	public void setPaymentsTabPanel(HtmlTabPanel paymentsTabPanel) {
		this.paymentsTabPanel = paymentsTabPanel;
	}
	public boolean isPaymentPossible() {
		if( viewRootMap.get( "paymentPossible" )!= null )
		{
			paymentPossible = Boolean.parseBoolean(viewRootMap.get( "paymentPossible" ).toString() );  
		}
		return paymentPossible;
	}
	public void setPaymentPossible(boolean paymentPossible) {
		
		this.paymentPossible = paymentPossible;
		viewRootMap.put( "paymentPossible" , this.paymentPossible); 
	}

    
}
