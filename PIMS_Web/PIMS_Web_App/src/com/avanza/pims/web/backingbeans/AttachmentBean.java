package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.DocumentManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DocumentFolderView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.ui.util.ResourceUtil;


public class AttachmentBean extends AbstractController 
{

	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(AttachmentBean.class);
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer pageIndex = 0;	
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	private HtmlDataTable dataTable = new HtmlDataTable();
	private HtmlPanelGrid uploadPanel = new HtmlPanelGrid(); 
	private HtmlSelectOneMenu docTypeCombo = new HtmlSelectOneMenu();
	
	private List<DocumentView> procDocs = new ArrayList<DocumentView>();
	private List<DocumentView> otherDocs = new ArrayList<DocumentView>();
	private List<DocumentView> allDocs = new ArrayList<DocumentView>();
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private String errorMessage = "";
	private String infoMessage = "";
	
	private final Long FILE_MAX_SIZE = 419430400L;
	private final String FILE_MAX_SIZE_STR = "400MB";
	
	private DocumentView searchDocument = new DocumentView();
	
	UtilityServiceAgent utilServiceAgent = new UtilityServiceAgent();
	
	public AttachmentBean (){
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
		
   	 	if(!isPostBack() )
   	 	{
   	 		getProcedureDocs();
   	 	}  	 	
   	 	//Specifically for NOL and open file because if a nol/file type is selected then in not post back it will bring Proc Doc
   	 	//but if in the same pagescope the nol/file type is changed then it will not go into the getProcDoc although we 
   	 	//require Proc doc to be updated
   	 	else if(
   	 			viewMap.containsKey(WebConstants.Attachment.PROCEDURE_SUB_KEY) && 
   	 			viewMap.containsKey(WebConstants.Attachment.PROCEDURE_SUB_KEY_OLD)
   	 		   )
   	 	{
   	 	 if(
   	 		viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY).toString().compareTo(viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY_OLD).toString())!=0
   	 	   )
   	 	 {
   	 		getProcedureDocs();
   	 	 }
   	 	}
 	 	else  if(viewMap.containsKey(WebConstants.Attachment.PROCEDURE_SUB_KEY) && !viewMap.containsKey(WebConstants.Attachment.PROCEDURE_SUB_KEY_OLD))
 	 	{
	 	  getProcedureDocs();
	 	  getAllStoredDocs();
 	 	}	

   	 	if(sessionMap.containsKey("success") && sessionMap.containsKey("documentView")){
   	 			sessionMap.remove("success");
		   	 	DocumentView documentView = (DocumentView)sessionMap.get("documentView");
		   	 	Boolean isProcDoc = (Boolean)sessionMap.get("isProcDoc");
		   	 	if(isProcDoc!=null && documentView!=null){
				   	if(isProcDoc){
						documentView.setCanDelete(false);
						Integer tempIndex = (Integer)sessionMap.get("procDocIndex");
						sessionMap.remove("procDocIndex");
						if(tempIndex!=null){
							int index = tempIndex.intValue();
							List<DocumentView> tempProcDocs = new ArrayList<DocumentView>();
							tempProcDocs = getProcDocs();
							if(!tempProcDocs.isEmpty()){
								tempProcDocs.remove(index);
								tempProcDocs.add(index,documentView);
								setProcDocs(tempProcDocs);
							}
						}
					}
					else{
						documentView.setCanDelete(true);
						List<DocumentView> tempProcDocs = getOtherDocs();
						tempProcDocs.add(documentView);
						setOtherDocs(tempProcDocs);
					}
				   	
				   	if(sessionMap.containsKey("updateObjectIdRequired")){
				   		appendToDocumentIdList(documentView.getDocumentId());
				   	}
		   	 }
	   	 	infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_SUCCESS);
   	 	}
   	 	
   	 	if(!viewMap.containsKey("gotAllStoredDocs"))
   	 		getAllStoredDocs();
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		try{
			
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessage = ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR);        	
		}
	}
	
	private Boolean mergeDocs(List<DocumentView> allUnmergedDocs){
		Boolean success = false;
		try{
			List<DocumentView> otherDocs = new ArrayList<DocumentView>();
			procDocs = getProcDocs();
			for(DocumentView documentView: allUnmergedDocs){
				if(documentView.getProcedureDocumentId()!=null){ // the unmerged document is a procedure document
					for(DocumentView procDocView: procDocs){
						if(procDocView.getProcedureDocumentId()==documentView.getProcedureDocumentId()){
							procDocs.remove(procDocView);
							documentView.setIsProcDoc(true);
							procDocs.add(documentView);
							break;
						}
					}
				}
				else{ // the unmerged document is NOT a procedure document
					otherDocs.add(documentView);
				}
			}
			setProcDocs(procDocs);
			setOtherDocs(otherDocs);
		}
		catch(Exception exception){
			logger.LogException("mergeDocs crashed...",exception);
		}
		return success;
	}
	
	
	@SuppressWarnings("unchecked")
	public Boolean getAllStoredDocs(){
		Boolean success = false;
		try
		{
			
			String associatedObjectId = null;
			if(viewMap.get(WebConstants.Attachment.ASSOCIATED_OBJECT_ID)!=null)
				associatedObjectId = viewMap.get(WebConstants.Attachment.ASSOCIATED_OBJECT_ID).toString();

			if(StringHelper.isNotEmpty(associatedObjectId))
			{
				String repositoryId = (String) viewMap.get(WebConstants.Attachment.REPOSITORY_ID);
				String externalId = (String) viewMap.get(WebConstants.Attachment.EXTERNAL_ID);			
				DocumentView searchDocView = new DocumentView();
				searchDocView.setAssociatedObjectId(associatedObjectId);
				searchDocView.setExternalId(externalId);
				searchDocView.setRepositoryId(repositoryId);
				
				List<DocumentView> allUnmergedDocs = utilServiceAgent.getDocuments(searchDocView);
				mergeDocs(allUnmergedDocs);
				success = true;
				viewMap.put("gotAllStoredDocs",true);
			}
			else
				success = false;
			
		}
		catch(Exception exception){
			logger.LogException("getAllStoredDocs crashed...",exception);
		}
		return success;
	}
	@SuppressWarnings("unchecked")
	public Boolean getProcedureDocs(){
		Boolean success = false;
		try{
			UtilityServiceAgent utilServiceAgent = new UtilityServiceAgent();
			String procedureKey = (String)viewMap.get(WebConstants.Attachment.PROCEDURE_KEY);
			String typeId = "";
			if(viewMap.containsKey(WebConstants.Attachment.PROCEDURE_SUB_KEY) && 
			  viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY).toString().trim()!=null &&
			  viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY).toString().trim().length()>0 &&
			  !viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY).toString().trim().equals("-1"))
			{
				typeId = (String)viewMap.get(WebConstants.Attachment.PROCEDURE_SUB_KEY);
				viewMap.put(WebConstants.Attachment.PROCEDURE_SUB_KEY_OLD, typeId);
			}
			if(procedureKey!=null)
			{
				if(procedureKey.compareTo(WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE) == 0){
					procDocs = utilServiceAgent.getProcedureDocumentsByFileType(procedureKey,typeId);
					viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS, this.procDocs);
				}
				else{
					procDocs = utilServiceAgent.getProcedureDocuments(procedureKey,typeId);
					viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS, this.procDocs);
				}
				
				success = true;
			}
			else
				success = false;
			
		}
		catch(Exception exception){
			logger.LogException("getProcedureDocs crashed...",exception);
		}
		return success;
	}
	@SuppressWarnings("unchecked")	
	public void removeAttachment(ActionEvent evt){
		try {
			otherDocs = getOtherDocs();
			DocumentView removeditem = (DocumentView) dataTable.getRowData();
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			Boolean success = utilityServiceAgent.deleteDocument(removeditem.getDocumentId());
			if(success){
				otherDocs.remove(removeditem);
				setOtherDocs(otherDocs);
				infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_DOC_DELETE_SUCCESS);
			}
			else
				throw new Exception();
		}
		catch (Exception exception) {
			logger.LogException("removeAttachment() crashed ", exception);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_DOC_DELETE_FAILURE);
		}
	}
	@SuppressWarnings("unchecked")	
	public void addFileToProcDoc(ActionEvent event){
		try {
			DocumentView selectedDoc =(DocumentView)dataTable.getRowData();
			viewMap.put("selectedDoc", selectedDoc);
			viewMap.put("isProcDoc", true);
			viewMap.put("showUploadPanel", true);
			viewMap.put("procDocIndex", dataTable.getRowIndex());
			viewMap.remove("comments");
			String docName = getIsEnglishLocale()?selectedDoc.getDocTypeEn():selectedDoc.getDocTypeAr();
			docName += " : " + selectedDoc.getDocumentTitle();
			infoMessage = CommonUtil.getParamBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_PROC_DOC_FILE, docName);
//			upload();
//			applyMode(); 
		}
		catch (Exception exception) {
			logger.LogException("addFileToProcDoc() crashed ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	public void addFileToProcDoc(){
		try {
			DocumentView selectedDoc =(DocumentView)dataTable.getRowData();
			sessionMap.put("selectedDoc", selectedDoc);
			sessionMap.put("isProcDoc", true);
			sessionMap.put("procDocIndex", dataTable.getRowIndex());
			String docName = getIsEnglishLocale()?selectedDoc.getDocTypeEn():selectedDoc.getDocTypeAr();
			docName += " : " + selectedDoc.getDocumentTitle();
			infoMessage = CommonUtil.getParamBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_PROC_DOC_FILE, docName);
			sessionMap.put("infoMessage", infoMessage);
//			upload();
//			applyMode(); 
		}
		catch (Exception exception) {
			logger.LogException("addFileToProcDoc() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	public String addOtherDoc() 
	{
		try 
		{
				sessionMap.remove("selectedDoc");
				sessionMap.put("isProcDoc", false);
				infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_OTHER_DOC_FILE);
				sessionMap.put("infoMessage", infoMessage);
				
		}
		catch (Exception exception) 
		{
			logger.LogException("addOtherDoc() crashed ", exception);
		}
		return "";
    }
	
	@SuppressWarnings("unchecked")
	public String cancel() 
	{
		try {
				viewMap.put("showUploadPanel", false);
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return "";
    }
	@SuppressWarnings("unchecked")
	private void appendToDocumentIdList(Long documentId){
		List<Long> docIdList = (List<Long>)viewMap.get("documentIdList");
		if(docIdList==null && viewMap.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS) != null )
		{
			docIdList = (List<Long>)viewMap.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS);
			
		}
		if(docIdList==null)
		{
			docIdList = new ArrayList<Long>();
		}
		docIdList.add(documentId);
		if(viewMap.get(WebConstants.Attachment.EXTERNAL_ID).toString().equals(WebConstants.Attachment.EXTERNAL_ID_PAYMENT_RECEIPTS))
			viewMap.put(WebConstants.Attachment.ATTACHED_CHEQUE_IDS,docIdList);
		else
		     viewMap.put("documentIdList",docIdList);
	}
	
	private Boolean validateForUpload(){
    	logger.logInfo("validateForUpload() started...");
	    Boolean validated = true;
    	try
 		{
    		if(!getIsProcDoc()){
    			if(getDocTypeId().equals("0")){
    				errorMessage = CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Attachment.DOC_NAME);
    				validated = false;
    				return validated;
    			}
    		
		    	if(StringHelper.isEmpty(getDocumentDescription())){
					errorMessage = CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Attachment.DOC_DESC);
					validated = false;
					return validated;
				}
    		}
    		
    	
    		UploadedFile file =  getSelectedFile();
    		if(file!=null){
    			Long size = file.getSize();
    			if(size>FILE_MAX_SIZE)
    			{
    				errorMessage = CommonUtil.getParamBundleMessage(MessageConstants.Attachment.MSG_FILE_SIZE_TOO_LARGE, FILE_MAX_SIZE_STR);
					validated = false;
					return validated;
    			}
    		}
    		
			logger.logInfo("validateForUpload() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForUpload() crashed ", exception);
		}
		return validated;
    }
	
	
	public String uploadFile()
	{	
		try 
		{
				if(validateForUpload())
				{
					Long documentId = upload();
					
					if(viewMap.containsKey("updateObjectIdRequired")){
						appendToDocumentIdList(documentId);
						viewMap.remove("updateObjectIdRequired");
					}
					if(documentId==null)
						errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED);
					else{
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_SUCCESS);
						viewMap.remove("showUploadPanel");
					}
					//outcome = Download(documentId);
				}
		} catch (Exception e) 
		{			
			logger.LogException("uploadFile crashed ", e);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAILURE);
		}
		return "";
	}
	
	public String download()
	{
		
		try {
				Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
				DocumentView downloadFile = (DocumentView) dataTable.getRowData();
				Long documentId = downloadFile.getDocumentId();
				sessionMap.put("documentId", documentId.toString());
				
	     } catch (Exception e) 
	     {			
	    	 logger.LogException(" download--- CRASHED --- ", e );
	    	 
		}
		return "DownloadFile";
	}
	
	public Long upload()
	{
		Long documentId = null;
		try
		{
			logger.logInfo("upload started...");

			fileUploadCtrl.setUploadedFile(getSelectedFile());
			
			if(fileUploadCtrl.getUploadedFile() != null)
			{			
				Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");
				DocumentView documentView = new DocumentView();
				if(isProcDoc)
					documentView = (DocumentView)viewMap.get("selectedDoc");

				UploadedFile file =  fileUploadCtrl.getUploadedFile();
				
				String updatedBy = CommonUtil.getLoggedInUser();
				String createdBy = CommonUtil.getLoggedInUser();
				Date updatedOn = new Date();
				Date createdOn = new Date();
				
				String repositoryId = (String) viewMap.get("repositoryId");
				String externalId = (String) viewMap.get("externalId");
				String associatedObjectId = (String) viewMap.get("associatedObjectId");
				
				logger.logInfo("repositoryId : %s | externalId : %s | associatedObjectId : %s ", repositoryId, externalId, associatedObjectId);
				
				if(StringHelper.isEmpty(associatedObjectId))
					viewMap.put("updateObjectIdRequired", true);
				
				String documentFileName = getFileNameForPath(file.getName());
				String documentPath = file.getName();
				String documentTitle = getDocumentDescription();
				String comments = getComments();
				String contentType = file.getContentType();
				Long sizeInBytes = file.getSize();
				documentId = documentView.getDocumentId();
				
				logger.logInfo("documentFileName : %s | contentType : %s | sizeInBytes : %s ", documentFileName, contentType, sizeInBytes);
				
				documentView.setUpdatedBy(updatedBy);
				documentView.setUpdatedOn(updatedOn);
				documentView.setCreatedOn(createdOn);
				documentView.setCreatedBy(createdBy);
//				documentView.setUploadedOn(createdOn);
//				documentView.setUploadedBy(createdBy);
				documentView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				documentView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
				
				documentView.setDocumentId(documentId);
				documentView.setAssociatedObjectId(associatedObjectId);
				documentView.setDocumentFileName(documentFileName);
				documentView.setDocumentPath(documentPath);
				documentView.setContentType(contentType);
				documentView.setComments(comments);
				documentView.setDocTypeId(Convert.toLong(getDocTypeId()));
				documentView.setExternalId(externalId);
				documentView.setRepositoryId(repositoryId);
				documentView.setSizeInBytes(sizeInBytes);
				documentView.setIsProcDoc(isProcDoc);
				if(!isProcDoc)
					documentView.setDocumentTitle(documentTitle);
				
				UtilityServiceAgent usa = new UtilityServiceAgent();
				
				if(file.getInputStream().available()!=0)
				{
					//InputStream inputStream  =  file.getInputStream();									
					//BufferedInputStream bis = new BufferedInputStream(inputStream);					
					byte[] fileByteArray = file.getBytes();
					byte[] encodedFileByteArray = Base64.encodeBase64(fileByteArray);					
					String base64EncodedString = new String(encodedFileByteArray);					
					documentView = usa.addAttachment(documentView, base64EncodedString);					
				}
				
				if(isProcDoc){
					documentView.setCanDelete(false);
					Integer tempIndex = (Integer)viewMap.get("procDocIndex");
					if(tempIndex!=null){
						int index = tempIndex.intValue();
						List<DocumentView> tempProcDocs = new ArrayList<DocumentView>();
						tempProcDocs = getProcDocs();
						if(!tempProcDocs.isEmpty()){
							tempProcDocs.remove(index);
							tempProcDocs.add(index,documentView);
							setProcDocs(tempProcDocs);
						}
					}
				}
				else{
					documentView.setCanDelete(true);
					List<DocumentView> tempProcDocs = getOtherDocs();
					tempProcDocs.add(documentView);
					setOtherDocs(tempProcDocs);
				}
				
				documentId = documentView.getDocumentId();
			 }
			else
				logger.logInfo("no file selected for upload!!!");
			
			logger.logInfo("upload completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("upload crashed...", ex);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAILURE);
		}
		return documentId;			
	}
	
	@SuppressWarnings("unchecked")
	public void onOpenArchiveDocumentsPopup()
	{
		try
		{
			String externalId = (String) viewMap.get("externalId");
			String associatedObjectId = (String) viewMap.get("associatedObjectId");
			String procedureKey=  (String ) viewMap.get(WebConstants.Attachment.PROCEDURE_KEY);
			
			sessionMap.put(WebConstants.Attachment.PROCEDURE_KEY, procedureKey);
			sessionMap.put("externalId", externalId);
			sessionMap.put("associatedObjectId", associatedObjectId);
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:openArchivedFilesPopup();";
	
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
		catch (Exception exception) 
		{
			logger.LogException("onOpenArchiveDocumentsPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void openUploadPopup(javax.faces.event.ActionEvent event)
	{
		try
		{
			if(event.getComponent().getId().equals("btnAddAttachment"))
				addOtherDoc();
			else if(event.getComponent().getId().equals("lnkAddAttachment"))
				addFileToProcDoc();
			
			String repositoryId = (String) viewMap.get("repositoryId");
			String externalId = (String) viewMap.get("externalId");
			String associatedObjectId = (String) viewMap.get("associatedObjectId");
			
			sessionMap.put("repositoryId", repositoryId);
			sessionMap.put("externalId", externalId);
			sessionMap.put("associatedObjectId", associatedObjectId);
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showUploadPopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
		catch (Exception exception) 
		{
			logger.LogException("openUploadPopup() crashed ", exception);
		}
    }
	
	
	private String getFileNameForPath (String path) {
        
        if ( path == null || path.equalsIgnoreCase("") ) {
            return path;
         }        
        return path.substring( path.lastIndexOf("\\") + 1, path.length() );
    }
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
	public String getErrorMessage() {
		List<String> temp = new ArrayList<String>();
		if(!errorMessage.equals(""))
			temp.add(errorMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}
	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}
	@SuppressWarnings("unchecked")
	public List<DocumentView> getOtherDocs() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		otherDocs = (List<DocumentView>)viewMap.get(WebConstants.Attachment.OTHER_DOCS);
		if(otherDocs==null)
			otherDocs = new ArrayList<DocumentView>();
		return otherDocs;
	}
	@SuppressWarnings("unchecked")
	public void setOtherDocs(List<DocumentView> otherDocs) {
		if(otherDocs!=null)
			viewMap.put(WebConstants.Attachment.OTHER_DOCS,otherDocs);
	}
	@SuppressWarnings("unchecked")
	public static Boolean mandatoryDocsValidated() throws Exception{
		logger.logInfo("mandatoryDocsValidated started...");
		try	
		{
			List<DocumentView> procDocs = (List<DocumentView>)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.Attachment.PROCEDURE_DOCS);

			if(procDocs!=null)
				for(DocumentView procDoc: procDocs){
					if(procDoc.getDocumentId()==null){
						return false;
					}
				}
			
			logger.logInfo("mandatoryDocsValidated completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("mandatoryDocsValidated crashed...",ex);
			throw ex;
		}
		
		return true;
	}
	@SuppressWarnings("unchecked")
	public List<DocumentView> getAllDocs() {
		allDocs = new ArrayList<DocumentView>();
		allDocs.addAll(getProcDocs());
		allDocs.addAll(getOtherDocs());
		viewMap.put("recordSize",allDocs.size());
		return allDocs;
	}
	public void setAllDocs(List<DocumentView> allDocs) {
		this.allDocs = allDocs;
	}
	@SuppressWarnings("unchecked")
	public List<DocumentView> getProcDocs() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		procDocs = (List<DocumentView>)viewMap.get(WebConstants.Attachment.PROCEDURE_DOCS);
		if(procDocs==null)
			procDocs = new ArrayList<DocumentView>();
		return procDocs;
	}
	@SuppressWarnings("unchecked")
	public void setProcDocs(List<DocumentView> procDocs) {
		if(procDocs!=null)
			viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS,procDocs);
	}
	
	public String getDocumentDescription() {
		String temp = (String)viewMap.get("documentDescription");
		if(temp==null)
			temp = "";
		return temp.trim();
	}

	@SuppressWarnings("unchecked")
	public void setDocumentDescription(String documentDescription) {
		if(documentDescription!=null)
			viewMap.put("documentDescription" , documentDescription);
	}
	
	public String getComments() {
		String temp = (String)viewMap.get("comments");
		return temp;
	}
	@SuppressWarnings("unchecked")
	public void setComments(String comments) {
		if(comments!=null)
			viewMap.put("comments" , comments);
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlPanelGrid getUploadPanel() {
		return uploadPanel;
	}
	public void setUploadPanel(HtmlPanelGrid uploadPanel) {
		this.uploadPanel = uploadPanel;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getShowUploadPanel() {
		Boolean showUploadPanel = (Boolean)viewMap.get("showUploadPanel");
		if(showUploadPanel==null)
			showUploadPanel = false;
		return showUploadPanel;
	}

	public void setShowUploadPanel(Boolean showUploadPanel) {
		if(showUploadPanel!=null)
			viewMap.put("showUploadPanel",showUploadPanel);
	}

	public UploadedFile getSelectedFile() {
		UploadedFile selectedFile = (UploadedFile)viewMap.get("selectedFile");
		
		return selectedFile;
		
	}

	public void setSelectedFile(UploadedFile selectedFile) {
		if(selectedFile!=null)
			viewMap.put("selectedFile", selectedFile);
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	public HtmlSelectOneMenu getDocTypeCombo() {
		return docTypeCombo;
	}

	public void setDocTypeCombo(HtmlSelectOneMenu docTypeCombo) {
		this.docTypeCombo = docTypeCombo;
	}

	public String getDocTypeId() {
		String docTypeId = (String)viewMap.get("docTypeId");
		if(docTypeId==null)
			docTypeId = "0";
		return docTypeId;
	}

	public void setDocTypeId(String docTypeId) {
		if(docTypeId!=null)
			viewMap.put("docTypeId",docTypeId);
	}

	public Boolean getIsProcDoc(){
		Boolean isProcDoc = (Boolean)viewMap.get("isProcDoc");
		if(isProcDoc==null)
			isProcDoc = false;
		return isProcDoc;
	}
	
	public Boolean getAttachmentReadonlyMode(){
		Boolean attachmentReadonlyMode = (Boolean)viewMap.get("attachmentReadonlyMode");
		if(attachmentReadonlyMode==null)
			attachmentReadonlyMode = false;
		return attachmentReadonlyMode;
	}
	
	public Boolean getCanAddAttachment(){
		Boolean canAddAttachment = (Boolean)viewMap.get("canAddAttachment");
		if(canAddAttachment==null)
			canAddAttachment = false;
		return canAddAttachment;
	}
	
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	/**
	 * Deacription: This method will be used to validate the Attachment while you want the functionality for documents to be provided later
	 * @return
	 * @throws Exception
	 */
	public static Boolean validateMandatoryAndLaterProvidedDocs() throws Exception{
		logger.logInfo("validateMandatoryAndLaterProvidedDocs started...");
		try	
		{
			List<DocumentView> procDocs = (List<DocumentView>)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.Attachment.PROCEDURE_DOCS);

			if(procDocs!=null)
				for(DocumentView procDoc: procDocs){
					if(procDoc.getDocumentId()==null && !procDoc.isToBeProvidedLater()){
						return false;
					}
				}
			
			logger.logInfo("mandatoryDocsValidated completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("mandatoryDocsValidated crashed...",ex);
			throw ex;
		}
		
		return true;
	}
	public boolean isOpenFileProcedure(){
		if(viewMap.get(WebConstants.Attachment.PROCEDURE_KEY) != null && viewMap.get(WebConstants.Attachment.PROCEDURE_KEY).toString().compareTo(WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE) == 0){
		return true;	
		}
		else 
			return false;
	}
	
	
	
	@SuppressWarnings("unchecked")	
	public void updateToFileNet(ActionEvent evt){
		try {
			DocumentManager documentManager = new DocumentManager();
			otherDocs = getOtherDocs();
			DocumentView documentView = (DocumentView) dataTable.getRowData();
			
			
			DocumentFolderView documentFolderView = new DocumentFolderView();
			documentFolderView.setExternalId(documentView.getExternalId());
			documentFolderView.setAssociatedObjectId(documentView.getAssociatedObjectId());
			documentFolderView.setDocumentId(documentView.getDocumentId());
			
			if(StringUtils.isEmpty(documentView.getFileNetDocId()) &&  (documentManager.getFolderName(documentFolderView))){
				String documentId = documentManager.uploadeFileNetDoc(documentView.getDocumentId());
				if(StringUtils.isNotEmpty(documentId)){
						otherDocs.get(otherDocs.indexOf(documentView)).setFileNetDocId(documentId);
						setOtherDocs(otherDocs);
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_SUCCESS);
				}else
					throw new Exception();
			}
			
		}
		catch (Exception exception) {
			logger.LogException("updateToFileNet() crashed ", exception);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAIL);
		}
	}

	/**
	 * @return the searchDocument
	 */
	public DocumentView getSearchDocument() {
		searchDocument.setRepositoryId((String) viewMap.get("repositoryId"));
		searchDocument.setExternalId((String)viewMap.get("externalId"));
		searchDocument.setAssociatedObjectId((String) viewMap.get("associatedObjectId"));
		return searchDocument;
	}

	/**
	 * @param searchDocument the searchDocument to set
	 */
	public void setSearchDocument(DocumentView searchDocument) {
		this.searchDocument = searchDocument;
	}
	
	
	@SuppressWarnings("unchecked")
	public void onSearchDocument(){
		
		try {
			
//			viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS, new ArrayList<DocumentView>());
			viewMap.put(WebConstants.Attachment.OTHER_DOCS,utilServiceAgent.getDocumentsWithCriteria(searchDocument));
			getAllDocs();
		} catch (PimsBusinessException e) {
			logger.LogException("onSearchDocument() crashed ", e);
		}
		
	}
}
