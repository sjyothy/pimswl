package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;

import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.proxy.pimschangetenantnamebpel.PIMSChangeTenantNameBPELPortClient;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PrintLeaseContractReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;

import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.RequiredDocumentsView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class ChangeTenantNameBean extends AbstractController {

	public String txtRequestId;
	public String txtcontractNum;
	public String txtStatus;
	public String txtStartDate;
	public String txtEndDate;
	public String createdOn;
	public String createdBy;
	public String txtTenantNumber;
	public String txtTenantName;
	public String txtContractType;
	public String txtTotalContractValue;
	public String txtunitRefNum;
	public String txtUnitRentValue;
	public String txtpropertyName;
	public String txtpropertyType;
	public String txtUnitType;
	public String txtNewTenantName;
	public String txtNewTenantCellNumber;
	public String txtNewTenantPassportNumber;
	public String txtNewTenantDesignation;
	public String txtNewTenantId;
	public String txtNewTenantNumber;
	public String txtTenantId;
	public String txtContractId;
	
	SystemParameters parameters = SystemParameters.getInstance();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(ChangeTenantNameBean.class);

	private List<DomainDataView> ddvList;
	private Long MODE_CASH_ID;

	// Payment tab - components/attributes
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	private HtmlDataTable dataTablePaymentSchedule;
	private String dateFormatForDataTable;
	private HtmlCommandButton requestStatus = new HtmlCommandButton();

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable tbl_RequiredDocs;
	private RequiredDocumentsView rdDataItem = new RequiredDocumentsView();
	private List<RequiredDocumentsView> rdDataList = new ArrayList<RequiredDocumentsView>(
			0);
	private HtmlDataTable tbl_PaymentSchedule;
	private PaymentScheduleView psDataItem = new PaymentScheduleView();
	private List<PaymentScheduleView> psDataList = new ArrayList<PaymentScheduleView>(
			0);
	ContractView contractView = null;
	PersonView newTenantView = null;
	PersonView newTenantViewBasicInfo = null;
	RequestView requestView = new RequestView();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	String pageMode = "";
	SystemParameters param = SystemParameters.getInstance();
	private final String PAGE_MODE_ADD = "PAGE_MODE_ADD";
	private final String PAGE_MODE_UPDATE = "PAGE_MODE_UPDATE";
	private final String PAGE_MODE_UPDATE_APPROVE = "PAGE_MODE_UPDATE_APPROVE";
	private final String PAGE_MODE_UPDATE_APPROVE_COLLECTED = "PAGE_MODE_UPDATE_APPROVE_COLLECTED";
	private final String PAGE_MODE_APPROVE_REJECT = "PAGE_MODE_APPROVE_REJECT";
	private final String PAGE_MODE_PRINT = "PAGE_MODE_PRINT";
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnApprove = new HtmlCommandButton();
	private HtmlCommandButton btnReject = new HtmlCommandButton();
	private HtmlCommandButton btnPay = new HtmlCommandButton();
	private HtmlCommandButton btnComplete = new HtmlCommandButton();
	private HtmlCommandButton btnSend = new HtmlCommandButton();
	private HtmlGraphicImage  imgSearchNewTenant = new HtmlGraphicImage ();
	private HtmlCommandLink lnkAddNewTenant = new HtmlCommandLink();
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	org.richfaces.component.html.HtmlTab tabAttachments=new org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabPayments=new org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabNewTenant=new org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabContractDetails=new org.richfaces.component.html.HtmlTab();
	org.richfaces.component.html.HtmlTab tabApplicantDetails=new org.richfaces.component.html.HtmlTab();
	
	private final String TAB_CONTRACT_DETAILS   = "tabContractDetails";
	private final String TAB_APPLICANT_DETAILS  = "tabApplicantDetails";
	private final String TAB_ATTACHMENT_DETAILS = "tabAttachments";
	private final String TAB_PAYMENT_DETAILS    = "tabPayments";
	private final String TAB_NEW_TENANT	        = "tabNewTenant";
	private final String OWNER_SHIP_TYPE_ID ="OWNER_SHIP_TYPE_ID";
	private String basicInfo="";
	private String hdnApplicantId;
	private HtmlGraphicImage  imgViewNewTenant = new HtmlGraphicImage();
	CommonUtil commonUtil = new CommonUtil();

	// Constructors

	/** default constructor */
	public ChangeTenantNameBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public String openPopUp(String URLtoOpen, String extraJavaScript) {
		String methodName = "openPopUp";
		// final String viewId = "/ChangeTenantName.jsf";
		logger.logInfo(methodName + "|" + "Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// ViewHandler viewHandler = facesContext.getApplication()
		// .getViewHandler();
		// String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = "var screen_width = screen.width;"
				+ "var screen_height = screen.height;"
				+ "window.open('"
				+ URLtoOpen
				+ "','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
		if (extraJavaScript != null && extraJavaScript.length() > 0)
			javaScriptText = extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	private boolean hasNewTenantErrors()
	{
		boolean hasNewTenantErrors =false;
		if (this.getNewTenantView()!=null && this.getNewTenantView().getPersonId() !=null ) 
		{
			
			if(txtTenantId.equals(this.getNewTenantView().getPersonId()))
	    	{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SAME_CURRENTANDNEWTENANT));
				hasNewTenantErrors = true;
	    	}
//			else if(!CommonUtil.isPersonContainCountry(this.getNewTenantView()))
//	    	{
//	    		errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.NationalyNotAdded"));
//	    		hasNewTenantErrors = true;
//	    	}
	    	
		}
        //if newTenantView is empty
		if (this.getNewTenantView()==null || this.getNewTenantView().getPersonId()==null) 
		{
			//if basic info view is not empty then ask person to add new tenant to system
			if(this.getNewTenantViewBasicInfo()!=null )
    			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_BASIC_INFO_PERSON_NOT_ADDED));
			//  if basic info view is also empty then it means that new tenant has not been selected
			else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_REQ_NEW_TENANT));
			hasNewTenantErrors = true;
		}
		return hasNewTenantErrors;
	}
  	private boolean hasErrors() throws PimsBusinessException,Exception {
		String methodName = "hasErrors";
		boolean isError = false;
		logger.logInfo(methodName + "|" + "Start..");

		//Validating New Tenant Start
		if(hasNewTenantErrors())
		{
			tabPanel.setSelectedTab(TAB_NEW_TENANT);
			return true;
		}
		 //Validating New Tenant Finish
		
		//Validating Applicant Start
		if (hdnApplicantId == null || hdnApplicantId.trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			tabPanel.setSelectedTab(TAB_APPLICANT_DETAILS);
			return true;
		}
		//Validating Applicant Finish
		
		//Validating Attachments Start
        if(!AttachmentBean.mandatoryDocsValidated())
	    {
	    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
	    		 tabPanel.setSelectedTab(TAB_ATTACHMENT_DETAILS);
	    		return  true;
	    }
         //Validating Attachments Finish

		logger.logInfo(methodName + "|" + "Finish..");
		return false;
	}

   @SuppressWarnings("unchecked")
	public String btnAdd_Click() 
	{
		String methodName = "btnAdd_Click";
		logger.logInfo(methodName + "|" + "Start...");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try 
		{

			if (!hasErrors())
			{
				String successMessage ="";
		 		if(viewRootMap.containsKey("REQUEST_VIEW"))
		 		   requestView = (RequestView)viewRootMap.get("REQUEST_VIEW");
				setRequestView();
				List<PaymentScheduleView> paymentScheduleViewList = (ArrayList<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				
				String sysComments =MessageConstants.RequestEvents.REQUEST_CREATED; 
				if(requestView.getRequestId()!=null)
				{
					sysComments =MessageConstants.RequestEvents.REQUEST_UPDATED;
					successMessage = ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SUCCESS_UPDATED);
				}
				requestView = psa.persistChangeTenantNameRequest(requestView,paymentScheduleViewList);
				viewRootMap.put("REQUEST_VIEW",requestView);
				saveCommentsAttachments( sysComments);

				if(successMessage.length()<=0)
				   successMessage= java.text.MessageFormat.format(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SUCCESS_ADD),requestView.getRequestNumber());
				
				successMessages.add(successMessage);
				viewRootMap.put("pageMode", PAGE_MODE_UPDATE);
				populateApplicationDetailsTab();

			}
			logger.logInfo(methodName + "|" + "Finish...");
		} catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

private void saveCommentsAttachments( String sysComments)throws Exception 
{
	String methodName ="saveCommentsAttachments";
	requestView = (RequestView) viewRootMap.get("REQUEST_VIEW");
	
	logger.logDebug(methodName + "|" + "Save Attachments...Start");
	saveAttachments(requestView.getRequestId().toString());
	logger.logDebug(methodName + "|" + "Save Attachments...Finish");

	logger.logDebug(methodName + "|" + "Save Comments...Start");
	saveComments(requestView.getRequestId());
	
	saveSystemComments(sysComments,requestView.getRequestId());
	logger.logDebug(methodName + "|" + "Save Comments...Finish");
}
   @SuppressWarnings("unchecked")
    public String btnSend_Click()
    {
    	String methodName ="btnSend_Click";
    	try
    	{
			String endPoint = parameters.getParameter(WebConstants.CHANGE_TENANT_BPEL_ENDPOINT);
			requestView = (RequestView) context.getViewRoot().getAttributes().get("REQUEST_VIEW");
    		logger.logInfo(methodName + "|Start|RequestId:%s|LoggedInUser:%s",requestView.getRequestId(),CommonUtil.getLoggedInUser() );
    		
    		PIMSChangeTenantNameBPELPortClient port = new PIMSChangeTenantNameBPELPortClient();
			txtRequestId = requestView.getRequestId().toString();
			port.setEndpoint(endPoint);
			logger.logInfo(methodName + "|EndPoint|%s", port.getEndpoint() );
			port.initiate(requestView.getRequestId(), CommonUtil.getLoggedInUser(), null, null);
			notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_ChangeTenant_Request_Received);
			String successMessage = ResourceUtil.getInstance().getProperty(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);
			successMessages.add(successMessage);
			saveCommentsAttachments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);
			viewRootMap.put("pageMode", "");
			
			CommonUtil.printReport(requestView.getRequestId());
    		
    		logger.logInfo(methodName + "|Finish...");
    		
		} 
    	catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		return "";
    }


	
	@SuppressWarnings("unchecked")
	public String btnPrint_Click() {	
		
		UtilityServiceAgent usa = new UtilityServiceAgent();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			   
				PrintLeaseContractReportCriteria reportCriteria = 
					new PrintLeaseContractReportCriteria(ReportConstant.Report.PRINT_LEASE_CONTRACT, ReportConstant.Processor.PRINT_LEASE_CONTRACT);
				reportCriteria.setContractId( this.txtContractId);
				sessionMap.put(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			   	//saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PRINTED);
			   	usa.printContract(new Long(this.txtContractId),getLoggedInUser());
		} 
		catch(Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException("btnPrint_Click|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		return null;
	}
	@SuppressWarnings("unchecked")
    public void imgAddPerson_Click() 
	{	
		
		String methodName="imgAddPerson_Click";
		logger.logInfo(methodName+"|Started...");
		try 
		{
			    FacesContext facesContext = FacesContext.getCurrentInstance();
				sessionMap.put(WebConstants.PERSON_BASIC_INFO, this.getNewTenantViewBasicInfo());
				String javaScriptText = "javaScript:addBasicInfo();";
    			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			    addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
			   	logger.logInfo(methodName+"|Finish...");    
		} 
		catch(Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	

	@SuppressWarnings("unchecked")
	private void putViewValuesInControl() 
	{
		String methodName = "putViewValuesInControl";
		logger.logInfo(methodName + "|" + "Start::");

		logger.logInfo(methodName + "|" + "ContractNumber::"+ contractView.getContractNumber());
		this.setTxtcontractNum(contractView.getContractNumber());

		if (contractView.getContractId() != null) 
			this.setTxtContractId(contractView.getContractId().toString());

		if (contractView.getStartDateString() != null) 
			this.setTxtStartDate(contractView.getStartDateString());
		if (contractView.getEndDateString() != null) 
			this.setTxtEndDate(contractView.getEndDateString());
		if (contractView.getStatus() != null) 
			this.setTxtStatus(getIsEnglishLocale() ? contractView.getStatusEn(): contractView.getStatusAr());
		if (contractView.getContractTypeId() != null) 
			this.setTxtContractType(getIsEnglishLocale() ? contractView.getContractTypeEn() : contractView.getContractTypeAr());
		this.setTxtTotalContractValue(contractView.getRentAmount() != null ? contractView.getRentAmount().toString(): "");

		if (contractView.getTenantView() != null) 
		{
			this.setTxtTenantId(contractView.getTenantView().getPersonId() != null ? contractView.getTenantView().getPersonId().toString(): "");
			this.setTxtTenantName(contractView.getTenantView().getPersonFullName().equals("") ? contractView
					.getTenantView().getCompanyName() : contractView
					.getTenantView().getPersonFullName());
			
			this.setTxtTenantNumber(contractView.getTenantView().getAccountNumber());
			viewRootMap.put("CURRENT_TENANT_VIEW", contractView.getTenantView());
		}
		if (contractView.getContractUnitView() != null && contractView.getContractUnitView().size() > 0) 
		{
			List<ContractUnitView> contractUnitViewList = new ArrayList<ContractUnitView>();
			contractUnitViewList.addAll(contractView.getContractUnitView());
			ContractUnitView contractUnitView = contractUnitViewList.get(0);
			this.setTxtUnitType(getIsEnglishLocale() ? contractUnitView.getUnitView().getUnitTypeEn() : 
				                                       contractUnitView.getUnitView().getUnitTypeAr());
			this.setTxtpropertyName(contractUnitView.getUnitView().getPropertyCommercialName());
			this.setTxtpropertyType(contractUnitView.getUnitView().getPropertyTypeEn());

			this.setTxtunitRefNum(contractUnitView.getUnitView().getUnitNumber());

			this.setTxtUnitRentValue(contractUnitView.getUnitView().getRentValue() != null ? 
					                                 contractUnitView.getUnitView().getRentValue().toString() : "");

		}
		
		if (this.getNewTenantView() != null && this.getNewTenantView().getPersonId()!=null) 
		{
			newTenantView = this.getNewTenantView();
			this.setTxtNewTenantId(newTenantView.getPersonId().toString());
			String name = newTenantView.getPersonFullName();
			this.setTxtNewTenantName(name.equals("") || name == null ? newTenantView.getCompanyName(): name);
			this.setTxtNewTenantCellNumber(newTenantView.getCellNumber() != null ? newTenantView.getCellNumber(): "");
			this.setTxtNewTenantPassportNumber(newTenantView.getPassportNumber() != null ? newTenantView.getPassportNumber() : "");
			this.setTxtNewTenantDesignation(newTenantView.getDesignation() != null ? newTenantView.getDesignation(): "");
		}
		else if (this.getNewTenantViewBasicInfo()!=null)
		{
			newTenantViewBasicInfo = this.getNewTenantViewBasicInfo();
			this.setTxtNewTenantName(newTenantViewBasicInfo.getPersonFullName());
			this.setTxtNewTenantCellNumber(newTenantViewBasicInfo.getCellNumber() != null ? newTenantViewBasicInfo.getCellNumber(): "");
			this.setTxtNewTenantPassportNumber(newTenantViewBasicInfo.getPassportNumber() != null ? newTenantViewBasicInfo.getPassportNumber() : "");
		}
		populateApplicationDetailsTab();
		logger.logInfo(methodName + "|" + "Finish::");
	}

	@SuppressWarnings("unchecked")
	private void populateApplicationDetailsTab() {
		// PopulatingApplicationDetailsTab
		if (requestView != null) 
		{
			if (requestView.getRequestNumber() != null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER,requestView.getRequestNumber());
			if (requestView.getApplicantView() != null&& requestView.getApplicantView().getPersonId() != null) 
			{
				//populateApplicantDetails(requestView.getApplicantView());
				hdnApplicantId = requestView.getApplicantView().getPersonId().toString();
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnApplicantId);
				if(requestView.getApplicantView().getPersonId()!=null )
			       	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId());
						}
			if(requestView.getDescription()!=null )viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,requestView.getDescription());
			
			if (requestView.getRequestDate() != null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,requestView.getRequestDate());
			if (requestView.getStatusId() != null) 
			{
				if (getIsEnglishLocale()) 
				{
					if (requestView.getStatusEn() != null)
						viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,requestView.getStatusEn());
				} 
				else if (getIsArabicLocale()) {if (requestView.getStatusAr() != null)
						viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,requestView.getStatusAr());}
			} 
			else 
			{
				List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
				DomainDataView ddv = CommonUtil.getIdFromType(ddvList,WebConstants.REQUEST_STATUS_NEW);
				if (getIsEnglishLocale()) 
				{

					viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,ddv.getDataDescEn());
				} 
				else if (getIsArabicLocale()) 
				{

					viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,ddv.getDataDescAr());
				}
			}
			
			if(requestView !=null && requestView.getRequestId()!=null)
	    	     loadAttachmentsAndComments(requestView.getRequestId().toString());
	    	
		}
	}

	public String btnApprove_Click() 
	{
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
//		logger.logInfo(methodName + "|" + " Start...");

		try 
		{
			ApproveRejectTask(TaskOutcome.APPROVE);
			requestView = (RequestView) context.getViewRoot().getAttributes().get("REQUEST_VIEW");
//			logger.logDebug(methodName + "|" + "Save Attachments...Start");
			notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_ChangeTenant_Request_Approved);
            saveCommentsAttachments(MessageConstants.RequestEvents.REQUEST_APPROVED); 
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SUCCESS_APPROVED));
//			logger.logInfo(methodName + "|" + " Finish...");
		}
		catch (PIMSWorkListException ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			logger.logError(methodName + "|" + " Error Occured..." + ex);

		} catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logError("btnApprove_Click| Error Occured..." + ex);
		}
		return "";
	}

	public String btnReject_Click() {
		String methodName = "btnReject_Click";

		logger.logInfo(methodName + "|" + " Start...");
		try {
			// if(pageMode.equals("UPDATE"))
			{
				ApproveRejectTask(TaskOutcome.REJECT);
				if(viewRootMap.containsKey("REQUEST_VIEW"))
			 		   requestView = (RequestView)viewRootMap.get("REQUEST_VIEW");
				new PropertyService().setPaymentScheduleCanceled(requestView.getRequestId());
				saveCommentsAttachments(MessageConstants.RequestEvents.REQUEST_REJECTED);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SUCCESS_REJECTED));
				logger.logInfo(methodName + "|" + " Finish...");
			}
		} catch (PIMSWorkListException ex) {

			logger.logError(methodName + "|" + " Error Occured..." + ex);

		} catch (Exception ex) {
			logger.logError(methodName + "|" + " Error Occured::" + ex);
		}

		return "";
	}

	private void ApproveRejectTask(TaskOutcome taskOutCome)
			throws PIMSWorkListException, Exception {
		String methodName = "ApproveRejectTask";
		logger.logInfo(methodName + "|" + " Start...");
		try {
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");
//			UserTask userTask = (UserTask) sessionMap.get(WebConstants.CHANGE_TENANT_USER_TASK_LIST);
			UserTask userTask = (UserTask) viewMap.get(WebConstants.CHANGE_TENANT_USER_TASK_LIST);
//			sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String loggedInUser = getLoggedInUser();
			logger.logInfo(methodName + "|" + " TaskOutCome..." + taskOutCome);
			logger.logInfo(methodName + "|" + " loggedInUser..."+ loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			context.getViewRoot().getAttributes().put("pageMode",PAGE_MODE_UPDATE_APPROVE);
			logger.logInfo(methodName + "|" + " Finish...");
		} catch (PIMSWorkListException ex) {

			logger.logError(methodName + "|" + " Error Occured..." + ex);
			throw ex;
		} catch (Exception ex) {
			logger.logError(methodName + "|" + " Error Occured..." + ex);
			throw ex;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		
		sessionMap = context.getExternalContext().getSessionMap();
		viewRootMap = context.getViewRoot().getAttributes();
		if (context.getViewRoot().getAttributes().get("newtenantsid") != null)
			txtNewTenantId = context.getViewRoot().getAttributes().get("newtenantsid").toString();
		if (context.getViewRoot().getAttributes().get("newtenantsName") != null)
			txtNewTenantName = context.getViewRoot().getAttributes().get("newtenantsName").toString();
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
		getDomainDataConstants();
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);

		try 
		{
			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
			if (!isPostBack())
				initForNotPostBack();
			if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
         		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
            )
            {
         	   viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, 
         			           sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
         	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
            } 
			if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)&& sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) != null
					&& (Boolean) sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)) 
			{
				//contractView = (ContractView) viewRootMap.get("CONTRACTVIEWUPDATEMODE");
				sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
				CollectPayments();
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException(methodName + "|" + "Exception Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void initForNotPostBack() throws PimsBusinessException, Exception 
	{
		removeSessions();
		pageMode = PAGE_MODE_ADD;
		loadAttachmentsAndComments(null); 
		getPersonTypeDomainData();
		if (getRequestParam(WebConstants.BACK_SCREEN) != null && getRequestParam(WebConstants.BACK_SCREEN).toString().trim().length() > 0)
			viewRootMap.put(WebConstants.BACK_SCREEN,getRequestParam(WebConstants.BACK_SCREEN));
		contractView = (ContractView) getRequestParam(WebConstants.Contract.CONTRACT_VIEW);
		if (contractView != null) 
		{
			getContract();
			getPaymentScheduleFromPaymentConfig( );
			putViewValuesInControl();
		}
		else 
		{
			if (sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) 
				getDataFromTaskList();
			else if (FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW) != null) 
				getRequest();
			getChangeTenantRequest(txtRequestId);
			loadAttachmentsAndComments(txtRequestId);
		}
        viewRootMap.put("pageMode", pageMode);
	}

	@SuppressWarnings("unchecked")
	private void getRequest() 
	{
		requestView = (RequestView) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
		if (requestView != null && requestView.getRequestId() != null)
		{
			txtRequestId = requestView.getRequestId().toString();
			viewRootMap.put("REQUEST_VIEW",requestView);
		}
		
		List<DomainDataView> requestStatusList =CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView ddvNew= CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_NEW);
		DomainDataView ddvApprove = CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_APPROVED);
		DomainDataView ddvComplete = CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_APPROVED_COLLECTED);
		DomainDataView ddvPrint= CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_COMPLETE);
		DomainDataView ddvApprovalReq = CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);

		if (requestView.getStatusId().compareTo(ddvNew.getDomainDataId()) == 0)
			pageMode = PAGE_MODE_UPDATE;
		else if(requestView.getStatusId().compareTo(ddvApprovalReq.getDomainDataId()) == 0)
			pageMode = PAGE_MODE_APPROVE_REJECT;
		else if (requestView.getStatusId().compareTo(ddvApprove.getDomainDataId()) == 0)
			pageMode = PAGE_MODE_UPDATE_APPROVE;
		else if (requestView.getStatusId().compareTo(ddvComplete.getDomainDataId()) == 0)
			pageMode = PAGE_MODE_UPDATE_APPROVE_COLLECTED;
		else if (requestView.getStatusId().compareTo(ddvPrint.getDomainDataId()) == 0)
			pageMode = PAGE_MODE_PRINT;
		else
			pageMode = "PAGE_MODE_UPDATE";

		// pageMode=PAGE_MODE_UPDATE;
	}
	@SuppressWarnings("unchecked")
	private void getDataFromTaskList() 
	{
		UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if (userTask.getTaskAttributes().get("REQUEST_ID") != null)
			txtRequestId = userTask.getTaskAttributes().get("REQUEST_ID").toString();

		viewMap.put(WebConstants.CHANGE_TENANT_USER_TASK_LIST,userTask);
		sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if (userTask.getTaskAttributes().get("TASK_TYPE") != null && userTask.getTaskAttributes().get("TASK_TYPE").toString().equalsIgnoreCase(WebConstants.ChangeTenant_TaskType.APPROVE_CHANGE_TENANT_REQUEST))
			pageMode = PAGE_MODE_APPROVE_REJECT;
		else if (userTask.getTaskAttributes().get("TASK_TYPE") != null
				&& userTask.getTaskAttributes().get("TASK_TYPE").toString().equalsIgnoreCase(WebConstants.ChangeTenant_TaskType.COLLECT_CHANGE_TENANT_PAYMENT))
			pageMode = PAGE_MODE_UPDATE_APPROVE;
	}

	@SuppressWarnings("unchecked")
	private void getPaymentScheduleFromPaymentConfig() throws PimsBusinessException, Exception 
	{
		PropertyServiceAgent psa =new PropertyServiceAgent();
		Long ownerShipTypeId =new Long(viewRootMap.get(OWNER_SHIP_TYPE_ID).toString());
		HashMap feesConditionsMap=new HashMap();
		if(
			contractView.getContractTypeId().compareTo(WebConstants.COMMERCIAL_LEASE_CONTRACT_ID)== 0 
		  )
		{
			feesConditionsMap.put(WebConstants.Fees.CHANGE_TENANT_COMMERCIAL_FEE_ID, WebConstants.Fees.CHANGE_TENANT_COMMERCIAL_FEE);
			
		}
		else
		{
			feesConditionsMap.put(WebConstants.Fees.CHANGE_TENANT_RESIDENTIAL_FEE_ID, WebConstants.Fees.CHANGE_TENANT_RESIDENTIAL_FEE);
		}
		paymentSchedules = psa.getPaymentScheduleFromPaymentConfiguration(
																			WebConstants.PROCEDURE_TYPE_CHANGE_TENANT_NAME,
																			null, 
																			feesConditionsMap ,
																			null, 
																			getIsEnglishLocale(), 
																			ownerShipTypeId
				               											 );
		
		DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		List<PaymentScheduleView> newPsList = new ArrayList<PaymentScheduleView>(0);
		for (PaymentScheduleView psView : paymentSchedules) 
		{
			psView.setPaymentModeId(ddv.getDomainDataId());
			psView.setPaymentModeEn(ddv.getDataDescEn());
			psView.setPaymentModeAr(ddv.getDataDescAr());
			psView.setContractId(contractView.getContractId());
			newPsList.add(psView);
		}
		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPsList);
	}
    @SuppressWarnings("unchecked")
	private void getContract()throws PimsBusinessException 
	{
    	PropertyServiceAgent psa = new PropertyServiceAgent();
		ContractView searchContract = new ContractView();
		searchContract.setContractId(contractView.getContractId());
		HashMap contractHashMap = new HashMap();
		contractHashMap.put("contractView", searchContract);
		contractHashMap.put("dateFormat", CommonUtil.getDateFormat());
		List<ContractView> contractViewList = psa.getAllContracts(contractHashMap);
		if (contractViewList.size() > 0)
		{
			contractView = (ContractView) contractViewList.get(0);
		}
		Long ownerShipTypeId=null;
		if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
		{
		  Iterator iter=contractView.getContractUnitView().iterator();
		  while(iter.hasNext())
		  {
			  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
			  ownerShipTypeId =contractUnitView.getUnitView().getPropertyCategoryId();
		  } 
		}
		viewRootMap.put(OWNER_SHIP_TYPE_ID, ownerShipTypeId);
	}
   @SuppressWarnings("unchecked")
	private void getPersonTypeDomainData() 
   {
		List<DomainDataView> ddl = CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
		viewRootMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE, ddl);
		DomainDataView ddvCompany = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_COMPANY);
		viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY, ddvCompany);
		DomainDataView ddvIndividual = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_INDIVIDUAL);
		viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL,ddvIndividual);
	}
	@SuppressWarnings("unchecked")
	private void fillApplicantInfo() throws PimsBusinessException 
	{
		String methodName = "FillApplicantInfo";
		logger.logInfo(methodName + "| Start");
		PersonView applicantView;
		if (viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW)&& 
			viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW) != null) 
		{
			applicantView = (PersonView) viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
			if (hdnApplicantId != null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId)) != 0) 
			{
				PropertyServiceAgent psa = new PropertyServiceAgent();
				PersonView pv = new PersonView();
				pv.setPersonId(new Long(hdnApplicantId));
				List<PersonView> personsList = psa.getPersonInformation(pv);
				if (personsList.size() > 0) 
				{
					pv = personsList.get(0);
					populateApplicantDetails(pv);
					viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW, pv);
				}

			}
		} 
		else if (hdnApplicantId != null && hdnApplicantId.trim().length() > 0) 
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			PersonView pv = new PersonView();
			pv.setPersonId(new Long(hdnApplicantId));
			List<PersonView> personsList = psa.getPersonInformation(pv);
			if (personsList.size() > 0) 
			{
				pv = personsList.get(0);
				populateApplicantDetails(pv);
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
			}
		}

		logger.logInfo(methodName + "| Finish");
	}
	@SuppressWarnings("unchecked")
	private void populateApplicantDetails(PersonView pv) 
	{
		if (pv.getPersonFullName() != null && pv.getPersonFullName().trim().length() > 0)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
		else if (pv.getCompanyName() != null && pv.getCompanyName().trim().length() > 0)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
		String personType = getPersonType(pv);
		if (personType != null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
		if (pv.getCellNumber() != null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
		if (pv.getPersonId() != null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID, pv.getPersonId());
		
	}

	private String getPersonType(PersonView tenantView) 
	{

		String method = "getTenantType";
		logger.logDebug(method + "|" + "Start...");
		DomainDataView ddv;
		if (tenantView.getIsCompany().compareTo(new Long(1)) == 0) 
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
		else 
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
		if (getIsArabicLocale())
			return ddv.getDataDescAr();
		else if (getIsEnglishLocale())
			return ddv.getDataDescEn();

		logger.logDebug(method + "|" + "Finish...");
		return "";

	}
	@SuppressWarnings("unchecked")
	private void getDomainDataConstants() 
	{
		if (ddvList == null)
			ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);

		if (MODE_CASH_ID == null)
			MODE_CASH_ID = CommonUtil.getIdFromType(ddvList,WebConstants.PAYMENT_SCHEDULE_MODE_CASH).getDomainDataId();
	}

	@SuppressWarnings("unchecked")
	public String btnBack_Click() 
	{
		String methodName = "btnBack_Click";
		logger.logInfo(methodName + "|" + "Start..");

		String backScreen = context.getViewRoot().getAttributes().get(
				WebConstants.BACK_SCREEN) != null ? context.getViewRoot()
				.getAttributes().get(WebConstants.BACK_SCREEN).toString() : "";

		if (backScreen.equals(WebConstants.BACK_SCREEN_TASK_LIST))
			return "TASK_LIST";
		else if (backScreen.equals(WebConstants.BACK_SCREEN_CONTRACT_SEARCH))
			{
				sessionMap.put("preserveSearchCriteria", true);	
				return "CONTRACT_SEARCH";
			}
		else if (backScreen.equals(WebConstants.BACK_SCREEN_REQUEST_SEARCH))
			return "REQUEST_SEARCH";
		else if (backScreen.equals(WebConstants.BACK_SCREEN_LEASE_CONTRACT)) {
			sessionMap.put("contractId", txtContractId);
			return "LEASE_CONTRACT";
		}

		logger.logDebug(methodName + "|" + "BackScreen.." + backScreen);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	@SuppressWarnings("unchecked")
			public String btnPay_Click() 
			{
			String methodName = "btnPay_Click";
			logger.logInfo(methodName + "|" + "Start..");
			errorMessages = new ArrayList<String>(0);
			errorMessages.clear();
			successMessages = new ArrayList<String>(0);
			successMessages.clear();

			try {
				List<PaymentScheduleView> paymentList = new ArrayList<PaymentScheduleView>();
				if (context.getViewRoot().getAttributes().containsKey("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE")) 
				{
					paymentList.addAll((ArrayList) context.getViewRoot().getAttributes().get("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE"));
				}
				Double amount = new Double(0);
				logger.logInfo(methodName + "|Iterating paymentList to calculate Amount..");
//				for (PaymentScheduleView paymentScheduleView : paymentList) 
//				{
//					amount += paymentScheduleView.getAmount();
//				}
//				logger.logInfo(methodName + "|" + "Amount=" + amount);
//				PaymentScheduleView paymentScheduleViewRow = (PaymentScheduleView) paymentList.get(0);
				List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
				for (PaymentScheduleView paymentScheduleView : paymentList)
				{
					if (paymentScheduleView.getStatusId().compareTo(Constant.PAYMENT_SCHEDULE_PENDING_ID)== 0 )
					{
						paymentsForCollection.add(paymentScheduleView);
					}
				}
				sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentsForCollection );
				PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
//				paymentReceiptView.setAmount(amount);
//				paymentReceiptView.setModeId(paymentScheduleViewRow.getPaymentModeId());
//				paymentReceiptView.setPaymentScheduleView(paymentScheduleViewRow);
				sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW,paymentReceiptView);
				requestView = (RequestView) context.getViewRoot().getAttributes().get("REQUEST_VIEW");
				saveAttachments(requestView.getRequestId().toString());

				saveComments(requestView.getRequestId());
				openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");

			} catch (Exception ex) {
				logger.LogException(methodName + "|" + "Error Occured::", ex);
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

			}

			logger.logInfo(methodName + "|" + "Finish..");
			return "";
		}

	@SuppressWarnings("unchecked")
	private void CollectPayments() 
	{
		String methodName = "CollectPayments";
		logger.logInfo(methodName + "|Start");
		
		try 
		{
			if( sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW)!=null)
			{
			PaymentReceiptView prv = (PaymentReceiptView) sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			PropertyServiceAgent psa = new PropertyServiceAgent();
			if(txtNewTenantId !=null)
			prv.setPaidById(new Long(txtNewTenantId));
			btnComplete_Click();
			prv = psa.collectPayments(prv);
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			//changeRequestStatus(WebConstants.REQUEST_STATUS_APPROVED_COLLECTED);
			//pageMode = PAGE_MODE_UPDATE_APPROVE_COLLECTED;
			//viewRootMap.put("pageMode",PAGE_MODE_UPDATE_APPROVE_COLLECTED);
			
			CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.CHANGE_TENANT_NAME);
			}
			else
				btnComplete_Click();
			successMessages = new ArrayList<String>(0);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
			logger.logInfo(methodName + "|" + "Finish...");
		}
		catch (Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
			logger.logError(methodName + "|" + "Exception Occured..." + ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.CHANGE_TENANT_NAME);
    	logger.logInfo(methodName+"|"+" Finish...");
    }

	@SuppressWarnings("unchecked")
	public String btnComplete_Click() 
	{
		String methodName = "btnComplete_Click|";
		logger.logInfo(methodName + "Start..");
		try 
		{
			requestView = (RequestView) context.getViewRoot().getAttributes().get("REQUEST_VIEW");
			if (requestView.getContractView().getContractId() != null )
			{
				changeContractTenant(requestView.getContractView().getContractId(),WebConstants.REQUEST_STATUS_COMPLETE);
			    ApproveRejectTask(TaskOutcome.COMPLETE);
			    notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_ChangeTenant_Request_Completed);
			    saveCommentsAttachments(MessageConstants.RequestEvents.REQUEST_COMPLETED);
				successMessages = new ArrayList<String>(0);
			    successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ChangeTenantName.MSG_SUCCESS_COMPLETED));
			    viewRootMap.put("pageMode",PAGE_MODE_PRINT);
			}
			context.getViewRoot().getAttributes().put("pageMode","");
			logger.logInfo(methodName + "Finish..");
		} catch (Exception ex) 
		{
			errorMessages = new ArrayList<String>(0);
			errorMessages.clear();
			logger.LogException(methodName + "Error Occured..", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";

	}

	private void changeContractTenant(Long contractId, String requestStatus)throws PimsBusinessException,Exception 
	{
//		String methodName = "changeContractTenant";
//		logger.logInfo(methodName + "|" + "Start");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		if(txtNewTenantId != null)
		psa.changeContractTenant(contractId, new Long(txtNewTenantId), requestView.getRequestId(),WebConstants.REQUEST_STATUS_COMPLETE);
//		logger.logInfo(methodName + "|" + "Finish");

	}

	private void removeSessions() {
		String methodName = "removeSessions";
		logger.logInfo(methodName + "|" + "Start");
		if (sessionMap.containsKey(WebConstants.CHANGE_TENANT_USER_TASK_LIST))
			sessionMap.remove(WebConstants.CHANGE_TENANT_USER_TASK_LIST);
		if (sessionMap.containsKey("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE"))
			sessionMap.remove("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE");
		if (sessionMap.containsKey("SESSION_CHNG_TENANT_REQ_DOC"))
			sessionMap.remove("SESSION_CHNG_TENANT_REQ_DOC");
		logger.logInfo(methodName + "|" + "Finish");
	}
	@SuppressWarnings("unchecked")
	private void getChangeTenantRequest(String requestId) throws Exception {
		String methodName = "getChangeTenantRequest";
		logger.logInfo(methodName + "|" + "Start");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		HashMap map = psa.getChangeTenantRequest(new Long(txtRequestId));
		requestView = (RequestView) map.get("RequestView");

		contractView = (ContractView) map.get("ContractView");
		if( map.containsKey("TenantView") && ((PersonView) map.get("TenantView")).getPersonId()!=null)
		    this.setNewTenantView((PersonView) map.get("TenantView"));
		if( map.containsKey("TenantViewBasicInfo") )
		{
		    this.setNewTenantViewBasicInfo((PersonView) map.get("TenantViewBasicInfo"));
		    this.setBasicInfo(map.get("BasicInfo").toString());
		    
		}
		List<PaymentScheduleView> paymentScheduleList = (ArrayList) map.get("PaymentScheduleList");
		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleList);
		context.getViewRoot().getAttributes().put("REQUEST_VIEW", requestView);
		context.getViewRoot().getAttributes().put("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE", paymentScheduleList);

		// contract
		putViewValuesInControl();
		logger.logInfo(methodName + "|" + "Finish");
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(String requestId){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_CHANGE_TENANT_NAME);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_CHANGE_TENANT);
		viewMap.put("noteowner", WebConstants.REQUEST);
		if(requestId!= null && requestId.trim().length()>0)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, requestId);
			viewMap.put("entityId", requestId);
		}
	}
	public void tabAttachmentsComments_Click()
	{
		
		String methodName="tabAttachmentsComments_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
			if(requestView !=null && requestView.getRequestId()!=null)
	    	     loadAttachmentsAndComments(requestView.getRequestId().toString());
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
		  
	}


	public void saveComments(Long requestId) throws Exception {
		try {

			String notesOwner = WebConstants.REQUEST;
			NotesController.saveNotes(notesOwner, requestId);

		} catch (Exception exception) {
			logger.LogException( "saveComments|crashed ", exception);
			throw exception;
		}
	}
	@SuppressWarnings("unchecked")
	public void saveSystemComments(String sysNoteType, Long requestId)throws Exception {
		String methodName = "saveSystemComments|";
		try {
			logger.logInfo(methodName + "started...");

			String notesOwner = WebConstants.REQUEST;
			viewRootMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, requestId);
			logger.logInfo(methodName + "notesOwner..." + notesOwner);NotesController.saveSystemNotesForRequest(notesOwner, sysNoteType,
					requestId);

			logger.logInfo(methodName + "completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
	}
@SuppressWarnings("unchecked")
	public void saveAttachments(String requestId) {
		String methodName = "saveAtttachments";
		logger.logInfo(methodName + "|" + "Start..");

		try 
		{
			viewRootMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, requestId);
			CommonUtil.updateDocuments();
			
			logger.logInfo("|" + "Saving attachment..complete");
		} catch (Exception ex) {
			logger.logInfo("|" + "Error Occured.." + ex);
		}
		logger.logInfo("|" + "Finish..");
	}

	

	@Override
	@SuppressWarnings("unchecked")
	public void prerender() 
	{
		super.prerender();

		String methodName = "prerender";
		logger.logInfo(methodName + "|" + "Start...");
		try 
		{
			requestView = (RequestView) viewRootMap.get("REQUEST_VIEW");
			fillApplicantInfo();
			fillTenantInfo();
			enableDisableButtons();
			lnkAddNewTenant.setRendered(false);
			imgViewNewTenant.setRendered(false);
			if(this.getNewTenantView()!=null && this.getNewTenantView().getPersonId()!=null)
			{
				lnkAddNewTenant.setRendered(false);
				imgViewNewTenant.setRendered(true);
			}
			else if(this.getNewTenantViewBasicInfo()!=null)
			{
				lnkAddNewTenant.setRendered(true);
				imgViewNewTenant.setRendered(false);
			}
			

		}
		catch (Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
		}

		logger.logInfo(methodName + "|" + "Finish...");
	}
	@SuppressWarnings("unchecked")
	private void fillTenantInfo() throws Exception
	{
		if (txtNewTenantId != null && txtNewTenantId.length() > 0) 
		{
			if(sessionMap.containsKey(WebConstants.PERSON_BASIC_INFO)  && sessionMap.get(WebConstants.PERSON_BASIC_INFO)!=null )
			{
				this.setNewTenantView((PersonView)sessionMap.get(WebConstants.PERSON_BASIC_INFO));
				sessionMap.remove(WebConstants.PERSON_BASIC_INFO);
			}
			else
			{
				newTenantView = new PersonView();
				PropertyServiceAgent psa = new PropertyServiceAgent();
				newTenantView.setPersonId(new Long(txtNewTenantId));
				newTenantView = (psa.getPersonInformation(newTenantView)).get(0);
				//newTenantView.setPersonFullName(txtNewTenantName);
				this.setNewTenantView(newTenantView);
				
			}
			viewRootMap.put("newtenantsid",txtNewTenantId);
			viewRootMap.put("newtenantsName",txtNewTenantName);
		}
	}

	private void enableDisableApprovRejectButtons(boolean rendered) {
		btnApprove.setRendered(rendered);
		btnReject.setRendered(rendered);
	}
	public boolean getIsPageModeAdd()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_ADD))
			return true;
		return false;
	}
	public boolean getIsPageModeUpdate()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE))
			return true;
		return false;
	}
	public boolean getIsPageModeUpdateApprove()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE_APPROVE))
			return true;
		return false;
	}
	public boolean getIsPageModeUpdateApproveCollected()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE_APPROVE_COLLECTED))
			return true;
		return false;
	}
	public boolean getIsPageModeApproveReject()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_APPROVE_REJECT))
			return true;
		return false;
	}
	public boolean getIsPageModePrint()
	{
		if (context.getViewRoot().getAttributes().get("pageMode") != null && context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_PRINT))
			return true;
		return false;
	}
	@SuppressWarnings("unchecked")
	private void enableDisableButtons() {
		if (context.getViewRoot().getAttributes().get("pageMode") != null) 
		{
			btnSend.setRendered(false);
			viewRootMap.put("applicationDetailsReadonlyMode",true);
			if(txtNewTenantId!=null && txtNewTenantId.length()>0)
				imgViewNewTenant.setRendered(true);
			else
				imgViewNewTenant.setRendered(false);
			imgSearchNewTenant.setRendered(false);
		
			// if page is in add Mode than only save button should appear
			if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_ADD)) 
			{
				enableDisableApprovRejectButtons(false);
				btnSave.setRendered(true);
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(false);
				imgSearchNewTenant.setRendered(true);
				btnPrint.setRendered(false);
				btnSend.setRendered(true);
				imgSearchNewTenant.setRendered(true);
				viewRootMap.put("applicationDetailsReadonlyMode",false);
			}
			// if page is in update Mode than only save,send buttons should appear
			else if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE)) 
			{
				enableDisableApprovRejectButtons(false);
				btnSave.setRendered(true);
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(false);
				imgSearchNewTenant.setRendered(true);
				btnPrint.setRendered(false);
				btnSend.setRendered(true);
				
			}

			// if request Status is APPROVED than ONLY PAY BUTTON button should
			// appear
			else if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE_APPROVE)&& 
					 context.getViewRoot().getAttributes().get("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE") != null) 
			{
				
				if(!isPostBack())
					tabPanel.setSelectedTab("tabPayments");
				imgSearchNewTenant.setRendered(false);
				btnSave.setRendered(false);
				enableDisableApprovRejectButtons(false);
				
				btnCollectPayment.setRendered(true);
				btnComplete.setRendered(false);
				btnPrint.setRendered(false);

			}
			// if request Status is APPROVED_COLLECTED than ONLY COMPLETE BUTTON
			// button should appear

			else if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_UPDATE_APPROVE_COLLECTED)) 
			{
				imgSearchNewTenant.setRendered(false);
				btnSave.setRendered(false);
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(true);
				btnPrint.setRendered(false);
				enableDisableApprovRejectButtons(false);
				
				// TODO:TANVEER
				// btnCancel.setRendered(false);
			}
			// if page COMES FROM TASK LIST THAN APPROVE REJECT BUTTON SHOULD
			// APPEAR
			else if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_APPROVE_REJECT)) 
			{
				enableDisableApprovRejectButtons(true);
				imgSearchNewTenant.setRendered(false);
				btnSave.setRendered(false);
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(false);
				btnPrint.setRendered(false);
			}
			else if (context.getViewRoot().getAttributes().get("pageMode").toString().equals(PAGE_MODE_PRINT))
			{
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(false);
				imgSearchNewTenant.setRendered(false);
				btnSave.setRendered(false);
				btnPrint.setRendered(true);
				enableDisableApprovRejectButtons(false);
			}
			// if request Status is COMPLETED than NO BUTTON button 
			// appear
			else 
			{
				btnCollectPayment.setRendered(false);
				btnComplete.setRendered(false);
				imgSearchNewTenant.setRendered(false);
				btnSave.setRendered(false);
				btnPrint.setRendered(false);
				enableDisableApprovRejectButtons(false);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public String btnTenant_Click() {

		String methodName = "btnTenant_Click";
		logger.logInfo(methodName + "|" + "Start..");
		context.getExternalContext().getRequestMap().put("tenantId",txtTenantId);
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+txtTenantId+");";
				
		openPopUp("", javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}

		public String getErrorMessages() {
		
		return CommonUtil.getErrorMessages(errorMessages);

	}

	public String getSuccessMessages() {
		String messageList = "";
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : successMessages) {
				messageList = messageList + "<LI>" + message; // + "\n";
			}
		}
		return (messageList);

	}

	private void setRequestFieldDetailView(Set<RequestFieldDetailView> requestFieldDetailViewSet )throws PimsBusinessException 
	{
		RequestFieldDetailView requestFieldDetailView = new RequestFieldDetailView();
		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
		requestFieldDetailView.setRequestKeyValue(txtNewTenantId);
		RequestKeyView requestKeyView = getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_CHANGE_TENANT_NAME,WebConstants.REQUEST_KEY_NEW_TENANT_ID);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		requestFieldDetailViewSet.add(requestFieldDetailView);
		if(this.getBasicInfo()!=null && this.getBasicInfo().trim().length()>0)
		{
			requestFieldDetailView = new RequestFieldDetailView();
			requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
			requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
			requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
			requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
			requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
			requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
			requestFieldDetailView.setRequestKeyValue(this.getBasicInfo());
			requestKeyView = getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_CHANGE_TENANT_NAME,WebConstants.REQUEST_KEY_PERSON_BASIC_INFO);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
	}

	public void tabRequestHistory_Click() {
		String methodName = "tabRequestHistory_Click";
		logger.logInfo(methodName + "|" + "Start..");
		try
		{
			RequestHistoryController rhc = new RequestHistoryController();
			if (txtRequestId != null && txtRequestId.length() >= 0)
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,txtRequestId);
		}
		catch (Exception ex) 
		{
			logger.LogException(methodName + "|" + "Error Occured..", ex);
		}
		logger.logInfo(methodName + "|" + "Finish..");
	}

	private void setRequestView() throws PimsBusinessException
	{

		List<DomainDataView> ddvList;
		DomainDataView ddv;
		PersonView oldTenantView = new PersonView();

		oldTenantView.setPersonId(new Long(txtTenantId));

		ContractView contractView = new ContractView();
		contractView.setContractId(new Long(txtContractId));
		requestView.setContractId(new Long(txtContractId));
		requestView.setContractView(contractView);
		requestView.setTenantsView(oldTenantView);

		requestView.setCreatedBy(getLoggedInUser());
		requestView.setCreatedOn(new Date());
		requestView.setUpdatedBy(getLoggedInUser());
		requestView.setUpdatedOn(new Date());
		requestView.setRecordStatus(new Long(1));
		requestView.setIsDeleted(new Long(0));

		requestView.setRequestDate(new Date());
		requestView.setContractId(new Long(txtContractId));

		ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_SOURCE);
		ddv = CommonUtil.getIdFromType(ddvList,WebConstants.REQUEST_SOURCE_TENANT);
		requestView.setRequestSource(ddv.getDomainDataId());

		ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		ddv = CommonUtil.getIdFromType(ddvList,WebConstants.REQUEST_STATUS_ACCEPTED);
		requestView.setStatusId(ddv.getDomainDataId());

		ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_ORIGINATION);
		ddv = CommonUtil.getIdFromType(ddvList,WebConstants.REQUEST_ORIGINATION_CSC);
		requestView.setRequestOrigination(ddv.getDomainDataId());

		ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_PRIORITY);
		ddv = CommonUtil.getIdFromType(ddvList,WebConstants.REQUEST_PRIORITY_NORMAL);
		requestView.setRequestPriorityId(ddv.getDomainDataId());

		PersonView pv = new PersonView();
		if(hdnApplicantId!=null)
		   pv.setPersonId(new Long(hdnApplicantId));
		requestView.setApplicantView(pv);
		
		Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>(0);
		
		setRequestFieldDetailView(requestFieldDetailViewSet);
		
		
		requestView.setRequestFieldDetailsView(requestFieldDetailViewSet);
		
	}

	/**
	 * Returns all the a request key view associated with desired procedure Type
	 * and keyName
	 * 
	 * @return List<RequestKey>
	 * @throws PimsBusinessException
	 * @author Danish Faroosq
	 */
	public RequestKeyView getRequestKeysForProcedureKeyName(
			String procedureType, String keyName) throws PimsBusinessException {
		List<RequestKeyView> rkViewList = commonUtil
				.getRequestKeysForProcedureType(procedureType);
		for (RequestKeyView requestKeyView : rkViewList)
			if (requestKeyView.getKeyName().equals(keyName))
				return requestKeyView;

		return null;
	}

	public Date getCreatedOnDate() throws Exception 
	{
		SystemParameters parameters = SystemParameters.getInstance();

		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);

		DateFormat df = new SimpleDateFormat(dateFormat);
		if (createdOn != null && createdOn.length() > 0) 
			return df.parse(createdOn);
		 else 
			return new Date();
	}

	public String getCreatedByUser(String loggedInUser) {
		String methodName = "getCreatedByUser";

		if (createdBy != null && createdBy.length() > 0) {
			logger.logInfo(methodName + "|" + "createdBy.." + createdBy);
			return createdBy;
		} else {
			logger.logInfo(methodName + "|" + "createdBy.." + loggedInUser);
			return loggedInUser;
		}
	}
	@SuppressWarnings("unchecked")
	public String btnContract_Click() 
	{
		String methodName = "btnContract_Click";
		logger.logInfo(methodName + "|" + "Start..");
		context.getExternalContext().getSessionMap().put("contractId",txtContractId);
		context.getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,
				WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		String javaScriptText = "var screen_width = screen.width;"
				+ "var screen_height = screen.height;"
				+"window.open('LeaseContract.jsf?"
				+ WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW
				+ "="
				+ WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW
				+ "&"
				+ WebConstants.VIEW_MODE
				+ "="
				+ WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP
				+ "','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";
		openPopUp("LeaseContract.jsf", javaScriptText);
		logger.logInfo(methodName + "|" + "Finish..");
		return "";
	}
	@SuppressWarnings( "unchecked" )
    public Long getPaymentSchedulePendingStausId()
    {
    	DomainDataView ddv = new DomainDataView();
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	 ddv= CommonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_PENDING);
    	}
    	else
    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_PENDING) ;
    	return ddv.getDomainDataId();
    	
    }
	@SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleExemtedStausId()
    {
    	DomainDataView ddv = new DomainDataView();
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey( WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED ) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	 ddv= CommonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
    	}
    	else
    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED) ;
    	return ddv.getDomainDataId();
    	
    }
	@SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleRealizedId()
    {
    	DomainDataView ddv= new DomainDataView();
    	
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey( WebConstants.REALIZED) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	ddv= CommonUtil.getIdFromType(ddvList, WebConstants.REALIZED);
    	}
    	else
    		ddv= (DomainDataView)viewRootMap.get(WebConstants.REALIZED) ;
    	
    	return ddv.getDomainDataId();
    	
    }
	@SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleCollectedId()
    {
    	DomainDataView ddv= new DomainDataView();
    	
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey( WebConstants.PAYMENT_SCHEDULE_COLLECTED ) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	ddv= CommonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
    	}
    	else
    		ddv= (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_COLLECTED) ;
    	
    	return ddv.getDomainDataId();
    	
    }
	@SuppressWarnings( "unchecked" )
	private void loadPaymentStatus()
	{
	  String methodName="loadPaymentStatus";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
		  {
			  List<DomainDataView>ddv= CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS); 
		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, ddv);
		  }
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}
	@SuppressWarnings("unchecked")
	public void btnEditPaymentSchedule_Click()
    {
    	String methodName = "btnEditPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) && viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
    	if( viewRootMap.get("pageMode").toString().equals(PAGE_MODE_APPROVE_REJECT) || 
    		viewRootMap.get("pageMode").toString().equals(PAGE_MODE_UPDATE_APPROVE)
    	  )
    		sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
    	openPopUp("", "javascript:openPopupEditPaymentSchedule();");
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	@SuppressWarnings("unchecked")
	public String openReceivePaymentsPopUp()
	{
		String methodName = "openReceivePaymentsPopUp";
		logger.logInfo(methodName + "|" + "Start..");

		// For PaymentSchedules/////
		
		List<PaymentScheduleView> paymentSchedules = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
		for (PaymentScheduleView paymentScheduleView : paymentSchedules)
		{
			if (paymentScheduleView.getStatusId().compareTo(Constant.PAYMENT_SCHEDULE_PENDING_ID)== 0 )
			{
				paymentsForCollection.add(paymentScheduleView);
			}
		}
		
		if( paymentsForCollection.size()>0 )
		{
			viewRootMap.put("payments",paymentsForCollection );
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentsForCollection );
			sessionMap.put("contractId", txtContractId);
			String extrajavaScriptText = "var screen_height = 450;var screen_width = 850;"
					+ "window.open('receivePayment.jsf','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=100,top=200,scrollbars=no,status=no');";

			PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
			sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
			openPopUp("receivePayment.jsf", extrajavaScriptText);
		}
		else
		{
			btnComplete_Click();
		}
		logger.logInfo(methodName + "|" + "Finish..");

		return "";
	}

	public String getTxtunitRefNum() {
		if (viewRootMap.containsKey("txtunitRefNum")
				&& viewRootMap.get("txtunitRefNum") != null)
			txtunitRefNum = viewRootMap.get("txtunitRefNum").toString();
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
		if (this.txtunitRefNum != null)
			viewRootMap.put("txtunitRefNum", this.txtunitRefNum);
	}

	public String getTxtcontractNum() {
		if (viewRootMap.containsKey("txtcontractNum")
				&& viewRootMap.get("txtcontractNum") != null)
			txtcontractNum = viewRootMap.get("txtcontractNum").toString();
		return txtcontractNum;
	}
	@SuppressWarnings("unchecked")
	public void setTxtcontractNum(String txtcontractNum) {
		this.txtcontractNum = txtcontractNum;
		if (this.txtcontractNum != null)
			viewRootMap.put("txtcontractNum", this.txtcontractNum);
	}

	public String getTxtStatus() {
		if (viewRootMap.containsKey("txtStatus")
				&& viewRootMap.get("txtStatus") != null)
			txtStatus = viewRootMap.get("txtStatus").toString();
		return txtStatus;
	}
	@SuppressWarnings("unchecked")
	public void setTxtStatus(String txtStatus) {
		this.txtStatus = txtStatus;
		if (this.txtStatus != null)
			viewRootMap.put("txtStatus", this.txtStatus);
	}

	public String getTxtStartDate() {
		if (viewRootMap.containsKey("txtStartDate")
				&& viewRootMap.get("txtStartDate") != null)
			txtStartDate = viewRootMap.get("txtStartDate").toString();
		return txtStartDate;
	}
	@SuppressWarnings("unchecked")
	public void setTxtStartDate(String txtStartDate) {
		this.txtStartDate = txtStartDate;
		if (this.txtStartDate != null)
			viewRootMap.put("txtStartDate", this.txtStartDate);
	}

	public String getTxtEndDate() {
		if (viewRootMap.containsKey("txtEndDate")
				&& viewRootMap.get("txtEndDate") != null)
			txtEndDate = viewRootMap.get("txtEndDate").toString();
		return txtEndDate;
	}
	@SuppressWarnings("unchecked")
	public void setTxtEndDate(String txtEndDate) {
		this.txtEndDate = txtEndDate;
		if (this.txtEndDate != null)
			viewRootMap.put("txtEndDate", this.txtEndDate);
	}

	public String getTxtTenantNumber() {
		if (viewRootMap.containsKey("txtTenantNumber")
				&& viewRootMap.get("txtTenantNumber") != null)
			txtTenantNumber = viewRootMap.get("txtTenantNumber").toString();
		return txtTenantNumber;
	}
	@SuppressWarnings("unchecked")
	public void setTxtTenantNumber(String txtTenantNumber) {
		this.txtTenantNumber = txtTenantNumber;
		if (this.txtTenantNumber != null)
			viewRootMap.put("txtTenantNumber", this.txtTenantNumber);
	}

	public String getTxtTenantName() {
		if (viewRootMap.containsKey("txtTenantName")
				&& viewRootMap.get("txtTenantName") != null)
			txtTenantName = viewRootMap.get("txtTenantName").toString();
		return txtTenantName;
	}
	@SuppressWarnings("unchecked")
	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
		if (this.txtTenantName != null)
			viewRootMap.put("txtTenantName", this.txtTenantName);
	}

	public String getTxtContractType() {
		if (viewRootMap.containsKey("txtContractType")
				&& viewRootMap.get("txtContractType") != null)
			txtContractType = viewRootMap.get("txtContractType").toString();
		return txtContractType;
	}
	@SuppressWarnings("unchecked")
	public void setTxtContractType(String txtContractType) {
		this.txtContractType = txtContractType;
		if (this.txtContractType != null)
			viewRootMap.put("txtContractType", this.txtContractType);
	}

	public String getTxtTotalContractValue() {
		if (viewRootMap.containsKey("txtTotalContractValue")
				&& viewRootMap.get("txtTotalContractValue") != null)
			txtTotalContractValue = viewRootMap.get("txtTotalContractValue")
					.toString();
		return txtTotalContractValue;
	}
	@SuppressWarnings("unchecked")
	public void setTxtTotalContractValue(String txtTotalContractValue) {
		this.txtTotalContractValue = txtTotalContractValue;
		if (this.txtTotalContractValue != null)
			viewRootMap
					.put("txtTotalContractValue", this.txtTotalContractValue);
	}

	public String getTxtUnitRentValue() {
		if (viewRootMap.containsKey("txtUnitRentValue")
				&& viewRootMap.get("txtUnitRentValue") != null)
			txtUnitRentValue = viewRootMap.get("txtUnitRentValue").toString();
		return txtUnitRentValue;
	}
	@SuppressWarnings("unchecked")
	public void setTxtUnitRentValue(String txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
		if (this.txtUnitRentValue != null)
			viewRootMap.put("txtUnitRentValue", this.txtUnitRentValue);
	}

	public String getTxtpropertyName() {
		if (viewRootMap.containsKey("txtpropertyName")
				&& viewRootMap.get("txtpropertyName") != null)
			txtpropertyName = viewRootMap.get("txtpropertyName").toString();
		return txtpropertyName;
	}
	@SuppressWarnings("unchecked")
	public void setTxtpropertyName(String txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
		if (this.txtpropertyName != null)
			viewRootMap.put("txtpropertyName", this.txtpropertyName);
	}

	public String getTxtUnitType() {
		if (viewRootMap.containsKey("txtUnitType")
				&& viewRootMap.get("txtUnitType") != null)
			txtUnitType = viewRootMap.get("txtUnitType").toString();
		return txtUnitType;
	}

	public void setTxtUnitType(String txtUnitType) {
		this.txtUnitType = txtUnitType;
		if (this.txtUnitType != null)
			viewRootMap.put("txtUnitType", this.txtUnitType);
	}

	public String getTxtNewTenantName() {
		if (viewRootMap.containsKey("txtNewTenantName")
				&& viewRootMap.get("txtNewTenantName") != null)
			txtNewTenantName = viewRootMap.get("txtNewTenantName").toString();
		return txtNewTenantName;
	}

	public void setTxtNewTenantName(String txtNewTenantName) {
		this.txtNewTenantName = txtNewTenantName;
		if (this.txtNewTenantName != null)
			viewRootMap.put("txtNewTenantName", this.txtNewTenantName);
	}

	public String getTxtNewTenantId() {
		if (viewRootMap.containsKey("txtNewTenantId")
				&& viewRootMap.get("txtNewTenantId") != null)
			txtNewTenantId = viewRootMap.get("txtNewTenantId").toString();
		return txtNewTenantId;
	}
	@SuppressWarnings("unchecked")
	public void setTxtNewTenantId(String txtNewTenantId) {
		this.txtNewTenantId = txtNewTenantId;
		if (this.txtNewTenantId != null)
			viewRootMap.put("txtNewTenantId", this.txtNewTenantId);
	}

	public String getTxtNewTenantNumber() {
		if (viewRootMap.containsKey("txtNewTenantNumber")
				&& viewRootMap.get("txtNewTenantNumber") != null)
			txtNewTenantNumber = viewRootMap.get("txtNewTenantNumber")
					.toString();
		return txtNewTenantNumber;
	}
	@SuppressWarnings("unchecked")
	public void setTxtNewTenantNumber(String txtNewTenantNumber) {
		this.txtNewTenantNumber = txtNewTenantNumber;
		if (this.txtNewTenantNumber != null)
			viewRootMap.put("txtNewTenantNumber", this.txtNewTenantNumber);
	}

	public String getTxtTenantId() {
		if (viewRootMap.containsKey("txtTenantId")
				&& viewRootMap.get("txtTenantId") != null)
			txtTenantId = viewRootMap.get("txtTenantId").toString();
		return txtTenantId;
	}

	public void setTxtTenantId(String txtTenantId) {
		this.txtTenantId = txtTenantId;
		if (this.txtTenantId != null)
			viewRootMap.put("txtTenantId", this.txtTenantId);
	}

	public String getTxtContractId() {
		if (viewRootMap.containsKey("txtContractId")
				&& viewRootMap.get("txtContractId") != null)
			txtContractId = viewRootMap.get("txtContractId").toString();
		return txtContractId;
	}

	public void setTxtContractId(String txtContractId) {
		this.txtContractId = txtContractId;
		if (this.txtContractId != null)
			viewRootMap.put("txtContractId", this.txtContractId);
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public HtmlDataTable getTbl_PaymentSchedule() {
		return tbl_PaymentSchedule;
	}

	public void setTbl_PaymentSchedule(HtmlDataTable tbl_PaymentSchedule) {
		this.tbl_PaymentSchedule = tbl_PaymentSchedule;
	}

	public PaymentScheduleView getPsDataItem() {
		return psDataItem;
	}

	public void setPsDataItem(PaymentScheduleView psDataItem) {
		this.psDataItem = psDataItem;
	}

	public List<PaymentScheduleView> getPsDataList() {
		if (context.getViewRoot().getAttributes().containsKey(
				"CHANGE_TENANT_NAME_PAYMENT_SCHEDULE"))
			psDataList = (ArrayList<PaymentScheduleView>) context.getViewRoot()
					.getAttributes().get("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE");
		return psDataList;
	}

	public void setPsDataList(List<PaymentScheduleView> psDataList) {
		this.psDataList = psDataList;
	}

	public String getTxtRequestId() {
		return txtRequestId;
	}

	public void setTxtRequestId(String txtRequestId) {
		this.txtRequestId = txtRequestId;
	}

	public HtmlDataTable getTbl_RequiredDocs() {
		return tbl_RequiredDocs;
	}

	public void setTbl_RequiredDocs(HtmlDataTable tbl_RequiredDocs) {
		this.tbl_RequiredDocs = tbl_RequiredDocs;
	}

	public RequiredDocumentsView getRdDataItem() {
		return rdDataItem;
	}

	public void setRdDataItem(RequiredDocumentsView rdDataItem) {
		this.rdDataItem = rdDataItem;
	}

	public List<RequiredDocumentsView> getRdDataList() {
		if (sessionMap.containsKey("SESSION_CHNG_TENANT_REQ_DOC"))
			rdDataList = (ArrayList<RequiredDocumentsView>) sessionMap
					.get("SESSION_CHNG_TENANT_REQ_DOC");

		return rdDataList;
	}

	public void setRdDataList(List<RequiredDocumentsView> rdDataList) {
		this.rdDataList = rdDataList;
	}

	public String getPageMode() {
		pageMode = context.getViewRoot().getAttributes().get("pageMode")
				.toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnPay() {
		return btnPay;
	}

	public void setBtnPay(HtmlCommandButton btnPay) {
		this.btnPay = btnPay;
	}

	

	public String getTxtpropertyType() {
		if (viewRootMap.containsKey("txtpropertyType")
				&& viewRootMap.get("txtpropertyType") != null)
			txtpropertyType = viewRootMap.get("txtpropertyType").toString();
		return txtpropertyType;
	}
	@SuppressWarnings( "unchecked" )
	public void setTxtpropertyType(String txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
		if (this.txtpropertyType != null)
			viewRootMap.put("txtpropertyType", this.txtpropertyType);
	}
@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentSchedules() 
	{
		if (viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) != null)
			paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		return paymentSchedules;
	}

	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(
			HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	public String getDateFormatForDataTable() {
		return dateFormatForDataTable;
	}

	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}

	public HtmlCommandButton getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(HtmlCommandButton requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getHdnApplicantId() {
		return hdnApplicantId;
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}

	public String getTxtNewTenantCellNumber() {
		if (viewRootMap.containsKey("txtNewTenantCellNumber")
				&& viewRootMap.get("txtNewTenantCellNumber") != null)
			txtNewTenantCellNumber = viewRootMap.get("txtNewTenantCellNumber")
					.toString();

		return txtNewTenantCellNumber;
	}

	public void setTxtNewTenantCellNumber(String txtNewTenantCellNumber) {
		this.txtNewTenantCellNumber = txtNewTenantCellNumber;

		if (txtNewTenantCellNumber != null)
			viewRootMap.put("txtNewTenantCellNumber", txtNewTenantCellNumber);
	}

	public String getTxtNewTenantPassportNumber() {
		if (viewRootMap.containsKey("txtNewTenantPassportNumber")
				&& viewRootMap.get("txtNewTenantPassportNumber") != null)
			txtNewTenantPassportNumber = viewRootMap.get(
					"txtNewTenantPassportNumber").toString();

		return txtNewTenantPassportNumber;
	}

	public void setTxtNewTenantPassportNumber(String txtNewTenantPassportNumber) {
		this.txtNewTenantPassportNumber = txtNewTenantPassportNumber;

		if (txtNewTenantPassportNumber != null)
			viewRootMap.put("txtNewTenantPassportNumber",
					txtNewTenantPassportNumber);
	}

	public String getTxtNewTenantDesignation() {
		if (viewRootMap.containsKey("txtNewTenantDesignation")
				&& viewRootMap.get("txtNewTenantDesignation") != null)
			txtNewTenantDesignation = viewRootMap.get(
					"txtNewTenantDesignation").toString();

		return txtNewTenantDesignation;
	}

	public void setTxtNewTenantDesignation(String txtNewTenantDesignation) {
		this.txtNewTenantDesignation = txtNewTenantDesignation;

		if (txtNewTenantDesignation != null)
			viewRootMap.put("txtNewTenantDesignation",
					txtNewTenantDesignation);
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlGraphicImage getImgViewNewTenant() {
		return imgViewNewTenant;
	}

	public void setImgViewNewTenant(HtmlGraphicImage imgViewNewTenant) {
		this.imgViewNewTenant = imgViewNewTenant;
	}

	public HtmlGraphicImage getImgSearchNewTenant() {
		return imgSearchNewTenant;
	}

	public void setImgSearchNewTenant(HtmlGraphicImage imgSearchNewTenant) {
		this.imgSearchNewTenant = imgSearchNewTenant;
	}

	public HtmlCommandButton getBtnSend() {
		return btnSend;
	}

	public void setBtnSend(HtmlCommandButton btnSend) {
		this.btnSend = btnSend;
	}

	public org.richfaces.component.html.HtmlTab getTabAttachments() {
		return tabAttachments;
	}

	public void setTabAttachments(
			org.richfaces.component.html.HtmlTab tabAttachments) {
		this.tabAttachments = tabAttachments;
	}

	public org.richfaces.component.html.HtmlTab getTabPayments() {
		return tabPayments;
	}

	public void setTabPayments(org.richfaces.component.html.HtmlTab tabPayments) {
		this.tabPayments = tabPayments;
	}

	public org.richfaces.component.html.HtmlTab getTabNewTenant() {
		return tabNewTenant;
	}

	public void setTabNewTenant(org.richfaces.component.html.HtmlTab tabNewTenant) {
		this.tabNewTenant = tabNewTenant;
	}

	public org.richfaces.component.html.HtmlTab getTabContractDetails() {
		return tabContractDetails;
	}

	public void setTabContractDetails(
			org.richfaces.component.html.HtmlTab tabContractDetails) {
		this.tabContractDetails = tabContractDetails;
	}

	public org.richfaces.component.html.HtmlTab getTabApplicantDetails() {
		return tabApplicantDetails;
	}

	public void setTabApplicantDetails(
			org.richfaces.component.html.HtmlTab tabApplicantDetails) {
		this.tabApplicantDetails = tabApplicantDetails;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		RequestView reqView = null;
		ContractView contView = null;
		PersonView applicant = null;
		PersonView currentTenant = null;
		PersonView newTenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		
		requestView = (RequestView) context.getViewRoot().getAttributes().get("REQUEST_VIEW");
		
		try
		{
		
		if(requestView != null)
		{
			contView = requestView.getContractView();
			if(contView != null)
			{
				if(viewRootMap.containsKey("CURRENT_TENANT_VIEW"))
					currentTenant = (PersonView)viewRootMap.get("CURRENT_TENANT_VIEW");
				
				if(currentTenant == null || currentTenant.getPersonId() == null)
					currentTenant = psAgent.getPersonInformation(new Long(this.getTxtTenantId()));
				
				newTenant = psAgent.getPersonInformation(Long.parseLong(this.getTxtNewTenantId()));
				applicant = requestView.getApplicantView();
				
							
				eventAttributesValueMap.put("REQUEST_NUMBER", requestView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", this.getTxtcontractNum());
				eventAttributesValueMap.put("OLD_TENANT", currentTenant.getPersonFullName());
				eventAttributesValueMap.put("NEW_TENANT", newTenant.getPersonFullName());
				eventAttributesValueMap.put("PROPERTY_NAME", this.getTxtpropertyName());
				eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(currentTenant != null && applicant.getPersonId().compareTo(currentTenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(currentTenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", currentTenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				if(newTenant != null && applicant.getPersonId().compareTo(newTenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(newTenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", newTenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}

				
			}
		}
		}
		catch (Exception e)
		{
			logger.LogException("requestStatusNotification crashed...",e);
		
		}
	}

	public PersonView getNewTenantViewBasicInfo() {
		if(viewRootMap.containsKey("newTenantViewBasicInfo"))
			newTenantViewBasicInfo= (PersonView)viewRootMap.get("newTenantViewBasicInfo");
		
		return newTenantViewBasicInfo;
	}

	public void setNewTenantViewBasicInfo(PersonView newTenantViewBasicInfo) {
		this.newTenantViewBasicInfo = newTenantViewBasicInfo;
		if(this.newTenantViewBasicInfo!=null )
			viewRootMap.put("newTenantViewBasicInfo", this.newTenantViewBasicInfo);
	}

	public PersonView getNewTenantView() {
		if(viewRootMap.containsKey("newTenantView"))
			newTenantView = (PersonView)viewRootMap.get("newTenantView");
		return newTenantView;
	}

	public void setNewTenantView(PersonView newTenantView) {
		
		this.newTenantView = newTenantView;
		if(this.newTenantView!=null )
			viewRootMap.put("newTenantView", this.newTenantView);
	}

	public String getBasicInfo() {
		if(viewRootMap.containsKey("basicInfo"))
			basicInfo = viewRootMap.get("basicInfo").toString();
		return basicInfo;
	}

	public void setBasicInfo(String basicInfo) {
		this.basicInfo = basicInfo;
		if(this.basicInfo!=null && this.basicInfo.trim().length()>0)
		viewRootMap.put("basicInfo",basicInfo);
	}


	public HtmlCommandLink getLnkAddNewTenant() {
		return lnkAddNewTenant;
	}

	public void setLnkAddNewTenant(HtmlCommandLink lnkAddNewTenant) {
		this.lnkAddNewTenant = lnkAddNewTenant;
	}
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click() {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }

}