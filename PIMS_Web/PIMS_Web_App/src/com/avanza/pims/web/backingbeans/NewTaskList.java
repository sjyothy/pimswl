package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.richfaces.component.html.HtmlColumn;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Request;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.ws.services.TaskServices;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.TaskListSearchCriteria;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.ui.util.ResourceUtil;

public class NewTaskList extends AbstractSearchBean{
	private HtmlDataTable dataTable;
	private HtmlColumn htmlColumnsAssignedDate = new HtmlColumn();
	private String sortField = null;
    private boolean sortAscending = true;
	private HtmlCommandLink btnAction = new HtmlCommandLink();
	private HtmlInputHidden addCount;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
//	private UserTask userTask = new UserTask();
//	private List<UserTask> userTasks = new ArrayList<UserTask>();
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String dateFormat = "";
	
	private Date assignedDateFrom;
	private Date assignedDateTo;
	private String applicationTitle;
	private String taskTitle;	
	private String hdnTenantId;
	private String hdnApplicantId;
	private String TENANT_INFO="TENANTINFO";
	
	private String contractNumber;
	private String contractUnitNumber;	
	private String contractTenantName;
	private String requestApplicant;
	private String requestNumber;	
	private String requestCreatedBy;
	private String inheritanceFileNumber;
	private String endowmentFileNumber;
	private static final String IS_CUSTOMER_SERVICE_RELATED_USER="IS_CUSTOMER_SERVICE_RELATED_USER";
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();	
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	private Map<Long,String> mapFile = new HashMap<Long, String>();
	private Map<Long,String> mapSocialResearch = new HashMap<Long, String>();
	private Map<Long,String> mapSocialResearchTaskDesc = new HashMap<Long, String>();
	private static Logger logger = Logger.getLogger( TaskList.class );
        
        
        private TaskListSearchCriteria criteria = new TaskListSearchCriteria();
        private List<TaskListVO> dataList = new ArrayList<TaskListVO>();
        
        private TaskServices services = new TaskServices();
        
        
    private final String DEFAULT_SORT_FIELD = "taskNumber";

	// Actions -----------------------------------------------------------------------------------
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
//	public boolean getIsSentToUserContainsLoggedInUser()
//	{
//		UserTask userTask  = (UserTask )dataTable.getRowData();
//		return userTask.getSentToUser().contains(getLoggedInUserId());
//		
//	}
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
//	public List<UserTask> getUserTasks() {
//		if (userTasks == null) {
//			loadDataList();
//		}
//		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//		userTasks = (List<UserTask>)sessionMap.get("userTasks");
//		return userTasks;
//	}		
	
	/** public String loadDataList() 
	{
		
		BPMWorklistClient bpmWorkListClient = null;
		boolean criteriaProvided=false;
		try {
			
	        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	        boolean isEnLocal = getIsEnglishLocale();     
	        logger.logInfo("Contextpath is:"+contextPath);
	        
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			
			logger.logInfo("Got BPMWorklistClient instance ");
			
			String loggedInUser = getLoggedInUser();
			TaskSearchCriteria taskSearchCriteria = new TaskSearchCriteria();
			
			if(assignedDateFrom != null)
				taskSearchCriteria.setAssignedDateFrom(assignedDateFrom);	
			if(assignedDateTo != null)
				taskSearchCriteria.setAssignedDateTo(assignedDateTo);
			

			Set<UserTask> userTaskss = bpmWorkListClient.getFilteredUserTasks(loggedInUser,taskSearchCriteria);
			userTasks = new ArrayList<UserTask>(); 
			
			HashMap<String, User> userHashMap = new HashMap<String, User>();
			HashMap<String, UserTask> taskHistoryMap = new HashMap<String, UserTask>();
			TaskListResult taskListResult = null;
			
			if  (   
					(contractNumber !=null && !contractNumber.trim().equals("")) ||
					(requestNumber !=null && !requestNumber.trim().equals("")) ||
					(contractUnitNumber !=null && !contractUnitNumber.trim().equals("")) ||
					(contractTenantName !=null && !contractTenantName.trim().equals("")) ||
					(requestApplicant !=null && !requestApplicant.trim().equals("")) ||
					(requestCreatedBy !=null && !requestCreatedBy.equals("") && !requestCreatedBy.equals("-1")) ||
					(inheritanceFileNumber !=null && !inheritanceFileNumber.trim().equals("")) ||
					(endowmentFileNumber !=null && !endowmentFileNumber.trim().equals(""))
				)
				{
					TaskListSearchCriteria taskListCriteria = new TaskListSearchCriteria();
					if( contractNumber != null  && !contractNumber.trim().equals(""))
					{
					    taskListCriteria.setContractNumber(contractNumber.trim());
					}
					if (contractTenantName != null && contractTenantName.trim().length() > 0)
					{
						taskListCriteria.setContractTenantName(contractTenantName.trim());
					}
					if(contractUnitNumber != null && contractUnitNumber.trim().length() > 0)
					{
						taskListCriteria.setContractUnitNumber(contractUnitNumber.trim());
					}
					if(requestApplicant != null  &&  requestApplicant.trim().length() > 0)
					{
						taskListCriteria.setRequestApplicant(requestApplicant.trim());
					}
					
					taskListCriteria.setRequestCreatedBy(requestCreatedBy);
					if( requestNumber != null &&  requestNumber.trim().length() > 0)
					{
						taskListCriteria.setRequestNumber(requestNumber.trim());
					}
					if(inheritanceFileNumber != null &&  inheritanceFileNumber.trim().length() > 0)
					{
						taskListCriteria.setInheritanceFileNumber(inheritanceFileNumber.trim());
					}
					if(endowmentFileNumber != null &&  endowmentFileNumber.trim().length() > 0)
					{
						taskListCriteria.setEndowmentFileNumber( endowmentFileNumber.trim());
					}
					TaskServices taskServices = new TaskServices();
					
					taskListResult = taskServices.getTaskListResult(taskListCriteria);
					criteriaProvided = true;
				}
			// Filtering on the basis of Application Title and Task Title
			
//			if( (applicationTitle != null && !applicationTitle.toString().equals("")) || taskTitle != null && !taskTitle.toString().equals(""))
//			{
			   InheritanceFileService inheritanceFileService = new InheritanceFileService();
			   PropertyService propertyService = new PropertyService();
			   
				for(UserTask userTask: userTaskss)
				{
						boolean addTask = false;
					
						if( taskHistoryMap.containsKey( userTask.getTaskId() ) )
						{continue;}
						
						String taskApplicationTitleEn = userTask.getRequestTitle_en();
						String taskApplicationTitleAr = userTask.getRequestTitle_ar();
					
						String taskTaskTitleEn = userTask.getTaskTitle_en();
						String taskTaskTitleAr = userTask.getTaskTitle_ar();
					
						String userId = userTask.getSentByUser();					
						User user = null;
					
						if(userHashMap.containsKey(userId))
						{
							user = userHashMap.get(userId);
						}
						
						else
						{
							user = SecurityManager.getUser(userId);
							if(user != null)
							{
								userHashMap.put(userId, user);
							}					
						}
					
						Map<String,String> taskAttributes =  userTask.getTaskAttributes();
						TaskListContract taskListContract = null;
						
						if(taskAttributes.containsKey("CONTRACT_ID"))
						{
							Object contractId = taskAttributes.get("CONTRACT_ID");
							
							if(contractId != null && !contractId.equals(""))
							{
								Long cntId = Long.parseLong(contractId.toString()); 
								if( taskListResult != null && taskListResult.getContracts() !=null && 
								   !taskListResult.getContracts().containsKey(cntId))
								{
									addTask = false;
								}
								else if(taskListResult != null && taskListResult.getContracts() !=null && 
										taskListResult.getContracts().containsKey(cntId))
								{
								 	 taskListContract = taskListResult.getContracts().get(cntId);
								 	 
								}
								else
								{
									taskListContract =  propertyService.getContractByContractId(cntId);
								}
							}
						}
					
						if(taskAttributes.containsKey("REQUEST_ID"))
						{
							Object reqId = taskAttributes.get("REQUEST_ID");
							
							if(reqId != null && !reqId.equals(""))
							{
								Long requestId = Long.parseLong(reqId.toString());

								if(
										(
											 taskListResult != null && 
											 taskListResult.getRequests() !=null && 
											 !taskListResult.getRequests().contains(requestId)
										)||
										( endowmentFileNumber !=null && !endowmentFileNumber.trim().equals("") )
								  )
								{
									addTask = false;
								}
								else 
								{
									//Search in contract 
									taskListContract = propertyService.getContractByRequestId(requestId);
									
									if( taskListResult != null && 
										taskListResult.getContracts() !=null && 
									   !taskListResult.getContracts().containsKey(taskListContract.getContractId())
									   )
									
									{
										  addTask = false;
									}
									else
									{
										//if request does not belong to contract than search in inheritance file
										taskListContract =inheritanceFileService.getInheritanceFileByRequestId(requestId);
										if(  taskListResult != null && 
											 taskListResult.getInheritanceFiles()  !=null && 
											!taskListResult.getInheritanceFiles().containsKey(taskListContract.getContractId())
										   )
										{
										  addTask = false;
										}
										else
										{
											//if request id doest not belong to file then search FINALLY in request
											TaskListContract taskListContracts = propertyService.getRequestByRequestId( requestId );
											if(  taskListResult != null && 
												 taskListResult.getRequests()    !=null && 
													!taskListResult.getRequests().contains(taskListContracts.getContractId())
											  )
											{
												  addTask = false;
											}
											
											else 
											{
												addTask = true;
											}
											if( taskListContracts != null && 
											   ( taskListContract==null || taskListContract.getContractId() == null ||
												 taskListContract.getContractNumber()  == null || taskListContract.getContractNumber().length()==0  	   
											    ) 
											  )
											{
												taskListContract = taskListContracts;
											}
										}
									}
	
										
								}
							}
						}
						if( taskAttributes.containsKey("FILE_ID") )
						{
							long fileId = Long.valueOf( taskAttributes.get("FILE_ID").toString() );
							if(  taskListResult != null && 
									 taskListResult.getInheritanceFiles()  !=null && 
									!taskListResult.getInheritanceFiles().containsKey(fileId)
								   )
								{
								  addTask = false;
								}
								else
								{
									addTask = true;
								}
							
						}
						if(taskAttributes.containsKey("SOCIAL_RESEARCH_ID"))
						{
							long rsrchId = Long.valueOf( taskAttributes.get("SOCIAL_RESEARCH_ID").toString() );
							SocialResearch rsrch = EntityManager.getBroker().findById(SocialResearch.class, rsrchId);
							
							if(rsrch != null && rsrch.getInheritanceFile() != null)
							{
								mapFile.put(rsrchId,rsrch.getInheritanceFile().getFileNumber());
								String taskType =    	  taskAttributes.get( "TASK_TYPE" ).toString();
								Long caringTypeId = SocialResearchBean.taskTypeCaringTypeMapping(taskType);
								String desc = UtilityService.getRecommendationDescriptionForTask( rsrch ,caringTypeId );
								
								mapSocialResearchTaskDesc.put(rsrchId, desc);
							}
							//if search criteria provided does not match with the file id from social research then donot show the task
							if(  taskListResult != null && 
								 taskListResult.getInheritanceFiles()  != null && 
								!taskListResult.getInheritanceFiles().containsKey(rsrch.getInheritanceFile().getInheritanceFileId())
							  )
							{
							  addTask = false;
							}
							//if either no search criteria provided or ONLY inheritance file search criteria provided which also matches with social research
							// file id then show the task
							else if( !criteriaProvided  ||
								   
									 (
										( endowmentFileNumber ==null || endowmentFileNumber.trim().equals("") )&&										 
										inheritanceFileNumber !=null && !inheritanceFileNumber.trim().equals("")&&
										(contractNumber ==null || contractNumber.trim().equals("") )&&
										(requestNumber ==null || requestNumber.trim().equals("") )&&
										(contractUnitNumber ==null || contractUnitNumber.trim().equals("") )&&
										(contractTenantName ==null || contractTenantName.trim().equals("") )&&
										(requestApplicant ==null || requestApplicant.trim().equals("") )&&
										(requestCreatedBy ==null || requestCreatedBy.equals("") || requestCreatedBy.equals("-1")) 
									  )
								   )
							{
								addTask = true;
							}
						}
						if(taskAttributes.containsKey("ENDOWMENT_FILE_NUMBER"))
						{
							addTask = false;
							String endFileNumber = taskAttributes.get("ENDOWMENT_FILE_NUMBER").toString();
							if( 
									endowmentFileNumber != null && 
									endowmentFileNumber.trim().length()>0 && 
									endFileNumber.toLowerCase().contains( endowmentFileNumber.toLowerCase() )
							  )
							{
								addTask = true;
							}
						
						}
						//if no criteria provided then show all the tasks
	                    if(!addTask && !criteriaProvided )
	                    {
	                    	addTask = true;
	                    }
						if(taskListContract != null)
						{
							 userTask.setContractNumber(taskListContract.getContractNumber());
						 	 userTask.setContractTenantNameEn(taskListContract.getContractTenantName());
							 userTask.setContractTenantNameAr(taskListContract.getContractTenantName());
						}
						
					
						if(addTask)
						{						
						
							if(isEnLocal)
							{
								if(applicationTitle!=null && !applicationTitle.toString().equals(""))
								{
									if(taskApplicationTitleEn != null && !taskApplicationTitleEn.toString().equals(""))
										{
										if(!taskApplicationTitleEn.toLowerCase().contains(applicationTitle.trim().toLowerCase()))
											addTask = false;
										}
																	
								}
								if(taskTitle!=null && !taskTitle.toString().equals(""))
								{
									if(taskTaskTitleEn != null && !taskTaskTitleEn.toString().equals(""))
									{
										
										if(!taskTaskTitleEn.toLowerCase().contains(taskTitle.trim().toLowerCase()))
											addTask = false;
									}						
										
								}
								
								if(user != null)
									userTask.setSentByUser(user.getFullName());
								
							}
							else
							{
								if(applicationTitle!=null && !applicationTitle.toString().equals(""))
								{
									if(taskApplicationTitleAr != null && !taskApplicationTitleAr.toString().equals(""))
									{
										if(!taskApplicationTitleAr.toLowerCase().contains(applicationTitle.trim().toLowerCase()))
											addTask = false;
									}							
								}
								if(taskTitle!=null && !taskTitle.toString().equals(""))
								{
									if(taskTaskTitleAr != null && !taskTaskTitleAr.toString().equals(""))
									{
										if(!taskTaskTitleAr.toLowerCase().contains(taskTitle.trim().toLowerCase()))
											addTask = false;
									}							
								}
								
								if(user != null)
									userTask.setSentByUser(user.getSecondaryFullName());
	//							ContractView contract=new ContractView();
	//							contract=new PropertyService().getContractByRequestId(userTask);
							}
							if(addTask && taskAttributes.containsKey("REQUEST_ID"))
							{
								String requestId=taskAttributes.get("REQUEST_ID");
								String contractId="";
								HashMap< Long, TaskListContract> requestContractMap=new HashMap<Long, TaskListContract>();
								if(requestId != null && !requestId.equals(""))
								{
									if(taskAttributes.containsKey("CONTRACT_ID"))
											contractId = taskAttributes.get("CONTRACT_ID").toString();
									
									TaskListContract tasListContract= new TaskListContract();
									if(!requestContractMap.containsKey(Long.parseLong(requestId)))
										tasListContract =  propertyService.getContractByRequestId(Long.parseLong(requestId));
									else
										tasListContract =requestContractMap.get(Long.parseLong(requestId));
									if(tasListContract!=null && tasListContract.getContractNumber()!=null)
									{
										if(!requestContractMap.containsKey(Long.parseLong(requestId)))
										{
											requestContractMap.put(Long.parseLong(requestId), tasListContract);
										}
										userTask.setContractNumber(tasListContract.getContractNumber());
										userTask.setContractTenantNameEn(tasListContract.getContractTenantName());
										userTask.setContractTenantNameAr(tasListContract.getContractTenantName());
									
										if(!contractId.equals(""))
										{
											
											if(tasListContract.getContractId().compareTo(Long.parseLong(contractId))!=0)
											{
												TaskListContract contract = new TaskListContract();
												contract = propertyService.getContractByContractId(Long.parseLong(contractId));
												//In case of renew the conrtactId will have latest contract and tasListContract.getContractId() will have
												//old contract
												if( tasListContract.getContractId().compareTo(Long.parseLong(contractId)) < 0 )
												{
												userTask.setContractNumber( tasListContract.getContractNumber()+ " \\ " +  contract.getContractNumber());
												userTask.setContractTenantNameEn( tasListContract.getContractTenantName() );
												userTask.setContractTenantNameAr( tasListContract.getContractTenantName() );
												}
												//In case of transfer the conrtactId will have old contract and tasListContract.getContractId() will have
												//latest contract
												else if( tasListContract.getContractId().compareTo(Long.parseLong(contractId)) > 0 )
												{
												userTask.setContractNumber( contract.getContractNumber()+ " \\ " + tasListContract.getContractNumber());
												String concatName = contract.getContractTenantName()+" \\ " +tasListContract.getContractTenantName();
												userTask.setContractTenantNameEn( concatName );
												userTask.setContractTenantNameAr( concatName );
												}
											}	
										}
									}
								}
							}
							if(addTask && taskAttributes.containsKey("FILE_ID"))
							{
								long fileId = Long.valueOf( taskAttributes.get("FILE_ID").toString() );
								if( mapFile.containsKey( fileId ) )
								{
									userTask.setContractNumber(mapFile.get( fileId )); 
								}
								else
								{
									InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, fileId);
									mapFile.put(fileId,file.getFileNumber());
									userTask.setContractNumber(file.getFileNumber());
									file=null;
								}
								
							}
							if(addTask && taskAttributes.containsKey("SOCIAL_RESEARCH_ID"))
							{
								String requestTitle=    	  null ;
								if( taskAttributes.get( "REQUEST_TITLE" ) != null )
								{
									requestTitle = taskAttributes.get( "REQUEST_TITLE" ).toString();
								}
								String taskDesciption ="";
								//Social program
								if(requestTitle.equals("BPM.WorkList.MEMSSocialSolidatoryBPEL.RequestTitle"))
								{
									long programId = Long.valueOf( taskAttributes.get("SOCIAL_RESEARCH_ID").toString() );
									SocialProgram program = EntityManager.getBroker().findById(SocialProgram.class, programId);
									userTask.setContractNumber(program.getProgramNum() +"/"+program.getProgramName());
								}
									//Social Research
								else
								{
									long rsrchId = Long.valueOf( taskAttributes.get("SOCIAL_RESEARCH_ID").toString() );
									if( mapSocialResearch.containsKey( rsrchId ) )
									{
										taskDesciption = mapFile.get( rsrchId );
										if( mapSocialResearchTaskDesc.get(rsrchId) != null )
										{
											taskDesciption +=  " / " + mapSocialResearchTaskDesc.get(rsrchId) ;
										}
										userTask.setContractNumber( taskDesciption );
										
									}
									else
									{
										SocialResearch rsrch = EntityManager.getBroker().findById(SocialResearch.class, rsrchId);
										mapFile.put(rsrchId,rsrch.getInheritanceFile().getFileNumber());
										String taskType =    	  taskAttributes.get( "TASK_TYPE" ).toString();
										Long caringTypeId = SocialResearchBean.taskTypeCaringTypeMapping(taskType);
										String desc = UtilityService.getRecommendationDescriptionForTask( rsrch ,caringTypeId );
										userTask.setContractNumber(rsrch.getInheritanceFile().getFileNumber() +"/"+desc );
										rsrch=null;
									}
								}
								
							}
							if(	taskAttributes.containsKey("ENDOWMENT_FILE_NUMBER") )
							{
								userTask.setContractNumber(taskAttributes.get("ENDOWMENT_FILE_NUMBER").toString() );
							}
							else if(	taskAttributes.containsKey("REQUEST_NUM") && 
									   (userTask.getContractNumber()==null || userTask.getContractNumber().trim().length() <= 0 )
									)
							{
								userTask.setContractNumber(taskAttributes.get("REQUEST_NUM").toString() );
							}
							else if(	taskAttributes.containsKey("ENDOWMENT_PROGRAM_NUM") && 
									   (userTask.getContractNumber()==null || userTask.getContractNumber().trim().length() <= 0 )
									)
							{
								userTask.setContractNumber(taskAttributes.get("ENDOWMENT_PROGRAM_NUM").toString() );
							}
					}
					
					
				
					
					if(addTask  )
					{
						userTasks.add(userTask);
						taskHistoryMap.put(userTask.getTaskId(),userTask );
					}
				
				}

			
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put("userTasks", userTasks);
			recordSize = userTasks.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
		}
		catch (PIMSWorkListException e) {
			
			logger.LogException("Task List could not be loaded.loadDataList method crashed due to:",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}
		catch (Exception e) {
			
			logger.LogException("Task List could not be loaded.loadDataList method crashed due to:",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage("commons.ErrorMessage"));
		}
		
		
		return "";
	}

**/
	public String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
//		if (sortField != null) {
//			Collections.sort(userTasks, new ListComparator(sortField,
//					sortAscending));
//			
//		}
	}

	// Getters -----------------------------------------------------------------------------------

	

	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------

	

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	//** default constructor *//*
	public NewTaskList() {
		//addCount.setValue(0);
	}


	// Property accessors


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

//	public UserTask getUserTask() {
//		return userTask;
//	}
//
//	public void setUserTask(UserTask dataItem) {
//		this.userTask = dataItem;
//	}
	


	//Public Methods

	// Actions -----------------------------------------------------------------------------------


	//Classes which should be implemented in Business Logic or from where we r fetching data	    

	public String reset() {

		return "success";
	}


	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public int getDataCount() {
		int rows = dataTable.getRows();
		int count = dataTable.getRowCount();
		return (count / rows) + ((count % rows != 0) ? 1 : 0);
	}        


	@Override
	@SuppressWarnings( "unchecked" )
	public void init() {		
		
		super.init();
		
		if(!isPostBack())
		{
                        initData();
			requestCreatedBy = "-1";
			if( sessionMap.containsKey( IS_CUSTOMER_SERVICE_RELATED_USER ) )
			{
				requestCreatedBy = getLoggedInUserId();
			}
			else if( !getLoggedInUserId().equals("pims_admin") )
			{
				User user = SecurityManager.getUser(getLoggedInUserId());
				for (UserGroup group : user.getUserGroups()) 
				{
					if( "PIMS_CUSTOMER_SUPPORT|PIMS_CONTRACT_APPROVER|PIMS_CS_READONLY".contains( group.getUserGroupId() ) )
					{
						 requestCreatedBy = getLoggedInUserId();
						 sessionMap.put( IS_CUSTOMER_SERVICE_RELATED_USER ,true); 
						 break;
					}
				}
			}
			
			
			assignedDateFrom = new Date();
			assignedDateTo = new Date();
//			loadDataList();
            loadTaskList();
                        
		}
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		String methodName = " preRender";
		logger.logInfo(methodName + " starts");
		try
		{
		
		FillTenantInfo();
		
		if(viewMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewMap.get(TENANT_INFO);
	       //tenantRefNum=tenantViewRow.getTenantNumber();
//	       txtTenantType=getTenantType(tenantViewRow);
	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        contractTenantName=tenantNames;
	       
	    }
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed", ex);
		}
	}
	private void FillTenantInfo() throws PimsBusinessException {
		// TODO Auto-generated method stub
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewMap.put(TENANT_INFO,tenantsList.get(0));
	    			
	    	}
		
	}

	@SuppressWarnings( "unchecked" )
//	public String actionClick(){
//		logger.logInfo("Stepped into the actionClick method");
//		userTask =  (UserTask) dataTable.getRowData();
//		setRequestParam(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
//
//		String taskType = userTask.getTaskType();
//		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
//		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.FROM_TASK_LIST,WebConstants.FROM_TASK_LIST);
//		//userTask = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
//		sessionMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
//		logger.logInfo("Task Type is:"+taskType);
//		if(
//			taskType.equals( "EndowmentDisbursementFinance" )||
//			taskType.equals( "ApproveRejectEndDisbursement" )||
//			taskType.equals( "ResubmitEndDisForApproval" )
//		  )
//		{
//			if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) != null )
//			{
//			  Long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
//			  Request request = EntityManager.getBroker().findById(Request.class, id);
//			  if( request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.MASRAF_DISBURSEMENT) == 0 )
//			  {
//				  
//				  if( taskType.equals( "EndowmentDisbursementFinance" ) )
//				  {
//					  taskType  = "EndowmentDisbursementFinanceMasraf";
//				  }
//				  else if( taskType.equals( "ApproveRejectEndDisbursement" ) )
//				  {
//					  taskType  = "ApproveRejectEndDisbursementMasraf";
//				  }
//				  else if( taskType.equals( "ResubmitEndDisForApproval" ) )
//				  {
//					  taskType  = "ResubmitEndDisForApprovalMasraf";
//				  }
//			  }
//			}
//		}
//		return taskType;
//	}
	
//	private BPMWorklistClient getBPMClientInstance() throws IOException
//	{
//		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
//		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
//		return bpmWorkListClient;
//	}
//	public String Claim()
//	{
//		try
//		{
//			errorMessages.clear();
//			String user = getLoggedInUser();
//			userTask =  (UserTask) dataTable.getRowData();
//			BPMWorklistClient bpmWorkListClient = getBPMClientInstance();
//			if(bpmWorkListClient.accquireTask(userTask, user))
//			{
//				userTask.setAccquiredby(user);
//				userTask.setIsAccquiredByUser(true);
//				CommonUtil.updateRequestTaskAcquiredBy( userTask,CommonUtil.getLoggedInUser()  );
//				
//			}
//			infoMessage =  getBundleMessage("TaskList.messages.claimSuccess");
//		}
//		catch(PIMSWorkListException pimsExp)
//		{
//			if(pimsExp.getExceptionCode() == ExceptionCodes.TASK_ALREADY_ACQUIRED)
//			{
//				errorMessages.add(CommonUtil.getParamBundleMessage("TaskList.messages.claimTaskFailureAlreadyClaimed",pimsExp.getUserIdAlreadyPerformedAction()));
//			}
//			else
//			{
//				errorMessages.add(getBundleMessage("TaskList.messages.claimTaskFailureGeneral"));
//			}
//		}
//		catch(Exception exp)
//		{
//			logger.LogException("Claim method crashed due to:",exp);
//			errorMessages.add(getBundleMessage("TaskList.messages.claimTaskFailureGeneral"));
//		}
//		
//		return "";
//	}
//
//	
//	public String Release()
//	{
//		logger.logInfo("Stepped into the Release method");
//		try
//		{
//			errorMessages.clear();
//			String user = getLoggedInUser();
//			userTask =  (UserTask) dataTable.getRowData();
//			BPMWorklistClient bpmWorkListClient = getBPMClientInstance();
//			if(bpmWorkListClient.releaseTask(userTask, user))
//			{
//				userTask.setAccquiredby(null);
//				userTask.setIsAccquiredByUser(false);
//				CommonUtil.updateRequestTaskAcquiredBy( userTask,null );
//			}
//			//loadDataList();
//			infoMessage =  getBundleMessage("TaskList.messages.releaseSuccess");
//		}
//		catch(PIMSWorkListException pimsExp)
//		{
//			logger.LogException("Release method crashed due to:",pimsExp);
//			if(pimsExp.getExceptionCode() == ExceptionCodes.TASK_IS_NOT_ACQUIRED)
//			{
//				errorMessages.add(getBundleMessage("TaskList.messages.releaseTaskFailureNotClaimed"));
//			}
//			else
//			{
//				errorMessages.add(getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
//			}
//		}
//		catch(Exception exp)
//		{
//			logger.LogException("Release method crashed due to:",exp);
//			errorMessages.add(getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
//		}
//		
//		return "";
//	}
	public void setBtnAction(HtmlCommandLink btnAction) {
		this.btnAction = btnAction;
	}

    public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	public HtmlCommandLink getBtnAction() {
		return btnAction;
	}
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}

	public Date getAssignedDateFrom() {
		return assignedDateFrom;
	}

	public void setAssignedDateFrom(Date assignedDateFrom) {
		this.assignedDateFrom = assignedDateFrom;
	}

	public Date getAssignedDateTo() {
		return assignedDateTo;
	}

	public void setAssignedDateTo(Date assignedDateTo) {
		this.assignedDateTo = assignedDateTo;
	}

	public String getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public HtmlColumn getHtmlColumnsAssignedDate() {
		return htmlColumnsAssignedDate;
	}

	public void setHtmlColumnsAssignedDate(HtmlColumn htmlColumnsAssignedDate) {
		this.htmlColumnsAssignedDate = htmlColumnsAssignedDate;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractUnitNumber() {
		return contractUnitNumber;
	}

	public void setContractUnitNumber(String contractUnitNumber) {
		this.contractUnitNumber = contractUnitNumber;
	}

	public String getContractTenantName() {
		return contractTenantName;
	}

	public void setContractTenantName(String contractTenantName) {
		this.contractTenantName = contractTenantName;
	}

	public String getRequestApplicant() {
		return requestApplicant;
	}

	public void setRequestApplicant(String requestApplicant) {
		this.requestApplicant = requestApplicant;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestCreatedBy() {
		return requestCreatedBy;
	}

	public void setRequestCreatedBy(String requestCreatedBy) {
		this.requestCreatedBy = requestCreatedBy;
	}
public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}

public String getHdnTenantId() {
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	if(viewMap.containsKey("hdnTenantId") && viewMap.get("hdnTenantId")!=null)
		hdnTenantId = viewMap.get("hdnTenantId").toString();
	return hdnTenantId;
}

public String getHdnApplicantId() {
	return hdnApplicantId;
}

public void setHdnTenantId(String hdnTenantId) {
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	this.hdnTenantId = hdnTenantId;
	if(this.hdnTenantId!=null)
		viewMap.put("hdnTenantId", this.hdnTenantId);
}

public void setHdnApplicantId(String hdnApplicantId) {
	this.hdnApplicantId = hdnApplicantId;
}

public String getInheritanceFileNumber() {
	return inheritanceFileNumber;
}

public void setInheritanceFileNumber(String inheritanceFileNumber) {
	this.inheritanceFileNumber = inheritanceFileNumber;
}

public String getEndowmentFileNumber() {
	return endowmentFileNumber;
}

public void setEndowmentFileNumber(String endowmentFileNumber) {
	this.endowmentFileNumber = endowmentFileNumber;
}

    public void setCriteria(TaskListSearchCriteria criteria) {
        if( criteria != null )
            viewMap.put("criteria",criteria);
        this.criteria = criteria;
    }

    public TaskListSearchCriteria getCriteria() {
        if(viewMap.containsKey("criteria") && viewMap.get("criteria")!=null)
                criteria = (TaskListSearchCriteria)viewMap.get("criteria");
        return criteria;
    }


    public void setDataList(List<TaskListVO> dataList) {
        if( dataList != null )
            viewMap.put("taskList",dataList);
        this.dataList = dataList;
    }

    public List<TaskListVO> getDataList() {
//        if(dataList == null)
//            loadTaskList();
//        Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        if(viewMap.containsKey("taskList") && viewMap.get("taskList")!=null)
                dataList = (List<TaskListVO>)viewMap.get("taskList");
        return dataList;
        
    }
    public void onSearch(){
    	
        loadTaskList();
        
    }
    public void loadTaskList(){
       dataList = services.getTaskList(criteria, getRowsPerPage(), 
    		   									getCurrentPage() == 0 ?1:getCurrentPage(), getLoggedInUserId());
       
        int totalRows = 0;
        
        
        if (dataList== null || dataList.isEmpty() ) 
        {
                errorMessages = new ArrayList<String>();
                errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
                forPaging(0);
        }
        else{
        	totalRows = dataList.size();
            setTotalRows(totalRows);
            doPagingComputations();
        }
        
        this.setDataList( dataList );
        forPaging(getTotalRows());
       
    }

    private void initData() {
        setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
        setSortField(DEFAULT_SORT_FIELD);
        setSortItemListAscending(false);
        
    }
    
    
    public String Claim()
          {
    	StringBuffer result = new StringBuffer();
                  try
                  {
                    TaskListVO task =  (TaskListVO) dataTable.getRowData();
                    WFClient wfClient = new WFClient();
                    result.append(wfClient.claimTask(task.getTaskId(), getLoggedInUser()));
                    task.setClaimBy(getLoggedInUser());
                    setDataList(dataList);
                      errorMessages.clear();
                      infoMessage =  getBundleMessage("TaskList.messages.claimSuccess");
                  }
                 
                  catch(Exception exp)
                  {
                          logger.LogException("Claim method crashed due to:",exp);
                          errorMessages.add(getBundleMessage("TaskList.messages.claimTaskFailureGeneral"));
                  }
                  
                  return result.toString();
          }
    
    public String Release()
	{
    	StringBuffer result = new StringBuffer();
		logger.logInfo("Stepped into the Release method");
		try
		{
			errorMessages.clear();
			TaskListVO task =  (TaskListVO) dataTable.getRowData();
			
			WFClient wfClient = new WFClient();
            result.append(wfClient.releaseTask(task.getTaskId(), getLoggedInUser()));
            task.setClaimBy(null);
            setDataList(dataList);
			
			infoMessage =  getBundleMessage("TaskList.messages.releaseSuccess");
		}
		catch(Exception exp)
		{
			logger.LogException("Release method crashed due to:",exp);
			errorMessages.add(getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
		}
		
		return result.toString();
	}
    
    
	public String actionClick() throws NumberFormatException, PimsBusinessException{
	logger.logInfo("Stepped into the actionClick method");
	TaskListVO userTask =  (TaskListVO) dataTable.getRowData();
	setRequestParam(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
	String taskType = "";
	UtilityManager manager = new UtilityManager();
	if(userTask.getHumanTaskId() != null)
		taskType = manager.getDomainDataByDomainDataId(Long.valueOf(userTask.getHumanTaskId())).getDataValue();
	
	getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
	getFacesContext().getExternalContext().getSessionMap().put(WebConstants.FROM_TASK_LIST,WebConstants.FROM_TASK_LIST);
	//userTask = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
	sessionMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
	logger.logInfo("Task Type is:"+taskType);
	if(
		taskType.equals( "EndowmentDisbursementFinance" )||
		taskType.equals( "ApproveRejectEndDisbursement" )||
		taskType.equals( "ResubmitEndDisForApproval" )
	  )
	{
		if(userTask.getRequestId() != null)
//		if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) != null )
		{
		  Long  id = new Long  ( userTask.getRequestId() );
		  Request request = EntityManager.getBroker().findById(Request.class, id);
		  if( request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.MASRAF_DISBURSEMENT) == 0 )
		  {
			  
			  if( taskType.equals( "EndowmentDisbursementFinance" ) )
			  {
				  taskType  = "EndowmentDisbursementFinanceMasraf";
			  }
			  else if( taskType.equals( "ApproveRejectEndDisbursement" ) )
			  {
				  taskType  = "ApproveRejectEndDisbursementMasraf";
			  }
			  else if( taskType.equals( "ResubmitEndDisForApproval" ) )
			  {
				  taskType  = "ResubmitEndDisForApprovalMasraf";
			  }
		  }
		}
	}
	return taskType;
}
     
}

