package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.ServletContext;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.ExceptionMessages;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AccountServiceAgent;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.business.services.GRPServiceAgent;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.business.services.PortfolioServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.StudyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.AssetType;
import com.avanza.pims.entity.AttendanceStatus;
import com.avanza.pims.entity.BlockingReason;
import com.avanza.pims.entity.BoxPlaceSubType;
import com.avanza.pims.entity.ComplaintType;
import com.avanza.pims.entity.CustomerReview;
import com.avanza.pims.entity.DisbursementReason;
import com.avanza.pims.entity.DiseaseType;
import com.avanza.pims.entity.DocumentFolder;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EducationLevel;
import com.avanza.pims.entity.EndProgDisSrc;
import com.avanza.pims.entity.EndProgEvalType;
import com.avanza.pims.entity.EndProgObjective;
import com.avanza.pims.entity.EndowmentCategory;
import com.avanza.pims.entity.EndowmentExpType;
import com.avanza.pims.entity.EndowmentPurpose;
import com.avanza.pims.entity.FollowUpActions;
import com.avanza.pims.entity.Grade;
import com.avanza.pims.entity.GrpSupplierCode;
import com.avanza.pims.entity.GrpSupplierName;
import com.avanza.pims.entity.HealthStatus;
import com.avanza.pims.entity.HobbyType;
import com.avanza.pims.entity.Month;
import com.avanza.pims.entity.OrphanCategory;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.construction.ConstructionService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.DonationBoxService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.MemsFollowupService;
import com.avanza.pims.ws.mems.SearchAssetsService;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.mems.TakharujService;
import com.avanza.pims.ws.mems.endowment.EndowmentExpenseTypeService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.property.FollowUpService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AccountTypeView;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.pims.ws.vo.ContractorTypeView;
import com.avanza.pims.ws.vo.CurrencyView;
import com.avanza.pims.ws.vo.DocumentTypeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.ExpectedRateView;
import com.avanza.pims.ws.vo.FollowUpActionsView;
import com.avanza.pims.ws.vo.InheritedAssetSharesView;
import com.avanza.pims.ws.vo.InvestmentOppSubjectView;
import com.avanza.pims.ws.vo.MaintenanceTypeView;
import com.avanza.pims.ws.vo.MemsNolTypeView;
import com.avanza.pims.ws.vo.OrderView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.PortfolioTypeView;
import com.avanza.pims.ws.vo.ProcedureTypeVO;
import com.avanza.pims.ws.vo.ProjectCategoryView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RelationshipView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.SearchAssetsCriteriaView;
import com.avanza.pims.ws.vo.SectorView;
import com.avanza.pims.ws.vo.StudySubjectView;
import com.avanza.pims.ws.vo.TradeLicenseIssuersVO;
import com.avanza.pims.ws.vo.TypeView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.ViolationCategoryView;
import com.avanza.pims.ws.vo.ViolationTypeView;
import com.avanza.pims.ws.vo.WorkTypeView;
import com.avanza.pims.ws.vo.XXAmafTransactionTypesView;
import com.avanza.pims.ws.vo.XxAmafBankRemitAccountView;
import com.avanza.pims.ws.vo.mems.BoxPlaceTypeView;
import com.avanza.pims.ws.vo.mems.GovtDepttView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class ApplicationBean extends AbstractController {

	private static final long serialVersionUID = -7993719108316479501L;

	private transient Logger logger = Logger.getLogger(ApplicationBean.class);

	private Map<String, List<SelectItem>> listMap = new HashMap<String, List<SelectItem>>();
	private ServletContext context = (ServletContext) FacesContext
			.getCurrentInstance().getExternalContext().getContext();

	private List<SelectItem> paymentType = new ArrayList();
	private Locale LOCALE_EN = new Locale("en");
	private UtilityService us = new UtilityService();
	private List<SelectItem> emptyList = new ArrayList<SelectItem>();

	public List<SelectItem> getDomainDataList(String domainType) {
		Boolean isEnglishLocale = isEnglishLocale();
		String listMapKeyEn = "SELECT_ITEM_LIST_EN_" + domainType.toUpperCase();
		String listMapKeyAr = "SELECT_ITEM_LIST_AR_" + domainType.toUpperCase();
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(listMapKeyEn);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(domainType) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(listMapKeyEn, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(listMapKeyAr);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(domainType) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(listMapKeyAr, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getCaringTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();

		try {
			List<CaringTypeView> caringTypeViewList = utilityServiceAgent
					.getCaringTypeList();
			if (caringTypeViewList != null && caringTypeViewList.size() > 0) {
				for (CaringTypeView caringTypeView : caringTypeViewList) {
					SelectItem item = new SelectItem();
					item.setValue(caringTypeView.getCaringTypeId().toString());

					if (isEnglishLocale) {
						String desc = (caringTypeView.getCaringTypeNameEn() != null) ? caringTypeView
								.getCaringTypeNameEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (caringTypeView.getCaringTypeNameAr() != null) ? caringTypeView
								.getCaringTypeNameAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load bank list actions list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getMemsNolTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();

		try {
			List<MemsNolTypeView> memsNolTypeViewList = utilityServiceAgent
					.getMemsNolTypeList(0L);
			if (memsNolTypeViewList != null && memsNolTypeViewList.size() > 0) {
				for (MemsNolTypeView memsNolTypeView : memsNolTypeViewList) {
					SelectItem item = new SelectItem();
					item
							.setValue(memsNolTypeView.getMemsNolTypeId()
									.toString());

					if (isEnglishLocale) {
						String desc = (memsNolTypeView.getMemsNolTypeNameEn() != null) ? memsNolTypeView
								.getMemsNolTypeNameEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (memsNolTypeView.getMemsNolTypeNameAr() != null) ? memsNolTypeView
								.getMemsNolTypeNameAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load bank list actions list due to :  ", e);
		}

		return cboList;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getBankList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();

		try {
			if (viewMap.get("BANK_LIST") != null) {
				return (List<SelectItem>) viewMap.get("BANK_LIST");
			}
			List<BankView> bankViewList = utilityServiceAgent.getBanksList();
			if (bankViewList != null && bankViewList.size() > 0) {
				for (BankView bankView : bankViewList) {
					SelectItem item = new SelectItem();
					item.setValue(bankView.getBankId().toString());

					if (isEnglishLocale) {
						String desc = (bankView.getBankEn() != null) ? bankView
								.getBankEn().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (bankView.getBankAr() != null) ? bankView
								.getBankAr().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				viewMap.put("BANK_LIST", cboList);
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load bank list actions list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getViolationCategoryList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();

		try {
			List<ViolationCategoryView> violationCategoryViewList = utilityServiceAgent
					.getViolationCategoryList();
			if (violationCategoryViewList != null
					&& violationCategoryViewList.size() > 0) {
				for (ViolationCategoryView vcvl : violationCategoryViewList) {
					SelectItem item = new SelectItem();
					item.setValue(vcvl.getViolationCategoryId().toString());

					if (isEnglishLocale) {
						String desc = (vcvl.getDescriptionEn() != null) ? vcvl
								.getDescriptionEn().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (vcvl.getDescriptionAr() != null) ? vcvl
								.getDescriptionAr().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger
					.LogException(
							"Failed to load violation category list actions list due to :  ",
							e);
		}

		return cboList;

	}

	public List<SelectItem> getViolationTypeListByCategory(Long id) {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();

		try {
			List<ViolationTypeView> violationTypeViewList = utilityServiceAgent
					.getViolationTypeListByCategory(id);
			if (violationTypeViewList != null
					&& violationTypeViewList.size() > 0) {
				for (ViolationTypeView vtl : violationTypeViewList) {
					SelectItem item = new SelectItem();
					item.setValue(vtl.getViolationTypeId().toString());

					if (isEnglishLocale) {
						String desc = (vtl.getDescriptionEn() != null) ? vtl
								.getDescriptionEn().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (vtl.getDescriptionAr() != null) ? vtl
								.getDescriptionAr().toString() : "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load bank list actions list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getAllMaintenanceTypes() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		ConstructionService constructionService = new ConstructionService();

		try {
			List<MaintenanceTypeView> maintenanceTypeViewList = constructionService
					.getAllMaintenanceTypes();
			if (maintenanceTypeViewList != null
					&& maintenanceTypeViewList.size() > 0) {
				for (MaintenanceTypeView maintenanceTypeView : maintenanceTypeViewList) {
					SelectItem item = new SelectItem();
					item.setValue(maintenanceTypeView.getMaintenanceTypeId()
							.toString());

					if (isEnglishLocale) {
						String desc = (maintenanceTypeView.getDescriptionEn() != null) ? maintenanceTypeView
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (maintenanceTypeView.getDescriptionAr() != null) ? maintenanceTypeView
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load maintenance types list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getAllFollowUpActions() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		FollowUpService followUpService = new FollowUpService();

		try {
			List<FollowUpActions> listFollowUpActions = followUpService
					.getAllFollowUpActions();
			if (listFollowUpActions != null && listFollowUpActions.size() > 0) {
				for (FollowUpActions followUpAction : listFollowUpActions) {
					SelectItem item = new SelectItem();
					item.setValue(followUpAction.getFollowUpActionsId()
							.toString());

					if (isEnglishLocale) {
						String desc = (followUpAction.getDescriptionEn() != null) ? followUpAction
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (followUpAction.getDescriptionAr() != null) ? followUpAction
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load followup actions list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getConstructionServiceTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("constructionServiceTypeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.CONSTRUCTION_SERVICE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("constructionServiceTypeListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("constructionServiceTypeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.CONSTRUCTION_SERVICE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("constructionServiceTypeListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DomainTypeView> getDomainTypeList() {
		return (List<DomainTypeView>) context
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	}

	@SuppressWarnings("unchecked")
	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentConfigConditionList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.PAYMENT_CONFIG_CONDITION_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CONDITION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.ApplicationBean.PAYMENT_CONFIG_CONDITION_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.PAYMENT_CONFIG_CONDITION_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CONDITION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.ApplicationBean.PAYMENT_CONFIG_CONDITION_AR,
								cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getRentProcessList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.RENT_PROCESS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.RENT_PROCESS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.RENT_PROCESS_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.RENT_PROCESS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.RENT_PROCESS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.RENT_PROCESS_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getChequeStatusAtBankList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.CHEQUE_STATUS_AT_BANK_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CHEQUE_STATUS_AT_BANK) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.CHEQUE_STATUS_AT_BANK_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.CHEQUE_STATUS_AT_BANK_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CHEQUE_STATUS_AT_BANK) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.CHEQUE_STATUS_AT_BANK_AR,
						cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getGracePeriodTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.GRACE_PERIOD_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRACE_PERIOD_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.GRACE_PERIOD_TYPE_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.GRACE_PERIOD_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRACE_PERIOD_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.GRACE_PERIOD_TYPE_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyCategoryList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.PROPERTY_CATEGORY_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PROPERTY_CATAGORY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.PROPERTY_CATEGORY_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.PROPERTY_CATEGORY_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PROPERTY_CATAGORY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.PROPERTY_CATEGORY_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getUnitUsageTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_USAGE_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.UNIT_USAGE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.UNIT_USAGE_TYPE_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_USAGE_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.UNIT_USAGE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(
						WebConstants.ApplicationBean.UNIT_USAGE_TYPE_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	public List<SelectItem> getFloorTypesList() {
		return getFloorTypesList(null);
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getFloorTypesList(
			Map<String, String> excludeFloorTypes) {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.FLOOR_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FLOOR_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if (excludeFloorTypes == null
									|| !excludeFloorTypes.containsKey(ddv
											.getDomainDataId().toString())) {
								item = new SelectItem(ddv.getDomainDataId()
										.toString(), ddv.getDataDescEn());
								cboList.add(item);
							}
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.ApplicationBean.FLOOR_TYPE_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.FLOOR_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FLOOR_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if (excludeFloorTypes == null
									|| !excludeFloorTypes.containsKey(ddv
											.getDomainDataId().toString())) {
								item = new SelectItem(ddv.getDomainDataId()
										.toString(), ddv.getDataDescAr());
								cboList.add(item);
							}
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.ApplicationBean.FLOOR_TYPE_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("propertyStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PROPERTY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyStatusListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("propertyStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PROPERTY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyStatusListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("propertyTypeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PROPERTY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyTypeListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("propertyTypeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PROPERTY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyTypeListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyUsageList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("propertyUsageListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROPERTY_USAGE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyUsageListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("propertyUsageListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROPERTY_USAGE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyUsageListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getInvestmentPurposeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("propertyInvestmentPurposeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INVESTMENT_POURPOSE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyInvestmentPurposeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("propertyInvestmentPurposeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INVESTMENT_POURPOSE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("propertyInvestmentPurposeAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getUnitTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.UNIT_TYPE_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.UNIT_TYPE_LIST_AR,
						cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getUnitInvestmentTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_INVESTMENT_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.UNIT_INVESTMENT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap
						.put(
								WebConstants.ApplicationBean.UNIT_INVESTMENT_TYPE_LIST_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_INVESTMENT_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.UNIT_INVESTMENT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap
						.put(
								WebConstants.ApplicationBean.UNIT_INVESTMENT_TYPE_LIST_AR,
								cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getTenderTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		logger.logInfo("getTenderTypeList() started...");
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("tenderTypeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderTypeListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("tenderTypeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}

				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderTypeListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getApplicationSourceList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("applicationSourceListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.APPLICATION_SOURCE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("applicationSourceListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("applicationSourceListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.APPLICATION_SOURCE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("applicationSourceListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getLicenseSourceList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("licenseSourceListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.LicenseSource.LICENSE_SOURCE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("licenseSourceListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("licenseSourceListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.LicenseSource.LICENSE_SOURCE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("licenseSourceListAr", cboList);
			}
			return cboList;
		}
	}

	// ////

	public List<SelectItem> getMaintenancePriorityList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("mainenancePriorityListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_PRIORITY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("mainenancePriorityListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("mainenancePriorityListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_PRIORITY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("mainenancePriorityListAr", cboList);
			}
			return cboList;
		}
	}

	// ////

	@SuppressWarnings("unchecked")
	public List<SelectItem> getExtendApplicationStatusList() {
		// FIXME change AUCTION_STATUS 1
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("extendApplicationStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.AUCTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("extendApplicationStatusListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("extendApplicationStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.AUCTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("extendApplicationStatusListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getStudyTypes() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("studyTypesEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.StudyType.STUDY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("studyTypesEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("studyTypesAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.StudyType.STUDY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("studyTypesAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectSizeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("projectSizeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_SIZE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("projectSizeListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("projectSizeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_SIZE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("projectSizeListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectCategoryList() {
		// Boolean isEnglishLocale = isEnglishLocale();
		//		
		// if(isEnglishLocale) {
		// List<SelectItem> cboList = listMap.get("projectCategoryListEn");
		// if(cboList==null) {
		// List<DomainTypeView> list = (List<DomainTypeView>)
		// context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		// Iterator<DomainTypeView> itr = list.iterator();
		// while( itr.hasNext() ) {
		// DomainTypeView dtv = itr.next();
		// if(
		// dtv.getTypeName().compareTo(WebConstants.Project.PROJECT_CATEGORY)==0
		// ) {
		// Set<DomainDataView> dd = dtv.getDomainDatas();
		// cboList = new ArrayList<SelectItem>();
		// SelectItem item = null;
		// for (DomainDataView ddv : dd) {
		// item = new SelectItem(ddv.getDomainDataId().toString(),
		// ddv.getDataDescEn());
		// cboList.add(item);
		// }
		// break;
		// }
		// }
		// if( cboList==null ) {
		// return emptyList;
		// }
		// Collections.sort(cboList,ListComparator.LIST_COMPARE);
		// listMap.put("projectCategoryListEn", cboList);
		// }
		// return cboList;
		// } else {
		// List<SelectItem> cboList = listMap.get("projectCategoryListAr");
		// if(cboList==null) {
		// List<DomainTypeView> list = (List<DomainTypeView>)
		// context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		// Iterator<DomainTypeView> itr = list.iterator();
		// while( itr.hasNext() ) {
		// DomainTypeView dtv = itr.next();
		// if(
		// dtv.getTypeName().compareTo(WebConstants.Project.PROJECT_CATEGORY)==0
		// ) {
		// Set<DomainDataView> dd = dtv.getDomainDatas();
		// cboList = new ArrayList<SelectItem>();
		// SelectItem item = null;
		// for (DomainDataView ddv : dd) {
		// item = new SelectItem(ddv.getDomainDataId().toString(),
		// ddv.getDataDescAr());
		// cboList.add(item);
		// }
		// break;
		// }
		// }
		// if( cboList==null ) {
		// return emptyList;
		// }
		// Collections.sort(cboList,ListComparator.LIST_COMPARE);
		// listMap.put("projectCategoryListAr", cboList);
		// }
		// return cboList;
		// }

		// ////Now this ProjectCategoryList is populated from PROJECT_CATEGORY
		// Table//////
		String methodName = "getProjectCategoriesList";
		logger.logInfo(methodName + "|started...");
		List<SelectItem> projectCategoryItemList = new ArrayList();
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		try {

			if (isEnglishLocale()) {
				if (!listMap.containsKey("ProjectCategoryListEn")
						|| listMap.get("ProjectCategoryListEn") == null) {
					List<ProjectCategoryView> projectCategoriesList = csa
							.getAllProjectCategories();
					if (projectCategoriesList != null) {

						for (ProjectCategoryView category : projectCategoriesList) {
							projectCategoryItemList.add(new SelectItem(category
									.getProjectCategoryId().toString(),
									category.getDescriptionEn()));
						}
						listMap.put("ProjectCategoryListEn",
								projectCategoryItemList);
					}
				} else
					projectCategoryItemList = (ArrayList<SelectItem>) listMap
							.get("ProjectCategoryListEn");
			}

			else if (!isEnglishLocale()) {
				if (!listMap.containsKey("ProjectCategoryListAr")
						|| listMap.get("ProjectCategoryListAr") == null) {
					List<ProjectCategoryView> projectCategoriesList = csa
							.getAllProjectCategories();
					if (projectCategoriesList != null) {

						for (ProjectCategoryView category : projectCategoriesList) {
							projectCategoryItemList.add(new SelectItem(category
									.getProjectCategoryId().toString(),
									category.getDescriptionAr()));
						}
						listMap.put("ProjectCategoryListAr",
								projectCategoryItemList);
					}
				} else
					projectCategoryItemList = (ArrayList<SelectItem>) listMap
							.get("ProjectCategoryListAr");
			}
			Collections.sort(projectCategoryItemList,
					ListComparator.LIST_COMPARE);
			logger.logInfo(methodName + "|completed successfully!!!");

		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured", e);
		}
		return projectCategoryItemList;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectPurposeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("projectPurposeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_PURPOSE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("projectPurposeListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("projectPurposeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_PURPOSE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("projectPurposeListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getAuctionStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("auctionStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.AUCTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("auctionStatusListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("auctionStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.AUCTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("auctionStatusListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getRequestStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.REQUEST_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.REQUEST_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.REQUEST_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.REQUEST_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getTenantType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("TenantTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.TENANT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("TenantTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("TenantTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.TENANT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("TenantTypeAr", cboList);
			}
			return cboList;
		}
	}

	/** ******************* */
	public List<SelectItem> getChequeType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ChequeTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CHEQUE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ChequeTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ChequeTypeaR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CHEQUE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ChequeTypeaR", cboList);
			}
			return cboList;
		}
	}

	/** ****************** */
	public List<SelectItem> getFacilityType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FacilityTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FACILITY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FacilityTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FacilityTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FACILITY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FacilityTypeAr", cboList);
			}

			return cboList;
		}
	}

	public List<SelectItem> getUnitType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("UnitTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("UnitTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("UnitTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("UnitTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getUnitSideType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("UnitSideTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.UNIT_SIDE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("UnitSideTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("UnitSideTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.UNIT_SIDE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("UnitSideTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPropertyOwnershipType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("PropertyOwenershipTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.OWNER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PropertyOwenershipTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("PropertyOwenershipTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.OWNER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PropertyOwnershipTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPropertyUsageType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("PropertyUsageTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROPERTY_USAGE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PropertyUsageTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("PropertyUsageTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROPERTY_USAGE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PropertyUsageTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInquiryMethod() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("InquiryMethodEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.INQUIRY_METHOD) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("InquiryMethodEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("InquiryMethodAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.INQUIRY_METHOD) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("InquiryMethodAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInquiryPriority() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("InquiryPriorityEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PRIORITY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("InquiryPriorityEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("InquiryPriorityAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PRIORITY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("InquiryPriorityAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getTitle() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("TitleEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.TITLES) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("TitleEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("TitleAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.TITLES) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("TitleAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInspectionType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.INSPECTION_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INSPECTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.INSPECTION_TYPE_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.INSPECTION_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INSPECTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.INSPECTION_TYPE_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInspectionStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.INSPECTION_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INSPECTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.INSPECTION_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.INSPECTION_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INSPECTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.INSPECTION_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getFeeType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FeeTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FEE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FeeTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FeeTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FEE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FeeTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getBasedOnType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("BasedOnTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.BASED_ON_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("BasedOnTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("BasedOnTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.BASED_ON_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("BasedOnTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getProcedureType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ProcedureTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROCEDURE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProcedureTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ProcedureTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.PROCEDURE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProcedureTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInspectionViolationType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.VIOLATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_TYPE_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.VIOLATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_TYPE_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInspectionViolationStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInspectionViolationCategory() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_CATEGORY_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_CATEGORY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_CATEGORY_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.VIOLATION_CATEGORY_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_CATEGORY) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.VIOLATION_CATEGORY_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getFeeSubType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FeeSubTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FEE_SUB_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FeeSubTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FeeSubTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.FEE_SUB_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FeeSubTypeAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getHours() {
		List<SelectItem> cboList = listMap.get("hours");
		SelectItem item = null;
		String hh = "";
		if (cboList == null) {
			cboList = new ArrayList<SelectItem>();
			for (int i = 0; i < 24; i++) {
				if (i < 10)
					hh = "0";
				else
					hh = "";
				hh += i;
				item = new SelectItem(hh, hh);
				cboList.add(item);
			}
			listMap.put("hours", cboList);
		}
		return cboList;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getMinutes() {
		List<SelectItem> cboList = listMap.get("minutes");
		SelectItem item = null;
		String mm = "";
		if (cboList == null) {
			cboList = new ArrayList<SelectItem>();
			for (int i = 0; i < 60; i = i + 5) {
				if (i < 10)
					mm = "0";
				else
					mm = "";
				mm += i;
				item = new SelectItem(mm, mm);
				cboList.add(item);
			}
			listMap.put("minutes", cboList);
		}
		return cboList;
	}

	public List<SelectItem> getPerosnSubTypeList(String type) {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.PERSON_SUB_TYPE);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(type) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.PERSON_SUB_TYPE, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.PERSON_SUB_TYPE);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(type) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put(WebConstants.PERSON_SUB_TYPE, cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getUnitStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.UNIT_STATUS_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.ApplicationBean.UNIT_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.UNIT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.ApplicationBean.UNIT_STATUS_LIST_AR,
						cboList);
			}

			return cboList;
		}
	}

	public List<SelectItem> getFollowUpType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FollowUpTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.FOLLOW_UP_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FollowUpTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FollowUpTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.FOLLOW_UP_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("FollowUpTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getFollowUpStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FollowUpStatusEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.FollowUpStatus.FOLLOW_UP_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("FollowUpStatusEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FollowUpStatusAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.FollowUpStatus.FOLLOW_UP_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("FollowUpStatusAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getContractType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ContractTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CONTRACT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ContractTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ContractTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.CONTRACT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ContractTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPersonKindList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(WebConstants.PERSON_KIND
					+ "_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PERSON_KIND) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.PERSON_KIND, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(WebConstants.PERSON_KIND
					+ "_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.PERSON_KIND) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.PERSON_KIND, cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDocumentTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		logger.logInfo("getDocumentTypeList() started...");
		try {
			if (isEnglishLocale) {
				// because this document type can be changed from Pims Setup and
				// after the change new item will not be reflected
				// if we put this list in session therefore each time it will
				// hit the db to get the updated records
				List<SelectItem> cboList = null;// listMap.get("documentTypeListEn");
				if (cboList == null) {
					cboList = new ArrayList<SelectItem>();
					UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
					List<DocumentTypeView> documentTypeList = utilityServiceAgent
							.getDocumentTypes();
					SelectItem item = null;
					for (DocumentTypeView documentTypeView : documentTypeList) {
						item = new SelectItem(documentTypeView
								.getDocumentTypeId().toString(),
								documentTypeView.getDescriptionEn());
						cboList.add(item);
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("documentTypeListEn", cboList);
				logger
						.logInfo("getDocumentTypeList() completed successfully...");
				return cboList;
			} else {
				List<SelectItem> cboList = null;// listMap.get("documentTypeListAr");
				if (cboList == null) {
					cboList = new ArrayList<SelectItem>();
					UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
					List<DocumentTypeView> documentTypeList = utilityServiceAgent
							.getDocumentTypes();
					SelectItem item = null;
					for (DocumentTypeView documentTypeView : documentTypeList) {
						item = new SelectItem(documentTypeView
								.getDocumentTypeId().toString(),
								documentTypeView.getDescriptionAr());
						cboList.add(item);
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("documentTypeListAr", cboList);
				logger
						.logInfo("getDocumentTypeList() completed successfully...");
				return cboList;
			}

		} catch (PimsBusinessException exception) {
			logger.LogException("getDocumentTypeList() crashed ", exception);
		} catch (Exception exception) {
			logger.LogException("getDocumentTypeList() crashed ", exception);
		}
		return emptyList;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getEvaluationPurposeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		logger.logInfo("getEvaluationPurposeList() started...");
		try {
			if (isEnglishLocale) {
				List<SelectItem> cboList = listMap
						.get("evaluationPurposeListEn");
				if (cboList == null) {
					cboList = new ArrayList<SelectItem>();
					ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
					List<TypeView> evaluationPurposeList = constructionServiceAgent
							.getEvaluationPurposeList();
					SelectItem item = null;
					for (TypeView typeView : evaluationPurposeList) {
						item = new SelectItem(typeView.getTypeId().toString(),
								typeView.getDescriptionEn());
						cboList.add(item);
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("evaluationPurposeListEn", cboList);
				logger
						.logInfo("getEvaluationPurposeList() completed successfully...");
				return cboList;
			} else {
				List<SelectItem> cboList = listMap
						.get("evaluationPurposeListAr");
				if (cboList == null) {
					cboList = new ArrayList<SelectItem>();
					ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
					List<TypeView> evaluationPurposeList = constructionServiceAgent
							.getEvaluationPurposeList();
					SelectItem item = null;
					for (TypeView typeView : evaluationPurposeList) {
						item = new SelectItem(typeView.getTypeId().toString(),
								typeView.getDescriptionAr());
						cboList.add(item);
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("evaluationPurposeListAr", cboList);
				logger
						.logInfo("getEvaluationPurposeList() completed successfully...");
				return cboList;
			}

		} catch (PimsBusinessException exception) {
			logger.LogException("getEvaluationPurposeList() crashed ",
					exception);
		} catch (Exception exception) {
			logger.LogException("getEvaluationPurposeList() crashed ",
					exception);
		}
		return emptyList;
	}

	public DomainDataView getDomainData(String typeName, String domainValue) {
		for (DomainTypeView domainType : this.getDomainTypeList()) {
			if (domainType.getTypeName().equalsIgnoreCase(typeName))
				for (DomainDataView data : domainType.getDomainDatas()) {
					if (data.getDataValue().equalsIgnoreCase(domainValue))
						return data;
				}
		}
		return null;
	}

	public List<SelectItem> getWorkTypeItems(List<WorkTypeView> workTypeViewList) {
		List<SelectItem> workTypeItems = new ArrayList<SelectItem>();
		boolean isLocaleEnglish = isEnglishLocale();

		for (WorkTypeView workTypeView : workTypeViewList)
			workTypeItems.add(new SelectItem(workTypeView.getWorkTypeId()
					.toString(), isLocaleEnglish ? workTypeView
					.getDescriptionEn() : workTypeView.getDescriptionAr()));

		return workTypeItems;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getTenderStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("tenderStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderStatusListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("tenderStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderStatusListAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getStrategicPlanStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("StrategicPlanStatusEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.STRATEGIC_PLAN_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("StrategicPlanStatusEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("StrategicPlanStatusAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.STRATEGIC_PLAN_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("StrategicPlanStatusAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getProjectStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ProjectStatusEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectStatusEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ProjectStatusAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectStatusAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getProjectType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ProjectTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}

						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);

				listMap.put("ProjectTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ProjectTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Project.PROJECT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectTypeAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getContractorTypeList()
			throws PimsBusinessException {
		Boolean isEnglishLocale = isEnglishLocale();
		logger.logInfo("getContractorTypeList() started...");
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("contractorTypeListEn");
			if (cboList == null) {
				ConstructionServiceAgent csa = new ConstructionServiceAgent();
				List<ContractorTypeView> contractTypeViewList = csa
						.getAllContractorType();
				for (ContractorTypeView contractorTypeView : contractTypeViewList) {
					if (cboList == null)
						cboList = new ArrayList<SelectItem>();
					cboList.add(new SelectItem(contractorTypeView
							.getContractorTypeId().toString(),
							contractorTypeView.getDescriptionEn()));
				}
				if (cboList == null)
					return emptyList;
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("contractorTypeListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("contractorTypeListAr");
			if (cboList == null) {
				ConstructionServiceAgent csa = new ConstructionServiceAgent();
				List<ContractorTypeView> contractTypeViewList = csa
						.getAllContractorType();
				for (ContractorTypeView contractorTypeView : contractTypeViewList) {
					if (cboList == null)
						cboList = new ArrayList<SelectItem>();
					cboList.add(new SelectItem(contractorTypeView
							.getContractorTypeId().toString(),
							contractorTypeView.getDescriptionAr()));
				}
				if (cboList == null)
					return emptyList;
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("contractorTypeListAr", cboList);
			}
			return cboList;
		}
	}

	// ///////////////Inserted By Kamran Ahmed///////////////////

	/** ********** */
	public List<SelectItem> getRequestTypeList() {
		List<SelectItem> requestTypeList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<RequestTypeView> requestTypeView = new ArrayList<RequestTypeView>(
				0);

		try {
			requestTypeView = psa.getAllRequestTypes();
		} catch (Exception e) {
			logger.LogException("getCoutryList()", e);
		}

		if (requestTypeView != null)
			for (RequestTypeView rtv : requestTypeView)
				requestTypeList.add(new SelectItem(rtv.getRequestTypeId()
						.toString(), isLocaleEnglish ? rtv.getRequestTypeEn()
						: rtv.getRequestTypeAr()));

		Collections.sort(requestTypeList, ListComparator.LIST_COMPARE);

		return requestTypeList;
	}

	/** *********** */
	public List<SelectItem> getCountryList() {
		List<SelectItem> countryList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<RegionView> regionViewList = new ArrayList<RegionView>(0);

		try {
			regionViewList = psa.getCountry();
		} catch (Exception e) {
			logger.LogException("getCoutryList()", e);
		}

		if (regionViewList != null)
			for (RegionView rgv : regionViewList)
				countryList.add(new SelectItem(rgv.getRegionId().toString(),
						isLocaleEnglish ? rgv.getDescriptionEn() : rgv
								.getDescriptionAr()));

		Collections.sort(countryList, ListComparator.LIST_COMPARE);

		return countryList;
	}

	/** *********** */
	public List<SelectItem> getPaymentTypeList() {
		List<SelectItem> paymentTypeList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		UtilityServiceAgent usa = new UtilityServiceAgent();
		List<PaymentTypeView> paymentTypeViewList = new ArrayList<PaymentTypeView>(
				0);

		try {
			paymentTypeViewList = usa.getAllPaymentTypes();
		} catch (Exception e) {
			logger.LogException("getPaymentTypeList()", e);
		}

		if (paymentTypeViewList != null)
			for (PaymentTypeView ptv : paymentTypeViewList)
				paymentTypeList.add(new SelectItem(ptv.getPaymentTypeId()
						.toString(), isLocaleEnglish ? ptv.getDescriptionEn()
						: ptv.getDescriptionAr()));

		Collections.sort(paymentTypeList, ListComparator.LIST_COMPARE);

		return paymentTypeList;
	}

	// /////////////////Kamran Ahmed///////////////////////////////////
	// I have used another method here to populate the PaymentType List, because
	// 'PAYMENT_TYPE' is not available in the DOMAIN_TYPE.
	// This list is coming from PAYMENT_TYPE Table.
	// ////////////////////////////////////////////////////
	// public List<SelectItem> getPaymentType() {
	//		
	// Boolean isEnglishLocale = isEnglishLocale();
	// if(isEnglishLocale) {
	// List<SelectItem> cboList = listMap.get("PaymentTypeEn");
	// if(cboList==null) {
	// List<DomainTypeView> list = (List<DomainTypeView>)
	// context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	// Iterator<DomainTypeView> itr = list.iterator();
	// while( itr.hasNext() ) {
	// DomainTypeView dtv = itr.next();
	//					
	// if( dtv.getTypeName().compareTo(WebConstants.GRP.PAYMENT_TYPE)==0 ) {
	//						
	// Set<DomainDataView> dd = dtv.getDomainDatas();
	// cboList = new ArrayList<SelectItem>();
	// SelectItem item = null;
	//									
	// for (DomainDataView ddv : dd) {
	//							
	// item = new SelectItem(ddv.getDomainDataId().toString(),
	// ddv.getDataDescEn());
	// cboList.add(item);
	// }
	//						
	// break;
	// }
	// }
	// if( cboList==null ) {
	// return emptyList;
	// }
	// if(cboList!=null)
	// listMap.put("PaymentTypeEn", cboList);
	// }
	//			
	// return cboList;
	// } else {
	// List<SelectItem> cboList = listMap.get("PaymentTypeAr");
	// if(cboList==null) {
	// List<DomainTypeView> list = (List<DomainTypeView>)
	// context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	// Iterator<DomainTypeView> itr = list.iterator();
	// while( itr.hasNext() ) {
	// DomainTypeView dtv = itr.next();
	// if( dtv.getTypeName().compareTo(WebConstants.GRP.PAYMENT_TYPE)==0 ) {
	// Set<DomainDataView> dd = dtv.getDomainDatas();
	// cboList = new ArrayList<SelectItem>();
	// SelectItem item = null;
	// for (DomainDataView ddv : dd) {
	// item = new SelectItem(ddv.getDomainDataId().toString(),
	// ddv.getDataDescAr());
	// cboList.add(item);
	// }
	// break;
	// }
	// }
	// if( cboList==null ) {
	// return emptyList;
	// }
	// listMap.put("PaymentTypeAr", cboList);
	// }
	// return cboList;
	// }
	// }
	// //////////////////////////////////////////////////////
	public List<SelectItem> getTrasnsactionType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("TrasnsactionTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.TRANSACTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}

						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}

				listMap.put("TrasnsactionTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("TrasnsactionTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.TRANSACTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("TrasnsactionTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getDirectionType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("DirectionTypeEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.DIRECTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}

						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}

				listMap.put("DirectionTypeEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("DirectionTypeAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.DIRECTION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				listMap.put("DirectionTypeAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPaymentPeriodList() {

		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("periodListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PaymentPeriods.PAYMENT_PERIOD_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}

						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);

				listMap.put("periodListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("periodListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PaymentPeriods.PAYMENT_PERIOD_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("periodListAr", cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentScheduleStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("PaymentScheduleStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PaymentScheduleStatusListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("PaymentScheduleStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PaymentScheduleStatusListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentMethodList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("PaymentMethodListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_MODE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PaymentMethodListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("PaymentMethodListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_MODE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PaymentMethodListAr", cboList);
			}

			return cboList;
		}
	}

	public List<SelectItem> getViolationActionList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ViolationActionListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_ACTION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ViolationActionListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ViolationActionListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.VIOLATION_ACTION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ViolationActionListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignNoteType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteTypeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteTypeListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteTypeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteTypeListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignNoteStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteStatusListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteStatusListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignNoteOwner() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteOwnerListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_OWNER) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteOwnerListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignNoteOwnerListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_NOTE_OWNER) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignNoteOwnerListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ProjectDesignTypeListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignTypeListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ProjectDesignTypeListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignTypeListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ProjectDesignStatusListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignStatusListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ProjectDesignStatusListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.DomainTypes.PROJECT_DESIGN_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignStatusListAr", cboList);
			}

			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getProjectDesignPresentedTo() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignPresentedToListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.DomainTypes.PROJECT_DESIGN_PRESENTED_TO) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignPresentedToListEn", cboList);
			}

			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("ProjectDesignPresentedToListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.DomainTypes.PROJECT_DESIGN_PRESENTED_TO) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ProjectDesignPresentedToListAr", cboList);
			}

			return cboList;
		}
	}

	public List<SelectItem> getStudyStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Study.STUDY_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Study.STUDY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Study.STUDY_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Study.STUDY_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Study.STUDY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Study.STUDY_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getStudyTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Study.STUDY_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Study.STUDY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Study.STUDY_TYPE_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Study.STUDY_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Study.STUDY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Study.STUDY_TYPE_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getStudySubjectList() {
		List<SelectItem> studySubjectList = new ArrayList<SelectItem>(0);

		boolean isLocaleEnglish = isEnglishLocale();

		StudyServiceAgent studyServiceAgent = new StudyServiceAgent();

		StudySubjectView studySubjectView = new StudySubjectView();
		studySubjectView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		studySubjectView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);

		List<StudySubjectView> studySubjectViewList = new ArrayList<StudySubjectView>(
				0);

		try {
			studySubjectViewList = studyServiceAgent
					.getStudySubjectList(studySubjectView);
		} catch (Exception e) {
			logger.LogException("getStudySubjectList()", e);
		}

		if (studySubjectViewList != null)
			for (StudySubjectView ssv : studySubjectViewList)
				studySubjectList.add(new SelectItem(ssv.getStudySubjectId(),
						isLocaleEnglish ? ssv.getStudySubjectEn() : ssv
								.getStudySubjectAr()));

		Collections.sort(studySubjectList, ListComparator.LIST_COMPARE);

		return studySubjectList;
	}

	public List<SelectItem> getAssetClassStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ASSET_CLASS_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.ASSET_CLASS_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ASSET_CLASS_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ASSET_CLASS_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.ASSET_CLASS_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ASSET_CLASS_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getContractStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ContractStatusEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CONTRACT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ContractStatusEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ContractStatusAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CONTRACT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ContractStatusAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPortfolioTypeList() {
		List<SelectItem> portfolioTypeList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		PortfolioServiceAgent portfolioServiceAgent = new PortfolioServiceAgent();
		PortfolioTypeView portfolioTypeView = new PortfolioTypeView();
		portfolioTypeView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		portfolioTypeView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<PortfolioTypeView> portfolioTypeViewList = new ArrayList<PortfolioTypeView>(
				0);

		try {
			portfolioTypeViewList = portfolioServiceAgent
					.getPortfolioTypeList(portfolioTypeView);
		} catch (Exception e) {
			logger.LogException("getPortfolioTypeList()", e);
		}

		if (portfolioTypeViewList != null)
			for (PortfolioTypeView ptv : portfolioTypeViewList)
				portfolioTypeList.add(new SelectItem(ptv.getPortfolioTypeId()
						.toString(), isLocaleEnglish ? ptv.getPortfolioTypeEn()
						: ptv.getPortfolioTypeAr()));

		Collections.sort(portfolioTypeList, ListComparator.LIST_COMPARE);

		return portfolioTypeList;
	}

	public List<SelectItem> getPortfolioStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Portfolio.PORTFOLIO_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Portfolio.PORTFOLIO_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Portfolio.PORTFOLIO_STATUS_LIST_EN,
						cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Portfolio.PORTFOLIO_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Portfolio.PORTFOLIO_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Portfolio.PORTFOLIO_STATUS_LIST_AR,
						cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getAccountTypeList() {
		List<SelectItem> accountTypeList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		AccountServiceAgent accountServiceAgent = new AccountServiceAgent();
		AccountTypeView accountTypeView = new AccountTypeView();
		accountTypeView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		accountTypeView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<AccountTypeView> accountTypeViewList = new ArrayList<AccountTypeView>(
				0);

		try {
			accountTypeViewList = accountServiceAgent
					.getAccountTypeList(accountTypeView);
		} catch (Exception e) {
			logger.LogException("getAccountTypeList()", e);
		}

		if (accountTypeViewList != null)
			for (AccountTypeView atv : accountTypeViewList)
				accountTypeList.add(new SelectItem(atv.getAccountTypeId()
						.toString(), isLocaleEnglish ? atv.getAccountTypeEn()
						: atv.getAccountTypeAr()));

		Collections.sort(accountTypeList, ListComparator.LIST_COMPARE);

		return accountTypeList;
	}

	public List<SelectItem> getExpectedRateList() {
		List<SelectItem> expectedRateList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		PortfolioServiceAgent portfolioServiceAgent = new PortfolioServiceAgent();
		ExpectedRateView expectedRateView = new ExpectedRateView();
		expectedRateView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		expectedRateView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<ExpectedRateView> expectedRateViewList = new ArrayList<ExpectedRateView>(
				0);

		try {
			expectedRateViewList = portfolioServiceAgent
					.getExpectedRateList(expectedRateView);
		} catch (Exception e) {
			logger.LogException("getAccountTypeList()", e);
		}

		if (expectedRateViewList != null)
			for (ExpectedRateView erv : expectedRateViewList)
				expectedRateList.add(new SelectItem(erv.getExpectedRateId(),
						isLocaleEnglish ? erv.getExpectedRateEn() : erv
								.getExpectedRateAr()));

		Collections.sort(expectedRateList, ListComparator.LIST_COMPARE);

		return expectedRateList;
	}

	public List<SelectItem> getAccountStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ACCOUNT_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.ACCOUNT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ACCOUNT_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ACCOUNT_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName()
							.compareTo(WebConstants.ACCOUNT_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ACCOUNT_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getRevenueAndExpenseStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("REVENUE_EXPENSE_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.EXPENSE_AND_REVENUE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REVENUE_EXPENSE_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("REVENUE_EXPENSE_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.EXPENSE_AND_REVENUE_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REVENUE_EXPENSE_STATUS_EN", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getRevenueAndExpenseType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("REVENUE_EXPENSE_TYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.EXPENSE_AND_REVENUE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REVENUE_EXPENSE_TYPE_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("REVENUE_EXPENSE_TYPE_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.EXPENSE_AND_REVENUE_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REVENUE_EXPENSE_TYPE_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getAssetStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("ASSET_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.ASSET_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ASSET_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("ASSET_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.ASSET_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("ASSET_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getCurrencyList() {
		List<SelectItem> currencyList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		CurrencyView currencyView = new CurrencyView();
		currencyView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		currencyView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<CurrencyView> currencyViewList = new ArrayList<CurrencyView>(0);

		try {
			currencyViewList = assetServiceAgent.getCurrencyList(currencyView);
		} catch (Exception e) {
			logger.LogException("getCurrencyList()", e);
		}

		if (currencyViewList != null)
			for (CurrencyView cV : currencyViewList)
				currencyList.add(new SelectItem(cV.getCurrencyId().toString(),
						isLocaleEnglish ? cV.getCurrencyNameEn() : cV
								.getCurrencyNameAr()));

		Collections.sort(currencyList, ListComparator.LIST_COMPARE);

		return currencyList;
	}

	public List<SelectItem> getAssetClassesList() {
		List<SelectItem> assetClassList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		AssetClassView assetClassView = new AssetClassView();
		assetClassView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		assetClassView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<AssetClassView> assetClassViewList = new ArrayList<AssetClassView>(
				0);

		try {
			assetClassViewList = assetServiceAgent
					.getAssetClasses(assetClassView);
		} catch (Exception e) {
			logger.LogException("getCurrencyList()", e);
		}

		if (assetClassViewList != null)
			for (AssetClassView aCV : assetClassViewList)
				assetClassList.add(new SelectItem(aCV.getAssetClassId()
						.toString(), isLocaleEnglish ? aCV
						.getAssetClassNameEn() : aCV.getAssetClassNameAr()));

		Collections.sort(assetClassList, ListComparator.LIST_COMPARE);

		return assetClassList;
	}

	public List<SelectItem> getSectorList() {
		List<SelectItem> sectorList1 = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		SectorView sectorView = new SectorView();
		sectorView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		sectorView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<SectorView> sectorViewList = new ArrayList<SectorView>(0);

		try {
			sectorViewList = assetServiceAgent.getSectorList(sectorView);
		} catch (Exception e) {
			logger.LogException("getSectorList", e);
		}

		if (sectorViewList != null)
			for (SectorView sV : sectorViewList)
				sectorList1.add(new SelectItem(sV.getSectorId().toString(),
						isLocaleEnglish ? sV.getSectorNameEn() : sV
								.getSectorNameAr()));

		Collections.sort(sectorList1, ListComparator.LIST_COMPARE);

		return sectorList1;
	}

	public List<SelectItem> getOrderStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.ORDER_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.ORDER_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Order.ORDER_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.ORDER_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.ORDER_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Order.ORDER_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getOrderTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.ORDER_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.ORDER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Order.ORDER_TYPE_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.ORDER_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.ORDER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.Order.ORDER_TYPE_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getTransactionLogList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.TRANSACTION_LOG_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.TRANSACTION_LOG) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(WebConstants.Order.TRANSACTION_LOG_LIST_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.Order.TRANSACTION_LOG_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Order.TRANSACTION_LOG) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(WebConstants.Order.TRANSACTION_LOG_LIST_AR,
								cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getMaritialStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("MARITIAL_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.MARITIAL_STATUS.MARITIAL_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("MARITIAL_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("MARITIAL_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.MARITIAL_STATUS.MARITIAL_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("MARITIAL_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPersonStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("PERSON_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Contractor.CONTRACTOR_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PERSON_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("PERSON_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Contractor.CONTRACTOR_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PERSON_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getOrderList() {

		List<SelectItem> transtionNoList = new ArrayList<SelectItem>();
		boolean isLocaleEnglish = isEnglishLocale();
		OrderServiceAgent orderServiceAgent = new OrderServiceAgent();
		OrderView orderView = new OrderView();
		orderView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		orderView.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<OrderView> orderViewList = new ArrayList<OrderView>();

		try {

			orderViewList = orderServiceAgent.getOrderList(orderView);
		} catch (Exception e) {
			logger.LogException("getCurrencyList()", e);
		}

		if (orderViewList != null)
			for (OrderView oV : orderViewList)
				transtionNoList.add(new SelectItem(oV.getOrderId().toString(),
						oV.getTransactionRefNo()));

		Collections.sort(transtionNoList, ListComparator.LIST_COMPARE);

		return transtionNoList;

	}

	public List<SelectItem> getWorkTypesList() {

		String methodName = "getWorkTypesList";
		logger.logInfo(methodName + "|started...");
		List<SelectItem> workTypeSelectList = new ArrayList();
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		try {

			if (isEnglishLocale()) {
				if (!listMap.containsKey("WorkTypesListEn")
						|| listMap.get("WorkTypesListEn") == null) {
					List<WorkTypeView> workTypeList = csa.getAllWorkTypes();
					if (workTypeList != null) {

						for (WorkTypeView workTypeView : workTypeList)
							workTypeSelectList.add(new SelectItem(workTypeView
									.getWorkTypeId().toString(), workTypeView
									.getDescriptionEn()));
						listMap.put("WorkTypesListEn", workTypeSelectList);
					}
				} else
					workTypeSelectList = (ArrayList<SelectItem>) listMap
							.get("WorkTypesListEn");
			}

			else if (!isEnglishLocale()) {
				if (!listMap.containsKey("WorkTypesListAr")
						|| listMap.get("WorkTypesListAr") == null) {
					List<WorkTypeView> workTypeList = csa.getAllWorkTypes();
					if (workTypeList != null) {

						for (WorkTypeView workTypeView : workTypeList)
							workTypeSelectList.add(new SelectItem(workTypeView
									.getWorkTypeId().toString(), workTypeView
									.getDescriptionAr()));

						listMap.put("WorkTypesListAr", workTypeSelectList);
					}
				} else
					workTypeSelectList = (ArrayList<SelectItem>) listMap
							.get("WorkTypesListAr");
			}
			Collections.sort(workTypeSelectList, ListComparator.LIST_COMPARE);
			logger.logInfo(methodName + "|completed successfully!!!");

		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured", e);
		}
		return workTypeSelectList;
	}

	public List<SelectItem> getMaintenanceTypesList() {

		String methodName = "getMaintenanceTypesList";
		logger.logInfo(methodName + "|started...");
		List<SelectItem> maintenanceTypeSelectList = new ArrayList();
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		String MaintenanceTypesListEn = "MaintenanceTypesListEn";
		String MaintenanceTypesListAr = "MaintenanceTypesListAr";
		try {

			if (isEnglishLocale()) {
				if (!listMap.containsKey(MaintenanceTypesListEn)
						|| listMap.get(MaintenanceTypesListEn) == null) {
					List<MaintenanceTypeView> maintenanceTypeList = csa
							.getAllMaintenanceTypes();
					if (maintenanceTypeList != null) {

						for (MaintenanceTypeView maintenanceTypeView : maintenanceTypeList) {
							maintenanceTypeSelectList.add(new SelectItem(
									maintenanceTypeView.getMaintenanceTypeId()
											.toString(), maintenanceTypeView
											.getDescriptionEn()));
						}
						listMap.put(MaintenanceTypesListEn,
								maintenanceTypeSelectList);
					}
				} else
					maintenanceTypeSelectList = (ArrayList<SelectItem>) listMap
							.get(MaintenanceTypesListEn);
			}

			else if (!isEnglishLocale()) {
				if (!listMap.containsKey(MaintenanceTypesListAr)
						|| listMap.get(MaintenanceTypesListAr) == null) {
					List<MaintenanceTypeView> maintenanceTypeList = csa
							.getAllMaintenanceTypes();
					if (maintenanceTypeList != null) {

						for (MaintenanceTypeView maintenanceTypeView : maintenanceTypeList) {
							maintenanceTypeSelectList.add(new SelectItem(
									maintenanceTypeView.getMaintenanceTypeId()
											.toString(), maintenanceTypeView
											.getDescriptionAr()));
						}
						listMap.put(MaintenanceTypesListAr,
								maintenanceTypeSelectList);
					}
				} else
					maintenanceTypeSelectList = (ArrayList<SelectItem>) listMap
							.get(MaintenanceTypesListAr);
			}
			Collections.sort(maintenanceTypeSelectList,
					ListComparator.LIST_COMPARE);
			logger.logInfo(methodName + "|completed successfully!!!");

		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured", e);
		}
		return maintenanceTypeSelectList;
	}

	public List<SelectItem> getRequestOriginationList() {
		Boolean isEnglishLocale = isEnglishLocale();
		String REQUEST_ORG_LIST_EN = "REQUEST_ORG_LIST_EN";
		String REQUEST_ORG_LIST_AR = "REQUEST_ORG_LIST_AR";
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(REQUEST_ORG_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_ORIGINATION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(REQUEST_ORG_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(REQUEST_ORG_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_ORIGINATION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put(REQUEST_ORG_LIST_AR, cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getTenderTypeNewList() {
		Boolean isEnglishLocale = isEnglishLocale();
		logger.logInfo("getTenderTypeList() started...");
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("tenderTypeNewListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_TYPE_NEW) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderTypeNewListEn", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("tenderTypeNewListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.Tender.TENDER_TYPE_NEW) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}

				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("tenderTypeNewListAr", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getIstisnaaMethodList() {
		Boolean isEnglishLocale = isEnglishLocale();
		String methodName = "getIstisnaaMethodList";
		logger.logInfo(methodName + "|started...");
		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("IstisnaaMethodListEn");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.IstisnaaMethod.ISTISNAA_METHOD) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("IstisnaaMethodListEn", cboList);
			}
			logger.logInfo(methodName + "|finish...");
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("IstisnaaMethodListAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.IstisnaaMethod.ISTISNAA_METHOD) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}

				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("IstisnaaMethodListAr", cboList);
			}
			logger.logInfo(methodName + "|finish...");
			return cboList;
		}
	}

	public List<SelectItem> getAllFollowUpActionsStatus() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		FollowUpServiceAgent followUpService = new FollowUpServiceAgent();

		try {
			List<FollowUpActionsView> listFollowUpActions = followUpService
					.getAllFollowUpActionsStatus();
			if (listFollowUpActions != null && listFollowUpActions.size() > 0) {
				for (FollowUpActionsView followUpAction : listFollowUpActions) {
					SelectItem item = new SelectItem();
					item.setValue(followUpAction.getFollowUpActionsId()
							.toString());

					if (isEnglishLocale) {
						String desc = (followUpAction.getFollowUpStatusEn() != null) ? followUpAction
								.getFollowUpStatusEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (followUpAction.getFollowUpStatusAr() != null) ? followUpAction
								.getFollowUpStatusAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					cboList.add(item);
				}
			}
		} catch (PimsBusinessException e) {
			logger.LogException(
					"Failed to load followup actions list due to :  ", e);
		}

		return cboList;

	}

	public List<SelectItem> getEvaluationRecommendationTypes() {
		return getDomainDataList(WebConstants.EvaluationApplication.EVALUATION_RECOMMENDATION_TYPE);
	}

	public List<SelectItem> getContractorStatus() {
		return getDomainDataList(WebConstants.CONTRACTOR_STATUS);
	}

	public List<SelectItem> getOpportunityStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS_LIST_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS_LIST_AR,
								cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getOpportunityTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE_LIST_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_TYPE_LIST_AR,
								cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getOpportunitySubjectList() {
		List<SelectItem> opportunitySubjectList = new ArrayList<SelectItem>(0);
		boolean isLocaleEnglish = isEnglishLocale();
		InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
		InvestmentOppSubjectView investmentOppSubjectView = new InvestmentOppSubjectView();
		investmentOppSubjectView.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		investmentOppSubjectView
				.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		List<InvestmentOppSubjectView> investmentOppSubjectViewList = new ArrayList<InvestmentOppSubjectView>(
				0);

		try {
			investmentOppSubjectViewList = investmentOpportunityServiceAgent
					.getInvestmentOppSubjectViewList(investmentOppSubjectView);
		} catch (Exception e) {
			logger.LogException("getOpportunitySubjectList()", e);
		}

		if (investmentOppSubjectViewList != null)
			for (InvestmentOppSubjectView iosv : investmentOppSubjectViewList)
				opportunitySubjectList.add(new SelectItem(iosv.getSubjectId()
						.toString(), isLocaleEnglish ? iosv.getSubjectEn()
						: iosv.getSubjectAr()));

		Collections.sort(opportunitySubjectList, ListComparator.LIST_COMPARE);

		return opportunitySubjectList;

	}

	public List<SelectItem> getOpportunityEvaluationTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE_LIST_EN);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE_LIST_EN,
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get(WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE_LIST_AR);
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv
							.getTypeName()
							.compareTo(
									WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put(
								WebConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_TYPE_LIST_AR,
								cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getXxAmafTransactionTypes() {

		GRPServiceAgent grpServiceAgent = new GRPServiceAgent();

		List<XXAmafTransactionTypesView> list = grpServiceAgent
				.getXXAmafTransactionTypes();
		List<SelectItem> cboList = listMap.get("XXAmafTransactionTypes");

		if (cboList == null) {
			cboList = new ArrayList<SelectItem>(0);
			for (XXAmafTransactionTypesView amafTransactionTypesView : list) {
				SelectItem item = new SelectItem(amafTransactionTypesView
						.getTrxTypeName(), amafTransactionTypesView
						.getDescription(), amafTransactionTypesView
						.getDescription());
				cboList.add(item);
			}
			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			listMap.put("XXAmafTransactionTypes", cboList);
		}
		return cboList;
	}

	public List<SelectItem> getRequestOrigination() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("REQUEST_ORIGINATION_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_ORIGINATION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REQUEST_ORIGINATION_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("REQUEST_ORIGINATION_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.REQUEST_ORIGINATION) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("REQUEST_ORIGINATION_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getXxAmafMemoLine() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> cboList = null;
		if (viewMap.get("XxAmafMemoLine") == null) {
			GRPServiceAgent grpServiceAgent = new GRPServiceAgent();
			Map<String, String> map = grpServiceAgent.getXXAmafMemoLinesMap();
			cboList = new ArrayList<SelectItem>(0);
			for (String key : map.keySet()) {
				SelectItem item = new SelectItem(key, map.get(key).toString());
				cboList.add(item);

			}

			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			viewMap.put("XxAmafMemoLine", cboList);
		} else {
			cboList = (ArrayList) viewMap.get("XxAmafMemoLine");
		}
		return cboList;

	}

	public List<SelectItem> getPrinterList() {
		List<SelectItem> printerList = listMap.get(WebConstants.PRINTER_LIST);

		if (printerList == null) {
			printerList = new ArrayList<SelectItem>(0);
			PrintService[] arrPrintService = PrintServiceLookup
					.lookupPrintServices(null, null);

			for (PrintService printService : arrPrintService) {
				printerList.add(new SelectItem(printService.getName(),
						printService.getName()));
			}

			listMap.put(WebConstants.PRINTER_LIST, printerList);
		}

		return printerList;
	}

	public List<SelectItem> getXxAmafReceiptType() {

		GRPServiceAgent grpServiceAgent = new GRPServiceAgent();

		Map<String, String> map = grpServiceAgent.getXXAmafReceiptType();

		List<SelectItem> cboList = listMap.get("XxAmafReceiptType");

		if (cboList == null) {
			cboList = new ArrayList<SelectItem>(0);
			for (String key : map.keySet()) {
				SelectItem item = new SelectItem(key, map.get(key).toString());
				cboList.add(item);

			}
			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			listMap.put("XxAmafReceiptType", cboList);
		}
		return cboList;
	}

	public List<SelectItem> getXxAmafReceiveableActivity() {

		GRPServiceAgent grpServiceAgent = new GRPServiceAgent();

		Map<String, String> map = grpServiceAgent
				.getXXAmafReceiveableActivity();

		List<SelectItem> cboList = listMap.get("XxAmafReceiveableActivity");

		if (cboList == null) {
			cboList = new ArrayList<SelectItem>(0);
			for (String key : map.keySet()) {
				if (map.get(key) != null) {
					SelectItem item = new SelectItem(key, map.get(key)
							.toString());
					cboList.add(item);
				}

			}
			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			listMap.put("XxAmafReceiveableActivity", cboList);
		}
		return cboList;
	}

	public List<SelectItem> getGRPStatus() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("GRP_STATUS_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.GRP_TRANSACTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("GRP_STATUS_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("GRP_STATUS_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.GRP.GRP_TRANSACTION_STATUS) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("GRP_STATUS_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getFinTranType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("FIN_TRX_TYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.TransactionType.TRX_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("FIN_TRX_TYPE_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("FIN_TRX_TYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.TransactionType.TRX_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("FIN_TRX_TYPE_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getXxAmafCountry() {

		GRPServiceAgent grpServiceAgent = new GRPServiceAgent();
		Map<String, String> map = grpServiceAgent.getXXAmafCountry();
		List<SelectItem> cboList = listMap.get("XxAmafCountry");
		if (cboList == null) {
			cboList = new ArrayList<SelectItem>(0);
			for (String key : map.keySet()) {
				SelectItem item = new SelectItem(key, map.get(key).toString()
						+ " - ( " + key.toUpperCase() + " )");
				cboList.add(item);
			}
			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			listMap.put("XxAmafCountry", cboList);
		}
		return cboList;
	}

	public List<SelectItem> getConstructionPaymentTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("CONSTRUCTION_PAYMENT_TYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CONSTRUCTION_PAYMENT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("CONSTRUCTION_PAYMENT_TYPE_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("CONSTRUCTION_PAYMENT_TYPE_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.CONSTRUCTION_PAYMENT_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("CONSTRUCTION_PAYMENT_TYPE_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPaymentScheduleSubType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("PAYMENT_SCHEDULE_SUBTYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_SUB_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PAYMENT_SCHEDULE_SUBTYPE_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("PAYMENT_SCHEDULE_SUBTYPE_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PAYMENT_SCHEDULE_SUB_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PAYMENT_SCHEDULE_SUBTYPE_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getInvestmentBasedOn() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap.get("INVESTMENT_BASED_ON_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INVESTMENT_COST_BASED_ON) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("INVESTMENT_BASED_ON_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get("INVESTMENT_BASED_ON_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.INVESTMENT_COST_BASED_ON) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("INVESTMENT_BASED_ON_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPortfolioEvaluationType() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("PORTFOLIO_EVALUATION_TYPE_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PORTFOLIO_EVALUATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PORTFOLIO_EVALUATION_TYPE_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("PORTFOLIO_EVALUATION_TYPE_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PORTFOLIO_EVALUATION_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PORTFOLIO_EVALUATION_TYPE_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getPortfolioEvaluationQuarter() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("PORTFOLIO_EVALUATION_QUARTER_EN");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PORTFOLIO_EVALUATION_QUARTER) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PORTFOLIO_EVALUATION_QUARTER_EN", cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("PORTFOLIO_EVALUATION_QUARTER_AR");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(
							WebConstants.PORTFOLIO_EVALUATION_QUARTER) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PORTFOLIO_EVALUATION_QUARTER_AR", cboList);
			}
			return cboList;
		}
	}

	public List<SelectItem> getUserNameList() {
		List<SelectItem> userList = new ArrayList<SelectItem>();
		try {
			HashMap<String, UserView> userMap = new UtilityService()
					.getUsersMap(new UserView());
			for (String key : userMap.keySet()) {
				String arName = "";
				UserView user = userMap.get(key);
				if (user.getFullNameSecondary() != null)
					arName = user.getFullNameSecondary();
				else
					arName = user.getFullNamePrimary();
				SelectItem item = new SelectItem(user.getUserName(),
						(CommonUtil.getIsEnglishLocale() ? user
								.getFullNamePrimary() : arName));
				userList.add(item);
			}
			Collections.sort(userList, ListComparator.LIST_COMPARE);

		} catch (Exception e) {
			logger.LogException("getUserNameList() crashed : ", e);
		}
		return userList;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getBankRemittanceAccNameList() {
		String methodName = "getBankRemittanceAccNameList|";
		logger.logInfo(methodName + "Start");
		List<SelectItem> itemList = new ArrayList<SelectItem>(0);

		for (XxAmafBankRemitAccountView pojo : (ArrayList<XxAmafBankRemitAccountView>) new GRPServiceAgent()
				.getXXAmafBankRemitAccountView()) {
			SelectItem item = new SelectItem(pojo.getBankAccountName(), pojo
					.getBankAccountName(), pojo.getBankAccountName());
			itemList.add(item);
		}

		logger.logInfo(methodName + "Finish");
		return itemList;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getUniqueBankRemittanceAccNameList() {
		String methodName = "getUniqueBankRemittanceAccNameList|";
		logger.logInfo(methodName + "Start");
		List<SelectItem> itemList = new ArrayList<SelectItem>(0);
		HashMap<String, String> itemMap = new HashMap<String, String>(0);
		for (XxAmafBankRemitAccountView pojo : (ArrayList<XxAmafBankRemitAccountView>) new GRPServiceAgent()
				.getXXAmafBankRemitAccountView()) {
			if (!itemMap.containsKey(pojo.getBankAccountName())) {

				SelectItem item = new SelectItem(pojo.getBankAccountName(),
						pojo.getBankAccountName(), pojo.getBankAccountName());
				itemList.add(item);
				itemMap.put(pojo.getBankAccountName(), pojo
						.getBankAccountName());
			}
		}

		logger.logInfo(methodName + "Finish");
		return itemList;

	}

	// public List<SelectItem> getProjectCategoriesList() {
	//		
	// String methodName="getProjectCategoriesList";
	// logger.logInfo(methodName+"|started...");
	// List<SelectItem> projectCategoryItemList = new ArrayList();
	// ConstructionServiceAgent csa = new ConstructionServiceAgent();
	// try{
	//			
	// if(isEnglishLocale() )
	// {
	// if(!listMap.containsKey("ProjectCategoryListEn")||
	// listMap.get("ProjectCategoryListEn")==null){
	// List<ProjectCategoryView> projectCategoriesList =
	// csa.getAllProjectCategories();
	// if(projectCategoriesList!=null) {
	//									
	// for (ProjectCategoryView category: projectCategoriesList) {
	// projectCategoryItemList.add(new
	// SelectItem(category.getProjectCategoryId().toString(),category.getDescriptionEn()));
	// }
	// listMap.put("ProjectCategoryListEn",projectCategoryItemList);
	// }
	// }
	// else
	// projectCategoryItemList=
	// (ArrayList<SelectItem>)listMap.get("ProjectCategoryListEn");
	// }
	//							
	// else if(!isEnglishLocale() )
	// {
	// if(!listMap.containsKey("ProjectCategoryListAr")||
	// listMap.get("ProjectCategoryListAr")==null){
	// List<ProjectCategoryView> projectCategoriesList =
	// csa.getAllProjectCategories();
	// if(projectCategoriesList!=null)
	// {
	//									
	// for (ProjectCategoryView category: projectCategoriesList)
	// {
	// projectCategoryItemList.add(new
	// SelectItem(category.getProjectCategoryId().toString(),category.getDescriptionAr()));
	// }
	// listMap.put("ProjectCategoryListAr",projectCategoryItemList);
	// }
	// }
	// else
	// projectCategoryItemList=
	// (ArrayList<SelectItem>)listMap.get("WorkTypesListAr");
	// }
	// Collections.sort(projectCategoryItemList, ListComparator.LIST_COMPARE);
	// logger.logInfo(methodName+"|completed successfully!!!");
	//			
	// }
	// catch(Exception e)
	// {
	// logger.LogException(methodName+"|Error Occured",e);
	// }
	// return projectCategoryItemList;
	// }
	public List<SelectItem> getPaymentMethodsFromPMTable() {

		List<SelectItem> paymentMethodsItem = new ArrayList();
		UtilityServiceAgent usa = new UtilityServiceAgent();
		try {

			if (isEnglishLocale()) {
				if (!listMap.containsKey("description")
						|| listMap.get("description") == null) {
					List<PaymentMethodView> paymentMethodList = usa
							.getAllPaymentMethods();
					if (paymentMethodList != null) {

						for (PaymentMethodView pMethod : paymentMethodList)
							paymentMethodsItem.add(new SelectItem(pMethod
									.getPaymentMethodId().toString(), pMethod
									.getDescription()));
						listMap.put("description", paymentMethodsItem);
					}
				} else
					paymentMethodsItem = (ArrayList<SelectItem>) listMap
							.get("description");
			}

			else if (!isEnglishLocale()) {
				if (!listMap.containsKey("descriptionAr")
						|| listMap.get("descriptionAr") == null) {
					List<PaymentMethodView> paymentMethodList = usa
							.getAllPaymentMethods();
					if (paymentMethodList != null) {
						for (PaymentMethodView pMethod : paymentMethodList)
							paymentMethodsItem.add(new SelectItem(pMethod
									.getPaymentMethodId().toString(), pMethod
									.getDescriptionAr()));
						listMap.put("descriptionAr", paymentMethodsItem);
					}
				} else
					paymentMethodsItem = (ArrayList<SelectItem>) listMap
							.get("descriptionAr");
			}
			Collections.sort(paymentMethodsItem, ListComparator.LIST_COMPARE);

		} catch (Exception e) {
			logger
					.LogException("getPaymentMethodsFromPMTable|Error Occured",
							e);
		}
		return paymentMethodsItem;
	}

	/**
	 * Return Inheritence File Status List
	 * 
	 * @author imran.mirza
	 * 
	 */
	public List<SelectItem> getInheritenceFileStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> inheritenceFileStatusList = new ArrayList<SelectItem>(
				0);
		List<DomainTypeView> list = (List<DomainTypeView>) context
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		if (list != null) {
			Iterator<DomainTypeView> itr = list.iterator();
			while (itr.hasNext()) {
				DomainTypeView dtv = itr.next();
				if (dtv.getTypeName().compareTo(
						WebConstants.InheritanceFileStatus.INH_FILE_STATUS) == 0) {
					Set<DomainDataView> dd = dtv.getDomainDatas();
					SelectItem item = null;
					for (DomainDataView ddv : dd)
						inheritenceFileStatusList.add(new SelectItem(ddv
								.getDomainDataId().toString(),
								isEnglishLocale ? ddv.getDataDescEn() : ddv
										.getDataDescAr()));

					break;
				}
			}
			Collections.sort(inheritenceFileStatusList,
					ListComparator.LIST_COMPARE);
		}
		return inheritenceFileStatusList;
	}

	/**
	 * Return Inheritence File Type List
	 * 
	 * @author imran.mirza
	 * 
	 */
	public List<SelectItem> getInheritenceFileTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> inheritenceFileTypeList = new ArrayList<SelectItem>(0);
		List<DomainTypeView> list = (List<DomainTypeView>) context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		if (list != null) 
		{
			Iterator<DomainTypeView> itr = list.iterator();
			while (itr.hasNext()) 
			{
				DomainTypeView dtv = itr.next();
				if (dtv.getTypeName().compareTo(WebConstants.InheritanceFileType.INH_FILE_TYPE) == 0) 
				{
					Set<DomainDataView> dd = dtv.getDomainDatas();
					SelectItem item = null;
					for (DomainDataView ddv : dd)
						inheritenceFileTypeList.add(
								new SelectItem(ddv.getDomainDataId().toString(),
								isEnglishLocale ? ddv.getDataDescEn() : ddv.getDataDescAr()));

					break;
				}
			}
			Collections.sort(inheritenceFileTypeList,
					ListComparator.LIST_COMPARE);
		}
		return inheritenceFileTypeList;
	}

	/**
	 * Return Inheritence Asset Type List
	 * 
	 * @author imran.mirza
	 * 
	 */
	public List<SelectItem> getFileAssetTypeList() {

		Boolean isEnglishLocale = isEnglishLocale();
		List<AssetType> assetTypeList = new ArrayList<AssetType>(0);
		List<SelectItem> fileAssetTypeList = new ArrayList<SelectItem>(0);
		InheritanceFileService inheritenceFileService = new InheritanceFileService();
		try {
			assetTypeList = inheritenceFileService.getAllActiveFileAssetType();
		} catch (Exception e) {
			logger.LogException("getFileAssetTypeList()", e);
		}

		if (fileAssetTypeList != null)
			for (AssetType assetType : assetTypeList)
				fileAssetTypeList
						.add(new SelectItem(assetType.getAssetTypeId()
								.toString(), isEnglishLocale ? assetType
								.getAssetTypeNameEn() : assetType
								.getAssetTypeNameAr()));

		Collections.sort(fileAssetTypeList, ListComparator.LIST_COMPARE);

		return fileAssetTypeList;
	}

	/**
	 * Return Income Category List
	 * 
	 * @author Kamran Ahmed
	 * 
	 */
	public List<SelectItem> getIncomeCategories() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> incomeCategories = new ArrayList<SelectItem>(0);
		List<DomainTypeView> list = (List<DomainTypeView>) context
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		if (list != null) {
			Iterator<DomainTypeView> itr = list.iterator();
			while (itr.hasNext()) {
				DomainTypeView dtv = itr.next();
				if (dtv.getTypeName().compareTo(
						WebConstants.InheritanceFileType.INCOME_CATEGORY) == 0) {
					Set<DomainDataView> dd = dtv.getDomainDatas();
					SelectItem item = null;
					for (DomainDataView ddv : dd)
						incomeCategories.add(new SelectItem(ddv
								.getDomainDataId().toString(),
								isEnglishLocale ? ddv.getDataDescEn() : ddv
										.getDataDescAr()));

					break;
				}
			}
			Collections.sort(incomeCategories, ListComparator.LIST_COMPARE);
		}
		return incomeCategories;
	}

	/**
	 * Return Income Category List
	 * 
	 * @author Kamran Ahmed
	 * 
	 */
	public List<SelectItem> getRelationList() {

		Boolean isEnglishLocale = isEnglishLocale();
		List<RelationshipView> relationshipsList = new ArrayList<RelationshipView>(
				0);
		List<SelectItem> relationshipTypeList = new ArrayList<SelectItem>(0);
		InheritanceFileService inheritenceFileService = new InheritanceFileService();
		try {
			relationshipsList = inheritenceFileService.getAllRelationships();
		} catch (PimsBusinessException e) {
			logger.LogException("getRelationList() crashed |", e);
		}

		if (relationshipTypeList != null)
			for (RelationshipView relationship : relationshipsList)
				relationshipTypeList.add(new SelectItem(relationship
						.getRelationshipId().toString(),
						isEnglishLocale ? relationship.getRelationshipDescEn()
								: relationship.getRelationshipDescAr()));

		Collections.sort(relationshipTypeList, ListComparator.LIST_COMPARE);

		return relationshipTypeList;
	}

	public List<SelectItem> getAssetTypesList() throws PimsBusinessException {

		if (viewMap.get("ASSET_TYPE_LIST") != null) {

			return (List<SelectItem>) viewMap.get("ASSET_TYPE_LIST");
		}
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<SearchAssetsCriteriaView> assetTypesList = new ArrayList<SearchAssetsCriteriaView>();
		assetTypesList = new SearchAssetsService().getAssetTypesList();
		if (assetTypesList != null && assetTypesList.size() > 0) {
			for (SearchAssetsCriteriaView assets : assetTypesList) {
				SelectItem item;
				item = new SelectItem(assets.getAssetTypeid().toString(),
						isEnglishLocale() ? assets.getAssetNameEn() : assets
								.getAssetNameAr());
				items.add(item);
			}
		}
		Collections.sort(items, ListComparator.LIST_COMPARE);
		viewMap.put("ASSET_TYPE_LIST", items);
		return items;
	}

	public List<SelectItem> getGovtDepttList() throws Exception {
		List<SelectItem> govtDepttList = new ArrayList<SelectItem>(0);
		List<GovtDepttView> govtDepttListView = new ArrayList<GovtDepttView>(0);

		govtDepttListView = new InheritanceFileService().getAllGovtDeptt();

		if (govtDepttListView != null)
			for (GovtDepttView gdv : govtDepttListView)
				govtDepttList.add(new SelectItem(gdv.getGovtDepttId()
						.toString(), isEnglishLocale() ? gdv.getGovtDepttEn()
						: gdv.getGovtDepttAr()));

		Collections.sort(govtDepttList, ListComparator.LIST_COMPARE);

		return govtDepttList;
	}

	/**
	 * Return Asset revenue Type List
	 * 
	 * @author Kamran Ahmed
	 * 
	 */
	public List<SelectItem> getRevenueTypeList() {
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> assetRevenueTypes = new ArrayList<SelectItem>(0);
		List<DomainTypeView> list = (List<DomainTypeView>) context
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		if (list != null) {
			Iterator<DomainTypeView> itr = list.iterator();
			while (itr.hasNext()) {
				DomainTypeView dtv = itr.next();
				if (dtv.getTypeName().compareTo(
						WebConstants.InheritanceFile.ASSET_REVENUE_TYPE) == 0) {
					Set<DomainDataView> dd = dtv.getDomainDatas();
					SelectItem item = null;
					for (DomainDataView ddv : dd)
						assetRevenueTypes.add(new SelectItem(ddv
								.getDomainDataId().toString(),
								isEnglishLocale ? ddv.getDataDescEn() : ddv
										.getDataDescAr()));

					break;
				}
			}
			Collections.sort(assetRevenueTypes, ListComparator.LIST_COMPARE);
		}
		return assetRevenueTypes;
	}

	public List<SelectItem> getMemsFollowupTypeList() {

		List<SelectItem> selectItemList = new ArrayList<SelectItem>(0);
		Boolean isEnglishLocale = isEnglishLocale();
		try {
			List<MemsFollowupView> list = new MemsFollowupService()
					.getMemsFollowupTypeList();
			if (list != null) {
				for (MemsFollowupView vo : list) {
					SelectItem item = new SelectItem(vo.getTypeId().toString(),
							isEnglishLocale ? vo.getTypeEn() : vo.getTypeAr());
					selectItemList.add(item);
				}

				Collections.sort(selectItemList, ListComparator.LIST_COMPARE);
			}
		} catch (Exception e) {
			logger.logInfo("getMemsFollowupTypeList|Error Occured:", e);

		}
		return selectItemList;
	}

	public List<SelectItem> getFollowupCategory() {
		Boolean isEnglishLocale = isEnglishLocale();
		String listNameEn = "FollowupCategoryListEn";
		String listNameAr = "FollowupCategoryListAr";
		String localeBasedCurrentList;
		List<SelectItem> cboList = null;
		if (isEnglishLocale) {
			localeBasedCurrentList = listNameEn;
		} else {
			localeBasedCurrentList = listNameAr;

		}
		cboList = listMap.get(localeBasedCurrentList);
		if (cboList == null) {
			List<DomainTypeView> list = (List<DomainTypeView>) context
					.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
			Iterator<DomainTypeView> itr = list.iterator();
			while (itr.hasNext()) {
				DomainTypeView dtv = itr.next();
				if (dtv.getTypeName().compareTo(
						WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE) == 0) {
					Set<DomainDataView> dd = dtv.getDomainDatas();
					cboList = new ArrayList<SelectItem>();
					SelectItem item = null;
					for (DomainDataView ddv : dd) {
						if (isEnglishLocale)
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
						else
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
						cboList.add(item);
					}
					break;
				}
			}
			if (cboList == null) {
				return emptyList;
			}
			Collections.sort(cboList, ListComparator.LIST_COMPARE);
			listMap.put(localeBasedCurrentList, cboList);
		}
		return cboList;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementTypesList() {

		HashMap<Long, String> disbursementTypeList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();

		try {
			disbursementTypeList = service
					.getDisbursementTypeList(isEnglishLocale());

			Set s = disbursementTypeList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
		} catch (Exception e) {
			logger.LogException("getDisbursementTypesList() crashed", e);
		}
		return selectItems;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementSourceList() {

		HashMap<Long, String> disbursementSourceList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();

		try {
			disbursementSourceList = service
					.getDisbursementSourceList(CommonUtil.getIsEnglishLocale());

			Set s = disbursementSourceList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}

			Collections.sort(selectItems, ListComparator.LIST_COMPARE);

		} catch (PimsBusinessException e) {
			logger.LogException(" getDisbursementSourceList() crashed", e);
		} catch (Exception e) {
			logger.LogException(" getDisbursementSourceList() crashed", e);
		}
		return selectItems;

	}

	public static List<SelectItem> getInheritedAssetList(
			InheritedAssetView inheritedAssetView,
			Map<String, InheritedAssetView> inheritedAssetMap) throws Exception {
		List<SelectItem> inheritedAssetSelectItems = new ArrayList<SelectItem>(
				0);

		boolean isLocaleEnglish = CommonUtil.getIsEnglishLocale();

		InheritedAssetService inheritedAssetService = new InheritedAssetService();
		List<InheritedAssetView> inheritedAssetViewList = new ArrayList<InheritedAssetView>(
				0);

		try {
			inheritedAssetViewList = inheritedAssetService
					.getInheritedAssetList(inheritedAssetView);
		} catch (Exception e) {
			throw e;
		}

		if (inheritedAssetViewList != null) {
			for (InheritedAssetView iav : inheritedAssetViewList) {
				if (iav.getStatus().compareTo(
						WebConstants.InheritedAssetStatus.INACTIVE_ID) == 0) {
					continue;
				}
				inheritedAssetSelectItems.add(new SelectItem(iav
						.getInheritedAssetId().toString(),
						isLocaleEnglish ? iav.getAssetMemsView()
								.getAssetNameEn() : iav.getAssetMemsView()
								.getAssetNameAr()));
				inheritedAssetMap
						.put(iav.getInheritedAssetId().toString(), iav);
			}
		}

		Collections
				.sort(inheritedAssetSelectItems, ListComparator.LIST_COMPARE);

		return inheritedAssetSelectItems;
	}

	public static Map<String, InheritedAssetSharesView> getInheritedAssetShareViewMap(
			InheritedAssetSharesView inheritedAssetSharesView,
			List<SelectItem> beneficiarySelectItems) throws Exception {
		Map<String, InheritedAssetSharesView> inheritedAssetSharesViewMap = new HashMap<String, InheritedAssetSharesView>(
				0);

		List<InheritedAssetSharesView> inheritedAssetSharesViewList = new ArrayList<InheritedAssetSharesView>(
				0);
		TakharujService takharujService = new TakharujService();
		try {
			inheritedAssetSharesViewList = takharujService
					.getInheritedAssetSharesViewList(inheritedAssetSharesView);
		} catch (Exception e) {
			throw e;
		}

		if (inheritedAssetSharesViewList != null
				&& inheritedAssetSharesViewList.size() > 0) {
			for (InheritedAssetSharesView iasv : inheritedAssetSharesViewList) {
				inheritedAssetSharesViewMap.put(iasv
						.getInheritedAssetSharesId().toString(), iasv);
				beneficiarySelectItems.add(new SelectItem(iasv
						.getInheritedAssetSharesId().toString(), iasv
						.getInheritanceBeneficiaryView().getBeneficiary()
						.getPersonFullName()));
			}
		}

		return inheritedAssetSharesViewMap;
	}

	public List<SelectItem> getAllUserGroups() throws Exception {
		List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
		List<SelectItem> listUserGrps = new ArrayList<SelectItem>(allUserGrps
				.size());
		for (UserGroup singleGrp : allUserGrps) {
			listUserGrps.add(new SelectItem(singleGrp.getUserGroupId(),
					isEnglishLocale() ? singleGrp.getPrimaryName() : singleGrp
							.getSecondaryName()));
		}
		Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
		return listUserGrps;
	}

	public List<SelectItem> getDistributionCriteriaWithoutGeneralAccount()
			throws Exception {

		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(WebConstants.DistributionCriteria.DISTRIBUTION_CRITERIA);
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (DomainDataView domainDataView : list) {
			if (!domainDataView.getDataValue().equals(
					WebConstants.DistributionCriteria.GENERAL_ACCOUNT)) {
				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());

				itemList.add(item);

			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);

		}
		return itemList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getSocialProgramTargetGrpList() {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		Map<String, String> map = new HashMap<String, String>();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("SocialProgramTargetGrpList") != null) {

				map = (Map<String, String>) viewMap
						.get("SocialProgramTargetGrpList");
			} else {
				map = new SocialProgramService()
						.getProgramTargetGroupList(CommonUtil
								.getIsEnglishLocale());
				viewMap.put("SocialProgramTargetGrpList", map);
			}

		}

		catch (Exception e) {
			logger.LogException("getSocialProgramTypeList() crashed", e);
		}
		return map;

	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getSocialProgramTypeList() {

		Map<String, String> map = new HashMap<String, String>();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();

		try {
			if (viewMap.get("SocialProgramTypeList") != null) {

				map = (Map<String, String>) viewMap
						.get("SocialProgramTypeList");
			} else {
				map = new SocialProgramService().getProgramTypeList(CommonUtil
						.getIsEnglishLocale());
				viewMap.put("SocialProgramTypeList", map);
			}

		}

		catch (Exception e) {
			logger.LogException("getSocialProgramTypeList() crashed", e);
		}
		return map;

	}

	public List<SelectItem> getAllHealthStatus() {
		List<HealthStatus> healthStatusList = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			healthStatusList = us.getAllHealthStatus();
			if (healthStatusList != null && healthStatusList.size() > 0) {
				for (HealthStatus healthStatus : healthStatusList) {
					SelectItem item = new SelectItem();
					item.setValue(healthStatus.getHealthStatusId().toString());

					if (isEnglishLocale()) {
						String desc = (healthStatus.getDescriptionEn() != null) ? healthStatus
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (healthStatus.getDescriptionAr() != null) ? healthStatus
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getAllDiseaseType() {
		List<DiseaseType> diseaseTypeList = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			diseaseTypeList = us.getAllDiseaseType();
			if (diseaseTypeList != null && diseaseTypeList.size() > 0) {
				for (DiseaseType diseaseType : diseaseTypeList) {
					SelectItem item = new SelectItem();
					item.setValue(diseaseType.getDiseaseTypeId().toString());

					if (isEnglishLocale()) {
						String desc = (diseaseType.getDescriptionEn() != null) ? diseaseType
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (diseaseType.getDescriptionAr() != null) ? diseaseType
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getAllOrphanCategories() {
		List<OrphanCategory> orphanCatList = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			orphanCatList = us.getAllOrphanCategories();
			if (orphanCatList != null && orphanCatList.size() > 0) {
				for (OrphanCategory orphanCat : orphanCatList) {
					SelectItem item = new SelectItem();
					item.setValue(orphanCat.getOrphanCategoryId().toString());

					if (isEnglishLocale()) {
						String desc = (orphanCat.getDescriptionEn() != null) ? orphanCat
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (orphanCat.getDescriptionAr() != null) ? orphanCat
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getAllHobbies() {
		List<HobbyType> hobbies = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			hobbies = us.getAllHobbies();
			if (hobbies != null && hobbies.size() > 0) {
				for (HobbyType hobby : hobbies) {
					SelectItem item = new SelectItem();
					item.setValue(hobby.getHobbyTypeId().toString());

					if (isEnglishLocale()) {
						String desc = (hobby.getDescriptionEn() != null) ? hobby
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (hobby.getDescriptionAr() != null) ? hobby
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getAllEducationLevel() {
		List<EducationLevel> levels = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			levels = us.getAllEducationLevel();
			if (levels != null && levels.size() > 0) {
				for (EducationLevel level : levels) {
					SelectItem item = new SelectItem();
					item.setValue(level.getEducationLevelId().toString());

					if (isEnglishLocale()) {
						String desc = (level.getDescriptionEn() != null) ? level
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (level.getDescriptionAr() != null) ? level
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getAllGrades() {
		List<Grade> grades = null;
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		try {
			grades = us.getAllGrades();
			if (grades != null && grades.size() > 0) {
				for (Grade grade : grades) {
					SelectItem item = new SelectItem();
					item.setValue(grade.getGradeId().toString());

					if (isEnglishLocale()) {
						String desc = (grade.getDescriptionEn() != null) ? grade
								.getDescriptionEn().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					} else {
						String desc = (grade.getDescriptionAr() != null) ? grade
								.getDescriptionAr().toString()
								: "";
						item.setDescription(desc);
						item.setLabel(desc);
					}

					itemList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return itemList;
	}

	public List<SelectItem> getDubaiCommunity() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(WebConstants.DUBAI_COMMUNITIES)) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap
						.get(WebConstants.DUBAI_COMMUNITIES);
			} else {
				// get data from db and put prepared list in viewMap
				Set<RegionView> dubaiCommunity = null;
				dubaiCommunity = new PropertyServiceAgent()
						.getState(WebConstants.DUBAI_REGION_ID);
				if (dubaiCommunity != null && dubaiCommunity.size() > 0) {
					for (RegionView community : dubaiCommunity) {
						SelectItem item = null;
						if (isEnglishLocale()) {
							item = new SelectItem(community.getRegionId()
									.toString(), community.getDescriptionEn());
						} else {
							item = new SelectItem(community.getRegionId()
									.toString(), community.getDescriptionAr());
						}
						items.add(item);
					}
					Collections.sort(items, ListComparator.LIST_COMPARE);
				}
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.DUBAI_COMMUNITIES, items);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	public List<SelectItem> getDonationBoxStatus() throws Exception {
		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(WebConstants.DomainTypes.DONATION_BOX_STATUS);
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (DomainDataView domainDataView : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				itemList.add(item);
			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
		}
		return itemList;
	}

	public List<SelectItem> getDonationBoxPlaces() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(WebConstants.DONATION_BOX_PLACES)) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap
						.get(WebConstants.DONATION_BOX_PLACES);
			} else {
				// get data from db and put prepared list in viewMap
				List<BoxPlaceTypeView> places = null;
				places = new DonationBoxService().getAllDonationBoxPlaces();
				if (places != null && places.size() > 0) {
					for (BoxPlaceTypeView place : places) {
						SelectItem item = null;
						if (isEnglishLocale()) {
							item = new SelectItem(place.getTypeId().toString(),
									place.getTypeNameEn());
						} else {
							item = new SelectItem(place.getTypeId().toString(),
									place.getTypeNameAr());
						}
						items.add(item);
					}
					Collections.sort(items, ListComparator.LIST_COMPARE);
				}
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.DONATION_BOX_PLACES, items);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	public List<SelectItem> getDonationBoxSubPlaces() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(WebConstants.DONATION_BOX_SUB_PLACES)) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap
						.get(WebConstants.DONATION_BOX_SUB_PLACES);
			} else {
				// get data from db and put prepared list in viewMap
				List<BoxPlaceSubType> places = null;
				places = new DonationBoxService().getAllDonationBoxSubPlaces();
				for (BoxPlaceSubType place : places) {
					SelectItem item = null;
					String name;
					if (isEnglishLocale()) {
						name = place.getBoxPlaceType().getTypeNameEn() + " - "
								+ place.getSubTypeNameEn();
					} else {
						name = place.getBoxPlaceType().getTypeNameAr() + " - "
								+ place.getSubTypeNameAr();
					}
					item = new SelectItem(place.getSubTypeId().toString(), name);
					items.add(item);
				}
				Collections.sort(items, ListComparator.LIST_COMPARE);
			}
			if (items != null && items.size() > 0) {
				viewMap.put(WebConstants.DONATION_BOX_SUB_PLACES, items);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	public List<SelectItem> getSecUserList() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(WebConstants.SEC_USER_LIST)) != null) {
				items = (List<SelectItem>) viewMap
						.get(WebConstants.SEC_USER_LIST);
			} else {
				items = CommonUtil.getSecUserList();
				Collections.sort(items, ListComparator.LIST_COMPARE);
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.SEC_USER_LIST, items);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getEndowmentFileStatus() throws Exception {
		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(Constant.EndowmentFileStatus.ENDOWMENT_FILE_STATUS);
		if (viewMap.get(Constant.EndowmentFileStatus.ENDOWMENT_FILE_STATUS) != null) {
			return (ArrayList<SelectItem>) viewMap
					.get(Constant.EndowmentFileStatus.ENDOWMENT_FILE_STATUS);
		}
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (DomainDataView domainDataView : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				itemList.add(item);
			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
			viewMap.put(Constant.EndowmentFileStatus.ENDOWMENT_FILE_STATUS,
					itemList);
		}
		return itemList;
	}

	public List<SelectItem> getEndowmentCategories() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(Constant.EndowmentCategory.EndowmentCategory)) != null) {
				items = (List<SelectItem>) viewMap
						.get(Constant.EndowmentCategory.EndowmentCategory);
				return items;
			}
			// get data from db and put prepared list in viewMap
			List<EndowmentCategory> list = null;
			list = new EndowmentService().getEndowmentsCategories();
			if (list == null && list.size() <= 0) {
				return items;
			}
			for (EndowmentCategory obj : list) {
				SelectItem item = null;
				item = new SelectItem(obj.getCategoryId().toString(),
						CommonUtil.getIsEnglishLocale() ? obj
								.getCategoryNameEn() : obj.getCategoryNameAr());
				items.add(item);
			}
			Collections.sort(items, ListComparator.LIST_COMPARE);
			viewMap.put(Constant.EndowmentCategory.EndowmentCategory, items);
		} catch (Exception e) {
			logger.LogException("getEndowmentCategories|Error occured", e);
		}
		return items;
	}

	public List<SelectItem> getEndowmentPurposes() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(Constant.EndowmentPurpose.EndowmentPurpose)) != null) {
				items = (List<SelectItem>) viewMap
						.get(Constant.EndowmentPurpose.EndowmentPurpose);
				return items;
			}
			// get data from db and put prepared list in viewMap
			List<EndowmentPurpose> list = null;
			list = new EndowmentService().getEndowmentsPurposes();
			if (list == null && list.size() <= 0) {
				return items;
			}
			for (EndowmentPurpose obj : list) {
				SelectItem item = null;
				item = new SelectItem(obj.getPurposeId().toString(), CommonUtil
						.getIsEnglishLocale() ? obj.getPurposeNameEn() : obj
						.getPurposeNameAr());
				items.add(item);
			}
			Collections.sort(items, ListComparator.LIST_COMPARE);
			viewMap.put(Constant.EndowmentPurpose.EndowmentPurpose, items);
		} catch (Exception e) {
			logger.LogException("getEndowmentPurposes|Error occured", e);
		}
		return items;
	}

	public List<SelectItem> getMonths() {

		try {
			return CommonUtil.getIsEnglishLocale() ? getMonthsEn()
					: getMonthsAr();
		} catch (Exception e) {
			logger.LogException("getMonths|Error occured", e);
		}
		return new ArrayList<SelectItem>();
	}

	public List<SelectItem> getMonthsEn() throws Exception {
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		if (context.getAttribute("MONTHS_LIST_EN") != null) {
			return (ArrayList<SelectItem>) context
					.getAttribute("MONTHS_LIST_EN");
		} else {
			List<Month> monthList = new UtilityService().getMonths();
			for (Month month : monthList) {
				SelectItem item = new SelectItem(month.getMonthId().toString(),
						month.getMonthEn());
				itemList.add(item);
			}
			context.setAttribute("MONTHS_LIST_EN", itemList);
			return itemList;

		}
	}

	public List<SelectItem> getMonthsAr() throws Exception {
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		if (context.getAttribute("MONTHS_LIST_AR") != null) {
			return (ArrayList<SelectItem>) context
					.getAttribute("MONTHS_LIST_AR");
		} else {
			List<Month> monthList = new UtilityService().getMonths();
			for (Month month : monthList) {
				SelectItem item = new SelectItem(month.getMonthId().toString(),
						month.getMonthAr());
				itemList.add(item);
			}
			context.setAttribute("MONTHS_LIST_AR", itemList);
			return itemList;

		}
	}

	public List<SelectItem> getYears() throws Exception {
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		if (context.getAttribute("YEAR_LIST") != null) {
			return (ArrayList<SelectItem>) context.getAttribute("YEAR_LIST");
		} else {
			List<Month> monthList = new UtilityService().getMonths();

			int year = 2013;
			for (int i = 0; i < 25; i++) {
				String yStr = String.valueOf(year);
				SelectItem item = new SelectItem(yStr, yStr);
				itemList.add(item);
				year++;
			}
			context.setAttribute("YEAR_LIST", itemList);
			return itemList;

		}
	}

	public List<SelectItem> getEndowmentAssetTypesList() throws Exception {

		if (viewMap.get("END_ASSET_TYPE_LIST") != null) {

			return (List<SelectItem>) viewMap.get("END_ASSET_TYPE_LIST");
		}
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<AssetType> assetTypesList = new ArrayList<AssetType>();
		assetTypesList = new UtilityService().getEndowmentAssetType();
		if (assetTypesList != null && assetTypesList.size() > 0) {
			for (AssetType assets : assetTypesList) {
				SelectItem item;
				item = new SelectItem(assets.getAssetTypeId().toString(),
						isEnglishLocale() ? assets.getAssetTypeNameEn()
								: assets.getAssetTypeNameAr());
				items.add(item);
			}
		}
		Collections.sort(items, ListComparator.LIST_COMPARE);
		viewMap.put("END_ASSET_TYPE_LIST", items);
		return items;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getEndowmentProgramStatus() throws Exception {
		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(Constant.EndowmentProgramStatus.KEY);
		if (viewMap.get(Constant.EndowmentProgramStatus.KEY) != null) {
			return (ArrayList<SelectItem>) viewMap
					.get(Constant.EndowmentProgramStatus.KEY);
		}
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (DomainDataView domainDataView : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				itemList.add(item);
			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
			viewMap.put(Constant.EndowmentProgramStatus.KEY, itemList);
		}
		return itemList;
	}

	public List<SelectItem> getEndowmentProgramObjectives() throws Exception {

		final String key = "EndowmentProgObjectives";
		if (viewMap.get(key) != null) {

			return (List<SelectItem>) viewMap.get(key);
		}
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<EndProgObjective> objList = new ArrayList<EndProgObjective>();
		objList = new UtilityService().getEndowmentProgramObjectives();
		if (objList != null && objList.size() > 0) {
			for (EndProgObjective obj : objList) {
				SelectItem item;
				item = new SelectItem(obj.getObjectiveId().toString(),
						isEnglishLocale() ? obj.getObjectiveNameEn() : obj
								.getObjectiveNameAr());
				items.add(item);
			}
		}
		Collections.sort(items, ListComparator.LIST_COMPARE);
		if (items != null && items.size() > 0) {
			viewMap.put(key, items);
		}
		return items;
	}

	public List<SelectItem> getEndowmentProgramDisbursemntSrc()
			throws Exception {

		final String key = "EndowmentProgramDisbursemntSrc";
		if (viewMap.get(key) != null) {

			return (List<SelectItem>) viewMap.get(key);
		}
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<EndProgDisSrc> objList = new ArrayList<EndProgDisSrc>();
		objList = new UtilityService().getEndowmentProgramDisbursementSrc();
		if (objList != null && objList.size() > 0) {
			for (EndProgDisSrc obj : objList) {
				SelectItem item;
				item = new SelectItem(obj.getEndProgDisSrcId().toString(),
						isEnglishLocale() ? obj.getSrcNameEn() : obj
								.getSrcNameAr());
				items.add(item);
			}
		}
		Collections.sort(items, ListComparator.LIST_COMPARE);
		viewMap.put(key, items);
		return items;
	}

	public List<SelectItem> getEndowmentProgramEvalutionType() throws Exception {

		final String key = "EndowmentProgramEvalutionType";
		if (viewMap.get(key) != null) {
			return (ArrayList<SelectItem>) viewMap.get(key);
		}

		List<SelectItem> items = new ArrayList<SelectItem>();
		List<EndProgEvalType> list = new ArrayList<EndProgEvalType>();
		list = new UtilityService().getEndowmentProgramEvalutionType();
		if (list != null && list.size() > 0) {
			for (EndProgEvalType obj : list) {
				SelectItem item = new SelectItem(obj.getEndProgEvalTypeId()
						.toString(), isEnglishLocale() ? obj.getTypeEn() : obj
						.getTypeAr());
				items.add(item);
			}
		}
		Collections.sort(items, ListComparator.LIST_COMPARE);
		viewMap.put(key, items);
		return items;
	}

	public List<SelectItem> getOpenBoxStatus() throws Exception {
		if (viewMap.get("OpenBoxStatus") != null) {
			return (List<SelectItem>) viewMap.get("OpenBoxStatus");
		}
		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(WebConstants.OpenBoxStatus.Key);
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (DomainDataView domainDataView : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				itemList.add(item);
			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
		}
		viewMap.put("OpenBoxStatus", itemList);
		return itemList;
	}

	public List<SelectItem> getRevenueFollowupStatus() throws Exception {
		if (viewMap.get(WebConstants.RevenueFollowupStatus.Key) != null) {
			return (List<SelectItem>) viewMap
					.get(WebConstants.RevenueFollowupStatus.Key);
		}
		List<DomainDataView> list = CommonUtil
				.getDomainDataListForDomainType(WebConstants.RevenueFollowupStatus.Key);
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (DomainDataView domainDataView : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				itemList.add(item);
			}
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
		}
		viewMap.put(WebConstants.RevenueFollowupStatus.Key, itemList);
		return itemList;
	}

	public List<SelectItem> getAllEndowmentExpTypes() throws Exception {
		final String key = "AllEndowmentExpTypes";
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		if (viewMap.get(key) != null) {
			return (List<SelectItem>) viewMap.get(key);
		}
		List<EndowmentExpType> list = EndowmentExpenseTypeService
				.getAllExpenseType();
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (EndowmentExpType obj : list) {

				SelectItem item;
				if (isEnglishLocale())
					item = new SelectItem(obj.getTypeId().toString(), obj
							.getTypeNameEn());
				else
					item = new SelectItem(obj.getTypeId().toString(), obj
							.getTypeNameAr());
				itemList.add(item);
			}
		}
		viewMap.put(key, itemList);
		return itemList;
	}

	public List<SelectItem> getAllGrpSupplierCodes() throws Exception {
		// final String key = "getAllGrpSupplierCodes";
		// if(viewMap.get( key )!= null)
		// {
		// return (List<SelectItem>)viewMap.get( key );
		// }
		List<GrpSupplierCode> list = DisbursementService
				.getAllGrpSupplierCodes();
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (GrpSupplierCode obj : list) {

				SelectItem item = new SelectItem(obj.getCodeId().toString(),
						obj.getSupplierCode());
				itemList.add(item);
			}
		}
		// viewMap.put( key , itemList );
		return itemList;
	}

	public List<SelectItem> getAllGrpSupplierName() throws Exception {
		final String key = "getAllGrpSupplierName";
		// if(viewMap.get( key )!= null)
		// {
		// return (List<SelectItem>)viewMap.get( key );
		// }
		List<GrpSupplierName> list = DisbursementService
				.getAllGrpSupplierNames();
		List<SelectItem> itemList = new ArrayList<SelectItem>();

		if (list != null && list.size() > 0) {
			for (GrpSupplierName obj : list) {

				SelectItem item = new SelectItem(
						obj.getSupplierId().toString(), obj.getSupplierName());
				itemList.add(item);
			}
		}
		// viewMap.put( key , itemList );
		return itemList;
	}

	public List<SelectItem> getUserGroups() throws Exception {
		if (!viewMap.containsKey("ALL_USER_GROUPS")) {
			List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
			List<SelectItem> listUserGrps = new ArrayList<SelectItem>(
					allUserGrps.size());
			for (UserGroup singleGrp : allUserGrps) {
				listUserGrps.add(new SelectItem(singleGrp.getUserGroupId(),
						isEnglishLocale() ? singleGrp.getPrimaryName()
								: singleGrp.getSecondaryName()));
			}
			Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
			viewMap.put("ALL_USER_GROUPS", listUserGrps);
			return listUserGrps;
		} else
			return (ArrayList<SelectItem>) viewMap.get("ALL_USER_GROUPS");
	}

	public List<SelectItem> getBlockingReasons() throws Exception {
		if (viewMap.get("BlockingReasons") != null) {
			return (ArrayList<SelectItem>) viewMap.get("BlockingReasons");
		}
		List<BlockingReason> list = UtilityService.getBlockingReason();
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (BlockingReason reason : list) {
			itemList.add(new SelectItem(reason.getReasonId().toString(),
					isEnglishLocale() ? reason.getReasonEn() : reason
							.getReasonAr()));
		}
		Collections.sort(itemList, ListComparator.LIST_COMPARE);
		viewMap.put("BlockingReasons", itemList);
		return itemList;

	}

	public List<SelectItem> getDisbursementReasons() throws Exception {
		if (viewMap.get("DisbursementReasons") != null) {
			return (ArrayList<SelectItem>) viewMap.get("DisbursementReasons");
		}
		UtilityService utilityWS = new UtilityService();
		List<DisbursementReason> reasons = utilityWS
				.getAllDisbursementReasonsList();
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (DisbursementReason reason : reasons) {
			itemList.add(new SelectItem(reason.getReasonId().toString(),
					isEnglishLocale() ? reason.getReasonEn() : reason
							.getReasonAr()));
		}
		viewMap.put("DisbursementReasons", itemList);
		return itemList;

	}

	public List<SelectItem> getDisbursementReasonsType() throws Exception {
		if (viewMap.get("DisbursementReasonsType") != null) {
			return (ArrayList<SelectItem>) viewMap
					.get("DisbursementReasonsType");
		}
		UtilityService utilityWS = new UtilityService();
		List<DomainDataView> typeList = utilityWS
				.getDomainDataByDomainTypeName("DISB_REASON_TYPE");
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (DomainDataView type : typeList) {
			itemList.add(new SelectItem(type.getDomainDataId().toString(),
					isEnglishLocale() ? type.getDataDescEn() : type
							.getDataDescAr()));
		}
		viewMap.put("DisbursementReasonsType", itemList);
		return itemList;

	}

	public List<SelectItem> getProgramMilestoneStatus() throws Exception {
		if (viewMap.get("ProgramMilestoneStatus") != null) {
			return (ArrayList<SelectItem>) viewMap
					.get("ProgramMilestoneStatus");
		}
		UtilityService utilityWS = new UtilityService();
		List<DomainDataView> typeList = utilityWS
				.getDomainDataByDomainTypeName(Constant.ProgramMilestoneStatus.KEY);
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (DomainDataView type : typeList) {
			itemList.add(new SelectItem(type.getDomainDataId().toString(),
					isEnglishLocale() ? type.getDataDescEn() : type
							.getDataDescAr()));
		}
		viewMap.put("ProgramMilestoneStatus", itemList);
		return itemList;

	}

	public List<SelectItem> getBoxCategory() throws Exception {
		if (viewMap.get("BoxCategory") != null) {
			return (ArrayList<SelectItem>) viewMap.get("BoxCategory");
		}
		UtilityService utilityWS = new UtilityService();
		List<DomainDataView> typeList = utilityWS
				.getDomainDataByDomainTypeName(Constant.BoxCategory.KEY);
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		for (DomainDataView type : typeList) {
			itemList.add(new SelectItem(type.getDomainDataId().toString(),
					isEnglishLocale() ? type.getDataDescEn() : type
							.getDataDescAr()));
		}
		viewMap.put("BoxCategory", itemList);
		return itemList;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDonationSource() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("DonationSource") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("DonationSource");
			} else {
				List<DomainDataView> list = new UtilityService()
						.getDomainDataByDomainTypeName(Constant.DonationSource.KEY);
				for (DomainDataView type : list) {
					selectItems.add(new SelectItem(type.getDomainDataId()
							.toString(), isEnglishLocale() ? type
							.getDataDescEn() : type.getDataDescAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("DonationSource", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getDonationSource() crashed", e);
		}
		return selectItems;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDonationCollectedByUser() throws Exception {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> itemList = new ArrayList<SelectItem>();
		if (viewMap.get("CollectedByUser") != null) {
			itemList = (ArrayList<SelectItem>) viewMap.get("CollectedByUser");
		} else {
			String groupNamesCSV = WebConstants.DonationRequest.COLLECTED_BY_USER_GRP;
			List<String> groupList = new ArrayList<String>();
			groupList.add(groupNamesCSV);
			itemList = CommonUtil.getUsersByGroupId(groupList);
			viewMap.put("CollectedByUser", itemList);
			Collections.sort(itemList, ListComparator.LIST_COMPARE);
		}
		return itemList;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getZakatStatus() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("ZakatStatus") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("ZakatStatus");
			} else {
				List<DomainDataView> list = new UtilityService()
						.getDomainDataByDomainTypeName(WebConstants.ZakatStatus.ZAKAT_STATUS);
				for (DomainDataView type : list) {
					selectItems.add(new SelectItem(type.getDomainDataId()
							.toString(), isEnglishLocale() ? type
							.getDataDescEn() : type.getDataDescAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("ZakatStatus", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getZakatStatus() crashed", e);
		}
		return selectItems;

	}

	public List<SelectItem> getPropertyOwnershipTypeExcludingThirdParty() {
		Boolean isEnglishLocale = isEnglishLocale();

		if (isEnglishLocale) {
			List<SelectItem> cboList = listMap
					.get("PropertyOwnershipTypeExcludingThirdParty");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.OWNER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if (ddv
									.getDomainDataId()
									.compareTo(
											WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) == 0)
								continue;
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescEn());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap
						.put("PropertyOwnershipTypeExcludingThirdParty",
								cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap
					.get("PropertyOwnershipTypeExcludingThirdPartyAr");
			if (cboList == null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context
						.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while (itr.hasNext()) {
					DomainTypeView dtv = itr.next();
					if (dtv.getTypeName().compareTo(WebConstants.OWNER_TYPE) == 0) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if (ddv
									.getDomainDataId()
									.compareTo(
											WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) == 0)
								continue;
							item = new SelectItem(ddv.getDomainDataId()
									.toString(), ddv.getDataDescAr());
							cboList.add(item);
						}
						break;
					}
				}
				if (cboList == null) {
					return emptyList;
				}
				Collections.sort(cboList, ListComparator.LIST_COMPARE);
				listMap.put("PropertyOwnershipTypeExcludingThirdPartyAr",
						cboList);
			}
			return cboList;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getEndowmentMainCategory() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("EndowmentMainCategory") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("EndowmentMainCategory");
			} else {
				List<DomainDataView> list = new UtilityService()
						.getDomainDataByDomainTypeName(Constant.EndowmentMainCategory.ENDOWMENT_MAIN_CATEOGORY);
				for (DomainDataView type : list) {
					selectItems.add(new SelectItem(type.getDomainDataId()
							.toString(), isEnglishLocale() ? type
							.getDataDescEn() : type.getDataDescAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("EndowmentMainCategory", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getEndowmentMainCategory() crashed", e);
		}
		return selectItems;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getCustomerReview() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("CustomerReviewList") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("CustomerReviewList");
			} else {
				List<CustomerReview> list = UtilityService.getCustomerReview();
				for (CustomerReview obj : list) {
					selectItems
							.add(new SelectItem(obj.getCustomerReviewId()
									.toString(), isEnglishLocale() ? obj
									.getCustomerReviewEn() : obj
									.getCustomerReviewAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("CustomerReviewList", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getCustomerReview() crashed", e);
		}
		return selectItems;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getComplaintTypes() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("ComplaintTypesList") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("ComplaintTypesList");
			} else {
				List<ComplaintType> list = UtilityService.getComplaintType();
				for (ComplaintType obj : list) {
					selectItems.add(new SelectItem(obj.getTypeId().toString(),
							isEnglishLocale() ? obj.getTypeNameEn() : obj
									.getTypeNameAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("ComplaintTypesList", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getComplaintTypes() crashed", e);
		}
		return selectItems;

	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementMainReason() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			if (viewMap.get("DisbursementMainReason") != null) {
				selectItems = (ArrayList<SelectItem>) viewMap
						.get("DisbursementMainReason");
			} else {

				List<DomainData> objList = UtilityService
						.getDomainDataPOJOById(217L);
				boolean hasReason = false;
				for (DomainData obj : objList) {

					selectItems.add(new SelectItem(obj.getDomainDataId()
							.toString(), isEnglishLocale() ? obj
							.getDataDescEn() : obj.getDataDescAr()));
				}
				Collections.sort(selectItems, ListComparator.LIST_COMPARE);
				viewMap.put("DisbursementMainReason", selectItems);
			}
		} catch (Exception e) {
			logger.LogException("getDisbursementMainReason() crashed", e);
		}
		return selectItems;

	}

	public List<SelectItem> getEndowmentStatusList() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get(WebConstants.EndowmentManage.STATUS) != null) {

				return (List<SelectItem>) viewMap
						.get(WebConstants.EndowmentManage.STATUS);
			}

			List<DomainDataView> list = new UtilityService()
					.getDomainDataByDomainTypeName(WebConstants.EndowmentManage.STATUS_TYPE_NAME);
			for (DomainDataView type : list) {
				selectItems.add(new SelectItem(type.getDomainDataId()
						.toString(), isEnglishLocale() ? type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put(WebConstants.EndowmentManage.STATUS, selectItems);

		} catch (Exception e) {
			logger.LogException("getEndowmentStatusList() crashed", e);
		}
		return selectItems;

	}

	public  List<SelectItem> getPassportTypeList()throws Exception{
		
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		Map viewMap=FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		try{
			for(String items:CommonUtil.getBundleMessage("mems.list.pasportType").split(","))
			{
				selectItems.add(new SelectItem(items,items));
			}
		}
		catch (Exception e) 
		{
			logger.LogException("getPassportTypeList() crashed", e);
		}
		return selectItems;

		}
	
	
	/**
	 * Get all against the provided Domain Type Name
	 * 
	 * @param void
	 * @return {@link SelectItem} combo
	 * @throws Exception
	 * @author Imran Ali
	 */
	public List<SelectItem> getFileNetProcedureType() throws Exception{
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		
		List<ProcedureTypeVO> list = new UtilityService().getAllProcedureType();
		for(ProcedureTypeVO vo:list){
			if(isEnglishLocale)
				cboList.add(new SelectItem(vo.getExternalId(),vo.getExternalId()));
			else
				cboList.add(new SelectItem(vo.getExternalId(),vo.getExternalId()));
		}

			return cboList;
	}
	
	/**
	 * Get all against the provided Domain Type Name
	 * 
	 * @param void
	 * @return {@link SelectItem} combo
	 * @throws Exception
	 * @author Imran Ali
	 */
	public List<SelectItem> getFileNetFolderName() throws Exception{
		
		Boolean isEnglishLocale = isEnglishLocale();
		List<SelectItem> cboList = new ArrayList<SelectItem>();
		
		 try
		 {  
			 List<DocumentFolder> procedureTypeList= UtilityService.getAllParentFolderName();
			 for (DocumentFolder pojo: procedureTypeList) {
				 cboList.add(new SelectItem(pojo.getId().toString(),pojo.getFolderName()));
			
			 }
		 }
		 catch(Exception exception) {
			 logger.LogException("getAllProcedureType() crashed ",exception);
			 throw new PimsBusinessException(ExceptionCodes.GENERAL_EXCEPTION,ExceptionMessages.GENERAL_EXCEPTION);
		 }
		 return cboList;
	}
	
	public List<SelectItem> getHousingRequestReason() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get(WebConstants.HousingRequest.PROCEDURE_HOUSING_REQUEST) != null) {

				return (List<SelectItem>) viewMap
						.get(WebConstants.HousingRequest.PROCEDURE_HOUSING_REQUEST);
			}

			List<DomainDataView> list = new UtilityService()
					.getDomainDataByDomainTypeName(WebConstants.HousingRequest.PROCEDURE_HOUSING_REQUEST);
			for (DomainDataView type : list) {
				selectItems.add(new SelectItem(type.getDomainDataId()
						.toString(), isEnglishLocale() ? type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put(WebConstants.HousingRequest.PROCEDURE_HOUSING_REQUEST, selectItems);

		} catch (Exception e) {
			logger.LogException("getHousingRequestReason() crashed", e);
		}
		return selectItems;

	}
	
	public List<SelectItem> getHousingRejectReason() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get(WebConstants.HousingRequest.PROCEDURE_REJECT_REASONS) != null) {

				return (List<SelectItem>) viewMap
						.get(WebConstants.HousingRequest.PROCEDURE_REJECT_REASONS);
			}

			List<DomainDataView> list = new UtilityService()
					.getDomainDataByDomainTypeName(WebConstants.HousingRequest.PROCEDURE_REJECT_REASONS);
			for (DomainDataView type : list) {
				selectItems.add(new SelectItem(type.getDomainDataId()
						.toString(), isEnglishLocale() ? type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put(WebConstants.HousingRequest.PROCEDURE_REJECT_REASONS, selectItems);

		} catch (Exception e) {
			logger.LogException("getHousingRequestReason() crashed", e);
		}
		return selectItems;

	}

	public List<SelectItem> getVillaCategory() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get("VILLA_CATEGORY") != null) {

				return (List<SelectItem>) viewMap.get("VILLA_CATEGORY");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName("VILLA_CATEGORY");
			for (DomainDataView type : list) 
			{
				selectItems.add(new SelectItem(type.getDomainDataId().toString(), isEnglishLocale() ? 
						type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("VILLA_CATEGORY", selectItems);

		} catch (Exception e) {
			logger.LogException("getVillaCategory() crashed", e);
		}
		return selectItems;

	}

	public List<SelectItem> getVillaStaffRolesList() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get("VILLA_ROLES_LIST") != null) {

				return (List<SelectItem>) viewMap.get("VILLA_ROLES_LIST");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName("VILLA_ROLES");
			for (DomainDataView type : list) 
			{
				selectItems.add(new SelectItem(type.getDomainDataId().toString(), isEnglishLocale() ? 
						type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("VILLA_ROLES_LIST", selectItems);

		} catch (Exception e) {
			logger.LogException("getVillaStaffRolesList() crashed", e);
		}
		return selectItems;

	}
	public List<SelectItem> getEmiratesList() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if ((viewMap.get(WebConstants.UAE_LIST)) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap.get(WebConstants.UAE_LIST);
			} else {
				// get data from db and put prepared list in viewMap
				Set<RegionView> dubaiCommunity = null;
				dubaiCommunity = new PropertyServiceAgent()
						.getState(WebConstants.Region.UAE_ID);
				if (dubaiCommunity != null && dubaiCommunity.size() > 0) {
					for (RegionView community : dubaiCommunity) {
						SelectItem item = null;
						if (isEnglishLocale()) {
							item = new SelectItem(community.getRegionId()
									.toString(), community.getDescriptionEn());
						} else {
							item = new SelectItem(community.getRegionId()
									.toString(), community.getDescriptionAr());
						}
						items.add(item);
					}
					Collections.sort(items, ListComparator.LIST_COMPARE);
				}
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.UAE_LIST, items);
				}
			}
		} catch (Exception e) {
			logger.LogException("getEmiratesList() crashed", e);
		}
		return items;
	}
	
	public List<SelectItem> getTerminateHousingRequestReason() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get(WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST) != null) {

				return (List<SelectItem>) viewMap
						.get(WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST);
			}

			List<DomainDataView> list = new UtilityService()
					.getDomainDataByDomainTypeName(WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST);
			for (DomainDataView type : list) {
				selectItems.add(new SelectItem(type.getDomainDataId()
						.toString(), isEnglishLocale() ? type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put(WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST, selectItems);

		} catch (Exception e) {
			logger.LogException("getTerminateHousingRequestReason() crashed", e);
		}
		return selectItems;

	}
	
	 public List<SelectItem> getSocialResearcherGroup() throws PimsBusinessException {

			String groupNamesCSV = new UtilityServiceAgent()
					.getValueFromSystemConfigration(WebConstants.GROUP_SOCIAL_RESEARCHER);
			if (groupNamesCSV != null) {
				String[] groupNames = groupNamesCSV.split(",");
				List groupList = Arrays.asList(groupNames);
				return CommonUtil.getUsersByGroupId(groupList);
			} else
				return null;

		}

	public List<SelectItem> getBeneficiaryCharacteristics() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get("CHARACTERISTICS_TYPE_LIST") != null) {

				return (List<SelectItem>) viewMap.get("CHARACTERISTICS_TYPE_LIST");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName(Constant.BeneficiaryCharacteristics.CHARACTERISTIC_TYPE);
			for (DomainDataView type : list) 
			{
				selectItems.add(
								new SelectItem(
												type.getDomainDataId().toString(), isEnglishLocale() ? 
																										type.getDataDescEn()
																										: type.getDataDescAr()
											  )
							   );
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("CHARACTERISTICS_TYPE_LIST", selectItems);

		} catch (Exception e) {
			logger.LogException("getBeneficiaryCharacteristics() crashed", e);
		}
		return selectItems;

	}
        
    public List<SelectItem> getSituationUnderstanding() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("SITUATION_UNDERSTANDING_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("SITUATION_UNDERSTANDING_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("SITUATION_UNDERSTANDING");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("SITUATION_UNDERSTANDING_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getSituationUnderstanding() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getMotherCaring() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("MOTHER_CARES_LIST") != null) {

                                    return (List<SelectItem>) viewMap.get("MOTHER_CARES_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("MOTHER_CARES");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("MOTHER_CARES_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getMotherCaring() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getMentalDisorderTypes() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("DISORDER_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap.get("DISORDER_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("DISORDER_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("DISORDER_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getMentalDisorderTypes() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getMonthlyIncomeRange() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("MONTHLY_INCOME_RANGE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("MONTHLY_INCOME_RANGE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("MONTHLY_INCOME_RANGE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("MONTHLY_INCOME_RANGE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getMonthlyIncomeRange() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getIncomeSource() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("INCOME_SOURCE_LIST") != null) {

                                    return (List<SelectItem>) viewMap.get("INCOME_SOURCE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("INCOME_SOURCE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("INCOME_SOURCE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getIncomeSource() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPsychologicalConditionCategory()
                            throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PSYCHOLOGICAL_CONDITION_CATEGORY_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PSYCHOLOGICAL_CONDITION_CATEGORY_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PSYCHOLOGICAL_CONDITION_CATEGORY");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PSYCHOLOGICAL_CONDITION_CATEGORY_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getPsychologicalConditionCategory() crashed",
                                            e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPsychologicalConditionEvaluation()
                            throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PSYCHOLOGICAL_CONDITION_EVALUATION_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PSYCHOLOGICAL_CONDITION_EVALUATION_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PSYCHOLOGICAL_CONDITION_EVALUATION");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PSYCHOLOGICAL_CONDITION_EVALUATION_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException(
                                            "getPsychologicalConditionEvaluation() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPsychologicalTestType() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PSYCHOLOGICAL_TEST_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PSYCHOLOGICAL_TEST_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PSYCHOLOGICAL_TEST_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PSYCHOLOGICAL_TEST_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getPsychologicalTestType() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getInterventionPlanCaringType() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("INTERVENTION_PLAN_CARING_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("INTERVENTION_PLAN_CARING_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("INTERVENTION_PLAN_CARING_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("INTERVENTION_PLAN_CARING_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getInterventionPlanCaringType() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getBeneficiaryProblemTypes() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("BENEFICIARY_PROBLEM_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("BENEFICIARY_PROBLEM_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("BENEFICIARY_PROBLEM_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("BENEFICIARY_PROBLEM_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getBeneficiaryProblemTypes() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getBeneficiaryProblemRiskLevels() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("BENEFICIARY_PROBLEM_RISK_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("BENEFICIARY_PROBLEM_RISK_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("BENEFICIARY_PROBLEM_RISK_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("BENEFICIARY_PROBLEM_RISK_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getBeneficiaryProblemRiskLevels() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getBeneficiaryProblemCommunicatedToOthers()
                            throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PROBLEM_COMMUNICATED_TO_OTHERS_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PROBLEM_COMMUNICATED_TO_OTHERS_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PROBLEM_COMMUNICATED_TO_OTHERS");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PROBLEM_COMMUNICATED_TO_OTHERS_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException(
                                            "getBeneficiaryProblemCommunicatedToOthers() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPhysicalMovementList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PHYSICAL_MOVEMENT_LIST") != null) {

                                    return (List<SelectItem>) viewMap.get("PHYSICAL_MOVEMENT_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PHYSICAL_MOVEMENT");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PHYSICAL_MOVEMENT_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getPhysicalMovementList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPsychologicalProblemsList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PSYCHOLOGICAL_PROBLEMS_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PSYCHOLOGICAL_PROBLEMS_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PSYCHOLOGICAL_PROBLEMS");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PSYCHOLOGICAL_PROBLEMS_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getPsychologicalProblemsList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getPersonalCareTypeList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("PERSONAL_CARE_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("PERSONAL_CARE_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("PERSONAL_CARE_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("PERSONAL_CARE_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getPersonalCareTypeList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getChronicDiseases() {
                    List<DiseaseType> diseaseTypeList = null;
                    List<SelectItem> itemList = new ArrayList<SelectItem>();
                    try {
                            if (viewMap.get(Constant.CHRONIC_DISEASE) != null) {
                                    itemList = (ArrayList<SelectItem>) viewMap
                                                    .get(Constant.CHRONIC_DISEASE);
                            } else {
                                    diseaseTypeList = UtilityService
                                                    .getDiseaseType(Constant.CHRONIC_DISEASE);
                                    for (DiseaseType diseaseType : diseaseTypeList) {
                                            SelectItem item = new SelectItem();
                                            String desc = isEnglishLocale() ? diseaseType
                                                            .getDescriptionEn().toString() : diseaseType
                                                            .getDescriptionAr();
                                            item.setValue(diseaseType.getDiseaseTypeId().toString());
                                            item.setDescription(desc);
                                            item.setLabel(desc);
                                            itemList.add(item);
                                    }
                                    if (itemList != null && itemList.size() > 0) {
                                            viewMap.put(Constant.CHRONIC_DISEASE, itemList);
                                    }
                            }

                    } catch (Exception e) {
                            logger.LogException("getChronicDiseases() crashed", e);
                    }
                    return itemList;
            }

            public List<SelectItem> getAcuteDiseases() {
                    List<DiseaseType> diseaseTypeList = null;
                    List<SelectItem> itemList = new ArrayList<SelectItem>();
                    try {
                            if (viewMap.get(Constant.ACUTE_DISEASE) != null) {
                                    itemList = (ArrayList<SelectItem>) viewMap
                                                    .get(Constant.ACUTE_DISEASE);
                            } else {
                                    diseaseTypeList = UtilityService
                                                    .getDiseaseType(Constant.ACUTE_DISEASE);
                                    for (DiseaseType diseaseType : diseaseTypeList) {
                                            SelectItem item = new SelectItem();
                                            String desc = isEnglishLocale() ? diseaseType
                                                            .getDescriptionEn().toString() : diseaseType
                                                            .getDescriptionAr();
                                            item.setValue(diseaseType.getDiseaseTypeId().toString());
                                            item.setDescription(desc);
                                            item.setLabel(desc);
                                            itemList.add(item);
                                    }
                                    if (itemList != null && itemList.size() > 0) {
                                            viewMap.put(Constant.ACUTE_DISEASE, itemList);
                                    }
                            }

                    } catch (Exception e) {
                            logger.LogException("getAcuteDiseases() crashed", e);
                    }
                    return itemList;
            }

            public List<SelectItem> getMedicalHistoryTypeList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("MEDICAL_HISTORY_TYPE_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("MEDICAL_HISTORY_TYPE_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("MEDICAL_HISTORY_TYPE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("MEDICAL_HISTORY_TYPE_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getMedicalHistoryTypeList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getAttendanceStatusList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("AttendanceStatus_LIST") != null) {

                                    return (List<SelectItem>) viewMap.get("AttendanceStatus_LIST");
                            }

                            List<AttendanceStatus> list = UtilityService.getAttendanceStatus();
                            for (AttendanceStatus type : list) {
                                    selectItems.add(new SelectItem(type.getAttendanceStatusId()
                                                    .toString(), isEnglishLocale() ? type
                                                    .getDescriptionEn() : type.getDescriptionAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("AttendanceStatus_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getAttendanceStatusList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getTerminateHousingRequestReasonsList()
                            throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("TERMINATE_HOUSING_REQUEST_REASON_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("TERMINATE_HOUSING_REQUEST_REASON_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("TERMINATE_HOUSING_REQUEST_REASON");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("TERMINATE_HOUSING_REQUEST_REASON_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException(
                                            "getTerminateHousingRequestReasonsList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getGoodInDealingWithList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("GOOD_IN_DEALING_WITH_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("GOOD_IN_DEALING_WITH_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("GOOD_IN_DEALING_WITH");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("GOOD_IN_DEALING_WITH_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getGoodInDealingWithList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getAngerReasons() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("ANGER_REASON") != null) {

                                    return (List<SelectItem>) viewMap.get("ANGER_REASON");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("ANGER_REASON");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("ANGER_REASON", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getAngerReasons() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getNotGoodInDealingWithList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("NOT_GOOD_IN_DEALING_WITH_LIST") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("NOT_GOOD_IN_DEALING_WITH_LIST");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("NOT_GOOD_IN_DEALING_WITH");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("NOT_GOOD_IN_DEALING_WITH_LIST", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getNotGoodInDealingWithList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getAngerManifestationList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("ANGER_MANIFESTATION") != null) {

                                    return (List<SelectItem>) viewMap.get("ANGER_MANIFESTATION");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("ANGER_MANIFESTATION");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("ANGER_MANIFESTATION", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getAngerManifestationList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getBehaviourInChangeOfConditionList()
                            throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("BEHAVIOUR_IN_CONDITION_CHANGE") != null) {

                                    return (List<SelectItem>) viewMap
                                                    .get("BEHAVIOUR_IN_CONDITION_CHANGE");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("BEHAVIOUR_IN_CONDITION_CHANGE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("BEHAVIOUR_IN_CONDITION_CHANGE", selectItems);

                    } catch (Exception e) {
                            logger.LogException(
                                            "getBehaviourInChangeOfConditionList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getBehaviourInPainList() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("BEHAVIOUR_IN_PAIN") != null) {

                                    return (List<SelectItem>) viewMap.get("BEHAVIOUR_IN_PAIN");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("BEHAVIOUR_IN_PAIN");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("BEHAVIOUR_IN_PAIN", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getBehaviourInPainList() crashed", e);
                    }
                    return selectItems;

            }

            public List<SelectItem> getCountryListForFamilyVillageBeneficiary() {
                    List<SelectItem> countryList = new ArrayList<SelectItem>(0);
                    boolean isLocaleEnglish = isEnglishLocale();
                    PropertyServiceAgent psa = new PropertyServiceAgent();
                    List<RegionView> regionViewList = new ArrayList<RegionView>(0);

                    try {
                            regionViewList = psa.getCountry();
                    } catch (Exception e) {
                            logger.LogException("getCountryListForFamilyVillageBeneficiary()",
                                            e);
                    }

                    for (RegionView rgv : regionViewList) {

                            if (!rgv.getGrpCountryCode().equals("AE")
                                            && !rgv.getGrpCountryCode().equals("UNKNOWN"))
                                    continue;
                            countryList.add(new SelectItem(rgv.getRegionId().toString(),
                                            isLocaleEnglish ? rgv.getDescriptionEn() : rgv
                                                            .getDescriptionAr()));
                    }

                    Collections.sort(countryList, ListComparator.LIST_COMPARE);

                    return countryList;
            }
            
    public List<SelectItem> getBeneficiaryLineage() throws Exception {

                    Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
                                    .getAttributes();
                    List<SelectItem> selectItems = new ArrayList<SelectItem>();
                    try {

                            if (viewMap.get("BeneficiaryLineage") != null) {

                                    return (List<SelectItem>) viewMap.get("BeneficiaryLineage");
                            }

                            List<DomainDataView> list = new UtilityService()
                                            .getDomainDataByDomainTypeName("BENEFICIARY_LINEAGE");
                            for (DomainDataView type : list) {
                                    selectItems.add(new SelectItem(type.getDomainDataId()
                                                    .toString(), isEnglishLocale() ? type.getDataDescEn()
                                                    : type.getDataDescAr()));
                            }
                            Collections.sort(selectItems, ListComparator.LIST_COMPARE);
                            viewMap.put("BeneficiaryLineage", selectItems);

                    } catch (Exception e) {
                            logger.LogException("getBeneficiaryLineage() crashed", e);
                    }
                    return selectItems;

            }


	
}
