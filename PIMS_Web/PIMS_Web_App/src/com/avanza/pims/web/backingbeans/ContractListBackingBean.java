package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.dao.ContractManager;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.SystemConfiguration;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.RenewContractsReminderLetterCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.property.PropertyTransformUtil;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class ContractListBackingBean extends AbstractController 
{

	private static final long serialVersionUID = 5324809347096492485L;
	DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	private Boolean markUnMarkAll;
	private String unitRefNum;
	private String contractType;
	private String contractStatus;
	private String contractNumber;
	private String startDate;
	private String hdnTenantId;
	private String endDate;
	private String tenantNumber;
	private String tenantName;
	private String tenantType;
	private String propertyComercialName;
	private String propertyEndowedName;
	private String propertyUsage;
	private String propertyType;
	private String countries;
	private String states;
	private String city;
	private String street;
	private String fromContractDate;
	private String toContractDate;
	private String fromContractExpDate;
	private String toContractExpDate;
	private Double fromRentAmount;
	private Double toRentAmount;
	private ContractView contractView = new ContractView();
	CommonUtil commonUtil = new CommonUtil();
	private String allContractStatus;
	private List<ContractView> contractList;
	private PropertyService propertyService = new PropertyService();
	private PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	List<SelectItem> statusList = new ArrayList<SelectItem>();
	List<SelectItem> typeList = new ArrayList<SelectItem>();
	List<SelectItem> tenantTypeList = new ArrayList<SelectItem>();
	List<SelectItem> propertyTypeList = new ArrayList<SelectItem>();
	List<SelectItem> propertyUsageList = new ArrayList<SelectItem>();
	List<SelectItem> countryList = new ArrayList<SelectItem>();
	List<SelectItem> stateList = new ArrayList<SelectItem>();
	List<SelectItem> cityList = new ArrayList<SelectItem>();
	private List<String> errorMessages;
	private List<String> successMessages;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String TENANT_INFO="TENANTINFO";

	 private HtmlDataTable dataTable;

	 public Date fromContractDateRich;
	 public Date toContractDateRich;
	 public Date fromContractExpDateRich;
	 public Date toContractExpDateRich;

	 private static Logger logger = Logger.getLogger( ContractListBackingBean.class );
	 private Boolean isCLStatus = true;
	 private Boolean isNOLStatus = true;

	 private Boolean isContrctStatusApproved=true;
	 private Boolean isTransferContractStatus = true;
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 private Boolean isContrctStatusNew;
	 private Boolean isContrctStatusActive;
	 private Boolean isContrctStatusComplete;
	 private Boolean isContrctStatusExpired;
	 private Boolean isContrctStatusRenewDraft;
	 private String hdnSelectedContracts;
	 private String blockComments;
	 private String unblockComments;


	 private String pageMode;
	 private String MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP";
	 private String MODE_SELECT_ONE_POPUP ="MODE_SELECT_ONE_POPUP";

	 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	 private RenewContractsReminderLetterCriteria reportCriteria;
	 @SuppressWarnings("unchecked")
	private Map selectedContracts = new HashMap();


	 HttpServletRequest requestParam = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

	@SuppressWarnings( "unchecked" )
	public Boolean getMarkUnMarkAll() {
		if(  viewMap.get("markUnMarkAll" ) != null)
		{
			markUnMarkAll = (Boolean)viewMap.get("markUnMarkAll");
		}
		return markUnMarkAll;
	}

	@SuppressWarnings( "unchecked" )
	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
		viewMap.put("markUnMarkAll",markUnMarkAll);
	}


	public ContractListBackingBean()
	{
		reportCriteria = new RenewContractsReminderLetterCriteria(ReportConstant.Report.RENEW_CONTRACT_REMINDER_LETTER, ReportConstant.Processor.RENEW_CONTRACT_REMINDER_LETTER, CommonUtil.getLoggedInUser());
	}


	@Override
	@SuppressWarnings( "unchecked" )
	public void init()
	{

		try
		{
			super.init();
			if ( !isPostBack() )
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		        setSortField("contracts.contractNumber");
				setSortItemListAscending(false);
				
				if(sessionMap.containsKey("preserveSearchCriteria") && sessionMap.get("preserveSearchCriteria") != null)
					retrieveSearchCriteria();
				unitRefNum = "";
				viewMap.put("stateList",new ArrayList<SelectItem>());
				loadCombos();

			    if(requestParam.getParameter("clStatus")!=null)
			    {
				  viewMap.put("clStatus",requestParam.getParameter("clStatus"));
				  requestParam.removeAttribute("clStatus");
			    }


			    if(requestParam.getParameter("pageMode")!=null&&
			    		requestParam.getParameter("pageMode").equals(MODE_SELECT_ONE_POPUP)) {
			    	viewMap.put("pageMode",MODE_SELECT_ONE_POPUP );
			    }
      	    	else if(requestParam.getParameter("pageMode")!=null&&
      	    			requestParam.getParameter("pageMode").equals(MODE_SELECT_MANY_POPUP)){
      	    		viewMap.put("pageMode",MODE_SELECT_MANY_POPUP );
      	    	}
	    		if(viewMap.get("pageMode")==null) {
	    			viewMap.put("pageMode","fullScreenMode");
	    		}


		}
		}catch(Exception e){
			logger.LogException( "init crashed due to:", e );
		}

	}

	public boolean getIsViewModePopUp() {
		if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp()) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean getIsPageModeSelectOnePopUp() {
		boolean isPageModeSelectOnePopUp= true;
		if(viewMap.get("pageMode")!=null) 	{
			if(viewMap.get("pageMode").toString().equals(MODE_SELECT_ONE_POPUP)) {
				isPageModeSelectOnePopUp = true;
			}
			else {
				isPageModeSelectOnePopUp = false;
			}
		} else {
			isPageModeSelectOnePopUp = false;
		}

		return isPageModeSelectOnePopUp;
	}

	public boolean getIsPageModeSelectManyPopUp()	{
		boolean isPageModeSelectManyPopUp= true;
		if(viewMap.get("pageMode")!=null) {
			if(viewMap.get("pageMode").toString().equals(MODE_SELECT_MANY_POPUP)) {
				isPageModeSelectManyPopUp= true;
			}
			else {
				isPageModeSelectManyPopUp= false;
			}
		}
		else {
			isPageModeSelectManyPopUp =false;
		}
		return isPageModeSelectManyPopUp;
	}


	@Override
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		super.prerender();

	}

	@SuppressWarnings( "unchecked" )
    public void onSelectAll()
    {           
    	try
    	{
    		markUnMarkAll=true;
//    		onMarkUnMarkChanged();
    	} 
    	catch(Exception ex) 
    	{
		  logger.LogException("[Exception occured in onSelectAll()]", ex);
		  errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    }

	@SuppressWarnings( "unchecked" )
	public String cmdRenewContractReminderLeter_Click()
	{

		ContractView contractView = ( ContractView ) dataTable.getRowData();
		reportCriteria.setContractIdsCSV( contractView.getContractId().toString());

		double suggestedRentAmount=0.0;
		try
		{
			suggestedRentAmount = Math.ceil( propertyService.calculateSuggestedRentAmount(contractView));
		}
		catch (PimsBusinessException e)
		{
			logger.LogException("Error cmdRenewContractReminderLeter_Click" , e);
		}
		reportCriteria.setRentValue(""+suggestedRentAmount);

		try {
			NotesController.saveSystemNotesForRequest(WebConstants.CONTRACT,"renewcontract.notificationletter.printed", contractView.getContractId() );
		} catch (Exception e) {
			logger.LogException("problem in saving system comments", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	

	@SuppressWarnings( "unchecked" )
	public void onMarkUnMarkChanged( )throws Exception 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
            contractList = getContractList();
            

		} catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onMarkUnMarkChanged| Error Occured...", ex);
		}

	}

	
	@SuppressWarnings("unchecked")
	private List<Contract> getUnPaginatedContractsWithoutBlackList(ContractManager contractManager )throws Exception
	{
		
		
		ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
		if(contractListCriteriaBackingBean.getContractNumber() !=null)
		{
			contractView.setContractNumber(contractListCriteriaBackingBean.getContractNumber());
		}
		contractView.setContractTypeEn(contractListCriteriaBackingBean.getContractType());

		HashMap dateHashMap = new HashMap();
		HashMap contractHashMap = new HashMap();
		clearSessionMap();
		DateFormat dfForRich = new SimpleDateFormat("dd/MM/yyyy");
		getCriteriaFromView(df, contractListCriteriaBackingBean,dateHashMap, contractHashMap, dfForRich);
		Contract contract = PropertyTransformUtil.transformToContract(contractView);
		contract.setStatusId(WebConstants.CONTRACT_STATUS_ACTIVE_ID);
		contractHashMap.put("contract", contract);
		contractHashMap.put("excludeBlackListed","1"); 
		List<Contract> contracts = contractManager.getAllContracts(
																	contractHashMap,  
																	null,
																	null, 
																	null,
																	null
																 );
		
		return contracts;
	}
	
	@SuppressWarnings("unchecked")
	public void renewContractReminderLeters_Click()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			if(!markUnMarkAll && ( hdnSelectedContracts == null || hdnSelectedContracts.length() <= 0) )
			{
	    		
	    		errorMessages.add(CommonUtil.getBundleMessage("renewalLetter.errMsg.pleaseSelectContracts"));
	    		return;
			}
			String template = " SELECT {0}  \"CONTRACT_ID\" , {1} \"NEW_RENT_AMOUNT\" FROM DUAL ";
		  	String query ="";
		  	String startQuery ="(";
		  	NumberFormat nf = new DecimalFormat("#0");
		  
			if(markUnMarkAll)
			{
				hdnSelectedContracts = "";
				ContractManager contractManager = new ContractManager();
				List<Contract>contractList = getUnPaginatedContractsWithoutBlackList(contractManager);
				SystemConfiguration config = EntityManager.getBroker().findById( SystemConfiguration.class,WebConstants.SYS_CONFIG_ID_MAX_NUMBER_RENEWALS_REMINDERS);
				int maxRecords = new Integer(config.getConfigvalue());
				if(contractList!=null && contractList.size()>0  && contractList.size()>maxRecords)
				{
					String msg = java.text.MessageFormat.format(  
																ResourceUtil.getInstance().getProperty("renewalReminder.msg.maxNumberofRecordsExceeded"),
																maxRecords
																);
					errorMessages.add(msg);
					return;
				}
				else if(contractList!=null && contractList.size()== 0  )
				{
					String msg = ResourceUtil.getInstance().getProperty("renewalReminder.msg.noActiveContractFound");
					errorMessages.add(msg);
					return;
				}
			    for (Contract contract : contractList) 
				{
			    	hdnSelectedContracts = hdnSelectedContracts + contract.getContractId().toString() +",";
					double suggestedRentAmount=0.0;
					try
					{
						suggestedRentAmount = Math.ceil( contractManager.calculateSuggestedRentAmount( contract ) );
						query += java.text.MessageFormat.format(
																template,nf.format( contract.getContractId() ), 
																nf.format(suggestedRentAmount)
																);
						query += " UNION";
					}
					catch (PimsBusinessException e)
					{
						logger.LogException("Error calculateSuggestedRentAmount()" , e);
					}
					try 
					{
						NotesController.saveSystemNotesForRequest(WebConstants.CONTRACT,"renewcontract.notificationletter.printed", new Long( contract.getContractId() ) );
					}
					catch (Exception e) 
					{
						logger.LogException("problem in saving system comments", e);
						errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
					}
				 
				}
			    
			    hdnSelectedContracts = hdnSelectedContracts.substring(0,hdnSelectedContracts.length()-1);
			 	query = query.substring(0,query.length()-5).concat(")");
				startQuery += query;
				  
				
			}
			else if(hdnSelectedContracts != null && hdnSelectedContracts.length() > 0 )
			{
				startQuery = getSelectedContractsQueryForRenewalReminder(	template, query, startQuery, nf );
			}
	
			reportCriteria.setContractIdsCSV(hdnSelectedContracts);
			reportCriteria.setContractRentQuery(startQuery);
	
	    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
	    	openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch (Exception e) 
		{
			logger.LogException( "renewContractReminderLeters_Click|crashed...", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}


	/**
	 * @param template
	 * @param query
	 * @param startQuery
	 * @param nf
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getSelectedContractsQueryForRenewalReminder(String template,String query, String startQuery, NumberFormat nf)throws Exception 
	{
		String[] splittedIds = hdnSelectedContracts.split(",");
		if(splittedIds !=null && splittedIds.length >0)
		for(String contractId :  splittedIds)
		{
			{
				double suggestedRentAmount=0.0;
				try
				{
					suggestedRentAmount = Math.ceil( propertyService.calculateSuggestedRentAmountByContractId(Long.parseLong(contractId)));
					query += java.text.MessageFormat.format(template,nf.format(Long.parseLong(contractId)), nf.format(suggestedRentAmount));
					query += " UNION";
				}
				catch (PimsBusinessException e)
				{
					logger.LogException("Error calculateSuggestedRentAmount()" , e);
				}
			}

			try 
			{
				NotesController.saveSystemNotesForRequest(WebConstants.CONTRACT,"renewcontract.notificationletter.printed", new Long(contractId) );
			}
			catch (Exception e) 
			{
				logger.LogException("problem in saving system comments", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
		query = query.substring(0,query.length()-5).concat(")");
		startQuery += query;
		return startQuery;
	}

	@SuppressWarnings("unchecked")
	public void blockContract_Click()
	{

    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
    	if(hdnSelectedContracts == null || hdnSelectedContracts.length() <= 0)
		{
    		errorMessages = new ArrayList<String>();
    		errorMessages.add(CommonUtil.getBundleMessage("blockContract.errMsg.pleaseSelectContracts"));
		}
    	else
    	{
    		super.getSessionMap().put("CONTRACT_BLOCK_UNBLOCK", hdnSelectedContracts);
    		openPopup("openBlockPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
    	}
	}

	@SuppressWarnings("unchecked")
	public void unblockContract_Click()
	{

    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
    	if(hdnSelectedContracts == null || hdnSelectedContracts.length() <= 0)
		{
    		errorMessages = new ArrayList<String>();
    		errorMessages.add(CommonUtil.getBundleMessage("unblockContract.errMsg.pleaseSelectContracts"));
		}
    	else
    	{
    		super.getSessionMap().put("CONTRACT_BLOCK_UNBLOCK", hdnSelectedContracts);
    		openPopup("openUnblockPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
    	}
	}

	public void blockWithComments(){
		logger.logInfo("blockWithComments() started...");
		try {
			 Object idsCSV = super.getSessionMap().get("CONTRACT_BLOCK_UNBLOCK");
			 if(idsCSV != null){
				 String[] splittedIds = idsCSV.toString().split(",");
				 if(splittedIds !=null && splittedIds.length > 0){
					 ArrayList<String> messages = (new PropertyService()).blockContractsForRenewal(splittedIds, this.getBlockComments());
					 successMessages = new ArrayList<String>();
					 errorMessages = new ArrayList<String>();
					 if(messages.get(0) != "")
						 successMessages.add(messages.get(0) + " "+ CommonUtil.getBundleMessage("blockContract.successMessage"));
					 if(messages.get(1) != "")
						 errorMessages.add(messages.get(1) + " "+ CommonUtil.getBundleMessage("blockContract.unableMessage"));
					 this.searchContracts();
				 }
			 }
		} catch (Exception exception) {
			logger.LogException("blockWithComments() crashed ", exception);
		}
	}

	public void unblockWithComments(){
		logger.logInfo("unblockWithComments() started...");
		try {
			 Object idsCSV = super.getSessionMap().get("CONTRACT_BLOCK_UNBLOCK");
			 if(idsCSV != null){
				 String[] splittedIds = idsCSV.toString().split(",");
				 if(splittedIds !=null && splittedIds.length > 0){
					 ArrayList<String> messages = (new PropertyService()).unblockContractsForRenewal(splittedIds, this.getBlockComments());
					 successMessages = new ArrayList<String>();
					 errorMessages = new ArrayList<String>();
					 if(messages.get(0) != "")
						 successMessages.add(messages.get(0) + " "+ CommonUtil.getBundleMessage("unblockContract.successMessage"));
					 if(messages.get(1) != "")
						 errorMessages.add(messages.get(1) + " "+ CommonUtil.getBundleMessage("unblockContract.unableMessage"));
					 this.searchContracts();
				 }
			 }
		} catch (Exception exception) {
			logger.LogException("unblockWithComments() crashed ", exception);
		}

	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public void retrieveTenant()
	{
		try
		{
		FillTenantInfo();

		if(viewMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewMap.get(TENANT_INFO);

//		   String tenant_Type=getTenantType(tenantViewRow);


	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
	        contractListCriteriaBackingBean.setTenantName(tenantNames);
	    }

		}
		catch(Exception ex)
		{
			logger.LogException(  "retrieveTenant| crashed", ex);
		}

	}
	@SuppressWarnings( "unchecked" )
    private String getTenantType(PersonView tenantView)
    {

       DomainDataView ddv;
       if(tenantView!=null && tenantView.getPersonId()!=null && tenantView.getIsCompany()!=null )
       {
	       if( tenantView.getIsCompany().compareTo(new Long(1))==0)
	    		 ddv = (DomainDataView )viewMap.get(WebConstants.TENANT_TYPE_COMPANY);
	       else
	    		 ddv = (DomainDataView )viewMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
	       if(getIsArabicLocale())
				return ddv.getDataDescAr();
		   else if(getIsEnglishLocale())
				return  ddv.getDataDescEn();
       }
            logger.logDebug( "getTenantType|Finish...");
    return "";



    }
	private void FillTenantInfo() throws PimsBusinessException {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  propertyService.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewMap.put(TENANT_INFO,tenantsList.get(0));

	    	}

	}



	public String getDateFormat()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	@SuppressWarnings( "unchecked" )
	private void loadCombos()
	{

		try
		{

		List<DomainDataView> domainDataListStatus =loadContractStatus();
		sessionMap.put(WebConstants.CONTRACT_STATUS, domainDataListStatus);

		List<DomainDataView> domainDataListTypes = loadContractTypes();
		sessionMap.put(WebConstants.CONTRACT_TYPE, domainDataListTypes);

		List<DomainDataView> domainDataTenantListTypes = loadTenantType();
		sessionMap.put(WebConstants.TENANT_TYPE, domainDataTenantListTypes);

		List<DomainDataView> domainDataPropertyTypes = loadPropertyType();
		sessionMap.put(WebConstants.PROPERTY_TYPE, domainDataPropertyTypes);

		List<DomainDataView> domainDataPropertyUsages = loadPropertyUsage();
		sessionMap.put(WebConstants.PROPERTY_USAGE, domainDataPropertyUsages);

		}
		catch(Exception e)
		{
			logger.LogException( "loadCombos| crashed due to:", e );
		}

	}

	public void processValueChange(ValueChangeEvent vce)
	{


		try
		{
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
				if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
			List <RegionView> regionViewList = propertyService.getCountryStates(selectedCountryName,getIsArabicLocale());
			for(int i=0;i<regionViewList.size();i++)
			{
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				  else
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
			      this.getStateList().add(item);
			}
		}
	    catch (Exception e)
		{
			logger.LogException( "processValueChange", e);
		}
	}
	 @SuppressWarnings( "unchecked" )
	 public void loadCountry()
	 {
			try
			{
				List <RegionView> regionViewList = propertyService.getCountry();
				for(int i=0;i<regionViewList.size();i++)
				{
					  RegionView rV=(RegionView)regionViewList.get(i);
					  SelectItem item;
					  if (getIsEnglishLocale())
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
					  else
						  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
				      this.getCountryList().add(item);

				}
				Collections.sort(countryList,ListComparator.LIST_COMPARE);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("countryList", countryList);
			}
			catch (Exception e)
			{
				logger.LogException("Exception Occured - In loadCountry()",e);

			}
		}
	 @SuppressWarnings( "unchecked" )
	 public void loadState(ValueChangeEvent vce)  {


			try {

			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
				if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
			List <RegionView> regionViewList = propertyService.getCountryStates(selectedCountryName,getIsArabicLocale());
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				  else
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
			      this.getStateList().add(item);
			  }
			Collections.sort(stateList,ListComparator.LIST_COMPARE);
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
			}
			catch (Exception e)
			{

				logger.LogException( "Error Occuired - In loadCountry() of Person search List", e );

			}
		}


	 @SuppressWarnings( "unchecked" )
	public List<DomainDataView> loadContractStatus() {


		List<DomainDataView> domainDataList =null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = propertyService.getDomainDataByDomainTypeName(WebConstants.CONTRACT_STATUS);


		}
		catch (PimsBusinessException e)
		{
			// TODO Auto-generated catch block
			logger.LogException( "loadContractStatus| crashed due to:",e );
		}

		 return domainDataList;
	}

public List<DomainDataView> loadPropertyType() {


		List<DomainDataView> domainDataList =null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = propertyService.getDomainDataByDomainTypeName(WebConstants.PROPERTY_TYPE);

		//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError("loadPropertyType| crashed due to:"+e.getStackTrace());
		}
		 return domainDataList;
	}


	public List<DomainDataView> loadPropertyUsage() {


	List<DomainDataView> domainDataList =null;
	try {

		domainDataList = new ArrayList<DomainDataView>();
		domainDataList = propertyService.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);

	//	domainDataList.get(0);

	} catch (PimsBusinessException e) {
		// TODO Auto-generated catch block
		logger.logError( "loadPropertyUsage| crashed due to:"+e.getStackTrace());
	}
	 return domainDataList;
	}

public List<DomainDataView> loadTenantType() {


		List<DomainDataView> domainDataList =null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = propertyService.getDomainDataByDomainTypeName(WebConstants.TENANT_TYPE);

		//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError( " loadTenantType|crashed due to:"+e.getStackTrace());
		}
		 return domainDataList;
	}

public List<DomainDataView> loadContractTypes() {

		List<DomainDataView> domainDataList =null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = propertyService.getDomainDataByDomainTypeName(WebConstants.CONTRACT_TYPE);


		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError( "loadContractTypes| crashed due to:"+e.getStackTrace());
		}
		
		 return domainDataList;
	}

@SuppressWarnings( "unchecked" )
    public List<SelectItem> getStatusList()
    {

		

		try {
                statusList.clear();

                List<DomainDataView> statusComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.CONTRACT_STATUS);
                for(int i=0;i< statusComboList.size();i++)
                {
                	DomainDataView ddv=(DomainDataView)statusComboList.get(i);
                	SelectItem item=null;
                	if (isEnglishLocale)
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
                	else
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


                statusList.add(item);

                }
                Collections.sort(statusList,ListComparator.LIST_COMPARE);
			}catch(Exception e){
			logger.logError(" getStatusList|crashed due to:"+e.getStackTrace());
			}

		
		 return statusList;
          }
    @SuppressWarnings( "unchecked" )
    public List<SelectItem> getTypeList()

    {
		

		try {

          typeList.clear();

          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.CONTRACT_TYPE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


              typeList.add(item);

          }
          Collections.sort(typeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError(" getTypeList|crashed due to:"+e.getStackTrace());
			}

		
          return typeList;

    }

	public boolean getIsArabicLocale()
	{


		isArabicLocale = !getIsEnglishLocale();



		return isArabicLocale;
	}

	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	@SuppressWarnings("unchecked")
	public String btnCmdLink_Click()
	{
		String eventOutCome="failure";
		ContractView contractViewRow=(ContractView)dataTable.getRowData();
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 //setRequestParam("contractId", contractView.getContractId());
		facesContext.getExternalContext().getSessionMap().put("contractId", contractViewRow.getContractId());
		eventOutCome="success";
		return eventOutCome;
	}

	@SuppressWarnings("unchecked")
	public String cmdNewMaintenanceApplication_Click()
	{
		String eventOutCome="failure";
		ContractView contractViewRow=(ContractView)dataTable.getRowData();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_ID, contractViewRow.getContractId());
		eventOutCome="newMaintenanceApplication";
		return eventOutCome;
	}


	@SuppressWarnings("unchecked")
	public void searchContracts()
	{
		hdnSelectedContracts = "";
		setMarkUnMarkAll( false );
		pageFirst();
		
	}

	@SuppressWarnings("unchecked")
	public void doSearchItemList() 
	{
		try 
		{
			 
			ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
			if(contractListCriteriaBackingBean.getContractNumber() !=null)
			{
				contractView.setContractNumber(contractListCriteriaBackingBean.getContractNumber());
			}
			contractView.setContractTypeEn(contractListCriteriaBackingBean.getContractType());

			HashMap dateHashMap = new HashMap();
			HashMap contractHashMap = new HashMap();
			clearSessionMap();
			DateFormat dfForRich = new SimpleDateFormat("dd/MM/yyyy");
			getCriteriaFromView(df, contractListCriteriaBackingBean,dateHashMap, contractHashMap, dfForRich);
			
			recordSize 	 = propertyService.getTotalNumberOfRecords(contractHashMap);
			
			setTotalRows(recordSize);
			doPagingComputations();
			
			contractList = propertyService.getAllContractsForContractList(
																			contractHashMap,  getRowsPerPage(),
																			getCurrentPage(), getSortField(),
																			isSortItemListAscending()
																		 );
			
			viewMap.put("contractList", contractList);
			

			if(contractList.isEmpty())
			{
				clearSessionMap();
				forPaging(0);
			}
			else
			{
				forPaging( recordSize );
			}
		} 
		catch (Exception e) 
		{
			logger.LogException("Search Contracts crashed due to : ", e);
		}
	}


	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void forPaging(int totalRows) 
	{
		viewMap.put("recordSize", totalRows);
		setTotalRows(totalRows);
		paginatorRows = getPaginatorRows();
		paginatorMaxPages = recordSize/paginatorRows;
		if((recordSize%paginatorRows)>0)
		{
			paginatorMaxPages++;
		}
		if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
		{
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		}
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
	}


	/**
	 * @param df
	 * @param contractListCriteriaBackingBean
	 * @param dateHashMap
	 * @param contractHashMap
	 * @param dfForRich
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	private void getCriteriaFromView(
										DateFormat df,
										ContractListCriteriaBackingBean contractListCriteriaBackingBean,
										HashMap dateHashMap, HashMap contractHashMap, DateFormat dfForRich
									)
	throws ParseException 
	{
				if(contractListCriteriaBackingBean.getFromContractDateRich()!=null )
				{
		
					dateHashMap.put("fromContractDate", dfForRich.format(contractListCriteriaBackingBean.getFromContractDateRich()));
					contractHashMap.put("fromContractDate", dfForRich.format(contractListCriteriaBackingBean.getFromContractDateRich()));
		
				}
		
				if(contractListCriteriaBackingBean.getToContractDateRich()!=null )
				{
		
					dateHashMap.put("toContractDate", dfForRich.format(contractListCriteriaBackingBean.getToContractDateRich()));
					contractHashMap.put("toContractDate", dfForRich.format(contractListCriteriaBackingBean.getToContractDateRich()));
		
				}
				if(contractListCriteriaBackingBean.getFromContractExpDateRich()!=null )
				{
		
					dateHashMap.put("fromContractExpDate",  dfForRich.format(contractListCriteriaBackingBean.getFromContractExpDateRich()));
					contractHashMap.put("fromContractExpDate", dfForRich.format(contractListCriteriaBackingBean.getFromContractExpDateRich()));
		
				}
				if(contractListCriteriaBackingBean.getToContractExpDateRich()!=null )
				{
		
					dateHashMap.put("toContractExpDate", dfForRich.format(contractListCriteriaBackingBean.getToContractExpDateRich()));
					contractHashMap.put("toContractExpDate", dfForRich.format(contractListCriteriaBackingBean.getToContractExpDateRich()));
		
				}

				if(contractListCriteriaBackingBean.getStartDate()!=null && !contractListCriteriaBackingBean.getStartDate().equals(""))
				{
						Date date =new Date();
						date = df.parse(contractListCriteriaBackingBean.getStartDate());
						contractView.setStartDate(date);
				}
				
				if(contractListCriteriaBackingBean.getEndDate()!=null && !contractListCriteriaBackingBean.getEndDate().equals(""))
				{
				
						Date date =new Date();
						date = df.parse(contractListCriteriaBackingBean.getEndDate());
						contractView.setEndDate(date);
				}
				if(contractListCriteriaBackingBean.getContractStatus()!=null && !contractListCriteriaBackingBean.getContractStatus().equals("")
							&& !contractListCriteriaBackingBean.getContractStatus().equals("-1"))
				{
						contractView.setStatus(new Long(contractListCriteriaBackingBean.getContractStatus()));
				}
				
				if(
					contractListCriteriaBackingBean.getContractType()!=null && 
					!contractListCriteriaBackingBean.getContractType().equals("") && 
					!contractListCriteriaBackingBean.getContractType().equals("-1")
				  )
				{
					
						contractView.setContractTypeId(new Long(contractListCriteriaBackingBean.getContractType()));
				
				}
				if(contractListCriteriaBackingBean.getTenantNumber()!=null && !contractListCriteriaBackingBean.getTenantNumber().equals(""))
				{
				
						contractHashMap.put("tenantNumber", contractListCriteriaBackingBean.getTenantNumber());
						//sessionMap.put("tenantNumber", tenantNumber);
				
				}
				if(contractListCriteriaBackingBean.getTenantName()!=null && !contractListCriteriaBackingBean.getTenantName().equals(""))
				{
				
						contractHashMap.put("tenantName", contractListCriteriaBackingBean.getTenantName());
				
				}
				if(contractListCriteriaBackingBean.getTenantType() != null && !contractListCriteriaBackingBean.getTenantType().equals("-1"))
				{

					if(contractListCriteriaBackingBean.getTenantType().equals("69001") )
						contractHashMap.put("tenantTypeId", "0");
					else
						contractHashMap.put("tenantTypeId", "1");
				
						
				}
				
				if(contractListCriteriaBackingBean.getPropertyComercialName()!=null && !contractListCriteriaBackingBean.getPropertyComercialName().equals(""))
				{
				
						contractHashMap.put("propertyComercialName", contractListCriteriaBackingBean.getPropertyComercialName());
						//sessionMap.put("propertyComercialName", contractListCriteriaBackingBean.getPropertyComercialName());
				
				}
				if(contractListCriteriaBackingBean.getPropertyEndowedName() !=null && !contractListCriteriaBackingBean.getPropertyEndowedName().equals(""))
				{
				
						contractHashMap.put("propertyEndowedName", contractListCriteriaBackingBean.getPropertyEndowedName());
						//sessionMap.put("propertyEndowedName", contractListCriteriaBackingBean.getPropertyEndowedName());
				
				}
				if(contractListCriteriaBackingBean.getPropertyType() != null && !contractListCriteriaBackingBean.getPropertyType().equals("-1"))
				{
				
						contractHashMap.put("propertyTypeId", contractListCriteriaBackingBean.getPropertyType());
						//sessionMap.put("propertyType", contractListCriteriaBackingBean.getPropertyType());
				
				}
				if(contractListCriteriaBackingBean.getPropertyUsage() != null && !contractListCriteriaBackingBean.getPropertyUsage().equals("-1"))
				{
				
						contractHashMap.put("propertyUsageId", contractListCriteriaBackingBean.getPropertyUsage());
						//sessionMap.put("propertyUsage", propertyUsage);
				
				}
				if(contractListCriteriaBackingBean.getFromRentAmount()!=null && contractListCriteriaBackingBean.getFromRentAmount().doubleValue() > 0)
				{
				
						contractHashMap.put("fromRentAmount", contractListCriteriaBackingBean.getFromRentAmount().toString());
						//sessionMap.put("fromRentAmount", fromRentAmount);
				
				}
				if(contractListCriteriaBackingBean.getToRentAmount()!=null && contractListCriteriaBackingBean.getToRentAmount().doubleValue() > 0)
				{
				
						contractHashMap.put("toRentAmount", contractListCriteriaBackingBean.getToRentAmount().toString());
						//sessionMap.put("toRentAmount", toRentAmount);
				
				}
				if(contractListCriteriaBackingBean.getCountries() != null && !contractListCriteriaBackingBean.getCountries().equals("-1"))
				{
				
						contractHashMap.put("countryId", contractListCriteriaBackingBean.getCountries());
						//sessionMap.put("countries", countries);
				
				}
				if(contractListCriteriaBackingBean.getStates() != null && !contractListCriteriaBackingBean.getStates().equals("-1"))
				{
				
						contractHashMap.put("stateId", contractListCriteriaBackingBean.getStates());
						//sessionMap.put("states", states);
				
				}
				if(contractListCriteriaBackingBean.getCity() != null && !contractListCriteriaBackingBean.getCity().equals("-1"))
				{
				
						contractHashMap.put("cityId", contractListCriteriaBackingBean.getCity());
						//sessionMap.put("states", states);
				
				}
				if(contractListCriteriaBackingBean.getUserName() != null && !contractListCriteriaBackingBean.getUserName().equals(""))
				{
						contractHashMap.put("userName", contractListCriteriaBackingBean.getUserName());
				}
				if(contractListCriteriaBackingBean.getEndowedNo() != null && !contractListCriteriaBackingBean.getEndowedNo().equals(""))
				{
						contractHashMap.put("endowedNo", contractListCriteriaBackingBean.getEndowedNo());
				}
				if(contractListCriteriaBackingBean.getFromContractStartDateRich()!=null )
				{
				
						dateHashMap.put("fromContractStartDate",  dfForRich.format(contractListCriteriaBackingBean.getFromContractStartDateRich()));
						contractHashMap.put("fromContractStartDate", dfForRich.format(contractListCriteriaBackingBean.getFromContractStartDateRich()));
				
				}
				if(contractListCriteriaBackingBean.getToContractStartDateRich()!=null )
				{
				
						dateHashMap.put("toContractStartDate", dfForRich.format(contractListCriteriaBackingBean.getToContractStartDateRich()));
						contractHashMap.put("toContractStartDate", dfForRich.format(contractListCriteriaBackingBean.getToContractStartDateRich()));
				
				}
				if(contractListCriteriaBackingBean.getStreet() != null && !contractListCriteriaBackingBean.getStreet().equals(""))
						contractHashMap.put("street",contractListCriteriaBackingBean.getStreet());
				if( contractListCriteriaBackingBean.getPrintedBeforeActive()!=null && contractListCriteriaBackingBean.getPrintedBeforeActive() )
				{
						contractView.setPrintedBeforeActive( 1L );
				}
				contractHashMap.put( "contractView", contractView );
				
				contractHashMap.put( "unitRefNum", contractListCriteriaBackingBean.getUnitRefNum() );
				contractHashMap.put( "unitAccNo", contractListCriteriaBackingBean.getUnitCostCenter().trim() );
				contractHashMap.put( "unitDesc", contractListCriteriaBackingBean.getUnitDesc().trim() );
				contractHashMap.put( "dateFormat", getDateFormat() );
	}


	public String getUnitRefNum() {

		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("unitNo"))
			unitRefNum = (String) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("unitNo");

		return unitRefNum;
	}


	public void setUnitRefNum(String unitRefNum) {
		this.unitRefNum = unitRefNum;

	}


	public ContractView getContractView() {
		return contractView;
	}


	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}

   @SuppressWarnings( "unchecked" )
	public List<ContractView> getContractList() {

		if (viewMap.containsKey("contractList"))
		{
			contractList = (ArrayList<ContractView>) viewMap.get("contractList");
		}

		return contractList;
	}


	public void setContractList(List<ContractView> contractList) {
		this.contractList = contractList;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}


	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public String getContractStatus() {
		return contractStatus;
	}


	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}


	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFromContractDate() {
		return fromContractDate;
	}

	public void setFromContractDate(String fromContractDate) {
		this.fromContractDate = fromContractDate;
	}

	public String getToContractDate() {
		return toContractDate;
	}

	public void setToContractDate(String toContractDate) {
		this.toContractDate = toContractDate;
	}

	public String getAllContractStatus() {
		return allContractStatus;
	}

	public void setAllContractStatus(String allContractStatus) {
		this.allContractStatus = allContractStatus;
	}

	public Long getClearanceLetterStatus() {
		Long clearanceLetterStatus = new Long(0);
		Object obj =ApplicationContext.getContext().get(WebContext.class).
						getAttribute(WebConstants.CONTRACT_STATUS_EXPIRED_ID);
		if(obj != null){
			clearanceLetterStatus = Long.parseLong(obj.toString());
		}
		else{
			try {
				clearanceLetterStatus = getContractStatusExpiredId();
				ApplicationContext.getContext().get(WebContext.class).
					setAttribute(WebConstants.CONTRACT_STATUS_EXPIRED_ID, clearanceLetterStatus);
			} catch (PimsBusinessException e) {
				logger.LogException("getCLStatus crashed...", e);
			}
		}
		return clearanceLetterStatus;
	}

@SuppressWarnings( "unchecked" )
	private Long getContractStatusExpiredId() throws PimsBusinessException
	{
		Long cLExpiredId = new Long(0);
		HashMap hMap= getIdFromType(
									getContractStatusList(),
					   		        WebConstants.CONTRACT_STATUS_TERMINATED);
  		if(hMap.containsKey("returnId")){
  			cLExpiredId = Long.parseLong(hMap.get("returnId").toString());
  			if(cLExpiredId.equals(null))
  				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
  		}
		return cLExpiredId;
	}

	public Long getNOLStatus() {
		Long nOLStatus = new Long(0);
		Object obj =ApplicationContext.getContext().get(WebContext.class).
						getAttribute(WebConstants.CONTRACT_STATUS_APPROVED_ID);
		if(obj != null){
			nOLStatus = Long.parseLong(obj.toString());
		}
		else{
			try {
				nOLStatus = getNOLId();
				ApplicationContext.getContext().get(WebContext.class).
					setAttribute(WebConstants.CONTRACT_STATUS_APPROVED_ID, nOLStatus);
			} catch (PimsBusinessException e) {
				logger.LogException("getCLStatus crashed...", e);
			}
		}
		return nOLStatus;
	}


@SuppressWarnings( "unchecked" )
	private Long getNOLId() throws PimsBusinessException
	{
		Long nOLId = new Long(0);
		HashMap hMap= getIdFromType( getContractStatusList(),
   		                             WebConstants.CONTRACT_STATUS_ACTIVE);
  		if(hMap.containsKey("returnId")){
  			nOLId = Long.parseLong(hMap.get("returnId").toString());
  			if(nOLId.equals(null))
  				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
  		}
		return nOLId;
	}
@SuppressWarnings( "unchecked" )
	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}

		}
		return hMap;

	}
	public Boolean getIsCLStatus() {
		ContractView contractView = (ContractView)dataTable.getRowData();
		if(contractView.getStatus().equals(getClearanceLetterStatus()))
			isCLStatus = true;
		else
			isCLStatus = false;
		return isCLStatus;
	}



	public Boolean getIsCLStatusTerminated()  {
		 String clStatusTerminated="";
		 isCLStatus = true;

		 try
		 {
		    	ContractView contractView = (ContractView)dataTable.getRowData();
				if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("clStatus")
				  && FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("clStatus")!=null
				  && !FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("clStatus").equals("ALL")){
					clStatusTerminated=(String)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("clStatus");

				}
				if(contractView.getStatus().equals(Long.parseLong(clStatusTerminated)))
					isCLStatus = true;
				else
					isCLStatus = false;

				if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("clStatus")
						  && FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("clStatus")!=null
						  &&FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("clStatus").equals("ALL")){
					isCLStatus = true;
				}
		 }
		 catch(Exception ex)
		 {
			 logger.LogException("EXCEPTION in getIsCLStatusTerminated:::::::: ", ex);
		 }
				return isCLStatus;
			}
	public void setIsCLStatus(Boolean isCLStatus) {
		this.isCLStatus = isCLStatus;
	}

	public void SetCancelContractStatus(Boolean status) {
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getCancelContractStatus() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(
													 getContractStatusList(),
				                                     WebConstants.CONTRACT_STATUS_ACTIVE);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			return true;
		else
			return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getIsContrctStatusApproved() {
		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(getContractStatusList() , WebConstants.CONTRACT_STATUS_APPROVED);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isContrctStatusApproved= true;
		else
			isContrctStatusApproved= false;
		return isContrctStatusApproved;
	}
	public void setIsChangeTenantStatus(Boolean isChangeTenantStatus) {
		this.isContrctStatusApproved = isChangeTenantStatus;
	}

	public Boolean getIsTransferContractStatus() {
		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType( getContractStatusList(),
				                                      WebConstants.CONTRACT_STATUS_ACTIVE);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isTransferContractStatus= true;
		else
			isTransferContractStatus= false;
		return isTransferContractStatus;
	}
	@SuppressWarnings( "unchecked" )
     public List<DomainDataView> getContractStatusList()
     {
		List<DomainDataView> list = null;
    	 if( viewMap.get( "contractStatusList" )== null )
    	 {
    		 list =  	 CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
    		 viewMap.put( "contractStatusList", list );
    	 }
    	 else
    		 list = ( ArrayList<DomainDataView> )viewMap.get( "contractStatusList" );
        return list;
     }

	public void setIsTransferContractStatus(Boolean isTransferContractStatus) {
		this.isTransferContractStatus = isTransferContractStatus;
	}

	public Boolean getIsNOLStatus() {
		ContractView contractView = (ContractView)dataTable.getRowData();
		if(contractView.getStatus().equals(getNOLStatus()))
			isNOLStatus = true;
		else
			isNOLStatus = false;
		return isNOLStatus;
	}



	public void setIsNOLStatus(Boolean isNOLStatus) {
		this.isNOLStatus = isNOLStatus;
	}

	public String issueCLCmdLink_Click()
	{
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
		
		}
		catch (Exception e) {
			logger.LogException("issueCLCmdLink_Clickcrashed...", e);
		}
		return WebConstants.ContractListNavigation.ISSUE_CLEARANCE_LETTER;
	}

	/**
	 * INSERT INTO DOMAIN_DATA ( DOMAIN_DATA_ID, DOMAIN_TYPE_ID, DATA_VALUE, DATA_DESC_EN, DATA_DESC_AR,
	 * UPDATED_ON, UPDATED_BY, RECORD_STATUS, IS_DELETED, CREATED_ON,
	 * CREATED_BY) VALUES (310011, 31, 'CANCEL_CONTRACT', 'Cancel Contract', 'Cancel Contract Ar', sysdate, 'admin', 1, 0, sysdate)
	 * @return
	 */
	public String cmdLinkCancelContract_Click(){
        try{
        	ContractView cv = (ContractView)dataTable.getRowData();
        	GRPService grpService=new GRPService();
//        	if(cv.getStatus().compareTo(WebConstants.CONTRACT_STATUS_ACTIVE_ID)==0)
//        	{
        		if(grpService.areAllTransactionsPostedToGRP(cv.getContractNumber()))
        		{
        			errorMessages=new ArrayList<String>();
	        		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeTerminatedTrasnferred"));
	        		return null;
        		}
//        	}
        	putContractInRequestMap((ContractView)dataTable.getRowData());
		}
		catch (Exception e) {
			logger.LogException( "cmdLinkCancelContract_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.CANCEL_CONTRACT;
	}
	@SuppressWarnings( "unchecked" )
	public String cmdLinkSettleContract_Click()
	{
        try
        {
        	putContractInRequestMap((ContractView)dataTable.getRowData());
        	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SETTLE_PAGE_MODE","readOnly");
        	webContext = ApplicationContext.getContext().get(WebContext.class);
        	webContext.setAttribute("parentPage", "contractList");
        
		}
		catch (Exception e)
		{
			logger.LogException(  "cmdLinkSettleContract_Click|crashed...", e);
		}

		return WebConstants.ContractListNavigation.SETTLE_CONTRACT;
	}
	@SuppressWarnings( "unchecked" )
	public String issueNOLCmdLink_Click(){
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
		}
		catch (Exception e) {
			logger.LogException( "issueNOLmdLink_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.ISSUE_NO_OBJECTION_LETTER;
	}
	@SuppressWarnings( "unchecked" )
	public String contractViolationCmdLink_Click(){
        try{
        	putContractInRequestMap((ContractView)dataTable.getRowData());
		
		}
		catch (Exception e) {
			logger.LogException(  "contractViolationCmdLink_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.CONTRACT_VIOLATION;
	}
	@SuppressWarnings( "unchecked" )
	public String cmdChangeTenant_Click()
	{
        try{

        	ContractView contractView=(ContractView)dataTable.getRowData();

        	RequestView requestView =new RequestView();
        	requestView.setContractView(contractView);
        	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);
        	List<RequestView> requestViewList= propertyServiceAgent.getAllRequests(requestView, null, null,null);
        	List<DomainDataView> ddRequestList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
        	DomainDataView ddvComplete=	CommonUtil.getIdFromType(ddRequestList,WebConstants.REQUEST_STATUS_COMPLETE);
        	DomainDataView ddvRejected=	CommonUtil.getIdFromType(ddRequestList,WebConstants.REQUEST_STATUS_REJECTED);
        	//if there is some request for change tenant name against this
        	//contract and its status is not completed OR rejected then display that request otherwise create new screen
        	requestView=null;
        	for (RequestView requestView2 : requestViewList)
        	{

        		if(requestView2.getStatusId().compareTo(ddvComplete.getDomainDataId())!=0 && requestView2.getStatusId().compareTo(ddvRejected.getDomainDataId())!=0)
        		{
        			requestView =requestView2;
        		   break;
        		}
			}
        	if(requestView!=null)
        	{

        		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
        	}
        	else
        	{
        	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
        	   putContractInRequestMap((ContractView)dataTable.getRowData());
        	}
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
		}
		catch (Exception e) {
			logger.LogException( "cmdChangeTenant_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.CHANGE_TENANT_NAME;
	}
	@SuppressWarnings( "unchecked" )
	public String cmdAmendLeaseContract_Click(){
        try{

        	ContractView contractView=(ContractView)dataTable.getRowData();
        	RequestServiceAgent rsa=new RequestServiceAgent();
        	RequestView requestView =new RequestView();
        	List<RequestView> requestViewList =new ArrayList<RequestView>(0);
        	//requestView.setContractView(contractView);//Id(contractView.getContractId());
        	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
        	RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);
        	requestViewList=
        		              rsa.getRequestFromRequestField(contractView.getContractId().toString(),
        		            		                         requestView.getRequestTypeId().toString(),
        		            		                         requestKeyView.getRequestKeyId().toString(),
        		            		                         null
        		                                            );




        	//List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null);

        	DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
        			                                    WebConstants.REQUEST_STATUS_COMPLETE);
        	DomainDataView ddvRejected= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
					WebConstants.REQUEST_STATUS_REJECTED);
	        DomainDataView ddvCancelled= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
					WebConstants.REQUEST_STATUS_CANCELED);
        	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
        	//if there is some request for Amend against this
        	//contract and its status is not completed/rejected/cancelled then display that request
        	//otherwise create new screen
        	requestView=null;
        	for (RequestView requestView2 : requestViewList) {

        		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0 &&
        		   requestView2.getStatusId().compareTo(ddvCancelled.getDomainDataId())!=0 &&
        		   requestView2.getStatusId().compareTo(ddvRejected.getDomainDataId())!=0
        		)
        		{
        			requestView =requestView2;
        		   break;
        		}
			}
        	if(requestView!=null)
        	{

        		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
        		setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
        	}
        	else
        	{
        	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, dataTable.getRowData());
        	   putContractInRequestMap((ContractView)dataTable.getRowData());
        	   setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
        	}
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
		
		}
		catch (Exception e) {
			logger.LogException( "cmdAmendLeaseContract_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.AMEND_CONTRACT;
	}




	@SuppressWarnings( "unchecked" )
	public String cmdTransferContract_Click()
	{
        try
        {

        	ContractView cv = (ContractView)dataTable.getRowData();
        	GRPService grpService=new GRPService();
    		if(grpService.areAllTransactionsPostedToGRP(cv.getContractNumber()))
    		{
    			errorMessages=new ArrayList<String>();
        		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeTerminatedTrasnferred"));
        		return null;
    		}        	
        	setRequestParam(WebConstants.Contract.CONTRACT_VIEW, cv);
        	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONTRACT_SEARCH);
		}
		catch (Exception e) 
		{
			logger.LogException("cmdTransferContract_Click|crashed...", e);
		}
		return WebConstants.ContractListNavigation.TRANSFER_CONTRACT;
	}

	@SuppressWarnings ( "unchecked" )
	public void cmdCancelContract_Click()
	{

        try
        {
        	ContractView cv = (ContractView)dataTable.getRowData();
        	GRPService grpService=new GRPService();
        	DomainDataView ddvActive= CommonUtil.getIdFromType( getContractStatusList(), WebConstants.CONTRACT_STATUS_ACTIVE);
        	if(cv!=null)
        		viewMap.put("CONTRACT_TO_BE_CANCELLED", cv);
        	if(cv.getStatus().compareTo(ddvActive.getDomainDataId())==0)
        	{

        		if(grpService.isAnyTransactionPostedToGRP(cv.getContractNumber()))
        		{
        			errorMessages=new ArrayList<String>();
	        		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeCancelled"));
        		}
	        	else
	        		openPopup("openRemarksPopup();");
        	}
        	else
        		openPopup("openRemarksPopup();");

		}
		catch (Exception e)
		{

			logger.LogException(  "cmdCancelContract_Click|crashed...", e);
		}
	}

	public void cancelContract()
	{
		try
		{
		String remarks="";
		successMessages=new ArrayList<String>();
    	if(sessionMap.containsKey("CANCEL_REMARKS") && sessionMap.get("CANCEL_REMARKS")!=null)
    		{
    			remarks=sessionMap.get("CANCEL_REMARKS").toString();
    			sessionMap.remove("CANCEL_REMARKS");
    		}
	    	ContractView cv=new ContractView();
	    	if(viewMap.get("CONTRACT_TO_BE_CANCELLED")!=null)
	    		cv=(ContractView) viewMap.get("CONTRACT_TO_BE_CANCELLED");
	    	new PropertyService().cancelContractFromSearch( cv.getContractId().longValue(), getLoggedInUserId());
	    	Long requestId=new PropertyService().getFirstRequestByContract(cv.getContractId());
	    	//For migrated data the request will not be present
	    	if( requestId !=null )
	    	{
	    	 if(remarks!=null && remarks.trim().length()>0)
	    		CommonUtil.saveRemarksAsComments(requestId, remarks, WebConstants.REQUEST);
	    	}
	    	NotesController.saveSystemNotesForRequest(WebConstants.CONTRACT,MessageConstants.ContractEvents.CONTRACT_CANCELLED, cv.getContractId() );
	    	searchContracts();
		    successMessages.add(CommonUtil.getBundleMessage("contractCancel.SuccesMsg"));
		}
		catch (PimsBusinessException e)
		{
			errorMessages=new ArrayList<String>();
			if(e.getExceptionCode().compareTo(ExceptionCodes.CONTRACT_CANNOT_BE_CANCELED)==0)
				errorMessages.add(CommonUtil.getBundleMessage("contractCantBeCancelled"));
			logger.LogException("cancelContract() crashed...", e);
		}
		catch (Exception e)
		{
			errorMessages=new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("cancelContract() crashed...", e);
		}

	}
	@SuppressWarnings( "unchecked" )
	private void putContractInRequestMap(ContractView contractView) {
        try{
        	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
		
		}
		catch (Exception e) {
			logger.LogException(  "putContractInSession|crashed...", e);
		}
	}

	public String getTenantNumber() {
		return tenantNumber;
	}

	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getTenantTypeList() {

		

		try {

			tenantTypeList.clear();
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_KIND);
          for(int i=0;i< typeComboList.size();i++)
          {
        	 Long isCompany=0L;
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
          	if(ddv.getDataValue().compareTo(WebConstants.PERSON_KIND_COMPANY)==0)
          		isCompany=1L;
        	if (isEnglishLocale)
        		item = new SelectItem(isCompany.toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(isCompany.toString(), ddv.getDataDescAr());


        	tenantTypeList.add(item);
          }
          Collections.sort(tenantTypeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError("getTenantTypeList| crashed due to:"+e.getStackTrace());
			}

		

		return tenantTypeList;
	}
	@SuppressWarnings( "unchecked" )
	public void setTenantTypeList(List<SelectItem> tenantTypeList) {
		this.tenantTypeList = tenantTypeList;
	}

	public String getPropertyComercialName() {
		return propertyComercialName;
	}

	public void setPropertyComercialName(String propertyComercialName) {
		this.propertyComercialName = propertyComercialName;
	}

	public String getPropertyEndowedName() {
		return propertyEndowedName;
	}

	public void setPropertyEndowedName(String propertyEndowedName) {
		this.propertyEndowedName = propertyEndowedName;
	}

	public String getPropertyUsage() {
		return propertyUsage;
	}

	public void setPropertyUsage(String propertyUsage) {
		this.propertyUsage = propertyUsage;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getPropertyTypeList() {

		

		try {

			propertyTypeList.clear();

          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.PROPERTY_TYPE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());


        	propertyTypeList.add(item);
          }
          Collections.sort(propertyTypeList,ListComparator.LIST_COMPARE);
		}catch(Exception e){
			logger.logError( "getPropertyTypeList| crashed due to:"+e.getStackTrace());
			}

		

		return propertyTypeList;
	}

	public void setPropertyTypeList(List<SelectItem> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}
    @SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyUsageList()
	{

		

		try
		{

			propertyUsageList.clear();
          List<DomainDataView> typeComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.PROPERTY_USAGE);
          for(int i=0;i< typeComboList.size();i++)
          {
          	DomainDataView ddv=(DomainDataView)typeComboList.get(i);
          	SelectItem item=null;
        	if (isEnglishLocale)
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
        	else
        		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
        	propertyUsageList.add(item);
          }

          Collections.sort(propertyUsageList,ListComparator.LIST_COMPARE);
          
		}
		catch(Exception e)
		{
			logger.LogException("getPropertyUsageList| crashed due to:",e);
		}





		return propertyUsageList;
	}

	public void setPropertyUsageList(List<SelectItem> propertyUsageList) {
		this.propertyUsageList = propertyUsageList;
	}

	public String getFromContractExpDate() {
		return fromContractExpDate;
	}

	public void setFromContractExpDate(String fromContractExpDate) {
		this.fromContractExpDate = fromContractExpDate;
	}

	public String getToContractExpDate() {
		return toContractExpDate;
	}

	public void setToContractExpDate(String toContractExpDate) {
		this.toContractExpDate = toContractExpDate;
	}

	public Double getFromRentAmount() {
		return fromRentAmount;
	}

	public void setFromRentAmount(Double fromRentAmount) {
		this.fromRentAmount = fromRentAmount;
	}

	public Double getToRentAmount() {
		return toRentAmount;
	}

	public void setToRentAmount(Double toRentAmount) {
		this.toRentAmount = toRentAmount;
	}

	public String getCountries() {
		return countries;
	}

	public void setCountries(String countries) {
		this.countries = countries;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}
	@SuppressWarnings( "unchecked" )
	public void setStreet(String street) {
		this.street = street;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getCountryList() {


		try {

			countryList.clear();

          List<RegionView> countryComboList=(ArrayList<RegionView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.SESSION_COUNTRY);

          for(int i=0;i< countryComboList.size();i++)
          {
        	  RegionView rV=(RegionView)countryComboList.get(i);

          	SelectItem item=null;

          	if (isEnglishLocale)
        		item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn().toString());
        	else
        		item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr().toString());


        	countryList.add(item);
          }
		}
		catch(Exception e)
		{
			logger.LogException( "getCountryList| crashed due to:", e );
		}



		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {

		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public List<SelectItem> getCityList() {
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}


	public Date getToContractExpDateRich() {
		return toContractExpDateRich;
	}


	public void setToContractExpDateRich(Date toContractExpDateRich) {
		this.toContractExpDateRich = toContractExpDateRich;
	}


	public Date getFromContractDateRich() {
		return fromContractDateRich;
	}


	public void setFromContractDateRich(Date fromContractDateRich) {
		this.fromContractDateRich = fromContractDateRich;
	}


	public Date getToContractDateRich() {
		return toContractDateRich;
	}


	public void setToContractDateRich(Date toContractDateRich) {
		this.toContractDateRich = toContractDateRich;
	}


	public Date getFromContractExpDateRich() {
		return fromContractExpDateRich;
	}


	public void setFromContractExpDateRich(Date fromContractExpDateRich) {
		this.fromContractExpDateRich = fromContractExpDateRich;
	}


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}



	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	@SuppressWarnings( "unchecked" )
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	@SuppressWarnings( "unchecked" )
	public String reset()
	{
		ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");

		  contractListCriteriaBackingBean.setUnitRefNum("");
		  contractListCriteriaBackingBean.setContractType("");
		  contractListCriteriaBackingBean.setContractStatus("");
		  contractListCriteriaBackingBean.setContractNumber("");
		  contractListCriteriaBackingBean.setStartDate ("");
		  contractListCriteriaBackingBean.setEndDate ("");
		  contractListCriteriaBackingBean.setTenantNumber ("");
		  contractListCriteriaBackingBean.setTenantName ("");
		  contractListCriteriaBackingBean.setTenantType ("");
		  contractListCriteriaBackingBean.setPropertyComercialName ("");
		  contractListCriteriaBackingBean.setPropertyEndowedName ("");
		  contractListCriteriaBackingBean.setPropertyUsage ("");
		  contractListCriteriaBackingBean.setPropertyType ("");
		  contractListCriteriaBackingBean.setCountries ("");
		  contractListCriteriaBackingBean.setStates ("");
		  contractListCriteriaBackingBean.setCity ("");
		  contractListCriteriaBackingBean.setStreet ("");
		  contractListCriteriaBackingBean.setFromContractDate ("");
		  contractListCriteriaBackingBean.setToContractDate ("");
		  contractListCriteriaBackingBean.setFromContractExpDate ("");
		  contractListCriteriaBackingBean.setToContractExpDate ("");
		  contractListCriteriaBackingBean.setFromRentAmount (null);
		  contractListCriteriaBackingBean.setToRentAmount (null);
		  contractListCriteriaBackingBean.setFromContractDateRich (null);
		  contractListCriteriaBackingBean.setToContractDateRich (null);
		  contractListCriteriaBackingBean.setFromContractExpDateRich (null);
		  contractListCriteriaBackingBean.setToContractExpDateRich (null);
		  contractListCriteriaBackingBean.setFromContractStartDateRich(null);
		  contractListCriteriaBackingBean.setToContractStartDateRich(null);
		  contractListCriteriaBackingBean.setUserName("");
		  contractListCriteriaBackingBean.setEndowedNo("");
		  contractListCriteriaBackingBean.setUnitCostCenter("");
		  contractListCriteriaBackingBean.setUnitDesc("");

		return "";
	}
	private boolean hasErrorsRenewContract(ContractView contract) throws Exception
	{
		boolean hasErrors =false;
		//Function Name is old Payment Exists but here it checks for all the payment of current contracts which
		//are not realized or pending
		if (CommonUtil.isOldPendingPaymentsExists( contract.getContractId(), contract.getRentAmount() ))
		{
			//Please Pay Old Payments
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAY_EXISTING_PAYENTS ));
			hasErrors = true;
		}
		return hasErrors;
	}
	@SuppressWarnings( "unchecked" )
	public String renewContract()
	{
		ContractView selectedContract = (ContractView) dataTable.getRowData();
		try
		{
			RequestView requestView = null;
			if( !hasErrorsRenewContract( selectedContract ) )
			{
				Long lastContractId = propertyService.getLastContractIdByOldContractId(selectedContract.getContractId());
				if(lastContractId !=null)
	    			requestView = new CommonUtil().getIncompleteRequestForRequestType(lastContractId ,WebConstants.REQUEST_TYPE_RENEW_CONTRACT);
				if(requestView !=null)
					FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
				else
				{
				    setRequestParam("contractId", selectedContract.getContractId());
					setRequestParam("suggestedRentAmount", 0.0);
					setRequestParam("oldRentValue", selectedContract.getRentAmount());
					setRequestParam("fromRenewList", "true");
				}
				return "renew";
			}
				return "";
		}
		catch(Exception e)
		{
			logger.LogException( "renewContract|Error Occured::",e);
		}
		return "";
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getIsContrctStatusNew() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddvNew= CommonUtil.getIdFromType(
				                                      getContractStatusList(),
				                                      WebConstants.CONTRACT_STATUS_NEW
				                                      );
		DomainDataView ddvApproved= CommonUtil.getIdFromType(
                getContractStatusList(),
                WebConstants.CONTRACT_STATUS_APPROVED
                );
		DomainDataView ddvActive= CommonUtil.getIdFromType(
                getContractStatusList(),
                WebConstants.CONTRACT_STATUS_ACTIVE
                );
		if(contractView.getStatus().toString().equals(ddvNew.getDomainDataId().toString())
			|| contractView.getStatus().toString().equals(ddvActive.getDomainDataId().toString())
			|| contractView.getStatus().toString().equals(ddvApproved.getDomainDataId().toString()))
			isContrctStatusNew = true;
		else
			isContrctStatusNew  = false;


		return isContrctStatusNew ;
	}



	@SuppressWarnings( "unchecked" )

	public Boolean getIsContrctStatusActive() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(
				                                      getContractStatusList(),
				                                      WebConstants.CONTRACT_STATUS_ACTIVE
				                                      );
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isContrctStatusActive= true;
		else
			isContrctStatusActive= false;


		return isContrctStatusActive;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getIsContrctStatusComplete() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(
														getContractStatusList(),
														WebConstants.CONTRACT_STATUS_COMPLETE);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isContrctStatusComplete= true;
		else
			isContrctStatusComplete= false;

		return isContrctStatusComplete;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getIsContrctStatusExpired() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(
														getContractStatusList(),
														WebConstants.CONTRACT_STATUS_EXPIRED);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isContrctStatusExpired= true;
		else
			isContrctStatusExpired= false;

		return isContrctStatusExpired;
	}

 @SuppressWarnings( "unchecked" )
	public Boolean getIsContrctStatusRenewDraft() {

		ContractView contractView = (ContractView)dataTable.getRowData();
		DomainDataView ddv= CommonUtil.getIdFromType(getContractStatusList(), WebConstants.CONTRACT_STATUS_RENEW_DRAFT);
		if(contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			isContrctStatusRenewDraft= true;
		else
			isContrctStatusRenewDraft= false;

		return isContrctStatusRenewDraft;
	}


	private void retrieveSearchCriteria()
	{


		if(sessionMap.containsKey("contractNumber") && sessionMap.get("contractNumber") != null)
		{
			setContractNumber((String)sessionMap.get("contractNumber"));
			sessionMap.remove("contractNumber");
		}
		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{
			setContractType((String)sessionMap.get("contractType"));
			sessionMap.remove("contractType");
		}

		if(sessionMap.containsKey("fromContractDate") && sessionMap.get("fromContractDate") != null)
			{
				setFromContractDateRich((Date)sessionMap.get("fromContractDate"));
				sessionMap.remove("fromContractDate");
			}

		if(sessionMap.containsKey("toContractDate") && sessionMap.get("toContractDate") != null)
			{
				setToContractDateRich((Date)sessionMap.get("toContractDate"));
				sessionMap.remove("toContractDate");
			}


		if(sessionMap.containsKey("fromContractExpDateRich") && sessionMap.get("fromContractExpDateRich") != null)
			{
				setFromContractExpDateRich((Date)sessionMap.get("fromContractExpDateRich") );
				sessionMap.remove("fromContractExpDateRich");
			}


		if(sessionMap.containsKey("toContractExpDateRich") && sessionMap.get("toContractExpDateRich") != null)
			{
				setToContractExpDateRich((Date)sessionMap.get("toContractExpDateRich"));
				sessionMap.remove("toContractExpDateRich");
			}


		if(sessionMap.containsKey("contractStatus") && sessionMap.get("contractStatus") != null)
			{
				setContractStatus((String)sessionMap.get("contractStatus"));
				sessionMap.remove("contractStatus");
			}


		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
			{
				setContractType((String)sessionMap.get("contractType"));
				sessionMap.remove("contractType");
			}


		if(sessionMap.containsKey("tenantNumber") && sessionMap.get("tenantNumber") != null)
			{
				setTenantNumber((String)sessionMap.get("tenantNumber"));
				sessionMap.remove("tenantNumber");
			}

		if(sessionMap.containsKey("tenantName") && sessionMap.get("tenantName") != null)
			{
				setTenantName((String)sessionMap.get("tenantName"));
				sessionMap.remove("tenantName");
			}


		if(sessionMap.containsKey("tenantType") && sessionMap.get("tenantType") != null)
			{
				setTenantType((String)sessionMap.get("tenantType"));
				sessionMap.remove("tenantType");
			}


		if(sessionMap.containsKey("propertyComercialName") && sessionMap.get("propertyComercialName") != null)
			{
				setPropertyComercialName((String)sessionMap.get("tenantType"));
				sessionMap.remove("propertyComercialName");
			}


		if(sessionMap.containsKey("propertyEndowedName") && sessionMap.get("propertyEndowedName") != null)
			{
				setPropertyEndowedName((String)sessionMap.get("propertyEndowedName"));
				sessionMap.remove("propertyEndowedName");
			}


		if(sessionMap.containsKey("propertyType") && sessionMap.get("propertyType") != null)
			{
				setPropertyType((String)sessionMap.get("propertyType"));
				sessionMap.remove("propertyType");
			}


		if(sessionMap.containsKey("propertyUsage") && sessionMap.get("propertyUsage") != null)
			{
				setPropertyUsage((String)sessionMap.get("propertyUsage"));
				sessionMap.remove("propertyUsage");
			}

		if(sessionMap.containsKey("fromRentAmount") && sessionMap.get("fromRentAmount") != null)
			{
				setFromRentAmount((Double)sessionMap.get("fromRentAmount"));
				sessionMap.remove("fromRentAmount");
			}

		if(sessionMap.containsKey("toRentAmount") && sessionMap.get("toRentAmount") != null)
			{
				setToRentAmount((Double)sessionMap.get("toRentAmount"));
				sessionMap.remove("toRentAmount");
			}

		if(sessionMap.containsKey("countries") && sessionMap.get("countries") != null)
			{
				setCountries((String)sessionMap.get("countries"));
				sessionMap.remove("countries");
			}

		if(sessionMap.containsKey("states") && sessionMap.get("states") != null)
			{
				setStates((String)sessionMap.get("states"));
				sessionMap.remove("states");
			}


		if(sessionMap.containsKey("unitRefNum") && sessionMap.get("unitRefNum") != null)
		{
			setUnitRefNum((String)sessionMap.get("unitRefNum"));
			sessionMap.remove("unitRefNum");
		}

/*		if(!this.countries.equals("-1"))
		{
			stateList=getStateList(new Long(countries));
			this.setStateList(stateList);
		}

		if(!this.states.equals("-1"))
		{
			cityList=getCityList(new Long(states));
			this.setCityList(cityList);
		}*/

		sessionMap.remove("preserveSearchCriteria");

		searchContracts();
	}

	public List<SelectItem> getCityList(Long stateId) {

		try {
			Set<RegionView> regionViewList = propertyServiceAgent.getCity(stateId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				cityList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("", e);
		}
		return cityList;
	}

	public List<SelectItem> getStateList(Long countryId) {

		try {
			Set<RegionView> regionViewList = propertyServiceAgent.getState(countryId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				stateList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("getStateList", e);
		}
		return stateList;
	}

	public void clearSessionMap()
	{

		if(sessionMap.containsKey("unitRefNum") && sessionMap.get("unitRefNum") != null)
		{
			sessionMap.remove("unitRefNum");
		}

		if(sessionMap.containsKey("contractNumber") && sessionMap.get("contractNumber") != null)
		{
			sessionMap.remove("contractNumber");
		}
		if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{
			sessionMap.remove("contractType");
		}
		if(sessionMap.containsKey("fromContractDate") && sessionMap.get("fromContractDate") != null)
		{
			sessionMap.remove("fromContractDate");
		}

		if(sessionMap.containsKey("toContractDate") && sessionMap.get("toContractDate") != null)
		{

			sessionMap.remove("toContractDate");
		}


	if(sessionMap.containsKey("fromContractExpDateRich") && sessionMap.get("fromContractExpDateRich") != null)
		{

			sessionMap.remove("fromContractExpDateRich");
		}


	if(sessionMap.containsKey("toContractExpDateRich") && sessionMap.get("toContractExpDateRich") != null)
		{

			sessionMap.remove("toContractExpDateRich");
		}


	if(sessionMap.containsKey("contractStatus") && sessionMap.get("contractStatus") != null)
		{

			sessionMap.remove("contractStatus");
		}


	if(sessionMap.containsKey("contractType") && sessionMap.get("contractType") != null)
		{

			sessionMap.remove("contractType");
		}


	if(sessionMap.containsKey("tenantNumber") && sessionMap.get("tenantNumber") != null)
		{

			sessionMap.remove("tenantNumber");
		}

	if(sessionMap.containsKey("tenantName") && sessionMap.get("tenantName") != null)
		{

			sessionMap.remove("tenantName");
		}


	if(sessionMap.containsKey("tenantType") && sessionMap.get("tenantType") != null)
		{
			sessionMap.remove("tenantType");
		}


	if(sessionMap.containsKey("propertyComercialName") && sessionMap.get("propertyComercialName") != null)
		{

			sessionMap.remove("propertyComercialName");
		}


	if(sessionMap.containsKey("propertyEndowedName") && sessionMap.get("propertyEndowedName") != null)
		{

			sessionMap.remove("propertyEndowedName");
		}


	if(sessionMap.containsKey("propertyType") && sessionMap.get("propertyType") != null)
		{

			sessionMap.remove("propertyType");
		}


	if(sessionMap.containsKey("propertyUsage") && sessionMap.get("propertyUsage") != null)
		{

			sessionMap.remove("propertyUsage");
		}

	if(sessionMap.containsKey("fromRentAmount") && sessionMap.get("fromRentAmount") != null)
		{

			sessionMap.remove("fromRentAmount");
		}

	if(sessionMap.containsKey("toRentAmount") && sessionMap.get("toRentAmount") != null)
		{

			sessionMap.remove("toRentAmount");
		}

	if(sessionMap.containsKey("countries") && sessionMap.get("countries") != null)
		{

			sessionMap.remove("countries");
		}

	if(sessionMap.containsKey("states") && sessionMap.get("states") != null)
		{

			sessionMap.remove("states");
		}
	}


	public RenewContractsReminderLetterCriteria getReportCriteria() {
		return reportCriteria;
	}


	public void setReportCriteria(RenewContractsReminderLetterCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}

	@SuppressWarnings( "unchecked" )
    public String getErrorMessages()
	{


		return CommonUtil.getErrorMessages(errorMessages);
	}
	@SuppressWarnings( "unchecked" )
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0))
		{
			messageList = "";
		}
		else
		{

			for (String message : successMessages)
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }

		}
		return (messageList);
	}
	@SuppressWarnings("unchecked")
	public void checkBoxClick(ValueChangeEvent e)
	{
		ContractView clickedContract = (ContractView) dataTable.getRowData();
		if(viewMap.get("SELECTED_CONTRACTS") != null)
			selectedContracts= (Map) viewMap.get("SELECTED_CONTRACTS");

		if((Boolean)e.getNewValue())
		{
			if( !selectedContracts.containsKey(clickedContract.getContractId()) )
				selectedContracts.put( clickedContract.getContractId(),clickedContract);
		}
		else
		{
			if(selectedContracts.containsKey(clickedContract.getContractId()))
				selectedContracts.remove(clickedContract.getContractorId());
		}
		viewMap.put("SELECTED_CONTRACTS", selectedContracts);

	}


	public Map getSelectedContracts() {
		return selectedContracts;
	}


	public void setSelectedContracts(Map selectedContracts) {
		this.selectedContracts = selectedContracts;
	}


	public String getHdnSelectedContracts() {
		return hdnSelectedContracts;
	}


	public void setHdnSelectedContracts(String hdnSelectedContracts) {
		this.hdnSelectedContracts = hdnSelectedContracts;
	}
	public String openMissingPaymentsScreen()
	{
		ContractView selectedContractView=(ContractView)dataTable.getRowData();
		sessionMap.put("CONTRACT_VIEW", selectedContractView);

		return "AddMissingPaymentsScreen";
	}


	public String getHdnTenantId() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		if(viewMap.containsKey("hdnTenantId") && viewMap.get("hdnTenantId")!=null)
			hdnTenantId = viewMap.get("hdnTenantId").toString();
		return hdnTenantId;
	}


	public void setHdnTenantId(String hdnTenantId) {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		this.hdnTenantId = hdnTenantId;
		if(this.hdnTenantId!=null)
		{
			viewMap.put("hdnTenantId", this.hdnTenantId);

		}
	}


	public String getPageMode() {
		return pageMode;
	}


	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	public String getBlockComments() {
		return blockComments;
	}


	public void setBlockComments(String blockComments) {
		this.blockComments = blockComments;
	}


	public String getUnblockComments() {
		return unblockComments;
	}


	public void setUnblockComments(String unblockComments) {
		this.unblockComments = unblockComments;
	}



}

