package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionRequestRegView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderAttendanceView;


public class ConductAuction extends AbstractController{

	//Variables
	 private transient Logger logger = Logger.getLogger(ConductAuction.class);
	
	 private HtmlDataTable dataTableBidder;

	 private BidderAttendanceView dataItemBidder = new BidderAttendanceView();
	 
	 private List<BidderAttendanceView> dataListBidder = new ArrayList<BidderAttendanceView>();
	 
	 private HtmlDataTable dataTableUnitsToExclude;

	 private AuctionRequestRegView dataItemUnitsToExclude = new AuctionRequestRegView();
	 
	 private List<AuctionRequestRegView> dataListUnitsToExclude = new ArrayList<AuctionRequestRegView>();
	 
	 private String hiddenAttendanceIds;
	 
	 private String hiddenUnitsToExclude;
	 
	 private static final String BIDDER_ATTENDANCE = "BIDDER_ATTENDANCE";
	 
	 private static final String AUCTION_UNIT_EXCLUSION = "AUCTION_UNIT_EXCLUSION";
	 
	private String auctionId;
	
	private String auctionNo;
	
	private String auctionDate;
	
	private String auctionTitle;
	
	private String auctionVenueName;
	
	private HtmlDataTable dataTableAuctionUnitRequests;
	
	private List<AuctionWinnersView> dataListAuctionUnitRequests = new ArrayList<AuctionWinnersView>();
	
	private AuctionWinnersView dataItemAuctionUnitRequests = new AuctionWinnersView(); 
	
	private List<String> messages;
	
	private String hdnResultPopupCalled;
	 
	//Constructor
	/** default constructor */
	public ConductAuction(){		
	}
	
	//Methods
	
	private void loadDataListBidder() {	
		String  methodName = "loadDataListBidder";
        logger.logInfo(methodName+" started...");
//		System.out.println("INSIDE loadDataListBidder()");
		try{
		
//			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			List<BidderAttendanceView> bidders = new ArrayList<BidderAttendanceView>();;
//			
//			if(sessionMap.containsKey(BIDDER_ATTENDANCE) == false){
				bidders = getAuctionBidders();
//				sessionMap.put(BIDDER_ATTENDANCE, bidders);
				dataListBidder = bidders;
//			}
//			else{
//				dataListBidder = (List<BidderAttendanceView>)sessionMap.get(BIDDER_ATTENDANCE);
//			}
			logger.logInfo(methodName+" completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}		
	}
	
	private void loadUnitsToExclude() {
		String  methodName = "loadUnitsToExclude";
        logger.logInfo(methodName+" started...");		
		try{
//			System.out.println("INSIDE loadUnitsToExclude");
//			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			List<AuctionRequestRegView> units = new ArrayList<AuctionRequestRegView>();
			
//			if(sessionMap.containsKey(AUCTION_UNIT_EXCLUSION) == false){
				units = getAuctionUnits();
//				sessionMap.put(AUCTION_UNIT_EXCLUSION, units);
				dataListUnitsToExclude = units;
//			}
//			else{
//				dataListUnitsToExclude = (List<AuctionRequestRegView>)sessionMap.get(AUCTION_UNIT_EXCLUSION);			
//			}
			logger.logInfo(methodName+" completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}		
	}
	
	public String save(){
		String  methodName = "save";
        logger.logInfo(methodName+" started...");		
				
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		
		//////////UPDATING ATTENDANCE OF AUCTION PARTICIPANTS
//		System.out.println(hiddenAttendanceIds);
		ArrayList<BidderAttendanceView> bidders = getupdatedAttendanceList();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		try {
			logger.logInfo("saveAttendance started...");
			Boolean updateStatus = pSAgent.updateBidderAttendance(bidders);
			
			//put bidders attendance in session
			sessionMap.put(BIDDER_ATTENDANCE, bidders);
			dataListBidder = bidders;	
//			System.out.println("Bidder Attendance marking update status:" + updateStatus);
			logger.logInfo("saveAttendance completed successfully...");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logInfo("saveAttendance crashed...");
			e.printStackTrace();
		}
		//////////UPDATING INCLUSION EXCLUSION OF AUCTION UNITS
//		System.out.println(hiddenUnitsToExclude);
		ArrayList<AuctionRequestRegView> auctionUnits = getUpdatedAuctionUnitList();
		try {
			logger.logInfo("saveUnitExclusion started...");
			Boolean updateStatus = pSAgent.updateAuctionUnits(auctionUnits);
			
			//put auction units inclusion/exclusion in session
			sessionMap.put(AUCTION_UNIT_EXCLUSION, auctionUnits);
			dataListUnitsToExclude = auctionUnits;
//			System.out.println("Auction Unit exclusion update status:" + updateStatus);
			logger.logInfo("saveUnitExclusion completed successfully...");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logInfo("saveUnitExclusion crashed...");
			e.printStackTrace();
		}
		logger.logInfo(methodName+" ended...");
		return "";		
	}
	
	private ArrayList<AuctionRequestRegView> getUpdatedAuctionUnitList() {
		String  methodName = "getUpdatedAuctionUnitList";
		ArrayList<AuctionRequestRegView> updatedAuctionUnits = getAuctionUnits();
		try{
			logger.logInfo(methodName+" started...");
			HashMap auctionUnitIdMap = getIdMap(hiddenUnitsToExclude);
			
			Iterator i = updatedAuctionUnits.iterator();
			while(i.hasNext()){
				AuctionRequestRegView auctionUnitView = (AuctionRequestRegView)i.next();
				if(auctionUnitIdMap.containsKey(auctionUnitView.getAuctionUnitId())){
					Boolean excluded = (Boolean)auctionUnitIdMap.get(auctionUnitView.getAuctionUnitId());
					
					if(excluded)
						auctionUnitView.setExcluded(true);
					else
						auctionUnitView.setExcluded(false);
				}			
			}
			logger.logInfo(methodName+" completed successfully...");
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}		
		return updatedAuctionUnits;
	}

	private ArrayList<AuctionRequestRegView> getAuctionUnits() {
		List<AuctionRequestRegView> auctionUnits = new ArrayList<AuctionRequestRegView>();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		logger.logInfo("getAuctionUnits started...");
		try {
			AuctionView auctionView = new AuctionView();
			auctionView.setAuctionId(Long.parseLong(getAuctionId()));
			auctionUnits = pSAgent.getAllAuctionUnits(auctionView);
			logger.logInfo("getAuctionUnits completed successfully...");
		} catch (PimsBusinessException e) {			
			logger.logInfo("getAuctionUnits crashed...");
			e.printStackTrace();
		}		
		return (ArrayList<AuctionRequestRegView>)auctionUnits;
	}

	private ArrayList<BidderAttendanceView> getupdatedAttendanceList() {
		
		ArrayList<BidderAttendanceView> updatedBidders = getAuctionBidders();
		logger.logInfo("getupdatedAttendanceList started...");
		try{
			HashMap attIdMap = getIdMap(hiddenAttendanceIds);
			
			Iterator i = updatedBidders.iterator();
			while(i.hasNext()){
				BidderAttendanceView bidderAttView = (BidderAttendanceView)i.next();
				if(attIdMap.containsKey(bidderAttView.getBidderAuctionRegId())){
					Boolean attended = (Boolean)attIdMap.get(bidderAttView.getBidderAuctionRegId());
					
					if(attended)
						bidderAttView.setAttendance(true);
					else
						bidderAttView.setAttendance(false);
				}			
			}
			logger.logInfo("getupdatedAttendanceList completed successfully...");
		} catch (Exception e) {			
			logger.logInfo("getupdatedAttendanceList crashed...");
			e.printStackTrace();
		}		
		return updatedBidders;
	}

		List<BidderAttendanceView> bidders = new ArrayList<BidderAttendanceView>();
		private ArrayList<BidderAttendanceView> getAuctionBidders() {
//		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
//		logger.logInfo("getAuctionBidders started...");
//		
//		try {
//			bidders = null;// pSAgent.getAuctionBidders(Long.parseLong(getAuctionId()));
//			logger.logInfo("getAuctionBidders completed successfully...");
//		} catch (PimsBusinessException e) {	
//			logger.logInfo("getAuctionBidders crashed...");
//			e.printStackTrace();
//		}
		return (ArrayList<BidderAttendanceView>)bidders;
	}
	
	private HashMap getIdMap(String ids){
		HashMap map = new HashMap();
		logger.logInfo("getIdMap started...");
		
		try {
			if(ids.length() > 0){
				String[] idArray = ids.split(",");
				for(int i=0;i<idArray.length;i++){
					Long key = Long.parseLong(idArray[i].split(":")[1]);
					String value = idArray[i].split(":")[0];
					if(value.equals("1"))
						map.put(key, true);
					else
						map.put(key, false);
				}
			}
			logger.logInfo("getIdMap completed successfully...");
		} catch (Exception e) {	
			logger.logInfo("getIdMap crashed...");
			e.printStackTrace();
		}
		return map;
	}

	private void loadAuctionInfo() {
		logger.logInfo("loadAuctionInfo started...");
		
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			AuctionView auctionView = pSAgent.getAuctionById(Long.parseLong(getAuctionId()));
			setAuctionDate(auctionView.getAuctionDate());
			setAuctionNo(auctionView.getAuctionNumber());
			setAuctionTitle(auctionView.getAuctionTitle());
			setAuctionVenueName(auctionView.getVenueName());			
			logger.logInfo("loadAuctionInfo completed successfully...");
		} catch (Exception e) {	
			logger.logInfo("loadAuctionInfo crashed...");
			e.printStackTrace();
		}
	}
	
	private void loadAuctionRequestsList() {
		String METHOD_NAME = "loadAuctionRequestsList";
		logger.logInfo(METHOD_NAME + " started...");
		
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();			
			List<AuctionWinnersView> auctionWinners = pSAgent.getAuctionParticipants(Long.parseLong(getAuctionId()));
			dataListAuctionUnitRequests = auctionWinners; 
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception e) {	
			logger.logInfo(METHOD_NAME + " crashed...");
			e.printStackTrace();
		}
	}
	
	public String editRequest(){

		AuctionWinnersView auctionWinnersView = (AuctionWinnersView)dataTableAuctionUnitRequests.getRowData();
		
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	 	sessionMap.put(WebConstants.ConductAuction.AUCTION_WINNER,auctionWinnersView);
		/////////////////////////////////////////////////////////////////////////////
		final String viewId = "/ConductAuction.jsp"; 
		
        FacesContext facesContext = FacesContext.getCurrentInstance();        

        // This is the proper way to get the view's url
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);

        //Your Java script code here
//        String javaScriptText = "javascript:showEditBidPrice();";
        AuctionWinnersView auctionWinnerView = (AuctionWinnersView)dataTableAuctionUnitRequests.getRowData();
        String javaScriptText = "javascript:showSelectWinner("+auctionWinnerView.getAuctionId()+","+auctionWinnerView.getAuctionUnitId()+","+auctionWinnerView.getBidPrice()+");";
//        String javaScriptText = "javascript:showSelectWinner();";
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	
	public String completeAuction(){
		String METHOD_NAME = "completeAuction";
		try {
//			logger.logInfo(METHOD_NAME + " started...");
//			PIMSConductAuctionBPELPortClient client = new PIMSConductAuctionBPELPortClient();
////			System.out.println(client.getEndpoint());
//			client.setEndpoint("http://soa:8889/orabpel/default/PIMSConductAuctionBPEL/1.0");
//			Long auctionId = Long.parseLong(getAuctionId());
//			client.initiate(auctionId, null, null);
//			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.logInfo(METHOD_NAME + " crashed...");
			e.printStackTrace();
		} 
		return "";
	}
	
	private void setRequestInfoInSession() {
		String METHOD_NAME = "setRequestInfoInSession";
		logger.logInfo(METHOD_NAME + " started...");
		try{		
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();						
			Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
			 Object obj = requestMap.get(WebConstants.AUCTION_ID);
			 if(obj != null){
				 sessionMap.put(WebConstants.ConductAuction.CONDUCT_AUCTION_ID, obj);
			 }
			 logger.logInfo(METHOD_NAME + " completed successfully...");
		}catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
	}
	
	private void showResultMessage() {
		String METHOD_NAME = "showResultMessage()";
		logger.logInfo(METHOD_NAME + " started...");
		try{		
			if(getHdnResultPopupCalled().equals("Y")){
				hdnResultPopupCalled = "N";
				messages = new ArrayList<String>();
				messages.add("Result is saved");				
			}
			else{
				messages = new ArrayList<String>();
			}
			logger.logInfo(METHOD_NAME + " completed successfully...");
		}catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}		
	}
	//-----------------PAGE LIFE CYCLE METHODS---------------------------
	/**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Override this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void init() {
		// TODO Auto-generated method stub		
		super.init();		
		if(!isPostBack()){
			setRequestInfoInSession();
		}
			loadAuctionInfo();
			loadDataListBidder();
			loadUnitsToExclude();
//			showResultMessage();
			//loadAuctionRequestsList();
		
	}

	/**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void prerender() {
		super.prerender();
		showResultMessage();
	}
	
    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called, regardless of whether
     * or not this was the page that was actually rendered.  Override this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
    public void destroy() {
        super.destroy();
    }

	
	//Getters and Setters
	public HtmlDataTable getDataTableBidder() {
		return dataTableBidder;
	}

	public void setDataTableBidder(HtmlDataTable dataTableBidder) {
		this.dataTableBidder = dataTableBidder;
	}

	public BidderAttendanceView getDataItemBidder() {
		return dataItemBidder;
	}

	public void setDataItemBidder(BidderAttendanceView dataItemBidder) {
		this.dataItemBidder = dataItemBidder;
	}

	public List<BidderAttendanceView> getDataListBidder() {
		return dataListBidder;
	}

	public void setDataListBidder(List<BidderAttendanceView> dataListBidder) {
		this.dataListBidder = dataListBidder;
	}

	public HtmlDataTable getDataTableUnitsToExclude() {
		return dataTableUnitsToExclude;
	}

	public void setDataTableUnitsToExclude(HtmlDataTable dataTableUnitsToExclude) {
		this.dataTableUnitsToExclude = dataTableUnitsToExclude;
	}

	public AuctionRequestRegView getDataItemUnitsToExclude() {
		return dataItemUnitsToExclude;
	}

	public void setDataItemUnitsToExclude(AuctionRequestRegView dataItemUnitsToExclude) {
		this.dataItemUnitsToExclude = dataItemUnitsToExclude;
	}

	public List<AuctionRequestRegView> getDataListUnitsToExclude() {
		return dataListUnitsToExclude;
	}

	public void setDataListUnitsToExclude(
			List<AuctionRequestRegView> dataListUnitsToExclude) {
		this.dataListUnitsToExclude = dataListUnitsToExclude;
	}

	public String getHiddenAttendanceIds() {
		return hiddenAttendanceIds;
	}

	public void setHiddenAttendanceIds(String hiddenAttendanceIds) {
		this.hiddenAttendanceIds = hiddenAttendanceIds;
	}

	public String getHiddenUnitsToExclude() {
		return hiddenUnitsToExclude;
	}

	public void setHiddenUnitsToExclude(String hiddenUnitsToExclude) {
		this.hiddenUnitsToExclude = hiddenUnitsToExclude;
	}

	public String getAuctionId() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		 //Object obj = sessionMap.get(WebConstants.ConductAuction.CONDUCT_AUCTION_ID);
		 Object obj = sessionMap.get(WebConstants.AUCTION_ID);
		 if(obj != null){
			 auctionId = obj.toString().trim();
		 }
		return auctionId;
	}

	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}

	public String getAuctionNo() {
		return auctionNo;
	}

	public void setAuctionNo(String auctionNo) {
		this.auctionNo = auctionNo;
	}

	public String getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}

	public String getAuctionTitle() {
		return auctionTitle;
	}

	public void setAuctionTitle(String auctionTitle) {
		this.auctionTitle = auctionTitle;
	}

	public String getAuctionVenueName() {
		return auctionVenueName;
	}

	public void setAuctionVenueName(String auctionVenueName) {
		this.auctionVenueName = auctionVenueName;
	}

	public HtmlDataTable getDataTableAuctionUnitRequests() {
		return dataTableAuctionUnitRequests;
	}

	public void setDataTableAuctionUnitRequests(
			HtmlDataTable dataTableAuctionUnitRequests) {
		this.dataTableAuctionUnitRequests = dataTableAuctionUnitRequests;
	}

	public List<AuctionWinnersView> getDataListAuctionUnitRequests() {
		loadAuctionRequestsList();
		return dataListAuctionUnitRequests;
	}

	public void setDataListAuctionUnitRequests(
			List<AuctionWinnersView> dataListAuctionUnitRequests) {
		this.dataListAuctionUnitRequests = dataListAuctionUnitRequests;
	}

	public AuctionWinnersView getDataItemAuctionUnitRequests() {
		return dataItemAuctionUnitRequests;
	}

	public void setDataItemAuctionUnitRequests(
			AuctionWinnersView dataItemAuctionUnitRequests) {
		this.dataItemAuctionUnitRequests = dataItemAuctionUnitRequests;
	}
	
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public String getMessages() {
		String messageList;
		if ((messages == null) ||
			(messages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<B><UL>\n";
			for(String message: messages) {
				messageList = messageList + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}

	public String getHdnResultPopupCalled() {
		if(hdnResultPopupCalled == null)
			hdnResultPopupCalled = "N";
		return hdnResultPopupCalled;
	}

	public void setHdnResultPopupCalled(String hdnResultPopupCalled) {
		this.hdnResultPopupCalled = hdnResultPopupCalled;
	}
	 
}
