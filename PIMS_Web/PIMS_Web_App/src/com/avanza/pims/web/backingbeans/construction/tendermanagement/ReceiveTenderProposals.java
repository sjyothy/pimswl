package com.avanza.pims.web.backingbeans.construction.tendermanagement; 

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.TenderProposalView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class ReceiveTenderProposals extends AbstractController {

	private transient Logger logger = Logger.getLogger(ReceiveTenderProposals.class);
	private String bankId;
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private Date guaranteeExpiryDate;
	private Double guaranteePercentage;
	//Tender Details Tab Start
	private HtmlInputTextarea proposalDescription = new HtmlInputTextarea();
	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	//Tender Details Tab End
	
	String hiddenContractorId = "";
	String hiddenTenderId = "";
	String hiddenLoadTender = "";
	String hiddenLoadContractor = "";
	
	private final String TAB_TENDER_DETAILS = "tenderDetailsTab";
	
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private final String PAGE_MODE ="PAGE_MODE";
	private String pageMode="default";
	
	private final String notesOwner = WebConstants.ReceiveTenderProposals.RECEIVE_TENDER_PROPOSALS;
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();

	private transient ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_SERVICE_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	
	public void init(){
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				setMode();
				setValues();
			}
			
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			///
//			sessionMap.put(WebConstants.Tender.TENDER_ID, 1L);
//	   		sessionMap.put(WebConstants.Tender.TENDER_MODE, WebConstants.Tender.TENDER_MODE_RECEIVE_PROPOSAL);
//	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TENDER_SEARCH);
			///

			// from tender search screen
			if(sessionMap.get(WebConstants.Tender.TENDER_MODE)!=null)
			{
				String receiveProposalMode = (String) sessionMap.get(WebConstants.Tender.TENDER_MODE);
				sessionMap.remove(WebConstants.Tender.TENDER_MODE);
				Long tenderId = (Long)sessionMap.get(WebConstants.Tender.TENDER_ID);
				CommonUtil.loadAttachmentsAndComments(tenderId.toString());
				sessionMap.remove(WebConstants.Tender.TENDER_ID);
				if(tenderId!=null){
					viewMap.put(WebConstants.Tender.TENDER_ID, tenderId+"");
					hiddenTenderId = tenderId.toString();
					viewMap.put("defaultTender", true);
				}
				
				canAddAttachmentsAndComments(true);
				viewMap.put("readonlyMode", true);
				viewMap.put("asterisk", "");
				
				if(receiveProposalMode.equals(WebConstants.Tender.TENDER_MODE_RECEIVE_PROPOSAL)){
					pageMode = PAGE_MODE_NEW;
					viewMap.put("readonlyMode", false);
					viewMap.put("asterisk", "*");
				}
				else
					pageMode = PAGE_MODE_VIEW;
				
				viewMap.put(PAGE_MODE, pageMode);
			}

			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			if(pageMode.equals(PAGE_MODE_NEW))
				setSubmittedDate(new Date());
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	public void preprocess(){
		super.preprocess();
	}
	public void prerender(){
		super.prerender();
		applyMode(); 
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			pageMode = pageMode==null?PAGE_MODE_VIEW:pageMode;
			
			logger.logInfo("pageMode : %s", pageMode);
			
			loadContractor();
			loadTender();
			manageRendering();
				
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	private void manageRendering(){
		Boolean readonly = getReadonlyMode();
		saveButton.setRendered(!readonly);
		proposalDescription.setReadonly(readonly);
	}
	
	@SuppressWarnings("unchecked")
	private void loadContractor(){
		try{
			logger.logInfo("loadContractor started...");
			
			Boolean contractorLoaded = (Boolean) viewMap.get("contractorLoaded");
			contractorLoaded = (contractorLoaded==null)?false:contractorLoaded;
			if(hiddenContractorId!=null && !hiddenContractorId.equals(""))
			{
			 ContractorView contractorView = constructionServiceAgent.getContractorByIdForExtendRequest(Long.valueOf(hiddenContractorId));
			 Boolean loadContractor = hiddenLoadContractor.equals("true");
				if(StringHelper.isNotEmpty(hiddenContractorId) && (!contractorLoaded || loadContractor))
				{
					String contractorId = contractorView.getPersonId().toString();
					viewMap.put("contractorId", contractorId);
					String contractorName = CommonUtil.getIsEnglishLocale()?contractorView.getContractorNameEn():contractorView.getContractorNameAr();
					viewMap.put("contractorName", contractorName);
					String contractorNumber = contractorView.getContractorNumber();
					viewMap.put("contractorNumber", contractorNumber);
					
					viewMap.put("contractorLoaded", true);
					hiddenLoadContractor = "false";
				}
			}
			logger.logInfo("loadContractor completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadContractor crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadTender(){
		try{
			logger.logInfo("loadTender started...");
			Boolean tenderLoaded = (Boolean) viewMap.get("tenderLoaded");
			tenderLoaded = (tenderLoaded==null)?false:tenderLoaded;
			Boolean loadTender = hiddenLoadTender.equals("true");
			if(StringHelper.isNotEmpty(hiddenTenderId) && (!tenderLoaded || loadTender)){
				TenderView tenderView = constructionServiceAgent.getTenderById(Long.parseLong(hiddenTenderId));
				String tenderId = tenderView.getTenderId().toString();
				viewMap.put("tenderId", tenderId);
				String tenderNumber = tenderView.getTenderNumber();
				viewMap.put("tenderNumber", tenderNumber);
				String tenderDescription = tenderView.getRemarks();
				if(tenderDescription!=null)
					viewMap.put("tenderDescription", tenderDescription);
				
				viewMap.put("tenderLoaded", true);
				hiddenLoadTender = "false";
			}
			logger.logInfo("loadTender completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadTender crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	
	public String getContractorName() {
		String contractorName = (String)viewMap.get("contractorName");
		return contractorName;
	}
	public void setContractorName(String contractorName) {
		if(contractorName!=null)
			viewMap.put("contractorName" , contractorName);
	}
	
	public String getContractorNumber() {
		String contractorNumber = (String)viewMap.get("contractorNumber");
		return contractorNumber;
	}
	public void setContractorNumber(String contractorNumber) {
		if(contractorNumber!=null)
			viewMap.put("contractorNumber" , contractorNumber);
	}
	
	public HtmlInputTextarea getProposalDescription() {
		return proposalDescription;
	}
	public void setProposalDescription(HtmlInputTextarea proposalDescription) {
		this.proposalDescription = proposalDescription;
	}
	
	public String getReceivedBy() {
		String receivedBy = (String)viewMap.get("receivedBy");
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		if(receivedBy!=null)
			viewMap.put("receivedBy" , receivedBy);
	}
	
	public String getSubmittedBy() {
		String submittedBy = (String)viewMap.get("submittedBy");
		return submittedBy;
	}
	public void setSubmittedBy(String submittedBy) {
		if(submittedBy!=null)
			viewMap.put("submittedBy" , submittedBy);
	}
	public Date getSubmittedDate() {
		Date submittedDate = (Date)viewMap.get("submittedDate");
		return submittedDate;
	}
	public void setSubmittedDate(Date submittedDate) {
		if(submittedDate!=null)
			viewMap.put("submittedDate" , submittedDate);
	}
	public String getTenderDescription() {
		String tenderDescription = (String)viewMap.get("tenderDescription");
		return tenderDescription;
	}
	public void setTenderDescription(String tenderDescription) {
		if(tenderDescription!=null)
			viewMap.put("tenderDescription" , tenderDescription);
	}
	
	public String getTenderNumber() {
		String tenderNumber = (String)viewMap.get("tenderNumber");
		return tenderNumber;
	}
	public void setTenderNumber(String tenderNumber) {
		if(tenderNumber!=null)
			viewMap.put("tenderNumber" , tenderNumber);
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	
	
	public String getLocale(){

		return new CommonUtil().getLocale();
	}

	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}

	
	
	
	@SuppressWarnings("unchecked")
	public void openContractorPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openContractorPopup() started...");
		try
		{
			if(StringHelper.isNotEmpty(hiddenTenderId)){
		        FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:showContractorPopup();";
		        sessionMap.put(WebConstants.ReceiveTenderProposals.TENDER_ID_FOR_CONTRACTOR_SEARCH, hiddenTenderId);
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			else
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_TENDER));
			
	        logger.logInfo("openContractorPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openContractorPopup() crashed ", exception);
		}
    }

	
	@SuppressWarnings("unchecked")
	public void viewContractorPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("viewContractorPopup() started...");
		try
		{
			if(StringHelper.isNotEmpty(hiddenContractorId)){
		        
				HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
				ContractorView contractorView = constructionServiceAgent.getContractorByIdForExtendRequest(Long.valueOf(hiddenContractorId));
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:viewContractorPopup();";
		        sessionMap.put(WebConstants.CONTRACTOR_DETAILS, contractorView);
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			else
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_CONTRACTOR));
			
	        logger.logInfo("viewContractorPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("viewContractorPopup() crashed ", exception);
		}
    }
	
	
	
	
	
	public String openTenderPopUp(){
		
		if(StringHelper.isNotEmpty(hiddenTenderId))
		{
	   		sessionMap.put(WebConstants.Tender.TENDER_ID, Long.parseLong(hiddenTenderId.toString()));
	   		sessionMap.put(WebConstants.Tender.TENDER_MODE, WebConstants.Tender.TENDER_MODE_VIEW);
	   		sessionMap.put(WebConstants.Tender.TENDER_MODE_POPUP, true);
	   		openPopup("openTenderPopUp();");
		}
		
		return "Success";
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Boolean validated() {
	    logger.logInfo("validated() started...");
	    Boolean validated = true;
	    try
		{
//	    	viewMap.put("tenderId", "1");
//	    	viewMap.put("contractorId", "15");
	    	
	    	String tenderId = (String)viewMap.get("tenderId");
	    	String contractorId = (String)viewMap.get("contractorId");
	    	
	    	logger.logInfo("tenderId : %s | contractorId : %s | submittedBy : %s | receivedBy : %s | proposalDescription : %s", tenderId, contractorId, getSubmittedBy(), getReceivedBy(), proposalDescription.getValue());
	    	
	    	if(StringHelper.isEmpty(tenderId))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_TENDER));
	        	tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
				return validated;
	        }

	    	if(StringHelper.isEmpty(contractorId))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ExtendApplication.MSG_SELECT_CONTRACTOR));
	        	tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
				return validated;
	        }
	    	
	    	if(StringHelper.isEmpty(getSubmittedBy())){
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ReceiveTenderProposals.SUBMITTED_BY));
	        	tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
				return validated;
	    	}
	    	
	    	if(StringHelper.isEmpty(getReceivedBy()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ReceiveTenderProposals.RECEIVED_BY));
	        	tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
				return validated;
	        }
	    	
	    	if(StringHelper.isEmpty((String)proposalDescription.getValue()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ReceiveTenderProposals.PROPOSAL_DESCRIPTION));
	        	tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
				return validated;
	        }
	    	if(this.getBankId()==null || this.getBankId().compareTo("-1")==0)
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(CommonUtil.getBundleMessage("serviceContract.guarantee.Bank"));
				validated = false;
				return validated;
	    	}	
	    	if(this.getGuaranteePercentage()!=null && this.getGuaranteePercentage().isNaN() )
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(CommonUtil.getBundleMessage("serviceContract.message.invalidBankGuaranteePercentage"));
				validated = false;
				return validated;
	    	}
	    	
	    	if(this.getGuaranteePercentage()!=null && this.getGuaranteePercentage().compareTo(0D)<=0 )
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(CommonUtil.getBundleMessage("serviceContract.guarantee.perentageNonNegative"));
				validated = false;
				return validated;
	    	}
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;
				return validated;
	    	}
	
			logger.logInfo("validated() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validated() crashed ", exception);
		}
		return validated;
	}
	
	@SuppressWarnings("unchecked")
	public String onSave() {
		logger.logInfo("onSave() started...");
		String success = "false";
		Boolean crashed = false;
		String tenderProposalId = "";
		try {
			if(validated()){
				TenderProposalView receiveProposalView = new TenderProposalView();
					 
				 String contractorId = (String)viewMap.get("contractorId");
				 receiveProposalView.setContractorId(contractorId);
				 
				 String tenderId = (String)viewMap.get("tenderId");
				 receiveProposalView.setTenderId(tenderId);
				 receiveProposalView.setBankView(getBankView());
				 receiveProposalView.setBankGuaranteePercentage(this.getGuaranteePercentage());
				 receiveProposalView.setGuaranteeExpiryDate(this.getGuaranteeExpiryDate());
				 receiveProposalView.setProposalDateVal(getSubmittedDate());
				 receiveProposalView.setSenderName(getSubmittedBy());
				 receiveProposalView.setReceiverName(getReceivedBy());
				 receiveProposalView.setDescription(proposalDescription.getValue().toString());
			 if(constructionServiceAgent.checkIsContractorAlreadyPresent(Long.parseLong(tenderId),Long.parseLong(contractorId)))
		      {	 
				 receiveProposalView = constructionServiceAgent.saveReceivedTenderProposal(receiveProposalView, getArgMap());
				 if(receiveProposalView.getTenderProposalId()!=null){
					tenderProposalId = receiveProposalView.getTenderProposalId();
					viewMap.put(WebConstants.ReceiveTenderProposals.TENDER_PROPOSAL_ID, tenderProposalId);
					
					if(tenderId !=null)
					{
					CommonUtil.loadAttachmentsAndComments(tenderId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(Long.parseLong(tenderId));
					Boolean commentSuccess = CommonUtil.saveComments(Long.parseLong(tenderId), noteOwner);
					
					
					}
					
					
					success = "true";
					successMessages.clear();
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveTenderProposals.MSG_PROPOSAL_SAVE_SUCCESS));
					
					viewMap.put(PAGE_MODE, PAGE_MODE_VIEW);
					canAddAttachmentsAndComments(false);
					viewMap.put("readonlyMode", true);
					viewMap.put("asterisk", "");
					tabPanel.setSelectedTab(TAB_TENDER_DETAILS);
			   }
		      }else
		      {
			    errorMessages.clear();
			    errorMessages.add(ResourceUtil.getInstance().getProperty("constructionTender.contractorAlreadySelected"));
			 }	 
			}
			else
				success = "validation";
			logger.logInfo("onSave() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("onSave() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("onSave() crashed ", exception);
		}
		finally{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveTenderProposals.MSG_PROPOSAL_SAVE_FAILURE));
		}
		return success;
	}
	
	private BankView getBankView()
	{
		BankView bankView=new BankView();
		bankView.setCreatedBy(CommonUtil.getLoggedInUser());
		bankView.setCreatedOn(new Date());
		bankView.setUpdatedBy(CommonUtil.getLoggedInUser());
		bankView.setUpdatedOn(new Date());
		bankView.setBankId(Long.parseLong(this.getBankId()));
		return bankView;
	}

	public String onCancel() {
		logger.logInfo("onCancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen == null)
					backScreen = "home";
		        logger.logInfo("onCancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("onCancel() crashed ", exception);
		}
		return backScreen;
    }
	
	
	public Boolean getReadonlyMode(){
		Boolean readonlyMode = (Boolean)viewMap.get("readonlyMode");
		return readonlyMode==null?false:readonlyMode;
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}

	public String getHiddenContractorId() {
		return hiddenContractorId;
	}

	public void setHiddenContractorId(String hiddenContractorId) {
		this.hiddenContractorId = hiddenContractorId;
	}

	public String getHiddenTenderId() {
		return hiddenTenderId;
	}

	public void setHiddenTenderId(String hiddenTenderId) {
		this.hiddenTenderId = hiddenTenderId;
	}
	
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		return asterisk==null?"":asterisk;
	}

	public String getHiddenLoadTender() {
		return hiddenLoadTender;
	}

	public void setHiddenLoadTender(String hiddenLoadTender) {
		this.hiddenLoadTender = hiddenLoadTender;
	}

	public String getHiddenLoadContractor() {
		return hiddenLoadContractor;
	}

	public void setHiddenLoadContractor(String hiddenLoadContractor) {
		this.hiddenLoadContractor = hiddenLoadContractor;
	}

	public Boolean getShowSelectTenderLink() {
		Boolean defaultTender = (Boolean) viewMap.get("defaultTender");
		return defaultTender==null?true:false;
	}

	public String getBankId() 
	{
		if(viewMap.containsKey("BANK_ID"))
			bankId=viewMap.get("BANK_ID").toString();
		return bankId;
	}

	public void setBankId(String bankId) 
	{
		if(bankId!=null)
			viewMap.put("BANK_ID", bankId);
		this.bankId = bankId;
	}

	public Date getGuaranteeExpiryDate() 
	{
		if(viewMap.containsKey("GUARANTEE_EXPIRY_DATE"))
			guaranteeExpiryDate=(Date)viewMap.get("GUARANTEE_EXPIRY_DATE");
		return guaranteeExpiryDate;
	}

	public void setGuaranteeExpiryDate(Date guaranteeExpiryDate) {
		this.guaranteeExpiryDate = guaranteeExpiryDate;
	}

	public Double getGuaranteePercentage()
	{
		if(viewMap.containsKey("GUARANTEE_PERCENTAGE"))
			guaranteePercentage=(Double)viewMap.get("GUARANTEE_PERCENTAGE");
		return guaranteePercentage;
	}

	public void setGuaranteePercentage(Double guaranteePercentage)
	{
		if(guaranteePercentage!=null)
			viewMap.put("GUARANTEE_PERCENTAGE",guaranteePercentage);
		this.guaranteePercentage = guaranteePercentage;
	}

}
