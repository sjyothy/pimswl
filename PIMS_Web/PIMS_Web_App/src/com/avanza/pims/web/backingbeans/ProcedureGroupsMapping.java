package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ProcedureTask;
import com.avanza.pims.entity.ProcedureTaskGroup;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class ProcedureGroupsMapping extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger
			.getLogger(ProcedureGroupsMapping.class);

	public static class Messages {
		public static String MAPPING_SAVE_SUCCESS = "procedureGroupsMapping.saveSuccessful";
		public static String MAPPING_SAVE_ERROR = "procedureGroupsMapping.saveError";
	}

	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private Integer paginatorMaxPages = 0;
	private Integer recordSize = 0;

	private List<String> errorMessages;
	private List<String> infoMessages;

	private HtmlCommandButton btnAddGroup;
	private HtmlDataTable groupsDataTable;

	private List<SelectItem> selectUserGroups = new ArrayList<SelectItem>();
	private String selectOneUserGroup;

	// these are attributes restored from session as passing arguments to this
	// pop-up window
	private List<UserGroup> procedureUserGroups;
	private ProcedureTask procedureTask;

	public void init() {
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		if (!isPostBack())
			getUserGroups();
	}

	// gets all the user groups from Security manager and populates those not
	// mapped with procedure task in combo-box and the remaining are listed in
	// the grid
	@SuppressWarnings("unchecked")
	private void getUserGroups() {
		logger.logInfo("getUserGroups() started...");

		try {
			if (!sessionMap.containsKey("procedureTaskGroups"))
				return;

			procedureUserGroups = (List<UserGroup>) sessionMap
					.get("procedureTaskGroups");
			procedureTask = (ProcedureTask) sessionMap.get("procedureTask");

			if (procedureUserGroups != null)
				viewMap.put("procedureTaskGroups", procedureUserGroups);

			if (procedureTask != null)
				viewMap.put("procedureTask", procedureTask);

			sessionMap.remove("procedureTaskGroups");
			sessionMap.remove("procedureTask");

			for (UserGroup userGroup : getSecUserGroups()) {
				boolean exists = false;

				for (UserGroup existingGroup : procedureUserGroups)
					if (existingGroup!=null && existingGroup.getUserGroupId().equalsIgnoreCase(userGroup.getUserGroupId())) 
					{
						exists = true;
						break;
					}

				if (!exists)
					selectUserGroups.add(new SelectItem(userGroup
							.getUserGroupId(), userGroup.getPrimaryName()));
			}

			viewMap.put("selectUserGroups", selectUserGroups);

			recordSize = procedureUserGroups.size();
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getUserGroups() crashed ", e);
		}
	}

	// adds a group mapping to the procedure task
	public void addGroup() {
		logger.logInfo("addGroup() started...");

		try {
			if (selectOneUserGroup == null
					|| selectOneUserGroup.trim().equals(""))
				return;

			procedureUserGroups.add(SecurityManager
					.getGroup(selectOneUserGroup));

			for (SelectItem selectItem_UserGroup : selectUserGroups)
				if (selectItem_UserGroup.getValue().equals(selectOneUserGroup)) {
					selectUserGroups.remove(selectItem_UserGroup);
					break;
				}

			if (selectUserGroups.size() == 0)
				btnAddGroup.setDisabled(true);

			recordSize = procedureUserGroups.size();
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("addGroup() crashed ", e);
		}
	}

	// removes a group mapping from the procedure task
	public void deleteGroup() {
		logger.logInfo("deleteGroup() started...");

		try {
			UserGroup selectedUserGroup = (UserGroup) groupsDataTable
					.getRowData();

			boolean inSelectList = false;

			for (SelectItem selectItem_UserGroup : selectUserGroups)
				if (selectItem_UserGroup.getValue().equals(
						selectedUserGroup.getUserGroupId())) {
					inSelectList = true;
					break;
				}

			if (!inSelectList)
				selectUserGroups.add(new SelectItem(selectedUserGroup
						.getUserGroupId(), selectedUserGroup.getPrimaryName()));

			procedureUserGroups.remove(selectedUserGroup);

			if (btnAddGroup.isDisabled())
				btnAddGroup.setDisabled(false);

			recordSize = procedureUserGroups.size();
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("deleteGroup() crashed ", e);
		}
	}

	// Save the mappings done by the user in database
	public void saveGroups() {
		logger.logInfo("saveGroups() started...");

		try {
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			List<ProcedureTaskGroup> procedureTaskGroups = utilityServiceAgent
					.getAllProcedureTaskGroups(getProcedureTask()
							.getProcedureTypeId(), getProcedureTask()
							.getProcedureTaskId());

			// mark all the groups related to the procedure task as 'Deleted'
			for (ProcedureTaskGroup procedureTaskGroup : procedureTaskGroups)
				procedureTaskGroup.setIsDeleted(1l);

			// iterate mapped groups by the user, and if found in the DB list,
			// re-mark them as Undeleted. Also those not found in list (i.e.
			// mapped for the first time ever), add a new mapping in database
			for (UserGroup mappedGroup : getProcedureUserGroups()) {
				boolean isAlreadyInDatabase = false;

				for (ProcedureTaskGroup procedureTaskGroup : procedureTaskGroups) {
					if (mappedGroup.getUserGroupId().equals(
							procedureTaskGroup.getUserGroupId())) {
						procedureTaskGroup.setIsDeleted(0l);
						isAlreadyInDatabase = true;
						EntityManager.getBroker().update(procedureTaskGroup);
						break;
					}
				}

				if (!isAlreadyInDatabase) {
					ProcedureTaskGroup newGroup = new ProcedureTaskGroup();
					newGroup.setCreatedBy("admin");
					newGroup.setCreatedOn(new Date());
					newGroup.setIsDeleted(0l);
					newGroup.setProcedureTask(procedureTask);
					newGroup.setRecordStatus(1l);
					newGroup.setUpdatedBy("admin");
					newGroup.setUpdatedOn(new Date());
					newGroup.setUserGroupId(mappedGroup.getUserGroupId());

					EntityManager.getBroker().add(newGroup);
				}
			}

			infoMessages.clear();
			infoMessages.add(ResourceUtil.getInstance().getProperty(
					Messages.MAPPING_SAVE_SUCCESS));

			closeWindowAndSubmit();
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					Messages.MAPPING_SAVE_ERROR));
			logger.LogException("saveGroups() crashed ", e);
		}
	}

	@SuppressWarnings("unchecked")
	private void closeWindowAndSubmit() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		sessionMap.put("realoadProcedureGroupsMapping", true);
		sessionMap.put("procedureTask", getProcedureTask());

		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:closeWindowSubmit();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	private List<UserGroup> getSecUserGroups() {
		logger.logInfo("getSecUserGroups() started...");

		List<UserGroup> userGroups = null;
		try {
			Search query = new Search();
			query.clear();
			query.addFrom(UserGroupDbImpl.class);
			userGroups = SecurityManager.findGroup(query);
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getSecUserGroups() crashed ", e);
		}

		return userGroups;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public Integer getRecordSize() {
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		return WebConstants.POPUP_RECORDS_PER_PAGE;
	}

	public String getSelectOneUserGroup() {
		return selectOneUserGroup;
	}

	public void setSelectOneUserGroup(String selectOneUserGroup) {
		this.selectOneUserGroup = selectOneUserGroup;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getSelectUserGroups() {
		if (viewMap.get("selectUserGroups") != null)
			selectUserGroups = (List<SelectItem>) viewMap
					.get("selectUserGroups");

		return selectUserGroups;
	}

	public void setSelectUserGroups(List<SelectItem> selectUserGroups) {
		this.selectUserGroups = selectUserGroups;
	}

	public HtmlCommandButton getBtnAddGroup() {
		return btnAddGroup;
	}

	public void setBtnAddGroup(HtmlCommandButton btnAddGroup) {
		this.btnAddGroup = btnAddGroup;
	}

	@SuppressWarnings("unchecked")
	public List<UserGroup> getProcedureUserGroups() {
		if (viewMap.get("procedureTaskGroups") != null)
			procedureUserGroups = (List<UserGroup>) viewMap
					.get("procedureTaskGroups");

		return procedureUserGroups;
	}

	public void setProcedureUserGroups(List<UserGroup> procedureUserGroups) {
		this.procedureUserGroups = procedureUserGroups;
	}

	public HtmlDataTable getGroupsDataTable() {
		return groupsDataTable;
	}

	public void setGroupsDataTable(HtmlDataTable groupsDataTable) {
		this.groupsDataTable = groupsDataTable;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public ProcedureTask getProcedureTask() {
		if (viewMap.get("procedureTask") != null)
			procedureTask = (ProcedureTask) viewMap.get("procedureTask");

		return procedureTask;
	}

	public void setProcedureTask(ProcedureTask procedureTask) {
		this.procedureTask = procedureTask;
	}

}
