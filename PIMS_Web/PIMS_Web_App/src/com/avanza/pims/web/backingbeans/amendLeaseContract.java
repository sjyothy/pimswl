package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import java.util.ResourceBundle;

import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import oracle.bpel.designer.workflow.display.jsp.generator.For;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.tabbedpane.HtmlPanelTab;
import org.apache.myfaces.custom.tabbedpane.HtmlPanelTabbedPane;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;


import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.bpel.pimscontractinitiatebpel.proxy.ContractInitiateBPELPortClient;
import com.avanza.pims.bpel.pimsamendleasecontract.proxy.PIMSAmendLeaseContractBPELPortClient;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.RequestDetail;
import com.avanza.pims.entity.RequestType;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PrintLeaseContractReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;

import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.CommercialActivityView;
import com.avanza.pims.ws.vo.ContractActivityView;
import com.avanza.pims.ws.vo.ContractPartnerView;
import com.avanza.pims.ws.vo.ContractPersonView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;

import com.avanza.pims.ws.vo.PaidFacilitiesView;

import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;


public class amendLeaseContract  extends AbstractController
{

    String moduleName="amendLeaseContract";
    private String AUCTION_UNIT_ID= "auctionUnitId";
    private String AUCTION_DEPOSIT_AMOUNT   = "auctionDepositAmont";
    private String AUCTION_UNIT_BIDDER_LIST = "auctionUnitBidderList";
    private String PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID = "paymentScheduleOwnerShipTypeId";
    private String PREVIOUS_UNIT_ID = "previousUnitId";
    private ResourceBundle bundle;
    private boolean isPageModeAmendAdd;
    private boolean isPageModeUpdate;
    private String DDV_COMMERCIAL_CONTRACT="DDV_COMMERCIAL_CONTRACT";
    private String PAGE_MODE_ADD="ADD";
    private String PAGE_MODE_UPDATE="UPDATE";
    private String PAGE_MODE_APPROVE_REJECT="APPROVE_REJECT";
    private String  REQUEST_FOR_AMEND = "REQUEST_FOR_AMEND";
    private String TENANT_INFO="TENANTINFO";
    private String UNIT_DATA_ITEM="UNITDATAITEM";
    //private String PERSON_SUB_TYPE="PERSON_SUB_TYPE";
    private HtmlInputText txtRefNumber=new HtmlInputText();
    private HtmlOutputLabel lblRefNum=new HtmlOutputLabel();
    private HtmlCommandButton btnOccuipers=new HtmlCommandButton();
    private HtmlCommandButton btnPartners=new HtmlCommandButton();
    private HtmlCommandButton btnSaveNewLease=new HtmlCommandButton();
    private HtmlCommandButton btnSaveAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnCompleteAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnSubmitAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnApprove=new HtmlCommandButton();
    private HtmlCommandButton btnReject=new HtmlCommandButton();
    private HtmlCommandButton btnApproveAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnRejectAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnGenPayments=new HtmlCommandButton();
    private HtmlCommandButton btnSendNewLeaseForApproval = new HtmlCommandButton();
    private HtmlCommandButton btnCollectPayment = new HtmlCommandButton();
    private HtmlCommandButton btnAddUnit = new HtmlCommandButton();
    private HtmlCommandButton btnAddAuctionUnit = new HtmlCommandButton();
    private HtmlCommandButton btnAddPayments = new HtmlCommandButton();
    HtmlSelectOneMenu cmbContractType = new HtmlSelectOneMenu(); 
    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
    org.richfaces.component.html.HtmlTab tabCommercialActivity=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabPartner=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabOccupier=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabContractHistory=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabRequestHistory=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabFacilities=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlCalendar issueDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    org.richfaces.component.html.HtmlCalendar startDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    org.richfaces.component.html.HtmlCalendar endDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    HtmlInputText txt_ActualRent = new HtmlInputText();
    private HtmlGraphicImage  btnPartnerDelete = new HtmlGraphicImage();
    private HtmlGraphicImage  btnOccupierDelete = new HtmlGraphicImage();

    private HtmlGraphicImage imgSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgTenant=new HtmlGraphicImage();
    private HtmlGraphicImage imgManager=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewTenant=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewManager=new HtmlGraphicImage();
    private HtmlGraphicImage imgDeleteSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgDeleteManager=new HtmlGraphicImage();
    
    private HtmlTabPanel tabPanel = new HtmlTabPanel();
    private HtmlTab tabApplicationDetails = new HtmlTab();
    private String pageMode; 
    private String refNum;
    private String datetimerequest;
    
    private String selectRequestStatus;
    private String tenantRefNum;
    private String unitRefNum;
    private String unitStatusId;
    private String unitRentValue;
    private String txtUnitStatus;
    private String auctionRefNum;
    private String hdnAuctionunitId;
    private String hdnUnitId;
    private String hdnUnitUsuageTypeId;
    private String txtUnitUsuageType;
    private String unitRentAmount;
    private String hdnTenantId;
    private String hdntenanttypeId;
    private String txtTenantName; 
    private String txtSponsor;
    private String txtTenantType;
    private String hdnSponsorId;
    private String txtManager;
    private String hdnManagerId;
    private String personSponsor;
    private String personManager;
    private String personOccupier;
    private String personPartner;
    private String txtMaintainenceAmount;
    private Double maintainenceAmount;
    private Date contractStartDate;
    private Date contractEndDate;
    private Date contractIssueDate;
    private String contractId;
    private String contractCreatedOn;
    private String contractCreatedBy;
    private String propertyCommercialName;
    private String propertyAddress;
    private String contractStatus;
    private Double totalContractValue=new Double(0);
    public String  txttotalContractValue;
    public String hdnApplicantId;
    
    List<SelectItem> priorityList = new ArrayList();
	List<SelectItem> statusList = new ArrayList();
	List<SelectItem> contractTypeList = new ArrayList();
	private List<String> errorMessages;
	private List<String> successMessages=new ArrayList<String>();
    private HtmlInputText txtRefNum;  
     boolean isCompanyTenant;
     boolean isUnitCommercial;
     boolean isPageUpdateStatusNew;
     String pageTitle;
    FacesContext context = null;
    UIViewRoot view =null; 
    
    PersonView tenantsView=new PersonView();
    RequestDetailView requestDetailView=new RequestDetailView();
	UnitView unitsView=new UnitView();
    
	HtmlPanelTabbedPane panelTabbedPane =new HtmlPanelTabbedPane ();
	private HtmlPanelTab panelTab=new HtmlPanelTab();
	private String TAB_ID_APPLICATION ="appDetailsTab";
	private String TAB_ID_BASIC ="tabBasicInfo";
	private String TAB_ID_UNIT ="tabUnit";
	private String TAB_ID_OCCUPIERS ="tabPersonOccupier";
	private String TAB_ID_PARTNERS ="tabPersonPartnr";
	private String TAB_ID_COMMERCIAL_ACTIVITY ="tabCommercialActivity";
	private String TAB_ID_FACILITIES ="tabPaidFacilities";
	private String TAB_ID_PAYMENT_SCHEDULE ="tabPaymentSchedule";
	private String TAB_ID_ATTACHEMENT ="attachmentTab";
	
	
	Map sessionMap;
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private HtmlDataTable paidFacilitiesDataTable;
	
	private List<PaidFacilitiesView> paidFacilitiesDataList = new ArrayList<PaidFacilitiesView>();
	private PaidFacilitiesView paidFacilitiesDataItem = new PaidFacilitiesView();
	private boolean isContractStatusApproved;
	private HtmlDataTable commercialActivityDataTable;
	private List<CommercialActivityView> commercialActivityList = new ArrayList<CommercialActivityView>();
	private CommercialActivityView commercialActivityItem = new CommercialActivityView();
	
	
	private HtmlDataTable tenantsDataTable;
	private List<TenantView> tenantsDataList = new ArrayList<TenantView>();
	private TenantView tenantsDataItem = new TenantView();
	private HtmlDataTable unitDataTable;
	private List<UnitView> unitDataList = new ArrayList<UnitView>();
	private UnitView unitDataItem = new UnitView();
	
	private HtmlDataTable paymentScheduleDataTable;
	private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
	private PaymentScheduleView paymentScheduleDataItem = new PaymentScheduleView();
	private HtmlDataTable propspectiveOccupierDataTable;
	private List<PersonView> prospectiveOccupierDataList = new ArrayList<PersonView>();
	private PersonView prospectiveOccupierDataItem = new PersonView();
	private HtmlDataTable propspectivepartnerDataTable;
	private List<PersonView> prospectivepartnerDataList  = new ArrayList<PersonView>();
	private PersonView prospectivepartnerDataItem = new PersonView();
	boolean isArabicLocale;
	boolean isEnglishLocale;
	private String selectOneContractType;
	ContractView contractView =new ContractView();
	RequestView requestView=new RequestView();
	private static Logger logger = Logger.getLogger( amendLeaseContract.class );
	CommonUtil commonUtil=new CommonUtil();
	HttpServletRequest request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	String dateFormat="";
	 HashMap feesConditionsMap=new HashMap();
	 HashMap depositConditionsMap=new HashMap();
	 private double totalRentAmount;
		private double totalCashAmount;
		private double totalChqAmount;
		private int installments;
		private double totalDepositAmount;
		private double totalFees;
		private double totalFines;
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	
    /** default constructor */
    public  amendLeaseContract()
    {
    	String methodName="Constructor";
    	logger.logInfo(methodName+"|"+" Start");
    	context = FacesContext.getCurrentInstance();
    	view    = context.getViewRoot();
    	
    	// String passport =context.getExternalContext().getRequestMap().get("passportNumber").toString();
    	//   	Load all the combos present in the screen
    	
    	logger.logInfo(methodName+"|"+" Finish");
    }
   

    
    @Override
    @SuppressWarnings("unchecked")
    public void init() 
    {
    	
    	String methodName="init";
    	logger.logInfo(methodName+"|"+"Start");
    	sessionMap =context.getExternalContext().getSessionMap();
		try
    	{  
			

		       if(!isPostBack())
		       {
		    	   logger.logDebug(methodName+"|"+"Is not postback");
		    	   loadAttachmentsAndComments(null);
		    	   if(getRequestParam("CONTRACT_VIEW_ROOT")!=null )
		    	   {
		    		   viewRootMap=(Map)getRequestParam("CONTRACT_VIEW_ROOT");
		    		   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		    		   contractId=contractView.getContractId().toString();
		    		   pageMode=PAGE_MODE_UPDATE;
		    		   viewRootMap.put("pageMode", pageMode);
		    		   
		    	   }
		    	   
		    	   if(request.getParameter(WebConstants.VIEW_MODE)!=null)
		    		   viewRootMap.put(WebConstants.VIEW_MODE, request.getParameter(WebConstants.VIEW_MODE));
			    	
			    		 //Remove session 
			    		 logger.logInfo(methodName+"|"+"Remove Sessions..Start");
			    		 RemoveSessions();
			    		 logger.logInfo(methodName+"|"+"Remove Sessions..Finish");
				         //Load Types such as UnitUsuageType,TenantType,ContractType
				         loadTypes();
				         //Load Combos data
				         loadCombos();
				    
				         String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR);
				         viewRootMap.put(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR,daysInYear );
				         setContractDate();
				     	 
				    	 if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null)
				    	 {
				    		  pageMode=getRequestParam(WebConstants.LEASE_AMEND_MODE).toString();
				    	      viewRootMap.put("pageMode", pageMode);
				    	 }
				    	 else if(sessionMap.containsKey(WebConstants.LEASE_AMEND_MODE))
				    	 {
				    		 pageMode=sessionMap.get(WebConstants.LEASE_AMEND_MODE).toString();
				    	     viewRootMap.put("pageMode", pageMode);
				    	     sessionMap.remove(WebConstants.LEASE_AMEND_MODE);
				    		 
				    	 }
				    	 logger.logInfo(methodName+"|"+"Checking if request came from taskList..");
				    	
				    	 //if come from tasklist
				    	 if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				    		getDataFromTaskList();
				     	 
				     	 //if come from request search or
				     	 //from contract search for incomplete request of amend
				    	 else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
				    	 {
				    		 requestView=(RequestView)getFacesContext().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
						     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
						        contractId=requestView.getContractView().getContractId().toString();
						      pageMode=PAGE_MODE_UPDATE;
				    	      viewRootMap.put("pageMode", pageMode);
						      getAmendDataFromRequestView();
				    	 }
				     	 
				     	 //if come from contract search for new lease contract OR AMEND FOR ADD
					     else if(sessionMap.containsKey("contractId"))
				    	 {
				    		 contractId= sessionMap.get("contractId").toString(); 
				    		 sessionMap.remove("contractId");
				    		 				    			
				    	 }
				     	if(contractId!=null && !contractId.equals("") )
				     	{
				     		getContractById(contractId);
			    			putViewValuesInControl(contractId);
				     	}
				     	else
			    		{
			    			logger.logInfo(methodName+"|"+"Add Mode");
			    			//Add Mode
			    			pageMode=PAGE_MODE_ADD;
			    			viewRootMap.put("pageMode", PAGE_MODE_ADD);
			    		}
		       }
		       //Is Post Back 
		       else
		        { 
		    	   if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) && 
		    		  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null && 
		    		  (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
		    	    )
			 	   {
	    		       contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	    		       sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
	    		       CollectPayments();
					   
			 	   }
		    	   //IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
		    	   //is copied to viewRoot of this page
                   if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
                		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
                   )
                   {
                	   viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
                	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
                   }    	   
                	   getPaymentScheduleListFromSession();
		     }
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured:",ex);
    	}
       
       logger.logInfo(methodName+"|"+"Finish");
    }
    
    @SuppressWarnings("unchecked")
    private void getAmendDataFromRequestView()
    {
    	String methodName="getDataFromRequestView";
    	logger.logDebug(methodName+"|"+"Start");
		
		
	   if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null)
	   {
		viewRootMap.put(REQUEST_FOR_AMEND,requestView);
	    if(requestView.getRequestFieldDetailsView()!=null && requestView.getRequestFieldDetailsView().size()>0)
	    	viewRootMap.put("AMEND_RFDV", requestView.getRequestFieldDetailsView());
	   }
	   
	   
	   
	   populateApplicationDetailsTab(); 
	    List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView ddvComplete= CommonUtil.getIdFromType(ddvList ,WebConstants.REQUEST_STATUS_COMPLETE);
		DomainDataView ddvRejected= CommonUtil.getIdFromType(ddvList ,WebConstants.REQUEST_STATUS_REJECTED);
		DomainDataView ddvCancelled= CommonUtil.getIdFromType(ddvList ,WebConstants.REQUEST_STATUS_CANCELED);
		
						
		if(requestView.getStatusId().compareTo(ddvComplete.getDomainDataId())==0 || 
		   requestView.getStatusId().compareTo(ddvRejected.getDomainDataId())==0 ||
		   requestView.getStatusId().compareTo(ddvCancelled.getDomainDataId())==0 
		  )
			pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW;
		//Either amend add or amend update
		else if(getRequestParam(WebConstants.LEASE_AMEND_MODE) !=null && getRequestParam(WebConstants.LEASE_AMEND_MODE).toString().trim().length()>0)
	    	pageMode=getRequestParam(WebConstants.LEASE_AMEND_MODE).toString();
	        
		
		 viewRootMap.put("pageMode", pageMode);
		 
		logger.logDebug(methodName+"|"+"Finish");
    }
    @SuppressWarnings("unchecked")
    private void getDataFromTaskList()throws Exception
    {
    	   String methodName="getDataFromTaskList";
    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
    	   viewRootMap.put(WebConstants.CONTRACT_USER_TASK_LIST, userTask);

    	   if(userTask.getTaskAttributes().get("CONTRACT_ID")!=null)
		       contractId= userTask.getTaskAttributes().get("CONTRACT_ID").toString();

    	   getPageModeFromTaskType();
    }
    private void setContractDate()
    {
    	
    	     String methodName="setContractDate";
    	     DateFormat dF= new SimpleDateFormat(getDateFormat());
    	     this.setDatetimerequest(dF.format(new Date()));
    	     logger.logInfo(methodName+"|Contract Start Date"+dF.format(new Date()));
	         this.setContractStartDate(new Date());
	         Calendar cal=new GregorianCalendar();
	         
			 cal.set(cal.get(Calendar.YEAR)+1, cal.get(Calendar.MONTH),cal.get(Calendar.DATE));
			 logger.logInfo(methodName+"|Contract End Date"+dF.format(cal.getTime()));
		     this.setContractEndDate(cal.getTime());
    	
    }
    @SuppressWarnings("unchecked")
    private void getPageModeFromTaskType()throws PimsBusinessException
    {
    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.CONTRACT_USER_TASK_LIST);
    	 boolean isTaskForAmend=false;
    	 	Long reqId=null;
    	if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
    		reqId = new Long(userTask.getTaskAttributes().get("REQUEST_ID").toString());
    	if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
    	{
	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.AmendLeaseContract_TaskTypes.APPROVE_AMEND_LEASE_CONTRACT))
	    	{
		    	pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT;
		    	viewRootMap.put("pageMode", pageMode);
		    	setRequestParam(WebConstants.LEASE_AMEND_MODE,pageMode);
		    	isTaskForAmend=true;
	    	}
	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.AmendLeaseContract_TaskTypes.COMPLETE_AMEND_LEASE_CONTRACT))
	    	{
		    	pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL;
		    	viewRootMap.put("pageMode", pageMode);
		    	setRequestParam(WebConstants.LEASE_AMEND_MODE,pageMode);
		    	
		    	isTaskForAmend=true;
	    	}
	    	if(isTaskForAmend && reqId !=null)
	    	{
	    		PropertyServiceAgent psa = new PropertyServiceAgent();
	    		requestView= psa.getRequestById(reqId);
	    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
	    		 if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
			        contractId=requestView.getContractView().getContractId().toString();
			    	 getAmendDataFromRequestView();
	    	}
    	}
    	
    }
    @SuppressWarnings("unchecked")
    private String getTenantType(PersonView tenantView)
    {
    
       String method="getTenantType";
       logger.logDebug(method+"|"+"Start...");
       DomainDataView ddv;
       if(tenantView!=null && tenantView.getPersonId()!=null && tenantView.getIsCompany()!=null )
       {
       if( tenantView.getIsCompany().compareTo(new Long(1))==0)
       {
    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
       }
       else
       {
    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
       }
       if(getIsArabicLocale())
			return ddv.getDataDescAr();
	   else if(getIsEnglishLocale())
			return  ddv.getDataDescEn();
       }
            logger.logDebug(method+"|"+"Finish...");
    return "";
    
    
    
    }
    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     **/
    @Override
    @SuppressWarnings("unchecked")
    public void preprocess() 
    {
    	super.preprocess();
    	if(!isPostBack())
        {
     	   
     	   logger.logInfo("|"+"Is not postback");
        }
        contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
    
    }
  
    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    @SuppressWarnings("unchecked")
    public void prerender() 
    {
    	
        String methodName="prerender";
    	try
    	{
		    	FillTenantInfo();
		    	FillApplicantInfo();
		    	//To ensure that unit data item is present in view root
		    	if(viewRootMap.containsKey("UNITINFO")|| viewRootMap.containsKey("AUCTIONUNITINFO"))
                        getUnitDataList();
		    	if(hdnManagerId!=null && hdnManagerId.trim().length()>0)
		    	{
		    	  String[] managerInfo =  getPersonNameIdFromHidden(hdnManagerId);
		    	  this.setTxtManager(managerInfo[1]);
		    	}
		    	if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0)
		    	{
		    	  String[] sponsorInfo =  getPersonNameIdFromHidden(hdnSponsorId);
		    	  this.setTxtSponsor(sponsorInfo[1]);
		    	}
		    	if(viewRootMap.containsKey(TENANT_INFO))
			    {
				   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);
			       //tenantRefNum=tenantViewRow.getTenantNumber();
			       txtTenantType=getTenantType(tenantViewRow);
			       String tenantNames="";
			        if(tenantViewRow.getPersonFullName()!=null)
			        tenantNames=tenantViewRow.getPersonFullName();
			        if(tenantNames.trim().length()<=0)
			        	 tenantNames=tenantViewRow.getCompanyName();
			       txtTenantName=tenantNames;
			       
			    }
		    	calculatePaymentScheduleSummaray();
		    	getContractStatus();
		    	EnableDisableButtons();
		    	setPageTitle();
		    	if(!isPostBack())
		    	{
		    		
			    	if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL)&& 
			    			this.getPaymentSchedule().size()>0)
			    	
		    		tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
		    	}
		    	   
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured:",ex);		
    	}
    }
    
    private void clearApplicationDetails() {
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_NUMBER);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DATE);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_ID);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_STATUS);
    	
    }
    @SuppressWarnings("unchecked")
    private void populateApplicationDetailsTab() {
		//PopulatingApplicationDetailsTab
		if(requestView!=null)
		{
			if(requestView.getRequestNumber()!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
			if(requestView.getRequestDate()!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
			if(requestView.getStatusId()!=null)
			{
				if(getIsEnglishLocale())
				{
					if(requestView.getStatusEn()!=null)	
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusEn());
				}else if(getIsArabicLocale())
				{
					if(requestView.getStatusAr()!=null)
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusAr());
				}
			}
			else
			{
				List<DomainDataView >ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
				DomainDataView ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
				if(getIsEnglishLocale())
				{
						
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescEn());
				}else if(getIsArabicLocale())
				{
					
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescAr());
				}
			}
			if(requestView.getApplicantView()!=null)
				populateApplicantDetails(requestView.getApplicantView());
			
			
		}
	}
    @SuppressWarnings("unchecked")
    private void populateApplicantDetails(PersonView pv)
    {
    	if(pv.getPersonId()!=null )
       	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,pv.getPersonId());
      if(pv.getPersonFullName()!=null && pv.getPersonFullName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
     else if(pv.getCompanyName()!=null && pv.getCompanyName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
    	 String personType = getTenantType(pv);
     if(personType !=null)
         viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
     if(pv.getCellNumber()!=null)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
     if(pv.getPersonId()!=null)
     {
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,pv.getPersonId());
    	 hdnApplicantId = pv.getPersonId().toString();
     }
    	 
    }
    private String[] getPersonNameIdFromHidden(String hdn)
    {
      return hdn.split(":");
    }
    @SuppressWarnings("unchecked")
    private void FillApplicantInfo()throws PimsBusinessException
    {
    	String methodName="FillApplicantInfo";
    	logger.logInfo(methodName+"| Start");
    	PersonView applicantView;
    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW) && 
    	   viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW)!=null		
    	   )
    	{
    		applicantView=(PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
	    	if(hdnApplicantId!=null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(hdnApplicantId));
	    		List<PersonView> personsList =  psa.getPersonInformation(pv);
	    		if(personsList.size()>0)
	    		{
	    			pv = personsList.get(0);
	    			populateApplicantDetails(pv);
	    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
	    		}
	    			
	    	}
    	}
    	else if(hdnApplicantId!=null && hdnApplicantId.trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(hdnApplicantId));
    		List<PersonView> personsList=  psa.getPersonInformation(pv);
    		if(personsList.size()>0)
    		{
    			pv = personsList.get(0);
    			populateApplicantDetails(pv);
    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
    		}	
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }
    @SuppressWarnings("unchecked")
    private void FillTenantInfo()throws PimsBusinessException
    {
    	String methodName="FillTenantInfo";
    	logger.logInfo(methodName+"| Start");
    	PersonView tenantView;
    	if(viewRootMap.containsKey(TENANT_INFO))
    	{
    		tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
	    	if(this.getHdnTenantId()!=null && this.getHdnTenantId().toString().trim().length()>0 && tenantView.getPersonId()!=null && tenantView.getPersonId().compareTo(new Long(this.getHdnTenantId()))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));
	    			
	    	}
    	}
    	else if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(this.getHdnTenantId()));
    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
    		if(tenantsList.size()>0)
    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));
    			
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;
		
		return isViewModePopup;
	}
	@SuppressWarnings("unchecked")
	public boolean getIsContractCommercial()
	{
		boolean isContractCommercial=true;
		  DomainDataView ddv=new DomainDataView();
		if(!viewRootMap.containsKey(DDV_COMMERCIAL_CONTRACT))
		{
		  ArrayList<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
		  ddv=CommonUtil.getIdFromType(ddvList, WebConstants.COMMERCIAL_LEASE_CONTRACT);
		   viewRootMap.put(DDV_COMMERCIAL_CONTRACT,ddv);
		}
		else 
		   ddv=(DomainDataView)viewRootMap.get(DDV_COMMERCIAL_CONTRACT);
		if(this.getSelectOneContractType()!=null && this.getSelectOneContractType().equals(ddv.getDomainDataId().toString()))
			isContractCommercial=true;
		else
			isContractCommercial=false;
		
		return isContractCommercial;
	}
	public void selectContractType_Change()
	{
	   
	   if(getIsContractCommercial())
	    tabCommercialActivity.setRendered(true);
	   else
	    tabCommercialActivity.setRendered(false);
	   
	   //sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);
	   
	}
   private void setPageTitle()
   {
	   if(pageMode.equals(PAGE_MODE_ADD) || pageMode.equals(PAGE_MODE_APPROVE_REJECT)|| 
			   pageMode.equals(PAGE_MODE_UPDATE)||pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW) )
	     pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_NEW_LEASE_CONTRACT);
	   else if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND) ||
			   pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)|| 
			   pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT)||
			   pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE) ||
			   pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL)||
			   pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW)
			   )
		   pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_AMEND_CONTRACT);
   }
   @SuppressWarnings("unchecked")
   private void EnableDisableButtons()
   {
	   String method="EnableDisableButtons";
	   logger.logDebug(method+"|"+"Start...");
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   imgViewTenant.setRendered(false);
	   viewRootMap.put("applicationDetailsReadonlyMode",true);
	   if(getIsContrctStatusRejected())
	   {
		   pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
		   viewRootMap.put("pageMode",pageMode);
	   }
	   if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_ADD))
	   {
		   viewRootMap.put("applicationDetailsReadonlyMode",false);
	       PageModeAdd();
	   }
	   
	   else if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_UPDATE))
	   {
 	       PageModeUpdate();
 	       
		   tabPaidFacility_Click();
	   }
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
	   {
		   viewRootMap.put("applicationDetailsReadonlyMode",false);
		   btnPrint.setRendered(false);
	       PageModeAmendAdd();
	       tabPaidFacility_Click();
	   }
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE))
	   {
		   btnPrint.setRendered(false);
		   PageModeAmendUpdate();
		   tabCommercialActivity_Click(); 
		   tabPaidFacility_Click();
	   }
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT))
		   PageModeAmendApproveReject();
	   else if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_APPROVE_REJECT))
		   PageModeApproveReject();
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW))
		   PageModeView();
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW))
		   PageModeAmendView();
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL))
	   {
		   tabPaidFacility_Click();
		   tabCommercialActivity_Click(); 
		   PageModeAmendCompleteCancel();
	   }
	   
	   logger.logDebug(method+"|"+"Finish...");
   }
   @SuppressWarnings("unchecked")
   private void PageModeAmendView()
   {
	   
       txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       viewRootMap.put("canAddAttachment", false); 
       viewRootMap.put("canAddNote", false);
       btnCollectPayment.setRendered(false);
       btnAddPayments.setRendered(false);
       imgTenant.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	   //txtRefNumber.setRendered(true);
	  // lblRefNum.setRendered(true);
	   tabContractHistory.setRendered(false);
	   tabRequestHistory.setRendered(false);
	   btnGenPayments.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(true);
	   //chkPaidFacilities.setDisabled(true);
	   
   }
   @SuppressWarnings("unchecked")
   private void PageModeView()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
       startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", false); 
       viewRootMap.put("canAddNote", false);
       btnCollectPayment.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	   btnAddPayments.setRendered(false);
	  // txtRefNumber.setRendered(true);
	   //lblRefNum.setRendered(true);
	   btnGenPayments.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(true);
	   tabApplicationDetails.setRendered(false);
	  //chkPaidFacilities.setDisabled(true);
	   
   }
   @SuppressWarnings("unchecked")
   private void PageModeAmendApproveReject()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
	   btnAddPayments.setRendered(false);
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true);
       btnCollectPayment.setRendered(false); 
       viewRootMap.put("canAddNote", true);
       btnGenPayments.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	  // txtRefNumber.setRendered(false);
	 //  lblRefNum.setRendered(false);
	   tabContractHistory.setRendered(false);
	   tabRequestHistory.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(true); 
	   btnRejectAmendLease.setRendered(true);
	   chkCommercial.setDisabled(true);
	  // chkPaidFacilities.setDisabled(true);
	   
   }
   @SuppressWarnings("unchecked")
   private void PageModeAmendCompleteCancel()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
	   btnAddPayments.setRendered(false);
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true);
       if(!isAllPaymentsCollected())
           btnCollectPayment.setRendered(true);
       else
    	   btnCollectPayment.setRendered(false);
       viewRootMap.put("canAddNote", true);
       btnGenPayments.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	  // txtRefNumber.setRendered(false);
	 //  lblRefNum.setRendered(false);
	   tabContractHistory.setRendered(false);
	   tabRequestHistory.setRendered(false);
	   btnCompleteAmendLease.setRendered(true);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   
	   
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(true);
	  // chkPaidFacilities.setDisabled(true);
	   tabCommercialActivity_Click();
	   
   }
   @SuppressWarnings("unchecked")
   private void PageModeApproveReject()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
	   btnAddPayments.setRendered(false);
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true); 
       viewRootMap.put("canAddNote", true);
       btnCollectPayment.setRendered(false);
       btnGenPayments.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	 //  txtRefNumber.setRendered(true);
	 //  lblRefNum.setRendered(true);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(true); 
	   btnReject.setRendered(true);
	   chkCommercial.setDisabled(true);
	  // chkPaidFacilities.setDisabled(true);
   }
   @SuppressWarnings("unchecked")
   private void PageModeAmendAdd()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);
       
	   cmbContractType.setDisabled(true);
	   btnAddPayments.setRendered(true);
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true); 
       viewRootMap.put("canAddNote", true);
       btnCollectPayment.setRendered(false);
       btnGenPayments.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(true); 
	   btnPartners.setRendered(true);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(true);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	  // txtRefNumber.setRendered(false);
	  // lblRefNum.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   tabContractHistory.setRendered(false);
	   tabRequestHistory.setRendered(false);
	   tabAuditTrail.setRendered(false);
	   if(getIsContractCommercial())
	   {
		   viewSpnsrMgrIcons();
		   tabCommercialActivity.setRendered(true);
		   tabPartner.setRendered(true);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity_Click();
		   imgDeleteSponsor.setRendered(false);
		   imgSponsor.setRendered(false);
	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);
		   
		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   }
	   chkCommercial.setDisabled(false);
	  // chkPaidFacilities.setDisabled(false);
   }
   @SuppressWarnings("unchecked")
   private void PageModeAmendUpdate()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);
       
	   cmbContractType.setDisabled(true);    
	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
        btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
        imgTenant.setRendered(false);
        btnCollectPayment.setRendered(false);
	    viewRootMap.put("canAddAttachment", true); 
        viewRootMap.put("canAddNote", true);
	    btnGenPayments.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false); 
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(true);
	   btnCompleteAmendLease.setRendered(false);
	//   txtRefNumber.setRendered(false);
	//   lblRefNum.setRendered(false);
	   tabContractHistory.setRendered(false);
	   tabRequestHistory.setRendered(false);
	   tabAuditTrail.setRendered(true);
	   btnSubmitAmendLease.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
       btnRejectAmendLease.setRendered(false);
	   if(getIsContractCommercial())
	   {
		   viewSpnsrMgrIcons();
		   tabCommercialActivity.setRendered(true);
		   tabPartner.setRendered(true);
		   imgSponsor.setRendered(false);
		   imgDeleteSponsor.setRendered(false);
		   
	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);
		   
		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   }
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   RequestView requestView=(RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
	   List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
	   DomainDataView ddvApproved=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVED);
	   DomainDataView ddvApprovalRequired=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
	   //DomainDataView ddvAccepted=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_ACCEPTED);
	   DomainDataView ddvNew=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
	   if(requestView!=null &&  ddvApproved.getDomainDataId().compareTo(requestView.getStatusId())==0)
	   {
		   btnSaveAmendLease.setRendered(false);
		   btnCompleteAmendLease.setRendered(true);
		   btnAddPayments.setRendered(false);
	   }
	   else if(requestView!=null &&  ddvApprovalRequired.getDomainDataId().compareTo(requestView.getStatusId())==0)
	   { 
		   btnSubmitAmendLease.setRendered(false);
	   }
	   /*else if(requestView!=null &&  (ddvAccepted.getDomainDataId().compareTo(requestView.getStatusId())==0 ||  
			   ddvNew.getDomainDataId().compareTo(requestView.getStatusId())==0)
			   )*/
	  else if(requestView!=null && ddvNew.getDomainDataId().compareTo(requestView.getStatusId())==0)
	   {
		   btnSubmitAmendLease.setRendered(true);
	   }
	   chkCommercial.setDisabled(true);
	  // chkPaidFacilities.setDisabled(true);
   }
   @SuppressWarnings("unchecked")
   private void PageModeUpdate()
   {
	   txt_ActualRent.setReadonly(false);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);
       
	   cmbContractType.setDisabled(false);
	   btnAddPayments.setRendered(true);
       startDateCalendar.setDisabled(false);
       endDateCalendar.setDisabled(false);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(true);
       btnAddAuctionUnit.setRendered(true);
       imgTenant.setRendered(true);
       if(hdnTenantId!=null && hdnTenantId.length()>0)
    	   imgViewTenant.setRendered(true);
       btnCollectPayment.setRendered(true);
       viewRootMap.put("canAddAttachment", true); 
       viewRootMap.put("canAddNote", true);
       btnGenPayments.setRendered(true);
	   btnOccuipers.setRendered(true); 
	   btnPartners.setRendered(true);
	   btnSaveNewLease.setRendered(true);
	   btnSaveAmendLease.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   
	 //  chkPaidFacilities.setDisabled(true);
	   if(getIsContractCommercial())
	   {
		   chkCommercial.setDisabled(false);
		   imgManager.setRendered(true);
		   imgSponsor.setRendered(true);
		   viewSpnsrMgrIcons();
		   tabCommercialActivity.setRendered(true);
		   tabCommercialActivity_Click();
		   tabPartner.setRendered(true);
		   
		   
	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);
		   
		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   
	   }

	   List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
	    requestView =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
	   DomainDataView ddvNew=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
	   //if contract has been saved and contract status is new
	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
	   {
		   btnSendNewLeaseForApproval.setRendered(true);
	       btnCollectPayment.setRendered(false);
	   }
	   else if(getIsContrctStatusActive())
		   ActiveContractForUpdateMode();
	   else if(getIsContractStatusApproved())
	   {
		   PageModeCollectPayment();
	   }
	   
	   if(requestView!=null &&  ddvNew.getDomainDataId().compareTo(requestView.getStatusId())!=0)
	   {  
    	   btnSendNewLeaseForApproval.setRendered(false);
	   }

	   
   }
   private void PageModeCollectPayment()
   {
	   
	   
	   PageModeView();
	   btnCollectPayment.setRendered(true);
	   if(!isPostBack())
	      tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
	   //btnDelPaymentSchedule_Clic
	   
	   
   }

    public void viewSpnsrMgrIcons() {
    	
		   if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0 )//&& getIsCompanyTenant())
		   {
			   imgSponsor.setRendered(false);
			   imgDeleteSponsor.setRendered(true);
		       imgViewSponsor.setRendered(true);
		   }
		   else
		   {
			   imgSponsor.setRendered(true);
			   imgDeleteSponsor.setRendered(false);
		       imgViewSponsor.setRendered(false);
		   }
		   if(hdnManagerId!=null && hdnManagerId.trim().length()>0 )
		   {
			   imgManager.setRendered(false);
			   imgDeleteManager.setRendered(true);
			   imgViewManager.setRendered(true);
		   }
		   else
		   {
			   imgManager.setRendered(true);
			   imgDeleteManager.setRendered(false);
			   imgViewManager.setRendered(false);
		   }
   }
    public void imgDelManager_Click()
    {
 	   hdnManagerId="";
 	  this.setTxtManager("");
    }
   public void imgDelSponsor_Click()
   {
	   hdnSponsorId="";
	   this.setTxtSponsor("");
   }
   private void ActiveContractForUpdateMode()
   {
	       tabApplicationDetails.setRendered(false);
		   txt_ActualRent.setReadonly(true);
	       btnPartnerDelete.setRendered(false);
	       btnOccupierDelete.setRendered(false);
	       imgDeleteManager.setRendered(false);
		   imgDeleteSponsor.setRendered(false);
		   cmbContractType.setDisabled(true);
	       btnAddPayments.setRendered(false);
	       startDateCalendar.setDisabled(true);
	       endDateCalendar.setDisabled(true);
	       issueDateCalendar.setDisabled(true);
	       btnAddUnit.setRendered(false);
	       btnAddAuctionUnit.setRendered(false);
	       imgTenant.setRendered(false);
	       
		   btnSendNewLeaseForApproval.setRendered(false);
		   btnOccuipers.setRendered(false); 
		   btnPartners.setRendered(false);
		   btnSaveNewLease.setRendered(false);
		   btnSaveAmendLease.setRendered(false);
		   
		   if(isAllPaymentsCollected())
		     btnCollectPayment.setRendered(false);
		   else 
			 btnCollectPayment.setRendered(true);
		   btnGenPayments.setRendered(false);
		   btnCompleteAmendLease.setRendered(false);
		   btnSubmitAmendLease.setRendered(false);
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   btnApprove.setRendered(false); 
		   btnReject.setRendered(false);
		   btnApproveAmendLease.setRendered(false); 
		   btnRejectAmendLease.setRendered(false);
		   chkCommercial.setDisabled(true);
		   chkPaidFacilities.setDisabled(true);
   }
   @SuppressWarnings("unchecked")
   private void PageModeAdd()
   {
	   
	   txt_ActualRent.setReadonly(false);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);
       
	   cmbContractType.setDisabled(false);
	   btnAddPayments.setRendered(true);
       startDateCalendar.setDisabled(false);
       endDateCalendar.setDisabled(false);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(true);
       btnAddAuctionUnit.setRendered(true);
       imgTenant.setRendered(true);
       if(hdnTenantId!=null && hdnTenantId.length()>0)
    	   imgViewTenant.setRendered(true);
       
       btnCollectPayment.setRendered(false);
	   btnGenPayments.setRendered(true);
	   btnOccuipers.setRendered(true); 
	   btnPartners.setRendered(true);
	   btnSaveNewLease.setRendered(true);
	   btnSaveAmendLease.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   btnApprove.setRendered(false); 
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false); 
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(false);
	   viewRootMap.put("canAddAttachment", true); 
       viewRootMap.put("canAddNote", true);
       
	   
	   if(viewRootMap.containsKey(UNIT_DATA_ITEM) )
	   {
		   tabFacilities.setRendered(true);
		   tabPaymentTerms.setRendered(true);
		   //tabPaidFacility_Click();
	   }
	   else{
		   tabFacilities.setRendered(false);
		   tabPaymentTerms.setRendered(false);
	   }
	   if(getIsContractCommercial())
	   {
		   viewSpnsrMgrIcons();
		   
		   tabCommercialActivity.setRendered(true);
		   tabCommercialActivity_Click();
		   tabPartner.setRendered(true);
	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);
		   
		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   }
	   //if contract has been saved and contract status is new 
	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
		   btnSendNewLeaseForApproval.setRendered(true);
	   else
		   btnSendNewLeaseForApproval.setRendered(false);
   }
    public boolean getIsCompanyTenant()
    {
    	
    	isCompanyTenant=false;
    	
		if(viewRootMap.containsKey(TENANT_INFO))
	    {
			PersonView tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
		    if(tenantView.getIsCompany().compareTo(new Long(1))==0)
		    	return true;
		    else
		    	return false;
	    }
		
		return isCompanyTenant;
    }
    public void RemoveSessions()
    {
    	
        //Remove session bound with Tabs Start
    	   
       	     if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER));
		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER);
		     if(sessionMap.containsKey(WebConstants.OCCUPIER));
		         sessionMap.remove(WebConstants.OCCUPIER);
		     if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
	         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);
	  
	         if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	         sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	         
	         if(viewRootMap.containsKey(WebConstants.CONTRACT_USER_TASK_LIST));
	         viewRootMap.remove(WebConstants.CONTRACT_USER_TASK_LIST);
	         //Remove session bound with Tabs Finish
	     
	         if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
	        	 sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
	         if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
	        	 sessionMap.remove(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
	         if(sessionMap.containsKey("UNITINFO"))
	        	 sessionMap.remove("UNITINFO");
	         
	         if(sessionMap.containsKey(TENANT_INFO))
	        	 sessionMap.remove(TENANT_INFO);
	         
	         if(sessionMap.containsKey("AUCTIONUNITINFO"))
	        	 sessionMap.remove("AUCTIONUNITINFO");
	         
	        
    	
    }
    public boolean getIsPageUpdateStatusNew()
    {
    	String method="getIsPageUpdateStatusNew";
    	isPageUpdateStatusNew=false;
    	logger.logDebug(method+"|"+"Start...");
    	logger.logDebug(method+"|"+"PAGE MODE..."+pageMode);
    	logger.logDebug(method+"|"+"Contract View Status..."+contractView.getStatus());
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
	    	{
	    		  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	        	  logger.logInfo(method+"|"+"Contract Status Approved..."+contractView.getStatus());
	    	      if(contractView.getStatus()!=null && contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_NEW)) )
	    	      {
	    	    		isPageUpdateStatusNew=true;
	    	    		
	    	    	}
	    		
	    	}
    	
    	logger.logDebug(method+"|"+"Finish...");
    	return isPageUpdateStatusNew;
    }
    public boolean getIsPageModeAdd()
    {
    	String method="getIsPageModeAdd";
    	boolean isPageModeAdd=false;
    	//logger.logDebug(method+"|"+"Start...");
    	//logger.logDebug(method+"|"+"PAGE MODE..."+pageMode);
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) )
	    	{
	    		isPageModeAdd=true;
	    		
	    	}
    	
    	logger.logDebug(method+"|"+"Finish...");
    	return isPageModeAdd;
    }
    public boolean getIsPageModeAmendAdd()
    {
    	String method="getIsPageModeAmendAdd";
    	isPageModeAmendAdd=false;
    	logger.logDebug(method+"|"+"Start...");
    	logger.logDebug(method+"|"+"PAGE MODE..."+pageMode);
	    	if(pageMode!=null && pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) )
	    	{
	    		isPageModeAmendAdd=true;
	    		
	    	}
    	
    	logger.logDebug(method+"|"+"Finish...");
    	return isPageModeAmendAdd;
    }
    public boolean getIsPageModeUpdate()
    {
    	String method="getIsPageModeUpdate";
    	isPageModeUpdate=false;
    	logger.logInfo(method+"|"+"Start...");
    	logger.logInfo(method+"|"+"PAGE MODE..."+pageMode);
    	//For both update and approve the view should remain same except for APPROVE/REJECT buttons
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE)  || pageMode.equals(PAGE_MODE_APPROVE_REJECT) )
	    	{
	    		isPageModeUpdate=true;
	    		
	    	}
    	
    	logger.logInfo(method+"|"+"Finish...");
    	return isPageModeUpdate;
    }
    @SuppressWarnings("unchecked")
    private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	String method="getDomainDataListForDomainType";
    	logger.logInfo(method+"|"+"Start...");
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
				  ddvList.add(ddv);	
				}
				break;
			}
		}
		logger.logInfo(method+"|"+"Finsih...");
    	return ddvList;
    }
    public boolean getIsContractStatusApproved()
    {
    	String method="getIsContractStatusApproved";
    	
    	logger.logDebug(method+"|"+"Start...");
    	isContractStatusApproved=false;
    	if(pageMode.equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
    	{
    	  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	  
    	  logger.logInfo(method+"|"+"Contract Status Approved..."+contractView.getStatus());
	    	if( contractView.getStatus()!=null && 
	    		( 
	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED))||
	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_RENEW_APPROVED))
	    		)
	    	  )
	    	{
	    		isContractStatusApproved=true;
	    		
	    	}
    	}
	    logger.logDebug(method+"|"+"IscontractStatusApproved..."+isContractStatusApproved);
    	logger.logDebug(method+"|"+"Finish...");
    	return isContractStatusApproved;
    }
    @SuppressWarnings("unchecked")
    private String getIdForStatus(String statusName)
    {
         Long ID=new Long(0);
    	       List<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
    	       for(int i=0;i<ddvList.size();i++)
    	       {
    	    	   
    	    	   	DomainDataView ddv=(DomainDataView)ddvList.get(i);
    	    	   if(ddv.getDataValue().equals(statusName))
    	    	   {
    	    		   
    	    		   ID=ddv.getDomainDataId();
    	    	        break;
    	    	   }
    	       }
    	return ID.toString();
    	
    	
    }
    @SuppressWarnings("unchecked")
    public boolean getIsUnitCommercial()
    {
    	String method="getIsUnitCommercial";
    	
    	//logger.logDebug(method+"|"+"Start...");
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
		{
    	  ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE);
		}
    	
    	if(viewRootMap.containsKey(UNIT_DATA_ITEM) )
	    {
			isUnitCommercial=false;
			    UnitView unitView=(UnitView)viewRootMap.get(UNIT_DATA_ITEM);
				HashMap hMap= getIdFromType(ddvList,WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL);
		//	    logger.logDebug(method+"|"+"The usuageType of unit..."+unitView.getUsageTypeId().toString());
			    if(hMap.containsKey("returnId"))
			    {
					logger.logDebug(method+"|"+"Domain Data id for type.."+WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL+" is.."+hMap.get("returnId").toString());
				    if(unitView.getUsageTypeId().toString().equals(hMap.get("returnId").toString()))
				    	isUnitCommercial=true;
				    else 
				    {
				    	isUnitCommercial=false;
				    	if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
		  		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);	
				    }
			    }
		}
//		else if (viewRootMap.containsKey("AUCTIONUNITINFO"))
//	    {
//			isUnitCommercial=false;
//			AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
//			HashMap hMap= getIdFromType(ddvList,WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL);
//		    logger.logDebug(method+"|"+"The usuageType of unit..."+auctionUnitView.getUnitUsageTypeID().toString());
//		    if(hMap.containsKey("returnId"))
//		    {
//				logger.logDebug(method+"|"+"Domain Data id for type.."+WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL+" is.."+hMap.get("returnId").toString());
//			    if(auctionUnitView.getUnitUsageTypeID().toString().equals(hMap.get("returnId").toString()))
//			    	isUnitCommercial=true;
//			    else 
//			    {
//			    	isUnitCommercial=false;
//			    	if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
//	  		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);	
//			    }
//		    }
//		
//				
//				 
//			
//	    }
		//logger.logDebug(method+"|"+"Finish...");
		return isUnitCommercial;
    }
    
    public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	
    	dateFormat= localeInfo.getDateFormat();
    	
		return dateFormat;
	}
    public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
    public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		LocaleInfo localeInfo = getLocaleInfo();
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	public LocaleInfo getLocaleInfo()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	     return localeInfo;
	}
	@SuppressWarnings("unchecked")
	private void loadTypes() throws PimsBusinessException
	{
		   String method="loadTypes";
		   logger.logInfo(method+"|"+"Start..");
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
		   {		   
			  logger.logInfo(method+"|"+"Getting types of unit from application context with type name:"+WebConstants.UNIT_USAGE_TYPE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE, CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_USAGE_TYPE));
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TENANT_TYPE))
		   {		   
			   logger.logInfo(method+"|"+"Getting types of tenant from application type with type type:"+WebConstants.TENANT_TYPE);
			   
			   List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
				   //psa.getDomainDataByDomainTypeName(WebConstants.TENANT_TYPE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE, ddl);
			   DomainDataView ddvCompany= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_COMPANY);
			   viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY,ddvCompany);
			   DomainDataView ddvIndividual= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_INDIVIDUAL);
			   viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL,ddvIndividual);
			   
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TYPE))
		   {		   
			   logger.logInfo(method+"|"+"Getting types of contract from application context with type name:"+WebConstants.CONTRACT_TYPE);
			   List<DomainDataView> contractTypeList = new ArrayList<DomainDataView>(0);
			   List<DomainDataView>ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);
			   DomainDataView ddv=CommonUtil.getIdFromType(ddvList, WebConstants.COMMERCIAL_LEASE_CONTRACT);
			   contractTypeList.add(ddv);
			   ddv=CommonUtil.getIdFromType(ddvList, WebConstants.RESIDENTIAL_LEASE_CONTRACT);
			   contractTypeList.add(ddv);
			   //Collections.sort(contractTypeList,ListComparator.LIST_COMPARE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_TYPE,contractTypeList );
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
		   {		   
			   logger.logInfo(method+"|"+"Getting types of persons from application context with type name:"+WebConstants.PERSON_TYPE);
			   viewRootMap.put(WebConstants.SESSION_PERSON_TYPE, CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE));
			 
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
		   {		   
			   logger.logInfo(method+"|"+"Getting types of request from application context with type name:"+WebConstants.REQUEST_STATUS);
			   viewRootMap.put(WebConstants.SESSION_REQUEST_STATUS, CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS));
			   
			   
		   }
		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CASH))
		   {		   
			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cash");
			   List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			   DomainDataView ddvCash= CommonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CASH,ddvCash);
			   
		   }
		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE))
		   {		   
			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cheque");
			   List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			   DomainDataView ddvCheque= CommonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE,ddvCheque);
			   
		   }
		       loadPaymentStatus();
		      
					   logger.logInfo(method+"|"+"Finish..");
	}
	@SuppressWarnings("unchecked")
	private void loadPaymentStatus()
	{
	  String methodName="loadPaymentStatus";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
		  {
			  List<DomainDataView>ddv=CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS); 
		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, ddv);
		  }
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}
	
	@SuppressWarnings("unchecked")
	private void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
			try
			{
				ContractView contract=new ContractView();
				contract.setContractId(new Long(contractId));
				
				HashMap contractHashMap=new HashMap();
				contractHashMap.put("contractView",contract);
				
				contractHashMap.put("dateFormat",getDateFormat());
				//contractHashMap.put(WebConstants.SHOW_DELETED_CONTRACTS, true);
				PropertyServiceAgent psa =new PropertyServiceAgent();
				logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
				List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
				
				logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
				if(contractViewList.size()>0)
				contractView=(ContractView)contractViewList.get(0);
				viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
				//not in amend mode the request should be filled 
				if(!getIsPageInAmendMode())
				getRequestForNewLeaseContract();
				
			
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|"+"Error occured:",ex);
				throw ex;
				
			}
	}

	private boolean getIsPageInAmendMode()
	{
		boolean isPageInAmendMode=false;
		if(viewRootMap.containsKey("pageMode") && viewRootMap.get("pageMode")!=null) 
		pageMode =viewRootMap.get("pageMode").toString();
		if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)||
				pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)||
				pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT)||
				pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW)
		)
			isPageInAmendMode=true;
		
		return isPageInAmendMode;
	}
	@SuppressWarnings("unchecked")
	private void getRequestForNewLeaseContract()
			throws PimsBusinessException {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_LEASE_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(contractView.getContractId());
		cv.setContractNumber(contractView.getContractNumber());
		rv.setContractView(cv);
		
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0); 
			viewRootMap.put("REQUESTVIEWUPDATEMODE",requestView );
			populateApplicationDetailsTab(); 
		}
	}
    private void loadCombos()throws PimsBusinessException
    {
    	String methodName="loadCombos";
    	logger.logInfo(methodName+"|"+" Start");
    	loadStatus();
    	logger.logInfo(methodName+"|"+" Finish");
    	
    }
    @SuppressWarnings("unchecked")
    private void loadStatus() throws PimsBusinessException
    {
    	String method="loadStatus";
    	logger.logInfo(method+"|"+" Start");
    	
       if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_STATUS))
   	   {	
   		    List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
   		   
   		   logger.logDebug(method+"|"+"Got "+ddl.size()+" rows for Contract status..");
   		   logger.logDebug(method+"|"+"Putting contract status list in session with key...."+WebConstants.SESSION_CONTRACT_STATUS);
   		    viewRootMap.put(WebConstants.SESSION_CONTRACT_STATUS, ddl);
   	   }
    	logger.logInfo(method+"|"+" Finish");
    	
    }
    public String btnBack_Click()
    {
    	String eventOutcome = "back";
    	return eventOutcome;
    }
    public String btnCancel_Click()
    {
    	String eventOutcome = "cancel";
    	return eventOutcome;
    }
    @SuppressWarnings("unchecked")    
    private void removeUnPaidRentFromPaymentScheduleSession()
    {
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> psvList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    	
    	for (PaymentScheduleView psv : psvList) 
    	{
		   //If payment is for Rent
    		if(psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 	)
			{
    			
    			//if Rent is paid
				if(!psv.getIsReceived().equals("N") )
					newPaymentScheduleViewList.add(psv);
			}
    		//If payment other than rent
			else
        	  	newPaymentScheduleViewList.add(psv);
        
    	}
    	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
    
    }
    @SuppressWarnings("unchecked")
    public String openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		        Set<RequestFieldDetailView> set = (Set<RequestFieldDetailView>)viewRootMap.get("AMEND_RFDV");
		        Long oldContractId= new Long(set.iterator().next().getRequestKeyValue());
		        
		        
		        if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
		    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null		
		    	 )
		    	{
    		        List<PaymentScheduleView> paymentSchedules = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		        List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
    				for (int i=0; i<paymentSchedules.size(); i++)
    				{
    					if (paymentSchedules.get(i).getSelected()!=null && paymentSchedules.get(i).getSelected())
    					{
 						  if(paymentSchedules.get(i).getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0)
 							paymentSchedules.get(i).setContractId(oldContractId);
    						
 						 paymentsForCollection.add(paymentSchedules.get(i));
    					}
    				}
    				sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
		    	}

		        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
		        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
		        openPopUp("", "javascript:openPopupReceivePayment();");
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
    @SuppressWarnings("unchecked")
    public void btnGenPayments_Click()
    {
    	String methodName = "btnGenPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
    	try
    	{
    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    		if(txttotalContractValue!=null && txttotalContractValue.length()>0)
    		{
            
    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
    		    {
    				removeUnPaidRentFromPaymentScheduleSession();
    		     alreadyPresentPS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,alreadyPresentPS);
    		    }
    			 String extrajavaScriptText ="var screen_width = 1024;"+
                 "var screen_height = 450;"+
                 "window.open('GeneratePayments.jsf?ownerShipTypeId="+
                    viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+"','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=100,top=150,scrollbars=yes,status=yes');";

 		        
 		        //sessionMap.put(WebConstants.DEPOSIT_AMOUNT,cash);
 				sessionMap.put(WebConstants.TOTAL_CONTRACT_VALUE,txttotalContractValue);
 				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_START_DATE,df.format(this.getContractStartDate()));
 				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_END_DATE,df.format(this.getContractEndDate()));
 				
 				
 		        openPopUp("GeneratePayments.jsf", extrajavaScriptText);
    		}
    		else
    			logger.logInfo(methodName+"|"+" Total Contract Value is not present...");
    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
    @SuppressWarnings("unchecked")
    public void btnEditPaymentSchedule_Click()
    {
    	String methodName = "btnEditPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
 			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
        openPopUp("", "javascript:openPopupEditPaymentSchedule();");
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	CommonUtil.printPaymentReceipt(this.contractId, psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.AMEND_CONTRACT);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings("unchecked")
    public void btnAddPayments_Click()
    {
    	String methodName = "btnAddPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> PS=new ArrayList<PaymentScheduleView>();
    	String psStatusInReq="";
    	try
    	{
    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null		
    			  )
    		    {
    		     PS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,PS);
    		    }
    			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
    			{
    			   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    			   DomainDataView ddv = CommonUtil.getDomainDataFromId((List<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS), 
	                       contractView.getStatus());
    			   if(ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_NEW && ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_DRAFT )
    			   {
    				    setRequestParam(WebConstants.PAYMENT_STATUS_IN_REQUEST,WebConstants.PAYMENT_SCHEDULE_PENDING);
    				    psStatusInReq = WebConstants.PAYMENT_SCHEDULE_PENDING;
    			   }
    			
    			}
    			 String extrajavaScriptText ="var screen_width = 1024;"+
                 "var screen_height = 450;"+
                 "window.open('PaymentSchedule.jsf?totalContractValue="+txttotalContractValue+"&"+WebConstants.PAYMENT_STATUS_IN_REQUEST+"="+psStatusInReq+
                 "&ownerShipTypeId="+viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+
                 		"','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
 		        openPopUp("PaymentSchedule.jsf", extrajavaScriptText);

    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
    @SuppressWarnings("unchecked")
    public Long getPaymentSchedulePendingStausId()
    {
    	DomainDataView ddv = new DomainDataView();
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	 ddv=CommonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_PENDING);
    	}
    	else
    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_PENDING) ;
    	return ddv.getDomainDataId();
    	
    }
   
    public void btnDelPaymentSchedule_Click()
    {
    	String methodName = "btnDelPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	try{
    	PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	if(pageMode.equals(PAGE_MODE_ADD))
    		removeFromPaymentScheduleSession(psv);
    	else //if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
    		markPaymentScheduleAsDeleted(psv);
    	
    	getPaymentScheduleListFromSession();
    	//calculatePaymentScheduleSummaray();
    	logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		
    	}
    }
    @SuppressWarnings("unchecked")
    private void markPaymentScheduleAsDeleted(PaymentScheduleView psv)
    {
    	String methodName="markPaymentScheduleAsDeleted";
    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
        {
            //If payment schedule existed in DB
        	if(psv.getPaymentScheduleId()!=null )
        	{
	        	if(psv.getPaymentScheduleId().compareTo(paymentScheduleView.getPaymentScheduleId())==0 )
	        	{
	        		paymentScheduleView.setIsDeleted(new Long(1));
	        
	        	}
	        	newPaymentScheduleViewList.add(paymentScheduleView);
        	}
        	//If payment schedule added during amend
        	else
        	{
	        		if(psv.getPaymentDueOn().compareTo(paymentScheduleView.getPaymentDueOn())==0     
	            		&&	psv.getTypeId().compareTo(paymentScheduleView.getTypeId())==0 
			   	       )
	        	      logger.logDebug(methodName+"|"+" ...");
         	       else
              		  newPaymentScheduleViewList.add(paymentScheduleView);	
	        	
        	        	
            
           }
           }
        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList);
    
    }
    @SuppressWarnings("unchecked")
    private void removeFromPaymentScheduleSession(PaymentScheduleView psv)
    {
    	String methodName="removeFromPaymentScheduleSession";
    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
        {
        	//To uniquely identify the payment in add Mode
        	//Combination of Payment Type and PaymentDueOn should be unique
        	if(psv.getPaymentDueOn().compareTo(paymentScheduleView.getPaymentDueOn())==0     
        		&&	psv.getTypeId().compareTo(paymentScheduleView.getTypeId())==0 
        			   	)
        		logger.logDebug(methodName+"|"+" ...");
        	else
              		newPaymentScheduleViewList .add(paymentScheduleView);
        }
      	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
    
    }
    private void setRequestFieldDetailView(
			RequestFieldDetailView requestFieldDetailView ) throws PimsBusinessException {
		
		
		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
		requestFieldDetailView.setRequestKeyValue(contractId);
		
		RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
	}
   public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_NEW_LEASE_CONTRACT;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if(requestId!= null){
			
	    	String entityId = requestId.toString();
			
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	@SuppressWarnings("unchecked")
	public void btnSubmitAmendLease_Click()
    {
        String methodName = "btnSubmitAmendLease_Click";
        SystemParameters parameters = SystemParameters.getInstance();
	    String endPoint= parameters.getParameter(WebConstants.AMEND_LEASE_CONTRACT_BPEL_ENDPOINT);
	    logger.logInfo(methodName+"|"+" Start...");
    	errorMessages=new ArrayList<String>(0);
    	try
    	{
    		PIMSAmendLeaseContractBPELPortClient port=new PIMSAmendLeaseContractBPELPortClient();
		   	port.setEndpoint(endPoint);
		   	logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
		   	RequestView rv = (RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
		   	logger.logInfo(methodName+"|"+" RequestId..."+rv.getRequestId());
		   	logger.logInfo(methodName+"|"+" ContractId..."+contractId);
		   	logger.logInfo(methodName+"|"+" LoggedInUser..."+getLoggedInUser());
		   	if(contractId!=null && contractId.length()>0 && rv.getRequestId()!=null)
		   	{
		   	       port.initiate(new Long(contractId), rv.getRequestId(),getLoggedInUser(),null, null);
		   	       rv =new PropertyServiceAgent().getRequestById(rv.getRequestId());
		   	       viewRootMap.put(REQUEST_FOR_AMEND,rv);
		   	}
		   	notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Amend_Request_Received);
		   	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
		   	viewRootMap.put("pageMode", WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT);
		   	logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
     	{
     		logger.LogException(methodName+"|"+" Error Occured...", ex);
     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
     		
     	}

        
        

    }
	@SuppressWarnings("unchecked")
   public void removePaymentScheduleIds()
   {
		String methodName = "removePaymentScheduleIds";
    	logger.logInfo(methodName+"|"+" Start...");
    	Set<PaymentScheduleView> newPsv=new HashSet<PaymentScheduleView>(0); 
    	Set<PaymentScheduleView> psv =contractView.getPaymentScheduleView();
    	Iterator<PaymentScheduleView> iter=psv.iterator();
    	while(iter.hasNext())
    	{
    		PaymentScheduleView  paymentScheduleView=(PaymentScheduleView)iter.next();
    		if(paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
    		{
    			if(paymentScheduleView.getPaymentScheduleId()!=null)
    			{
    			   paymentScheduleView.setOldPaymentScheduleId(paymentScheduleView.getPaymentScheduleId().toString());
    			   paymentScheduleView.setPaymentScheduleId(null);
    			   paymentScheduleView.setPaymentNumber(null);
    			}
    			newPsv.add(paymentScheduleView);
    		}
    	}
    	contractView.setPaymentScheduleView(newPsv);
	   
    	logger.logInfo(methodName+"|"+" Finish...");
   }
   @SuppressWarnings("unchecked")
	public String btnSaveAmendLease_Click()
    {
    	String methodName = "btnSaveAmendLease_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	
    	List<PaymentReceiptView> paymentReceiptViewList=new ArrayList<PaymentReceiptView>();
     	
     	try
     	{
 		    	PropertyServiceAgent myPort = new PropertyServiceAgent();
 		    	valueChangeListerner_RentValue();
 				if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
 			    {
 					if(!putControlValuesInView())
 			    	{
 						logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
 						removePaymentScheduleIds();
 						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
 						
 						Long newContractId=contractView.getContractId();
 				    	logger.logInfo(methodName+"|"+" Contract added with Id::"+newContractId);
 		                this.contractId=newContractId.toString();
 		                requestView.setContractView(contractView);
 		                List<RequestView> rvList = new PropertyServiceAgent().getAllRequests(requestView, null, null, null);
 		               if(rvList.size()>0)
 		            	  viewRootMap.put(REQUEST_FOR_AMEND,rvList.get(0));
 		                pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE;
	         		    viewRootMap.put("pageMode", pageMode);
 		                getAmendDataFromRequestView();
 		                String successMessage="";
 		                successMessage=ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_SAVED);
 				 	    successMessages.add(successMessage);
 	         		   
 	         		   btnSubmitAmendLease.setRendered(true);
 	         		   logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..COMPLETED "); 
 	         		    
 						getContractById(contractId.toString());
 						logger.logInfo(methodName+"|"+" Reference Number:"+contractView.getContractNumber());
 						this.setRefNum(contractView.getContractNumber());
 			    	
 						}
 				    	

 			    }
 				else
 				{
 					if(putUpdatedControlValuesInView())
 					{
 					    if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
 						  paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);	
 					    contractView.setPaymentScheduleView(getPaymentSchedule());
 					    logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
 						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
 						Long contractId=contractView.getContractId();
 						successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY));
 				    	logger.logInfo(methodName+"|"+" Contract updated with Id::"+contractId);
 					}
 				}
 				//For time being 
                PageModeAmendUpdate();
                btnSaveAmendLease.setRendered(false);
 				viewRootMap.put("pageMode",pageMode);
 			logger.logInfo(methodName+"|"+" Finish...");
     	}
     	catch(Exception ex)
     	{
     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
     		logger.LogException(methodName+"|"+" Error Occured...", ex);
     		
     	}
     	return "";
    }
   @SuppressWarnings("unchecked")
    public void btnCompleteAmend_Click()
	{
		String methodName="btnCompleteAmend_Click";
		logger.logInfo(methodName+"|"+"Start...");
		errorMessages=new ArrayList<String>(0);
        errorMessages.clear();
		try
		{
			
		    PropertyServiceAgent psa =new PropertyServiceAgent();
	      	 requestView=(RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
	      	
	      	Set<RequestFieldDetailView> setRequestFieldDetailsView=(HashSet<RequestFieldDetailView>)viewRootMap.get("AMEND_RFDV" );
	      	Iterator ITER= setRequestFieldDetailsView.iterator();
	      	RequestFieldDetailView rfdv=new RequestFieldDetailView ();
	      	
	      	while(ITER.hasNext())
	      	{
	      		rfdv=(RequestFieldDetailView)ITER.next();
	      		//rfdv.get
	      		break;
	      	}

	      	contractView.setCreatedBy(getLoggedInUser());
	      	contractView.setCreatedOn(new Date());
	      	
	      	
	      	if(!putControlValuesInView())
	      	{
	      		Set<RequestView> rvSet = new HashSet<RequestView>(0);
		      	rvSet.add(requestView);
		      	contractView.setRequestsView(rvSet);
	      		psa.transferContract(requestView.getRequestId(),new Long(rfdv.getRequestKeyValue()),contractView);
	      		ApproveRejectTask(TaskOutcome.COMPLETE);
	      	    notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Amend_Request_Completed);
	      	    btnCompleteAmendLease.setRendered(false);
		      	viewRootMap.put("pageMode","");
	      	    successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_COMPLETED));
	      	
	      	}
	      	
		    logger.logInfo(methodName+"|"+"Finish...");
	    }
		catch(Exception ex)
    	{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}
	}    

   @SuppressWarnings("unchecked")
    public String btnApproveAmend_Click()
    {
    	String methodName = "btnApproveAmend_Click";
        errorMessages=new ArrayList<String>(0);
        errorMessages.clear();
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{    	   
   		        ApproveRejectTask(TaskOutcome.APPROVE);
   				 
 		      	logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode start...");
 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED);
 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
 		      	if(approvedStatus!=null)
 		      	   contractView.setStatus( new Long(approvedStatus));
 		      	logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode Finish...");   
 		      	notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Amend_Request_Approved);
 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_APPROVED));
 		      	btnApproveAmendLease.setRendered(false);
 		      	btnRejectAmendLease.setRendered(false);
 		      	viewRootMap.put("pageMode", WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL);
 		      	//viewRootMap.put("pageMode","");
    	        logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException exc)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",exc);
    	
    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}
    	return "";
    }
   @SuppressWarnings("unchecked")
    public String btnRejectAmend_Click()
    {
    	String methodName = "btnRejectAmend_Click";
        errorMessages=new ArrayList<String>(0);
        errorMessages.clear();
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{    	   
   		        ApproveRejectTask(TaskOutcome.REJECT);
   		        RequestView rv = (RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
   		        new PropertyService().setPaymentScheduleCanceled(rv.getRequestId());
 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode start...");
 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_REJECT);
 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
 		      	if(approvedStatus!=null)
 		      	   contractView.setStatus( new Long(approvedStatus));
 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode Finish...");   
 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_REJECTED));
 		      	btnApproveAmendLease.setRendered(false);
 		      	btnRejectAmendLease.setRendered(false);
 		      	viewRootMap.put("pageMode","");
    	        logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException exc)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",exc);
    	
    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}
    	return "";
    }
    private void ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String methodName="ApproveRejectTask";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
    		if(!viewRootMap.containsKey(WebConstants.CONTRACT_USER_TASK_LIST))
    		   getIncompleteRequestTasks();
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.CONTRACT_USER_TASK_LIST);
	    	//userTask.
			String loggedInUser=getLoggedInUser();
			//logger.logInfo(methodName+"|"+" TaskId..."+userTask.);
			logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
			logger.logInfo(methodName+"|"+" TaskOutCome..."+taskOutCome);
			logger.logInfo(methodName+"|"+" loggedInUser..."+loggedInUser);
			   BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			   bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
    @SuppressWarnings("unchecked")
    public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			RequestView rv =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.CONTRACT_USER_TASK_LIST,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}
	}
	
    public String getErrorMessages()
	{
		
		
		return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
    @SuppressWarnings("unchecked")
    public void btnOccupierDelete_Click()
    {
    	String methodName="btnOccupierDelete_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PersonView selectedOccupier = (PersonView)propspectiveOccupierDataTable.getRowData();
    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.OCCUPIER);
    	for (PersonView personView : previousOccupiersList) 
    	{
		  if(selectedOccupier.getPersonId().compareTo(personView.getPersonId())!=0)
			  newOccupiersList.add(personView);
		
    	}
    	
    	sessionMap.put(WebConstants.OCCUPIER, newOccupiersList);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings("unchecked")
    public void btnPartnerDelete_Click()
    {
    	String methodName="btnPartnerDelete_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PersonView selectedPartner = (PersonView)propspectivepartnerDataTable.getRowData();
    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.PARTNER);
    	for (PersonView personView : previousOccupiersList) 
    	{
          if(personView.getNewPerson())
          {
		    if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
			   newOccupiersList.add(personView);
		  }
		  //If already existed and deleted in amend mode
		  else
		  {
		     if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
                newOccupiersList.add(personView);
		  }
    	}
    	sessionMap.put(WebConstants.PARTNER, newOccupiersList);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings("unchecked")
    private boolean putUpdatedControlValuesInView() throws Exception
    {
    	
    	String methodName="putUpdatedControlValuesInView";
    	logger.logInfo(methodName+"|"+" Start...");
    	String loggedInUser=getLoggedInUser();
    	errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	boolean isValidated=true;
    	contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
    	try
    	{
    		
    		if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
		         contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
        		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
        	    logger.logInfo(methodName+"|"+" ContractType..."+this.getSelectOneContractType());
        	    if(!AttachmentBean.mandatoryDocsValidated())
		    	{
		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
		    	      isValidated=false;
		    	}
		        if(this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1"))
			    contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
			    else
			    {
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
			    }
		        
		    	
		    	if(contractStartDate!=null && !contractStartDate.equals("") 
						 && contractEndDate!=null && !contractEndDate.equals("") )
    			{
    			    
		    		
    				//Contract start date shud only be less than Contract EXPIRY date
		    		  if (contractStartDate.compareTo(contractEndDate)>=0)
		    			{
		    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
		    				isValidated=false;
		    			}
		    		  
    			}
		    	else
		    	{
		    		
		    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
		    	      isValidated=false;
		    	}
		    
        		
		    	logger.logInfo(methodName+"|"+" tenantRefNum..."+this.getTenantRefNum());
		    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
		    	if(viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null )
		    	{
		    		tenantsView.setPersonId((new Long(this.getHdnTenantId())));
		    		//tenantsView.setTenantNumber(tenantRefNum);
		    		contractView.setTenantView(tenantsView);
		    		//requestView.setTenantsView(tenantsView);
		    		
		    	}
		    	else
		    	{
		    		
		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
		    		isValidated=false;
		    	}
        		
		    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
		    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
		    	{
		    	  if(isUnitValid())
		    	  {
		    		if(unitRentAmount!=null && unitRentAmount.trim().length()>0)
		    		{
		    		  // unitsView.setUnitNumber(this.getUnitRefNum());
		    		   unitsView.setUnitId(new Long(hdnUnitId));
		    		   if(viewRootMap.containsKey(AUCTION_UNIT_ID) && viewRootMap.get(AUCTION_UNIT_ID)!=null )
			    	   {
			    		    unitsView.setAuctionUnitId(new Long(viewRootMap.get(AUCTION_UNIT_ID).toString()));
			    		}
                       contractView.setContractUnitView(getContractUnitSet(loggedInUser));
		    		}
		    		else
			        {
			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
			    		isValidated = false;
			    	}
		    	  }
		    	  else
			        {
			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UNITS_INVALID));
			    		isValidated = false;
			    	}
		    	}
		    	else
		        {
		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
		    		isValidated = false;
		    	}
        		if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null)
		    	{
		    		//if tenant is company
		    		if(getIsCompanyTenant())
		    		{
		    			//if Contract is commercial and tenant is company then sponsor info is required.
			    		if(getIsContractCommercial())
			    		{
			    			if(this.getTxtSponsor() ==null || this.getTxtSponsor().length()<=0 )
			    			{
			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
					    		isValidated = false;
			    				
			    			}
			    			
			    			
			    		}
			    		//if unit is residential than and tenant is residential than contract can not be added
			    		else 
			    		{
			    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
			    			isValidated = false;
			    			
			    		}
		    		}
		    		
		    		
		    	}
        		  
        		 if(contractStartDate!=null && !contractStartDate.equals("") )
			        {
			        	contractView.setStartDate(contractStartDate);
			        	contractView.setOriginalStartDate(contractStartDate);
			        	
			        }
			        else
			        {
			    		
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_START_DATE));
			        	isValidated = false;
			    	}
			        
			        logger.logInfo(methodName+"|"+" contractEndDate..."+contractEndDate);
			        if(contractEndDate!=null && !contractEndDate.equals(""))
			        {
			        	contractView.setEndDate(contractEndDate);
			        	contractView.setOriginalEndDate(contractEndDate);
			        	
			        }
			        else
			        {
			    		
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_EXPIRY_DATE));
			        	isValidated = false;
			    	}
             		if(getIsContractValueandRentAmountEqual())
             		{
             			if(getIsContractCommercial() && !isCommercialActivitySelected())
    	    			{
    	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
    			    		isValidated = false;
    	    			}
             			else if( getIsContractCommercial())
	 			        {
	 			        	contractView.setContractActivitiesView(getCommercialActivities());
	 			        	
	 			        }
				        logger.logInfo(methodName+"|"+" totalContractAmount..."+txttotalContractValue);
				        contractView.setRentAmount(new Double(txttotalContractValue));
				        contractView.setPaymentScheduleView(getPaymentSchedule());
				        contractView.setContractPersonView(getContractPersons());
				        contractView.setContractPartnerView(getContractParntner());
				        if(hdnUnitId!=null && hdnUnitId.trim().length()>0) 
				        {
				           unitsView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
			    		   
				           contractView.setContractUnitView(getContractUnitSet(loggedInUser));
				        }
				        //contractView.set
				        contractView.setUpdatedBy(loggedInUser);
				        
				        contractView.setUpdatedOn(new Date());
				        
             		}
             		else
             		{
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
			        	isValidated = false;
			        }

    	}
    	catch(Exception ex)
    	{
    		logger.logError(methodName+"|"+" Exception Occured..."+ex);
    		throw ex;
    		
    	}
    	logger.logInfo(methodName+"|"+" Finish...");
    	return isValidated ;
    }
    @SuppressWarnings("unchecked")
    private boolean hasErrors()throws Exception
    {
       if(pageMode.compareTo(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL)!=0 )
       {
	       //Validating Applicant Tab Start
	         if(!viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) || viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)==null)
	         {
			   errorMessages.add(getBundle().getString(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			   tabPanel.setSelectedTab(TAB_ID_APPLICATION);
			   return true;
			 }
	         
	       //Validating Applicant Tab Finish
	         
	       //Validating Basic Info Start
	         
	         if(hasBasicInfoErrors())
	         {
	        	 tabPanel.setSelectedTab(TAB_ID_BASIC);
	        	 return true;
	         }
	       //Validating Basic Info Finish
	         
	      // Validating Unit Tab Start
	         
	         if(hasUnitTabErrors())
	         {
	        	 tabPanel.setSelectedTab(TAB_ID_UNIT);
	        	 return true;
	         }
	       //Validating Unit Tab Finish
	         
	       //Validating Commercial Activity Start
	        if(getIsContractCommercial() && !isCommercialActivitySelected())
			{
					errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
					tabPanel.setSelectedTab(TAB_ID_COMMERCIAL_ACTIVITY);
					return true;
			}
	      //Validating Commercial Activity Finish
	        
	        //Validating Payment Schedules Start
//	        if(!getIsContractValueandRentAmountEqual())
//	        {
//	        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
//	        	tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
//	        	return true;
//	        }
	        //Validating Payment Schedules Finish
	        
	        
	         //Validating Attachments Start
	        if(!AttachmentBean.mandatoryDocsValidated())
		    {
		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
		    		 tabPanel.setSelectedTab(TAB_ID_ATTACHEMENT);
		    		return  true;
		    }
	         //Validating Attachments Finish
       }
       else
       {
    	   if(getAnyPaymentsToBeCollected())
    	   {
    			errorMessages.add(ResourceUtil.getInstance().getProperty("amendContract.msg.CollectPendingPayments"));
	    		tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
	    		return  true;
    	   }
       }
    	   
         return false;
    }
    @SuppressWarnings("unchecked")
    private boolean getAnyPaymentsToBeCollected()
    {
    	
    	List<DomainDataView> paymentStatusList= (ArrayList<DomainDataView> )viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
		DomainDataView ddvPendingStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
        if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{
		  List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		  for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
		  {
			  if(paymentScheduleView.getStatusId().compareTo(ddvPendingStatus.getDomainDataId() )==0)
				  return true;
			
		  }
		}    	
    return false;
    }
    private boolean hasUnitTabErrors()throws Exception
    {
    	boolean hasUnitTabErrors=false;
    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
        {
    	
    		if(unitRentAmount==null || unitRentAmount.trim().length()<=0)
        	{
		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
		    		hasUnitTabErrors=true;
		    }
    		else 
    		{
    			try
    			{
    			  new Double(unitRentAmount);
    			}
    			catch(Exception ex)
    			{
	    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_INVALID_RENT_VALUE));
	    		hasUnitTabErrors=true;
    			}
	        }
        
        
        
        
        }
    	else
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
    		hasUnitTabErrors=true;
    	}
    
       return hasUnitTabErrors;  	
    }
    private boolean hasBasicInfoErrors()throws Exception
    {
    	boolean hasBasicInfoErrors=false;
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());	
    	if(!viewRootMap.containsKey(TENANT_INFO) || viewRootMap.get(TENANT_INFO)==null )
    	{
    		
    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
    		hasBasicInfoErrors=true;
    	}
    	if(this.getSelectOneContractType()==null || this.getSelectOneContractType().equals("-1"))
	    {
	        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
	        	hasBasicInfoErrors = true;
	    }
    	if(contractStartDate!=null && !contractStartDate.equals("") 
				 && contractEndDate!=null && !contractEndDate.equals("") )
		{
		    
   		
			String todayDate=dateFormat.format(new Date());
			logger.logDebug("TodayDate.."+todayDate);
			//Contract start date shud only be less than Contract EXPIRY date
	   		  if (contractStartDate.compareTo(contractEndDate)>=0)
	   		  {
	   				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
	   				hasBasicInfoErrors=true;
	   		  }
//	   		  else if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) && contractStartDate.compareTo(dateFormat.parse(todayDate))==-1)
//	   		  {
//	   			  
//				      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_TODAY));
//				      hasBasicInfoErrors=true;
//	   		  }
		}
	   	else
	   	{
	   		
	   	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
	   	   hasBasicInfoErrors=true;
	   	}
    	//if tenant is company
    	if(( this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1") )&& getIsCompanyTenant())
    	{
    			//if Contract is commercial and tenant is company then sponsor info is required.
	    		if(getIsContractCommercial())
	    		{
	    			if(this.getTxtSponsor()==null || this.getTxtSponsor().length()<=0 )
	    			{
	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
	    				hasBasicInfoErrors=true;
	    			}
	    		}
	    		//if unit is residential than and tenant is company than contract can not be added
	    		else 
	    		{
	    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
	    			hasBasicInfoErrors=true;
	    		}
    	}
    		
    		
    	
    	
    	
    	return hasBasicInfoErrors;
    }
    @SuppressWarnings("unchecked")
    private boolean putControlValuesInView() throws Exception
    {
    	String methodName="putControlValuesInView";
    	logger.logInfo(methodName+"|"+" Start...");
    	errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	boolean isError=false;
    	contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
    	String loggedInUser=getLoggedInUser();
    	
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
    	try
    	{
    		      if(hasErrors())
    		    	  return true;
    		       DomainDataView ddContractCategory = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.ContractCategory.CONTRACT_CATEGORY), WebConstants.ContractCategory.LEASE_CONTRACT);
    		       
    		       contractView.setContractCategory(ddContractCategory.getDomainDataId() );
             		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
    		        if(contractId!=null && !contractId.equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			    	{
			    	  contractView.setContractId(new Long(contractId));

			    	}
                    logger.logInfo(methodName+"|"+" ContractType..."+this.getSelectOneContractType());
    		        contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
    			    logger.logInfo(methodName+"|"+" datetimerequest..."+this.getDatetimerequest());
			    	contractView.setContractDate(dateFormat.parse(this.getDatetimerequest()));
			    	requestView.setRequestDate(dateFormat.parse(this.getDatetimerequest()));
			    	
			    	//requestView.setDescription(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
			    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) && viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)!=null)
			    	{
			    		logger.logInfo(methodName+"|"+" applicantId..."+viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString());
			    		PersonView applicantView = new PersonView();
			    		applicantView.setPersonId(new Long(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString()));
			    		requestView.setApplicantView(applicantView);
			    		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) && 
			    				viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION)!=null)
			    		{
			    		  requestView.setDescription(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString() );
			    		  contractView.setRemarks(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString());
			    		}
			    	}

			    	else
			    	{
			    		errorMessages.add(getBundle().getString(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			    		isError = true;
			    	}
			    	if(contractStartDate!=null && !contractStartDate.equals("") 
							 && contractEndDate!=null && !contractEndDate.equals("") )
	    			{
	    			    
			    		
	    				String todayDate=dateFormat.format(new Date());
		    			logger.logDebug(methodName+"|"+"TodayDate.."+todayDate);
		    			//Contract start date shud only be less than Contract EXPIRY date
			    		  if (contractStartDate.compareTo(contractEndDate)>=0)
			    		  {
			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
			    				isError=true;
			    		  }
//			    		  else if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) && contractStartDate.compareTo(dateFormat.parse(todayDate))==-1)
//			    		  {
//			    			  
//						      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_TODAY));
//						      isError=true;
//			    		  }
	    			}
			    	else
			    	{
			    		
			    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
					      isError=true;
			    	}

			    	logger.logInfo(methodName+"|"+" refNum..."+this.getRefNum());
			    	if(this.getRefNum()!=null && !this.getRefNum().equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			    	{
			    		contractView.setContractNumber(this.getRefNum());
			    	}
			    	
			    	
			    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
			    	tenantsView = (PersonView)viewRootMap.get(TENANT_INFO);
			    	if(tenantsView.getPersonId()!=null)
			    		this.setHdnTenantId(tenantsView.getPersonId().toString());
			    	contractView.setTenantView(tenantsView);
			    	requestView.setTenantsView(tenantsView);
			    	
			    	//logger.logInfo(methodName+"|"+" unitRefNum..."+this.getUnitRefNum());
			    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
			    	unitsView.setUnitId(new Long(hdnUnitId));
			    	if(viewRootMap.containsKey(AUCTION_UNIT_ID) && viewRootMap.get(AUCTION_UNIT_ID)!=null )
			    	{
			    		    unitsView.setAuctionUnitId(new Long(viewRootMap.get(AUCTION_UNIT_ID).toString()));
			    	}
                    requestView.setRequestDetailView(getRequestDetailSet(loggedInUser));
                    contractView.setContractUnitView(getContractUnitSet(loggedInUser));
			    	logger.logInfo(methodName+"|"+" contractStartDate..."+contractStartDate);
			        contractView.setStartDate(contractStartDate);
			        contractView.setOriginalStartDate(contractStartDate);
			        logger.logInfo(methodName+"|"+" contractEndDate..."+contractEndDate);
			        contractView.setEndDate(contractEndDate);
			        contractView.setOriginalEndDate(contractEndDate);
			        if(this.getSelectRequestStatus()!=null && !this.getSelectRequestStatus().equals(""))
			        {
			        	//requestView.setStatusId(new Long(selectRequestStatus));
			        	contractView.setStatus(new Long(this.getSelectRequestStatus()));
			        }
			        if(txttotalContractValue!=null && ! txttotalContractValue.equals(""))
			        {
			        	contractView.setRentAmount(new Double(txttotalContractValue));
			        	
			        }
			        //Check for the units type if its commercial than add partners.
			        contractView.setContractPartnerView(getContractParntner());
			        contractView.setContractPersonView(getContractPersons());
			        contractView.setPaymentScheduleView(getPaymentSchedule());
                                  //For time being 
			        
			        contractView.setIsDeleted(new Long(0));
			        contractView.setRecordStatus(new Long(1));
			        contractView.setUpdatedBy(loggedInUser);
			        
			        contractView.setUpdatedOn(new Date());
			        if(pageMode!=null &&( pageMode.equals(PAGE_MODE_ADD)|| pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)))
			        {
			        	contractView.setCreatedOn(new Date());
			            contractView.setCreatedBy(loggedInUser);
			            requestView.setCreatedOn(new Date());
			        }
			        else if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) )  
			        {
			        	if(contractCreatedOn!=null && !contractCreatedOn.equals(""))
			        	contractView.setCreatedOn(dateFormat.parse(contractCreatedOn));
			        	if(contractCreatedBy!=null && !contractCreatedBy.equals(""))
			                contractView.setCreatedBy(contractCreatedBy);
			        
			        }
			        if(getIsContractCommercial())
			        {
			        	contractView.setContractActivitiesView(getCommercialActivities());
			        	
			        }
			        requestView.setUpdatedOn(new Date());
			        requestView.setIsDeleted(new Long(0));
			        requestView.setRecordStatus(new Long(1));
			        requestView.setUpdatedBy(loggedInUser);
			        requestView.setCreatedBy(loggedInUser);
			        logger.logInfo(methodName+"|"+" Set request status...");
			        if(viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
			        {
			        	List<DomainDataView> ddv=(ArrayList) viewRootMap.get(WebConstants.SESSION_REQUEST_STATUS);
			          if(pageMode.equals(PAGE_MODE_ADD))
			          {
			        	HashMap hMap=getIdFromType(ddv, WebConstants.REQUEST_STATUS_NEW);
			          	if(hMap.containsKey("returnId"))
			          	   requestView.setStatusId(new Long(hMap.get("returnId").toString()));
			          }
			          else if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			          {
			        	  HashMap hMap=getIdFromType(ddv, WebConstants.REQUEST_STATUS_NEW);
				          	if(hMap.containsKey("returnId"))
				          	   requestView.setStatusId(new Long(hMap.get("returnId").toString()));
			        	  
				          	RequestTypeView rtv=new RequestTypeView();
				          	rtv.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
				          	requestView.setRequestTypeId(rtv.getRequestTypeId());
				          	
				          	RequestFieldDetailView requestFieldDetailView=new RequestFieldDetailView();
				          	setRequestFieldDetailView(requestFieldDetailView);
							Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>(0);
							requestFieldDetailViewSet.add(requestFieldDetailView);
							
							requestView.setRequestFieldDetailsView(requestFieldDetailViewSet);
				          	List<DomainDataView>ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
				          	DomainDataView domainDataView=CommonUtil.getIdFromType(ddvList, WebConstants.CONTRACT_STATUS_DRAFT);
				          	contractView.setStatus(domainDataView.getDomainDataId());
			          }
			        }
			        contractView.getRequestsView().add(requestView);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Exception Occured...",ex);
    		throw ex;
    		
    	}
    	logger.logInfo(methodName+"|"+" Finish...");
    	return isError;
    }
    private boolean getIsContractValueandRentAmountEqual()
    {
    	String methodName="getIsContractValueandRentAmountEqual";
    	logger.logInfo(methodName+"|"+"Start");
    	boolean isEqual=false;
    	if(txttotalContractValue!=null && txttotalContractValue.length()>0)
    	{
    	  Double contractValue=new Double(txttotalContractValue);	
    		if(contractValue.compareTo(getSumofAllRentAmounts())==0)
    			isEqual=true;
    	}
    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
    	logger.logInfo(methodName+"|"+"Finish");
    	return isEqual;
    }
    
    @SuppressWarnings("unchecked")
    private Double getSumofAllRentAmounts()
    {
    	String methodName="getSumofAllRentAmounts";
    	logger.logInfo(methodName+"|"+"Start");
    	Double rentAmount=new Double(0);
    	List<DomainDataView> paymentStatusList= (ArrayList<DomainDataView> )viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	DomainDataView ddvDraftStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
		DomainDataView ddvPendingStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		DomainDataView ddvCollectedStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		DomainDataView ddvRealizedStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.REALIZED);
		
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				//If payment Schedule is for the rent 
				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&& 
						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 &&
						(paymentScheduleView.getStatusId().compareTo(ddvDraftStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvPendingStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvCollectedStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0 )
						)
							
				
				   rentAmount+=paymentScheduleView.getAmount();
			}
		    
        }
    	logger.logInfo(methodName+"|"+"Finish");
    	return Math.ceil(rentAmount);
    	
    }
    @SuppressWarnings("unchecked")
    private boolean isCommercialActivitySelected()
    {
    	boolean isSelected=false;
    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
    	{
    		List<CommercialActivityView> commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
    		for (CommercialActivityView commercialActivityView : commercialActivityViewList) 
    		{
			  if(commercialActivityView.getIsSelected())
			  {
				  isSelected=true;
				  break;
			  }
			}
    	}
    	
    	return isSelected;
    }
    @SuppressWarnings("unchecked")
    private Set<ContractActivityView>  getCommercialActivities()
    {
    	String methodName ="getCommercialActivities";
    	logger.logInfo(methodName+"|"+"Start...");
    	List<CommercialActivityView> commercialActivityViewList=new ArrayList<CommercialActivityView>(0);
    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
    		commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"); 
    	Set<ContractActivityView> contractActivityViewSet=new HashSet<ContractActivityView>(0);
    	for (CommercialActivityView commercialActivityView : commercialActivityViewList) 
    	{
    		if(commercialActivityView.getIsSelected())
    		{
    		 ContractActivityView contractActivityView=new ContractActivityView();
    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Name::"+commercialActivityView.getCommercialActivityName());
    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Id::"+commercialActivityView.getCommercialActivityId());
    		 contractActivityView.setCommercialActivityId(commercialActivityView.getCommercialActivityId());
    		 contractActivityView.setUpdatedOn(new Date());
    		 contractActivityView.setUpdatedBy(getLoggedInUser());
    		 contractActivityView.setCreatedOn(new Date());
    		 contractActivityView.setCreatedBy(getLoggedInUser());
    		 contractActivityView.setRecordStatus(new Long(1));
    		 contractActivityView.setIsDeleted(new Long(0));
    		 contractActivityViewSet.add(contractActivityView);
    		
    		}
    		
			
		}
    	
    	
    	logger.logInfo(methodName+"|"+"Finish...");
    	return contractActivityViewSet;
    	
    }
    @SuppressWarnings("unchecked")
    public Set<ContractPartnerView> getContractParntner()
    {
    	String methodName="getContractParntner";
    	logger.logInfo(methodName+"|"+"Start");
    		Set<ContractPartnerView> contractPartnerViewSet =new HashSet<ContractPartnerView>(0);
    		if(sessionMap.containsKey(WebConstants.PARTNER))
   	        {
    			List<PersonView> personViewList= (ArrayList)sessionMap.get(WebConstants.PARTNER);
    			for(int i=0;i< personViewList.size();i++)
    			{
    			   PersonView personView =(PersonView)personViewList.get(i);
    			   ContractPartnerView contractPartnerView =new ContractPartnerView();
    			   
    			   //For amend to delete already existing partner
    			   if(personView.getIsDeleted()==null || personView.getIsDeleted().compareTo(new Long(0))==0)
    				   contractPartnerView.setIsDeleted(new Long(0));
    			   else if(personView.getIsDeleted().compareTo(new Long(1))==0)
    			   {
    			     contractPartnerView.setIsDeleted(new Long(1));
    			   }
    			   
    			   contractPartnerView.setPersonView(personView);
    			  contractPartnerViewSet.add(contractPartnerView);
    			}
    			
   	        }
    		logger.logInfo(methodName+"|"+"Finish");
    	return contractPartnerViewSet;
    }
    @SuppressWarnings("unchecked")
    public Set<ContractUnitView> getContractUnitSet(String loggedInUser)
    {
    	String methodName="getContractUnitSet";
    	logger.logInfo(methodName+"|"+"Start");
         Set<ContractUnitView> contractUnitViewSet =new HashSet<ContractUnitView>(0);
        ContractUnitView contractUnitView= new ContractUnitView();
        //Contract Unit Occupier check for the company here...
        contractUnitView.setContractUnitPersons(getContractUnitOccupiers());
        contractUnitView.setUnitView(unitsView);
        
        // COMMENT::Rent Value of the Unit on the basis of calculation
		// logger.logInfo(methodName+"|"+"Units Actual Annual Rent::"+unitRentAmount);
		// Double unitAmount = getUnitAmuntForSelectedDuration();
		// logger.logInfo(methodName+"|"+"Units Rent after calculation::"+unitAmount);
        
        contractUnitView.setRentValue(new Double(unitRentAmount));
	    contractUnitView.setPaidFacilitieses(getPaidFacilitiesSet());
	    Set setContractUnitView =contractView.getContractUnitView();
	    while(setContractUnitView.iterator().hasNext())
	    {
	    	contractUnitView.setContractUnitId(((ContractUnitView)setContractUnitView.iterator().next()).getContractUnitId());
	    	break;
	    }
	    contractUnitViewSet.add(contractUnitView);
	    logger.logInfo(methodName+"|"+"Finish");
    	return contractUnitViewSet;
    }
    @SuppressWarnings("unchecked")
    private void putSelectedFacilitiesInView()
    {
    	String methodName="putSelectedFacilitiesInView";
    	logger.logInfo(methodName+"|"+"Start");
    	if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) || viewRootMap.get("pageMode").equals(PAGE_MODE_UPDATE)   )
    	{
	    	if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES));
	   	       paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);
	   	    if(paidFacilitiesDataList !=null)
	   	    {
	   	   	    logger.logDebug(methodName+"|"+"Facilities List is not null");
	   	    for (PaidFacilitiesView pfv: paidFacilitiesDataList) {
	
	   	    	    if(pfv.getSelected())
	   					addToSelectedFacilities(pfv);
	   				else
	  					removeFromSelectedFacilities(pfv);
	   	
	   	    }
	   	    }
    	}
   	    logger.logInfo(methodName+"|"+"Finish");
    	
    }
    @SuppressWarnings("unchecked")
    private Set<PaidFacilitiesView> getPaidFacilitiesSet()
    {
    	String methodName="getPaidFacilitiesSet";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PaidFacilitiesView> paidFacilitiesViewSet=new HashSet<PaidFacilitiesView>(0);
        putSelectedFacilitiesInView();    	
    	logger.logInfo(methodName+"|"+"Checking for existence of session for paid Facility");
	     if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
	     {
	    	 logger.logInfo(methodName+"|"+"Session for paid Facility exists..");
	    	 List<PaidFacilitiesView> paidFacilitiesViewList=(ArrayList)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
	    	 logger.logInfo(methodName+"|"+"Size of selected paid facilities is.."+paidFacilitiesViewList.size());
	    	 int dateDiffDays=1;
		      dateDiffDays=getDaysDifference(contractStartDate,contractEndDate);	    	
	    	 for (PaidFacilitiesView paidFacilityView : paidFacilitiesViewList) 
		     {
	    	   Double facilityRentValue = getFacilityAmountForDuration(
		            		   dateDiffDays, paidFacilityView);
	    	   paidFacilityView.setRent(facilityRentValue);
	    	   paidFacilityView.setIsMandatory("1");
	    	   paidFacilityView.setDateFrom(contractStartDate);
	    	   paidFacilityView.setDateTo(contractEndDate);
	    	   if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
	    	   {
	    		   logger.logDebug(methodName+"|"+"Page Mode Amend");
	    		   if(paidFacilityView.getPaidFacilitiesId()!=null)
	    			  paidFacilityView.setPaidFacilitiesId(null);
	    		   if(paidFacilityView.getNewFacility())
	    		   {
	    			   logger.logDebug(methodName+"|"+"New Facility so setting date From to today's date");
	    			   
	    			   facilityRentValue = getFacilityAmountForDuration(
	    					   getDaysDifference(new Date(),contractEndDate), paidFacilityView);
	    	            paidFacilityView.setRent(facilityRentValue);
	    			   paidFacilityView.setDateFrom(new Date());
	    		   }
	    		   else if(!paidFacilityView.getNewFacility() && paidFacilityView.getIsDeleted().compareTo(1L)==0)
	    		   {
	    			   logger.logDebug(methodName+"|"+"Old Facility and deleted so setting date To to today's date");
	    			   paidFacilityView.setDateTo(new Date());   
	    		   }
	    	   
	    	   }
	    	   paidFacilitiesViewSet.add(paidFacilityView);
			 }
	    	 
	     }	 
	     logger.logInfo(methodName+"|"+"Finish");
    	return paidFacilitiesViewSet;
    	
    }
    @SuppressWarnings("unchecked")
    private Set<PersonView> getContractUnitOccupiers()
    {
    	String methodName="getContractUnitOccupiers";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PersonView> personViewSet=new HashSet<PersonView>(0);
    	
    	if(sessionMap.containsKey(WebConstants.OCCUPIER))
        {
		    List<PersonView> contractUnitOccupierList= (ArrayList)sessionMap.get(WebConstants.OCCUPIER);
			for(int i=0;i< contractUnitOccupierList.size();i++)
			{
				PersonView personView =(PersonView)contractUnitOccupierList.get(i);
			    personViewSet.add(personView);
			}
		
        }
    	
    	logger.logInfo(methodName+"|"+"Finish");
    	return personViewSet;
    }
    @SuppressWarnings("unchecked")
    private Set<ContractPersonView> getContractPersons()
    {
    	String methodName="getContractUnitPersons";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<ContractPersonView> contractPersonViewSet=new HashSet<ContractPersonView>(0);
    	PersonView personView =new PersonView();
    	ContractPersonView contractPersonView =new ContractPersonView();
    	List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0); 
    	//Sponsor
    	if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
			ddvList=(ArrayList) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);

    	   logger.logInfo(methodName+"|"+"txtSponsor::"+this.getTxtSponsor());
    	   logger.logInfo(methodName+"|"+"SponsorId::"+hdnSponsorId);
    	   
    	   if(hdnSponsorId!= null && hdnSponsorId.trim().length()>0)
    	   {
    	    String[] spnsrInfo = getPersonNameIdFromHidden(hdnSponsorId);
    	     personView.setPersonId(new Long(spnsrInfo[0]));
    	     contractPersonView.setPersonView(personView);
    	     DomainDataView ddvSponsor = CommonUtil.getIdFromType(ddvList,WebConstants.SPONSOR);
    	     contractPersonView.setPersonTypeId(ddvSponsor.getDomainDataId());
    	     contractPersonViewSet.add(contractPersonView);
    	     
    	   }
    	   //Manager
    	   logger.logInfo(methodName+"|"+"txtManager::"+this.getTxtManager());
    	   logger.logInfo(methodName+"|"+"ManagerId::"+hdnManagerId);
           if(hdnManagerId!= null && hdnManagerId.trim().length()>0)
           {
              String[] mgrInfo = getPersonNameIdFromHidden(hdnManagerId);
    	    
        	   contractPersonView=new ContractPersonView();
        	   personView =new PersonView();
 	           personView.setPersonId(new Long(mgrInfo[0]));
 	           contractPersonView.setPersonView(personView);
 	          DomainDataView ddvManager = CommonUtil.getIdFromType(ddvList,WebConstants.MANAGER);
 	          contractPersonView.setPersonTypeId(ddvManager.getDomainDataId());
 	          contractPersonViewSet.add(contractPersonView);
           }
           logger.logInfo(methodName+"|"+"Finish");
    	return contractPersonViewSet;
    }
    @SuppressWarnings("unchecked")
    private Set<PaymentScheduleView> getPaymentSchedule()
    {
    	String methodName="getPaymentSchedule";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
    	
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				paymentScheduleView.setOwnerShipTypeId(
						new Long (viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString())); 
				
				paymentScheduleViewSet.add(paymentScheduleView);
				
			    
			}
		    
        }
    	
    	logger.logInfo(methodName+"|"+"Finish");
    	return paymentScheduleViewSet;
    }
        public Set<RequestDetailView> getRequestDetailSet(String loggedInUser)
    {
    	  Set<RequestDetailView> requestDetailSet=new HashSet<RequestDetailView>();
    	   requestDetailView.setUnitView(unitsView);
		    
		
		requestDetailSet.add(requestDetailView);
		return requestDetailSet;
    	
    	
    }
        @SuppressWarnings("unchecked")
    private void putViewValuesInControl(String contractId) throws PimsBusinessException,Exception
    {
    	String methodName="putViewValuesInControl";
    	
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());//format!=null && format.length()>0?format:"dd/MM/yyyy");
    	logger.logInfo(methodName+"|"+" Start...");
    	
    	logger.logInfo(methodName+"|"+" Reference Number:"+contractView.getContractNumber());
    	this.setRefNum(contractView.getContractNumber());
    	
    	
    	logger.logInfo(methodName+"|"+" ContractType:"+contractView.getContractTypeId());
    	if(contractView.getContractTypeId()!=null)
    		this.setSelectOneContractType(contractView.getContractTypeId().toString());
    	
    	
    	logger.logInfo(methodName+"|"+" StatusId:"+contractView.getStatus());
    	if(contractView.getStatus()!=null)
      	  this.setSelectRequestStatus(contractView.getStatus().toString());
    	logger.logInfo(methodName+"|"+" Issue Date:"+contractView.getIssueDate());
    	if(contractView.getIssueDate()!=null)
        	  this.setContractIssueDate(contractView.getIssueDate());
    	
    	if(contractView.getTenantView()!=null)
    	{
    	  	
	    	  tenantsView=contractView.getTenantView();
	      	  this.setHdnTenantId(tenantsView.getPersonId().toString());
	      	  hdntenanttypeId=tenantsView.getIsCompany().toString();
	      	  
	      	  String tenantNames="";
	          if(tenantsView.getPersonFullName()!=null && tenantsView.getPersonFullName().length()>0)
	             tenantNames=tenantsView.getPersonFullName();
	          if(tenantNames.trim().length()<=0)
	         	 tenantNames=tenantsView.getCompanyName();
	          if(tenantNames.trim().length()<=0)
	        	  tenantNames=tenantsView.getGovtDepartment();
	    	  txtTenantName=tenantNames;
	    	  txtTenantType=getTenantType(tenantsView);
	    	  viewRootMap.put(TENANT_INFO, tenantsView);
    	}
    	
    	logger.logInfo(methodName+"|"+"Checking for nullability of contract Unit ");
    	if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
    	{
    	  logger.logInfo(methodName+"|"+"Getting iterator for contract View ");
    	  Iterator iter=contractView.getContractUnitView().iterator();
    	  while(iter.hasNext())
    	  {
    		  
    		  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
    		  
    		  //Reason:: Because when contract was created the rent amount of the unit might gets changed due
    		  //to the fact that contract might come from Auction(which has auction price) or because
    		  //of duration of contract
    		  //and when its viewed later the rent value  of the unit in unit table is different than that of 
    		  //rent value of unit in contract unit.
    		  contractUnitView.getUnitView().setRentValue(contractUnitView.getRentValue());
    		  viewRootMap.put(UNIT_DATA_ITEM, contractUnitView.getUnitView());
    		  viewRootMap.put("UNITINFO",contractUnitView.getUnitView());
    		  viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,contractUnitView.getUnitView().getPropertyCategoryId());
    		  if(contractUnitView.getUnitView().getAuctionUnitId()!=null)
    		  {
    		   getDepositAmountForBidder(null, null, contractView.getContractId());
    		   viewRootMap.put(AUCTION_UNIT_ID,contractUnitView.getUnitView().getAuctionUnitId());
    		  }
              logger.logInfo(methodName+"|"+"Unit Ref Number:"+contractUnitView.getUnitView().getUnitNumber());
    		 this.setUnitRefNum(contractUnitView.getUnitView().getUnitNumber()); 
      	     logger.logInfo(methodName+"|"+"Unit Id:"+contractUnitView.getUnitView().getUnitId().toString());
      	       hdnUnitId=contractUnitView.getUnitView().getUnitId().toString();
      	     logger.logInfo(methodName+"|"+"Unit Usuage Type Id:"+contractUnitView.getUnitView().getUsageTypeId().toString());
    	       hdnUnitUsuageTypeId=contractUnitView.getUnitView().getUsageTypeId().toString();
    	     logger.logInfo(methodName+"|"+"Contract Unit Rent Value:"+contractUnitView.getRentValue().toString());
    	       unitRentAmount=contractUnitView.getRentValue().toString();
    	     logger.logInfo(methodName+"|"+"PropertyCommercialName:"+contractUnitView.getUnitView().getPropertyCommercialName());
    	       propertyCommercialName=contractUnitView.getUnitView().getPropertyCommercialName();
    	     logger.logInfo(methodName+"|"+"Unit Usuage Type :"+contractUnitView.getUnitView().getUsageTypeEn());
    	       txtUnitUsuageType=(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
    	     logger.logInfo(methodName+"|"+"Property Address:"+contractUnitView.getUnitView().getUnitAddress());
    	       propertyAddress=contractUnitView.getUnitView().getUnitAddress();
    	  }
    	}
    	logger.logInfo(methodName+"|"+" size of ContractPerson :"+contractView.getContractPersonView().size());
    	if(contractView.getContractPersonView().size()>0)
    		getContractPersons(contractView.getContractId());
    	        
    	logger.logInfo(methodName+"|"+"ContractDate:"+contractView.getContractDateString());
    	if(contractView.getContractDateString()!=null && !contractView.getContractDateString().equals(""))
    		this.setDatetimerequest(contractView.getContractDateString());
    	logger.logInfo(methodName+"|"+"CreatedOn:"+contractView.getCreatedOn());
    	if(contractView.getCreatedOn()!=null && !contractView.getCreatedOn().equals(""))
    		contractCreatedOn=dateFormat.format(contractView.getCreatedOn());
    	logger.logInfo(methodName+"|"+"CreatedBy:"+contractView.getCreatedBy());
    	if(contractView.getCreatedBy()!=null && !contractView.getCreatedBy().equals(""))
    		contractCreatedBy=contractView.getCreatedBy();
    	logger.logInfo(methodName+"|"+"Contract Start Date:"+contractView.getStartDateString());
    	if(contractView.getStartDateString()!=null && !contractView.getStartDateString().equals(""))
    	{
    		
    		contractStartDate=dateFormat.parse(contractView.getStartDateString());
    		viewRootMap.put("CONTRACT_START_DATE",contractStartDate);
    	}
    	logger.logInfo(methodName+"|"+"Contract End Date:"+contractView.getEndDateString());
    	if(contractView.getEndDateString()!=null && !contractView.getEndDateString().equals(""))
    	{
    		contractEndDate=dateFormat.parse(contractView.getEndDateString());
    		viewRootMap.put("CONTRACT_END_DATE",contractEndDate);
    	}
    	logger.logInfo(methodName+"|"+"Contract Rent Amount:"+contractView.getRentAmount());
    	if(contractView.getRentAmount()!=null)
    		txttotalContractValue=contractView.getRentAmount().toString();
    	getOccupiers(contractId);
    	getPartners(contractId);
    	
		
    	if(requestView !=null && requestView.getRequestId()!=null)
    	{ 
    		loadAttachmentsAndComments(requestView.getRequestId());
    		getPaymentScheduleFromRequest(requestView.getRequestId().toString());
    	}
    	
    	logger.logInfo(methodName+"|"+" Finish..");
    }
        @SuppressWarnings("unchecked")
    private void getContractPersons(Long contractId) throws PimsBusinessException
    {
    	String methodName ="getContractPersons(Long contractId)";
    	logger.logInfo(methodName+"|"+"Start...");
    	String sponsorTypeId="";
    	String managerTypeId="";
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<ContractPersonView> contractPersonViewList =  psa.getContractPersons(contractId);
        for (ContractPersonView contractPersonView : contractPersonViewList) {
        	
        	PersonView personView = contractPersonView.getPersonView();
    		if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
    		{
    			List<DomainDataView> ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);
    			HashMap hMap=new HashMap(0);
    		    hMap= getIdFromType(ddvList,WebConstants.SPONSOR);
    		    if(hMap.containsKey("returnId"))
    		    	sponsorTypeId= hMap.get("returnId").toString();
    		    	hMap.clear();
    		    	hMap= getIdFromType(ddvList,WebConstants.MANAGER);
    		    	if(hMap.containsKey("returnId"))
        		    	managerTypeId=hMap.get("returnId").toString();

    		}
    		
    		if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(sponsorTypeId))
    		{
    			this.setTxtSponsor(personView.getPersonFullName());
    		     hdnSponsorId=personView.getPersonId().toString()+":"+personView.getPersonFullName();	
    		}
    		else if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(managerTypeId))
    		{
    			this.setTxtManager(personView.getPersonFullName());
    		     hdnManagerId=personView.getPersonId().toString()+":"+personView.getPersonFullName();	
    		}	
		}
    	logger.logInfo(methodName+"|"+"Finish");
    	
    }
    @SuppressWarnings("unchecked")
    private void getOccupiers(String contractId) throws PimsBusinessException
    {
    	String methodName ="getOccupiers";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<PersonView> personViewList = psa.getContractUnitOccupiers(new Long(contractId));
    	
        sessionMap.put(WebConstants.OCCUPIER, personViewList);
    	
    	logger.logInfo(methodName+"|"+"Finish");
    	
    }
    @SuppressWarnings("unchecked")
    private void getPartners(String contractId) throws PimsBusinessException
    {
    	String methodName ="getPartners";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<PersonView> personViewList = psa.getContractPartners(new Long(contractId));
    	
        sessionMap.put(WebConstants.PARTNER, personViewList);
    	
    	logger.logInfo(methodName+"|"+"Finish");
    	
    }
    @SuppressWarnings("unchecked")
    private void getPaymentScheduleFromRequest(String entityId) throws PimsBusinessException
	{
		
    	String methodName ="getContractPaymentSchedule";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
    	List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(null,new Long(entityId));
        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);
    	logger.logInfo(methodName+"|"+"Finish...");
	}
    
 
    public void valueChangeListerner_RentValue()//ValueChangeEvent e)
    {
    	
    	String methodName="valueChangeListerner_RentValue";
    	logger.logInfo(methodName+"|"+"Start..");
    	try
    	{
    		//only add when there is some unit and contract start and end date are not empty
    		if( viewRootMap.containsKey(UNIT_DATA_ITEM) )
		    	CalculateTotalRentValue();
				
    	}
    	catch(Exception ex)
    	{
    		logger.logError(methodName+"|"+"Error Occured.."+ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
   
   
    	
    }
    
    public static int getDaysDifference( Date fromDate, Date toDate )
    {
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();

        calFirst.setTime( fromDate );
        calSecond.setTime( toDate );
        // int dayFirst = calFirst.get(calFirst.DAY_OF_YEAR);
        // int daySecond = calSecond.get(calSecond.DAY_OF_MONTH);

        return getDaysBetween( calFirst, calSecond );
    }

    public static int getDaysBetween( Calendar d1, Calendar d2 )
    {
        if ( d1.after( d2 ) )
        { // swap dates so that d1 is start and d2 is end
            Calendar swap = d1;
            d1 = d2;
            d2 = swap;
        }
        int days = d2.get( Calendar.DAY_OF_YEAR ) - d1.get( Calendar.DAY_OF_YEAR );
        int y2 = d2.get( Calendar.YEAR );
        if ( d1.get( Calendar.YEAR ) != y2 )
        {
            d1 = (Calendar) d1.clone();
            do
            {
                days += d1.getActualMaximum( Calendar.DAY_OF_YEAR );
                d1.add( Calendar.YEAR, 1 );
            } while ( d1.get( Calendar.YEAR ) != y2 );
        }
        return days;
    }
    @SuppressWarnings("unchecked")
    public void CalculateTotalRentValue()
    {
    	
    	String methodName="CalculateTotalRentValue";
    	logger.logInfo(methodName+"|"+"Start..");
    	try
    	{
    		totalContractValue=new Double(0);
    		Double unitAmount=new Double(0);
    		if(contractStartDate!=null && !contractStartDate.equals("") && contractEndDate!=null && !contractEndDate.equals(""))
		    {       
    			int dateDiffDays=1;
		        dateDiffDays=getDaysDifference(contractStartDate,contractEndDate);

    			   if(unitRentAmount!=null && unitRentAmount.trim().length()>0)	
		        	{
    				  unitAmount=new Double(unitRentAmount);
		        	  totalContractValue=totalContractValue+(unitAmount);  
		        	  logger.logInfo(methodName+"|"+"totalContractValue:"+totalContractValue);
		        	}
    			    calculateFacilityAmount(dateDiffDays);
			        logger.logInfo(methodName+"|"+"Applying Ceiling to Total Contract Value:"+totalContractValue);
			        totalContractValue=Math.ceil(totalContractValue);
			        txttotalContractValue=totalContractValue.toString();
		       }
		    logger.logInfo(methodName+"|"+"Total Contract Value Calculated::"+totalContractValue);
		    logger.logInfo(methodName+"|"+"Finish..");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	
    	
    }

    @SuppressWarnings("unchecked")
	private void calculateFacilityAmount( int dateDiffDays) {
		
		String methodName="calculateFacilityAmount";
		List<PaidFacilitiesView> paidFacilitiesViewList;
		putSelectedFacilitiesInView();
		if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)&& 
			viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null  )		
		{		     
			paidFacilitiesViewList =(ArrayList)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
		    logger.logInfo(methodName+"|"+"Size of selected paid Facilities list:"+paidFacilitiesViewList.size() );
		
		    for (PaidFacilitiesView paidFacilityView : paidFacilitiesViewList) 
		    {
		           Double  facilityRentValue = getFacilityAmountForDuration(dateDiffDays, paidFacilityView);
		           logger.logInfo(methodName+"|"+"totalContractValue+ facilityRentValue:"+totalContractValue+"+"+ facilityRentValue);
					totalContractValue=totalContractValue+ facilityRentValue;
					logger.logInfo(methodName+"|"+"totalContractValue:"+totalContractValue);
			}
		}
	}


	private Double getFacilityAmountForDuration(int dateDiffDays,PaidFacilitiesView paidFacilityView) 
	{
		String methodName="getFacilityAmountForDuration";
		logger.logInfo(methodName+"|"+"Start:");
		Double dailyFacilityRentValue =new Double(0);
        Double facilityRentValue=new Double(0);
        
        logger.logDebug(methodName+"|"+"Paid Facility ID:"+paidFacilityView.getPaidFacilitiesId() );
        logger.logDebug(methodName+"|"+"Paid Facility Name:"+paidFacilityView.getFacilityName() );
        logger.logDebug(methodName+"|"+"Paid Facility Rent:"+paidFacilityView.getRent() );
        logger.logDebug(methodName+"|"+"Paid Facility From Date:"+paidFacilityView.getDateFrom() );
        logger.logDebug(methodName+"|"+"Paid Facility To Date:"+paidFacilityView.getDateTo());
        if(pageMode.equals(PAGE_MODE_ADD) && paidFacilityView.getRent()!=null)
        {
        	Double daysInYear= new Double(viewRootMap.get(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR).toString());
			logger.logInfo(methodName+"|"+"Calculating facilities daily rent amount by formula:Facility Rent / days in year from sytem config::::: "+paidFacilityView.getRent()+"/ "+daysInYear);
			 dailyFacilityRentValue=paidFacilityView.getRent()/daysInYear;
			logger.logInfo(methodName+"|"+"Value of facilities daily rent amount:: "+dailyFacilityRentValue);
			  //dailyFacilityRentValue=new Double(paidFacilityView.getRent())/365;
			  logger.logInfo(methodName+"|"+"Formula for calculating facility rent :Facility Daily Rent * Diff of Contract Start and End Date:: "+dailyFacilityRentValue+" * "+dateDiffDays);
			  facilityRentValue=dailyFacilityRentValue*dateDiffDays;
			  
			  logger.logInfo(methodName+"|"+"Facility rent "+facilityRentValue);
        }
        else if(paidFacilityView.getRent()!=null)
        {
        	facilityRentValue=new Double(paidFacilityView.getRent());
        }
        logger.logInfo(methodName+"|"+"Finish:");
		return facilityRentValue;
	}

	

    
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
  
    
    @SuppressWarnings("unchecked")
	private void getPaymentScheduleListFromSession()
	{
		
		int rowId = 0;
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) 
			    && viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
		  )
		{
			 //paymentScheduleDataList =(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			List<PaymentScheduleView> tempPaymentScheduleDataList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			paymentScheduleDataList.clear();
			for (PaymentScheduleView paymentScheduleView : tempPaymentScheduleDataList) {
    			//IF DURING AMEND THE ALREADY EXISTED ID DELETED
				if(paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
				{
				  paymentScheduleView.setRowId(rowId);
				  rowId = rowId + 1 ;
				  paymentScheduleDataList.add(paymentScheduleView);
			    }
		}
		}
	}
	
	public List<PaymentScheduleView> getPaymentScheduleDataList() 
	{
		    getPaymentScheduleListFromSession(); 
			return paymentScheduleDataList;
	}
 
	@SuppressWarnings("unchecked")
	public List<PaidFacilitiesView> getPaidFacilitiesDataList()
	{
		    	if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES));
	        	 paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);
	

		return paidFacilitiesDataList;
	}
	@SuppressWarnings("unchecked")
	public List<PersonView> getProspectiveOccupierDataList()
	{
		

		if (sessionMap.containsKey(WebConstants.OCCUPIER))
		{
			prospectiveOccupierDataList = new ArrayList<PersonView>(0);
			List<PersonView> tempOccupierDataList  = (ArrayList)sessionMap.get(WebConstants.OCCUPIER); 
			  for (PersonView personView : tempOccupierDataList  ) {
				if(personView.getIsDeleted().compareTo(new Long(1))!=0)
					prospectiveOccupierDataList.add(personView);
			  }
		}
		return prospectiveOccupierDataList ;
	}
	@SuppressWarnings("unchecked")
	private boolean isUnitValid()
	{
		String method="isUnitValid";
		boolean isUnitValid=false;
		logger.logInfo(method+"|"+"Start..");
		List<DomainDataView> ddl= getDomainDataListForDomainType(WebConstants.UNIT_STATUS);
		String[] validUnitStautses=WebConstants.VALID_UNITS_STATUSES.split(",");
		//For update mode this criteria should not be checked as 
		//unit would be in rented state after approval  

		if(pageMode.equals(PAGE_MODE_ADD))
		{
			for(int j=0;j<validUnitStautses.length;j++)
			{
				HashMap unitStatusmap=getIdFromType(ddl, validUnitStautses[j]);
				if(this.getUnitStatusId().equals(unitStatusmap.get("returnId").toString()))
				{
					isUnitValid=true;
					break;
				}
				
				
			}
		}
		//if page mode is other than add specifically entered for PAGE_MODE_AMEND
		else
			isUnitValid=true;
		
		logger.logInfo(method+"|"+"Finish..");
		return isUnitValid;
	}
	@SuppressWarnings("unchecked")
	public void tabPaymentTerms_Click()
	{
		String methodName="tabPaymentTerms_Click";
		logger.logInfo(methodName+"|Start");
		valueChangeListerner_RentValue();
		calculatePaymentScheduleSummaray();
		logger.logInfo(methodName+"|Finish");
		
		
	}
	public String getRentId()
	{
		return WebConstants.PAYMENT_TYPE_RENT_ID.toString();
		
	}
	@SuppressWarnings("unchecked")	
	private void CollectPayments()
	{
		String methodName = "CollectPayments";
		logger.logInfo(methodName+"|Start");
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		try
		{
			 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     tenantsView = (PersonView)viewRootMap.get(TENANT_INFO);
		     prv.setPaidById(tenantsView.getPersonId());
		     prv = psa.collectPayments(prv);
		     getPaymentScheduleFromRequest(contractView.getContractId().toString());
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PAYMENT_COLLECTED);
		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.AMEND_CONTRACT);
		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
		     logger.logInfo(methodName + "|" + "Finish...");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + "|" + "Exception Occured...",ex);
			if(sessionMap.containsKey(WebConstants.Attachment.ATTACHED_CHEQUE_IDS) && sessionMap.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS)!=null)
				 sessionMap.remove(WebConstants.Attachment.ATTACHED_CHEQUE_IDS);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	private boolean isAllPaymentsCollected()
	{
		boolean isAllPaymentsCollected = true;
		DomainDataView ddvPending = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
				                       WebConstants.PAYMENT_SCHEDULE_PENDING);
		List<PaymentScheduleView> psvList = getPaymentScheduleDataList();
		for (PaymentScheduleView paymentScheduleView : psvList) 
		{
			if(paymentScheduleView.getStatusId().compareTo(ddvPending.getDomainDataId())==0)
				return false;
			
		}
		
	   return isAllPaymentsCollected;
	}
	@SuppressWarnings("unchecked")
	public void calculatePaymentScheduleSummaray()
	{
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{
			
			List<PaymentScheduleView> paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		Long cashId = ddvCash.getDomainDataId();
		DomainDataView ddvCheque = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
		Long chqId = ddvCheque.getDomainDataId();
		List<DomainDataView> paymentStatusList= (ArrayList<DomainDataView> )viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
		DomainDataView ddvDraftStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
		DomainDataView ddvPendingStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		DomainDataView ddvCollectedStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		DomainDataView ddvRealizedStatus =CommonUtil.getIdFromType(paymentStatusList, WebConstants.REALIZED);
		this.setTotalCashAmount(0) ;
		this.setTotalChqAmount(0) ;
		this.setTotalRentAmount(0) ;
		this.setTotalDepositAmount(0) ;
		this.setTotalFines(0) ;
		this.setTotalFees(0);
		this.setInstallments(0) ;
		
		for (int i=0; i<paymentSchedules.size(); i++)
		{
			//Checking Type
			if (paymentSchedules.get(i).getIsDeleted().compareTo(new Long(0))==0)
			{
				if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue() &&
					(paymentSchedules.get(i).getStatusId().compareTo(ddvDraftStatus.getDomainDataId())==0 ||
					 paymentSchedules.get(i).getStatusId().compareTo(ddvPendingStatus.getDomainDataId())==0 ||
					 paymentSchedules.get(i).getStatusId().compareTo(ddvCollectedStatus.getDomainDataId())==0 ||
					 paymentSchedules.get(i).getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0 )
				   )
				{
					installments++;
					this.setTotalRentAmount(this.getTotalRentAmount()  + paymentSchedules.get(i).getAmount());
				}
				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue())
				{
					this.setTotalDepositAmount(this.getTotalDepositAmount() + paymentSchedules.get(i).getAmount());
				}
				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue())
				{
					this.setTotalFees(this.getTotalFees() + paymentSchedules.get(i).getAmount());
				}
				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue())
				{
					this.setTotalFines( this.getTotalFines()  + paymentSchedules.get(i).getAmount());
				}
				
				//Checking Mode
				if (paymentSchedules.get(i).getPaymentModeId() != null)
				{
						if (paymentSchedules.get(i).getPaymentModeId().longValue() == cashId.longValue())
						{
							this.setTotalCashAmount(this.getTotalCashAmount()  +  paymentSchedules.get(i).getAmount());
						}
						else if (paymentSchedules.get(i).getPaymentModeId().longValue() == chqId.longValue())
						{
							this.setTotalChqAmount( this.getTotalChqAmount() + paymentSchedules.get(i).getAmount());
						}
					}
			}
		  }
		}
	}
	
	@SuppressWarnings("unchecked")
	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logDebug(methodName+"|"+"Start...");
		logger.logDebug(methodName+"|"+"DomainDataType To search..."+type);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logDebug(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	logger.logDebug(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}
			
		}
		logger.logDebug(methodName+"|"+"Finish...");
		return hMap;
		
	}
	public HtmlDataTable getPropspectivepartnerDataTable() {
		return propspectivepartnerDataTable;
	}
	@SuppressWarnings("unchecked")
	public void tabAttachmentsComments_Click()
	{
		
		String methodName="tabAttachmentsComments_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
			if( viewRootMap.containsKey("REQUESTVIEWUPDATEMODE") && viewRootMap.get("REQUESTVIEWUPDATEMODE")!=null)
			requestView  = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
			if(viewRootMap.containsKey(REQUEST_FOR_AMEND) && viewRootMap.get(REQUEST_FOR_AMEND)!=null && 
					requestView ==null || requestView.getRequestId()==null )
			requestView  = (RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
			if(requestView !=null && requestView.getRequestId()!=null)
	    	     loadAttachmentsAndComments(requestView.getRequestId());
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
		  
	}
	@SuppressWarnings("unchecked")
	public void tabCommercialActivity_Click()
	{
		String methodName="tabCommercialActivity_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
					if(!viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
				   CommercialActivityController.getAllCommercialActivities(contractId);
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
	}
	@SuppressWarnings("unchecked")
    public void chkPaidFacilities_Changed()
    {
    	String methodName="chkPaidFacilities_Changed";
		logger.logInfo(methodName+"|"+"Start...");
		PaidFacilitiesView pfv=(PaidFacilitiesView)paidFacilitiesDataTable.getRowData();
		logger.logDebug(methodName+"|"+"Facility Name..."+pfv.getFacilityName());
		logger.logDebug(methodName+"|"+"Facility ID..."+pfv.getFacilityId());
		
		if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			processPaidFacilitiesForAmend(pfv);
		
		logger.logInfo(methodName+"|"+"Finish...");
    }
    @SuppressWarnings("unchecked")
    private void processPaidFacilitiesForAmend(PaidFacilitiesView pfv)
    {
    	String methodName="processPaidFacilitiesForAmend";
    	logger.logInfo(methodName+"|"+"Start...");
    	int dateDiffDays=1;
    	//if facility has been selected
    	
    	if(pfv.getSelected())
    	{
    		//if this facility is being added for the first time in contract.
    		if(pfv.getNewFacility())
    			//Since this facility is being added from Now, so its rent amount should be calculated from Now to ContractEndDate
		        addToSelectedFacilities(pfv);

    		
    		//if  this facility was binded with contract before amendment
    		//i.e. this facility was selected before this amendment,
    		//then unselected during this process and now again has been selected
    		else
    		{
    			pfv.setRent(pfv.getOriginalRent());
    			pfv.setIsDeleted(new Long(0));
    			
    		}
    	}
    	//if facility has been marked for deletion
    	else
    	{
    		//if this facility was binded with contract before amendment
    		if(!pfv.getNewFacility())
    		{
	    		pfv.setIsDeleted(new Long(1));
	    		//Since this facility is being deleted from Now so its rent amount should be calculated
				//from ContractStartDate to Now
		    
	    		  dateDiffDays=getDaysDifference(contractStartDate,new Date());
	    		  logger.logDebug(methodName+"|"+"Facility's Original Rent..."+pfv.getOriginalRent());
	    		  //Setting rent of facility back to the original rent 
	    		  //bcz current rent in paidFacility was set according to duration selected in at time of contract creation
	    		  pfv.setRent(pfv.getOriginalRent());
			        Double  facilityRentValue = getFacilityAmountForDuration(dateDiffDays, pfv);
		            pfv.setRent(facilityRentValue);
    		}
    		//if this facility was not binded with contract before amendment and selected during the
    		//the process and again unselected,so removing it from ViewRootMap
    		if(pfv.getNewFacility())
    		{
    			pfv.setRent(pfv.getOriginalRent());
    			removeFromSelectedFacilities(pfv);
    		}
  			
    	}
    	logger.logInfo(methodName+"|"+"Finish...");
    }
    @SuppressWarnings("unchecked")
    private void addToSelectedFacilities(PaidFacilitiesView pfv)
    {
    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
      	if(viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null)
    	    paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
      	if(!paidFacilitiesViewList.contains(pfv))
       	    paidFacilitiesViewList.add(pfv);
      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,paidFacilitiesViewList);
    
    }
    @SuppressWarnings("unchecked")
    private void removeFromSelectedFacilities(PaidFacilitiesView pfv)
    {
    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
    	List<PaidFacilitiesView> newFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
        for (PaidFacilitiesView paidFacilitiesView : paidFacilitiesViewList) 
        {
        	if(pfv.getFacilityId().compareTo(paidFacilitiesView.getFacilityId())!=0)
		       newFacilitiesViewList.add(paidFacilitiesView);
        }
      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,newFacilitiesViewList);
    
    }
    @SuppressWarnings("unchecked")
	public void tabPaidFacility_Click()
	{
		String methodName="tabPaidFacility_Click";
		logger.logInfo(methodName+"|"+"Start...");
		List<PaidFacilitiesView> facilityViewList=new ArrayList<PaidFacilitiesView>(0);
		if(errorMessages==null)
		 errorMessages=new ArrayList<String>(0);
        try
		{
					if(!viewRootMap.containsKey(WebConstants.PAID_FACILITIES))
					{
						facilityViewList=	 getAllPaidFacilities();
						for (PaidFacilitiesView paidFacilitiesView : facilityViewList) {
							if(paidFacilitiesView.getSelected())
								addToSelectedFacilities(paidFacilitiesView);
							
						}
		                viewRootMap.put(WebConstants.PAID_FACILITIES, facilityViewList);
					}
		}
		catch (Exception ex)
		{
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
		
	}
	@SuppressWarnings("unchecked")
	public void tabRequestHistory_Click()
	{
		
		String methodName="tabRequestHistory_Click";
		logger.logInfo(methodName+"|"+"Start...");
		
		try
		{
			errorMessages=new ArrayList<String>(0);
	    	
	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void btnContractHistory_Click()
	{
		
		String methodName="tabContractHistory_Click";
		logger.logInfo(methodName+"|"+"Start...");
		
		try
		{
			errorMessages=new ArrayList<String>(0);
	    	String javaScriptText ="var screen_width = 1024;"+
	        "var screen_height = 450;"+
	        "window.open('ContractHistoryPopUp.jsf?contractId="+contractId+
	        "','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=40,scrollbars=yes,status=yes');";
                
	        openPopUp("ContractHistoryPopUp.jsf?contractId="+contractId+"'", javaScriptText);

	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.CONTRACT;
    		logger.logInfo(methodName + "contractId..."+contractId);
    		if(contractId!=null && contractId.trim().length()>0)
    		{
    		  Long contractsId = new Long(contractId);
    		  logger.logInfo(methodName + "notesOwner..."+notesOwner);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, contractsId );
	    	logger.logInfo(methodName + "completed successfully!!!");
    		}
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	@SuppressWarnings("unchecked")
	public void tabAuditTrail_Click()
    {
    	String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(contractId!=null && contractId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }
	@SuppressWarnings("unchecked")
	public void tabBasicInfo_Click()
	{
		String methodName="tabBasicInfo_Click";
		logger.logInfo(methodName+"|"+"Start...");
		
		try
		{
			errorMessages=new ArrayList<String>(0);
			valueChangeListerner_RentValue();
	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}

		
		
	}
	
	@SuppressWarnings("unchecked")
	
	private  List<PaidFacilitiesView> getAllPaidFacilities() throws PimsBusinessException
    {
    	String methodName="getAllPaidFacilities";
		logger.logInfo(methodName+"|"+"Start...");
        PropertyServiceAgent psa=new PropertyServiceAgent();
        Long conId=contractId!=null && contractId.length()>0? new Long(contractId): null;
        Long propertyId=new Long(0);
        if(viewRootMap.containsKey(UNIT_DATA_ITEM))
		{
		 UnitView unitView=(UnitView)viewRootMap.get(UNIT_DATA_ITEM);
		 propertyId=unitView.getPropertyId();
		}
        logger.logInfo(methodName+"|"+"PropertyId..."+propertyId);
        List<PaidFacilitiesView> facilityViewList =psa.getPropertyFacilitiesForContractByPropertyId(propertyId,conId);
        logger.logInfo(methodName+"|"+"Finish...");
    	return facilityViewList;
        
    }
	
    
    
	
	public void setPropspectivepartnerDataTable(
			HtmlDataTable propspectivepartnerDataTable) {
		this.propspectivepartnerDataTable = propspectivepartnerDataTable;
	}

	
	@SuppressWarnings("unchecked")
	public List<PersonView> getProspectivepartnerDataList() {
 
		if (sessionMap.containsKey(WebConstants.PARTNER))
		{
		    prospectivepartnerDataList=new ArrayList<PersonView>(0);
			List<PersonView> tempPartnerDataList  = (ArrayList<PersonView>)sessionMap.get(WebConstants.PARTNER); 
		  for (PersonView personView : tempPartnerDataList ) {
			if(personView.getIsDeleted().compareTo(new Long(1))!=0)
			prospectivepartnerDataList.add(personView);
		}
		}
		
		return prospectivepartnerDataList;
	}


	public void setProspectivepartnerDataList(
			List<PersonView> prospectivepartnerDataList) {
		this.prospectivepartnerDataList = prospectivepartnerDataList;
	}


	public PersonView getProspectivepartnerDataItem() {
		return prospectivepartnerDataItem;
	}

   
	public void setProspectivepartnerDataItem(PersonView prospectivepartnerDataItem) {
		this.prospectivepartnerDataItem = prospectivepartnerDataItem;
	}
 
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	
	public String openUnitsPopUp()
	{
		String methodName="openUnitsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        if(viewRootMap.containsKey("AUCTIONUNITINFO"))
        	viewRootMap.remove("AUCTIONUNITINFO");
        auctionRefNum="";
        hdnAuctionunitId="";
        String javaScriptText ="var screen_width = 1024;"+
        "var screen_height = 450;"+
        "var popup_width = screen_width-200;"+
        "var popup_height = screen_height;"+
        "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"+
        "window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=NewLeaseContract','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";


        openPopUp("UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=NewLeaseContract", javaScriptText);
          
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings("unchecked")
	public String openAuctionUnitsPopUp()
	{
		String methodName="openAuctionUnitsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        String javaScriptText ="var screen_width = 1024;"+
        "var screen_height = 500;"+
        "window.open('AllWonAuctions.jsf','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');";

        if(viewRootMap.containsKey("UNITINFO"))
        	viewRootMap.remove("UNITINFO");
        
        openPopUp("AllWonAuctions.jsf", javaScriptText);
        
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

   
	public String getRefNum() {
		if(viewRootMap.containsKey("refNum") && viewRootMap.get("refNum")!=null)
			refNum = viewRootMap.get("refNum").toString();
		return refNum;
	}

	@SuppressWarnings("unchecked")
	public void setRefNum(String refNum) {
		this.refNum = refNum;
		if(this.refNum!=null)
			 viewRootMap.put("refNum",this.refNum);
			
	}


	public String getDatetimerequest() {
       
		if(viewRootMap.containsKey("datetimerequest") && viewRootMap.get("datetimerequest")!=null)
			datetimerequest = viewRootMap.get("datetimerequest").toString();
		return datetimerequest;
	}

	@SuppressWarnings("unchecked")
	public void setDatetimerequest(String datetimerequest) {
		this.datetimerequest = datetimerequest;
		if(this.datetimerequest!=null)
			viewRootMap.put("datetimerequest",this.datetimerequest);
			
	}

	public String getTenantRefNum() {
		if(viewRootMap.containsKey("tenantRefNum") && viewRootMap.get("tenantRefNum")!=null)
			tenantRefNum = viewRootMap.get("tenantRefNum").toString();
		return tenantRefNum;
	}

	@SuppressWarnings("unchecked")
	public void setTenantRefNum(String tenantRefNum) {
		this.tenantRefNum = tenantRefNum;
		if(this.tenantRefNum  !=null)
			viewRootMap.put("tenantRefNum",this.tenantRefNum );
	}


	public String getUnitRefNum() {
		if(viewRootMap.containsKey("unitRefNum") && viewRootMap.get("unitRefNum")!=null)
			unitRefNum= viewRootMap.get("unitRefNum").toString();
		return unitRefNum;
	}

	@SuppressWarnings("unchecked")
	public void setUnitRefNum(String unitRefNum) {
		this.unitRefNum = unitRefNum;
		if(this.unitRefNum!=null)
			viewRootMap.put("unitRefNum",this.unitRefNum);
	}


	public String getAuctionRefNum() {
		if(viewRootMap.containsKey("auctionRefNum") && viewRootMap.get("auctionRefNum")!=null)
			auctionRefNum = viewRootMap.get("auctionRefNum").toString();
		return auctionRefNum;
	}

	@SuppressWarnings("unchecked")
	public void setAuctionRefNum(String auctionRefNum) {
		this.auctionRefNum = auctionRefNum;
		if(this.auctionRefNum !=null)
			viewRootMap.put("auctionRefNum",this.auctionRefNum);
	}


	public HtmlInputText getTxtRefNum() {
		return txtRefNum;
	}


	public void setTxtRefNum(HtmlInputText txtRefNum) {
		this.txtRefNum = txtRefNum;
	}



	@SuppressWarnings("unchecked")
	public List<SelectItem> getStatusList()
	{
		statusList.clear();
		List<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
		for(int i=0;i<ddvList.size();i++)
		{
			SelectItem item=null; 
			DomainDataView ddv=(DomainDataView)ddvList.get(i);
			//if page mode is in add mode than take the contract status as new
			if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
			{
			  if(getIsArabicLocale())
			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
			  else if(getIsEnglishLocale())
				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
            		
			  statusList.add(item);
			  break;
			}

			// IF PAGE IS IN UPDATE MODE 
			else if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) )
			{
				  if(getIsArabicLocale())
				   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
				  else if(getIsEnglishLocale())
					item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
				  statusList.add(item);
				}
			
			
			
		}
		
		return statusList;
	}


	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}


	public String getHdnUnitId() {
		if(viewRootMap.containsKey("hdnUnitId") && viewRootMap.get("hdnUnitId")!=null)
			hdnUnitId = viewRootMap.get("hdnUnitId").toString();
		return hdnUnitId;
	}
	@SuppressWarnings("unchecked")
	public void setHdnUnitId(String hdnUnitId) {
		this.hdnUnitId = hdnUnitId;
		if(this.hdnUnitId!=null)
			viewRootMap.put("hdnUnitId", this.hdnUnitId);
	}


	public String getHdnTenantId() {
		if(viewRootMap.containsKey("hdnTenantId") && viewRootMap.get("hdnTenantId")!=null)
			hdnTenantId = viewRootMap.get("hdnTenantId").toString();
		return hdnTenantId;
	}

	@SuppressWarnings("unchecked")
	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
		if(this.hdnTenantId!=null)
			viewRootMap.put("hdnTenantId", this.hdnTenantId);
	}


	public HtmlPanelTabbedPane getPanelTabbedPane() {
		return panelTabbedPane;
	}


	public void setPanelTabbedPane(HtmlPanelTabbedPane panelTabbedPane) {
		this.panelTabbedPane = panelTabbedPane;
	}


	public HtmlPanelTab getPanelTab() {
		return panelTab;
	}


	public void setPanelTab(HtmlPanelTab panelTab) {
		this.panelTab = panelTab;
	}


	public String getPersonSponsor() {
		personSponsor=WebConstants.SPONSOR;
		return personSponsor;
	}


	public void setPersonSponsor(String personSponsor) {
		
		this.personSponsor = personSponsor;
	}


	public String getPersonManager() {
		personManager=WebConstants.MANAGER;
		return personManager;
	}


	public void setPersonManager(String personManager) {
		this.personManager = personManager;
	}


	public String getPersonOccupier() {
		personOccupier=WebConstants.OCCUPIER;
		return personOccupier;
	}
	public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}

	public void setPersonOccupier(String personOccupier) {
		this.personOccupier = personOccupier;
	}


	public String getPersonPartner() {
		personPartner=WebConstants.PARTNER;
		return personPartner;
	}


	public void setPersonPartner(String personPartner) {
		this.personPartner = personPartner;
	}


	public PersonView getProspectiveOccupierDataItem() {
		return prospectiveOccupierDataItem;
	}


	public void setProspectiveOccupierDataItem(
			PersonView prospectiveOccupierDataItem) {
		this.prospectiveOccupierDataItem = prospectiveOccupierDataItem;
	}


	public void setProspectiveOccupierDataList(
			List<PersonView> prospectiveOccupierDataList) {
		this.prospectiveOccupierDataList = prospectiveOccupierDataList;
	}


	public HtmlDataTable getPropspectiveOccupierDataTable() {
		return propspectiveOccupierDataTable;
	}


	public void setPropspectiveOccupierDataTable(
			HtmlDataTable propspectiveOccupierDataTable) {
		this.propspectiveOccupierDataTable = propspectiveOccupierDataTable;
	}


	public Date getContractStartDate() {
	
	    if(viewRootMap.containsKey("CONTRACT_START_DATE"))
	    contractStartDate=(Date)viewRootMap.get("CONTRACT_START_DATE");
		return contractStartDate;
	}

	@SuppressWarnings("unchecked")
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
		viewRootMap.put("CONTRACT_START_DATE",this.contractStartDate);
	}


	public Date getContractEndDate() {
	 if(viewRootMap.containsKey("CONTRACT_END_DATE"))
	    contractEndDate=(Date)viewRootMap.get("CONTRACT_END_DATE");
		return contractEndDate;
	}

	@SuppressWarnings("unchecked")
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
		viewRootMap.put("CONTRACT_END_DATE",this.contractEndDate);
	}


	public HtmlDataTable getPaymentScheduleDataTable() {
		return paymentScheduleDataTable;
	}


	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
		this.paymentScheduleDataTable = paymentScheduleDataTable;
	}

	public void setPaymentScheduleDataList(
			List<PaymentScheduleView> paymentScheduleDataList) {
		this.paymentScheduleDataList = paymentScheduleDataList;
	}


	public PaymentScheduleView getPaymentScheduleDataItem() {
		return paymentScheduleDataItem;
	}


	public void setPaymentScheduleDataItem(
			PaymentScheduleView paymentScheduleDataItem) {
		this.paymentScheduleDataItem = paymentScheduleDataItem;
	}


	public HtmlDataTable getPaidFacilitiesDataTable() {
		return paidFacilitiesDataTable;
	}


	public void setPaidFacilitiesDataTable(HtmlDataTable paidFacilitiesDataTable) {
		this.paidFacilitiesDataTable = paidFacilitiesDataTable;
	}


	public PaidFacilitiesView getPaidFacilitiesDataItem() {
		return paidFacilitiesDataItem;
	}


	public void setPaidFacilitiesDataItem(PaidFacilitiesView paidFacilitiesDataItem) {
		this.paidFacilitiesDataItem = paidFacilitiesDataItem;
	}


	public void setPaidFacilitiesDataList(
			List<PaidFacilitiesView> paidFacilitiesDataList) {
		this.paidFacilitiesDataList = paidFacilitiesDataList;
	}


	public String getUnitRentValue() {
		if(viewRootMap.containsKey("unitRentValue") &&  viewRootMap.get("unitRentValue")!=null )
			unitRentValue = viewRootMap.get("unitRentValue").toString();
		return unitRentValue;
	}

	@SuppressWarnings("unchecked")
	public void setUnitRentValue(String unitRentValue) {
		this.unitRentValue = unitRentValue;
		if(this.unitRentValue!=null)
			viewRootMap.put("unitRentValue",this.unitRentValue);
	}


	public String getTxtSponsor() {
		if(viewRootMap.containsKey("txtSponsor") &&  viewRootMap.get("txtSponsor")!=null )
			txtSponsor = viewRootMap.get("txtSponsor").toString();
		return txtSponsor;
	}

	@SuppressWarnings("unchecked")
	public void setTxtSponsor(String txtSponsor) {
		this.txtSponsor = txtSponsor;
		if(this.txtSponsor!=null)
			viewRootMap.put("txtSponsor",this.txtSponsor);
	}


	public String getHdnSponsorId() {
		return hdnSponsorId;
	}


	public void setHdnSponsorId(String hdnSponsorId) {
		this.hdnSponsorId = hdnSponsorId;
	}


	public String getTxtManager() {
		if(viewRootMap.containsKey("txtManager") &&  viewRootMap.get("txtManager")!=null )
			txtManager = viewRootMap.get("txtManager").toString();
		
		return txtManager;
	}

	@SuppressWarnings("unchecked")
	public void setTxtManager(String txtManager) {
		this.txtManager = txtManager;
		if(this.txtManager !=null)
			viewRootMap.put("txtManager",this.txtManager);
	}


	public String getHdnManagerId() {
		return hdnManagerId;
	}


	public void setHdnManagerId(String hdnManagerId) {
		this.hdnManagerId = hdnManagerId;
	}


	public String getHdnUnitUsuageTypeId() {
		
		return hdnUnitUsuageTypeId;
	}


	public void setHdnUnitUsuageTypeId(String hdnUnitUsuageTypeId) {
		this.hdnUnitUsuageTypeId = hdnUnitUsuageTypeId;
	}


	public String getHdntenanttypeId() {
		if(viewRootMap.containsKey("hdntenanttypeId") && viewRootMap.get("hdntenanttypeId")!=null)
			hdntenanttypeId= viewRootMap.get("hdntenanttypeId").toString();
		return hdntenanttypeId;
	}


	public void setHdntenanttypeId(String hdntenanttypeId) {
		this.hdntenanttypeId = hdntenanttypeId;
	}


	public String getUnitRentAmount() {
		return unitRentAmount;
	}


	public void setUnitRentAmount(String unitRentAmount) {
		this.unitRentAmount = unitRentAmount;
	}


	public String getPageMode() {
		return pageMode;
	}


	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	public String getHdnAuctionunitId() {
		return hdnAuctionunitId;
	}


	public void setHdnAuctionunitId(String hdnAuctionunitId) {
		this.hdnAuctionunitId = hdnAuctionunitId;
	}


	public Double getTotalContractValue() {
		return totalContractValue;
	}


	public void setTotalContractValue(Double totalContractValue) {
		this.totalContractValue = totalContractValue;
	}


	public String getTxttotalContractValue() {
		if(totalContractValue>0)
		{
		txttotalContractValue=totalContractValue.toString();
		}
		return txttotalContractValue;
	}


	public void setTxttotalContractValue(String txttotalContractValue) {
		this.txttotalContractValue = txttotalContractValue;
	}


	public String getContractId() {
		return contractId;
	}


	public void setContractId(String contractId) {
		this.contractId = contractId;
	}


	public String getContractCreatedOn() {
		return contractCreatedOn;
	}


	public void setContractCreatedOn(String contractCreatedOn) {
		this.contractCreatedOn = contractCreatedOn;
	}


	public String getContractCreatedBy() {
		return contractCreatedBy;
	}


	public void setContractCreatedBy(String contractCreatedBy) {
		this.contractCreatedBy = contractCreatedBy;
	}


	public String getSelectRequestStatus() {
		
		if(viewRootMap.containsKey("selectRequestStatus") && viewRootMap.get("selectRequestStatus")!=null)
			selectRequestStatus = viewRootMap.get("selectRequestStatus").toString();
		return selectRequestStatus;
	}

	@SuppressWarnings("unchecked")
	public void setSelectRequestStatus(String selectRequestStatus) {
		this.selectRequestStatus = selectRequestStatus;
		if(this.selectRequestStatus!=null)
			viewRootMap.put("selectRequestStatus",this.selectRequestStatus);
		
	}
	 public ResourceBundle getBundle() {

		 

		 ResourceBundle bundle;

		             bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());


         return bundle;

   }



   public void setBundle(ResourceBundle bundle) {

         this.bundle = bundle;

   }


public String getTxtTenantName() {
	return txtTenantName;
}


public void setTxtTenantName(String txtTenantName) {
	this.txtTenantName = txtTenantName;
}


public String getTxtTenantType() {
	return txtTenantType;
}


public void setTxtTenantType(String txtTenantType) {
	this.txtTenantType = txtTenantType;
}


public String getTxtUnitUsuageType() {
	return txtUnitUsuageType;
}


public void setTxtUnitUsuageType(String txtUnitUsuageType) {
	this.txtUnitUsuageType = txtUnitUsuageType;
}


public String getPropertyCommercialName() {
	return propertyCommercialName;
}


public void setPropertyCommercialName(String propertyCommercialName) {
	this.propertyCommercialName = propertyCommercialName;
}


public String getPropertyAddress() {
	return propertyAddress;
}


public void setPropertyAddress(String propertyAddress) {
	this.propertyAddress = propertyAddress;
}

public boolean getIsItemEditable()
{
  boolean isItemEditable = true;	
  if(pageMode!=null && (pageMode.equals(PAGE_MODE_ADD)|| pageMode.equals(PAGE_MODE_UPDATE) ||
      pageMode.equals(WebConstants.LEASE_AMEND_MODE) || pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) || 
      pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)
      ))
	  return isItemEditable ;
  else if(pageMode!=null )
  {
	  if(getIsContractStatusDraft() || getIsContractStatusNew() || getIsContractStatusApprovalRequired())
	   return true;
	  else
		  return false;
	  
  }
  return isItemEditable;
}
@SuppressWarnings("unchecked")
public String getContractStatus() 
{
	String methodName="getContractStatus";
	logger.logDebug(methodName+"|"+"Start");
	logger.logDebug(methodName+"|"+"PageMode :"+pageMode);
	contractStatus="";
	List<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
	for(int i=0;i<ddvList.size();i++)
	{
		 
		DomainDataView ddv=(DomainDataView)ddvList.get(i);
		logger.logDebug(methodName+"|"+"Domain Data Id :"+ddv.getDomainDataId());
		
		//if page mode is in add mode than take the contract status as new
		if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
		{
		  if(getIsArabicLocale())
		       contractStatus= ddv.getDataDescAr();
		  else if(getIsEnglishLocale())
			   contractStatus= ddv.getDataDescEn();
		  
		  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
        	break;
		}

		// IF PAGE IS IN UPDATE MODE 
		else if(pageMode!=null && !pageMode.equals(PAGE_MODE_ADD)  && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
		{
			
			  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	    	  logger.logDebug(methodName+"|"+"Contract Status ..."+contractView.getStatus());
		      if(contractView.getStatus()!=null &&  ddv.getDomainDataId().toString().equals(contractView.getStatus().toString()) )
		      {
				  if(getIsArabicLocale())
					  contractStatus= ddv.getDataDescAr();  
				  else if(getIsEnglishLocale())
					  contractStatus= ddv.getDataDescEn();
				  
				  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
				  break;
		      }
			  
	    }
	}
		
		
	logger.logDebug(methodName+"|"+"Finish");
	return contractStatus;
}


public void setContractStatus(String contractStatus) {
	this.contractStatus = contractStatus;
}


public String getTxtMaintainenceAmount() {
	return txtMaintainenceAmount;
}


public void setTxtMaintainenceAmount(String txtMaintainenceAmount) {
	this.txtMaintainenceAmount = txtMaintainenceAmount;
}


public Double getMaintainenceAmount() {
	return maintainenceAmount;
}


public void setMaintainenceAmount(Double maintainenceAmount) {
	this.maintainenceAmount = maintainenceAmount;
}


public String getUnitStatusId() {
	if(viewRootMap.containsKey("unitStatusId") && viewRootMap.get("unitStatusId") !=null)
		unitStatusId = viewRootMap.get("unitStatusId").toString(); 
	return unitStatusId;
}

@SuppressWarnings("unchecked")
public void setUnitStatusId(String unitStatusId) {
	this.unitStatusId = unitStatusId;
	if(this.unitStatusId!=null)
		viewRootMap.put("unitStatusId", this.unitStatusId);
}


public String getTxtUnitStatus() {
	return txtUnitStatus;
}


public void setTxtUnitStatus(String txtUnitStatus) {
	this.txtUnitStatus = txtUnitStatus;
}





public HtmlDataTable getUnitDataTable() {
	return unitDataTable;
}


public void setUnitDataTable(HtmlDataTable unitDataTable) {
	this.unitDataTable = unitDataTable;
}

@SuppressWarnings("unchecked")
public List<UnitView> getUnitDataList() {

	
	if(viewRootMap.containsKey("AUCTIONUNITINFO"))
	 {
		AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
		if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
				 auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
		  )
		{
			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
			viewRootMap.remove(AUCTION_UNIT_ID);
		}
	
		if(!viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
				(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
				 auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
				 )
		  )
		 {
				 viewRootMap.put(PREVIOUS_UNIT_ID,auctionUnitView.getUnitId());
			     populateAuctionUnitDetail();
			     viewRootMap.remove(WebConstants.PAID_FACILITIES);
		 }
	 }
	 else if(viewRootMap.containsKey("UNITINFO"))
	 {
		 UnitView unitView=(UnitView)viewRootMap.get("UNITINFO");
		 if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
				 unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
		  )
		{
			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
			viewRootMap.remove(AUCTION_UNIT_ID);
		}
		 if(!viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
			(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
			 unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
			 )
			)
		 {
			 logger.logInfo("getUnitDataList|PropertyCategoryId",unitView.getPropertyCategoryId());
		     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
			 viewRootMap.put(PREVIOUS_UNIT_ID,unitView.getUnitId());
			 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(unitView.getUnitId().toString()))
			 {
			  unitDataList.clear();
			  unitDataList.add(unitView);
			  this.setUnitRefNum(unitView.getUnitNumber());
			  if(unitView.getStatusId()!=null)
		      {
		            unitView.setStatusId(unitView.getStatusId());
		            this.setUnitStatusId(unitView.getStatusId().toString());
		       }
			  viewRootMap.put(UNIT_DATA_ITEM, unitView);
			//  if(unitView.getRentValue()!= null && (unitRentAmount== null || unitRentAmount.trim().length()==0))
		        	 unitRentAmount = unitView.getRentValue().toString();
		        	 viewRootMap.remove(WebConstants.PAID_FACILITIES);
			 }
		 }
	 }
		 
	return unitDataList;
}

@SuppressWarnings("unchecked")
private void populateAuctionUnitDetail() {
	String methodName= "populateAuctionUnitDetail";
	logger.logInfo(methodName+"|Start");
	AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
	 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(auctionUnitView.getUnitId().toString()))
	 {
		 
		 UnitView unitView =auctionUnitView.getUnitView();
		 unitView.setAuctionUnitId(auctionUnitView.getAuctionUnitId());
		 viewRootMap.put(AUCTION_UNIT_ID,auctionUnitView.getAuctionUnitId());
		 unitView.setRentValue(auctionUnitView.getBidPrice());
		 unitView.setUnitId(auctionUnitView.getUnitId());
		 
	     unitView.setUnitNumber( auctionUnitView.getUnitNumber());
	     this.setUnitRefNum(auctionUnitView.getUnitNumber());
	     unitView.setUsageTypeId(auctionUnitView.getUnitUsageTypeID());
	     unitView.setUnitAddress(auctionUnitView.getPropertyAdress());
	     unitView.setPropertyCommercialName(auctionUnitView.getPropertyCommercialName());
	     //unitView.setFloorName(floorName)
	     unitView.setUsageTypeEn(auctionUnitView.getUnitUsageTypeEn());
	     unitView.setUsageTypeAr(auctionUnitView.getUnitUsageTypeAr());
	     unitView.setStatusEn( auctionUnitView.getUnitStatusEn());
	     unitView.setStatusAr(auctionUnitView.getUnitStatusAr());
	     logger.logInfo(methodName+"|PropertyCategoryId",unitView.getPropertyCategoryId());
	     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
	     if(auctionUnitView.getUnitStatusId()!=null)
	     {
	        unitView.setStatusId(auctionUnitView.getUnitStatusId());
	        this.setUnitStatusId(auctionUnitView.getUnitStatusId().toString());
	     }
	     if(auctionUnitView.getBidderId()!=null)
	    	 getDepositAmountForBidder(auctionUnitView.getAuctionUnitId(), auctionUnitView.getBidderId(),null);
	     unitDataList.clear();
	     unitDataList.add(unitView);
	     viewRootMap.put(UNIT_DATA_ITEM, unitView);
	     //if(unitView.getRentValue()!= null && (unitRentAmount== null || unitRentAmount.trim().length()==0))
	    	 unitRentAmount = unitView.getRentValue().toString(); 
	     logger.logInfo(methodName+"|Finish");
	 }
}
@SuppressWarnings("unchecked")
private void getDepositAmountForBidder(Long auctionUnitId,Long bidderId,Long movedToContractId) {
	 
	 String methodName = "getDepositAmountForBidder";
	 RequestServiceAgent rsa ;
	 try
	 {
		 rsa = new RequestServiceAgent();
		 List<RequestDetailView> rdViewList = new ArrayList<RequestDetailView>(0);
		 List<RequestDetailView> newRDViewList = new ArrayList<RequestDetailView>(0);
		 
		 if(auctionUnitId != null && bidderId != null )
		  rdViewList = rsa.getRequestDetailForBidderAuctionUnit(auctionUnitId, bidderId);
		 else if(movedToContractId != null )
		  rdViewList = rsa.getRequestDetailForMovedToContractId(movedToContractId);
		 Double auctionDepositAmount =0D;
		 if(rdViewList!=null)
	
		 for (RequestDetailView requestDetailView : rdViewList) {
			 auctionDepositAmount += (requestDetailView.getDepositAmount() -  requestDetailView.getConfiscatedAmount());
		     requestDetailView.setUpdatedBy(getLoggedInUser());
		     newRDViewList.add(requestDetailView);
		 }
		 	 viewRootMap.put(AUCTION_UNIT_BIDDER_LIST,newRDViewList);
		 if(auctionDepositAmount >0D)
			 viewRootMap.put(AUCTION_DEPOSIT_AMOUNT,auctionDepositAmount);
	 }
	 catch(Exception e)
	 {
		 logger.LogException(methodName+"|Error Occured",e);
	 }
}
public void setUnitDataList(List<UnitView> unitDataList) {
	this.unitDataList = unitDataList;
}


public UnitView getUnitDataItem() {
	//List<UnitView> unitViewList = getUnitDataList();
	if(viewRootMap.containsKey(UNIT_DATA_ITEM))
		unitDataItem = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
	return unitDataItem;
}


public void setUnitDataItem(UnitView unitDataItem) {
	this.unitDataItem = unitDataItem;
}


public HtmlDataTable getCommercialActivityDataTable() {
	return commercialActivityDataTable;
}


public void setCommercialActivityDataTable(
		HtmlDataTable commercialActivityDataTable) {
	this.commercialActivityDataTable = commercialActivityDataTable;
}

@SuppressWarnings("unchecked")
public List<CommercialActivityView> getCommercialActivityList() {
	commercialActivityList=null;
		 	if(commercialActivityList==null || commercialActivityList.size()<=0)
		 	{
		 	  if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
		 		commercialActivityList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
		 	 
		 	}
 	return commercialActivityList;
}


public void setCommercialActivityList(
		List<CommercialActivityView> commercialActivityList) {
	this.commercialActivityList = commercialActivityList;
}


public CommercialActivityView getCommercialActivityItem() {
	return commercialActivityItem;
}


public void setCommercialActivityItem(
		CommercialActivityView commercialActivityItem) {
	this.commercialActivityItem = commercialActivityItem;
}


public HtmlCommandButton getBtnOccuipers() {
	return btnOccuipers;
}


public void setBtnOccuipers(HtmlCommandButton btnOccuipers) {
	this.btnOccuipers = btnOccuipers;
}


public HtmlCommandButton getBtnPartners() {
	return btnPartners;
}


public void setBtnPartners(HtmlCommandButton btnPartners) {
	this.btnPartners = btnPartners;
}


public HtmlCommandButton getBtnSaveNewLease() {
	return btnSaveNewLease;
}


public void setBtnSaveNewLease(HtmlCommandButton btnSaveNewLease) {
	this.btnSaveNewLease = btnSaveNewLease;
}


public HtmlCommandButton getBtnSaveAmendLease() {
	return btnSaveAmendLease;
}


public void setBtnSaveAmendLease(HtmlCommandButton btnSaveAmendLease) {
	this.btnSaveAmendLease = btnSaveAmendLease;
}


public HtmlCommandButton getBtnCompleteAmendLease() {
	return btnCompleteAmendLease;
}


public void setBtnCompleteAmendLease(HtmlCommandButton btnCompleteAmendLease) {
	this.btnCompleteAmendLease = btnCompleteAmendLease;
}


public HtmlCommandButton getBtnSubmitAmendLease() {
	return btnSubmitAmendLease;
}


public void setBtnSubmitAmendLease(HtmlCommandButton btnSubmitAmendLease) {
	this.btnSubmitAmendLease = btnSubmitAmendLease;
}


public HtmlGraphicImage getImgSponsor() {
	return imgSponsor;
}


public void setImgSponsor(HtmlGraphicImage imgSponsor) {
	this.imgSponsor = imgSponsor;
}


public HtmlGraphicImage getImgManager() {
	return imgManager;
}


public void setImgManager(HtmlGraphicImage imgManager) {
	this.imgManager = imgManager;
}


public HtmlCommandButton getBtnApprove() {
	return btnApprove;
}


public void setBtnApprove(HtmlCommandButton btnApprove) {
	this.btnApprove = btnApprove;
}


public HtmlCommandButton getBtnReject() {
	return btnReject;
}


public void setBtnReject(HtmlCommandButton btnReject) {
	this.btnReject = btnReject;
}


public HtmlCommandButton getBtnGenPayments() {
	return btnGenPayments;
}


public void setBtnGenPayments(HtmlCommandButton btnGenPayments) {
	this.btnGenPayments = btnGenPayments;
}


public HtmlCommandButton getBtnApproveAmendLease() {
	return btnApproveAmendLease;
}


public void setBtnApproveAmendLease(HtmlCommandButton btnApproveAmendLease) {
	this.btnApproveAmendLease = btnApproveAmendLease;
}


public HtmlCommandButton getBtnRejectAmendLease() {
	return btnRejectAmendLease;
}


public void setBtnRejectAmendLease(HtmlCommandButton btnRejectAmendLease) {
	this.btnRejectAmendLease = btnRejectAmendLease;
}


public String getPageTitle() {
	return pageTitle;
}


public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
}


public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkCommercial() {
	return chkCommercial;
}


public void setChkCommercial(
		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial) {
	this.chkCommercial = chkCommercial;
}


public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkPaidFacilities() {
	return chkPaidFacilities;
}


public void setChkPaidFacilities(
		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities) {
	this.chkPaidFacilities = chkPaidFacilities;
}


public HtmlInputText getTxtRefNumber() {
	
	return txtRefNumber;
}


public void setTxtRefNumber(HtmlInputText txtRefNumber) {
	this.txtRefNumber = txtRefNumber;
}


public HtmlOutputLabel getLblRefNum() {
	return lblRefNum;
}


public void setLblRefNum(HtmlOutputLabel lblRefNum) {
	this.lblRefNum = lblRefNum;
}


public String getStrTimeZone()
{
	
	 return TimeZone.getDefault().toString();
	
}


public TimeZone getTimeZone()
{
	 return TimeZone.getDefault();
	
}




public Date getContractIssueDate() {

    if(viewRootMap.containsKey("CONTRACT_ISSUE_DATE"))
     contractIssueDate=(Date)viewRootMap.get("CONTRACT_ISSUE_DATE");
	return contractIssueDate;
}

@SuppressWarnings("unchecked")
public void setContractIssueDate(Date contractIssueDate) {
	this.contractIssueDate = contractIssueDate;
	if(this.contractIssueDate!=null)
	 viewRootMap.put("CONTRACT_ISSUE_DATE",this.contractIssueDate);
}

@SuppressWarnings("unchecked")
public List<SelectItem> getContractTypeList() {
	contractTypeList.clear();
	List<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
	for(int i=0;i<ddvList.size();i++)
	{
		SelectItem item=null; 
		DomainDataView ddv=(DomainDataView)ddvList.get(i);
			  if(getIsArabicLocale())
			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
			  else if(getIsEnglishLocale())
				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
			  contractTypeList.add(item);

		
		
		
	}
	
	
	return contractTypeList;
}


public void setContractTypeList(List<SelectItem> contractTypeList) {
	this.contractTypeList = contractTypeList;
}


public String getSelectOneContractType() {
	if(viewRootMap.containsKey("selectOneContractType") && viewRootMap.get("selectOneContractType")!=null)
		selectOneContractType= viewRootMap.get("selectOneContractType").toString();
		return selectOneContractType;
}

@SuppressWarnings("unchecked")
public void setSelectOneContractType(String selectOneContractType) {
	this.selectOneContractType = selectOneContractType;
	if(this.selectOneContractType !=null)
		viewRootMap.put("selectOneContractType",this.selectOneContractType);
}

public double getTotalRentAmount() {
	return totalRentAmount;
}


public void setTotalRentAmount(double totalRentAmount) {
	this.totalRentAmount = totalRentAmount;
}


public double getTotalCashAmount() {
	return totalCashAmount;
}


public void setTotalCashAmount(double totalCashAmount) {
	this.totalCashAmount = totalCashAmount;
}


public double getTotalChqAmount() {
	return totalChqAmount;
}


public void setTotalChqAmount(double totalChqAmount) {
	this.totalChqAmount = totalChqAmount;
}


public int getInstallments() {
	return installments;
}


public void setInstallments(int installments) {
	this.installments = installments;
}


public double getTotalDepositAmount() {
	return totalDepositAmount;
}


public void setTotalDepositAmount(double totalDepositAmount) {
	this.totalDepositAmount = totalDepositAmount;
}


public double getTotalFees() {
	return totalFees;
}


public void setTotalFees(double totalFees) {
	this.totalFees = totalFees;
}


public double getTotalFines() {
	return totalFines;
}


public void setTotalFines(double totalFines) {
	this.totalFines = totalFines;
}	








public org.richfaces.component.html.HtmlTab getTabCommercialActivity() {
	return tabCommercialActivity;
}


public void setTabCommercialActivity(
		org.richfaces.component.html.HtmlTab tabCommercialActivity) {
	this.tabCommercialActivity = tabCommercialActivity;
}


public org.richfaces.component.html.HtmlTab getTabPartner() {
	return tabPartner;
}


public void setTabPartner(org.richfaces.component.html.HtmlTab tabPartner) {
	this.tabPartner = tabPartner;
}


public org.richfaces.component.html.HtmlTab getTabOccupier() {
	return tabOccupier;
}


public void setTabOccupier(org.richfaces.component.html.HtmlTab tabOccupier) {
	this.tabOccupier = tabOccupier;
}


public org.richfaces.component.html.HtmlTab getTabContractHistory() {
	return tabContractHistory;
}


public void setTabContractHistory(
		org.richfaces.component.html.HtmlTab tabContractHistory) {
	this.tabContractHistory = tabContractHistory;
}


public org.richfaces.component.html.HtmlTab getTabRequestHistory() {
	return tabRequestHistory;
}


public void setTabRequestHistory(
		org.richfaces.component.html.HtmlTab tabRequestHistory) {
	this.tabRequestHistory = tabRequestHistory;
}


public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
	return tabPaymentTerms;
}


public void setTabPaymentTerms(
		org.richfaces.component.html.HtmlTab tabPaymentTerms) {
	this.tabPaymentTerms = tabPaymentTerms;
}


public org.richfaces.component.html.HtmlTab getTabFacilities() {
	return tabFacilities;
}


public void setTabFacilities(org.richfaces.component.html.HtmlTab tabFacilities) {
	this.tabFacilities = tabFacilities;
}
@SuppressWarnings("unchecked")
public String cmdChangeTenant_Click(){
	String  METHOD_NAME = "cmdChangeTenant_Click|";		
    logger.logInfo(METHOD_NAME+"started...");
    try{

    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	clearApplicationDetails();
    	PropertyServiceAgent psa=new PropertyServiceAgent();
    	RequestView requestView =new RequestView();
    	requestView.setContractView(contractView);//Id(contractView.getContractId());
    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);
    	List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null,null);

    	DomainDataView ddv=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
    			                                    WebConstants.REQUEST_STATUS_COMPLETE);
    	//if there is some request for change tenant name against this 
    	//contract and its status is not completed then display that request
    	//otherwise create new screen
    	requestView=null;
    	for (RequestView requestView2 : requestViewList) {
			
    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
    		{
    			requestView =requestView2;
    		   break;
    		}
		}
    	if(requestView!=null)
    	{
    		
    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
    	}
    	else 
    	{
    		//context.
    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   
    	}
    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	return WebConstants.ContractListNavigation.CHANGE_TENANT_NAME;
}
@SuppressWarnings("unchecked")
public String cmdAmendLeaseContract_Click(){
	String  METHOD_NAME = "cmdAmendLeaseContract_Click|";		
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	CommonUtil commonUtil = new CommonUtil();
    	RequestServiceAgent rsa=new RequestServiceAgent();
    	RequestView requestView =new RequestView();
    	List<RequestView> requestViewList =new ArrayList<RequestView>(0);
    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
    	RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);        	
    	requestViewList= 
    		              rsa.getRequestFromRequestField(contractView.getContractId().toString(), 
    		            		                         requestView.getRequestTypeId().toString(),
    		            		                         requestKeyView.getRequestKeyId().toString(),
    		            		                         null 
    		                                            );
    	DomainDataView ddv=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
    			                                    WebConstants.REQUEST_STATUS_COMPLETE);
    	
    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
    	//if there is some request for Amend against this 
    	//contract and its status is not completed then display that request
    	//otherwise create new screen
    	requestView=null;
    	for (RequestView requestView2 : requestViewList) {
			
    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
    		{
    			requestView =requestView2;
    		   break;
    		}
		}
    	if(requestView!=null)
    	{
    		
    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
    		setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
    	}
    	else 
    	{
    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   
    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
    	   sessionMap.put(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
    	}
    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	return WebConstants.ContractListNavigation.LEASE_CONTRACT_SCREEN;
}

@SuppressWarnings("unchecked")
public String cmdTransferContract_Click(){
	String  METHOD_NAME = "cmdTransferContract_Click|";		
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
    	
    	
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	return WebConstants.ContractListNavigation.TRANSFER_CONTRACT;
}
@SuppressWarnings("unchecked")
public String cmdLinkCancelContract_Click(){
	String  METHOD_NAME = "cmdLinkCancelContract_Click|";		
    logger.logDebug(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
  	    logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	
	return WebConstants.ContractListNavigation.CANCEL_CONTRACT;
}
@SuppressWarnings("unchecked")
public String cmdLinkSettleContract_Click(){
	String  METHOD_NAME = "cmdLinkSettleContract_Click|";		
    logger.logDebug(METHOD_NAME+"started...");
    try{
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
  
    	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SETTLE_PAGE_MODE","readOnly");
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	
	return WebConstants.ContractListNavigation.SETTLE_CONTRACT;
}
@SuppressWarnings("unchecked")
public String issueCLCmdLink_Click(){
	String  METHOD_NAME = "issueCLCmdLink_Click|";		
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);

		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	return WebConstants.ContractListNavigation.ISSUE_CLEARANCE_LETTER;		
}

@SuppressWarnings("unchecked")
public String issueNOLCmdLink_Click(){
	String  METHOD_NAME = "issueNOLmdLink_Click|";		
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
  
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);			
	}
	return WebConstants.ContractListNavigation.ISSUE_NO_OBJECTION_LETTER;
}
@SuppressWarnings("unchecked")
public String renewContract()
{
	clearApplicationDetails();
  	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	ContractView selectedContract = contractView;
	
	setRequestParam("contractId", selectedContract.getContractId());
	setRequestParam("suggestedRentAmount", 0.0);
	setRequestParam("oldRentValue", selectedContract.getRentAmount());
	setRequestParam("fromRenewList", "true");
	
	return "renew";
}
@SuppressWarnings("unchecked")   
private Boolean compareContrctStatus(String status) 
{
	
	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	DomainDataView ddv= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS), status);
	if(contractView!=null && contractView.getStatus()!=null&& contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
		return  true;
	else
		return  false;
	
}
public Boolean getIsContrctStatusApproved() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVED);
}
public Boolean getIsContrctStatusComplete() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_COMPLETE);
}public Boolean getIsContrctStatusExpired() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_EXPIRED);
}
public Boolean getIsContrctStatusRenewDraft() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_RENEW_DRAFT);
}
public Boolean getIsContrctStatusActive() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_ACTIVE);
}
public Boolean getIsContrctStatusTerminated() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_TERMINATED);
}
public Boolean getIsContrctStatusRejected() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_REJECTED);
}
public Boolean getIsContractStatusNew() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_NEW);
}
public Boolean getIsContractStatusDraft() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_DRAFT);
}
public Boolean getIsContractStatusApprovalRequired() {
	
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED);
}


public HtmlCommandButton getBtnSendNewLeaseForApproval() {
	return btnSendNewLeaseForApproval;
}


public void setBtnSendNewLeaseForApproval(
		HtmlCommandButton btnSendNewLeaseForApproval) {
	this.btnSendNewLeaseForApproval = btnSendNewLeaseForApproval;
}


public HtmlGraphicImage getImgDeleteSponsor() {
	return imgDeleteSponsor;
}


public void setImgDeleteSponsor(HtmlGraphicImage imgDeleteSponsor) {
	this.imgDeleteSponsor = imgDeleteSponsor;
}


public HtmlGraphicImage getImgDeleteManager() {
	return imgDeleteManager;
}


public void setImgDeleteManager(HtmlGraphicImage imgDeleteManager) {
	this.imgDeleteManager = imgDeleteManager;
}


public String getHdnApplicantId() {
	return hdnApplicantId;
}


public void setHdnApplicantId(String hdnApplicantId) {
	this.hdnApplicantId = hdnApplicantId;
}


public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
	return tabAuditTrail;
}


public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
	this.tabAuditTrail = tabAuditTrail;
}


public HtmlCommandButton getBtnCollectPayment() {
	return btnCollectPayment;
}


public void setBtnCollectPayment(HtmlCommandButton btnCollectPayment) {
	this.btnCollectPayment = btnCollectPayment;
}


public HtmlGraphicImage getImgTenant() {
	return imgTenant;
}


public void setImgTenant(HtmlGraphicImage imgTenant) {
	this.imgTenant = imgTenant;
}


public HtmlCommandButton getBtnAddUnit() {
	return btnAddUnit;
}


public void setBtnAddUnit(HtmlCommandButton btnAddUnit) {
	this.btnAddUnit = btnAddUnit;
}


public HtmlCommandButton getBtnAddAuctionUnit() {
	return btnAddAuctionUnit;
}


public void setBtnAddAuctionUnit(HtmlCommandButton btnAddAuctionUnit) {
	this.btnAddAuctionUnit = btnAddAuctionUnit;
}


public org.richfaces.component.html.HtmlCalendar getIssueDateCalendar() {
	return issueDateCalendar;
}


public void setIssueDateCalendar(
		org.richfaces.component.html.HtmlCalendar issueDateCalendar) {
	this.issueDateCalendar = issueDateCalendar;
}


public org.richfaces.component.html.HtmlCalendar getStartDateCalendar() {
	return startDateCalendar;
}


public void setStartDateCalendar(
		org.richfaces.component.html.HtmlCalendar startDateCalendar) {
	this.startDateCalendar = startDateCalendar;
}


public org.richfaces.component.html.HtmlCalendar getEndDateCalendar() {
	return endDateCalendar;
}


public void setEndDateCalendar(
		org.richfaces.component.html.HtmlCalendar endDateCalendar) {
	this.endDateCalendar = endDateCalendar;
}


public HtmlCommandButton getBtnAddPayments() {
	return btnAddPayments;
}


public void setBtnAddPayments(HtmlCommandButton btnAddPayments) {
	this.btnAddPayments = btnAddPayments;
}


public HtmlSelectOneMenu getCmbContractType() {
	return cmbContractType;
}


public void setCmbContractType(HtmlSelectOneMenu cmbContractType) {
	this.cmbContractType = cmbContractType;
}


public HtmlInputText getTxt_ActualRent() {
	return txt_ActualRent;
}


public void setTxt_ActualRent(HtmlInputText txt_ActualRent) {
	this.txt_ActualRent = txt_ActualRent;
}


public HtmlGraphicImage getBtnPartnerDelete() {
	return btnPartnerDelete;
}


public void setBtnPartnerDelete(HtmlGraphicImage btnPartnerDelete) {
	this.btnPartnerDelete = btnPartnerDelete;
}


public HtmlGraphicImage getBtnOccupierDelete() {
	return btnOccupierDelete;
}


public void setBtnOccupierDelete(HtmlGraphicImage btnOccupierDelete) {
	this.btnOccupierDelete = btnOccupierDelete;
}


public HtmlCommandButton getBtnPrint() {
	return btnPrint;
}


public void setBtnPrint(HtmlCommandButton btnPrint) {
	this.btnPrint = btnPrint;
}
@SuppressWarnings("unchecked")
public String btnPrint_Click() {	
	
	String methodName="btnPrint_Click";
	logger.logInfo(methodName+"|Started...");
	UtilityServiceAgent usa = new UtilityServiceAgent(); 
	try {
			PrintLeaseContractReportCriteria reportCriteria = 
				new PrintLeaseContractReportCriteria(ReportConstant.Report.PRINT_LEASE_CONTRACT, ReportConstant.Processor.PRINT_LEASE_CONTRACT);

			reportCriteria.setContractId( this.contractId );
			

	    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		   	saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PRINTED);
		   	usa.printContract(new Long(this.getContractId()),getLoggedInUser());
		   	logger.logInfo(methodName+"|Finish...");    
	} 
	catch(Exception ex)
	{
	   	   logger.LogException(methodName+"|Error Occured...",ex);
	}
	return null;
}
@SuppressWarnings("unchecked")
private void openPopup(String javaScriptText) {
	logger.logInfo("openPopup() started...");
	try {
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
		logger.logInfo("openPopup() completed successfully!!!");
	} catch (Exception exception) {
		logger.LogException("openPopup() crashed ", exception);
	}
}


public HtmlTabPanel getTabPanel() {
	return tabPanel;
}


public void setTabPanel(HtmlTabPanel tabPanel) {
	this.tabPanel = tabPanel;
}


public HtmlTab getTabApplicationDetails() {
	return tabApplicationDetails;
}


public void setTabApplicationDetails(HtmlTab tabApplicationDetails) {
	this.tabApplicationDetails = tabApplicationDetails;
}


public HtmlGraphicImage getImgViewSponsor() {
	return imgViewSponsor;
}


public void setImgViewSponsor(HtmlGraphicImage imgViewSponsor) {
	this.imgViewSponsor = imgViewSponsor;
}


public HtmlGraphicImage getImgViewTenant() {
	return imgViewTenant;
}


public void setImgViewTenant(HtmlGraphicImage imgViewTenant) {
	this.imgViewTenant = imgViewTenant;
}


public HtmlGraphicImage getImgViewManager() {
	return imgViewManager;
}


public void setImgViewManager(HtmlGraphicImage imgViewManager) {
	this.imgViewManager = imgViewManager;
}
@SuppressWarnings("unchecked")
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		ContractView contView = null;
		PersonView applicant = null;
		PersonView tenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		RequestView reqView = (RequestView)viewRootMap.get("REQUEST_FOR_AMEND");
		try
		{
		if(reqView != null)
		{
			contView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			if(contView != null)
			{
				tenant = contView.getTenantView();
				if(tenant.getPersonFullName().equals(""))
					tenant = (PersonView) viewRootMap.get(TENANT_INFO);
				//applicant = reqView.getApplicantView();
				if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) && viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)!=null)
				{
					Long personId = (Long)viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID);
					applicant = psAgent.getPersonInformation(personId);
				}
				UnitView unitView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
						
				eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
				eventAttributesValueMap.put("CONTRACT_START_DATE", contView.getStartDateString());
				eventAttributesValueMap.put("CONTRACT_END_DATE", contView.getEndDateString());
				eventAttributesValueMap.put("PROPERTY_NAME", unitView.getPropertyCommercialName());
				eventAttributesValueMap.put("UNIT_NO", unitView.getUnitNumber());
				eventAttributesValueMap.put("TOTAL_VALUE", this.getTxttotalContractValue());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(tenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
	
				
			}
		}
		}
		catch (Exception e)
		{
			logger.LogException("requestStatusNotification crashed...",e);
		
		}
	}
@Override
@SuppressWarnings("unchecked")
	public List<ContactInfo> getContactInfoList(PersonView personView) 
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(personView.getPersonId().toString(), personView.getCellNumber(), null, ciView.getEmail()));    			
            }
    	}
    	return emailList;
	}
	
}
