package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlInputTextarea;
import org.apache.myfaces.component.html.ext.HtmlOutputLabel;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.component.html.ext.HtmlPanelGroup;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.proxy.PIMSNewMaintenanceRequestPortClient;
import com.avanza.pims.proxy.pimsmaintenancerequestbpelproxy.PIMSMaintenanceRequestBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.ContractorSearch;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.ServiceContractListBackingBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ComplaintDetailsView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SiteVisitView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;

public class NewMaintenanceRequest extends AbstractController 
{
	private static final long serialVersionUID = 1L;
	private transient Logger logger = Logger.getLogger(NewMaintenanceRequest.class);
	public interface Page_Mode 
	{
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String EDIT="PAGE_MODE_EDIT";
		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
		public static final String SITE_VISIT_REQUIRED="PAGE_MODE_SITE_VISIT_REQUIRED";
		public static final String COMPLETE="PAGE_MODE_COMPLETE";
		public static final String POPUP="PAGE_MODE_POPUP";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String FOLLOW_UP				   = "PAGE_MODE_FOLLOW_UP";
		public static final String WITH_CONTRACTOR				   = "WITH_CONTRACTOR";
		public static final String CONTRACTOR_EXECUTED		   = "PAGE_MODE_CONTRACTOR_EXECUTED";
		public static final String PAGE_MODE_CUSTOMER_REJECTED = "PAGE_MODE_CUSTOMER_REJECTED";
		
	}
	
    public interface TAB_ID 
    {
		public static final String ApplicationDetails="tabApplicationDetails";
		public static final String MaintenanceInfo="tabMaintenanceInfo";
		public static final String PaymentSchedule="tabPaymentSchedule";
		public static final String Attachment="attachmentTab";
		public static final String SiteVist="tabSiteVisit";
		public static final String FollowUp="tabFollowUp";
	}
    
    public static final String SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL = "SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL";
    
	protected  String pageMode="pageMode";
	HttpServletRequest request ;
	protected SystemParameters parameters = SystemParameters.getInstance();
	Map<String, Object> viewRootMap ;
	Map sessionMap ;
	FacesContext context ;
	protected List<String> errorMessages = new ArrayList<String>();
	protected List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil ;

	protected String PROCEDURE_TYPE;
	protected String EXTERNAL_ID;

	protected String createdOn;
	protected String createdBy;
	public PersonView  contractor = new PersonView();

	protected String DD_REQUEST_STATUS_LIST ="DD_REQUEST_STATUS_LIST";
	protected String CONTRACTOR_VIEW  ="CONTRACTOR_VIEW";
	protected String CONTRACT_VIEW    ="CONTRACT_VIEW";
	protected String MAINTENANCE_CONTRACT_VIEW    ="MAINTENANCE_CONTRACT_VIEW";
	protected String REQUEST_VIEW     ="REQUEST_VIEW";
	protected String REQUEST_PRIORITY_COMBO     ="REQUEST_PRIORITY_COMBO";
	protected RequestView requestView = new RequestView();
	
	public String pageTitle;
	protected org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabMaintenanceInfo=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabApplicationDetails=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabSiteVisit=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabFollowUp=new org.richfaces.component.html.HtmlTab();
	
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnSend_For_Approval= new HtmlCommandButton();
	protected HtmlCommandButton btnApprove = new HtmlCommandButton();
    protected HtmlCommandButton btnReject = new HtmlCommandButton();
    protected HtmlCommandButton btnSiteVisitReq = new HtmlCommandButton();
	protected HtmlCommandButton btnSaveSiteVisit = new HtmlCommandButton();
	protected HtmlCommandButton btnCompleteSiteVisit = new HtmlCommandButton();
	protected HtmlCommandButton btnComplete = new HtmlCommandButton();
	protected HtmlCommandButton btnSaveFollowUp= new HtmlCommandButton();
	protected HtmlCommandButton btnResendToContractor= new HtmlCommandButton();
	protected HtmlCommandButton btnCustomerRejected= new HtmlCommandButton();
	protected HtmlCommandButton btnContractorExecuted= new HtmlCommandButton();
	private HtmlCommandButton btnOtherSave = new HtmlCommandButton();
	private HtmlOutputLabel lblRemarks=new HtmlOutputLabel();
	private HtmlOutputLabel lblServiceProviderRemarks =new HtmlOutputLabel();
	private HtmlInputTextarea txtMaintenaceDetails = new HtmlInputTextarea();
	private HtmlInputTextarea  txtServiceProviderRemarks = new HtmlInputTextarea();
	private HtmlInputTextarea  txtFMRemarks = new HtmlInputTextarea();
	private HtmlOutputLabel lblFMRemarks=new HtmlOutputLabel();
	HtmlSelectOneMenu cmbMaintenanceType = new HtmlSelectOneMenu();
	private String selectOneMaintenanceType;
	private String selectOneWorkType;
	private List<String> selectManyWorkType;
	HtmlSelectOneRadio  cmbWorkType = new HtmlSelectOneRadio();
	
	HtmlSelectOneMenu cmbRequestPriority = new HtmlSelectOneMenu();
	private String selectOneRequestPriority ;
	
	private HtmlOutputLabel lblCustomerReview	=new HtmlOutputLabel();
	HtmlSelectOneMenu cmbCustomerReview = new HtmlSelectOneMenu();
	private String selectOneCmbCustomerReview;
	
	private HtmlInputText txtContractNum = new HtmlInputText();
	private HtmlInputText txtTelephoneNumber = new HtmlInputText();
	private HtmlInputText txtContractorNum = new HtmlInputText();
	private HtmlInputTextarea txtboxRemarks = new HtmlInputTextarea();
	protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    
    private HtmlGraphicImage imgViewUnitDetails=new HtmlGraphicImage();
    private HtmlGraphicImage imgUnitSearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgPropertySearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgContractSearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchContractor=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchEngineer=new HtmlGraphicImage();
    private HtmlGraphicImage imgClearContract = new HtmlGraphicImage();
    private HtmlGraphicImage imgMaintenanceContractSearch = new HtmlGraphicImage();
    // Site Visit table 
	private HtmlDataTable siteVisitDataTable = new HtmlDataTable();
	private List<SiteVisitView> siteVisitList = new ArrayList<SiteVisitView>();
	
    
	HtmlPanelGroup grpFMRemarks = new HtmlPanelGroup();
	HtmlPanelGroup grpRemarks = new HtmlPanelGroup();
	HtmlPanelGroup grpServiceProviderRemarks = new HtmlPanelGroup();
	// Follow Up History
	private HtmlDataTable followUpDataTable = new HtmlDataTable();
	private List<FollowUpView> followUpList = new ArrayList<FollowUpView>();

    
	private List<SelectItem> requestPriority = new ArrayList<SelectItem>();
    private String hdnPropertyId  = new String();
    private String hdnApplicantId = new String();
    public UnitView  unitView = new UnitView(); 
    public UserView userView = new UserView();
    public ContractView contractView = new ContractView();
    public ComplaintDetailsView complaintDetailsView = new ComplaintDetailsView();
    public ContractView maintenanceContractView = new ContractView();
    protected String txtRemarks;    
    protected String hdnContractorId;
    protected String hdnContractId;
    private String hdnMaintenanceContractSearchContractContractId;
    
	protected String requestId;
	

	private String engineerName;
	private String actualCost;
	private boolean extraWork;
	private String estimatedCost;
	private String priorityValue;
	private Date visitDate;
	
	private int recordSize=0;
	
	private PropertyServiceAgent psa;
	private PaymentScheduleView  paymentInvoice = new PaymentScheduleView();
	private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
	private HtmlDataTable paymentScheduleDataTable;
	
	public HtmlPanelGroup getGrpFMRemarks() {
		return grpFMRemarks;
	}
	public void setGrpFMRemarks(HtmlPanelGroup grpFMRemarks) {
		this.grpFMRemarks = grpFMRemarks;
	}
	public HtmlPanelGroup getGrpRemarks() {
		return grpRemarks;
	}
	public void setGrpRemarks(HtmlPanelGroup grpRemarks) {
		this.grpRemarks = grpRemarks;
	}
	public HtmlPanelGroup getGrpServiceProviderRemarks() {
		return grpServiceProviderRemarks;
	}
	public void setGrpServiceProviderRemarks(
			HtmlPanelGroup grpServiceProviderRemarks) {
		this.grpServiceProviderRemarks = grpServiceProviderRemarks;
	}
	@SuppressWarnings("unchecked")
	public PaymentScheduleView getPaymentInvoice() {
		if(viewMap.get("paymentInvoice") != null )
		{
			paymentInvoice =(PaymentScheduleView)viewMap.get("paymentInvoice") ;
		}
		return paymentInvoice;
	}
	@SuppressWarnings("unchecked")
	public void setPaymentInvoice(PaymentScheduleView paymentInvoice) {
		this.paymentInvoice = paymentInvoice;
		if(this.paymentInvoice != null )
		{
			viewMap.put("paymentInvoice",this.paymentInvoice );
		}
	}
	public HtmlDataTable getPaymentScheduleDataTable() {
		return paymentScheduleDataTable;
	}
	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
		this.paymentScheduleDataTable = paymentScheduleDataTable;
	}
	@SuppressWarnings("unchecked")
	public List<PaymentScheduleView> getPaymentScheduleDataList() 
	{
		if(viewMap.get("paymentScheduleDataList") != null )
		{
			paymentScheduleDataList=(ArrayList<PaymentScheduleView>)viewMap.get("paymentScheduleDataList") ;
		}
		return paymentScheduleDataList;
	}
	
	@SuppressWarnings("unchecked")
	public void setPaymentScheduleDataList(List<PaymentScheduleView> paymentScheduleDataList) 
	{
		this.paymentScheduleDataList = paymentScheduleDataList;
		if(this.paymentScheduleDataList != null )
		{
			viewMap.put("paymentScheduleDataList", this.paymentScheduleDataList); 
		}
		
	}

	@SuppressWarnings("unchecked")
	public ComplaintDetailsView getComplaintDetailsView() 
	{
		if(viewRootMap.containsKey("complaintDetailsView") )
		{
			complaintDetailsView=  (ComplaintDetailsView)viewRootMap.get("complaintDetailsView");
		}
		return complaintDetailsView;
	}

	@SuppressWarnings("unchecked")
	public void setComplaintDetailsView(ComplaintDetailsView complaintDetailsView) {
		
		this.complaintDetailsView = complaintDetailsView;
		if( this.complaintDetailsView != null )
		{
			viewRootMap.put( "complaintDetailsView",this.complaintDetailsView );
		}
	}

	@SuppressWarnings("unchecked")
	public String getEngineerName() {
		if(viewRootMap.containsKey("engineerName") && viewRootMap.get("engineerName")!=null)
			engineerName=  viewRootMap.get("engineerName").toString();
		return engineerName;
	}

	@SuppressWarnings("unchecked")
	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
		
		if(this.engineerName !=null)
			viewRootMap.put("engineerName",this.engineerName);
	}



	public Date getVisitDate() {
		if(viewRootMap.containsKey("visitDate") && viewRootMap.get("visitDate")!=null)
			visitDate= (Date) viewRootMap.get("visitDate");
		
		return visitDate;
	}



	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
		
		if(this.visitDate !=null)
			viewRootMap.put("visitDate",this.visitDate);
	}

	public String getActualCost() {
		if(viewRootMap.containsKey("actualCost") && viewRootMap.get("actualCost")!=null)
			actualCost = viewRootMap.get("actualCost").toString();
		
		return actualCost;
	}



	public void setActualCost(String actualCost) {
		this.actualCost = actualCost;
	}


	public String getEstimatedCost() {
		if(viewRootMap.containsKey("estimatedCost") && viewRootMap.get("estimatedCost")!=null)
			estimatedCost= viewRootMap.get("estimatedCost").toString();
		
		return estimatedCost;
	}



	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
		
		if(this.estimatedCost !=null)
			viewRootMap.put("estimatedCost",this.estimatedCost);
	}
	public String getPriorityValue() {
		if(viewRootMap.containsKey("priorityValue") && viewRootMap.get("priorityValue")!=null)
			priorityValue = viewRootMap.get("priorityValue").toString();
		return priorityValue;
	}

	public void setPriorityValue(String priorityValue) {
		this.priorityValue = priorityValue;
	}
	
	Map requestMap ;


	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public NewMaintenanceRequest(){
		request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getFacesContext().getExternalContext().getSessionMap();
		requestMap = getFacesContext().getExternalContext().getRequestMap();
		context = FacesContext.getCurrentInstance();
		commonUtil = new CommonUtil();
		PROCEDURE_TYPE ="procedureType";
		EXTERNAL_ID ="externalId";
		
		psa = new PropertyServiceAgent();
	}
	
	@Override
	@SuppressWarnings( "unchecked" )
    public void init() 
    {
    	
    	try{
	    	
    			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
		       if( !isPostBack() )
		       {
		    	   initData();
		       }
		       else
		       {
		    	   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		    	   //From Unit Search
		    	   populateUnitorPropertyInfo();
		    	   //From SearchMemberPopUp
		    	   populateEngineer();
		    	   //setPageModeBasedOnRequestStatus();
		       }
    	}
    	catch(Exception ex)
    	{
    		errorMessages = new ArrayList<String>(0);
    		logger.LogException("init|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    }

	/**
	 * @throws Exception
	 * @throws PimsBusinessException
	 */
	@SuppressWarnings("unchecked")
	private void initData() throws Exception, PimsBusinessException 
	{
		   imgSearchContractor.setRendered( false );
		   imgClearContract.setRendered( false );
		   
		   viewRootMap.put(PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_MAINTENANCE_REQUEST);
		   viewRootMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ADD);
		   viewRootMap.put("PERSON_TYPE_LIST", CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE));
		   loadAttachmentsAndComments(null);
		   	    	   
		   
			if( sessionMap.containsKey( WebConstants.TASK_LIST_SELECTED_USER_TASK )  )
			{
		       getDataFromTaskList();
			}
			else if(requestMap.containsKey(WebConstants.REQUEST_VIEW) && requestMap.get(WebConstants.REQUEST_VIEW)!=null)
			{
				requestView = (RequestView)requestMap.get(WebConstants.REQUEST_VIEW);
				this.requestId = requestView.getRequestId().toString();
				
			}
			else if(sessionMap.get(WebConstants.Contract.CONTRACT_ID) != null )
			{
				hdnMaintenanceContractSearchContractContractId = sessionMap.remove(WebConstants.Contract.CONTRACT_ID).toString();
				populateMaintenanceContractSearchContract();
			}
			else if( sessionMap.get("complaintDetails") != null )
			{
				populateFromComplaints();
				
			}
			if(this.requestId!=null)
			{
				putViewValuesInControl();
				setPageModeBasedOnRequestStatus();
				
				if(
						viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT)||
						viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD)||
						viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED)
				  )
				{	
					if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
				    {
				     imgSearchContractor.setRendered(false);
				     imgClearContract.setRendered(true);
				    }
				    else
				    {
					 imgSearchContractor.setRendered(true);
					 imgClearContract.setRendered(false);
				    }
				}
				else
				{
					 imgSearchContractor.setRendered(false);
					 imgClearContract.setRendered(false);
				}
				
				if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW) )
				{
					List<PaymentScheduleView> psList = new PropertyServiceAgent().getPaymentScheduleByRequestID(requestView.getRequestId());
				    if(psList!=null && psList.size()>0)
				    	viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,psList);
				}
			}
	}

	@SuppressWarnings( "unchecked")
	private void populateFromComplaints() throws Exception 
	{
		ComplaintDetailsView cdView  = (ComplaintDetailsView)sessionMap.remove("complaintDetails");
		this.setComplaintDetailsView(cdView);
		if( cdView.getContractId() != null )
		{
			maintenanceContractView = getContractViewFromContractId(  cdView.getContractId() );
			setMaintenanceContractView(maintenanceContractView);
		}
		
		if(cdView.getUnitId() != null)
		{
			populateUnitInfo( cdView.getUnitId() );
		}
		else if( cdView.getPropertyId() != null )
		{
			this.hdnPropertyId  = cdView.getPropertyId().toString(); 
			populatePropertyInfoInTab();
		}
		Long applicantId  = cdView.getApplicantId();
		PersonView applicantView = new PropertyService().getPersonById(applicantId);
		populateApplicantDetails(applicantView);
	}
	
	@SuppressWarnings( "unchecked")
	public void prerender()
	{
		super.prerender();
		try
		{
			 viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND,"0");
			 enableDisableControls();
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("prerender|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked")
	private void createRequestPriorityCombo()
	{
		if(!viewRootMap.containsKey(REQUEST_PRIORITY_COMBO) )
		{
			List<DomainDataView> ddvRequestPriority = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_PRIORITY);
			List<SelectItem> listSelectItem = new ArrayList<SelectItem>(0);
			DomainDataView ddvNormal = CommonUtil.getIdFromType(ddvRequestPriority,WebConstants.REQUEST_PRIORITY_NORMAL);
			DomainDataView ddvUrgent = CommonUtil.getIdFromType(ddvRequestPriority,WebConstants.REQUEST_PRIORITY_URGENT);
		    SelectItem item1 = new SelectItem(ddvNormal.getDomainDataId().toString(),(getIsEnglishLocale()?ddvNormal.getDataDescEn():ddvNormal.getDataDescAr()));
			listSelectItem.add(item1);
			SelectItem item2 = new SelectItem(ddvUrgent.getDomainDataId().toString(),(getIsEnglishLocale()?ddvUrgent.getDataDescEn():ddvUrgent.getDataDescAr()));
			listSelectItem.add(item2);
			viewRootMap.put(REQUEST_PRIORITY_COMBO, listSelectItem);
		}
	}
	@SuppressWarnings( "unchecked")
	private void putViewValuesInControl()throws PimsBusinessException,Exception
	{
		getRequestView();
		
		populateApplicationDetailsTab();
		if(requestView.getCustomerReviewId() != null)
		{
			cmbCustomerReview.setValue(requestView.getCustomerReviewId().toString() );
		}
		if(requestView.getMemsNolReason() != null)
		{
			txtFMRemarks.setValue( requestView.getMemsNolReason() );
		}
		if(requestView.getMaintenanceDetails() != null)
		{
			txtMaintenaceDetails.setValue(requestView.getMaintenanceDetails());
		}
		if(requestView.getMaintenanceRequestSerialNo() != null)
		{
			txtTelephoneNumber.setValue(requestView.getMaintenanceRequestSerialNo());
		}
		else if (viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL) != null )
		{
			txtTelephoneNumber.setValue(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL).toString());
		}
		if(requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().iterator().hasNext())
		{
			
			RequestDetailView rdv = requestView.getRequestDetailView().iterator().next();
			populateUnitInfo(rdv.getUnitView().getUnitId());
		}
		else if(requestView.getPropertyId()!=null)
		{
			this.hdnPropertyId =  requestView.getPropertyId().toString();
			populatePropertyInfoInTab();
		}
		this.setSelectOneMaintenanceType(requestView.getMaintenanceTypeId().toString());
//		this.setSelectManyWorkType( Arrays.asList( requestView.getMaintenanceWorkTypes() ) );
		if(requestView.getMaintenanceWorkTypes() != null && requestView.getMaintenanceWorkTypes().length > 0 )
		{
			this.setSelectOneWorkType( requestView.getMaintenanceWorkTypes()[0] );
		}
		if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
		{
		   this.setContractView(requestView.getContractView());
		   
		   PersonView contractorView = new PersonView();
		   if(getIsEnglishLocale()) {
			   contractorView.setPersonFullName(this.getContractView().getContractorNameEn());
		   }  
		   else {
			   contractorView.setPersonFullName(this.getContractView().getContractorNameAr());
		   }   
		   this.setContractor(contractorView); 
		   if (getContractView().getContractorId()!=null) {
			   this.hdnContractorId(this.getContractView().getContractorId().toString());
		   }
		}
		
		if(requestView.getContactorView()!=null && requestView.getContactorView().getPersonId()!= null)
			this.setContractor(requestView.getContactorView());
		if(requestView.getRequestPriorityId()!=null)
			this.setSelectOneRequestPriority(requestView.getRequestPriorityId().toString());
	    
		
		if (requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().size()>0) {
			
			RequestDetailView requestDetailView = requestView.getRequestDetailView().iterator().next();
			
			if (requestDetailView.getMaintenanceAppContractId()!=null){
				maintenanceContractView = getContractViewFromContractId(requestDetailView.getMaintenanceAppContractId());
				setMaintenanceContractView(maintenanceContractView);
			}
			
		}
		if( requestView.getContractorComments() != null && requestView.getContractorComments().trim().length()> 0 )
		{
			txtServiceProviderRemarks.setValue( requestView.getContractorComments().trim() );
		}
		
		if( requestView.getComplaintDetailsView() != null && requestView.getComplaintDetailsView().getComplaintId() != null )
		{
			this.setComplaintDetailsView( requestView.getComplaintDetailsView() );
		}
		populatePaymentsDataList();
		if(requestView.getRequestId()!=null)
		loadAttachmentsAndComments(requestView.getRequestId());
	}



	/**
	 * @param rdv
	 * @throws PimsBusinessException
	 */
	@SuppressWarnings("unchecked")
	private void populateUnitInfo(long unitId)throws PimsBusinessException 
	{
		HashMap hMap = new HashMap();
		hMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID, unitId);
		List<UnitView> uvList =new PropertyServiceAgent().getPropertyUnitsByCriteria(hMap);
		if(uvList!=null && uvList.size()>0)
		{
			this.setUnitView(uvList.get(0));
		}
	}
	private void hdnContractorId(String string) {
		// TODO Auto-generated method stub
		
	}



	private void hideAll()
	{
		btnContractorExecuted.setRendered(false);
		lblCustomerReview.setRendered(false);
		cmbCustomerReview.setRendered(false);
		btnCustomerRejected.setRendered(false);
		btnResendToContractor.setRendered(false);
		lblServiceProviderRemarks.setRendered(false);
		grpServiceProviderRemarks.setRendered(false);
		txtServiceProviderRemarks.setRendered(false);
		btnSave.setRendered(false);
		btnSend_For_Approval.setRendered(false);
		btnOtherSave.setRendered(false);
		btnSiteVisitReq.setRendered(false);
        btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnSaveSiteVisit.setRendered(false);
		btnCompleteSiteVisit.setRendered(false);
		btnSaveFollowUp.setRendered(false);
        btnComplete.setRendered(false);
        
        imgContractSearch.setRendered(false);
        //imgSearchContractor.setRendered(false);
        imgPropertySearch.setRendered(false);
        imgUnitSearch.setRendered(false);
        imgMaintenanceContractSearch.setRendered(false);
        imgSearchEngineer.setRendered(false);
        txtboxRemarks.setRendered(false);
        lblRemarks.setRendered(false);
        grpRemarks.setRendered( false);
        tbl_Action.setRendered(false);
        tabPaymentTerms.setRendered(false);
        tabSiteVisit.setRendered(false);
		tabFollowUp.setRendered(false);
		cmbRequestPriority.setDisabled(true);
		cmbWorkType.setDisabled(true);
		cmbMaintenanceType.setDisabled(true);
		txtMaintenaceDetails.setDisabled(true);
	}
	public void enableDisableControls()
	{
	    hideAll();
//	    ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		viewRootMap.put("applicationDetailsReadonlyMode", true);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		canAddAttachmentAndComments(true);
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
		{
		
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			viewRootMap.put("applicationDetailsReadonlyMode",false);
			PageModeAdd();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_ADD_MODE));
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{
			PageModeEdit();
			viewRootMap.put("applicationDetailsReadonlyMode",false);
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_ADD_MODE));

		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
			PageModeApprovalRequired();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_APPROVAL_REQUIRED_MODE));
            createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.SITE_VISIT_REQUIRED))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			PageModeSiteVisitRequired();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_SITE_VISIT_MODE));
			createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.FOLLOW_UP))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		    PageModeFollowUp();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_FOLLOWUP_MODE));
		    createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.WITH_CONTRACTOR))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			PageModeWithContractor();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_FOLLOWUP_MODE));
		    createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.CONTRACTOR_EXECUTED))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			PageModeContractorExecuted();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_FOLLOWUP_MODE));
		    createRequestPriorityCombo();
		}
		else if( viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals( Page_Mode.PAGE_MODE_CUSTOMER_REJECTED ) )
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
			PageModeCustomerRejected();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_FOLLOWUP_MODE));
		    createRequestPriorityCombo();
		}
		
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
		{
			//scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
			PageModeView();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
			createRequestPriorityCombo();
		}
		
	}
	private void canAddAttachmentAndComments(boolean canAdd)
	{
		viewRootMap.put("canAddAttachment", canAdd);
		viewRootMap.put("canAddNote", canAdd);
	}
	protected void PageModeAdd()
	{
		btnSave.setRendered(true);
		canAddAttachmentAndComments(true);
		imgPropertySearch.setRendered(true);
        imgUnitSearch.setRendered(true);
        imgMaintenanceContractSearch.setRendered(true);
        cmbWorkType.setDisabled(false);
        cmbMaintenanceType.setDisabled(false);
        txtMaintenaceDetails.setDisabled(false);
        txtTelephoneNumber.setDisabled(false);
	}
	protected void PageModeEdit()
	{
		btnSend_For_Approval.setRendered(true);
		btnSave.setRendered(true);
		canAddAttachmentAndComments(true);
		imgPropertySearch.setRendered(true);
        imgUnitSearch.setRendered(true);
        imgMaintenanceContractSearch.setRendered(true);
        cmbWorkType.setDisabled(false);
        cmbMaintenanceType.setDisabled(false);
        txtMaintenaceDetails.setDisabled(false);
        txtTelephoneNumber.setDisabled(false);
	}
	protected void PageModeApprovalRequired()
	{
		canAddAttachmentAndComments(true);
		btnOtherSave.setRendered(true);
		btnSiteVisitReq.setRendered(true);
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		tbl_Action.setRendered(true);
		imgContractSearch.setRendered(true);
		txtMaintenaceDetails.setDisabled(true);
		//imgSearchContractor.setRendered(true);
		cmbRequestPriority.setDisabled(false);
		txtboxRemarks.setRendered(true);
		tabPaymentTerms.setRendered(true);
		lblRemarks.setRendered(true);
		grpRemarks.setRendered( true);
		tabSiteVisit.setRendered(true);
		txtTelephoneNumber.setDisabled(true);
		if(requestView != null && requestView.getMemsNolReason() != null && requestView.getMemsNolReason().trim().length() > 0)
		{
			txtServiceProviderRemarks.setRendered(true);
			txtServiceProviderRemarks.setReadonly( true);
			lblServiceProviderRemarks.setRendered(true);
			grpServiceProviderRemarks.setRendered(true);
		}
	}
	protected void PageModeSiteVisitRequired()
	{
		canAddAttachmentAndComments(true);
		btnSaveSiteVisit.setRendered(true);
		btnCompleteSiteVisit.setRendered(true);
		tbl_Action.setRendered(true);
		tabSiteVisit.setRendered(true);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		grpRemarks.setRendered( true);
		imgSearchEngineer.setRendered(true);
		txtMaintenaceDetails.setDisabled(true);
		txtTelephoneNumber.setDisabled(true);
	}
	
	protected void PageModeFollowUp()
	{
		lblServiceProviderRemarks.setRendered(true);
		grpServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setRendered(true);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(true);
		btnSaveFollowUp.setRendered(true);
		tabSiteVisit.setRendered(true);
		tabFollowUp.setRendered(true);
		tabPaymentTerms.setRendered(true);
		imgContractSearch.setRendered(false);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		grpRemarks.setRendered( true);
		txtMaintenaceDetails.setDisabled(true);
		txtTelephoneNumber.setDisabled(true);
		
	}
	protected void PageModeCustomerRejected()
	{
		lblCustomerReview.setRendered(true);
		cmbCustomerReview.setRendered(true);
		lblServiceProviderRemarks.setRendered(true);
		grpServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setDisabled(true);
		txtServiceProviderRemarks.setStyleClass("READONLY");
		
		tbl_Action.setRendered(true);
		btnResendToContractor.setRendered(true);
		btnSiteVisitReq.setRendered(true);
//		btnSiteVisitReq.setRendered(true);
		btnComplete.setRendered(true);
		tabSiteVisit.setRendered(true);
		tabPaymentTerms.setRendered(true);
		imgContractSearch.setRendered(false);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		grpRemarks.setRendered( true);
		txtMaintenaceDetails.setDisabled(true);
		txtTelephoneNumber.setDisabled(true);
		
	}
	
	protected void PageModeContractorExecuted()
	{
		
		txtFMRemarks.setDisabled(true);
		lblCustomerReview.setRendered(true);
		cmbCustomerReview.setRendered(true);
		lblServiceProviderRemarks.setRendered(true);
		grpServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setRendered(true);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(true);
		btnSaveFollowUp.setRendered(false);
		btnCustomerRejected.setRendered(true);
		txtServiceProviderRemarks.setDisabled(true);
		txtServiceProviderRemarks.setStyleClass("READONLY");
		tabSiteVisit.setRendered(true);
		tabPaymentTerms.setRendered(true);
		imgContractSearch.setRendered(false);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		grpRemarks.setRendered(true );
		txtMaintenaceDetails.setDisabled(true);
		txtTelephoneNumber.setDisabled(true);
		
	}
	protected void PageModeWithContractor()
	{
		btnContractorExecuted.setRendered(true);
		lblServiceProviderRemarks.setRendered(true);
		grpServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setRendered(true);
		txtServiceProviderRemarks.setDisabled(false);
		txtServiceProviderRemarks.setReadonly( false);
		tbl_Action.setRendered(true);
		tabSiteVisit.setRendered(true);
		
		imgContractSearch.setRendered(false);
		
		tabPaymentTerms.setRendered(true);
		txtMaintenaceDetails.setDisabled(true);
		txtTelephoneNumber.setDisabled(true);
		
	}
	
	protected void PageModeView()
	{
	  lblCustomerReview.setRendered(true);
	  cmbCustomerReview.setRendered(true);
	  cmbCustomerReview.setDisabled(true);
	  lblServiceProviderRemarks.setRendered(true);
	  grpServiceProviderRemarks.setRendered(true);
	  txtServiceProviderRemarks.setRendered(true);
	  txtServiceProviderRemarks.setReadonly(true);
	  txtServiceProviderRemarks.setStyleClass("READONLY");
//	  tabFollowUp.setRendered(true);
	  tabSiteVisit.setRendered(true);
	  tabPaymentTerms.setRendered(true);
	  tbl_Action.setRendered(true);
	  txtboxRemarks.setRendered(true);
	  txtboxRemarks.setReadonly(true);
	  txtboxRemarks.setStyleClass("READONLY");
	  lblRemarks.setRendered(true);
	  grpRemarks.setRendered( true);
	  imgViewUnitDetails.setRendered(true);
	  txtMaintenaceDetails.setDisabled(true);
	  txtTelephoneNumber.setDisabled(true);
		
	}
    public boolean getIsPageModeSiteVisitRequired()
    {
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.SITE_VISIT_REQUIRED))
    		return true ;
    	else
    		return false;
    	
    }
    public boolean getIsPageModeFollowUp()
    {
    	
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.FOLLOW_UP))
    		return true ;
    	else
    		return false;
    	
    }

	public void populatePropertyInfoInTab()
	{
		
		try {
			
			if(hdnPropertyId!=null && hdnPropertyId.trim().length()>0)
			{
			  PropertyServiceAgent psa = new PropertyServiceAgent();
			  PropertyView propertyView = psa.getPropertyByID(new Long(hdnPropertyId));
			  unitView = new UnitView();
			  unitView.setPropertyId(new Long(hdnPropertyId));
			  unitView.setPropertyCommercialName(propertyView.getCommercialName());
			  unitView.setPropertyNumber(propertyView.getPropertyNumber());
			  unitView.setPropertyEndowedName(propertyView.getEndowedName());
			  unitView.setPropertyCategoryId(propertyView.getCategoryId());
			  unitView.setPropertyCategoryEn(propertyView.getCategoryTypeEn());
			  unitView.setPropertyCategoryAr(propertyView.getCategoryTypeAr());
			  unitView.setUnitAddress(propertyView.getAddressLineOne());
			   if(viewRootMap.get(MAINTENANCE_CONTRACT_VIEW)!=null) 
			   { // clean up the contractView
	 			   viewRootMap.remove(MAINTENANCE_CONTRACT_VIEW);
	 		   }
			  this.setUnitView(unitView);
			  this.getUnitView();
			}
		
		
		} catch (Exception e) 
		{
			logger.logInfo("populatePropertyInfoInTab|ErrorOccured::",e);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	private void populateEngineer()
	{
		if(sessionMap.containsKey(WebConstants.USER_VIEW))
		{
			this.setUserView((UserView)sessionMap.get(WebConstants.USER_VIEW));
		    sessionMap.remove(WebConstants.USER_VIEW);
		}
		
	}
	@SuppressWarnings("unchecked")
	public void populateContract()
	{
		if(sessionMap.containsKey(WebConstants.CONTRACT))
		{
			if( this.getContractView() != null  )
			{
				contractView = this.getContractView();
			
			this.setContractView((ContractView)sessionMap.remove(WebConstants.CONTRACT));
			String auditTrail="";
			if(	viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL )!= null)
			{
				auditTrail += viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ).toString();
			}
			auditTrail +=  
							CommonUtil.getParamBundleMessage(
															 "maintenancerequest.event.serviceContractAdded",
															 "en",
															 contractView.getContractNumber()
															)
						   +"&&"+
							CommonUtil.getParamBundleMessage(
																 "maintenancerequest.event.serviceContractAdded",
																 "ar",
																 contractView.getContractNumber()
															)+",";
			viewMap.put( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL,auditTrail );
		}
		    this.setHdnContractorId(this.getContractView().getContractorId().toString());
		    populateContractorForContract();
		}
		
	}
	
	private ContractView getContractViewFromContractId(Long contractId) throws PimsBusinessException
	{
		ContractView contract= new ContractView();
		contract.setContractId(contractId);
		HashMap<String,Object> contractHashMap=new HashMap<String,Object>();
		contractHashMap.put("contractView",contract);
		contractHashMap.put("dateFormat",getDateFormat());
		List<ContractView> contractViewList = psa.getAllContracts(contractHashMap);
		if (	
				contractViewList != null && 
				contractViewList.size() > 0	
		   ) 
		{
			contract = contractViewList.get(0);
		}		
		return contract;
	}
	
	public void populateContractor()
	{
		try
		{
			this.setContractor(getContractorInfo(false));
		}
		catch(PimsBusinessException e)
		{
			logger.LogException("populateContractor|Error Occured::", e);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void populateContractorForContract()
	{
		try
		{
			imgSearchContractor.setRendered(false);
			imgClearContract.setRendered(true);
			this.setContractor(getContractorInfo(true));

		}
		catch(PimsBusinessException e)
		{
			logger.LogException("populateContractor|Error Occured::", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void populateMaintenanceContractSearchContract()
	{
		if (hdnMaintenanceContractSearchContractContractId==null || hdnMaintenanceContractSearchContractContractId.trim().length()<=0){return;}
		try 
		{
			logger.logInfo("populateMaintenanceContractSearchContract|ContractId:%s",hdnMaintenanceContractSearchContractContractId);
			ContractView contract = new ContractView();
			contract.setContractId(new Long(hdnMaintenanceContractSearchContractContractId));
			HashMap<String,Object> contractHashMap=new HashMap<String,Object>();
			contractHashMap.put("contractView",contract);
			contractHashMap.put("dateFormat",getDateFormat());
			List<ContractView> contractViewList = psa.getAllContracts(contractHashMap);
			if (contractViewList!=null && contractViewList.size()>0)
			{
				maintenanceContractView = contractViewList.get(0);
				for (ContractUnitView contractUnitView : maintenanceContractView.getContractUnitView())
				{
					unitView = contractUnitView.getUnitView();
					setUnitView(unitView);
				}
				setMaintenanceContractView(maintenanceContractView);
				validateContractUnit();
			}
			logger.logInfo("populateMaintenanceContractSearchContract|Finish");
		}
		catch (Exception e) 
		{
			logger.LogException( "Crashed populateMaintenanceContractSearchContract()",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onDeleteMaintenanceContract()
	{
		try
		{
			contractView = this.getContractView();
			String auditTrail="";
			if(	viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL )!= null)
			{
				auditTrail += viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ).toString();
			}
			auditTrail +=
							CommonUtil.getParamBundleMessage(
															 "maintenancerequest.event.serviceContractRemoved",
															 "en",
															 contractView.getContractNumber()
															)
						   +"&&"+
							CommonUtil.getParamBundleMessage(
																 "maintenancerequest.event.serviceContractRemoved",
																 "ar",
																 contractView.getContractNumber()
															)+",";
			viewMap.put( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL,auditTrail );
			this.setContractView(null);
			viewRootMap.remove(CONTRACT_VIEW);
			this.setHdnContractorId(null);
			this.setContractor(null);
			
			viewRootMap.remove(CONTRACTOR_VIEW);
			imgSearchContractor.setRendered(true);
			imgClearContract.setRendered(false);
			
			
			contractView = null;
		}
		catch ( Exception e) 
		{
			logger.LogException( "Crashed onDeleteMaintenanceContract()",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
    private void populateUnitorPropertyInfo()throws Exception
    {
	   if(!sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT)){return;}
       
	   this.setUnitView((UnitView)sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT));
	   sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);  
	
	   if(viewRootMap.get(MAINTENANCE_CONTRACT_VIEW)!=null)
	   {  // clean up the contractView
		   viewRootMap.remove(MAINTENANCE_CONTRACT_VIEW);
	   }
	   validateContractUnit();
	   
    }

	private void saveCommentAndAttachements(String eventDesc)throws Exception 
	{
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		if(requestView != null && requestView.getRequestId()!=null)
		 {
		     saveComments(requestView.getRequestId());
			 saveAttachments(requestView.getRequestId().toString());
		 }
		 saveSystemComments(eventDesc);
	}	
    private boolean hasError() throws Exception
	{
	 	boolean hasErrors =false;
	 	errorMessages = new ArrayList<String>(0);
	 		
	 		/////Validate Application Details Tab Start///////////////
		 	if(hdnApplicantId!=null && hdnApplicantId.trim().length()<=0)
		 	{ 
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			   tabPanel.setSelectedTab(TAB_ID.ApplicationDetails);
			   return true;
		 	}
	 		/////Validate Application Details Tab Finish///////////////
		 	
		    /////Validate Unit/Property Details Tab Start///////////////
		 	else if(this.getUnitView()==null || (this.getUnitView().getUnitId()==null && this.getUnitView().getPropertyId()==null))
            {
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.UNIT_OR_PROPERTY_REQ));
				tabPanel.setSelectedTab(TAB_ID.MaintenanceInfo);
				return true;
            	
            }
		/////Validate if Contractor or Contract is present..Finish///////////////

		 	else if (validateContractUnit()) 
		 	{
		 		tabPanel.setSelectedTab(TAB_ID.MaintenanceInfo);
				return true;
		 	}
            /////Validate Unit/Property Details Tab Finish///////////////
	 		
	 		/////Validate Attachment Tab Start///////////////
		 	else if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		hasErrors = true;
	    		tabPanel.setSelectedTab(TAB_ID.Attachment);
	        	return true;
	    	}
		    /////Validate Attachment Tab Finish///////////////
		    
		 	/////Validate if Contractor or Contract is present..Start///////////////
		 	else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED)&&
		 			 (this.getContractView()==null || this.getContractView().getContractId()==null) && 
		 			 (this.getContractor()==null || this.getContractor().getPersonId()==null))
		 	{
		 			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.CONTRACT_OR_CONTACTOR_REQ));
	    		    hasErrors = true;
		 	}
		 	if(txtMaintenaceDetails == null || txtMaintenaceDetails.getValue() == null || txtMaintenaceDetails.getValue().toString().trim().length() <= 0)
		 	{
		 		errorMessages.add(CommonUtil.getBundleMessage("errMsg.maintenanceDtlRequired"));
    		    hasErrors = true;
		 	}
		 	if(  txtTelephoneNumber == null || txtTelephoneNumber.getValue()==null || txtTelephoneNumber.getValue().toString().trim().length() <=0 )
		 	{
		 		errorMessages.add(CommonUtil.getBundleMessage("errMsg.maintenanceContactNoReq"));
    		    hasErrors = true;
		 		
		 	}
	 	return hasErrors;
		
	}
	private boolean validateContractUnit() throws Exception 
	{
		boolean isNotValid = false;
		
		if ( !isContrctStatusActive() ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("newMaintenanceApplication.error.contractInActive"));
			isNotValid = true;
		} 
		else 
		{
			errorMessages.remove(CommonUtil.getBundleMessage("newMaintenanceApplication.error.contractInActive"));
		}
		return isNotValid;
	}
	
	private boolean isRequestExistsForContractUnit() throws PimsBusinessException 
	{
		boolean isRequestExistsForContractUnit = false;
		if ( getRequestId()!=null && !"".equalsIgnoreCase(getRequestId().trim())) 
		{
			// If the request is just saved that is the request is in Edit Mode
			return isRequestExistsForContractUnit;
		}
		HashMap<String, Object> searchCriteriaMap = new HashMap<String, Object>();
		RequestView requestView = new RequestView();
		//add unitId
		if ( getUnitView()!=null )
		{
	        if( getUnitView().getUnitId()!=null) 
			{
				searchCriteriaMap.put("requestDetailUnitId", getUnitView().getUnitId());
			}
			else if( this.getUnitView().getPropertyId() != null )
	    	{
				searchCriteriaMap.put("propertyId", getUnitView().getPropertyId() );
	    	}
		
		}
		List<Long> requestStatusList = new ArrayList<Long>(0);
		//requestStstuaes
		List<DomainDataView> ddvRequestStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		requestStatusList.add(CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId());
		Long ddClosed = CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_CLOSED).getDomainDataId();
		if( ddClosed != null )
		{
		 requestStatusList.add(CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_CLOSED).getDomainDataId());
		}
		requestStatusList.add(CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_REJECTED).getDomainDataId());
		requestStatusList.add(CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_CANCELED).getDomainDataId());
		requestStatusList.add(CommonUtil.getIdFromType(ddvRequestStatusList, WebConstants.REQUEST_STATUS_FINAL_LIMITATION_DONE).getDomainDataId());
		searchCriteriaMap.put("requestNotHavingFollowingStatus", requestStatusList );
		//requestTypes
		Long requestTypeId = WebConstants.REQUEST_TYPE_MAINTENANCE_APPLICATION; 
		requestView.setRequestTypeId(requestTypeId);
		searchCriteriaMap.put("requestView", requestView);
		List<RequestView> requests = psa.getAllMaintenanceRequests(requestView, null, null, searchCriteriaMap);
		if (requests!=null && requests.size()>0) 
		{
			String requestNumbers="";
			for (RequestView reqView: requests) 
			{
				requestNumbers+= reqView.getRequestNumber() + ", "; 
			}
			errorMessages.add(CommonUtil.getParamBundleMessage("newMaintenanceApplication.error.requestsExistsForSelectedContractUnit",requestNumbers.substring(0, requestNumbers.length()-2)));
			isRequestExistsForContractUnit = true;
		}
		return isRequestExistsForContractUnit;
	}

	public boolean isContrctStatusActive() 
	{
		boolean isContrctStatusActive = false;
		DomainDataView ddv= CommonUtil.getIdFromType(getContractStatusList(),WebConstants.CONTRACT_STATUS_ACTIVE);
		if (getMaintenanceContractView()!=null && getMaintenanceContractView().getStatus()!=null) 
		{
			if(  getMaintenanceContractView().getStatus().toString().equals( ddv.getDomainDataId().toString() )  ) 
			{
				isContrctStatusActive= true;
			}	
			else 
			{
				isContrctStatusActive= false;
			}
		}
		if (getMaintenanceContractView()==null || getMaintenanceContractView().getStatus()==null) 
		{
			// for the cases where we dont have contract status
			isContrctStatusActive = true;
		}
		
		return isContrctStatusActive;
	}
	
	public List<DomainDataView> getContractStatusList()  {
		List<DomainDataView> list = null;
   	 if( viewRootMap.get("contractStatusList" )== null) {
   		 list = CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
   		 viewRootMap.put( "contractStatusList", list );
   	 } 
   	 else {
   		 list = ( ArrayList<DomainDataView> )viewRootMap.get( "contractStatusList" );
   	 }	 
       return list;
    }



	private void setRequestView()
	{
		
		if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
			requestView = new RequestView();
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUserId());
            requestView.setIsDeleted(new Long(0));
            requestView.setRecordStatus(new Long(1));
            
            DomainDataView ddRequestStatus = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
            		                         WebConstants.REQUEST_STATUS_NEW);
            requestView.setStatusId(ddRequestStatus.getDomainDataId());  
            requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_MAINTENANCE_APPLICATION);
            requestView.setRequestDate(new Date());
            DomainDataView ddRequestOrgination= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_ORIGINATION), WebConstants.REQUEST_ORIGINATION_CSC);
            requestView.setRequestOrigination(ddRequestOrgination.getDomainDataId());
        }
		else
		{
		   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		}
		PersonView applicantView =new PersonView();
	    applicantView.setPersonId(new Long(hdnApplicantId));
	    requestView.setApplicantView(applicantView);
		requestView.setMaintenanceTypeId(new Long(this.getSelectOneMaintenanceType()));
//		String[] workTypes=new String[ this.getSelectManyWorkType().size() ]; 
//		workTypes = this.getSelectManyWorkType().toArray( workTypes); 
//        requestView.setMaintenanceWorkTypes( workTypes );
		String[] workTypes=new String[ 1 ]; 
		workTypes[0] = this.getSelectOneWorkType(); 
        requestView.setMaintenanceWorkTypes( workTypes );
        if(txtMaintenaceDetails != null && txtMaintenaceDetails.getValue() != null && txtMaintenaceDetails.getValue().toString().trim().length() > 0)
		{
			 requestView.setMaintenanceDetails(txtMaintenaceDetails.getValue().toString());
		}
        if(txtTelephoneNumber != null && txtTelephoneNumber.getValue() != null && txtTelephoneNumber.getValue().toString().trim().length() > 0)
		{
			 requestView.setMaintenanceRequestSerialNo( txtTelephoneNumber.getValue().toString() );
		}
        //If a maintenance contract is present for this property/unit
        if(this.getContractView()!=null && this.getContractView().getContractId()!=null) 
        {
            requestView.setContractView(this.getContractView());
        }  
        else if(requestView.getContractId()!=null)
        {
        	requestView.setContractId(null);
        	requestView.setContractView(null);
        }
        
        if (this.getSelectOneCmbCustomerReview() !=null && !this.getSelectOneCmbCustomerReview().equals("-1") ) 
        {
        	requestView.setCustomerReviewId(  new Long (this.getSelectOneCmbCustomerReview()) );
        }
        
        if (this.getMaintenanceContractView()!=null && this.getMaintenanceContractView().getContractId()!=null) 
        {
        	RequestDetailView requestDetailView = new RequestDetailView();
    		
        	if (requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().size()>0) 
        	{
        		requestDetailView =	requestView.getRequestDetailView().iterator().next();
        		
        		if (requestDetailView == null) 
        		{
        			requestDetailView = new RequestDetailView();
        		}	
        		
        		requestDetailView.setMaintenanceAppContractId(getMaintenanceContractView().getContractId());
        	} 
        	else 
        	{
        		requestDetailView.setMaintenanceAppContractId(getMaintenanceContractView().getContractId());
        		Set<RequestDetailView> requestDetailViewSet = new HashSet<RequestDetailView>(0);
        		requestDetailViewSet.add(requestDetailView);
				requestView.setRequestDetailView(requestDetailViewSet);
        	}
        	
        }	
        
        if(this.getContractView()!=null && this.getContractView().getContractId()!=null
        	&&	this.getContractor()!=null && this.getContractor().getPersonId()!=null)
        {
        	requestView.setContactorView(null);
        }
        else if(this.getContractor()!=null && this.getContractor().getPersonId()!=null)
        {
        	requestView.setContactorView(this.getContractor());
        }
        if(this.getUnitView() != null )
        {
        	if(this.getUnitView().getUnitId()== null && this.getUnitView().getPropertyId() != null)
        	{
        	     requestView.setPropertyId(this.getUnitView().getPropertyId());
        	}
            else if(this.getUnitView().getUnitId()!= null )
            {
            	getRequestDetailSetForUnit(requestView);
            }
        }
        if(this.getSelectOneRequestPriority()!=null && this.getSelectOneRequestPriority().trim().length()>0)
        {
        	requestView.setRequestPriorityId(new Long(this.getSelectOneRequestPriority()));
        }
        if( this.getComplaintDetailsView() != null && this.getComplaintDetailsView().getComplaintId() != null )
        {
        	requestView.setComplaintDetailsView( this.getComplaintDetailsView() );
        }
		requestView.setUpdatedBy(getLoggedInUserId());
        requestView.setUpdatedOn(new Date());
        
	}
	 public Set<RequestDetailView> getRequestDetailSetForUnit(RequestView requestView)
	 {
	       Set<RequestDetailView> requestDetailSet=new HashSet<RequestDetailView>();
	       RequestDetailView requestDetailView ;
    	   
    	   if(requestView.getRequestId()==null )
    	   {
    		   requestDetailView = new RequestDetailView();
    		   
    		   if (requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().size()>0) {
        		   requestDetailView = (RequestDetailView)requestView.getRequestDetailView().iterator().next();  
        	   }
    		   
    		   requestDetailView.setCreatedOn(new Date());
        	   requestDetailView.setCreatedBy(getLoggedInUserId());
        	   
        	
    	   }
    	   else
    	   {
    		   requestDetailView = (RequestDetailView)requestView.getRequestDetailView().iterator().next();
    	   }
    	   requestDetailView.setUnitView(this.getUnitView());
    	   requestDetailView.setUpdatedBy(getLoggedInUserId());
    	   requestDetailView.setUpdatedOn(new Date());
    	   requestDetailView.setRecordStatus(new Long(1));
    	   requestDetailView.setIsDeleted(new Long(0));
		   requestDetailSet.add(requestDetailView);
		   requestView.setRequestDetailView(requestDetailSet);
		   return requestDetailSet;
	 }
	private void putControlValuesinViews() throws Exception
	{
         //Set Request View 
         setRequestView();
	}
	@SuppressWarnings( "unchecked" )
	public void onSendForApproval()
	{
	
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
	    try 
	    {
	        String endPoint= parameters.getParameter("PIMSNewMaintenanceRequestBPELEndPoint");
	        PIMSNewMaintenanceRequestPortClient port=new PIMSNewMaintenanceRequestPortClient();
	   	    port.setEndpoint(endPoint);
	   		if(this.requestId!=null && this.requestId.trim().length()>0)
	   		{
			   	 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			   	
//			   	 sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
			   	 requestView.setOldStatusId( requestView.getStatusId() );
			   	requestView.setOldStatusId(requestView.getStatusId());
				requestView.setStatusId(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID);
				requestView.setUpdatedBy(getLoggedInUserId());
				requestView.setUpdatedOn(new Date());
				requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
//			   	 PropertyService.sendMaintenanceContractForApproval(requestView, getLoggedInUserId());
			   	 
			   	 port.initiate(getLoggedInUserId(), Integer.valueOf( requestView.getRequestId().toString() ), requestView.getRequestNumber(),null, null);
			   	getRequestView();
			   	 saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL) ;
			   	 notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest_Submiision);
		   		 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
	   	         successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
	   	         imgSearchContractor.setRendered(true);
	   	         CommonUtil.printReport(requestView.getRequestId());
	   		}
	    }
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException("onSendForApproval| Error Occured...",ex);
    	}
	}
	public void onSave()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasError())
			{
				 putControlValuesinViews();
					
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
				 {
				     eventDesc  = MessageConstants.RequestEvents.REQUEST_CREATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_ADDED); 
				 }
				 else 
				 {
				     eventDesc  = MessageConstants.RequestEvents.REQUEST_UPDATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_UPDATED); 
				 }
				 
		         requestView = rsa.persistMaintenanceRequest(requestView);
		         this.setRequestId(requestView.getRequestId().toString());
		         getRequestView();
				 populateApplicationDetailsTab();
				 saveCommentAndAttachements(eventDesc);
				 if(
						 requestView.getComplaintDetailsView() != null &&
						 requestView.getComplaintDetailsView().getRequestId() != null &&
						 eventDesc.equals(MessageConstants.RequestEvents.REQUEST_CREATED) 
				   )
				 {
					 saveSystemCommentsOnComplaint(requestView.getComplaintDetailsView().getRequestId(),requestView.getRequestNumber() );
				 }
				 saveContractorContractAuditTrail();
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD) || viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT) )
				    viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				 else
					 this.setTxtRemarks("");
				 
				 successMessages.add(successMsg);
				 
			}
		}
		catch(Exception e)
		{
			logger.LogException("onSave|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}
	public void tabSiteVisit_Click()
	{

		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			if(this.getSiteVisitList()==null || this.getSiteVisitList().size()<=0)
			{
			     requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			     siteVisitList =  rsa.getSiteVisitsByRequest(requestView.getRequestId());
			     this.setSiteVisitList(siteVisitList);
			}
			     this.setRecordSize(this.getSiteVisitList().size());
				    	
			  
		}
		catch(Exception e)
		{
			logger.LogException( "tabSiteVisit_Click|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		

	}
	private boolean hasSaveSiteVisitContainsError()
	{
		boolean hasSiteVisitError = false;
		if( this.extraWork )
		{
			if(this.getEstimatedCost()== null  || this.getEstimatedCost().trim().length()==0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.ESTIMATED_COST_REQ));
				hasSiteVisitError = true;
			}
			else
			{
				try {
					new Double(this.getEstimatedCost());
				} catch (Exception e) {
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ESTIMATED_COST));
					hasSiteVisitError = true;
				}
				
			}
		}
		if(this.getVisitDate()==null)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.VISIT_DATE_REQ));
			hasSiteVisitError = true;
		}
		if(this.getUserView()== null || this.getUserView().getUserName()==null)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.ENGINEER_NAME_REQ));
			hasSiteVisitError = true;
		}
		
		return hasSiteVisitError;
	}
	private void setSiteVisit(SiteVisitView svv)
	{
        
		svv.setRemarks(this.txtRemarks);
        svv.setVisitDate(this.getVisitDate());
        svv.setEngineerName( this.getUserView().getUserName());
        svv.setRequestId(Long.valueOf( requestView.getRequestId()) );
        if( this.extraWork )
        {
         svv.setEstimatedCost( Double.valueOf(this.getEstimatedCost()));
        }
        svv.setCreatedBy(getLoggedInUserId());
        svv.setCreatedOn(new Date());
        svv.setUpdatedBy(getLoggedInUserId());
        svv.setUpdatedOn(new Date());
        svv.setIsDeleted(new Long(0));
        svv.setRecordStatus(new Long(1));
        
	}
	private void clearSiteVisitFields()
	{
		this.setTxtRemarks("");
		this.setEstimatedCost("");
		this.setEngineerName("");
		this.setVisitDate(null);
		viewRootMap.remove("visitDate");
		this.setUserView(null);
		viewRootMap.remove(WebConstants.USER_VIEW);
	}
	public void onSaveSiteVisit()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasSaveSiteVisitContainsError())
			{
				 tabSiteVisit_Click();
				 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
				 SiteVisitView siteVisitView = new SiteVisitView();
				 setSiteVisit(siteVisitView);
				 eventDesc  = MessageConstants.RequestEvents.REQUEST_SAVED_SITE_VISIT;
				 successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.SITE_VISIT_SAVED_SUCCESSFULLY); 
				 siteVisitView = rsa.addSiteVisit(siteVisitView);
				 
				 saveCommentAndAttachements(eventDesc);
				 successMessages.add(successMsg);
				 tabPanel.setSelectedTab(TAB_ID.SiteVist);
				 this.getSiteVisitList().add(siteVisitView);
				 clearSiteVisitFields();
				 
				 imgSearchContractor.setRendered(false);
				 imgClearContract.setRendered(false);
				 imgContractSearch.setRendered(false);
			}
		}
		catch(Exception e)
		{
			logger.LogException( "onSaveSiteVisit||Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}



	
	public void onCompleteSiteVisit()
	{
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			    	
					setTaskOutCome(TaskOutcome.OK);
					//No old status will be set on this step so we could know whether the 
					//task was sent by FM  from approval requried step or customer rejected step. 
					//requestView.setOldStatusId(requestView.getStatusId());
					requestView.setStatusId(WebConstants.REQUEST_STATUS_VISIT_DONE_ID);
					requestView.setUpdatedBy(getLoggedInUserId());
					requestView.setUpdatedOn(new Date());
					requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);


			    	saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SITE_VISIT_DONE);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.SITE_VISIT_COMPLETED));
			    	tabSiteVisit.setRendered(true);
			    	tabSiteVisit_Click();
			    	
			    	requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			    	List<RequestView>requestViewList= new PropertyServiceAgent().getAllRequests(requestView, null, null, null);
			    	
					if(requestViewList.size()>0 ||viewRootMap.containsKey(REQUEST_VIEW))
					{
						if(requestViewList.size()>0 )
						{
						requestView =requestViewList.get(0); 
						viewRootMap.put(REQUEST_VIEW,requestView );
						}
						 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);  
					     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
					     {
					      imgSearchContractor.setRendered(false);
					      imgClearContract.setRendered(true);
					     }
					     else
					     {
						  imgSearchContractor.setRendered(true);
						  imgClearContract.setRendered(false);
					     }
						 
					}
					viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "onCompleteSiteVisit|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	
	@SuppressWarnings("unchecked")
	public void onAddInvoicePayment()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		try 
		{
		  makePaymentSchedule(getPaymentInvoice());
		  setPaymentInvoice(new PaymentScheduleView());
		}
		catch(Exception e)
		{
			logger.LogException( "onAddInvoicePayment|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
	}
	public void onEditInvoicePayment() 
	{
    	try
    	{
    		PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
    		setPaymentInvoice(psv);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onEditInvoicePayment| Error Occured...",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}

	}
	
	public void onDeleteInvoicePayment() 
	{
    	try
    	{
	    	PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
	    	List<PaymentScheduleView> paymentList = getPaymentScheduleDataList();
	    	if(psv.getPaymentScheduleId() == null )
	    	{
	    		paymentList.remove(paymentScheduleDataTable.getRowIndex());
	    		setPaymentScheduleDataList(paymentList);
	    	}
	    	else
	    	{
	    	  psv.setIsDeleted(1L);
	    	  psv.setUpdatedOn(new Date());
	    	  psv.setUpdatedBy( getLoggedInUserId() );
	    	  psa.updatePaymentSchedule(psv);
	    	  populatePaymentsDataList();
//	    	  paymentList.set(paymentScheduleDataTable.getRowIndex(),psv);
	    	  
	    	  
	    	}
	    	 
    	
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onDeleteInvoicePayment| Error Occured...",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}

	}
	
	public void tabFollowUp_Click()
	{
		
    	String methodName = "tabFollowUp_Click";
		logger.logInfo(methodName +"|Start");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			    if(this.getFollowUpList()!=null && this.getFollowUpList().size()<=0)
			    {
			     requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			     sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
			     followUpList  =  rsa.getFollowupsByRequest(requestView.getRequestId());
				 this.setFollowUpList(followUpList);
			    }
			    this.setRecordSize(this.getFollowUpList().size());
	    	
		}
		catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		

	}
	
	private void setFollowUpView(FollowUpView followUpView)
	{
        
		if(followUpView!=null && followUpView.getFollowUpId()!=null)
		{
			followUpView.setRemarks(this.txtRemarks);
			 if( this.getActualCost() != null && !this.getActualCost().equals("") )
		        	followUpView.setActualCost( Double.valueOf(this.getActualCost()) ) ;
		}
		else
		{
			List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
		    ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpType.FOLLOW_UP_TYPE);
			DomainDataView ddFollowUpType = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpType.CUSTOM); 
	        ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS);
	        DomainDataView ddFollowUpStatus = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpStatus.CLOSE);
			followUpView.setRemarks(this.txtRemarks);
	        followUpView.setRequestId( Long.valueOf( this.getRequestId() ) );
	        if( this.getActualCost() != null && !this.getActualCost().equals("") )
	        	followUpView.setActualCost( Double.valueOf(this.getActualCost()) ) ; 
	        followUpView.setFollowupDate(new Date());
	        followUpView.setCreatedBy(getLoggedInUserId());
	        followUpView.setUpdatedBy(getLoggedInUserId());
	        followUpView.setCreatedOn(new Date());
	        followUpView.setUpdatedOn(new Date());
	        followUpView.setStartDate( new Date()); 
	        followUpView.setFollowUpTypeId(ddFollowUpType.getDomainDataId()); // Will not be used but setting the value due to not null
	        followUpView.setStatusId(ddFollowUpStatus.getDomainDataId() ); // Will not be used but setting the value due to not null
	        followUpView.setIsDeleted(0L);
	        followUpView.setRecordStatus(1L);
		}
		 if(viewRootMap.containsKey("SELECTED_FOLLOWUP"))
			 viewRootMap.remove("SELECTED_FOLLOWUP");
	}
	private void clearFollowUpFields()
	{
		this.setActualCost("");
		this.setTxtRemarks("");
	}
	
	private void makePaymentSchedule(PaymentScheduleView obj) throws Exception
	{

		List<DomainDataView> ddvListStatus = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);
		
		obj.setRequestId(Long.valueOf(this.getRequestId()));
		
		if(obj.getPaymentScheduleId() == null)
		{
			DomainDataView ddvDraft = CommonUtil.getIdFromType(ddvListStatus, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
			obj.setRequestId( requestView.getRequestId() );
			obj.setIsDeleted(0L);
			obj.setRecordStatus(1L);
			obj.setCreatedOn(new Date());
			obj.setCreatedBy(getLoggedInUserId());
			obj.setTypeId(WebConstants.PAYMENT_TYPE_MAINTENANCE_EXPENSES_ID);
			
			obj.setDirectionTypeInbound(0L);
			obj.setIsReceived("N");
			  
			obj.setOwnerShipTypeId(this.getUnitView().getPropertyCategoryId());
			obj.setStatusId(ddvDraft.getDomainDataId());
			obj.setStatusAr(ddvDraft.getDataDescAr());
			obj.setStatusEn(ddvDraft.getDataDescEn());
		}
		
		obj.setUpdatedOn(new Date());
		obj.setUpdatedBy(getLoggedInUserId());
		obj.setDescription("Maintenance Request Payment");
		
		psa.addPaymentScheduleByPaymentSchedule(obj);
		populatePaymentsDataList();
		

       
	}
	/**
	 * @param ps
	 * @throws PimsBusinessException
	 */
	private void populatePaymentsDataList()throws Exception 
	{
		this.setPaymentScheduleDataList( psa.getPaymentScheduleByRequestID( requestView.getRequestId() ) );
	}
	private boolean hasSaveFollowUpErrors()
	{
		boolean hasSaveFollowUpErrors=false;
/*		if(this.getActualCost()!=null && this.getActualCost().trim().length()==0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.ACTUAL_COST_REQ));
			return true;		
		}
		else
		*//******Recently Commented*****//*
		{
			try {
		          new Double(this.getActualCost());		
			} catch (Exception e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
				return true;
			}
			
		}*/
		if(this.getActualCost()!=null && this.getActualCost().trim().length()!=0)
		{
			try {
		        new Double(this.getActualCost());		
			} catch (Exception e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
				return true;
			}
		}
		if(this.getActualCost()!=null && !StringHelper.isEmpty(this.getActualCost()))
		{
			Double cost;
			cost=Double.parseDouble(this.getActualCost());
			if(cost.compareTo(0D)<=0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
				return true;
			}
				
		}
		return hasSaveFollowUpErrors;
	}
	public void onSaveFollowUp()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
				 String eventDesc =	 "";
				 String successMsg =	 "";
				 if(!hasSaveFollowUpErrors())
				 {return;}
				 
				 
				 tabFollowUp_Click();
				 FollowUpView followUpView = new FollowUpView();
				 if(viewRootMap.containsKey("SELECTED_FOLLOWUP"))
					 followUpView=(FollowUpView) viewRootMap.get("SELECTED_FOLLOWUP");
				 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
				 setFollowUpView(followUpView );
				 followUpView = rsa.addFollowup(followUpView);
				 if(viewRootMap.containsKey("SELECTED_INDEX"))
				 {	
				   List<FollowUpView> fvl;
				   fvl=this.getFollowUpList();
				   fvl.remove(Integer.parseInt(viewRootMap.get("SELECTED_INDEX").toString()));
				   fvl.add(Integer.parseInt(viewRootMap.get("SELECTED_INDEX").toString()), followUpView);
				   viewRootMap.remove("SELECTED_INDEX");
				   viewRootMap.put("followUpList",fvl);
					 
				 }
				 else
				 {
				 	List<FollowUpView> fvl=this.getFollowUpList();
				 	fvl.add(followUpView);
				 	viewRootMap.put("followUpList",fvl);
				}
				 this.getFollowUpList();
				 viewRootMap.put("RECORD_SIZE", this.getFollowUpList().size());
				 eventDesc  = MessageConstants.RequestEvents.REQUEST_SAVED_FOLLOWUP;
				 successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.FOLLOWUP_SAVED_SUCCESSFULLY); 
				 saveCommentAndAttachements( eventDesc);
				 successMessages.add(successMsg);
				 tabPanel.setSelectedTab(TAB_ID.FollowUp);
				 clearFollowUpFields();
//				 makePaymentSchedule();
				 
		}
		catch(Exception e)
		{
			logger.LogException("onSaveFollowUp|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}	
	@SuppressWarnings("unchecked")
	public void onMaintenanceContractSearchClick()
	{
		if(this.getUnitView()==null)
		{return;}
		
	    if(this.getUnitView().getPropertyId()!=null && this.getUnitView().getPropertyCommercialName().trim().length()>0)
	    {
		 sessionMap.put(ServiceContractListBackingBean.PROPERTY_COMM_NAME, this.getUnitView().getPropertyCommercialName());
		 sessionMap.put(ServiceContractListBackingBean.PROPERTY_ID, this.getUnitView().getPropertyId());
	    }
	    //Not Sending Unit Id even if the maintenance contract is place on property we will use the whole property contract
//		    if(this.getUnitView().getUnitId()!=null )
//		    {
//			sessionMap.put(ServiceContractListBackingBean.UNIT_ID, this.getUnitView().getUnitId());
//			sessionMap.put(ServiceContractListBackingBean.UNIT_NUMBER, this.getUnitView().getUnitNumber());
//		    }
	    String javaScriptText="showServiceContract();";
	    AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
        addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
	}
	public void getIncompleteRequestTasks()
	{
		String taskType = "";
		 
		try{
			
			RequestView rv =(RequestView)viewRootMap.get(REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUserId();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		
		
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setPageModeBasedOnRequestStatus()
	{
		
			if(requestView == null || requestView.getStatusId()==null)
			{return;}
			DomainDataView ddRequestStatus = CommonUtil.getDomainDataFromId( getRequestStatusList(),requestView.getStatusId());
			if(ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_NEW))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				return;
			}
			else if (
						ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)
						||
						
						( 
								ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_VISIT_DONE ) &&
								
								( requestView.getMemsNolReason() == null || requestView.getMemsNolReason().trim().length() <=0 )
						)
								
								
					)
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_APPROVED))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.FOLLOW_UP);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_SITE_VISIT_REQUIRED))
			{
				tabPanel.setSelectedTab("tabSiteVisit");
				tabSiteVisit_Click();
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.SITE_VISIT_REQUIRED);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_FOLLOW_UP) )
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.FOLLOW_UP);
				return;
			}
			else if (  ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_SENT_CONTRACTOR)  ||
					   ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_RECEIVED_BY_CONTRACTOR)   
			        )
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.WITH_CONTRACTOR);
				return;
			}

			else if (  ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_CONTRACTOR_EXECUTED )  
			        )
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.CONTRACTOR_EXECUTED);
				return;
			}
			else if (  ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_CUSTOMER_REJECTED)   ||
					 ( 
								ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_VISIT_DONE ) &&
								( requestView.getMemsNolReason() != null || requestView.getMemsNolReason().trim().length() > 0 )
					  )
					 )
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.PAGE_MODE_CUSTOMER_REJECTED);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_COMPLETE )|| 
					 ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_REJECTED ))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
				return;
			}
	}

	@SuppressWarnings( "unchecked" )
	protected List<DomainDataView>  getRequestStatusList() {
		List<DomainDataView> ddvRequestStatusList = null;
		if(!viewRootMap.containsKey(DD_REQUEST_STATUS_LIST))
		{
		ddvRequestStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		viewRootMap.put(DD_REQUEST_STATUS_LIST, ddvRequestStatusList );
		}
		else
			ddvRequestStatusList =(ArrayList<DomainDataView>)viewRootMap.get(DD_REQUEST_STATUS_LIST);
		return ddvRequestStatusList ;
	}
	
	@SuppressWarnings( "unchecked" )
	private void getRequestView()throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_MAINTENANCE_APPLICATION);
		rv.setRequestId(new Long(requestId));
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0); 
			viewRootMap.put(REQUEST_VIEW,requestView );
			sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
			 
		}
    }
	
	@SuppressWarnings("unchecked")
	protected PersonView getContractorInfo(boolean fromMainteanceContract)throws PimsBusinessException
    {
    	
    	
	    if(viewRootMap.containsKey(CONTRACTOR_VIEW))
    	{
    		contractor=(PersonView)viewRootMap.get(CONTRACTOR_VIEW);
	    	if(
	    		hdnContractorId!=null && 
	    		hdnContractorId.trim().length()>0 && 
	    		contractor.getPersonId()!=null && 
	    		contractor.getPersonId().compareTo(new Long(hdnContractorId)) != 0
	    	  )
	    	{
	    		if(!fromMainteanceContract)
	    		{
		    		String auditTrail="";
					if(	viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL )!= null)
					{
						auditTrail += viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ).toString();
					}
					auditTrail +=
									CommonUtil.getParamBundleMessage(
																	 "maintenancerequest.event.contractorRemoved",
																	 "en",
																	 contractor.getPersonFullName()
																	)
								   +"&&"+
									CommonUtil.getParamBundleMessage(
																		 "maintenancerequest.event.contractorRemoved",
																		 "ar",
																		 contractor.getPersonFullName()
																	)+",";
					viewMap.put( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL,auditTrail );
	    		}
	    		getContractorInfoFromDB(fromMainteanceContract);
	    	
	    			
	    	}
	     
    	}
    	else if(
    				hdnContractorId!=null && 
    				hdnContractorId.trim().length()>0 
    			)
    	{
    		getContractorInfoFromDB(fromMainteanceContract);
    	}
    	else 
    		return null;
	    
	    return contractor;	    
    }

	@SuppressWarnings("unchecked")
	private void getContractorInfoFromDB(boolean fromMainteanceContract) throws PimsBusinessException 
	{
		PropertyServiceAgent psa=new PropertyServiceAgent();
		PersonView pv=new PersonView();
		pv.setPersonId(new Long(this.getHdnContractorId()));
		List<PersonView> investorsList =  psa.getPersonInformation(pv);
		//contractor =  csa.getContractorById(new Long(hdnContractorId), null);
		if(investorsList.size()>0)
		{
			viewRootMap.put(CONTRACTOR_VIEW, investorsList.get(0) );
		    contractor =  (PersonView)investorsList.get(0) ;
		    if(!fromMainteanceContract) 
		    {
			    String auditTrail="";
				if(	viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL )!= null)
				{
					auditTrail += viewMap.get( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ).toString();
				}
				auditTrail +=
								CommonUtil.getParamBundleMessage(
																 "maintenancerequest.event.contractorAdded",
																 "en",
																 contractor.getPersonFullName()
																)
							   +"&&"+
								CommonUtil.getParamBundleMessage(
																	 "maintenancerequest.event.contractorAdded",
																	 "ar",
																	 contractor.getPersonFullName()
																)+",";
				viewMap.put( SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL,auditTrail );
		    }

		}
		
	}
		
	private void refresh() throws PimsBusinessException 
	{
		getRequestView();
	}
	
	@SuppressWarnings("unchecked")
	private void notifyMaintenanceRequest(String notificationType)throws Exception
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		RequestView reqView = null;
		ContractView maintenanceContractView = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		List<RequestView> requestViewList = new  ArrayList<RequestView>();
		PersonView contractorView = new PersonView();
		PropertyView propertyView = new PropertyView();
		reqView = (RequestView)viewRootMap.get("REQUEST_VIEW");
		requestViewList = psAgent.getAllRequests(reqView, null, null, null);
		PersonView tenant= null;	
			
		if(
			(requestViewList != null && requestViewList.size()>0) || 
			viewRootMap.containsKey("REQUEST_VIEW") 
		   )
		{
			if((requestViewList != null && requestViewList.size()>0))
				reqView = requestViewList.get(0);
			else
				reqView = (RequestView)viewRootMap.get("REQUEST_VIEW");
			
			maintenanceContractView = reqView.getContractView();
			if(reqView.getContactorView()!=null) 
			{
				contractorView.setPersonId(reqView.getContactorView().getPersonId());
				if(reqView.getContactorView().getPersonFullName() != null)
				{
					eventAttributesValueMap.put("CONTRACTOR_NAME", reqView.getContactorView().getPersonFullName());
				}
				
			}
			
			else if(maintenanceContractView!=null)
			{
				contractorView.setPersonId(maintenanceContractView.getContractorId());
				if(maintenanceContractView.getContractorNameEn() != null)
				{
					eventAttributesValueMap.put("CONTRACTOR_NAME", maintenanceContractView.getContractorNameEn());
				}
				else 
					eventAttributesValueMap.put("CONTRACTOR_NAME", maintenanceContractView.getContractorNameAr());
//				else
//				eventAttributesValueMap.put("CONTRACTOR_NAME", contView.getContractorNameAr());
			}
			
			if(!eventAttributesValueMap.containsKey( "CONTRACTOR_NAME") || eventAttributesValueMap.get( "CONTRACTOR_NAME") == null ) 
			{
				eventAttributesValueMap.put("CONTRACTOR_NAME","_");
			}
			
			
			if(requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().iterator().hasNext())
			{
				HashMap hMap = new HashMap();
				RequestDetailView rdv = requestView.getRequestDetailView().iterator().next();
				hMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID, rdv.getUnitView().getUnitId());
			    List<UnitView> uvList =new PropertyServiceAgent().getPropertyUnitsByCriteria(hMap);
			    if(uvList!=null && uvList.size()>0)
			    {
			    	UnitView unitView = uvList.get(0);
			    	eventAttributesValueMap.put("UNIT_NUMBER", unitView.getUnitNumber());
			    	if(
				    		notificationType.equals(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest )
				      )
			    	{
				      eventAttributesValueMap.put("PROPERTY_NUMBER", unitView.getPropertyNumber());
			    	}
			    }
			    
			    if(
			    			rdv.getMaintenanceAppContractId() !=null && 
			    		!notificationType.equals(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest )
			      )
				{
			    	Contract contract = EntityManager.getBroker().findById(Contract.class, rdv.getMaintenanceAppContractId() );
			    	tenant = psAgent.getPersonInformation( contract.getTenant().getPersonId() );
			    	if( tenant.getFullNameEn() != null )
			    	{
			    		eventAttributesValueMap.put("TENANT_NAME", tenant.getFullNameEn());
			    	}
			    	else
			    	{
			    		eventAttributesValueMap.put("TENANT_NAME", tenant.getPersonFullName() );
			    	}
				}
			}
			DateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
			if(
	    	 
	    		!notificationType.equals(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest )
			)
			{
				eventAttributesValueMap.put("DATE", format.format( new Date() ) );
			}
			if(notificationType.equals(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest_Rejection) )
			{
				eventAttributesValueMap.put("COMMENTS", txtRemarks.trim());
			}
			
			eventAttributesValueMap.put("REQUEST_NUMBER", requestView.getRequestNumber());
			if(contractorView != null && notificationType.equals(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest))
			{
				contactInfoList = getContactInfoList(contractorView);
				contactInfoList.add(new ContactInfo(contractorView.getPersonId().toString(), null, null, "farooq.danish2@gmail.com"));
			}
			else if( tenant != null )
			{
				contactInfoList = getContactInfoList(tenant);
				
			}
			if(contactInfoList != null )
			{
				generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
			}
		}
				
		
	}
	
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }
    public String getErrorMessages()
	{

    	return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
    
    
    public void saveSystemComments(String sysNoteType) throws Exception
    {
    		String notesOwner = WebConstants.REQUEST;
    		if(requestId!=null && requestId.trim().length()>0)
    		{
    		  Long requestsId = new Long(requestId);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestsId);
    		}
    }
    public void saveSystemCommentsOnComplaint(Long complaintRequestId,String requestNumber) throws Exception
    {
	  NotesController.saveSystemNotesForRequest(WebConstants.REQUEST,"complaint.event.maintenanceRequestCreated", complaintRequestId,requestNumber);
    }
	public Boolean saveComments(Long referenceId) throws Exception
    {
			Boolean success = false;
	    	String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	if(txtRemarks!=null && txtRemarks.length()>0)
	    	{
	    		saveRemarksAsComments(referenceId, notesOwner,txtRemarks.toString().trim() );
	    	}
	    	success = true;
    	return success;
    }

	/**
	 * @param referenceId
	 * @param notesOwner
	 */
	private void saveRemarksAsComments(Long referenceId, String notesOwner,String remarks) 
	{
		   CommonUtil.saveRemarksAsComments(referenceId, remarks, notesOwner) ;
	}
	public Boolean saveAttachments(String referenceId) throws Exception
    {
		Boolean success = false;
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments(Long requestId){
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewRootMap.get(PROCEDURE_TYPE).toString());
    		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
        	String externalId = viewRootMap.get(EXTERNAL_ID).toString();
        	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
    		viewMap.put("noteowner", WebConstants.REQUEST);
    		if(requestId!= null){
    	    	String entityId = requestId.toString();
    			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
    			viewMap.put("entityId", entityId);
    		}
    	}	
	@SuppressWarnings( "unchecked" )
    public void tabAttachmentsComments_Click()
    {
		if( viewRootMap.get(REQUEST_VIEW) !=null )
		{
		  requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		  sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
		  if(requestView != null && requestView.getRequestId()!=null)
			 loadAttachmentsAndComments(requestView.getRequestId());
		}
    }
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	   UserTask userTask = (UserTask) sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
      viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
      if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
	  this.requestId = userTask.getTaskAttributes().get("REQUEST_ID").toString();
       
    }
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
		    return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
    public void findApplicantById()throws PimsBusinessException
    {
    	String methodName="cmdAddApplicant_Click";
    	logger.logInfo(methodName+"| Start");
    	PersonView applicantView;
    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW) && 
    	   viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW)!=null		
    	   )
    	{
    		applicantView=(PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
	    	if(hdnApplicantId!=null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(hdnApplicantId));
	    		List<PersonView> personsList =  psa.getPersonInformation(pv);
	    		if(personsList.size()>0)
	    		{
	    			pv = personsList.get(0);
	    			populateApplicantDetails(pv);
	    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
	    		}
	    			
	    	}
    	}
    	else if(hdnApplicantId!=null && hdnApplicantId.trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(hdnApplicantId));
    		List<PersonView> personsList=  psa.getPersonInformation(pv);
    		if(personsList.size()>0)
    		{
    			pv = personsList.get(0);
    			populateApplicantDetails(pv);
    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
    		}	
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }

	private void populateApplicationDetailsTab() {
		//PopulatingApplicationDetailsTab
		if(requestView!=null)
		{
			if(requestView.getRequestNumber()!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
			if(requestView.getRequestDate()!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
			if(requestView.getStatusId()!=null)
			{
				if(getIsEnglishLocale())
				{
					if(requestView.getStatusEn()!=null)	
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusEn());
				}else if(getIsArabicLocale())
				{
					if(requestView.getStatusAr()!=null)
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusAr());
				}
			}
			else
			{
				List<DomainDataView >ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
				DomainDataView ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
				if(getIsEnglishLocale())
				{
						
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescEn());
				}else if(getIsArabicLocale())
				{
					
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescAr());
				}
			}
			if(requestView.getApplicantView()!=null)
				populateApplicantDetails(requestView.getApplicantView());
			loadAttachmentsAndComments(requestView.getRequestId());
			
		}
	}
    private void populateApplicantDetails(PersonView pv)
    {
        List<DomainDataView>  ddl = (ArrayList<DomainDataView>)viewRootMap.get("PERSON_TYPE_LIST");
		DomainDataView ddvCompany = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_COMPANY);
		viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY, ddvCompany);
		DomainDataView ddvIndividual = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_INDIVIDUAL);
		viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL, ddvIndividual);
		
    	if(pv.getPersonId()!=null )
       	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,pv.getPersonId());
      if(pv.getPersonFullName()!=null && pv.getPersonFullName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
     else if(pv.getCompanyName()!=null && pv.getCompanyName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
      if(!viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE) )
      {
	      String personType = getPersonType(pv);
	      if(personType !=null)
	         viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
      }
     if(pv.getCellNumber()!=null)
     {
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
    	 if( txtTelephoneNumber.getValue() == null )
    	 {
    		 txtTelephoneNumber.setValue(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL).toString());
    	 }
     }
     if(pv.getPersonId()!=null)
     {
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,pv.getPersonId());
    	 hdnApplicantId = pv.getPersonId().toString();
     }
    	 
    }
    private String getPersonType(PersonView tenantView) {

		String method = "getPersonType";
		logger.logDebug(method + "|" + "Start...");
		DomainDataView ddv;
		if (tenantView.getIsCompany().compareTo(new Long(1)) == 0) 
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
		else
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
		if (getIsArabicLocale())
			return ddv.getDataDescAr();
		else if (getIsEnglishLocale())
			return ddv.getDataDescEn();

		logger.logDebug(method + "|" + "Finish...");
		return "";

	}

	@SuppressWarnings("unchecked")
	public void onPaymentTermsTab()
	{
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onPaymentTermsTab|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}

	public PersonView getContractorFromViewRoot()
	{
		
		if(viewRootMap.get(CONTRACTOR_VIEW)!=null)
			return (PersonView)viewRootMap.get(CONTRACTOR_VIEW);
		else
			return null;
		
	}
	
	public RequestView getRequestViewFromViewRoot()
	{
		if(viewRootMap.get(REQUEST_VIEW)!=null)
			return (RequestView)viewRootMap.get(REQUEST_VIEW);
		else
			return null;
		
	}
	public void tabAuditTrail_Click()
	{
		String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	errorMessages = new ArrayList<String>(0);
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(requestId!=null && requestId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
		
	}
	
	public void onSendForSiteVisit()
	{
		successMessages= new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			setTaskOutCome(TaskOutcome.SEND_FOR_SITE_VISIT);
			requestView.setOldStatusId( requestView.getStatusId() );
			requestView.setStatusId(WebConstants.REQUEST_STATUS_SITE_VISIT_REQUIRED_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);

			saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SITE_VISIT_REQ);
			viewRootMap.put(Page_Mode.PAGE_MODE , Page_Mode.VIEW);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_SENT_FOR_SITE_VISIT));
			imgSearchContractor.setRendered(false);
			imgClearContract.setRendered(false);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "onSendForSiteVisit|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	private boolean hasApprovalErrors()
	{
		errorMessages = new ArrayList<String>(0);
	  /*if((this.getHdnContractId() == null || this.getHdnContractId().trim().length()<=0 )&& 
		 (this.getHdnContractorId()==null || this.getHdnContractorId().trim().length()<=0) )
		  return true;*/
      if( (this.getContractView()==null || this.getContractView().getContractId()==null) && 
	 	  (this.getContractor()==null || this.getContractor().getPersonId()==null))
	 	{
    	  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.CONTRACT_OR_CONTACTOR_REQ));
   		   return true;
	 	}
	  //else if(this.getHdnContractorId()==null || this.getHdnContractorId().trim().length()<=0)
		//  return true;
	  else 
		  return false;
		
	}
	
	private boolean hasContractorExecutedErrors()
	{
		errorMessages = new ArrayList<String>(0);
        if( 
        		txtServiceProviderRemarks.getValue() == null ||
        		txtServiceProviderRemarks.getValue().toString().length() <= 0
          )
	    {
    	   errorMessages.add(ResourceUtil.getInstance().getProperty("maintenancerequest.msg.contractorCommentsRequired"));
   		   return true;
	    }
	    else 
		  return false;
		
	}
	@SuppressWarnings("unchecked")
	public void onContractorExecuted()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(hasContractorExecutedErrors()){return;}
			requestView.setOldStatusId( requestView.getStatusId() );
			requestView.setStatusId(WebConstants.REQUEST_STATUS_CONTRACTOR_EXECUTED_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView.setContractorComments( txtServiceProviderRemarks.getValue().toString().trim());
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
			
			setTaskOutCome(TaskOutcome.OK);
	    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
	    	saveCommentAndAttachements("maintenancerequest.event.contractorExecuted") ;
//	    	notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest);
	    	txtboxRemarks.setValue("");
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
	    	btnResendToContractor.setRendered(false);
	    	successMessages.add(
								 ResourceUtil.getInstance().getProperty("maintenancerequest.event.contractorExecuted") 
			   				   );
    	}
    	catch(Exception ex)
    	{
    	 logger.LogException("onContractorExecuted|Error Occured..",ex);
    	 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	
	@SuppressWarnings("unchecked")
	public void onResendToContractor()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(hasApprovalErrors()){return;}
			//No old status will be set on this step so we could know whether the 
			//task was sent by FM  from approval requried step or customer rejected step. 
			if( requestView.getStatusId().compareTo(WebConstants.REQUEST_STATUS_VISIT_DONE_ID)!= 0  )
			{
				requestView.setOldStatusId( requestView.getStatusId() );
			}
			requestView.setStatusId(WebConstants.REQUEST_STATUS_SENT_CONTRACTOR_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView.setContractView(this.getContractView());
			requestView.setContactorView( this.getContractor() );
			if(txtFMRemarks.getValue()!= null)
			{
				String fmRemarks = txtFMRemarks.getValue().toString().trim();
				requestView.setMemsNolReason( fmRemarks );
				saveRemarksAsComments(requestView.getRequestId(), WebConstants.REQUEST,fmRemarks);
			}
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
			setTaskOutCome(TaskOutcome.APPROVE);
	    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
	    	saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_RESENT_TO_CONTRACTOR) ;
	    	saveContractorContractAuditTrail();
	    	notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest);
	    	txtboxRemarks.setValue("");
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
	    	btnResendToContractor.setRendered(false);
	    	successMessages.add(
								 ResourceUtil.getInstance().getProperty("maintenancerequest.msg.requestResentToContractor") 
			   				   );
    	}
    	catch(Exception ex)
    	{
    	 logger.LogException("onResendToContractor|Error Occured..",ex);
    	 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	
	@SuppressWarnings("unchecked")
	public void onCustomerRejected()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if( this.txtRemarks == null || this.txtRemarks.trim().length() <= 0 )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.RequiredRemarks"));
				return;
			}
			requestView.setOldStatusId(requestView.getStatusId());
			requestView.setStatusId(WebConstants.REQUEST_STATUS_CUSTOMER_REJECTED_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
			setTaskOutCome(TaskOutcome.REJECT);
	    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
	    	saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_CUSTOMER_REJECTED) ;
	    	txtboxRemarks.setValue("");
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
	    	btnCustomerRejected.setRendered(false);
	    	successMessages.add(
								 ResourceUtil.getInstance().getProperty("comons.sendToFM") 
			   				   );
    	}
    	catch(Exception ex)
    	{
    	 logger.LogException("onCustomerRejected|Error Occured..",ex);
    	 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	
	public void saveContractorContractAuditTrail() throws Exception
    {
		if(
				viewMap.get(SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ) == null  &&
				requestId!=null && requestId.trim().length()>0
		  )
		{return;}
    	String trails = viewMap.get(SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL ).toString() ;
		String[] eachTrail = trails.split(",");
		for (String trail : eachTrail) 
		{
			
			String[] localeWiseTrail = trail.split("&&");
			NotesController.saveSystemNotesForRequest(WebConstants.REQUEST,localeWiseTrail[0],localeWiseTrail[1], new Long ( requestId ) );
		}
		viewMap.remove(SYS_COMMENTS_SERVICE_CONTRACT_CONTRACTOR_AUDIT_TRAIL );
    		
    }
	@SuppressWarnings("unchecked")
	public void onApprove()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(hasApprovalErrors()){return;}
			requestView.setOldStatusId(requestView.getStatusId());
			requestView.setStatusId(WebConstants.REQUEST_STATUS_SENT_CONTRACTOR_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView.setContractView(this.getContractView());
			requestView.setContactorView( this.getContractor() );
			if(txtFMRemarks.getValue()!= null)
			{
				String fmRemarks = txtFMRemarks.getValue().toString().trim();
				requestView.setMemsNolReason( fmRemarks );
				saveRemarksAsComments(requestView.getRequestId(), WebConstants.REQUEST,fmRemarks);
			}	    	
			setTaskOutCome(TaskOutcome.APPROVE);
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
			viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_APPROVED);
			saveContractorContractAuditTrail();
	    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
	    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_APPROVED));
	    	txtboxRemarks.setValue("");
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
	    	notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest);
	    	notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_Maintenance_Approve);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onApprove|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	public void onReject()
	{
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if( this.txtRemarks == null || this.txtRemarks.trim().length() <= 0 )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.RequiredRemarks"));
				return;
			}
	    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
	    	{
				  getIncompleteRequestTasks();
	    	}
			setTaskOutCome(TaskOutcome.REJECT);
			requestView.setOldStatusId( requestView.getStatusId() );
			requestView.setStatusId(WebConstants.REQUEST_STATUS_SITE_VISIT_REQUIRED_ID);
			requestView.setUpdatedBy(getLoggedInUserId());
			requestView.setUpdatedOn(new Date());
			requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
			viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_REJECTED);
			notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_MaintenanceRequest_Rejection);
	    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_REJECTED));
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onReject|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	
	
	public void onComplete()
	{
		errorMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent();
		try	
    	{
			 	 if(hasCompletionErrors())
			 	 {return;}
			 	 
			 	 
			 	 if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();

			 	
	        	putControlValuesinViews();
	        	requestView.setOldStatusId(requestView.getStatusId());
	        	requestView.setStatusId(WebConstants.REQUEST_STATUS_COMPLETE_ID);
				requestView.setUpdatedBy(getLoggedInUserId());
				requestView.setUpdatedOn(new Date());
				requestView = new RequestServiceAgent().persistMaintenanceRequest(requestView);
				
				
				if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
				{
					setTaskOutCome(TaskOutcome.CLOSE);
					saveCommentAndAttachements("request.event.completedByFM");
				}
				else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.PAGE_MODE_CUSTOMER_REJECTED))
				{
					setTaskOutCome(TaskOutcome.REJECT);
					saveCommentAndAttachements("request.event.completedByFM");
				}
				else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.CONTRACTOR_EXECUTED))
				{				
					setTaskOutCome(TaskOutcome.CLOSE);
					saveCommentAndAttachements("request.event.completedByCS");
				}
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
		    	lblRemarks.setRendered(false);
		    	grpRemarks.setRendered( false);
		    	txtboxRemarks.setRendered(false);
		    	notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_Maintenance_Completed);
		    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_COMPLETED));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onComplete|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	
		
	public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
		return tabPaymentTerms;
	}

	public void setTabPaymentTerms(
			org.richfaces.component.html.HtmlTab tabPaymentTerms) {
		this.tabPaymentTerms = tabPaymentTerms;
	}
	public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
		return tabAuditTrail;
	}
	
	
	public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
		this.tabAuditTrail = tabAuditTrail;
	}
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;
		
		return isViewModePopup;
	}

	public String getPageTitle() {
		if(viewRootMap.containsKey("pageTitle") && viewRootMap.get("pageTitle")!=null)
			pageTitle = viewRootMap.get("pageTitle").toString();
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
		if(this.pageTitle !=null)
			viewRootMap.put("pageTitle",this.pageTitle );
			
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}


	public String getPageMode() {
		if(viewRootMap.get(Page_Mode.PAGE_MODE)!=null)
			pageMode =viewRootMap.get(Page_Mode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getContractCreatedOn() {
		return createdOn;
	}

	public void setContractCreatedOn(String contractCreatedOn) {
		this.createdOn = contractCreatedOn;
	}

	public String getContractCreatedBy() {
		return createdBy;
	}

	public void setContractCreatedBy(String contractCreatedBy) {
		this.createdBy = contractCreatedBy;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnSiteVisitReq() {
		return btnSiteVisitReq;
	}

	public void setBtnSiteVisitReq(HtmlCommandButton btnSiteVisitReq) {
		this.btnSiteVisitReq = btnSiteVisitReq;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}



	public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	
		public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnSend_For_Approval() {
		return btnSend_For_Approval;
	}

	public void setBtnSend_For_Approval(HtmlCommandButton btnSend_For_Approval) {
		this.btnSend_For_Approval = btnSend_For_Approval;
	}

			public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public PersonView getContractor() {
	 if(viewRootMap.containsKey(CONTRACTOR_VIEW))
		 contractor = (PersonView)viewRootMap.get(CONTRACTOR_VIEW);
		return contractor;
	}
	public void setContractor(PersonView contractor) {
		this.contractor = contractor;
		if(this.contractor!=null)
			viewRootMap.put(CONTRACTOR_VIEW,this.contractor);
	}

	public org.richfaces.component.html.HtmlTab getTabMaintenanceInfo() {
		return tabMaintenanceInfo;
	}

	public void setTabMaintenanceInfo(
			org.richfaces.component.html.HtmlTab tabMaintenanceInfo) {
		this.tabMaintenanceInfo = tabMaintenanceInfo;
	}

	public HtmlGraphicImage getImgUnitSearch() {
		return imgUnitSearch;
	}

	public void setImgUnitSearch(HtmlGraphicImage imgUnitSearch) {
		this.imgUnitSearch = imgUnitSearch;
	}

	public UnitView getUnitView() {
		if(viewRootMap.containsKey(WebConstants.UNIT_VIEW))
				unitView = (UnitView)viewRootMap.get(WebConstants.UNIT_VIEW);
		return unitView;
	}

	public void setUnitView(UnitView unitView) {
		this.unitView = unitView;
		if(this.unitView!=null)
			viewRootMap.put(WebConstants.UNIT_VIEW,this.unitView);
	}
	public UserView getUserView() {
		if(viewRootMap.containsKey(WebConstants.USER_VIEW))
				userView = (UserView)viewRootMap.get(WebConstants.USER_VIEW);
		return userView;
	}

	public void setUserView(UserView userView) {
		this.userView = userView;
		if(this.userView!=null)
			viewRootMap.put(WebConstants.USER_VIEW,this.userView);
	}

	public ContractView getContractView() {
		if(viewRootMap.containsKey(CONTRACT_VIEW))
				contractView = (ContractView)viewRootMap.get(CONTRACT_VIEW);
		return contractView;
	}

	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
		if(this.contractView!=null)
			viewRootMap.put(CONTRACT_VIEW,this.contractView);
	}
	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlGraphicImage getImgPropertySearch() {
		return imgPropertySearch;
	}

	public void setImgPropertySearch(HtmlGraphicImage imgPropertySearch) {
		this.imgPropertySearch = imgPropertySearch;
	}

	public HtmlSelectOneMenu getCmbMaintenanceType() {
		return cmbMaintenanceType;
	}

	public void setCmbMaintenanceType(HtmlSelectOneMenu cmbMaintenanceType) {
		this.cmbMaintenanceType = cmbMaintenanceType;
	}

	public String getSelectOneMaintenanceType() {
		if(viewRootMap.containsKey("selectOneMaintenanceType"))
				selectOneMaintenanceType = viewRootMap.get("selectOneMaintenanceType").toString();
		return selectOneMaintenanceType;
	}

	public void setSelectOneMaintenanceType(String selectOneMaintenanceType) {
		this.selectOneMaintenanceType = selectOneMaintenanceType;
		if(this.selectOneMaintenanceType !=null)
			viewRootMap.put("selectOneMaintenanceType", this.selectOneMaintenanceType);
	}

	@SuppressWarnings( "unchecked" )
	public String getSelectOneWorkType() {
		if(viewRootMap.containsKey("selectOneWorkType"))
		{
			selectOneWorkType =  viewRootMap.get("selectOneWorkType").toString();
		}
		return selectOneWorkType;
	}

	@SuppressWarnings( "unchecked" )
	public void setSelectOneWorkType(String selectOneWorkType) {
		this.selectOneWorkType = selectOneWorkType;
		if( this.selectOneWorkType !=null )
		{
			viewRootMap.put("selectOneWorkType", this.selectOneWorkType);
		}
	}
	@SuppressWarnings( "unchecked" )
	public List<String> getSelectManyWorkType() {
		if(viewRootMap.containsKey("selectManyWorkTypes"))
		{
			selectManyWorkType =  (List<String>)viewRootMap.get("selectManyWorkTypes");
		}
		return selectManyWorkType;
	}

	@SuppressWarnings( "unchecked" )
	public void setSelectManyWorkType( List<String> selectManyWorkType) {
		
		this.selectManyWorkType = selectManyWorkType;
		if( this.selectManyWorkType !=null )
		{
			viewRootMap.put("selectManyWorkTypes", this.selectManyWorkType);
		}
	}

	public HtmlSelectOneRadio getCmbWorkType() {
		return cmbWorkType;
	}

	public void setCmbWorkType(HtmlSelectOneRadio cmbWorkType) {
		this.cmbWorkType = cmbWorkType;
	}

	public String getHdnApplicantId() {
		return hdnApplicantId;
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}

	public org.richfaces.component.html.HtmlTab getTabApplicationDetails() {
		return tabApplicationDetails;
	}

	public void setTabApplicationDetails(
			org.richfaces.component.html.HtmlTab tabApplicationDetails) {
		this.tabApplicationDetails = tabApplicationDetails;
	}

	public HtmlCommandButton getBtnCompleteSiteVisit() {
		return btnCompleteSiteVisit;
	}

	public void setBtnCompleteSiteVisit(HtmlCommandButton btnCompleteSiteVisit) {
		this.btnCompleteSiteVisit = btnCompleteSiteVisit;
	}

	public HtmlCommandButton getBtnSaveSiteVisit() {
		return btnSaveSiteVisit;
	}

	public void setBtnSaveSiteVisit(HtmlCommandButton btnSaveSiteVisit) {
		this.btnSaveSiteVisit = btnSaveSiteVisit;
	}

	public org.richfaces.component.html.HtmlTab getTabSiteVisit() {
		return tabSiteVisit;
	}

	public void setTabSiteVisit(org.richfaces.component.html.HtmlTab tabSiteVisit) {
		this.tabSiteVisit = tabSiteVisit;
	}

	public HtmlDataTable getSiteVisitDataTable() {
		return siteVisitDataTable;
	}

	public void setSiteVisitDataTable(HtmlDataTable siteVisitDataTable) {
		this.siteVisitDataTable = siteVisitDataTable;
	}

	public List<SiteVisitView> getSiteVisitList() {
		if(viewRootMap.containsKey("siteVisitList"))
			siteVisitList =(ArrayList<SiteVisitView>)viewRootMap.get("siteVisitList");
		return siteVisitList;
	}

	public void setSiteVisitList(List<SiteVisitView> siteVisitList) {
		this.siteVisitList = siteVisitList;
		if(this.siteVisitList !=null && this.siteVisitList.size()>0)
			viewRootMap.put("siteVisitList",this.siteVisitList); 
	}

	public List<FollowUpView> getFollowUpList() {
		if(viewRootMap.containsKey("followUpList"))
			{
				followUpList =(ArrayList<FollowUpView>)viewRootMap.get("followUpList");
				viewRootMap.put("RECORD_SIZE",followUpList.size());
			}
		return followUpList;
	}



	public void setFollowUpList(List<FollowUpView> followUpList) {
		this.followUpList = followUpList;
		if(this.followUpList  !=null && this.followUpList.size()>0)
			viewRootMap.put("followUpList",this.followUpList );
	}


	public HtmlDataTable getFollowUpDataTable() {
		return followUpDataTable;
	}



	public void setFollowUpDataTable(HtmlDataTable followUpDataTable) {
		this.followUpDataTable = followUpDataTable;
	}




	public int getRecordSize()
	{
		if(viewRootMap.containsKey("RECORD_SIZE"))
			recordSize=(Integer) viewRootMap.get("RECORD_SIZE");
		return recordSize;
	}



	public void setRecordSize(int recordSize) {
		this.recordSize = recordSize;
	}



	public HtmlCommandButton getBtnSaveFollowUp() {
		return btnSaveFollowUp;
	}



	public void setBtnSaveFollowUp(HtmlCommandButton btnSaveFollowUp) {
		this.btnSaveFollowUp = btnSaveFollowUp;
	}



	public org.richfaces.component.html.HtmlTab getTabFollowUp() {
		return tabFollowUp;
	}



	public void setTabFollowUp(org.richfaces.component.html.HtmlTab tabFollowUp) {
		this.tabFollowUp = tabFollowUp;
	}



	public HtmlGraphicImage getImgContractSearch() {
		return imgContractSearch;
	}



	public void setImgContractSearch(HtmlGraphicImage imgContractSearch) {
		this.imgContractSearch = imgContractSearch;
	}



	public String getHdnContractId() {
		return hdnContractId;
	}



	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}
	
	public String getContractorScreenQueryStringPopUpMode() {
		
		return ContractorSearch.Keys.MODE_SELECT_ONE_POPUP;
	}

	

	public String getContractorScreenQueryStringViewMode() {
		return ContractorSearch.Keys.PAGE_MODE;
	}



	public HtmlInputText getTxtContractNum() {
		return txtContractNum;
	}



	public void setTxtContractNum(HtmlInputText txtContractNum) {
		this.txtContractNum = txtContractNum;
	}



	public HtmlInputText getTxtContractorNum() {
		return txtContractorNum;
	}



	public void setTxtContractorNum(HtmlInputText txtContractorNum) {
		this.txtContractorNum = txtContractorNum;
	}



	public HtmlGraphicImage getImgSearchContractor() {
		return imgSearchContractor;
	}



	public void setImgSearchContractor(HtmlGraphicImage imgSearchContractor) {
		this.imgSearchContractor = imgSearchContractor;
	}



	public HtmlCommandButton getBtnOtherSave() {
		return btnOtherSave;
	}



	public void setBtnOtherSave(HtmlCommandButton btnOtherSave) {
		this.btnOtherSave = btnOtherSave;
	}



	public HtmlSelectOneMenu getCmbRequestPriority() {
		return cmbRequestPriority;
	}



	public void setCmbRequestPriority(HtmlSelectOneMenu cmbRequestPriority) {
		this.cmbRequestPriority = cmbRequestPriority;
	}



	public String getSelectOneRequestPriority() {
		return selectOneRequestPriority;
	}



	public void setSelectOneRequestPriority(String selectOneRequestPriority) {
		this.selectOneRequestPriority = selectOneRequestPriority;
	}



	public List<SelectItem> getRequestPriority() {
		if(viewRootMap.containsKey(REQUEST_PRIORITY_COMBO))
			requestPriority = (ArrayList<SelectItem>)viewRootMap.get(REQUEST_PRIORITY_COMBO);
		return requestPriority;
	}



	public void setRequestPriority(List<SelectItem> requestPriority) {
		this.requestPriority = requestPriority;
	}



	public HtmlGraphicImage getImgSearchEngineer() {
		return imgSearchEngineer;
	}



	public void setImgSearchEngineer(HtmlGraphicImage imgSearchEngineer) {
		this.imgSearchEngineer = imgSearchEngineer;
	}



	public HtmlInputTextarea getTxtboxRemarks() {
		return txtboxRemarks;
	}



	public void setTxtboxRemarks(HtmlInputTextarea txtboxRemarks) {
		this.txtboxRemarks = txtboxRemarks;
	}



	public HtmlOutputLabel getLblRemarks() {
		return lblRemarks;
	}



	public void setLblRemarks(HtmlOutputLabel lblRemarks) {
		this.lblRemarks = lblRemarks;
	}


	private boolean hasCompletionErrors()
	{
		boolean hasErrors=false;

		if(this.getSelectOneCmbCustomerReview() ==null || this.getSelectOneCmbCustomerReview().equals("-1") )
		{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.customerReview"));
				hasErrors = true;
				return hasErrors;
		}

		return hasErrors;
	}

	public void addActualCostToFollowUp()
	{
		FollowUpView followUpView=(FollowUpView) followUpDataTable.getRowData();
		viewRootMap.put("SELECTED_FOLLOWUP", followUpView);
		viewRootMap.put("SELECTED_INDEX", followUpDataTable.getRowIndex());
		if(followUpView.getRemarks()!=null)
			this.setTxtRemarks(followUpView.getRemarks());
		if(followUpView.getActualCost()!=null)
			this.setActualCost(followUpView.getActualCost().toString());
		
	}



	public HtmlGraphicImage getImgClearContract() {
		return imgClearContract;
	}



	public void setImgClearContract(HtmlGraphicImage imgClearContract) {
		this.imgClearContract = imgClearContract;
	}



	public HtmlGraphicImage getImgMaintenanceContractSearch() {
		return imgMaintenanceContractSearch;
	}



	public void setImgMaintenanceContractSearch(
			HtmlGraphicImage imgMaintenanceContractSearch) {
		this.imgMaintenanceContractSearch = imgMaintenanceContractSearch;
	}



	public String getHdnMaintenanceContractSearchContractContractId() {
		return hdnMaintenanceContractSearchContractContractId;
	}



	public void setHdnMaintenanceContractSearchContractContractId(
			String hdnMaintenanceContractSearchContractContractId) {
		this.hdnMaintenanceContractSearchContractContractId = hdnMaintenanceContractSearchContractContractId;
	}



	public ContractView getMaintenanceContractView() {
		if(viewRootMap.containsKey(MAINTENANCE_CONTRACT_VIEW)) {
			maintenanceContractView = (ContractView)viewRootMap.get(MAINTENANCE_CONTRACT_VIEW);
		}	
		return maintenanceContractView;
	}

	public void setMaintenanceContractView(ContractView maintenanceContractView) {
		this.maintenanceContractView = maintenanceContractView;
		
		if (this.maintenanceContractView!=null) {
			viewRootMap.put(MAINTENANCE_CONTRACT_VIEW,this.maintenanceContractView);
		}
	}

	public HtmlGraphicImage getImgViewUnitDetails() {
		return imgViewUnitDetails;
	}

	public void setImgViewUnitDetails(HtmlGraphicImage imgViewUnitDetails) {
		this.imgViewUnitDetails = imgViewUnitDetails;
	}
	
	public HtmlInputTextarea getTxtMaintenaceDetails() {
		return txtMaintenaceDetails;
	}

	public void setTxtMaintenaceDetails(HtmlInputTextarea txtMaintenaceDetails) {
		this.txtMaintenaceDetails = txtMaintenaceDetails;
	}

	public HtmlInputText getTxtTelephoneNumber() {
		return txtTelephoneNumber;
	}

	public void setTxtTelephoneNumber(HtmlInputText txtTelephoneNumber) {
		this.txtTelephoneNumber = txtTelephoneNumber;
	}

	public boolean isExtraWork() {
		return extraWork;
	}



	public void setExtraWork(boolean extraWork) {
		this.extraWork = extraWork;
	}



	public HtmlInputTextarea getTxtServiceProviderRemarks() {
		return txtServiceProviderRemarks;
	}



	public void setTxtServiceProviderRemarks(
			HtmlInputTextarea txtServiceProviderRemarks) {
		this.txtServiceProviderRemarks = txtServiceProviderRemarks;
	}



	public HtmlOutputLabel getLblServiceProviderRemarks() {
		return lblServiceProviderRemarks;
	}



	public void setLblServiceProviderRemarks(
			HtmlOutputLabel lblServiceProviderRemarks) {
		this.lblServiceProviderRemarks = lblServiceProviderRemarks;
	}



	public HtmlCommandButton getBtnResendToContractor() {
		return btnResendToContractor;
	}



	public void setBtnResendToContractor(HtmlCommandButton btnResendToContractor) {
		this.btnResendToContractor = btnResendToContractor;
	}



	public HtmlCommandButton getBtnCustomerRejected() {
		return btnCustomerRejected;
	}



	public void setBtnCustomerRejected(HtmlCommandButton btnCustomerRejected) {
		this.btnCustomerRejected = btnCustomerRejected;
	}



	public HtmlOutputLabel getLblCustomerReview() {
		return lblCustomerReview;
	}



	public void setLblCustomerReview(HtmlOutputLabel lblCustomerReview) {
		this.lblCustomerReview = lblCustomerReview;
	}



	public HtmlSelectOneMenu getCmbCustomerReview() {
		return cmbCustomerReview;
	}



	public void setCmbCustomerReview(HtmlSelectOneMenu cmbCustomerReview) {
		this.cmbCustomerReview = cmbCustomerReview;
	}


	@SuppressWarnings("unchecked")
	public String getSelectOneCmbCustomerReview() 
	{
		if(
				viewMap.get("selectOneCmbCustomerReview")!= null && 
				!viewMap.get("selectOneCmbCustomerReview").toString().equals("-1")
				
		  )
		{
			selectOneCmbCustomerReview= viewMap.get("selectOneCmbCustomerReview").toString();
		}
		return selectOneCmbCustomerReview;
	}


	@SuppressWarnings("unchecked")
	public void setSelectOneCmbCustomerReview(String selectOneCmbCustomerReview) {
		this.selectOneCmbCustomerReview = selectOneCmbCustomerReview;
		if(this.selectOneCmbCustomerReview != null && this.selectOneCmbCustomerReview.equals("-1") )
		{
			
			viewMap.put("selectOneCmbCustomerReview",this.selectOneCmbCustomerReview );
		}
	}



	public HtmlInputTextarea getTxtFMRemarks() {
		return txtFMRemarks;
	}



	public void setTxtFMRemarks(HtmlInputTextarea txtFMRemarks) {
		this.txtFMRemarks = txtFMRemarks;
	}



	public HtmlOutputLabel getLblFMRemarks() {
		return lblFMRemarks;
	}



	public void setLblFMRemarks(HtmlOutputLabel lblFMRemarks) {
		this.lblFMRemarks = lblFMRemarks;
	}



	public HtmlCommandButton getBtnContractorExecuted() {
		return btnContractorExecuted;
	}



	public void setBtnContractorExecuted(HtmlCommandButton btnContractorExecuted) {
		this.btnContractorExecuted = btnContractorExecuted;
	}
	
}
