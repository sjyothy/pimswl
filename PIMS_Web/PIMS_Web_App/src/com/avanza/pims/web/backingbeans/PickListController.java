package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.div.Div;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.meta.MetaPicklist;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.meta.PicklistData;
import com.avanza.core.security.Permission;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;

public class PickListController  extends AbstractController{

    /**
     * @author Muhammad Ali
     */
    private static final long serialVersionUID = -1009429671577769550L;

    private String SUCCESS = "Success";
    private List<MetaPicklist> picklistList;
    private MetaPicklist selectedPicklist;
    private List<PicklistData> selectedPicklistData;
    private List<PicklistData> oldPicklistData;
    private List<PicklistData> newPicklistData;
    
    private Properties picklistMapping ;
    
    private PicklistData picklistDataItem;
    
    private boolean flag;
    
    private boolean mode = false;
    
    private boolean addNew = false;
    
    private PicklistData picklistDataDeleteItem;
    
    private String permissionId ;
    
    
    
    //private UIDataTable dataTable = null;
    
    private HtmlDataTable dataTable = null;
    
    private static Logger logger = Logger.getLogger( PickListController.class );
    Map sessionMap;
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	FacesContext context = null;
	
	UIViewRoot view =null; 
	
	private List<SelectItem> picklistTypes; 
	private HtmlSelectOneMenu picklistTypesSelectOneMenu = new HtmlSelectOneMenu();
	 //org.apache.myfaces.custom.div.Div
	 Div div =new Div();
	HtmlInputText dataEn = new HtmlInputText();
	HtmlInputText dataAr = new HtmlInputText();
	HtmlInputText dataValue = new HtmlInputText();
	
	String strdataEn ;
	String strdataAr; 
	String strdataValue ;
	
	
	private HtmlCommandButton btnAddType = new HtmlCommandButton();
	private HtmlCommandButton btnSaveType = new HtmlCommandButton(); 
	
	
	public String getStrdataEn() {
		return strdataEn;
	}

	public void setStrdataEn(String strdataEn) {
		this.strdataEn = strdataEn;
	}

	public String getStrdataAr() {
		return strdataAr;
	}

	public void setStrdataAr(String strdataAr) {
		this.strdataAr = strdataAr;
	}

	public String getStrdataValue() {
		return strdataValue;
	}

	public void setStrdataValue(String strdataValue) {
		this.strdataValue = strdataValue;
	}

	public boolean isAddNew() {
		return addNew;
	}

	public void setAddNew(boolean addNew) {
		this.addNew = addNew;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public HtmlInputText getDataEn() {
		return dataEn;
	}

	public void setDataEn(HtmlInputText dataEn) {
		this.dataEn = dataEn;
	}

	public HtmlInputText getDataAr() {
		return dataAr;
	}

	public void setDataAr(HtmlInputText dataAr) {
		this.dataAr = dataAr;
	}

	public HtmlInputText getDataValue() {
		return dataValue;
	}

	public void setDataValue(HtmlInputText dataValue) {
		this.dataValue = dataValue;
	}

	public boolean isMode() {
		return mode;
	}

	public void setMode(boolean mode) {
		this.mode = mode;
	}

	public HtmlSelectOneMenu getPicklistTypesSelectOneMenu() {
		return picklistTypesSelectOneMenu;
	}

	public void setPicklistTypesSelectOneMenu(
			HtmlSelectOneMenu picklistTypesSelectOneMenu) {
		this.picklistTypesSelectOneMenu = picklistTypesSelectOneMenu;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public PicklistData getPicklistDataDeleteItem() {
		return picklistDataDeleteItem;
	}

	public void setPicklistDataDeleteItem(PicklistData picklistDataDeleteItem) {
		this.picklistDataDeleteItem = picklistDataDeleteItem;
	}

	public PicklistData getPicklistDataItem() {
		return picklistDataItem;
	}

	public void setPicklistDataItem(PicklistData picklistDataItem) {
		this.picklistDataItem = picklistDataItem;
	}

	public List<PicklistData> getSelectedPicklistData() {
		return selectedPicklistData;
	}

	public void setSelectedPicklistData(List<PicklistData> selectedPicklistData) {
		this.selectedPicklistData = selectedPicklistData;
	}

	public List<PicklistData> getOldPicklistData() {
		return oldPicklistData;
	}

	public void setOldPicklistData(List<PicklistData> oldPicklistData) {
		this.oldPicklistData = oldPicklistData;
	}

	public List<PicklistData> getNewPicklistData() {
		return newPicklistData;
	}

	public void setNewPicklistData(List<PicklistData> newPicklistData) {
		this.newPicklistData = newPicklistData;
	}

	public PickListController(){
    	context = FacesContext.getCurrentInstance();
    	view    = context.getViewRoot();
	
    	// String passport =context.getExternalContext().getRequestMap().get("passportNumber").toString();
    	//   	Load all the combos present in the screen
    	
	}
	 @Override
	    public void prerender() 
	    {
		  //mode=true;
		 if(mode)
			 div.setRendered(true); 
					 else
						 div.setRendered(false); 
	    }
    /**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Override this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void init() 
    {
    	
    	String methodName="init";
    	logger.logInfo(methodName+"|"+"Start");
    	sessionMap =context.getExternalContext().getSessionMap();
        
		
	    Map  requestMap=context.getExternalContext().getRequestMap();
	    
	    picklistList = PickListManager.getMetaPicklistList();
	    
	    
	    ///////////////////
	    
        Permission setupPermissions = SecurityManager.getPermission("Pims.Setup");
        
        List<Permission> lstPermission =  setupPermissions.getPermissions();
        
        List<String> lstPermissionIds = new ArrayList();
        
        if( lstPermission != null )
	        for(int i = 0 ; i < lstPermission.size(); i++){
	        	
	        	Permission permission = lstPermission.get(i);
	        	lstPermissionIds.add( permission.getPermissionId() ) ;
	        }
        
        
        picklistMapping = (Properties)FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("picklistMapping");
        
        List<MetaPicklist> allowedPicklistList= new ArrayList<MetaPicklist>();
        
        if( picklistList != null )
        for(int i = 0 ; i < picklistList.size(); i++){
        	
        	
        	
        	MetaPicklist metaPicklist = picklistList.get(i);
        	String plistId = metaPicklist.getId()  ;
        	 
        	String permId =( String ) picklistMapping.getProperty(plistId, null);
        	
        	if(permId != null ){
        		allowedPicklistList.add(metaPicklist);
        	}
        	
        }	
    
        picklistList = allowedPicklistList;
        
	    ///////////////////
	    String picklistId =(String ) sessionMap.get("picklistId");
	    
	    // TODO : Change the Request Type to Parameter
	    //selectedPicklist = PickListManager.getMetaPickList("CommercialActivity");
	    //selectedPicklist = PickListManager.getMetaPickList("RequestType");
	    if( picklistId != null)
	    selectedPicklist = PickListManager.getMetaPickList(picklistId);
	    
	    
	    if (picklistList != null) 
	    {
	    	
			picklistTypes = new ArrayList<SelectItem>();
			for (MetaPicklist plMetaData : picklistList) 
			{
				
				SelectItem item = new SelectItem(plMetaData.getId().toString(), plMetaData.getPicklistName());
				picklistTypes.add(item);
			}
			Collections.sort(picklistTypes, ListComparator.LIST_COMPARE);
		}
	    else if(picklistTypesSelectOneMenu.getValue() != null)
	    {
			selectedPicklist = PickListManager.getMetaPickList(picklistTypesSelectOneMenu.getValue().toString());
		}
	    else
	    {
			selectedPicklist = PickListManager.getMetaPickList("RequestType");
		}
	    
	    getInit();
	    
	    /* Code Updated By Anil Verani Start */
	    dataEn.setRendered(false);
	    dataAr.setRendered(false);
	   // dataValue.setRendered(false);
	    if ( ! isPostBack() )
	    	selectedPicklistData = new ArrayList<PicklistData>(0);
	    /* Code Updated By Anil Verani End */
	    
       logger.logInfo(methodName+"|"+"Finish");
    }

    public String getInit() {
        if (selectedPicklist == null) {
            selectedPicklist = new MetaPicklist();
            selectedPicklistData = new ArrayList<PicklistData>();
        }
        else{
        	selectedPicklistData = PickListManager.getPickListData(selectedPicklist);
        	
        	
        			
        	try {
				oldPicklistData = PickListManager.getClonePickListData(selectedPicklistData);
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        if (newPicklistData == null) newPicklistData = new ArrayList<PicklistData>();

        newPicklistData.clear();

        return StringHelper.EMPTY;
    }

    public void setInit(String init) {
    }

    public List<MetaPicklist> getPicklistList() {
        if (picklistList == null) picklistList = PickListManager.getMetaPicklistList();
        return picklistList;
    }

    public void setPicklistList(List<MetaPicklist> picklistList) {
        this.picklistList = picklistList;
    }

    public String delete() {
        return SUCCESS;
    }

    public MetaPicklist getSelectedPicklist() {
        if (selectedPicklist == null) selectedPicklist = new MetaPicklist();

        return selectedPicklist;
    }

    public void setSelectedPicklist(MetaPicklist selectedPicklist) {
        this.selectedPicklist = selectedPicklist;
    }

    public List<PicklistData> getPicklistData() {
        if (selectedPicklist == null || StringHelper.isEmpty(selectedPicklist.getId())) return null;

        if (flag) {
            try {
                selectedPicklistData.clear();
                selectedPicklistData.addAll(PickListManager.getPickListData(selectedPicklist));
                flag = false;
                oldPicklistData = PickListManager.getClonePickListData(selectedPicklistData);
            } catch (Exception e) {
                selectedPicklistData.clear();
            }
        }
        return selectedPicklistData;
    }

    public void setPicklistData(List<PicklistData> data) {
        selectedPicklistData = data;
    }

    public void selectPicklist(ValueChangeEvent event) 
    {
        Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String picklistId = (String) requestMap.get("picklistId");

        System.out.println( event.getNewValue().toString());
        dataAr.setValue("");
        dataEn.setValue("");
        
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        
        if(event.getNewValue().toString().equals( "-1" ))
        {
        	selectedPicklist = new MetaPicklist();
            selectedPicklistData = new ArrayList<PicklistData>();
        }
        else if ( 
        			picklistId != null && 
        			StringHelper.isNotEmpty(picklistId) && 
        			(
        					selectedPicklist == null || 
        					!picklistId.equals(	selectedPicklist.getId() )
        			)
        		) 
        {
            selectedPicklist = PickListManager.getMetaPickList(picklistId);
            flag = true;
            getInit();
            
            sessionMap.put("picklistId", selectedPicklist.getId()); 
            permissionId = "Pims.Tendering.Tender";
            
            String mapping = picklistMapping.getProperty(selectedPicklist.getId());
            
            req.setAttribute("permissionValueId", mapping);
            
            permissionId = mapping;
        }
        else if(event.getNewValue().toString() != null )
        {
        	 selectedPicklist = PickListManager.getMetaPickList(event.getNewValue().toString());
             flag = true;
             getInit();
             
             sessionMap.put("picklistId", selectedPicklist.getId());
             
             String mapping = picklistMapping.getProperty(selectedPicklist.getId());
             
             req.setAttribute("permissionValueId", mapping);
             
             permissionId = mapping;
        }
        else{
        	 selectedPicklist = new MetaPicklist();
             selectedPicklistData = new ArrayList<PicklistData>();
        	
        }
        
        if(selectedPicklist == null ){
        	
        	selectedPicklist = new MetaPicklist();
            selectedPicklistData = new ArrayList<PicklistData>();
        	
        }
        
        sessionMap.remove("addNew"); 
        sessionMap.remove("mode"); 
        mode = false;
        flag = true;
        
        /* Code Updated By Anil Verani Start */
	    dataEn.setRendered(false);
	    dataAr.setRendered(false);
	//    dataValue.setRendered(false);
	    btnSaveType.setRendered(false);
	    btnAddType.setRendered(true);
	    /* Code Updated By Anil Verani End */
        
    }

    public void savePicklistData(ActionEvent event) 
    {
    	
    	WebContext webContext = ApplicationContext.getContext().get(WebContext.class);

    	UserDbImpl currentCustObj = (UserDbImpl) webContext.getAttribute(CoreConstants.CurrentUser);
        
        int retVal = 0;
        String msg;
        
        // Getting values to check the Mode ( Add , Edit , View ) 
        String strAddNew = (String)sessionMap.get("addNew"); 
        String strMode = (String)sessionMap.get("mode"); 
        
        if(strMode != null && strMode.equals("true"))
        {
        	
        	String dataPri = (String )dataEn.getValue();
            String dataSec = (String )dataAr.getValue();
            String dataNumber = (String )dataValue.getValue();
            
        	// TODO : Validation 
            if(dataSec == null || dataPri == null || dataSec == null ||
               dataPri.trim().equalsIgnoreCase("") || dataSec.trim().equalsIgnoreCase("") ) 
            {
            	
            	retVal = -2;
            }
           
            else if(strAddNew != null && strAddNew.equals("true"))
            { 
            	// Add New Case
                PicklistData newItem = new PicklistData(dataPri , dataSec , "");
                // Insert in Database
                retVal = PickListManager.insertPickListItem(selectedPicklist, newItem, currentCustObj.getLoginId());
                newPicklistData = new ArrayList<PicklistData>();
                newPicklistData.add(newItem);
                if (retVal == 1)
                {
                  PickListManager.addPickListData(selectedPicklist.getId(), newPicklistData);
                  selectedPicklist = PickListManager.getMetaPickList(picklistTypesSelectOneMenu.getValue().toString());
                  getInit();
                }
        	}
        	else
        	{ // Edit Case
        		
        		PicklistData oldItem = (PicklistData)sessionMap.get("OldPicklistData");
        		
        		if( oldItem != null ){
        			
	                PicklistData newItem = new PicklistData(dataPri , dataSec , oldItem.getValue() );
	                retVal = PickListManager.updatePickListItem(selectedPicklist, oldItem, newItem, currentCustObj.getLoginId());
	                PickListManager.removePickListData(selectedPicklist.getId() , oldItem);
	                //PickListManager.deletePickListItem(selectedPicklist, oldItem, newItem);
	                //PickListManager.cachedPicklistData.(newItem);
	             // Insert in Cache
	                
	                List<PicklistData> addList = new ArrayList<PicklistData>();
	                //newPicklistData = new ArrayList<PicklistData>();
	                addList.add(newItem);
	                
	                PickListManager.addPickListData(selectedPicklist.getId(), addList);
        		}
        		else{
        			retVal = -1;
        		}
        	}
        }
        
        if (retVal == -1)
        {
            msg = "Picklist data could not be saved.";
        }
        else if (retVal == -2)
        {
        	msg = "The fields are required. Please provide all the values.";
        }
        else
        {
            msg = "Data successfully saved.";
            sessionMap.remove("addNew"); 
            sessionMap.remove("mode"); 
            mode = false;
            flag = true;
            
            /* Code Updated By Anil Verani Start */
    	    dataEn.setRendered(false);
    	    dataAr.setRendered(false);
    	    //dataValue.setRendered(false);
    	    btnSaveType.setRendered(false);
    	    btnAddType.setRendered(true);
    	    /* Code Updated By Anil Verani End */
        }
        
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        sessionMap.put("picklistId", selectedPicklist.getId()); 
        permissionId = "Pims.Tendering.Tender";
        
        String mapping = picklistMapping.getProperty(selectedPicklist.getId());
        
        req.setAttribute("permissionValueId", mapping);
        
        permissionId = mapping;
        
        dataAr.setValue("");
        dataEn.setValue("");
       // dataValue.setValue("");
        
        FacesContext.getCurrentInstance().addMessage("savePicklistData", new FacesMessage(msg));              
    }


    public void addPicklistData() {
    	
        flag = false;
        mode = true;
        addNew = true;
        
        dataEn.getValue();
        
        dataAr.setValue("");
        dataEn.setValue("");
        //dataValue.setValue("");
        
        picklistTypesSelectOneMenu.getValue();
        PicklistData addData = new PicklistData(null, null, null);
        // To indicate that this is Not persisted
        addData.setSelected(false);
        
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        sessionMap.put("picklistId", selectedPicklist.getId()); 
        permissionId = "Pims.Tendering.Tender";
        
        String mapping = picklistMapping.getProperty(selectedPicklist.getId());
        
        req.setAttribute("permissionValueId", mapping);
        
        permissionId = mapping;
        
        newPicklistData = new ArrayList<PicklistData>();
        
        
        //selectedPicklistData.add(addData);
        sessionMap.put("picklistId", selectedPicklist.getId()); 
        sessionMap.put("addNew", "true"); 
        sessionMap.put("mode", "true");
        
        /* Code Updated By Anil Verani Start */
        dataEn.setRendered(true);
        dataAr.setRendered(true);
       // dataValue.setRendered(true);
        btnAddType.setRendered(false);
        btnSaveType.setRendered(true);
        /* Code Updated By Anil Verani End */
        
    }
    

    public void deletePicklistData() {

    	PicklistData dataToDelete = (PicklistData) dataTable.getRowData();
    	int aaa = PickListManager.deletePickListItem(selectedPicklist, dataToDelete);
    	
    	selectedPicklistData.remove(dataToDelete);
    	
    	
    	if(aaa > 0 )
    	{
    		FacesContext.getCurrentInstance().addMessage("deletePicklistData", new FacesMessage("Picklist data successfully deleted!"));
    	}else{
    		FacesContext.getCurrentInstance().addMessage("deletePicklistData", new FacesMessage("Picklist data not deleted!"));
    	}
    	
    	flag = true;
        mode = false;
        
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        sessionMap.put("picklistId", selectedPicklist.getId()); 
        permissionId = "Pims.Tendering.Tender";
        
        String mapping = picklistMapping.getProperty(selectedPicklist.getId());
        
        req.setAttribute("permissionValueId", mapping);
        
        permissionId = mapping;
    }
    
    
    public void editPicklistData() {
    	
  	flag = false;
    mode = true;
    addNew = false;
    
    PicklistData dataToEdit = (PicklistData) dataTable.getRowData();
  

    
    strdataEn = dataToEdit.getDisplayPrimary();
    strdataAr = dataToEdit.getDisplaySecondary();
    strdataValue = dataToEdit.getValue();
    
    HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    
    sessionMap.put("picklistId", selectedPicklist.getId()); 
    permissionId = "Pims.Tendering.Tender";
    
    String mapping = picklistMapping.getProperty(selectedPicklist.getId());
    
    req.setAttribute("permissionValueId", mapping);
    
    permissionId = mapping;
    
    sessionMap.put("OldPicklistData", dataToEdit );
    sessionMap.put("picklistId", selectedPicklist.getId()); 
    sessionMap.put("mode", "true");
    sessionMap.put("addNew", "false");
    /* Code Updated By Anil Verani Start */
    dataEn.setRendered(true);
    dataAr.setRendered(true);
  //  dataValue.setRendered(true);
    btnAddType.setRendered(false);
    btnSaveType.setRendered(true);
    dataEn.setValue(strdataEn);
    dataAr.setValue(strdataAr);
    dataValue.setValue(strdataValue);
    /* Code Updated By Anil Verani End */
  	
  }

	public List<SelectItem> getPicklistTypes() {
		return picklistTypes;
	}

	public void setPicklistTypes(List<SelectItem> picklistTypes) {
		this.picklistTypes = picklistTypes;
	}

	public Div getDiv() {
		return div;
	}

	public void setDiv(Div div) {
		this.div = div;
	}

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public HtmlCommandButton getBtnAddType() {
		return btnAddType;
	}

	public void setBtnAddType(HtmlCommandButton btnAddType) {
		this.btnAddType = btnAddType;
	}

	public HtmlCommandButton getBtnSaveType() {
		return btnSaveType;
	}

	public void setBtnSaveType(HtmlCommandButton btnSaveType) {
		this.btnSaveType = btnSaveType;
	}
	
	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {		 
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getRecordSize() {
		return selectedPicklistData.size();
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
}