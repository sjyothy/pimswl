package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.proxy.PIMSInsuranceTenderingBPELPortClient;
import com.avanza.pims.proxy.PIMSSecurityTenderingBPELPortClient;
import com.avanza.pims.proxy.pimstenderingmaintenancecontractbpelproxy.PIMSMaintenanceTenderingBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.construction.plugins.Contractors;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExtendApplicationFilterView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TenderContractorView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderPropertyView;
import com.avanza.pims.ws.vo.TenderProposalView;
import com.avanza.pims.ws.vo.TenderTypeView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.pims.ws.vo.TenderWorkScopeView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;

public class TenderManagement extends AbstractController {
	//Common Initialization
	private boolean isArabicLocale = false;
	private boolean canDelete= true;
	private boolean isEnglishLocale = false;

	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	RequestView requestView = new RequestView();
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	SystemParameters parameters = SystemParameters.getInstance();
	private transient Logger logger = Logger.getLogger(TenderManagement.class);
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil = new CommonUtil();
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	ConstructionServiceAgent constructionServiceAgent  = new ConstructionServiceAgent();

	private boolean isViewModeApproval=false;
	//Contractors Tab
	private HtmlCommandButton addContractor = new HtmlCommandButton();
	private String hdnContractorId;

	//Tender Details Tab
	private HtmlInputText tenderNumber = new HtmlInputText();
	private HtmlInputHidden hdnTenderId = new HtmlInputHidden();
	private HtmlInputText tenderStatue =new HtmlInputText();
	private HtmlInputText tenderDescription = new HtmlInputText();
	private HtmlInputText expectedCost = new HtmlInputText();
	private HtmlInputText tenderPrice = new HtmlInputText();
	private HtmlInputText contractorNumber = new HtmlInputText();
	private HtmlInputText contractorName = new HtmlInputText();
	private HtmlInputText submittedBy = new HtmlInputText();
	private HtmlInputText receivedBy = new HtmlInputText();
	private HtmlInputText serviceTenderType =new HtmlInputText();
	private HtmlInputTextarea proposalDescription = new HtmlInputTextarea();
	private HtmlInputText estimatedCost = new HtmlInputText();

	private HtmlInputText tenderDocSellingPrice = new HtmlInputText();
	private HtmlInputTextarea actionRemarks=new HtmlInputTextarea();
	private Date endDate;
	private Date startDate ;
	private Date submittedDate ;
	private HtmlSelectOneMenu tenderType = new HtmlSelectOneMenu();
	private List<SelectItem> tenderTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu startDateHoursMenu =new HtmlSelectOneMenu();
	private HtmlSelectOneMenu endDateHoursMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu startDateMinsMenu =new HtmlSelectOneMenu();
	private HtmlSelectOneMenu endDateMinsMenu = new HtmlSelectOneMenu();
	private List<SelectItem> hoursList = new ArrayList<SelectItem>();
	private List<SelectItem> minsList = new ArrayList<SelectItem>();
	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton sendButton = new HtmlCommandButton();
	private HtmlCommandButton undoWinnerMarked = new HtmlCommandButton();
	public HtmlTab inquiryAppTab = new HtmlTab();
	//Property Details Tab
	private HtmlDataTable propertyGrid = new HtmlDataTable();
	private List<PropertyView> propertyDataList;
	private HtmlCommandButton addUnit = new HtmlCommandButton();
	private HtmlCommandButton addProperty = new HtmlCommandButton();
	//Scope of Work Tab
	private HtmlSelectOneMenu workTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> workTypeItems = new ArrayList<SelectItem>();
	private HtmlInputTextarea scopeWorkDescription = new HtmlInputTextarea();
	private HtmlDataTable scopeOfWorkDataTable = new HtmlDataTable();
//	private List<WorkScopeView> scopeOfWorkList= new ArrayList<WorkScopeView>();
	private HtmlCommandButton saveScopeOfWork = new HtmlCommandButton();

	//Approve Tender Data
	private Date actionDate ;
	private HtmlInputTextarea commentsText = new HtmlInputTextarea();
	private HtmlCommandButton approveButton= new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	//Session Details Tab
	private Date sessionDate ;
	private HtmlSelectOneMenu sessionTimeHourMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu sessionTimeEndMenu = new HtmlSelectOneMenu();
	private HtmlInputText sessionLocation = new HtmlInputText();
	private HtmlInputTextarea sessionRemarks = new HtmlInputTextarea();
	//Committe Memebers Tab
	private HtmlDataTable committeMemebersDataTable = new HtmlDataTable();
	private HtmlCommandButton addCommittee = new HtmlCommandButton();
	private List<UserView> committeMemebersDataList = new ArrayList<UserView>();
	//Proposals View Tab
	private HtmlDataTable proposalDatatable = new HtmlDataTable();
//	private List<ProposalView> proposalDataList = new ArrayList<ProposalView>();
	//Inquries App Tab
	private HtmlDataTable inquiryDataTable = new HtmlDataTable();
	private List<RequestView> inquiryDataList = new ArrayList<RequestView>();
	private HtmlCommandButton replyToSelected = new HtmlCommandButton();
	//Extended App Tab
	private HtmlDataTable extendedDataTable = new HtmlDataTable();
	private List<ExtendApplicationFilterView> extendedDataList = new ArrayList<ExtendApplicationFilterView>();
	private HtmlCommandButton extend= new HtmlCommandButton();
	private List<TenderInquiryView> tenderInquiryList = new ArrayList<TenderInquiryView>();
	public HtmlTab extendsAppTab = new HtmlTab();

	Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	private String pageMode = "";
	private Boolean canAdd = true;

	private final String noteOwner = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_SERVICE_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	private static final String TENDER_INFORMATION_FOR_UPDATE = "TENDER_INFORMATION_FOR_UPDATE";

    @SuppressWarnings( "unchecked" )
	public void init(){
		super.init();
		try {

		   if(!isPostBack())
		   {
			modeAdd();
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
		   }

			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS),
					                                      Constant.TENDER_STATUS_NEW);
			DomainDataView ddv2 =CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.TENDER_TYPE_NEW), WebConstants.TENDER_TYPE_SERVICE);

			if( !( sessionMap.containsKey(WebConstants.Tender.TENDER_ID) &&
				   sessionMap.containsKey(WebConstants.Tender.TENDER_MODE)
				 )
			  )
			{
				if( !getIsArabicLocale() )
				{
					tenderStatue.setValue(ddv.getDataDescEn());
				}
				else
				{
					tenderStatue.setValue(ddv.getDataDescAr());
				}
			}

			if(!getIsArabicLocale())
			{
				serviceTenderType.setValue(ddv2.getDataDescEn());
			}
			else
			{
				serviceTenderType.setValue(ddv2.getDataDescAr());
			}


		if(sessionMap.get(WebConstants.Tender.TENDER_ID)!=null &&
		   sessionMap.get(WebConstants.Tender.TENDER_MODE)!=null
		  )
		{
			Long tenderId = (Long)sessionMap.remove(WebConstants.Tender.TENDER_ID);
			String mode = sessionMap.remove(WebConstants.Tender.TENDER_MODE).toString();

			if(mode.equals("TENDER_MODE_VIEW"))
			{
				modeView();
			}
			else if(mode.equals("TENDER_MODE_EDIT"))
			{
				modeEdit();
			}
			if(sessionMap.get(WebConstants.Tender.TENDER_MODE_POPUP)!=null)
			{
				viewMap.put(WebConstants.Tender.TENDER_MODE_POPUP, sessionMap.remove(WebConstants.Tender.TENDER_MODE_POPUP));
			}
			populateTabs(tenderId);
		 }
		if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))

		{
			Long tenderId = 0L;
			Long requestId = 0L;
			UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			viewMap.put(WebConstants.USER_TASK_TENDER_MANAGEMENT,userTask);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			if(userTask!=null)
			{
				if(userTask.getTaskAttributes().get("TENDER_ID")!=null)
				{
					tenderId = Convert.toLong(userTask.getTaskAttributes().get("TENDER_ID"));
					populateTabs(tenderId);
				}

				String taskType = userTask.getTaskType();
				viewMap.put(WebConstants.TASK_TYPE, taskType);
				if(taskType.equals(WebConstants.UserTasks.TenderManagement.APPROVE_MAINTENANCE_TENDER)){
					pageMode="PAGE_MODE_APPROVAL";
					viewMap.put("PAGE_MODE",pageMode);
				}
				if(taskType.equals(WebConstants.UserTasks.TenderManagement.APPROVE_INSURANCE_TENDER)){
					pageMode="PAGE_MODE_APPROVAL";
					viewMap.put("PAGE_MODE",pageMode);
				}
				if(taskType.equals(WebConstants.UserTasks.TenderManagement.APPROVE_SECURITY_TENDER)){
					pageMode="PAGE_MODE_APPROVAL";
					viewMap.put("PAGE_MODE",pageMode);
				}
			}

		}
		if(viewMap.containsKey(WebConstants.USER_TASK_TENDER_MANAGEMENT)&&
				viewMap.get(WebConstants.USER_TASK_TENDER_MANAGEMENT)!=null)
		{
			modeApproveReject();
		}

		if(sessionMap.containsKey(WebConstants.Tender.REQUEST_ID))
		{
		  viewMap.put(WebConstants.Tender.REQUEST_ID, sessionMap.get(WebConstants.Tender.REQUEST_ID));
		  sessionMap.remove(WebConstants.Tender.REQUEST_ID);
		}

	     }
		catch (Exception e1)
		{

			logger.LogException("init|Error Occured..",e1);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

	    }

	}
    @SuppressWarnings("unchecked"  )
	public void preprocess(){
		super.preprocess();
		PropertyView propertyView=null;
		UnitView unitView = null;
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

		if(getPropertyDataList()==null)
			propertyDataList = new ArrayList<PropertyView>();
		//Object sessionObj = ApplicationContext.getContext().get(WebContext.class).getAttribute(WebConstants.UnitDetails.PROPERTY_VIEW);
		if(sessionMap.get("MANYPROPERTYINFO")!=null)
		{
			List<PropertyView> pvList= (ArrayList<PropertyView>)sessionMap.get("MANYPROPERTYINFO");
			sessionMap.remove("MANYPROPERTYINFO");
				if(pvList != null && pvList.size()>0)
				{
					boolean isExist = false;
					List<PropertyView> tempPropertyList = new ArrayList<PropertyView>();

					for(PropertyView pvSelected:pvList)
					{
					 isExist = false;
					  for(PropertyView tempPropertyView:propertyDataList)
						{
							// if property already present in the list
							if(!(tempPropertyView.getUnitRefNo()!=null)
								 &&  tempPropertyView.getPropertyId().equals(pvSelected.getPropertyId()) )
							{
								isExist = true;
								break;
							}


						}

					  if(!isExist)
						{
						    pvSelected.setTenderPropertyIsDeleted(0L);
							getPropertyDataList().add(pvSelected);
						}
					}

				 viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);
			   }
		}
		try {
		if(	sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)
	    &&sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)!=null){
			List<UnitView> unitViewList = (ArrayList<UnitView>)sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
			sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
			for(UnitView tempUnitView:unitViewList){

				for(PropertyView propertyView2:getPropertyDataList())
				{   // if unit already present in  the list
					canAdd = true;
					if(propertyView2.getUnitId()!=null && propertyView2.getUnitId().compareTo(tempUnitView.getUnitId())==0)
					{
						canAdd = false;
						break;
					}
				}
				if(canAdd && tempUnitView.getPropertyId()!=null)
				{
					propertyView = propertyServiceAgent.getPropertyByID(tempUnitView.getPropertyId());


					propertyView.setUnitRefNo(tempUnitView.getUnitNumber());
					propertyView.setTenderPropertyIsDeleted(0L);
					propertyView.setUnitId(tempUnitView.getUnitId());
					getPropertyDataList().add(propertyView);
				}


			}
			viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);

		}
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.LogException("saveAction|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}

		if(sessionMap.containsKey(WebConstants.UNIT_VIEW)&&
				sessionMap.get(WebConstants.UNIT_VIEW)!=null)
		{
			unitView = (UnitView) sessionMap.get(WebConstants.UNIT_VIEW);
			sessionMap.remove(WebConstants.UNIT_VIEW);
			try {

				if(unitView.getPropertyId()!=null){
					propertyView = propertyServiceAgent.getPropertyByID(unitView.getPropertyId());


					propertyView.setUnitRefNo(unitView.getUnitNumber());
					getPropertyDataList().add(propertyView);
				}
				viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);

			} catch (PimsBusinessException e) {
				// TODO Auto-generated catch block
				logger.LogException("saveAction|Error Occured..",e);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

			}
		}


		/*if(viewMap.containsKey("CONTRACTOR_ALREADY_EXIST")&&
				Boolean.valueOf(viewMap.get("CONTRACTOR_ALREADY_EXIST").toString())){
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.CONTRACTOR_ALREADY_EXIST));
			viewMap.remove("CONTRACTOR_ALREADY_EXIST");
		}

		if(viewMap.containsKey("MULTIPLE_CONTRACTOR_ALREADY_EXIST")&&
				Boolean.valueOf(viewMap.get("MULTIPLE_CONTRACTOR_ALREADY_EXIST").toString())){
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MULTIPLE_CONTRACTOR_ALREADY_EXIST));
			viewMap.remove("MULTIPLE_CONTRACTOR_ALREADY_EXIST");
		}*/

	}
    @SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		try
		{
				if(hdnContractorId!=null){
					ConstructionServiceAgent csa=new ConstructionServiceAgent();
			    		ContractorView contractorView = new ContractorView();

				    	if(hdnContractorId!=null && hdnContractorId.toString().trim().length()>0)
				    	{
							contractorView =  csa.getContractorById(new Long(hdnContractorId), null);
				    	    viewMap.put("CONTRACTOR_VIEW", contractorView );
				    	    hdnContractorId="";
				    	}
				    	else if(sessionMap.containsKey(WebConstants.MANY_CONTRACTORS_SELECTED) &&
								sessionMap.get(WebConstants.MANY_CONTRACTORS_SELECTED) != null)
				    	{
				    			List<ContractorView> contractorsList = (ArrayList<ContractorView>)sessionMap.get(WebConstants.MANY_CONTRACTORS_SELECTED);
								sessionMap.remove(WebConstants.MANY_CONTRACTORS_SELECTED);
								viewMap.put(WebConstants.MANY_CONTRACTORS_SELECTED, contractorsList );
				    	}

				}

				if(viewMap.get("CONTRACTOR_VIEW")!=null || viewMap.get(WebConstants.MANY_CONTRACTORS_SELECTED)!=null)
					 new Contractors();

				if(viewMap.containsKey("CONTRACTOR_ALREADY_EXIST")&&
						Boolean.valueOf(viewMap.get("CONTRACTOR_ALREADY_EXIST").toString())){
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.CONTRACTOR_ALREADY_EXIST));
					viewMap.remove("CONTRACTOR_ALREADY_EXIST");
				}

				if(viewMap.containsKey("MULTIPLE_CONTRACTOR_ALREADY_EXIST")&&
						Boolean.valueOf(viewMap.get("MULTIPLE_CONTRACTOR_ALREADY_EXIST").toString())){
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MULTIPLE_CONTRACTOR_ALREADY_EXIST));
					viewMap.remove("MULTIPLE_CONTRACTOR_ALREADY_EXIST");
				}
		}

		catch(Exception e)
		{
			logger.LogException("prerender|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}





	public Date getActionDate() {
		if(viewMap.containsKey("ACTION_DATE"))
			actionDate = (Date)(viewMap.get("ACTION_DATE"));
		return actionDate;
	}
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
		if(this.actionDate!=null)
		viewMap.put("ACTION_DATE", actionDate);
	}
	public HtmlInputTextarea getActionRemarks() {
		return actionRemarks;
	}
	public void setActionRemarks(HtmlInputTextarea actionRemarks) {
		this.actionRemarks = actionRemarks;
	}
	public HtmlCommandButton getAddCommittee() {
		return addCommittee;
	}
	public void setAddCommittee(HtmlCommandButton addCommittee) {
		this.addCommittee = addCommittee;
	}

	public HtmlCommandButton getAddProperty() {
		return addProperty;
	}
	public void setAddProperty(HtmlCommandButton addProperty) {
		this.addProperty = addProperty;
	}
	public HtmlCommandButton getAddUnit() {
		return addUnit;
	}
	public void setAddUnit(HtmlCommandButton addUnit) {
		this.addUnit = addUnit;
	}
	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	public HtmlInputTextarea getCommentsText() {
		return commentsText;
	}
	public void setCommentsText(HtmlInputTextarea commentsText) {
		this.commentsText = commentsText;
	}
	public List<UserView> getCommitteMemebersDataList() {
		return committeMemebersDataList;
	}
	public void setCommitteMemebersDataList(List<UserView> committeMemebersDataList) {
		this.committeMemebersDataList = committeMemebersDataList;
	}
	public HtmlDataTable getCommitteMemebersDataTable() {
		return committeMemebersDataTable;
	}
	public void setCommitteMemebersDataTable(HtmlDataTable committeMemebersDataTable) {
		this.committeMemebersDataTable = committeMemebersDataTable;
	}

	public HtmlInputText getContractorName() {
		return contractorName;
	}
	public void setContractorName(HtmlInputText contractorName) {
		this.contractorName = contractorName;
	}
	public HtmlInputText getContractorNumber() {
		return contractorNumber;
	}
	public void setContractorNumber(HtmlInputText contractorNumber) {
		this.contractorNumber = contractorNumber;
	}

	public Date getEndDate() {
		if(viewMap.containsKey("END_DATE"))
			endDate = (Date)(viewMap.get("END_DATE"));
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		if(this.endDate!=null)
			viewMap.put("END_DATE", endDate);
	}
	public HtmlSelectOneMenu getEndDateHoursMenu() {
		return endDateHoursMenu;
	}
	public void setEndDateHoursMenu(HtmlSelectOneMenu endDateHoursMenu) {
		this.endDateHoursMenu = endDateHoursMenu;
	}
	public HtmlSelectOneMenu getEndDateMinsMenu() {
		return endDateMinsMenu;
	}
	public void setEndDateMinsMenu(HtmlSelectOneMenu endDateMinsMenu) {
		this.endDateMinsMenu = endDateMinsMenu;
	}
	public HtmlInputText getExpectedCost() {
		return expectedCost;
	}
	public void setExpectedCost(HtmlInputText expectedCost) {
		this.expectedCost = expectedCost;
	}
	public HtmlCommandButton getExtend() {
		return extend;
	}
	public void setExtend(HtmlCommandButton extend) {
		this.extend = extend;
	}
	public List<ExtendApplicationFilterView> getExtendedDataList() {
		if(viewMap.get("EXTEND_APPLICATION_LIST")!=null)
			extendedDataList = (List<ExtendApplicationFilterView>)viewMap.get("EXTEND_APPLICATION_LIST");
		return extendedDataList;
	}
	public void setExtendedDataList(List<ExtendApplicationFilterView> extendedDataList) {
		this.extendedDataList = extendedDataList;
		if(this.extendedDataList!=null)
			viewMap.put("EXTEND_APPLICATION_LIST",this.extendedDataList);
	}
	public Integer getExtendAppRecordSize() {
		return extendedDataList.size();
	}
	public HtmlDataTable getExtendedDataTable() {
		return extendedDataTable;
	}
	public void setExtendedDataTable(HtmlDataTable extendedDataTable) {
		this.extendedDataTable = extendedDataTable;
	}
	public List<SelectItem> getHoursList() {
		return hoursList;
	}
	public void setHoursList(List<SelectItem> hoursList) {
		this.hoursList = hoursList;
	}
	public List<RequestView> getInquiryDataList() {
		return inquiryDataList;
	}
	public void setInquiryDataList(List<RequestView> inquiryDataList) {
		this.inquiryDataList = inquiryDataList;
	}
	public HtmlDataTable getInquiryDataTable() {
		return inquiryDataTable;
	}
	public void setInquiryDataTable(HtmlDataTable inquiryDataTable) {
		this.inquiryDataTable = inquiryDataTable;
	}
	public List<SelectItem> getMinsList() {
		return minsList;
	}
	public void setMinsList(List<SelectItem> minsList) {
		this.minsList = minsList;
	}
	public List<PropertyView> getPropertyDataList() {
		if(viewMap.containsKey(WebConstants.Contract.CONTRACT_UNIT_LIST))
			propertyDataList = (List<PropertyView>) viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);

		if(propertyDataList == null)
			propertyDataList = new ArrayList<PropertyView>();


		return propertyDataList;
	}
	public void setPropertyDataList(List<PropertyView> propertyDataList) {
		this.propertyDataList = propertyDataList;
	}
	public HtmlDataTable getPropertyGrid() {
		return propertyGrid;
	}
	public void setPropertyGrid(HtmlDataTable propertyGrid) {
		this.propertyGrid = propertyGrid;
	}
	public HtmlDataTable getProposalDatatable() {
		return proposalDatatable;
	}
	public void setProposalDatatable(HtmlDataTable proposalDatatable) {
		this.proposalDatatable = proposalDatatable;
	}
	public HtmlInputTextarea getProposalDescription() {
		return proposalDescription;
	}
	public void setProposalDescription(HtmlInputTextarea proposalDescription) {
		this.proposalDescription = proposalDescription;
	}
	public HtmlInputText getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(HtmlInputText receivedBy) {
		this.receivedBy = receivedBy;
	}
	public HtmlCommandButton getReplyToSelected() {
		return replyToSelected;
	}
	public void setReplyToSelected(HtmlCommandButton replyToSelected) {
		this.replyToSelected = replyToSelected;
	}
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	public HtmlCommandButton getSaveScopeOfWork() {
		return saveScopeOfWork;
	}
	public void setSaveScopeOfWork(HtmlCommandButton saveScopeOfWork) {
		this.saveScopeOfWork = saveScopeOfWork;
	}
	public HtmlDataTable getScopeOfWorkDataTable() {
		return scopeOfWorkDataTable;
	}
	public void setScopeOfWorkDataTable(HtmlDataTable scopeOfWorkDataTable) {
		this.scopeOfWorkDataTable = scopeOfWorkDataTable;
	}
	public HtmlInputTextarea getScopeWorkDescription() {
		return scopeWorkDescription;
	}
	public void setScopeWorkDescription(HtmlInputTextarea scopeWorkDescription) {
		this.scopeWorkDescription = scopeWorkDescription;
	}
	public HtmlCommandButton getSendButton() {
		return sendButton;
	}
	public void setSendButton(HtmlCommandButton sendButton) {
		this.sendButton = sendButton;
	}
	public Date getSessionDate() {
		return sessionDate;
	}
	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}
	public HtmlInputText getSessionLocation() {
		return sessionLocation;
	}
	public void setSessionLocation(HtmlInputText sessionLocation) {
		this.sessionLocation = sessionLocation;
	}
	public HtmlInputTextarea getSessionRemarks() {
		return sessionRemarks;
	}
	public void setSessionRemarks(HtmlInputTextarea sessionRemarks) {
		this.sessionRemarks = sessionRemarks;
	}
	public HtmlSelectOneMenu getSessionTimeEndMenu() {
		return sessionTimeEndMenu;
	}
	public void setSessionTimeEndMenu(HtmlSelectOneMenu sessionTimeEndMenu) {
		this.sessionTimeEndMenu = sessionTimeEndMenu;
	}
	public HtmlSelectOneMenu getSessionTimeHourMenu() {
		return sessionTimeHourMenu;
	}
	public void setSessionTimeHourMenu(HtmlSelectOneMenu sessionTimeHourMenu) {
		this.sessionTimeHourMenu = sessionTimeHourMenu;
	}
	public Date getStartDate() {
		if(viewMap.containsKey("START_DATE"))
			startDate = (Date)(viewMap.get("START_DATE"));
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		if(this.startDate!=null)
			viewMap.put("START_DATE", startDate);
	}
	public HtmlSelectOneMenu getStartDateHoursMenu() {
		return startDateHoursMenu;
	}
	public void setStartDateHoursMenu(HtmlSelectOneMenu startDateHoursMenu) {
		this.startDateHoursMenu = startDateHoursMenu;
	}
	public HtmlSelectOneMenu getStartDateMinsMenu() {
		return startDateMinsMenu;
	}
	public void setStartDateMinsMenu(HtmlSelectOneMenu startDateMinsMenu) {
		this.startDateMinsMenu = startDateMinsMenu;
	}
	public HtmlInputText getSubmittedBy() {
		return submittedBy;
	}
	public void setSubmittedBy(HtmlInputText submittedBy) {
		this.submittedBy = submittedBy;
	}
	public Date getSubmittedDate() {
		return submittedDate;
	}
	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	public HtmlInputText getTenderDescription() {
		return tenderDescription;
	}
	public void setTenderDescription(HtmlInputText tenderDescription) {
		this.tenderDescription = tenderDescription;
	}
	public HtmlInputText getTenderDocSellingPrice() {
		return tenderDocSellingPrice;
	}
	public void setTenderDocSellingPrice(HtmlInputText tenderDocSellingPrice) {
		this.tenderDocSellingPrice = tenderDocSellingPrice;
	}
	public HtmlInputText getTenderNumber() {
		return tenderNumber;
	}
	public void setTenderNumber(HtmlInputText tenderNumber) {
		this.tenderNumber = tenderNumber;
	}
	public HtmlInputText getTenderPrice() {
		return tenderPrice;
	}
	public void setTenderPrice(HtmlInputText tenderPrice) {
		this.tenderPrice = tenderPrice;
	}
	public HtmlInputText getTenderStatue() {
		return tenderStatue;
	}
	public void setTenderStatue(HtmlInputText tenderStatue) {
		this.tenderStatue = tenderStatue;
	}
	public HtmlSelectOneMenu getTenderType() {
		return tenderType;
	}
	public void setTenderType(HtmlSelectOneMenu tenderType) {
		this.tenderType = tenderType;
	}
	public List<SelectItem> getTenderTypeList() {
		return tenderTypeList;
	}
	public void setTenderTypeList(List<SelectItem> tenderTypeList) {
		this.tenderTypeList = tenderTypeList;
	}
	public List<SelectItem> getWorkTypeItems() {
		return workTypeItems;
	}
	public void setWorkTypeItems(List<SelectItem> workTypeItems) {
		this.workTypeItems = workTypeItems;
	}
	public HtmlSelectOneMenu getWorkTypeMenu() {
		return workTypeMenu;
	}
	public void setWorkTypeMenu(HtmlSelectOneMenu workTypeMenu) {
		this.workTypeMenu = workTypeMenu;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public HtmlInputText getEstimatedCost() {
		return estimatedCost;
	}
	public void setEstimatedCost(HtmlInputText estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getLocale(){

		return new CommonUtil().getLocale();
	}

	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
	public java.util.TimeZone getTimeZone(){
		return CommonUtil.getTimeZone();
	}

	public Integer getInquiryAppRecordSize() {
		return tenderInquiryList.size();
	}
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	//Property Tab Methods
	@SuppressWarnings( "unchecked" )
public String cmdPropertyDelete_Click(){
	List<PropertyView> tempPropertyList = new ArrayList<PropertyView>();
	PropertyView propertyView = (PropertyView)propertyGrid.getRowData();

	if(viewMap.containsKey(WebConstants.Contract.CONTRACT_UNIT_LIST)&&
			viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST)!=null){

		propertyDataList= (List<PropertyView>)viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);
		// if property not is DB
		if(!(propertyView.getTenderPropertyId()!=null))
		{
			propertyDataList.remove(propertyView);
			viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);
			return "Done";
		}
	//to delete property from list
	if(!(propertyView.getUnitRefNo()!=null))
	{
		for(PropertyView tempPropertyView:propertyDataList){
			if( !(tempPropertyView.getUnitRefNo()!=null) && //bcoz in case of unit is null
					!(tempPropertyView.getPropertyId().compareTo(propertyView.getPropertyId())==0 ))
			{
				tempPropertyList.add(tempPropertyView);
			}
			else if( !(tempPropertyView.getUnitRefNo()!=null) && //bcoz in case of unit is null
					    (tempPropertyView.getPropertyId().compareTo(propertyView.getPropertyId())==0 ))
			{
				tempPropertyView.setTenderPropertyIsDeleted(1L);
				tempPropertyList.add(tempPropertyView);
			}
			else if(tempPropertyView.getUnitRefNo()!=null )
			{
				tempPropertyList.add(tempPropertyView);
			}
		}
		viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST, tempPropertyList);
		viewMap.put("recordSize",tempPropertyList.size());
		getPropertyRecordSize();
	}
	//to delete Unit from list
	else
	    {
		for(PropertyView tempPropertyView:propertyDataList){
			if( (tempPropertyView.getUnitId()!=null) && //bcoz in case of unit list it is not null
					!(tempPropertyView.getUnitId().compareTo(propertyView.getUnitId())==0 ))
			{
				tempPropertyList.add(tempPropertyView);
			}else if( (tempPropertyView.getUnitId()!=null) && //bcoz in case of unit list it is not null
					(tempPropertyView.getUnitId().compareTo(propertyView.getUnitId())==0 ))
			{
				tempPropertyView.setTenderPropertyIsDeleted(1L);
				tempPropertyList.add(tempPropertyView);
			}
			else if(!(tempPropertyView.getUnitId()!=null ))
			{
				tempPropertyList.add(tempPropertyView);
			}
		}
		viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST, tempPropertyList);
		viewMap.put("recordSize",tempPropertyList.size());
		getPropertyRecordSize();
		}
	}

	return "success";

}

	public void showUnitsPopup(){
		String javaScriptText = "javascript:showUnitPopup();";
		openPopup(javaScriptText);
	}
	public void showPropertyPopup(){
		String javaScriptText = "javascript:showPropertyPopup();";
		openPopup(javaScriptText);
	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public Integer getPropertyRecordSize(){

		if(getFacesContext().getViewRoot().getAttributes().containsKey(WebConstants.Contract.CONTRACT_UNIT_LIST))
					propertyDataList = (ArrayList<PropertyView>)viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);

		return propertyDataList.size();
	}
	public HtmlCommandButton getAddContractor() {
		return addContractor;
	}
	public void setAddContractor(HtmlCommandButton addContractor) {
		this.addContractor = addContractor;
	}
	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}
	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}
	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}
	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}

	public void  approveButton_action() {
		try {

			if(commentsText !=null && commentsText.getValue()!=null && commentsText.getValue().toString().length()>0)
			{
		        ApproveRejectTask(TaskOutcome.APPROVE);
				Long tenderId = Long.valueOf(hdnTenderId.getValue().toString());


				if(tenderId !=null)
				{
				saveAttachmentComments("tender.event.approved", tenderId);

				CommonUtil.saveRemarksAsComments(tenderId, commentsText.getValue().toString(), noteOwner);
				}

				modeView();
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_SUCCESS_APPROVED));
			}else
			{
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			}

			} catch (Exception e) {
				logger.LogException("approveButton_action|Error Occured..",e);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}

	}

	public void rejectButton_action() {
		try {

			if(commentsText !=null && commentsText.getValue()!=null && commentsText.getValue().toString().length()>0)
			{

			ApproveRejectTask(TaskOutcome.REJECT);
			Long tenderId = Long.valueOf(hdnTenderId.getValue().toString());
			if(tenderId !=null)
			{
 			saveAttachmentComments("tender.event.rejected", tenderId);
			CommonUtil.saveRemarksAsComments(tenderId, commentsText.getValue().toString(), noteOwner);
			}

			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_SUCCESS_REJECTED));
			modeView();
			}else
			{
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.LogException("rejectButton_action|Error Occured..",e);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}

	}
	@SuppressWarnings( "unchecked" )
	private void modeView(){
		tenderNumber.setReadonly(true);
		tenderType.setReadonly(true);
		tenderDescription.setReadonly(true);
		expectedCost.setReadonly(true);
		tenderPrice.setReadonly(true);

		undoWinnerMarked.setRendered(true);
		sendButton.setRendered(false);
		approveButton.setRendered(false);
		rejectButton.setRendered(false);
		addContractor.setRendered(false);

		addProperty.setRendered(false);
		addUnit.setRendered(false);
		cancelButton.setRendered(false);
		addCommittee.setRendered(false);
		saveButton.setRendered(false);
		inquiryAppTab.setRendered(true);
		extendsAppTab.setRendered(true);
		viewMap.put("canAddAttachment",false);
		viewMap.put("canAddWorkScope",false);
		viewMap.put("modeAction",true);
		viewMap.put("canAddNote",false);
		viewMap.put("CAN_DELETE_TENDER_PROPERTY",false);
		viewMap.put("CAN_DELETE_TENDER_CONTRACTOR",false);
		viewMap.put("CAN_DELETE_TENDER_SCOPE_OF_WORK",false);
		viewMap.put("CAN_ADD_TENDER_SCOPE_OF_WORK",false);




	}
	@SuppressWarnings( "unchecked" )
	private void modeApproveReject(){
		tenderNumber.setReadonly(true);
		tenderType.setReadonly(true);
		tenderDescription.setReadonly(true);
		expectedCost.setReadonly(true);
		tenderPrice.setReadonly(true);

		undoWinnerMarked.setRendered(false);
		sendButton.setRendered(false);
		approveButton.setRendered(true);
		rejectButton.setRendered(true);
		addContractor.setRendered(false);

		addProperty.setRendered(false);
		addUnit.setRendered(false);
		cancelButton.setRendered(false);
		addCommittee.setRendered(false);
		saveButton.setRendered(false);
		inquiryAppTab.setRendered(true);
		extendsAppTab.setRendered(true);
		viewMap.put("canAddAttachment",false);
		viewMap.put("canAddWorkScope",false);
		viewMap.put("modeAction",true);
		viewMap.put("canAddNote",false);
		viewMap.put("CAN_DELETE_TENDER_PROPERTY",false);
		viewMap.put("CAN_DELETE_TENDER_CONTRACTOR",false);
		viewMap.put("CAN_DELETE_TENDER_SCOPE_OF_WORK",false);
		viewMap.put("CAN_ADD_TENDER_SCOPE_OF_WORK",false);




	}
	@SuppressWarnings( "unchecked" )
	private void modeEdit(){
		tenderNumber.setReadonly(true);

		tenderType.setReadonly(false);
		tenderDescription.setReadonly(false);
		expectedCost.setReadonly(false);
		tenderPrice.setReadonly(false);

		undoWinnerMarked.setRendered(false);
		sendButton.setRendered(true);
		approveButton.setRendered(true);
		rejectButton.setRendered(true);
		addContractor.setRendered(true);

		addProperty.setRendered(true);
		addUnit.setRendered(true);
		cancelButton.setRendered(true);
		addCommittee.setRendered(true);
		saveButton.setRendered(true);
		inquiryAppTab.setRendered(true);
		extendsAppTab.setRendered(true);
		viewMap.put("canAddAttachment",true);
		viewMap.put("canAddWorkScope",true);
		viewMap.put("modeAction",false);
		viewMap.put("canAddNote",true);
	}
	@SuppressWarnings( "unchecked" )
	private void modeAdd(){
		tenderNumber.setReadonly(true);

		tenderType.setReadonly(false);
		tenderDescription.setReadonly(false);
		expectedCost.setReadonly(false);
		tenderPrice.setReadonly(false);

		undoWinnerMarked.setRendered(false);
		sendButton.setRendered(false);
		approveButton.setRendered(true);
		rejectButton.setRendered(true);
		addContractor.setRendered(true);

		addProperty.setRendered(true);
		addUnit.setRendered(true);
		cancelButton.setRendered(true);
		addCommittee.setRendered(true);
		saveButton.setRendered(true);
		inquiryAppTab.setRendered(false);
		extendsAppTab.setRendered(false);
		viewMap.put("canAddAttachment",true);
		viewMap.put("canAddWorkScope",true);
		viewMap.put("modeAction",false);
		viewMap.put("canAddNote",true);
	}


	@SuppressWarnings( "unchecked" )
	private void saveAttachmentComments(String sysNote,long id) throws Exception
	{
		CommonUtil.loadAttachmentsAndComments( String.valueOf( id ) );
		CommonUtil.saveAttachments(id);
		CommonUtil.saveComments(id, noteOwner);
		if(sysNote != null && sysNote.length()>0)
		{
		 CommonUtil.saveSystemComments(noteOwner, sysNote, id );
		}

	}
	@SuppressWarnings( "unchecked" )
	public void saveAction()
	{
		try
		{
			TenderView tenderView = null;
			boolean isUpdate = false;
			String sysNote="tender.event.saved";
			if(validateValues())
			{
				tenderView = fillTenderValues();
				TenderView tenderInformation =  getTenderView();
				if(tenderInformation !=null )
				{
					// for update set updated on and updated by
					tenderView.setTenderId(tenderInformation.getTenderId());
					tenderView.setTenderNumber(tenderInformation.getTenderNumber());
					tenderView.setCreatedBy(tenderInformation.getCreatedBy());
					tenderView.setCreatedOn(tenderInformation.getCreatedOn());
					tenderView.setIsDeleted(tenderInformation.getIsDeleted());
					tenderView.setRecordStatus(tenderInformation.getRecordStatus());
					tenderView.setUpdatedOn(new Date());
					tenderView.setUpdatedBy(getLoggedInUser());
					isUpdate = true;
					sysNote = "tender.event.updated";

				}
				tenderView = constructionServiceAgent.addTender(tenderView);
				// for edit mode
				sessionMap.put(WebConstants.Tender.TENDER_ID,tenderView.getTenderId());
				sessionMap.put(WebConstants.Tender.TENDER_MODE,"TENDER_MODE_EDIT");
				tenderNumber.setValue(tenderView.getTenderNumber());
				if(!getIsArabicLocale())
				{
					tenderStatue.setValue(tenderView.getStatusEn());
				}
				else
				{
					tenderStatue.setValue(tenderView.getStatusAr());
				}

				setTenderView(  tenderView );
				// if coming from service contract
				// than save tender id against that tender
				if(viewMap.containsKey(WebConstants.Tender.REQUEST_ID))
				{
					requestView =  loadRequest((Long)viewMap.get(WebConstants.Tender.REQUEST_ID));
					requestView.setTenderView(tenderView);
					requestServiceAgent.addRequest(requestView);

				}

				if(tenderView.getTenderId()!=null)
				{
					saveAttachmentComments(sysNote ,tenderView.getTenderId());
				}
			if(!isUpdate)
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MSG_SAVED_SUCCESSFULLY));
			else
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MSG_UPDATED_SUCCESSFULLY));

			modeEdit();
			}
		}
		catch ( Exception e)
		{
			logger.LogException("saveAction|Error Occured..",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	public String  cancel(){

		return "SearchServiceTender";

	}

	private boolean validateValues(){
		boolean flag = true;
		errorMessages.clear();

		if(tenderType.getValue()==null||tenderType.getValue().toString().equals("-1")){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_TENDER_TYPE_REQUIRED));

		}
		if(expectedCost.getValue()==null||expectedCost.getValue().toString().trim().length()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_EXPECTED_COST_REQUIRED));

		}
		if(getStartDate()==null||getStartDate().toString().trim().length()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_EXPECTED_START_DATE_REQUIRED));

		}
		if(getEndDate()==null||getEndDate().toString().trim().length()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_EXPECTED_END_DATE_REQUIRED));

		}
		if(getEndDate()!=null&&getStartDate()!=null){
			 if(getStartDate().after(getEndDate()) || getEndDate().before(getStartDate())){
				 flag = false;

				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_INVALID_DATE));
			 }
		}
		if(tenderPrice.getValue()==null||tenderPrice.getValue().toString().trim().length()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_TENDER_PRICE_REQUIRED));

		}
		try{

			if(tenderPrice.getValue()!=null&&tenderPrice.getValue().toString().trim().length()>0)
				Double.parseDouble(tenderPrice.getValue().toString());

		}
		catch (NumberFormatException e) {
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_PROPERTY_INVALID_TENDER_PRICE));

		}
		try{

			if(expectedCost.getValue()!=null&&expectedCost.getValue().toString().trim().length()>0)
				Double.parseDouble(expectedCost.getValue().toString());
		}
		catch (NumberFormatException e) {
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_PROPERTY_INVALID_EXPECTED_COST));

		}

		return flag;
	}
	@SuppressWarnings( "unchecked" )
	private TenderView fillTenderValues(){
	TenderView tenderView = new TenderView();

	DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS),
			                                      Constant.TENDER_STATUS_NEW);
	if(ddv!=null){
		tenderView.setStatusId(ddv.getDomainDataId());
		tenderView.setStatusAr(ddv.getDataDescAr());
		tenderView.setStatusEn(ddv.getDataDescEn());
	}

	TenderTypeView tenderTypeView = new TenderTypeView();
	tenderTypeView.setTenderTypeId(Long.valueOf(tenderType.getValue().toString()));
	tenderView.setTenderTypeView(tenderTypeView);

	if(hdnTenderId.getValue()!=null&&
			hdnTenderId.getValue().toString().trim().length()>0)
		tenderView.setTenderId(Long.valueOf(hdnTenderId.getValue().toString()));
	tenderView.setRemarks(tenderDescription.getValue().toString());
	tenderView.setIssueDate(getStartDate());
	tenderView.setEndDate(getEndDate());

	tenderView.setExpectedCost(Double.valueOf(expectedCost.getValue().toString()));
	tenderView.setTenderPrice(Double.valueOf(tenderPrice.getValue().toString()));
	tenderView.setCreatedBy(getLoggedInUser());
	tenderView.setUpdatedBy(getLoggedInUser());

	if( viewMap.containsKey(WebConstants.Tender.SCOPE_OF_WORK_LIST)&&
			viewMap.get( WebConstants.Tender.SCOPE_OF_WORK_LIST ) != null )
		fillScopeOfWorkList( tenderView, (List<TenderWorkScopeView>) viewMap.get( WebConstants.Tender.SCOPE_OF_WORK_LIST ) );
	if( viewMap.containsKey(WebConstants.Tender.CONTRACTORS_LIST)&&
			viewMap.get( WebConstants.Tender.CONTRACTORS_LIST ) != null )
		fillContractorList( tenderView, (List<ContractorView>) viewMap.get( WebConstants.Tender.CONTRACTORS_LIST ) );

	if( viewMap.containsKey(WebConstants.Contract.CONTRACT_UNIT_LIST)&&
			viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST)!= null ){
		List<PropertyView> propertyViewList = (List<PropertyView>) viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);
		List<TenderPropertyView> tenderPropertyViewList = new ArrayList<TenderPropertyView>();
		for(PropertyView propertyView :propertyViewList)
		{
			TenderPropertyView tenderPropertyView = new TenderPropertyView();
			//in case of update
			if(propertyView.getTenderPropertyId()!=null)
			{
				tenderPropertyView.setTenderPropertyId(propertyView.getTenderPropertyId());
				tenderPropertyView.setIsDeleted(propertyView.getTenderPropertyIsDeleted());
				tenderPropertyView.setPropertyId(propertyView.getPropertyId());
				tenderPropertyView.setUnitId(propertyView.getUnitId());
				tenderPropertyView.setCreatedBy(getLoggedInUser());
				tenderPropertyView.setUpdatedBy(getLoggedInUser());

			}else{ //in case of Add new
				tenderPropertyView.setPropertyId(propertyView.getPropertyId());
				tenderPropertyView.setUnitId(propertyView.getUnitId());
				tenderPropertyView.setCreatedBy(getLoggedInUser());
				tenderPropertyView.setUpdatedBy(getLoggedInUser());
			   }
			tenderPropertyViewList.add(tenderPropertyView);
		}
		fillPropertyList(tenderView, tenderPropertyViewList);
	}
		return tenderView;
	}

	private RequestView loadRequest (Long requestId)
	{

	 try {

			requestView.setRequestId(requestId);
			List<RequestView> requestViewList = propertyServiceAgent.getAllRequests(requestView, null, null, null);
			requestView = requestViewList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return requestView;
	}

	private void fillScopeOfWorkList( TenderView  tenderView, List<TenderWorkScopeView> tenderWorkScopeViewList )
	{
		tenderView.setTenderWorkScopeViewList(tenderWorkScopeViewList);
	}

	private void fillPropertyList( TenderView  tenderView, List<TenderPropertyView> tenderPropertyViewList )
	{
		tenderView.setPropertyViewList(tenderPropertyViewList);
	}

	private void fillContractorList( TenderView  tenderView, List<ContractorView> contractorsViewList )
	{
		tenderView.setContractorsViewList(contractorsViewList);


	}
	public void submitAction(){
		//write initiate task body in this method

		TenderView tenderView =null;
		try {


		    if(validateValuesForSend())
		    {
				if(getTenderView() !=null)
				{
					tenderView= getTenderView();

				}

				PIMSMaintenanceTenderingBPELPortClient portClient = new PIMSMaintenanceTenderingBPELPortClient();
				PIMSSecurityTenderingBPELPortClient secPortClient = new PIMSSecurityTenderingBPELPortClient();
				PIMSInsuranceTenderingBPELPortClient insPortClient = new PIMSInsuranceTenderingBPELPortClient();



				String endPoint = parameters.getParameter(WebConstants.Tender.TENDER_BPEL_END_POINT);
				String secEndPoint = parameters.getParameter(WebConstants.Tender.TENDER_SECURITY_BPEL_END_POINT);
				String insEndPoint = parameters.getParameter(WebConstants.Tender.TENDER_INSURANCE_BPEL_END_POINT);
				portClient.setEndpoint(endPoint);
				secPortClient.setEndpoint(secEndPoint);
				insPortClient.setEndpoint(insEndPoint);
				List<DomainDataView> ddvList;
				DomainDataView ddvIns;
				DomainDataView ddvMain;
				DomainDataView ddvSec;

				ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.Tender.TENDER_TYPE);
				ddvIns=CommonUtil.getIdFromType(ddvList, WebConstants.Tender.TENDER_TYPE_INSURANCE);
				ddvMain=CommonUtil.getIdFromType(ddvList, WebConstants.Tender.TENDER_TYPE_MAINTENANCE);
				ddvSec=CommonUtil.getIdFromType(ddvList, WebConstants.Tender.TENDER_TYPE_SECURITY);

				if(tenderType.getValue().toString().equals(ddvIns.getDomainDataId().toString()))
					insPortClient.initiate(Long.valueOf(tenderType.getValue().toString()),tenderView.getTenderId(),getLoggedInUser(), null, null);
				else if(tenderType.getValue().toString().equals(ddvMain.getDomainDataId().toString()))
					portClient.initiate(Long.valueOf(tenderType.getValue().toString()),tenderView.getTenderId(),getLoggedInUser(), null, null);
				else if(tenderType.getValue().toString().equals(ddvSec.getDomainDataId().toString()))
					secPortClient.initiate(Long.valueOf(tenderType.getValue().toString()),tenderView.getTenderId(),getLoggedInUser(), null, null);

				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MSG_SEND_FOR_EVALUATION_SUCCESSFULLY));

				DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.TENDER_STATUS_PROCESSING);
				constructionServiceAgent.changeTenderStatus(tenderView.getTenderId(), ddv.getDomainDataId());
				if(tenderView.getTenderId()!=null)
				{
						saveAttachmentComments("tender.event.submitted" ,tenderView.getTenderId());
				}
				if(getIsEnglishLocale())
				{
				tenderStatue.setValue(ddv.getDataDescEn());
				}
				else
				{
				tenderStatue.setValue(ddv.getDataDescAr());
				}

			    modeView();
		      }
		    } catch (Exception e)
		    {
		    	logger.LogException("submitAction|Error Occured..",e);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		     }

	}

	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public boolean getViewModeApproval() {
		if(viewMap.containsKey("PAGE_MODE")&&viewMap.get("PAGE_MODE").toString().equals("PAGE_MODE_APPROVAL"))
			return true;
		else
			return false;
	}



	public void setViewModeApproval(boolean isViewModeApproval) {
		this.isViewModeApproval = isViewModeApproval;
	}
	@SuppressWarnings( "unchecked" )
	private void populateTabs(Long tenderId) throws PimsBusinessException ,Exception
	{
		CommonUtil.loadAttachmentsAndComments(tenderId.toString());
		TenderView tenderView = constructionServiceAgent.getTenderByIdForTender(tenderId);
		setTenderView( tenderView );
		tenderNumber.setValue(tenderView.getTenderNumber());

		if ( tenderId != null )
		{
            HashMap searchMap = new HashMap ();
            searchMap.put("tenderId", tenderId);

            tenderInquiryList = constructionServiceAgent.getAllTenderInquiries(searchMap);
            setTenderInquiryList(tenderInquiryList);
		}

	 if ( tenderId != null )
		{
			extendedDataList = requestServiceAgent.getExtendApplicationRequestsForTender(tenderId,WebConstants.REQUEST_TYPE_TENDER_EXTEND_APPLICATION, getArgMap());
			setExtendedDataList(extendedDataList);
		}

		if(getIsArabicLocale())
			tenderStatue.setValue(tenderView.getStatusAr());
		else
			tenderStatue.setValue(tenderView.getStatusEn());

		startDate = tenderView.getIssueDate();
		viewMap.put("START_DATE", startDate);
		hdnTenderId.setValue(String.valueOf(tenderId));


		endDate = tenderView.getEndDate();
		viewMap.put("END_DATE", endDate);
		expectedCost.setValue(tenderView.getExpectedCost());
		tenderPrice.setValue(tenderView.getTenderPrice());
		tenderDescription.setValue(tenderView.getRemarks());
		tenderType.setValue(tenderView.getTenderTypeView().getTenderTypeId().toString());

		PropertyView propertyView;
		if(propertyDataList==null)
			propertyDataList = new ArrayList<PropertyView>();
		for(TenderPropertyView tenderPropertyView : tenderView.getPropertyViewList()){
			propertyView = new PropertyView();
			propertyView.setTenderPropertyId(tenderPropertyView.getTenderPropertyId());
			propertyView.setTenderPropertyIsDeleted(tenderPropertyView.getIsDeleted());
			propertyView.setPropertyNumber(tenderPropertyView.getPropertyNumber());
			propertyView.setPropertyId(tenderPropertyView.getPropertyId());
			propertyView.setCommercialName(tenderPropertyView.getCommercialName());
			propertyView.setPropertyTypeAr(tenderPropertyView.getPropertyTypeAr());
			propertyView.setPropertyTypeEn(tenderPropertyView.getPropertyTypeEn());
			propertyView.setTypeId(tenderPropertyView.getTypeId());
			propertyView.setUnitRefNo(tenderPropertyView.getUnitRefNo());
			propertyView.setUnitId(tenderPropertyView.getUnitId());
			propertyView.setEmirateNameAr(tenderPropertyView.getEmirateNameAr());
			propertyView.setEmirateNameEn(tenderPropertyView.getEmirateNameEn());


			propertyDataList.add(propertyView);

		}
		viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,propertyDataList);
		viewMap.put(WebConstants.Tender.CONTRACTORS_LIST,tenderView.getContractorsViewList());
		viewMap.put(WebConstants.Tender.SCOPE_OF_WORK_LIST,tenderView.getTenderWorkScopeViewList());

		DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.WINNER_SELECTED);
		if(tenderView.getStatusId().equals(ddv.getDomainDataId()))
			this.undoWinnerMarked.setRendered(true);
		else
			this.undoWinnerMarked.setRendered(false);
	}
	@SuppressWarnings( "unchecked"  )
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	private Boolean ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
	{
		Boolean success = false;
		String methodName="ApproveRejectTask";
		logger.logInfo(methodName+"|"+" Start...");
		try
		{
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;

			UserTask userTask = (UserTask) viewMap.get(WebConstants.USER_TASK_TENDER_MANAGEMENT);
			String loggedInUser=CommonUtil.getLoggedInUser();
			logger.logInfo(methodName+"|"+" TaskOutCome..."+taskOutCome);
			logger.logInfo(methodName+"|"+" loggedInUser..."+loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
			success = true;
		}
		catch(PIMSWorkListException ex)
		{
			logger.LogException(methodName+" crashed...",ex);
			throw ex;
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+" crashed...",ex);
			throw ex;
		}

		return success;
	}
	private void setRequestView(RequestView requestView) {

		List<DomainDataView> ddvList;
		DomainDataView ddv;
		Long tenantId = 0L;

		requestView.setContractId(null);
		requestView.setTenantsView(null);
		requestView.setContractView(null);
		requestView.setApplicantView(null);
		requestView.setCreatedBy(commonUtil.getLoggedInUser());
		requestView.setCreatedOn(new Date());
		requestView.setUpdatedBy(commonUtil.getLoggedInUser());
		requestView.setUpdatedOn(new Date());
		requestView.setRecordStatus(new Long(1));
		requestView.setIsDeleted(new Long(0));

		requestView.setRequestDate(new Date());


		ddvList=commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		ddv=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
		requestView.setStatusId(ddv.getDomainDataId());
/*
		ddvList=commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_ORIGINATION);
		ddv=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_ORIGINATION_CSC);
		requestView.setRequestOrigination(ddv.getDomainDataId());

		ddvList=commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_PRIORITY);
		ddv=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_PRIORITY_NORMAL);
		requestView.setRequestPriorityId(ddv.getDomainDataId());
*/
	}
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		String method="getIsEnglishLocale";

		logger.logInfo(method+"|"+"Start...");
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		logger.logInfo(method+"|"+"Finish...");
		return isEnglishLocale;
	}

	public HtmlInputHidden getHdnTenderId() {
		return hdnTenderId;
	}

	public void setHdnTenderId(HtmlInputHidden hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	public boolean isCanDelete() {
		return canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	public HtmlInputText getServiceTenderType() {
		return serviceTenderType;
	}

	public void setServiceTenderType(HtmlInputText serviceTenderType) {
		this.serviceTenderType = serviceTenderType;
	}

	public Boolean getCanDeleteTenderProperty()
	{
        if (viewMap.get("CAN_DELETE_TENDER_PROPERTY")!=null)
        	return false;
        else
        	return true;

	}

	private boolean validateValuesForSend(){
		boolean flag = true;
		errorMessages.clear();
		if(getPropertyDataList()==null||getPropertyDataList().size()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_UNIT_REQUIRED));
		}

		if( !viewMap.containsKey(WebConstants.Tender.SCOPE_OF_WORK_LIST)||
				viewMap.get( WebConstants.Tender.SCOPE_OF_WORK_LIST ) == null ||
				((List<TenderWorkScopeView>) viewMap.get( WebConstants.Tender.SCOPE_OF_WORK_LIST )).size()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_SCOPE_OF_WORK_LIST));
				}
		if( !viewMap.containsKey(WebConstants.Tender.CONTRACTORS_LIST)||
				viewMap.get( WebConstants.Tender.CONTRACTORS_LIST ) == null ||
				((List<TenderContractorView>) viewMap.get( WebConstants.Tender.CONTRACTORS_LIST )).size()==0){
			flag = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_CONTRACTORS_LIST));
				}


		return flag;
	}
	@SuppressWarnings("unchecked")
    public TenderView getTenderView()
    {
    	if( viewMap.get(WebConstants.Tender.TENDER_VIEW) != null )
    	{
    	return (TenderView)viewMap.get( WebConstants.Tender.TENDER_VIEW);
    	}
    	else
    		return null;
    }
	public void requestHistoryTabClick()
	{
		TenderView view = getTenderView();
		if( view != null && view.getTenderId() != null )
		{
			try
			{
			  RequestHistoryController rhc=new RequestHistoryController();
			  rhc.getAllRequestTasksForRequest(noteOwner,view.getTenderId().toString());
			}
			catch(Exception ex)
			{
				logger.LogException( "requestHistoryTabClick|Error Occured..",ex);
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}
	@SuppressWarnings("unchecked")
    public void setTenderView(TenderView arg)
    {
    	if( arg != null )
    	{
    	  viewMap.put( WebConstants.Tender.TENDER_VIEW, arg);
    	}
    }
	public HtmlTab getInquiryAppTab() {
		return inquiryAppTab;
	}

	public void setInquiryAppTab(HtmlTab inquiryAppTab) {
		this.inquiryAppTab = inquiryAppTab;
	}

	public List<TenderInquiryView> getTenderInquiryList() {
		if(viewMap.get("TENDER_INQUIRY")!=null)
		tenderInquiryList = (List<TenderInquiryView>)viewMap.get("TENDER_INQUIRY");
		return tenderInquiryList;
	}

	public void setTenderInquiryList(List<TenderInquiryView> tenderInquiryList) {
		this.tenderInquiryList = tenderInquiryList;
		if(this.tenderInquiryList!=null)
			viewMap.put("TENDER_INQUIRY", this.tenderInquiryList);
	}

	public HtmlTab getExtendsAppTab() {
		return extendsAppTab;
	}

	public void setExtendsAppTab(HtmlTab extendsAppTab) {
		this.extendsAppTab = extendsAppTab;
	}

	public Boolean getViewModePopUp()
	{
		if(viewMap.get(WebConstants.Tender.TENDER_MODE_POPUP)!=null)
			return true;
		else
			return false;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}
	public HtmlCommandButton getUndoWinnerMarked() {
		return undoWinnerMarked;
	}
	public void setUndoWinnerMarked(HtmlCommandButton undoWinnerMarked) {
		this.undoWinnerMarked = undoWinnerMarked;
	}

	public void undoWinnerMarkedAction(){
		String METHOD_NAME = "undoWinnerMarkedAction()";
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{

			TenderView tenderDetailsView = (TenderView) getTenderView();

			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.TENDER_STATUS_APPROVED);
			csa.setContractAsWinner( Long.valueOf(tenderDetailsView.getTenderId()),
					                 null, ddv.getDomainDataId(),getLoggedInUser()
					                );
			this.unmarkAllProposalsFirst(tenderDetailsView.getTenderId());
			successMessages.add(ResourceUtil.getInstance().getProperty("tenderProposal.DeselectWinnersMessage") );

			populateTabs(Long.valueOf(tenderDetailsView.getTenderId()));
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");

		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private Boolean unmarkAllProposalsFirst(Long tenderId){
		try
		{
			ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
			List<TenderProposalView> proposalViews = constructionServiceAgent.getTenderProposalList(tenderId);
			for(TenderProposalView proposalView : proposalViews){
				this.changeProposalStatusByProposal(proposalView, Constant.TENDER_PROPOSAL_STATUS_OPENED);
			}
			return true;
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposal() --- exception --- ", e);
			return false;
		}
	}
	private void changeProposalStatusByProposal(TenderProposalView selectedTenderProposalView, String status) throws Exception
	{
		DomainData domainData = new UtilityManager().getDomainDataByValue( status );
		selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
		selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
		selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
		ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
		selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
		constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
	}
}


