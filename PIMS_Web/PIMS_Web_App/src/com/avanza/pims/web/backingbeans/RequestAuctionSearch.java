package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;

public class RequestAuctionSearch extends AbstractController {
	private transient Logger logger = Logger
			.getLogger(RequestAuctionSearch.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

	/**
	 * 
	 */

	public RequestAuctionSearch() {
		// System.out.println("RequestAuctionSearch Constructor");

	}

	private String message;

	private AuctionView auctionView = new AuctionView();

	private AuctionView dataItem = new AuctionView();
	private ArrayList<AuctionView> dataList = new ArrayList<AuctionView>();

	private List<SelectItem> auctionStatuses = new ArrayList<SelectItem>();

	private List<String> errorMessages;
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private boolean sortAscending = true;

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	private Boolean isSelectable;
	
	private static final String REQUEST_AUCTION_SEARCH_LIST = "REQUEST_AUCTION_SEARCH_LIST";

	/*
	 * Methods
	 */

	/**
	 * @param isEnglishLocale
	 *            the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale
	 *            the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	/**
	 * Loads the list with the statuses of Auction which is binded with the
	 * Select Menu (Combo Box)
	 * 
	 * @throws PimsBusinessException
	 * @author Syed Hammad Ahmed
	 */
	private void loadAuctionStatuses() {
		logger.logInfo("loadAuctionStatuses() started...");

		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

		try {
			auctionStatuses = new ArrayList<SelectItem>();
			auctionStatuses.add(new SelectItem("0", WebConstants.Statuses.ALL));

			List<DomainDataView> domainDataList = propertyServiceAgent
					.getAuctionStatusList();
			String statusId = null;
			if (!domainDataList.isEmpty()) {
				for (DomainDataView temp : domainDataList) {
					statusId = temp.getDomainDataId() + "";
					String desc_English = temp.getDataDescEn();
					String desc_Arabic = temp.getDataDescAr();

					if (getIsEnglishLocale())
						auctionStatuses.add(new SelectItem(statusId,
								desc_English));
					else
						auctionStatuses.add(new SelectItem(statusId,
								desc_Arabic));

				}
			} else {
				errorMessages = new ArrayList<String>();
				errorMessages.add("Status not loaded properly");
			}
			logger.logInfo("loadAuctionStatuses() completed successfully!!!");
		} catch (PimsBusinessException exception) {
			logger.LogException("loadAuctionStatuses() crashed ", exception);
		} catch (Exception exception) {
			logger.LogException("loadAuctionStatuses() crashed ", exception);
		}
	}

	public List<AuctionView> getAuctionDataList() {
		 if (dataList == null || dataList.size() == 0) {
//		 loadAuctionList(); // Preload by lazy loading.
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object list = viewRootMap.get(REQUEST_AUCTION_SEARCH_LIST);
			if(list != null){
				dataList = (ArrayList<AuctionView>)list;
			}
		 }
		return dataList;
	}

	private void loadAuctionList() {
		logger.logInfo("loadAuctionList() started...");

		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Long auctionEditModeId; // set when auction is in "draft" mode i.e.
								// status =
								// WebConstants.Statuses.AUCTION_DRAFT_STATUS
		Long auctionViewModeId; // set when auction is in "ready for
								// advertisement" mode i.e. status =
								// WebConstants.Statuses.AUCTION_READY_FOR_ADVERTISEMENT_STATUS
		Long auctionVenueSelectModeId; // set when auction is in "venue date
										// select" mode i.e. status =
										// WebConstants.Statuses.AUCTION_TO_SELECT_DATE_VENUE_STATUS
		Long auctionConductModeId; // set when auction is in "ready for
									// request" mode i.e. status =
									// WebConstants.Statuses.AUCTION_READY_STATUS
		Long auctionCompleteModeId; // set when auction is in "complete" mode
									// i.e. status =
									// WebConstants.Statuses.AUCTION_COMPLETED_STATUS

		try {
			dataList = new ArrayList<AuctionView>();
			auctionEditModeId = propertyServiceAgent.getDomainDataByValue(
					WebConstants.Statuses.AUCTION_STATUS,
					WebConstants.Statuses.AUCTION_DRAFT_STATUS)
					.getDomainDataId();
			auctionViewModeId = propertyServiceAgent
					.getDomainDataByValue(
							WebConstants.Statuses.AUCTION_STATUS,
							WebConstants.Statuses.AUCTION_READY_FOR_ADVERTISEMENT_STATUS)
					.getDomainDataId();
			auctionVenueSelectModeId = propertyServiceAgent
					.getDomainDataByValue(
							WebConstants.Statuses.AUCTION_STATUS,
							WebConstants.Statuses.AUCTION_TO_SELECT_DATE_VENUE_STATUS)
					.getDomainDataId();
			auctionConductModeId = propertyServiceAgent.getDomainDataByValue(
					WebConstants.Statuses.AUCTION_STATUS,
					WebConstants.Statuses.AUCTION_READY_STATUS)
					.getDomainDataId();
			auctionCompleteModeId = propertyServiceAgent.getDomainDataByValue(
					WebConstants.Statuses.AUCTION_STATUS,
					WebConstants.Statuses.AUCTION_COMPLETED_STATUS)
					.getDomainDataId();
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			// auctionView.setAuctionStatusId("0");
			auctionView
					.setAuctionStatusId(getAuctionReadyStatusId().toString());
			List<AuctionView> auctionList = propertyServiceAgent
					.getAuctions(auctionView);
			// AuctionView temp;
			Long statusId = null;
			if (!auctionList.isEmpty()) {
				for (AuctionView temp : auctionList) {
					// temp = auctionList.get(i);
					if (temp.getAuctionDateVal() != null) {
						String dateStr = formatter.format(temp
								.getAuctionDateVal());
						temp.setAuctionDate(dateStr);
					}
					if (temp.getCreatedOn() != null) {
						String dateStr = formatter.format(temp.getCreatedOn());
						temp.setAuctionCreationDateString(dateStr);
					}
					Boolean set = false;
					statusId = Long.valueOf(temp.getAuctionStatusId());
					if (statusId.equals(auctionEditModeId)) {
						temp.setAuctionEditMode(true);
						set = true;
					} else
						temp.setAuctionEditMode(false);

					if ((statusId.equals(auctionViewModeId))
							|| (statusId.equals(auctionCompleteModeId))) {
						temp.setAuctionViewMode(true);
						set = true;
					} else
						temp.setAuctionViewMode(false);

					if (statusId.equals(auctionVenueSelectModeId)) {
						set = true;
						temp.setAuctionVenueSelectMode(true);
					} else
						temp.setAuctionVenueSelectMode(false);

					if (statusId.equals(auctionConductModeId)) {
						set = true;
						temp.setAuctionConductMode(true);
					} else
						temp.setAuctionConductMode(false);

					if (!set)
						temp.setAuctionViewMode(true);
					temp.setAuctionTitle(temp.getAuctionTitle().replace("\"","'"));
					System.out.println("temp.getAuctionTitle()"+temp.getAuctionTitle());
					dataList.add(temp);
				}
			} else {
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadAuctionList() completed successfully!!!");
		} catch (PimsBusinessException exception) {
			logger.LogException("loadAuctionList() crashed ", exception);
		} catch (Exception exception) {
			logger.LogException("loadAuctionList() crashed ", exception);
		}
	}

	private Long getAuctionReadyStatusId() {
		Long statusId = new Long(0);
		logger.logInfo("getAuctionReadyStatusId() started...");
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			DomainDataView domainDataView = pSAgent.getDomainDataByValue(
					WebConstants.AUCTION_STATUS,
					WebConstants.AUCTION_READY_STATUS);
			statusId = domainDataView.getDomainDataId();
			logger
					.logInfo("getAuctionReadyStatusId() completed successfully!!!");
		} catch (Exception exception) {
			logger
					.LogException("getAuctionReadyStatusId() crashed ",
							exception);
		}
		return statusId;
	}

	/*
	 * Validation Methods
	 */

	public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
//		if (auctionView.getAuctionNumber().lastIndexOf(" ")>=0) {
//		 errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.NUMBER) + " " + getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.SHOULD_NOT_HAVE_SPACE));
//		 validated = false;
//		}
		if (auctionView.getAuctionDateVal()==null){
			if( (!auctionView.getAuctionStartTime().equals("")) || (!auctionView.getAuctionEndTime().equals(""))) 
			{
			 errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.SHOULD_HAVE_AUCTION_DATE));
			 validated = false;
			}
		}

		return validated;
	}

	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return (messageList);
	}

	/*
	 * JSF Lifecycle methods
	 */

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		try {
			// if(!isPostBack())
			loadAuctionStatuses();
			logger.logInfo("init() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}

		// searchAuctions();
		// System.out.println("AuctionSearchBackingBean init");
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		// System.out.println("AuctionSearchBackingBean preprocess");
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		// System.out.println("AuctionSearchBackingBean prerender");
	}

	/*
	 * Button Click Action Handlers
	 */

	public String auctionDetailsEditMode() {
		logger.logInfo("auctionDetailsEditMode() started...");
		try {
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map requestMap = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestMap();
			requestMap
					.put(WebConstants.AUCTION_ID, aucView.getAuctionId() + "");
			requestMap.put(WebConstants.VIEW_MODE, WebConstants.EDIT_MODE);

			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsEditMode() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("auctionDetailsEditMode() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("auctionDetailsEditMode() crashed ", exception);
		}
		return "auctionDetailsEditMode";
	}

	public void auctionDetailsViewMode(javax.faces.event.ActionEvent event){
		logger.logInfo("auctionDetailsViewMode() started...");
		try {
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
			requestMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			requestMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			sessionMap.put("isAuctionSelected", true);
			FacesContext facesContext = FacesContext.getCurrentInstance();
            String javaScriptText = "javascript:closeWindowSubmit();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("auctionDetailsViewMode() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
//		return "auctionDetailsViewMode";
	}

	public String auctionDetailsConductMode() {
		logger.logInfo("auctionDetailsConductMode() started...");
		try {
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map requestMap = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestMap();
			requestMap
					.put(WebConstants.AUCTION_ID, aucView.getAuctionId() + "");
			requestMap.put(WebConstants.CONDUCT_AUCTION_MODE,
					WebConstants.CONDUCT_AUCTION_MODE);

			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsConductMode() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("auctionDetailsViewMode() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("auctionDetailsConductMode() crashed ",
					exception);
		}
		return "auctionDetailsConductMode";
	}

	public String auctionSelectDateVenueMode() {
		logger.logInfo("auctionSelectDateVenueMode() started...");
		try {
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map requestMap = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestMap();
			requestMap
					.put(WebConstants.AUCTION_ID, aucView.getAuctionId() + "");
			requestMap.put(WebConstants.SELECT_VENUE_MODE,
					WebConstants.SELECT_VENUE_MODE);
			requestMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);

			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionSelectDateVenueMode() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("auctionSelectDateVenueMode() crashed
		// ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("auctionSelectDateVenueMode() crashed ",
					exception);
		}
		return "auctionSelectDateVenueMode";
	}

	public String searchAuctions() {
		logger.logInfo("searchAuctions() started...");
		try {
			if (validateFields()){
				loadAuctionList();
				Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				viewRootMap.put(REQUEST_AUCTION_SEARCH_LIST, dataList);
			}
			logger.logInfo("searchAuctions() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("searchAuctions() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}
		return "searchAuctions";
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		try {

			logger.logInfo("cancel() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("cancel() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return "cancel";
	}

	/*
	 * Setters / Getters
	 */

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
		// AuctionVenueView temp = new AuctionVenueView();
		// temp.setVenueName(auctionView.getVenueName());
		// auctionView.setAuctionVenue(temp);
		return auctionView;
	}

	/**
	 * @param auctionView
	 *            the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
		// AuctionVenueView temp = new AuctionVenueView();
		// temp.setVenueName(auctionView.getVenueName());
		// auctionView.setAuctionVenue(temp);
		this.auctionView = auctionView;
	}

	/**
	 * @return the dataItem
	 */
	public AuctionView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem
	 *            the dataItem to set
	 */
	public void setDataItem(AuctionView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public ArrayList<AuctionView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList
	 *            the dataList to set
	 */
	public void setDataList(ArrayList<AuctionView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable
	 *            the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount
	 *            the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}


	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending
	 *            the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the auctionStatuses
	 */
	public List<SelectItem> getAuctionStatuses() {
		return auctionStatuses;
	}

	/**
	 * @param auctionStatuses
	 *            the auctionStatuses to set
	 */
	public void setAuctionStatuses(List<SelectItem> auctionStatuses) {
		this.auctionStatuses = auctionStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent
	 *            the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(
			PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	public Integer getRecordSize(){
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object list = viewRootMap.get(REQUEST_AUCTION_SEARCH_LIST);
		if(list != null){
			return ((ArrayList<AuctionView>)list).size(); 
		}
		else{
			return 0;
		}
	}

	public Boolean getIsSelectable() {
		Object obj = dataTable.getRowData();
		AuctionView auctionView = (AuctionView) obj;
		if(auctionView != null && auctionView.getAuctionDateVal() != null){
			if(auctionView.getAuctionDateVal().compareTo(new Date()) >= 0)
				isSelectable = true;
			else
				isSelectable = false;
		}
//		return isSelectable;
		return true;
	}

	public void setIsSelectable(Boolean isSelectable) {
		this.isSelectable = isSelectable;
	}
}
