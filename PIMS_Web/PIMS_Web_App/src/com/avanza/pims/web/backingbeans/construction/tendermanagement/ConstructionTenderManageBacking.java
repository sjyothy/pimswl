package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlCommandButton;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.column.HtmlSimpleColumn;
import org.apache.myfaces.custom.div.Div;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ConstructionTenderServiceAgent;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.proxy.pimsconstructiontenderingbpelproxy.PIMSConstructionTenderingBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.construction.plugins.Contractors;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ConstructionTenderDetailsView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExtendApplicationFilterView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.TenderContractorView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderProposalView;
import com.avanza.pims.ws.vo.TenderSessionView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class ConstructionTenderManageBacking extends AbstractController {

	private static final long serialVersionUID = 7793998854649305357L;

	private Logger logger = Logger.getLogger(ConstructionTenderManageBacking.class);
	private CommonUtil commonUtil = new CommonUtil();
	private UtilityManager utilityManager = new UtilityManager();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	@SuppressWarnings( "unchecked" )
	private Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	@SuppressWarnings( "unchecked" )
	private Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private SystemParameters parameters = SystemParameters.getInstance();

	public Div divApproveReject = new Div();
	public HtmlCalendar calActionDate = new HtmlCalendar();
	public HtmlInputTextarea txtComments = new HtmlInputTextarea();
	public HtmlCommandButton btnApprove = new HtmlCommandButton();
	public HtmlCommandButton btnReject = new HtmlCommandButton();

	public HtmlTabPanel tabPanel = new HtmlTabPanel();

	public HtmlTab tenderDetailsTab = new HtmlTab();

	public HtmlTab contractorsTab = new HtmlTab();
	public HtmlCommandButton btnAddContractor = new HtmlCommandButton();

	public HtmlTab sessionDetailsTab = new HtmlTab();
	public HtmlCalendar calSessionDate = new HtmlCalendar();
	public HtmlSelectOneMenu ddnSessionTimeHH = new HtmlSelectOneMenu();
	public HtmlSelectOneMenu ddnSessionTimeMM = new HtmlSelectOneMenu();
	public HtmlInputText txtSessionLocation = new HtmlInputText();
	public HtmlInputText txtSessionRemarks = new HtmlInputText();

	public HtmlTab proposalsTab = new HtmlTab();
	public HtmlDataTable proposalDataTable = new HtmlDataTable();
	public HtmlSimpleColumn proposalActionCol = new HtmlSimpleColumn();

	public HtmlTab extendsAppTab = new HtmlTab();
	public HtmlTab inquiryAppTab = new HtmlTab();
	public HtmlSimpleColumn actionInquiryAppCol = new HtmlSimpleColumn();
	public HtmlSimpleColumn actionextendsAppCol = new HtmlSimpleColumn();
	public HtmlDataTable extendedDataTable = new HtmlDataTable();
	public HtmlDataTable inquiryDataTable = new HtmlDataTable();

	public HtmlCommandButton btnSave = new HtmlCommandButton();
	public HtmlCommandButton btnDeselect = new HtmlCommandButton();
	public HtmlCommandButton btnCancel = new HtmlCommandButton();
	public HtmlCommandButton btnSend = new HtmlCommandButton();

	private RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	private ConstructionServiceAgent constructionServiceAgent  = new ConstructionServiceAgent();
	private NotesServiceAgent notesServiceAgent = new NotesServiceAgent();
	private ConstructionTenderServiceAgent constructionTenderServiceAgent = new ConstructionTenderServiceAgent();
	private String pageMode = WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_NEW;
	private Long constructionTenderId = null;
	private NotesVO note = new NotesVO();
	private String hdnContractorId;
	private List<ContractorView> contractorsViewList = new ArrayList<ContractorView>();
	private TenderSessionView tenderSessionView = new TenderSessionView();
	private List<TenderProposalView> proposalDataList = new ArrayList<TenderProposalView>();
	private List<ExtendApplicationFilterView> extendedDataList = new ArrayList<ExtendApplicationFilterView>();
	private List<TenderInquiryView> tenderInquiryList = new ArrayList<TenderInquiryView>();
	private UserTask userTask = null;
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CONSTRUCTION_TENDERING;

	private String remarks;

	public ConstructionTenderManageBacking() {

	}

	public void init() {
		super.init();

		try	{
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			note.setCreatedOn(new Date());
			updateValuesFromMap();
		}
		catch (Exception exception) {
			logger.LogException( " init--- crashed --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
	   super.prerender();
       try
       {
	    if(hdnContractorId!=null && hdnContractorId.toString().trim().length()>0)
	    {
	    	ConstructionServiceAgent csa=new ConstructionServiceAgent();
			ContractorView contractorView = new ContractorView();
	    	try
	    	{
	    		contractorView =  csa.getContractorById(new Long(hdnContractorId), null);
			}
	    	catch (NumberFormatException e)
	    	{
	    		e.printStackTrace();
			}
	    	catch (PimsBusinessException e)
	    	{
	    		e.printStackTrace();
			}

	    	viewMap.put("CONTRACTOR_VIEW", contractorView );
	    	hdnContractorId="";
	    }
	    else if(sessionMap.containsKey(WebConstants.MANY_CONTRACTORS_SELECTED) &&
				sessionMap.get(WebConstants.MANY_CONTRACTORS_SELECTED) != null)
    	{
    			List<ContractorView> contractorsList = (ArrayList<ContractorView>)sessionMap.get(WebConstants.MANY_CONTRACTORS_SELECTED);
				sessionMap.remove(WebConstants.MANY_CONTRACTORS_SELECTED);
				viewMap.put(WebConstants.MANY_CONTRACTORS_SELECTED, contractorsList );


    	}

		if(viewMap.get("CONTRACTOR_VIEW")!=null || viewMap.get(WebConstants.MANY_CONTRACTORS_SELECTED)!=null)
		{
			 new Contractors();
			 if ( 
					 pageMode.equals(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW )
				)
			{
						 
				
			  saveTenderContractor();	 
			}
		}

		if(viewMap.containsKey("CONTRACTOR_ALREADY_EXIST")&&
				Boolean.valueOf(viewMap.get("CONTRACTOR_ALREADY_EXIST").toString())){
			infoMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.CONTRACTOR_ALREADY_EXIST));
			viewMap.remove("CONTRACTOR_ALREADY_EXIST");
		}

		if(viewMap.containsKey("MULTIPLE_CONTRACTOR_ALREADY_EXIST")&&
				Boolean.valueOf(viewMap.get("MULTIPLE_CONTRACTOR_ALREADY_EXIST").toString())){
			infoMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MULTIPLE_CONTRACTOR_ALREADY_EXIST));
			viewMap.remove("MULTIPLE_CONTRACTOR_ALREADY_EXIST");
		}

		} catch (Exception e)
		{
			logger.LogException( " init--- crashed --- ", e );
		}

	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMap() throws Exception {

		// User Task
		if ( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		}
		else if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.put(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_APPROVE_REJECT);
			sessionMap.put(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, Long.valueOf( userTask.getTaskAttributes().get("TENDER_ID") ) );
		}

		// Page Mode
		if ( viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
			getIsViewMode();
		}
		else if ( sessionMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
			sessionMap.remove( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
			getIsViewMode();
		}

		// Construction Tender Id
		if ( viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID ) != null )
		{
			constructionTenderId = (Long) viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
			CommonUtil.loadAttachmentsAndComments(constructionTenderId.toString());
		}
		else if ( sessionMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID ) != null )
		{
			constructionTenderId = (Long) sessionMap.remove( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
			sessionMap.remove( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
			CommonUtil.loadAttachmentsAndComments(constructionTenderId.toString());
		}

		// Contractors View List
		if ( viewMap.get(WebConstants.Tender.CONTRACTORS_LIST) != null )
		{
			contractorsViewList = (List<ContractorView>) viewMap.get(WebConstants.Tender.CONTRACTORS_LIST);
		}
		else if ( constructionTenderId != null )
		{
			TenderView tenderView = constructionServiceAgent.getTenderByIdForTender(constructionTenderId);
			if ( tenderView != null )
				contractorsViewList = tenderView.getContractorsViewList();
		}

		// Tender Session
		if( viewMap.get(WebConstants.Tender.TENDER_SESSION_VIEW) != null )
		{
			tenderSessionView = (TenderSessionView) viewMap.get(WebConstants.Tender.TENDER_SESSION_VIEW);
		}
		else if ( constructionTenderId != null )
		{
			tenderSessionView = constructionServiceAgent.getTenderSessionViewById(constructionTenderId);
		}

		// Tender Proposals
		if( viewMap.get(WebConstants.Tender.PROPOSAL_VIEW_LIST) != null && sessionMap.get("reloadOpener") == null )
		{
			proposalDataList = (List<TenderProposalView>) viewMap.get(WebConstants.Tender.PROPOSAL_VIEW_LIST);
		}
		else if ( constructionTenderId != null )
		{
			proposalDataList = constructionServiceAgent.getTenderProposalList(constructionTenderId);
			sessionMap.remove("reloadOpener");
		}

		// Extend Application
		if ( viewMap.get("requestList") != null )
		{
			extendedDataList = (List<ExtendApplicationFilterView>) viewMap.get("requestList");
		}
		else if ( constructionTenderId != null )
		{
			extendedDataList = requestServiceAgent.getExtendApplicationRequestsForTender(constructionTenderId,null, getArgMap());
		}

		// Inquiry Application
		if ( viewMap.get("InquiryList") != null )
		{
			tenderInquiryList = (List<TenderInquiryView>) viewMap.get("InquiryList");
		}
		else if ( constructionTenderId != null )
		{
            HashMap searchMap = new HashMap ();
            searchMap.put("tenderId", constructionTenderId);
            searchMap.put("FROM_CONSTRUCTION_TENDER",true);
            tenderInquiryList = constructionServiceAgent.getAllTenderInquiries(searchMap);
		}

		if(sessionMap.get(WebConstants.Tender.TENDER_MODE_POPUP)!=null)
		{
		viewMap.put(WebConstants.Tender.TENDER_MODE_POPUP, sessionMap.get(WebConstants.Tender.TENDER_MODE_POPUP));
		sessionMap.remove(WebConstants.Tender.TENDER_MODE_POPUP);
		}

		changePageMode(pageMode);
	}

	@SuppressWarnings("unchecked")
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}

	private void changePageMode( String pageMode ) throws Exception {
		this.pageMode = pageMode;
		updateValuesToMap();
		updatePageForMode(this.pageMode);
	}

	private void updateValuesToMap() {
		// Page Mode
		viewMap.put( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, pageMode );

		// Construction Tender Id
		if ( constructionTenderId != null )
			viewMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, constructionTenderId );

		// Contractor View List
		if ( contractorsViewList != null )
			viewMap.put(WebConstants.Tender.CONTRACTORS_LIST, contractorsViewList);

		// Tender Session
		if ( tenderSessionView != null )
			viewMap.put(WebConstants.Tender.TENDER_SESSION_VIEW, tenderSessionView);

		// Tender Proposal List
		if ( proposalDataList != null )
			viewMap.put(WebConstants.Tender.PROPOSAL_VIEW_LIST, proposalDataList);

		// Extended Application List
		if ( extendedDataList != null )
			viewMap.put("requestList", extendedDataList);

		// Inquiry Application List
		if ( tenderInquiryList != null )
			viewMap.put("InquiryList",tenderInquiryList);


		// User Task
		if ( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}

	public String onCancel() {
		String METHOD_NAME = "onCancel()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
			path = "constructionTenderSearch";
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
		return path;
	}

	private Boolean validate()
	{
		Boolean isValid = true;

		ConstructionTenderDetailsView constructionTenderDetailsView = (ConstructionTenderDetailsView) getTenderView();

		if ( constructionTenderDetailsView.getTenderTypeId() == null || constructionTenderDetailsView.getTenderTypeId().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( "tender.type" ) );
		}

		if ( constructionTenderDetailsView.getConstructionServiceTypeId() == null || constructionTenderDetailsView.getConstructionServiceTypeId().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( "constructionTender.search.construction.service.type" ) );
		}

		if ( constructionTenderDetailsView.getProjectNumber() == null || constructionTenderDetailsView.getProjectNumber().equalsIgnoreCase("") )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( "constructionTender.search.project.number" ) );
		}
		if (constructionTenderDetailsView.getTenderDocumentSellingPrice()!=null && constructionTenderDetailsView.getTenderDocumentSellingPrice() .compareTo(0D)==-1 )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage("constructionTender.search.docSellingPriceNOZ" ) );
		}

		if ( constructionTenderDetailsView.getTenderIssueDate() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( "constructionTender.search.tender.issueDate" ) );
		}

		if ( constructionTenderDetailsView.getTenderEndDate() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getFieldRequiredMessage( "constructionTender.search.tender.endDate" ) );
		}

		return isValid;
	}

	public String onDeselectWinners(){
		String METHOD_NAME = "onDeselectWinners()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{

			ConstructionTenderDetailsView constructionTenderDetailsView = (ConstructionTenderDetailsView) getTenderView();

			this.unmarkAllProposalsFirst(constructionTenderDetailsView.getTenderId());

			constructionTenderDetailsView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			constructionTenderDetailsView.setContractorId(null);
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.TENDER_STATUS_APPROVED);
			constructionTenderDetailsView.setTenderStatusId( String.valueOf( ddv.getDomainDataId() ) );
			constructionTenderServiceAgent.updateConstructionTenderDetailsView( constructionTenderDetailsView );

			infoMessages.add(ResourceUtil.getInstance().getProperty("tenderProposal.DeselectWinnersMessage") );
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");

		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return path;
	}
	public String onSave() {
		String METHOD_NAME = "onSave()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{
			if ( validate() )
			{
				logger.logInfo(METHOD_NAME + " #--- saveTenderDetails --- ");
				saveTenderDetails();
				logger.logInfo(METHOD_NAME + " #--- saveTenderContractor --- ");
				saveTenderContractor();
				logger.logInfo(METHOD_NAME + " #--- saveTenderSessionDetails --- ");
				saveTenderSessionDetails();

				changePageMode( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_EDIT );
				changeInnerPageMode(pageMode);


				infoMessages.add(ResourceUtil.getInstance().getProperty("constructionTender.btn.save") );

				logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			}
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return path;
	}

	public String onSend()
	{
		String path = null;
		try
		{
		 if(contractorsViewList !=null && contractorsViewList.size()>0)
		 {
			ConstructionTenderDetailsView constructionTenderDetailsView = getTenderView();
			if ( constructionTenderDetailsView  != null )
			{
				PIMSConstructionTenderingBPELPortClient portClient = new PIMSConstructionTenderingBPELPortClient();
				String endPoint = parameters.getParameter( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_BPEL_END_POINT );
				portClient.setEndpoint( endPoint );
				portClient.initiate( Long.valueOf( constructionTenderDetailsView.getTenderTypeId() ),
									 constructionTenderDetailsView.getTenderId(),
									 CommonUtil.getLoggedInUser(), null, null );
				infoMessages.add(ResourceUtil.getInstance().getProperty("constructionTender.btn.send" ) );
				// this code is for changing the status of tender after sending
		        DomainDataView ddv = CommonUtil.getIdFromType(
		        		                                       CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS),
		        		                                       Constant.TENDER_STATUS_PROCESSING
		        		                                      );
		        constructionTenderDetailsView.setTenderStatusId( String.valueOf( ddv.getDomainDataId() ) );
		        saveAttachmentComments("tender.event.submitted", constructionTenderDetailsView.getTenderId());
		        setTenderView(constructionTenderDetailsView);
		        constructionTenderServiceAgent.updateConstructionTenderDetailsView( constructionTenderDetailsView );
			}

				changePageMode( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW );
				changeInnerPageMode(pageMode);

		 }
		 else
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty("constructionTender.btn.send.contractorNotSelected" ) );
		 }
		}
		catch (Exception exception)
		{
			logger.LogException( "onSend --- crashed --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty("commons.ErrorMessage" ) );
		}
		return path;
	}

	private Boolean saveTenderDetails() throws Exception {

		Boolean success = false;
		String sysNote = "tender.event.updated";
		ConstructionTenderDetailsView constructionTenderDetailsView =  getTenderView();
		if ( constructionTenderDetailsView != null )
		{
			if ( constructionTenderDetailsView.getTenderId() == null )
			{
				sysNote = "tender.event.saved";
				constructionTenderDetailsView.setCreatedBy( CommonUtil.getLoggedInUser() );
				constructionTenderDetailsView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				success = constructionTenderServiceAgent.addConstructionTenderDetailsView( constructionTenderDetailsView );
				setTenderView(constructionTenderDetailsView);
				if (success)
				{
					constructionTenderId = constructionTenderDetailsView.getTenderId();

				}
			}
			else
			{
				constructionTenderDetailsView.setUpdatedBy( CommonUtil.getLoggedInUser() );

				success = constructionTenderServiceAgent.updateConstructionTenderDetailsView( constructionTenderDetailsView );
			}
			saveAttachmentComments(sysNote,constructionTenderId );
		}



		return success;
	}

	@SuppressWarnings( "unchecked" )
	private void setTenderView(ConstructionTenderDetailsView constructionTenderDetailsView)
	{
		if( constructionTenderDetailsView != null)
		{
		 viewMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW,constructionTenderDetailsView );
		}
	}
	@SuppressWarnings( "unchecked" )
	public ConstructionTenderDetailsView getTenderView()
	{
		if(viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW ) != null)
		{
		 return (ConstructionTenderDetailsView)viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW );
		}
		return null;
	}

	private Boolean saveTenderContractor() throws Exception
	{
		Boolean success = false;

		if ( contractorsViewList != null )
		{
			for ( ContractorView contractorView : contractorsViewList )
			{
				if ( contractorView.getTenderId() == null )
				{
					TenderContractorView tenderContractorView = new TenderContractorView();
					tenderContractorView.setTenderId( constructionTenderId );
					tenderContractorView.setContractorId( contractorView.getPersonId() );
					tenderContractorView.setCreatedBy( CommonUtil.getLoggedInUser() );
					tenderContractorView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					tenderContractorView.setIsInvited( 1L );
					tenderContractorView.setIsSelected("0");
					success = constructionTenderServiceAgent.addPurchaseTenderRequest(tenderContractorView);
					if (success)
					{
						contractorView.setTenderId( tenderContractorView.getTenderId() );
					}
				}
				else if(contractorView.getTenderContractorIsDeleted().compareTo(1L)==0)
				{
					// Tender Contractor Update Code Goes Here Which Will Be Used In Tender Contractor Deletion Process
					TenderContractorView tenderContractorView = new TenderContractorView();
					tenderContractorView.setTenderId( constructionTenderId );
					tenderContractorView.setContractorId( contractorView.getPersonId() );
					tenderContractorView.setCreatedBy( CommonUtil.getLoggedInUser() );
					tenderContractorView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					tenderContractorView.setIsInvited( 1L );
					tenderContractorView.setIsSelected("0");
					tenderContractorView.setIsDeleted(1L);
					tenderContractorView.setTenderContractorId(contractorView.getTenderContractorId());
					success = constructionTenderServiceAgent.updatePurchaseTenderRequest(tenderContractorView);
					if (success)
					{
						contractorView.setTenderId( tenderContractorView.getTenderId() );
					}
				}
			}
		}

		return success;
	}

	private Boolean saveTenderSessionDetails() throws Exception
	{
		Boolean success = false;

		if ( tenderSessionView != null )
		{
			if ( tenderSessionView.getTenderSessionId() != null )
			{
				tenderSessionView.setTenderSessionUpdatedOn( new Date() );
				tenderSessionView.setTenderSessionUpdatedBy( CommonUtil.getLoggedInUser() );
				success = constructionServiceAgent.updateTenderSession( tenderSessionView );
			}
			else
			{
				tenderSessionView.setTenderId( constructionTenderId );
				tenderSessionView.setTenderSessionCreatedOn( new Date() );
				tenderSessionView.setTenderSessionCreatedBy( CommonUtil.getLoggedInUser() );
				tenderSessionView.setTenderSessionUpdatedOn( new Date() );
				tenderSessionView.setTenderSessionUpdatedBy( CommonUtil.getLoggedInUser() );
				tenderSessionView.setTenderSessionIsDeleted( Constant.DEFAULT_IS_DELETED );
				tenderSessionView.setTenderSessionRecordStatus( Constant.DEFAULT_RECORD_STATUS );
				success = constructionServiceAgent.addTenderSession( tenderSessionView );
			}
		}

		return success;
	}

	private void changeInnerPageMode( String modeId ) throws Exception {
		ConstructionTenderDetailsBacking constructionTenderDetailsBacking = (ConstructionTenderDetailsBacking) getBean("pages$constructionTenderDetails");
		constructionTenderDetailsBacking.changePageMode(pageMode);
	}

	public String onApprove() {
		String METHOD_NAME = "onApprove()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{

		if(txtComments !=null && txtComments.getValue()!=null && txtComments.getValue().toString().length()>0)
		{
			if ( userTask != null )
			{
				String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				ApproveRejectTask(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
				changePageMode( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW );
				changeInnerPageMode(pageMode);
				infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_SUCCESS_APPROVED));
			}

			ConstructionTenderDetailsView constructionTenderDetailsView  = getTenderView();
			if ( constructionTenderDetailsView  != null )
			{


				if ( constructionTenderDetailsView.getTenderId() != null )
				{
					saveAttachmentComments("tender.event.approved", constructionTenderDetailsView.getTenderId() );
					CommonUtil.saveRemarksAsComments(constructionTenderDetailsView.getTenderId(), txtComments.getValue().toString(), noteOwner);

				}
			}
		}else
		{
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
		}

			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return path;
	}

	public String onReject() {
		String METHOD_NAME = "onReject()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try
		{

		if(txtComments !=null && txtComments.getValue()!=null && txtComments.getValue().toString().length()>0)
		{
			if ( userTask != null )
			{
				String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				ApproveRejectTask(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
				changePageMode( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW );
				changeInnerPageMode(pageMode);
				infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tender.MSG_SUCCESS_REJECTED));
			}
			ConstructionTenderDetailsView constructionTenderDetailsView = getTenderView();
			if ( constructionTenderDetailsView != null )
			{


				if ( constructionTenderDetailsView.getTenderId() != null )
				{
					saveAttachmentComments("tender.event.rejected", constructionTenderDetailsView.getTenderId());
					CommonUtil.saveRemarksAsComments(constructionTenderDetailsView.getTenderId(), txtComments.getValue().toString(), noteOwner);

				}
			}
		}else
		{
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
		}

			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
		return path;
	}

	private void ApproveRejectTask(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception
	{
		String METHOD_NAME = "ApproveRejectTask()";
		logger.logInfo(METHOD_NAME + " --- started --- ");
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
	}

	@SuppressWarnings( "unchecked" )
	private void saveAttachmentComments(String sysNote,long id, String... placeHolderVals) throws Exception
	{
		CommonUtil.saveAttachments(id);
		CommonUtil.saveComments(id, noteOwner);
		if(sysNote != null && sysNote.length()>0)
		{
		 CommonUtil.saveSystemComments(noteOwner, sysNote, id ,placeHolderVals);
		}

	}
	@SuppressWarnings( "unchecked" )
	public void requestHistoryTabClick()
	{
		ConstructionTenderDetailsView view =getTenderView();
		if( view != null && view.getTenderId() != null )
		{
			try
			{
			  RequestHistoryController rhc=new RequestHistoryController();
			  rhc.getAllRequestTasksForRequest(noteOwner,view.getTenderId().toString());
			}
			catch(Exception ex)
			{
				logger.LogException( "requestHistoryTabClick|Error Occured..",ex);
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}


	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public String editLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showEditTenderProposalPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	public String viewLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showViewTenderProposalPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	public String addLink_action() {
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		String javaScriptText = "javascript:showAddProposalRecommendationPopup();";
		sessionMap.put(WebConstants.Tender.PROPOSAL_VIEW, selectedTenderProposalView);
		openPopup(javaScriptText);
		return null;
	}

	public String cancelLink_action()
	{
		changeProposalStatus( Constant.TENDER_PROPOSAL_STATUS_CANCEL );
		updateTenderProposal();
		infoMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_CANCEL_SUCCESS ) );
		return null;
	}

	public String deleteLink_action()
	{
		changeProposalIsDeleted("1");
		updateTenderProposal();
		infoMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_DELETE_SUCCESS ) );
		return null;
	}

	public String rejectLink_action()
	{
		changeProposalStatus( Constant.TENDER_PROPOSAL_STATUS_REJECT );
		updateTenderProposal();
		infoMessages.add( getBundleMessage( WebConstants.Tender.TENDER_PROPOSAL_REJECT_SUCCESS ) );
		return null;
	}

	private void changeProposalIsDeleted(String isDeleted)
	{
		TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
		selectedTenderProposalView.setIsDeleted(isDeleted);
	}

	private void changeProposalStatus(String status)
	{
		logger.logInfo("changeProposalStatus --- started");
		try
		{
			DomainData domainData = utilityManager.getDomainDataByValue( status );
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
			selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
			selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
			logger.logInfo("changeProposalStatus --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("changeProposalStatus --- crashed --- ", e);
		}
	}

	private void updateTenderProposal()
	{
		try
		{
			logger.logInfo("updateTenderProposal() --- started");
			TenderProposalView selectedTenderProposalView = (TenderProposalView) proposalDataTable.getRowData();
			selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
			constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
			proposalDataList = constructionServiceAgent.getTenderProposalList(constructionTenderId);
			updateValuesToMap();
			logger.logInfo("updateTenderProposal() --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposal() --- exception --- ", e);
		}
	}

	private Boolean unmarkAllProposalsFirst(Long tenderId){
		try
		{
			List<TenderProposalView> proposalViews = constructionServiceAgent.getTenderProposalList(tenderId);
			for(TenderProposalView proposalView : proposalViews){
				this.changeProposalStatusByProposal(proposalView, Constant.TENDER_PROPOSAL_STATUS_OPENED);
				this.updateTenderProposalByProposal(proposalView);
			}
			return true;
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposal() --- exception --- ", e);
			return false;
		}
	}
	private void changeProposalStatusByProposal(TenderProposalView selectedTenderProposalView, String status)
	{
		logger.logInfo("changeProposalStatusByProposal --- started");
		try
		{
			DomainData domainData = utilityManager.getDomainDataByValue( status );
			selectedTenderProposalView.setStatusId( String.valueOf( domainData.getDomainDataId() ) );
			selectedTenderProposalView.setStatusEn( domainData.getDataDescEn() );
			selectedTenderProposalView.setStatusAr( domainData.getDataDescAr() );
			logger.logInfo("changeProposalStatusByProposal --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("changeProposalStatusByProposal --- crashed --- ", e);
		}
	}
	private void updateTenderProposalByProposal(TenderProposalView selectedTenderProposalView)
	{
		try
		{
			logger.logInfo("updateTenderProposalByProposal() --- started");
			selectedTenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedTenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
			constructionServiceAgent.updateTenderProposal(selectedTenderProposalView);
			proposalDataList = constructionServiceAgent.getTenderProposalList(constructionTenderId);
			updateValuesToMap();
			logger.logInfo("updateTenderProposalByProposal() --- ended successfully");
		}
		catch ( Exception e )
		{
			logger.LogException("updateTenderProposalByProposal() --- exception --- ", e);
		}
	}

	@SuppressWarnings( "unchecked" )
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_VIEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH_TENDER);
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "extendApplicationDetailsFromTender";
	}
	@SuppressWarnings( "unchecked" )
	public String onFollowUp(){
		String METHOD_NAME = "onFollowUp()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_FOLLOW_UP);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH_TENDER);

	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "extendApplicationDetailsFromTender";
	}

	@SuppressWarnings("unchecked")
	public String onManage(){
		logger.logInfo("onManage() started...");
		String taskType = "";
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();

	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);

				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH_TENDER);

				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		logger.logInfo("onManage() completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("onManage() crashed ", exception);
			errorMessages = new ArrayList<String>();
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "extendApplicationDetailsFromTender";
	}

	public String onPublish(){
		String METHOD_NAME = "onPublish()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_PUBLISH);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH_TENDER);
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "extendApplicationDetailsFromTender";
	}


	@SuppressWarnings("unchecked")
	public String onDelete()
	{
		String METHOD_NAME = "onDelete()";
		infoMessages = new ArrayList<String>();
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)extendedDataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		Boolean success = requestServiceAgent.deleteRequest(requestId, getLoggedInUser());
	   		if(success){
	   			infoMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_SUCCESS));
	   			extendedDataList = (List<ExtendApplicationFilterView>) viewMap.get("requestList");
	   	    	viewMap.put("requestList",extendedDataList);
	   	    	viewMap.put("recordSize",extendedDataList.size());
	   		}
	   		else
	   			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_FAILURE));

			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public void showHideAttachmentAndCommentsBtn()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_NEW ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_EDIT) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_APPROVE_REJECT ) )
			canAddAttachmentsAndComments(false);
		if ( pageMode.equalsIgnoreCase( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW ) )
			canAddAttachmentsAndComments(false);


	}

	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }

	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Integer getRecordSize() {
		return proposalDataList.size();
	}

	public Integer getExtendAppRecordSize() {
		return extendedDataList.size();
	}
	@SuppressWarnings( "unchecked" )
	public String getLocale() {
		return CommonUtil.getLocale();
	}

	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public Long getConstructionTenderId() {
		return constructionTenderId;
	}

	public void setConstructionTenderId(Long constructionTenderId) {
		this.constructionTenderId = constructionTenderId;
	}

	public HtmlCommandButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(HtmlCommandButton btnCancel) {
		this.btnCancel = btnCancel;
	}

	public HtmlTab getTenderDetailsTab() {
		return tenderDetailsTab;
	}

	public void setTenderDetailsTab(HtmlTab tenderDetailsTab) {
		this.tenderDetailsTab = tenderDetailsTab;
	}

	public ConstructionTenderServiceAgent getConstructionTenderServiceAgent() {
		return constructionTenderServiceAgent;
	}

	public void setConstructionTenderServiceAgent(
			ConstructionTenderServiceAgent constructionTenderServiceAgent) {
		this.constructionTenderServiceAgent = constructionTenderServiceAgent;
	}

	public Div getDivApproveReject() {
		return divApproveReject;
	}

	public void setDivApproveReject(Div divApproveReject) {
		this.divApproveReject = divApproveReject;
	}

	public NotesVO getNote() {
		return note;
	}

	public void setNote(NotesVO note) {
		this.note = note;
	}

	public HtmlCalendar getCalActionDate() {
		return calActionDate;
	}

	public void setCalActionDate(HtmlCalendar calActionDate) {
		this.calActionDate = calActionDate;
	}

	public HtmlInputTextarea getTxtComments() {
		return txtComments;
	}

	public void setTxtComments(HtmlInputTextarea txtComments) {
		this.txtComments = txtComments;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public NotesServiceAgent getNotesServiceAgent() {
		return notesServiceAgent;
	}

	public void setNotesServiceAgent(NotesServiceAgent notesServiceAgent) {
		this.notesServiceAgent = notesServiceAgent;
	}

	public HtmlTab getContractorsTab() {
		return contractorsTab;
	}

	public void setContractorsTab(HtmlTab contractorsTab) {
		this.contractorsTab = contractorsTab;
	}

	public HtmlCommandButton getBtnAddContractor() {
		return btnAddContractor;
	}

	public void setBtnAddContractor(HtmlCommandButton btnAddContractor) {
		this.btnAddContractor = btnAddContractor;
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public List<ContractorView> getContractorsViewList() {
		return contractorsViewList;
	}

	public void setContractorsViewList(List<ContractorView> contractorsViewList) {
		this.contractorsViewList = contractorsViewList;
	}

	public HtmlTab getSessionDetailsTab() {
		return sessionDetailsTab;
	}

	public void setSessionDetailsTab(HtmlTab sessionDetailsTab) {
		this.sessionDetailsTab = sessionDetailsTab;
	}

	public HtmlCalendar getCalSessionDate() {
		return calSessionDate;
	}

	public void setCalSessionDate(HtmlCalendar calSessionDate) {
		this.calSessionDate = calSessionDate;
	}

	public HtmlSelectOneMenu getDdnSessionTimeHH() {
		return ddnSessionTimeHH;
	}

	public void setDdnSessionTimeHH(HtmlSelectOneMenu ddnSessionTimeHH) {
		this.ddnSessionTimeHH = ddnSessionTimeHH;
	}

	public HtmlSelectOneMenu getDdnSessionTimeMM() {
		return ddnSessionTimeMM;
	}

	public void setDdnSessionTimeMM(HtmlSelectOneMenu ddnSessionTimeMM) {
		this.ddnSessionTimeMM = ddnSessionTimeMM;
	}

	public HtmlInputText getTxtSessionLocation() {
		return txtSessionLocation;
	}

	public void setTxtSessionLocation(HtmlInputText txtSessionLocation) {
		this.txtSessionLocation = txtSessionLocation;
	}

	public HtmlInputText getTxtSessionRemarks() {
		return txtSessionRemarks;
	}

	public void setTxtSessionRemarks(HtmlInputText txtSessionRemarks) {
		this.txtSessionRemarks = txtSessionRemarks;
	}

	public ConstructionServiceAgent getConstructionServiceAgent() {
		return constructionServiceAgent;
	}

	public void setConstructionServiceAgent(
			ConstructionServiceAgent constructionServiceAgent) {
		this.constructionServiceAgent = constructionServiceAgent;
	}

	public TenderSessionView getTenderSessionView() {
		return tenderSessionView;
	}

	public void setTenderSessionView(TenderSessionView tenderSessionView) {
		this.tenderSessionView = tenderSessionView;
	}

	public HtmlTab getProposalsTab() {
		return proposalsTab;
	}

	public void setProposalsTab(HtmlTab proposalsTab) {
		this.proposalsTab = proposalsTab;
	}

	public HtmlDataTable getProposalDataTable() {
		return proposalDataTable;
	}

	public void setProposalDataTable(HtmlDataTable proposalDataTable) {
		this.proposalDataTable = proposalDataTable;
	}

	public List<TenderProposalView> getProposalDataList() {
		return proposalDataList;
	}

	public void setProposalDataList(List<TenderProposalView> proposalDataList) {
		this.proposalDataList = proposalDataList;
	}

	public UtilityManager getUtilityManager() {
		return utilityManager;
	}

	public void setUtilityManager(UtilityManager utilityManager) {
		this.utilityManager = utilityManager;
	}

	public HtmlTab getExtendsAppTab() {
		return extendsAppTab;
	}

	public void setExtendsAppTab(HtmlTab extendsAppTab) {
		this.extendsAppTab = extendsAppTab;
	}

	public HtmlDataTable getExtendedDataTable() {
		return extendedDataTable;
	}

	public void setExtendedDataTable(HtmlDataTable extendedDataTable) {
		this.extendedDataTable = extendedDataTable;
	}

	public List<ExtendApplicationFilterView> getExtendedDataList() {
		return extendedDataList;
	}

	public void setExtendedDataList(
			List<ExtendApplicationFilterView> extendedDataList) {
		this.extendedDataList = extendedDataList;
	}

	public RequestServiceAgent getRequestServiceAgent() {
		return requestServiceAgent;
	}

	public void setRequestServiceAgent(RequestServiceAgent requestServiceAgent) {
		this.requestServiceAgent = requestServiceAgent;
	}

	public HtmlCommandButton getBtnSend() {
		return btnSend;
	}

	public void setBtnSend(HtmlCommandButton btnSend) {
		this.btnSend = btnSend;
	}

	public SystemParameters getParameters() {
		return parameters;
	}

	public void setParameters(SystemParameters parameters) {
		this.parameters = parameters;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public HtmlTab getInquiryAppTab() {
		return inquiryAppTab;
	}

	public void setInquiryAppTab(HtmlTab inquiryAppTab) {
		this.inquiryAppTab = inquiryAppTab;
	}
    @SuppressWarnings( "unchecked" )
	public List<TenderInquiryView> getTenderInquiryList() {
		if(viewMap.containsKey("TENDER_INQUIRY_LIST"))
			tenderInquiryList = (List<TenderInquiryView>)viewMap.get("TENDER_INQUIRY_LIST");
		return tenderInquiryList;
	}

	public void setTenderInquiryList(List<TenderInquiryView> tenderInquiryList) {
		this.tenderInquiryList = tenderInquiryList;

		if(this.tenderInquiryList!=null)
			viewMap.put("TENDER_INQUIRY_LIST",this.tenderInquiryList);
	}

	public HtmlDataTable getInquiryDataTable() {
		return inquiryDataTable;
	}

	public void setInquiryDataTable(HtmlDataTable inquiryDataTable) {
		this.inquiryDataTable = inquiryDataTable;
	}

	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();

	}

	public Integer getInquiryAppRecordSize() {
		return tenderInquiryList.size();
	}
	@SuppressWarnings( "unchecked" )
	public String btnManage_Click(){

		TenderInquiryView tT = (TenderInquiryView) inquiryDataTable.getRowData();
		sessionMap.put("FROM_INQUIRY_GRID", tT);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:showTenderInquiryReplies();";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "btnManage_Click";
	}
    public String btnDeleteInquiry_Click(){

		TenderInquiryView tIV = (TenderInquiryView) inquiryDataTable.getRowData();
		Integer index =  inquiryDataTable.getRowIndex();
		viewMap.put("delete", index);

		Boolean flage = false;
		infoMessages = new ArrayList<String>();
		try {
			flage = new ConstructionServiceAgent().setTenderInquiryToDeleted(tIV.getRequestId());

			if(flage){
				index = (Integer) viewMap.get("delete");
				List<TenderInquiryView> tempList = getTenderInquiryList();
				tempList.remove(index.intValue());
				setTenderInquiryList(tempList);
				infoMessages.add(CommonUtil.getBundleMessage("tenderInquiryReplies.msg.DetailRequired"));
			}

		} catch (PimsBusinessException e) {
			logger.LogException("Exception in btnDeleteViolation_Click::::::::::::::", e);
		}

		return "EditViolation";


	}
	public String btnViewInquiry_Click(){

		TenderInquiryView tT = (TenderInquiryView) inquiryDataTable.getRowData();
		sessionMap.put("FROM_INQUIRY_GRID", tT);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:showTanderInquiryAddPopUp();";
		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

		return "btnManage_Click";
	}

	public HtmlSimpleColumn getActionInquiryAppCol() {
		return actionInquiryAppCol;
	}

	public void setActionInquiryAppCol(HtmlSimpleColumn actionInquiryAppCol) {
		this.actionInquiryAppCol = actionInquiryAppCol;
	}

	public HtmlSimpleColumn getActionextendsAppCol() {
		return actionextendsAppCol;
	}

	public void setActionextendsAppCol(HtmlSimpleColumn actionextendsAppCol) {
		this.actionextendsAppCol = actionextendsAppCol;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public HtmlSimpleColumn getProposalActionCol() {
		return proposalActionCol;
	}

	public void setProposalActionCol(HtmlSimpleColumn proposalActionCol) {
		this.proposalActionCol = proposalActionCol;
	}


	public String winnerLink_action()
	{
		Boolean success = false;
		String METHOD_NAME = "winnerLink_action()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- started --- ");

		try
		{
			TenderProposalView row = (TenderProposalView) proposalDataTable.getRowData();
			ConstructionTenderDetailsView constructionTenderDetailsView = (ConstructionTenderDetailsView) getTenderView();
			constructionTenderDetailsView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			constructionTenderDetailsView.setContractorId( row.getContractorId() );
			DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.WINNER_SELECTED);
			constructionTenderDetailsView.setTenderStatusId( String.valueOf( ddv.getDomainDataId() ) );
			success = constructionTenderServiceAgent.updateConstructionTenderDetailsView( constructionTenderDetailsView );

			if(this.unmarkAllProposalsFirst(constructionTenderDetailsView.getTenderId()) == true){
				this.changeProposalStatus(WebConstants.TENDER_PROPOSAL_STATUS_WINNER);
				this.updateTenderProposal();
				infoMessages.clear();
				infoMessages.add(CommonUtil.getBundleMessage(MessageConstants.Tender.MARK_AS_WINNER));
				//viewMap.put("MARK_WINNER_DONE",true);
				this.sendNotification(row);
			}

			if ( success )
			{
				changePageMode( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW );
				changeInnerPageMode(pageMode);
			}
			String winner =   row.getProposalNumber()+"/"+
            (
           		 ( row.getContractorNameAr()!= null && row.getContractorNameAr().length() > 0 )?row.getContractorNameAr():
           			                                                                            row.getContractorNameEn()
             );
             saveAttachmentComments( "tender.event.winnerSelected", Long.valueOf( row.getTenderId() ), winner );
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add( CommonUtil.getBundleMessage("commons.operation.failed") );
		}

		return path;
	}


	private void sendNotification(TenderProposalView selectedTenderProposalView)
	{
		List<TenderProposalView> proposalList = this.getProposalDataList();
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		PersonView contractor = new PersonView();
		PropertyServiceAgent psa = new PropertyServiceAgent();

		ConstructionTenderDetailsView constructionTenderDetailsView = new ConstructionTenderDetailsView();


		try
		{

		if(viewMap.containsKey(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW))
			constructionTenderDetailsView=  getTenderView();

		DomainData domainData = new UtilityManager().getDomainDataByDomainDataId(new Long (constructionTenderDetailsView.getConstructionServiceTypeId()));

		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		eventAttributesValueMap.put("TENDER_NUMBER", constructionTenderDetailsView.getTenderNumber());
		eventAttributesValueMap.put("TENDER_TYPE",domainData.getDataDescEn());


		for(TenderProposalView proposalView : proposalList)
		{
			eventAttributesValueMap.put("PROPOSAL_NUMBER", proposalView.getProposalNumber());
			contractor = psa.getPersonInformation(new Long(proposalView.getContractorId()));
			eventAttributesValueMap.put("CONTRACTOR_NAME", proposalView.getContractorNameEn());

			if(proposalView.getTenderProposalId().compareTo(selectedTenderProposalView.getTenderProposalId()) == 0)
			{
				eventAttributesValueMap.put("PROPOSAL_ACTION", "accepted");
			}
			else
				eventAttributesValueMap.put("PROPOSAL_ACTION", "rejected");

			contactInfoList = getContactInfoList(contractor);
			generateNotification(WebConstants.Notification_MetaEvents.Event_Service_Tender_Status_Notification,contactInfoList, eventAttributesValueMap,null);
		}
		}
		catch (PimsBusinessException ex)
		{
			logger.LogException("notification failed",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}


 public Boolean getIsViewMode ()
 {
	if((pageMode.equals(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW))
		||(pageMode.equals(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_APPROVE_REJECT))
		||(pageMode.equals(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_MANAGE)))
	{
		viewMap.put("CAN_DELETE_TENDER_CONTRACTOR",false);
		return false;
	}
	else
	{

		return true;
	}



 }

 public Boolean getMarkWinnerLink()
	{
		if(viewMap.containsKey("MARK_WINNER_DONE"))
		return false;
		else
		return true;
	}

 public Boolean getViewModePopUp()
	{
		if(viewMap.get(WebConstants.Tender.TENDER_MODE_POPUP)!=null)
			return true;
		else
			return false;
	}

public HtmlCommandButton getBtnDeselect() {
	return btnDeselect;
}

public void setBtnDeselect(HtmlCommandButton btnDeselect) {
	this.btnDeselect = btnDeselect;
}

}
