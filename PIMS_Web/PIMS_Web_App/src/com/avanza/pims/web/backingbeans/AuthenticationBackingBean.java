package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.AuthenticationStatus;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.web.config.LocaleCatalog;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;


public class AuthenticationBackingBean extends AbstractController
{


//	private static final Logger logger = Logger.getLogger(AuthenticationBackingBean.class);

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idnumber;
    public String getIdnumber() { return idnumber; }
    public void setIdnumber(String idnumber) { this.idnumber = idnumber; }


	private List<String> messages = new ArrayList();
    private HtmlInputText userName = new HtmlInputText();
    private HtmlInputSecret password = new HtmlInputSecret();
   

   
    @Override
    public void init() {
            // TODO Auto-generated method stub
            super.init();
    }

    @Override
    public void preprocess() {
            // TODO Auto-generated method stub
            super.preprocess();
    }

    @Override
    public void prerender() {
            // TODO Auto-generated method stub
            super.prerender();
    }

    //Private Methods
    public String authenticate()
    {
       
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot view = context.getViewRoot();
        messages = new ArrayList<String>();
        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
        
        this.setIdnumber(((HttpServletRequest)context.getExternalContext().getRequest()).getParameter("idNumber"));
        
        try 
        {
        		
        	System.out.println("AuthenticationBackingBean|authenticate");     
//          Search query = new Search();
//          query.clear();
//          query.addFrom(UserDbImpl.class);
            
            UserDbImpl user  = (UserDbImpl) SecurityManager.getUser(((String)this.userName.getSubmittedValue()).trim());
            
            if( user.getStatusId()!= null && user.getStatusId().equals("0") )
            {
            	
                messages.add( ReportMessageResource.getPropertyEn( "authentication.userInactive") );
                return "failure";

            }
            
            if (user!=null && password!=null && ((String)password.getSubmittedValue()).trim().length()>0)
            {	
            user.getPermission("PIMS");
            
//            String pwd = UserDbImpl.getCryptographer().encrypt( (String)password.getSubmittedValue());
            if ( 
            		user!=null && 
            		user.isSystemUser() && 
            	    ((String)password.getSubmittedValue()).trim().equals(user.getCryptographer().decrypt(user.getPassword()))
            	)
            {
        		System.out.println("AuthenticationBackingBean|user credential correct adding in session");
                session.setAttribute(WebConstants.USER_IN_SESSION,user);

            	prepareLocale(session, user);
            	System.out.println("AuthenticationBackingBean|success");
                return "success";
            }
            else if (user!=null && !user.isSystemUser())
            {
                AuthenticationStatus au = SecurityManager.authenticateUser((String)userName.getSubmittedValue(),(String)password.getSubmittedValue());
                
                if (au.getAuthenStatus().toString().equalsIgnoreCase("success"))
                {   
                    prepareLocale(session, user);
                    
                    return "success";
                }
                else 
                {
                	messages.add( ReportMessageResource.getPropertyAr( MessageConstants.Authentication.AUTHENTICATION_FALIURE) );
                	System.out.println("AuthenticationBackingBean|failure for loginId:"+(String)userName.getSubmittedValue());
                	return "failure";
                }

            }
            else
            	{
                	messages.add( ReportMessageResource.getPropertyAr( MessageConstants.Authentication.AUTHENTICATION_FALIURE) );
                	System.out.println("AuthenticationBackingBean|failure");
                	return "failure";
            	}
            }
            else
            {
                messages.add( ReportMessageResource.getPropertyAr( MessageConstants.Authentication.AUTHENTICATION_FALIURE) );
                System.out.println("AuthenticationBackingBean|failure");
            	return "failure";
            }
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        return "success";
    }
	private void prepareLocale(HttpSession session, UserDbImpl user) {
		session.setAttribute(WebConstants.USER_IN_SESSION,user);
               
		//Change by kazim locale framework integerated in PIMS.
		LocaleInfo locale = LocaleCatalog.getInstance().getDefaultLocale();
		
		//Setting the locale 
		if (Integer.parseInt(this.getIdnumber())==1) {
			locale = LocaleCatalog.getInstance().getLocale("en", "US");
		    
		} else { 
			locale = LocaleCatalog.getInstance().getLocale("ar", "SA");
		}

		session.setAttribute(CoreConstants.CurrentLocale, locale);
	}

	public HtmlInputText getUserName() {
		return userName;
	}

	public void setUserName(HtmlInputText userName) {
		this.userName = userName;
	}

	public HtmlInputSecret getPassword() {
		return password;
	}

	public void setPassword(HtmlInputSecret password) {
		this.password = password;
	}   
	
	public String getMessages() {
		return getErrorMessage(messages);
		}

		public void setMessages(List<String> messages) {
			this.messages = messages;
		}
	
	private String getErrorMessage(List<String> errorMessages)
	{
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		}
		else {
			messageList = "";
			for (String message : errorMessages) {
				messageList = messageList + "<br>" + message;
			}
		}
		return (messageList);
	}

}
