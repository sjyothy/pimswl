package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.FollowUpService;
import com.avanza.pims.ws.vo.FollowUpActionsView;
import com.avanza.ui.util.ResourceUtil;

public class FollowUpActionsSearchBean extends AbstractController
{
	
	
	FacesContext context ;
	Map<String, Object> sessionMap; 
	Map<String,Object> viewMap ;
	private static Logger logger = Logger.getLogger(FollowUpActionsSearchBean.class);;
	
	private HtmlInputText txt_followUpActionDescEn;
	private HtmlInputText txt_followUpActionDescAr;
	private javax.faces.component.html.HtmlSelectOneMenu cmb_followUpStatus;
	private HtmlDataTable dataTable;
	private ArrayList<String> errorMessages;
	private ArrayList<String> successMessages;
	private List<FollowUpActionsView> list ;
	private Integer paginatorMaxPages =10;
	Integer paginatorRows=15;
	Integer recordSize=0;

	
	@SuppressWarnings("unchecked")
	public FollowUpActionsSearchBean()
	{
		
		context = FacesContext.getCurrentInstance();
		sessionMap = context.getExternalContext().getSessionMap();
		viewMap = (Map<String,Object>)FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		
		txt_followUpActionDescEn = new HtmlInputText();
		txt_followUpActionDescAr = new HtmlInputText();
		cmb_followUpStatus = new javax.faces.component.html.HtmlSelectOneMenu();
		
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		list = new ArrayList<FollowUpActionsView>(0);
	}
	public boolean getIsArabicLocale()
	{
		return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String btnClear_Click()
	{
//		txt_followUpActionDescEn.setValue("");
//		txt_followUpActionDescAr.setValue("");
//		cmb_followUpStatus.setValue("");
//		this.setRecordSize(0);
//		this.setList(null);
//		setRequestParam("requestScope",0);
		return "clear";
		
	}
	public void btnSearch_Click()
	{
		String methodName= "btnSearch_Click";
		
		
		try 
		{
			FollowUpServiceAgent fsa = new FollowUpServiceAgent();
			logger.logInfo(methodName+"|Start");
			FollowUpActionsView vo = new FollowUpActionsView();
			
			if(txt_followUpActionDescEn.getValue()!=null && txt_followUpActionDescEn.getValue().toString().trim().length()>0)
			   vo.setDescriptionEn(txt_followUpActionDescEn.getValue().toString().trim());
			
			if(txt_followUpActionDescAr.getValue()!=null && txt_followUpActionDescAr.getValue().toString().trim().length()>0)
			   vo.setDescriptionAr(txt_followUpActionDescAr.getValue().toString().trim());
			
			if(cmb_followUpStatus.getValue()!=null && !cmb_followUpStatus.getValue().toString().equals("-1"))
			   vo.setFollowUpStatus(new Long(cmb_followUpStatus.getValue().toString()));    
			
			this.setList(fsa.getAllFollowUpActionsForSeacrh(vo));
		    if(this.getList() != null )
		    	this.setRecordSize(this.getList().size());
			
			logger.logInfo(methodName+"|Finish");
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
	}
	public void imgEdit_Click()
	{
		String methodName= "imgEdit_Click";
		
		
		try 
		{
			logger.logInfo(methodName+"|Start");
		    FollowUpActionsView fpView = (FollowUpActionsView)dataTable.getRowData();
			sessionMap.put(WebConstants.FollowUp.FOLLOW_UP_VIEW,fpView);
		    String javaScriptText="javaScript:openManageFollowUpActionsPopup();";
	        AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
	        addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(methodName+"|Finish");
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
	}

	public void onRefresh()
	{
		String methodName= "onRefresh";
		
		
		try 
		{
			logger.logInfo(methodName+"|Start");
		    this.btnSearch_Click();
		    successMessages.add(ResourceUtil.getInstance().getProperty("followupaction.savedSuccessfully"));
			
			logger.logInfo(methodName+"|Finish");
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
	}


	public HtmlInputText getTxt_followUpActionDescEn() {
		return txt_followUpActionDescEn;
	}


	public void setTxt_followUpActionDescEn(HtmlInputText txt_followUpActionDescEn) {
		this.txt_followUpActionDescEn = txt_followUpActionDescEn;
	}


	public HtmlInputText getTxt_followUpActionDescAr() {
		return txt_followUpActionDescAr;
	}


	public void setTxt_followUpActionDescAr(HtmlInputText txt_followUpActionDescAr) {
		this.txt_followUpActionDescAr = txt_followUpActionDescAr;
	}


	public javax.faces.component.html.HtmlSelectOneMenu getCmb_followUpStatus() {
		return cmb_followUpStatus;
	}


	public void setCmb_followUpStatus(javax.faces.component.html.HtmlSelectOneMenu cmb_followUpStatus) {
		this.cmb_followUpStatus = cmb_followUpStatus;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}


	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}


	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings("unchecked")
	public List<FollowUpActionsView> getList() {
		if(viewMap.containsKey("list"))
		   list = (ArrayList<FollowUpActionsView>)viewMap.get("list");
		return list;
	}


	public void setList(List<FollowUpActionsView> list) {
		this.list = list;
		if(this.list!=null && this.list.size()>0)
			viewMap.put("list", this.list);
		else
			viewMap.remove("list");
	}


	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}


	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	public Integer getPaginatorRows() {
		return paginatorRows;
	}


	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	public Integer getRecordSize() {
	   if(viewMap.containsKey("recordSize"))	
		   recordSize = new Integer(viewMap.get("recordSize").toString());
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if(this.recordSize!=null)
			viewMap.put("recordSize", this.recordSize);
	}







}
