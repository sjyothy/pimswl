package com.avanza.pims.web.backingbeans;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractPaymentDetailReportCriteria;
import com.avanza.pims.report.criteria.TenantStatementReportCriteria;
import com.avanza.pims.report.criteria.TenantViolationsReportCriteria;
import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PersonView;

public class TenantStatementBean extends AbstractController {

	TenantStatementReportCriteria  reportCriteria;
	 private static Logger logger = Logger.getLogger("TenantStatementBean");
	 private HtmlCommandButton btnPrint = new HtmlCommandButton();
	
	 /** FOR TANENT DIALOG **/
		private String hdnTenantId;
		private String TENANT_INFO="TENANTINFO";
		
		@SuppressWarnings( "unchecked" )
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		
		public String getPersonTenant() {
			
			return WebConstants.PERSON_TYPE_TENANT;
		}
		
		/** FOR TANENT DIALOG END **/
		
	public TenantStatementBean() 
	{
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new TenantStatementReportCriteria(ReportConstant.Report.TENANT_STATEMENT_REPORT_EN, ReportConstant.Processor.TENANT_STATEMENT, CommonUtil.getLoggedInUser());		
		else
			reportCriteria = new TenantStatementReportCriteria(ReportConstant.Report.TENANT_STATEMENT_REPORT_AR, ReportConstant.Processor.TENANT_STATEMENT, CommonUtil.getLoggedInUser());		
	}
	
	public TenantStatementReportCriteria getReportCriteria() {
		return reportCriteria;
	}

	public void setReportCriteria(TenantStatementReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}
	

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public String cmdView_Click() 
	{
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	/** FOR TANENT DIALOG **/
	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}
	
	public void retrieveTenant()
	{
		String methodName =  " retrieveTenant";
		logger.logInfo(methodName+ " Started");
		try
		{
		FillTenantInfo();

		if(viewRootMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);

//		   String tenant_Type=getTenantType(tenantViewRow);


	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
	        contractListCriteriaBackingBean.setTenantName(tenantNames);
//	        contractListCriteriaBackingBean.setTenantType(tenant_Type);
	    }

		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed", ex);
		}

	}
	private void FillTenantInfo() throws PimsBusinessException {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

	    	}

	}
	/** FOR TANENT DIALOG END **/
	

}
