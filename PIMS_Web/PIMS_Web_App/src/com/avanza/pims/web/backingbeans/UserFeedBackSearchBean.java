/**
 * 
 */
package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.AppointmentDetails;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.services.UserFeedbackService;
import com.avanza.pims.ws.vo.UserFeedbackVO;
import com.avanza.ui.util.ResourceUtil;

/**
 * UserFeedBackSearchBean - Controller for search user feedback
 * @author Imran Ali
 *
 */
public class UserFeedBackSearchBean extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "userFeedbackId";
	private final String DATA_LIST = "DATA_LIST";
	UserFeedbackService service = new UserFeedbackService();

	UserFeedbackVO criteria ;
	List<UserFeedbackVO> dataList = new ArrayList<UserFeedbackVO>();
	
	
	public UserFeedBackSearchBean() {
		criteria = new UserFeedbackVO();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			UserFeedbackVO obj = null;
			obj = (UserFeedbackVO) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}


	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			AppointmentDetails row = (AppointmentDetails) dataTable.getRowData();
			if (row != null) 
			{
				getRequestMap().put(Constant.AppointmentProgram.APPOINTMENT_PROGRAM, row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
			setSortField(DEFAULT_SORT_FIELD);			
			loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}



	@SuppressWarnings("unchecked")
	public List<UserFeedbackVO> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<UserFeedbackVO>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<UserFeedbackVO> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}


	/**
	 * @return the service
	 */
	public UserFeedbackService getService() {
		return service;
	}


	/**
	 * @param service the service to set
	 */
	public void setService(UserFeedbackService service) {
		this.service = service;
	}


	/**
	 * @return the criteria
	 */
	public UserFeedbackVO getCriteria() {
		return criteria;
	}


	/**
	 * @param criteria the criteria to set
	 */
	public void setCriteria(UserFeedbackVO criteria) {
		this.criteria = criteria;
	}


	
}
