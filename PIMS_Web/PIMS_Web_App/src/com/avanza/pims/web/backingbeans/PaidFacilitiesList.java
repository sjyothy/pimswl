package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.PaidFacilitiesView;




public class PaidFacilitiesList  extends AbstractController
{
	
	public String selectedFacilty;
	public String fromDate;
	public String toDate;
	public String rentValue="0.00";
	public String hdnrentIdValue="";
	List<SelectItem> facilityList = new ArrayList();
	public boolean showProspectiveGrid;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger( PaidFacilitiesList.class );
	
	private ArrayList<PaidFacilitiesView> dataList ;
	private HtmlDataTable tbl_Facility;
	private FacilityView dataItem = new FacilityView();
	
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	List<SelectItem> paidFacilitiesList=new ArrayList<SelectItem>();
  



	  



	
	// Constructors

	/** default constructor */
	public PaidFacilitiesList() 
	{
		logger.logInfo("Constructor|Inside Constructor");
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
	
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		isEnglishLocale =  view.getLocale().toString().equals("en");
		return isEnglishLocale;
	}
	private void loadPaidFacilityCombo()
	{
	  String methodName="loadPaidFacilityCombo";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  PropertyServiceAgent psa =new PropertyServiceAgent();
		  logger.logInfo(methodName +"|"+"Querying DB for facility Typ"+WebConstants.FACILITY_TYPE_PAID);
//		  List<FacilityView> facilityViewList= psa.getFacilitiesByFacilityTypeId(new Long(WebConstants.FACILITY_TYPE_PAID));
//		  logger.logInfo(methodName +"|"+"Got "+facilityViewList.size()+" elements from DB");
		  logger.logInfo(methodName +"|"+"Placing in sessionMap with key::"+WebConstants.FACILITY_TYPE_PAID);
//		  sessionMap.put(WebConstants.FACILITY_TYPE_PAID, facilityViewList);
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	}
	
	  logger.logInfo(methodName +"|"+"Finish ");
	}
	
	private String getLoggedInUser() 
	{
	        context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public boolean getshowProspectiveGrid()
	{
		showProspectiveGrid=false;
		if(sessionMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) )
			showProspectiveGrid=true;
		
		return showProspectiveGrid;
		
	}
	  public List<PaidFacilitiesView> getGridDataList()
	{
		if(dataList==null)
		{
	         if(sessionMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) 
	           && sessionMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null 
	           )		
                  dataList =(ArrayList)sessionMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
			
		}
		return dataList;
	}
	public HtmlDataTable getTbl_Facility() {
		return tbl_Facility;
	}


	public void setTbl_Facility(HtmlDataTable tbl_Facility) {
		this.tbl_Facility = tbl_Facility;
	}


	public FacilityView getDataItem() {
		return dataItem;
	}


	public void setDataItem(FacilityView dataItem) {
		this.dataItem = dataItem;
	}


	private PaidFacilitiesView putControlValuesInView() throws Exception
	{
	
		String methodName="putControlValuesInView";
		logger.logInfo(methodName +"|"+"Start");
		PaidFacilitiesView paidFacilityView=new PaidFacilitiesView();	
		logger.logInfo(methodName +"|"+"selectedFacilty::"+selectedFacilty);
		DateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");
		try 
		{
			if(selectedFacilty!=null && !selectedFacilty.equals("") && !selectedFacilty.equals("-1"))
			{
			  paidFacilityView.setFacilityId(new Long(selectedFacilty));
			  //Assigning Name and description against facility id
			  logger.logInfo(methodName +"|"+"Getting Name and description against facility id::"+selectedFacilty);
			  Hashtable hash= getFacilityValuesForFacilityId(selectedFacilty);
			  logger.logInfo(methodName +"|"+"FacilityName::"+hash.get("facilityname").toString());
			  paidFacilityView.setFacilityName(hash.get("facilityname").toString() );
			  logger.logInfo(methodName +"|"+"FacilityDescription::"+hash.get("facilitydescription").toString());
			  paidFacilityView.setFacilityDescription(hash.get("facilitydescription").toString() );
			}
			else
			{
			}
			logger.logInfo(methodName +"|"+"fromDate::"+fromDate);
			if(fromDate!=null && !fromDate.equals(""))
			{
			  
			   paidFacilityView.setDateFrom(dateFormat.parse(fromDate));
			 
			}
			else
			{
			}
			
			logger.logInfo(methodName +"|"+"rentValue::"+fromDate);
			if( rentValue!=null && !rentValue.equals(""))
			{
			  
			   paidFacilityView.setRent(new Double(rentValue));
			 
			}
			else
			{
			}
			logger.logInfo(methodName +"|"+"CreatedBy::"+getLoggedInUser());
			   paidFacilityView.setCreatedBy(getLoggedInUser());
			logger.logInfo(methodName +"|"+"UpdatedBy::"+getLoggedInUser());
			   paidFacilityView.setUpdatedBy(getLoggedInUser());
			logger.logInfo(methodName +"|"+"RecordStatus::1");
			   paidFacilityView.setRecordStatus(new Long(1));
			logger.logInfo(methodName +"|"+"IsDeleted::0");
			   paidFacilityView.setIsDeleted(new Long(0));
			logger.logInfo(methodName +"|"+"IsManddatory::1");
			   paidFacilityView.setIsMandatory("1");
		}
		catch (Exception e) 
		{
              logger.logError(methodName +"|"+"Exception Occured::"+e);
              throw e;
              
		}
		
		logger.logInfo(methodName +"|"+"Finsih");
		return paidFacilityView;
	}
    private Hashtable getFacilityValuesForFacilityId(String selectedfacilityId)
    {
       String methodName="getFacilityValuesForFacilityId";
       Hashtable outCome =new Hashtable();
        List<FacilityView>facilityViewList=(ArrayList)sessionMap.get(WebConstants.FACILITY_TYPE_PAID);
		FacilityView facilityView;
		logger.logInfo(methodName+"|"+"Got"+facilityViewList.size()+" paid facilities row from session");
			for(int i=0 ;i<facilityViewList.size();i++)
			{
				 
				  facilityView=(FacilityView)facilityViewList.get(i);
				  logger.logInfo(methodName+"|"+"facilityId from Session"+facilityView.getFacilityId());
				  logger.logInfo(methodName+"|"+"Selected"+selectedfacilityId);
				  if(facilityView.getFacilityId().toString().equals(selectedfacilityId))
				  {
				    outCome.put("facilityname", facilityView.getFacilityName());
				    outCome.put("facilitydescription",facilityView.getDescription());
				  }
				
			}
		
			
         return outCome;
    
    
    } 	
	public String btnAdd_Click()
	{
		String methodName="btnAdd_Click";
		try
		{
		   addNewItemsInSession();
        

        
        }
        catch(Exception e)
        {
             logger.logError(methodName +"|"+"Exception Occured::"+e);
        }
    	return "";
	}
	private void addNewItemsInSession() throws Exception
	{
	   String methodName="addNewItemsInSession";
	   List<PaidFacilitiesView> previousPaidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
	   List<PaidFacilitiesView> newPaidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
	   PaidFacilitiesView paidFacilitiesView =new PaidFacilitiesView(); 
		try
		{
		  paidFacilitiesView=putControlValuesInView();
          if(sessionMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) && sessionMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null)
          {
           previousPaidFacilitiesViewList=(ArrayList)sessionMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES); 
           
           for(int i=0;i<previousPaidFacilitiesViewList.size();i++)
           {
             PaidFacilitiesView previousPaidFacilityView=(PaidFacilitiesView) previousPaidFacilitiesViewList.get(i);
            
            
            
              logger.logInfo(methodName+"|"+"previousPaidFacilityView.getFacilityId():::"+previousPaidFacilityView.getFacilityId());
			  logger.logInfo(methodName+"|"+"Selected paidFacilitiesView.getFacilityId():::"+paidFacilitiesView.getFacilityId());
			  //If previous List has same item with id  as new View 
              //then discard it else add that item to new List.    
			  
             if(!previousPaidFacilityView.getFacilityId().toString().equals(paidFacilitiesView.getFacilityId().toString()))
             {
              newPaidFacilitiesViewList.add(previousPaidFacilityView);
              
             }
           
           } 
           
           //newPaidFacilitiesViewList =previousPaidFacilitiesViewList;
          }
          //Adding new updated List to session
          newPaidFacilitiesViewList.add(paidFacilitiesView);
          sessionMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES, newPaidFacilitiesViewList);
        }
        catch(Exception e)
        {
             logger.logError(methodName +"|"+"Exception Occured::"+e);
             throw e;
        }

	



     }
	public List<SelectItem> getPaidFacilitiesList() 
	{
		String methodName="getPaidFacilitiesList";
		paidFacilitiesList.clear();
		hdnrentIdValue="";
		List<FacilityView> facilityViewList =new ArrayList<FacilityView>();
		logger.logInfo(methodName+"|"+"Start");
		logger.logInfo(methodName+"|"+"Checking the key by name ( "+WebConstants.FACILITY_TYPE_PAID+" ) in session");
		if(!sessionMap.containsKey(WebConstants.FACILITY_TYPE_PAID))
		{
			logger.logInfo(methodName+"|"+"key by name ( "+WebConstants.FACILITY_TYPE_PAID+" ) not in session,so getting it from db");
			loadPaidFacilityCombo();
		}
		else
		{
			logger.logInfo(methodName+"|"+"key by name ( "+WebConstants.FACILITY_TYPE_PAID+" ) is in session");
		
		}
		facilityViewList=(ArrayList)sessionMap.get(WebConstants.FACILITY_TYPE_PAID);
		FacilityView facilityView;
		logger.logInfo(methodName+"|"+"Got"+facilityViewList.size()+" paid facilities row from session");
			for(int i=0 ;i<facilityViewList.size();i++)
			{
				 
				  facilityView=(FacilityView)facilityViewList.get(i);
				  SelectItem item = new SelectItem(facilityView.getFacilityId().toString(), facilityView.getFacilityName());
				        hdnrentIdValue+=facilityView.getFacilityId().toString()+",";
				     if   (facilityView.getRent()!=null)
				        hdnrentIdValue+=facilityView.getRent().toString();
				     else
				        hdnrentIdValue+="";
				  if(i<facilityViewList.size()-1)
				     hdnrentIdValue+="|";
				  paidFacilitiesList.add(item);  
				
				
			}
		
			
		logger.logInfo(methodName+"|"+"Finish");
		return paidFacilitiesList;
	}


	public void setPaidFacilitiesList(List<SelectItem> paidFacilitiesList) {
		this.paidFacilitiesList = paidFacilitiesList;
	}


	public String getSelectedFacilty() {
		return selectedFacilty;
	}


	public void setSelectedFacilty(String selectedFacilty) {
		this.selectedFacilty = selectedFacilty;
	}


	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}


	public String getRentValue() {
		return rentValue;
	}


	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}


	public String getHdnrentIdValue() {
		return hdnrentIdValue;
	}


	public void setHdnrentIdValue(String hdnrentIdValue) {
		this.hdnrentIdValue = hdnrentIdValue;
	}


    @Override
   public void init() 
   {
   	
   	String methodName="init";
   	logger.logInfo(methodName +"|"+"Start");
   	
   	logger.logInfo(methodName +"|"+"isPostBack()="+isPostBack());
   	sessionMap =context.getExternalContext().getSessionMap();
   	if(!isPostBack())
   	{
   		logger.logInfo(methodName +"|"+"Checking if facility List is already in session");
   		if(!sessionMap.containsKey(WebConstants.FACILITY_TYPE_PAID))
   		{
   		
   		 logger.logInfo(methodName +"|"+"Facility List is not in session,so fetching from db ");
   		 loadPaidFacilityCombo();
   		}
   		else
   			logger.logInfo(methodName +"|"+"Facility List is  in session");
   	}
   	logger.logInfo(methodName +"|"+"Finish");
   	super.init();
   }


 
   @Override
   public void preprocess() {
   	super.preprocess();
   	System.out.println("Here there everywhere preprocess");
   	
   }

   @Override
   public void prerender() {
   	super.prerender();
   	System.out.println("Here there everywhere"+"prerender");
   }
	
	
	
	

}