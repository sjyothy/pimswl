package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.FollowUpServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FollowUpProgressView;

public class AddFollowUpProgressBacking extends AbstractController {
	
	private static final long serialVersionUID = -7491594603683782554L;
	
	// Commons
	private Logger logger = Logger.getLogger(AddFollowUpProgressBacking.class);
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	private Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private CommonUtil commonUtil = new CommonUtil();
	private FollowUpServiceAgent followUpServiceAgent = new FollowUpServiceAgent();
	
	// Follow Up Progress 
	private FollowUpProgressView followUpProgressView = new FollowUpProgressView();
	
	@Override
	public void init() {		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- started --- ");
			super.init();
			updateValuesFromMap();
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMap() throws Exception { 
		
		if ( sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW) != null )
		{
			followUpProgressView = (FollowUpProgressView) sessionMap.get(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW);			
			sessionMap.remove(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW);
		}
		else if ( viewMap.get(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW) != null )
		{
			followUpProgressView = (FollowUpProgressView) viewMap.get(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW);
		}
		
		updateValuesToMap();
	}
	
	private void updateValuesToMap() {
		if ( followUpProgressView != null )
			viewMap.put(WebConstants.FollowUp.FOLLOW_UP_PROGRESS_VIEW, followUpProgressView);
	}
	
	@Override
	public void preprocess() {		
		super.preprocess();
	}
	
	@Override
	public void prerender() {		
		super.prerender();
	}
	public void cmbFollowUpActions_Changed()
	{
		if(followUpProgressView.getFollowUpActionIdString()!=null && followUpProgressView.getFollowUpActionIdString().length()>0)
		  followUpProgressView.setStatusId(new Long(followUpProgressView.getFollowUpActionIdString()));
		else
			followUpProgressView.setStatusId(new Long("-1"));
		
		
	}
	
	public String btnSave_Click() {
		String METHOD_NAME = "btnSave_Click()";
		String path = null;
		Boolean success = false;
		logger.logInfo(METHOD_NAME + " --- started --- ");
		try	{
			followUpProgressView.setIsLast(true);
			followUpProgressView.setCreatedBy( CommonUtil.getLoggedInUser() );
			followUpProgressView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			success = followUpServiceAgent.addFollowUpProgress(followUpProgressView);
			if ( success ) {
				sessionMap.put(WebConstants.FollowUp.RE_FETCH, true);
				String javaScriptText = "javascript:closePopupAndRefreshOpener();";
				openPopup(javaScriptText);
			}
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
		return path;
	}
	
	public Boolean getIsFollowUpProgressAction() {
		Boolean isAction = true;
		if ( followUpProgressView != null && followUpProgressView.getFollowUpProgressTypeId() != null )
		{
			DomainDataView domainDataView =  CommonUtil.getIdFromType(
					                         CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUp.DOMAIN_TYPE_FOLLOW_UP_PROGRESS_TYPE), 
					                         WebConstants.FollowUp.DOMAIN_DATA_COURT_RESULT
					                         );
			if ( followUpProgressView.getFollowUpProgressTypeId().equals( domainDataView.getDomainDataId() ) )
				isAction = false;
		}
		return isAction;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	
	public Map<String, Object> getViewMap() {
		return viewMap;
	}
	
	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public FollowUpProgressView getFollowUpProgressView() {
		return followUpProgressView;
	}

	public void setFollowUpProgressView(FollowUpProgressView followUpProgressView) {
		this.followUpProgressView = followUpProgressView;
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public FollowUpServiceAgent getFollowUpServiceAgent() {
		return followUpServiceAgent;
	}

	public void setFollowUpServiceAgent(FollowUpServiceAgent followUpServiceAgent) {
		this.followUpServiceAgent = followUpServiceAgent;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}
}
