package com.avanza.pims.web.backingbeans;
import com.avanza.pims.report.criteria.PropertyDetailReportCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.util.CommonUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.RegionView;

public class PropertyDetailBackingBean extends AbstractController
{	

	private static final long serialVersionUID = 3124991600424588667L;
	private PropertyDetailReportCriteria propertyDetailReportCriteria;
	private Logger logger;
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public PropertyDetailBackingBean() 
	{
		logger = Logger.getLogger(PropertyDetailBackingBean.class);
		if(CommonUtil.getIsEnglishLocale())
			propertyDetailReportCriteria = new PropertyDetailReportCriteria(ReportConstant.Report.PROPERTY_DETAIL_EN, ReportConstant.Processor.PROPERTY_DETAIL,CommonUtil.getLoggedInUser());
		else
			propertyDetailReportCriteria = new PropertyDetailReportCriteria(ReportConstant.Report.PROPERTY_DETAIL_AR, ReportConstant.Processor.PROPERTY_DETAIL,CommonUtil.getLoggedInUser());
		
	}

	public PropertyDetailReportCriteria getPropertyDetailReportCriteria() {
		return propertyDetailReportCriteria;
	}

	public void setPropertyDetailReportCriteria(
			PropertyDetailReportCriteria propertyDetailReportCriteria) {
		this.propertyDetailReportCriteria = propertyDetailReportCriteria;
	}

	
	
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertyDetailReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	
	
	/////////////
	public void init() {
		super.init();
			//loadCountry();
			loadState();
			//loadCity(20L);
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", new ArrayList<SelectItem>(0));
//	    		if(request.getParameter("pageMode")!=null&&
//	    				request.getParameter("pageMode").equals(MODE_SELECT_ONE_POPUP)) 
//      	    		viewMap.put("pageMode",MODE_SELECT_ONE_POPUP );
//      	    	else if(request.getParameter("pageMode")!=null&&
//      	    			request.getParameter("pageMode").equals(MODE_SELECT_MANY_POPUP))
//      	    		viewMap.put("pageMode",MODE_SELECT_MANY_POPUP );
//	    		if(viewMap.get("pageMode")==null)
//	    			viewMap.put("pageMode","fullScreenMode");
			}
	}
	
	
/*	public void loadCountry()  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      
			
		      this.getCountryList().add(item);
		  }
		
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("country", countryList);
		
		}catch (Exception e){
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}
	
*/	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void loadState(/*ValueChangeEvent vce*/)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "UAE";
		/*for (int i=0;i<this.getCountryList().size();i++)
		{
			if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
			{
				selectedCountryName = 	this.getCountryList().get(i).getLabel();
			}
		}	*/
		
		//List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
		
		Long UAE_Region_Id = 19L;
		List <RegionView> regionViewList = psa.getCountryStatesExt(UAE_Region_Id);
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}

	/////////////
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@SuppressWarnings("unchecked")
	public void  loadCity(Long stateId)
	{
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try 
		{
			 Set<RegionView> regionViewList = null;
			 if ( stateId != null && stateId.toString().length()>0 && 
				  !stateId.toString().equals( "0" )
			    )
			 {
				 regionViewList =  psa.getCity( stateId );
				Iterator itrator = regionViewList.iterator();
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)itrator.next();
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
					  }
					  else 
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      
					  cmbCityList.add(item);
				  }
				
				Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
			 }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", cmbCityList);
			 logger.logInfo(methodName+"|"+"Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);
			
		}
	   }
	
	@SuppressWarnings("unchecked")
	public void  loadCity(ValueChangeEvent vce )
	{
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try 
		{
			 Set<RegionView> regionViewList = null;
			 if ( vce.getNewValue() != null && vce.getNewValue().toString().length()>0 && 
				  !vce.getNewValue().toString().equals( "0" )
			    )
			 {
				 regionViewList =  psa.getCity( new Long( vce.getNewValue().toString() ) );
				Iterator itrator = regionViewList.iterator();
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)itrator.next();
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
					  }
					  else 
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      
					  cmbCityList.add(item);
				  }
				
				Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
			 }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", cmbCityList);
			 logger.logInfo(methodName+"|"+"Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);
			
		}
	   }
	
}
