package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;

import org.apache.myfaces.component.html.ext.HtmlDataTable;




import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;

import com.avanza.pims.dao.InquiryManager;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Facility;
import com.avanza.pims.entity.Inquiry;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;

import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.entity.Property;
import com.avanza.ui.util.ResourceUtil;

public class AddFacility extends AbstractController {
	
	
	private String pageMode;
	private String PAGE_MODE_ADD="ADD";
	private String PAGE_MODE_EDIT="EDIT";
	private HtmlDataTable dataTable;
	private String facilityId;
	private String facilityName;
	private String facilityDescription;
	private Double rentValue;
	private String selectOneFacilityType;
	private Boolean txtBoxShowHide;
	
	private HtmlInputText ctlRentValue=new HtmlInputText();
	
	private boolean isPageModeAdd;
	
	String selectOneTitle;
	 
	private FacilityView facilityView = new FacilityView();
	private FacilityView facilityViewEdit = new FacilityView();
	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private static Logger logger = Logger.getLogger( AddFacility.class );
	FacesContext context=getFacesContext();
    Map sessionMap;
    Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    CommonUtil	commonUtil = new CommonUtil();
	
    public AddFacility (){
		
	}
	
	public void init() 
    {
   	
   	 super.init();
   	 try
   	 {
   		 System.out.println("Testing");
    sessionMap=context.getExternalContext().getSessionMap();
   
    if(!isPostBack()){
    	
            txtBoxShowHide =false;    	
    		PropertyServiceAgent psa = new PropertyServiceAgent();
    	   Map  request=context.getExternalContext().getRequestMap();
           if(request.get("facilityId")!=null && !request.get("facilityId").toString().trim().equals("")){
        	  facilityId=request.get("facilityId").toString().trim();
        	  facilityViewEdit.setFacilityId(new Long(facilityId));
        	  facilityViewEdit =  psa.getFacilityById(new Long(facilityId));
        	  
        	  if(facilityViewEdit.getCreatedBy()!=null){
               	  viewMap.put("CreatedBy", facilityViewEdit.getCreatedBy());}
               	  if(facilityViewEdit.getCreatedOn()!=null){
               	  viewMap.put("CreatedOn", facilityViewEdit.getCreatedOn());}
    		 
    	    pageMode=PAGE_MODE_EDIT;	
    	    putFacilityViewValueInControl();
    	}
    	else
    	{
    		selectOneFacilityType = WebConstants.FACILITY_FACILITY_TYPE_PAID;
    		pageMode=PAGE_MODE_ADD;
    		
    	}
    		
     }
   		 
   	 }
   	 catch(Exception es)
   	 {
   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
   		 System.out.println(es);
   		 
   	 }
	        
	 }

	public void preprocess(){
	super.preprocess();
	
	if (facilityViewEdit.getFacilityTypeId() != null && pageMode.equals("EDIT"))
    {
 	   selectOneFacilityType = facilityViewEdit.getFacilityTypeId().toString();
    	
 
    	DomainDataView ddv= getTypeFromId( getDomainDataListForDomainType(WebConstants.FACILITY_TYPE),
	 		       selectOneFacilityType
	              );
    	
    	if (ddv.getDataValue().equals(WebConstants.FACILITY_FACILITY_TYPE_FREE)){
	    	txtBoxShowHide = true;
	   	    			}
    	selectOneFacilityType = facilityViewEdit.getFacilityTypeId().toString();    	   
	   }
}
	

	
	public DomainDataView  getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		DomainDataView returnDomainDataView =new DomainDataView();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				returnDomainDataView=domainDataView;
				return returnDomainDataView;
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return returnDomainDataView;
		
	}
	public DomainDataView  getTypeFromId(List<DomainDataView> ddv ,String id)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+id);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		DomainDataView returnDomainDataView =new DomainDataView();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDomainDataId().toString().equals(id))
			{
				returnDomainDataView=domainDataView;
				return returnDomainDataView;
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return returnDomainDataView;
		
	}
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
				  ddvList.add(ddv);	
				}
				break;
			}
		}
    	
    	return ddvList;
    }
public void prerender(){
	super.prerender();
	
	
	if (facilityViewEdit.getFacilityTypeId() != null && pageMode.equals("EDIT"))
    {
 	   selectOneFacilityType = facilityViewEdit.getFacilityTypeId().toString();
    	
 
    	DomainDataView ddv= getTypeFromId( getDomainDataListForDomainType(WebConstants.FACILITY_TYPE),
	 		       selectOneFacilityType
	              );
    	
    	if (ddv.getDataValue().equals(WebConstants.FACILITY_FACILITY_TYPE_FREE)){
	    	txtBoxShowHide = true;
	   	    			}
    	selectOneFacilityType = facilityViewEdit.getFacilityTypeId().toString();    	   
	   }
	
}

private boolean  putFacilityViewValueInControl() throws ParseException{
		
		String methodName = "putTenantViewValueInControl";
		DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
		
	       boolean isSucceed=true;
	      
	    if(facilityViewEdit.getFacilityName()!=null ){
	       	
	    	 facilityName=facilityViewEdit.getFacilityName();
	       }

	    if (facilityViewEdit.getDescription() != null)
	       {
	    	   facilityDescription = facilityViewEdit.getDescription();
		   }
	     
	    if (	facilityViewEdit.getRent() != null)
	       {
	    	   rentValue = facilityViewEdit.getRent();
		   }

	    if (facilityViewEdit.getFacilityTypeId() != null)
	       {
	    	   selectOneFacilityType = facilityViewEdit.getFacilityTypeId().toString();
	       	
	    
	       	DomainDataView ddv= getTypeFromId( getDomainDataListForDomainType(WebConstants.FACILITY_TYPE),
		 		       selectOneFacilityType
		              );
	       	
	       	if (ddv.getDataValue().equals(WebConstants.FACILITY_FACILITY_TYPE_FREE)){
	   	    	txtBoxShowHide = true;
    	   	    			}
	       	selectOneFacilityType = ddv.getDataDescEn();    	   
	 	   }
	    
	    
	    
	      return isSucceed;
	}
		
	
public String getErrorMessages() {
	return commonUtil.getErrorMessages(this.errorMessages);
}

public String getSuccessMessages() {
	
	return commonUtil.getErrorMessages(this.successMessages);
}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


public String btnAdd_Click() throws Exception {

	String methodName = "btnAdd_Click";
	String eventOutCome = "failure";
	String s1;
	errorMessages = new ArrayList<String>();
	successMessages = new ArrayList<String>();
	logger.logInfo(methodName +" Starts::::::::::::::::::::::");
	try {
 
		if(putControlValuesInView())
		{
		PropertyServiceAgent psa  =new PropertyServiceAgent();
		logger.logInfo(methodName +" AddRecordStarts::::::::::::::::::::::");
			psa.addFacility(facilityViewEdit);
		logger.logInfo(methodName +" AddRecordEnds::::::::::::::::::::::");
			
			successMessages.add( ResourceUtil.getInstance().getProperty("common.messages.Save"));
			eventOutCome = "success";
		}
		logger.logInfo(methodName +" Ends::::::::::::::::::::::");
	} catch (Exception ex) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.logInfo(methodName +" Crashed::::::::::::::::::::::");
		ex.printStackTrace();
	}

	return eventOutCome;

}

private boolean putControlValuesInView() throws ParseException {
	
	String methodName = "putControlValuesInView";
	
	
	
       boolean isSucceed=true;
       String message;
       errorMessages.clear();
       successMessages.clear();
       
       logger.logInfo(methodName +" Ends::::::::::::::::::::::");
    
   try{  
      if (pageMode.equals("EDIT")){
    	  facilityViewEdit.setFacilityId(new Long(facilityId));
       }
      
       if(facilityName!=null && !facilityName.equals("") ){
       	
    	   facilityViewEdit.setFacilityName(facilityName);
       }
       else
   	{
   		
   		message = ResourceUtil.getInstance().getProperty("facility.message.facilityName");
   		errorMessages.add(message);
   		isSucceed=false;
   		return isSucceed;
   	}
       
      if (facilityDescription != null && !facilityDescription .equals(""))
	    {
    	  facilityViewEdit.setDescription(facilityDescription );
       }
      else
     	{
     		
     		message = ResourceUtil.getInstance().getProperty("facility.message.discription");
     		errorMessages.add(message);
     		isSucceed=false;
     		return isSucceed;
     	}
       
       if (rentValue != null && !rentValue.equals("") )
      {
    	   facilityViewEdit.setRent(new Double(rentValue));
	   }
	
       if (selectOneFacilityType != null && !selectOneFacilityType.equals("")&& !selectOneFacilityType.equals("-1"))
       {
 		
 		DomainDataView ddv= getIdFromType( getDomainDataListForDomainType(WebConstants.FACILITY_TYPE),
 		       selectOneFacilityType
              );
 		facilityViewEdit.setFacilityTypeId(ddv.getDomainDataId());
 	   }
       
       if (selectOneFacilityType !=null && rentValue == null && selectOneFacilityType.equals(WebConstants.FACILITY_FACILITY_TYPE_PAID)){
    	   
    	   message = ResourceUtil.getInstance().getProperty("facility.message.facilityTypePaid");
    	   errorMessages.add(message);
    		isSucceed=false;
    		return isSucceed;
       }
    	   
       
       
       
       facilityViewEdit.setCreatedBy(getCreatedByUser(getLoggedInUser()));
       facilityViewEdit.setCreatedOn(getCreatedOnDate());
       facilityViewEdit.setUpdatedBy(getLoggedInUser());
       facilityViewEdit.setUpdatedOn(new Date());
       facilityViewEdit.setIsDeleted(new Long(0));
       facilityViewEdit.setRecordStatus(new Long(1));
       logger.logInfo(methodName +" Ends::::::::::::::::::::::");
   
   }catch (Exception e) {
	   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	   
	   logger.logInfo(methodName +" Crashed::::::::::::::::::::::");
	// TODO: handle exception
	   e.printStackTrace();
} 
       
      return isSucceed;
}

public String getFacilityName() {
	return facilityName;
}

public void setFacilityName(String facilityName) {
	this.facilityName = facilityName;
}

public String getFacilityDescription() {
	return facilityDescription;
}

public void setFacilityDescription(String facilityDescription) {
	this.facilityDescription = facilityDescription;
}

public String getSelectOneFacilityType() {
	return selectOneFacilityType;
}

public void setSelectOneFacilityType(String selectOneFacilityType) {
	this.selectOneFacilityType = selectOneFacilityType;
}

public String getFacilityId() {
	return facilityId;
}

public void setFacilityId(String facilityId) {
	this.facilityId = facilityId;
}

public FacilityView getFacilityView() {
	return facilityView;
}

public void setFacilityView(FacilityView facilityView) {
	this.facilityView = facilityView;
}

public FacilityView getFacilityViewEdit() {
	return facilityViewEdit;
}

public void setFacilityViewEdit(FacilityView facilityViewEdit) {
	this.facilityViewEdit = facilityViewEdit;
}
public Double getRentValue() {
	return rentValue;
}

public void setRentValue(Double rentValue) {
	this.rentValue = rentValue;
}

public boolean isPageModeAdd() {
	return isPageModeAdd;
}

public void setPageModeAdd(boolean isPageModeAdd) {
	this.isPageModeAdd = isPageModeAdd;
}

public HtmlInputText getCtlRentValue() {
	return ctlRentValue;
}

public void setCtlRentValue(HtmlInputText ctlRentValue) {
	this.ctlRentValue = ctlRentValue;
}

public String getPageMode() {
	return pageMode;
}

public void setPageMode(String pageMode) {
	this.pageMode = pageMode;
}

public Boolean getTxtBoxShowHide() {
	return txtBoxShowHide;
}

public void setTxtBoxShowHide(Boolean txtBoxShowHide) {
	this.txtBoxShowHide = txtBoxShowHide;
}

public String btnBack_Click(){
	
	return "BackScreen";
	

}
private String getLoggedInUser() {
	context = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession) context.getExternalContext()
			.getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
		UserDbImpl user = (UserDbImpl) session
				.getAttribute(WebConstants.USER_IN_SESSION);
		loggedInUser = user.getLoginId();
	}

	return loggedInUser;
}
public Date getCreatedOnDate( )throws Exception
{
	String methodName="getCreatedOnDate";
	SystemParameters parameters = SystemParameters.getInstance();
	
	DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	if(viewMap.containsKey("CreatedOn")
	  && viewMap.get("CreatedOn").toString()!=null 
	  && (viewMap.get("CreatedOn").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdOn.."+viewMap.get("CreatedOn").toString());
		return  df.parse(df.format(viewMap.get("CreatedOn")));
		
	}
	else
	{
		logger.logInfo(methodName+"|"+"createdOn.."+new Date());
		return  new Date();
	}
	 
	
}
public String getCreatedByUser(String loggedInUser)
{
	String methodName="getCreatedByUser";
	
  if(viewMap.containsKey("CreatedBy")
	 && viewMap.get("CreatedBy").toString()!=null 
	 && (viewMap.get("CreatedBy").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdBy.."+viewMap.get("CreatedBy").toString());
		return  viewMap.get("CreatedBy").toString();	
	}
  else
  {
		logger.logInfo(methodName+"|"+"createdBy.."+loggedInUser);
		return  loggedInUser;
  }
	 
	
}
}
