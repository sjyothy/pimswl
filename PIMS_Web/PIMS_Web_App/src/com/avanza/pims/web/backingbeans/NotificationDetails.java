package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.db.NotificationImpl;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationDistributor;
import com.avanza.notificationservice.notification.NotificationManager;
import com.avanza.notificationservice.notification.NotificationState;
import com.avanza.notificationservice.notification.PriorityType;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.PaymentManager;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.GenerateNotificationView;
import com.avanza.pims.ws.vo.NotificationView;
import com.avanza.ui.util.ResourceUtil;


public class NotificationDetails extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionUnitBackingBean.class);
	/**
	 * 
	 */
	
	public NotificationDetails(){
		
	}
	
	private String infoMessage = "";

	private NotificationView notificationView = new NotificationView();

	private List<String> errorMessages = new ArrayList<String>();
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	
	private String notificationTypeId;
	private String notificationSubject;
	private String notificationBody;
	private String screenModeValue;
	
	private String notificationId;
	
	
	private HtmlSelectOneMenu notificationTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlInputText subjectInputText = new HtmlInputText();
	private HtmlInputTextarea bodyInputText = new HtmlInputTextarea();
	
	private HtmlInputHidden screenMode = new HtmlInputHidden();
	private HtmlInputHidden notificationIdHidden = new HtmlInputHidden();
	
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton generateButton = new HtmlCommandButton();
	
	private List<GenerateNotificationView> generateList; 
	
//	private HtmlCommandButton generateButton = new HtmlCommandButton();
	
//	private Boolean selected = false;
	
	public String editDataItem(){
//		SelectItem temp = new SelectItem();
//		temp.getValue();
		return "edit";
	}
	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}

		return loggedInUser;
	}
	
	@SuppressWarnings("unchecked")
	public void generateNotification(){
		
		
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		
		PaymentManager paymentManager= new PaymentManager();
		
		logger.logInfo("generateNotification() started...");
		boolean isUpdated = false;
		List<GenerateNotificationView> selectedList = new ArrayList();
		
		List<Notification> transformedList = new ArrayList();
		int count = 0;
		
		try {
			
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();
			
			if( generateList == null){
				
				generateList = (List<GenerateNotificationView>) viewMap.get("generationList");
			}
			
			if(generateList != null){
				
				for(int i =0; i < generateList.size(); i++ ){
					
					if( generateList.get(i).getSelected()!= null && generateList.get(i).getSelected() ){
						
						selectedList.add( generateList.get(i) );
					}
				}
			}
			
			ChannelCatalog catalog = ChannelCatalog.getInstance();
			ProtocolAdapter protocolAdapter = catalog.getAdapter( notificationTypeId );
			
			//DataBroker notificationDB = DataRepository.getBroker(NotificationDistributor.class.getName());
			
			
			
			if(selectedList != null){
				
				for(int i =0; i < selectedList.size(); i++ ){
					
					 GenerateNotificationView gnView = selectedList.get(i); 
					 NotificationImpl notification = null;
					 notification = transformToNotification(gnView);
					 
					 transformedList.add( notification );
					 
					 if( notification != null ){
						 try{
							 
//							 NotificationManager nm = NotificationDistributor.getInstance();
//							 nm.notifyMessage(notification);
//		    				protocolAdapter.notify2(notification);
//		    				count ++;
		    				
						 }catch(Exception exception ){
							 logger.LogException("generateNotification() crashed ",exception);
						 }
		    				
		    			}
					 
					
					 
				}
				 transformedList = paymentManager.persistNotification(transformedList);
				 
				 for(int i =0; i < transformedList.size(); i++ ){
					 
					 NotificationImpl notificationImpl = (NotificationImpl) transformedList.get(i); 
					 protocolAdapter.notify(notificationImpl);
					 count ++;
					 
				 }
			}
			
			
			errorMessages = new ArrayList<String>();
			errorMessages.add(count + " message(s) processed successfully.");
					        
		}
    	catch(Exception exception){
    		logger.LogException("generateNotification() crashed ",exception);
    		errorMessages.add(count + " message(s) processed successfully.");
    	}
	}
	
	
	
	public NotificationImpl transformToNotification(GenerateNotificationView notificationView){
		
		NotificationImpl notification = new NotificationImpl();
		
		notification.setNotificationId( notificationView.getNotificationId() );
		notification.setServerInstanceId(notificationView.getServerInstanceId() );
		notification.setBody( notificationBody );
		notification.setChannelId( notificationTypeId );
		notification.setContent(null );
		notification.setCreatedBy( getLoggedInUser() );
		notification.setCreatedOn( new Date() );
		notification.setDeleted(new Boolean(false) );
		notification.setDocumentList( null );
		
		notification.setExternalId1( notificationView.getExternalId1().toString() );
		notification.setExternalId2( notificationView.getExternalId2() );
		notification.setExternalId3( notificationView.getExternalId3() );
		notification.setLocale( notificationView.getLocale() );
		
		// Default Values
		notification.setNsFrom( notificationView.getNsFrom() );
		notification.setPriority( PriorityType.Medium ) ;
		
		
		
		notification.setProtocolFields( null );
		notification.setRecipientBccList( null );
		notification.setRecipientCcList( null );
		notification.setRecipientToList( null );
		notification.setRecipientToList( notificationView.getEmailAddress() );
		
		notification.setRecordStatus( notificationView.getRecordStatus() );
		notification.setRetryCount(  notificationView.getRetryCount() );
		
		notification.setSentTime( new Date().toString() );
		
		notification.setNotificationState( NotificationState.New ) ;
		
		notification.setStatusText(  notificationView.getStatusText() );
		
		notification.setServerInstanceId( notificationView.getServerInstanceId() );
		
		notification.setSubject( notificationSubject );
		
		if( notificationView.getTimeOut() != null){
			notification.setTimeOut( notificationView.getTimeOut() );
		}
		else{
			notification.setTimeOut(60 );
		}
		
		notification.setUpdatedBy( getLoggedInUser() );
		notification.setUpdatedOn( new Date() );
		
		if( notificationView.getTimeOut() != null){
			notification.setWaitTime( notificationView.getWaitTime() );
		}
		else{
			notification.setWaitTime( 60 );
		}
		
		notification.setUpdation( getLoggedInUser() );

		return notification;
	}

	
	@SuppressWarnings("unchecked")
	public void saveNotification(){
		
		Long auctionUnitId = 0L;
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		
		PaymentManager paymentManager= new PaymentManager();
		
		logger.logInfo("saveNotification() started...");
		boolean isUpdated = false;
		try {
				
  			isUpdated = paymentManager.updateNotificationById(Long.valueOf( notificationId ), notificationBody , getLoggedInUser());
	  		
	        if( isUpdated ){
	        	//final String viewId = "/auctionUnitPriceEdit.jsp"; 
	        
  				FacesContext facesContext = FacesContext.getCurrentInstance();
  				String javaScriptText = "javascript:closeWindowSubmit();";
	
  				// Add the Javascript to the rendered page's header for immediate execution
//  				AddResource addResource = AddResourceFactory.getInstance(facesContext);
//  				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
  				errorMessages
				.add("Resord saved successfully.");
	        }
	        else{
	        	errorMessages = new ArrayList<String>();
				errorMessages
						.add("Resord not saved successfully.");
	        }
	        
		}
    	catch(PimsBusinessException exception){
    		logger.LogException("saveNotification() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("saveNotification() crashed ",exception);
    	}
	}

    public void cancel() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";
        
        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);         
    }
    
    public String clearSearch(){
    	
//    	auctionUnitView.setPropertyCommercialName("");
//    	auctionUnitView.setPropertyEndowedName("");
//    	auctionUnitView.setPropertyNumber("");
    	
//    	propertyTypeSelectMenu.setValue(propertyTypes.get(0).getValue());    	
//    	private HtmlSelectOneMenu propertyCategorySelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu propertyUsageSelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu investmentPurposeSelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu propertyStatusSelectMenu=
    	return "cleared";
    }
    
    public Boolean validateFields() {
    	Boolean validated = true;
		errorMessages = new ArrayList<String>();
		
//		if (propertyView.getPropertyNumber().lastIndexOf(" ")>=0) {
//		   errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Property.PROPERTY_NUMBER) + " " + getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.SHOULD_NOT_HAVE_SPACE));
//		   validated = false;
//		}
//		propertyView.setCommercialName(propertyView.getCommercialName().trim());
//		propertyView.setEndowedName(propertyView.getEndowedName().trim());
		return validated;
	}
    
    public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
    
    
    // Helpers -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	
	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	
	@Override
	public void init() {
		
		super.init();
		
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		
		if(!isPostBack())
		{
			
			notificationView = (NotificationView) sessionMap.get( WebConstants.NOTIFICATION_VIEW_OBJECT );
			screenModeValue = (String) sessionMap.get( WebConstants.NOTIFICATION_SCREEN_MODE );
			
			notificationTypeSelectMenu.setReadonly(true);
			subjectInputText.setReadonly(true);
			
			if( screenModeValue != null && screenModeValue.equals("VIEW")  ){
				
				bodyInputText.setReadonly(true);
				saveButton.setRendered(false);
				generateButton.setRendered(false);
				
			}
			else if( screenModeValue != null && screenModeValue.equals("EDIT")  ){
				
				bodyInputText.setReadonly(false);
				saveButton.setRendered(true);
				generateButton.setRendered(false);
				
			}
			else if( screenModeValue != null && screenModeValue.equals("ADD")  ){
				
				bodyInputText.setReadonly(false);
				notificationTypeSelectMenu.setReadonly(false);
				subjectInputText.setReadonly(false);
				saveButton.setRendered(false);
				generateButton.setRendered(true);
				
				Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
				
				generateList = (List) sessionMap.get("dataListForGeneration");
				sessionMap.remove("dataListForGeneration");
				
				
				viewMap.put("generationList",generateList);
				
			}
			else{
				bodyInputText.setReadonly(true);
				saveButton.setRendered(false);
				generateButton.setRendered(false);
			}
			
			if( notificationView != null ){
			
				notificationTypeId = notificationView.getChannelId();
				notificationSubject = notificationView.getSubject();
				notificationBody = notificationView.getBody();
				notificationId = notificationView.getNotificationId().toString();
			}
			
			sessionMap.remove( WebConstants.NOTIFICATION_VIEW_OBJECT );
			sessionMap.remove( WebConstants.NOTIFICATION_SCREEN_MODE );
			
		}
		else{
			
		}
		
		
		System.out.println("NotificationDetails init");
		try
		{
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);

	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public NotificationView getNotificationView() {
		return notificationView;
	}

	public void setNotificationView(NotificationView notificationView) {
		this.notificationView = notificationView;
	}

	public String getNotificationTypeId() {
		return notificationTypeId;
	}

	public void setNotificationTypeId(String notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}

	public String getNotificationSubject() {
		return notificationSubject;
	}

	public void setNotificationSubject(String notificationSubject) {
		this.notificationSubject = notificationSubject;
	}

	public String getNotificationBody() {
		return notificationBody;
	}

	public void setNotificationBody(String notificationBody) {
		this.notificationBody = notificationBody;
	}

	public String getScreenModeValue() {
		return screenModeValue;
	}

	public void setScreenModeValue(String screenModeValue) {
		this.screenModeValue = screenModeValue;
	}

	public HtmlSelectOneMenu getNotificationTypeSelectMenu() {
		return notificationTypeSelectMenu;
	}

	public void setNotificationTypeSelectMenu(
			HtmlSelectOneMenu notificationTypeSelectMenu) {
		this.notificationTypeSelectMenu = notificationTypeSelectMenu;
	}

	public HtmlInputText getSubjectInputText() {
		return subjectInputText;
	}

	public void setSubjectInputText(HtmlInputText subjectInputText) {
		this.subjectInputText = subjectInputText;
	}

	public HtmlInputTextarea getBodyInputText() {
		return bodyInputText;
	}

	public void setBodyInputText(HtmlInputTextarea bodyInputText) {
		this.bodyInputText = bodyInputText;
	}

	public HtmlInputHidden getScreenMode() {
		return screenMode;
	}

	public void setScreenMode(HtmlInputHidden screenMode) {
		this.screenMode = screenMode;
	}

	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}

	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}

	public HtmlCommandButton getGenerateButton() {
		return generateButton;
	}

	public void setGenerateButton(HtmlCommandButton generateButton) {
		this.generateButton = generateButton;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public HtmlInputHidden getNotificationIdHidden() {
		return notificationIdHidden;
	}

	public void setNotificationIdHidden(HtmlInputHidden notificationIdHidden) {
		this.notificationIdHidden = notificationIdHidden;
	}
}