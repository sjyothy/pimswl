/* BouncedChequesListBackingBean.java
 * Creation Date: 20 Mar 2009
 * Author: Asif Iqbal
 * Description: Backing Bean for BouncedChequesList.jsp
 */

package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.FinanceServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.BouncedChequeView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.ui.util.ResourceUtil;


public class BouncedChequesListBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionUnitBackingBean.class);
	/**
	 * 
	 */
	
	public BouncedChequesListBackingBean(){
		
	}
	
//	
//	private static final long serialVersionUID = 1L;
//
	private BouncedChequeView dataItem = new BouncedChequeView();
	private List<BouncedChequeView> dataList = new ArrayList<BouncedChequeView>();

	private List<String> errorMessages = new ArrayList<String>();
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	
	private String reasons = "";
	
	private HtmlDataTable dataTable;
	
	public List<BouncedChequeView> getBouncedChequesList(List<Long> bouncedChequesList){
		logger.logInfo("getBouncedChequesList() started...");
		List<BouncedChequeView> bouncedChequeViewList = null;
    	try{
    		FinanceServiceAgent financeServiceAgent = new FinanceServiceAgent();
    		bouncedChequeViewList = financeServiceAgent.getBouncedChequesList(bouncedChequesList);
    		logger.logInfo("getBouncedChequesList() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getBouncedChequesList() crashed ",exception);
    	}
    	return bouncedChequeViewList;
	}
	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
    
    public void btnCancel(ActionEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";
        
        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);         
    }
    
    public void btnSave(ActionEvent event) {

    	try {
        	logger.logInfo("btnSave started...");
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			List<Long> selectedBoucnedChequesList = (List<Long>)sessionMap.get("SELECTED_BOUNCED_CHEQUES");
	    	
			FinanceServiceAgent financeAgent = new FinanceServiceAgent();
			financeAgent.UpdateChequeStatusAsBounced(selectedBoucnedChequesList, reasons);
	    	
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "window.close();";
	        
	        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
	        
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        	logger.logInfo("btnSave ended...");
    	}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}        
    }    
    
    public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
    
	/**
	 * @return the dataItem
	 */
	public BouncedChequeView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(BouncedChequeView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<BouncedChequeView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<BouncedChequeView> dataList) {
		this.dataList = dataList;
	}


	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if(!isPostBack())
		{
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			List<Long> selectedBoucnedChequesList = (List<Long>)sessionMap.get("SELECTED_BOUNCED_CHEQUES");
			dataList = getBouncedChequesList(selectedBoucnedChequesList);
		}
		System.out.println("AuctionDetailsBackingBean init");
		try
		{
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);

	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}
}