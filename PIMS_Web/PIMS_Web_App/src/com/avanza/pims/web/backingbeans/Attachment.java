package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.avanza.pims.web.controller.AbstractController;

public class Attachment extends AbstractController {
	
	private List<UploadedFile> fileList = new ArrayList<UploadedFile>();
	private HtmlInputFileUpload fileUpload;
	private HtmlDataTable dataTable = new HtmlDataTable();
	private UploadedFile file;
	
	public void uploadAction( /*ActionEvent event*/ ) {
		System.out.println("uploadAction");
		this.toString();
		if( file!=null ) {
			fileList.add(file);
		}
		file = null;
	}
	
	public void removeFile( ActionEvent event ) {
		event.getSource();
		int i = dataTable.getRowIndex();
		fileList.remove(i);		
	}

	public HtmlInputFileUpload getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(HtmlInputFileUpload fileUpload) {
		this.fileUpload = fileUpload;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public List<UploadedFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<UploadedFile> fileList) {
		this.fileList = fileList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

}
