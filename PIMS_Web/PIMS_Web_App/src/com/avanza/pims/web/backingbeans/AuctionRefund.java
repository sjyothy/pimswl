package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionVenueView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.RequestView;

public class AuctionRefund extends AbstractController{
	private transient Logger logger = Logger.getLogger(AuctionRefund.class);
	private HtmlInputText socialSecurityNoText = new HtmlInputText();
	private HtmlInputText passportText = new HtmlInputText();
	private HtmlInputText bidderFirstNameText = new HtmlInputText();
	private HtmlSelectOneMenu auctionMenu = new HtmlSelectOneMenu();
	private List<SelectItem> auctions = new ArrayList();
	private HtmlInputText bidderLastNameText = new HtmlInputText();
	private HtmlInputText totalAcutionAmount = new HtmlInputText();
	private HtmlCommandButton buttonSearchBidder = new HtmlCommandButton();
	private HtmlCommandButton buttonAuctionSearch = new HtmlCommandButton();
	private HtmlDataTable bidderTable = new HtmlDataTable();
	private List<BidderView> bidderList = new ArrayList();
	private List<BidderAuctionRegView> requestViewList = new ArrayList();
	private HtmlInputText totalDepositAmount =new HtmlInputText();
	private HtmlInputText totalRefundAmount = new HtmlInputText();
	private HtmlInputText auctionDateText = new HtmlInputText();
	private HtmlInputText auctionVenueText = new HtmlInputText();
	
	private HtmlInputText auctionTitleText = new HtmlInputText();
	private HtmlInputText auctionNumberText = new HtmlInputText();
	private BidderAuctionRegView requestView = new BidderAuctionRegView();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	SystemParameters parameters = SystemParameters.getInstance();
	DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));	
	//added for richfaces calendar
	private Date auctionDate;

	
	
	

	private Boolean enableAuctionPanel = false;
	PropertyServiceAgent pSAgent = new PropertyServiceAgent();
	public void init(){
		super.init();
		/*if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.REQUEST_VIEW_LIST)!=null)
			requestViewList = (List<BidderAuctionRegView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.REQUEST_VIEW_LIST);
*/loadRequestList();	
	}
	public void preprocess(){
		super.preprocess();
		if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_VENUE_NAME)!=null){
			auctionVenueText.setValue(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_VENUE_NAME));
			//getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AUCTION_VENUE_NAME,"");
			getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.AUCTION_VENUE_NAME);
		}
	}
	public void prerender(){
		super.prerender();
	}

	public List<BidderView> getBidderDataList() {
		if (bidderList == null) {
			loadRequestList();
		}
		return bidderList;
	}
	public String search(){
		String  methodName = "search";
		logger.logInfo(methodName+" started...");
	
			loadRequestList();
		logger.logInfo(methodName+" ended...");
		return "";
	}	
	private void loadAuctionsList(){
		try{
			List<AuctionView> auctionViewList = new ArrayList();
			auctions= new ArrayList();
			Map auctionViewMap =new HashMap();
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.REQUEST_VIEW_LIST)){
			 List<BidderAuctionRegView> requestViewList = (List<BidderAuctionRegView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.REQUEST_VIEW_LIST);
			 for(int index = 0;index<requestViewList.size();index++){
				// String strAuctionNumber = requestViewList.get(index).getAuctionView().getAuctionNumber();
			//	 auctionViewMap.put(strAuctionNumber, strAuctionNumber);
			 }
			
			
			 Iterator it = auctionViewMap.values().iterator();
			 while(it.hasNext()){
				 String auctionNumber = (String)it.next();
					auctions.add(new SelectItem(auctionNumber,auctionNumber));				
			 }
		
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AUCTION_NUMBER_LIST, auctions);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	public void selectVenue(javax.faces.event.ActionEvent event){
		logger.logInfo("selectVenue() started...");
		try
		{
	        final String viewId = "/RefundAuction.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String javaScriptText = "javascript:showVenuePopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("selectVenue() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("selectVenue() crashed ", exception);
		}
    }
	
	
	private void loadRequestList(){
		String  methodName = "loadDataList";
		logger.logInfo(methodName+" started...");

		try {
			BidderAuctionRegView requestView = new BidderAuctionRegView();
			BidderView bidderView = new BidderView();
			AuctionView auctionView = new AuctionView();
			AuctionVenueView auctionVenueView = new AuctionVenueView();
			if(bidderFirstNameText.getValue() != null && 
					bidderFirstNameText.getValue().toString().trim().length()>0)
				bidderView.setFirstName(bidderFirstNameText.getValue().toString());
			if(bidderLastNameText.getValue()!= null 
					&& bidderLastNameText.getValue().toString().trim().length()>0)
				bidderView.setLastName(bidderLastNameText.getValue().toString());
			if(socialSecurityNoText.getValue() != null && socialSecurityNoText.getValue().toString().trim().length()>0)
				bidderView.setSocialSecNumber(socialSecurityNoText.getValue().toString());
			if( passportText.getValue() != null && passportText.getValue().toString().trim().length()>0)
				bidderView.setPassportNumber(passportText.getValue().toString());
			//added for richfaces calendar
			if(auctionDate != null && auctionDate.toString().trim().length()>0){
				auctionView.setAuctionDate(df.parse(getStringFromDate(auctionDate)).toString());
				auctionView.setAuctionStartDatetime(df.parse(getStringFromDate(auctionDate)));				
			}
//			if(auctionDateText.getValue()!=null && 
//					auctionDateText.getValue().toString().trim().length()>0){
//				auctionView.setAuctionDate(df.parse(auctionDateText.getValue().toString()).toString());
//			}
			if(auctionNumberText.getValue()!=null &&
					auctionNumberText.getValue().toString().trim().length()>0){
				auctionView.setAuctionNumber(auctionNumberText.getValue().toString());
			}
			if(auctionTitleText.getValue()!=null&&
					auctionTitleText.getValue().toString().trim().length()>0){
				auctionView.setAuctionTitle(auctionTitleText.getValue().toString());
			}
			if(auctionVenueText.getValue()!=null&&
					auctionVenueText.getValue().toString().trim().length()>0){
				auctionVenueView.setVenueName(auctionVenueText.getValue().toString());
				auctionVenueView.setVenueId(Long.parseLong(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_VENUE_ID).toString()));
				auctionView.setAuctionVenue(auctionVenueView);
			}
			//requestView.setBidderView(bidderView);
			requestView.setAuctionView(auctionView);
			
			requestViewList = pSAgent.getRequestWithAuctionAndBidderView(requestView);
			
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.REQUEST_VIEW_LIST,requestViewList);
			
			
			logger.logInfo(methodName+" completed successfully...");
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}
		logger.logInfo(methodName+" ended...");
	}
	
  
	public String editDataItem() {
		requestView = (BidderAuctionRegView) bidderTable.getRowData();
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TOTAL_DEPOSIT_AMOUNT, requestView.getTotalDepositAmount());
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.REFUND_AMOUNT, requestView.getTotalDepositAmount()-requestView.getConfiscatedAmount());


		
		return "edit";


	}

	public List<BidderAuctionRegView> getRequestsList() 
	{
		if(isPostBack()){
		if ((requestViewList == null || requestViewList.size()==0 )) {
			
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.RECEIVE_PAYMENT_TRANSACTION_GRID, requestViewList);
		}
	}

		return requestViewList;
	}
	public String refund(){
		
		requestView = (BidderAuctionRegView) bidderTable.getRowData();
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AUCTION_REG_VIEW, requestView);
//		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.REFUND_AMOUNT, requestView.getTotalDepositAmount()-requestView.getConfiscatedAmount());


		return "refund";
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
    ///////////added for richfaces calender
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	public Integer getRecordSize(){
		if(requestViewList != null){
			return requestViewList.size(); 
		}
		else{
			return 0;
		}
	}
	
	public Date getAuctionDate() {
		return auctionDate;
	}
	public void setAuctionDate(Date auctionDate) {
		this.auctionDate = auctionDate;
	}
	///////////added for richfaces calender
	public HtmlInputText getAuctionDateText() {
		return auctionDateText;
	}
	public void setAuctionDateText(HtmlInputText auctionDateText) {
		this.auctionDateText = auctionDateText;
	}
	public HtmlInputText getAuctionNumberText() {
		return auctionNumberText;
	}
	public void setAuctionNumberText(HtmlInputText auctionNumberText) {
		this.auctionNumberText = auctionNumberText;
	}
	public HtmlInputText getAuctionTitleText() {
		return auctionTitleText;
	}
	public void setAuctionTitleText(HtmlInputText auctionTitleText) {
		this.auctionTitleText = auctionTitleText;
	}
	public HtmlInputText getAuctionVenueText() {
		return auctionVenueText;
	}
	public void setAuctionVenueText(HtmlInputText auctionVenueText) {
		this.auctionVenueText = auctionVenueText;
	}
	public HtmlInputText getPassportText(){
		return passportText;
	}
	public void setPassportText(HtmlInputText passportText){
		this.passportText = passportText;
	}

	public HtmlInputText getBidderFirstNameText() {
		return bidderFirstNameText;
	}
	public void setBidderFirstNameText(HtmlInputText bidderFirstNameText) {
		this.bidderFirstNameText = bidderFirstNameText;
	}
	public List<BidderView> getBidderList() {
		return bidderList;
	}
	public void setBidderList(List<BidderView> bidderList) {
		this.bidderList = bidderList;
	}

	public HtmlDataTable getBidderTable() {
		return bidderTable;
	}
	public void setBidderTable(HtmlDataTable bidderTable) {
		this.bidderTable = bidderTable;
	}
	public HtmlCommandButton getButtonAuctionSearch() {
		return buttonAuctionSearch;
	}
	public void setButtonAuctionSearch(HtmlCommandButton buttonAuctionSearch) {
		this.buttonAuctionSearch = buttonAuctionSearch;
	}
	public HtmlCommandButton getButtonSearchBidder() {
		return buttonSearchBidder;
	}
	public void setButtonSearchBidder(HtmlCommandButton buttonSearchBidder) {
		this.buttonSearchBidder = buttonSearchBidder;
	}
	public HtmlInputText getSocialSecurityNoText() {
		return socialSecurityNoText;
	}
	public void setSocialSecurityNoText(HtmlInputText socialSecurityNoText) {
		this.socialSecurityNoText = socialSecurityNoText;
	}
	public HtmlInputText getTotalAcutionAmount() {
		return totalAcutionAmount;
	}
	public void setTotalAcutionAmount(HtmlInputText totalAcutionAmount) {
		this.totalAcutionAmount = totalAcutionAmount;
	}
	public HtmlInputText getTotalDepositAmount() {
		return totalDepositAmount;
	}
	public void setTotalDepositAmount(HtmlInputText totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}
	public HtmlInputText getTotalRefundAmount() {
		return totalRefundAmount;
	}
	public void setTotalRefundAmount(HtmlInputText totalRefundAmount) {
		this.totalRefundAmount = totalRefundAmount;
	}
	public HtmlInputText getBidderLastNameText() {
		return bidderLastNameText;
	}
	public void setBidderLastNameText(HtmlInputText bidderLastNameText) {
		this.bidderLastNameText = bidderLastNameText;
	}
	public Boolean getEnableAuctionPanel() {
		return enableAuctionPanel;
	}
	public void setEnableAuctionPanel(Boolean enableAuctionPanel) {
		this.enableAuctionPanel = enableAuctionPanel;
	}
	public HtmlSelectOneMenu getAuctionMenu() {
		return auctionMenu;
	}
	public void setAuctionMenu(HtmlSelectOneMenu auctionMenu) {
		this.auctionMenu = auctionMenu;
	}
	public List<SelectItem> getAuctions() {
		return auctions;
	}
	public void setAuctions(List<SelectItem> auctions) {
		this.auctions = auctions;
	}


}
