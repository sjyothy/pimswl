package com.avanza.pims.web.backingbeans;
import com.avanza.pims.report.criteria.PropertiesListReportCriteria;
import com.avanza.pims.web.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.RegionView;

public class PropertiesListBackingBean extends AbstractController
{	

	private static final long serialVersionUID = 3124991600424588667L;
	private PropertiesListReportCriteria propertiesListReportCriteria;
	private Logger logger;
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public PropertiesListBackingBean() 
	{
		logger = Logger.getLogger(PropertiesListBackingBean.class);
		if(CommonUtil.getIsEnglishLocale())
			propertiesListReportCriteria = new PropertiesListReportCriteria(ReportConstant.Report.PROPERTIES_LIST_EN, ReportConstant.Processor.PROPERTIES_LIST,CommonUtil.getLoggedInUser());
		else
			propertiesListReportCriteria = new PropertiesListReportCriteria(ReportConstant.Report.PROPERTIES_LIST_AR, ReportConstant.Processor.PROPERTIES_LIST,CommonUtil.getLoggedInUser());
		
	}

	public PropertiesListReportCriteria getPropertiesListReportCriteria() {
		return propertiesListReportCriteria;
	}

	public void setPropertiesListReportCriteria(
			PropertiesListReportCriteria propertiesListReportCriteria) {
		this.propertiesListReportCriteria = propertiesListReportCriteria;
	}


	
	
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertiesListReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	
	
	/////////////
	public void init() {
		super.init();
			//loadCountry();
			loadState();
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
//	    		if(request.getParameter("pageMode")!=null&&
//	    				request.getParameter("pageMode").equals(MODE_SELECT_ONE_POPUP)) 
//      	    		viewMap.put("pageMode",MODE_SELECT_ONE_POPUP );
//      	    	else if(request.getParameter("pageMode")!=null&&
//      	    			request.getParameter("pageMode").equals(MODE_SELECT_MANY_POPUP))
//      	    		viewMap.put("pageMode",MODE_SELECT_MANY_POPUP );
//	    		if(viewMap.get("pageMode")==null)
//	    			viewMap.put("pageMode","fullScreenMode");
			}
	}
	
	
/*	public void loadCountry()  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      
			
		      this.getCountryList().add(item);
		  }
		
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("country", countryList);
		
		}catch (Exception e){
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}
	
*/	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void loadState(/*ValueChangeEvent vce*/)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "UAE";
		/*for (int i=0;i<this.getCountryList().size();i++)
		{
			if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
			{
				selectedCountryName = 	this.getCountryList().get(i).getLabel();
			}
		}	*/
		
		List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}

	/////////////
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
}
