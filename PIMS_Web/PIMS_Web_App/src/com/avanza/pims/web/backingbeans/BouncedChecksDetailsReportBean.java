package com.avanza.pims.web.backingbeans;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.BouncedChecksDetailsReportCriteria;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;

public class BouncedChecksDetailsReportBean extends AbstractController{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1947259251426018838L;
	private BouncedChecksDetailsReportCriteria reportCriteria; 
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	public BouncedChecksDetailsReportBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new BouncedChecksDetailsReportCriteria(ReportConstant.Report.BOUNCED_CHECKS_DETAILS_EN, ReportConstant.Processor.BOUNCED_CHECKS_DETAILS, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new BouncedChecksDetailsReportCriteria(ReportConstant.Report.BOUNCED_CHECKS_DETAILS_AR, ReportConstant.Processor.BOUNCED_CHECKS_DETAILS, CommonUtil.getLoggedInUser());
	}
   public List<SelectItem> getPaymentScheduleStatusList()
   {
	   List<SelectItem> statusList = new ArrayList<SelectItem>( 0 );
	   if( viewMap.get("PS_STATUS_LIST")==null )
	   {
	   DomainDataView bounced =CommonUtil.getIdFromType( getAllPsStatus(),WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
	   DomainDataView bouncedReturned =CommonUtil.getIdFromType( getAllPsStatus(),WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_RETURN);
	   SelectItem item1= new SelectItem( bounced.getDomainDataId().toString(),
			               (CommonUtil.getIsEnglishLocale()?bounced.getDataDescEn():bounced.getDataDescAr() ));
	   statusList.add(item1);
	   SelectItem item2= new SelectItem( bouncedReturned.getDomainDataId().toString(),
               (CommonUtil.getIsEnglishLocale()?bouncedReturned.getDataDescEn():bouncedReturned.getDataDescAr() ));
	   statusList.add(item2);
	   }
	   else
		   statusList = (ArrayList<SelectItem> )viewMap.get("PS_STATUS_LIST");
	   return statusList;
   }
   @SuppressWarnings( "unchecked" )
   public List<DomainDataView> getAllPsStatus()
   {
		   
		   if( viewMap.get( WebConstants.PAYMENT_SCHEDULE_STATUS ) == null )
			   viewMap.put( WebConstants.PAYMENT_SCHEDULE_STATUS ,CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS));
			   
		   return (List<DomainDataView>)viewMap.get( WebConstants.PAYMENT_SCHEDULE_STATUS ) ;
			   
	   
   }
	public BouncedChecksDetailsReportCriteria getReportCriteria() {
		return reportCriteria;
	}
	public void setReportCriteria(BouncedChecksDetailsReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}
	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}
	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	 private static Logger logger = Logger.getLogger("BouncedChecksDetailsReportBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

	public String btnPrint_Click() {	
		
		    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
				return "";
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
}
