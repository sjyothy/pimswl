package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.study.StudySearchBacking;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.UnitView;

public class InquiryMatchedPropertiesBacking extends AbstractController 
{	
	private static final long serialVersionUID = 2138698495895273959L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String, Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private InquiryView inquiryView;
	private List<UnitView> matchedUnitViewList;
	private PropertyServiceAgent propertyServiceAgent;
	
	public InquiryMatchedPropertiesBacking() 
	{
		logger = Logger.getLogger(StudySearchBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		inquiryView = new InquiryView();
		matchedUnitViewList = new ArrayList<UnitView>(0);
		propertyServiceAgent = new PropertyServiceAgent();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@Override
	public void preprocess() {		
		super.preprocess();
	}
	
	@Override
	public void prerender() {		
		super.prerender();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// INQUIRY VIEW
		if ( sessionMap.get( WebConstants.INQUIRY ) != null )
		{
			inquiryView = (InquiryView) sessionMap.get( WebConstants.INQUIRY );
			sessionMap.remove( WebConstants.INQUIRY  );
		}
		else if ( viewMap.get( WebConstants.INQUIRY ) != null )
		{
			inquiryView = (InquiryView) viewMap.get( WebConstants.INQUIRY );
		}
		
		// MATCHED UNIT VIEW LIST
		if ( viewMap.get( WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST ) != null )
		{
			matchedUnitViewList = (List<UnitView>) viewMap.get( WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST );
		}
		else if ( inquiryView != null )
		{
			matchedUnitViewList = inquiryView.getUnitView();			
			
			// check if inquiry units is zero the inquiry must be criteria based inquiry
			if ( matchedUnitViewList.isEmpty() )
			{
				matchedUnitViewList = propertyServiceAgent.getPropertyUnitsByCriteria( getUnitSearchCriteriaMap( inquiryView ) );
			}			
		}
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// INQUIRY VIEW
		if ( inquiryView != null )
		{
			viewMap.put( WebConstants.INQUIRY, inquiryView );
		}
		
		// MATCHED UNIT VIEW LIST
		if ( matchedUnitViewList != null )
		{
			viewMap.put( WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST, matchedUnitViewList );
		}
	}
	
	private HashMap<String, Object> getUnitSearchCriteriaMap( InquiryView inquiryView ) 
	{
		HashMap<String, Object> unitSearchCriteriaHashMap = new HashMap<String, Object>(0);
		
		if ( inquiryView.getUnitType() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.UNIT_TYPE, String.valueOf( inquiryView.getUnitType() ) );
		
		if ( inquiryView.getUnitUsageTypeId() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.UNIT_USAGE, String.valueOf( inquiryView.getUnitUsageTypeId() ) );
		
		if ( inquiryView.getCountryId() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.EMIRATE, String.valueOf( inquiryView.getCountryId() ) );
		
		if ( inquiryView.getUnitInvestmentTypeId() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.INVESTMENT_TYPE, String.valueOf( inquiryView.getUnitInvestmentTypeId() ) );
		
		if ( inquiryView.getUnitSideTypeId() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.UNIT_SIDE, String.valueOf( inquiryView.getUnitSideTypeId() ) );
		
		if ( inquiryView.getRentValueMin() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.FROM_RENT_AMOUNT, String.valueOf( inquiryView.getRentValueMin() ) );
		
		if ( inquiryView.getRentValueMax() != null )
			unitSearchCriteriaHashMap.put( WebConstants.UNIT_SEARCH_CRITERIA.TO_RENT_AMOUNT, String.valueOf( inquiryView.getRentValueMax() ) );
		
		return unitSearchCriteriaHashMap;
	}
	
	public String btnCancel_Click() 
	{
		return null;
	}
	
	public String btnSendNotification_Click() 
	{
		System.out.println( matchedUnitViewList.size() );
		return null;
	}
	
	public Integer getPaginatorRows() 
	{		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() 
	{
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() 
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public Integer getRecordSize() 
	{
		return matchedUnitViewList.size();
	}
	
	public String getDateFormat()
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return CommonUtil.getTimeZone();
	}
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public InquiryView getInquiryView() {
		return inquiryView;
	}

	public void setInquiryView(InquiryView inquiryView) {
		this.inquiryView = inquiryView;
	}

	public List<UnitView> getMatchedUnitViewList() {
		return matchedUnitViewList;
	}

	public void setMatchedUnitViewList(List<UnitView> matchedUnitViewList) {
		this.matchedUnitViewList = matchedUnitViewList;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}
}
