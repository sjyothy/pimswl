package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.ajax4jsf.component.html.HtmlAjaxCommandButton;
import org.ajax4jsf.component.html.HtmlAjaxCommandLink;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.proxy.pimsinspectionbpelproxy.PIMSInspectionBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ComplaintDetailsView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;

public class inspectionDetailsBean extends AbstractController {

	private String pageMode = "";
	private String PAGE_MODE_ADD = "ADD";
	private String PAGE_MODE_UPDATE = "UPDATE";
	private String PAGE_MODE_VIEW = "VIEW";
	private String PAGE_MODE_TASK_VIOLATION_ACTION = "PAGE_MODE_TASK_VIOLATION_ACTION";
	private String PAGE_MODE_TASK_VIOLATION_CLOSE = "PAGE_MODE_TASK_VIOLATION_CLOSE";
	

	private String selectedStatus;
	private String inspectionDate;
	private String inspectionId;
	private String createdOn;
	private String createdBy;
	private String teamName;
	private String teamId;
	private String txtinspectionNum;
	
	List<SelectItem> inspectionTeamList = new ArrayList();
	List<SelectItem> inspectionTypeList = new ArrayList();
	List selectedInspectionTeamList = new ArrayList();
	List<SelectItem> usersInspectionList = new ArrayList();
	List selectedUsersInspectionList = new ArrayList();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	List inspectionUnitsDataList = new ArrayList();
	private HtmlDataTable tbl_InspectionUnits;
	InspectionUnitView dataItem = new InspectionUnitView();
	String dateFormat = "";
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger
			.getLogger(inspectionDetailsBean.class);
	HtmlCommandLink violationLink = new HtmlCommandLink();
	HtmlAjaxCommandLink cmdDelUnit = new HtmlAjaxCommandLink();
	HtmlCommandLink deleteLink = new HtmlCommandLink();

	HtmlAjaxCommandButton cmdAddTeamMembers = new HtmlAjaxCommandButton();
	HtmlAjaxCommandButton cmdRemoveTeamMembers = new HtmlAjaxCommandButton();
	org.richfaces.component.html.HtmlCalendar calendarInspectionFrom = new org.richfaces.component.html.HtmlCalendar();
	InspectionView inspectionView = new InspectionView();
	private HtmlCommandButton btnAdd = new HtmlCommandButton();
	private HtmlCommandButton btnAddUnits = new HtmlCommandButton();
	private HtmlCommandButton btnClear = new HtmlCommandButton();
	private HtmlCommandButton addViolationTeam = new HtmlCommandButton();
	private HtmlCommandButton addViolationBtn = new HtmlCommandButton();
	private HtmlCommandButton btnAddTeamMembers = new HtmlCommandButton();
	
	private HtmlCommandButton assignViolationActions  =new HtmlCommandButton();
	private HtmlCommandButton sendForRecommendation  =new HtmlCommandButton();
	private HtmlCommandButton sendForClosingViolation  =new HtmlCommandButton();
	private HtmlCommandButton sendForViolationAction  =new HtmlCommandButton();
	private HtmlCommandButton closeViolation  =new HtmlCommandButton();
	
	private HtmlCommandButton send = new HtmlCommandButton();
	
	
	
	HtmlSelectOneMenu comboInspectionType = new HtmlSelectOneMenu();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	
	private Integer recordSize = 0;
	private Integer receivingTeamRecordSize = 0;
	private Integer violationRecordSize = 0;
	
	HtmlCommandLink cmdLnkViolationActions = new HtmlCommandLink();
	HtmlCommandLink cmdLnkDelViolation = new HtmlCommandLink();
	HtmlCommandLink cmdLnkEditViolation = new HtmlCommandLink();
	HtmlCommandLink cmdLnkCloseViolation = new HtmlCommandLink();
	HtmlInputTextarea inspectionDescription = new HtmlInputTextarea();
	
	private Boolean actionColume ;
	private Boolean actionColumeForVoilation ;
	
	UserTask userTask;
	
	private HtmlDataTable dataTableViolation = new HtmlDataTable();
	private HtmlTab violationTab = new HtmlTab();


	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();
	// Constructors

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();


	// Receiving Team
	private HtmlDataTable inspectionTeamGrid = new HtmlDataTable();
	
	
	private UserView dataitemTeam = new UserView();
	private List<ViolationView> violationViewDataList = new ArrayList<ViolationView>();
	private Boolean canChange = new Boolean(false);
	private Boolean canAddVlAction = new Boolean(false);
	private Boolean canInitiateTask = new Boolean(false);
	private Boolean showSendToRentComittee = new Boolean(false);
	private Boolean showCompleteInspection = new Boolean(false);
	PropertyServiceAgent psa = new PropertyServiceAgent();
	CommonUtil commUtil = new CommonUtil();
	ServletContext servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();

	@SuppressWarnings("unchecked")
	public Boolean getCanInitiateTask() 
	{
		if (viewRootMap.containsKey("canInitiateTask") && viewRootMap.get("canInitiateTask") != null)
		{
			
			canInitiateTask = (Boolean) viewRootMap.get("canInitiateTask");
		}
		
		return canInitiateTask;
	}

	@SuppressWarnings("unchecked")
	public void setCanInitiateTask(Boolean canInitiateTask) {
		this.canInitiateTask = canInitiateTask;
		
		if(canInitiateTask != null ){
			viewRootMap.put("canInitiateTask", canInitiateTask);
		}
	}
	@SuppressWarnings("unchecked")
	public Boolean getShowCompleteInspection() {
		if (viewRootMap.containsKey(
		"showCompleteInspection") && viewRootMap.get("showCompleteInspection") != null){
			
			showCompleteInspection = (Boolean) viewRootMap
			.get("showCompleteInspection");
		}
		return showCompleteInspection;
	}

	@SuppressWarnings("unchecked")
	public void setShowCompleteInspection(Boolean showCompleteInspection) {
		this.showCompleteInspection = showCompleteInspection;
		
		if(showCompleteInspection != null ){
			viewRootMap.put("showCompleteInspection", showCompleteInspection);
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean getShowSendToRentComittee() {
		showSendToRentComittee = true;
		if (viewRootMap.containsKey(
		"showSendToRentComittee") && viewRootMap.get("showSendToRentComittee") != null){
			
			showSendToRentComittee = (Boolean) viewRootMap
			.get("showSendToRentComittee");
		}
		return showSendToRentComittee;
	}

	@SuppressWarnings("unchecked")
	public void setShowSendToRentComittee(Boolean showSendToRentComittee) {
		this.showSendToRentComittee = showSendToRentComittee;
			
		if(showSendToRentComittee != null ){
			viewRootMap.put("showSendToRentComittee", showSendToRentComittee);
		}
	}
	@SuppressWarnings("unchecked")
	public Boolean getCanAddVlAction() {
		
		if (viewRootMap.containsKey(
		"canAddVlAction") && viewRootMap.get("canAddVlAction") != null){
			
			canAddVlAction = (Boolean) viewRootMap
			.get("canAddVlAction");
		}
		
		return canAddVlAction;
	}
	@SuppressWarnings("unchecked")
	public void setCanAddVlAction(Boolean canAddVlAction) {
		this.canAddVlAction = canAddVlAction;
		
		if(canAddVlAction != null ){
			viewRootMap.put("canAddVlAction", canAddVlAction);
		}
	}

	public Boolean getCanChange() {

		this.setPageMode((String) viewRootMap.get("pageMode"));

		if (this.getPageMode() != null
				&& (this.getPageMode().equals(PAGE_MODE_ADD) || this.getPageMode().equals(PAGE_MODE_UPDATE) ) ) {
			canChange = true;
		} else {

			canChange = false;
		}
		
		if( getShowCompleteInspection() || getShowSendToRentComittee() )
		{
			canChange = false;
		}
		else{
			canChange = true;
		}
		
		return canChange;
	}

	public void setCanChange(Boolean canChange) {
		this.canChange = canChange;
	}

	/** default constructor */
	public inspectionDetailsBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public String cmdLinkViolation_Clicked() {
		String methodName = "cmdLinkViolation_Clicked";
		logger.logInfo(methodName + "|" + "Start");
		String outCome = "success";
		InspectionUnitView inspectionUnitView = (InspectionUnitView) tbl_InspectionUnits
				.getRowData();
		if (inspectionUnitView.getUnitView() != null
				&& inspectionUnitView.getUnitView().getUnitId() != null) {
			logger.logInfo(methodName + "|" + "UnitId::"
					+ inspectionUnitView.getUnitView().getUnitId());
			Long unitId = inspectionUnitView.getUnitView().getUnitId();
			String unitType = "";
			if (getIsEnglishLocale())
				unitType = inspectionUnitView.getUnitView().getUnitTypeEn();
			else
				unitType = inspectionUnitView.getUnitView().getUnitTypeAr();

			setRequestParam("fromInsList", "true");
			setRequestParam("unitId", unitId);
			setRequestParam("unitType", unitType);
			setRequestParam("propertyName", inspectionUnitView.getUnitView()
					.getPropertyCommercialName());

		}
		setRequestParam("contractNumber", inspectionUnitView
				.getContractNumber());
		setRequestParam("tenantName", inspectionUnitView.getTenantName());

		logger.logInfo(methodName + "|" + "inspectionId::"
				+ this.getInspectionId());
		// request.setAttribute("inspectionId", inspectionId);
		setRequestParam("inspectionId", this.getInspectionId());

		logger.logInfo(methodName + "|" + "Finish");
		return outCome;
	}

	private boolean putControlValuesInView() throws Exception {

		String methodName = "putControlValuesInView";
		logger.logInfo(methodName + "|" + "Start");
		boolean isError = false;
		errorMessages = new ArrayList<String>();
		errorMessages.clear();

		String loggedInUser = "";
		CommonUtil commonUtil = new CommonUtil();
		try {
			DateFormat df = new SimpleDateFormat(getDateFormat());
			inspectionView = getInspectionView();
			if(inspectionView == null)
				inspectionView = new InspectionView();
			
			loggedInUser = getLoggedInUser();
			logger.logInfo(methodName + "|" + "loggedInUser:" + loggedInUser);

			logger.logInfo(methodName + "|" + "InspectionId:"
					+ this.getInspectionId());
			if (this.getInspectionId() != null
					&& this.getInspectionId().length() > 0)
				inspectionView
						.setInspectionId(new Long(this.getInspectionId()));

			if(getPageMode().equals(PAGE_MODE_ADD))
			{
				inspectionView.setCreatedBy(loggedInUser);
				inspectionView.setCreatedOn(new Date());
				inspectionView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				inspectionView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			}
			
			inspectionView.setUpdatedBy(loggedInUser);
			inspectionView.setUpdatedOn(new Date());

			
			if ( !( inspectionView.getInspectionDate() != null	&&  inspectionView.getInspectionDate().toString().length() > 0))
				
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.InspectionDetails.MSG_REQ_DATE));
				isError = true;
			}
			logger.logInfo(methodName + "|" + "Inspection Number:"
					+ txtinspectionNum);			

			

			if (this.getSelectedType() != null
					&& !this.getSelectedType().equals("-1"))
				inspectionView.setType(new Long(this.getSelectedType()));
			else {
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.InspectionDetails.MSG_REQ_TYPE));
				isError = true;
			}	

				
				List<InspectionUnitView> inspectionUnitViewSet = getInspectionUnitsView();
				if (inspectionUnitViewSet.size() <= 0) 
				{
					
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.InspectionDetails.MSG_REQ_UNIT));
					isError = true;
				}
				else
				{
					for(InspectionUnitView uv: inspectionUnitViewSet)
					{
						if(!(uv.getInspectionUnitId() !=null && !uv.getInspectionUnitId().toString().equals("") && uv.getInspectionUnitId() >0 ))
						{
							uv.setCreatedBy(loggedInUser);
							uv.setCreatedOn(new Date());
							uv.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
							uv.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
						}
						uv.setUpdatedBy(loggedInUser);
						uv.setUpdatedOn(new Date());
					}
				}
				
				List<TeamMemberView> teamMemberViewList = getInspectionTeamDataList();
				if (teamMemberViewList.size() <= 0) 
				{
					
					errorMessages.add(CommonUtil.getBundleMessage("inspection.msg.teamMember"));
					isError = true;
				}
			

		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::", e);
			throw e;

		}

		logger.logInfo(methodName + "|" + "Finsih");
		return isError;
	}

	public void loadAttachmentsAndComments(Long inspectionId) throws Exception {
		String methodName = "loadAttachmentsAndComments|";
		try {
			logger.logInfo(methodName + "started...");
			String repositoryId = WebConstants.ATTACHMENT_REPOSITORY_ID;
			String fileSystemRepository = WebConstants.FILE_SYSTEM_REPOSITORY_INSPECTION;
			String user = getLoggedInUser();
			String entityId = inspectionId.toString();
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put("repositoryId", repositoryId);
			viewMap.put("fsRepositoryId", fileSystemRepository);
			viewMap.put("externalId", user);
			viewMap.put("associatedObjectId", entityId);
			viewMap.put("noteowner", WebConstants.REQUEST);
			viewMap.put("entityId", entityId);
			logger.logInfo(methodName + "completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
	}

	
	public void saveViolations(Long inspectionId) throws Exception {
		String methodName = "saveComments|";
		try {
			logger.logInfo(methodName + "started...");

			String notesOwner = WebConstants.INSPECTION;
			logger.logInfo(methodName + "notesOwner..." + notesOwner);
			//NotesController.saveNotes(notesOwner, inspectionId);
			PropertyServiceAgent psa = new PropertyServiceAgent();
			if( violationViewDataList != null)
			{
				for(int index = 0 ; index < violationViewDataList.size(); index++){
					
					ViolationView violationViewToAdd = violationViewDataList.get(index);
					violationViewToAdd.setInspectionId(inspectionId);
					//Long newViolationID = psa.addViolation(violationViewToAdd);
				}
			}

			logger.logInfo(methodName + "completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
	}
	
	public String sendTask(){
		
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		try {
			
			ApproveRejectTask(TaskOutcome.OK);

			
			setRequestParam("InspectionId", inspectionId);
			
			
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.inspectionSentToRentComm"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logError("sendTask|Error Occured:"+e);
		}
		
		
		return "sentTask";
	}
	
	public String markTaskCompleted(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		try {
			
			ApproveRejectTask(TaskOutcome.OK);
			setRequestParam("InspectionId", inspectionId);
			psa.setInspectionStatusAsComplete(Long.parseLong(inspectionId),getLoggedInUser());
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.inspectionComplete"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logError("markTaskCompleted|Error Occured:"+e);
		}
		
		
		return "sentTask";
	}
	
	public String onAssignViolationActions(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		try 
		{
			ApproveRejectTask(TaskOutcome.OK);
			setRequestParam("InspectionId", inspectionId);
			psa.changeInspectionStatus(Long.parseLong(inspectionId), getLoggedInUser(), WebConstants.INS_STATUS_CLOSE_VIOLATION_ID);
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.closeViolations"));
			viewMode();
		} catch (Exception e) {
			logger.LogException( "onAssignViolationActions|Error Occured:",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
		return "sentTask";
	}
	
	public String onSendForRecommendation(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		
		try {
			
			ApproveRejectTask(TaskOutcome.SEND_FOR_RECOMMENDATIONS);
			
			setRequestParam("InspectionId", inspectionId);
			
			DomainDataView ddv = CommonUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.INSPECTION_STATUS),
					WebConstants.INS_STATUS_RECOMMENDATION_REQUIRED);
			psa.changeInspectionStatus(Long.parseLong(inspectionId), getLoggedInUser(), ddv.getDomainDataId());
			
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.sendForRecommendations"));
			viewMode();
		} catch (Exception e) {
			logger.logError( "onSendForRecommendation|Error Occured:"+e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
		return "sentTask";
	}
	
	public String onSendForClosingViolation(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
	
		try 
		{
			
			ApproveRejectTask(TaskOutcome.SEND_FOR_CLOSING_VIOLATIONS);
			setRequestParam("InspectionId", inspectionId);
			psa.changeInspectionStatus(Long.parseLong(inspectionId), getLoggedInUser(), WebConstants.INS_STATUS_CLOSE_VIOLATION_ID);

		   	
			
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.inspectionComplete"));
			viewMode();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.logError("onSendForClosingViolation|Error Occured:"+e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
		return "sentTask";
	}
	
	public String onSendForViolationAction(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		try 
		{
			
			ApproveRejectTask(TaskOutcome.OK);
			setRequestParam("InspectionId", inspectionId);
			psa.changeInspectionStatus(Long.parseLong(inspectionId), getLoggedInUser(), WebConstants.INS_STATUS_PROCESSING_ID);
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.assignViolationActions"));
			viewMode();
		} 
		catch (Exception e) 
		{
			logger.logError("onSendForViolationAction|Error Occured:"+e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
		return "sentTask";
	}
	
    public String onCloseViolation(){
		
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	successMessages = new ArrayList<String>();
    	errorMessages = new ArrayList<String>();
		try 
		{
			
			ApproveRejectTask(TaskOutcome.OK);
			setRequestParam("InspectionId", inspectionId);
			psa.changeInspectionStatus(Long.parseLong(inspectionId), getLoggedInUser(), WebConstants.INS_STATUS_COMPLETED_ID);
			successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.inspectionComplete"));
			viewMode();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.logError("onCloseViolation|Error Occured:"+e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
		
		
		return "sentTask";
	}
	
	
	
	private boolean hasInitiateBPELTaskError()
	{
		errorMessages = new ArrayList<String>(0);
		if(this.getViolationViewDataList()==null || this.getViolationViewDataList().size()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("violationDetails.msg.AddViolation"));
			return true;
		}
		
		return false;
	}
	
	public String onSend(){
		
    	successMessages = new ArrayList<String>();
		errorMessages.clear();	
		try {
			    if(hasInitiateBPELTaskError())
			    {return"";}
				PIMSInspectionBPELPortClient PIMSInspectionBPEL = new PIMSInspectionBPELPortClient();
				SystemParameters parameters = SystemParameters.getInstance();
	            String endPoint= parameters.getParameter(WebConstants.INSPECTION_BPEL_ENDPOINT);
				PIMSInspectionBPEL.setEndpoint(endPoint);
				PIMSInspectionBPEL.initiate( Long.valueOf( inspectionId ), getLoggedInUser(), null, null);
				setRequestParam("InspectionId", inspectionId);
				DomainDataView ddvNew = CommonUtil.getIdFromType(
																	getDomainDataListForDomainType(WebConstants.INSPECTION_STATUS),
																	WebConstants.INS_STATUS_NEW
																);
				psa.changeInspectionStatus(
											Long.parseLong(inspectionId), 
											getLoggedInUser(), 
											ddvNew.getDomainDataId()
										  );
				
				viewMode();
				successMessages.add(ResourceUtil.getInstance().getProperty("inspection.message.assignViolationActions"));
		} 
		catch (Exception e) 
		{
			logger.LogException("onSend|Error Occured:",e);
 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
		return "sentTask";
	}

	public void saveComments(Long inspectionId) throws Exception 
	{

			String notesOwner = WebConstants.INSPECTION;
			NotesController.saveNotes(notesOwner, inspectionId);

	}

	@SuppressWarnings("unchecked")
	public Boolean saveAttachments(String referenceId) throws Exception
    {
		Boolean success = false;
	    	if(referenceId!=null)
	    	{
		    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		    		List<Long> docIdList = (List<Long>)viewMap.get("documentIdList");
		    		if(docIdList!=null && !docIdList.isEmpty() && StringHelper.isNotEmpty(referenceId))
		    		{
		    			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent(); 
		    			success = utilityServiceAgent.updateDocuments(docIdList, referenceId);
		    			if(success)
		    			{
		    				docIdList = new ArrayList<Long>();
		    				viewMap.put("documentIdList",docIdList);
		    			}
		    		}
		    }
    	
    	return success;
    }

	public Date getCreatedOnDate() throws Exception {
		SystemParameters parameters = SystemParameters.getInstance();

		String dateFormat = parameters
				.getParameter(WebConstants.SHORT_DATE_FORMAT);

		DateFormat df = new SimpleDateFormat(dateFormat);
		if (createdOn != null && createdOn.length() > 0) {
			return df.parse(createdOn);
		} else {
			return new Date();
		}

	}

	public String getCreatedByUser(String loggedInUser) {

		if (createdBy != null && createdBy.length() > 0) {
			return createdBy;
		} else {
			return loggedInUser;
		}

	}
	
	public String cmdInspectionTeamMemberDelete_Click() {
		
		try {
			
			TeamMemberView teamMemberViewdel = (TeamMemberView) inspectionTeamGrid.getRowData();
			List<TeamMemberView> teamMemberViewList =   getInspectionTeamDataList();
			if(teamMemberViewList != null && teamMemberViewList.size() > 0)
			{
				for(TeamMemberView teamMemberView : teamMemberViewList)
				{
					if(teamMemberViewdel.getLoginId().equals(teamMemberView.getLoginId()))
					{
						if(teamMemberView.getTeamMemberId() != null && !teamMemberView.getTeamMemberId().equals("") && teamMemberView.getTeamMemberId()>0 )
						{
							PropertyService ps = new PropertyService();
							teamMemberView.setIsDeleted(1L);
							TeamView teamView = getTeamView(); 
							ps.updateTeamMember(teamMemberView,teamView);
							teamMemberViewList.remove(teamMemberView);
							
						}
						else
							teamMemberViewList.remove(teamMemberView);
						
						setInspectionTeam(teamMemberViewList);
						break;
					}
				}
			}

		} catch (Exception e) {
			logger.logError("|" + "Exception Occured::" + e);
		}
		return "delete";
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	public String btnAdd_Click() {
		errorMessages = new ArrayList<String>();
		errorMessages.clear();
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			if (!putControlValuesInView())
			{
				
				inspectionView = psa.addInspections(inspectionView);
				setInspectionView(inspectionView);
				
				saveAttachments(inspectionView.getInspectionId().toString());
				saveComments(inspectionView.getInspectionId());
				
				if(!viewRootMap.containsKey("IN_UPDATE_MODE"))
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.InspectionDetails.MSG_SUCCESS_ADD));
				else
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.InspectionDetails.MSG_SUCCESS_UPDATED));
				
				viewMap.put(WebConstants.INSPECTION_ID, this.getInspectionId());
				
				setInspectionViewFromInspectionId(inspectionView.getInspectionId());
				getViolationViewDataList();
				this.setPageMode(PAGE_MODE_UPDATE);
			    
				addEditInspectionViolationMode();
				
			}

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logError( "btnAdd_Click|Exception Occured::" + e);
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public String btnSearch_Click() 
	{
		Set<User> usersSet;
		try {
			{
               PropertyServiceAgent psa = new PropertyServiceAgent();
               String inspectionTeamGroup = psa.getSystemConfigurationValueByKey(WebConstants.PIMS_INSPECTION);
				selectedInspectionTeamList = selectedInspectionTeamList;
				UserGroup userGroup = (UserGroup) SecurityManager
						.getGroup(inspectionTeamGroup);
				usersSet = userGroup.getUsers();				

			}
		} catch (Exception e) {
			logger.logError( "btnSearch_Click|Exception Occured::" + e);
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public String btnRemoveItems_Click() {
		String methodName = "btnRemoveItems_Click";

		try {
			HashMap selectedUsersMap = new HashMap();
			for (int i = 0; i < selectedUsersInspectionList.size(); i++) {
				selectedUsersMap.put(selectedUsersInspectionList.get(i)
						.toString(), selectedUsersInspectionList.get(i)
						.toString());

			}
			removeItemsFromList(selectedUsersMap);

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public String btnAddItems_Click() {
		String methodName = "btnAddItems_Click";

		try {
			addItemsToList();

		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::", e);
		}
		return "";
	}

	private void removeItemsFromList(HashMap selectedUsersMap) {
		if (selectedUsersInspectionList.size() > 0) {
			if (sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
				sessionMap.remove("SESSION_USER_INSPECTIONS");
			// usersInspectionList.clear();
		} else {
			if (sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
				usersInspectionList = (ArrayList<SelectItem>) sessionMap
						.get("SESSION_USER_INSPECTIONS");
		}
		List<SelectItem> tempList = new ArrayList<SelectItem>();
		for (int j = 0; j < usersInspectionList.size(); j++) {
			SelectItem useritem = (SelectItem) usersInspectionList.get(j);
			if (!selectedUsersMap.containsKey(useritem.getValue())) {
				tempList.add(useritem);
			}

		}
		usersInspectionList = tempList;
		sessionMap.put("SESSION_USER_INSPECTIONS", usersInspectionList);

	}

	@SuppressWarnings("unchecked")
	private void addItemsToList() 
	{
		HashMap hMap = new HashMap();

		if (sessionMap.containsKey("SESSION_HASHMAP_INSPECTION_TEAM_USERS"))
			hMap = (HashMap) sessionMap
					.get("SESSION_HASHMAP_INSPECTION_TEAM_USERS");
		if (sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
			usersInspectionList = (ArrayList<SelectItem>) sessionMap.get("SESSION_USER_INSPECTIONS");

		for (int i = 0; i < selectedInspectionTeamList.size(); i++) {
			if (hMap.containsKey(selectedInspectionTeamList.get(i).toString())) {
				User user = (User) hMap.get(selectedInspectionTeamList.get(i)
						.toString());
				SelectItem item = null;
				item = new SelectItem(user.getLoginId(), user.getFullName());

				if (!isItemExists(user)) {
					usersInspectionList.add(item);

				}

			}
		}
		sessionMap.put("USER_INSPECTIONS", usersInspectionList);
		sessionMap.put("SESSION_USER_INSPECTIONS", usersInspectionList);
	}

	@SuppressWarnings("unchecked")
	private boolean isItemExists(User user) {
		String methodName = "isItemExists";
		boolean isExist = false;
		logger.logInfo("|" + "Start..");
		List<SelectItem> itemList = (List<SelectItem>) sessionMap
				.get("SESSION_USER_INSPECTIONS");
		if (itemList != null) {
			for (int i = 0; i < itemList.size(); i++) {
				SelectItem item = (SelectItem) itemList.get(i);
				if (item.getValue().equals(user.getLoginId())) {
					isExist = true;
					break;
				}
			}
		}

		logger.logInfo("|" + "Finish..");
		return isExist;
	}

	public String getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	@SuppressWarnings("unchecked")
	private void setMode()throws Exception
	{
		  //Only uncomment to simulate task list functionality-Start
//		  userTask = new UserTask();
//		  userTask.setTaskType( "AssignViolationActions");
////		   userTask.setTaskType( "SendForRecommendations");
//		  // userTask.setTaskType( "CloseViolations");
//		  Map<String, String> taskAttribute  = new HashMap<String, String>();
//		  taskAttribute.put(WebConstants.INSPECTION_ID_FOR_TASk,"1263");
//		  userTask.setTaskAttributes(taskAttribute);
//		  sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK , userTask);
		  //Only uncomment to simulate task list functionality-Finish
		  if(
				  ( 
				    sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK )&& 
				    sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null
				  ) 
				  || 
				  (
					viewMap.containsKey( WebConstants.TASK_LIST_SELECTED_USER_TASK )&& 
				    viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null
				  )
			)
		  {
				if(sessionMap.containsKey( WebConstants.TASK_LIST_SELECTED_USER_TASK))
				{
				 userTask = (UserTask) sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				 viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);	
				}
				if (userTask != null) 
				{
					inspectionId = userTask.getTaskAttributes().get("INSPECTION_ID");
					this.setPageMode(PAGE_MODE_VIEW);
					setInspectionViewFromInspectionId(Long.parseLong(inspectionId.toString()));					
					getInspectionViolationsList(inspectionView);
					this.setInspectionView(  inspectionView );

					if(userTask.getTaskType() != null && userTask.getTaskType().equals("AssignViolationActions"))
					{
						Map taskAttributes =  userTask.getTaskAttributes();
						Long inspectionId = Convert.toLong(taskAttributes.get(WebConstants.INSPECTION_ID_FOR_TASk));
						setInspectionViewFromInspectionId(inspectionId);
						this.setPageMode( PAGE_MODE_TASK_VIOLATION_ACTION );
						addViolationActionsMode();
					}
					else if(userTask.getTaskType() != null && userTask.getTaskType().equals("SendForRecommendations"))
					{
						Map taskAttributes =  userTask.getTaskAttributes();
						Long inspectionId = Convert.toLong(taskAttributes.get(WebConstants.INSPECTION_ID_FOR_TASk));
						setInspectionViewFromInspectionId(inspectionId);
						this.setPageMode( PAGE_MODE_TASK_VIOLATION_ACTION );
						addViolationRecommendationMode();
					}
					else if (userTask.getTaskType() != null && userTask.getTaskType().equals("CloseViolations"))
					{
						Map taskAttributes =  userTask.getTaskAttributes();
						Long inspectionId = Convert.toLong(taskAttributes.get(WebConstants.INSPECTION_ID_FOR_TASk));
						setInspectionViewFromInspectionId(inspectionId);  						
						this.setPageMode( PAGE_MODE_TASK_VIOLATION_CLOSE );
						addViolationCLoseMode();
					}
				}
		  }
		 else
		 { 
				if (getRequestParam("InspectionView") != null) 
				{
					this.setPageMode(PAGE_MODE_UPDATE);
					InspectionView inspectionView = (InspectionView) getRequestParam("InspectionView");
					setInspectionViewFromInspectionId(inspectionView.getInspectionId());					
					getInspectionViolationsList(inspectionView);
					viewRootMap.put("IN_UPDATE_MODE", true);
					
					if(inspectionView.getStatus().compareTo(WebConstants.INS_STATUS_NEW_ID)==0)
					{
						addEditInspectionViolationMode();
					}
					else
					{
						viewMode();
					}
		
				} 
				else if (getRequestParam("InspectionId") != null) 

				{
					this.setPageMode(PAGE_MODE_UPDATE);					
					setInspectionViewFromInspectionId(new Long(getRequestParam("InspectionId").toString()));					
					getInspectionViolationsList(inspectionView);
					this.setInspectionView(  inspectionView );
				}	
				else
				{
					setInspectionView(new InspectionView());
					this.setPageMode(PAGE_MODE_ADD);
					
					List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.INSPECTION_STATUS);
			             DomainDataView  ddv     = CommonUtil.getIdFromType(ddvList,WebConstants.INS_STATUS_NEW);
			        if (ddv != null)
			        {
				         inspectionView.setStatus(ddv.getDomainDataId());
				         inspectionView.setStatusEn(ddv.getDataDescEn());
				         inspectionView.setStatusAr(ddv.getDataDescAr());
				         setInspectionView(inspectionView);
			        }
					
			        addEditInspectionMode();

				}
			}
	}
	@Override
	public void init() 
	{

		sessionMap = context.getExternalContext().getSessionMap();
		successMessages = new ArrayList<String>();
		errorMessages = new ArrayList<String>();
		try 
		{
			if (!isPostBack()) 
			{
				removeSessions();
				setMode();
				if( sessionMap.get("complaintDetails") != null )
		    	{
		    		populateFromComplaints();
		    		
		    	}

				loadAttachmentsAndComments();
			}
			else
			{
				// FOR Violation and units popup - Returning from popups
				if(sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS))
		    	 {
					 getUnitDataFromSession();
					 
		    	 }
				if(sessionMap.containsKey(WebConstants.TEAM_DATA_LIST) && sessionMap.get(WebConstants.TEAM_DATA_LIST) != null )				
				{
					getTeamDataFromSession();
					sessionMap.remove(WebConstants.TEAM_DATA_LIST);	
				}
				
				if (sessionMap.containsKey("NEW_VIOLATION_VIEW")) 
				{
					getViolationViewFromSession();
		    		sessionMap.remove("NEW_VIOLATION_VIEW");
				}
			}
			
			if(
				   getInspectionView() != null && 
				   getInspectionView().getStatus() != null && 
				   getInspectionView().getStatus().compareTo(WebConstants.INS_STATUS_SCHEDULED_ID)==0
			  ) 
			{ 
				this.setCanInitiateTask(true);
			}
			else
			{
				this.setCanInitiateTask(false);
			}
			if(
					getInspectionId() != null && getInspectionView() != null && 
					getInspectionView().getStatus() != null && 
					getInspectionView().getStatus() != null && 
					getInspectionView().getStatus().compareTo(WebConstants.INS_STATUS_VIOLATION_ID)!=0
			  ) 
			{
				this.setCanAddVlAction(true);
				this.setCanChange(false);
			}
			else
			{
				this.setCanAddVlAction(false);
				this.setCanChange(true);
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("init|Exception Occured::", ex);
			viewMode();
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked")
	private void populateFromComplaints() throws Exception 
	{
		ComplaintDetailsView cdView  = (ComplaintDetailsView)sessionMap.remove("complaintDetails");
//		this.setComplaintDetailsView(cdView);
//		if( cdView.getContractId() != null )
//		{
//			maintenanceContractView = getContractViewFromContractId(  cdView.getContractId() );
//			setMaintenanceContractView(maintenanceContractView);
//		}
//		
//		if(cdView.getUnitId() != null)
//		{
//			populateUnitInfo( cdView.getUnitId() );
//		}
//		else if( cdView.getPropertyId() != null )
//		{
//			this.hdnPropertyId  = cdView.getPropertyId().toString(); 
//			populatePropertyInfoInTab();
//		}
//		
	}
	@SuppressWarnings("unchecked")
	private void getViolationViewFromSession() throws Exception 
	{
		if(sessionMap.containsKey("SAVE_DONE"))
		{
		 List<ViolationView> violationViewList = (List<ViolationView>) sessionMap.remove("NEW_VIOLATION_VIEW");
		 List<ViolationView> oldViolation = getViolationViewDataList();
		 oldViolation.addAll(violationViewList);
		 setViolationViewDataList(oldViolation);
		 sessionMap.remove("SAVE_DONE");
		 successMessages.add(CommonUtil.getBundleMessage("violation.msg.SaveViolation"));
		}
		if(sessionMap.containsKey("EDIT_DONE"))
		{
			  List<ViolationView> violationViewList = (List<ViolationView>) sessionMap.remove("NEW_VIOLATION_VIEW");
		      if(!violationViewList.isEmpty())
		      {
				    ViolationView tempView = violationViewList.get(0);
					Integer index = (Integer) viewRootMap.get("index");
					List<ViolationView> tempList = getViolationViewDataList();
					tempList.remove(index.intValue());
					tempList.add(index.intValue(), tempView);
					setViolationViewDataList(tempList);
					sessionMap.remove("EDIT_DONE");
					successMessages.add(CommonUtil.getBundleMessage("violation.msg.EditViolation"));
				}
		}
	}

	@SuppressWarnings("unchecked")
	private void getUnitDataFromSession() throws Exception 
	{
		//populateInspectionUnitsFromUnits();
		 List<InspectionUnitView> selectedUnits = (List<InspectionUnitView>) sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
		 List<InspectionUnitView> oldUnits = getInspectionUnitsView();
		 oldUnits.addAll(selectedUnits);
		 setInspectionUnitsView(oldUnits);
	}

	@SuppressWarnings("unchecked") 
	private void getTeamDataFromSession( ) throws Exception
	{
		String  loggedInUser= getLoggedInUserId();
		List<UserView> receivingDataList = (List<UserView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TEAM_DATA_LIST);					
		List<TeamMemberView> oldTeamMembers = getInspectionTeamDataList();
		List<TeamMemberView> newTeamMemebers = new ArrayList<TeamMemberView>();					
		for(UserView userView: receivingDataList )
		{						
			TeamMemberView teamMemberView = new TeamMemberView();
			teamMemberView.setSecUserView(userView);
			teamMemberView.setCreatedBy(loggedInUser);
			teamMemberView.setCreatedOn(new Date());
			teamMemberView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			teamMemberView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			teamMemberView.setUpdatedBy(loggedInUser);
			teamMemberView.setUpdatedOn(new Date());
			teamMemberView.setLoginId(userView.getUserName());
			newTeamMemebers.add(teamMemberView);
		}
		oldTeamMembers.addAll(newTeamMemebers);
		setInspectionTeam(oldTeamMembers);
	}

    @SuppressWarnings("unchecked")
    private void ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
		bpmWorkListClient.completeTask(userTask,     	getLoggedInUser(), taskOutCome);
    }

    @SuppressWarnings("unchecked")
	private void getInspectionViolationsList(InspectionView inspectionView)throws Exception 
    {
		List<ViolationView> violationViewList = new ArrayList<ViolationView>();
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		List<InspectionUnitView> inspectionUnitView = inspectionView.getInspectionUnitsView();
		InspectionUnitView iuv = null;
		InspectionViolationView ivv = null;
		if (inspectionUnitView == null) 
		{return;}
		Iterator itUnit = inspectionUnitView.iterator();
		while (itUnit.hasNext()) 
		{
			iuv = (InspectionUnitView) itUnit.next();
			ivv = propertyServiceAgent.getViolationsByUnitId(
																iuv.getUnitView().getUnitId(), 
																inspectionView.getInspectionId(), 
																dateFormat,
																iuv.getTenantName(), 
																iuv.getContractNumber(), 
																iuv.getUnitView().getUnitNumber()
															);
			if (ivv != null && ivv.getViolations() != null)
				violationViewList.addAll(ivv.getViolations());
		}
		this.setViolationViewDataList(violationViewList);
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		dateFormat = localeInfo.getDateFormat();
		return dateFormat;
	}

	public String getLocale() {
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}

	public LocaleInfo getLocaleInfo() {

		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo;

	}

	@SuppressWarnings("unchecked")
	private void setInspectionView(InspectionView inpectionView)
	{
		viewRootMap.put(WebConstants.INSPECTION_VIEW, inpectionView);
	}
	@SuppressWarnings("unchecked")
	private void setInspectionViewFromInspectionId(Long inspectionId) throws PimsBusinessException
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		InspectionView objInspectionView = new InspectionView();
		objInspectionView.setInspectionId(new Long(inspectionId));
		viewMap.put(WebConstants.INSPECTION_ID,objInspectionView.getInspectionId());
		List<InspectionView> inspectionViewList = psa.getAllInspections(objInspectionView);
		InspectionView inspectionView = (InspectionView) inspectionViewList.get(0);
		setInspectionView(inspectionView);
		//getInspectionView(inspectionView);
	}

	private void removeSessions() {
		if (sessionMap.containsKey("SESSION_HASHMAP_INSPECTION_TEAM_USERS"))
			sessionMap.remove("SESSION_HASHMAP_INSPECTION_TEAM_USERS");
		if (sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
			sessionMap.remove("SESSION_USER_INSPECTIONS");
		if (sessionMap.containsKey("SESSION_INSPECTION_TEAM_USERS"))
			sessionMap.remove("SESSION_INSPECTION_TEAM_USERS");
		if (sessionMap.containsKey("SESSION_INSPECTION_UNITS"))
			sessionMap.remove("SESSION_INSPECTION_UNITS");
	}




	

	@SuppressWarnings("unchecked")
	public void btnDeleteUnits_Click() {
		try 
		{
			InspectionUnitView iuv = (InspectionUnitView) tbl_InspectionUnits.getRowData();
			List<InspectionUnitView> inspectionUnitViewList =   getInspectionUnitsView();
			if(inspectionUnitViewList != null && inspectionUnitViewList.size() > 0)
			{
				for(InspectionUnitView inspectionUnitView : inspectionUnitViewList)
				{
					if(inspectionUnitView.getUnitView().getUnitId().compareTo(iuv.getUnitView().getUnitId()) == 0)
					{
						if(inspectionUnitView.getInspectionUnitId() != null && !inspectionUnitView.getInspectionUnitId().equals("") && inspectionUnitView.getInspectionUnitId()>0 )
						{
							PropertyService ps = new PropertyService();
							inspectionUnitView.setIsDeleted(1L);
							inspectionView = getInspectionView();
							ps.updateInspectionUnit(inspectionUnitView,inspectionView);							
							inspectionUnitViewList.remove(inspectionUnitView);
							
						}
						else
							inspectionUnitViewList.remove(inspectionUnitView);
						
						setInspectionUnitsView(inspectionUnitViewList);
						break;
					}
				}
			}
			
		} catch (Exception ex) {
			logger.LogException("btnDeleteUnits_Click| Error Occured...", ex);

		}
	}
	
	@SuppressWarnings("unchecked")
	public void closeViolation() 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try {

			UtilityManager utilityManager = new UtilityManager();
        	
        	DomainData domainData = 
                utilityManager.getDomainDataByValue(Constant.VIOLATION_STATUS, 
                                                    Constant.VIOLATION_STATUS_CLOSED);
        	
//			ViolationView vv = (ViolationView) dataTableViolation.getRowData();

			List<ViolationView> violationList = (ArrayList) getViolationViewDataList();
			for (ViolationView vv : violationList) 
			{
					if(psa.setViolationStatusAsClose(vv.getViolationId() , getLoggedInUser()) )
					{
						vv.setStatusId(domainData.getDomainDataId());
						vv.setStatusAr(domainData.getDataDescAr());
						vv.setStatusEn(domainData.getDataDescEn());
						
					}
			}
				recordSize = violationList.size();
				viewRootMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize / paginatorRows;
				if ((recordSize % paginatorRows) > 0)
					paginatorMaxPages++;

				if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

				viewRootMap.put("paginatorMaxPages", paginatorMaxPages);

				this.setViolationViewDataList(violationList);
			

		} catch (Exception ex) {
			logger.LogException("closeViolation| Error Occured...", ex);

		}
	}
	
	public void btnDeleteViolation_Click() {
		String methodName = "btnDeleteViolation_Click";
		logger.logInfo(methodName + "|" + " Start...");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try {

			ViolationView vv = (ViolationView) dataTableViolation.getRowData();

			List<ViolationView> violationList = (ArrayList) getViolationViewDataList();
			if (violationList != null) {
				
				if(vv.getViolationId() != null ){
					if(psa.deleteInspectionViolation(vv.getViolationId()))
					{
						violationList.remove(vv);
						
					}
				}
				
				recordSize = violationList.size();

				viewRootMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize / paginatorRows;
				if ((recordSize % paginatorRows) > 0)
					paginatorMaxPages++;

				if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

				viewRootMap.put("paginatorMaxPages", paginatorMaxPages);

				this.setViolationViewDataList(violationList);
			}
			logger.logInfo(methodName + "|" + " Finish...");

		} catch (Exception ex) {
			logger.LogException(methodName + "|" + " Error Occured...", ex);

		}
	}

	public String getSelectedType() {
		String selectType = "";
		inspectionView = getInspectionView();		
		if (inspectionView != null && inspectionView.getType() != null)
		{
			selectType = inspectionView.getType().toString();
		}
		return selectType;
	}

	public void setSelectedType(String selectedType) {
		inspectionView = getInspectionView();		
		if (inspectionView != null)
		{
			inspectionView.setType(Long.parseLong(selectedType.toString()));
		}
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

   public List<SelectItem> getInspectionTeamList() {
		Set<User> usersSet = new HashSet<User>(0);
		if (sessionMap.containsKey("SESSION_INSPECTION_TEAM_USERS"))
			usersSet = (Set<User>) sessionMap
					.get("SESSION_INSPECTION_TEAM_USERS");
		Iterator iter = usersSet.iterator();
		inspectionTeamList.clear();
		while (iter.hasNext()) {
			User user = (User) iter.next();
			SelectItem item = null;
			item = new SelectItem(user.getLoginId(), user.getFullName());
			inspectionTeamList.add(item);

		}

		return inspectionTeamList;
	}

	public void setInspectionTeamList(List<SelectItem> inspectionTeamList) {
		this.inspectionTeamList = inspectionTeamList;
	}

	public List getSelectedInspectionTeamList() {
		return selectedInspectionTeamList;
	}

	public void setSelectedInspectionTeamList(List selectedInspectionTeamList) {
		this.selectedInspectionTeamList = selectedInspectionTeamList;
	}

	public List<SelectItem> getUsersInspectionList() {

		if (sessionMap.containsKey("SESSION_USER_INSPECTIONS")) {
			List<SelectItem> itemsList = (List<SelectItem>) sessionMap
					.get("SESSION_USER_INSPECTIONS");
			usersInspectionList = itemsList;
		}

		return usersInspectionList;
	}

	public void setUsersInspectionList(List<SelectItem> usersInspectionList) {
		this.usersInspectionList = usersInspectionList;
	}

	public List getSelectedUsersInspectionList() {
		return selectedUsersInspectionList;
	}

	public void setSelectedUsersInspectionList(List selectedUsersInspectionList) {
		this.selectedUsersInspectionList = selectedUsersInspectionList;
	}

	public List getInspectionUnitsDataList() {
		return getInspectionUnitsView();
	}

	public void setInspectionUnitsDataList(List inspectionUnitsDataList) {
		this.inspectionUnitsDataList = inspectionUnitsDataList;
	}

	public HtmlDataTable getTbl_InspectionUnits() {
		return tbl_InspectionUnits;
	}

	public void setTbl_InspectionUnits(HtmlDataTable tbl_InspectionUnits) {
		this.tbl_InspectionUnits = tbl_InspectionUnits;
	}

	public InspectionUnitView getDataItem() {
		return dataItem;
	}

	public void setDataItem(InspectionUnitView dataItem) {
		this.dataItem = dataItem;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getInspectionId() {

		if (viewRootMap.containsKey("inspectionId")
				&& viewRootMap.get("inspectionId") != null)
			inspectionId = viewRootMap.get("inspectionId").toString();

		return inspectionId;
	}

	public void setInspectionId(String inspectionId) {
		this.inspectionId = inspectionId;
		if (this.inspectionId != null)
			viewRootMap.put("inspectionId", this.inspectionId);
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public List<SelectItem> getInspectionTypeList() {
		CommonUtil commonUtil = new CommonUtil();
		String methodName = "getInspectionTpyeList";
		logger.logDebug(methodName + "|" + "Start");
		if (!viewRootMap.containsKey("inspectionTypeList")
				&& viewRootMap.get("inspectionTypeList") == null) {
			List<DomainDataView> ddvList = commonUtil
					.getDomainDataListForDomainType(WebConstants.INSPECTION_TYPE);
			for (DomainDataView domainDataView : ddvList) {
				SelectItem item;
				if (getIsEnglishLocale())
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescEn());
				else
					item = new SelectItem(domainDataView.getDomainDataId()
							.toString(), domainDataView.getDataDescAr());
				inspectionTypeList.add(item);
			}
			viewRootMap.put("inspectionTypeList", inspectionTypeList);
		} else
			inspectionTypeList = (ArrayList<SelectItem>) viewRootMap
					.get("inspectionTypeList");
		logger.logDebug(methodName + "|" + "Finish");
		return inspectionTypeList;
	}

	public void setInspectionTpyeList(List<SelectItem> inspectionTpyeList) {
		this.inspectionTypeList = inspectionTpyeList;
	}

	public HtmlCommandLink getViolationLink() {
		return violationLink;
	}

	public void setViolationLink(HtmlCommandLink violationLink) {
		this.violationLink = violationLink;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows
	 *            the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize
	 *            the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public HtmlCommandButton getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(HtmlCommandButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	public HtmlAjaxCommandLink getCmdDelUnit() {
		return cmdDelUnit;
	}

	public void setCmdDelUnit(HtmlAjaxCommandLink cmdDelUnit) {
		this.cmdDelUnit = cmdDelUnit;
	}

	public HtmlCommandButton getBtnAddUnits() {
		return btnAddUnits;
	}

	public void setBtnAddUnits(HtmlCommandButton btnAddUnits) {
		this.btnAddUnits = btnAddUnits;
	}

	public HtmlAjaxCommandButton getCmdAddTeamMembers() {
		return cmdAddTeamMembers;
	}

	public void setCmdAddTeamMembers(HtmlAjaxCommandButton cmdAddTeamMembers) {
		this.cmdAddTeamMembers = cmdAddTeamMembers;
	}

	public HtmlAjaxCommandButton getCmdRemoveTeamMembers() {
		return cmdRemoveTeamMembers;
	}

	public void setCmdRemoveTeamMembers(
			HtmlAjaxCommandButton cmdRemoveTeamMembers) {
		this.cmdRemoveTeamMembers = cmdRemoveTeamMembers;
	}

	public org.richfaces.component.html.HtmlCalendar getCalendarInspectionFrom() {
		return calendarInspectionFrom;
	}

	public void setCalendarInspectionFrom(
			org.richfaces.component.html.HtmlCalendar calendarInspectionFrom) {
		this.calendarInspectionFrom = calendarInspectionFrom;
	}

	public HtmlSelectOneMenu getComboInspectionType() {
		return comboInspectionType;
	}

	public void setComboInspectionType(HtmlSelectOneMenu comboInspectionType) {
		this.comboInspectionType = comboInspectionType;
	}

	public HtmlCommandButton getBtnClear() {
		return btnClear;
	}

	public void setBtnClear(HtmlCommandButton btnClear) {
		this.btnClear = btnClear;
	}

	public Boolean saveAttachments(Long referenceId) {
		Boolean success = false;
		try {
			logger.logInfo("saveAtttachments started...");
			if (referenceId != null) {
				success = CommonUtil.updateDocuments();
			}
			logger.logInfo("saveAtttachments completed successfully!!!");
		} catch (Throwable throwable) {
			success = false;
			logger.LogException("saveAtttachments crashed ", throwable);
		}

		return success;
	}

	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments() {

		// TODO : Add your procedure type - Done
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,
				WebConstants.PROCEDURE_TYPE_INSPECTION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_INSPECTION;

		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);

		// TODO : Add your procedure type - Done

		viewMap.put("noteowner", WebConstants.INSPECTION);

		// TODO : Add when saving the Inspection ID - Done
		if (viewMap.containsKey(WebConstants.INSPECTION_ID)
				&& viewMap.get(WebConstants.INSPECTION_ID) != null
				&& !viewMap.get(WebConstants.INSPECTION_ID).equals("")) {
			Long auctionId = Long.valueOf(viewMap.get(
					WebConstants.INSPECTION_ID).toString());
			String entityId = auctionId.toString();

			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	public String tabInspectionTeam_Click() {

		String methodName = "tabInspectionTeam_Click";
		logger.logInfo(methodName + "|" + "Start..");
		String eventOutcome = "";
		logger.logInfo("|" + "Finish..");
		return eventOutcome;

	}

	private void loadViolationDataList(boolean isUpdated) {

		String sMethod = "loadTeamDataList";

		// this.setInspectionTeamDataList( new ArrayList<UserView>());

		if (getFacesContext().getExternalContext().getSessionMap().get(
				WebConstants.INSPECTION_VIOLATION_ADD) != null) {
			getFacesContext().getExternalContext().getSessionMap().put(
					WebConstants.INSPECTION_VIOLATION_ADD, null);
			if (getFacesContext().getExternalContext().getSessionMap().get(
					WebConstants.INSPECTION_VIEW) != null
					&& !isUpdated)
				inspectionView = (InspectionView) getFacesContext()
						.getExternalContext().getSessionMap().get(
								WebConstants.INSPECTION_VIEW);

			if (getFacesContext().getExternalContext().getSessionMap().get(
					WebConstants.INSPECTION_VIOLATION_DATA_LIST) != null) {

				this
						.setViolationViewDataList((List<ViolationView>) getFacesContext()
								.getExternalContext()
								.getSessionMap()
								.get(
										WebConstants.INSPECTION_VIOLATION_DATA_LIST));

				getFacesContext().getExternalContext().getSessionMap().put(
						WebConstants.INSPECTION_VIOLATION_DATA_LIST, null);

				getFacesContext().getViewRoot().getAttributes().put(
						"VIOLATION_DATA_LIST", this.getViolationViewDataList());

			}

		} else {
			if (getFacesContext().getViewRoot().getAttributes().containsKey(
					"VIOLATION_DATA_LIST")) {
				this.setViolationViewDataList((List) getFacesContext()
						.getViewRoot().getAttributes().get(
								"VIOLATION_DATA_LIST"));
			}
		}

	}

	public void showAddViolationPopup() {

		// put some identifier here for violation
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.INSPECTION_VIOLATION_ADD, true);
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.INSPECTION_VIEW, getInspectionView());

		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.INSPECTION_VIOLATION_DATA_LIST, getViolationViewDataList());
		
		sessionMap.put("SESSION_INSPECTION_UNITS",viewMap.get("SESSION_INSPECTION_UNITS"));
		// getFacesContext().getExternalContext().getSessionMap().put(
		// WebConstants.INSPECTION_ID, inspectionView);

		boolean isUnitValidated = true;
		if (this.getInspectionUnitsDataList() == null) {

			isUnitValidated = false;
		} else if (this.getInspectionUnitsDataList() != null
				&& this.getInspectionUnitsDataList().size() <= 0) {

			isUnitValidated = false;
		}

		if (!isUnitValidated) {

			errorMessages
					.add("At least one unit must be selected to all violation.");
			return  ;
		}
		// It is not nessasay to put another object in session, I will use the
		// same session Object with key SESSION_INSPECTION_UNITS

		// getFacesContext().getExternalContext().getSessionMap().put(
		// WebConstants.SELECTED_INSPECTION_UNITS,
		// this.getInspectionUnitsDataList());

		// also put unit list in session

		final String viewId = "/inspectionDetails.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showAddViolationPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	public void showViolationActionsPopup() {

		getFacesContext().getExternalContext().getSessionMap().put(
				"FOR_VIOLATION_ACTIONS", true);
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.INSPECTION_VIEW, inspectionView);
		
		ViolationView selectedViolation = (ViolationView) dataTableViolation
		.getRowData();
		
		setRequestParam("violationView", selectedViolation);
		setRequestParam("unitId", selectedViolation.getUnitId().toString());
		setRequestParam("inspectionId", selectedViolation.getInspectionId());

		getFacesContext().getExternalContext().getSessionMap().put(
				"INSPECTION_VIOLATION_VIEW_FOR_VA", selectedViolation);
		

		setRequestParam("violationId", selectedViolation.getViolationId());
		setRequestParam("violationDate", selectedViolation.getViolationDateString());

		final String viewId = "/inspectionDetails.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showViolationActionsPopUp();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		
		//==============================================
		
		String methodName = "addAction";
		logger.logInfo(methodName + "|" + " Start");

		
				

		logger.logInfo(methodName + "|" + " Finish");

		//return "violationAction";
	}
	public void showMemberSearchPopup() {

		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.Property.SEARCH_RECEIVING, true);
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.INSPECTION_VIEW, inspectionView);

		final String viewId = "/inspectionDetails.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:SearchMemberPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	public InspectionView getInspectionView() {
		
		if (viewRootMap.containsKey(WebConstants.INSPECTION_VIEW) && viewRootMap.get(WebConstants.INSPECTION_VIEW) != null)
		{	
			inspectionView = (InspectionView) viewRootMap.get(WebConstants.INSPECTION_VIEW);
		}		
		return inspectionView;
	}
	@SuppressWarnings("unchecked")
	public List<InspectionUnitView> getInspectionUnitsView() {
		
		inspectionView = getInspectionView();
		List<InspectionUnitView> inspectionViewList = new ArrayList<InspectionUnitView>();
		if(inspectionView != null && inspectionView.getInspectionUnitsView() != null) 
		{
			inspectionViewList = inspectionView.getInspectionUnitsView();
			viewMap.put("SESSION_INSPECTION_UNITS", inspectionViewList);
			viewMap.put("recordSize", inspectionViewList.size());
		}
		return inspectionViewList;
	}
	public void setInspectionUnitsView(List<InspectionUnitView> inspectionUnitViewList) {
		
		inspectionView = getInspectionView();		
		if(inspectionView != null) 
		{
			inspectionView.setInspectionUnitsView(inspectionUnitViewList);
		}		
	}

	public TeamView getTeamView() {
		
		String loggedInUser = getLoggedInUser();
		inspectionView = getInspectionView();
		TeamView teamView = null;
		if(inspectionView != null && inspectionView.getTeamView() != null) 
		{
			teamView = inspectionView.getTeamView();
		}
		else
		{
			teamView = new TeamView();
			teamView.setTeamDescription("Inspection Team");
			teamView.setTeamName("Inspection Team");
			teamView.setCreatedBy(loggedInUser);
			teamView.setCreatedOn(new Date());
			teamView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			teamView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			teamView.setUpdatedBy(loggedInUser);
			teamView.setUpdatedOn(new Date());
			setTeamView(teamView);
		}
		return teamView;
	}
	public void setTeamView(TeamView teamView) {
		
		inspectionView = getInspectionView();		
		if(inspectionView != null) 
		{
			inspectionView.setTeamView(teamView);
		}		
	}
	/*
	public void setInspectionView(InspectionView inspectionView) {
		this.inspectionView = inspectionView;
		
		if (this.inspectionView != null)
			viewRootMap.put("inspectionView", this.inspectionView);
	}
	*/

	public HtmlDataTable getInspectionTeamGrid() {
		return inspectionTeamGrid;
	}

	public void setInspectionTeamGrid(HtmlDataTable inspectionTeamGrid) {
		this.inspectionTeamGrid = inspectionTeamGrid;
	}

	public List<TeamMemberView> getInspectionTeamDataList() {

		List<TeamMemberView> teamMemberViewList = new ArrayList<TeamMemberView>();
		TeamView teamView =  getTeamView();
		
		if(teamView != null && teamView.getTeamMembersView()!= null )
		{
			teamMemberViewList.addAll(teamView.getTeamMembersView());
		}
		viewMap.put("receivingTeamRecordSize", teamMemberViewList.size());
		return teamMemberViewList;
	}

	public void setInspectionTeamDataList(List<TeamMemberView> inspectionTeamDataList) {
		
		List<TeamMemberView> teamMemberViewList = new ArrayList<TeamMemberView>();
	}
	public void setInspectionTeam(List<TeamMemberView> inspectionTeamDataList) {

		TeamView teamView =  getTeamView();
		
		if(teamView != null  )
		{
			Set<TeamMemberView> teamMemberViewList = new HashSet<TeamMemberView>();
			if(inspectionTeamDataList != null)
				teamMemberViewList.addAll(inspectionTeamDataList);
			teamView.setTeamMembersView(teamMemberViewList);			
		}
	
	}

	public HtmlCommandButton getAddViolationTeam() {
		return addViolationTeam;
	}

	public void setAddViolationTeam(HtmlCommandButton addViolationTeam) {
		this.addViolationTeam = addViolationTeam;
	}

	public UserView getDataitemTeam() {
		return dataitemTeam;
	}

	public void setDataitemTeam(UserView dataitemTeam) {
		this.dataitemTeam = dataitemTeam;
	}

	public void setInspectionTypeList(List<SelectItem> inspectionTypeList) {
		this.inspectionTypeList = inspectionTypeList;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getPageMode() {

		if (getFacesContext().getViewRoot().getAttributes().containsKey(
				"pageMode")
				&& getFacesContext().getViewRoot().getAttributes().get(
						"pageMode") != null)
			pageMode = (String) getFacesContext().getViewRoot().getAttributes()
					.get("pageMode");

		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;

		if (this.pageMode != null)
			viewRootMap.put("pageMode", this.pageMode);
	}

	public HtmlCommandLink getDeleteLink() {
		return deleteLink;
	}

	public void setDeleteLink(HtmlCommandLink deleteLink) {
		this.deleteLink = deleteLink;
	}

	public HtmlDataTable getDataTableViolation() {
		return dataTableViolation;
	}

	public void setDataTableViolation(HtmlDataTable dataTableViolation) {
		this.dataTableViolation = dataTableViolation;
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public List<ViolationView> getViolationViewDataList() {
		
		InspectionView ispVio = new InspectionView();
		List<ViolationView> violationViewList = new ArrayList<ViolationView>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try {
		if(viewRootMap.containsKey(WebConstants.INSPECTION_VIEW)){
			ispVio = (InspectionView)viewRootMap.get(WebConstants.INSPECTION_VIEW);
			
			violationViewList =	psa.getInspectionViolation(ispVio.getInspectionId());
		
			if(violationViewList!=null && violationViewList.size()>0){
				violationViewDataList = violationViewList;
				viewMap.put("VIOLATION_DATA_LIST",violationViewDataList);
				viewMap.put("violationRecordSize", violationViewDataList.size());
			}
		}
		
		} catch (PimsBusinessException e) {
		   logger.LogException("Exception In getViolationViewDataList():::::::::", e);
		}
 
		return violationViewDataList;
	}

	public void setViolationViewDataList(List<ViolationView> violationViewDataList) {

		this.violationViewDataList = violationViewDataList;

		if (this.violationViewDataList != null
				&& this.violationViewDataList.size() > 0)
			viewRootMap.put("VIOLATION_DATA_LIST", this.violationViewDataList);
	}

	public Integer getReceivingTeamRecordSize() {
		receivingTeamRecordSize = (Integer) viewMap.get("receivingTeamRecordSize");
		if(receivingTeamRecordSize==null)
			receivingTeamRecordSize = 0;
		return receivingTeamRecordSize;
	}

	public void setReceivingTeamRecordSize(Integer receivingTeamRecordSize) {
		this.receivingTeamRecordSize = receivingTeamRecordSize;
	}

	public HtmlCommandButton getBtnAddTeamMembers() {
		return btnAddTeamMembers;
	}

	public void setBtnAddTeamMembers(HtmlCommandButton btnAddTeamMembers) {
		this.btnAddTeamMembers = btnAddTeamMembers;
	}

	public HtmlCommandLink getCmdLnkViolationActions() {
		return cmdLnkViolationActions;
	}

	public void setCmdLnkViolationActions(HtmlCommandLink cmdLnkViolationActions) {
		this.cmdLnkViolationActions = cmdLnkViolationActions;
	}

	public HtmlCommandLink getCmdLnkDelViolation() {
		return cmdLnkDelViolation;
	}

	public void setCmdLnkDelViolation(HtmlCommandLink cmdLnkDelViolation) {
		this.cmdLnkDelViolation = cmdLnkDelViolation;
	}

	public HtmlCommandLink getCmdLnkCloseViolation() {
		return cmdLnkCloseViolation;
	}

	public void setCmdLnkCloseViolation(HtmlCommandLink cmdLnkCloseViolation) {
		this.cmdLnkCloseViolation = cmdLnkCloseViolation;
	}


	@SuppressWarnings("unchecked")
	public String  btnEditViolation_Click ()
	{

		 ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		 Integer index =  dataTableViolation.getRowIndex();
		 viewRootMap.put("index", index);
		 sessionMap.put("VIOLATION_VIEW_FOR_EDIT", vV);
		 sessionMap.put("SESSION_INSPECTION_UNITS",viewMap.get("SESSION_INSPECTION_UNITS"));
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 String javaScriptText = "javascript:showAddViolationPopup();";
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
		 return "EditViolation";
		
	}
	
	@SuppressWarnings("unchecked")
	public String  btnAddViolationAction_Click (){

		ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		viewRootMap.put("index", dataTableViolation.getRowIndex());
		sessionMap.put("VIOLATION_VIEW_FOR_EDIT", vV);
		sessionMap.put("ADD_VIOLATION_ACTION", true);
		sessionMap.put("SESSION_INSPECTION_UNITS",viewMap.get("SESSION_INSPECTION_UNITS"));
		String javaScriptText = "javascript:showAddViolationPopup();";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
		
		return "AddViolationAction";
		
	}
	
	
	public String btnDeleteAddedViolation_Click(){
	
		
		
		ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		Integer index =  dataTableViolation.getRowIndex();
		viewRootMap.put("delete", index);
		
		Boolean flage = false;
		successMessages = new ArrayList<String>();
		try {
			flage = psa.setInspectionViolationToDeleted(vV.getViolationId());
			
			if(flage){
				index = (Integer) viewRootMap.get("delete");
				List<ViolationView> tempList = getViolationViewDataList();
				tempList.remove(index.intValue());
				setViolationViewDataList(tempList);
				
				if(tempList!=null )
				 viewMap.put("violationRecordSize",tempList.size());
				
			successMessages.add(CommonUtil.getBundleMessage("violation.msg.DeleteViolation"));
			}
			
		} catch (PimsBusinessException e) {
			logger.LogException("Exception in btnDeleteViolation_Click::::::::::::::", e);
		}	
			
		return "EditViolation";
		
		
	}


	public HtmlTab getViolationTab() {
		return violationTab;
	}

	public void setViolationTab(HtmlTab violationTab) {
		this.violationTab = violationTab;
	}

	@SuppressWarnings("unchecked")
	public void openUnitPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openUnitPopup() started...");
		try
		{
			List<InspectionUnitView> allUnits = getInspectionUnitsView();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.ALREADY_SELECTED_PROPERTY_UNITS, allUnits);			
			
	        final String viewId = "/inspectionDetails.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	        String javaScriptText = "javascript:showUnitSearchPopUp();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openUnitPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openUnitPopup() crashed ", exception);
		}
    }

	public Integer getViolationRecordSize() {
		violationRecordSize = (Integer) viewMap.get("violationRecordSize");
		if(violationRecordSize==null)
			violationRecordSize = 0;
		return violationRecordSize;
	}

	public void setViolationRecordSize(Integer violationRecordSize) {
		this.violationRecordSize = violationRecordSize;
	}

	public HtmlInputTextarea getInspectionDescription() {
		return inspectionDescription;
	}

	public void setInspectionDescription(HtmlInputTextarea inspectionDescription) {
		this.inspectionDescription = inspectionDescription;
	}

	public Boolean getActionColume() {
		if(viewRootMap.containsKey("ACTION_COLUME"))
		 actionColume =(Boolean) viewRootMap.get("ACTION_COLUME");
		return actionColume;
	}

	public void setActionColume(Boolean actionColume) {
		this.actionColume = actionColume;
		
		if(this.actionColume!=null)
			viewRootMap.put("ACTION_COLUME",this.actionColume);
		
	}

	public HtmlCommandButton getAddViolationBtn() {
		return addViolationBtn;
	}

	public void setAddViolationBtn(HtmlCommandButton addViolationBtn) {
		this.addViolationBtn = addViolationBtn;
	}
	
	public void addEditInspectionMode()
	{
		violationTab.setRendered(false);
		btnAddUnits.setRendered(true);
		cmdDelUnit.setRendered(true);
		btnAddTeamMembers.setRendered(true);
		deleteLink.setRendered(true);
		addViolationTeam.setRendered(true);
		comboInspectionType.setReadonly(false);
		calendarInspectionFrom.setDisabled(false);
		btnAdd.setRendered(true);
		inspectionDescription.setReadonly(false);
		setActionColume(true);
		addViolationBtn.setRendered(false);
		
		
		send.setRendered(false);
		assignViolationActions.setRendered(false);
		sendForRecommendation.setRendered(false);
		sendForClosingViolation.setRendered(false);
		sendForViolationAction.setRendered(false);
		closeViolation.setRendered(false);
		
		
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
	
	}
	
	public void addEditInspectionViolationMode()
	{
		violationTab.setRendered(true);
		btnAddUnits.setRendered(false);
		cmdDelUnit.setRendered(false);
		btnAddTeamMembers.setRendered(true);
		deleteLink.setRendered(true);
		addViolationTeam.setRendered(true);
		comboInspectionType.setReadonly(false);
		calendarInspectionFrom.setDisabled(false);
		btnAdd.setRendered(true);
		inspectionDescription.setReadonly(false);
		setActionColume(true);
		setActionColumeForVoilation(true);
		addViolationBtn.setRendered(true);
		
		send.setRendered(true);
		assignViolationActions.setRendered(false);
		sendForRecommendation.setRendered(false);
		sendForClosingViolation.setRendered(false);
		sendForViolationAction.setRendered(false);
		closeViolation.setRendered(false);
		
		cmdLnkEditViolation.setRendered(true);
		cmdLnkViolationActions.setRendered(true);
		cmdLnkDelViolation.setRendered(true);
		
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
	
	}
	
	public void addViolationActionsMode()
	{
		violationTab.setRendered(true);
		btnAddUnits.setRendered(false);
		cmdDelUnit.setRendered(false);
		btnAddTeamMembers.setRendered(false);
		deleteLink.setRendered(false);
		addViolationTeam.setRendered(false);
		comboInspectionType.setReadonly(true);
		calendarInspectionFrom.setDisabled(true);
		btnAdd.setRendered(false);
		inspectionDescription.setReadonly(true);
		setActionColume(false);
		setActionColumeForVoilation(true);
		addViolationBtn.setRendered(false);
		
		send.setRendered(false);
		assignViolationActions.setRendered(true);
		sendForRecommendation.setRendered(true);
		sendForClosingViolation.setRendered(false);
		sendForViolationAction.setRendered(false);
		closeViolation.setRendered(false);
		
		
		
		cmdLnkEditViolation.setRendered(false);
		cmdLnkViolationActions.setRendered(true);
		cmdLnkDelViolation.setRendered(false);
		
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
	
	}
	
	public void addViolationRecommendationMode()
	{
		violationTab.setRendered(true);
		btnAddUnits.setRendered(false);
		cmdDelUnit.setRendered(false);
		btnAddTeamMembers.setRendered(false);
		deleteLink.setRendered(false);
		addViolationTeam.setRendered(false);
		comboInspectionType.setReadonly(true);
		calendarInspectionFrom.setDisabled(true);
		btnAdd.setRendered(false);
		inspectionDescription.setReadonly(true);
		setActionColume(false);
		setActionColumeForVoilation(true);
		addViolationBtn.setRendered(false);
		
		send.setRendered(false);
		assignViolationActions.setRendered(false);
		sendForRecommendation.setRendered(false);
		sendForClosingViolation.setRendered(true);
		sendForViolationAction.setRendered(true);
		closeViolation.setRendered(false);
		
		
		
		cmdLnkEditViolation.setRendered(false);
		cmdLnkViolationActions.setRendered(false);
		cmdLnkDelViolation.setRendered(false);
		
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
	
	}
	
	public void addViolationCLoseMode()
	{
		violationTab.setRendered(true);
		btnAddUnits.setRendered(false);
		cmdDelUnit.setRendered(false);
		btnAddTeamMembers.setRendered(false);
		deleteLink.setRendered(false);
		addViolationTeam.setRendered(false);
		comboInspectionType.setReadonly(true);
		calendarInspectionFrom.setDisabled(true);
		btnAdd.setRendered(false);
		inspectionDescription.setReadonly(true);
		setActionColume(false);
		setActionColumeForVoilation(true);
		addViolationBtn.setRendered(false);
		
		send.setRendered(false);
		assignViolationActions.setRendered(false);
		sendForRecommendation.setRendered(false);
		sendForClosingViolation.setRendered(false);
		sendForViolationAction.setRendered(false);
		closeViolation.setRendered(true);
		
		
		
		cmdLnkEditViolation.setRendered(false);
		cmdLnkViolationActions.setRendered(true);
		cmdLnkDelViolation.setRendered(false);
		
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
	
	}
	
	public void viewMode()
	{
		violationTab.setRendered(true);
		btnAddUnits.setRendered(false);
		cmdDelUnit.setRendered(false);
		btnAddTeamMembers.setRendered(false);
		deleteLink.setRendered(false);
		addViolationTeam.setRendered(false);
		comboInspectionType.setReadonly(true);
		calendarInspectionFrom.setDisabled(true);
		btnAdd.setRendered(false);
		inspectionDescription.setReadonly(true);
		setActionColume(false);
		setActionColumeForVoilation(false);
		addViolationBtn.setRendered(false);
		
		send.setRendered(false);
		assignViolationActions.setRendered(false);
		sendForRecommendation.setRendered(false);
		sendForClosingViolation.setRendered(false);
		sendForViolationAction.setRendered(false);
		closeViolation.setRendered(false);
		
		
		
		cmdLnkEditViolation.setRendered(false);
		cmdLnkViolationActions.setRendered(false);
		cmdLnkDelViolation.setRendered(false);
		
		viewMap.put("canAddAttachment", false);
		viewMap.put("canAddNote", false);
	
	}

	
	
	
	
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	
	public String sendToInspectionSearch(){
		return "InspectionSearch";
	}

	public HtmlCommandButton getAssignViolationActions() {
		return assignViolationActions;
	}

	public void setAssignViolationActions(HtmlCommandButton assignViolationActions) {
		this.assignViolationActions = assignViolationActions;
	}

	public HtmlCommandButton getSend() {
		return send;
	}

	public void setSend(HtmlCommandButton send) {
		this.send = send;
	}

	public HtmlCommandLink getCmdLnkEditViolation() {
		return cmdLnkEditViolation;
	}

	public void setCmdLnkEditViolation(HtmlCommandLink cmdLnkEditViolation) {
		this.cmdLnkEditViolation = cmdLnkEditViolation;
	}

	public Boolean getActionColumeForVoilation() {
		if(viewRootMap.containsKey("ACTION_COLUME_FOR_VIOLATION"))
			actionColumeForVoilation =(Boolean) viewRootMap.get("ACTION_COLUME_FOR_VIOLATION");
		return actionColumeForVoilation;
	}

	public void setActionColumeForVoilation(Boolean actionColumeForVoilation) {
		this.actionColumeForVoilation = actionColumeForVoilation;
		
		if(this.actionColumeForVoilation!=null)
			viewRootMap.put("ACTION_COLUME_FOR_VIOLATION",this.actionColumeForVoilation);
	}

	public HtmlCommandButton getSendForRecommendation() {
		return sendForRecommendation;
	}

	public void setSendForRecommendation(HtmlCommandButton sendForRecommendation) {
		this.sendForRecommendation = sendForRecommendation;
	}

	public HtmlCommandButton getSendForClosingViolation() {
		return sendForClosingViolation;
	}

	public void setSendForClosingViolation(HtmlCommandButton sendForClosingViolation) {
		this.sendForClosingViolation = sendForClosingViolation;
	}

	public HtmlCommandButton getSendForViolationAction() {
		return sendForViolationAction;
	}

	public void setSendForViolationAction(HtmlCommandButton sendForViolationAction) {
		this.sendForViolationAction = sendForViolationAction;
	}

	public HtmlCommandButton getCloseViolation() {
		return closeViolation;
	}

	public void setCloseViolation(HtmlCommandButton closeViolation) {
		this.closeViolation = closeViolation;
	}
}
