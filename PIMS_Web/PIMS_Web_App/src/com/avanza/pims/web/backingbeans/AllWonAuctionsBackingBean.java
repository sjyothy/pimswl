package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;

import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.UnitView;

public class AllWonAuctionsBackingBean extends AbstractController {
	
	
	
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String auctionNumber;
	private String unitNumber;
	private String bidderName;
	private Date auctionStartDate;
	private Date auctionEndDate;
	private String dateformat="";
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private List<AuctionUnitView> auctionUnitViews;
	
	private HtmlDataTable dataTable;
	
	 private static Logger logger = Logger.getLogger(AllWonAuctionsBackingBean.class);
	 private Integer paginatorMaxPages = 0;
		Integer paginatorRows=0;
		Integer recordSize=0;
	   
	@Override
	public void init() {
		
		logger.logInfo("Stepped into the init method");
		// TODO Auto-generated method stub	
		try{
		super.init();	
		
		if (!isPostBack()){
			//searchAllWonAuctions();
		
		}
		}catch(Exception e){
			logger.logError("init crashed due to:"+e.getStackTrace());
		}
		logger.logInfo("init method completed Succesfully");
	}
	
	public String searchAllWonAuctions(){
		
		logger.logInfo("Stepped into the searchAllWonAuctions method");
		
		HashMap auctionHashMap = new HashMap();
		AuctionView auctionView = new AuctionView();
		DateFormat df=new SimpleDateFormat(getDateformat());
		try {
			
			if (unitNumber != null && unitNumber.trim().length()>0)
				auctionHashMap.put("unitNumber", unitNumber.trim());
			
			if (bidderName != null && bidderName.trim().length()>0)
				auctionHashMap.put("bidderName", bidderName.trim());
			
			if (auctionNumber != null && auctionNumber.trim().length()>0){
				
				auctionView.setAuctionNumber(auctionNumber.trim());
				auctionHashMap.put("auctionView", auctionView);
			}
			if (auctionStartDate != null ){
				
				auctionView.setAuctionStartDatetime(auctionStartDate);
				auctionHashMap.put("auctionView", auctionView);
			}	
			
			if (auctionEndDate != null ){
				
				auctionView.setAuctionEndDatetime(auctionEndDate);
				auctionHashMap.put("auctionView", auctionView);
			}	
			
			auctionUnitViews = psa.getAllWonAuctions(auctionHashMap);
			  if(auctionUnitViews !=null)
					recordSize = auctionUnitViews .size();
				viewRootMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize/paginatorRows;
				if((recordSize%paginatorRows)>0)
					paginatorMaxPages++;
				if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
		        
			viewRootMap.put("ALL_WON_AUCTION_LIST", auctionUnitViews );
			logger.logInfo("searchAllWonAuctions method completed Succesfully");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError("searchAllWonAuctions crashed due to:"+e.getStackTrace());
		}
		
		
		return "";
	}
	@SuppressWarnings( "unchecked" )
	 public void sendAuctionUnitInfoToParent(javax.faces.event.ActionEvent event)
	 {
		    String method="sendUnitInfoToParent";
	    	//Write your server side code here
		    logger.logInfo(method+"|"+"Start..");
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        AuctionUnitView aucionUnitViewRow=(AuctionUnitView)dataTable.getRowData();
	        String javaScriptText ="window.opener.populateAuctionUnit(" +
			                                                     "'"+aucionUnitViewRow.getUnitId().toString()+"'," +
			                                                     "'"+aucionUnitViewRow.getUnitNumber().toString()+"'," +
			                                                     "'"+aucionUnitViewRow.getUnitUsageTypeID().toString()+"',"+
			                                                     "'"+aucionUnitViewRow.getOpeningPrice().toString()+"',"+
			                                                     "'"+aucionUnitViewRow.getAuctionUnitId().toString()+"',"+
			                                                     "'"+aucionUnitViewRow.getAuctionNumber().toString()+"',"+
			                                                     "'"+aucionUnitViewRow.getPropertyAdress()+"',"+
			                                                     "'"+aucionUnitViewRow.getPropertyCommercialName().toString()+"',"+
			                                                     "'"+(getIsEnglishLocale()?aucionUnitViewRow.getUnitUsageTypeEn():aucionUnitViewRow.getUnitUsageTypeAr())+"',"+
			                                                     "'"+(getIsEnglishLocale()?aucionUnitViewRow.getUnitStatusEn():aucionUnitViewRow.getUnitStatusAr())+"',"+
			                                                     "'"+aucionUnitViewRow.getUnitStatusId()+"',"+
			                                                     "'"+aucionUnitViewRow.getBidderId()+"'"+
			                                                     ");"+
	        		               "window.opener.document.forms[0].submit();" +
	        		               "window.close();"; 
	        	
	        Map sessionMap=facesContext.getExternalContext().getSessionMap();
	        sessionMap.put("AUCTIONUNITINFO", aucionUnitViewRow);
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
	        logger.logInfo(method+"|"+"Finish..");
	    }
	 public String getLocale(){
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode();
		}

	public String getDateformat() {
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		dateformat= localeInfo.getDateFormat();
		return dateformat;
	}
     
	
	

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
	
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		//FacesContext context = FacesContext.getCurrentInstance();
		//HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		//UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		//isEnglishLocale =  view.getLocale().toString().equals("en");
		
		
		
		return isEnglishLocale;
	}

	public String getAuctionNumber() {
		return auctionNumber;
	}

	public void setAuctionNumber(String auctionNumber) {
		this.auctionNumber = auctionNumber;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getBidderName() {
		return bidderName;
	}

	public void setBidderName(String bidderName) {
		this.bidderName = bidderName;
	}

	public List<AuctionUnitView> getAuctionUnitViews() {
		auctionUnitViews=new ArrayList<AuctionUnitView>(0);
		if(viewRootMap.containsKey("ALL_WON_AUCTION_LIST"))
				auctionUnitViews=(ArrayList<AuctionUnitView>)viewRootMap.get("ALL_WON_AUCTION_LIST");
		
			
		return auctionUnitViews;
	}

	public void setAuctionUnitViews(List<AuctionUnitView> auctionUnitViews) {
		this.auctionUnitViews = auctionUnitViews;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	
	public Date getAuctionStartDate() {
		return auctionStartDate;
	}

	public void setAuctionStartDate(Date auctionStartDate) {
		this.auctionStartDate = auctionStartDate;
	}

	public Date getAuctionEndDate() {
		return auctionEndDate;
	}

	public void setAuctionEndDate(Date auctionEndDate) {
		this.auctionEndDate = auctionEndDate;
	}


	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	
	
}
	
	
		
	