package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.EIDDataReader;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AssignedPersonTypeView;
import com.avanza.pims.ws.vo.CardData;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
public class SearchPerson extends AbstractController
{
	protected static SystemParameters parameters = SystemParameters.getInstance();
	private static Logger logger = Logger.getLogger( SearchPerson.class );
	final String REQUESTED_PERSON_TYPE="REQUESTED_PERSON_TYPE"; 
	private HtmlDataTable dataTable;
	private String ef_idn_cn; 
	private String ef_mod_data;
	private String ef_non_mod_data;
	private String ef_sign_image;
	private String ef_home_address;
	private String ef_work_address;
	private String firstName  ;
	private String lastName ;
	private Long personTypeId;
	private String passportNumber;
	private String residenseVisaNumber;
	private String personalSecCardNo;
	private String drivingLicenseNumber;
	private String socialSecNumber;
	private String cellNumber;
	private String designation;
	private String nationalityId;
	private String selectedPersonKind;
	private String licenseNumber;
	private String licenseSource;
	private String visaIssuePlace;
	private String passportIssuePlace;
	private Boolean isBlackListed;
	private String status;
	private String grpNumber;
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private String countryId;
	private PersonView personView = new PersonView();
	String controlName;
	String controlForId;
	String requestForPersonType;
	String displayStylePersonTypeCombo;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    List<SelectItem> personTypeList =new ArrayList<SelectItem>();
	public boolean selectManyPersonType;
	CommonUtil commonUtil=new CommonUtil();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
    Map viewRootMap;	
	String VIEW_MODE="VIEW_MODE";
	String SELECT_MODE="SELECT_MODE";
    String sessionName="";
    private String  DEFAULT_SORT_FIELD = "socialSecNumber";
    private String contractNumber; 
    
	private String loginID;
    PersonView personFromEmiratesCardScan = new PersonView();
    
    @SuppressWarnings("unchecked")
	public PersonView getPersonFromEmiratesCardScan() {
		if( viewMap.containsKey( "personFromEmiratesCardScan") )
		{
			personFromEmiratesCardScan = (PersonView)viewMap.get( "personFromEmiratesCardScan");
		}
		return personFromEmiratesCardScan;
	}
    
	@SuppressWarnings("unchecked")
	public void setPersonFromEmiratesCardScan(PersonView personFromEmiratesCardScan) {
		this.personFromEmiratesCardScan = personFromEmiratesCardScan;
		if( this.personFromEmiratesCardScan !=null )
		{
			viewMap.put("personFromEmiratesCardScan",this.personFromEmiratesCardScan );
		}
	}
	public String getEf_idn_cn() {
		if( viewMap.containsKey( "ef_idn_cn") )
		{
			ef_idn_cn = viewMap.get("ef_idn_cn").toString() ;
			
		}
			
		return ef_idn_cn;
	}
	public void setEf_idn_cn(String ef_idn_cn) {
		this.ef_idn_cn = ef_idn_cn;
	}
	public String getEf_mod_data() {
		if( viewMap.containsKey( "ef_mod_data") )
		{
			ef_mod_data= viewMap.get("ef_mod_data").toString() ;
			
		}
		return ef_mod_data;
	}
	public void setEf_mod_data(String ef_mod_data) {
		
		this.ef_mod_data = ef_mod_data;
	}
	public String getEf_non_mod_data() {
		if( viewMap.containsKey( "ef_non_mod_data")  )
		{
			ef_non_mod_data= viewMap.get("ef_non_mod_data").toString() ;
			
		}
		return ef_non_mod_data;
	}
	public void setEf_non_mod_data(String ef_non_mod_data) {
		this.ef_non_mod_data = ef_non_mod_data;
	}
	public String getEf_sign_image() {
		if( viewMap.containsKey( "ef_sign_image")  )
		{
			ef_sign_image= viewMap.get("ef_sign_image").toString() ;
			
		}
		return ef_sign_image;
	}
	public void setEf_sign_image(String ef_sign_image) {
		this.ef_sign_image = ef_sign_image;
	}
	public String getEf_home_address() {
		if( viewMap.containsKey( "ef_home_address"))
		{
			ef_home_address= viewMap.get("ef_home_address").toString() ;
			
		}
		return ef_home_address;
	}
	public void setEf_home_address(String ef_home_address) {
		this.ef_home_address = ef_home_address;
	}
	public String getEf_work_address() {
		if( viewMap.containsKey( "ef_work_address") )
		{
			ef_work_address= viewMap.get("ef_work_address").toString() ;
			
		}
		return ef_work_address;
	}
	public void setEf_work_address(String ef_work_address) {
		this.ef_work_address = ef_work_address;
	}
	@Override 
	@SuppressWarnings("unchecked")	
	 public void init() 
     {
    	
    	 super.init();
    	 try
    	 {
    		 viewRootMap = getFacesContext().getViewRoot().getAttributes();
    		 
    		 if(!isPostBack())
    		 {
    			    setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
			        setSortField(DEFAULT_SORT_FIELD);
			        setSortItemListAscending(true);
			        viewRootMap.put("recordSize",0);
    			   getQueryString();
    			   loadCombos();
        		 
    		 }
    		 
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("init|Error Occured", es);
    		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		 
    	 }
	        
	 }
		private void loadCombos()
		{
			loadCountry();
		}
		@SuppressWarnings("unchecked")
		private void getQueryString() {
			HttpServletRequest request =
				 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			 
			if(request.getParameter("isCompany") != null)
			{
				boolean  isCompany = Boolean.parseBoolean(request.getParameter("isCompany"));
				if(isCompany )
				viewRootMap.put("IS_COMPANY", true);
			}
			else if(getRequestParam("isCompany")!=null)
			 {
				boolean  isCompany = Boolean.parseBoolean(request.getParameter("isCompany"));
				if(isCompany )
				viewRootMap.put("IS_COMPANY", true);
			 }
			
			if(request.getParameter("persontype")!=null)
			 {
				 requestForPersonType=request.getParameter("persontype").toString();
			     viewRootMap.put(REQUESTED_PERSON_TYPE, requestForPersonType);
				 loadPersonTypeCombo();
			 }
			 else if(getRequestParam("persontype")!=null)
			 {
				 requestForPersonType= getRequestParam("persontype").toString();
			     viewRootMap.put(REQUESTED_PERSON_TYPE, requestForPersonType);
				 loadPersonTypeCombo();
			 }
			 if(request.getParameter(WebConstants.SELECT_BLACK_LIST)!=null)
			 {
			     viewRootMap.put(WebConstants.SELECT_BLACK_LIST, true);
				 
			 }
			 else if(getRequestParam(WebConstants.SELECT_BLACK_LIST)!=null)
			 {
				 viewRootMap.put(WebConstants.SELECT_BLACK_LIST, true);
			 }
			 if(request.getParameter("viewMode")!=null)
				 viewRootMap.put(VIEW_MODE, request.getParameter("viewMode").toString());
			 else if(getRequestParam("viewMode")!=null)
				 viewRootMap.put(VIEW_MODE, getRequestParam("viewMode").toString());
			 
			 if(request.getParameter("selectMode")!=null)
				 viewRootMap.put(SELECT_MODE, request.getParameter("selectMode").toString());
			 else if(getRequestParam("selectMode")!=null)
				 viewRootMap.put(SELECT_MODE, getRequestParam("selectMode").toString());
			 else if(sessionMap.containsKey(SELECT_MODE)&& sessionMap.get(SELECT_MODE)!=null)
			 {
				 viewRootMap.put(SELECT_MODE,sessionMap.get(SELECT_MODE));
				 sessionMap.remove(SELECT_MODE);
			 }
			 if(request.getParameter("displaycontrolname")!=null)
				controlName=request.getParameter("displaycontrolname").toString();
			 else if(getRequestParam("displaycontrolname")!=null)
				 controlName= getRequestParam("displaycontrolname").toString();
			
			 if(request.getParameter("hdncontrolname")!=null)
				controlForId=request.getParameter("hdncontrolname").toString();
			 else if(getRequestParam("hdncontrolname")!=null)
				 controlForId= getRequestParam("hdncontrolname").toString();
		}
		public boolean getSelectManyPersonType() 
	     {
	    	 selectManyPersonType = false;
	    	 if(viewRootMap.containsKey(SELECT_MODE) && viewRootMap.get(SELECT_MODE).toString().equalsIgnoreCase("many"))
	    		 selectManyPersonType = true;
	    			 
			return selectManyPersonType;
		 }
	 public boolean getIsViewModePopUp()
	 {
		 
		 
		 if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase("popup"))
			 return true;
		 else 
			 return false;
	 }
	 private void loadPersonTypeCombo() 
	 {
		 PropertyServiceAgent psa=new PropertyServiceAgent();
		 try
		 {
			 if(!viewRootMap.containsKey(WebConstants.PERSON_TYPE) )
			 {
				 
				 List<DomainDataView> domainDataList=psa.getDomainDataByDomainTypeName(WebConstants.PERSON_TYPE);
				 viewRootMap.put(WebConstants.PERSON_TYPE, domainDataList);
				 
			 }
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 logger.logError("loadPersonTypeCombo| Error Occured", ex);
			 
		 }
		 
	 }
	 
		//We have to look for the list population for facilities
	private List<String> errorMessages = new ArrayList<String>();
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private PersonView dataItem = new PersonView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<PersonView> dataList = new ArrayList<PersonView>();
  
	//	public Unit dataItem = new Unit();
	public String btnAdd_Click()
	{
		
		
		if(viewRootMap.containsKey(REQUESTED_PERSON_TYPE))
		   setRequestParam("PERSON_TYPE", viewRootMap.get(REQUESTED_PERSON_TYPE));
		if(getIsViewModePopUp())
		   setRequestParam("viewMode", "popup");
		if(viewRootMap.containsKey(WebConstants.SELECT_BLACK_LIST))
			setRequestParam(WebConstants.SELECT_BLACK_LIST, true);
		//Here we want to track that whether popup is in multiselect mode or single select mode. 
		//This value will be used in AddPerson.java in getQueryString()
		sessionMap.put("MANY_PERSON",getSelectManyPersonType());
		return "addPerson";
		
	}
	public String cmdEdit_Click()
	{
		PersonView dataItem = new PersonView();
		if(dataTable.getRowCount()>0 && dataTable.getRowData()!=null)
		   dataItem =(PersonView)dataTable.getRowData();
		
		if(viewRootMap.containsKey(REQUESTED_PERSON_TYPE))
		   setRequestParam("PERSON_TYPE", viewRootMap.get(REQUESTED_PERSON_TYPE));
		setRequestParam(WebConstants.PERSON_ID,dataItem.getPersonId());
		if(getIsViewModePopUp())
			   setRequestParam("viewMode", "popup");
		return "editPerson";
		
	}

	@SuppressWarnings("unchecked")
	public List<PersonView> getPropertyInquiryDataList()
	{
		if (dataList == null || dataList.size()==0)
		{
			if(viewMap.containsKey("PERSON_LIST"))	
				  dataList= (ArrayList)viewMap.get("PERSON_LIST");
		}
		return dataList;
	}
	
	private void ClearEmiratesCardRelatedFields()
	{
		this.setSocialSecNumber("");
		this.setCellNumber("");
		this.setPassportNumber("");
		this.setFirstName(  "");
	}
	
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCardAndSearchByCardNum()
	{
		try 
		{
			ClearEmiratesCardRelatedFields();
			personFromEmiratesCardScan = this.getPersonFromEmiratesCardScan();
			
			this.setSocialSecNumber(personFromEmiratesCardScan.getSocialSecNumber());
			doSearchItemList();
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		  logger.LogException("onScanEmiratesCardAndSearchByCardNum|Error Occured:", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCardAndSearchByCellNum()
	{
		try 
		{
			ClearEmiratesCardRelatedFields();
			personFromEmiratesCardScan = this.getPersonFromEmiratesCardScan();
			this.setCellNumber( personFromEmiratesCardScan.getCellNumber() );
			if( cellNumber != null && cellNumber.trim().length()>0 )
			{
				doSearchItemList();
			}
			else
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("searchPerson.msg.cellNumberNotPresentInCard"));
			}
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCardAndSearchByNameArabic()
	{
		try 
		{
			ClearEmiratesCardRelatedFields();
			personFromEmiratesCardScan = this.getPersonFromEmiratesCardScan();
			this.setFirstName( personFromEmiratesCardScan.getPersonFullName() );
			doSearchItemList();
			
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCardAndSearchByNameEnglish()
	{
		try 
		{
			ClearEmiratesCardRelatedFields();
			personFromEmiratesCardScan = this.getPersonFromEmiratesCardScan();
			this.setFirstName( personFromEmiratesCardScan.getFullNameEn() );
			doSearchItemList();
			
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCardAndSearchByPassport()
	{
		
		try 
		{
			ClearEmiratesCardRelatedFields();
			personFromEmiratesCardScan = this.getPersonFromEmiratesCardScan();
			this.setPassportNumber(personFromEmiratesCardScan.getPassportNumber());
			if( this.getPassportNumber()!= null && this.getPassportNumber().trim().length()>0 )
			{
				doSearchItemList();
			}
			else
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("searchPerson.msg.passportNumberNotPresentInCard"));
			}
		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void onScanEmiratesCard()
	{
		String dataFromReader="" ;
		
		try 
		{
			ClearEmiratesCardRelatedFields();
			this.setPersonFromEmiratesCardScan( new PersonView() );
			String endPoint= parameters.getParameter("CardDataWS");
			String postInfo = getPostInfo();
			dataFromReader = EIDDataReader.callUrlJSON(endPoint, postInfo);
			
			logger.logInfo(dataFromReader);
	        Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();    
	        CardData input = jsonObj.fromJson(dataFromReader, CardData.class);
	        
	        personFromEmiratesCardScan = input.getPersonDetailsFromCardData();
	        this.setPersonFromEmiratesCardScan(personFromEmiratesCardScan);

		}
		catch (Exception e) 
		{
		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 logger.LogException("onScanEmiratesCard|Error Occured:", e);
		}
		
	}
	private String getPostInfo() throws Exception
	{
		
		String postInfo = "{ \"ef_idn_cn\":\""+ getEf_idn_cn()+ "\", "+
				  "\"ef_mod_data\":\""+ getEf_mod_data()+"\","+
				  "\"ef_non_mod_data\":\""+ getEf_non_mod_data()+"\","+
				  "\"ef_home_address\":\""+ getEf_home_address()+"\","+
				  "\"ef_work_address\":\""+ getEf_work_address()+"\"}";
		
		return postInfo;
				  
		
	}
	@SuppressWarnings("unchecked")
	public String doSearch (){
                      logger.logInfo("doSearch Starts:::::::::::::::::::::::");
                    try {
                            putControlValuesinView();
		                    dataList.clear();
		                    if(sessionMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED) != null)
		                    {
		                    	
		                    	viewRootMap.put(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED, sessionMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED));
                    			sessionMap.remove(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED);
		                    }
		                    loadDataList();
		                    logger.logInfo("doSearch Ends:::::::::::::::::::::::");
	                    }
                    catch (Exception e){
                    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                    	logger.LogException("doSearch Crashed:", e);
                    }
                return "";

     }
	/**
	 * 
	 */
	private void putControlValuesinView() {
		
		if(this.selectedPersonKind!=null && !this.selectedPersonKind.equals("-1"))
		{
			if(this.selectedPersonKind.equals("1"))
 			   personView.setIsCompany(1L);
			else
			   personView.setIsCompany(0L);
		}
		else if(viewRootMap.get("IS_COMPANY") != null)
		{
			   personView.setIsCompany(1L);
		}
		if( grpNumber !=null && grpNumber.trim().length() > 0 )
			personView.setGrpCustomerNo(grpNumber);
		if (firstName !=null && !firstName.equals(""))
			personView.setFirstName(firstName.trim().replace(" ", ""));                           	
		if(personalSecCardNo!=null && !personalSecCardNo.equals(""))
			personView.setPersonalSecCardNo(personalSecCardNo);
		if(this.passportIssuePlace!=null && !this.passportIssuePlace.equals("-1"))
			personView.setPassportIssuePlaceId(new Long(this.passportIssuePlace));
		if(passportNumber!=null && !passportNumber.equals(""))
			personView.setPassportNumber(passportNumber);
		if(this.visaIssuePlace!=null && !this.visaIssuePlace.equals("-1"))
			personView.setResidenseVisaIssuePlaceId(new Long(this.visaIssuePlace));
		if(residenseVisaNumber!=null && !residenseVisaNumber.equals(""))
			personView.setResidenseVisaNumber(residenseVisaNumber);
		if(drivingLicenseNumber!=null && !drivingLicenseNumber.equals(""))
			personView.setDrivingLicenseNumber(drivingLicenseNumber);
		if(socialSecNumber!=null && !socialSecNumber.equals(""))
			personView.setSocialSecNumber(socialSecNumber.toLowerCase());
		if(status!=null && status.length()>0 && !status.equals("-1"))
			personView.setStatusId(new Long(this.status));
		if(this.licenseSource!=null && !this.licenseSource.equals("-1"))
			personView.setLicenseIssuePlaceId(new Long(this.licenseSource));
		if(licenseNumber!=null && !licenseNumber.trim().equals(""))
			personView.setLicenseNumber(licenseNumber.toLowerCase());
		if(this.getIsBlackListed() !=null && this.getIsBlackListed()==true)
			personView.setIsBlacklisted(1L);
		if(this.getNationalityId() != null && this.getNationalityId().length() > 0)
			personView.setNationalityId(new Long(this.getNationalityId()));
		if( contractNumber !=null && contractNumber.trim().length() > 0 )
			personView.setContractNumber(contractNumber.trim());
		if( cellNumber  !=null && cellNumber.trim().length() > 0 )
			personView.setCellNumber( cellNumber.trim() ) ;
		if(  loginID !=null && loginID.trim().length() > 0 )
			personView.setLoginId(loginID.trim() ) ;
		
	}
	public void doSearchItemList()
	{
		putControlValuesinView() ;
		loadDataList();
	}
	@SuppressWarnings("unchecked")
	public void loadCountry()  {
		
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
				  countryList.add(item);
			     
			  }
			
			 viewMap.put("countryList", countryList);
			}catch (Exception e){
				logger.LogException("loadCountry|Error Occured ",e);
				
			}
		}
	@SuppressWarnings("unchecked")
	public List<PersonView> loadDataList() 
	{
		 List<PersonView> list = new ArrayList();
		
		try
		{
			PropertyService psa = new PropertyService();
			
			List<Long> idsToBeExcluded = null;
			if(viewRootMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED) != null)
			{
				idsToBeExcluded = (List<Long>) viewRootMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED);
				personView.setIdsToBeExcluded(idsToBeExcluded);
			}
             /////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  psa.searchPersonGetTotalNumberOfRecords(personView);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
	           list =  psa.getPersonInformationByView(personView,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
	           
	           Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				viewMap.put("PERSON_LIST", list);
				recordSize = totalRows;
				viewMap.put("recordSize", totalRows);
				paginatorRows = getPaginatorRows();
	           
//	           sessionMap.put("UNIT_LIST", list);
		        dataList=(ArrayList<PersonView>) list;
		        
		        viewRootMap.put("PERSONS", dataList);
		        logger.logInfo("Record Found In Load Data List : %s",dataList.size());
		        
		        if (dataList.size()==0){
		        	errorMessages = new ArrayList<String>();
					errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
		        	
		        }
	    }
		catch (Exception ex) 
		{
    			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				logger.LogException( "Crashed:",ex);
	    }
		return list;
		
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}
	@SuppressWarnings("unchecked")
	public void sendSinglePersonToParent()
	{
		PersonView personView = (PersonView)dataTable.getRowData();
		if ( personView.getGrpCustomerNo()==null || personView.getGrpCustomerNo().trim().length() <= 0 )
    	{
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty("selectPerson.msg.GRPNumberNotPresent"));
    		return;
    	}
		FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "javaScript:RowDoubleClick('"+this.getRequestForPersonType()+"','"+
                                                             personView.getPersonFullName()+"','"+
                                                             personView.getPersonId()      +"','"+
                                                            (personView.getPassportNumber()==null ?"":personView.getPassportNumber())+"','"+
                                                            (personView.getCellNumber() ==null ?"":personView.getCellNumber()   ) +"','"+
                                                            (personView.getDesignation()==null ?"":personView.getDesignation()  ) +"','"+
                                                             personView.getIsCompany()     +"');";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

	}
	@SuppressWarnings("unchecked")
	public void sendInfoToParent()
	{
		if(viewRootMap.containsKey(REQUESTED_PERSON_TYPE) && viewRootMap.get(REQUESTED_PERSON_TYPE).toString().trim().length()>0)
 			 sessionName=viewRootMap.get(REQUESTED_PERSON_TYPE).toString().trim();
		
		List<PersonView> personViewList = new ArrayList<PersonView>();
			
		if(viewRootMap.containsKey("PERSONS") && viewRootMap.get("PERSONS")!=null)
			personViewList = (ArrayList<PersonView> ) viewRootMap.get("PERSONS"); 
		List<PersonView> newpersonViewList= new ArrayList<PersonView>();
		if(sessionName!=null && sessionName.length()>0)
		{
			List<PersonView> alreadySelectedPersonsList =new ArrayList<PersonView>();
			
			if(sessionMap.containsKey(sessionName) && sessionMap.get(sessionName)!=null)
			{
				alreadySelectedPersonsList = (ArrayList<PersonView>)sessionMap.get(sessionName);	
			}
			
			for (PersonView personView : personViewList) 
			{
			  if(personView.getSelected() && !personAlreadyInSession(personView))
			  {
				  newpersonViewList.add(personView);
			  }
			}
			if(alreadySelectedPersonsList.size()>0)
				newpersonViewList.addAll(alreadySelectedPersonsList);
			sessionMap.put(sessionName, newpersonViewList);
		}
		else
		{
			for (PersonView personView : personViewList) 
			{
			  if(personView.getSelected() )
			  {
				  newpersonViewList.add(personView);
			  }
			}
			sessionMap.put(WebConstants.SELECTED_PERSONS_LIST, newpersonViewList);
		}
		openPopUp();
	}
	public String openPopUp()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.opener.document.forms[0].submit();" +
                                "window.close();";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	private boolean personAlreadyInSession(PersonView editpersonView) 
	{
	    boolean isPersonInSession=false;
		List<PersonView> previouspersonViewList =new ArrayList<PersonView>();
		try
		{
			if(sessionMap.containsKey(sessionName)  && sessionMap.get(sessionName)!=null)
			{
			 	previouspersonViewList= (ArrayList)sessionMap.get(sessionName);
			 	for(int i=0;i<previouspersonViewList.size();i++)
		        {
		             PersonView previousPreviousView=(PersonView) previouspersonViewList.get(i);
		              //If previous List has same item with id  as new View 
		              //then discard it else add that item to new List.    
					 if(previousPreviousView.getPersonId().toString().equals(editpersonView.getPersonId().toString()))
		             {
		            	isPersonInSession=true;
		              
		             }
		         }
			}
			
		}
        catch(Exception e)
        {
             logger.logError("personAlreadyInSession|Exception Occured::"+e);
             
        }

        return isPersonInSession;



     }

	public boolean getIsRenderSendToParent()
	{
		boolean isRenderSendToParent=true;
		try
		{
		DomainDataView requestedPersonDataView =null;
		if(dataTable.getRowCount()!=0 && dataTable.getRowData()!=null)
		{
			PersonView personViewRow = (PersonView)dataTable.getRowData();
			//If person is black listed than he is not allowed to do any transaction therefore no select icon will be shown against that person 
			//except for replace cheque in that case the SELECT_BLACK_LIST key should come from the Replace Cheque Screen
			if(!viewRootMap.containsKey(WebConstants.SELECT_BLACK_LIST) && 
					personViewRow.getIsBlacklisted()!= null && personViewRow.getIsBlacklisted().compareTo(1L)==0)
			{
				isRenderSendToParent=false;
			}
			else
			{
				if(viewRootMap.containsKey(WebConstants.PERSON_TYPE))
				{
		            requestedPersonDataView = CommonUtil.getIdFromType((List)viewRootMap.get(WebConstants.PERSON_TYPE), viewRootMap.get(REQUESTED_PERSON_TYPE).toString());
				String requestedPersonType = (String) viewRootMap.get(REQUESTED_PERSON_TYPE);
				AssignedPersonTypeView aptView=commonUtil.getAssignedPersonTypeFromSet(personViewRow.getAssignedPersonTypeViewSet(), requestedPersonType);
		        if(aptView!=null && requestedPersonDataView !=null  && aptView.getPersonTypeId().compareTo(requestedPersonDataView.getDomainDataId())== 0)
		        {
		        	isRenderSendToParent=true;
		        }
		        else 
		        	isRenderSendToParent=false;
				}
			}
		}
		}
		catch(Exception ex)
		{
			logger.LogException("getIsRenderSendToParent|Error Occured..",ex);
		}
		return isRenderSendToParent;
	}
	public String getBundleMessage(String key){
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		}
		catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }


	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Long getPersonTypeId() {
		return personTypeId;
	}



	public void setPersonTypeId(Long personTypeId) {
		this.personTypeId = personTypeId;
	}


	public String getPassportNumber() {
		return passportNumber;
	}



	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}



	public String getResidenseVisaNumber() {
		return residenseVisaNumber;
	}



	public void setResidenseVisaNumber(String residenseVisaNumber) {
		this.residenseVisaNumber = residenseVisaNumber;
	}



	public String getPersonalSecCardNo() {
		return personalSecCardNo;
	}



	public void setPersonalSecCardNo(String personalSecCardNo) {
		this.personalSecCardNo = personalSecCardNo;
	}



	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}



	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}



	public String getSocialSecNumber() {
		return socialSecNumber;
	}



	public void setSocialSecNumber(String socialSecNumber) {
		this.socialSecNumber = socialSecNumber;
	}



	public PersonView getPersonView() {
		return personView;
	}



	public void setPersonView(PersonView personView) {
		this.personView = personView;
	}



	public PersonView getDataItem() {
		return dataItem;
	}



	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}



	public List<PersonView> getDataList() {
		return dataList;
	}



	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlForId() {
		return controlForId;
	}
	public void setControlForId(String controlForId) {
		this.controlForId = controlForId;
	}
	public String getRequestForPersonType() {
		if(viewRootMap.containsKey("REQUESTED_PERSON_TYPE") && viewRootMap.get("REQUESTED_PERSON_TYPE")!=null)
			requestForPersonType = viewRootMap.get("REQUESTED_PERSON_TYPE").toString();
		return requestForPersonType;
	}
	public void setRequestForPersonType(String requestForPersonType) {
		this.requestForPersonType = requestForPersonType;
	}
	public String getDisplayStylePersonTypeCombo() {
		if(requestForPersonType!=null && !requestForPersonType.equals(""))
			displayStylePersonTypeCombo="display:inline";
		else
			displayStylePersonTypeCombo="display:none";
		return displayStylePersonTypeCombo;
	}
	public void setDisplayStylePersonTypeCombo(String displayStylePersonTypeCombo) {
		this.displayStylePersonTypeCombo = displayStylePersonTypeCombo;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}
	
	public List<SelectItem> getPersonTypeList() 
	{
		personTypeList.clear();
		if(!sessionMap.containsKey(WebConstants.PERSON_TYPE))
			loadPersonTypeCombo();
		else
		{
			List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
			for(int i=0;i<domainDataList.size();i++)
			{
				SelectItem item =null;
				DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
				System.out.println(domainDataView.getDataValue());
				if(domainDataView.getDataValue().equals( requestForPersonType)   )
				{
					if(getIsEnglishLocale())  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn() );
					else  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr() );
					  personTypeList.add(item);
				}
				
				
			}
		}
		return personTypeList;
	}

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	public String getNationalityId() {
		return nationalityId;
	}
	public void setNationalityId(String nationalityId) {
		this.nationalityId = nationalityId;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getLicenseSource() {
		return licenseSource;
	}
	public void setLicenseSource(String licenseSource) {
		this.licenseSource = licenseSource;
	}
	public String getVisaIssuePlace() {
		return visaIssuePlace;
	}
	public void setVisaIssuePlace(String visaIssuePlace) {
		this.visaIssuePlace = visaIssuePlace;
	}
	public String getPassportIssuePlace() {
		return passportIssuePlace;
	}
	public void setPassportIssuePlace(String passportIssuePlace) {
		this.passportIssuePlace = passportIssuePlace;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSelectedPersonKind() {
		return selectedPersonKind;
	}
	public void setSelectedPersonKind(String selectedPersonKind) {
		this.selectedPersonKind = selectedPersonKind;
	}
	public List<SelectItem> getCountryList() {
		if(viewRootMap.containsKey("countryList"))
			countryList = (ArrayList<SelectItem>)viewRootMap.get("countryList");
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public Boolean getIsBlackListed() {
		return isBlackListed;
	}
	public void setIsBlackListed(Boolean isBlackListed) {
		this.isBlackListed = isBlackListed;
	}
	public String getGrpNumber() {
		return grpNumber;
	}
	public void setGrpNumber(String grpNumber) {
		this.grpNumber = grpNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	
}
