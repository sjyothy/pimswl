package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.column.HtmlSimpleColumn;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionRequestRegView;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;


public class AuctionRequestReg extends AbstractController
{
	
	
	//-----------------MEMBER DECLARATIONS---------------------------
	public transient Logger logger = Logger.getLogger(AuctionRequestReg.class);
	
	private HtmlInputText inputTextBidder = new HtmlInputText();
	private HtmlInputText inputTextTotalDepositAmount;
	private HtmlDataTable dataTable;
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlSimpleColumn selectColumn = new HtmlSimpleColumn();
	
	private AuctionRequestRegView dataItem = new AuctionRequestRegView();
	private List<AuctionRequestRegView> dataList = new ArrayList<AuctionRequestRegView>();
	
	private boolean sortAscending = true;
	
	private final String TAB_BIDDER = "bidderInfoTab";
	private final String TAB_AUCTION = "auctionTab";
	private final String TAB_PAYMENTS = "paymentsTab";
	
	private String hiddenBidderName;
	private String hiddenBidderId;
	private String hiddenTotalDeposit;
	private String hiddenTotalSelected;
	private String hiddenAuctionUnitsOfRequest;
	private String hiddenPassportNo;
	private String hiddenSocialSecNo;
	private String hiddenCellNumber;
	private String hiddenNationality;
	private String hiddenEmail;

	private String auctionId;
	private String auctionNo;
	private String auctionDate;
	private String regStartDate;
	private String regEndDate;
	private String auctionTitle;
	private String auctionVenueName;
	
	private Boolean hideSelectBooleanCheckBox;
	private Boolean showSearchBtn;
	private Boolean allowSave;
	private Boolean allowRePrint;
	private Boolean allowPay;
	private String isPaymentPopupClosed;
	private String requestSaved;
	private String requestReprinted;
	
	private Boolean isEnglishLocale;
	private Boolean isArabicLocale;
	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	
	private ResourceBundle bundle;

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	/// Payments Tab Variables ///
	private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
	private HtmlDataTable paymentScheduleDataTable = new HtmlDataTable();
	
	/// ---------------------- ///
	
	public static final String AUCTION_UNITS_REG_LIST = "AUCTION_UNITS_REG_LIST";
	public static final String AUCTION_UNITS_REG_BIDDER_ID = "AUCTION_UNITS_REG_BIDDER_ID";

	
	//-----------------CONSTRUCTOR---------------------------
	public AuctionRequestReg(){		
	}

	//-----------------PAGE LIFE CYCLE METHODS---------------------------
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		
		if(!isPostBack())
		{
			clearAllData();
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
			loadAttachmentsAndComments();
		}
		
		if(sessionMap.get("isAuctionSelected")!=null)
		{
			Boolean isAuctionSelected = (Boolean)sessionMap.get("isAuctionSelected");
			sessionMap.remove("isAuctionSelected");
			viewMap.put("count", 1);
			viewMap.put("allowPay",isAuctionSelected);
			viewMap.put("auctionLoaded", false);
		}
		if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
		{
			List<PaymentScheduleView> temp = (List<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			viewMap.put(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE,temp);
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		}
		if(sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null)
		{
			Boolean success = (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
			sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
			if(success)
			{
			   updateAuctionRequest();
		 	}
		}
		
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public void prerender() {
		super.prerender();
		loadBidder();
		loadAuction();
		Integer count = (Integer) viewMap.get("count");
		if(StringHelper.isNotEmpty(hiddenAuctionUnitsOfRequest))
			viewMap.put("hiddenAuctionUnitsOfRequest", hiddenAuctionUnitsOfRequest);
		if(count==null || count == 1){
			loadAvailableUnitsOfBidder();
		}
		
	}

	
	//-----------------METHODS-------------------------------
	
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_REGISTER_AUCTION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if(viewMap.containsKey("requestId")){
			Long requestId = (Long) viewMap.get("requestId");
	    	String entityId = requestId.toString();
			
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	@SuppressWarnings("unchecked")
	private Boolean createAuctionRequest(){
		Boolean success = true;
		logger.logInfo("createAuctionRequest started...");
		
		errorMessages = new ArrayList<String>();
        infoMessages = new ArrayList<String>();
        Long requestId = null;
		try{
			requestId = (Long) viewMap.get("requestId");
			
			if(isValid() && requestId==null)
			{
				RequestView requestView = new RequestView();
		        setRequestView(requestView);
		        PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		        requestId = pSAgent.createAuctionRequest(requestView);
				viewMap.put("requestId", requestId);
		        
			}
			else
				success = false;
			
			if(requestId!=null)
				success = true;
			logger.logInfo("createAuctionRequest completed successfully...");
		}
		catch(Exception ex)
		{
			success = false;
			logger.LogException("createAuctionRequest crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_CREATE_REQUEST_FAILED));
		}
		return success;
	}

	private void setRequestView(RequestView requestView) 
	{
		requestView.setCreatedBy(getLoggedInUser());
		requestView.setCreatedOn(new Date());
		requestView.setDepositAmount((Double.parseDouble(getHiddenTotalDeposit())));
		requestView.setIsDeleted(new Long(0));
		requestView.setRecordStatus(new Long(1));
		requestView.setRequestDate(new Date());
		requestView.setDescription(CommonUtil.getBundleMessage(WebConstants.AuctionRequest.REQUEST_DESC));
		requestView.setUpdatedBy(getLoggedInUser());
		requestView.setUpdatedOn(new Date());
		auctionId = (String) viewMap.get("auctionId");
		requestView.setAuctionId(Long.parseLong(auctionId));
		AuctionView auctionView = new AuctionView();
		auctionView.setAuctionId(Long.parseLong(auctionId));
		requestView.setAuctionView(auctionView);
		
		PersonView bidderView = new PersonView();
		hiddenBidderId = (String) viewMap.get("hiddenBidderId");
		bidderView.setPersonId(Long.parseLong(hiddenBidderId));
		requestView.setBidderView(bidderView);
		
		requestView.setTenantsView(null);
		requestView.setContractView(null);
	}
	
	private Map<Long,Double> getDepositAmountMap(){
		List<AuctionRequestRegView> auctionUnits = getAuctionDataList();
		Map<Long,Double> depositAmountMap = new HashMap<Long, Double>();
		for(AuctionRequestRegView auctionUnit : auctionUnits){
			if(auctionUnit.getAuctionUnitId()!=null && auctionUnit.getDepositAmount()!=null)
				depositAmountMap.put(auctionUnit.getAuctionUnitId(), auctionUnit.getDepositAmount());
		}
		return depositAmountMap;
	}
	
	@SuppressWarnings("unchecked")
	
	private void updateAuctionRequest(){
		logger.logInfo("updateAuctionRequest started...");
		
		errorMessages = new ArrayList<String>();
        infoMessages = new ArrayList<String>();
        
		try{
			RequestView requestView = new RequestView();
			Long requestId = (Long) viewMap.get("requestId");			
			requestView.setRequestId(requestId);
			
			Set reqDetailViewSet = new HashSet<RequestDetailView>();
	        
	        hiddenAuctionUnitsOfRequest = (String) viewMap.get("hiddenAuctionUnitsOfRequest");
	        
	        if(hiddenAuctionUnitsOfRequest != null && hiddenAuctionUnitsOfRequest.length() > 0){
	        	Map<Long,Double> depositAmountMap = getDepositAmountMap();
	        	
		        String[] auctionUnitsArr = hiddenAuctionUnitsOfRequest.split(",");
		        for(int i=0;i<auctionUnitsArr.length;i++){
		        	
		        	Long auctionUnitId = new Long(auctionUnitsArr[i].split(":")[0]);
		        	Long unitId = new Long(auctionUnitsArr[i].split(":")[1]);
		        	
			        RequestDetailView rdv = new RequestDetailView();
			        rdv.setCreatedBy(getLoggedInUser());
			        rdv.setCreatedOn(new Date());
			        rdv.setUpdatedBy(getLoggedInUser());
			        rdv.setUpdatedOn(new Date());
			        rdv.setIsDeleted(new Long(0));
			        rdv.setRecordStatus(new Long(1));
			        rdv.setDepositAmount(depositAmountMap.get(auctionUnitId));
			        rdv.setWonAuction(false);
			        rdv.setHasRefunded(false);
			        rdv.setMoveToContract(0L);
			        rdv.setConfiscatedAmount(0.0D);
			        UnitView uv = new UnitView();
			        uv.setUnitId(unitId);
			        rdv.setUnitView(uv);
			        
			        AuctionUnitView auctionUnitView = new AuctionUnitView();
			        auctionUnitView.setAuctionUnitId(auctionUnitId);
			        rdv.setAuctionUnitView(auctionUnitView);
			        
			        reqDetailViewSet.add(rdv);        
		        }
		        requestView.setRequestDetailView(reqDetailViewSet);
		        
		        List<PaymentScheduleView> paymentScheduleViewList = null;
		        
		        if(getPaymentScheduleNeedsUpdate()){
		        	paymentScheduleViewList = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE);	
		        }
		        
		        AuctionView auctionView = new AuctionView();
		        auctionId = (String) viewMap.get("auctionId");
		        auctionView.setAuctionId(Long.parseLong(auctionId));
		        
		        PersonView bidderView = new PersonView();
		        hiddenBidderId = (String) viewMap.get("hiddenBidderId");
		        bidderView.setPersonId(Long.parseLong(hiddenBidderId));
		        
		        BidderAuctionRegView bidderAucRegView = new BidderAuctionRegView();
		        bidderAucRegView.setBidderId(bidderView.getPersonId());
		        bidderAucRegView.setBidderView(bidderView);
		        bidderAucRegView.setAuctionId(Long.parseLong(auctionId));
		        bidderAucRegView.setAuctionView(auctionView);
		        bidderAucRegView.setAnyWin("N");
		        bidderAucRegView.setAttended("Y");
		        bidderAucRegView.setBalanceDepositAmount(0.00);
		        bidderAucRegView.setConfiscatedAmount(new Double(0));
		        bidderAucRegView.setHasRefunded("N");
		        Double total = (Double) viewMap.get("totalAmount");
		        bidderAucRegView.setTotalDepositAmount(total);
		        bidderAucRegView.setCreatedBy(getLoggedInUser());
		        bidderAucRegView.setCreatedOn(new Date());
		        bidderAucRegView.setUpdatedBy(getLoggedInUser());
		        bidderAucRegView.setUpdatedOn(new Date());
		        bidderAucRegView.setIsDeleted(new Long(0));
		        bidderAucRegView.setRecordStatus(new Long(1));
		        
		        PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		        DomainDataView temp = pSAgent.getDomainDataByValue(WebConstants.ConductAuction.BIDDER_AUCTION_STATUS, WebConstants.ConductAuction.PRESENT);
		        bidderAucRegView.setStatusId(temp.getDomainDataId());
		        
				
				Boolean collectPaymentsSuccess = collectPayments();
				if(collectPaymentsSuccess)
				{
				
					loadAttachmentsAndComments();
				    saveAttachments(requestId);
					saveComments(requestId);
					Boolean success = pSAgent.updateAuctionRequest(requestView, bidderAucRegView, paymentScheduleViewList);
					if(success)
					{
						infoMessages.add(getBundle().getString(MessageConstants.AuctionRegistration.MSG_REQUEST_SAVED));
						viewMap.put(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE,pSAgent.getPaymentScheduleByRequestID(requestId));				
						
						viewMap.put("isPaymentCollected",true);
						viewMap.put("canAddAttachment", false);
						viewMap.put("canAddNote", false);
						viewMap.put("allowSave", true);
						viewMap.put("done", true);
					}
					else
						throw new Exception();
				}
			}
		}
		catch(Exception ex)
		{
			logger.LogException("registerUnits crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_UPDATE_REQUEST_FAILED));
		}
	}
	
	public String registerUnits(){
		logger.logInfo("registerUnits started...");
		
		errorMessages = new ArrayList<String>();
        infoMessages = new ArrayList<String>();
        
		try{
			if(createAuctionRequest()){
				if(generatePaymentSchedules())
						openReceivePaymentsPopUp();
			}
			logger.logInfo("registerUnits completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("registerUnits crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	
	@SuppressWarnings("unchecked")
	private void clearAllData() {
		String  methodName = "clearAllData";		
        logger.logInfo(methodName+" started...");
        try{
        	hiddenCellNumber = "";
        	hiddenNationality = "";
        	hiddenEmail = "";
			hiddenPassportNo = "";		
			hiddenSocialSecNo = "";
			hiddenBidderName = "";		
			hiddenBidderId = "";		
			auctionId = "";		
			auctionNo = "";
			regStartDate = "";
			regEndDate = "";
			auctionDate = "";		
			auctionTitle = "";		
			auctionVenueName = "";		
			hiddenTotalDeposit = "";
			hiddenTotalSelected = "";
			hiddenAuctionUnitsOfRequest = "";		
			isPaymentPopupClosed = "N";
			requestReprinted = "N";
			requestSaved = "N";
			allowSave = false;		
			allowRePrint = false;
//			allowPay = false;
			showSearchBtn = true;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			viewMap.remove(WebConstants.AuctionRequest.ADDED_REQUEST);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED);
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewRootMap.remove(AUCTION_UNITS_REG_LIST);
			viewRootMap.remove(AUCTION_UNITS_REG_BIDDER_ID);
			viewMap.remove(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE);
			viewMap.remove("auctionUnits");
			viewMap.remove("done");
			viewMap.remove("isPaymentCollected");
			viewMap.remove("paymentScheduleGeneratedUnits");
			viewMap.remove("paymentScheduleNeedsUpdate");
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
			tabPanel.setSelectedTab(TAB_BIDDER);
			viewMap.remove("totalAmount");
			sessionMap.remove(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE);
			viewMap.remove("canCollectPayment");
			
	        logger.logInfo(methodName+" completed successfully...");
        } catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}       
	}
	@SuppressWarnings("unchecked")
	public List<AuctionRequestRegView> getAuctionDataList() {
 		if(viewMap.get("auctionUnits")!=null)
			dataList = (List<AuctionRequestRegView>)viewMap.get("auctionUnits");
		return dataList;
	}	
	@SuppressWarnings("unchecked")
	private void loadBidder(){
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try{
			logger.logInfo("loadBidder started...");
			
			Boolean bidderLoaded = (Boolean) viewMap.get("bidderLoaded");
			bidderLoaded = (bidderLoaded==null)?false:bidderLoaded;
			if(StringHelper.isNotEmpty(hiddenBidderId) && !bidderLoaded){
				viewMap.put("hiddenBidderId", hiddenBidderId);
				viewMap.put("count", 1);
				PersonView bidderView = psa.getPersonInformation(Long.parseLong(hiddenBidderId));
				hiddenBidderName = bidderView.getPersonFullName();
				hiddenPassportNo = bidderView.getPassportNumber();
				hiddenSocialSecNo = bidderView.getSocialSecNumber();
				hiddenCellNumber = bidderView.getCellNumber();
				hiddenNationality = getIsEnglishLocale()?bidderView.getNationalityEn():bidderView.getNationalityAr();
				
				Set<ContactInfoView> temp = bidderView.getContactInfoViewSet();
				if(temp!=null && temp.size()>0)
				{
					hiddenEmail = ((ContactInfoView)temp.iterator().next()).getEmail();
				}
				viewMap.put("bidderLoaded", true);
			}
			logger.logInfo("loadBidder completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadBidder crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	
	private void loadAuction()
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try
		{
			logger.logInfo("loadAuction started...");
			Boolean auctionLoaded = (Boolean) viewMap.get("auctionLoaded");
			auctionLoaded = (auctionLoaded==null)?false:auctionLoaded;
			if(StringHelper.isNotEmpty(auctionId) && !auctionLoaded){
				viewMap.put("auctionId", auctionId);
			AuctionView auctionView = psa.getAuctionById(Long.parseLong(auctionId));
			auctionNo = auctionView.getAuctionNumber();
			auctionDate = auctionView.getAuctionDate();
			if(auctionView.getRequestStartDate()!=null)
				regStartDate = CommonUtil.getStringFromDate(auctionView.getRequestStartDate());
			if(auctionView.getRequestEndDate()!=null)
				regEndDate = CommonUtil.getStringFromDate(auctionView.getRequestEndDate());
			auctionTitle = auctionView.getAuctionTitle();
			auctionVenueName = auctionView.getVenueName();
			viewMap.put("registrationEndDate", auctionView.getRequestEndDate());
			viewMap.put("registrationStartDate", auctionView.getRequestStartDate());
			viewMap.put("auctionLoaded", true);
			viewMap.put("count", 1);
			}
			logger.logInfo("loadAuction completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadAuction crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void setCount(ActionEvent e)
	{
		viewMap.put("count", 1);
		viewMap.put("bidderLoaded", false);
	}
	
	@SuppressWarnings("unchecked")
	private void loadAvailableUnitsOfBidder() 
	{
		String  methodName = "loadAvailableUnitsOfBidder";		
        logger.logInfo(methodName+" started...");
        try{
			//dataList = getAvailableAuctionUnitList();
        	viewMap.put("count",2);
        	Boolean loadFromDB = false;
        	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//			Object list = viewRootMap.get(AUCTION_UNITS_REG_LIST);
			Object list = null;
			if(list != null){
				dataList = (List<AuctionRequestRegView>)list;
				if(dataList.size() > 0){
					AuctionRequestRegView auctionReqRegView = dataList.iterator().next();
					Object bId = viewRootMap.get(AUCTION_UNITS_REG_BIDDER_ID);
					if(!auctionReqRegView.getAuctionId().toString().equals(auctionId) || (bId == null) || (bId != null && !bId.toString().equals(hiddenBidderId))){
						loadFromDB = true;
					}
				}
				else{
					loadFromDB = true;
				}
			}
			else{
				loadFromDB = true;
			}
			if(loadFromDB){
				dataList = getAvailableAuctionUnitList();
				viewRootMap.put(AUCTION_UNITS_REG_BIDDER_ID, hiddenBidderId);
				viewRootMap.put(AUCTION_UNITS_REG_LIST, dataList);
			}
        	
	
			if(getRequestSaved().equals("Y")){
				List<AuctionRequestRegView> requestUnits = (List<AuctionRequestRegView>)viewMap.get(WebConstants.AuctionRequest.ADDED_REQUEST);
				dataList = requestUnits;
				setHideSelectBooleanCheckBox(false);
				allowRePrint = true;
				viewMap.put("allowPay",false);
//				allowPay = false;
				allowSave = false;
				showSearchBtn = false;
			}
			else{
				allowRePrint = false;
//				allowPay = true;
				allowSave = true;
				showSearchBtn = true;
			}
			viewMap.put("auctionUnits", dataList);
			if(dataList.size()==0){
				if(StringHelper.isNotEmpty(hiddenBidderId) && StringHelper.isNotEmpty(auctionId)){
					errorMessages.clear();
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_NO_UNITS_TO_REGISTER));
					viewMap.put("done",true);
					viewMap.put("canAddAttachment", false);
					viewMap.put("canAddNote", false);
				}
			}
	        logger.logInfo(methodName+" completed successfully...");
        } catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}   
	}
	
	
	
	/**
	 * Query the database and fetch all the auction units available for the bidder in this auction
	 * @return List<AuctionRequestRegView>
	 */
	private List<AuctionRequestRegView> getAvailableAuctionUnitList()
	{
		List<AuctionRequestRegView> aucUnitList = new ArrayList<AuctionRequestRegView>();
		
		String  methodName = "getAvailableAuctionUnitList";		
        logger.logInfo(methodName+" started...");
        try
        {
		
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			AuctionView auctionView = new AuctionView();
			if(auctionId != null && !auctionId.equals(""))
				auctionView.setAuctionId(Long.parseLong(auctionId));		
	        
	        PersonView bidderView = new PersonView();
	        if(hiddenBidderId != null && !hiddenBidderId.equals(""))
	        	bidderView.setPersonId(Long.parseLong(hiddenBidderId));
	        
			try {
				if(auctionId != null && !auctionId.equals(""))
					aucUnitList = pSAgent.getUnRegisteredUnitsOfBidderForAuction(auctionView, bidderView);
			} catch (PimsBusinessException e) {	
				logger.LogException(methodName+" crashed ...", e);
				e.printStackTrace();
			}
	        logger.logInfo(methodName+" completed successfully...");
        }
        catch (Exception exception) 
        {
			logger.LogException(methodName+" crashed ...", exception);
		}   
		return aucUnitList;		
	}
	public String rePrintReceipt()
	{
		String  methodName = "rePrintReceipt";		
        logger.logInfo(methodName+" started...");
        try{
			infoMessages = new ArrayList<String>();			
			infoMessages.add(getBundle().getString(MessageConstants.AuctionRegistration.MSG_RECEIPT_REPRINTED));
			requestReprinted = "Y";

			logger.logInfo(methodName+" completed successfully...");
        } catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
		}
		return "";
	}
	
	
    private String getLoggedInUser() 
	{
		return CommonUtil.getLoggedInUser();
	}
    
    @SuppressWarnings("unchecked")
	public void tabPaymentSchedule_Click()
	{
		String methodName="tabPaymentSchedule_Click|";
		logger.logInfo(methodName +"started...");
		try
		{	
			if(generatePaymentSchedules())
				viewMap.put("paymentScheduleNeedsUpdate", true);
			logger.logInfo(methodName+"completed successfully...");
	    } catch (Exception exception) 
	    {
	    	logger.LogException(methodName+"crashed...", exception);
		}    
	}
    //////////////////////// Payments Tab Methods Start//////////////////////
    @SuppressWarnings("unchecked")
    private List<PaymentScheduleView> getPaymentScheduleFromPaymentConfiguration(Long paymentType,HashMap conditionsMap) throws PimsBusinessException
	{
    	logger.logInfo("getPaymentScheduleFromPaymentConfiguration started...");
    	
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	DomainDataView cashDDV = getDomainDataForCashMode();
    	UtilityServiceAgent usa = new UtilityServiceAgent();
    	DomainDataView ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
    	conditionsMap.put("userId", getLoggedInUser());
    	conditionsMap.put("statusId", ddv.getDomainDataId());
    	conditionsMap.put("paymentModeId", cashDDV.getDomainDataId());
    	Long requestId = (Long) viewMap.get("requestId");
    	conditionsMap.put("requestId", requestId);
    	List<PaymentScheduleView> temp = psa.getPaymentScheduleForRegisterAuction(WebConstants.PROCEDURE_TYPE_REGISTER_AUCTION, paymentType, conditionsMap, null, getIsEnglishLocale());
    	for(PaymentScheduleView paymentScheduleView: temp)
    		paymentScheduleView.setRequestId( new Long(viewMap.get("requestId").toString()));
    	
    	logger.logInfo("getPaymentScheduleFromPaymentConfiguration completed successfully...");
    	return temp;
	}
    @SuppressWarnings("unchecked")
    public Boolean generatePaymentSchedules()
    {
    	List<PaymentScheduleView> paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
    	List<AuctionRequestRegView> allAuctionUnits = getAuctionDataList();
    	List<AuctionRequestRegView> selectedAuctionUnits = new ArrayList<AuctionRequestRegView>();
    	Boolean success = true;
    	logger.logInfo("generatePaymentSchedules started...");
		try
		{
			String paymentScheduleGeneratedUnits = (String) viewMap.get("paymentScheduleGeneratedUnits");
			if(paymentScheduleGeneratedUnits==null)
				paymentScheduleGeneratedUnits = "";
			hiddenAuctionUnitsOfRequest = (String) viewMap.get("hiddenAuctionUnitsOfRequest");
			if(hiddenAuctionUnitsOfRequest!=null && !hiddenAuctionUnitsOfRequest.equals(paymentScheduleGeneratedUnits))
			{
			//  List<PaymentScheduleView> configPaymentScheduleViewList = new ArrayList<PaymentScheduleView>();
			//	HashMap map = new HashMap();
//				configPaymentScheduleViewList = getPaymentScheduleFromPaymentConfiguration(WebConstants.PAYMENT_TYPE_FEES_ID,map);
//				if(configPaymentScheduleViewList!=null)
//					paymentScheduleViewList.addAll(configPaymentScheduleViewList);
				Double totalAmount = 0.00D;
				for(AuctionRequestRegView auctionUnit: allAuctionUnits)
				{
					if(auctionUnit.getIsSelected()!= null && auctionUnit.getIsSelected())
					{
						selectedAuctionUnits.add(auctionUnit);							
						totalAmount+=auctionUnit.getDepositAmount();
					}
				}
				viewMap.put("selectedUnits", selectedAuctionUnits);
				if(totalAmount>0.00D)
				{
					Long requestId = (Long) viewMap.get("requestId");
					PropertyServiceAgent psa = new PropertyServiceAgent();
					paymentScheduleViewList.addAll(psa.generatePaymentScheduleForAuctionRegistration(selectedAuctionUnits, getLoggedInUser(), requestId));
					viewMap.put("totalAmount", totalAmount);
					viewMap.put(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);
					viewMap.put("canCollectPayment",true);
					viewMap.put("paymentScheduleGeneratedUnits", hiddenAuctionUnitsOfRequest);
				}
				else
					viewMap.put("canCollectPayment",false);
			 }
			 logger.logInfo("generatePaymentSchedules completed successfully...");
		}
		catch(Exception ex)
		{
			success = false;
			logger.LogException("generatePaymentSchedules crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_GENERATE_PAYMENT_SCHEDULE_FAILED));
		}
		
		return success;
	}
    @SuppressWarnings("unchecked")
    public void btnEditPaymentSchedule_Click()
    {
    	String methodName = "btnEditPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	//psv.setRowId(psv.getClass().hashCode());
    	if(viewMap.containsKey(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE) )
    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE));
    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
    	
    	String javaScriptText ="var screen_width = 1024;"+
         "var screen_height = 450;"+
        "window.open('EditPaymentSchedule.jsf','_blank','width='+(screen_width-500)+',height='+(screen_height-200)+',left=300,top=250,scrollbars=yes,status=yes');";
    	AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
        addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);
        
    	

 
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    private Boolean isValid()
    {
    	Boolean isValid = true;
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	try
    	{
    	if(hiddenBidderId == null || hiddenBidderId.trim().equals(""))
        {
        	isValid = false;
        	errorMessages.clear();
        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_SELECT_BIDDER));
        	tabPanel.setSelectedTab(TAB_BIDDER);
			return isValid;
        }

        if(auctionId == null || auctionId.trim().equals(""))
        {
			isValid = false;
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_SELECT_AUCTION));
			tabPanel.setSelectedTab(TAB_AUCTION);
			return isValid;
		}
        
        if(hiddenAuctionUnitsOfRequest.length() == 0)
        {
        	isValid = false;
        	errorMessages.clear();
        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_SELECT_UNIT_TO_PAY));
        	tabPanel.setSelectedTab(TAB_AUCTION);
			return isValid;
        }
        
        Date registrationStartDate = (Date) viewMap.get("registrationStartDate");
        Date today = new Date();
        if(df.parse(df.format(today)).before(df.parse(df.format(registrationStartDate))))
        {
        	isValid = false;
        	errorMessages.clear();
        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_REGISTRATION_NOT_YET_STARTED));
        	tabPanel.setSelectedTab(TAB_AUCTION);
			return isValid;
        }
        
        Date registrationEndDate = (Date) viewMap.get("registrationEndDate");
        if(df.parse(df.format(today)).after(df.parse(df.format(registrationEndDate ))))
        {
        	isValid = false;
        	errorMessages.clear();
        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRegistration.MSG_DEADLINE_CROSSED));
        	tabPanel.setSelectedTab(TAB_AUCTION);
			return isValid;
        }
    	}
    	catch(Exception e)
    	{
    		logger.LogException("canCollectPayment|Error Occured", e);
    	}
        
        return isValid;
    }
    @SuppressWarnings("unchecked")
    public String openReceivePaymentsPopUp()
	{
    	logger.logInfo("openReceivePaymentsPopUp started...");
		infoMessages.clear();
		errorMessages.clear();
		try
		{
		        tabPanel.setSelectedTab(TAB_PAYMENTS);
		        if(viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE)!=null)
		    	{
    		        List<PaymentScheduleView> paymentsForCollection = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE);
    				sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
    				PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
    		        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
			        FacesContext facesContext = FacesContext.getCurrentInstance();
			        String javaScriptText = "javascript:receivePayments();";
			        AddResource addResource = AddResourceFactory.getInstance(facesContext);
			        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		    	}
		        logger.logInfo("openReceivePaymentsPopUp completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("openReceivePaymentsPopUp crashed...", ex);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		return "";
	}
    
    private Boolean collectPayments()
	{
    	logger.logInfo("collectPayments started...");
    	Boolean success = true;
		infoMessages.clear();
		errorMessages.clear();

		try{
			 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();

		     prv=psa.collectPayments(prv);
		     
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.AUCTION_REQ_REG);
		     logger.logInfo("collectPayments completed successfully...");
		}
		catch(Exception ex)
		{
			success = false;
			logger.LogException("collectPayments crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED_FAILED));
		}
		return success;
	}
    
	
    
    //////////////////////// Payments Tab Methods End //////////////////////
	
	//-----------------MEMBER GETTERS/SETTERS---------------------------
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public AuctionRequestRegView getDataItem() {
		return dataItem;
	}

	public void setDataItem(AuctionRequestRegView dataItem) {
		this.dataItem = dataItem;
	}

	public List<AuctionRequestRegView> getDataList() {
		return dataList;
	}

	public void setDataList(List<AuctionRequestRegView> dataList) {
		this.dataList = dataList;
	}


	public boolean isSortAscending() {
		return sortAscending;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	public HtmlInputText getInputTextBidder() {
		return inputTextBidder;
	}

	public void setInputTextBidder(HtmlInputText inputTextBidder) {
		this.inputTextBidder = inputTextBidder;
	}

	public HtmlInputText getInputTextTotalDepositAmount() {
		return inputTextTotalDepositAmount;
	}

	public void setInputTextTotalDepositAmount(
			HtmlInputText inputTextTotalDepositAmount) {
		this.inputTextTotalDepositAmount = inputTextTotalDepositAmount;
	}
	
	public String getHiddenBidderName() {
		return hiddenBidderName;
	}

	public void setHiddenBidderName(String hiddenBidderName) {
		this.hiddenBidderName = hiddenBidderName;
	}
	
	public String getHiddenTotalDeposit() {
		if(hiddenTotalDeposit == null || hiddenTotalDeposit.equals(""))
        	hiddenTotalDeposit = "0.00";
		return hiddenTotalDeposit;
	}

	public void setHiddenTotalDeposit(String _hiddenTotalDeposit) {
		if(_hiddenTotalDeposit == null || _hiddenTotalDeposit.equals(""))
        	_hiddenTotalDeposit = "0.00";
		this.hiddenTotalDeposit = _hiddenTotalDeposit;
	}

	
	public String getHiddenTotalSelected() {
		if(StringHelper.isEmpty(hiddenTotalSelected))
			hiddenTotalSelected = "0";
		return hiddenTotalSelected;
	}

	public void setHiddenTotalSelected(String hiddenTotalSelected) {
		if(StringHelper.isEmpty(hiddenTotalSelected))
			hiddenTotalSelected = "0";
		this.hiddenTotalSelected = hiddenTotalSelected;
	}
	
	public String getHiddenAuctionUnitsOfRequest() {
		return hiddenAuctionUnitsOfRequest;
	}

	public void setHiddenAuctionUnitsOfRequest(
			String hiddenAuctionUnitsOfRequest) {
		this.hiddenAuctionUnitsOfRequest = hiddenAuctionUnitsOfRequest;
	}
	
	

	public String getHiddenBidderId() {
		return hiddenBidderId;
	}

	public void setHiddenBidderId(String hiddenBidderId) {
		this.hiddenBidderId = hiddenBidderId;
	}

	public String getAuctionId() {
		return auctionId;
	}

	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}

	public String getAuctionNo() {
		return auctionNo;
	}

	public void setAuctionNo(String auctionNo) {
		this.auctionNo = auctionNo;
	}

	public String getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}

	public String getAuctionTitle() {
		return auctionTitle;
	}

	public void setAuctionTitle(String auctionTitle) {
		this.auctionTitle = auctionTitle;
	}

	public String getAuctionVenueName() {
		return auctionVenueName;
	}

	public void setAuctionVenueName(String auctionVenueName) {
		this.auctionVenueName = auctionVenueName;
	}

	public Boolean getAllowSave() {
		if(viewMap.get("allowSave")!=null){
			return !(Boolean)viewMap.get("allowSave");
		}
		return true;
	}

	public void setAllowSave(Boolean allowSave) {
		this.allowSave = allowSave;
	}

	public Boolean getAllowRePrint() {
		return allowRePrint;
	}

	public void setAllowRePrint(Boolean allowRePrint) {
		this.allowRePrint = allowRePrint;
	}

	public Boolean getAllowPay() {
		if(viewMap.get("allowPay")!=null)
			allowPay = (Boolean)viewMap.get("allowPay");
		if(allowPay==null)
			allowPay = false;
		return allowPay;
	}

	public void setAllowPay(Boolean allowPay) {
		this.allowPay = allowPay;
	}

	public String getIsPaymentPopupClosed() {
		return isPaymentPopupClosed;
	}

	public void setIsPaymentPopupClosed(String isPaymentPopupClosed) {
		this.isPaymentPopupClosed = isPaymentPopupClosed;
	}
	
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public ResourceBundle getBundle() {
		if(getIsArabicLocale())
			bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");
		else
			bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");
		return bundle;
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}

	public String getHiddenPassportNo() {
		return hiddenPassportNo;
	}

	public void setHiddenPassportNo(String hiddenPassportNo) {
		this.hiddenPassportNo = hiddenPassportNo;
	}

	public String getHiddenSocialSecNo() {
		return hiddenSocialSecNo;
	}

	public void setHiddenSocialSecNo(String hiddenSocialSecNo) {
		this.hiddenSocialSecNo = hiddenSocialSecNo;
	}

	public String getRequestSaved() {
		if(requestSaved == null)
			requestSaved = "N";
		return requestSaved;
	}

	public void setRequestSaved(String requestSaved) {
		this.requestSaved = requestSaved;
	}

	public String getRequestReprinted() {
		if(requestReprinted == null)
			requestReprinted = "N";	
		return requestReprinted;
	}

	public void setRequestReprinted(String requestReprinted) {
		this.requestReprinted = requestReprinted;
	}

	public Boolean getHideSelectBooleanCheckBox() {
		return hideSelectBooleanCheckBox;
	}

	public void setHideSelectBooleanCheckBox(Boolean hideSelectBooleanCheckBox) {
		this.hideSelectBooleanCheckBox = hideSelectBooleanCheckBox;
	}

	public Boolean getShowSearchBtn() {
		return showSearchBtn;
	}

	public void setShowSearchBtn(Boolean showSearchBtn) {
		this.showSearchBtn = showSearchBtn;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHiddenCellNumber() {
		return hiddenCellNumber;
	}

	public void setHiddenCellNumber(String hiddenCellNumber) {
		this.hiddenCellNumber = hiddenCellNumber;
	}

	public String getHiddenNationality() {
		return hiddenNationality;
	}

	public void setHiddenNationality(String hiddenNationality) {
		this.hiddenNationality = hiddenNationality;
	}

	public String getHiddenEmail() {
		return hiddenEmail;
	}

	public void setHiddenEmail(String hiddenEmail) {
		this.hiddenEmail = hiddenEmail;
	}

	public String getRegStartDate() {
		return regStartDate;
	}

	public void setRegStartDate(String regStartDate) {
		this.regStartDate = regStartDate;
	}

	public String getRegEndDate() {
		return regEndDate;
	}

	public void setRegEndDate(String regEndDate) {
		this.regEndDate = regEndDate;
	}

	public HtmlSimpleColumn getSelectColumn() {
		return selectColumn;
	}

	public void setSelectColumn(HtmlSimpleColumn selectColumn) {
		this.selectColumn = selectColumn;
	}
	@SuppressWarnings("unchecked")
	public List<PaymentScheduleView> getPaymentScheduleDataList() {
		if(viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE)!=null)
			paymentScheduleDataList = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE);
		return paymentScheduleDataList;
	}

	public HtmlDataTable getPaymentScheduleDataTable() {
		return paymentScheduleDataTable;
	}

	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
		this.paymentScheduleDataTable = paymentScheduleDataTable;
	}

	public void setPaymentScheduleDataList(
			List<PaymentScheduleView> paymentScheduleDataList) {
		this.paymentScheduleDataList = paymentScheduleDataList;
	}
	@SuppressWarnings("unchecked")
	public List<AuctionRequestRegView> getSelectedUnits(){
		List<AuctionRequestRegView> temp = (List<AuctionRequestRegView>) viewMap.get("selectedUnits");
		if(temp==null)
			temp = new ArrayList<AuctionRequestRegView>();
		
		return temp;
	}
	
	public Boolean getIsPaymentCollected(){
		if(viewMap.get("isPaymentCollected")!=null){
			return (Boolean)viewMap.get("isPaymentCollected");
		}
		return false;
	}

	public Boolean getPaymentScheduleNeedsUpdate(){
		if(viewMap.get("paymentScheduleNeedsUpdate")!=null){
			return (Boolean)viewMap.get("paymentScheduleNeedsUpdate");
		}
		return false;
	}		
			
	public Boolean getDone(){
		if(viewMap.get("done")!=null){
			return (Boolean)viewMap.get("done");
		}
		return false;
	}
	
	public String getNumberFormat(){
    	return CommonUtil.getDoubleFormat();
    }
	@SuppressWarnings("unchecked")
	public DomainDataView getDomainDataForCashMode(){
		DomainDataView ddvCash = null;
		if(viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH)==null){
			
			List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			ddvCash= CommonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CASH,ddvCash);
		}
		else{
			ddvCash = (DomainDataView) viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		}
		
		return ddvCash;
	}
}
