package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.TenderPropertyView;
import com.avanza.pims.ws.vo.UnitView;



public class ServiceContractPropertyDetailsTab extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ServiceContractPropertyDetailsTab.class);
	   public interface Page_Mode {
			
	    	public static final String EDIT="PAGE_MODE_EDIT";
	 		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
	 		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
	 		public static final String COMPLETE="PAGE_MODE_COMPLETE";
	 		public static final String POPUP="PAGE_MODE_POPUP";
	 		public static final String VIEW="PAGE_MODE_VIEW";
	 		public static final String ACTIVE="PAGE_MODE_ACTIVE";
	 		public static final String DISABLE_SETUP_WORKERS="DISABLE_SETUP_WORKERS";
			
		}
	   private Integer paginatorMaxPages = 0;
		private Integer paginatorRows = 0;
	private HtmlGraphicImage imgAddWorkers=new HtmlGraphicImage();
    private HtmlGraphicImage imgdelWorkers=new HtmlGraphicImage();
    private HtmlCommandButton btnAddUnits = new HtmlCommandButton();
    private HtmlCommandButton btnAddProperty = new HtmlCommandButton();
    private HtmlCommandButton btnSetWorkers = new HtmlCommandButton();
    
    private HtmlCommandLink lnkDelPropUnit = new HtmlCommandLink();
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    int recordSize =0;
    String ROW_ID ="rowId";
    Map viewRootMap=context.getViewRoot().getAttributes();
	List<TenderPropertyView> tenderPropertyViewList = new ArrayList<TenderPropertyView>(0);
	private HtmlDataTable tenderPropertyDataTable;
    @Override 
	public void init() 
     {
    	 super.init();
    	 try
    	 {
			sessionMap=context.getExternalContext().getSessionMap();
			Map requestMap= context.getExternalContext().getRequestMap();
			HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
			
			if (sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)|| sessionMap.containsKey("MANYPROPERTYINFO")) 
			{

               addUnitInfoToPropertyDetails();
				sessionMap.remove("MANYPROPERTYINFO");
				sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
				
				
			}
			String property_unit = "";
	 		if(sessionMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.PROPERTY_UNIT))
	 		{
	 			property_unit =sessionMap.get(WebConstants.ServiceContractPropertyDetailsTab.PROPERTY_UNIT).toString();
	 			addSetWorkersNumberToList(property_unit );
	 			sessionMap.remove(WebConstants.ServiceContractPropertyDetailsTab.PROPERTY_UNIT);
	 			sessionMap.remove(WebConstants.ServiceContractPropertyDetailsTab.NUM_WORKERS);
	 			sessionMap.remove(WebConstants.ServiceContractPropertyDetailsTab.REMARK_WORKERS);
	 		}

    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

     public void enableDisableControls(String pageMode)
  	{
  		
  		if(pageMode.equals(Page_Mode.APPROVAL_REQUIRED)  ||pageMode.equals(Page_Mode.ACTIVE) || pageMode.equals(Page_Mode.COMPLETE) 
  				|| pageMode.equals(Page_Mode.LEGAL_REVIEW_REQUIRED) || pageMode.equals(Page_Mode.VIEW))
  		{
  			btnAddProperty.setRendered(false);
  			btnAddUnits.setRendered(false);
  			btnSetWorkers.setRendered(false);
  			imgdelWorkers.setRendered(false);
  			lnkDelPropUnit.setRendered(false);
  			imgAddWorkers.setRendered(false);
  		}
  		if(pageMode.equals(Page_Mode.DISABLE_SETUP_WORKERS))
  		{
  			btnSetWorkers.setRendered(false);
  			imgAddWorkers.setRendered(false);
  		}
  		  		
  		
  	}
    private void addSetWorkersNumberToList(String property_unit )
    {
      List<TenderPropertyView> newtpViewList =new ArrayList<TenderPropertyView>(0); 
      List<TenderPropertyView> tpViewList = (ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
      Long noOfWorkers = new Long(sessionMap.get(WebConstants.ServiceContractPropertyDetailsTab.NUM_WORKERS).toString());
      String remarks   = sessionMap.get(WebConstants.ServiceContractPropertyDetailsTab.REMARK_WORKERS).toString();
      for (TenderPropertyView tenderPropertyView : tpViewList) {
           if(property_unit.trim().length()>0 )
           {
        	   
        	   if(property_unit.equals(tenderPropertyView.getPropertyNumber()+"_"+(tenderPropertyView.getUnitRefNo()!=null?tenderPropertyView.getUnitRefNo():"")))
        	   {
        		   tenderPropertyView.setNoOfWorkers(noOfWorkers);
        	       tenderPropertyView.setNoofWorkersRemarks(remarks);
        	       tenderPropertyView.setUpdatedBy(CommonUtil.getLoggedInUser());
        	   }
           }
           else if(tenderPropertyView.getSelected())
           {
        	   
        	   tenderPropertyView.setNoOfWorkers(noOfWorkers);
        	   tenderPropertyView.setNoofWorkersRemarks(remarks);
        	   tenderPropertyView.setUpdatedBy(CommonUtil.getLoggedInUser());
           }
           newtpViewList.add(tenderPropertyView);
		
	  }
      viewRootMap.put(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST, newtpViewList);
    }
 	private void addUnitInfoToPropertyDetails()
 	{
 		String methodName = "addUnitInfoToPropertyDetails";
 	
 		ServiceContractAgent sca= new ServiceContractAgent();
        try
        {
        	
			logger.logInfo(methodName + "|" + "Start...");
			List<TenderPropertyView> tenderPropertyViewList= new ArrayList<TenderPropertyView>(0);
			List<TenderPropertyView> oldtenderPropertyViewList = new ArrayList<TenderPropertyView>(0); 
			if(viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST))
				oldtenderPropertyViewList =(ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
			if(sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)!=null)
			{
				int i=0;
				List<UnitView> uvList = (ArrayList<UnitView>)sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
				Long[] unitIds = new Long[uvList.size()];
				logger.logInfo(methodName + "|" + "	Count of Unit selected..."+unitIds.length);
				for (UnitView unitView : uvList ) {
				{
					if(!isUnitAlreadyPresent(oldtenderPropertyViewList, unitView.getUnitId()))
					{
					 unitIds[i] = unitView.getUnitId();
					 i++;
							 
					}
						
				}
				tenderPropertyViewList = sca.getTenderPropertyViewByUnitIds(unitIds);
			}
			}
			else if(sessionMap.get("MANYPROPERTYINFO")!=null)
			{
			  List<PropertyView>pvList= (ArrayList<PropertyView>)sessionMap.get("MANYPROPERTYINFO");
			  removeAlreadyPresentProperty(oldtenderPropertyViewList,pvList);
			  tenderPropertyViewList.addAll(sca.getTenderPropertyViewFromPropertyView(pvList)) ;
			}
			for (TenderPropertyView tenderPropertyView : tenderPropertyViewList) {
			
				tenderPropertyView.setCreatedOn(new Date());
				tenderPropertyView.setCreatedBy(CommonUtil.getLoggedInUser());
				tenderPropertyView.setUpdatedOn(new Date());
				tenderPropertyView.setUpdatedBy(CommonUtil.getLoggedInUser());
				tenderPropertyView.setIsDeleted(new Long(0));
				tenderPropertyView.setRecordStatus(new Long(1));
				
				oldtenderPropertyViewList.add(tenderPropertyView);				
			}
			
			viewRootMap.put(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST, oldtenderPropertyViewList);
			logger.logInfo(methodName + "|" + "Finish...");
		}
        catch(Exception ex)
        {
        	logger.LogException(methodName+"| Error Occured", ex);
        }
 	}
 	private void removeAlreadyPresentProperty(List<TenderPropertyView> oldtenderPropertyViewList,List<PropertyView> pvList)
 	{
 		Map<Long,TenderPropertyView> hMap = new HashMap<Long,TenderPropertyView>(0);
 		
 		
 		for (TenderPropertyView tenderPropertyView : oldtenderPropertyViewList) 
 		{
 		 
 			hMap.put(tenderPropertyView.getPropertyId() , tenderPropertyView);
 			
		}
 		for (PropertyView propertyView : pvList) {
	 			
 			    if(hMap.containsKey(propertyView.getPropertyId()))
	 			{
 			    	TenderPropertyView tenderPropertyView =(TenderPropertyView)hMap.get(propertyView.getPropertyId());
	 			     if(tenderPropertyView.getUnitId()==null)
	 			          pvList.remove(propertyView);
	 			}
 			
 			}
 		
 	
 		
 	}
 	private boolean isUnitAlreadyPresent(List<TenderPropertyView> oldtenderPropertyViewList,Long unitId)
 	{
 		for (TenderPropertyView tenderPropertyView : oldtenderPropertyViewList) 
 		{
 		 	
 			if(tenderPropertyView.getUnitId()!=null && tenderPropertyView.getUnitId().compareTo(unitId)==0)
 			 return true;
			
		}
 		return false;
 	
 	}
 	
 	public void imgDelProp_Click()
 	{
 		String methodName="imgDelProp_Click";
 		logger.logInfo(methodName+"|Start"); 
 		TenderPropertyView tpv = (TenderPropertyView)tenderPropertyDataTable.getRowData();
 		List tpvList =(ArrayList<TenderPropertyView> )viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
 		tpvList.remove(tpv);
 		viewRootMap.put(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST,tpvList);
 		
 		logger.logInfo(methodName+"|Finish");
 		
 	}
 	public LocaleInfo getLocaleInfo()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    return localeInfo;
		
	}
 	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
 	public Boolean getIsArabicLocale()
	{
    	
		return CommonUtil.getIsArabicLocale();
	}
 	public Boolean getIsEnglishLocale()
	{
    	
		return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
    	
		return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		 return CommonUtil.getTimeZone();
		
	}

	public HtmlGraphicImage getImgAddWorkers() {
		return imgAddWorkers;
	}

	public void setImgAddWorkers(HtmlGraphicImage imgAddWorkers) {
		this.imgAddWorkers = imgAddWorkers;
	}

	public HtmlGraphicImage getImgdelWorkers() {
		return imgdelWorkers;
	}

	public void setImgdelWorkers(HtmlGraphicImage imgdelWorkers) {
		this.imgdelWorkers = imgdelWorkers;
	}

	public HtmlCommandButton getBtnAddUnits() {
		return btnAddUnits;
	}

	public void setBtnAddUnits(HtmlCommandButton btnAddUnits) {
		this.btnAddUnits = btnAddUnits;
	}

	public HtmlCommandButton getBtnAddProperty() {
		return btnAddProperty;
	}

	public void setBtnAddProperty(HtmlCommandButton btnAddProperty) {
		this.btnAddProperty = btnAddProperty;
	}

	public HtmlCommandButton getBtnSetWorkers() {
		return btnSetWorkers;
	}

	public void setBtnSetWorkers(HtmlCommandButton btnSetWorkers) {
		this.btnSetWorkers = btnSetWorkers;
	}

	public List<TenderPropertyView> getTenderPropertyViewList() {
		if(viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST) &&
				viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)!=null
		  )
			tenderPropertyViewList =(ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
		return tenderPropertyViewList;
	}

	public void setTenderPropertyViewList(
			List<TenderPropertyView> tenderPropertyViewList) {
		if(viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST) && 
				viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)!=null		
		)
			tenderPropertyViewList = (ArrayList<TenderPropertyView> )viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
		this.tenderPropertyViewList = tenderPropertyViewList;
	}

	public HtmlDataTable getTenderPropertyDataTable() {
		return tenderPropertyDataTable;
	}

	public void setTenderPropertyDataTable(HtmlDataTable tenderPropertyDataTable) {
		this.tenderPropertyDataTable = tenderPropertyDataTable;
	}

	public int getRecordSize() {
		recordSize=0;
		if(viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST) && 
				viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)!=null)
			 recordSize = ((ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)).size();
		return recordSize;
	}

	public void setRecordSize(int recordSize) {
		this.recordSize = recordSize;
	}

	public HtmlCommandLink getLnkDelPropUnit() {
		return lnkDelPropUnit;
	}

	public void setLnkDelPropUnit(HtmlCommandLink lnkDelPropUnit) {
		this.lnkDelPropUnit = lnkDelPropUnit;
	}
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}		
}
