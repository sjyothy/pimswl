package com.avanza.pims.web.backingbeans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.tabbedpane.HtmlPanelTab;
import org.apache.myfaces.custom.tabbedpane.HtmlPanelTabbedPane;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.bpel.pimscontractinitiatebpel.proxy.ContractInitiateBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.GRPCustomerServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractDisclosureReportCriteria;
import com.avanza.pims.report.criteria.ContractPaymentReportCriteria;
import com.avanza.pims.report.criteria.NOLCriteria;
import com.avanza.pims.report.criteria.PrintLeaseContractReportCriteria;
import com.avanza.pims.report.criteria.RequestDetailsReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.EjariIntegration;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.ContractDistributionInfoService;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.CommercialActivityView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractActivityView;
import com.avanza.pims.ws.vo.ContractPartnerView;
import com.avanza.pims.ws.vo.ContractPersonView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PaidFacilitiesView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.EjariResponse;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;


public class RequestDetails  extends AbstractController
{

    /**
	 *
	 */
	private static final long serialVersionUID = -9105620637780986643L;
	String moduleName="RequestDetails";
	private String ejariServiceEndPoint=    "";
    private String AUCTION_UNIT_ID= "auctionUnitId";
    private String AUCTION_DEPOSIT_AMOUNT   = "auctionDepositAmont";
    private String AUCTION_UNIT_BIDDER_LIST = "auctionUnitBidderList";
    private String PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID = "paymentScheduleOwnerShipTypeId";
    private RequestDetailsReportCriteria requestDetailsReporemovertCriteria;
    private String PREVIOUS_UNIT_ID = "previousUnitId";
    private String OLD_RENT_VALUE = "OLD_RENT_VALUE";
    private String OLD_CONTRACT_TYPE = "OLD_CONTRACT_TYPE";
    private String IS_DATE_CHANGED = "IS_DATE_CHANGED";
    private ResourceBundle bundle;
    private boolean isPageModeAmendAdd;
    private boolean isPageModeUpdate;
    private String DDV_COMMERCIAL_CONTRACT="DDV_COMMERCIAL_CONTRACT";
    private String DDV_RESIDENTIAL_CONTRACT="DDV_RESIDENTIAL_CONTRACT";
    private String PAGE_MODE_ADD="ADD";
    private String PAGE_MODE_UPDATE="UPDATE";
    private String PAGE_MODE_APPROVE_REJECT="APPROVE_REJECT";
    private String  REQUEST_FOR_AMEND = "REQUEST_FOR_AMEND";
    private String TENANT_INFO="TENANTINFO";
    private String UNIT_DATA_ITEM="UNITDATAITEM";
    //private String PERSON_SUB_TYPE="PERSON_SUB_TYPE";
    private HtmlInputText txtRefNumber=new HtmlInputText();
    private HtmlInputText txtEjariNum=new HtmlInputText();
    private HtmlOutputLabel lblRefNum=new HtmlOutputLabel();
    private HtmlCommandButton btnOccuipers=new HtmlCommandButton();
    private HtmlCommandButton btnPartners=new HtmlCommandButton();
    private HtmlCommandButton btnSaveNewLease=new HtmlCommandButton();
    private HtmlCommandButton btnComplete=new HtmlCommandButton();
    
    private HtmlCommandButton btnSaveAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnCompleteAmendLease=new HtmlCommandButton();
    
    private HtmlCommandButton btnSubmitAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnApprove=new HtmlCommandButton();
    private HtmlCommandButton btnReject=new HtmlCommandButton();
    private HtmlCommandButton btnRegisterEjari =new HtmlCommandButton();
    private HtmlCommandButton btnTerminateInEjari =new HtmlCommandButton();
    private HtmlCommandButton btnApproveAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnRejectAmendLease=new HtmlCommandButton();
    private HtmlCommandButton btnGenPayments=new HtmlCommandButton();
    private HtmlCommandButton btnSendNewLeaseForApproval = new HtmlCommandButton();
    //private HtmlCommandButton btnCollectPayment = new HtmlCommandButton();
    private HtmlCommandButton btnCancelContract = new HtmlCommandButton();
    private HtmlCommandButton btnAddUnit = new HtmlCommandButton();
    private HtmlCommandButton btnAddAuctionUnit = new HtmlCommandButton();
    private HtmlCommandButton btnAddPayments = new HtmlCommandButton();
    HtmlSelectOneMenu cmbContractType = new HtmlSelectOneMenu();
    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
    org.richfaces.component.html.HtmlTab tabCommercialActivity=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabPartner=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabOccupier=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabContractHistory=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabRequestHistory=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabFacilities=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
    org.richfaces.component.html.HtmlCalendar issueDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    org.richfaces.component.html.HtmlCalendar startDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    org.richfaces.component.html.HtmlCalendar endDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    HtmlCommandButton btnAllPayment = new HtmlCommandButton();
    HtmlInputText txt_ActualRent = new HtmlInputText();
    private HtmlGraphicImage  btnPartnerDelete = new HtmlGraphicImage();
    private HtmlGraphicImage  btnOccupierDelete = new HtmlGraphicImage();

    private HtmlGraphicImage imgSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgTenant=new HtmlGraphicImage();
    private HtmlGraphicImage imgManager=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewTenant=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewManager=new HtmlGraphicImage();
    private HtmlGraphicImage imgDeleteSponsor=new HtmlGraphicImage();
    private HtmlGraphicImage imgDeleteManager=new HtmlGraphicImage();

    private HtmlTabPanel tabPanel = new HtmlTabPanel();
    private HtmlTab tabApplicationDetails = new HtmlTab();
    private String pageMode;
    private String refNum;
    private String datetimerequest;

    private String selectRequestStatus;
    private String tenantRefNum;
    private String unitRefNum;
    private String unitStatusId;
    private String unitRentValue;
    private String txtUnitStatus;
    private String auctionRefNum;
    private String hdnAuctionunitId;
    private String hdnUnitId;
    private String hdnUnitUsuageTypeId;
    private String txtUnitUsuageType;
    private String unitRentAmount;
    private String hdnTenantId;
    private String hdntenanttypeId;
    private String txtTenantName;
    private String txtSponsor;
    private String txtTenantType;
    private String hdnTenantBlackListed;
    private String hdnSponsorId;
    private String txtManager;
    private String hdnManagerId;
    private String personSponsor;
    private String personManager;
    private String personOccupier;
    private String personPartner;
    private String txtMaintainenceAmount;
    private Double maintainenceAmount;
    private Date contractStartDate;
    private Date contractEndDate;
    private Date contractIssueDate;
    private String contractId;
    private String contractCreatedOn;
    private String contractCreatedBy;
    private String propertyCommercialName;
    private String propertyAddress;
    private String contractStatus;
    private Double totalContractValue=new Double(0);
    public String  txttotalContractValue;
    public String hdnApplicantId;

    List<SelectItem> priorityList = new ArrayList<SelectItem>();
	List<SelectItem> statusList = new ArrayList<SelectItem>();
	List<SelectItem> contractTypeList = new ArrayList<SelectItem>();
	private List<String> errorMessages;
	private List<String> successMessages=new ArrayList<String>();
    private HtmlInputText txtRefNum;
     boolean isCompanyTenant;
     boolean isUnitCommercial;
     boolean isPageUpdateStatusNew;
     boolean isBlockForRenewal;
     String blockComment = "";
     String unblockComment = "";

     String pageTitle;
    FacesContext context = null;
    UIViewRoot view =null;

    PersonView tenantsView=new PersonView();
    RequestDetailView requestDetailView=new RequestDetailView();
	UnitView unitsView=new UnitView();

	HtmlPanelTabbedPane panelTabbedPane =new HtmlPanelTabbedPane ();
	private HtmlPanelTab panelTab=new HtmlPanelTab();
	private String TAB_ID_APPLICATION ="appDetailsTab";
	private String TAB_ID_BASIC ="tabBasicInfo";
	private String TAB_ID_UNIT ="tabUnit";
	private String TAB_ID_OCCUPIERS ="tabPersonOccupier";
	private String TAB_ID_PARTNERS ="tabPersonPartnr";
	private String TAB_ID_COMMERCIAL_ACTIVITY ="tabCommercialActivity";
	private String TAB_ID_FACILITIES ="tabPaidFacilities";
	private String TAB_ID_PAYMENT_SCHEDULE ="tabPaymentSchedule";
	private String TAB_ID_ATTACHEMENT ="attachmentTab";


	Map sessionMap;
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private HtmlDataTable paidFacilitiesDataTable;

	private List<PaidFacilitiesView> paidFacilitiesDataList = new ArrayList<PaidFacilitiesView>();
	private PaidFacilitiesView paidFacilitiesDataItem = new PaidFacilitiesView();
	private boolean isContractStatusApproved;
	private HtmlDataTable commercialActivityDataTable;
	private List<CommercialActivityView> commercialActivityList = new ArrayList<CommercialActivityView>();
	private CommercialActivityView commercialActivityItem = new CommercialActivityView();


	private HtmlDataTable unitDataTable;
	private List<UnitView> unitDataList = new ArrayList<UnitView>();
	private UnitView unitDataItem = new UnitView();

	private HtmlDataTable paymentScheduleDataTable;
	private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
	private PaymentScheduleView paymentScheduleDataItem = new PaymentScheduleView();
	private HtmlDataTable propspectiveOccupierDataTable;
	private List<PersonView> prospectiveOccupierDataList = new ArrayList<PersonView>();
	private PersonView prospectiveOccupierDataItem = new PersonView();
	private HtmlDataTable propspectivepartnerDataTable;
	private List<PersonView> prospectivepartnerDataList  = new ArrayList<PersonView>();
	private PersonView prospectivepartnerDataItem = new PersonView();
	boolean isArabicLocale;
	boolean isEnglishLocale;
	private String selectOneContractType;
	ContractView contractView =new ContractView();
	RequestView requestView=new RequestView();
	private static Logger logger = Logger.getLogger( RequestDetails.class );
	CommonUtil commonUtil=new CommonUtil();
	HttpServletRequest request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	String dateFormat="";
	@SuppressWarnings( "unchecked" )
	HashMap feesConditionsMap=new HashMap();
	@SuppressWarnings( "unchecked" )
	HashMap depositConditionsMap=new HashMap();
	private double totalRentAmount;
	private double totalCashAmount;
	private double totalChqAmount;
	private int installments;
	private double totalDepositAmount;
	private double totalFees;
	private double totalFines;
	private double totalRealizedRentAmount;
    
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

	private HtmlCommandButton btnPrint = new HtmlCommandButton();

    public String getHdnTenantBlackListed() {
		return hdnTenantBlackListed;
	}


	public void setHdnTenantBlackListed(String hdnTenantBlackListed) {
		this.hdnTenantBlackListed = hdnTenantBlackListed;
	}


	/** default constructor */
    public  RequestDetails()
    {
    	String methodName="Constructor";
    	logger.logInfo(methodName+"|"+" Start");
    	context = FacesContext.getCurrentInstance();
    	view    = context.getViewRoot();

    	// String passport =context.getExternalContext().getRequestMap().get("passportNumber").toString();
    	//   	Load all the combos present in the screen

    	logger.logInfo(methodName+"|"+" Finish");
    }


    @Override
    @SuppressWarnings("unchecked")
    public void init()
    {

//    	String methodName="init";
    	logger.logInfo("init|Start");
    	sessionMap =context.getExternalContext().getSessionMap();
	    Map  requestMap=context.getExternalContext().getRequestMap();
		try
    	{
				sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
		       if(!isPostBack())
		       {
		    	   setEjariServiceEndPoint( EjariIntegration.getEjariWebServiceEndPoint());
		    	   loadAttachmentsAndComments(null);
		    	   viewRootMap.put(OLD_CONTRACT_TYPE,"-1");
		    	   viewRootMap.put(IS_DATE_CHANGED,"0");

		    	   if(getRequestParam("CONTRACT_VIEW_ROOT")!=null )
		    	   {
		    		   viewRootMap=(Map)getRequestParam("CONTRACT_VIEW_ROOT");
		    		   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		    		   contractId=contractView.getContractId().toString();
		    		   pageMode=PAGE_MODE_UPDATE;
		    		   viewRootMap.put("pageMode", pageMode);
		    	   }
		    	   if(request.getParameter(WebConstants.VIEW_MODE)!=null)
		    	   {
		    		   viewRootMap.put(WebConstants.VIEW_MODE, request.getParameter(WebConstants.VIEW_MODE));
		    		   viewRootMap.put("pageMode",WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		    		   this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		    	   }
			    	ArrayList<RequestView> list =new ArrayList<RequestView>();
			    		 //Remove session
			    		 RemoveSessions();
				         //Load Types such as UnitUsuageType,TenantType,ContractType
				         loadTypes();
				         //Load Combos data
				         loadCombos();

				         String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR);
				         if(daysInYear!=null)
				         viewRootMap.put(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR,daysInYear );
				         setContractDate(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT);

				    	 if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null)
				    	 {
				    		  pageMode=getRequestParam(WebConstants.LEASE_AMEND_MODE).toString();
				    	      viewRootMap.put("pageMode", pageMode);
				    	 }
				    	 else if(sessionMap.containsKey(WebConstants.LEASE_AMEND_MODE))
				    	 {
				    		 pageMode=sessionMap.get(WebConstants.LEASE_AMEND_MODE).toString();
				    	     viewRootMap.put("pageMode", pageMode);
				    	     sessionMap.remove(WebConstants.LEASE_AMEND_MODE);

				    	 }

				    	 //if come from tasklist

				    	 if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				    		getDataFromTaskList();

				     	 //if come from request search or
				     	 //from contract search for incomplete request of amend
				    	 else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
				    	 {
				    		 requestView=(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
						     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
						       contractId=requestView.getContractView().getContractId().toString();
						      pageMode=PAGE_MODE_UPDATE;
				    	      viewRootMap.put("pageMode", pageMode);
				    	 }
				     	 //if come from contract search for new lease contract OR AMEND FOR ADD
					     else if(sessionMap.containsKey("contractId")  || request.getParameter("contractId") != null )
				    	 {
					    	 if(sessionMap.containsKey("contractId")  )
					    	 {
				    		   contractId= sessionMap.remove("contractId").toString();
					    	 }
					    	 else if ( request.getParameter("contractId") != null)
					    	 {
					    		 contractId= request.getParameter("contractId").toString();
					    	 }
				    		 if(getRequestParam(WebConstants.LEASE_AMEND_MODE)==null)
				    		 CheckIfPageModeView();

				    	 }
				     	if(contractId!=null && !contractId.equals("") )
				     	{
				     		getContractById(contractId);
			    			putViewValuesInControl(contractId);
				     	}
				     	else
			    		{
			    			//Add Mode
			    			pageMode=PAGE_MODE_ADD;
			    			viewRootMap.put("pageMode", PAGE_MODE_ADD);
			    		}
				     	
					     
		       }
		       //Is Post Back
		       else
		        {

		    	   if(viewRootMap.containsKey("pageMode"))
		    			pageMode=viewRootMap.get("pageMode").toString();
		    	   if(sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT))
		    	   {

		    		   viewRootMap.put( "UNITINFO", sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT) );
		    		   sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);


		    	   }
		    	   else if(sessionMap.containsKey("AUCTIONUNITINFO"))
		    	   {
		    		   viewRootMap.put("AUCTIONUNITINFO", sessionMap.get("AUCTIONUNITINFO"));
		    		   sessionMap.remove("AUCTIONUNITINFO");
		    	   }
		    	   if(sessionMap.containsKey(TENANT_INFO))
		    	    {
		    		   PersonView tenantViewRow=(PersonView)sessionMap.remove(TENANT_INFO);
		    		   viewRootMap.put(TENANT_INFO,tenantViewRow);
		    		   hdnTenantBlackListed = tenantViewRow.getIsBlacklisted()!= null ?tenantViewRow.getIsBlacklisted().toString():"0";
		    	       txtTenantType=getTenantType(tenantViewRow);
		    	    }
		    	   if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
		    		 &&  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null
		    		 && (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
		    	    )
			 	   {
	    		       contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	    		       sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
	    		       collectPayment();
			 	   }
		    	   //IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
		    	   //is copied to viewRoot of this page
                   if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
                		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
                   )
                   {
                	   viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
                			           sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
                	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
                   }
                	   getPaymentScheduleListFromSession();
		     }

//		       this.errorMessages = new ArrayList<String>(0);
//		       this.errorMessages.add(this.getBundle().getString("blocking.DetailsStatusMessage"));

		       logger.logInfo("init|Finish");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("init|Error Occured:",ex);
    	}

    }
    private void CheckIfPageModeView()
    {
    	 if((context.getExternalContext().getRequestMap().containsKey(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
                 &&	context.getExternalContext().getRequestMap().get(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW).toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
                 )||
                 (request.getParameter(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)!=null &&
              	request.getParameter(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW).toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
	    		    )
                 )
	    		 {
	    				pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
	    		 }

	    		 else
	    			pageMode=PAGE_MODE_UPDATE;

	    			viewRootMap.put("pageMode", pageMode);

    }
    @SuppressWarnings( "unchecked" )
    private void getDataFromTaskList()throws Exception
    {
    	   String methodName="getDataFromTaskList";
    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
		   if(userTask.getTaskAttributes().get("CONTRACT_ID")!=null)
		       contractId= userTask.getTaskAttributes().get("CONTRACT_ID").toString();
	       viewRootMap.put(WebConstants.CONTRACT_USER_TASK_LIST, userTask);
	       if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
	    	{
	    	    String taskType = userTask.getTaskAttributes().get("TASK_TYPE").toString();
		    	if( taskType.equals(WebConstants.NewLeaseContract_TaskTypes.APPROVE_LEASE_CONTRACT))
		    	{
		    		pageMode=PAGE_MODE_APPROVE_REJECT;
			    	viewRootMap.put("pageMode", pageMode);
		    	}
		    	if( taskType.equals(WebConstants.NewLeaseContract_TaskTypes.COLLECT_LEASE_CONTRACT_PAYMENT ) ||
		    		taskType.equals(WebConstants.NewLeaseContract_TaskTypes.SEND_CONTRACT_FOR_APPROVAL_AFTER_REJECTION )
		    	   )
		    	{
			    	pageMode=PAGE_MODE_UPDATE;
			    	viewRootMap.put("pageMode", pageMode);
		    	}
	    	}

	       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);



    }
    private void setContractDate( String sysConfigKey)throws Exception
    {

    	     String methodName="setContractDate";
    	     logger.logInfo(methodName,"Start");
    	     DateFormat dF= new SimpleDateFormat(getDateFormat());
    	     this.setDatetimerequest(dF.format(new Date()));
    	     logger.logInfo(methodName+"|Contract Start Date"+dF.format(new Date()));
    	     //IF DATE IS NOT CHANGED BY USER
    	     if( viewRootMap.get(IS_DATE_CHANGED).toString().equals("0") )
    	     {
    	     Calendar startCal=new GregorianCalendar();
    	     String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration(sysConfigKey);

    	     //Subtracting 1 as todays date will also be included
    	     startCal.add(Calendar.DATE, Integer.parseInt(daysInYear) );
	         this.setContractStartDate(startCal.getTime());
	         Calendar cal=new GregorianCalendar();
			 cal.set(startCal.get(Calendar.YEAR)+1, startCal.get(Calendar.MONTH),startCal.get(Calendar.DATE)-1);
			 logger.logInfo(methodName+"|Contract End Date"+dF.format(cal.getTime()));
		     this.setContractEndDate(cal.getTime());
    	     }

		     logger.logInfo(methodName,"Finish");

    }

    @SuppressWarnings( "unchecked" )
    private String getTenantType(PersonView tenantView)
    {

       String method="getTenantType";
       logger.logDebug(method+"|"+"Start...");
       DomainDataView ddv;
       if(tenantView!=null && tenantView.getPersonId()!=null && tenantView.getIsCompany()!=null )
       {
	       if( tenantView.getIsCompany().compareTo(new Long(1))==0)
	    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
	       else
	    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
	       if(getIsArabicLocale())
				return ddv.getDataDescAr();
		   else if(getIsEnglishLocale())
				return  ddv.getDataDescEn();
       }
            logger.logDebug(method+"|"+"Finish...");
    return "";



    }
    @Override
    public void preprocess()
    {
    	super.preprocess();
    	if(!isPostBack())
     	   logger.logInfo("|"+"Is not postback");
        setContractStartDate( (Date)viewRootMap.get("CONTRACT_START_DATE") );
    	setContractEndDate( (Date)viewRootMap.get("CONTRACT_END_DATE") );

    }

    @Override
    public void prerender()
    {

        String methodName="prerender";
    	try
    	{
    		   super.prerender();
		    	FillTenantInfo();
		    	FillApplicantInfo();
		    	//To ensure that unit data item is present in view root
		    	if(viewRootMap.containsKey("UNITINFO")|| viewRootMap.containsKey("AUCTIONUNITINFO"))
                        getUnitDataList();
		    	if(hdnManagerId!=null && hdnManagerId.trim().length()>0)
		    	{
		    	  String[] managerInfo =  getPersonNameIdFromHidden(hdnManagerId);
		    	  this.setTxtManager(managerInfo[1]);
		    	}
		    	if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0)
		    	{
		    	  String[] sponsorInfo =  getPersonNameIdFromHidden(hdnSponsorId);
		    	  this.setTxtSponsor(sponsorInfo[1]);
		    	}
		    	if(viewRootMap.containsKey(TENANT_INFO))
			    {
				   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);
			       //tenantRefNum=tenantViewRow.getTenantNumber();
				   hdnTenantBlackListed = tenantViewRow.getIsBlacklisted()!= null ?tenantViewRow.getIsBlacklisted().toString():"0";
			       txtTenantType=getTenantType(tenantViewRow);
			       String tenantNames="";
			        if(tenantViewRow.getPersonFullName()!=null)
			        tenantNames=tenantViewRow.getPersonFullName();
			        if(tenantNames.trim().length()<=0)
			        	 tenantNames=tenantViewRow.getCompanyName();
			       txtTenantName=tenantNames;

			    }
		    	calculatePaymentScheduleSummaray();
		    	getContractStatus();
		    	EnableDisableButtons();
		    	setPageTitle();

    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured:",ex);
    	}
    }
    @SuppressWarnings( "unchecked" )
    private void clearApplicationDetails()
    {
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_NUMBER);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DATE);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_ID);
    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_STATUS);

    }
    @SuppressWarnings( "unchecked" )
    private void populateApplicationDetailsTab()
    {
		//PopulatingApplicationDetailsTab
		if(requestView!=null)
		{
			if(requestView.getRequestNumber()!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
			if(requestView.getRequestDate()!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
			if(requestView.getStatusId()!=null)
			{
				if(getIsEnglishLocale())
				{
					if(requestView.getStatusEn()!=null)
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusEn());
				}else if(getIsArabicLocale())
				{
					if(requestView.getStatusAr()!=null)
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusAr());
				}
			}
			else
			{
				List<DomainDataView >ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
				DomainDataView ddv= CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
				if(getIsEnglishLocale())
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescEn());
			    else if(getIsArabicLocale())
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescAr());
			}
			if(requestView.getApplicantView()!=null)
				populateApplicantDetails(requestView.getApplicantView());


		}
	}
    @SuppressWarnings( "unchecked" )
    private void populateApplicantDetails(PersonView pv)
    {
    	if(pv.getPersonId()!=null )
       	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,pv.getPersonId());
      if(pv.getPersonFullName()!=null && pv.getPersonFullName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
     else if(pv.getCompanyName()!=null && pv.getCompanyName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
    	 String personType = getTenantType(pv);
     if(personType !=null)
         viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
     if(pv.getCellNumber()!=null)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
     if(pv.getPersonId()!=null)
     {
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,pv.getPersonId());
    	 hdnApplicantId = pv.getPersonId().toString();
     }

    }
    private String[] getPersonNameIdFromHidden(String hdn)
    {
      return hdn.split(":");
    }
    @SuppressWarnings( "unchecked" )
    private void FillApplicantInfo()throws PimsBusinessException
    {
    	String methodName="FillApplicantInfo";
    	logger.logInfo(methodName+"| Start");
    	PersonView applicantView;
    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW) &&
    	   viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW)!=null
    	   )
    	{
    		applicantView=(PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
	    	if(hdnApplicantId!=null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(hdnApplicantId));
	    		List<PersonView> personsList =  psa.getPersonInformation(pv);
	    		if(personsList.size()>0)
	    		{
	    			pv = personsList.get(0);
	    			populateApplicantDetails(pv);
	    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
	    		}

	    	}
    	}
    	else if(hdnApplicantId!=null && hdnApplicantId.trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(hdnApplicantId));
    		List<PersonView> personsList=  psa.getPersonInformation(pv);
    		if(personsList.size()>0)
    		{
    			pv = personsList.get(0);
    			populateApplicantDetails(pv);
    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
    		}
    	}

    	logger.logInfo(methodName+"| Finish");
    }
    @SuppressWarnings("unchecked")
    private void FillTenantInfo()throws PimsBusinessException
    {
    	String methodName="FillTenantInfo";
    	logger.logInfo(methodName+"| Start");
    	PersonView tenantView;
    	if(viewRootMap.containsKey(TENANT_INFO))
    	{
    		tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
	    	if(this.getHdnTenantId()!=null && this.getHdnTenantId().toString().trim().length()>0 && tenantView.getPersonId()!=null && tenantView.getPersonId().compareTo(new Long(this.getHdnTenantId()))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

	    	}
    	}
    	else if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(this.getHdnTenantId()));
    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
    		if(tenantsList.size()>0)
    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

    	}

    	logger.logInfo(methodName+"| Finish");
    }
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;

		return isViewModePopup;
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsContractCommercial()
	{
		boolean isContractCommercial=true;
		  DomainDataView ddv=new DomainDataView();
		if(!viewRootMap.containsKey(DDV_COMMERCIAL_CONTRACT))
		{
		  ArrayList<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
		  ddv=CommonUtil.getIdFromType(ddvList, WebConstants.COMMERCIAL_LEASE_CONTRACT);
		   viewRootMap.put(DDV_COMMERCIAL_CONTRACT,ddv);
		}
		else
		   ddv=(DomainDataView)viewRootMap.get(DDV_COMMERCIAL_CONTRACT);
		if(this.getSelectOneContractType()!=null && this.getSelectOneContractType().equals(ddv.getDomainDataId().toString()))
			isContractCommercial=true;
		else
			isContractCommercial=false;

		return isContractCommercial;
	}
	@SuppressWarnings( "unchecked" )
	public DomainDataView getResidentialContractDD()
	{

		DomainDataView ddv=new DomainDataView();
		if(!viewRootMap.containsKey(DDV_RESIDENTIAL_CONTRACT))
		{
		  ArrayList<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
		  ddv=CommonUtil.getIdFromType(ddvList, WebConstants.RESIDENTIAL_LEASE_CONTRACT);
		   viewRootMap.put(DDV_RESIDENTIAL_CONTRACT,ddv);
		}
		else
		   ddv=(DomainDataView)viewRootMap.get( DDV_RESIDENTIAL_CONTRACT );

		return ddv;
	}
	@SuppressWarnings( "unchecked" )
	public void selectContractType_Change()
	{
	   try
	   {
	   if(getIsContractCommercial())
	   {
	    tabCommercialActivity.setRendered(true);

	    if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_ADD) )
	    {
	    	setContractDate(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_COMMERCIAL_CONTRACT);

	    }
	   }
	   else
	   {
	    tabCommercialActivity.setRendered(false);
	    if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_ADD) )
	    {
	    	setContractDate(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT);

	    }
	   }
	   }
	   catch(Exception E)
	   {
		   logger.LogException("selectContractType_Change|Error ", E);
		   errorMessages = new ArrayList<String>(0);
		   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	   }

	}
	@SuppressWarnings( "unchecked" )
   private void setPageTitle()
   {
	     pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_NEW_LEASE_CONTRACT);

   }
   @SuppressWarnings("unchecked")
   private void EnableDisableButtons()throws Exception
   {
//	   String method="EnableDisableButtons";
//	   logger.logDebug(method+"|"+"Start...");
	   btnApproveAmendLease.setRendered(false);
	   btnRejectAmendLease.setRendered(false);
	   btnCancelContract.setRendered(false);
	   imgViewTenant.setRendered(false);
	   btnAllPayment.setRendered(false);
	   btnComplete.setRendered(false);
	   btnTerminateInEjari.setRendered(false);
	   btnRegisterEjari.setRendered(false);
	   if(isAnyPaymentCollected())
		   btnAllPayment.setRendered(true);
	   viewRootMap.put("applicationDetailsReadonlyMode",true);
	   tabPaidFacility_Click();
	   if(
			   getIsContrctStatusRejected()||
			   getIsContrctStatusCancelled()||
			   getIsContractStatusSettled() ||
			   getIsContrctStatusTerminated()
		 )
	   {
		   if(
				   (
					getIsContractStatusSettled() ||
				    getIsContrctStatusTerminated() 
				   )
				   && 
				   viewRootMap.get("CONTRACTVIEWUPDATEMODE")!= null
			 )
		   {
			   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			   if(
						contractView.getEjariNumber() != null && 
						contractView.getEjariNumber().trim().length()>0 &&
						(
							contractView.getTerminatedInEjari()== null || 
							contractView.getTerminatedInEjari().equals("0")
						 )
						
				  )
				{
				   btnTerminateInEjari.setRendered(true);	
				}
			   
			   
		   }
		   pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
		   viewRootMap.put("pageMode",pageMode);
	   }
	   if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_ADD))
	   {
		   viewRootMap.put("applicationDetailsReadonlyMode",false);
		   PageModeAdd();
		   if( !isPostBack() )
		   tabPanel.setSelectedTab(TAB_ID_BASIC);

	   }
	   else if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_UPDATE))
 	       PageModeUpdate();
	   else if(viewRootMap.get("pageMode").toString().equals(PAGE_MODE_APPROVE_REJECT))
		   PageModeApproveReject();
	   else if(viewRootMap.get("pageMode").toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW))
		   PageModeView();

//	   logger.logDebug(method+"|"+"Finish...");
   }
   @SuppressWarnings( "unchecked" )
   private void PageModeApproveReject()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);

	   btnAddPayments.setRendered(true);  // payment is editable for approval required state

	   startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true);
       viewRootMap.put("canAddNote", true);

       btnCollectPayment.setRendered(false); // payment is editable for approval required state
       btnGenPayments.setRendered(true);

	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false);
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	   
	 //  txtRefNumber.setRendered(true);
	 //  lblRefNum.setRendered(true);
       btnPrint.setRendered( true );
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(true);
	   btnReject.setRendered(true);
	   chkCommercial.setDisabled(true);
	   
	  // chkPaidFacilities.setDisabled(true);
   }
   @SuppressWarnings( "unchecked" )
   private void PageModeView()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(false);
       btnOccupierDelete.setRendered(false);
       imgDeleteManager.setRendered(false);
	   imgDeleteSponsor.setRendered(false);
	   cmbContractType.setDisabled(true);
       startDateCalendar.setDisabled(true);
       endDateCalendar.setDisabled(true);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(false);
       btnAddAuctionUnit.setRendered(false);
       imgTenant.setRendered(false);
       viewRootMap.put("canAddAttachment", true);
       viewRootMap.put("canAddNote", true);
       btnCollectPayment.setRendered(false);
	   btnSendNewLeaseForApproval.setRendered(false);
	   btnOccuipers.setRendered(false);
	   btnPartners.setRendered(false);
	   btnSaveNewLease.setRendered(false);
	   btnSaveAmendLease.setRendered(false);
	   btnAddPayments.setRendered(false);
	   
	  // txtRefNumber.setRendered(true);
	   //lblRefNum.setRendered(true);
	   btnGenPayments.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   imgManager.setRendered(false);
	   imgSponsor.setRendered(false);
	   btnApprove.setRendered(false);
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false);
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(true);
	   tabApplicationDetails.setRendered(false);
	   
	  //chkPaidFacilities.setDisabled(true);

   }
   @SuppressWarnings( "unchecked" )
   private void PageModeUpdate()
   {
	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);
	   cmbContractType.setDisabled(false);
	   btnAddPayments.setRendered(true);
       startDateCalendar.setDisabled(false);
       endDateCalendar.setDisabled(false);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(true);
       btnAddAuctionUnit.setRendered(true);
       imgTenant.setRendered(true);
       if(hdnTenantId!=null && hdnTenantId.length()>0)
    	 imgViewTenant.setRendered(true);
       btnCollectPayment.setRendered(true);
       viewRootMap.put("canAddAttachment", true);
       viewRootMap.put("canAddNote", true);
       btnGenPayments.setRendered(true);
	   btnOccuipers.setRendered(true);
	   btnPartners.setRendered(true);
	   btnSaveNewLease.setRendered(true);
	   btnSaveAmendLease.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   btnApprove.setRendered(false);
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false);
	   btnRejectAmendLease.setRendered(false);

	 //  chkPaidFacilities.setDisabled(true);
	   if(getIsContractCommercial())
	   {
		   chkCommercial.setDisabled(false);
		   imgManager.setRendered(true);
		   imgSponsor.setRendered(true);
		   viewSpnsrMgrIcons();
		   tabCommercialActivity.setRendered(true);
		   tabCommercialActivity_Click();
		   tabPartner.setRendered(true);


	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);
		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   }
	   List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
	    requestView =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
	   DomainDataView ddvNew=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
	   DomainDataView ddvAppReq=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
	   //if contract has been saved and contract status is new
	   if(contractId != null && contractId.trim().length()>0 &&
			   (getIsContractStatusNew() || getIsContractStatusApprovalRequired() ) )
	   {
		   //Even if the status is approval required the setRendered for SendNewLeaseForApproval is true for the case if contract was rejected
		   //then this send button is used to resend it for approval
		   btnSendNewLeaseForApproval.setRendered(true);
	       btnCollectPayment.setRendered(false);
	       btnPrint.setRendered( true );
	   }

	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew() )
		   btnCancelContract.setRendered(true);
	   else if(getIsContrctStatusActive())
		   ActiveContractForUpdateMode();
	   else if(getIsContractStatusApproved())
	   {
		   if(isAllPaymentsCollected())
		   {
			   btnComplete.setRendered(true);
			   if(viewRootMap.get("CONTRACTVIEWUPDATEMODE") != null)
			   {
				   contractView= (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE"); 
				   if(contractView.getEjariNumber()== null || contractView.getEjariNumber().trim().length()<=0)
				   {
					   btnRegisterEjari.setRendered(true);
				   }
			   }
			   btnCollectPayment.setRendered(false);
		   }
		   else
		   {
			   PageModeCollectPayment();
		   }
	   }

	   //Even if the status is approval required the setRendered for SendNewLeaseForApproval is true for the case if contract was rejected
	   //then this send button is used to resend it for approval

	   if( getIsContractStatusApprovalRequired() )
		   btnSendNewLeaseForApproval.setRendered( true );

	   if( requestView!=null &&
		   ddvNew.getDomainDataId().compareTo( requestView.getStatusId() )!=0  &&
		   ddvAppReq.getDomainDataId().compareTo( requestView.getStatusId() )!=0
		 )
    	   btnSendNewLeaseForApproval.setRendered(false);


   }
   @SuppressWarnings( "unchecked" )
   private void PageModeCollectPayment()
   {
	   PageModeView();
	   viewRootMap.put("canAddAttachment", true);
       viewRootMap.put("canAddNote", true);
       if( getTxttotalContractValue() != null)
       {
    	   double contractValue = new Double( txttotalContractValue );
    	   if( getTotalRentAmount() != contractValue )
    	   {
    		   errorMessages  = new ArrayList<String>(0);
           	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
           	tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
           	btnCollectPayment.setRendered(false);
    	   }
           	else
            {
     	     btnCollectPayment.setRendered(true);
            }
       }

	   //if no single payment is collected for the contract then it will cancelled otherwise it will not be cancelled
	   if(!isSinglePaymentCollected())
	    btnCancelContract.setRendered(true);
	   if(!isPostBack())
	      tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
   }
   @SuppressWarnings( "unchecked" )
    public void viewSpnsrMgrIcons() {

		   if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0 )//&& getIsCompanyTenant())
		   {
			   imgSponsor.setRendered(false);
			   imgDeleteSponsor.setRendered(true);
		       imgViewSponsor.setRendered(true);
		   }
		   else
		   {
			   imgSponsor.setRendered(true);
			   imgDeleteSponsor.setRendered(false);
		       imgViewSponsor.setRendered(false);
		   }
		   if(hdnManagerId!=null && hdnManagerId.trim().length()>0 )
		   {
			   imgManager.setRendered(false);
			   imgDeleteManager.setRendered(true);
			   imgViewManager.setRendered(true);
		   }
		   else
		   {
			   imgManager.setRendered(true);
			   imgDeleteManager.setRendered(false);
			   imgViewManager.setRendered(false);
		   }
   }
    public void imgDelManager_Click()
    {
 	   hdnManagerId="";
 	  this.setTxtManager("");
    }
   public void imgDelSponsor_Click()
   {
	   hdnSponsorId="";
	   this.setTxtSponsor("");
   }
   @SuppressWarnings( "unchecked" )
   private void ActiveContractForUpdateMode()
   {
	       tabApplicationDetails.setRendered(false);
		   txt_ActualRent.setReadonly(true);
	       btnPartnerDelete.setRendered(false);
	       btnRegisterEjari.setRendered(false);
	       btnOccupierDelete.setRendered(false);
	       imgDeleteManager.setRendered(false);
		   imgDeleteSponsor.setRendered(false);
		   cmbContractType.setDisabled(true);
	       btnAddPayments.setRendered(false);
	       btnSaveNewLease.setRendered(false);
	       if(! hasEditContractDurationPermission() )
	       {
	        startDateCalendar.setDisabled(true);
	        endDateCalendar.setDisabled(true);

	       }
	       else
	    	   btnSaveNewLease.setRendered(true);
	       
	       issueDateCalendar.setDisabled(true);
	       btnAddUnit.setRendered(false);
	       btnAddAuctionUnit.setRendered(false);
	       imgTenant.setRendered(false);
	       btnCancelContract.setRendered(true);
		   btnSendNewLeaseForApproval.setRendered(false);
		   btnOccuipers.setRendered(false);
		   btnPartners.setRendered(false);

		   btnSaveAmendLease.setRendered(false);

		   if(isAllPaymentsCollected())
		     btnCollectPayment.setRendered(false);
		   else
		   {
			 btnCollectPayment.setRendered(true);
			 
		   }
		   if(contractView.getEjariNumber()== null || contractView.getEjariNumber().trim().length()<=0)
		   {
				   btnRegisterEjari.setRendered(true);
		   }
		   btnGenPayments.setRendered(false);
		   btnCompleteAmendLease.setRendered(false);
		   btnSubmitAmendLease.setRendered(false);
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   btnApprove.setRendered(false);
		   btnReject.setRendered(false);
		   btnApproveAmendLease.setRendered(false);
		   btnRejectAmendLease.setRendered(false);
		   chkCommercial.setDisabled(true);
		   chkPaidFacilities.setDisabled(true);
		   btnAllPayment.setRendered(true);

   }
   @SuppressWarnings( "unchecked" )
   private void PageModeAdd()
   {

	   txt_ActualRent.setReadonly(true);
       btnPartnerDelete.setRendered(true);
       btnOccupierDelete.setRendered(true);

	   cmbContractType.setDisabled(false);
	   btnAddPayments.setRendered(true);
       startDateCalendar.setDisabled(false);
       endDateCalendar.setDisabled(false);
       issueDateCalendar.setDisabled(true);
       btnAddUnit.setRendered(true);
       btnAddAuctionUnit.setRendered(true);
       imgTenant.setRendered(true);
       if(hdnTenantId!=null && hdnTenantId.length()>0)
    	   imgViewTenant.setRendered(true);

       btnCollectPayment.setRendered(false);
	   btnGenPayments.setRendered(true);
	   btnOccuipers.setRendered(true);
	   btnPartners.setRendered(true);
	   btnSaveNewLease.setRendered(true);
	   btnSaveAmendLease.setRendered(false);
	   btnCompleteAmendLease.setRendered(false);
	   btnSubmitAmendLease.setRendered(false);
	   btnApprove.setRendered(false);
	   btnReject.setRendered(false);
	   btnApproveAmendLease.setRendered(false);
	   btnRejectAmendLease.setRendered(false);
	   chkCommercial.setDisabled(false);
	   viewRootMap.put("canAddAttachment", true);
       viewRootMap.put("canAddNote", true);


	   if(viewRootMap.containsKey(UNIT_DATA_ITEM) )
	   {
		   tabFacilities.setRendered(true);
		   tabPaymentTerms.setRendered(true);
		   //tabPaidFacility_Click();
	   }
	   else{
		   tabFacilities.setRendered(false);
		   tabPaymentTerms.setRendered(false);
	   }
	   if(getIsContractCommercial())
	   {
		   viewSpnsrMgrIcons();

		   tabCommercialActivity.setRendered(true);
		   tabCommercialActivity_Click();
		   tabPartner.setRendered(true);
	   }
	   else
	   {
		   this.setHdnManagerId("");
		   this.setHdnSponsorId("");
		   this.txtSponsor="";
		   this.txtManager="";
		   imgManager.setRendered(false);
		   imgSponsor.setRendered(false);
		   tabCommercialActivity.setRendered(false);
		   tabPartner.setRendered(false);
		   sessionMap.remove(WebConstants.PARTNER);

		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	   }
	   //if contract has been saved and contract status is new
	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
		   btnSendNewLeaseForApproval.setRendered(true);
	   else
		   btnSendNewLeaseForApproval.setRendered(false);

//	   if(!isPostBack() )
//		   this.setSelectOneContractType(getResidentialContractDD().getDomainDataId().toString());
   }
    public boolean getIsCompanyTenant()
    {

    	isCompanyTenant=false;

		if(viewRootMap.containsKey(TENANT_INFO))
	    {
			PersonView tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
		    if(tenantView.getIsCompany().compareTo(new Long(1))==0)
		    	return true;
		    else
		    	return false;
	    }

		return isCompanyTenant;
    }
    public void RemoveSessions()
    {

        //Remove session bound with Tabs Start

       	     if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER));
		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER);
		     if(sessionMap.containsKey(WebConstants.OCCUPIER));
		         sessionMap.remove(WebConstants.OCCUPIER);
		     if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
	         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);

	         if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	         sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);

	         if(viewRootMap.containsKey(WebConstants.CONTRACT_USER_TASK_LIST));
	         viewRootMap.remove(WebConstants.CONTRACT_USER_TASK_LIST);
	         //Remove session bound with Tabs Finish

	         if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
	        	 sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
	         if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
	        	 sessionMap.remove(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
	         if(sessionMap.containsKey("UNITINFO"))
	        	 sessionMap.remove("UNITINFO");

	         if(sessionMap.containsKey(TENANT_INFO))
	        	 sessionMap.remove(TENANT_INFO);

	         if(sessionMap.containsKey("AUCTIONUNITINFO"))
	        	 sessionMap.remove("AUCTIONUNITINFO");



    }
    public boolean getIsPageUpdateStatusNew()
    {
    	String method="getIsPageUpdateStatusNew";
    	isPageUpdateStatusNew=false;
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
	    	{
	    		  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	        	  logger.logInfo(method+"|"+"Contract Status Approved..."+contractView.getStatus());
	    	      if(contractView.getStatus()!=null && contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_NEW)) )
	    	      {
	    	    		isPageUpdateStatusNew=true;

	    	    	}

	    	}

    	return isPageUpdateStatusNew;
    }
    public boolean getIsPageModeAdd()
    {
    	String method="getIsPageModeAdd";
    	boolean isPageModeAdd=false;
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) )
	    	{
	    		isPageModeAdd=true;

	    	}

    	return isPageModeAdd;
    }
    public boolean getIsPageModeAmendAdd()
    {
    	String method="getIsPageModeAmendAdd";
    	isPageModeAmendAdd=false;
	    	if(pageMode!=null && pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) )
	    	{
	    		isPageModeAmendAdd=true;

	    	}

    	return isPageModeAmendAdd;
    }
    public boolean getIsPageModeUpdate()
    {
    	String method="getIsPageModeUpdate";
    	isPageModeUpdate=false;
    	//For both update and approve the view should remain same except for APPROVE/REJECT buttons
	    	if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE)  || pageMode.equals(PAGE_MODE_APPROVE_REJECT) )
	    	{
	    		isPageModeUpdate=true;

	    	}

    	return isPageModeUpdate;
    }
    private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	String method="getDomainDataListForDomainType";
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() )
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 )
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd)
				{
				  ddvList.add(ddv);
				}
				break;
			}
		}
    	return ddvList;
    }
    @SuppressWarnings( "unchecked" )
    public boolean getIsContractStatusApproved()
    {
    	isContractStatusApproved=false;
    	if(pageMode.equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
    	{
    	  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	    	if( contractView.getStatus()!=null &&
	    		(
	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED))||
	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_RENEW_APPROVED))
	    		)
	    	  )
	    	{
	    		isContractStatusApproved=true;

	    	}
    	}
    	return isContractStatusApproved;
    }
    @SuppressWarnings( "unchecked" )
    private String getIdForStatus(String statusName)
    {
         Long ID=new Long(0);

    	       List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
    	       for(int i=0;i<ddvList.size();i++)
    	       {

    	    	   	DomainDataView ddv=(DomainDataView)ddvList.get(i);
    	    	   if(ddv.getDataValue().equals(statusName))
    	    	   {

    	    		   ID=ddv.getDomainDataId();
    	    		//   logger.logInfo(method+"|"+"Status Matched with id..."+ID);
    	    	        break;
    	    	   }
    	       }
    	return ID.toString();


    }
    @SuppressWarnings( "unchecked" )
    public boolean getIsUnitCommercial()
    {
    	String method="getIsUnitCommercial";

    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
    	  ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE);

    	if(viewRootMap.containsKey(UNIT_DATA_ITEM) )
	    {
			isUnitCommercial=false;
			    UnitView unitView=(UnitView)viewRootMap.get(UNIT_DATA_ITEM);
				HashMap hMap= getIdFromType(ddvList,WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL);
			    logger.logDebug(method+"|"+"The usuageType of unit..."+unitView.getUsageTypeId().toString());
			    if(hMap.containsKey("returnId"))
			    {
				    if(unitView.getUsageTypeId().toString().equals(hMap.get("returnId").toString()))
				    	isUnitCommercial=true;
				    else
				    {
				    	isUnitCommercial=false;
				    	if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
		  		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);
				    }
			    }
		}
		return isUnitCommercial;
    }

    public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);

    	dateFormat= localeInfo.getDateFormat();

		return dateFormat;
	}
    public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
    public boolean getIsArabicLocale()
	{

		isArabicLocale = !getIsEnglishLocale();

		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{

				LocaleInfo localeInfo = getLocaleInfo();

				isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}
	public LocaleInfo getLocaleInfo()
	{

    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);

	     return localeInfo;

	}
	@SuppressWarnings( "unchecked" )
	private void loadTypes() throws PimsBusinessException
	{
		   String method="loadTypes";
		   logger.logInfo(method+"|"+"Start..");
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
		   {
			  logger.logInfo(method+"|"+"Getting types of unit from application context with type name:"+WebConstants.UNIT_USAGE_TYPE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE, commonUtil.getDomainDataListForDomainType(WebConstants.UNIT_USAGE_TYPE));
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TENANT_TYPE))
		   {
			   logger.logInfo(method+"|"+"Getting types of tenant from application type with type type:"+WebConstants.TENANT_TYPE);

			   List<DomainDataView> ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
				   //psa.getDomainDataByDomainTypeName(WebConstants.TENANT_TYPE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE, ddl);
			   DomainDataView ddvCompany= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_COMPANY);
			   viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY,ddvCompany);
			   DomainDataView ddvIndividual= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_INDIVIDUAL);
			   viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL,ddvIndividual);

		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TYPE))
		   {
			   logger.logInfo(method+"|"+"Getting types of contract from application context with type name:"+WebConstants.CONTRACT_TYPE);
			   List<DomainDataView> contractTypeList = new ArrayList<DomainDataView>(0);
			   List<DomainDataView>ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);
			   DomainDataView ddv = CommonUtil.getIdFromType(ddvList, WebConstants.COMMERCIAL_LEASE_CONTRACT);
			   contractTypeList.add(ddv);
			   ddv = CommonUtil.getIdFromType(ddvList, WebConstants.RESIDENTIAL_LEASE_CONTRACT);
			   contractTypeList.add(ddv);
			   //Collections.sort(contractTypeList,ListComparator.LIST_COMPARE);
			   viewRootMap.put(WebConstants.SESSION_CONTRACT_TYPE,contractTypeList );
		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
		   {
			   logger.logInfo(method+"|"+"Getting types of persons from application context with type name:"+WebConstants.PERSON_TYPE);
			   viewRootMap.put(WebConstants.SESSION_PERSON_TYPE, CommonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE));

		   }
		   if(!viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
		   {
			   logger.logInfo(method+"|"+"Getting types of request from application context with type name:"+WebConstants.REQUEST_STATUS);
			   viewRootMap.put(WebConstants.SESSION_REQUEST_STATUS, CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS));


		   }
		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CASH))
		   {
			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cash");
			   List<DomainDataView> ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			   DomainDataView ddvCash= CommonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CASH,ddvCash);

		   }
		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE))
		   {
			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cheque");
			   List<DomainDataView> ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			   DomainDataView ddvCheque= CommonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE,ddvCheque);

		   }
		       loadPaymentStatus();

					   logger.logInfo(method+"|"+"Finish..");
	}
	@SuppressWarnings( "unchecked" )
	private void loadPaymentStatus()
	{
	  String methodName="loadPaymentStatus";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
		  {
			  List<DomainDataView>ddv= CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);
		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, ddv);
		  }
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }

	  logger.logInfo(methodName +"|"+"Finish ");



	}
	@SuppressWarnings( "unchecked" )
	private void getContractById(String contractId)throws Exception,PimsBusinessException
	{
				ContractView contract=new ContractView();
				contract.setContractId(new Long(contractId));
				HashMap contractHashMap=new HashMap();
				contractHashMap.put("contractView",contract);
				contractHashMap.put("dateFormat",getDateFormat());
				PropertyServiceAgent psa =new PropertyServiceAgent();
				List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
				if(contractViewList.size()>0)
				{
					contractView=(ContractView)contractViewList.get(0);
					
					this.setBlockForRenewal(contractView.isRenewBlocked());
					this.setBlockComment(contractView.getBlockComment());
					this.setUnblockComment(contractView.getUnblockComment());
					viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
					getRequestForNewLeaseContract();
				}
	}
	@SuppressWarnings( "unchecked" )
	private void getRequestForNewLeaseContract()
			throws PimsBusinessException {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_LEASE_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(contractView.getContractId());
		cv.setContractNumber(contractView.getContractNumber());
		rv.setContractView(cv);

		if ( getRequestAndPopulateAppDetails(psa, rv) )
		{
			return;
		}
	  //If Contract was not made due to Lease Contract App than it might be due to Renew Contract App
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_RENEW_CONTRACT);
		if ( getRequestAndPopulateAppDetails(psa, rv) )
		{
			return;
		}
		//If Contract was not made due to Renew App than it might be due to Transfer Contract App
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_TRANSFER_CONTRACT);
		if ( getRequestAndPopulateAppDetails(psa, rv) )
		{
			return;
		}
	}

	@SuppressWarnings("unchecked")
	private boolean getRequestAndPopulateAppDetails(PropertyServiceAgent psa,
			RequestView rv) throws PimsBusinessException {
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0);
			viewRootMap.put("REQUESTVIEWUPDATEMODE",requestView );
			populateApplicationDetailsTab();
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
    private void loadCombos()throws PimsBusinessException
    {
    	loadStatus();

    }
	@SuppressWarnings( "unchecked" )
    private void loadStatus() throws PimsBusinessException
    {

       if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_STATUS))
   	   {
   		    List<DomainDataView> ddl = CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
   		    viewRootMap.put(WebConstants.SESSION_CONTRACT_STATUS, ddl);
   	   }

    }
    public String btnBack_Click()
    {
    	String eventOutcome = "back";
    	return eventOutcome;
    }
    public void btnCancelContract_Click()
    {
    	String methodName = "cancel";
    	logger.logInfo(methodName+"|"+" Start...");
    	errorMessages=new ArrayList<String>(0);
        errorMessages.clear();
        successMessages=new ArrayList<String>(0);
        successMessages.clear();
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	try
    	{

    		    if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
	   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    		    DomainDataView ddvContractStatusActive = CommonUtil.getIdFromType((List<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS),WebConstants.CONTRACT_STATUS_ACTIVE);
		    	if(contractView.getStatus().compareTo(ddvContractStatusActive.getDomainDataId())==0)
	        	{
	        		if(new GRPService().isAnyTransactionPostedToGRP(contractView.getContractNumber()))
	        		{
	        			errorMessages=new ArrayList<String>();
		        		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeCancelled"));
	        		}
		        	else
		        		openPopup("openRemarksPopup();");
	        	}
	        	else
	        		openPopup("openRemarksPopup();");

			    logger.logInfo(methodName+"|"+" Finish...");


    	}

    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}


    }
    @SuppressWarnings( "unchecked" )
    public String btnApprove_Click()
    {
    	String methodName = "btnApprove_Click";
        errorMessages=new ArrayList<String>(0);
        successMessages=new ArrayList<String>(0);
        errorMessages.clear();
        successMessages.clear();
    	RequestServiceAgent rsa = new RequestServiceAgent();
    	try
    	{
    	   if(pageMode.equals(PAGE_MODE_APPROVE_REJECT) )
		   {

    		   if (!hasErrors() && addRequest()) {

    		   if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
			   {
			     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
			     if(contractId !=null && contractId.trim().length()>0)
			     rsa.changeRequestDetailStatusForAuction(rdvList , new Long(contractId),WebConstants.MovedToContract.BINDED_WITH_CONTRACT,WebConstants.AuctionUnitStatus.IN_CONTRACT);
				}
    		   if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
   	   	        getRequestForNewLeaseContract();//updateUnitStatusToRented();
   	   	        logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode start...");
 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED);
 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
 		      	if(approvedStatus!=null)
 		      	   contractView.setStatus( new Long(approvedStatus));

 		      	if(requestView!=null  && requestView.getRequestId()!=null)
 		      	{
 		      		saveComments(requestView.getRequestId());
				    saveAttachments(requestView.getRequestId().toString() );
 		      	}

 		      	notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_LeaseContract_Approval);
 		      	pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
			    viewRootMap.put("pageMode",pageMode);
			    saveSystemComments(MessageConstants.ContractEvents.CONTRACT_APPROVED);

			    ApproveRejectTask(TaskOutcome.APPROVE);
			    successMessages = new ArrayList<String>( 0 );
 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_APPROVED_SUCCESSFULLY));
    		   }
		  }
    	}
//    	catch(PIMSWorkListException ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//
//    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);

    	}
    	return "";
    }

    @SuppressWarnings( "unchecked" )
    public String btnReject_Click()
    {
    	String methodName = "btnReject_Click";

    	logger.logInfo(methodName+"|"+" Start...");
    	errorMessages=new ArrayList<String>(0);
        errorMessages.clear();
        successMessages=new ArrayList<String>(0);
        successMessages.clear();
    	RequestServiceAgent rsa = new RequestServiceAgent();
    	try
    	{
    	 if(pageMode.equals(PAGE_MODE_APPROVE_REJECT))
		 {
		    	ApproveRejectTask(TaskOutcome.REJECT);
//		    	if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
//				{
//				 List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
//				 rsa.changeRequestDetailStatusForAuction(rdvList , null,WebConstants.MovedToContract.NOT_MOVED_TO_CONTRACT,WebConstants.AuctionUnitStatus.WON);
//				}
		    	//new PropertyService().setContractAsNew( Long.parseLong( contractId ) );
		    	if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
	   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		    	String status=getIdForStatus(WebConstants.CONTRACT_STATUS_REJECT);
//		    	String status=getIdForStatus(WebConstants.CONTRACT_STATUS_NEW);
 		      	if(status!=null)
 		      	   contractView.setStatus( new Long(status));

		    	getRequestForNewLeaseContract();
	   	   	    if(requestView!=null  && requestView.getRequestId()!=null)
		      	{
//		      		logger.logInfo(methodName+"|"+" Saving Notes...Start");
		      		saveComments(requestView.getRequestId());
//				    logger.logInfo(methodName+"|"+" Saving Notes...Finish");
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Start");
				    saveAttachments(requestView.getRequestId().toString() );
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		      	}
		    	pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
			    viewRootMap.put("pageMode",pageMode);
			    saveSystemComments(MessageConstants.ContractEvents.CONTRACT_REJECTED);
			    successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_REJECED_SUCCESSFULLY));
			    logger.logInfo(methodName+"|"+" Finish...");

		 }
    	}
    	catch(PIMSWorkListException ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);

    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}


    	return "";
    }
    @SuppressWarnings( "unchecked" )
    private void removeUnPaidRentFromPaymentScheduleSession()
    {
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> psvList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);

    	for (PaymentScheduleView psv : psvList)
    	{
		   //If payment is for Rent
    		if(psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 	)
			{

    			//if Rent is paid
				if(!psv.getIsReceived().equals("N") ) {
					newPaymentScheduleViewList.add(psv);
				}
				else if (psv.getPaymentScheduleId()!=null){ // that are not paid but are in DB
					psv.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					psv.setRecordStatus(WebConstants.DELETED_RECORD_STATUS);
					newPaymentScheduleViewList.add(psv);
				}
			}
    		//If payment other than rent
			else
        	  	newPaymentScheduleViewList.add(psv);

    	}
    	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );

    }
    @SuppressWarnings( "unchecked" )
    public String openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp|";
		logger.logInfo(methodName+"Start|loggedInUser:"+getLoggedInUser());
        if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
    	 )
    	{
	        List<PaymentScheduleView> paymentSchedules = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	        List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
			for (int i=0; i<paymentSchedules.size(); i++)
			{
				if (paymentSchedules.get(i).getSelected()!=null && paymentSchedules.get(i).getSelected())
					paymentsForCollection.add(paymentSchedules.get(i));
			}
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentsForCollection );
    	}
        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
        openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");
		logger.logInfo(methodName+"Finish|loggedInUser:"+getLoggedInUser());

		return "";
	}
    @SuppressWarnings( "unchecked" )
    public void btnGenPayments_Click()
    {
    	String methodName = "btnGenPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
    	try
    	{
    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    		if(txttotalContractValue!=null && txttotalContractValue.length()>0)
    		{

    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
    		    {
    			 removeUnPaidRentFromPaymentScheduleSession();
    		     alreadyPresentPS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,alreadyPresentPS);
    		    }
    			 String extrajavaScriptText ="var screen_width = 1024;"+
                 "var screen_height = 450;"+
                 "window.open('GeneratePayments.jsf?ownerShipTypeId="+
                viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+"','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=100,top=150,scrollbars=yes,status=yes');";
 				sessionMap.put(WebConstants.TOTAL_CONTRACT_VALUE,txttotalContractValue);
 				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_START_DATE,df.format(this.getContractStartDate()));
 				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_END_DATE,df.format(this.getContractEndDate()));

 				sessionMap.put(WebConstants.GEN_PAY_UPDATE_PAYMENT, true);

 		        openPopUp("GeneratePayments.jsf", extrajavaScriptText);
    		}
    		else
    			logger.logInfo(methodName+"|"+" Total Contract Value is not present...");
    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);
    	}
    }
    @SuppressWarnings( "unchecked" )
    public void btnEditPaymentSchedule_Click()
    {
    	String methodName = "btnEditPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) && viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
    	if( psv.getIsSystem() != null && psv.getIsSystem().longValue() == 1l )
    	{
	    	sessionMap.put("STOP_AMOUNT_EDIT",true);
    	}
    	if( getIsPageModeUpdate() && getIsContractStatusApproved() )
    		sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
    	openPopUp("", "javascript:openPopupEditPaymentSchedule();");
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()
    {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	CommonUtil.viewPaymentDetails(psv);

    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings( "unchecked" )
    public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	CommonUtil.printPaymentReceipt(this.contractId, psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.NEW_LEASE_CONTRACT);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    @SuppressWarnings( "unchecked" )
    public void btnAddPayments_Click()
    {
    	String methodName = "btnAddPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PropertyServiceAgent psa =new PropertyServiceAgent();
    	List<PaymentScheduleView> PS=new ArrayList<PaymentScheduleView>();
    	String psStatusInReq="";
    	try
    	{
    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
    			  )
    		    {
    		     PS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,PS);
    		    }
    			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
    			{
    			   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    			   DomainDataView ddv = CommonUtil.getDomainDataFromId
    			                                            (
    					                                     (List<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS),
	                                                          contractView.getStatus()
	                                                         );
    			   if(ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_NEW && ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_DRAFT )
    			   {
    				    setRequestParam(WebConstants.PAYMENT_STATUS_IN_REQUEST,WebConstants.PAYMENT_SCHEDULE_PENDING);
    				    psStatusInReq = WebConstants.PAYMENT_SCHEDULE_PENDING;
    			   }

    			}
    			 String extrajavaScriptText ="var screen_width = 1024;"+
                 "var screen_height = 450;"+
                 "window.open('PaymentSchedule.jsf?" +
                 "totalContractValue="+txttotalContractValue+"&"+
                 WebConstants.PAYMENT_STATUS_IN_REQUEST+"="+psStatusInReq+
                 "&ownerShipTypeId="+viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+
                 "','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
 		        openPopUp("PaymentSchedule.jsf", extrajavaScriptText);

    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);
    	}
    }
    @SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleStausId( String status)
    {
    	DomainDataView ddv = new DomainDataView();
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
    	{
    	 List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	 ddv= CommonUtil.getIdFromType(ddvList, status);
    	}
    	else
    	 ddv = (DomainDataView)viewRootMap.get( status ) ;
    	return ddv.getDomainDataId();

    }
    @SuppressWarnings( "unchecked" )
    public Long getPaymentSchedulePendingStausId()
    {
    	DomainDataView ddv = new DomainDataView();
    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
    		loadPaymentStatus();
    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
    	{
    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	 ddv= CommonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_PENDING);
    	}
    	else
    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_PENDING) ;
    	return ddv.getDomainDataId();

    }
    @SuppressWarnings( "unchecked" )
    public void btnDelPaymentSchedule_Click()
    {
    	String methodName = "btnDelPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	try{
    	PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	if(pageMode.equals(PAGE_MODE_ADD) || psv.getPaymentScheduleId() == null )
    		removeFromPaymentScheduleSession(psv);
    	else
    		markPaymentScheduleAsDeleted(psv);

    	getPaymentScheduleListFromSession();
    	logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);

    	}
    }
    @SuppressWarnings( "unchecked" )
    private void markPaymentScheduleAsDeleted(PaymentScheduleView psv)
    {
    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList)
        {
            //If payment schedule existed in DB
        	if(psv.getPaymentScheduleId()!=null )
        	{
	        	if(psv.getPaymentScheduleId().compareTo(paymentScheduleView.getPaymentScheduleId())==0 )
	        	{
	        		paymentScheduleView.setIsDeleted(new Long(1));

	        	}
	        	newPaymentScheduleViewList.add(paymentScheduleView);
        	}

           }
        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList);

    }
    @SuppressWarnings( "unchecked" )
    private void removeFromPaymentScheduleSession(PaymentScheduleView psv)
    {
    	String methodName="removeFromPaymentScheduleSession";
    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList)
//        {
//        	//To uniquely identify the payment in add Mode
//        	//Combination of Payment Type and PaymentDueOn should be unique
////        	if(psv.getPaymentDueOn().compareTo(paymentScheduleView.getPaymentDueOn())==0
////        		&&	psv.getTypeId().compareTo(paymentScheduleView.getTypeId())==0
////        			   	)
////        		logger.logDebug(methodName+"|"+" ...");
////        	else
//        	if( psv.getRowId().compareTo( paymentScheduleView.getRowId() )!= 0 )
//        	{
//              		newPaymentScheduleViewList.add(paymentScheduleView);
//        	}
//        }
      	//viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
      	paymentScheduleViewList.remove(psv);
      	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList );



    }
    @SuppressWarnings( "unchecked" )
    private void setRequestFieldDetailView(
			RequestFieldDetailView requestFieldDetailView ) throws PimsBusinessException {


		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
		requestFieldDetailView.setRequestKeyValue(contractId);

		RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
	}
    @SuppressWarnings( "unchecked" )
   public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	String notesOwner = WebConstants.REQUEST;

	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
    	}
    	catch (Throwable throwable) {
			logger.LogException(  "saveComments| crashed ", throwable);
		}
    	return success;
    }
    @SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{

	    	if(referenceId!=null)
	    	{
	    		viewRootMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    		viewRootMap.put("entityId", referenceId);
		    	success = CommonUtil.updateDocuments();
	    	}
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}

    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments(Long requestId)
	{
	
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_NEW_LEASE_CONTRACT;

    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);

		if(requestId!= null){

	    	String entityId = requestId.toString();

			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	@SuppressWarnings( "unchecked" )
    public void removePaymentScheduleIds()
   {
		String methodName = "removePaymentScheduleIds";
    	logger.logInfo(methodName+"|"+" Start...");
    	Set<PaymentScheduleView> newPsv=new HashSet<PaymentScheduleView>(0);
    	Set<PaymentScheduleView> psv =contractView.getPaymentScheduleView();
    	Iterator iter=psv.iterator();
    	while(iter.hasNext())
    	{
    		PaymentScheduleView  paymentScheduleView=(PaymentScheduleView)iter.next();
    		if(paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
    		{
    			if(paymentScheduleView.getPaymentScheduleId()!=null)
    			{
    			   paymentScheduleView.setOldPaymentScheduleId(paymentScheduleView.getPaymentScheduleId().toString());
    			   paymentScheduleView.setPaymentScheduleId(null);
    			   paymentScheduleView.setPaymentNumber(null);
    			}
    			newPsv.add(paymentScheduleView);
    		}
    	}
    	contractView.setPaymentScheduleView(newPsv);

    	logger.logInfo(methodName+"|"+" Finish...");
   }
	@SuppressWarnings( "unchecked" )
	public String btnSubmit_Click()
    {
        String eventOutcome="failure";
        try
        {
	
	        FacesContext context = FacesContext.getCurrentInstance();
	        UIViewRoot view = context.getViewRoot();
	        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
	        session.setAttribute("view",view);
	        //errorMessages.clear();
			valueChangeListerner_RentValue();
	        if(addRequest())
	        {
	           eventOutcome="";
	        }
	        
        } catch(Exception e)
        {
        	logger.LogException("btnSubmit_Click|Error Occured...",e);	
        }

        return eventOutcome;
    }
	@SuppressWarnings( "unchecked" )
    public void btnSendNewLeaseForApproval()
    {
    	String methodName = "btnSendLeaseForApproval";

    	logger.logInfo(methodName+"|"+" Start...");
    	SystemParameters parameters = SystemParameters.getInstance();
    	errorMessages = new ArrayList<String>(0);
    	try
    	{
    		requestView  =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
    		if( !hasErrors() )
    		{
	    	    String endPoint= parameters.getParameter(WebConstants.LEASE_CONTRACT_BPEL_ENDPOINT);
	    	    ContractInitiateBPELPortClient port=new ContractInitiateBPELPortClient();
		   	    port.setEndpoint(endPoint);
		   	    if(requestView.getRequestId()!=null)
		    	{
		    		Long requestId=requestView.getRequestId();
		    		CommonUtil.printReport(requestId);

		    	}

		   		if(contractId!=null && contractId.trim().length()>0)
		   		{
		   			String approvedrequired=getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED);
		   			if( getIsContractStatusNew() )
		   			{
		   				logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
		   				port.initiate(new Long(contractId),requestView.getRequestId(),getLoggedInUser(), null, null);
				   	    if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
				   	    contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
   				   	    if(approvedrequired!=null)
				   	      contractView.setStatus( new Long(approvedrequired));
				   	    getRequestForNewLeaseContract();
		   			}
		   			else
		   				ApproveRejectTask(TaskOutcome.OK);



		   	      notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_LeaseContract_Received);
		   	      successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
		   	      pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
			      viewRootMap.put("pageMode",pageMode);
			      if(requestView!=null  && requestView.getRequestId()!=null)
	 		      	{
	 		      		logger.logInfo(methodName+"|"+" Saving Notes...Start");
	 		      		saveComments(requestView.getRequestId());
					    logger.logInfo(methodName+"|"+" Saving Notes...Finish");
					    logger.logInfo(methodName+"|"+" Saving Attachments...Start");
					    saveAttachments(requestView.getRequestId().toString() );
					    logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
	 		      	}

			      saveSystemComments(MessageConstants.ContractEvents.CONTRACT_SENT_FOR_APPROVAL);
		   	      btnSendNewLeaseForApproval.setRendered(false);
		   		}
			    logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..COMPLETED ");
    		}
		    logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    	}



    }
	@SuppressWarnings( "unchecked" )
    private void refreshContract()
    {
    	contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	//refreshPaymentSchedules - Start
    	List psViewList =  new ArrayList<PaymentScheduleView>();
    	Set<PaymentScheduleView> psSet = contractView.getPaymentScheduleView();
    	for (PaymentScheduleView paymentScheduleView : psSet) {
    		psViewList.add(paymentScheduleView );
		}
    	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, psViewList);
    	//refreshPaymentSchedules - Finish
    }
	@SuppressWarnings( "unchecked" )
    private boolean addRequest()
    {
    	String methodName = "addRequest";
    	boolean isSucceed=false;
    	List<PaymentReceiptView> paymentReceiptViewList=new ArrayList<PaymentReceiptView>();
    	logger.logInfo(methodName+"|Start|pageMode:%s",pageMode);
    	try
    	{

		    	PropertyServiceAgent myPort = new PropertyServiceAgent();
		    	RequestServiceAgent  rsa = new RequestServiceAgent();
				if(pageMode.equals("ADD"))
			    {
					if(!putControlValuesInView())
			    	{
						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
						Long contractsId=contractView.getContractId();
						if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
						{
					     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
					     rsa.changeRequestDetailStatusForAuction(rdvList , contractsId,WebConstants.MovedToContract.MOVED_TO_CONTRACT,WebConstants.AuctionUnitStatus.IN_CONTRACT);
						}
						contractId =contractView.getContractId().toString();
				    	viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
				    	getRequestForNewLeaseContract();
				    	refreshContract();

	         		    pageMode=PAGE_MODE_UPDATE;
						viewRootMap.put("pageMode",pageMode);
				    	this.setRefNum(contractView.getContractNumber());
				    	this.txtEjariNum.setValue(contractView.getEjariNumber());
				    	
				    	saveSystemComments(MessageConstants.ContractEvents.CONTRACT_CREATED);
				    	tabPanel.setSelectedTab("appDetailsTab");
				    	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_CREATED_SUCCESSFULLY));
			    	}
			    }
				else
				{
					if(getIsContrctStatusActive())
					{
						
						saveOnActive();

					}

					else if(putUpdatedControlValuesInView())
					{
					  if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
						paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
					    contractView.setPaymentScheduleView(getPaymentSchedule());
						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
						Long contractId=contractView.getContractId();
						viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
						if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
						{
					     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
					     rsa.changeRequestDetailStatusForAuction(rdvList , contractId,WebConstants.MovedToContract.MOVED_TO_CONTRACT,WebConstants.AuctionUnitStatus.IN_CONTRACT);
						}

						saveSystemComments(MessageConstants.ContractEvents.CONTRACT_UPDATED);
						successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY));
					}

				}
				requestView = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
				if(requestView !=null && requestView.getRequestId()!=null)
				{

				 saveComments(requestView.getRequestId());
				 saveAttachments(requestView.getRequestId().toString());
				}
				
				isSucceed=true;

			logger.logInfo(methodName+"|"+" Finish...");
    	}

    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...", ex);
    	}
    	return isSucceed;



    }
	private boolean hasSaveOnActiveErrors()
	{
		boolean hasErrors = false;
		if(getContractStartDate()!=null && !getContractStartDate().equals("")
				 && getContractEndDate()!=null && !getContractEndDate().equals("") )
		{

  		  if (getContractStartDate().compareTo(getContractEndDate())>=0)
  		  {
  				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
  				return true;
  		  }
		}
	  	else
	  	{

	  	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
			  return true;
	  	}

		return hasErrors;
	}
	@SuppressWarnings( "unchecked" )
	private void saveOnActive() throws Exception
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		try
		{
       		if( !hasSaveOnActiveErrors() )
       		{

       			DateFormat dateFormat=new SimpleDateFormat( "dd/MM/yyyy" );
       			if(	dateFormat.parse( dateFormat.format( getContractStartDate())).compareTo( dateFormat.parse( dateFormat.format(getActualContractStartDate()  )) ) !=0 ||
       			    dateFormat.parse( dateFormat.format( getContractEndDate())).compareTo( dateFormat.parse( dateFormat.format(getActualContractEndDate()  )) ) !=0
       			  )
       			{
       			 ContractView contractView = (ContractView)viewRootMap.get( "CONTRACTVIEWUPDATEMODE" );
       			 new PropertyService().saveContractOnActive( contractView.getContractId(),
       					                                    getContractStartDate(),
                                                            getContractEndDate(),
                                                            CommonUtil.getLoggedInUser());

       			 saveSystemMessageForStartEndDateChanged();

       			 successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY));
       			}

       		}

		} catch (Exception e)
		{
			logger.LogException(  "saveOnActive|Error Occured...",e);
			throw e;
		}


	}
	private void saveSystemMessageForStartEndDateChanged()throws Exception
	{
		Locale localeAr = new Locale( "ar" );
		  Locale localeEn = new Locale( "en" );


		ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
		ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
		DateFormat dateFormat=new SimpleDateFormat( "dd/MM/yyyy" );
		if(	dateFormat.parse( dateFormat.format( getContractStartDate())).compareTo( dateFormat.parse( dateFormat.format(getActualContractStartDate()  )) ) !=0 )
    	{
		 String sysMsgEn = java.text.MessageFormat.format( bundleEn.getString( MessageConstants.ContractEvents.CONTRACT_START_DATE_CHANGED ),
				 dateFormat.format( getActualContractStartDate() ),
				 dateFormat.format( getContractStartDate())
				                                            );
		 String sysMsgAr = java.text.MessageFormat.format( bundleAr.getString( MessageConstants.ContractEvents.CONTRACT_START_DATE_CHANGED ),
				dateFormat.format( getActualContractStartDate() ),
				dateFormat.format( getContractStartDate())
                );
		 NotesController.saveSystemNotesForRequest( WebConstants.CONTRACT , sysMsgEn,sysMsgAr,new Long(contractId));
    	}
    	if(	dateFormat.parse( dateFormat.format( getContractEndDate())).compareTo( dateFormat.parse( dateFormat.format(getActualContractEndDate()  )) ) !=0 )
    	{
		String sysEndMsgEn = java.text.MessageFormat.format( bundleEn.getString( MessageConstants.ContractEvents.CONTRACT_END_DATE_CHANGED ),
				dateFormat.format( getActualContractEndDate() ),
				dateFormat.format( getContractEndDate())
                );
        String sysEndMsgAr = java.text.MessageFormat.format( bundleAr.getString( MessageConstants.ContractEvents.CONTRACT_END_DATE_CHANGED ),
        		dateFormat.format( getActualContractEndDate() ),
        		dateFormat.format( getContractEndDate() )
                );
        NotesController.saveSystemNotesForRequest( WebConstants.CONTRACT , sysEndMsgEn,sysEndMsgAr,new Long(contractId));
    	}
	}
	
	@SuppressWarnings( "unchecked" )
    private void ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
    		if(!viewRootMap.containsKey(WebConstants.CONTRACT_USER_TASK_LIST))
    		{
    		   getIncompleteRequestTasks();
    		}
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.CONTRACT_USER_TASK_LIST);
			String loggedInUser=getLoggedInUser();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(PIMSWorkListException ex)
    	{
    		logger.LogException( "ApproveRejectTask| Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("ApproveRejectTask| Error Occured...",ex);
    		throw ex;
    	}
    }
	@SuppressWarnings( "unchecked" )
    public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";

		try{
			RequestView rv =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);

				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.CONTRACT_USER_TASK_LIST,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}
	}
	@SuppressWarnings( "unchecked" )
    public String getErrorMessages()
	{


		return CommonUtil.getErrorMessages(errorMessages);
	}
	@SuppressWarnings( "unchecked" )
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0))
		{
			messageList = "";
		}
		else
		{

			for (String message : successMessages)
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }

		}
		return (messageList);
	}
	@SuppressWarnings( "unchecked" )
    public void btnOccupierDelete_Click()
    {
    	String methodName="btnOccupierDelete_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PersonView selectedOccupier = (PersonView)propspectiveOccupierDataTable.getRowData();
    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.OCCUPIER);
    	for (PersonView personView : previousOccupiersList)
    	{
		  if(selectedOccupier.getPersonId().compareTo(personView.getPersonId())!=0)
			  newOccupiersList.add(personView);

    	}

    	sessionMap.put(WebConstants.OCCUPIER, newOccupiersList);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	@SuppressWarnings( "unchecked" )
    public void btnPartnerDelete_Click()
    {
    	String methodName="btnPartnerDelete_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PersonView selectedPartner = (PersonView)propspectivepartnerDataTable.getRowData();
    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.PARTNER);
    	for (PersonView personView : previousOccupiersList)
    	{
          if(personView.getNewPerson())
          {
		    if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
			   newOccupiersList.add(personView);
		  }
		  //If already existed and deleted in amend mode
		  else
		  {
		     if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
                newOccupiersList.add(personView);
		  }
    	}
    	sessionMap.put(WebConstants.PARTNER, newOccupiersList);
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	@SuppressWarnings( "unchecked" )
    private boolean putUpdatedControlValuesInView() throws Exception
    {

    	String methodName="putUpdatedControlValuesInView";
    	logger.logInfo(methodName+"|"+" Start...");
    	String loggedInUser=getLoggedInUser();
    	errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	boolean isValidated=true;
    	setContractStartDate(  (Date)viewRootMap.get("CONTRACT_START_DATE") );
    	setContractEndDate(  (Date)viewRootMap.get("CONTRACT_END_DATE") );
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
    	try
    	{

    		if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
		         contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
        		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
        	    logger.logInfo(methodName+"|"+" ContractType..."+this.getSelectOneContractType());
        	    if(!AttachmentBean.mandatoryDocsValidated())
		    	{
		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
		    	      isValidated=false;
		    	}
		        if(this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1"))
			    contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
			    else
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
		    	if(getContractStartDate()!=null && !getContractStartDate().equals("")
						 && getContractEndDate()!=null && !getContractEndDate().equals("") )
    			{


    				//Contract start date shud only be less than Contract EXPIRY date
		    		  if (getContractStartDate().compareTo(getContractEndDate())>=0)
		    			{
		    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
		    				isValidated=false;
		    			}

    			}
		    	else
		    	{

		    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
		    	      isValidated=false;
		    	}


		    	logger.logInfo(methodName+"|"+" tenantRefNum..."+this.getTenantRefNum());
		    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
		    	if(viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null )
		    	{
		    		tenantsView.setPersonId((new Long(this.getHdnTenantId())));
		    		//tenantsView.setTenantNumber(tenantRefNum);
		    		contractView.setTenantView(tenantsView);
		    		//requestView.setTenantsView(tenantsView);

		    	}
		    	else
		    	{

		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
		    		isValidated=false;
		    	}

		    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
		    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
		    	{
		    	  if(isUnitValid())
		    	  {
		    		if(getUnitRentAmount()!=null && getUnitRentAmount().trim().length()>0)
		    		{
		    		  // unitsView.setUnitNumber(this.getUnitRefNum());
		    		   unitsView.setUnitId( new Long( hdnUnitId ) );
		    		   if( viewRootMap.containsKey( AUCTION_UNIT_ID ) && viewRootMap.get( AUCTION_UNIT_ID ) != null )
			    		    unitsView.setAuctionUnitId( new Long( viewRootMap.get( AUCTION_UNIT_ID ).toString() ) );
                       contractView.setContractUnitView( getContractUnitSet( loggedInUser ) );
		    		}
		    		else
			        {
			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
			    		isValidated = false;
			    	}
		    	  }
		    	  else
			        {
			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UNITS_INVALID));
			    		isValidated = false;
			    	}
		    	}
		    	else
		        {
		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
		    		isValidated = false;
		    	}
        		if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null)
		    	{
		    		//if tenant is company
		    		if(getIsCompanyTenant())
		    		{
		    			//if Contract is commercial and tenant is company then sponsor info is required.
			    		if(getIsContractCommercial())
			    		{
			    			if(this.getTxtSponsor() ==null || this.getTxtSponsor().length()<=0 )
			    			{
			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
					    		isValidated = false;

			    			}


			    		}
			    		//if unit is residential than and tenant is residential than contract can not be added
			    		//Commented as requested by business users
//			    		else
//			    		{
//			    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
//			    			isValidated = false;
//
//			    		}
		    		}


		    	}
        		String contractNumber = new PropertyServiceAgent().unitAlreadyAssociatedWithOtherContract( unitsView.getUnitId(),contractView.getContractNumber() );
		        if( contractNumber != null && contractNumber.trim().length() > 0 )
		    	{
		        	String message = java.text.MessageFormat.format(
		        			                                         getBundle().getString( "contract.message.unitAlreadyPresentInSomeOtherContract"),
	                                                                 contractNumber
	                                                                 );
		    		errorMessages.add( message );
		    		isValidated = false;
		    	}

        		 if(getContractStartDate()!=null && !getContractStartDate().equals("") )
			        {
			        	contractView.setStartDate(getContractStartDate());
			        	contractView.setOriginalStartDate(getContractStartDate());

			        }
			        else
			        {

			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_START_DATE));
			        	isValidated = false;
			    	}

			        logger.logInfo(methodName+"|"+" contractEndDate..."+getContractEndDate() );
			        if(getContractEndDate()!=null && !getContractEndDate().equals(""))
			        {
			        	contractView.setEndDate(getContractEndDate());
			        	contractView.setOriginalEndDate(getContractEndDate());

			        }
			        else
			        {

			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_EXPIRY_DATE));
			        	isValidated = false;
			    	}
             		if(getIsContractValueandRentAmountEqual())
             		{
             			if(getIsContractCommercial() && !isCommercialActivitySelected())
    	    			{
    	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
    			    		isValidated = false;
    	    			}
             			else if( getIsContractCommercial())
	 			        {
	 			        	contractView.setContractActivitiesView(getCommercialActivities());

	 			        }
				        logger.logInfo(methodName+"|"+" totalContractAmount..."+txttotalContractValue);
				        contractView.setRentAmount(new Double(txttotalContractValue));
				        contractView.setPaymentScheduleView(getPaymentSchedule());
				        contractView.setContractPersonView(getContractPersons());
				        contractView.setContractPartnerView(getContractParntner());
				        if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
				        {
				           unitsView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);

				           contractView.setContractUnitView(getContractUnitSet(loggedInUser));
				        }
				        //contractView.set
				        contractView.setUpdatedBy(loggedInUser);

				        contractView.setUpdatedOn(new Date());

             		}
             		else
             		{
			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
			        	isValidated = false;
			        }

    	}
    	catch(Exception ex)
    	{
    		logger.logError(methodName+"|"+" Exception Occured..."+ex);
    		throw ex;

    	}
    	logger.logInfo(methodName+"|"+" Finish...");
    	return isValidated ;
    }
	@SuppressWarnings( "unchecked" )
    private boolean hasErrors()throws Exception
    {
       boolean hasErrors =false;
       //Validating Applicant Tab Start
         if(!viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) || viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)==null)
         {
		   errorMessages.add(getBundle().getString(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
		   tabPanel.setSelectedTab(TAB_ID_APPLICATION);
		   return true;
		 }

       //Validating Applicant Tab Finish

       //Validating Basic Info Start

         if(hasBasicInfoErrors())
         {
        	 tabPanel.setSelectedTab(TAB_ID_BASIC);
        	 return true;
         }
       //Validating Basic Info Finish

      // Validating Unit Tab Start

         if(hasUnitTabErrors())
         {
        	 tabPanel.setSelectedTab(TAB_ID_UNIT);
        	 return true;
         }
       //Validating Unit Tab Finish

       //Validating Commercial Activity Start
        if(getIsContractCommercial() && !isCommercialActivitySelected())
		{
				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
				tabPanel.setSelectedTab(TAB_ID_COMMERCIAL_ACTIVITY);
				return true;
		}
      //Validating Commercial Activity Finish

        //Validating Payment Schedules Start
        if(!getIsContractValueandRentAmountEqual())
        {
        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
        	tabPanel.setSelectedTab(TAB_ID_PAYMENT_SCHEDULE);
        	return true;
        }
        //Validating Payment Schedules Finish


         //Validating Attachments Start
        if(!AttachmentBean.mandatoryDocsValidated())
	    {
	    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
	    		 tabPanel.setSelectedTab(TAB_ID_ATTACHEMENT);
	    		return  true;
	    }
         //Validating Attachments Finish

         return false;
    }
	@SuppressWarnings( "unchecked" )
    private boolean hasUnitTabErrors()throws Exception
    {
    	boolean hasUnitTabErrors=false;
    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
        {

    		if(getUnitRentAmount()==null || getUnitRentAmount().trim().length()<=0)
        	{
		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
		    		hasUnitTabErrors=true;
		    }
    		else
    		{
    			try
    			{
    			  new Double( getUnitRentAmount() );
    			}
    			catch(Exception ex)
    			{
	    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_INVALID_RENT_VALUE));
	    		hasUnitTabErrors=true;
    			}
	        }
        }
    	else
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
    		hasUnitTabErrors=true;
    	}

       return hasUnitTabErrors;
    }
	@SuppressWarnings( "unchecked" )
    private boolean hasBasicInfoErrors()throws Exception
    {
    	boolean hasBasicInfoErrors=false;
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
    	if(!viewRootMap.containsKey(TENANT_INFO) || viewRootMap.get(TENANT_INFO)==null )
    	{

    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
    		hasBasicInfoErrors=true;
    	}
    	if(this.getSelectOneContractType()==null || this.getSelectOneContractType().equals("-1"))
	    {
	        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
	        	hasBasicInfoErrors = true;
	    }

    	if( getContractStartDate()!=null && !getContractStartDate().equals("")
				 && getContractEndDate()!=null && !getContractEndDate().equals("") )
		{
			String todayDate=dateFormat.format(new Date());
			logger.logDebug("TodayDate.."+todayDate);
			//Contract start date shud only be less than Contract EXPIRY date
	   		  if (getContractStartDate().compareTo( getContractEndDate() )>=0)
	   		  {
	   				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
	   				hasBasicInfoErrors=true;
	   		  }
		}
	   	else
	   	{
	   	   errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
	   	   hasBasicInfoErrors=true;
	   	}
    	//if tenant is company
    	if(( this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1") )&& getIsCompanyTenant())
    	{
    			//if Contract is commercial and tenant is company then sponsor info is required.
	    		if(getIsContractCommercial())
	    		{
	    			if(this.getTxtSponsor()==null || this.getTxtSponsor().length()<=0 )
	    			{
	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
	    				hasBasicInfoErrors=true;
	    			}
	    		}
	    		//if unit is residential than and tenant is company than contract can not be added
	    		//Commented as requested by Business Users
//	    		else
//	    		{
//	    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
//	    			hasBasicInfoErrors=true;
//	    		}
    	}



    	return hasBasicInfoErrors;
    }
	@SuppressWarnings( "unchecked" )
    private boolean putControlValuesInView() throws Exception
    {
    	String methodName="putControlValuesInView";
    	logger.logInfo(methodName+"|"+" Start...");
    	errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	boolean isError=false;
    	setContractStartDate( (Date)viewRootMap.get("CONTRACT_START_DATE") );
    	setContractEndDate( (Date)viewRootMap.get("CONTRACT_END_DATE") );
    	String loggedInUser=getLoggedInUser();

    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
    	try
    	{
    		      if(hasErrors())
    		    	  return true;
    		       DomainDataView ddContractCategory = CommonUtil.getIdFromType
    		                                                         (
    		    		                                              CommonUtil.getDomainDataListForDomainType(WebConstants.ContractCategory.CONTRACT_CATEGORY),
    		    		                                              WebConstants.ContractCategory.LEASE_CONTRACT
    		    		                                              );

    		       contractView.setContractCategory(ddContractCategory.getDomainDataId() );
             		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
    		        if(contractId!=null && !contractId.equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			    	{
			    	  contractView.setContractId(new Long(contractId));

			    	}
                    logger.logInfo(methodName+"| ContractType..."+this.getSelectOneContractType());
    		        contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
    			    logger.logInfo(methodName+"|"+" datetimerequest..."+this.getDatetimerequest());
			    	contractView.setContractDate(dateFormat.parse(this.getDatetimerequest()));
			    	requestView.setRequestDate(dateFormat.parse(this.getDatetimerequest()));
			    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) && viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)!=null)
			    	{
			    		logger.logInfo(methodName+"| applicantId..."+viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString());
			    		PersonView applicantView = new PersonView();
			    		applicantView.setPersonId(new Long(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString()));
			    		requestView.setApplicantView(applicantView);
			    		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) &&
			    				viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION)!=null)
			    		{
			    		  requestView.setDescription(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString() );
			    		  contractView.setRemarks(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString());
			    		}
			    	}

			    	else
			    	{
			    		errorMessages.add(getBundle().getString(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			    		isError = true;
			    	}
			    	if(getContractStartDate()!=null && !getContractStartDate().equals("")
							 && getContractEndDate()!=null && !getContractEndDate().equals("") )
	    			{
	    				String todayDate=dateFormat.format(new Date());
		    			logger.logDebug(methodName+"|"+"TodayDate.."+todayDate);
		    			//Contract start date shud only be less than Contract EXPIRY date
			    		  if (getContractStartDate().compareTo(getContractEndDate())>=0)
			    		  {
			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
			    				isError=true;
			    		  }
	    			}
			    	else
			    	{

			    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
					      isError=true;
			    	}

			    	logger.logInfo(methodName+"|"+" refNum..."+this.getRefNum());
			    	if(this.getRefNum()!=null && !this.getRefNum().equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			    		contractView.setContractNumber(this.getRefNum());
			    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
			    	tenantsView = (PersonView)viewRootMap.get(TENANT_INFO);
			    	if(tenantsView.getPersonId()!=null)
			    		this.setHdnTenantId(tenantsView.getPersonId().toString());
			    	contractView.setTenantView(tenantsView);
			    	requestView.setTenantsView(tenantsView);

			    	//logger.logInfo(methodName+"|"+" unitRefNum..."+this.getUnitRefNum());
			    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
			    	unitsView.setUnitId(new Long(hdnUnitId));
			    	if(viewRootMap.containsKey(AUCTION_UNIT_ID) && viewRootMap.get(AUCTION_UNIT_ID)!=null )
			    	   unitsView.setAuctionUnitId(new Long(viewRootMap.get(AUCTION_UNIT_ID).toString()));
                    requestView.setRequestDetailView(getRequestDetailSet(loggedInUser));
                    contractView.setContractUnitView(getContractUnitSet(loggedInUser));
			    	logger.logInfo(methodName+"|"+" contractStartDate..."+getContractStartDate());
			        contractView.setStartDate(getContractStartDate());
			        contractView.setOriginalStartDate(getContractStartDate());
			        logger.logInfo(methodName+"|"+" contractEndDate..."+getContractEndDate());
			        contractView.setEndDate(getContractEndDate());
			        contractView.setOriginalEndDate(getContractEndDate());
			        if(this.getSelectRequestStatus()!=null && !this.getSelectRequestStatus().equals(""))
			        	contractView.setStatus(new Long(this.getSelectRequestStatus()));
			        if(txttotalContractValue!=null && ! txttotalContractValue.equals(""))
			        	contractView.setRentAmount(new Double(txttotalContractValue));

			        //Check for the units type if its commercial than add partners.
			        contractView.setContractPartnerView(getContractParntner());
			        contractView.setContractPersonView(getContractPersons());
			        contractView.setPaymentScheduleView(getPaymentSchedule());
			        contractView.setIsDeleted(new Long(0));
			        contractView.setRecordStatus(new Long(1));
			        contractView.setUpdatedBy(loggedInUser);

			        contractView.setUpdatedOn(new Date());
			        if(pageMode!=null &&( pageMode.equals(PAGE_MODE_ADD)|| pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)))
			        {
			        	contractView.setCreatedOn(new Date());
			            contractView.setCreatedBy(loggedInUser);
			            requestView.setCreatedOn(new Date());
			        }
			        else if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) )
			        {
			        	if(contractCreatedOn!=null && !contractCreatedOn.equals(""))
			        	contractView.setCreatedOn(dateFormat.parse(contractCreatedOn));
			        	if(contractCreatedBy!=null && !contractCreatedBy.equals(""))
			                contractView.setCreatedBy(contractCreatedBy);

			        }
			        if(getIsContractCommercial())
			        	contractView.setContractActivitiesView(getCommercialActivities());
			        requestView.setUpdatedOn(new Date());
			        requestView.setIsDeleted(new Long(0));
			        requestView.setRecordStatus(new Long(1));
			        requestView.setUpdatedBy(loggedInUser);
			        requestView.setCreatedBy(loggedInUser);
			        logger.logInfo(methodName+"|"+" Set request status...");
			        if(viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
			        {
			        	List<DomainDataView> ddv=(ArrayList) viewRootMap.get(WebConstants.SESSION_REQUEST_STATUS);
			          if(pageMode.equals(PAGE_MODE_ADD))
			          {
			        	HashMap hMap=getIdFromType(ddv, WebConstants.REQUEST_STATUS_NEW);
			          	if(hMap.containsKey("returnId"))
			          	   requestView.setStatusId(new Long(hMap.get("returnId").toString()));
			          }

			        }
			        contractView.getRequestsView().add(requestView);
			        String contractNumber = new PropertyServiceAgent().unitAlreadyAssociatedWithOtherContract( unitsView.getUnitId(),"" );
			        if( contractNumber != null && contractNumber.trim().length() > 0 )
			    	{
			        	String message = java.text.MessageFormat.format(
			        			                                         getBundle().getString( "contract.message.unitAlreadyPresentInSomeOtherContract"),
		                                                                 contractNumber
		                                                                 );
			    		errorMessages.add( message );
			    		 return true;
			    	}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Exception Occured...",ex);
    		throw ex;

    	}
    	logger.logInfo(methodName+"|"+" Finish...");
    	return isError;
    }
	@SuppressWarnings( "unchecked" )
    private boolean getIsContractValueandRentAmountEqual()
    {
    	String methodName="getIsContractValueandRentAmountEqual";
    	logger.logInfo(methodName+"|"+"Start");
    	boolean isEqual=false;
    	if(txttotalContractValue!=null && txttotalContractValue.length()>0)
    	{
    	  Double contractValue=new Double(txttotalContractValue);
    		if(contractValue.compareTo(getSumofAllRentAmounts())==0)
    			isEqual=true;
    	}
    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
    	logger.logInfo(methodName+"|"+"Finish");
    	return isEqual;
    }
	@SuppressWarnings( "unchecked" )
    private Double getSumofAllRentAmounts()
    {
    	String methodName="getSumofAllRentAmounts";
    	logger.logInfo(methodName+"|"+"Start");
    	Double rentAmount=new Double(0);
    	List<DomainDataView> paymentStatusList= (ArrayList<DomainDataView> )viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
    	DomainDataView ddvDraftStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
		DomainDataView ddvPendingStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		DomainDataView ddvCollectedStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		DomainDataView ddvRealizedStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.REALIZED);

    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				//If payment Schedule is for the rent
				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&&
						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 &&
						(paymentScheduleView.getStatusId().compareTo(ddvDraftStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvPendingStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvCollectedStatus.getDomainDataId())==0 ||
						 paymentScheduleView.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0 )
						)


				   rentAmount+=paymentScheduleView.getAmount();
			}

        }
    	logger.logInfo(methodName+"|"+"Finish");
    	return Math.ceil(rentAmount);

    }
	@SuppressWarnings( "unchecked" )
    private boolean isCommercialActivitySelected()
    {
    	boolean isSelected=false;
    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
    	{
    		List<CommercialActivityView> commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
    		for (CommercialActivityView commercialActivityView : commercialActivityViewList)
    		{
			  if(commercialActivityView.getIsSelected())
			  {
				  isSelected=true;
				  break;
			  }
			}
    	}

    	return isSelected;
    }
	@SuppressWarnings( "unchecked" )
    private Set<ContractActivityView>  getCommercialActivities()
    {
    	String methodName ="getCommercialActivities";
    	logger.logInfo(methodName+"|"+"Start...");
    	List<CommercialActivityView> commercialActivityViewList=new ArrayList<CommercialActivityView>(0);
    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
    		commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
    	Set<ContractActivityView> contractActivityViewSet=new HashSet<ContractActivityView>(0);
    	for (CommercialActivityView commercialActivityView : commercialActivityViewList)
    	{
    		if(commercialActivityView.getIsSelected())
    		{
    		 ContractActivityView contractActivityView=new ContractActivityView();
    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Name::"+commercialActivityView.getCommercialActivityName());
    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Id::"+commercialActivityView.getCommercialActivityId());
    		 contractActivityView.setCommercialActivityId(commercialActivityView.getCommercialActivityId());
    		 contractActivityView.setUpdatedOn(new Date());
    		 contractActivityView.setUpdatedBy(getLoggedInUser());
    		 contractActivityView.setCreatedOn(new Date());
    		 contractActivityView.setCreatedBy(getLoggedInUser());
    		 contractActivityView.setRecordStatus(new Long(1));
    		 contractActivityView.setIsDeleted(new Long(0));
    		 contractActivityViewSet.add(contractActivityView);

    		}


		}


    	logger.logInfo(methodName+"|"+"Finish...");
    	return contractActivityViewSet;

    }
	@SuppressWarnings( "unchecked" )
    public Set<ContractPartnerView> getContractParntner()
    {
    	String methodName="getContractParntner";
    	logger.logInfo(methodName+"|"+"Start");
    		Set<ContractPartnerView> contractPartnerViewSet =new HashSet<ContractPartnerView>(0);
    		if(sessionMap.containsKey(WebConstants.PARTNER))
   	        {
    			List<PersonView> personViewList= (ArrayList)sessionMap.get(WebConstants.PARTNER);
    			for(int i=0;i< personViewList.size();i++)
    			{
    			   PersonView personView =(PersonView)personViewList.get(i);
    			   ContractPartnerView contractPartnerView =new ContractPartnerView();

    			   //For amend to delete already existing partner
    			   if(personView.getIsDeleted()==null || personView.getIsDeleted().compareTo(new Long(0))==0)
    				   contractPartnerView.setIsDeleted(new Long(0));
    			   else if(personView.getIsDeleted().compareTo(new Long(1))==0)
    			   {
    			     contractPartnerView.setIsDeleted(new Long(1));
    			   }

    			   contractPartnerView.setPersonView(personView);
    			  contractPartnerViewSet.add(contractPartnerView);
    			}

   	        }
    		logger.logInfo(methodName+"|"+"Finish");
    	return contractPartnerViewSet;
    }
	@SuppressWarnings( "unchecked" )
    public Set<ContractUnitView> getContractUnitSet(String loggedInUser) throws Exception
    {
    	String methodName="getContractUnitSet";
    	logger.logInfo(methodName+"|"+"Start");
         Set<ContractUnitView> contractUnitViewSet =new HashSet<ContractUnitView>(0);
        ContractUnitView contractUnitView= new ContractUnitView();
        //Contract Unit Occupier check for the company here...
        contractUnitView.setContractUnitPersons(getContractUnitOccupiers());
        contractUnitView.setUnitView(unitsView);

        // COMMENT::Rent Value of the Unit on the basis of calculation
		// logger.logInfo(methodName+"|"+"Units Actual Annual Rent::"+unitRentAmount);
		// Double unitAmount = getUnitAmuntForSelectedDuration();
		// logger.logInfo(methodName+"|"+"Units Rent after calculation::"+unitAmount);

        contractUnitView.setRentValue( new Double( getUnitRentAmount() ) );
	    contractUnitView.setPaidFacilitieses(getPaidFacilitiesSet());
	    Set setContractUnitView =contractView.getContractUnitView();
	    while(setContractUnitView.iterator().hasNext())
	    {
	    	ContractUnitView existingCUV = (ContractUnitView)setContractUnitView.iterator().next();
	        contractUnitView.setContractUnitId(  existingCUV.getContractUnitId());
	        contractUnitView.setOldUnitId( existingCUV.getOldUnitId() );
	        contractUnitView.setOldAuctionUnitId( existingCUV.getOldAuctionUnitId() );
	        break;
	    }
	    contractUnitViewSet.add(contractUnitView);
	    logger.logInfo(methodName+"|"+"Finish");
    	return contractUnitViewSet;
    }
	@SuppressWarnings( "unchecked" )
    private void putSelectedFacilitiesInView()
    {
    	String methodName="putSelectedFacilitiesInView";
    	logger.logInfo(methodName+"|"+"Start");
    	if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) || viewRootMap.get("pageMode").equals(PAGE_MODE_UPDATE)   )
    	{
	    	if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES));
	   	       paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);
	   	    if(paidFacilitiesDataList !=null)
	   	    {
	   	   	    logger.logDebug(methodName+"|"+"Facilities List is not null");
	   	    for (PaidFacilitiesView pfv: paidFacilitiesDataList) {

	   	    	    if(pfv.getSelected())
	   					addToSelectedFacilities(pfv);
	   				else
	  					removeFromSelectedFacilities(pfv);

	   	    }
	   	    }
    	}
   	    logger.logInfo(methodName+"|"+"Finish");

    }
	@SuppressWarnings( "unchecked" )
    private Set<PaidFacilitiesView> getPaidFacilitiesSet()throws Exception
    {
    	String methodName="getPaidFacilitiesSet";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PaidFacilitiesView> paidFacilitiesViewSet=new HashSet<PaidFacilitiesView>(0);
        putSelectedFacilitiesInView();
    	logger.logInfo(methodName+"|"+"Checking for existence of session for paid Facility");
	     if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
	     {
	    	 logger.logInfo(methodName+"|"+"Session for paid Facility exists..");
	    	 List<PaidFacilitiesView> paidFacilitiesViewList=(ArrayList)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
	    	 logger.logInfo(methodName+"|"+"Size of selected paid facilities is.."+paidFacilitiesViewList.size());
	    	 int dateDiffDays=1;
		      dateDiffDays= CommonUtil.getDaysDifference(getContractStartDate(),getContractEndDate());
	    	 for (PaidFacilitiesView paidFacilityView : paidFacilitiesViewList)
		     {
	    	   Double facilityRentValue = getFacilityAmountForDuration(
		            		   dateDiffDays, paidFacilityView);
	    	   paidFacilityView.setRent(facilityRentValue);
	    	   paidFacilityView.setIsMandatory("1");
	    	   paidFacilityView.setDateFrom(getContractStartDate());
	    	   paidFacilityView.setDateTo(getContractEndDate());
	    	   if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
	    	   {
	    		   logger.logDebug(methodName+"|"+"Page Mode Amend");
	    		   if(paidFacilityView.getPaidFacilitiesId()!=null)
	    			  paidFacilityView.setPaidFacilitiesId(null);
	    		   if(paidFacilityView.getNewFacility())
	    		   {
	    			   logger.logDebug(methodName+"|"+"New Facility so setting date From to today's date");

	    			   facilityRentValue = getFacilityAmountForDuration(
	    					   CommonUtil.getDaysDifference(new Date(),getContractEndDate()), paidFacilityView);
	    	            paidFacilityView.setRent(facilityRentValue);
	    			   paidFacilityView.setDateFrom(new Date());
	    		   }
	    		   else if(!paidFacilityView.getNewFacility() && paidFacilityView.getIsDeleted().compareTo(1L)==0)
	    		   {
	    			   logger.logDebug(methodName+"|"+"Old Facility and deleted so setting date To to today's date");
	    			   paidFacilityView.setDateTo(new Date());
	    		   }

	    	   }
	    	   paidFacilitiesViewSet.add(paidFacilityView);
			 }

	     }
	     logger.logInfo(methodName+"|"+"Finish");
    	return paidFacilitiesViewSet;

    }
	@SuppressWarnings( "unchecked" )
    private Set<PersonView> getContractUnitOccupiers()
    {
    	String methodName="getContractUnitOccupiers";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PersonView> personViewSet=new HashSet<PersonView>(0);

    	if(sessionMap.containsKey(WebConstants.OCCUPIER))
        {
		    List<PersonView> contractUnitOccupierList= (ArrayList)sessionMap.get(WebConstants.OCCUPIER);
			for(int i=0;i< contractUnitOccupierList.size();i++)
			{
				PersonView personView =(PersonView)contractUnitOccupierList.get(i);
			    personViewSet.add(personView);
			}

        }

    	logger.logInfo(methodName+"|"+"Finish");
    	return personViewSet;
    }
	@SuppressWarnings( "unchecked" )
    private Set<ContractPersonView> getContractPersons()
    {
    	String methodName="getContractUnitPersons";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<ContractPersonView> contractPersonViewSet=new HashSet<ContractPersonView>(0);
    	PersonView personView =new PersonView();
    	ContractPersonView contractPersonView =new ContractPersonView();
    	List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
    	//Sponsor
    	if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
		{
			ddvList=(ArrayList) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);
		    DomainDataView ddvSponsor = CommonUtil.getIdFromType(ddvList,WebConstants.SPONSOR);
		}

    	   logger.logInfo(methodName+"|"+"txtSponsor::"+this.getTxtSponsor());
    	   logger.logInfo(methodName+"|"+"SponsorId::"+hdnSponsorId);

    	   if(hdnSponsorId!= null && hdnSponsorId.trim().length()>0)
    	   {
    	    String[] spnsrInfo = getPersonNameIdFromHidden(hdnSponsorId);
    	     personView.setPersonId(new Long(spnsrInfo[0]));
    	     contractPersonView.setPersonView(personView);
    	     DomainDataView ddvSponsor = CommonUtil.getIdFromType(ddvList,WebConstants.SPONSOR);
    	     contractPersonView.setPersonTypeId(ddvSponsor.getDomainDataId());
    	     contractPersonViewSet.add(contractPersonView);

    	   }
    	   //Manager
    	   logger.logInfo(methodName+"|"+"txtManager::"+this.getTxtManager());
    	   logger.logInfo(methodName+"|"+"ManagerId::"+hdnManagerId);
           if(hdnManagerId!= null && hdnManagerId.trim().length()>0)
           {
              String[] mgrInfo = getPersonNameIdFromHidden(hdnManagerId);

        	   contractPersonView=new ContractPersonView();
        	   personView =new PersonView();
 	           personView.setPersonId(new Long(mgrInfo[0]));
 	           contractPersonView.setPersonView(personView);
 	          DomainDataView ddvManager = CommonUtil.getIdFromType(ddvList,WebConstants.MANAGER);
 	          contractPersonView.setPersonTypeId(ddvManager.getDomainDataId());
 	          contractPersonViewSet.add(contractPersonView);
           }
           logger.logInfo(methodName+"|"+"Finish");
    	return contractPersonViewSet;
    }
	@SuppressWarnings( "unchecked" )
    private Set<PaymentScheduleView> getPaymentSchedule()
    {
    	String methodName="getPaymentSchedule";
    	logger.logInfo(methodName+"|"+"Start");
    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);

    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				paymentScheduleView.setOwnerShipTypeId(
						new Long (viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()));

				paymentScheduleViewSet.add(paymentScheduleView);


			}

        }

    	logger.logInfo(methodName+"|"+"Finish");
    	return paymentScheduleViewSet;
    }
	@SuppressWarnings( "unchecked" )
    private void getPaymentReceiptViewFromReceivePaymentScreen()
    {
    	String methodName="getPaymentReceiptViewFromReceivePaymentScreen";
    	logger.logInfo(methodName+"|"+"Start");
    	List<PaymentReceiptView> paymentReceiptViewList;
    	//Checking if hashMAP HAS ALREADY BEEN IN THE SESSION

    	    if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
    	    	paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
    		else
    			paymentReceiptViewList=new ArrayList<PaymentReceiptView>();

 	       if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
 	       {
 		     PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
 		    paymentReceiptViewList.add(prv);
 		     sessionMap.put(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS, paymentReceiptViewList);
 	         sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
 	       }
    	logger.logInfo(methodName+"|"+"Finish");

    }
	@SuppressWarnings( "unchecked" )
    public Set<RequestDetailView> getRequestDetailSet(String loggedInUser)
    {
    	  Set<RequestDetailView> requestDetailSet=new HashSet<RequestDetailView>();
    	   requestDetailView.setUnitView(unitsView);
		requestDetailSet.add(requestDetailView);
		return requestDetailSet;


    }
	@SuppressWarnings( "unchecked" )
    private void putViewValuesInControl(String contractId) throws PimsBusinessException,Exception
    {
    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());//format!=null && format.length()>0?format:"dd/MM/yyyy");
    	this.setRefNum(contractView.getContractNumber());
    	this.txtEjariNum.setValue(contractView.getEjariNumber());
    	this.linkPrintEjariCertificate.setValue(
				EjariIntegration.getEjariCertificateLink(contractView.getEjariNumber())		
		);
	this.linkPrintEjariContract.setValue(
			EjariIntegration.getEjariContractLink( contractView.getEjariNumber())		
	);
    	if(contractView.getContractTypeId()!=null)
    	{
    		this.setSelectOneContractType(contractView.getContractTypeId().toString());
    		viewRootMap.put(OLD_CONTRACT_TYPE,this.getSelectOneContractType());
    	}
    	if(contractView.getStatus()!=null)
      	  this.setSelectRequestStatus(contractView.getStatus().toString());
    	if(contractView.getIssueDate()!=null)
        	  this.setContractIssueDate(contractView.getIssueDate());
    	if(contractView.getTenantView()!=null)
    	{
    	  tenantsView=contractView.getTenantView();
      	  this.setHdnTenantId(tenantsView.getPersonId().toString());
      	  hdntenanttypeId=tenantsView.getIsCompany().toString();
      	   hdnTenantBlackListed = tenantsView.getIsBlacklisted()!= null ?tenantsView.getIsBlacklisted().toString():"0";
      	  String tenantNames="";
          if(tenantsView.getPersonFullName()!=null && tenantsView.getPersonFullName().length()>0)
             tenantNames=tenantsView.getPersonFullName();
          if(tenantNames.trim().length()<=0)
         	 tenantNames=tenantsView.getCompanyName();
          if(tenantNames.trim().length()<=0)
        	  tenantNames=tenantsView.getGovtDepartment();
    	  txtTenantName=tenantNames;
    	  txtTenantType=getTenantType(tenantsView);
    	  viewRootMap.put(TENANT_INFO, tenantsView);
    	}
    	if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
    	{
    	  Iterator iter=contractView.getContractUnitView().iterator();
    	  while(iter.hasNext())
    	  {
    		  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
    		  //Reason:: Because when contract was created the rent amount of the unit might gets changed due
    		  //to the fact that contract might come from Auction(which has auction price) or because
    		  //of duration of contract
    		  //and when its viewed later the rent value  of the unit in unit table is different than that of
    		  //rent value of unit in contract unit.
    		  contractUnitView.getUnitView().setContractUnitRentValue( contractUnitView.getRentValue() ) ;
    		  viewRootMap.put(UNIT_DATA_ITEM, contractUnitView.getUnitView());
    		  viewRootMap.put("UNITINFO",contractUnitView.getUnitView());
    		  viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,contractUnitView.getUnitView().getPropertyCategoryId());
    		  if(contractUnitView.getUnitView().getAuctionUnitId()!=null)
    		  {
    		   getDepositAmountForBidder(null, null, contractView.getContractId());
    		   viewRootMap.put(AUCTION_UNIT_ID,contractUnitView.getUnitView().getAuctionUnitId());
    		  }
    		 this.setUnitRefNum(contractUnitView.getUnitView().getUnitNumber());
      	       hdnUnitId=contractUnitView.getUnitView().getUnitId().toString();
    	       hdnUnitUsuageTypeId=contractUnitView.getUnitView().getUsageTypeId().toString();
    	       setUnitRentAmount ( contractUnitView.getRentValue().toString() );
    	       propertyCommercialName=contractUnitView.getUnitView().getPropertyCommercialName();
    	       txtUnitUsuageType=(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
    	       propertyAddress=contractUnitView.getUnitView().getUnitAddress();
    	  }
    	}
    	if(contractView.getContractPersonView().size()>0)
    		getContractPersons(contractView.getContractId());
    	if(contractView.getContractDateString()!=null && !contractView.getContractDateString().equals(""))
    		this.setDatetimerequest(contractView.getContractDateString());
    	if(contractView.getCreatedOn()!=null && !contractView.getCreatedOn().equals(""))
    		contractCreatedOn=dateFormat.format(contractView.getCreatedOn());
    	if(contractView.getCreatedBy()!=null && !contractView.getCreatedBy().equals(""))
    		contractCreatedBy=contractView.getCreatedBy();
    	if(contractView.getStartDateString()!=null && !contractView.getStartDateString().equals(""))
    	{
    		setActualContractStartDate(dateFormat.parse(contractView.getStartDateString()) );
    		setContractStartDate( dateFormat.parse(contractView.getStartDateString()) );
    		viewRootMap.put("CONTRACT_START_DATE",getContractStartDate());
    	}
    	if(contractView.getEndDateString()!=null && !contractView.getEndDateString().equals(""))
    	{
    		setActualContractEndDate(dateFormat.parse(contractView.getEndDateString()) );
    		setContractEndDate( dateFormat.parse(contractView.getEndDateString()) );
    		viewRootMap.put("CONTRACT_END_DATE",getContractEndDate());
    	}
    	if(contractView.getRentAmount()!=null)
    		txttotalContractValue=contractView.getRentAmount().toString();
    	getOccupiers(contractId);
    	getPartners(contractId);
    	getContractPaymentSchedule(contractId);
    	if(requestView !=null && requestView.getRequestId()!=null)
    	     loadAttachmentsAndComments(requestView.getRequestId());
    }
	@SuppressWarnings( "unchecked" )
    private void getContractPersons(Long contractId) throws PimsBusinessException
    {
    	String methodName ="getContractPersons(Long contractId)";
    	logger.logInfo(methodName+"|"+"Start...");
    	String sponsorTypeId="";
    	String managerTypeId="";
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<ContractPersonView> contractPersonViewList =  psa.getContractPersons(contractId);
        for (ContractPersonView contractPersonView : contractPersonViewList) {

        	PersonView personView = contractPersonView.getPersonView();
    		if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
    		{
    			List<DomainDataView> ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);
    			HashMap hMap=new HashMap(0);
    		    hMap= getIdFromType(ddvList,WebConstants.SPONSOR);
    		    if(hMap.containsKey("returnId"))
    		    	sponsorTypeId= hMap.get("returnId").toString();
    		    	hMap.clear();
    		    	hMap= getIdFromType(ddvList,WebConstants.MANAGER);
    		    	if(hMap.containsKey("returnId"))
        		    	managerTypeId=hMap.get("returnId").toString();

    		}

    		if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(sponsorTypeId))
    		{
    			this.setTxtSponsor(personView.getPersonFullName());
    		     hdnSponsorId=personView.getPersonId().toString()+":"+personView.getPersonFullName();
    		}
    		else if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(managerTypeId))
    		{
    			this.setTxtManager(personView.getPersonFullName());
    		     hdnManagerId=personView.getPersonId().toString()+":"+personView.getPersonFullName();
    		}
		}
    	logger.logInfo(methodName+"|"+"Finish");

    }

    private void getOccupiers(String contractId) throws PimsBusinessException
    {
    	String methodName ="getOccupiers";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<PersonView> personViewList = psa.getContractUnitOccupiers(new Long(contractId));

        sessionMap.put(WebConstants.OCCUPIER, personViewList);

    	logger.logInfo(methodName+"|"+"Finish");

    }

    private void getPartners(String contractId) throws PimsBusinessException
    {
    	String methodName ="getPartners";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<PersonView> personViewList = psa.getContractPartners(new Long(contractId));

        sessionMap.put(WebConstants.PARTNER, personViewList);

    	logger.logInfo(methodName+"|"+"Finish");

    }
    private void getPaidFacilities(String contractId) throws PimsBusinessException
    {

    	String methodName ="getPaidFacilities";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
        List<PaidFacilitiesView> paidFacilitiesViewList = psa.getContractUnitPaidFacilities(new Long(contractId));

        viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES, paidFacilitiesViewList );

    	logger.logInfo(methodName+"|"+"Finish");


    }
    @SuppressWarnings( "unchecked" )
	private void getContractPaymentSchedule(String contractId) throws PimsBusinessException
	{

    	String methodName ="getContractPaymentSchedule";
    	logger.logInfo(methodName+"|"+"Start...");
    	PropertyServiceAgent psa= new PropertyServiceAgent();
    	List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(new Long(contractId),null);

    	for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList)
    	{

    		if( getIsContractStatusApprovalRequired() )
    		{
    			paymentScheduleView.setShowEdit( true );
    			paymentScheduleView.setShowDelete( true );
    		}
    		else if( getIsContractStatusApproved() )
    		{
    			paymentScheduleView.setShowEdit( true );
    			paymentScheduleView.setShowDelete( false);
    		}
    		else if ( getIsContrctStatusActive()  )
    		{
    			paymentScheduleView.setShowEdit( false );
    			paymentScheduleView.setShowDelete( false );
    		}
    		if ( paymentScheduleView.getIsSystem() !=null &&  paymentScheduleView.getIsSystem().longValue() ==1 )
   			 paymentScheduleView.setShowDelete( false );
   		    if( paymentScheduleView.getStatusId().compareTo( getPaymentSchedulePendingStausId() ) != 0 &&
   		    	paymentScheduleView.getStatusId().compareTo( getPaymentScheduleStausId( WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT ) ) !=0 )
   		    {
   		    	paymentScheduleView.setShowEdit( false );
    			paymentScheduleView.setShowDelete( false );
   		    }

		}
        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);

    	logger.logInfo(methodName+"|"+"Finish...");



	}
    @SuppressWarnings( "unchecked" )
    public void onContractDurationChanged()
    {
    	try
    	{
	    	if( viewRootMap.get( UNIT_DATA_ITEM )!= null && !getIsContrctStatusActive() )
	    	{
		      UnitView unitView=(UnitView)viewRootMap.get( UNIT_DATA_ITEM );
		   	  setUnitRentAmount( unitView.getRentValue().toString() );
		   	  //if( !viewRootMap.containsKey("AUCTIONUNITINFO") )
		   	  getUnitAmuntForSelectedDuration();
		   	  valueChangeListerner_RentValue();
	    	}
	    	if( isPostBack())
	    	viewRootMap.put(IS_DATE_CHANGED,"1");
	    }
		catch (Exception ex)
		{
			logger.LogException( "onContractDurationChanged|Exception Occured...",ex);
		}

    }
    @SuppressWarnings( "unchecked" )
    public void valueChangeListerner_RentValue()
    {

    	try
    	{

    		if( getTxttotalContractValue() !=null && getTxttotalContractValue().toString().length()>0 )
    			viewRootMap.put(OLD_RENT_VALUE,new Double(txttotalContractValue));
    		else
    			viewRootMap.put(OLD_RENT_VALUE,0D);

    		contractStartDate=getContractStartDate();
    		contractEndDate=getContractEndDate();
    		//only add when there is some unit and contract start and end date are not empty
    		if( 
    				viewRootMap.containsKey(UNIT_DATA_ITEM) && 
    				contractStartDate!=null && 
    				!contractStartDate.equals("") && 
    				contractEndDate!=null && 
    				!contractEndDate.equals("")
			    )
			{

    			  //Contract start date shud only be less than Contract EXPIRY date
	    		  if (contractStartDate.compareTo(contractEndDate)>=0)
	    		  {
	    				errorMessages=new ArrayList<String>();
	    				errorMessages.clear();
	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));

	    		  }
	    		  else
	    		  {
		    			CalculateTotalRentValue();
		    			txttotalContractValue = totalContractValue.toString();
		    			if(
		    					(
		    						getIsContractStatusNew() || getIsContractStatusDraft()||pageMode.equals(PAGE_MODE_ADD) ||
		    						( 
		    						  pageMode.equals(PAGE_MODE_UPDATE) && 
		    						  getIsContractStatusApprovalRequired() 
		    						)
		    				     ) &&
		    					(
		    					    new Double(viewRootMap.get(OLD_RENT_VALUE).toString()).compareTo(totalContractValue)!=0|| 
		    						viewRootMap.get(OLD_CONTRACT_TYPE).toString().compareTo(this.getSelectOneContractType())!=0
		    					)
		    			 )
		    			{
		    				addDepositToPaymentScheduleSession();
		    				DomainDataView ddv = null;
		        			//Only For residential property
	    					if(
	    						!getIsContractCommercial() && 
	    						this.getSelectOneContractType()!=null && 
	    						!this.getSelectOneContractType().equals("-1")
	    					   )
			    			{
	    						 ddv =CommonUtil.getIdFromType(
																CommonUtil.getDomainDataListForDomainType(WebConstants.CONDITION),
																WebConstants.Fees.LEASE_CONTRACT_ADMINISTRATIVE_FEE
 															  );
			    			    feesConditionsMap.put(ddv.getDomainDataId(), ddv.getDataValue());
//	    						addFeeToPaymentScheduleSession();
//	    						feesConditionsMap.remove(ddv.getDomainDataId());

			    			}
			    			else
			    			{
			    				ddv =CommonUtil.getIdFromType(
																CommonUtil.getDomainDataListForDomainType(WebConstants.CONDITION),
																WebConstants.Fees.LEASE_CONTRACT_COMMERCIAL_FEE 
														     );
			    				feesConditionsMap.put(ddv.getDomainDataId(), ddv.getDataValue());
//			    				List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			    				paymentScheduleViewList  = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_FEES_ID);
//			    				viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);

			    			}
	    					//Added after changes made for commerical contract fee -Start
	    					addFeeToPaymentScheduleSession();
	    					//Added after changes made for commerical contract fee -Finish
    						feesConditionsMap.remove(ddv.getDomainDataId());
	    					if(
	    						this.getSelectOneContractType()!=null && 
 
	    						!this.getSelectOneContractType().equals("-1")
	    					  )
	    					{
    				 			viewRootMap.put(OLD_CONTRACT_TYPE,this.getSelectOneContractType());
	    					}
    				 		else
    				 		{
    				 			viewRootMap.put(OLD_CONTRACT_TYPE,"-1");
    				 		}

		    			}
	    	    }



			}

    	}
    	catch(Exception ex)
    	{
    		logger.LogException("valueChangeListerner_RentValue|Error Occured..",ex);
    	}



    }


    public static int getDaysBetween( Calendar d1, Calendar d2 )
    {
        if ( d1.after( d2 ) )
        { // swap dates so that d1 is start and d2 is end
            Calendar swap = d1;
            d1 = d2;
            d2 = swap;
        }
        int days = d2.get( Calendar.DAY_OF_YEAR ) - d1.get( Calendar.DAY_OF_YEAR );
        int y2 = d2.get( Calendar.YEAR );
        if ( d1.get( Calendar.YEAR ) != y2 )
        {
            d1 = (Calendar) d1.clone();
            do
            {
                days += d1.getActualMaximum( Calendar.DAY_OF_YEAR );
                d1.add( Calendar.YEAR, 1 );
            } while ( d1.get( Calendar.YEAR ) != y2 );
        }
        return days;
    }

    public void CalculateTotalRentValue()
    {

    	String methodName="CalculateTotalRentValue";
    	logger.logInfo(methodName+"|"+"Start..");
    	try
    	{
    		totalContractValue=new Double(0);
    		Double unitAmount=new Double(0);
    		if(getContractStartDate()!=null && !getContractStartDate().equals("") && getContractEndDate()!=null && !getContractEndDate().equals(""))
		    {
    		   int dateDiffDays=1;
		       dateDiffDays= CommonUtil.getDaysDifference( getContractStartDate(), getContractEndDate() );
			   if(  getUnitRentAmount() != null && getUnitRentAmount().trim().length() > 0 )
	           {
				  unitAmount=new Double( getUnitRentAmount() );
	        	  totalContractValue=totalContractValue+(unitAmount);
	        	  logger.logInfo(methodName+"|UnitRentAmount:%s|totalContractValue:%s", getUnitRentAmount(), totalContractValue );
	           }
			   calculateFacilityAmount(dateDiffDays);
		       logger.logInfo(methodName+"|"+"Applying Ceiling to Total Contract Value:"+totalContractValue);
		       totalContractValue=Math.ceil(totalContractValue);
		    }
		    logger.logInfo(methodName+"|"+"Total Contract Value Calculated::"+totalContractValue);
		    logger.logInfo(methodName+"|"+"Finish..");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}


    }


	private void calculateFacilityAmount( int dateDiffDays) {

		String methodName="calculateFacilityAmount";
		List<PaidFacilitiesView> paidFacilitiesViewList;
		putSelectedFacilitiesInView();
		if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)&&
			viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null  )
		{
			paidFacilitiesViewList =(ArrayList)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
		    logger.logInfo(methodName+"|"+"Size of selected paid Facilities list:"+paidFacilitiesViewList.size() );

		    for (PaidFacilitiesView paidFacilityView : paidFacilitiesViewList)
		    {
		           Double  facilityRentValue = getFacilityAmountForDuration(
		        		   dateDiffDays, paidFacilityView);
		           logger.logInfo(methodName+"|"+"totalContractValue+ facilityRentValue:"+totalContractValue+"+"+ facilityRentValue);
					totalContractValue=totalContractValue+ facilityRentValue;
					logger.logInfo(methodName+"|"+"totalContractValue:"+totalContractValue);
			}
		}
	}


	private Double getFacilityAmountForDuration(int dateDiffDays,PaidFacilitiesView paidFacilityView)
	{
		String methodName="getFacilityAmountForDuration";
		logger.logInfo(methodName+"|"+"Start:");
		Double dailyFacilityRentValue =new Double(0);
        Double facilityRentValue=new Double(0);

        logger.logDebug(methodName+"|"+"Paid Facility ID:"+paidFacilityView.getPaidFacilitiesId() );
        logger.logDebug(methodName+"|"+"Paid Facility Name:"+paidFacilityView.getFacilityName() );
        logger.logDebug(methodName+"|"+"Paid Facility Rent:"+paidFacilityView.getRent() );
        if(pageMode.equals(PAGE_MODE_ADD) && paidFacilityView.getRent()!=null)
        {
        	Double daysInYear= new Double(viewRootMap.get(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR).toString());
			logger.logInfo(methodName+"|"+"Calculating facilities daily rent amount by formula:Facility Rent / days in year from sytem config::::: "+paidFacilityView.getRent()+"/ "+daysInYear);
			 dailyFacilityRentValue=paidFacilityView.getRent()/daysInYear;
			logger.logInfo(methodName+"|"+"Value of facilities daily rent amount:: "+dailyFacilityRentValue);
			  //dailyFacilityRentValue=new Double(paidFacilityView.getRent())/365;
			  logger.logInfo(methodName+"|"+"Formula for calculating facility rent :Facility Daily Rent * Diff of Contract Start and End Date:: "+dailyFacilityRentValue+" * "+dateDiffDays);
			  facilityRentValue=dailyFacilityRentValue*dateDiffDays;

			  logger.logInfo(methodName+"|"+"Facility rent "+facilityRentValue);
        }
        else if(paidFacilityView.getRent()!=null)
        {
        	facilityRentValue=new Double(paidFacilityView.getRent());
        }
        logger.logInfo(methodName+"|"+"Finish:");
		return facilityRentValue;
	}

    @SuppressWarnings( "unchecked" )
	private Double getUnitAmuntForSelectedDuration()throws Exception
	{
	  String methodName="getUnitAmuntForSelectedDuration";
	  BigDecimal unitAmount;
	  int dateDiffDays=1;
	  BigDecimal unitDailyRent=new BigDecimal(0.0);
	  BigDecimal daysInYear= new BigDecimal(viewRootMap.get(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR).toString());
	  logger.logInfo(methodName+"|Start|Calculating unit's daily rent amount by formula " +
	  		"Unit Offered Rent Amount/days in year from system config:"+ getUnitRentAmount()+"/" +daysInYear );


	  unitDailyRent=new BigDecimal( getUnitRentAmount() ).divide( new BigDecimal( daysInYear.doubleValue() ),20,RoundingMode.HALF_UP);
	  logger.logInfo(methodName+"|Value of unit's daily rent amount by formula:%s|Contract Start Date:%s" +
	  		"|Contract End Date:%s ",unitDailyRent,getContractStartDate(),getContractEndDate());
	  dateDiffDays = CommonUtil.getDaysDifference(getContractStartDate(),getContractEndDate());
	  logger.logInfo(methodName+"|Diff in days:%s",dateDiffDays);
	  unitAmount = unitDailyRent.multiply(  new BigDecimal(  dateDiffDays ) );
	  logger.logInfo(methodName+"|unitAmount:%s",unitAmount);

	  setUnitRentAmount(  new Double( Math.ceil( unitAmount.doubleValue() ) ).toString() );
	  logger.logInfo(methodName+"|Fiinish|After ceiling value is:%s",new Double( Math.ceil( unitAmount.doubleValue() ) ).toString() );
	  return new Double( Math.ceil( unitAmount.doubleValue() ) );
	}


    private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}



	private void getPaymentScheduleListFromSession()
	{

		int rowId = 0;
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)
			    && viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
		  )
		{
			List<PaymentScheduleView> tempPaymentScheduleDataList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			paymentScheduleDataList.clear();
			for (PaymentScheduleView paymentScheduleView : tempPaymentScheduleDataList) {
    			//IF DURING AMEND THE ALREADY EXISTED ID DELETED
				if(paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
				{
				  paymentScheduleView.setRowId( paymentScheduleView.hashCode() );
				  rowId = rowId + 1 ;
				  paymentScheduleDataList.add( paymentScheduleView );
			    }
		}
		}
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentScheduleDataList()
	{
	    getPaymentScheduleListFromSession();
		return paymentScheduleDataList;
	}
	@SuppressWarnings( "unchecked" )
	  public List<PaidFacilitiesView> getPaidFacilitiesDataList()
	{
	    	if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES));
	    	paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);
			return paidFacilitiesDataList;
		}


	  @SuppressWarnings( "unchecked" )
	public List<PersonView> getProspectiveOccupierDataList()
	{
		if (sessionMap.containsKey(WebConstants.OCCUPIER))
		{
			prospectiveOccupierDataList = new ArrayList<PersonView>(0);
			List<PersonView> tempOccupierDataList  = (ArrayList)sessionMap.get(WebConstants.OCCUPIER);
			  for (PersonView personView : tempOccupierDataList  ) {
				if(personView.getIsDeleted().compareTo(new Long(1))!=0)
					prospectiveOccupierDataList.add(personView);
			  }
		}
		return prospectiveOccupierDataList ;
	}
	@SuppressWarnings( "unchecked" )
	private boolean isUnitValid()
	{
		boolean isUnitValid=false;
		List<DomainDataView> ddl= getDomainDataListForDomainType(WebConstants.UNIT_STATUS);
		String[] validUnitStautses=WebConstants.VALID_UNITS_STATUSES.split(",");
		//For update mode this criteria should not be checked as
		//unit would be in rented state after approval

		if(pageMode.equals(PAGE_MODE_ADD))
		{
			for(int j=0;j<validUnitStautses.length;j++)
			{
				HashMap unitStatusmap=getIdFromType(ddl, validUnitStautses[j]);
				if(this.getUnitStatusId().equals(unitStatusmap.get("returnId").toString()))
				{
					isUnitValid=true;
					break;
				}
			}
		}
		//if page mode is other than add specifically entered for PAGE_MODE_AMEND
		else
			isUnitValid=true;

		return isUnitValid;
	}
	@SuppressWarnings( "unchecked" )
	public void tabPaymentTerms_Click()
	{
		valueChangeListerner_RentValue();
		calculatePaymentScheduleSummaray();


	}
	@SuppressWarnings( "unchecked" )
	public String getRentId()
	{
		return WebConstants.PAYMENT_TYPE_RENT_ID.toString();

	}
	//Collect Payments Method
	@SuppressWarnings( "unchecked" )
	private void collectPayment()
	{
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		try
		{
			 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     prv.setPaidById(tenantsView.getPersonId());
		     prv = psa.collectPayments(prv);

		     activateContract(psa);
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PAYMENT_COLLECTED);
		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
		}
		catch(Exception ex)
		{
			logger.LogException(  "CollectPayments|Exception Occured...",ex);
			if(sessionMap.containsKey(WebConstants.Attachment.ATTACHED_CHEQUE_IDS) && sessionMap.get(WebConstants.Attachment.ATTACHED_CHEQUE_IDS)!=null)
				 sessionMap.remove(WebConstants.Attachment.ATTACHED_CHEQUE_IDS);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onComplete()
	{
		try	
		{
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
			{
   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			}
			activateContract(new PropertyServiceAgent());
			if(requestView !=null && requestView.getRequestId()!=null)
			{
			 saveComments(requestView.getRequestId());
			 saveAttachments(requestView.getRequestId().toString());
			}

		    saveSystemComments("contract.event.ContractActivated");
		    successMessages.add(ResourceUtil.getInstance().getProperty("contract.event.ContractActivated"));
			
		}
		catch(Exception ex)
		{
			logger.LogException("onComplete|Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onTerminateInEjari()
	{	
		errorMessages = new ArrayList<String>(0);
		try
		{
			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
			{
   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			}
			
			if(contractView.getEjariNumber() != null && contractView.getEjariNumber().trim().length()>0)
			{
				EjariResponse response= EjariIntegration.terminateContractInEjariResponse(
												   												contractView.getContractId(),
												   												requestView.getRequestNumber(),
												   												getLoggedInUser()
												   										 );
				if(response.getStatus().equals("0"))
				{
					errorMessages.add( response.getMsg() );
				}
				else
				{
					saveSystemComments("request.event.contractTerminatedInEjari");
				   
				}
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("onTerminateInEjari| Exception Occured::", ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void onRegisterForEjari()
	{
		try	
		{
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			EjariResponse response= null;
			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
			{
   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			}
			if(EjariIntegration.hasRegistrationErrors(contractView.getContractId(), errorMessages)){return;}
			//If old contract has ejari number and it is not yet terminated in ejari then renew that contract 
			if(contractView.getOldContractId()!= null )
			{
				Contract oldContract = EntityManager.getBroker().findById(Contract.class, contractView.getOldContractId());
				if(
					oldContract.getEjariNumber() != null && 
					oldContract.getEjariNumber().trim().length()>0 &&
					(
						oldContract.getTerminatedInEjari()== null || 
						oldContract.getTerminatedInEjari().equals("0")
					)
						
				  )
				{
					logger.logInfo("Renewing Ejari");
					response = EjariIntegration.renewContract(
																contractView.getContractId(), 
																oldContract , 
																getLoggedInUserId()
														 	 );
				}
				else if(
							oldContract.getEjariNumber() == null  
						)
				{
						logger.logInfo("Creating New Ejari");
						response = EjariIntegration.registerContract(
																	contractView.getContractId(),
																	getLoggedInUserId()			
																  );
				}
			}
			else
			{
					logger.logInfo("Creating New Ejari");
					response = EjariIntegration.registerContract(
																contractView.getContractId(),
																getLoggedInUserId()			
															  );
			}
			logger.logInfo("response is null? %s",response==null);
			if(response.getStatus().equals("0"))
			{
				errorMessages.add( response.getMsg() );
				
			}
			else
			{
				if(requestView !=null && requestView.getRequestId()!=null)
				{
				 saveComments(requestView.getRequestId());
				 saveAttachments(requestView.getRequestId().toString());
				}
			    saveSystemComments("contract.event.ContractRegisteredInEjari");
			    contractView.setEjariNumber( response.getContractNumber());
			    this.txtEjariNum.setValue(response.getContractNumber());
			    this.linkPrintEjariCertificate.setValue(
														 EjariIntegration.getEjariCertificateLink(contractView.getEjariNumber())		
													   );
				this.linkPrintEjariContract.setValue(
													  EjariIntegration.getEjariContractLink( contractView.getEjariNumber())		
													);
			    viewRootMap.put("CONTRACTVIEWUPDATEMODE", contractView);
			    btnRegisterEjari.setRendered(false);
			    
			    successMessages.add(ResourceUtil.getInstance().getProperty("contract.event.ContractRegisteredInEjari"));
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onRegisterForEjari|Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void activateContract(PropertyServiceAgent psa)throws PimsBusinessException, Exception, PIMSWorkListException 
	{
		 DomainDataView ddvContractStatusActive = CommonUtil.getIdFromType((List<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS),WebConstants.CONTRACT_STATUS_ACTIVE);
		 getContractPaymentSchedule(contractView.getContractId().toString());
		 contractId = contractView.getContractId().toString();
		 tenantsView = (PersonView)viewRootMap.get(TENANT_INFO); 
		 if(!getIsContrctStatusActive() && isAllPaymentsCollected())
		 {

			 UnitView unitView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);

			 requestView = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
			 // 
			 ContractDistributionInfoService.persistContractDistributionInfoOnActive(contractView);
			 //call sendPersonLocationToGRPWithRequest() here
			 if(requestView != null && requestView.getRequestId() != null)
			 {
				 new GRPCustomerServiceAgent().sendPersonLocationToGRP(
						 												requestView.getRequestId(), 
						 												tenantsView.getPersonId().longValue(), 
						 												unitView.getUnitId().longValue(), 
						 												null
						 											  );
			 }
			 //Refund The Deposit of Auction completely
			 if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
			 {
			     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
			     psa.makeAuctionDepositRefundConfiscateTransaction(rdvList.get(0), getLoggedInUser());
			 }
			 
			 ApproveRejectTask(TaskOutcome.COMPLETE);
		     notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_LeaseContract_Completed);
		     if(contractView.getStatus().compareTo(ddvContractStatusActive.getDomainDataId())!=0)
		     {
		    	 contractView.setStatus(ddvContractStatusActive.getDomainDataId());
		    	 viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
		     }

		 }
		 else
			 this.enableCollectPayment();
	}
	@SuppressWarnings( "unchecked" )
	private boolean isAllPaymentsCollected()
	{
		boolean isAllPaymentsCollected = true;
		DomainDataView ddvPending = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
				                       WebConstants.PAYMENT_SCHEDULE_PENDING);
		List<PaymentScheduleView> psvList = getPaymentScheduleDataList();
		for (PaymentScheduleView paymentScheduleView : psvList) {

			if(paymentScheduleView.getStatusId().compareTo(ddvPending.getDomainDataId())==0)
			{
				return false;
			}

		}

	   return isAllPaymentsCollected;
	}
	@SuppressWarnings( "unchecked" )
	private boolean isSinglePaymentCollected()
	{
		boolean isSinglePaymentCollected = false;
		DomainDataView ddvPending = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
				                       WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
                WebConstants.REALIZED);
		List<PaymentScheduleView> psvList = getPaymentScheduleDataList();
		for (PaymentScheduleView paymentScheduleView : psvList) {

			if( paymentScheduleView.getStatusId().compareTo(ddvPending.getDomainDataId())==0 ||
				paymentScheduleView.getStatusId().compareTo(ddvRealized.getDomainDataId())==0
			  )
			{
				return true;
			}

		}

	   return isSinglePaymentCollected;
	}

	@SuppressWarnings( "unchecked" )
	public void calculatePaymentScheduleSummaray()
	{
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{

			List<PaymentScheduleView> paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			Long cashId = ddvCash.getDomainDataId();
			DomainDataView ddvCheque = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
			Long chqId = ddvCheque.getDomainDataId();
			List<DomainDataView> paymentStatusList= (ArrayList<DomainDataView> )viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
			DomainDataView ddvDraftStatus     = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
			DomainDataView ddvPendingStatus   = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
			DomainDataView ddvCollectedStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
			DomainDataView ddvRealizedStatus  = CommonUtil.getIdFromType(paymentStatusList, WebConstants.REALIZED);
			DomainDataView ddvExemptedStatus  = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);


			this.setTotalCashAmount(0) ;
			this.setTotalChqAmount(0) ;
			this.setTotalRentAmount(0) ;
			this.setTotalDepositAmount(0) ;
			this.setTotalFines(0) ;
			this.setTotalFees(0);
			this.setInstallments(0) ;
			totalRealizedRentAmount = 0;

			for (PaymentScheduleView paymentSchedule : paymentSchedules)
			{
				//Checking Type
				if (paymentSchedule.getIsDeleted().compareTo(new Long(0))==0 && !CommonUtil.isPaymentBouncedReplacedOrExempted(paymentSchedule.getStatusId()))
				{
					if (paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue() &&
						  (
						   paymentSchedule.getStatusId().compareTo(ddvDraftStatus.getDomainDataId())==0 ||
						   paymentSchedule.getStatusId().compareTo(ddvPendingStatus.getDomainDataId())==0 ||
						   paymentSchedule.getStatusId().compareTo(ddvCollectedStatus.getDomainDataId())==0 ||
						   paymentSchedule.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0
						  )
					    )
					{
						installments++;
						this.setTotalRentAmount( this.getTotalRentAmount()  + paymentSchedule.getAmount() );


						if (paymentSchedule.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0) { // realized rent amount
							totalRealizedRentAmount+=paymentSchedule.getAmount();
						}
					}
					else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue() &&
							  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0
					         ) {
						this.setTotalDepositAmount(this.getTotalDepositAmount() + paymentSchedule.getAmount());
					}
					else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue() &&
							  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0
							 ) {
						this.setTotalFees( this.getTotalFees() + paymentSchedule.getAmount() );
					}

					else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue() &&
							  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0
							 ) {
						this.setTotalFines( this.getTotalFines()  + paymentSchedule.getAmount());
					}
					//Checking Mode
					if ( paymentSchedule.getPaymentModeId() != null &&
						 paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) !=0
					   )
					{
							if (paymentSchedule.getPaymentModeId().longValue() == cashId.longValue()) {
								this.setTotalCashAmount(this.getTotalCashAmount()  +  paymentSchedule.getAmount());
							}
							else if (paymentSchedule.getPaymentModeId().longValue() == chqId.longValue()) {
								this.setTotalChqAmount( this.getTotalChqAmount() + paymentSchedule.getAmount());
							}
					}
				}
			  }
		}
	}

	private List<PaymentScheduleView> getPaymentScheduleFromPaymentConfiguration(Long paymentType,HashMap conditionsMap) throws PimsBusinessException,Exception
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		Long ownerShipTypeId = new Long(viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString());
		if(totalContractValue!=null && totalContractValue>0)
		 return psa.getPaymentScheduleFromPaymentConfiguration(WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT, paymentType, conditionsMap, totalContractValue, getIsEnglishLocale(),ownerShipTypeId);
		else
		 return null;

	}
	@SuppressWarnings("unchecked")
	private void addFeeToPaymentScheduleSession() throws PimsBusinessException,Exception
	{
		List<PaymentScheduleView> NewpaymentScheduleViewList=new ArrayList();
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{
			List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			paymentScheduleViewList = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_FEES_ID);
			NewpaymentScheduleViewList.addAll(paymentScheduleViewList);
		}
		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		List<PaymentScheduleView>  feesList =getPaymentScheduleFromPaymentConfiguration(WebConstants.PAYMENT_TYPE_FEES_ID,feesConditionsMap) ;
		
			for (PaymentScheduleView paymentScheduleView : feesList) 
			{
				paymentScheduleView.setPaymentModeId(ddvCash.getDomainDataId());
				paymentScheduleView.setPaymentModeEn(ddvCash.getDataDescEn());
				paymentScheduleView.setPaymentModeAr(ddvCash.getDataDescAr());
				paymentScheduleView.setShowEdit(true);
				NewpaymentScheduleViewList.add(paymentScheduleView);
			}
		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, NewpaymentScheduleViewList);
		getPaymentScheduleListFromSession();
	}
	private void addDepositToPaymentScheduleSession() throws PimsBusinessException,Exception
	{
		PaymentScheduleView psv=new PaymentScheduleView();

		List<PaymentScheduleView> NewpaymentScheduleViewList=new ArrayList();
		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{
			List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			paymentScheduleViewList = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_DEPOSIT_ID);
			NewpaymentScheduleViewList.addAll(paymentScheduleViewList);
		}
		List<PaymentScheduleView>  depositsList = getPaymentScheduleFromPaymentConfiguration(WebConstants.PAYMENT_TYPE_DEPOSIT_ID,depositConditionsMap) ;
		if(depositsList!=null && depositsList.size()>0)
			for (PaymentScheduleView paymentScheduleView : depositsList)
			{
				paymentScheduleView.setPaymentModeId(ddvCash.getDomainDataId());
				paymentScheduleView.setPaymentModeEn(ddvCash.getDataDescEn());
				paymentScheduleView.setPaymentModeAr(ddvCash.getDataDescAr());
				paymentScheduleView.setShowEdit(true);

				NewpaymentScheduleViewList.add(paymentScheduleView);
			}
		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, NewpaymentScheduleViewList);
		getPaymentScheduleListFromSession();

	}
    private List<PaymentScheduleView> removePaymentTypeFromList(List<PaymentScheduleView> paymentScheduleViewList,Long paymentType)
    {
            List<PaymentScheduleView> NewpaymentScheduleViewList = new ArrayList<PaymentScheduleView>(0);
            for(int i=0;i<paymentScheduleViewList.size();i++)
            {
                    
                            PaymentScheduleView psv =(PaymentScheduleView)paymentScheduleViewList.get(i);
                            logger.logDebug("PaymentType:%s",psv.getTypeId());
                            //if not system payment than do not remove it
                            if( psv.getIsSystem().compareTo(1L)!=0 ){
                                    NewpaymentScheduleViewList.add(psv);
                            }
                            //if system payment and payment type is null (i.e. fees or fines)and not deposit 
                            else if (
                                                    paymentType==null &&
                                                    psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_DEPOSIT_ID)!=0 
                                            )
                            {
                                    if (psv.getPaymentScheduleId()!=null)
                                    {
                                        psv.setIsDeleted(new Long(1));
                                        psv.setRecordStatus(new Long(0));
                                            NewpaymentScheduleViewList.add(psv);
                                    }
                            }
                            //if its system payment and payment type is not equal to argument(like deposit,fee)
                            else if(
                                            (paymentType!=null && psv.getTypeId().compareTo(paymentType)!=0 )||
                                            ( paymentType==null && psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_DEPOSIT_ID)==0 )
                                            
                                   ) 
                            {
                                    NewpaymentScheduleViewList.add(psv);
                            }
            }
            return NewpaymentScheduleViewList;
    }


	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
//		logger.logDebug(methodName+"|"+"Start...");
//		logger.logDebug(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logDebug(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	logger.logDebug(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}

		}
		//logger.logDebug(methodName+"|"+"Finish...");
		return hMap;

	}
	public HtmlDataTable getPropspectivepartnerDataTable() {
		return propspectivepartnerDataTable;
	}

	public void tabAttachmentsComments_Click()
	{

		String methodName="tabAttachmentsComments_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
			if( viewRootMap.containsKey("REQUESTVIEWUPDATEMODE") && viewRootMap.get("REQUESTVIEWUPDATEMODE")!=null)
			requestView  = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
			if(viewRootMap.containsKey(REQUEST_FOR_AMEND) && viewRootMap.get(REQUEST_FOR_AMEND)!=null &&
					requestView ==null || requestView.getRequestId()==null )
			requestView  = (RequestView)viewRootMap.get(REQUEST_FOR_AMEND);
			if(requestView !=null && requestView.getRequestId()!=null)
	    	     loadAttachmentsAndComments(requestView.getRequestId());
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");

	}
	public void tabCommercialActivity_Click()
	{
		String methodName="tabCommercialActivity_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
					if(!viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
				   CommercialActivityController.getAllCommercialActivities(contractId);
		    //sessionMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityList);
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
	}
    public void chkPaidFacilities_Changed()
    {
    	try
    	{
			PaidFacilitiesView pfv=(PaidFacilitiesView)paidFacilitiesDataTable.getRowData();
			if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
			{
				processPaidFacilitiesForAmend(pfv);
			}
	    }
		catch (Exception ex)
		{
			logger.LogException( "chkPaidFacilities_Changed|Exception Occured...",ex);
		}
    }
    private void processPaidFacilitiesForAmend(PaidFacilitiesView pfv)throws Exception
    {
    	String methodName="processPaidFacilitiesForAmend";
    	logger.logInfo(methodName+"|"+"Start...");
    	int dateDiffDays=1;
    	logger.logDebug(methodName+"|"+"If Facility is selected ??..."+pfv.getSelected());
    	//if facility has been selected

    	if(pfv.getSelected())
    	{
    		logger.logDebug(methodName+"|"+"If Facility is deleted?? ..."+pfv.getIsDeleted());
    		//if this facility is being added for the first time
    		//in contract.
    		if(pfv.getNewFacility())//pfv.getIsDeleted().compareTo(new Long(0))==0)
    		{

    			//pfv.setIsDeleted(new Long(0));

    			//Since this facility is being added from Now, so its rent amount should be calculated
    			//from Now to ContractEndDate
		        addToSelectedFacilities(pfv);


    		}
    		//if  this facility was binded with contract before amendment
    		//i.e. this facility was selected before this amendment,
    		//then unselected during this process and now again has been selected
    		else
    		{
    			pfv.setRent(pfv.getOriginalRent());
    			pfv.setIsDeleted(new Long(0));

    		}
    	}
    	//if facility has been marked for deletion
    	else
    	{
    		//if this facility was binded with contract before amendment
    		if(!pfv.getNewFacility())
    		{
	    		pfv.setIsDeleted(new Long(1));
	    		//Since this facility is being deleted from Now so its rent amount should be calculated
				//from ContractStartDate to Now

	    		  dateDiffDays = CommonUtil.getDaysDifference( getContractStartDate(),new Date());
	    		  logger.logDebug(methodName+"|"+"Facility's Original Rent..."+pfv.getOriginalRent());
	    		  //Setting rent of facility back to the original rent
	    		  //bcz current rent in paidFacility was set according to duration
	    		  //selected in at time of contract creation
	    		  pfv.setRent(pfv.getOriginalRent());
			        Double  facilityRentValue = getFacilityAmountForDuration( dateDiffDays, pfv );
		            pfv.setRent(facilityRentValue);
    		}
    		//if this facility was not binded with contract before amendment and selected during the
    		//the process and again unselected,so removing it from ViewRootMap
    		if(pfv.getNewFacility())
    		{
    			pfv.setRent(pfv.getOriginalRent());
    			removeFromSelectedFacilities(pfv);
    		}

    	}
    	logger.logInfo(methodName+"|"+"Finish...");
    }
    private void addToSelectedFacilities(PaidFacilitiesView pfv)
    {
    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) &&
      			viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null)
    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
//        if(paidFacilitiesViewList.size()>0)
//        {
//	      	for (PaidFacilitiesView paidFacilitiesView : paidFacilitiesViewList) {
//				if(paidFacilitiesView.getFacilityId().compareTo(pfv.getFacilityId())!=0)
//	        	 paidFacilitiesViewList.add(pfv);
//	         }
//        }
//        else
      	if(!paidFacilitiesViewList.contains(pfv))
       	 paidFacilitiesViewList.add(pfv);
      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,paidFacilitiesViewList);

    }
    private void removeFromSelectedFacilities(PaidFacilitiesView pfv)
    {
    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
    	List<PaidFacilitiesView> newFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
        for (PaidFacilitiesView paidFacilitiesView : paidFacilitiesViewList)
        {
        	if(pfv.getFacilityId().compareTo(paidFacilitiesView.getFacilityId())!=0)
		       newFacilitiesViewList.add(paidFacilitiesView);
        }
        //paidFacilitiesViewList.subList(fromIndex, toIndex)
        //Collections.
      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,newFacilitiesViewList);

    }
	public void tabPaidFacility_Click()
	{
		String methodName="tabPaidFacility_Click";
		logger.logInfo(methodName+"|"+"Start...");
		List<PaidFacilitiesView> facilityViewList=new ArrayList<PaidFacilitiesView>(0);
		if(errorMessages==null)
		 errorMessages=new ArrayList<String>(0);
        try
		{
					if(!viewRootMap.containsKey(WebConstants.PAID_FACILITIES))
					{
						facilityViewList=	 getAllPaidFacilities();
						for (PaidFacilitiesView paidFacilitiesView : facilityViewList)
						{
							if(paidFacilitiesView.getSelected()!=null && paidFacilitiesView.getSelected())
								addToSelectedFacilities(paidFacilitiesView);

						}
		                viewRootMap.put(WebConstants.PAID_FACILITIES, facilityViewList);
					}
		}
		catch (Exception ex)
		{

			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");

	}
	public void tabRequestHistory_Click()
	{

		String methodName="tabRequestHistory_Click";
		logger.logInfo(methodName+"|"+"Start...");
		try
		{
			errorMessages=new ArrayList<String>(0);
	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}

	}
	public void btnContractHistory_Click()
	{

		String methodName="tabContractHistory_Click";
		logger.logInfo(methodName+"|"+"Start...");

		try
		{
			errorMessages=new ArrayList<String>(0);
	    	String javaScriptText ="var screen_width = 1024;"+
	        "var screen_height = 450;"+
	        "window.open('ContractHistoryPopUp.jsf?contractId="+contractId+
	        "','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=40,scrollbars=yes,status=yes');";

	        openPopUp("ContractHistoryPopUp.jsf?contractId="+contractId+"'", javaScriptText);

	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}

	}
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    		final String notesOwner = WebConstants.CONTRACT;
    		if(contractId!=null && contractId.trim().length()>0)
    		{
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, new Long(contractId) );
    		}
    }

	public void tabAuditTrail_Click()
    {
    	String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(contractId!=null && contractId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }
	public void tabBasicInfo_Click()
	{
		String methodName="tabBasicInfo_Click";
		logger.logInfo(methodName+"|"+"Start...");

		try
		{
			errorMessages=new ArrayList<String>(0);
			valueChangeListerner_RentValue();
	    	logger.logInfo(methodName+"|"+"Finish...");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}



	}



	private  List<PaidFacilitiesView> getAllPaidFacilities() throws PimsBusinessException
    {
    	String methodName="getAllPaidFacilities";
		logger.logInfo(methodName+"|"+"Start...");
        PropertyServiceAgent psa=new PropertyServiceAgent();
        Long conId=contractId!=null && contractId.length()>0? new Long(contractId): null;
        Long propertyId=new Long(0);
        if(viewRootMap.containsKey(UNIT_DATA_ITEM))
		{
		 UnitView unitView=(UnitView)viewRootMap.get(UNIT_DATA_ITEM);
		 propertyId=unitView.getPropertyId();
		}
//		else if(viewRootMap.containsKey("AUCTIONUNITINFO"))
//		{
//			AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
//			propertyId=auctionUnitView.getPropertyId();
//		}
        logger.logInfo(methodName+"|"+"PropertyId..."+propertyId);
        //List<PaidFacilitiesView> facilityViewList =psa.getFacilitiesByFacilityTypeId(new Long(WebConstants.FACILITY_TYPE_PAID),conId);


        List<PaidFacilitiesView> facilityViewList =psa.getPropertyFacilitiesForContractByPropertyId(propertyId,conId);
        logger.logInfo(methodName+"|"+"Finish...");
    	return facilityViewList;

    }
    private List<CommercialActivityView> getAllCommercialActivities() throws PimsBusinessException
    {
    	String methodName="getAllCommercialActivities";
		logger.logInfo(methodName+"|"+"Start...");
        PropertyServiceAgent psa=new PropertyServiceAgent();
        List<CommercialActivityView> commercialActivityViewList =psa.getAllCommercialActivities(contractId);
        viewRootMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityViewList);
		logger.logInfo(methodName+"|"+"Finish...");
    	return commercialActivityViewList;
    }

    private Boolean generateNotification(String eventName)
    {
    	   String methodName ="generateNotification";
    	      Boolean success = false;
    	            try
    	            {
    	                  logger.logInfo(methodName+"|Start");
    	                  HashMap placeHolderMap = new HashMap();
    	                  contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	                  List<ContactInfo> tenantsEmailList    = new ArrayList<ContactInfo>(0);
    	                  List<ContactInfo> applicantsEmailList = new ArrayList<ContactInfo>(0);
    	                  NotificationFactory nsfactory = NotificationFactory.getInstance();
    	                  NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
    	                  Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
    	                  getNotificationPlaceHolder(event);
    	                  if(viewRootMap.containsKey(TENANT_INFO))
    	                  {
    	                        tenantsEmailList = getEmailContactInfos((PersonView)viewRootMap.get(TENANT_INFO));
    	                        if(tenantsEmailList.size()>0)
    	                              notifier.fireEvent(event, tenantsEmailList);
    	                  }
    	                  if(hdnTenantId != hdnApplicantId &&
    	                              viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW))
    	                  {
    	                      applicantsEmailList = getEmailContactInfos((PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW));
    	                      if(applicantsEmailList.size()>0)
    	                         notifier.fireEvent(event, applicantsEmailList);
    	                  }
    	                  success  = true;
    	                  logger.logInfo(methodName+"|Finish");
    	            }
    	            catch(Exception ex)
    	            {
    	                  logger.LogException(methodName+"|Finish", ex);
    	            }
    	            return success;
    }
    private List<ContactInfo> getEmailContactInfos(PersonView personView)
    {
    	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
    			//emailList.add(ciView);
            }
    	}
    	return emailList;
    }
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		if(contractView.getTenantView()!=null)
		placeHolderMap.setValue("TENANT_NAME",contractView.getTenantView().getPersonFullName());
		placeHolderMap.setValue("CONTRACT_NO",contractView.getContractNumber());
		placeHolderMap.setValue("CONTRACT_START_DATE",contractView.getStartDateString());
		placeHolderMap.setValue("CONTRACT_END_DATE",contractView.getEndDateString());
		if(viewRootMap.containsKey(UNIT_DATA_ITEM))
		{
		 UnitView unitView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
		 placeHolderMap.setValue("CONTRACT_UNIT_NO",unitView.getUnitNumber());
		 placeHolderMap.setValue("CONTRACT_UNIT_ADDRESS",unitView.getUnitAddress());
		 placeHolderMap.setValue("CONTRACT_UNIT_PROPERTY_NAME",unitView.getPropertyCommercialName());
		}
		logger.logDebug(methodName+"|Finish");

	}
	public void setPropspectivepartnerDataTable(
			HtmlDataTable propspectivepartnerDataTable) {
		this.propspectivepartnerDataTable = propspectivepartnerDataTable;
	}

	@SuppressWarnings( "unchecked" )

	public List<PersonView> getProspectivepartnerDataList() {

		if (sessionMap.containsKey(WebConstants.PARTNER))
		{
		    prospectivepartnerDataList=new ArrayList<PersonView>(0);
			List<PersonView> tempPartnerDataList  = (ArrayList)sessionMap.get(WebConstants.PARTNER);
		  for (PersonView personView : tempPartnerDataList ) {
			if(personView.getIsDeleted().compareTo(new Long(1))!=0)
			prospectivepartnerDataList.add(personView);
		}
		}

		return prospectivepartnerDataList;
	}


	public void setProspectivepartnerDataList(
			List<PersonView> prospectivepartnerDataList) {
		this.prospectivepartnerDataList = prospectivepartnerDataList;
	}


	public PersonView getProspectivepartnerDataItem() {
		return prospectivepartnerDataItem;
	}


	public void setProspectivepartnerDataItem(PersonView prospectivepartnerDataItem) {
		this.prospectivepartnerDataItem = prospectivepartnerDataItem;
	}

	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/LeaseContract.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;

        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}


	public String openUnitsPopUp()
	{
		String methodName="openUnitsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        //WebConstants.UNIT
        if(viewRootMap.containsKey("AUCTIONUNITINFO"))
        	viewRootMap.remove("AUCTIONUNITINFO");
        auctionRefNum="";
        hdnAuctionunitId="";
        String javaScriptText ="var screen_width = 1024;"+
        "var screen_height = 450;"+
        "var popup_width = screen_width-200;"+
        "var popup_height = screen_height;"+
        "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"+
        "window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=NewLeaseContract','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
        openPopUp("UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=NewLeaseContract", javaScriptText);
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings( "unchecked" )
	public String openAuctionUnitsPopUp()
	{
		String methodName="openAuctionUnitsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        String javaScriptText ="var screen_width = 1024;"+
        "var screen_height = 500;"+
        "window.open('AllWonAuctions.jsf','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');";

        if(viewRootMap.containsKey("UNITINFO"))
        	viewRootMap.remove("UNITINFO");

        openPopUp("AllWonAuctions.jsf", javaScriptText);

		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}


	public String getRefNum() {
		if(viewRootMap.containsKey("refNum") && viewRootMap.get("refNum")!=null)
			refNum = viewRootMap.get("refNum").toString();
		return refNum;
	}

	@SuppressWarnings( "unchecked" )
	public void setRefNum(String refNum) {
		this.refNum = refNum;
		if(this.refNum!=null)
			 viewRootMap.put("refNum",this.refNum);

	}


	public String getDatetimerequest() {

		if(viewRootMap.containsKey("datetimerequest") && viewRootMap.get("datetimerequest")!=null)
			datetimerequest = viewRootMap.get("datetimerequest").toString();
		return datetimerequest;
	}

	@SuppressWarnings( "unchecked" )
	public void setDatetimerequest(String datetimerequest) {
		this.datetimerequest = datetimerequest;
		if(this.datetimerequest!=null)
			viewRootMap.put("datetimerequest",this.datetimerequest);

	}

	public String getTenantRefNum() {
		if(viewRootMap.containsKey("tenantRefNum") && viewRootMap.get("tenantRefNum")!=null)
			tenantRefNum = viewRootMap.get("tenantRefNum").toString();
		return tenantRefNum;
	}


	public void setTenantRefNum(String tenantRefNum) {
		this.tenantRefNum = tenantRefNum;
		if(this.tenantRefNum  !=null)
			viewRootMap.put("tenantRefNum",this.tenantRefNum );
	}


	public String getUnitRefNum() {
		if(viewRootMap.containsKey("unitRefNum") && viewRootMap.get("unitRefNum")!=null)
			unitRefNum= viewRootMap.get("unitRefNum").toString();
		return unitRefNum;
	}

	@SuppressWarnings( "unchecked" )
	public void setUnitRefNum(String unitRefNum) {
		this.unitRefNum = unitRefNum;
		if(this.unitRefNum!=null)
			viewRootMap.put("unitRefNum",this.unitRefNum);
	}


	public String getAuctionRefNum() {
		if(viewRootMap.containsKey("auctionRefNum") && viewRootMap.get("auctionRefNum")!=null)
			auctionRefNum = viewRootMap.get("auctionRefNum").toString();
		return auctionRefNum;
	}

	@SuppressWarnings( "unchecked" )
	public void setAuctionRefNum(String auctionRefNum) {
		this.auctionRefNum = auctionRefNum;
		if(this.auctionRefNum !=null)
			viewRootMap.put("auctionRefNum",this.auctionRefNum);
	}


	public HtmlInputText getTxtRefNum() {
		return txtRefNum;
	}


	public void setTxtRefNum(HtmlInputText txtRefNum) {
		this.txtRefNum = txtRefNum;
	}



	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getStatusList()
	{
		statusList.clear();
		List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
		for(int i=0;i<ddvList.size();i++)
		{
			SelectItem item=null;
			DomainDataView ddv=(DomainDataView)ddvList.get(i);
			//if page mode is in add mode than take the contract status as new
			if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
			{
			  if(getIsArabicLocale())
			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
			  else if(getIsEnglishLocale())
				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());

			  statusList.add(item);
			  break;
			}

			// IF PAGE IS IN UPDATE MODE
			else if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) )
			{
				  if(getIsArabicLocale())
				   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
				  else if(getIsEnglishLocale())
					item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
				  statusList.add(item);
				}



		}

		return statusList;
	}


	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}


	public String getHdnUnitId() {
		if(viewRootMap.containsKey("hdnUnitId") && viewRootMap.get("hdnUnitId")!=null)
			hdnUnitId = viewRootMap.get("hdnUnitId").toString();
		return hdnUnitId;
	}

	@SuppressWarnings( "unchecked" )
	public void setHdnUnitId(String hdnUnitId) {
		this.hdnUnitId = hdnUnitId;
		if(this.hdnUnitId!=null)
			viewRootMap.put("hdnUnitId", this.hdnUnitId);
	}


	public String getHdnTenantId() {
		if(viewRootMap.containsKey("hdnTenantId") && viewRootMap.get("hdnTenantId")!=null)
			hdnTenantId = viewRootMap.get("hdnTenantId").toString();
		return hdnTenantId;
	}

	@SuppressWarnings( "unchecked" )
	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
		if(this.hdnTenantId!=null)
			viewRootMap.put("hdnTenantId", this.hdnTenantId);
	}


	public HtmlPanelTabbedPane getPanelTabbedPane() {
		return panelTabbedPane;
	}


	public void setPanelTabbedPane(HtmlPanelTabbedPane panelTabbedPane) {
		this.panelTabbedPane = panelTabbedPane;
	}


	public HtmlPanelTab getPanelTab() {
		return panelTab;
	}


	public void setPanelTab(HtmlPanelTab panelTab) {
		this.panelTab = panelTab;
	}


	public String getPersonSponsor() {
		personSponsor=WebConstants.SPONSOR;
		return personSponsor;
	}


	public void setPersonSponsor(String personSponsor) {

		this.personSponsor = personSponsor;
	}


	public String getPersonManager() {
		personManager=WebConstants.MANAGER;
		return personManager;
	}


	public void setPersonManager(String personManager) {
		this.personManager = personManager;
	}


	public String getPersonOccupier() {
		personOccupier=WebConstants.OCCUPIER;
		return personOccupier;
	}
	public String getPersonTenant() {

		return WebConstants.PERSON_TYPE_TENANT;
	}

	public void setPersonOccupier(String personOccupier) {
		this.personOccupier = personOccupier;
	}


	public String getPersonPartner() {
		personPartner=WebConstants.PARTNER;
		return personPartner;
	}


	public void setPersonPartner(String personPartner) {
		this.personPartner = personPartner;
	}


	public PersonView getProspectiveOccupierDataItem() {
		return prospectiveOccupierDataItem;
	}


	public void setProspectiveOccupierDataItem(
			PersonView prospectiveOccupierDataItem) {
		this.prospectiveOccupierDataItem = prospectiveOccupierDataItem;
	}


	public void setProspectiveOccupierDataList(
			List<PersonView> prospectiveOccupierDataList) {
		this.prospectiveOccupierDataList = prospectiveOccupierDataList;
	}


	public HtmlDataTable getPropspectiveOccupierDataTable() {
		return propspectiveOccupierDataTable;
	}


	public void setPropspectiveOccupierDataTable(
			HtmlDataTable propspectiveOccupierDataTable) {
		this.propspectiveOccupierDataTable = propspectiveOccupierDataTable;
	}


	public Date getContractStartDate() {

	    if(viewRootMap.containsKey("CONTRACT_START_DATE"))
	    contractStartDate=(Date)viewRootMap.get("CONTRACT_START_DATE");
		return contractStartDate;
	}


	@SuppressWarnings( "unchecked" )
	public void setActualContractStartDate(Date contractStartDate) {

		viewRootMap.put("ACTUAL_CONTRACT_START_DATE",contractStartDate);
	}
	@SuppressWarnings( "unchecked" )
	public void setActualContractEndDate(Date contractEndDate) {

		viewRootMap.put("ACTUAL_CONTRACT_END_DATE",contractEndDate);
	}

	@SuppressWarnings( "unchecked" )
	public Date getActualContractStartDate() {

		return (Date)viewRootMap.get("ACTUAL_CONTRACT_START_DATE");
	}
	@SuppressWarnings( "unchecked" )
	public Date getActualContractEndDate() {

		return (Date)viewRootMap.get("ACTUAL_CONTRACT_END_DATE");
	}

	@SuppressWarnings( "unchecked" )
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
		viewRootMap.put("CONTRACT_START_DATE",this.contractStartDate);
	}


	public Date getContractEndDate() {
	 if(viewRootMap.containsKey("CONTRACT_END_DATE"))
	    contractEndDate=(Date)viewRootMap.get("CONTRACT_END_DATE");
		return contractEndDate;
	}

	@SuppressWarnings( "unchecked" )
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
		viewRootMap.put("CONTRACT_END_DATE",this.contractEndDate);
	}


	public HtmlDataTable getPaymentScheduleDataTable() {
		return paymentScheduleDataTable;
	}


	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
		this.paymentScheduleDataTable = paymentScheduleDataTable;
	}

	public void setPaymentScheduleDataList(
			List<PaymentScheduleView> paymentScheduleDataList) {
		this.paymentScheduleDataList = paymentScheduleDataList;
	}


	public PaymentScheduleView getPaymentScheduleDataItem() {
		return paymentScheduleDataItem;
	}


	public void setPaymentScheduleDataItem(
			PaymentScheduleView paymentScheduleDataItem) {
		this.paymentScheduleDataItem = paymentScheduleDataItem;
	}


	public HtmlDataTable getPaidFacilitiesDataTable() {
		return paidFacilitiesDataTable;
	}


	public void setPaidFacilitiesDataTable(HtmlDataTable paidFacilitiesDataTable) {
		this.paidFacilitiesDataTable = paidFacilitiesDataTable;
	}


	public PaidFacilitiesView getPaidFacilitiesDataItem() {
		return paidFacilitiesDataItem;
	}


	public void setPaidFacilitiesDataItem(PaidFacilitiesView paidFacilitiesDataItem) {
		this.paidFacilitiesDataItem = paidFacilitiesDataItem;
	}


	public void setPaidFacilitiesDataList(
			List<PaidFacilitiesView> paidFacilitiesDataList) {
		this.paidFacilitiesDataList = paidFacilitiesDataList;
	}


	public String getUnitRentValue() {
		if(viewRootMap.containsKey("unitRentValue") &&  viewRootMap.get("unitRentValue")!=null )
			unitRentValue = viewRootMap.get("unitRentValue").toString();
		return unitRentValue;
	}
	@SuppressWarnings( "unchecked" )
	public void setUnitRentValue(String unitRentValue) {
		this.unitRentValue = unitRentValue;
		if(this.unitRentValue!=null)
			viewRootMap.put("unitRentValue",this.unitRentValue);
	}


	public String getTxtSponsor() {
		if(viewRootMap.containsKey("txtSponsor") &&  viewRootMap.get("txtSponsor")!=null )
			txtSponsor = viewRootMap.get("txtSponsor").toString();
		return txtSponsor;
	}
	@SuppressWarnings( "unchecked" )
	public void setTxtSponsor(String txtSponsor) {
		this.txtSponsor = txtSponsor;
		if(this.txtSponsor!=null)
			viewRootMap.put("txtSponsor",this.txtSponsor);
	}


	public String getHdnSponsorId() {
		return hdnSponsorId;
	}


	public void setHdnSponsorId(String hdnSponsorId) {
		this.hdnSponsorId = hdnSponsorId;
	}


	public String getTxtManager() {
		if(viewRootMap.containsKey("txtManager") &&  viewRootMap.get("txtManager")!=null )
			txtManager = viewRootMap.get("txtManager").toString();

		return txtManager;
	}

	@SuppressWarnings( "unchecked" )
	public void setTxtManager(String txtManager) {
		this.txtManager = txtManager;
		if(this.txtManager !=null)
			viewRootMap.put("txtManager",this.txtManager);
	}


	public String getHdnManagerId() {
		return hdnManagerId;
	}


	public void setHdnManagerId(String hdnManagerId) {
		this.hdnManagerId = hdnManagerId;
	}


	public String getHdnUnitUsuageTypeId() {

		return hdnUnitUsuageTypeId;
	}


	public void setHdnUnitUsuageTypeId(String hdnUnitUsuageTypeId) {
		this.hdnUnitUsuageTypeId = hdnUnitUsuageTypeId;
	}


	public String getHdntenanttypeId() {
		if(viewRootMap.containsKey("hdntenanttypeId") && viewRootMap.get("hdntenanttypeId")!=null)
			hdntenanttypeId= viewRootMap.get("hdntenanttypeId").toString();
		return hdntenanttypeId;
	}


	public void setHdntenanttypeId(String hdntenanttypeId) {
		this.hdntenanttypeId = hdntenanttypeId;
	}


	public String getUnitRentAmount() {
		if( viewRootMap.get( "unitRentAmount") !=null )
		{
			unitRentAmount = viewRootMap.get( "unitRentAmount").toString().trim();
			logger.logDebug("getUnitRentAmount|unitRentAmount:%s",unitRentAmount);
		}
		return unitRentAmount;
	}

	@SuppressWarnings( "unchecked" )
	public void setUnitRentAmount(String unitRentAmount) {
		this.unitRentAmount = unitRentAmount;
		if( this.unitRentAmount !=null )
		{
			viewRootMap.put( "unitRentAmount", this.unitRentAmount );
			txt_ActualRent.setValue( this.unitRentAmount );
		}
	}


	public String getPageMode() {
		return pageMode;
	}


	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	public String getHdnAuctionunitId() {
		return hdnAuctionunitId;
	}


	public void setHdnAuctionunitId(String hdnAuctionunitId) {
		this.hdnAuctionunitId = hdnAuctionunitId;
	}


	public Double getTotalContractValue() {
		return totalContractValue;
	}


	public void setTotalContractValue(Double totalContractValue) {
		this.totalContractValue = totalContractValue;
	}


	public String getTxttotalContractValue() {
		if(totalContractValue>0)
		{
		txttotalContractValue=totalContractValue.toString();
		}
		return txttotalContractValue;
	}


	public void setTxttotalContractValue(String txttotalContractValue) {
		this.txttotalContractValue = txttotalContractValue;
	}


	public String getContractId() {
		return contractId;
	}


	public void setContractId(String contractId) {
		this.contractId = contractId;
	}


	public String getContractCreatedOn() {
		return contractCreatedOn;
	}


	public void setContractCreatedOn(String contractCreatedOn) {
		this.contractCreatedOn = contractCreatedOn;
	}


	public String getContractCreatedBy() {
		return contractCreatedBy;
	}


	public void setContractCreatedBy(String contractCreatedBy) {
		this.contractCreatedBy = contractCreatedBy;
	}


	public String getSelectRequestStatus() {

		if(viewRootMap.containsKey("selectRequestStatus") && viewRootMap.get("selectRequestStatus")!=null)
			selectRequestStatus = viewRootMap.get("selectRequestStatus").toString();
		return selectRequestStatus;
	}


	public void setSelectRequestStatus(String selectRequestStatus) {
		this.selectRequestStatus = selectRequestStatus;
		if(this.selectRequestStatus!=null)
			viewRootMap.put("selectRequestStatus",this.selectRequestStatus);

	}
	 public ResourceBundle getBundle() {



		 ResourceBundle bundle;

		             bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());


         return bundle;

   }



   public void setBundle(ResourceBundle bundle) {

         this.bundle = bundle;

   }


public String getTxtTenantName() {
	return txtTenantName;
}


public void setTxtTenantName(String txtTenantName) {
	this.txtTenantName = txtTenantName;
}


public String getTxtTenantType() {
	return txtTenantType;
}


public void setTxtTenantType(String txtTenantType) {
	this.txtTenantType = txtTenantType;
}


public String getTxtUnitUsuageType() {
	return txtUnitUsuageType;
}


public void setTxtUnitUsuageType(String txtUnitUsuageType) {
	this.txtUnitUsuageType = txtUnitUsuageType;
}


public String getPropertyCommercialName() {
	return propertyCommercialName;
}


public void setPropertyCommercialName(String propertyCommercialName) {
	this.propertyCommercialName = propertyCommercialName;
}


public String getPropertyAddress() {
	return propertyAddress;
}


public void setPropertyAddress(String propertyAddress) {
	this.propertyAddress = propertyAddress;
}

public boolean getIsItemEditable()
{
  boolean isItemEditable = true;
  if(pageMode!=null && (pageMode.equals(PAGE_MODE_ADD)|| pageMode.equals(PAGE_MODE_UPDATE) ||
      pageMode.equals(WebConstants.LEASE_AMEND_MODE) || pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) ||
      pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)
      ))
	  return isItemEditable ;
  else if(pageMode!=null )
  {
	  if(getIsContractStatusDraft() || getIsContractStatusNew() || getIsContractStatusApprovalRequired())
	   return true;
	  else
		  return false;

  }
  return isItemEditable;
}

public String getContractStatus()
{
	String methodName="getContractStatus";
	contractStatus="";
	List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
	for(int i=0;i<ddvList.size();i++)
	{

		DomainDataView ddv=(DomainDataView)ddvList.get(i);

		//if page mode is in add mode than take the contract status as new
		if(pageMode!=null && pageMode.equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
		{
		  if(getIsArabicLocale())
		       contractStatus= ddv.getDataDescAr();
		  else if(getIsEnglishLocale())
			   contractStatus= ddv.getDataDescEn();

		  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
        	break;
		}

		// IF PAGE IS IN UPDATE MODE
		else if(pageMode!=null && !pageMode.equals(PAGE_MODE_ADD)  && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
		{

			  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		      if(contractView.getStatus()!=null &&  ddv.getDomainDataId().toString().equals(contractView.getStatus().toString()) )
		      {
				  if(getIsArabicLocale())
					  contractStatus= ddv.getDataDescAr();
				  else if(getIsEnglishLocale())
					  contractStatus= ddv.getDataDescEn();

				  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
				  break;
		      }

	    }
	}


	return contractStatus;
}


public void setContractStatus(String contractStatus) {
	this.contractStatus = contractStatus;
}


public String getTxtMaintainenceAmount() {
	return txtMaintainenceAmount;
}


public void setTxtMaintainenceAmount(String txtMaintainenceAmount) {
	this.txtMaintainenceAmount = txtMaintainenceAmount;
}


public Double getMaintainenceAmount() {
	return maintainenceAmount;
}


public void setMaintainenceAmount(Double maintainenceAmount) {
	this.maintainenceAmount = maintainenceAmount;
}


public String getUnitStatusId() {
	if(viewRootMap.containsKey("unitStatusId") && viewRootMap.get("unitStatusId") !=null)
		unitStatusId = viewRootMap.get("unitStatusId").toString();
	return unitStatusId;
}


public void setUnitStatusId(String unitStatusId) {
	this.unitStatusId = unitStatusId;
	if(this.unitStatusId!=null)
		viewRootMap.put("unitStatusId", this.unitStatusId);
}


public String getTxtUnitStatus() {
	return txtUnitStatus;
}


public void setTxtUnitStatus(String txtUnitStatus) {
	this.txtUnitStatus = txtUnitStatus;
}





public HtmlDataTable getUnitDataTable() {
	return unitDataTable;
}


public void setUnitDataTable(HtmlDataTable unitDataTable) {
	this.unitDataTable = unitDataTable;
}

@SuppressWarnings( "unchecked" )
public List<UnitView> getUnitDataList() throws Exception{


	if(viewRootMap.containsKey("AUCTIONUNITINFO"))
	 {
		AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
		if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  &&
		   auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
		  )
		{
			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
			viewRootMap.remove(AUCTION_UNIT_ID);
		}

		if(!viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
			(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  &&
			 auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
			 )
		  )
		 {
			 viewRootMap.put(PREVIOUS_UNIT_ID,auctionUnitView.getUnitId());
		     populateAuctionUnitDetail();
		     viewRootMap.remove(WebConstants.PAID_FACILITIES);
		 }
	 }
	 else if(viewRootMap.containsKey("UNITINFO"))
	 {
		 UnitView unitView=(UnitView)viewRootMap.get("UNITINFO");
		 logger.logInfo( "getUnitDataList|unitRentAmount:%s",getUnitRentAmount() );
		 if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  &&
				 unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
		  )
		{
			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
			viewRootMap.remove(AUCTION_UNIT_ID);
		}
		 if( !viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
			( viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  &&
			  unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
			 )
			)
		 {
			 logger.logInfo("getUnitDataList|PropertyCategoryId",unitView.getPropertyCategoryId());
		     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
			 viewRootMap.put(PREVIOUS_UNIT_ID,unitView.getUnitId());
			 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(unitView.getUnitId().toString()))
			 {
			  unitDataList.clear();
			  unitDataList.add(unitView);
			  this.setUnitRefNum(unitView.getUnitNumber());
			  if(unitView.getStatusId()!=null)
		      {
	            unitView.setStatusId(unitView.getStatusId());
	            this.setUnitStatusId(unitView.getStatusId().toString());
		      }
			  viewRootMap.put(UNIT_DATA_ITEM, unitView);
			  if( unitView.getContractUnitRentValue()!=null &&  unitView.getContractUnitRentValue().toString().length()>0 )
				  setUnitRentAmount(  unitView.getContractUnitRentValue().toString() );
			  else
			  {
        	    setUnitRentAmount(  unitView.getRentValue().toString() );
        	    getUnitAmuntForSelectedDuration();
			  }
        	  viewRootMap.remove(WebConstants.PAID_FACILITIES);
			 }
		 }
	 }

	return unitDataList;
}

   
	private void markAllSystemPaymentScheduleAsDeleted()
	{
		String methodName="markAllSystemPaymentScheduleAsDeleted";
		List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
		List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
	  	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
	  	  paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	    for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList)
	    {
	    	if(paymentScheduleView.getIsSystem()!=null && paymentScheduleView.getIsSystem().compareTo(1L)==0)
	    	{
	        		paymentScheduleView.setIsDeleted(new Long(1));
	        		paymentScheduleView.setRecordStatus(new Long(0));
	    	}
	    	newPaymentScheduleViewList.add(paymentScheduleView);
	    }
	    viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList);

	}
@SuppressWarnings( "unchecked" )
private void populateAuctionUnitDetail()
{
	String methodName= "populateAuctionUnitDetail";
	logger.logInfo(methodName+"|Start");
	AuctionUnitView auctionUnitView=(AuctionUnitView)viewRootMap.get("AUCTIONUNITINFO");
	 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(auctionUnitView.getUnitId().toString()))
	 {
		 UnitView unitView =auctionUnitView.getUnitView();
		 unitView.setAuctionUnitId(auctionUnitView.getAuctionUnitId());
		 viewRootMap.put(AUCTION_UNIT_ID,auctionUnitView.getAuctionUnitId());
		 unitView.setRentValue(auctionUnitView.getBidPrice());
		 unitView.setUnitId(auctionUnitView.getUnitId());
	     unitView.setUnitNumber( auctionUnitView.getUnitNumber());
	     this.setUnitRefNum(auctionUnitView.getUnitNumber());
	     unitView.setUsageTypeId(auctionUnitView.getUnitUsageTypeID());
	     unitView.setUnitAddress(auctionUnitView.getPropertyAdress());
	     unitView.setPropertyCommercialName(auctionUnitView.getPropertyCommercialName());
	     //unitView.setFloorName(floorName)
	     unitView.setUsageTypeEn(auctionUnitView.getUnitUsageTypeEn());
	     unitView.setUsageTypeAr(auctionUnitView.getUnitUsageTypeAr());
	     unitView.setStatusEn( auctionUnitView.getUnitStatusEn());
	     unitView.setStatusAr(auctionUnitView.getUnitStatusAr());
	     logger.logInfo(methodName+"|PropertyCategoryId",unitView.getPropertyCategoryId());
	     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
	     if(auctionUnitView.getUnitStatusId()!=null)
	     {
	        unitView.setStatusId(auctionUnitView.getUnitStatusId());
	        this.setUnitStatusId(auctionUnitView.getUnitStatusId().toString());
	     }
	     if(auctionUnitView.getBidderId()!=null)
	    	 getDepositAmountForBidder(auctionUnitView.getAuctionUnitId(), auctionUnitView.getBidderId(),null);
	     unitDataList.clear();
	     unitDataList.add(unitView);
	     viewRootMap.put(UNIT_DATA_ITEM, unitView);
	     setUnitRentAmount( unitView.getRentValue().toString() );
	     logger.logInfo(methodName+"|Finish");
	 }
}
	@SuppressWarnings( "unchecked" )
	private void getDepositAmountForBidder(Long auctionUnitId,Long bidderId,Long movedToContractId) {

		 String methodName = "getDepositAmountForBidder";
		 RequestServiceAgent rsa ;
		 try
		 {
			 rsa = new RequestServiceAgent();
			 List<RequestDetailView> rdViewList = new ArrayList<RequestDetailView>(0);
			 List<RequestDetailView> newRDViewList = new ArrayList<RequestDetailView>(0);
			 if(auctionUnitId != null && bidderId != null )
			  rdViewList = rsa.getRequestDetailForBidderAuctionUnit(auctionUnitId, bidderId);
			 else if(movedToContractId != null )
			  rdViewList = rsa.getRequestDetailForMovedToContractId(movedToContractId);
			 Double auctionDepositAmount =0D;
			 for (RequestDetailView requestDetailView : rdViewList)
			 {
				 auctionDepositAmount += (requestDetailView.getDepositAmount() -  requestDetailView.getConfiscatedAmount());
			     requestDetailView.setUpdatedBy(getLoggedInUser());
			     newRDViewList.add(requestDetailView);
			 }
			 viewRootMap.put(  AUCTION_UNIT_BIDDER_LIST, newRDViewList );
			 if( auctionDepositAmount > 0D )
				viewRootMap.put(AUCTION_DEPOSIT_AMOUNT,auctionDepositAmount);
		 }
		 catch(Exception e)
		 {
			 logger.LogException(methodName+"|Error Occured",e);
		 }
	}
public void setUnitDataList(List<UnitView> unitDataList) {
	this.unitDataList = unitDataList;
}


public UnitView getUnitDataItem() {
	//List<UnitView> unitViewList = getUnitDataList();
	if(viewRootMap.containsKey(UNIT_DATA_ITEM))
		unitDataItem = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
	return unitDataItem;
}


public void setUnitDataItem(UnitView unitDataItem) {
	this.unitDataItem = unitDataItem;
}


public HtmlDataTable getCommercialActivityDataTable() {
	return commercialActivityDataTable;
}


public void setCommercialActivityDataTable(
		HtmlDataTable commercialActivityDataTable) {
	this.commercialActivityDataTable = commercialActivityDataTable;
}


public List<CommercialActivityView> getCommercialActivityList() {
	commercialActivityList=null;
		 	if(commercialActivityList==null || commercialActivityList.size()<=0)
		 	{
		 	  if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
		 		commercialActivityList=(ArrayList)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");

		 	}
 	return commercialActivityList;
}


public void setCommercialActivityList(
		List<CommercialActivityView> commercialActivityList) {
	this.commercialActivityList = commercialActivityList;
}


public CommercialActivityView getCommercialActivityItem() {
	return commercialActivityItem;
}


public void setCommercialActivityItem(
		CommercialActivityView commercialActivityItem) {
	this.commercialActivityItem = commercialActivityItem;
}


public HtmlCommandButton getBtnOccuipers() {
	return btnOccuipers;
}


public void setBtnOccuipers(HtmlCommandButton btnOccuipers) {
	this.btnOccuipers = btnOccuipers;
}


public HtmlCommandButton getBtnPartners() {
	return btnPartners;
}


public void setBtnPartners(HtmlCommandButton btnPartners) {
	this.btnPartners = btnPartners;
}


public HtmlCommandButton getBtnSaveNewLease() {
	return btnSaveNewLease;
}


public void setBtnSaveNewLease(HtmlCommandButton btnSaveNewLease) {
	this.btnSaveNewLease = btnSaveNewLease;
}


public HtmlCommandButton getBtnSaveAmendLease() {
	return btnSaveAmendLease;
}


public void setBtnSaveAmendLease(HtmlCommandButton btnSaveAmendLease) {
	this.btnSaveAmendLease = btnSaveAmendLease;
}


public HtmlCommandButton getBtnCompleteAmendLease() {
	return btnCompleteAmendLease;
}


public void setBtnCompleteAmendLease(HtmlCommandButton btnCompleteAmendLease) {
	this.btnCompleteAmendLease = btnCompleteAmendLease;
}


public HtmlCommandButton getBtnSubmitAmendLease() {
	return btnSubmitAmendLease;
}


public void setBtnSubmitAmendLease(HtmlCommandButton btnSubmitAmendLease) {
	this.btnSubmitAmendLease = btnSubmitAmendLease;
}


public HtmlGraphicImage getImgSponsor() {
	return imgSponsor;
}


public void setImgSponsor(HtmlGraphicImage imgSponsor) {
	this.imgSponsor = imgSponsor;
}


public HtmlGraphicImage getImgManager() {
	return imgManager;
}


public void setImgManager(HtmlGraphicImage imgManager) {
	this.imgManager = imgManager;
}


public HtmlCommandButton getBtnApprove() {
	return btnApprove;
}


public void setBtnApprove(HtmlCommandButton btnApprove) {
	this.btnApprove = btnApprove;
}


public HtmlCommandButton getBtnReject() {
	return btnReject;
}


public void setBtnReject(HtmlCommandButton btnReject) {
	this.btnReject = btnReject;
}


public HtmlCommandButton getBtnGenPayments() {
	return btnGenPayments;
}


public void setBtnGenPayments(HtmlCommandButton btnGenPayments) {
	this.btnGenPayments = btnGenPayments;
}


public HtmlCommandButton getBtnApproveAmendLease() {
	return btnApproveAmendLease;
}


public void setBtnApproveAmendLease(HtmlCommandButton btnApproveAmendLease) {
	this.btnApproveAmendLease = btnApproveAmendLease;
}


public HtmlCommandButton getBtnRejectAmendLease() {
	return btnRejectAmendLease;
}


public void setBtnRejectAmendLease(HtmlCommandButton btnRejectAmendLease) {
	this.btnRejectAmendLease = btnRejectAmendLease;
}


public String getPageTitle() {
	return pageTitle;
}


public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
}


public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkCommercial() {
	return chkCommercial;
}


public void setChkCommercial(
		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial) {
	this.chkCommercial = chkCommercial;
}


public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkPaidFacilities() {
	return chkPaidFacilities;
}


public void setChkPaidFacilities(
		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities) {
	this.chkPaidFacilities = chkPaidFacilities;
}


public HtmlInputText getTxtRefNumber() {

	return txtRefNumber;
}


public void setTxtRefNumber(HtmlInputText txtRefNumber) {
	this.txtRefNumber = txtRefNumber;
}


public HtmlOutputLabel getLblRefNum() {
	return lblRefNum;
}


public void setLblRefNum(HtmlOutputLabel lblRefNum) {
	this.lblRefNum = lblRefNum;
}


public String getStrTimeZone()
{

	 return TimeZone.getDefault().toString();

}


public TimeZone getTimeZone()
{
	 return TimeZone.getDefault();

}




public Date getContractIssueDate() {

    if(viewRootMap.containsKey("CONTRACT_ISSUE_DATE"))
     contractIssueDate=(Date)viewRootMap.get("CONTRACT_ISSUE_DATE");
	return contractIssueDate;
}


public void setContractIssueDate(Date contractIssueDate) {
	this.contractIssueDate = contractIssueDate;
	if(this.contractIssueDate!=null)
	 viewRootMap.put("CONTRACT_ISSUE_DATE",this.contractIssueDate);
}


public List<SelectItem> getContractTypeList() {
	contractTypeList.clear();
	List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
	for(int i=0;i<ddvList.size();i++)
	{
		SelectItem item=null;
		DomainDataView ddv=(DomainDataView)ddvList.get(i);
			  if(getIsArabicLocale())
			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
			  else if(getIsEnglishLocale())
				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
			  contractTypeList.add(item);




	}


	return contractTypeList;
}


public void setContractTypeList(List<SelectItem> contractTypeList) {
	this.contractTypeList = contractTypeList;
}


public String getSelectOneContractType() {
	if(viewRootMap.containsKey("selectOneContractType") && viewRootMap.get("selectOneContractType")!=null)
		selectOneContractType= viewRootMap.get("selectOneContractType").toString();
		return selectOneContractType;
}


public void setSelectOneContractType(String selectOneContractType) {
	this.selectOneContractType = selectOneContractType;
	if(this.selectOneContractType !=null)
		viewRootMap.put("selectOneContractType",this.selectOneContractType);
}

public double getTotalRentAmount() {
	return totalRentAmount;
}


public void setTotalRentAmount(double totalRentAmount) {
	this.totalRentAmount = totalRentAmount;
}


public double getTotalCashAmount() {
	return totalCashAmount;
}


public void setTotalCashAmount(double totalCashAmount) {
	this.totalCashAmount = totalCashAmount;
}


public double getTotalChqAmount() {
	return totalChqAmount;
}


public void setTotalChqAmount(double totalChqAmount) {
	this.totalChqAmount = totalChqAmount;
}


public int getInstallments() {
	return installments;
}


public void setInstallments(int installments) {
	this.installments = installments;
}


public double getTotalDepositAmount() {
	return totalDepositAmount;
}


public void setTotalDepositAmount(double totalDepositAmount) {
	this.totalDepositAmount = totalDepositAmount;
}


public double getTotalFees() {
	return totalFees;
}


public void setTotalFees(double totalFees) {
	this.totalFees = totalFees;
}


public double getTotalFines() {
	return totalFines;
}


public void setTotalFines(double totalFines) {
	this.totalFines = totalFines;
}








public org.richfaces.component.html.HtmlTab getTabCommercialActivity() {
	return tabCommercialActivity;
}


public void setTabCommercialActivity(
		org.richfaces.component.html.HtmlTab tabCommercialActivity) {
	this.tabCommercialActivity = tabCommercialActivity;
}


public org.richfaces.component.html.HtmlTab getTabPartner() {
	return tabPartner;
}


public void setTabPartner(org.richfaces.component.html.HtmlTab tabPartner) {
	this.tabPartner = tabPartner;
}


public org.richfaces.component.html.HtmlTab getTabOccupier() {
	return tabOccupier;
}


public void setTabOccupier(org.richfaces.component.html.HtmlTab tabOccupier) {
	this.tabOccupier = tabOccupier;
}


public org.richfaces.component.html.HtmlTab getTabContractHistory() {
	return tabContractHistory;
}


public void setTabContractHistory(
		org.richfaces.component.html.HtmlTab tabContractHistory) {
	this.tabContractHistory = tabContractHistory;
}


public org.richfaces.component.html.HtmlTab getTabRequestHistory() {
	return tabRequestHistory;
}


public void setTabRequestHistory(
		org.richfaces.component.html.HtmlTab tabRequestHistory) {
	this.tabRequestHistory = tabRequestHistory;
}


public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
	return tabPaymentTerms;
}


public void setTabPaymentTerms(
		org.richfaces.component.html.HtmlTab tabPaymentTerms) {
	this.tabPaymentTerms = tabPaymentTerms;
}


public org.richfaces.component.html.HtmlTab getTabFacilities() {
	return tabFacilities;
}


public void setTabFacilities(org.richfaces.component.html.HtmlTab tabFacilities) {
	this.tabFacilities = tabFacilities;
}
@SuppressWarnings( "unchecked" )
public String cmdChangeTenant_Click(){
	String  METHOD_NAME = "cmdChangeTenant_Click|";
    logger.logInfo(METHOD_NAME+"started...");
    try{

    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	clearApplicationDetails();
    	PropertyServiceAgent psa=new PropertyServiceAgent();
    	RequestView requestView =new RequestView();
    	requestView.setContractView(contractView);//Id(contractView.getContractId());
    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);

    	List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null,null);

    	DomainDataView ddv=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
    			                                    WebConstants.REQUEST_STATUS_COMPLETE);
    	//if there is some request for change tenant name against this
    	//contract and its status is not completed then display that request
    	//otherwise create new screen
    	requestView=null;
    	for (RequestView requestView2 : requestViewList) {

    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
    		{
    			requestView =requestView2;
    		   break;
    		}
		}
    	if(requestView!=null)
    	{

    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
    	}
    	else
    	{
    		//context.
    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);

    	}
    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}
	return WebConstants.ContractListNavigation.CHANGE_TENANT_NAME;
}
public String cmdAmendLeaseContract_Click(){
	String  METHOD_NAME = "cmdAmendLeaseContract_Click|";
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	CommonUtil commonUtil = new CommonUtil();
    	PropertyServiceAgent psa=new PropertyServiceAgent();
    	RequestServiceAgent rsa=new RequestServiceAgent();
    	RequestView requestView =new RequestView();
    	List<RequestView> requestViewList =new ArrayList<RequestView>(0);
    	//requestView.setContractView(contractView);//Id(contractView.getContractId());
    	RequestTypeView requestTypeView =new RequestTypeView();
    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
    	RequestFieldDetailView rfdv=new RequestFieldDetailView();
    	RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);
    	requestViewList=
    		              rsa.getRequestFromRequestField(contractView.getContractId().toString(),
    		            		                         requestView.getRequestTypeId().toString(),
    		            		                         requestKeyView.getRequestKeyId().toString(),
    		            		                         null
    		                                            );




    	//List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null);

    	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
    			                                    WebConstants.REQUEST_STATUS_COMPLETE);

    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
    	//if there is some request for Amend against this
    	//contract and its status is not completed then display that request
    	//otherwise create new screen
    	requestView=null;
    	for (RequestView requestView2 : requestViewList) {

    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
    		{
    			requestView =requestView2;
    		   break;
    		}
		}
    	if(requestView!=null)
    	{

    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
    		setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
    	}
    	else
    	{
    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);

    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
    	   setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
    	   sessionMap.put(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
    	}
    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}
	return WebConstants.ContractListNavigation.AMEND_CONTRACT;
}


public String cmdTransferContract_Click(){
    try
    {
    	if(this.isBlockForRenewal() == false) 
    	{
	    	clearApplicationDetails();
	    	
	    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
	    	GRPService grpService=new GRPService();
    		if(grpService.areAllTransactionsPostedToGRP(contractView.getContractNumber()))
    		{
    			errorMessages=new ArrayList<String>();
        		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeTerminatedTrasnferred"));
        		return null;
    		}   
	    	setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
	    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
	    	return WebConstants.ContractListNavigation.TRANSFER_CONTRACT;
    	}
    	else {
		       this.errorMessages = new ArrayList<String>(0);
		       this.errorMessages.add(this.getBundle().getString("blocking.DetailsStatusMessage"));
    	}
	}
	catch (Exception e) {
		logger.LogException("cmdTransferContract_Click|crashed...", e);
	}
	return "";
}
public String cmdLinkCancelContract_Click(){
	String  METHOD_NAME = "cmdLinkCancelContract_Click|";
    logger.logDebug(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
    	
    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	GRPService grpService=new GRPService();
   		if(grpService.areAllTransactionsPostedToGRP(contractView.getContractNumber()))
		{
			errorMessages=new ArrayList<String>();
    		errorMessages.add(CommonUtil.getBundleMessage("contractCantBeTerminatedTrasnferred"));
    		return null;
		}
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
  	    logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}

	return WebConstants.ContractListNavigation.CANCEL_CONTRACT;
}

public String cmdLinkSettleContract_Click(){
	String  METHOD_NAME = "cmdLinkSettleContract_Click|";
    logger.logDebug(METHOD_NAME+"started...");
    try{
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);

    	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SETTLE_PAGE_MODE","readOnly");
		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}

	return WebConstants.ContractListNavigation.SETTLE_CONTRACT;
}
public String issueCLCmdLink_Click(){
	String  METHOD_NAME = "issueCLCmdLink_Click|";
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);

		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}
	return WebConstants.ContractListNavigation.ISSUE_CLEARANCE_LETTER;
}


public String issueNOLCmdLink_Click(){
	String  METHOD_NAME = "issueNOLmdLink_Click|";
    logger.logInfo(METHOD_NAME+"started...");
    try{
    	clearApplicationDetails();
      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);

		logger.logInfo(METHOD_NAME+"completed successfully...");
	}
	catch (Exception e) {
		logger.LogException(METHOD_NAME + "crashed...", e);
	}
	return WebConstants.ContractListNavigation.ISSUE_NO_OBJECTION_LETTER;
}
private boolean hasErrorsRenewContract(ContractView contract) throws Exception
{
	boolean hasErrors =false;
	//Function Name is old Payment Exists but here it checks for all the payment of current contracts which
	//are not realized or pending
	if (CommonUtil.isOldPendingPaymentsExists( contract.getContractId(), contract.getRentAmount() ))
	{
		//Please Pay Old Payments
		errorMessages = new ArrayList<String>(0);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAY_EXISTING_PAYENTS ));
		hasErrors = true;
	}
	return hasErrors;
}

public String renewContract()
{
	String methodName = "renewContract";
	try
	{
		if(this.isBlockForRenewal() == false)
		{
			ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			if( !hasErrorsRenewContract( contractView ) )
			{
				clearApplicationDetails();
				ContractView selectedContract = contractView;
				setRequestParam("contractId", selectedContract.getContractId());
				setRequestParam("suggestedRentAmount", 0.0);
				setRequestParam("oldRentValue", selectedContract.getRentAmount());
				setRequestParam("fromRenewList", "true");
				return "renew";
			}
		}
		else {
		       this.errorMessages = new ArrayList<String>(0);
		       this.errorMessages.add(this.getBundle().getString("blocking.DetailsStatusMessage"));
		}
     }
	catch(Exception e)
	{
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		logger.LogException(methodName+"|Error Occured::",e);
	}
	return "";
}

private Boolean compareContrctStatus(String status)
{

	if( viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
	{
		ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		ContractView selectedContract = contractView;
		CommonUtil util=new CommonUtil();
		DomainDataView ddv= util.getIdFromType(util.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS), status);
		if(contractView!=null && contractView.getStatus()!=null&& contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
			return  true;
		else
			return  false;
	}
	else
		return false;

}
public Boolean getIsContrctStatusApproved() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVED);
}
public Boolean getIsContrctStatusComplete() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_COMPLETE);
}public Boolean getIsContrctStatusExpired() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_EXPIRED);
}
public Boolean getIsContrctStatusRenewDraft() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_RENEW_DRAFT);
}
public Boolean getIsContrctStatusActive() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_ACTIVE);
}
public Boolean getIsContrctStatusTerminated() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_TERMINATED);
}
public Boolean getIsContractStatusSettled(){
	return compareContrctStatus(WebConstants.CONTRACT_STATUS_SETTLED);
}
public Boolean getIsContrctStatusRejected() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_REJECTED);
}
public Boolean getIsContrctStatusCancelled() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_CANCELLED);
}
public Boolean getIsContractStatusNew() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_NEW);
}
public Boolean getIsContractStatusDraft() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_DRAFT);
}
public Boolean getIsContractStatusApprovalRequired() {

	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED);
}


public HtmlCommandButton getBtnSendNewLeaseForApproval() {
	return btnSendNewLeaseForApproval;
}


public void setBtnSendNewLeaseForApproval(
		HtmlCommandButton btnSendNewLeaseForApproval) {
	this.btnSendNewLeaseForApproval = btnSendNewLeaseForApproval;
}


public HtmlGraphicImage getImgDeleteSponsor() {
	return imgDeleteSponsor;
}


public void setImgDeleteSponsor(HtmlGraphicImage imgDeleteSponsor) {
	this.imgDeleteSponsor = imgDeleteSponsor;
}


public HtmlGraphicImage getImgDeleteManager() {
	return imgDeleteManager;
}


public void setImgDeleteManager(HtmlGraphicImage imgDeleteManager) {
	this.imgDeleteManager = imgDeleteManager;
}


public String getHdnApplicantId() {
	return hdnApplicantId;
}


public void setHdnApplicantId(String hdnApplicantId) {
	this.hdnApplicantId = hdnApplicantId;
}


public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
	return tabAuditTrail;
}


public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
	this.tabAuditTrail = tabAuditTrail;
}


//public HtmlCommandButton getBtnCollectPayment() {
//	return btnCollectPayment;
//}
//
//
//public void setBtnCollectPayment(HtmlCommandButton btnCollectPayment) {
//	this.btnCollectPayment = btnCollectPayment;
//}


public HtmlGraphicImage getImgTenant() {
	return imgTenant;
}


public void setImgTenant(HtmlGraphicImage imgTenant) {
	this.imgTenant = imgTenant;
}


public HtmlCommandButton getBtnAddUnit() {
	return btnAddUnit;
}


public void setBtnAddUnit(HtmlCommandButton btnAddUnit) {
	this.btnAddUnit = btnAddUnit;
}


public HtmlCommandButton getBtnAddAuctionUnit() {
	return btnAddAuctionUnit;
}


public void setBtnAddAuctionUnit(HtmlCommandButton btnAddAuctionUnit) {
	this.btnAddAuctionUnit = btnAddAuctionUnit;
}


public org.richfaces.component.html.HtmlCalendar getIssueDateCalendar() {
	return issueDateCalendar;
}


public void setIssueDateCalendar(
		org.richfaces.component.html.HtmlCalendar issueDateCalendar) {
	this.issueDateCalendar = issueDateCalendar;
}


public org.richfaces.component.html.HtmlCalendar getStartDateCalendar() {
	return startDateCalendar;
}


public void setStartDateCalendar(
		org.richfaces.component.html.HtmlCalendar startDateCalendar) {
	this.startDateCalendar = startDateCalendar;
}


public org.richfaces.component.html.HtmlCalendar getEndDateCalendar() {
	return endDateCalendar;
}


public void setEndDateCalendar(
		org.richfaces.component.html.HtmlCalendar endDateCalendar) {
	this.endDateCalendar = endDateCalendar;
}


public HtmlCommandButton getBtnAddPayments() {
	return btnAddPayments;
}


public void setBtnAddPayments(HtmlCommandButton btnAddPayments) {
	this.btnAddPayments = btnAddPayments;
}


public HtmlSelectOneMenu getCmbContractType() {
	return cmbContractType;
}


public void setCmbContractType(HtmlSelectOneMenu cmbContractType) {
	this.cmbContractType = cmbContractType;
}


public HtmlInputText getTxt_ActualRent() {
	return txt_ActualRent;
}


public void setTxt_ActualRent(HtmlInputText txt_ActualRent) {
	this.txt_ActualRent = txt_ActualRent;
}


public HtmlGraphicImage getBtnPartnerDelete() {
	return btnPartnerDelete;
}


public void setBtnPartnerDelete(HtmlGraphicImage btnPartnerDelete) {
	this.btnPartnerDelete = btnPartnerDelete;
}


public HtmlGraphicImage getBtnOccupierDelete() {
	return btnOccupierDelete;
}


public void setBtnOccupierDelete(HtmlGraphicImage btnOccupierDelete) {
	this.btnOccupierDelete = btnOccupierDelete;
}


public HtmlCommandButton getBtnPrint() {
	return btnPrint;
}


public void setBtnPrint(HtmlCommandButton btnPrint) {
	this.btnPrint = btnPrint;
}

public String btnPrint_Click()
{

	
	logger.logInfo("btnPrint_Click|Started...");
	UtilityServiceAgent usa = new UtilityServiceAgent();
	try
	{
		UnitView unitView=(UnitView)viewRootMap.get("UNITINFO");
		PrintLeaseContractReportCriteria reportCriteria =null;
		if( unitView != null && unitView.getPropertyCategoryId().compareTo(WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID)==0 )
		{
			reportCriteria=new PrintLeaseContractReportCriteria("PrintLeaseContractZawaya.rpt", ReportConstant.Processor.PRINT_LEASE_CONTRACT);	
		}
		else
		{
		 	reportCriteria= new PrintLeaseContractReportCriteria(ReportConstant.Report.PRINT_LEASE_CONTRACT, ReportConstant.Processor.PRINT_LEASE_CONTRACT);
		}
	    
		reportCriteria.setContractId( this.contractId );
		ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
		if ( contractView.getStatus() != null )
		{
		 reportCriteria.setStatusId( contractView.getStatus().toString() );
		 if(  !getIsContrctStatusActive() )
		  new PropertyService().changeUnitStatusToUnderContractPrinted(contractView, getLoggedInUserId() );
		}
	    HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
	   	saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PRINTED);
	   	usa.printContract(new Long(this.getContractId()),getLoggedInUser());
	   	logger.logInfo("btnPrint_Click|Finish...");
	}
	catch(Exception ex)
	{
		errorMessages =  new ArrayList<String>(0);
		logger.LogException("btnPrint_Click|Error Occured...",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	return null;
}
public String btnPrintDisclosure_Click() {

	String methodName="btnPrint_Click";
	logger.logInfo(methodName+"|Started...");
	UtilityServiceAgent usa = new UtilityServiceAgent();
	try {
			ContractDisclosureReportCriteria reportCriteria ;
			if(getIsContractCommercial())
				reportCriteria=new ContractDisclosureReportCriteria(ReportConstant.Report.COMMERCIAL_DISCLOSURE, ReportConstant.Processor.CONTRACT_DISCLOSURE_PROCESSOR);
			else
				reportCriteria=new ContractDisclosureReportCriteria(ReportConstant.Report.RESIDENTIAL_DISCLOSURE, ReportConstant.Processor.CONTRACT_DISCLOSURE_PROCESSOR);
			reportCriteria.setContractId( this.contractId );
	    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		   	saveSystemComments(MessageConstants.ContractEvents.DISCLOSURE_PRINTED);
		   	usa.printContract(new Long(this.getContractId()),getLoggedInUser());
		   	logger.logInfo(methodName+"|Finish...");
	}catch(Exception ex)
	{
		errorMessages =  new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured...",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	return null;
}
public void img_PrintConnectServicesLetter()
{
	String methodName="img_PrintConnectServicesLetter";
	try {

	    printNolReport(new PropertyServiceAgent().getSystemConfigurationValueByKey(WebConstants.FreeNolReport.ConnectingElectricityServices));
	    saveSystemComments(MessageConstants.ContractEvents.CONNECT_ELECTRICAL_SERVICES_PRINTED);

	}
	catch (Exception e)
	{
		errorMessages =  new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured...",e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}


}
public void img_PrintConnectWaterServicesLetter()
{
	String methodName="img_PrintConnectServicesLetter";
	try {

	    printNolReport(new PropertyServiceAgent().getSystemConfigurationValueByKey(WebConstants.FreeNolReport.ConnectingWaterServices));
	    saveSystemComments(MessageConstants.ContractEvents.CONNECT_WATER_SERVICES_PRINTED);

	}
	catch (Exception e)
	{
		errorMessages =  new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured...",e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));;
	}


}

public void img_PrintActivityPracticingLetter()
{
	String methodName="img_PrintConnectServicesLetter";
	try {

	    printNolReport(new PropertyServiceAgent().getSystemConfigurationValueByKey(WebConstants.FreeNolReport.PracticingActivity));
	    saveSystemComments(MessageConstants.ContractEvents.ACTIVITY_PRACTICING_LETTER_PRINTED);
	}
	catch (Exception e)
	{
		errorMessages =  new ArrayList<String>(0);
		logger.LogException(methodName+"|Error Occured...",e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}


}

private void printNolReport(String reportType) throws Exception
{

	        String methodName="printNolReport";
	        SystemParameters parameters = SystemParameters.getInstance();
			logger.logInfo(methodName+"|Started|Report : "+reportType);
			//String reportName= parameters.getParameter(reportType);
			NOLCriteria NOLReportCriteria=new NOLCriteria(reportType, ReportConstant.Processor.NOL_REPORT,CommonUtil.getLoggedInUser());
			requestView  =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
			NOLReportCriteria.setRequestId(requestView.getRequestId().toString() );
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, NOLReportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		   	logger.logInfo(methodName+"|Finish...");


}

private void openPopup(String javaScriptText) {
	logger.logInfo("openPopup() started...");
	try {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo("openPopup() completed successfully!!!");
	} catch (Exception exception) {
		logger.LogException("openPopup() crashed ", exception);
	}
}


public HtmlTabPanel getTabPanel() {
	return tabPanel;
}


public void setTabPanel(HtmlTabPanel tabPanel) {
	this.tabPanel = tabPanel;
}


public HtmlTab getTabApplicationDetails() {
	return tabApplicationDetails;
}


public void setTabApplicationDetails(HtmlTab tabApplicationDetails) {
	this.tabApplicationDetails = tabApplicationDetails;
}


public HtmlGraphicImage getImgViewSponsor() {
	return imgViewSponsor;
}


public void setImgViewSponsor(HtmlGraphicImage imgViewSponsor) {
	this.imgViewSponsor = imgViewSponsor;
}


public HtmlGraphicImage getImgViewTenant() {
	return imgViewTenant;
}


public void setImgViewTenant(HtmlGraphicImage imgViewTenant) {
	this.imgViewTenant = imgViewTenant;
}


public HtmlGraphicImage getImgViewManager() {
	return imgViewManager;
}


public void setImgViewManager(HtmlGraphicImage imgViewManager) {
	this.imgViewManager = imgViewManager;
}


public HtmlCommandButton getBtnCancelContract() {
	return btnCancelContract;
}


public void setBtnCancelContract(HtmlCommandButton btnCancelContract) {
	this.btnCancelContract = btnCancelContract;
}

	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		ContractView contView = null;
		PersonView applicant = null;
		PersonView tenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		RequestView reqView = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
		try
		{
		if(reqView != null)
		{
			contView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			if(contView != null)
			{
				tenant = contView.getTenantView();
				applicant = reqView.getApplicantView();
				UnitView unitView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);

				eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
				eventAttributesValueMap.put("CONTRACT_START_DATE", contView.getStartDateString());
				eventAttributesValueMap.put("CONTRACT_END_DATE", contView.getEndDateString());
				eventAttributesValueMap.put("PROPERTY_NAME", unitView.getPropertyCommercialName());
				eventAttributesValueMap.put("UNIT_NO", unitView.getUnitNumber());
				eventAttributesValueMap.put("TOTAL_VALUE", this.getTxttotalContractValue());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());

				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}

				if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(tenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}


			}
		}
		}
		catch (Exception e)
		{
			logger.LogException("requestStatusNotification crashed...",e);

		}
	}

	public void cancelContract()
	{

		try
		{
		    String remarks="";
		    successMessages=new ArrayList<String>();
    	    if(sessionMap.containsKey("CANCEL_REMARKS") && sessionMap.get("CANCEL_REMARKS")!=null)
    		{
    			remarks=sessionMap.get("CANCEL_REMARKS").toString();
    			sessionMap.remove("CANCEL_REMARKS");
    		}

    	    ContractView cv=new ContractView();
	    	if(viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null)
	    		contractView=(ContractView) viewRootMap.get("CONTRACTVIEWUPDATEMODE");

	    	getRequestForNewLeaseContract();
	        new PropertyServiceAgent().CancelNewLeaseContract(contractView.getContractId(),requestView.getRequestId(),getLoggedInUser());
	        DomainDataView ddvContractStatusActive = CommonUtil.getIdFromType((List<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS),WebConstants.CONTRACT_STATUS_ACTIVE);
            if( getIsContractStatusApproved() )
		    ApproveRejectTask(TaskOutcome.CANCEL);

	        if(requestView!=null  && requestView.getRequestId()!=null)
	      	{
	      		logger.logInfo("cancelContract() |"+" Saving Notes...Start");
	      		saveComments(requestView.getRequestId());
			    logger.logInfo("cancelContract() |"+" Saving Notes...Finish");
			    logger.logInfo("cancelContract() |"+" Saving Attachments...Start");
			    saveAttachments(requestView.getRequestId().toString() );
			    logger.logInfo("cancelContract() |"+" Saving Attachments...Finish");
	      	}
	        String status=getIdForStatus(WebConstants.CONTRACT_STATUS_CANCELLED);
	      	if(status!=null)
	      	   contractView.setStatus( new Long(status));

	    	pageMode=WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW;
		    viewRootMap.put("pageMode",pageMode);
		    saveSystemComments(MessageConstants.ContractEvents.CONTRACT_CANCELLED);
		    successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_CANCELLED_SUCCESSFULLY));
	    	if(remarks!=null && remarks.trim().length()>0 && requestView.getRequestId()!=null)
	    		commonUtil.saveRemarksAsComments(requestView.getRequestId(), remarks, WebConstants.REQUEST);
		}
		catch(PIMSWorkListException ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException("cancelContract() |Error Occured...",ex);

    	}
		catch (PimsBusinessException e)
		{
			errorMessages=new ArrayList<String>();
			if(e.getExceptionCode().compareTo(com.avanza.pims.business.exceptions.ExceptionCodes.CONTRACT_CANNOT_BE_CANCELED)==0)
				errorMessages.add(CommonUtil.getBundleMessage("contractCantBeCancelled"));
			logger.LogException("cancelContract() crashed...", e);
		}
		catch (Exception e)
		{
			errorMessages=new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("cancelContract() crashed...", e);
		}

	}
	private int getGracePeridForCommercial()
	{
		int grace_Period =0;
		try
		{
		  	String sysConfigKey = WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_COMMERCIAL_CONTRACT;
			if( !viewRootMap.containsKey(sysConfigKey) )
			{
				String gracePeriod = new UtilityServiceAgent().getValueFromSystemConfigration(sysConfigKey);
		        grace_Period = Integer.parseInt( gracePeriod ) ;
		        viewRootMap.put(sysConfigKey,gracePeriod);
			}
			else
				grace_Period = Integer.parseInt( viewRootMap.get( sysConfigKey).toString());

		}
		catch(Exception e)
		{
			logger.LogException("getGracePeridForCommercial() crashed...", e);

		}
		return grace_Period;
	}
	private int getGracePeridForResidential()
	{
		int grace_Period =0;
		try
		{

			if( !viewRootMap.containsKey(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT) )
			{
				String gracePeriod = new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT);
		        grace_Period = Integer.parseInt( gracePeriod ) ;
		        viewRootMap.put(WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT,gracePeriod);
			}
			else
				grace_Period = Integer.parseInt( viewRootMap.get( WebConstants.SYS_CONFIG_KEY_GRACE_PERIOD_DAYS_FOR_RESIDENTIAL_CONTRACT).toString());

		}
		catch(Exception e)
		{
			logger.LogException("getGracePeridForResidential() crashed...", e);

		}
		return grace_Period;
	}
	@SuppressWarnings( "unchecked" )
	private Boolean hasEditContractDurationPermission()
	{
		boolean hasPermission = false;
		//If contract status is active then based on permission user can edit contract duration
		if( getIsContrctStatusActive() )
		{
			User user = SecurityManager.getUser( getLoggedInUserId() );
			hasPermission = user.hasPermission("Pims.LeaseContractManagement.LeaseContract.EditContractDuration");
		}
		else
			hasPermission = false;
		return hasPermission;
	}


	public double getTotalRealizedRentAmount() {
		return totalRealizedRentAmount;
	}


	public void setTotalRealizedRentAmount(double totalRealizedRentAmount) {
		this.totalRealizedRentAmount = totalRealizedRentAmount;
	}
	/**
	 * This method will open the report for contract payments
	 */
	public void printContractPaymentReport() {

	    if(viewRootMap.get("CONTRACTVIEWUPDATEMODE") !=null)
	    {
	    	contractView = ( ContractView )viewRootMap.get("CONTRACTVIEWUPDATEMODE");
			ContractPaymentReportCriteria contractPayment = new ContractPaymentReportCriteria(ReportConstant.Report.CONTRACT_PAYMENTS, ReportConstant.Processor.CONTRACT_PAYMENTS,getLoggedInUser());
			contractPayment.setContractId(contractView.getContractId().toString());
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY,contractPayment);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP+ "');");
		}
		else{
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}


	public HtmlCommandButton getBtnAllPayment() {
		return btnAllPayment;
	}


	public void setBtnAllPayment(HtmlCommandButton btnAllPayment) {
		this.btnAllPayment = btnAllPayment;
	}
	private boolean isAnyPaymentCollected()
	{
		boolean isAnyPaymentCollected = false;

		if (viewRootMap.get("IS_ANY_PAYMENT_COLLECTED") != null) {
			isAnyPaymentCollected = true;
		}
		else {
				List<PaymentScheduleView> psvList = getPaymentScheduleDataList();

				for (PaymentScheduleView paymentScheduleView : psvList) {
				if (paymentScheduleView.getIsReceived().toUpperCase().compareTo("Y") == 0)
				{
					isAnyPaymentCollected = true;
					viewRootMap.put("IS_ANY_PAYMENT_COLLECTED", true);
					break;
				}
			}
		}
		return isAnyPaymentCollected;
	}

	public boolean isBlockForRenewal() {
		return isBlockForRenewal = Boolean.valueOf(this.viewMap.get("isBlockForRenewal").toString());
	}


	public void setBlockForRenewal(boolean isBlockForRenewal) {
		this.viewMap.put("isBlockForRenewal", isBlockForRenewal);
		this.isBlockForRenewal = isBlockForRenewal;
	}


	public String getBlockComment() {
		if(viewMap.get("blockComment") != null)
		this.blockComment = this.viewMap.get("blockComment").toString();
		return blockComment;
	}


	public void setBlockComment(String blockComment) {
		if(blockComment == null) blockComment = "";
		this.viewMap.put("blockComment", blockComment);
		this.blockComment = blockComment;
	}


	public String getUnblockComment() {
		if(viewMap.get("unblockComment") != null)
		this.unblockComment = this.viewMap.get("unblockComment").toString();
		return unblockComment;
	}


	public void setUnblockComment(String unblockComment) {
		if(unblockComment == null) unblockComment = "";
		this.viewMap.put("unblockComment", unblockComment);
		this.unblockComment = unblockComment;
	}


	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}


	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}


	public HtmlCommandButton getBtnRegisterEjari() {
		return btnRegisterEjari;
	}


	public void setBtnRegisterEjari(HtmlCommandButton btnRegisterEjari) {
		this.btnRegisterEjari = btnRegisterEjari;
	}


	public HtmlInputText getTxtEjariNum() {
		return txtEjariNum;
	}


	public void setTxtEjariNum(HtmlInputText txtEjariNum) {
		this.txtEjariNum = txtEjariNum;
	}

	@SuppressWarnings("unchecked")
	public String getEjariServiceEndPoint() {
		if(viewRootMap.get("ejariServiceEndPoint")!=null)
		{
			ejariServiceEndPoint = viewRootMap.get("ejariServiceEndPoint").toString();
		}
		return ejariServiceEndPoint;
	}

	@SuppressWarnings("unchecked")
	public void setEjariServiceEndPoint(String ejariServiceEndPoint) {
		
			this.ejariServiceEndPoint = ejariServiceEndPoint;
			viewRootMap.put("ejariServiceEndPoint",this.ejariServiceEndPoint);
		}


	public HtmlCommandButton getBtnTerminateInEjari() {
		return btnTerminateInEjari;
	}


	public void setBtnTerminateInEjari(HtmlCommandButton btnTerminateInEjari) {
		this.btnTerminateInEjari = btnTerminateInEjari;
	}
}


