package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RecommendationServiceAgent;
import com.avanza.pims.business.services.StudyServiceAgent;
import com.avanza.pims.entity.StudyPartcipant;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.StudyPartcipant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.StudyParticipantView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.RecommendationView;
import com.avanza.pims.ws.vo.RelatedStudiesView;
import com.avanza.pims.ws.vo.StudyDetailView;

public class AssetSubClassManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	private String pageMode;
	
	private AssetClassView assetClassView;
	
		
	public AssetSubClassManageBacking() 
	{
		
		logger = Logger.getLogger(AssetSubClassManageBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		commonUtil = new CommonUtil();
		pageMode = WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_ADD_ASSET_CLASS;
		assetClassView = new AssetClassView();
	
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			
			//for Status of asset Sub Class 
			DomainDataView ddvAssetStatusActive = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.ASSET_CLASS_STATUS), WebConstants.ASSET_CLASS_STATUS_ACTIVE);
			assetClassView.setStatusIdForUI( String.valueOf( ddvAssetStatusActive.getDomainDataId() ) );
			//assetClassView.setStatusId(ddvAssetStatusActive.getDomainDataId());
			
			
			
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY);
			sessionMap.remove( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY );
		}
		
		// ASSET Sub CLASS VIEW
		if ( sessionMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW ) != null )
		{
			assetClassView = (AssetClassView) sessionMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW );
			sessionMap.remove( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW );
		}
		else if ( viewMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW )  != null )
		{
			assetClassView = (AssetClassView) viewMap.get( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW ) ;
		}
		
		
			
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		
		// ASSET CLASS VIEW
		if ( assetClassView != null )
		{
			viewMap.put( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW, assetClassView );
		}
		
	
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "studySearch";
		return path;
	}
	
	public String onSave() 
	{
	 String path = null;
		 if(checkMissingField())
		  {	
			    DomainDataView ddvAssetStatus = CommonUtil.getDomainDataFromId(CommonUtil.getDomainDataListForDomainType(WebConstants.ASSET_CLASS_STATUS), Long.parseLong(assetClassView.getStatusIdForUI()));
				assetClassView.setStatusId(ddvAssetStatus.getDomainDataId());
				
				if(getIsEnglishLocale())
					assetClassView.setStatusEn(ddvAssetStatus.getDataDescEn());
				else
					assetClassView.setStatusAr(ddvAssetStatus.getDataDescAr());
					
			sessionMap.put( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW, assetClassView );		
			executeJavascript( "callOpenerFn(); closeWindow();" );		
		  }	
		return path;
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public boolean checkMissingField()
	{
		boolean check = true;
		
		if(assetClassView.getAssetClassNameAr()==null || assetClassView.getAssetClassNameAr().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.nameAr") );
			check = false;
		}
		if(assetClassView.getAssetClassNameEn()==null || assetClassView.getAssetClassNameEn().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.nameEn") );
			check = false;
		}
		if(assetClassView.getStatusIdForUI()==null || assetClassView.getStatusIdForUI().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.status") );
			check = false;
		}
		
		return check;
	}
	
	public String cmdEdit_Click(){
		return "cmdEdit_Click";
	}
	public String cmdDelete_Click(){
		return "cmdDelete_Click";
	}


	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS) )
			isViewMode = true;
		
		return isViewMode;
	}
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY ) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}


	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}


	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}

	
	
	


	public AssetClassView getAssetClassView() {
		return assetClassView;
	}

	public void setAssetClassView(AssetClassView assetClassView) {
		this.assetClassView = assetClassView;
	}



	
	// Shiraz Code End Here
}
