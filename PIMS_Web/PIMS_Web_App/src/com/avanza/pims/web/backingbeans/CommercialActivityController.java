package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.CommercialActivityView;
import com.avanza.pims.ws.vo.NotesVO;

public class CommercialActivityController extends AbstractController{

	private static Logger logger = Logger.getLogger(AttachmentController.class);
	
	private static final String NOTES_LIST_KEY = "NotesListData";
	private static final String NOTES_DATA = "NotesData";
	static Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();;
	private static PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private HtmlDataTable commercialActivityDataTable;
	private List<CommercialActivityView> commercialActivityList; //= new ArrayList<CommercialActivityView>();
	private CommercialActivityView commercialActivityItem = new CommercialActivityView();

	 public static List<CommercialActivityView> getAllCommercialActivities(String contractId) throws PimsBusinessException
	    {
	    	String methodName="getAllCommercialActivities";
			logger.logInfo(methodName+"|"+"Start...");
	        PropertyServiceAgent psa=new PropertyServiceAgent();
	        List<CommercialActivityView> commercialActivityViewList =psa.getAllCommercialActivities(contractId);
	        FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityViewList);
			//commercialActivityL 
	        logger.logInfo(methodName+"|"+"Finish...");
	    	return commercialActivityViewList;
	    }
	 @Override
		public void prerender() {
			super.prerender();
			String methodName="prerender"; 
			  logger.logInfo(methodName + "|" + "Finish...");
		}
	 @Override
		public void init() 
	   {
			super.init();
			String methodName="init"; 
			  logger.logInfo(methodName + "|" + "Finish...");
		}
	 public HtmlDataTable getCommercialActivityDataTable() {
	 	return commercialActivityDataTable;
	 }


	 public void setCommercialActivityDataTable(
	 		HtmlDataTable commercialActivityDataTable) {
	 	this.commercialActivityDataTable = commercialActivityDataTable;
	 }


	 public List<CommercialActivityView> getCommercialActivityList() {
	 	//commercialActivityList=null;
		 
	 	if(commercialActivityList==null || commercialActivityList.size()<=0)
	 	{
	 	  if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
	 		commercialActivityList=(ArrayList)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
	 	 }
		 
		 return commercialActivityList;
	 }
	 public void setCommercialActivityList(
	 		List<CommercialActivityView> commercialActivityList) {
	 	this.commercialActivityList = commercialActivityList;
	 }


	 public CommercialActivityView getCommercialActivityItem() {
	 	return commercialActivityItem;
	 }


	 public void setCommercialActivityItem(
	 		CommercialActivityView commercialActivityItem) {
	 	this.commercialActivityItem = commercialActivityItem;
	 }


	
	
}
