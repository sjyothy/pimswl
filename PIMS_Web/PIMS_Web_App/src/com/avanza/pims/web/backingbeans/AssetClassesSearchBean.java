package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.investment.AssetService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.AssetView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.ViolationView;

public class AssetClassesSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(AssetClassesSearchBean.class);
	private List<AssetClassView> dataList;
	AssetClassView assetClassView = new AssetClassView();
	AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
	private HtmlDataTable tbl_AssetClassSearch;
	private AssetClassView dataItem = new AssetClassView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	private String pageMode;
	private List<Long> alreadySelectedAssetClassList;
	
	private HtmlSelectOneMenu htmlSelectOneAssetClassStatus = new HtmlSelectOneMenu();	
	private HtmlInputText htmlAssetNumber = new HtmlInputText();
	private HtmlInputText htmlAssetNameAr = new HtmlInputText();
	private HtmlInputText htmlAssetNameEn = new HtmlInputText();
	List<SelectItem> assetClassStatusList = new ArrayList<SelectItem>();
	
	
	
	


	
	// Constructors

	/** default constructor */
	public AssetClassesSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
		pageMode = WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_SEARCH;
		alreadySelectedAssetClassList = new ArrayList<Long>(0);
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public AssetClassView getDataItem() {
		return dataItem;
	}

	public void setDataItem(AssetClassView dataItem) {
		this.dataItem = dataItem;
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/ViolationSearch.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               //"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        {
        	javaScriptText=extraJavaScript;
        }
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}


	


	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		try {

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();		
		
		String emptyValue = "-1";	
		
		Object assetNumber = htmlAssetNumber.getValue();
		Object assetNameAr = htmlAssetNameAr.getValue();
		Object assetNameEn = htmlAssetNameEn.getValue();
		Object assetStatus = htmlSelectOneAssetClassStatus.getValue();		
		
		
		if(assetNumber != null && !assetNumber.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_CLASS_SEARCH_CRITERIA.ASSET_CLASS_NUMBER, assetNumber.toString());
		}
		if(assetNameAr != null && !assetNameAr.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_CLASS_SEARCH_CRITERIA.ASSET_NAME_AR, assetNameAr.toString());
		}
		if(assetNameEn != null && !assetNameEn.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_CLASS_SEARCH_CRITERIA.ASSET_NAME_EN, assetNameEn.toString());
		}
		if(assetStatus != null && !assetStatus.toString().trim().equals("") && !assetStatus.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_CLASS_SEARCH_CRITERIA.ASSET_CLASS_STATUS, assetStatus.toString());
		}		
		
	   if(assetStatus == null || assetStatus.toString().trim().equals("") || assetStatus.toString().trim().equals(emptyValue))
		{
			ArrayList<Long> statuses = new ArrayList<Long>();
		    List<SelectItem> StatusList =  getAssetClassStatusList();
			for(SelectItem item :StatusList)
			{
				Object Stat = item.getValue();
				
				if(Stat != null && Stat.toString().trim().length()>0)
					statuses.add(Long.parseLong(Stat.toString()));					
				
			}
			
			if(statuses.size() >0)
				searchCriteria.put("ASSET_CLASS_STATUS_IN",statuses);
	     }
		
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
		Object assetNumber = htmlAssetNumber.getValue();
		Object assetNameAr = htmlAssetNameAr.getValue();
		Object assetNameEn = htmlAssetNameEn.getValue();
		Object assetStatus = htmlSelectOneAssetClassStatus.getValue();
		
		
		
		if(StringHelper.isNotEmpty(assetNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetNameAr.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetNameEn.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetStatus.toString()) && !assetStatus.toString().equals("-1"))
			return true;
		
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		logger.logInfo(methodName + "|" + "Start::");
		try 
		{
			if(atleastOneCriterionEntered()){
				loadDataList();
				if(viewRootMap.get("SESSION_ASSET_CLASS_SEARCH_LIST")!=null){
					List<AssetClassView> assetClassList = (List<AssetClassView>) viewRootMap.get("SESSION_ASSET_CLASS_SEARCH_LIST");
					if(assetClassList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
			}
			else{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		logger.logInfo(methodName + "|" + "Finish::");
		return "";
	}

	
   private void loadDataList() throws Exception
   {
	   String methodName = "btnSearch_Click";
	   logger.logInfo(methodName + "|" + "Start::");
	   Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	   HashMap searchMap= getSearchCriteria();
		
	   if(viewRootMap.containsKey("SESSION_ASSET_CLASS_SEARCH_LIST"))
		   viewRootMap.remove("SESSION_ASSET_CLASS_SEARCH_LIST");
	   
        // new function right here   	   
		List<AssetClassView> assetClassList =assetServiceAgent.getAllAssetClasses(searchMap, alreadySelectedAssetClassList);
		viewRootMap.put("SESSION_ASSET_CLASS_SEARCH_LIST",assetClassList);
		dataList=assetClassList;
	   
		  if(dataList!=null)
				recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
	        
		
		logger.logInfo(methodName + "|" + "Finish::");
   }
	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) 
			{
			PropertyService ps = new PropertyService();
			RegionView rV =  ps.getRegionViewByRegionId(19L);
				
		    }
			updateValuesFromMaps();
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY );
			LoadAssetClassStatuses();
		}
		else if ( viewRootMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewRootMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY );
		}
		// ALREADY SELECTED ASSET CLASS LIST
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST) != null )
		{
			alreadySelectedAssetClassList = (List<Long>) sessionMap.get( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST );
			sessionMap.remove( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST );
		}
		else if ( viewRootMap.get( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST ) != null )
		{
			alreadySelectedAssetClassList = (List<Long>) viewRootMap.get( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST );
		}
		
		
		
				
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewRootMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY, pageMode );
		}
	
		// ALREADY SELECTED ASSET CLASS LIST
		if ( alreadySelectedAssetClassList != null )
		{
			viewRootMap.put( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST, alreadySelectedAssetClassList );
		}
	
	}
    
	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		HashMap searchMap;
		String methodName="prerender"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
		  if(sessionMap.containsKey("FROM_STATUS")  && sessionMap.get("FROM_STATUS").toString().equals("1"))
		  {
			  sessionMap.remove("FROM_STATUS");
			  loadDataList();
			    
		  }
		   
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		  logger.logInfo(methodName + "|" + "Finish...");
	}

	

	public List<AssetClassView> getDataList() {
		dataList =null;
		if (dataList == null) 
		{
			if(viewRootMap.containsKey("SESSION_ASSET_CLASS_SEARCH_LIST"));
			dataList=(ArrayList)viewRootMap.get("SESSION_ASSET_CLASS_SEARCH_LIST");
		}
		return dataList;

	}

	public void setDataList(List<AssetClassView> dataList) {
		this.dataList = dataList;
	}

	


	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getTbl_AssetClassSearch() {
		return tbl_AssetClassSearch;
	}

	public void setTbl_AssetClassSearch(HtmlDataTable tbl_AssetClassSearch) {
		this.tbl_AssetClassSearch = tbl_AssetClassSearch;
	}


	public String btnAddAssetClasses_Click(){
	
		return "assetClassManage";
	}



	public HtmlInputText getHtmlAssetNumber() {
		return htmlAssetNumber;
	}

	public void setHtmlAssetNumber(HtmlInputText htmlAssetNumber) {
		this.htmlAssetNumber = htmlAssetNumber;
	}

	public HtmlInputText getHtmlAssetNameAr() {
		return htmlAssetNameAr;
	}

	public void setHtmlAssetNameAr(HtmlInputText htmlAssetNameAr) {
		this.htmlAssetNameAr = htmlAssetNameAr;
	}

	public HtmlInputText getHtmlAssetNameEn() {
		return htmlAssetNameEn;
	}

	public void setHtmlAssetNameEn(HtmlInputText htmlAssetNameEn) {
		this.htmlAssetNameEn = htmlAssetNameEn;
	}
	
	
	public String cmdView_Click() 
	{
		String path = null;
		assetClassView = (AssetClassView)tbl_AssetClassSearch.getRowData();
		sessionMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW , assetClassView);
		sessionMap.put(WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY, WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS);
		sessionMap.put(WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY, WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY);
		openPopup("openAssetClassManagePopup();");
		return path;
	}
	
	
	public String cmdEdit_Click(){
		assetClassView = (AssetClassView)tbl_AssetClassSearch.getRowData();
		sessionMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW , assetClassView);
		sessionMap.put(WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY, WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS);
		return "assetManage";
	}

	public String cmdDelete_Click()
	{
		try {
		
		AssetClassView assetView = (AssetClassView) tbl_AssetClassSearch.getRowData();
		assetView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
		Integer index =  tbl_AssetClassSearch.getRowIndex();
		viewRootMap.put("delete", index);
		assetServiceAgent.updateAssetClass(assetView);
		
		index = (Integer) viewRootMap.get("delete");
		if(viewRootMap.containsKey("SESSION_ASSET_CLASS_SEARCH_LIST"))
		{
		 List<AssetClassView> tempList = (List<AssetClassView>) viewRootMap.get("SESSION_ASSET_CLASS_SEARCH_LIST");
		 tempList.remove(index.intValue());
         dataList = tempList;
         viewRootMap.put("SESSION_ASSET_CLASS_SEARCH_LIST",dataList);
        if(dataList!=null)
        {
			recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
         }
         
	     infoMessages.add(CommonUtil.getBundleMessage("assetClass.msg.DeleteClass"));
		}
		
		} 
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "cmdDelete_Click"; 
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsSearchMode()
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_SEARCH) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Boolean getIsMultiSelectPopupMode()
	{
		Boolean isMultiSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP ) )
		{   
			isMultiSelectPopupMode = true;
		}	
		
		return isMultiSelectPopupMode;
	}
	
	public Boolean getIsSingleSelectPopupMode() 
	{
		Boolean isSingleSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP ) )
		{
			isSingleSelectPopupMode = true;
		}	
		
		return isSingleSelectPopupMode;
	}
	


	public String onSingleSelect()
	{
		AssetClassView selectedAssetClass = (AssetClassView) tbl_AssetClassSearch.getRowData();
		sessionMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW , selectedAssetClass );
		executeJavascript("closeAndPassSelected();");
		return null;
	}
	
	public String onMultiSelect()
	{
		List<AssetClassView> selectedAssetClassList = new ArrayList<AssetClassView>(0);
		
		if ( dataList != null )
		{			
			for ( AssetClassView assetClassView : dataList )
			{
				if ( assetClassView.getIsSelected()!=null &&  assetClassView.getIsSelected() )
				{
					selectedAssetClassList.add( assetClassView );
				}
			}			
		}
		
		if ( selectedAssetClassList.size() == 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.NO_PORTFOLIO_SELECTED_ERROR ) );
		}
		else
		{
			sessionMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW_LIST , selectedAssetClassList );
			executeJavascript("closeAndPassSelected();");
		}
		
		return null;
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
	}
	
	 private void LoadAssetClassStatuses()
     {
    	 List<SelectItem> allowedAssetClassStatuses = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.ASSET_CLASS_STATUS);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(pageMode.equals(WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP) && ( !(dd.getDataValue().equals(WebConstants.ASSET_CLASS_STATUS_ACTIVE)))  )
    			   isAdd = false;
    			 if(pageMode.equals(WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP) && ( !(dd.getDataValue().equals(WebConstants.ASSET_CLASS_STATUS_ACTIVE)))  )
      			   isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedAssetClassStatuses.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedAssetClassStatuses!= null && allowedAssetClassStatuses.size()>1)
			 Collections.sort(allowedAssetClassStatuses,ListComparator.LIST_COMPARE);
		
    	 viewRootMap.put("ALLOWED_ASSET_CLASS_STATUSES",allowedAssetClassStatuses);
     }

	public HtmlSelectOneMenu getHtmlSelectOneAssetClassStatus() {
		return htmlSelectOneAssetClassStatus;
	}

	public void setHtmlSelectOneAssetClassStatus(
			HtmlSelectOneMenu htmlSelectOneAssetClassStatus) {
		this.htmlSelectOneAssetClassStatus = htmlSelectOneAssetClassStatus;
	}

	public List<Long> getAlreadySelectedAssetClassList() {
		return alreadySelectedAssetClassList;
	}

	public void setAlreadySelectedAssetClassList(
			List<Long> alreadySelectedAssetClassList) {
		this.alreadySelectedAssetClassList = alreadySelectedAssetClassList;
	}

	public List<SelectItem> getAssetClassStatusList() {
		List<SelectItem> allowedAssetClassStatuses = new ArrayList<SelectItem>(); 	 
   	    if(viewRootMap.containsKey("ALLOWED_ASSET_CLASS_STATUSES"))
   	    	allowedAssetClassStatuses = (List<SelectItem>) viewRootMap.get("ALLOWED_ASSET_CLASS_STATUSES");
		return allowedAssetClassStatuses;
	}

	public void setAssetClassStatusList(List<SelectItem> assetClassStatusList) {
		this.assetClassStatusList = assetClassStatusList;
	}
}