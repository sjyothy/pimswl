package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class BidderAuctionRefund extends AbstractController {

	// Fields
	private HtmlDataTable auctionUnitTable = new HtmlDataTable();
	private List<AuctionUnitView> auctionUnitList = new ArrayList();
	private List<String> successMessages = new ArrayList<String>(0);
	private Logger logger = Logger.getLogger(BidderAuctionRefund.class);
	private HtmlInputText inputTextConfiscatedAmount = new HtmlInputText();
	
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	// Constructor
	/** default constructor */
	public BidderAuctionRefund() {
	}
	private void loadAuctionUnitList(){
		String  methodName = "loadAUctionUnitList";

		try{

			BidderAuctionRegView bidderAuctionRegView = getBidderAuctionRegView();
			AuctionView auctionView = new AuctionView();
			auctionView.setAuctionId(bidderAuctionRegView.getAuctionId());
			BidderView bidderView = new BidderView();
			bidderView.setBidderId(bidderAuctionRegView.getBidderId());
			RequestView requestView = new RequestView();
			List<RequestView> listRequestView = new ArrayList<RequestView>();
			//requestView.setBidderView(bidderView);
			requestView.setAuctionView(auctionView);
			requestView.setAuctionId(bidderAuctionRegView.getAuctionId());
			AuctionUnitView auctionUnitView;
			listRequestView = propertyServiceAgent.getAuctionUnits(requestView);
			Iterator iteratorRequest = listRequestView.iterator();
			Iterator iteratorRequestDetail;
			RequestDetailView requestDetailView;
			while(iteratorRequest.hasNext()){

				requestView = (RequestView)iteratorRequest.next();
			
				iteratorRequestDetail= requestView.getRequestDetailView().iterator();
				while(iteratorRequestDetail.hasNext()){
					requestDetailView = (RequestDetailView)iteratorRequestDetail.next();
					auctionUnitView = new AuctionUnitView();
					auctionUnitList.add(requestDetailView.getAuctionUnitView());
				}

			}
		}
		catch(Exception exception){
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();




		}

	}
	public BidderAuctionRegView getBidderAuctionRegView() {
		BidderAuctionRegView bidderAuctionRegView = null;
		Object obj = getFacesContext()
				.getExternalContext()
				.getSessionMap()
				.get(
						WebConstants.AuctionDepositRefundApprovalScreen.SELECTED_BIDDER_AUCTION_REG);
		if (obj != null)
			bidderAuctionRegView = (BidderAuctionRegView) obj;

		return bidderAuctionRegView;
	}

	// Methods
	private void loadData() {
		String methodName = "loadData";
		logger.logInfo(methodName + " started...");

		try {
				
			BidderAuctionRegView bidderAuctionRegView = getBidderAuctionRegView();
			if(bidderAuctionRegView !=null)
				inputTextConfiscatedAmount.setValue(bidderAuctionRegView.getConfiscatedAmount());
			
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed ...", exception);
			exception.printStackTrace();
		}
		logger.logInfo(methodName + " ended...");
	}

	// Getters and Setters

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub

		if (!isPostBack())
			loadData();
		loadAuctionUnitList();

		super.init();
	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}	

	public HtmlInputText getInputTextConfiscatedAmount() {
		return inputTextConfiscatedAmount;
	}

	public void setInputTextConfiscatedAmount(
			HtmlInputText inputTextConfiscatedAmount) {
		this.inputTextConfiscatedAmount = inputTextConfiscatedAmount;
	}

	public void saveAuctionRequest(javax.faces.event.ActionEvent event) {

		BidderAuctionRegView bidderAuctionRegView = getBidderAuctionRegView();

		if(bidderAuctionRegView !=null && inputTextConfiscatedAmount.getValue() != null )
		 {
			
			bidderAuctionRegView.setConfiscatedAmount(Double.parseDouble((String)inputTextConfiscatedAmount.getValue()));
//			ArrayList<BidderAuctionRegView> bidderAuctionRegViewList = new ArrayList<BidderAuctionRegView>(0);
//			bidderAuctionRegViewList.add(bidderAuctionRegView);
//			PropertyServiceAgent psAgent = new PropertyServiceAgent();
//			try {
//				psAgent.updateBidderAuctionRegs(bidderAuctionRegViewList);
//			} catch (PimsBusinessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.AuctionRefund.MSG_REQ_CONFISCATED_AMOUNT));	
		 }
		else{
			final String viewId = "/BidderAuctionRefund.jsp";
				    
			 FacesContext facesContext = FacesContext.getCurrentInstance();
			
			 	

			    // This is the proper way to get the view's url
			    ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
			    String actionUrl = viewHandler.getActionURL(facesContext, viewId);
			    
			    String javaScriptText = "javascript:closeWindowSubmit();";
			    
			    // Add the Javascript to the rendered page's header for immediate execution
			    AddResource addResource = AddResourceFactory.getInstance(facesContext);
			    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		
			successMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.SUCCESS_APPROVE));
		}
	}
	
	public Integer getRecordSize(){
		if(auctionUnitList != null){
			return auctionUnitList.size(); 
		}
		else{
			return 0;
		}
	}
	
	public List<AuctionUnitView> getAuctionUnitList() {
		return auctionUnitList;
	}
	public void setAuctionUnitList(List<AuctionUnitView> auctionUnitList) {
		this.auctionUnitList = auctionUnitList;
	}
	public HtmlDataTable getAuctionUnitTable() {
		return auctionUnitTable;
	}
	public void setAuctionUnitTable(HtmlDataTable auctionUnitTable) {
		this.auctionUnitTable = auctionUnitTable;
	}


}
