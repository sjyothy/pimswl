package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;


public class MaintenanceApplicationSearch extends AbstractController
{
	private transient Logger logger = Logger.getLogger(MaintenanceApplicationSearch.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	
	private Map<String,Long> requestStatusMap = new HashMap<String,Long>();
	private Map<String,Long> requestTypeMap = new HashMap<String,Long>();
	
	private RequestView requestView = new RequestView();
	private Date toDate = null;
	private Date fromDate = null;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private String requestTypeId = "";
	private String statusId = "";
	
	private RequestView dataItem = new RequestView();
	private List<RequestView> dataList = new ArrayList<RequestView>();
	
	private HtmlSelectOneMenu requestTypeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu requestStatusCombo = new HtmlSelectOneMenu();
	
	List<RequestTypeView> requestTypeList = new ArrayList<RequestTypeView>();
	private List<SelectItem> requestStatuses = new ArrayList<SelectItem>();
	private List<SelectItem> requestTypes = new ArrayList<SelectItem>();
	
	private List<SelectItem> countries = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>();
	private List<SelectItem> cities = new ArrayList<SelectItem>();
	
	private Map<Long,RegionView> countryMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> stateMap = new HashMap<Long,RegionView>();
	
	private List<String> errorMessages;
	private String infoMessage = "";
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private Boolean sortAscending = true;

	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	
	
	// hashMap field From Shiraz 
	
	
	private String unitRefNo;
	private String propertyRefNo;
	private String selectOneTenantType;
	private String selectOneContractType;
	private String selectOneState;
	private String selectOneCity;
	private String selectOnePropertyUsage;


	public MaintenanceApplicationSearch(){
		
	}
	

	/*
	 * Methods
	 */
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = "";
		if(user!=null)
			loginId = user.getLoginId();
		return loginId;
	}
	
	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			sessionMap.remove(WebConstants.REQUEST_TYPE_LIST);
			sessionMap.remove(WebConstants.REQUEST_TYPE_MAP);
			sessionMap.remove(WebConstants.REQUEST_STATUS_MAP);
			sessionMap.remove(WebConstants.REQUEST_TYPE_COMBO_EN);
			sessionMap.remove(WebConstants.REQUEST_TYPE_COMBO_AR);
			
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
	
	public String gotoAddRequest()
	{
		String methodName="gotoAddRequest";
		logger.logInfo(methodName+"|"+"Start..");

		
		
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "addMaintenenaceRequest";
	}
	
	@SuppressWarnings("unchecked")
	public String approvalRequiredAction(){
		String METHOD_NAME = "approvalRequiredAction()";
		logger.logInfo(METHOD_NAME + " started...");
		String taskType = "";
		try{
			
			clearSessionMap();
	   		RequestView reqView=(RequestView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		/*WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	   		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);*/

			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return taskType;
//		return "home";
	}
	
	public String detailsAction(){
		String METHOD_NAME = "detailsAction()";
		String requestTypeId = null;
		String taskType = "";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			RequestView reqView=(RequestView)dataTable.getRowData();
			if( reqView.getRequestTypeId() != null ){
				requestTypeId = reqView.getRequestTypeId().toString().trim();
				if(     requestTypeId.equals(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_REPLACE_CHEQUE.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_LEASE_CONTRACT.toString())||
						requestTypeId.equals(WebConstants.REQUEST_TYPE_RENEW_CONTRACT.toString())
						)

					FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW, reqView);
				else
					ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW, reqView);
			}
			if(requestTypeId.equals(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString()))
			    setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
			else if(requestTypeId.equals(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT.toString()))
			{
				setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
			   setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
			}

			else if (requestTypeId.equals(WebConstants.REQUEST_TYPE_TRANSFER_CONTRACT.toString())
					||requestTypeId.equals(WebConstants.REQUEST_TYPE_REPLACE_CHEQUE.toString())
					||requestTypeId.equals(WebConstants.NOL.REQUEST_TYPE_CLEARANCE_LETTER_ID.toString())
					||requestTypeId.equals(WebConstants.REQUEST_TYPE_RENEW_CONTRACT.toString())
					||requestTypeId.equals(WebConstants.NOL.REQUEST_TYPE_NOL_ID.toString())
					||requestTypeId.equals(Constant.REQUEST_TYPE_MAINTENANCE_REQUEST.toString()))
			{
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_REQUEST_SEARCH);
				
				Long requestId = reqView.getRequestId();
		   		//Long requestStatusId = reqView.getStatusId();
		   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
		   		String taskId = reqTaskView.getTaskId();
		   		String user = getLoggedInUser();
		   		
		   		if(taskId!=null)
		   		{
			   		BPMWorklistClient bpmWorkListClient = null;
			        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
					bpmWorkListClient = new BPMWorklistClient(contextPath);
					
					UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
					taskType = userTask.getTaskType();
					getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
					logger.logInfo("Task Type is: " + taskType);
					return taskType;
		   		}
		   		else
		   		{
		   			errorMessages = new ArrayList<String>();
					errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
					requestTypeId = "";
					FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW, reqView);

		   		}
				
			}
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		if( requestTypeId == null )
			return "";
		else
			return requestTypeId;
	}

	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	@SuppressWarnings("unchecked")
	public String technicalCommentsRequiredAction(){
		String METHOD_NAME = "technicalCommentsRequiredAction()";
		logger.logInfo(METHOD_NAME + " started...");
		String taskType = "";
		try{
			
			clearSessionMap();
	   		RequestView reqView=(RequestView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		//Long requestStatusId = reqView.getStatusId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is: " + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		/*WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	   		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);*/

			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return taskType;
//		return "home";
	}
	
	private void loadRequestList() throws PimsBusinessException{
		logger.logInfo("loadRequestList() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		isEnglishLocale = getIsEnglishLocale();
		HashMap requestMap = new HashMap ();
    	
    	try{
    		 loadRequestStatusMap();
			 dataList = new ArrayList<RequestView>();
			 /*HttpServletRequest tmp = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			 tmp.getAttribute("pageNumber");
			 tmp.getParameter("pageNumber");*/
//			 Map paramMap = (Map)FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderValuesMap();
//			 Map reqMap = (Map)FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
//			 System.out.println("paramMap " + paramMap.get("pageNumber"));
//			 System.out.println("reqMap " + reqMap.get("pageNumber"));
			 
			 DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			 String toDateString = "";
			 String fromDateString = "";
			 if(toDate!=null)
				toDateString = formatter.format(toDate);
			 if(fromDate!=null)
				 fromDateString = formatter.format(fromDate);
			 
			 if (selectOneContractType!=null && !selectOneContractType.equals("")&& !selectOneContractType.equals("0")){
			    requestMap.put("ContractType",new Long(selectOneContractType));	
			 }
			 if (selectOneTenantType!=null && !selectOneTenantType.equals("")&& !selectOneTenantType.equals("0")){
				    requestMap.put("TenantType",new Long(selectOneTenantType));	
			 }
			 if (selectOneState!=null && !selectOneState.equals("")&& !selectOneState.equals("0")){
				    requestMap.put("State",new Long(selectOneState));	
			}
			 if (selectOneCity!=null && !selectOneCity.equals("")&& !selectOneCity.equals("0")){
				    requestMap.put("City",new Long(selectOneCity));	
			}
			 if (selectOnePropertyUsage!=null && !selectOnePropertyUsage.equals("")&& !selectOnePropertyUsage.equals("0")){
				    requestMap.put("PropertyUsage",new Long(selectOnePropertyUsage));	
			}
			 if (propertyRefNo!=null && !propertyRefNo.equals("")){
				    requestMap.put("PropertyRefNo",propertyRefNo);	
			}
			 if (unitRefNo!=null && !unitRefNo.equals("")){
				    requestMap.put("UnitRefNo",unitRefNo);	
			}
			 
			List<RequestView> requestViewList = propertyServiceAgent.getAllRequests(requestView, fromDateString, toDateString,requestMap);
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("requestList", requestViewList);
			recordSize = requestViewList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			Long requestStatusId = null;
			if(!requestViewList.isEmpty())
			{
				for(RequestView temp: requestViewList){
					if(temp.getRequestDate()!=null)
					{
						String dateStr = formatter.format(temp.getRequestDate());
						temp.setRequestDateString(dateStr);
					}
					if(isEnglishLocale)
					{
						temp.setRequestType(temp.getRequestTypeEn());
						temp.setStatus(temp.getStatusEn());
					}
					else{
						temp.setRequestType(temp.getRequestTypeAr());
						temp.setStatus(temp.getStatusAr());
					}
	//				temp = auctionList.get(i);
					Boolean set = false;
					requestStatusId = temp.getStatusId();
					if(requestStatusId.compareTo(requestStatusMap.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED))==0)
					{
						temp.setApprovalRequiredMode(true);
						temp.setTechnicalCommentsRequiredMode(false);
						set = true;
					}
					else if(requestStatusId.compareTo(requestStatusMap.get(WebConstants.REQUEST_STATUS_TECHNICAL_COMMENTS_REQUIRED))==0){
						temp.setApprovalRequiredMode(false);
						temp.setTechnicalCommentsRequiredMode(true);
						set = true;
					}
					else{
						temp.setApprovalRequiredMode(false);
						temp.setTechnicalCommentsRequiredMode(false);
						set = true;
					}
					/*else if(requestStatusId.compareTo(requestStatusMap.get(WebConstants.Statuses.AUCTION_DRAFT_STATUS))==0)
					{
						temp.setApprovalRequiredMode(false);
						temp.setTechnicalRecommendationRequiredMode(true);
						set = true;
					}*/
/*
					if(!set)
						temp.setRequestViewMode(true);*/
					
					dataList.add(temp);
				}
			}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadRequestList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadRequestList() crashed ",exception);
			throw exception;
		}
		catch(Exception exception){
			logger.LogException("loadRequestList() crashed ",exception);
		}
	}	
	
	
	private void setValues(){
		String METHOD_NAME = "setValues()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			requestView.setRequestTypeId(Convert.toLong(requestTypeId));
			requestView.setStatusId(Convert.toLong(statusId));
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}
	}
	
	public Boolean validateFields() {
		String METHOD_NAME = "setValues()";
		logger.logInfo(METHOD_NAME + " started...");
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		try{

			
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}

		return validated;
	}
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@Override
	public void init() {
		String METHOD_NAME = "init()";
		logger.logInfo(METHOD_NAME + " started...");
		super.init();
		try
		{
			loadCountries();
			loadStateMap();
			
			if(!isPostBack())
			{
				clearSessionMap();
//				loadRequestTypeCombo();
			}
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}
		
		requestTypeId = "14";
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}
	
	
	private void loadRequestTypeList(){
		String METHOD_NAME = "loadRequestTypeList()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(sessionMap.containsKey(WebConstants.REQUEST_TYPE_LIST))
				requestTypeList = (List<RequestTypeView>)sessionMap.get(WebConstants.REQUEST_TYPE_LIST);
			else{
				PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
				requestTypeList = propertyServiceAgent.getAllRequestTypes();
				sessionMap.put(WebConstants.REQUEST_TYPE_LIST,requestTypeList);
			}
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException(METHOD_NAME + " crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException(METHOD_NAME + " crashed ",exception);
    	}
	}
	
	private void loadRequestTypeMap(){
		String METHOD_NAME = "loadRequestTypeList()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			if((requestTypeList.size()==0) || (requestTypeList==null))
				loadRequestTypeList();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(sessionMap.containsKey(WebConstants.REQUEST_TYPE_MAP)){
				requestTypeMap = (HashMap<String,Long>)sessionMap.get(WebConstants.REQUEST_TYPE_MAP);
			}
			else
			{
				for(RequestTypeView reqTypeView: requestTypeList)
					requestTypeMap.put(reqTypeView.getDescription(),reqTypeView.getRequestTypeId());
				sessionMap.put(WebConstants.REQUEST_TYPE_MAP,requestTypeMap);
			}
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException(METHOD_NAME + " crashed ",exception);
    	}
	}
	

	@SuppressWarnings("unchecked")
	public void loadRequestStatusMap() {
		logger.logInfo("loadRequestStatusMap() started...");
		try{
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(sessionMap.containsKey(WebConstants.REQUEST_STATUS_MAP)){
				requestStatusMap = (HashMap<String,Long>)sessionMap.get(WebConstants.REQUEST_STATUS_MAP);
			}
			else
			{
				List<DomainDataView> requestStatusList = propertyServiceAgent.getDomainDataByDomainTypeName(WebConstants.REQUEST_STATUS);
				if(requestStatusList!=null) {
					for (DomainDataView domainDataView : requestStatusList) {
						requestStatusMap.put(domainDataView.getDataValue(), domainDataView.getDomainDataId());
					}
					sessionMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
				}
			}
			logger.logInfo("loadRequestStatusMap() completed successfully!!!");
		}
    	catch(PimsBusinessException exception){
    		logger.LogException("loadRequestStatusMap() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("loadRequestStatusMap() crashed ",exception);
    	}
	}
	
	private void loadRequestTypeCombo(){
		String METHOD_NAME = "loadRequestTypeList()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			if((requestTypeList.size()==0) || (requestTypeList==null))
				loadRequestTypeList();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(getIsEnglishLocale())
			{
				if(sessionMap.containsKey(WebConstants.REQUEST_TYPE_COMBO_EN)){
					requestTypes = (List<SelectItem>)sessionMap.get(WebConstants.REQUEST_TYPE_COMBO_EN);
				}
				else
				{
					for(RequestTypeView reqTypeView: requestTypeList)
					{
						SelectItem item = null;
						item = new SelectItem(reqTypeView.getRequestTypeId().toString(),reqTypeView.getRequestTypeEn());
					    requestTypes.add(item);
					}
					sessionMap.put(WebConstants.REQUEST_TYPE_COMBO_EN,requestTypes);
				}
			}
			else{
				if(sessionMap.containsKey(WebConstants.REQUEST_TYPE_COMBO_AR)){
					requestTypes = (List<SelectItem>)sessionMap.get(WebConstants.REQUEST_TYPE_COMBO_AR);
				}
				else
				{
					for(RequestTypeView reqTypeView: requestTypeList)
					{
						SelectItem item = null;
						item = new SelectItem(reqTypeView.getRequestTypeId().toString(),reqTypeView.getRequestTypeAr());
					    requestTypes.add(item);
					}
					sessionMap.put(WebConstants.REQUEST_TYPE_COMBO_AR,requestTypes);
				}
			}
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException(METHOD_NAME + " crashed ",exception);
    	}
	}
	
	/*
	 * Action Handlers 
	 */
	 
	public String mode1()
    {
    	logger.logInfo("mode1() started...");
    	try{
    		 /*clearSessionMap();
    		 RequestView reqView=(RequestView)dataTable.getRowData();
    		 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    		 sessionMap.put(WebConstants.AUCTION_ID,reqView.getRequestId());
    		 sessionMap.put(WebConstants.VIEW_MODE,WebConstants.EDIT_MODE);
    		 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);*/
    		logger.logInfo("mode1() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("mode1() crashed ",exception);
    	}
        return "mode1";
    }
	
	public void clear(javax.faces.event.ActionEvent event){
    	logger.logInfo("clear() started...");
    	try{
    		fromDate=null;
    		toDate=null;
    		requestView.getTenantsView().setFirstName("");
    		requestView.getContractView().setContractNumber("");
    		/*requestTypeCombo.resetValue();
    		requestStatusCombo.resetValue();*/
    		logger.logInfo("clear() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("clear() crashed ",exception);
    	}
    }
	
		
    public String searchRequests()
    {
    	logger.logInfo("searchRequests() started...");
    	try{
    		if(validateFields())
    		{
    			setValues();
    			loadRequestList();
    		}
    			
    		logger.logInfo("searchRequests() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
        return "searchRequests";
    }
	
    
    public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        clearSessionMap();
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		        logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }
    
    	
	/*
	 * Setters / Getters
	 */
    
	/**
	 * @return the requestView
	 */
	public RequestView getRequestView() {
		return requestView;
	}

	/**
	 * @param requestView the requestView to set
	 */
	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	/**
	 * @return the dataItem
	 */
	public RequestView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(RequestView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<RequestView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<RequestView>)viewMap.get("requestList");
		if(dataList==null)
			dataList = new ArrayList<RequestView>();
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<RequestView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}



	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the requestStatuses
	 */
	public List<SelectItem> getRequestStatuses() {
		return requestStatuses;
	}

	/**
	 * @param requestStatuses the requestStatuses to set
	 */
	public void setRequestStatuses(List<SelectItem> requestStatuses) {
		this.requestStatuses = requestStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the requestStatusMap
	 */
	public Map<String, Long> getRequestStatusMap() {
		return requestStatusMap;
	}

	/**
	 * @param requestStatusMap the requestStatusMap to set
	 */
	public void setRequestStatusMap(Map<String, Long> requestStatusMap) {
		this.requestStatusMap = requestStatusMap;
	}

	/**
	 * @return the requestTypeMap
	 */
	public Map<String, Long> getRequestTypeMap() {
		return requestTypeMap;
	}

	/**
	 * @param requestTypeMap the requestTypeMap to set
	 */
	public void setRequestTypeMap(Map<String, Long> requestTypeMap) {
		this.requestTypeMap = requestTypeMap;
	}

	
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}


	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	/**
	 * @return the requestTypes
	 */
	public List<SelectItem> getRequestTypes() {
		loadRequestTypeCombo();
		return requestTypes;
	}

	/**
	 * @param requestTypes the requestTypes to set
	 */
	public void setRequestTypes(List<SelectItem> requestTypes) {
		this.requestTypes = requestTypes;
	}

	/**
	 * @return the requestTypeId
	 */
	public String getRequestTypeId() {
		return requestTypeId;
	}

	/**
	 * @param requestTypeId the requestTypeId to set
	 */
	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the requestTypeList
	 */
	public List<RequestTypeView> getRequestTypeList() {
		return requestTypeList;
	}

	/**
	 * @param requestTypeList the requestTypeList to set
	 */
	public void setRequestTypeList(List<RequestTypeView> requestTypeList) {
		this.requestTypeList = requestTypeList;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message;;
			}
			messageList = messageList + "</UL>";
		}
		return (messageList);

	}
	
	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}


	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}


	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}


	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}


	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	/**
	 * @return the requestTypeCombo
	 */
	public HtmlSelectOneMenu getRequestTypeCombo() {
		return requestTypeCombo;
	}


	/**
	 * @param requestTypeCombo the requestTypeCombo to set
	 */
	public void setRequestTypeCombo(HtmlSelectOneMenu requestTypeCombo) {
		this.requestTypeCombo = requestTypeCombo;
	}


	/**
	 * @return the requestStatusCombo
	 */
	public HtmlSelectOneMenu getRequestStatusCombo() {
		return requestStatusCombo;
	}


	/**
	 * @param requestStatusCombo the requestStatusCombo to set
	 */
	public void setRequestStatusCombo(HtmlSelectOneMenu requestStatusCombo) {
		this.requestStatusCombo = requestStatusCombo;
	}


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}


	public String getUnitRefNo() {
		return unitRefNo;
	}


	public void setUnitRefNo(String unitRefNo) {
		this.unitRefNo = unitRefNo;
	}


	public String getPropertyRefNo() {
		return propertyRefNo;
	}


	public void setPropertyRefNo(String propertyRefNo) {
		this.propertyRefNo = propertyRefNo;
	}


	public String getSelectOneTenantType() {
		return selectOneTenantType;
	}


	public void setSelectOneTenantType(String selectOneTenantType) {
		this.selectOneTenantType = selectOneTenantType;
	}


	public String getSelectOneContractType() {
		return selectOneContractType;
	}


	public void setSelectOneContractType(String selectOneContractType) {
		this.selectOneContractType = selectOneContractType;
	}


	public String getSelectOneState() {
		return selectOneState;
	}


	public void setSelectOneState(String selectOneState) {
		this.selectOneState = selectOneState;
	}


	public String getSelectOneCity() {
		return selectOneCity;
	}


	public void setSelectOneCity(String selectOneCity) {
		this.selectOneCity = selectOneCity;
	}


	public String getSelectOnePropertyUsage() {
		return selectOnePropertyUsage;
	}


	public void setSelectOnePropertyUsage(String selectOnePropertyUsage) {
		this.selectOnePropertyUsage = selectOnePropertyUsage;
	}


	public List<SelectItem> getCountries() {
		return countries;
	}


	public void setCountries(List<SelectItem> countries) {
		this.countries = countries;
	}


	public List<SelectItem> getStates() {
		
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		states = (List<SelectItem>) sessionMap.get("states");
		if(states==null)
			states = new ArrayList<SelectItem>();
		return states;
	}


	public void setStates(List<SelectItem> states) {
		this.states = states;
	}


	public Map<Long, RegionView> getCountryMap() {
		return countryMap;
	}


	public void setCountryMap(Map<Long, RegionView> countryMap) {
		this.countryMap = countryMap;
	}


	public Map<Long, RegionView> getStateMap() {
		
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		stateMap = (Map<Long, RegionView>) sessionMap.get("stateMap");
		if(stateMap==null)
			stateMap = new HashMap<Long, RegionView>();
		return stateMap;
	}


	public void setStateMap(Map<Long, RegionView> stateMap) {
		this.stateMap = stateMap;
	}

	private void loadCountries() {
		try {
			logger.logInfo("loadCountries() started...");
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			countries = new ArrayList();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			if(sessionMap.containsKey("countryMap"))
			{
				countryMap = (HashMap<Long, RegionView>) sessionMap.get("countryMap");
			}
			else{
				countryMap = new HashMap<Long, RegionView>();
				for(RegionView regView :regionViewList)
				{
					countryMap.put(regView.getRegionId(), regView);
				}
				sessionMap.put("countryMap",countryMap);
			}
			if(sessionMap.containsKey("countries"))
			{
				countries = (List<SelectItem>) sessionMap.get("countries");
			}
			else{	 
				for(RegionView regionView :regionViewList)
				{
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
				  countries.add(item);
				}
				sessionMap.put("countries",countries);
			}
			logger.logInfo("loadCountries() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadCountries() crashed ",exception);
    	}
		
		

//		for (int i = 0; i < countries.size(); i++) {
//			getPropertyTypeSelectMenu().setValue(new SelectItem(countries.get(i)));
//		}
	}
	
	public void loadStateMap(){
		try {
			logger.logInfo("loadStateMap() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			stateMap = new HashMap<Long, RegionView>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> statesList = psa.getAllStates();
			for(RegionView regView :statesList)
			{
				stateMap.put(regView.getRegionId(), regView);
			}
			sessionMap.put("stateMap",stateMap);
			logger.logInfo("loadStateMap() completed successfully!!!");
		}catch(Exception exception){
			logger.LogException("loadStateMap() crashed ",exception);
		}
	}
	
	public void loadStates(ValueChangeEvent vce)  {
		try {
			logger.logInfo("loadStates() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			states = new ArrayList<SelectItem>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			String regionName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)countryMap.get(regionId);
			if(temp!=null)
			{
				regionName = temp.getRegionName();
				List <RegionView> statesList = psa.getCountryStates(regionName,getIsArabicLocale());
				
				for(RegionView stateView:statesList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
					else 
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
					states.add(item);
				}
				sessionMap.put("states",states);
			}
			else{
				sessionMap.remove("states");
			}
				
//			loadCities(vce);
			logger.logInfo("loadStates() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadStates() crashed ",exception);
    	}
	}
	
	public void loadCities(ValueChangeEvent vce)  {
		try {
			logger.logInfo("loadCities() started...");
			cities = new ArrayList<SelectItem>();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			String selectedStateName = "";
			Long regionId = Convert.toLong(vce.getNewValue());
			RegionView temp = (RegionView)stateMap.get(regionId);
			if(temp!=null)
			{
				selectedStateName = temp.getRegionName();
				List <RegionView> cityList = psa.getCountryStates(selectedStateName,getIsArabicLocale());
			
			
				for(RegionView cityView:cityList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionEn());			  }
					else 
						item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionAr());	  
					cities.add(item);
				}
			}
			logger.logInfo("loadCities() completed successfully!!!");
		}catch(Exception exception){
    		logger.LogException("loadCities() crashed ",exception);
    	}
	}


}

