package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.evaluation.PropertyEvaluationDetailsTabBacking;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.EvaluationByUnitTypeView;

public class EvaluationUnitValuePopupBean extends AbstractController 
{
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private transient Logger logger = Logger.getLogger(EvaluationUnitValuePopupBean.class);
	private List<String> errorMessages = new ArrayList<String>();
	private String unitType;
	private int noOfUnits;
	private Double newRentAmount;
	private EvaluationByUnitTypeView eUnit=new EvaluationByUnitTypeView();
	public void init()
	{
		if(!isPostBack())
		{
		if(sessionMap.containsKey("SELECTED_UNIT_TYPE")&& sessionMap.get("SELECTED_UNIT_TYPE")!=null)
			{
				eUnit=(EvaluationByUnitTypeView) sessionMap.get("SELECTED_UNIT_TYPE");
				sessionMap.remove("SELECTED_UNIT_TYPE");
				viewMap.put("SELECTED_UNIT_TYPE", eUnit);
			}
		}
	}
	public EvaluationByUnitTypeView getEUnit()
	{
		if(viewMap.containsKey("SELECTED_UNIT_TYPE"))
			eUnit=(EvaluationByUnitTypeView) viewMap.get("SELECTED_UNIT_TYPE");
		return eUnit;
	}
	public void setEUnit(EvaluationByUnitTypeView unit) {
		eUnit = unit;
	}
	public String getUnitType() 
	{
		if(getEUnit()!=null)
			unitType=getEUnit().getUnitType();
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public int getNoOfUnits() 
	{
		if(getEUnit()!=null)
			noOfUnits=getEUnit().getNoOfUnits();
		return noOfUnits;
	}
	public void setNoOfUnits(int noOfUnits) 
	{
		this.noOfUnits = noOfUnits;
	}
	public Double getNewRentAmount() 
	{
		if(viewMap.containsKey("NEW_RENT"))
			newRentAmount=(Double) viewMap.get("NEW_RENT");
		else if(getEUnit()!=null)
			newRentAmount=getEUnit().getNewRentValue();
		return newRentAmount;
	}
	public void setNewRentAmount(Double newRentAmount)
	{
		viewMap.put("NEW_RENT", newRentAmount);
		this.newRentAmount = newRentAmount;
	}
	public void sendNewAmount()
	{
		if(viewMap.containsKey("NEW_RENT"))
		{
			if(((Double)viewMap.get("NEW_RENT")).compareTo(0D)<=0)
			{
				errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage("property.evaluationValueZero"));
	        	setNewRentAmount(0D);
	        	getNewRentAmount();
	        	return;
			}
			else
			{
				getEUnit().setNewRentValue((Double) viewMap.get("NEW_RENT"));
				sessionMap.put("UNIT_WITH_NEW_RENT",eUnit);
			}
		}
		executeJavascript("sendNewRentAmount();");
	}
	public void executeJavascript(String javascript) {
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);		
		} 
		catch (Exception exception) 
		{			
			logger.LogException(" executing JavaScript CRASHED --- ", exception);
		}
	}
	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
}
