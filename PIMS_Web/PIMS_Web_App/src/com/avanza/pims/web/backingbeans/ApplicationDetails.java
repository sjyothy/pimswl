package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.PersonView;



public class ApplicationDetails extends AbstractController
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(ApplicationDetails.class);
	private javax.faces.component.html.HtmlSelectOneMenu cmbAllowedApplicants = new HtmlSelectOneMenu();
	private String applicationNo;
	private HtmlInputText txtApplicationNo;
	private String status;
	private HtmlInputText txtApplicationStatus;
	private HtmlInputText txtApplicationDate;
	private Date appDate;
	private HtmlInputTextarea txtDescription;
	private String description;
	private HtmlInputText txtName = new HtmlInputText();
	private String name;
	private String selectOneApplicantType;
	private HtmlInputText txtCellNo;
	private String cellNo;
	private HtmlInputText txtEmail;
	private String email;
	private HtmlInputText txtApplicantType;
	private String applicantType;
	private boolean isArabicLocale;	
    private boolean isEnglishLocale;
    String selectBlackList ;
	private String pageMode;
	List<SelectItem> memsAllowedApplicantsList = new ArrayList<SelectItem>(); 
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap=context.getViewRoot().getAttributes();
	 @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
			sessionMap=context.getExternalContext().getSessionMap();
			cmbAllowedApplicants.setRendered(false);
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
     
        
	public String getSelectBlackListKey()
	{
		 return WebConstants.SELECT_BLACK_LIST;
	}
	
	@SuppressWarnings("unchecked")
 	public  void populateApplicationDetails(
 											 String applicationNo,String status,Date appDate,String description,
 											 String name,String applicantType,String cellNo,String email
 											)
 	{
 	  this.setApplicationNo(applicationNo);	
 	  this.setStatus(status);
 	  this.setAppDate(appDate);
 	  this.setDescription(description);
 	  this.setSelectOneApplicantType(applicantType);
 	  this.setCellNo(cellNo);
 	  this.setEmail(email);
 	  this.setName(name);
 	  	  
 	}
 	
 	@SuppressWarnings("unchecked")
 	public  void getApplicatoinDetails(String applicationNo,String status,Date appDate,String description,
 			String name,String applicantType,String cellNo,String email)
 	{
 	  this.setApplicationNo(applicationNo);	
 	  this.setStatus(status);
 	  this.setAppDate(appDate);
 	  this.setDescription(description);
 	  this.setSelectOneApplicantType(applicantType);
 	  this.setCellNo(cellNo);
 	  this.setEmail(email);
 	  	  
 	}
	private List<String> errorMessages;
		public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

		public String getApplicationNo() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_NUMBER)  )
				applicationNo=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_NUMBER).toString();
				
			return applicationNo;
		}
		public String getApplicationId() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID)  )
				return viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID).toString();
			else	
			return "";
		}
		public boolean getIsViewPersonImgShow()
		{
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID)  )
				return true;
			else 
				return false;
		}
		
		@SuppressWarnings("unchecked")
		public void setApplicationNo(String applicationNo) {
			this.applicationNo = applicationNo;
			if(this.applicationNo!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, this.applicationNo);
		}

		public HtmlInputText getTxtApplicationNo() {
			return txtApplicationNo;
		}

		public void setTxtApplicationNo(HtmlInputText txtApplicationNo) {
			this.txtApplicationNo = txtApplicationNo;
		}

		public String getStatus() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_STATUS)  )
				status=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_STATUS).toString();
			
			return status;
		}

		@SuppressWarnings("unchecked")
		public void setStatus(String status) {
			this.status = status;
			if(this.status !=null  )
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,this.status);
		}

		public HtmlInputText getTxtApplicationStatus() {
			return txtApplicationStatus;
		}

		public void setTxtApplicationStatus(HtmlInputText txtApplicationStatus) {
			this.txtApplicationStatus = txtApplicationStatus;
		}

		public HtmlInputText getTxtApplicationDate() {
			return txtApplicationDate;
		}

		public void setTxtApplicationDate(HtmlInputText txtApplicationDate) {
			this.txtApplicationDate = txtApplicationDate;
		}
		
		@SuppressWarnings("unchecked")
		public Date getAppDate() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DATE)  )
				appDate=(Date)viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DATE);
		
			return appDate;
		}

		@SuppressWarnings("unchecked")
		public void setAppDate(Date appDate) {
			this.appDate = appDate;
			if(this.appDate!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,this.appDate);
		}

		@SuppressWarnings("unchecked")
		public String getDescription() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
				description=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString();
			return description;
		}

		@SuppressWarnings("unchecked")
		public void setDescription(String description) {
			this.description = description;
			if(this.description!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,this.description);
		}

		public HtmlInputText getTxtName() {
			return txtName;
		}

		public void setTxtName(HtmlInputText txtName) {
			this.txtName = txtName;
		}

		@SuppressWarnings("unchecked")
		public String getName() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME))
				name=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME).toString();
			return name;
		}
		
		@SuppressWarnings("unchecked")
		public void setName(String name) {
			this.name = name;
			if(this.name!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,this.name);
		}

		public String getSelectOneApplicantType() {
			return selectOneApplicantType;
		}

		public void setSelectOneApplicantType(String selectOneApplicantType) {
			this.selectOneApplicantType = selectOneApplicantType;
		}

		public HtmlInputText getTxtCellNo() {
			return txtCellNo;
		}

		public void setTxtCellNo(HtmlInputText txtCellNo) {
			this.txtCellNo = txtCellNo;
			
		}
		
		@SuppressWarnings("unchecked")
		public String getCellNo() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL))
				cellNo=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL).toString();
				return cellNo;
		}

		@SuppressWarnings("unchecked")
		public void setCellNo(String cellNo) {
			this.cellNo = cellNo;
			if(this.cellNo!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,this.cellNo);
		}

		public HtmlInputText getTxtEmail() {
			return txtEmail;
		}

		public void setTxtEmail(HtmlInputText txtEmail) {
			this.txtEmail = txtEmail;
		}

		@SuppressWarnings("unchecked")
		public String getEmail() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL))
				email=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL).toString();
			return email;
		}

		@SuppressWarnings("unchecked")
		public void setEmail(String email) {
			
			this.email = email;
			if(this.email!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL,this.email);
		}
	
		public String getDateFormat()
		{
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    	 
			return localeInfo.getDateFormat();
		}
		public TimeZone getTimeZone()
		{
			 return TimeZone.getDefault();
			
		}

		public HtmlInputText getTxtApplicantType() {
			return txtApplicantType;
		}

		public void setTxtApplicantType(HtmlInputText txtApplicantType) {
			this.txtApplicantType = txtApplicantType;
		}

		@SuppressWarnings("unchecked")
		public String getApplicantType() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE))
				applicantType=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE).toString();
			return applicantType;
		}
		
		@SuppressWarnings("unchecked")
		public void setApplicantType(String applicantType) {
			this.applicantType = applicantType;
			if(this.applicantType!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,this.applicantType);
			
		}

		@SuppressWarnings("unchecked")
		public Boolean getApplicationDetailsReadonlyMode(){
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Boolean applicationDetailsReadonlyMode = (Boolean)viewMap.get("applicationDetailsReadonlyMode");
			if(applicationDetailsReadonlyMode==null)
				applicationDetailsReadonlyMode = false;
			return applicationDetailsReadonlyMode;
		}
	
		@SuppressWarnings("unchecked")
		public String getApplicationDescriptionReadonlyMode(){
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			String applicationDetailsReadonlyMode = "APPLICANT_DETAILS_INPUT "+(String)viewMap.get("applicationDescriptionReadonlyMode");
			if(applicationDetailsReadonlyMode==null)
				applicationDetailsReadonlyMode = "APPLICANT_DETAILS_INPUT";
			return applicationDetailsReadonlyMode;
		}

		public void setSelectBlackList(String selectBlackList) {
			this.selectBlackList = selectBlackList;
		}
		
		@SuppressWarnings("unchecked")
		public String getSelectBlackList() {
			selectBlackList ="0";
	    	if(viewRootMap.containsKey(WebConstants.SELECT_BLACK_LIST ))
	    		selectBlackList ="1";
	    	return selectBlackList; 
		}

		public HtmlInputTextarea getTxtDescription() {
			return txtDescription;
		}

		public void setTxtDescription(HtmlInputTextarea txtDescription) {
			this.txtDescription = txtDescription;
		}
	
		@SuppressWarnings("unchecked")
		public void getAllowedApplicantFromInheritanceFile( Long inheritanceFileId )throws Exception
		
		{
			List<PersonView> list = UtilityService.getAllowedApplicantFromInheritanceFile(inheritanceFileId);
			List<SelectItem> itemList = new ArrayList<SelectItem>();
			for (PersonView personView : list) 
			{
			  itemList.add( 
					  		new SelectItem(  personView.getPersonId().toString(), personView.getPersonFullName() )
			              );
			  
			}
			setMemsAllowedApplicantsList(itemList);
			getShowAllowedApplicantsList();
		}

		@SuppressWarnings("unchecked")
		public boolean getShowAllowedApplicantsList()
		{
			memsAllowedApplicantsList = getMemsAllowedApplicantsList();
			if ( 
					memsAllowedApplicantsList  != null   && 
					memsAllowedApplicantsList.size() > 0 
			   )
			{
				txtName.setRendered(false);
				cmbAllowedApplicants.setRendered(true);
				return true;
			}
			else
			{
				txtName.setRendered(true);
				cmbAllowedApplicants.setRendered(false);
			}
			return false;
			
		}
		
		@SuppressWarnings("unchecked")
		public List<SelectItem> getMemsAllowedApplicantsList() 
		{
	    	if( viewRootMap.containsKey(WebConstants.ApplicationDetails.MEMS_ALLOWED_APPLICANTS) )
	    	{
	    		memsAllowedApplicantsList = (ArrayList<SelectItem>)viewRootMap.get(WebConstants.ApplicationDetails.MEMS_ALLOWED_APPLICANTS);
	    	}
	    		
			return memsAllowedApplicantsList;
		}

		@SuppressWarnings("unchecked")
		public void setMemsAllowedApplicantsList(List<SelectItem> memsAllowedApplicantsList) 
		{
			this.memsAllowedApplicantsList = memsAllowedApplicantsList;
			if( this.memsAllowedApplicantsList != null )
			{
				viewRootMap.put(WebConstants.ApplicationDetails.MEMS_ALLOWED_APPLICANTS,this.memsAllowedApplicantsList);
			}
		}


		public javax.faces.component.html.HtmlSelectOneMenu getCmbAllowedApplicants() {
			return cmbAllowedApplicants;
		}


		public void setCmbAllowedApplicants(
				javax.faces.component.html.HtmlSelectOneMenu cmbAllowedApplicants) {
			this.cmbAllowedApplicants = cmbAllowedApplicants;
		}
	
}
