package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;

public class LeaseContractsSummaryBean  extends AbstractController {

	private LeaseContractsSummaryReportCriteria reportCriteria; 
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlCommandButton btnPrintDetail = new HtmlCommandButton();
	private String dateFormat;

	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	Map viewRootMap=getFacesContext().getViewRoot().getAttributes();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	
	/** FOR TANENT DIALOG **/
	private String hdnTenantId;
	private String TENANT_INFO="TENANTINFO";
	
	@SuppressWarnings( "unchecked" )
	//Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	/** FOR TANENT DIALOG END **/
	
	public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}
	
	 public boolean getIsArabicLocale() {
		 isArabicLocale = !getIsEnglishLocale();
		 return isArabicLocale;
	 }

	 public boolean getIsEnglishLocale() {
		 
		 WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		 LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		 isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	
		 return isEnglishLocale;
	 }
	
	public LeaseContractsSummaryBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new LeaseContractsSummaryReportCriteria(ReportConstant.Report.LEASE_CONTRACTS_SUMMARY_EN, ReportConstant.Processor.LEASE_CONTRACTS_SUMMARY, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new LeaseContractsSummaryReportCriteria(ReportConstant.Report.LEASE_CONTRACTS_SUMMARY_AR, ReportConstant.Processor.LEASE_CONTRACTS_SUMMARY, CommonUtil.getLoggedInUser());
	}
	public LeaseContractsSummaryReportCriteria getReportCriteria() {
		return reportCriteria;
	}
	public void setReportCriteria(LeaseContractsSummaryReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}
	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	
	
	 private static Logger logger = Logger.getLogger("LeaseContractsSummaryBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

	public String btnPrint_Click() {	
		
    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return "";
	}
	public String btnPrintDetail_Click() {	
		
    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	reportCriteria.setReportProcessorClassName( ReportConstant.Processor.LEASE_CONTRACTS_DETAIL );
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria.setReportFileName( ReportConstant.Report.LEASE_CONTRACTS_DETAIL_EN );
		else
			reportCriteria.setReportFileName( ReportConstant.Report.LEASE_CONTRACTS_DETAIL_AR );

		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return "";
	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	
    	dateFormat= localeInfo.getDateFormat();
    	
		return dateFormat;
	}
	public HtmlCommandButton getBtnPrintDetail() {
		return btnPrintDetail;
	}
	public void setBtnPrintDetail(HtmlCommandButton btnPrintDetail) {
		this.btnPrintDetail = btnPrintDetail;
	}
	public void init(){
		this.loadCity(20L);
	}
	
	@SuppressWarnings("unchecked")
	public void  loadCity(Long stateId2){
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();

		if(viewRootMap.containsKey("cityList")){
			viewRootMap.remove("cityList");
			cityList.clear();
		}

		try 
		{

			Set<RegionView> regionViewList = null;
			if(stateId2!=null && new Long(stateId2).compareTo(new Long(-1))!=0)
				regionViewList =  psa.getCity(new Long(stateId2));
			sessionMap.put(WebConstants.SESSION_CITY,regionViewList);


			Iterator itrator = regionViewList.iterator();

			for(int i=0;i<regionViewList.size();i++)
			{
				RegionView rV=(RegionView)itrator.next();
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				else 
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				this.getCityList().add(item);


			}
			Collections.sort(cityList,ListComparator.LIST_COMPARE);
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
			logger.logInfo(methodName+"|"+"Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);

		}

	}

	
	public List<SelectItem> getCityList() {
		return cityList;
	}
	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}
	
	/** FOR TANENT DIALOG **/
	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}
	
	public void retrieveTenant()
	{
		String methodName =  " retrieveTenant";
		logger.logInfo(methodName+ " Started");
		try
		{
		FillTenantInfo();

		if(viewRootMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);

//		   String tenant_Type=getTenantType(tenantViewRow);


	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
	        contractListCriteriaBackingBean.setTenantName(tenantNames);
//	        contractListCriteriaBackingBean.setTenantType(tenant_Type);
	    }

		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed", ex);
		}

	}
	private void FillTenantInfo() throws PimsBusinessException {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

	    	}

	}
	/** FOR TANENT DIALOG END **/

}
