package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;


@SuppressWarnings("unchecked")
public class PaymentSchedulePopupController extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	private static final String VIEW_MODE 			= "viewMode";
	private static final String VIEW_MODE_POPUP 	= "popup";
	private static final String DISBURSEMENT_VIEW 	= "disbursement";
	private static final String TO_DELETE_LIST		= "toDeleteList";
	private static final String ACTIVE_LIST			= "activeList";	
	
	private static final int MONTH 		= 1;
	private static final int BI_YEAR	= 6;
	private static final int YEAR		= 12;
	private static final int QUARTER	= 3;
		
	private HtmlSelectOneMenu cmbDisburseEvery = new HtmlSelectOneMenu();
	private HtmlInputText txtInstallments = new HtmlInputText();
	private HtmlCalendar clndFirstInstallmentDate = new HtmlCalendar();
	private HtmlDataTable scheduleTable = new HtmlDataTable();	
	 

	public HtmlDataTable getScheduleTable() {
		return scheduleTable;
	}

	public void setScheduleTable(HtmlDataTable scheduleTable) {
		this.scheduleTable = scheduleTable;
	}

	@Override
	public void init() {
		super.init();
		try {
			if( !isPostBack() ) {				
				//order of statement are important 
				viewMap.put( TO_DELETE_LIST, new ArrayList<PaymentDetailsView>());
				ApplicationBean bean = new ApplicationBean();
				HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
				String viewMode = request.getParameter( VIEW_MODE);
				if( null!=viewMode && viewMode.equals( VIEW_MODE_POPUP) ) {
					viewMap.put( VIEW_MODE, VIEW_MODE_POPUP);				
				}
				DisbursementDetailsView disbursementDetails = (DisbursementDetailsView) sessionMap.remove( WebConstants.MemsNormalDisbursements.LOAD_PAYMENT_SCHEDULE);
				if( sessionMap.get(  WebConstants.InheritanceFile.INHERITANCE_FILE_ID ) != null)
				{
					viewMap.put(  WebConstants.InheritanceFile.INHERITANCE_FILE_ID,
																			sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString() 
			                   );
				}
				Boolean render = (Boolean) sessionMap.remove( WebConstants.MemsNormalDisbursements.RENDER_PAYMENT_ICON);
				if( render ) {
					viewMap.put( WebConstants.MemsNormalDisbursements.RENDER_PAYMENT_ICON, "");
				}
				
				viewMap.put( DISBURSEMENT_VIEW, disbursementDetails);
				if( null==disbursementDetails.getPaymentDetails() ) {
					List<PaymentDetailsView> listPaymentDetails = new ArrayList<PaymentDetailsView>();
					disbursementDetails.setPaymentDetails( listPaymentDetails);
					List<PaymentDetailsView> activeList = new ArrayList<PaymentDetailsView>();
					
					setPaymentDetails(activeList);
					
				} else {					
					List<PaymentDetailsView> activeList = getActiveList();
					Collections.sort( activeList, new ListComparator("refDate",true));

					setPaymentDetails(activeList);
					
					getDisbursementDetails().getPaymentDetails().clear();
				}
				List<SelectItem> disburseEvery = bean.getDomainDataList( WebConstants.MemsDisburseEvery.DOMAIN_KEY);
				viewMap.put( WebConstants.MemsDisburseEvery.DOMAIN_KEY, disburseEvery);				
			}
		} catch(Exception ex) {
			logger.LogException( "[Exception occured in init()]", ex);
			ex.printStackTrace();
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));					
		}
	}
	
	public List<SelectItem> getDisburseEvery() {
		if( viewMap.containsKey( WebConstants.MemsDisburseEvery.DOMAIN_KEY)) {
			return (List<SelectItem>) viewMap.get( WebConstants.MemsDisburseEvery.DOMAIN_KEY);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	private List<PaymentDetailsView> getActiveList() {
		List<PaymentDetailsView> activeList = new ArrayList<PaymentDetailsView>();
		List<PaymentDetailsView> toDeleteList = getToDeleteList();
		for(PaymentDetailsView singlePayment : getDisbursementDetails().getPaymentDetails()) {
			if( singlePayment.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED)) {
				activeList.add( singlePayment);
			} else {
				toDeleteList.add( singlePayment);
			}
		}			
		return activeList;
	}
	
	public List<PaymentDetailsView> getPaymentDetails() {
		if( viewMap.containsKey( ACTIVE_LIST)) {
			return (List<PaymentDetailsView>) viewMap.get( ACTIVE_LIST);
		}
		return new ArrayList<PaymentDetailsView>();
	}

	public void setPaymentDetails(List<PaymentDetailsView> activeList)
	{
		if( activeList != null )
		{
			Collections.sort( activeList, new ListComparator("refDate",true));
			viewMap.put( ACTIVE_LIST, activeList);
		}
	}
	public List<PaymentDetailsView> getToDeleteList() {
		if( viewMap.containsKey( TO_DELETE_LIST)) {
			return (List<PaymentDetailsView>) viewMap.get( TO_DELETE_LIST);
		}
		return new ArrayList<PaymentDetailsView>();
	}
	
	public DisbursementDetailsView getDisbursementDetails() {
		if( viewMap.containsKey( DISBURSEMENT_VIEW)) {
			return (DisbursementDetailsView) viewMap.get( DISBURSEMENT_VIEW);
		}
		return new DisbursementDetailsView();
	}
	
	private boolean validateAddSchedule() {
		logger.logInfo("[validateAddSchedule() .. Starts]");
		boolean bValid = true;
		if ( null==txtInstallments.getValue() || 0==txtInstallments.getValue().toString().trim().length() ) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_INST_REQ));
			bValid = false;
		} else { 
			try {
				int installments = Integer.parseInt(txtInstallments.getValue().toString().trim());
				if( 1 >= installments ) {
					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.
										MemsNormalDisbursementMsgs.ERR_INSTALLMENT));	
					bValid = false;
				}
			} catch(NumberFormatException ex) {
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
								   		.MemsNormalDisbursementMsgs.ERR_INSTALLMENT_NUM));
				bValid = false;
			}
		}
		
		if( null==cmbDisburseEvery.getValue() || cmbDisburseEvery.getValue().equals("-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_INST_FREQ));
			bValid = false;									
		}
		
		if( null==clndFirstInstallmentDate.getValue() || 0==clndFirstInstallmentDate.getValue().toString().trim().length()) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_INST_REQ));
			bValid = false;
		}
		logger.logInfo("[validateAddSchedule() .. Ends]");
		return bValid;
	}
	
	public void addSchedule() {
		logger.logInfo("[addSchedule() .. Starts]");
		try {
			if( validateAddSchedule() ) {				
				int installment = Integer.parseInt( txtInstallments.getValue().toString());
				Double totalAmount = getDisbursementDetails().getAmount();
				Double amountPerSchedule = totalAmount/installment;
				Long freqId = new Long(cmbDisburseEvery.getValue().toString());
				
				Long yearlyId = CommonUtil.getDomainDataId( WebConstants.MemsDisburseEvery.DOMAIN_KEY, 
											   				WebConstants.MemsDisburseEvery.DISBURSE_YEARLY);
				
				Long biYearlyId = CommonUtil.getDomainDataId( WebConstants.MemsDisburseEvery.DOMAIN_KEY, 
															  WebConstants.MemsDisburseEvery.DISBURSE_HALF_YEAER);
				
				Long quarterlyId = CommonUtil.getDomainDataId( WebConstants.MemsDisburseEvery.DOMAIN_KEY, 
															   WebConstants.MemsDisburseEvery.DISBURSE_QUARTER);
				
				Long montlyId = CommonUtil.getDomainDataId( WebConstants.MemsDisburseEvery.DOMAIN_KEY, 
						 									WebConstants.MemsDisburseEvery.DISBURSE_MONTH);
				
				int numberOfMonths = 0;
				if( freqId.equals( yearlyId)) {
					numberOfMonths = YEAR;
				} else if ( freqId.equals( biYearlyId)) {
					numberOfMonths = BI_YEAR;
				} else if ( freqId.equals( quarterlyId)) {
					numberOfMonths = QUARTER;
				} else if ( freqId.equals( montlyId)) { 				
					numberOfMonths = MONTH;
				}				
				List<PaymentDetailsView> existingList = getPaymentDetails();
				List<PaymentDetailsView> toDeleteList = getToDeleteList();
				for(PaymentDetailsView existingRecord : existingList) {
					if( null!=existingRecord.getPaymentDetailsId()) {
						existingRecord.setIsDeleted( WebConstants.IS_DELETED_TRUE);
						toDeleteList.add( existingRecord);						
					}
				}
				existingList.clear();
				setTotalRows( installment);				
				for( int i=0 ; i <installment ; ++i ) {									
					PaymentDetailsView payment = new PaymentDetailsView();
					payment.setMyHashCode(payment.getMyHashCode());
					payment.setStatus( WebConstants.NOT_DISBURSED);
					payment.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
					payment.setAmount( amountPerSchedule);						
					Date date = (Date) clndFirstInstallmentDate.getValue();					
					if( i>0 ) {
						Date nextDate = getNextDate( existingList.get( i-1).getRefDate(), numberOfMonths);
						payment.setRefDate( nextDate);
					} else {
						payment.setRefDate( date);
					}
					existingList.add( payment);
				}					
				setPaymentDetails(existingList);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_MSG_SCH_ADD));
				//clear fields Start
				txtInstallments.setValue("");
				cmbDisburseEvery.setValue("-1");
				clndFirstInstallmentDate.setValue(null);
				//Finish
			}
					
		} catch(Exception ex) {
			logger.LogException("[Exception occured in addSchedule]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));				
		}
		
		logger.logInfo("[addSchedule() .. Ends]");
	}
	
	public void editPaymentDetails() {
		try
		{
			PaymentDetailsView paymentDetails = (PaymentDetailsView) scheduleTable.getRowData();	
			//paymentDetails.setHashCode(paymentDetails.hashCode());
			String paidToCriteria = "";
			if( getDisbursementDetails().getRequestBeneficiaries() != null && getDisbursementDetails().getRequestBeneficiaries().size() >0 )
			{
				DisbursementRequestBeneficiaryView beneficiary = getDisbursementDetails().getRequestBeneficiaries().get( 0);
				paidToCriteria = WebConstants.PERSON_ID+ "="+ beneficiary.getPersonId().toString();
			}
			else if( viewMap.get(  WebConstants.InheritanceFile.INHERITANCE_FILE_ID ) != null)
			{
				paidToCriteria =  WebConstants.InheritanceFile.INHERITANCE_FILE_ID+ "="+ viewMap.get(  WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString();
			}
			
			if( getDisbursementDetails().getIsMultiple()!= null && getDisbursementDetails().getIsMultiple().longValue() == 1 )
			{
				sessionMap.put( WebConstants.MULTIPLE_BENEFICIARIES, "1" );
				sessionMap.put( WebConstants.DISBURSEMENT_DETAIL_ID, getDisbursementDetails().getDisDetId() );
			}
			sessionMap.put(WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP, paymentDetails);
			String javaScriptText = " var screen_width = 800;"
					+ "var screen_height = 280;"
					+ "var popup_width = screen_width-200;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('paymentDetailsPopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP&"
					+ paidToCriteria
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
			sendToParent(javaScriptText);
		}
		catch(Exception ex) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in handlePaymentDetailsChange()]", ex);
		}
	}
	
	public void handlePaymentDetailsChange() 
	{
		try 
		{
			PaymentDetailsView paymentDetails = (PaymentDetailsView) sessionMap.remove( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP);
			
			List<PaymentDetailsView> listPaymentDetails = getPaymentDetails();
			for(PaymentDetailsView singlePaymentDetail : listPaymentDetails) 
			{
				boolean areSame = singlePaymentDetail.getPaymentDetailsId() == null ?
						                                       singlePaymentDetail.getMyHashCode() == paymentDetails.getMyHashCode():
						                                       singlePaymentDetail.getPaymentDetailsId().equals( paymentDetails.getPaymentDetailsId() ); 
			if( areSame ) 
				{
					listPaymentDetails.remove( singlePaymentDetail);
					paymentDetails.setAmount( singlePaymentDetail.getAmount());
					listPaymentDetails.add( paymentDetails);
					
					break;
				}
			}
			//setPaymentDetails ( listPaymentDetails ) ;
		}
		catch(Exception ex) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[Exception occured in handlePaymentDetailsChange()]", ex);
		}
	}
	
	@Override
	public void prerender() {
		if( getRenderEditPaymentIcon() ) {
			//txtInstallments.setDisabled( true);
			//txtInstallments.setStyleClass( "READONLY");
			//cmbDisburseEvery.setDisabled( true);
			//cmbDisburseEvery.setStyle( "READONLY");
			//clndFirstInstallmentDate.setDisabled( true);
			//clndFirstInstallmentDate.setStyleClass( "READONLY");
		}
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() {
		 return TimeZone.getDefault();		
	}
	private Date getNextDate(Date currentDate, final int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime( currentDate);
        cal.add( Calendar.MONTH, months);
        return cal.getTime();
	}
	
	private boolean validateDone() {
		logger.logInfo("[validateDone() .. Starts]");
		logger.logInfo("[validateDone() .. Ends]");
		return true;
	}
	
	
	public void done() {
		if( validateDone() ) {
			try {
				DisbursementDetailsView disbursement = getDisbursementDetails();
				disbursement.getPaymentDetails().addAll( getPaymentDetails());
				disbursement.getPaymentDetails().addAll( getToDeleteList());
				sessionMap.put( WebConstants.MemsNormalDisbursements.DONE_PAYMENT_SCHEDULE, getDisbursementDetails());				 
				String javaScriptText = "window.opener.handlePaymentSchedule(); window.close();";						
				sendToParent( javaScriptText);				
			} catch(Exception ex) {
				logger.LogException( "[Exception occured in done()]", ex);
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
	}
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}
	
	public Boolean getRenderEditPaymentIcon() {
		if( viewMap.containsKey( WebConstants.MemsNormalDisbursements.RENDER_PAYMENT_ICON)) {
			return true;
		}
		return false;
	}
	
	private String sendToParent(String javaScript) {
		final String viewId = "/ReviewConfirmDisbursements.jsp";		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	
	public Boolean getViewMode() {
		if( viewMap.containsKey( VIEW_MODE)) {
			return ((String) viewMap.get( VIEW_MODE)).equals( VIEW_MODE_POPUP);
		}
		return false;		
	}
	
	public HtmlSelectOneMenu getCmbDisburseEvery() {
		return cmbDisburseEvery;
	}
	
	public void setCmbDisburseEvery(HtmlSelectOneMenu cmbDisburseEvery) {
		this.cmbDisburseEvery = cmbDisburseEvery;
	}
	
	public HtmlInputText getTxtInstallments() {
		return txtInstallments;
	}
	
	public void setTxtInstallments(HtmlInputText txtInstallments) {
		this.txtInstallments = txtInstallments;
	}

	public HtmlCalendar getClndFirstInstallmentDate() {
		return clndFirstInstallmentDate;
	}

	public void setClndFirstInstallmentDate(HtmlCalendar clndFirstInstallmentDate) {
		this.clndFirstInstallmentDate = clndFirstInstallmentDate;
	}	
}
