package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestRepliesView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class TenderInquiryReplies extends AbstractController {

	/**
	 * 
	 */
	

	private final Logger logger = Logger.getLogger(TenderInquiryReplies.class);

	public interface Keys {
		public static final String TENDER_INQUIRY_LIST = "TENDER_INQUIRY_LIST";
	}


	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;
	private List<String> errorMessages;
	private List<String> infoMessages;
	private final String notesOwner = WebConstants.TENDER_INQUIRY_REQUESTS;
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	//field used in the tender Inquiry Add jsp
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private String  applicationNumber;
	private String applicationStatus;
	private Date applicationDate;
	private String applicationSourceId;
	private String applicationSource;
	private String contractorName;
	private String contractorNo;
	private String tenderNumber;
	private String tenderDes;
	private String reasons;
	private String replayText;

	//hdn variable for contractor and tender  
	private String hdnContractorId;
	private String hdnContractorNumber;
	private String hdnContractorNameEn;
	private String hdnContractorNameAr;
	private String hdnTenderId;
	private String hdnTenderNumber;
	private String hdnTenderDescription;
	private String hdnRequestId;
	
	private TenderInquiryView tenderInquiryView = new TenderInquiryView();
	private ContractorView contractorView = new ContractorView();
	private TenderView tenderView = new TenderView();
	private CommonUtil commUtil  =new CommonUtil();
	private RequestRepliesView requestRepliesView = new RequestRepliesView();
	private List<RequestRepliesView> requestRepliesViewList = new ArrayList<RequestRepliesView>();
	private HtmlDataTable requestRepliesDataTable = new HtmlDataTable();;
	private UploadedFile selectedFile;
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	
	private final String NOTIFICATION_EVENT_PUBLISH = "Event.ExtendApplication.Publish";
	
	private final String noteOwner = WebConstants.TENDER_INQUIRY_REQUESTS;//WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_TENDER_INQUIRY;//WebConstants.Attachment.EXTERNAL_ID_SERVICE_TENDER;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_TENDER_INQUIRY_REQUESTS;//WebConstants.PROCEDURE_TYPE_SERVICE_TENDERING;
	
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();
		

		try {
			if (!isPostBack()) {
			
			
				 
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			// for setting default date on the date field 	
				applicationDate = new Date();
			// for enabling button on the Attachment and Comments tab 	
				canAddAttachmentsAndComments(true);
				
			if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
			 {
					UserTask task = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		     		//for Reply 
					if(task!=null && task.getTaskType().equals(WebConstants.TENDER_INQUIRY_REPLY))
					 {
						Map taskAttributes =  task.getTaskAttributes();
						Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
						this.hdnRequestId = requestId.toString();
						CommonUtil.loadAttachmentsAndComments(hdnRequestId);
						
						requestRepliesViewList = new RequestServiceAgent().getRequestRepliesList(requestId);
						
						 
						
						if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)){
						viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK));
						sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);}
						
						populateRequestFields(this.hdnRequestId);
						viewMap.put("SAVE_DONE",true);
				   }	
			}
			
			
			if(getFacesContext().getExternalContext().getSessionMap().containsKey("FROM_INQUIRY_GRID"))
			  {
				  TenderInquiryView tIV =  (TenderInquiryView)sessionMap.get("FROM_INQUIRY_GRID");
				  sessionMap.remove("FROM_INQUIRY_GRID");
				  this.hdnRequestId = tIV.getRequestId().toString();
				  requestRepliesViewList = new RequestServiceAgent().getRequestRepliesList(tIV.getRequestId());
				  populateRequestFields(this.hdnRequestId);
				  viewMap.put("IN_POPUP",true);
				  viewMap.put("WORK_DONE",true);
				  CommonUtil.loadAttachmentsAndComments(hdnRequestId);
				  
			 }
		
			}	
			
			updateTenderInquiryReplies();
			logger.logInfo("init method completed successfully!");
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}
	
	public void prerender(){
		super.prerender();
		if(hdnContractorNumber!=null && !hdnContractorNumber.equals("")){
		 contractorNo = hdnContractorNumber;	
		}
		if(getIsEnglishLocale()&& hdnContractorNameEn!=null && !hdnContractorNameEn.equals("")){
		contractorName = hdnContractorNameEn;	
		}
		if(!getIsEnglishLocale()&& hdnContractorNameAr!=null && !hdnContractorNameAr.equals("")){
		contractorName = hdnContractorNameAr;	
	    }
		if(hdnTenderNumber!=null && !hdnTenderNumber.equals("")){
		tenderNumber = hdnTenderNumber;
		}
		if(hdnTenderDescription!=null && !hdnTenderDescription.equals("")){
		tenderDes = hdnTenderDescription; 	
		}
			
	}
	
	public void populateRequestFields(String requestId){
		
		RequestServiceAgent rsa = new RequestServiceAgent();
		
		try {
			tenderInquiryView = rsa.getRequestByIdForTenderInquiry(Long.parseLong(requestId));
             
			
			setApplicationNumber(tenderInquiryView.getRequestNumber());
			if(getIsEnglishLocale())
			setApplicationStatus(tenderInquiryView.getStatusEn());
		    else
		    setApplicationStatus(tenderInquiryView.getStatusAr());
			
			setApplicationDate(tenderInquiryView.getRequestDate());
			
			if(getIsEnglishLocale())
			setApplicationSource(tenderInquiryView.getRequestSourceEn());
			else
			setApplicationSource(tenderInquiryView.getRequestSourceAr());
			
			if(getIsEnglishLocale()){
			setContractorName(tenderInquiryView.getContractorNameEn());
			viewMap.put("PERSON_NAME", tenderInquiryView.getContractorNameEn());
			}
			else{
			setContractorName(tenderInquiryView.getContractorNameAr());
			viewMap.put("PERSON_NAME", tenderInquiryView.getContractorNameAr());
			}
			
			viewMap.put("TENDER_NUMBER", tenderInquiryView.getTenderNumber());
			
			setContractorNo(tenderInquiryView.getContractorNumber());
			setTenderNumber(tenderInquiryView.getTenderNumber());
			setTenderDes(tenderInquiryView.getTenderDescription());
			setReasons(tenderInquiryView.getRequestDescription());

			//hdn variable for contractor and tender  
			this.hdnContractorId= tenderInquiryView.getContractorId().toString();
			viewMap.put("PERSON_ID", tenderInquiryView.getContractorId());
			
			this.hdnContractorNumber= tenderInquiryView.getContractorNumber();
			this.hdnContractorNameEn= tenderInquiryView.getContractorNameEn();
			this.hdnContractorNameAr= tenderInquiryView.getContractorNameAr();
			this.hdnTenderId= tenderInquiryView.getTenderId().toString();
			this.hdnTenderNumber= tenderInquiryView.getTenderNumber();
			this.hdnTenderDescription= tenderInquiryView.getTenderDescription();
			this.hdnRequestId= tenderInquiryView.getRequestId().toString();
			
			viewMap.put("EMAIL_LIST", tenderInquiryView.getEmailList());
		
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PimsBusinessException e) {
			e.printStackTrace();
		}
		
	}

	// Accessors starts from here
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}



	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
    
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}
  
	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		backScreen = "cancelReplies";
		return backScreen;
    }
	
	public String saveReplies() {
		logger.logInfo("cancel() started...");
		
		RequestServiceAgent rsa = new RequestServiceAgent();
		
		try {

		//tenderInquiryView.setRequestNumber(applicationNumber);
		//tenderInquiryView.setRecordStatus(Long.parseLong(applicationStatus));
		tenderInquiryView.setRequestDate(applicationDate);
		tenderInquiryView.setRequestSource(Long.parseLong(applicationSourceId));
		tenderInquiryView.setContractorNameEn(hdnContractorNameEn);
		tenderInquiryView.setContractorNameAr(hdnContractorNameAr);
		tenderInquiryView.setContractorNumber(hdnContractorNumber);
		tenderInquiryView.setContractorId(Long.parseLong(hdnContractorId));
		tenderInquiryView.setTenderId(Long.parseLong(hdnTenderId));
		tenderInquiryView.setTenderNumber(hdnTenderNumber);
		tenderInquiryView.setTenderDescription(hdnTenderDescription);
		tenderInquiryView.setRequestDescription(reasons);
		
		tenderInquiryView.setUpdatedBy(getLoggedInUser());
		tenderInquiryView.setUpdatedOn(new Date());
		tenderInquiryView.setCreatedBy(getLoggedInUser());
		tenderInquiryView.setCreatedOn(new Date());
		tenderInquiryView.setRecordStatus(1L);
		tenderInquiryView.setIsDeleted(0L);
		HashMap map =  new HashMap ();
    	tenderInquiryView = rsa.saveTenderInquiryRequest(tenderInquiryView,map);
    	
    	// after adding request successfully save show request number and status 
    	applicationNumber = tenderInquiryView.getRequestNumber();
    	
    	if(getIsEnglishLocale())
    	applicationStatus = tenderInquiryView.getStatusEn();
    	else
    	applicationStatus = tenderInquiryView.getStatusEn();
    	
    	//saveAttachments(tenderInquiryView.getRequestId());
    	//saveComments(tenderInquiryView.getRequestId());
    	
		Boolean attachSuccess = CommonUtil.saveAttachments(tenderInquiryView.getRequestId());
		Boolean commentSuccess = CommonUtil.saveComments(tenderInquiryView.getRequestId(), noteOwner);
		
		CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
		CommonUtil.loadAttachmentsAndComments(tenderInquiryView.getRequestId().toString());
		
		} catch (PimsBusinessException e) {
			e.printStackTrace();
		}
		
		return "";
    }

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public String getApplicationNumber() {
		if(viewMap.containsKey("APPLICATION_NUMBER"))
			applicationNumber=viewMap.get("APPLICATION_NUMBER").toString();
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
		if(this.applicationNumber!=null)
			viewMap.put("APPLICATION_NUMBER",this.applicationNumber);
	}

	public String getApplicationStatus() {
		if(viewMap.containsKey("APPLICATION_STATUS"))
			applicationStatus=viewMap.get("APPLICATION_STATUS").toString();
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
		if(this.applicationStatus!=null)
			viewMap.put("APPLICATION_STATUS",this.applicationStatus);
	}

	public Date getApplicationDate() {
		if(viewMap.containsKey("APPLICAITON_DATE"))
			applicationDate = (Date)viewMap.get("APPLICATION_DATE");
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
		if(this.applicationDate!=null)
			viewMap.put("APPLICATION_DATE",this.applicationDate);
		
	}

	public String getApplicationSourceId() {
		if(viewMap.containsKey("APPLICAITON_SOURCE"))
			applicationSourceId = viewMap.get("APPLICATION_SOURCE").toString();
		return applicationSourceId;
	
	}

	public void setApplicationSourceId(String applicationSourceId) {
		this.applicationSourceId = applicationSourceId;
		if(this.applicationSourceId!=null)
			viewMap.put("APPLICATION_SOURCE",this.applicationSourceId);
		
	}

	public String getContractorName() {
		if(viewMap.containsKey("CONTRACTOR_NAME"))
			contractorName = viewMap.get("CONTRACTOR_NAME").toString();
		return contractorName;
		
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
		if(this.contractorName!=null)
			viewMap.put("CONTRACTOR_NAME",this.contractorName);
		
	}

	public String getContractorNo() {
		if(viewMap.containsKey("CONTRACTOR_NUMBER"))
			contractorNo = viewMap.get("CONTRACTOR_NUMBER").toString();
		return contractorNo;
		
	}

	public void setContractorNo(String contractorNo) {
		this.contractorNo = contractorNo;
		if(this.contractorNo!=null)
			viewMap.put("CONTRACTOR_NUMBER",this.contractorNo);
		
	}

	public String getTenderNumber() {
		if(viewMap.containsKey("TENDER_NUMBER"))
			tenderNumber = viewMap.get("TENDER_NUMBER").toString();
		return tenderNumber;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
		if(this.tenderNumber!=null)
			viewMap.put("TENDER_NUMBER",this.tenderNumber);
		
	}

	public String getTenderDes() {
		if(viewMap.containsKey("TENDER_DESCRIPTION"))
			tenderDes = viewMap.get("TENDER_DESCRIPTION").toString();
		return tenderDes;
		
	}

	public void setTenderDes(String tenderDes) {
		this.tenderDes = tenderDes;
		if(this.tenderDes!=null)
			viewMap.put("TENDER_DESCRIPTION",this.tenderDes);
		
	}

	public String getReasons() {
		if(viewMap.containsKey("REASON"))
			reasons = viewMap.get("REASON").toString();
		return reasons;
		
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
		if(this.reasons!=null)
			viewMap.put("REASON",this.reasons);
		
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public String getHdnContractorNumber() {
		return hdnContractorNumber;
	}

	public void setHdnContractorNumber(String hdnContractorNumber) {
		this.hdnContractorNumber = hdnContractorNumber;
	}

	public String getHdnContractorNameEn() {
		return hdnContractorNameEn;
	}

	public void setHdnContractorNameEn(String hdnContractorNameEn) {
		this.hdnContractorNameEn = hdnContractorNameEn;
	}

	public String getHdnContractorNameAr() {
		return hdnContractorNameAr;
	}

	public void setHdnContractorNameAr(String hdnContractorNameAr) {
		this.hdnContractorNameAr = hdnContractorNameAr;
	}

	public String getHdnTenderId() {
		return hdnTenderId;
	}

	public void setHdnTenderId(String hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	public String getHdnTenderNumber() {
		return hdnTenderNumber;
	}

	public void setHdnTenderNumber(String hdnTenderNumber) {
		this.hdnTenderNumber = hdnTenderNumber;
	}

	public String getHdnTenderDescription() {
		return hdnTenderDescription;
	}

	public void setHdnTenderDescription(String hdnTenderDescription) {
		this.hdnTenderDescription = hdnTenderDescription;
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	

	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}
	
	public String getApplicationSource() {
		if(viewMap.containsKey("APPLICATION_SOURCE"))
			applicationSource = viewMap.get("APPLICATION_SOURCE").toString();
		return applicationSource;
		
	}

	public void setApplicationSource(String applicationSource) {
		this.applicationSource = applicationSource;
		if(this.applicationSource!=null)
			viewMap.put("APPLICATION_SOURCE",this.applicationSource);
		
	}

	public String getReplayText() {
		if(viewMap.containsKey("REPLAY_TEXT"))
			replayText = viewMap.get("REPLAY_TEXT").toString();
		return replayText;

		
	}

	public void setReplayText(String replayText) {
		this.replayText = replayText;
		if(this.replayText!=null)
			viewMap.put("REPLAY_TEXT",this.replayText);

	}
	
	public String replied(){
		
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		DomainDataView ddv = new DomainDataView();
		errorMessages = new  ArrayList<String>();
		infoMessages = new ArrayList<String>();
		
		try {
			
			UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client;
			client = new BPMWorklistClient(contextPath);
			
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			this.hdnRequestId = requestId.toString();
	
	    	
	    	ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
					WebConstants.REQUEST_STATUS_REPLIED);
		     
	    	RequestServiceAgent rsa = new RequestServiceAgent();
	    	
	    
	    	
	    if(requestRepliesViewList!=null && requestRepliesViewList.size()>0)
	      {	
	  	    for(RequestRepliesView rRV:requestRepliesViewList)
	  	    {
	  	      if(rRV.getRequestRepliesId()==null)	
	  	    	rsa.saveTenderInquiryReply(Long.parseLong(this.hdnRequestId), ddv.getDomainDataId(), rRV);
	  	      else if(rRV.getRequestRepliesId()!=null && rRV.getIsDeleted().compareTo(1L)==0)
	            {
	    	    rsa.saveTenderInquiryReply(Long.parseLong(this.hdnRequestId), ddv.getDomainDataId(), rRV);
	    	    viewMap.put("DELETE_RECORD", true);
	            }
	  	    }
	    	
	  	    
			CommonUtil.loadAttachmentsAndComments(hdnRequestId);
	    	Boolean attachSuccess = CommonUtil.saveAttachments(Long.parseLong(this.hdnRequestId));
			Boolean commentSuccess = CommonUtil.saveComments(Long.parseLong(this.hdnRequestId), noteOwner);

	    	canAddAttachmentsAndComments(false);
	    
	    	generateNotification(WebConstants.Event_TENDERINQUIRY_REPLYSEND);
	    	//put success msg here
	    	
	    	client.completeTask(userTask, getLoggedInUser(), TaskOutcome.REPLY);
	    	
	    	infoMessages.add(getBundleMessage("tenderInquiryReplies.msg.replaySend"));
	    	viewMap.put("WORK_DONE", true); 
	    	viewMap.put("SAVE_DONE", true);
	    	viewMap.put("REPLY_DONE", true);
		 }
				
		
	     } 
		catch (Exception e) {
			 logger.LogException("replied Exception Occure::::::", e);
		      }
	
		
		return "success";
		
	}
	
	
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		
		placeHolderMap.setValue("CONTRACTOR_NAME", "Contractor");
		placeHolderMap.setValue("TENDER_NO", "ABC");
		placeHolderMap.setValue("CLOSURE_DATE", "12/12/2009");
		
		logger.logDebug(methodName+"|Finish");
	}
	
	
	
	public String cancelRequest(){
		
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		DomainDataView ddv = new DomainDataView();
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();
		try {
			
			UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client;
			client = new BPMWorklistClient(contextPath);
			
		
			
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			this.hdnRequestId = requestId.toString();
			
			//saveAttachments(Long.parseLong(this.hdnRequestId));
	    	//saveComments(Long.parseLong(this.hdnRequestId));	
	    	
	    	Boolean attachSuccess = CommonUtil.saveAttachments(Long.parseLong(this.hdnRequestId));
			Boolean commentSuccess = CommonUtil.saveComments(Long.parseLong(this.hdnRequestId), noteOwner);
	         
	    	RequestServiceAgent rsa = new RequestServiceAgent();
			rsa.setRequestAsCanceled(Long.parseLong(this.hdnRequestId), getLoggedInUser());
	    	
			generateNotification(WebConstants.Event_TENDERINQUIRY_REPLYREJECT);
	    	//put success msg here
			client.completeTask(userTask, getLoggedInUser(), TaskOutcome.CANCEL);
			infoMessages.add(getBundleMessage("tenderInquiryReplies.msg.replayCancel"));
			viewMap.put("WORK_DONE", true);
			viewMap.put("SAVE_DONE", true);
			viewMap.put("REPLY_DONE", true);
		
		
	     }catch (NumberFormatException e) {
		  logger.LogException("Excetpion in cancelRequest:::::::::::", e);
		 } catch (PimsBusinessException e) {
			 logger.LogException("Excetpion in cancelRequest:::::::::::", e);
		 }catch (PIMSWorkListException e1) {
			 logger.LogException("Excetpion in cancelRequest:::::::::::", e1);
	     }catch (IOException e) {
	    	 logger.LogException("Excetpion in cancelRequest:::::::::::", e);
	     }
	
		
		return "success";
		
	}
	
private List<DomainDataView> getDomainDataListForDomainType(String domainType)
{
	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	Iterator<DomainTypeView> itr = list.iterator();
	//Iterates for all the domain Types
	while( itr.hasNext() ) 
	{
		DomainTypeView dtv = itr.next();
		if( dtv.getTypeName().compareTo(domainType)==0 ) 
		{
			Set<DomainDataView> dd = dtv.getDomainDatas();
			//Iterates over all the domain datas
			for (DomainDataView ddv : dd) 
			{
			  ddvList.add(ddv);	
			}
			break;
		}
	}
	
	return ddvList;
}

public UploadedFile getSelectedFile() {
	if(viewMap.containsKey("SELECTED_FILE"))
	  selectedFile = (UploadedFile)viewMap.get("SELECTED_FILE");
	return selectedFile;
}

public void setSelectedFile(UploadedFile selectedFile) {
	this.selectedFile = selectedFile;
	if(this.selectedFile !=null)
		viewMap.put("SELECTED_FILE", selectedFile);
}

public HtmlInputFileUpload getFileUploadCtrl() {
	return fileUploadCtrl;
}

public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
	this.fileUploadCtrl = fileUploadCtrl;
}
public TimeZone getTimeZone()
{
       return TimeZone.getDefault();
}
public String getBundleMessage(String key) {
	logger.logInfo("getBundleMessage(String) started...");
	String message = "";
	try {
		message = ResourceUtil.getInstance().getProperty(key);

		logger
				.logInfo("getBundleMessage(String) completed successfully!!!");
	} catch (Exception exception) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(
				MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("getBundleMessage(String) crashed ", exception);
	}
	return message;
}

public Boolean getHideButton(){
	
	if(viewMap.containsKey("WORK_DONE"))
		return false;
	else
		return true;
	
  }

public Boolean getShowCancelButton(){
	
	if(viewMap.containsKey("WORK_DONE"))
		return true;
	else
		return false;
	
  }


public Boolean getHideSaveButton(){
	
	if(viewMap.containsKey("SAVE_DONE"))
		return false;
	else
		return true;
	
  }

public Boolean getHideReplyButton(){
	
	if(viewMap.containsKey("REPLY_DONE"))
		return false;
	else
		return true;
	
  }


public Boolean getHideHeader(){
	
	if(viewMap.containsKey("IN_POPUP"))
		return false;
	else
		return true;
	
  }

public String  btnSave_Click(){
	
	errorMessages = new ArrayList<String>();
	infoMessages = new ArrayList<String>();
  
	try {	
	RequestServiceAgent rsa = new RequestServiceAgent(); 	
	DomainDataView ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
			WebConstants.REQUEST_STATUS_REPLIED);
	
   if(requestRepliesViewList!=null && requestRepliesViewList.size()>0)
      {	
	    for(RequestRepliesView rRV:requestRepliesViewList)
	    {
	      if(rRV.getRequestRepliesId()==null)	
	    	rsa.saveTenderInquiryReply(Long.parseLong(this.hdnRequestId), ddv.getDomainDataId(), rRV);
	      else if(rRV.getRequestRepliesId()!=null && rRV.getIsDeleted().compareTo(1L)==0)
	          {
	    	  rsa.saveTenderInquiryReply(Long.parseLong(this.hdnRequestId), ddv.getDomainDataId(), rRV);
	    	  viewMap.put("DELETE_RECORD", true);
	          }
	    }
		//saving Attachment and Comments
	    //DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), "Reply", null);
		CommonUtil.loadAttachmentsAndComments(hdnRequestId);
	//	saveAttachments(Long.parseLong(this.hdnRequestId));
    //	saveComments(Long.parseLong(this.hdnRequestId));
    	
    	Boolean attachSuccess = CommonUtil.saveAttachments(Long.parseLong(this.hdnRequestId));
		Boolean commentSuccess = CommonUtil.saveComments(Long.parseLong(this.hdnRequestId), noteOwner);

		
		CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
		
		
		
		
		
		generateNotification(WebConstants.Event_TENDERINQUIRY_REPLYSEND);
		
		infoMessages.add(getBundleMessage("tenderInquiryReplies.msg.replaySend"));
    	//viewMap.put("WORK_DONE", true); 
    	viewMap.put("SAVE_DONE", true);
    	viewMap.put("REPLY_DONE", true);
    	
    	canAddAttachmentsAndComments(false);
      }	

	
	} catch (NumberFormatException e) {
	logger.LogException("Exception in btnSave_Click:::::", e);
	} catch (PimsBusinessException e) {
	logger.LogException("Exception in btnSave_Click:::::", e);	
	}
	
	return "";
  }

public void generateNotification(String eventType)
{
	   String methodName ="generateNotification";
	   String personName ;
	   String tenderNumber;
	   Long personId;
	   PersonView personView = new PersonView();
	   
	      Boolean success = false;
	            try
	            {
	            	logger.logInfo(methodName+"|Start");
	            	
	            	
	            	
	            	
	            	
	            	PropertyServiceAgent psa =  new PropertyServiceAgent();
	                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
	                NotificationFactory nsfactory = NotificationFactory.getInstance();
	                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
	                Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
	        		
	                if(viewMap.containsKey("PERSON_NAME")&&viewMap.containsKey("TENDER_NUMBER")&&viewMap.containsKey("PERSON_ID")){
	                
	            
	            		personName = (String)viewMap.get("PERSON_NAME");
	            		tenderNumber = (String)viewMap.get("TENDER_NUMBER");
	            		personId = (Long)viewMap.get("PERSON_ID");
	            		personView =  psa.getPersonInformation(personId);
	            		getNotificationPlaceHolder(event,tenderNumber,personName);
	            		personsEmailList = getEmailContactInfos((PersonView)personView);
                        if(personsEmailList.size()>0)
                              notifier.fireEvent(event, personsEmailList);
	        			
	        	
	                  
	                }	
	                  success  = true;
	                  logger.logInfo(methodName+"|Finish");
	            }
	            catch(Exception ex)
	            {
	                  logger.LogException(methodName+"|Finish", ex);
	            }
	          
}
private List<ContactInfo> getEmailContactInfos(PersonView personView)
{
	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
	Iterator iter = personView.getContactInfoViewSet().iterator();
	HashMap previousEmailAddressMap = new HashMap();
	while(iter.hasNext())
	{
		ContactInfoView ciView = (ContactInfoView)iter.next();
		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
        {
			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
			//emailList.add(ciView);
        }
	}
	return emailList;
}
private void  getNotificationPlaceHolder(Event placeHolderMap,String tenderNumber,String personName)
{
	String methodName = "getNotificationPlaceHolder";
	logger.logDebug(methodName+"|Start");
	
	placeHolderMap.setValue("PERSON_NAME",personName);
	placeHolderMap.setValue("TENDER_NUMBER",tenderNumber);
	
	logger.logDebug(methodName+"|Finish");
	
}

public String  onAddReplies(){
    
	sessionMap.put(WebConstants.FROM_TENDER_INQUIRY_REPLIES, true);
	openPopup("showTenderInquiryRepliesAll();");
	return "";
}

public String  receiveReplies(){
	RequestRepliesView  requestRepliesView = new RequestRepliesView();
	try {
	if( sessionMap.get(WebConstants.TENDER_INQUIRY_ANSWER)!=null)
	{
		// Not in the list
		requestRepliesView.setAnswer((String)sessionMap.get(WebConstants.TENDER_INQUIRY_ANSWER));
		requestRepliesView.setCreatedBy( CommonUtil.getLoggedInUser() );
		requestRepliesView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		requestRepliesView.setCreatedOn( new Date() );
		requestRepliesView.setUpdatedOn( new Date() );
		requestRepliesView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
		requestRepliesView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
		requestRepliesViewList.add(requestRepliesView );
		
		//this.setTenderDes(requestRepliesView.getAnswer());
		
		sessionMap.remove(WebConstants.TENDER_INQUIRY_ANSWER);
		
		if(sessionMap.get(WebConstants.TENDER_INQUIRY_ATTACHED_FILE)!=null){
			CommonUtil.loadAttachmentsAndComments(this.hdnRequestId);  
			fileUploadCtrl.setUploadedFile((UploadedFile)sessionMap.get(WebConstants.TENDER_INQUIRY_ATTACHED_FILE));
			DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), "Reply", null);
		    sessionMap.remove(WebConstants.TENDER_INQUIRY_ATTACHED_FILE);
		}
		
		CommonUtil.loadAttachmentsAndComments(this.hdnRequestId);  
		Boolean attachSuccess = CommonUtil.saveAttachments(Long.parseLong(this.hdnRequestId));
		Boolean commentSuccess = CommonUtil.saveComments(Long.parseLong(this.hdnRequestId), noteOwner);
		
		CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
		((AttachmentBean)getBean("pages$attachment")).getAllStoredDocs();  

   }
     updateTenderInquiryReplies();
     
	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (PimsBusinessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     
	return "";
}

public String  cmdDeleteReplies(){


	String path = null;
	
	RequestRepliesView selectedRequestRepliesView = (RequestRepliesView) requestRepliesDataTable.getRowData();
	
	if ( selectedRequestRepliesView.getRequestRepliesId() == null )
		requestRepliesViewList.remove( selectedRequestRepliesView);
	else
		selectedRequestRepliesView.setIsDeleted(1L);
	
	updateTenderInquiryReplies();
	
	return path;
	
	
}
public Integer getRepliesRecordSize()
{
	int size = 0;
	
	for ( RequestRepliesView assetClsView :  requestRepliesViewList )
	{
		if ( assetClsView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
			size++;
	}
	return size;
}
private void openPopup(String javaScriptText) {
	String METHOD_NAME = "openPopup()"; 
	logger.logInfo(METHOD_NAME + " --- STARTED --- ");
	try 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
	} 
	catch (Exception exception) 
	{			
		logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
	}
}
public void updateTenderInquiryReplies()
 {
    if(viewMap.get("REPLIES_LIST")!=null)
    {
      requestRepliesViewList =(List<RequestRepliesView>) viewMap.get("REPLIES_LIST"); 	
    }
    
    if(requestRepliesViewList!=null && requestRepliesViewList.size()>0)
    {
    	viewMap.put("REPLIES_LIST", requestRepliesViewList);
    }
    	
 }
public Integer getPaginatorRows() {		
	return WebConstants.RECORDS_PER_PAGE;
}
public Integer getPaginatorMaxPages() {
	return WebConstants.SEARCH_RESULTS_MAX_PAGES;
}

public HtmlDataTable getRequestRepliesDataTable() {
	return requestRepliesDataTable;
}

public void setRequestRepliesDataTable(HtmlDataTable requestRepliesDataTable) {
	this.requestRepliesDataTable = requestRepliesDataTable;
}

public List<RequestRepliesView> getRequestRepliesViewList() {
	return requestRepliesViewList;
}

public void setRequestRepliesViewList(
		List<RequestRepliesView> requestRepliesViewList) {
	this.requestRepliesViewList = requestRepliesViewList;
}


}