package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.ExtendApplicationFilterView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestTypeView;


public class ExtendApplicationSearchBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ExtendApplicationSearchBacking.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	
	private Map<String,Long> requestStatusMap = new HashMap<String,Long>();
	
	private ExtendApplicationFilterView searchFilterView = new ExtendApplicationFilterView();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	
	private ExtendApplicationFilterView dataItem = new ExtendApplicationFilterView();
	private List<ExtendApplicationFilterView> dataList = new ArrayList<ExtendApplicationFilterView>();
	
	private HtmlSelectOneMenu applicationStatusCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu applicationSourceCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu tenderTypeCombo = new HtmlSelectOneMenu();
	
	private List<SelectItem> requestStatuses = new ArrayList<SelectItem>();
	private List<SelectItem> tenderTypeList=new ArrayList<SelectItem>();
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private HtmlDataTable dataTable;
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	HttpServletRequest request = (HttpServletRequest) FacesContext
	.getCurrentInstance().getExternalContext().getRequest();
	
	private ServletContext context1 = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private Map<String,List<SelectItem>> listMap = new HashMap<String,List<SelectItem>>();
	private List<SelectItem> emptyList = new ArrayList<SelectItem>();
	
	public ExtendApplicationSearchBacking(){
		
	}
	

	/*
	 * Methods
	 */
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		return CommonUtil.getLoggedInUser();
	}
	
	@SuppressWarnings("unchecked")
	public String onManage(){
		logger.logInfo("onManage() started...");
		String taskType = "";
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
				
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
	   		}
	   		logger.logInfo("onManage() completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("onManage() crashed ", exception);
		}	
		return "extendApplicationDetails";
	}
	
	public String onFollowUp(){
		String METHOD_NAME = "onFollowUp()";
		String requestTypeId = null;
		String taskType = "";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_FOLLOW_UP);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "extendApplicationDetails";
	}

	public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }
	
	@SuppressWarnings("unchecked")
	public String onPublish(){
		String METHOD_NAME = "onPublish()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_PUBLISH);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "extendApplicationDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_ID, requestId);
	   		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_VIEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "extendApplicationDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onDelete(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ExtendApplicationFilterView reqView=(ExtendApplicationFilterView)dataTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		Boolean success = requestServiceAgent.deleteRequest(requestId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   	    	dataList = (List<ExtendApplicationFilterView>) viewMap.get("requestList");
	   	    	ExtendApplicationFilterView temp = dataList.remove(index);
	   	    	viewMap.put("requestList",dataList);
	   	    	viewMap.put("recordSize",dataList.size());
	   		}
	   		else
	   			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_FAILURE));
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "";
	}
	
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	
	private void loadRequestList() throws PimsBusinessException{
		logger.logInfo("loadRequestList() started...");
    	try{
			dataList = new ArrayList<ExtendApplicationFilterView>();
			Map argMap = getArgMap();
		
//			if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
//				searchFilterView.setFromConstruction(true);
//			else
//				searchFilterView.setFromConstruction(false);
			
			List<ExtendApplicationFilterView> requestViewList = requestServiceAgent.getExtendApplicationRequests(searchFilterView, argMap);
			viewMap.put("requestList", requestViewList);
			recordSize = requestViewList.size();
			viewMap.put("recordSize", recordSize);
			if(requestViewList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadRequestList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadRequestList() crashed ",exception);
			throw exception;
		}
		catch(Exception exception){
			logger.LogException("loadRequestList() crashed ",exception);
		}
	}
	
	public Boolean validateFields() {
		if(StringHelper.isNotEmpty(searchFilterView.getRequestNumber()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getApplicationStatusId()) && !searchFilterView.getApplicationStatusId().equals("0"))
			return true;
		if(searchFilterView.getRequestDateFrom()!=null)
			return true;
		if(searchFilterView.getRequestDateTo()!=null)
			return true;			
		if(StringHelper.isNotEmpty(searchFilterView.getContractorName()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getSourceId()) && !searchFilterView.getSourceId().equals("0"))
			return true;		
		if(StringHelper.isNotEmpty(searchFilterView.getTenderNumber()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getTenderTypeId()) && !searchFilterView.getTenderTypeId().equals("0"))
			return true;
		return false;
	}
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
//			if(!isPostBack())
//			{
//				if(request.getParameter("ConstructionTender")!=null)
//					viewMap.put("FROM_CONSTRUCTION_TENDER", true);
//			}
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}
	
	public String onAddServiceRequest()
    {
    	logger.logInfo("onAddServiceRequest() started...");
    	try{
//    	Commented code was written for adding two different type of request, but here we are seperating the code to add request
//    		if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
//    		 sessionMap.put("FROM_CONSTRUCTION_TENDER", viewMap.get("FROM_CONSTRUCTION_TENDER"));
    		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_NEW);
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
    		logger.logInfo("onAddServiceRequest() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddServiceRequest() crashed ",exception);
    	}
        return "extendApplicationDetails";
    }
	public String onAddConstructionRequest()
    {
    	logger.logInfo("onAddConstructionRequest() started...");
    try{
     		    sessionMap.put("FROM_CONSTRUCTION_TENDER", true);
    		sessionMap.put(WebConstants.ExtendApplication.EXTEND_REQUEST_MODE, WebConstants.ExtendApplication.EXTEND_REQUEST_MODE_NEW);
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EXTEND_REQUEST_SEARCH);
    		logger.logInfo("onAddConstructionRequest() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddConstructionRequest() crashed ",exception);
    	}
        return "extendApplicationDetails";
    }
	
    public String searchRequests()
    {
    	logger.logInfo("searchRequests() started...");
    	try{
    		errorMessages = new ArrayList<String>();
    		if(validateFields())
    			loadRequestList();
    		else
    			errorMessages.add(getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
    			
    		logger.logInfo("searchRequests() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchRequests() crashed ",exception);
    	}
        return "searchRequests";
    }
	
    
    public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		        logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }
    
    public List<SelectItem> getRequestStatusList() {
		Boolean isEnglishLocale = CommonUtil.getIsEnglishLocale();
		
		if(isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_EN);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NEW)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_ACCEPTED_APPROVED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_ACCEPTED_REJECTED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CANCELED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_AR);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NEW)==0
							   ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)==0
							   ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_ACCEPTED_APPROVED)==0
							   ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_ACCEPTED_REJECTED)==0
							   ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CANCELED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
		
	}
    
    	
	/*
	 * Setters / Getters
	 */
    

	/**
	 * @return the dataItem
	 */
	public ExtendApplicationFilterView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(ExtendApplicationFilterView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<ExtendApplicationFilterView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<ExtendApplicationFilterView>)viewMap.get("requestList");
		if(dataList==null)
			dataList = new ArrayList<ExtendApplicationFilterView>();
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<ExtendApplicationFilterView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	/**
	 * @return the requestStatuses
	 */
	public List<SelectItem> getRequestStatuses() {
		return requestStatuses;
	}

	/**
	 * @param requestStatuses the requestStatuses to set
	 */
	public void setRequestStatuses(List<SelectItem> requestStatuses) {
		this.requestStatuses = requestStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the requestStatusMap
	 */
	public Map<String, Long> getRequestStatusMap() {
		return requestStatusMap;
	}

	/**
	 * @param requestStatusMap the requestStatusMap to set
	 */
	public void setRequestStatusMap(Map<String, Long> requestStatusMap) {
		this.requestStatusMap = requestStatusMap;
	}


	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public ExtendApplicationFilterView getSearchFilterView() {
		return searchFilterView;
	}


	public void setSearchFilterView(ExtendApplicationFilterView searchFilterView) {
		this.searchFilterView = searchFilterView;
	}


	public HtmlSelectOneMenu getApplicationStatusCombo() {
		return applicationStatusCombo;
	}


	public void setApplicationStatusCombo(HtmlSelectOneMenu applicationStatusCombo) {
		this.applicationStatusCombo = applicationStatusCombo;
	}

	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }


	public HtmlSelectOneMenu getApplicationSourceCombo() {
		return applicationSourceCombo;
	}


	public void setApplicationSourceCombo(HtmlSelectOneMenu applicationSourceCombo) {
		this.applicationSourceCombo = applicationSourceCombo;
	}


	public HtmlSelectOneMenu getTenderTypeCombo() {
		return tenderTypeCombo;
	}


	public void setTenderTypeCombo(HtmlSelectOneMenu tenderTypeCombo) {
		this.tenderTypeCombo = tenderTypeCombo;
	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}


	public List<SelectItem> getTenderTypeList() 
	{
		if(tenderTypeList.size()<=0)
		{	
			ApplicationBean applicationBean=new ApplicationBean();
			tenderTypeList=applicationBean.getTenderTypeList();
			tenderTypeList.addAll(applicationBean.getConstructionServiceTypeList());
		}	
		return tenderTypeList;
	}


	public void setTenderTypeList(List<SelectItem> tenderTypeList) {
		this.tenderTypeList = tenderTypeList;
	}
	

	
}

