package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.report.criteria.PropertyTenantsListReportCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;

public class PropertyTenantsListBackingBean extends AbstractController
{	
	private static final long serialVersionUID = -3809550508144945879L;	
	private PropertyTenantsListReportCriteria propertyTenantsListReportCriteria;
	private Logger logger;
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	/** FOR TANENT DIALOG **/
	private String hdnTenantId;
	private String TENANT_INFO="TENANTINFO";
	
	@SuppressWarnings( "unchecked" )
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	/** FOR TANENT DIALOG END **/
	
	public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}
	
	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public PropertyTenantsListBackingBean() 
	{
		logger = Logger.getLogger(PropertyTenantsListBackingBean.class);
		
		if(CommonUtil.getIsEnglishLocale())
			propertyTenantsListReportCriteria=new PropertyTenantsListReportCriteria(ReportConstant.Report.PROPERTY_TENANTS_LIST_EN, ReportConstant.Processor.PROPERTY_TENANTS_LIST,CommonUtil.getLoggedInUser());
		else    
			propertyTenantsListReportCriteria=new PropertyTenantsListReportCriteria(ReportConstant.Report.PROPERTY_TENANTS_LIST_AR, ReportConstant.Processor.PROPERTY_TENANTS_LIST,CommonUtil.getLoggedInUser());
	}

	public PropertyTenantsListReportCriteria getPropertyTenantsListReportCriteria() {
		return propertyTenantsListReportCriteria;
	}

	public void setPropertyTenantsListReportCriteria(
			PropertyTenantsListReportCriteria propertyTenantsListReportCriteria) {
		this.propertyTenantsListReportCriteria = propertyTenantsListReportCriteria;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertyTenantsListReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	@Override
	public void init() {
		super.init();
			//loadCountry();
			loadState();
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", new ArrayList<SelectItem>(0));
//	    		if(request.getParameter("pageMode")!=null&&
//	    				request.getParameter("pageMode").equals(MODE_SELECT_ONE_POPUP)) 
//      	    		viewMap.put("pageMode",MODE_SELECT_ONE_POPUP );
//      	    	else if(request.getParameter("pageMode")!=null&&
//      	    			request.getParameter("pageMode").equals(MODE_SELECT_MANY_POPUP))
//      	    		viewMap.put("pageMode",MODE_SELECT_MANY_POPUP );
//	    		if(viewMap.get("pageMode")==null)
//	    			viewMap.put("pageMode","fullScreenMode");
			}
	}
	
	
/*	public void loadCountry()  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      
			
		      this.getCountryList().add(item);
		  }
		
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("country", countryList);
		
		}catch (Exception e){
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}
	
*/	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void loadState(/*ValueChangeEvent vce*/)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "UAE";
		/*for (int i=0;i<this.getCountryList().size();i++)
		{
			if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
			{
				selectedCountryName = 	this.getCountryList().get(i).getLabel();
			}
		}	*/
		
		Long UAE_Region_Id = 19L;
		List <RegionView> regionViewList = psa.getCountryStatesExt(UAE_Region_Id);
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void  loadCity(ValueChangeEvent vce )
	{
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try 
		{
			 Set<RegionView> regionViewList = null;
			 if ( vce.getNewValue() != null && vce.getNewValue().toString().length()>0 && 
				  !vce.getNewValue().toString().equals( "0" )
			    )
			 {
				 regionViewList =  psa.getCity( new Long( vce.getNewValue().toString() ) );
				Iterator itrator = regionViewList.iterator();
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)itrator.next();
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
					  }
					  else 
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      
					  cmbCityList.add(item);
				  }
				
				Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
			 }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", cmbCityList);
			 logger.logInfo(methodName+"|"+"Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);
			
		}
	   }
	
	/** FOR TANENT DIALOG **/
	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}
	
	public void retrieveTenant()
	{
		String methodName =  " retrieveTenant";
		logger.logInfo(methodName+ " Started");
		try
		{
		FillTenantInfo();

		if(viewRootMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);

//		   String tenant_Type=getTenantType(tenantViewRow);


	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
	        contractListCriteriaBackingBean.setTenantName(tenantNames);
//	        contractListCriteriaBackingBean.setTenantType(tenant_Type);
	    }

		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed", ex);
		}

	}
	private void FillTenantInfo() throws PimsBusinessException {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

	    	}

	}
	/** FOR TANENT DIALOG END **/
}
