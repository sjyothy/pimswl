package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.PaymentConfigurationManager;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.ui.util.ResourceUtil;


public class AuctionUnitBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionUnitBackingBean.class);
	/**
	 * 
	 */
	
	public AuctionUnitBackingBean(){
		
	}
	
//	
//	private static final long serialVersionUID = 1L;
//
	
	private String infoMessage = "";
	
	private PropertyView propertyView = new PropertyView();
	private AuctionUnitView auctionUnitView = new AuctionUnitView();
	private AuctionUnitView dataItem = new AuctionUnitView();
	private List<AuctionUnitView> dataList = new ArrayList<AuctionUnitView>();

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private List<String> errorMessages = new ArrayList<String>();
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
//	private String propertyNumber;
//	private String commercialName;
//	private String endowedName;
	private String propertyCategoryId;
	private String propertyTypeId;
	private String propertyUsageId;
	private String investmentPurposeId;
	private String propertyStatusId;
	
	private List<SelectItem> propertyTypes = new ArrayList();
	private List<SelectItem> propertyCategories = new ArrayList();
	private List<SelectItem> propertyUsages = new ArrayList();
	private List<SelectItem> investmentPurposes = new ArrayList();
	private List<SelectItem> propertyStatuses = new ArrayList();
	
	private HtmlSelectOneMenu propertyTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyCategorySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyUsageSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu investmentPurposeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyStatusSelectMenu= new HtmlSelectOneMenu();
	
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private Boolean sortAscending = true;
	
	private Boolean selected = false;
	
	
	/**
	 * @return the selected
	 */
	public Boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String editDataItem(){
//		SelectItem temp = new SelectItem();
//		temp.getValue();
		return "edit";
	}
	
	public List<AuctionUnitView> getSelectedAuctionUnits(){
		logger.logInfo("getSelectedAuctionUnits() started...");
		List<AuctionUnitView> selectedAuctionUnitList = new ArrayList<AuctionUnitView>(0);
    	try{
    		if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.VACANT_UNIT_LIST))
    				dataList = (List<AuctionUnitView>) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.VACANT_UNIT_LIST); 
    		if((!dataList.isEmpty()) && (dataList!=null))
    		{
    			for(AuctionUnitView auctionUnit: dataList){
    				if(auctionUnit.getSelected()!=null)
    					if(auctionUnit.getSelected())
    						{
    						 auctionUnit.setCreatedBy(getLoggedInUser());
    				 		 auctionUnit.setCreatedOn(new Date());
    						 auctionUnit.setUpdatedBy(getLoggedInUser());
    						 auctionUnit.setUpdatedOn(new Date());
    						 auctionUnit.setRecordStatus(1L);
    						 auctionUnit.setIsDeleted(0L);
    						 removeIfAlreadySelected(auctionUnit.getUnitId());
    						 selectedAuctionUnitList.add(auctionUnit);
    						}
    			}
    		}
    		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.SELECTED_AUCTION_UNITS,selectedAuctionUnitList);
    		
    		logger.logInfo("getSelectedAuctionUnits() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("getSelectedAuctionUnits() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedAuctionUnits() crashed ",exception);
    	}
    	
    	return selectedAuctionUnitList;
	}
	
	public void removeIfAlreadySelected(Long unitId){
		logger.logInfo("removeIfAlreadySelected() started...");
		List<AuctionUnitView> allSelectedUnitList = new ArrayList<AuctionUnitView>();
        if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
        	allSelectedUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
    	try{
    		if((!allSelectedUnitList.isEmpty()) && (allSelectedUnitList!=null))
    		{
    			for(int i=0; i<allSelectedUnitList.size(); i++){
    				AuctionUnitView auctionUnit = allSelectedUnitList.get(i);
    				if(auctionUnit.getUnitId()!=null)
    					if(auctionUnit.getUnitId().equals(unitId))
    						{
    							allSelectedUnitList.remove(i);
    						}
    			}
    		}
    		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.ALL_SELECTED_AUCTION_UNITS,allSelectedUnitList);
    		
    		logger.logInfo("removeIfAlreadySelected() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("removeIfAlreadySelected() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("removeIfAlreadySelected() crashed ",exception);
    	}
	}
	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	
	
	@SuppressWarnings("unchecked")
	public void saveAuctionUnitPrices(ActionEvent event){
		Long auctionUnitId = 0L;
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		logger.logInfo("saveAuctionUnitPrices() started...");
		try{
			if(sessionMap.containsKey(WebConstants.AUCTION_UNIT_ID))
	  		{
	  			auctionUnitId = Long.valueOf(((String) sessionMap.get(WebConstants.AUCTION_UNIT_ID)));
	  			AuctionUnitView aucUnitView = propertyServiceAgent.getAuctionUnitById(auctionUnitId);
	  		}
		
	        final String viewId = "/auctionUnitPriceEdit.jsp"; 
	        
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:closeWindowSubmit();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
    	catch(PimsBusinessException exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void addToAuction(ActionEvent event){
				
        final String viewId = "/auctionUnits.jsp"; 
        List<AuctionUnitView> selectedUnitList = new ArrayList<AuctionUnitView>();
        List<AuctionUnitView> allSelectedUnitList = new ArrayList<AuctionUnitView>();
        selectedUnitList = getSelectedAuctionUnits();
        if(!selectedUnitList.isEmpty())
        {
	        if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
	        	allSelectedUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
	//        Set<AuctionUnitView> temp = new HashSet<AuctionUnitView>();
	//  	    temp.addAll(allSelectedUnitList);
	//  	    allSelectedUnitList = new ArrayList<AuctionUnitView>();
	        allSelectedUnitList.addAll(selectedUnitList);
	        getFacesContext().getExternalContext().getSessionMap().put(WebConstants.ALL_SELECTED_AUCTION_UNITS, allSelectedUnitList);
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	//        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	        String javaScriptText = "javascript:closeWindowSubmit();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        }
        else
        {
        	errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.NO_UNIT_SELECTED));
        }
	}
	
//    public String cancel(ActionEvent event){
//    	
//    	return "cancel";
//    }
    
    public void cancel(ActionEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";
        
        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);         
    }

    
    public String clearSearch(){
    	auctionUnitView.setPropertyCommercialName("");
    	auctionUnitView.setPropertyEndowedName("");
    	auctionUnitView.setPropertyNumber("");
    	propertyTypeSelectMenu.setValue(propertyTypes.get(0).getValue());
//    	private HtmlSelectOneMenu propertyCategorySelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu propertyUsageSelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu investmentPurposeSelectMenu= new HtmlSelectOneMenu();
//    	private HtmlSelectOneMenu propertyStatusSelectMenu=
    	return "cleared";
    }
    
    public Boolean validateFields() {
    	Boolean validated = true;
		errorMessages = new ArrayList<String>();
		if (propertyView.getPropertyNumber().lastIndexOf(" ")>=0) {
		   errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Property.PROPERTY_NUMBER) + " " + getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.SHOULD_NOT_HAVE_SPACE));
		   validated = false;
		}
		propertyView.setCommercialName(propertyView.getCommercialName().trim());
		propertyView.setEndowedName(propertyView.getEndowedName().trim());
		return validated;
	}
	
    public void setFieldsToView(){
    	logger.logInfo("setFieldsToView() started...");
    	try{
    		if(propertyCategoryId!=null)
    			propertyView.setCategoryId(Long.valueOf(propertyCategoryId));
    		if(propertyTypeId!=null)
    			propertyView.setTypeId(Long.valueOf(propertyTypeId));
    		if(propertyUsageId!=null)
    			propertyView.setUsageTypeId(Long.valueOf(propertyUsageId));
    		if(investmentPurposeId!=null)
    			propertyView.setInvestmentPurposeId(Long.valueOf(investmentPurposeId));
    		if(propertyStatusId!=null)
    			propertyView.setStatusId(Long.valueOf(propertyStatusId));
    		logger.logInfo("setFieldsToView() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("searchAuctions() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("setFieldsToView() crashed ",exception);
    	}
    }
    
    
    @SuppressWarnings("unchecked")
    public void searchVacantUnits(){
     	//loadVacantUnitList();
    	logger.logInfo("searchAuctions() started...");
    	try{
    		if(validateFields())
    		{
    			setFieldsToView();
    			loadAuctionUnitList();
//    			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.SELECTED_AUCTION_UNITS, dataList);
    		}
    		logger.logInfo("searchAuctions() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("searchAuctions() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("searchAuctions() crashed ",exception);
    	}
    }
    
    public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
    
    private Integer filterSelectedUnits(){
    	logger.logInfo("filterSelectedUnits() started...");
    	List<AuctionUnitView> filteredList = new ArrayList<AuctionUnitView>();
    	List<AuctionUnitView> vacantUnitList = new ArrayList<AuctionUnitView>();
    	List<AuctionUnitView> allSelectedUnitListCopy = new ArrayList<AuctionUnitView>();
    	List<AuctionUnitView> allSelectedUnitList = new ArrayList<AuctionUnitView>();
    	Integer totalFiltered = 0;
    	try{
    		if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.VACANT_UNIT_LIST))
    			vacantUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.VACANT_UNIT_LIST);
    		if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
    			allSelectedUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
    		
    		for(AuctionUnitView auctionUnit: allSelectedUnitList)
			{
				AuctionUnitView temp = new AuctionUnitView();
				temp.setUnitId(auctionUnit.getUnitId());
				allSelectedUnitListCopy.add(temp);
			}
				
			if(allSelectedUnitListCopy!=null && !allSelectedUnitListCopy.isEmpty())
			{
	    		for(AuctionUnitView vacantUnit: vacantUnitList){
					for(int i=0; i<allSelectedUnitListCopy.size(); i++){
		    			AuctionUnitView selectedUnit = allSelectedUnitListCopy.get(i);
						if(selectedUnit.getUnitId().compareTo(vacantUnit.getUnitId())==0)
						{
							filteredList.add(vacantUnit);
							totalFiltered++;
						}
					}
					if(allSelectedUnitListCopy.isEmpty())
						break;
				}
	    		vacantUnitList.removeAll(filteredList);
	    		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.VACANT_UNIT_LIST,vacantUnitList);
			}
    		
			logger.logInfo("filterSelectedUnits() completed successfully!!!");
    	}
		catch(Exception exception){
			logger.LogException("filterSelectedUnits() crashed ",exception);
		}
		
		return totalFiltered;
    }
    
    private void loadAuctionUnitList() {
		logger.logInfo("loadAuctionUnitList() started...");
		
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
    	
    	try{		
    		    		
			dataList = propertyServiceAgent.getUnitsAvailableForAuctionByProperty(propertyView);
			
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			try
			{
				List<AuctionUnitView> alreadySelectedUnits = (List<AuctionUnitView>)sessionMap.get(WebConstants.ALREADY_SELECTED_AUCTION_UNITS);
				if(alreadySelectedUnits!=null && alreadySelectedUnits.size() >0)
				{
					for(AuctionUnitView selectedAuctionUnit : alreadySelectedUnits)
					{
						for(AuctionUnitView vacantAuctionUnit : dataList)
						{
							if(selectedAuctionUnit.getUnitId().longValue() == vacantAuctionUnit.getUnitId().longValue())
							{
								dataList.remove(vacantAuctionUnit);
								break;
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				logger.LogException("Failed to exclude selected auction units", ex);
			}
			
			
			UtilityServiceAgent usa = new UtilityServiceAgent();
			
			PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
			
			paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_AUCTION_DEPOSIT_ID);
			paymentConfigurationView.setProcedureTypeKey(WebConstants.PROCEDURE_TYPE_PREPARE_AUCTION);
			paymentConfigurationView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			paymentConfigurationView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);			
			List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
			
			
			Double depositAmount = new Double(0);
			Double minAmount = new Double(0);
			Double maxAmount = Double.MAX_VALUE; 
			String basedOn = "";
			
			if(paymentConfigList != null && paymentConfigList.size() >0)
			{
				paymentConfigurationView = paymentConfigList.get(0);
				if(paymentConfigurationView.getIsFixed().longValue() == 1)
				{
					depositAmount = paymentConfigurationView.getAmount();
				}
				else
				{
					basedOn = paymentConfigurationView.getBasedOnTypeKey();
					minAmount = paymentConfigurationView.getMinAmount();
					maxAmount = paymentConfigurationView.getMaxAmount();
					depositAmount = paymentConfigurationView.getAmount();		
					
				}
			}
			
			for(AuctionUnitView auctionUnitView: dataList)
			{
				if(!basedOn.toString().equals(""))
				{
					if(basedOn.equals(WebConstants.BASED_ON_ANNUAL_RENT)){
						depositAmount = (depositAmount / 100) * auctionUnitView.getRentValue(); 
					}
					else if(basedOn.equals(WebConstants.BASED_ON_OPENING_AMOUNT)){
						depositAmount = (depositAmount / 100) * auctionUnitView.getOpeningPrice();
					}
					
					if(depositAmount < minAmount)
						depositAmount = minAmount;
					else if(depositAmount > maxAmount)
						depositAmount = maxAmount;
						
				}
				auctionUnitView.setDepositAmount(depositAmount);
			}
			
			
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.VACANT_UNIT_LIST, dataList);
			Integer filtered = filterSelectedUnits();
			System.out.println("Filtered : " + filtered);
//			Integer filtered = 0;
			
			recordSize = dataList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			
//			AuctionView temp;
			if(dataList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadAuctionUnitList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadAuctionUnitList() crashed ",exception);
		}
		catch(Exception exception){
			logger.LogException("loadAuctionUnitList() crashed ",exception);
		}
	}	
    


    
    public void selectAuctionUnit(ValueChangeEvent event){
    	int index = dataTable.getRowIndex();
    	AuctionUnitView temp = (AuctionUnitView)dataTable.getRowData();
//    	errorMessages.add("Selected:" + dataTable.getRowIndex() + " " + temp.getUnitNumber() + " " + temp.getSelected() + " " + event.getNewValue().toString());
    	
    	dataList = (List<AuctionUnitView>) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.VACANT_UNIT_LIST);
    	AuctionUnitView auctionUnitView = dataList.remove(index);
    	auctionUnitView.setSelected((Boolean)event.getNewValue());
    	dataList.add(index,auctionUnitView);
    	getFacesContext().getExternalContext().getSessionMap().put(WebConstants.VACANT_UNIT_LIST,dataList);
    }
    
    
    // Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	
	public List<AuctionUnitView> getUnitDataList() {
//		if (dataList == null) {
//			loadVacantUnitList(); // Preload by lazy loading.
//		}
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		if(sessionMap.containsKey(WebConstants.VACANT_UNIT_LIST))
		 	 dataList = (List<AuctionUnitView>) sessionMap.get(WebConstants.VACANT_UNIT_LIST);
		 
		return dataList;
	}
	
	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the auctionUnitView
	 */
	public AuctionUnitView getAuctionUnitView() {
		return auctionUnitView;
	}

	/**
	 * @param auctionUnitView the auctionUnitView to set
	 */
	public void setAuctionUnitView(AuctionUnitView auctionUnitView) {
		this.auctionUnitView = auctionUnitView;
	}

	/**
	 * @return the dataItem
	 */
	public AuctionUnitView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(AuctionUnitView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<AuctionUnitView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<AuctionUnitView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the propertyTypes
	 */
	public List<SelectItem> getPropertyTypes() {
		return propertyTypes;
	}

	/**
	 * @param propertyTypes the propertyTypes to set
	 */
	public void setPropertyTypes(List<SelectItem> propertyTypes) {
		this.propertyTypes = propertyTypes;
	}

	/**
	 * @return the propertyCategories
	 */
	public List<SelectItem> getPropertyCategories() {
		return propertyCategories;
	}

	/**
	 * @param propertyCategories the propertyCategories to set
	 */
	public void setPropertyCategories(List<SelectItem> propertyCategories) {
		this.propertyCategories = propertyCategories;
	}

	/**
	 * @return the propertyUsages
	 */
	public List<SelectItem> getPropertyUsages() {
		return propertyUsages;
	}

	/**
	 * @param propertyUsages the propertyUsages to set
	 */
	public void setPropertyUsages(List<SelectItem> propertyUsages) {
		this.propertyUsages = propertyUsages;
	}

	/**
	 * @return the investmentPurposes
	 */
	public List<SelectItem> getInvestmentPurposes() {
		return investmentPurposes;
	}

	/**
	 * @param investmentPurposes the investmentPurposes to set
	 */
	public void setInvestmentPurposes(List<SelectItem> investmentPurposes) {
		this.investmentPurposes = investmentPurposes;
	}

	/**
	 * @return the propertyStatuses
	 */
	public List<SelectItem> getPropertyStatuses() {
		return propertyStatuses;
	}

	/**
	 * @param propertyStatuses the propertyStatuses to set
	 */
	public void setPropertyStatuses(List<SelectItem> propertyStatuses) {
		this.propertyStatuses = propertyStatuses;
	}

	/**
	 * @return the propertyTypeSelectMenu
	 */
	public HtmlSelectOneMenu getPropertyTypeSelectMenu() {
		return propertyTypeSelectMenu;
	}

	/**
	 * @param propertyTypeSelectMenu the propertyTypeSelectMenu to set
	 */
	public void setPropertyTypeSelectMenu(HtmlSelectOneMenu propertyTypeSelectMenu) {
		this.propertyTypeSelectMenu = propertyTypeSelectMenu;
	}

	/**
	 * @return the propertyCategorySelectMenu
	 */
	public HtmlSelectOneMenu getPropertyCategorySelectMenu() {
		return propertyCategorySelectMenu;
	}

	/**
	 * @param propertyCategorySelectMenu the propertyCategorySelectMenu to set
	 */
	public void setPropertyCategorySelectMenu(
			HtmlSelectOneMenu propertyCategorySelectMenu) {
		this.propertyCategorySelectMenu = propertyCategorySelectMenu;
	}

	/**
	 * @return the propertyUsageSelectMenu
	 */
	public HtmlSelectOneMenu getPropertyUsageSelectMenu() {
		return propertyUsageSelectMenu;
	}

	/**
	 * @param propertyUsageSelectMenu the propertyUsageSelectMenu to set
	 */
	public void setPropertyUsageSelectMenu(HtmlSelectOneMenu propertyUsageSelectMenu) {
		this.propertyUsageSelectMenu = propertyUsageSelectMenu;
	}

	/**
	 * @return the investmentPurposeSelectMenu
	 */
	public HtmlSelectOneMenu getInvestmentPurposeSelectMenu() {
		return investmentPurposeSelectMenu;
	}

	/**
	 * @param investmentPurposeSelectMenu the investmentPurposeSelectMenu to set
	 */
	public void setInvestmentPurposeSelectMenu(
			HtmlSelectOneMenu investmentPurposeSelectMenu) {
		this.investmentPurposeSelectMenu = investmentPurposeSelectMenu;
	}

	/**
	 * @return the propertyStatusSelectMenu
	 */
	public HtmlSelectOneMenu getPropertyStatusSelectMenu() {
		return propertyStatusSelectMenu;
	}

	/**
	 * @param propertyStatusSelectMenu the propertyStatusSelectMenu to set
	 */
	public void setPropertyStatusSelectMenu(
			HtmlSelectOneMenu propertyStatusSelectMenu) {
		this.propertyStatusSelectMenu = propertyStatusSelectMenu;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}


	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if(!isPostBack())
		{
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			sessionMap.remove(WebConstants.VACANT_UNIT_LIST);
		}
		System.out.println("AuctionDetailsBackingBean init");
		try
		{
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);

	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
	}

	/**
	 * @return the propertyView
	 */
	public PropertyView getPropertyView() {
		return propertyView;
	}

	/**
	 * @param propertyView the propertyView to set
	 */
	public void setPropertyView(PropertyView propertyView) {
		this.propertyView = propertyView;
	}

	/**
	 * @return the propertyCategoryId
	 */
	public String getPropertyCategoryId() {
		return propertyCategoryId;
	}

	/**
	 * @param propertyCategoryId the propertyCategoryId to set
	 */
	public void setPropertyCategoryId(String propertyCategoryId) {
		this.propertyCategoryId = propertyCategoryId;
	}

	/**
	 * @return the propertyTypeId
	 */
	public String getPropertyTypeId() {
		return propertyTypeId;
	}

	/**
	 * @param propertyTypeId the propertyTypeId to set
	 */
	public void setPropertyTypeId(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	/**
	 * @return the propertyUsageId
	 */
	public String getPropertyUsageId() {
		return propertyUsageId;
	}

	/**
	 * @param propertyUsageId the propertyUsageId to set
	 */
	public void setPropertyUsageId(String propertyUsageId) {
		this.propertyUsageId = propertyUsageId;
	}

	/**
	 * @return the investmentPurposeId
	 */
	public String getInvestmentPurposeId() {
		return investmentPurposeId;
	}

	/**
	 * @param investmentPurposeId the investmentPurposeId to set
	 */
	public void setInvestmentPurposeId(String investmentPurposeId) {
		this.investmentPurposeId = investmentPurposeId;
	}

	/**
	 * @return the propertyStatusId
	 */
	public String getPropertyStatusId() {
		return propertyStatusId;
	}

	/**
	 * @param propertyStatusId the propertyStatusId to set
	 */
	public void setPropertyStatusId(String propertyStatusId) {
		this.propertyStatusId = propertyStatusId;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	

}