package com.avanza.pims.web.backingbeans.Investment.Opportunities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.OpportunityEvaluationView;

public class OpportunityEvaluationManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = -1679571586771753326L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent;
	private UtilityServiceAgent utilityServiceAgent;
	
	private OpportunityEvaluationView opportunityEvaluationView;
	
	@SuppressWarnings("unchecked")
	public OpportunityEvaluationManageBacking()	
	{
		logger = Logger.getLogger( this.getClass() );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
		utilityServiceAgent = new UtilityServiceAgent();
		
		opportunityEvaluationView = new OpportunityEvaluationView();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");			
			super.init();						
			updateValuesFromMaps();			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}		
	}

	private void updateValuesFromMaps() 
	{
		// OPPORTUNITY EVALUATION VIEW
		if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION ) != null )
		{
			opportunityEvaluationView = (OpportunityEvaluationView) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION );
		}
		
		updateValuesToMaps();		
	}

	private void updateValuesToMaps() 
	{	
		// OPPORTUNITY EVALUATION VIEW
		if ( opportunityEvaluationView != null )
		{
			viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION, opportunityEvaluationView );
		}
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validate() )
			{
				// setting evaluation type using domain data view
				DomainDataView ddvSelectedEvaluationType = utilityServiceAgent.getDomainDataByDomainDataId( Long.valueOf( opportunityEvaluationView.getEvaluationTypeId() ) );
				opportunityEvaluationView.setEvaluationTypeEn( ddvSelectedEvaluationType.getDataDescEn() );
				opportunityEvaluationView.setEvaluationTypeAr( ddvSelectedEvaluationType.getDataDescAr() );
				
				// setting evaluation properties
				opportunityEvaluationView.setCreatedOn( new Date() );
				opportunityEvaluationView.setUpdatedOn( new Date() );
				opportunityEvaluationView.setCreatedBy( CommonUtil.getLoggedInUser() );
				opportunityEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				opportunityEvaluationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				opportunityEvaluationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				// keeping evaluation view in session to be used by the opener backing
				sessionMap.put(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION, opportunityEvaluationView);
				executeJavascript("sendEvaluation();");
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}

	private boolean validate() 
	{
		boolean isValid = true;
		errorMessages=new ArrayList<String>(0);
		if(opportunityEvaluationView.getEvaluationTypeId().equals(""))
			{
				isValid=false;
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.EVALUATION_REQUIRED) );
			}
		if(opportunityEvaluationView.getEvaluationRecommendations()==null || opportunityEvaluationView.getEvaluationRecommendations().trim().length()<=0)
			{
				isValid=false;
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.EVALUATION_RECOMMENDATION_REQUIRED ) );
			}
		if(opportunityEvaluationView.getEvaluationDetails()==null || opportunityEvaluationView.getEvaluationDetails().trim().length()<=0)
			{
				isValid=false;
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.EVALUATION_DETAILS_REQUIRED ) );
			}
		return isValid;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public InvestmentOpportunityServiceAgent getInvestmentOpportunityServiceAgent() {
		return investmentOpportunityServiceAgent;
	}

	public void setInvestmentOpportunityServiceAgent(
			InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent) {
		this.investmentOpportunityServiceAgent = investmentOpportunityServiceAgent;
	}

	public OpportunityEvaluationView getOpportunityEvaluationView() {
		return opportunityEvaluationView;
	}

	public void setOpportunityEvaluationView(
			OpportunityEvaluationView opportunityEvaluationView) {
		this.opportunityEvaluationView = opportunityEvaluationView;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public UtilityServiceAgent getUtilityServiceAgent() {
		return utilityServiceAgent;
	}

	public void setUtilityServiceAgent(UtilityServiceAgent utilityServiceAgent) {
		this.utilityServiceAgent = utilityServiceAgent;
	}
}
