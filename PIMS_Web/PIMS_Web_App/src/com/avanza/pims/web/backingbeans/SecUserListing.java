package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.DomainDataView;

public class SecUserListing extends AbstractController{
	
	private transient Logger logger = Logger.getLogger(SecUserListing.class);
	private String loginId;

	private boolean admin;

	private String userStatus;

	private String secPolicy;



	private String fullName;

	private String fullNameSec;

	private String firstName;

	private String lastName;

	private String middleName;

	private String emailUrl;

	private HtmlDataTable dataTable;

	private UserDbImpl dataItem;

	private HtmlSelectManyListbox groupsOfUserList = new HtmlSelectManyListbox();

	private HtmlInputText loginIdTextBox = new HtmlInputText();
	

	private HtmlSelectManyListbox groupNotBelongsToUserList = new HtmlSelectManyListbox();

	List<SelectItem> groupOfUser = new ArrayList();

	List<SelectItem> groupNotBelongsToUser = new ArrayList();

	


	private HtmlInputHidden listOfGroupsBelongsToUser= new HtmlInputHidden();
	private HtmlInputHidden listOfGroupsNotBelongsToUser= new HtmlInputHidden();

	private HtmlInputText fullNameTextBox = new HtmlInputText();

	private HtmlInputText fullNameSecTextBox = new HtmlInputText();

	private HtmlInputText firstNameTextBox = new HtmlInputText();

	private HtmlInputText lastNameTextBox = new HtmlInputText();

	private HtmlInputText middleNameTextBox = new HtmlInputText();

	private HtmlInputText emailUrlTextBox = new HtmlInputText();

	private HtmlSelectBooleanCheckbox adminCheck = new HtmlSelectBooleanCheckbox();



	private HtmlSelectOneMenu statusSelectMenu = new HtmlSelectOneMenu();

	private HtmlSelectOneMenu policySelectMenu = new HtmlSelectOneMenu();

	List<SelectItem> policies = new ArrayList();

	List<SelectItem> status = new ArrayList();

	private List<User> dataList = new ArrayList();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

	// Actions -----------------------------------------------------------------------------------

	// Getters -----------------------------------------------------------------------------------

	// Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	/** default constructor */
	public SecUserListing() {





	}

	
	private void loadGroupsNotBelongsToUserList() {



		Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

		List<UserGroup> allGroupsList = SecurityManager.findGroup(query);


		groupNotBelongsToUser = new ArrayList();

		for (int i = 0; i < allGroupsList.size(); i++) {
			groupNotBelongsToUser.add(new SelectItem(allGroupsList.get(i).getUserGroupId(),
					allGroupsList.get(i).getPrimaryName()));
			getGroupNotBelongsToUserList().setValue(new SelectItem(groupNotBelongsToUser.get(i)));
		}



	}

	private void loadGroupsOfUserList(UserDbImpl user) {
		//Here we need to change the source of data
		//with the list of POJO



		Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

		Set userGroupSet = user.getGroupBindings();
		String strGroupId;
		UserGroup objUserGroup;
		List groupsList= new ArrayList();
		Iterator it =userGroupSet.iterator();

		while(it.hasNext()){
			it.next();
			strGroupId = ((GroupUserBinding)userGroupSet.iterator().next()).getUserGroupBinding().getUserGroupId();


			objUserGroup = SecurityManager.getGroup(strGroupId);
			groupsList.add(objUserGroup);

		}
		for (int i = 0; i < groupsList.size(); i++) {
			groupOfUser.add(new SelectItem(((UserGroup)groupsList.get(i)).getUserGroupId(),
					((UserGroup)groupsList.get(i)).getPrimaryName()));
			getGroupsOfUserList().setValue(new SelectItem(groupOfUser.get(i)));
		}


	}
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false; 
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
    
	// Property accessors

	private void loadStatusList() {
		  try
		  {
			  UtilityServiceAgent utilityServiceAgent=new UtilityServiceAgent();
			  if(!getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.User.USER_STATUS_IN_SESSION))
			  {
			    List<DomainDataView> ddv= (ArrayList)utilityServiceAgent.getDomainDataByDomainTypeName(WebConstants.User.USER_STATUS);
				for (int i = 0; i < ddv.size(); i++) {

					if (getIsEnglishLocale())
						status.add(new SelectItem(ddv.get(i).getDomainDataId().toString(),ddv.get(i).getDataDescEn()));
					else
						status.add(new SelectItem(ddv.get(i).getDomainDataId().toString(),ddv.get(i).getDataDescAr()));						

					
				}
			    getFacesContext().getExternalContext().getSessionMap().put(WebConstants.User.USER_STATUS_IN_SESSION, status);
			  }
			  else{
				  status = ((List<SelectItem>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.User.USER_STATUS_IN_SESSION));

			  }
			  
			  
		  }
		  catch (Exception e)
		  {
			e.printStackTrace();
		  }
		
//		USER_STATUS
		
	

	}




	public User getDataItem() {
		return dataItem;
	}

	public void setDataItem(UserDbImpl dataItem) {
		this.dataItem = dataItem;
	}

	public List<User> getDataList() 
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<User>)viewMap.get("SEARCHED_USER_LIST");
		if(dataList==null)
			dataList = new ArrayList<User>();
		return dataList;
	}

	public void setDataList(List<User> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getEmailUrl() {
		return emailUrl;
	}

	public void setEmailUrl(String emailUrl) {
		this.emailUrl = emailUrl;
	}

	public HtmlInputText getEmailUrlTextBox() {
		return emailUrlTextBox;
	}

	public void setEmailUrlTextBox(HtmlInputText emailUrlTextBox) {
		this.emailUrlTextBox = emailUrlTextBox;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public HtmlInputText getFirstNameTextBox() {
		return firstNameTextBox;
	}

	public void setFirstNameTextBox(HtmlInputText firstNameTextBox) {
		this.firstNameTextBox = firstNameTextBox;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullNameSec() {
		return fullNameSec;
	}

	public void setFullNameSec(String fullNameSec) {
		this.fullNameSec = fullNameSec;
	}

	public HtmlInputText getFullNameSecTextBox() {
		return fullNameSecTextBox;
	}

	public void setFullNameSecTextBox(HtmlInputText fullNameSecTextBox) {
		this.fullNameSecTextBox = fullNameSecTextBox;
	}

	public HtmlInputText getFullNameTextBox() {
		return fullNameTextBox;
	}

	public void setFullNameTextBox(HtmlInputText fullNameTextBox) {
		this.fullNameTextBox = fullNameTextBox;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public HtmlInputText getLastNameTextBox() {
		return lastNameTextBox;
	}

	public void setLastNameTextBox(HtmlInputText lastNameTextBox) {
		this.lastNameTextBox = lastNameTextBox;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public HtmlInputText getMiddleNameTextBox() {
		return middleNameTextBox;
	}

	public void setMiddleNameTextBox(HtmlInputText middleNameTextBox) {
		this.middleNameTextBox = middleNameTextBox;
	}


	public HtmlSelectOneMenu getPolicySelectMenu() {
		return policySelectMenu;
	}

	public void setPolicySelectMenu(HtmlSelectOneMenu policySelectMenu) {
		this.policySelectMenu = policySelectMenu;
	}

	public String getSecPolicy() {
		return secPolicy;
	}

	public void setSecPolicy(String secPolicy) {
		this.secPolicy = secPolicy;
	}

	public HtmlSelectOneMenu getStatusSelectMenu() {
		return statusSelectMenu;
	}

	public void setStatusSelectMenu(HtmlSelectOneMenu statusSelectMenu) {
		this.statusSelectMenu = statusSelectMenu;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public List<User> getSecUserDataList() 
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<User>)viewMap.get("SEARCHED_USER_LIST");
		if(dataList==null)
			dataList = new ArrayList<User>();
		return dataList;
	}

	//Public Methods

	// Actions -----------------------------------------------------------------------------------

	public String editDataItem() 
	{		
		try 
		{
			if(dataTable.isRowAvailable())
			{
				Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
				UserDbImpl user=(UserDbImpl)dataTable.getRowData();			
				
				sessionMap.put(WebConstants.User.EDIT_USER,user);				
			}
			logger.logInfo("editDataItem() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editDataItem() crashed ", exception);
		}				
		return "edit";
	}

	
	//Classes which should be implemented in Business Logic or from where we r fetching data
	
	/*	    public List getPagedData(SomeCriteriaObject someObject, int start, int page) {
	 try {
	 Criteria criteria = getSession().createCriteria(ClassToBeQueried.class);
	 //Build Criteria object here
	 criteria.setFirstResult(start);
	 criteria.setMaxResults(page);
	 return criteria.list();
	 } catch (HibernateException hibernateException) {
	 //do something here with the exception
	 }
	 }        





	 public int getDataCount(SomeCriteriaObject someObject) {
	 Criteria criteria = getSession().createCriteria(ClassToBeQueried.class);
	 criteria.setProjection(Projections.rowCount());

	 // Build Criteria object here
	 Number nuofRecords = ((Number) criteria.uniqueResult());
	 return nuofRecords == null ? 0 : nuofRecords.intValue();
	 }        
	 */

	@SuppressWarnings("unchecked")
	private void loadDataList() {

		Search query = new Search();

		query.clear();

		query.addFrom(UserDbImpl.class);
		dataList = new ArrayList<User>();
		
		if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.User.USER_DATA_LIST)!=null)
		{
			dataList = (List<User>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.User.USER_DATA_LIST);
			recordSize = dataList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
		
		}
		else
		{
			List<User> userList = SecurityManager.findUser(query);
			recordSize = userList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			dataList.addAll( userList);
		}

	}

	public String reset() {
		dataList= null;
		firstNameTextBox.setValue("");
		middleNameTextBox.setValue("");
		lastNameTextBox.setValue("");
		fullNameTextBox.setValue("");
		emailUrlTextBox.setValue("");
		loginIdTextBox.setValue("");
		adminCheck.setSelected(false);
		
		return "success";
	}
	private ArrayList getGroupList(String strGroupListCommaSeperated){
		UserGroupDbImpl objUserGroup = new UserGroupDbImpl();



		ArrayList<String> ret=new ArrayList();
		String strArr[]= strGroupListCommaSeperated.split(",");
		for(int i = 0;i<strArr.length;i++){
			if (!ret.contains(strArr[i])) ret.add(strArr[i]);
		}




		return ret;


	}
	public String addUser(){
		return "addUser";
	}
	@SuppressWarnings("unchecked")
	public String searchUser()
	{
		Search search = new Search();
		search.clear();
		search.addFrom(UserDbImpl.class);

	
		Criterion criterion;
		if(emailUrlTextBox.getValue()!=null && emailUrlTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("emailUrl", "%" + emailUrlTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		if(fullNameTextBox.getValue()!=null&& fullNameTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("fullName", "%" + fullNameTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		if(firstNameTextBox.getValue()!=null && firstNameTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("firstName", "%" + firstNameTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		if(middleNameTextBox.getValue()!=null && middleNameTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("middleName", "%" + middleNameTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		if(lastNameTextBox.getValue()!=null && lastNameTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("lastName", "%" + lastNameTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		if(fullNameSecTextBox.getValue()!=null && fullNameSecTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("secondaryFullName", "%" + fullNameSecTextBox.getValue().toString() + "%");
			search.addCriterion(criterion);
		}
		if(loginIdTextBox.getValue().toString().trim().length()>0){
			criterion = SimpleCriterion.like("loginId", "%" +loginIdTextBox.getValue().toString() + "%",true);
			search.addCriterion(criterion);
		}
		
		if(userStatus != null && !userStatus.toString().equals("") && !userStatus.toString().equals("-1"))
		{
			criterion = SimpleCriterion.equal("statusId",userStatus);
			search.addCriterion(criterion);
		}
//		criterion = SimpleCriterion.equal( "recordStatus", new Integer(1),true);
//		search.addCriterion(criterion);
		
		List<User> userList = SecurityManager.findUser(search);
		
		List<User> newUserList = new ArrayList<User>();  
		for(User user :userList)
		{
			if( user.getRecordStatus() == null)
			{
				continue;
			}
			user.getUserGroups();
			newUserList.add(user);
			
		}
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put("SEARCHED_USER_LIST", newUserList);
		recordSize = newUserList.size();
		viewMap.put("recordSize", recordSize);
		paginatorRows = getPaginatorRows();
		return "searched";
	}
	

	public boolean isAdmin() {

		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public HtmlSelectBooleanCheckbox getAdminCheck() {
		return adminCheck;
	}

	public void setAdminCheck(HtmlSelectBooleanCheckbox adminCheck) {
		this.adminCheck = adminCheck;
	}

	public List<SelectItem> getPolicies() {
		return policies;
	}

	public void setPolicies(List<SelectItem> policies) {
		this.policies = policies;
	}

	public List<SelectItem> getStatus() {
		return status;
	}

	public void setStatus(List<SelectItem> status) {
		this.status = status;
	}

	public List<SelectItem> getGroupNotBelongsToUser() {
		return groupNotBelongsToUser;
	}

	public void setGroupNotBelongsToUser(List<SelectItem> groupNotBelongsToUser) {
		this.groupNotBelongsToUser = groupNotBelongsToUser;
	}

	public HtmlSelectManyListbox getGroupNotBelongsToUserList() {
		return groupNotBelongsToUserList;
	}

	public void setGroupNotBelongsToUserList(
			HtmlSelectManyListbox groupNotBelongsToUserList) {
		this.groupNotBelongsToUserList = groupNotBelongsToUserList;
	}

	public List<SelectItem> getGroupOfUser() {
		return groupOfUser;
	}

	public void setGroupOfUser(List<SelectItem> groupOfUser) {
		this.groupOfUser = groupOfUser;
	}

	public HtmlSelectManyListbox getGroupsOfUserList() {
		return groupsOfUserList;
	}

	public void setGroupsOfUserList(HtmlSelectManyListbox groupsOfUserList) {
		this.groupsOfUserList = groupsOfUserList;
	}

	

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		loadStatusList();
		
//		loadGroupsNotBelongsToUserList();
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	public HtmlInputHidden getListOfGroupsBelongsToUser() {
		return listOfGroupsBelongsToUser;
	}

	public void setListOfGroupsBelongsToUser(
			HtmlInputHidden listOfGroupsBelongsToUser) {
		this.listOfGroupsBelongsToUser = listOfGroupsBelongsToUser;
	}

	public HtmlInputHidden getListOfGroupsNotBelongsToUser() {
		return listOfGroupsNotBelongsToUser;
	}

	public void setListOfGroupsNotBelongsToUser(
			HtmlInputHidden listOfGroupsNotBelongsToUser) {
		this.listOfGroupsNotBelongsToUser = listOfGroupsNotBelongsToUser;
	}

	public HtmlInputText getLoginIdTextBox() {
		return loginIdTextBox;
	}

	public void setLoginIdTextBox(HtmlInputText loginIdTextBox) {
		this.loginIdTextBox = loginIdTextBox;
	}

	//Private Methods

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


}