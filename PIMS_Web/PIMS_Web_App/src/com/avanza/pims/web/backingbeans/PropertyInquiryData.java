package com.avanza.pims.web.backingbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.dao.InquiryManager;
import com.avanza.pims.entity.Inquiry;
import com.avanza.pims.entity.Unit;


public class PropertyInquiryData  implements Serializable {
	
	private String unitNumber;
	
	private String unitDesc;
	 
	private String createdBy;
	
	private String createdOn;
	
    private Double unitArea;

    private Long noOfBed;
    
    private Long noOfLiving;
    
    private Long noOfBath;
    
    private String dewaNumber;
    
    private Double rentValue;
    
    private Double requiredPrice;


	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public Double getUnitArea() {
		return unitArea;
	}

	public void setUnitArea(Double unitArea) {
		this.unitArea = unitArea;
	}


	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getUnitDesc() {
		return unitDesc;
	}

	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}



	public Long getNoOfLiving() {
		return noOfLiving;
	}

	public void setNoOfLiving(Long noOfLiving) {
		this.noOfLiving = noOfLiving;
	}

	public Long getNoOfBath() {
		return noOfBath;
	}

	public void setNoOfBath(Long noOfBath) {
		this.noOfBath = noOfBath;
	}

	public String getDewaNumber() {
		return dewaNumber;
	}

	public void setDewaNumber(String dewaNumber) {
		this.dewaNumber = dewaNumber;
	}

	public Double getRentValue() {
		return rentValue;
	}

	public void setRentValue(Double rentValue) {
		this.rentValue = rentValue;
	}

	public Double getRequiredPrice() {
		return requiredPrice;
	}

	public void setRequiredPrice(Double requiredPrice) {
		this.requiredPrice = requiredPrice;
	}

	public Long getNoOfBed() {
		return noOfBed;
	}

	public void setNoOfBed(Long noOfBed) {
		this.noOfBed = noOfBed;
	}
	
}
