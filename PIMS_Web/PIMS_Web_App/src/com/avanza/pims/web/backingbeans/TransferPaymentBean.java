package com.avanza.pims.web.backingbeans;

import java.io.Serializable;

import com.avanza.pims.ws.vo.PaymentScheduleView;

public class TransferPaymentBean implements Serializable {
	
	private String category;
	private String paymentType;
	private String paymentDate;
	private String dueDate;
	private String amount;
	private String status;
	private String recieptNo;
	private String paymentMethod;
	private PaymentScheduleView paymentScheduleView;
	
	public PaymentScheduleView getPaymentScheduleView() {
		return paymentScheduleView;
	}
	public void setPaymentScheduleView(PaymentScheduleView paymentScheduleView) {
		this.paymentScheduleView = paymentScheduleView;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getRecieptNo() {
		return recieptNo;
	}
	public void setRecieptNo(String recieptNo) {
		this.recieptNo = recieptNo;
	}
}
