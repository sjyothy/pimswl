package com.avanza.pims.web.backingbeans;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

//import pimscontractbpelproxy.ContractInitiateBPELPortClient;

import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;

public class ContractDetailTestBackingBaean extends AbstractController {
	
	private String contractId;
	private String outcome;
	private UserTask userTask;
	
	
	@Override
	public void init() {
		
		//Get userTask by Request
		//userTask = (UserTask) getRequestParam(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		
		//Get userTask by Session
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		userTask = (UserTask) session.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		
		setContractId(userTask.getTaskAttributes().get("CONTRACT_ID").toString());
	}
	
	public String claimAndCompleteTask(){
		
		
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		try {
			
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			//bpmWorkListClient.getTasksForUser("admin");
			
			//bpmWorkListClient.accquireTask(userTask, "Afzal");
			//TaskOutcome taskOutcome = TaskOutcome.APPROVE;
			bpmWorkListClient.completeTask(userTask, "admin", TaskOutcome.APPROVE);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	
	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	
	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

}
