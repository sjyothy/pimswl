package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.div.Div;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.dao.DocumentManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DocumentTypeView;
import com.avanza.ui.util.ResourceUtil;

public class DocumentTypeController  extends AbstractSearchBean{

    /**
     * @author Imran Ali
     */
    private static final long serialVersionUID = -1009429671577769550L;

    private List<DocumentTypeView> docTypeList = new ArrayList<DocumentTypeView>();
    private DocumentTypeView documentTypeView = new DocumentTypeView();
    
    private static Logger logger = Logger.getLogger( DocumentTypeController.class );
	
	private DocumentManager documentManager = new DocumentManager();
	
	UtilityService service = new UtilityService();
	
	private static final String DOCUMENT_TYPE_LIST = "DOUCMENT_TYPE_LIST";
	private static final String DOCUMENT_TYPE_FORM = "DOUCMENT_TYPE_FORM";

	private static final String DEFAULT_SORT_FIELD = "descriptionEn";
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
    {
		super.init();
    	String methodName="init";
    	logger.logInfo(methodName+"|"+"Start");
   	 	if(!isPostBack() )
   	 	{
   	 		
   	 		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
			setSortField(DEFAULT_SORT_FIELD);
			setSortItemListAscending(false);
			checkDataInQueryString();
			pageFirst();
   	 	}  	 	
   	 	//Specifically for NOL and open file because if a nol/file type is selected then in not post back it will bring Proc Doc
   	 	//but if in the same pagescope the nol/file type is changed then it will not go into the getProcDoc although we 
   	 	//require Proc doc to be updated
   	 	else 
   	 	{
   	 		updateValuesFromMap();
   	 	}
	    
	    
       logger.logInfo(methodName+"|"+"Finish");
    }

	private void getDocumentTypeList() {
		
		try {
			updateValuesFromMap();
			setTotalRows( service.getDocumentTypes().size());
//			setTotalRows(docTypeList.size());
//			setRecordSize(docTypeList.size());
			doPagingComputations();
			docTypeList = documentManager.searchDocumentTypeViewCriteria(documentTypeView,getRowsPerPage(),getCurrentPage(),
																		 getSortField(),isSortItemListAscending(),isEnglishLocale());
			forPaging(getTotalRows());
			updateValuesToMap();
		} catch (PimsBusinessException e) {
			errorMessages = new ArrayList<String>(0);
	 		logger.LogException("getDocumentTypeList"+"|Error Occured", e);
	 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		 catch (Exception e) {
				errorMessages = new ArrayList<String>(0);
		 		logger.LogException("getDocumentTypeList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				
			}
		
		
	}
	
	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	
	
	public void doSearchItemList()
	{
		try {
			
			getDocumentTypeList();
			
		}
		    
		catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    
		}
	}

	/**
	 * @return the docTypeList
	 */
	public List<DocumentTypeView> getDocTypeList() {
		
		return docTypeList;
	}

	/**
	 * @param docTypeList the docTypeList to set
	 */
	public void setDocTypeList(List<DocumentTypeView> docTypeList) {
		this.docTypeList = docTypeList;
	}

	/**
	 * @return the documentTypeView
	 */
	public DocumentTypeView getDocumentTypeView() {
		return documentTypeView;
	}

	/**
	 * @param documentTypeView the documentTypeView to set
	 */
	public void setDocumentTypeView(DocumentTypeView documentTypeView) {
		this.documentTypeView = documentTypeView;
	}

	@Override
	public void onSearch() {
		
	}
	
	public void onAdd(){
		errorMessages = new ArrayList<String>(0);
		String methodName  = "onAdd";
		logger.logInfo(methodName+"|"+"Start");
//		if(viewMap.get(DOCUMENT_TYPE_LIST) != null){
//			docTypeList = ((List<DocumentTypeView>)viewMap.get(DOCUMENT_TYPE_LIST));
			try {
				if(hasErrors()){
					return;
				}
				if(documentTypeView.getDocumentTypeId() == null && service.checkDocumentTypeExist(documentTypeView)){
					errorMessages.add(ResourceUtil.getInstance().getProperty("documentType.err.fileNetIdExist"));
				}
				else{
					Long id = service.addUpdateDocumentType(documentTypeView,getLoggedInUserId());
					if(id != null){
						documentTypeView.setDocumentTypeId(id);
						docTypeList.add(0,documentTypeView);
						documentTypeView = new DocumentTypeView();
						documentTypeView.setShowEdit(false);
						updateValuesToMap();
						successMessages.add(CommonUtil.getBundleMessage("common.messages.Save"));
					}
				}
			} catch (PimsBusinessException e) {
				e.printStackTrace();
				errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		logger.logInfo(methodName+"|"+"Finished");
		
	}
	
	private boolean hasErrors() {
		
		boolean hasErrors=  false;
			
		if(StringUtils.isBlank(documentTypeView.getDescriptionEn()))
		{
			errorMessages.add(CommonUtil.getBundleMessage("documentType.err.descEn"));
			return true;
			
		}
		
		if(StringUtils.isBlank(documentTypeView.getDescriptionAr()))
		{
			errorMessages.add(CommonUtil.getBundleMessage("documentType.err.descAr"));
			return true;
			
		}
		
		if( null == (documentTypeView.getFileNetTypeId()))
		{
			errorMessages.add(CommonUtil.getBundleMessage("documentType.err.fileNetDocId"));
			return true;
			
		}
		
		return hasErrors;
	}

	public void onEdit(){
		errorMessages = new ArrayList<String>(0);
		String methodName  = "onEdit";
		logger.logInfo(methodName+"|"+"Start");
		try{
			documentTypeView = ((DocumentTypeView)dataTable.getRowData());
			docTypeList.remove(docTypeList.indexOf(documentTypeView));
			documentTypeView.setShowEdit(true);
			updateValuesToMap();
		}catch(Exception ex){
			logger.LogException(methodName+"|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public void onClear(){
		errorMessages = new ArrayList<String>(0);
		String methodName  = "onClear";
		logger.logInfo(methodName+"|"+"Start");
		try{
			if(documentTypeView.getDocumentTypeId() != null){
				docTypeList.add(0,documentTypeView);
				documentTypeView = new DocumentTypeView();
			}
			documentTypeView.setShowEdit(false);
			updateValuesToMap();
		}catch(Exception ex){
			logger.LogException(methodName+"|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	private void updateValuesToMap() {
			if( docTypeList != null )
			{
				 viewMap.put( DOCUMENT_TYPE_LIST ,docTypeList ) ;
			}
			if( documentTypeView != null )
			{
				 viewMap.put( DOCUMENT_TYPE_FORM ,documentTypeView ) ;
			}
			
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() 
	{
		if( viewMap.get( DOCUMENT_TYPE_LIST ) != null )
		{
			docTypeList = (List<DocumentTypeView>)viewMap.get( DOCUMENT_TYPE_LIST );
		}
		
		if( viewMap.get( DOCUMENT_TYPE_FORM ) != null )
		{
			documentTypeView = (DocumentTypeView)viewMap.get( DOCUMENT_TYPE_FORM );
		}
		
		updateValuesToMap();
	}
    
	
}