package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BidderAuctionRegSearchView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.ui.util.ResourceUtil;

public class AuctionDepositRefundApproval extends AbstractController{
	
	public class PageOutcomes
	{
		public static final String FailedToSaveAuctionRequest = "FailedToSaveAuctionRequest";	 
		public static final String SuccessToSaveAuctionRequest = "SuccessToSaveAuctionRequest";
		public static final String FailedToApproveAuctionRequest = "FailedToApproveAuctionRequest";
		public static final String SuccessToApproveAuctionRequest = "SuccessToApproveAuctionRequest";
	}
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private BidderAuctionRegView bidderAuctionRegView = new BidderAuctionRegView();
	private HtmlDataTable dataTable;
	private String sortField = null;
    private boolean sortAscending = true;
	private HtmlCommandLink btnAction = new HtmlCommandLink();
	private HtmlInputHidden addCount;
	private HtmlInputText inputTextBidderNameSearch = new HtmlInputText();
	private List<String> errorMessages = new ArrayList<String>(0);
	private List<String> successMessages = new ArrayList<String>(0);
	private Boolean showActionButtons= true;
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String shortDateFormat = "";
	private String longDateFormat = "";
	
	private static Logger logger = Logger.getLogger( AuctionDepositRefundApproval.class );

	// Actions -----------------------------------------------------------------------------------
	
	public HtmlInputText getInputTextBidderNameSearch() {
		return inputTextBidderNameSearch;
	}
	public void setInputTextBidderNameSearch(HtmlInputText inputTextBidderNameSearch) {
		this.inputTextBidderNameSearch = inputTextBidderNameSearch;
	}
	
	public String getShortDateFormat()
	{
		SystemParameters parameters = SystemParameters.getInstance();
		shortDateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		return shortDateFormat;
	}
	public String getLongDateFormat()
	{
		SystemParameters parameters = SystemParameters.getInstance();
		longDateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		return longDateFormat;
	}
	public Boolean getShowActionButtons()
	{
		showActionButtons = (Boolean)viewMap.get("showActionButtons");
		if(showActionButtons == null)
			showActionButtons = true;
		return showActionButtons;
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public BidderAuctionRegView getBidderAuctionRegView() {
		return bidderAuctionRegView;
	}

	public void setABidderAuctionRegView(BidderAuctionRegView dataItem) {
		this.bidderAuctionRegView = dataItem;
	}
	
	public AuctionView getAuctionView()
	{
		AuctionView auctionView = null;
		Object obj =  getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AuctionDepositRefundApprovalScreen.AUCTION_VO);
		if(obj != null)
			auctionView = (AuctionView) obj ;
		return auctionView;
	}
	public ArrayList<BidderAuctionRegView> getBidderAuctionRegList() {
		ArrayList<BidderAuctionRegView> bidderAuctionRegViewList = (ArrayList<BidderAuctionRegView>) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AuctionDepositRefundApprovalScreen.BIDDER_AUCTION_REG_LIST);
		return bidderAuctionRegViewList;
	}
	
	public Integer getRecordSize(){
		Object list = getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AuctionDepositRefundApprovalScreen.BIDDER_AUCTION_REG_LIST);
		if(list != null){
			return ((ArrayList<BidderAuctionRegView>)list).size(); 
		}
		else{
			return 0;
		}
	}

	private void loadAuctionData() {
		
		logger.logInfo("Stepped into the loadAuctionData method");		
		try {
			
			UserTask task = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			if(task !=null)
			{
				PropertyServiceAgent psAgent = new PropertyServiceAgent();
				Map taskAttributes =  task.getTaskAttributes();				
				long auctionId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.AuctionDepositRefundApproval.AUCTION_ID));
				BidderAuctionRegSearchView bidderAuctionRegSearchView = new BidderAuctionRegSearchView();
			
				bidderAuctionRegSearchView.setAuctionId(auctionId);
				if(inputTextBidderNameSearch!= null && inputTextBidderNameSearch.getValue()!=null && !inputTextBidderNameSearch.getValue().equals(""))
				{
					bidderAuctionRegSearchView.setBidderName((String)inputTextBidderNameSearch.getValue());
				}				
				ArrayList<BidderAuctionRegView> bidderAuctionRegViewList = new ArrayList();
				ArrayList<BidderAuctionRegView> tempBidderAuctionRegViewList = psAgent.getAllBidderAuctionRegforLostBidders(bidderAuctionRegSearchView);
				ResourceBundle bundle = 
					ResourceBundle.getBundle(getFacesContext().getApplication()
							.getMessageBundle(), getFacesContext().getViewRoot().getLocale());
				
				BidderAuctionRegView bidderAuctionRegView;
				Iterator itBidderAuctionRegView = tempBidderAuctionRegViewList.iterator();
				while(itBidderAuctionRegView.hasNext()){
					bidderAuctionRegView = (BidderAuctionRegView)itBidderAuctionRegView.next();
					if(bidderAuctionRegView.getAttended().equals("Y"))
					{
						
							bidderAuctionRegView.setAttendedDisplay(bundle.getString("commons.yes"));
						
							
							
					}
					else{
						bidderAuctionRegView.setAttendedDisplay(bundle.getString("commons.no"));
					}
					bidderAuctionRegViewList.add(bidderAuctionRegView);
				}
				AuctionView actuinView =  psAgent.getAuctionById(auctionId);
				
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AuctionDepositRefundApprovalScreen.AUCTION_VO, actuinView);
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AuctionDepositRefundApprovalScreen.BIDDER_AUCTION_REG_LIST, bidderAuctionRegViewList);
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AuctionDepositRefundApprovalScreen.USER_TASK, task);
			}
	        
		}
		catch (Exception e) {
			
			logger.logError("loadAuctionData method crashed due to:"+e.getStackTrace());
			e.printStackTrace();
		}
		
		logger.logInfo("Stepped out of the loadAuctionData method");
	}
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
//		if (sortField != null) {
//			Collections.sort(userTasks, new ListComparator(sortField,
//					sortAscending));
//			
//		}
	}

	// Getters -----------------------------------------------------------------------------------


	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------


	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	//** default constructor *//*
	public AuctionDepositRefundApproval() {
		//addCount.setValue(0);
	}


	// Property accessors


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	//Public Methods

	// Actions -----------------------------------------------------------------------------------


	//Classes which should be implemented in Business Logic or from where we r fetching data	    

	public String reset() {

		return "success";
	}


	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public int getDataCount() {
		int rows = dataTable.getRows();
		int count = dataTable.getRowCount();
		return (count / rows) + ((count % rows != 0) ? 1 : 0);
	}        


	@Override
	public void init() {
		// TODO Auto-generated method stub
		
		if(!isPostBack())
			loadAuctionData();

		super.init();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	public String actionClick(){
		return "";
	}
	
	
	public void setBtnAction(HtmlCommandLink btnAction) {
		this.btnAction = btnAction;
	}


	public HtmlCommandLink getBtnAction() {
		return btnAction;
	}
	
	public String saveAuctionRequests(){
		String outcome = AuctionDepositRefundApproval.PageOutcomes.FailedToSaveAuctionRequest;
		try
		{
			PropertyServiceAgent psAgent = new PropertyServiceAgent();			
			ArrayList<BidderAuctionRegView> bidderAuctionRegViewList = (ArrayList<BidderAuctionRegView>) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AuctionDepositRefundApprovalScreen.BIDDER_AUCTION_REG_LIST);
			
			psAgent.updateBidderAuctionRegs(bidderAuctionRegViewList);			
			outcome = AuctionDepositRefundApproval.PageOutcomes.SuccessToSaveAuctionRequest;
			successMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.SUCCESS_SAVE));
		}
		catch(Exception ex)
		{
			logger.logError("Failed to save auction Requests due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_SAVE));
			ex.printStackTrace();		
		}
		return outcome;
	}
	public String approveAuctionRequests(){
		String outcome = AuctionDepositRefundApproval.PageOutcomes.FailedToApproveAuctionRequest;
		try
		{
			saveAuctionRequests();
			UserTask userTask = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AuctionDepositRefundApprovalScreen.USER_TASK);						
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
			outcome = AuctionDepositRefundApproval.PageOutcomes.SuccessToApproveAuctionRequest;
			successMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.SUCCESS_APPROVE));
			viewMap.put("showActionButtons",false);
		}
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE));
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			logger.logError("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE));
			ex.printStackTrace();
		}
		return outcome;
	}	
	public String searchBidderAuctionReg(){
		
		try
		{
			loadAuctionData();
		}

		catch(Exception ex)
		{
			logger.logError("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.AuctionDepositRefundApprovalScreen.Messages.FAIL_APPROVE));
			ex.printStackTrace();
		}
		
		return "";
	}	
	public void editBidderAuctionRefund(ActionEvent event){		
	
		BidderAuctionRegView  bidderAuctionRegView  = (BidderAuctionRegView) dataTable.getRowData();
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AuctionDepositRefundApprovalScreen.SELECTED_BIDDER_AUCTION_REG, bidderAuctionRegView);
		
	    final String viewId = "/AuctionDepositRefundApproval.jsf"; 
	    
	    FacesContext facesContext = FacesContext.getCurrentInstance();

	    // This is the proper way to get the view's url
	    ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	    String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	    
	    String javaScriptText = "javascript:showPopup();";
	    
	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
		/*String messageList;
		if ((errorMessages == null) ||
			(errorMessages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for(String message: errorMessages) {
				messageList = messageList + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);*/
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
		/*String messageList;
		if ((successMessages == null) ||
			(successMessages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<FONT COLOR=GREEN><B><UL>\n";
			for(String message: successMessages) {
				messageList = messageList + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);*/
	}
	


}

