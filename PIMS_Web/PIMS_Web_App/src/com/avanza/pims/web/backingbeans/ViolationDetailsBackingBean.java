package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;



import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.proxy.pimsinspectionbpelproxy.PIMSInspectionBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ViolationView;

public class ViolationDetailsBackingBean extends AbstractController {

	private InspectionViolationView inspectionViolationView;
	private List<ViolationView> violationView;
	private HtmlDataTable dataTable;
	private String violationDate ;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private ResourceBundle bundle;
	//Variables for fields
	private String unitRefNo;
	private String unitType;
	private String contractRefNo;
	private String propertyName;
	private String description;
	private String unitStatus;
	
	private String violationType;
	private String violationCategory;
	private String damageAmount;
	private Long inspectionId;
	private Long unitId;
	private Long contractUnitId;
	private boolean fromList;
	private boolean editable;
	
	public Date violationDateRich; 
	
	UserTask userTask;
	
	private ContractView contractView;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	List<SelectItem> typeList=new ArrayList();
	List<SelectItem> categoryList=new ArrayList();
	private List<String> errorMessages;
	private static Logger logger = Logger.getLogger(ViolationDetailsBackingBean.class);
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();	
		
		String methodName="init";
    	logger.logInfo(methodName+"|"+" Start");
    	
    	

		
		if (!isPostBack()){
			
			if (getRequestParam("fromInsList") != null){
				fromList = true;
			}
			else
			{
				fromList = false;
			}
			
			if (getRequestParam("unitId") != null){
			
			unitId = (Long)getRequestParam("unitId");
			String inspectionString =getRequestParam("inspectionId").toString();
			inspectionId =Long.parseLong(inspectionString);
			
			contractRefNo = (String) getRequestParam("contractNumber");
			
			}
			else
			{
			
				
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request =(HttpServletRequest)context.getExternalContext().getRequest();
			
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			userTask = (UserTask) session.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			if(userTask != null )
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put( WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
			
			session.removeAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			if (userTask != null){
				
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				BPMWorklistClient bpmWorkListClient;
				
					
					try {
						bpmWorkListClient = new BPMWorklistClient(contextPath);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					unitId =Long.parseLong(userTask.getTaskAttributes().get("UNIT_ID"));
					inspectionId =Long.parseLong(userTask.getTaskAttributes().get("INSPECTION_ID"));
			}
			}
			
			loadCombos();
			getIsEnglishLocale();
			getIsArabicLocale();
			getInspectionViolation();
			
			getIsEditable();
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		
	}
	
	private void getIsEditable() {
		
		try {
		
			editable = true;
			
		if (fromList == true)
		{
			
			DomainDataView domainDataStatusActionsDefined = psa.getDomainDataByValue(WebConstants.VIOLATION_STATUS, WebConstants.VIOLATION_STATUS_ACTIONS_DEFINED);
			DomainDataView domainDataStatusSent = psa.getDomainDataByValue(WebConstants.VIOLATION_STATUS, WebConstants.VIOLATION_STATUS_SENT);
			
			if (domainDataStatusSent != null && domainDataStatusActionsDefined!=null)
			{
				for (int i=0; i<violationView.size(); i++)
				{
					if (violationView.get(i).getStatusId().longValue() == domainDataStatusActionsDefined.getDomainDataId().longValue())
						editable = false;
					
					if (violationView.get(i).getStatusId().longValue() == domainDataStatusSent.getDomainDataId().longValue())
						editable = false;
				}
			}
		}
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void getContractView(){
		
		String methodName="getContractView";
    	logger.logInfo(methodName+"|"+" Start");
		
		try {
			
			contractView = psa.getContractNumberByUnitId(unitId, inspectionId, getDateFormat());
			
			logger.logInfo(methodName+"|"+" Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
	}
	
	public void getInspectionViolation(){
		
		String methodName="getInspectionViolation";
    	logger.logInfo(methodName+"|"+" Start");
    	
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request =(HttpServletRequest)context.getExternalContext().getRequest();
	
    	//unitId=(Long)request.getAttribute("unitId");
		//inspectionId=(Long)request.getAttribute("inspectionId");
		try {
			
			getContractView();
			inspectionViolationView = null;
			inspectionViolationView = psa.getViolationsByUnitId(getUnitId(),getInspectionId(),getDateFormat());
			
			//contractView = psa.getContractNumberByUnitId(unitId, inspectionId, getDateFormat());
			//inspectionViolationView.setContractNumber(contractView.getContractNumber());
			violationView =  inspectionViolationView.getViolations();
			unitRefNo = inspectionViolationView.getUnitRefNumber();
			
			contractRefNo = (String)getRequestParam("contractNumber");
			
			if (isArabicLocale)
			{
				unitType = inspectionViolationView.getUnitTypeAr();
				unitStatus = inspectionViolationView.getUnitStatusAr();
			}
			else
			{
				unitType = inspectionViolationView.getUnitTypeEn();
				unitStatus = inspectionViolationView.getUnitStatusEn();
			}
			
			
			if (contractView != null )
			{
				contractRefNo = contractView.getContractNumber();
			}
			
			
			propertyName = inspectionViolationView.getPropertyName();
			
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("violationView",violationView);
			
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
	}
	public ResourceBundle getBundle() {
		
		String methodName="getBundle";
    	logger.logInfo(methodName+"|"+" Start");

        if(getIsArabicLocale())

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

        else

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

        logger.logInfo(methodName+"|"+" Finish");
        
        return bundle;

  }

	private boolean hasErrors()
	{
		
		String methodName="hasErrors";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean isError=false;
		
		errorMessages=new ArrayList<String>();
    	errorMessages.clear();
		if(description==null || description.length()<=0 )
		{
			
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_DESC_NULL));
			isError = true;
		}
		else if(description.length()>250 )
		{
			
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_DESC_SIZE));
			isError = true;
		}
		
		if (violationCategory.equals("-1"))
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}
		
		if (violationType.equals("-1"))
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_TYPE));
			isError = true;
		}
		
		try
		{
			Double d;
			if (damageAmount != null && damageAmount.length() > 0)
					d = new Double(damageAmount); 
		}
		catch(Exception ex )
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_DAMAGE_FORMAT));
			isError = true;
		}
		
		try
		{
		 
			DateFormat df = new SimpleDateFormat(getDateFormat());
			df.format(violationDateRich);
		}
		catch(Exception ex )
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_DATE_FORMAT));
			isError = true;
		}
		
		
    	logger.logInfo(methodName+"|"+" Finish");
		
		
		return isError;
	}
	
	public void clearAll(){
		
		description = "";
		damageAmount = "";
		violationCategory = "-1";
		violationType = "-1";
		violationDateRich = null;
		
		getIsEnglishLocale();
		getIsArabicLocale();
		
	}
	
	public String addViolation(){
	
		String methodName="addViolation";
    	logger.logInfo(methodName+"|"+" Start");
		
		
		ViolationView violationViewToAdd = new ViolationView();
		
		try {
		
		DomainDataView domainDataView = new DomainDataView();
	
		domainDataView = psa.getDomainDataByValue(WebConstants.VIOLATION_STATUS, WebConstants.VIOLATION_STATUS_REPORTED);
		
		
		DateFormat df = new SimpleDateFormat(getDateFormat());
		if(!hasErrors())
		{
		
		violationViewToAdd.setCategoryId(Long.parseLong(violationCategory));
		violationViewToAdd.setTypeId(Long.parseLong(violationType));
		violationViewToAdd.setDescription(description);
		violationViewToAdd.setStatusId(domainDataView.getDomainDataId());
		violationViewToAdd.setUpdatedBy(getLoggedInUser());
		violationViewToAdd.setUpdatedOn(new Date());
		violationViewToAdd.setCreatedBy(getLoggedInUser());
		violationViewToAdd.setCreatedOn(new Date());
		violationViewToAdd.setViolationDate(violationDateRich);
		violationViewToAdd.setInspectionId(inspectionId);
		violationViewToAdd.setUnitId(unitId);
		violationViewToAdd.setRecordStatus(1L);
		violationViewToAdd.setIsDeleted(0L);
		
		Long newViolationID ;//= //psa.addViolation(violationViewToAdd);
		
		if (damageAmount != null && damageAmount.length()>0)
		{
		
		PaymentScheduleView psv = new PaymentScheduleView();
		
		DomainDataView domainDataViewCash;
		DomainDataView domainDataViewDamage;
		
			
			domainDataViewCash = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
			domainDataViewDamage = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_TYPE, WebConstants.PAYMENT_SCHEDULE_TYPE_DAMAGE);
			DomainDataView domainDataPaymentStatusPending = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_STATUS, WebConstants.PAYMENT_SCHEDULE_PENDING);
			
			getInspectionViolation();
			
			psv.setAmount(Double.parseDouble(damageAmount));
	    	psv.setPaymentModeId(domainDataViewCash.getDomainDataId());
	    	psv.setTypeId(domainDataViewDamage.getDomainDataId());
	    	psv.setStatusId(domainDataPaymentStatusPending.getDomainDataId());
	    	
	    	if (contractView != null)
	    		psv.setContractId(contractView.getContractId());
	    	
	    //	psv.setViolationId(newViolationID);
	    	
	    	psv.setIsReceived("N");
	    	psv.setUpdatedBy(getLoggedInUser());
	    	psv.setUpdatedOn(new Date());
	    	psv.setCreatedBy(getLoggedInUser());
	    	psv.setCreatedOn(new Date());
	    	psv.setRecordStatus(1L);
	    	psv.setIsDeleted(0L);
	    	
	    	
	    	psa.addPaymentScheduleByPaymentSchedule(psv);
		}
		
		getInspectionViolation();
		clearAll();
		
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
		
		return "";
	}
	
	public String okTask(){
		
		String methodName="okTask";
    	logger.logInfo(methodName+"|"+" Start");
		
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			UserTask userTask = (UserTask) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.OK);
			
			//session.removeAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			logger.logInfo(methodName+"|"+" Finish");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		} catch (PIMSWorkListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
		
		return "ok";
	}
	
	public String btnCancel_Click()
	{
		String methodName="btnCancel_Click";
    	logger.logInfo(methodName+"|"+" Start");
	
		setRequestParam("InspectionId", inspectionId);
		logger.logInfo(methodName+"|"+" Finish");
		return "sentTask";
	}
	
	public String inspectionDetails()
	{
		String methodName="btnCancel_Click";
    	logger.logInfo(methodName+"|"+" Start");
	
		setRequestParam("InspectionId", inspectionId);
		logger.logInfo(methodName+"|"+" Finish");
		return "sentTask";
	}
	
	public String sendTask(){
		
		String methodName="sendTask";
    	logger.logInfo(methodName+"|"+" Start");
	
		try {
			
			SystemParameters parameters = SystemParameters.getInstance();
            String endPoint= parameters.getParameter(WebConstants.INSPECTION_BPEL_ENDPOINT);

			PIMSInspectionBPELPortClient PIMSInspectionBPEL = new PIMSInspectionBPELPortClient();
			
			
			PIMSInspectionBPEL.setEndpoint(endPoint);
			
			PIMSInspectionBPEL.initiate( Long.valueOf( inspectionId ), getLoggedInUser() ,  null, null);
		    
			setAllViolationsAsSent();
			
			setRequestParam("InspectionId", inspectionId);
		
			logger.logInfo(methodName+"|"+" Finish");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
		
		return "sentTask";
	}
	
	private void setAllViolationsAsSent(){
		
		try {
			
			DomainDataView domainDataStatusSent = psa.getDomainDataByValue(WebConstants.VIOLATION_STATUS, WebConstants.VIOLATION_STATUS_SENT);
			Long statusId = domainDataStatusSent.getDomainDataId();
			
			for (int i=0; i<violationView.size(); i++)
			{
				Long violationId = violationView.get(i).getViolationId();
				
				psa.setViolationStatus(violationId, statusId);
			}
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String addAction(){
		
		String methodName="addAction";
    	logger.logInfo(methodName+"|"+" Start");
	
    	setRequestParam("unitId", unitId);
    	setRequestParam("inspectionId", inspectionId);
    	
    	ViolationView selectedViolation = (ViolationView) dataTable.getRowData();
    	
    	setRequestParam("violationId", selectedViolation.getViolationId());
    	setRequestParam("violationDate", selectedViolation.getViolationDateString());
    	
    	logger.logInfo(methodName+"|"+" Finish");
    	
		return "violationAction";
	}
	
	public String deleteViolation(){
		
		String methodName="deleteViolation";
    	logger.logInfo(methodName+"|"+" Start");
		
		ViolationView violation = (ViolationView) dataTable.getRowData();
		
		try {
			
			psa.deleteInspectionViolation(violation.getViolationId());
			
			getInspectionViolation();
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return "";
	}
	
	private void loadCombos()
	{
	
		String METHOD_NAME = "loadCombos()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		// TODO Auto-generated method stub	
		try{
		
		List<DomainDataView> domainDataListType =loadViolationTypes();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.VIOLATION_TYPE, domainDataListType);
		
		List<DomainDataView> domainDataListCategory =loadViolationCategory();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.VIOLATION_CATEGORY, domainDataListCategory);
		
		
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
	}
	

	
public List<DomainDataView> loadViolationTypes() {
		
		String METHOD_NAME = "loadViolationTypes()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.VIOLATION_TYPE);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}

public List<DomainDataView> loadViolationCategory() {
	
	String METHOD_NAME = "loadViolationCategory()";
	logger.logInfo("Stepped into the "+METHOD_NAME+" method");
	
	List<DomainDataView> domainDataList =null;
	try {
		
		domainDataList = new ArrayList<DomainDataView>();
		domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.VIOLATION_CATEGORY);
		
	//	domainDataList.get(0);
		
	} catch (PimsBusinessException e) {
		// TODO Auto-generated catch block
		logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
	}
	logger.logInfo(METHOD_NAME+" method completed Succesfully");
	 return domainDataList;
}
	

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getUnitRefNo() {
		return unitRefNo;
	}

	public void setUnitRefNo(String unitRefNo) {
		this.unitRefNo = unitRefNo;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public String getViolationType() {
		return violationType;
	}

	public void setViolationType(String violationType) {
		this.violationType = violationType;
	}

	public String getDamageAmount() {
		return damageAmount;
	}

	public void setDamageAmount(String damageAmount) {
		this.damageAmount = damageAmount;
	}

	public String getViolationCategory() {
		return violationCategory;
	}

	public void setViolationCategory(String violationCategory) {
		this.violationCategory = violationCategory;
	}

	public List<SelectItem> getTypeList() {
		
		String METHOD_NAME = "getTypeList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {
                typeList.clear();
                
                List<DomainDataView> getCategoryList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.VIOLATION_TYPE);
                for(int i=0;i< getCategoryList.size();i++)
                {
                	DomainDataView ddv=(DomainDataView)getCategoryList.get(i);
                	SelectItem item=null;
                	if (isEnglishLocale)
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
                	else
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());

                	typeList.add(item);
                }
               
			}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		return typeList;
	}

	public void setTypeList(List<SelectItem> typeList) {
		this.typeList = typeList;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);

		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);

		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");


		
		return isEnglishLocale;
	}

	public List<SelectItem> getCategoryList() {
		
		String METHOD_NAME = "getCategoryList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		try {
			categoryList.clear();
                
                List<DomainDataView> categoryComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.VIOLATION_CATEGORY);
                for(int i=0;i< categoryComboList.size();i++)
                {
                	DomainDataView ddv=(DomainDataView)categoryComboList.get(i);
                	SelectItem item=null;
                	if (isEnglishLocale)
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
                	else
                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());

                	categoryList.add(item);
                }
               
			}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		
		return categoryList;
	}
	
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	
	public String getDateFormat()
	{
		String methodName="getDateFormat";
    	logger.logInfo(methodName+"|"+" Start");
		
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		
		logger.logInfo(methodName+"|"+" Finish");
		return dateFormat;
	}

	public void setCategoryList(List<SelectItem> categoryList) {
		this.categoryList = categoryList;
	}

	public List<ViolationView> getViolationView() {
		
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey("violationView"))
			
			violationView = (List<ViolationView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("violationView");
		
		return violationView;
	}

	public void setViolationView(List<ViolationView> violationView) {
		this.violationView = violationView;
	}

	public void setViolationDate(String violationDate) {
		this.violationDate = violationDate;
	}

	public String getErrorMessages() 
	{
		String methodName="getErrorMessages";
    	logger.logInfo(methodName+"|"+" Start");
		
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
				{
					messageList += "<LI>"+ message+ " <br></br>" ;
			    }
			messageList = messageList + "</UL></B></FONT>\n";
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return	messageList;
		
	}

	public String getViolationDate() {
		return violationDate;
	}

	public Date getViolationDateRich() {
		return violationDateRich;
	}

	public void setViolationDateRich(Date violationDateRich) {
		this.violationDateRich = violationDateRich;
	}

	public Long getContractUnitId() {
		return contractUnitId;
	}

	public void setContractUnitId(Long contractUnitId) {
		this.contractUnitId = contractUnitId;
	}

	public Long getInspectionId() {
		return inspectionId;
	}

	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public String getUnitStatus() {
		return unitStatus;
	}

	public void setUnitStatus(String unitStatus) {
		this.unitStatus = unitStatus;
	}

	public boolean isFromList() {
		return fromList;
	}

	public void setFromList(boolean fromList) {
		this.fromList = fromList;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	
}
