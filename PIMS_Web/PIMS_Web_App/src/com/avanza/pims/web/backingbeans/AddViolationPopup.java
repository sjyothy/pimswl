package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.context.portlet.SessionMap;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.ViolationType;
import com.avanza.pims.proxy.pimsinspectionbpelproxy.PIMSInspectionBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorAdd.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationCategoryView;
import com.avanza.pims.ws.vo.ViolationTypeView;
import com.avanza.pims.ws.vo.ViolationView;
import com.crystaldecisions.report.web.shared.ErrorManager;


public class AddViolationPopup extends AbstractController {

	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private InspectionViolationView inspectionViolationView;
	private List<ViolationView> violationView;
	private HtmlDataTable dataTable;
	private String violationDate;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private UtilityServiceAgent usa = new UtilityServiceAgent();
	private ResourceBundle bundle;
	//Variables for fields
	private String unitRefNo;
	private String unitType;
	private String contractRefNo;
	private String propertyName;
	private String description;
	private String unitStatus;

	private String violationType;
	private String violationCategory;
	private String damageAmount;
	private Long inspectionId;
	private Long unitId;
	private Long contractUnitId;
	private boolean fromList;
	private boolean editable;

	public Date violationDateRich;

	UserTask userTask;

	private ContractView contractView;

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;

	List<SelectItem> typeList = new ArrayList();
	List<SelectItem> violationTypes = new ArrayList();
	List<SelectItem> categoryList = new ArrayList();
	
	List<SelectItem> statusList = new ArrayList();
	List<SelectItem> unitList = new ArrayList();
	
	private String violationStatus;
	private String violationUnit;
	private String contractNumber;
	private String tenantName;
	
	
	List<SelectItem> selectedUnitList = new ArrayList();
	private HtmlDataTable dataTableViolation = new HtmlDataTable();
	private List<ViolationView> violationViewDataList = new ArrayList<ViolationView>();
	
	private List<String> errorMessages;
	private static Logger logger = Logger
			.getLogger(AddViolationPopup.class);
	
	HashMap ispectionUnitMap = new HashMap ();
	private Map<String, String> violationActionList;
	private List<String> violationActions;
    private Boolean renderedDiv;
    private Boolean enableActionOnly;
    private String voilationDescription;
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		// TODO Auto-generated method stub
		super.init();

		String methodName = "init";
		logger.logInfo(methodName + "|" + " Start");
		setRenderedDiv(true);
		setEnableActionOnly(false);
		
		if(viewRootMap.containsKey("EDIT_THIS_VIOLATION"))
			setRenderedDiv(false);

		if (!isPostBack()) {
   
			fromList = true;
			loadCombos();
			
			if(getFacesContext().getExternalContext().getSessionMap().containsKey("SESSION_INSPECTION_UNITS")){
				viewRootMap.put("SESSION_INSPECTION_UNITS", 
						getFacesContext().getExternalContext().getSessionMap().get("SESSION_INSPECTION_UNITS"));
				getFacesContext().getExternalContext().getSessionMap().remove("SESSION_INSPECTION_UNITS");
			}
			
			//when comming from inspection after saving inspection
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.INSPECTION_VIEW)){
			InspectionView inspectionView = (InspectionView)	getFacesContext().getExternalContext().getSessionMap().get(WebConstants.INSPECTION_VIEW);
			if(inspectionView!=null && inspectionView.getInspectionId()!=null)
				viewRootMap.put("INSPECTION_ID", inspectionView.getInspectionId());
			}
			loadViolationActionList();
			viewRootMap.put("VIOLATION_ACTION_LIST", violationActionList);
			
			// when comming from inpection for editing violation
			if(getFacesContext().getExternalContext().getSessionMap().containsKey("VIOLATION_VIEW_FOR_EDIT")){
				ViolationView violationView = (ViolationView)	getFacesContext().getExternalContext().getSessionMap().get("VIOLATION_VIEW_FOR_EDIT");
				getFacesContext().getExternalContext().getSessionMap().remove("VIOLATION_VIEW_FOR_EDIT");
				viewRootMap.put("EDIT_VIEW", violationView);
				viewRootMap.put("EDIT_THIS_VIOLATION", true);
				putViolationViewValueInControl(violationView);
				violationViewDataList = new ArrayList<ViolationView>();
				violationViewDataList.add(violationView);
				setRenderedDiv(false);
					if(getFacesContext().getExternalContext().getSessionMap().containsKey("ADD_VIOLATION_ACTION")){
						getFacesContext().getExternalContext().getSessionMap().remove("ADD_VIOLATION_ACTION");
						setEnableActionOnly(true);
					}
				
			}
			if(getFacesContext().getExternalContext().getSessionMap().containsKey("VIOLATION_VIEW_FROM_SEARCH")){
				ViolationView violationView = (ViolationView)	getFacesContext().getExternalContext().getSessionMap().get("VIOLATION_VIEW_FROM_SEARCH");
				getFacesContext().getExternalContext().getSessionMap().remove("VIOLATION_VIEW_FROM_SEARCH");
				putViolationViewValueInControlForViolationSearch(violationView);
				viewRootMap.put("EDIT_VIEW", violationView);
				viewRootMap.put("VIOLATION_VIEW_FROM_SEARCH", violationView);
				viewRootMap.put("FROM_VIOLATION_SEARCH", true);
				viewRootMap.put("EDIT_THIS_VIOLATION", true);
				viewRootMap.put("FROM_VIOLATION_CONTRACT",true);
				violationViewDataList = new ArrayList<ViolationView>();
				violationViewDataList.add(violationView);
				setRenderedDiv(false);
				getUnitList();
				if(violationView.getInspectionId()!=null){
					viewRootMap.put("INSPECTION_ID_FROM_SEARCH", violationView.getInspectionId());
				}
			}
			
			
			if(getFacesContext().getExternalContext().getSessionMap().containsKey("FROM_VIOLATION_CONTRACT")){
				viewRootMap.put("FROM_VIOLATION_CONTRACT", 
						getFacesContext().getExternalContext().getSessionMap().get("FROM_VIOLATION_CONTRACT"));
				getFacesContext().getExternalContext().getSessionMap().remove("FROM_VIOLATION_CONTRACT");
				
				viewRootMap.put("VIOLATION_CONTRACT_VIEW", 
						getFacesContext().getExternalContext().getSessionMap().get("VIOLATION_CONTRACT_VIEW"));
				getFacesContext().getExternalContext().getSessionMap().remove("VIOLATION_CONTRACT_VIEW");
			}
			
			getIsEnglishLocale();
			getIsArabicLocale();
		}

		logger.logInfo(methodName + "|" + " Finish");

	}


	private void getContractView() {

		String methodName = "getContractView";
		logger.logInfo(methodName + "|" + " Start");

		try {

			contractView = psa.getContractNumberByUnitId(unitId, inspectionId,
					getDateFormat());

			logger.logInfo(methodName + "|" + " Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		}

	}

	public void getInspectionViolation() {

		String methodName = "getInspectionViolation";
		logger.logInfo(methodName + "|" + " Start");

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context
				.getExternalContext().getRequest();

		//unitId=(Long)request.getAttribute("unitId");
		//inspectionId=(Long)request.getAttribute("inspectionId");
		try {

			getContractView();
			inspectionViolationView = null;
			inspectionViolationView = psa.getViolationsByUnitId(unitId,
					inspectionId, getDateFormat());

			//contractView = psa.getContractNumberByUnitId(unitId, inspectionId, getDateFormat());
			//inspectionViolationView.setContractNumber(contractView.getContractNumber());
			violationView = inspectionViolationView.getViolations();
			unitRefNo = inspectionViolationView.getUnitRefNumber();

			contractRefNo = (String) getRequestParam("contractNumber");

			if (isArabicLocale) {
				unitType = inspectionViolationView.getUnitTypeAr();
				unitStatus = inspectionViolationView.getUnitStatusAr();
			} else {
				unitType = inspectionViolationView.getUnitTypeEn();
				unitStatus = inspectionViolationView.getUnitStatusEn();
			}

			if (contractView != null) {
				contractRefNo = contractView.getContractNumber();
			}

			propertyName = inspectionViolationView.getPropertyName();

			FacesContext.getCurrentInstance().getViewRoot().getAttributes()
					.put("violationView", violationView);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		}

	}

	public ResourceBundle getBundle() {

		String methodName = "getBundle";
		logger.logInfo(methodName + "|" + " Start");

		if (getIsArabicLocale())

			bundle = ResourceBundle
					.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

		else

			bundle = ResourceBundle
					.getBundle("com.avanza.pims.web.messageresource.Messages");

		logger.logInfo(methodName + "|" + " Finish");

		return bundle;

	}

	private boolean hasErrors() {

		String methodName = "hasErrors";
		logger.logInfo(methodName + "|" + " Start");

		boolean isError = false;

		errorMessages = new ArrayList<String>();
		errorMessages.clear();

		if (violationCategory!=null && violationCategory.equals("-1")) {
			errorMessages.add(getBundle().getString(
					MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}
		
		if (violationUnit !=null && violationUnit.equals("-1")) {
			errorMessages.add(getBundle().getString(
					MessageConstants.ViolationDetails.MSG_SELECT_UNIT));
			isError = true;
		}

		if (violationType!=null && violationType.equals("-1")) {
			errorMessages.add(getBundle().getString(
					MessageConstants.ViolationDetails.MSG_SELECT_TYPE));
			isError = true;
		}
		if(voilationDescription !=null && voilationDescription.length()>=250)
		{
			errorMessages.add(CommonUtil.getBundleMessage("violation.Description"));
			isError = true;
		}

		try {
            if(getDateFormat()!=null){
			DateFormat df = new SimpleDateFormat(getDateFormat());
			df.format(getViolationDateRich());
            }
		} catch (Exception ex) {
			errorMessages.add(getBundle().getString(
					MessageConstants.ViolationDetails.MSG_DATE_FORMAT));
			isError = true;
		}
		
		

		logger.logInfo(methodName + "|" + " Finish");

		return isError;
	}

	public void clearAll() {


		setViolationCategory("-1");
		setViolationType("-1");
		setViolationDateRich(null);
		setViolationStatus("-1");
		setViolationUnit("-1");
        setViolationActions(null);
        setTenantName("");
        setContractNumber("");
        setVoilationDescription("");
        
		getIsEnglishLocale();
		getIsArabicLocale();

	}
	public String addViolation() {

		String methodName = "addViolation";
		logger.logInfo(methodName + "|" + " Start");

		ViolationView violationViewToAdd = new ViolationView();
		UtilityManager utilityManager = new UtilityManager();

		try {

			DomainDataView domainDataView = new DomainDataView();

			domainDataView = psa.getDomainDataByValue(
					WebConstants.VIOLATION_STATUS,
					WebConstants.VIOLATION_STATUS_REPORTED);
			
			DomainDataView domainDataViewCategory = new DomainDataView();
			domainDataViewCategory =  psa.getDomainDataByDomainDataId( Long.valueOf( violationCategory ) );
			 
			DomainDataView domainDataViewType = new DomainDataView();
			
			domainDataViewType = psa.getDomainDataByDomainDataId( Long.valueOf( violationType ) );
			

			DateFormat df = new SimpleDateFormat(getDateFormat());
			if (!hasErrors()) {

				violationViewToAdd.setCategoryId(Long.parseLong(violationCategory));
				violationViewToAdd.setCategoryEn(domainDataViewCategory.getDataDescEn());
				violationViewToAdd.setCategoryAr(domainDataViewCategory.getDataDescAr());
				
				violationViewToAdd.setTypeId(Long.parseLong(violationType));
				violationViewToAdd.setTypeEn(domainDataViewType.getDataDescEn());
				violationViewToAdd.setTypeAr(domainDataViewType.getDataDescAr());
				

				
				// Status
				violationViewToAdd.setStatusId(domainDataView.getDomainDataId());
				violationViewToAdd.setStatusEn(domainDataView.getDataDescEn());
				violationViewToAdd.setStatusAr(domainDataView.getDataDescAr());
				
				violationViewToAdd.setUpdatedBy(getLoggedInUser());
				violationViewToAdd.setUpdatedOn(new Date());
				violationViewToAdd.setCreatedBy(getLoggedInUser());
				violationViewToAdd.setCreatedOn(new Date());
				violationViewToAdd.setViolationDate(violationDateRich);
				violationViewToAdd.setDescription(voilationDescription);
				violationViewToAdd.setInspectionId(inspectionId);
				violationViewToAdd.setUnitId(unitId);
				violationViewToAdd.setRecordStatus(1L);
				violationViewToAdd.setIsDeleted(0L);

//				Long newViolationID = psa.addViolation(violationViewToAdd);

				if (damageAmount != null && damageAmount.length() > 0) {

					PaymentScheduleView psv = new PaymentScheduleView();

					DomainDataView domainDataViewCash;
					DomainDataView domainDataViewDamage;

					domainDataViewCash = psa.getDomainDataByValue(
							WebConstants.PAYMENT_SCHEDULE_MODE,
							WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
					domainDataViewDamage = psa.getDomainDataByValue(
							WebConstants.PAYMENT_SCHEDULE_TYPE,
							WebConstants.PAYMENT_SCHEDULE_TYPE_DAMAGE);
					DomainDataView domainDataPaymentStatusPending = psa
							.getDomainDataByValue(
									WebConstants.PAYMENT_SCHEDULE_STATUS,
									WebConstants.PAYMENT_SCHEDULE_PENDING);

					getInspectionViolation();

					psv.setAmount(Double.parseDouble(damageAmount));
					psv.setPaymentModeId(domainDataViewCash.getDomainDataId());
					psv.setTypeId(domainDataViewDamage.getDomainDataId());
					psv.setStatusId(domainDataPaymentStatusPending
							.getDomainDataId());

					if (contractView != null)
						psv.setContractId(contractView.getContractId());

					psv.setViolationId(null);

					psv.setIsReceived("N");
					psv.setUpdatedBy(getLoggedInUser());
					psv.setUpdatedOn(new Date());
					psv.setCreatedBy(getLoggedInUser());
					psv.setCreatedOn(new Date());
					psv.setRecordStatus(1L);
					psv.setIsDeleted(0L);

//					psa.addPaymentScheduleByPaymentSchedule(psv);
				}
				

//				getInspectionViolation();
				clearAll();

			}

			logger.logInfo(methodName + "|" + " Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);

		}

		return "";
	}

	public String okTask() {

		String methodName = "okTask";
		logger.logInfo(methodName + "|" + " Start");

		String contextPath = ((ServletContext) getFacesContext()
				.getExternalContext().getContext())
				.getRealPath("\\WEB-INF\\config.properties");
		BPMWorklistClient bpmWorkListClient;
		try {

			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			UserTask userTask = (UserTask) FacesContext.getCurrentInstance()
					.getViewRoot().getAttributes().get(
							WebConstants.TASK_LIST_SELECTED_USER_TASK);
			bpmWorkListClient.completeTask(userTask, getLoggedInUser(),
					TaskOutcome.OK);

			//session.removeAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			logger.logInfo(methodName + "|" + " Finish");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		} catch (PIMSWorkListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		}

		return "ok";
	}

	public String btnCancel_Click() {
		String methodName = "btnCancel_Click";
		logger.logInfo(methodName + "|" + " Start");

		setRequestParam("InspectionId", inspectionId);
		logger.logInfo(methodName + "|" + " Finish");
		return "sentTask";
	}

	public String inspectionDetails() {
		String methodName = "btnCancel_Click";
		logger.logInfo(methodName + "|" + " Start");

		setRequestParam("InspectionId", inspectionId);
		logger.logInfo(methodName + "|" + " Finish");
		return "sentTask";
	}

	public String sendTask() {

		String methodName = "sendTask";
		logger.logInfo(methodName + "|" + " Start");

		try {

			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters
					.getParameter(WebConstants.INSPECTION_BPEL_ENDPOINT);

			PIMSInspectionBPELPortClient PIMSInspectionBPEL = new PIMSInspectionBPELPortClient();

			PIMSInspectionBPEL.setEndpoint(endPoint);

			PIMSInspectionBPEL.initiate( inspectionId, getLoggedInUser() ,null, null);

			setAllViolationsAsSent();

			setRequestParam("InspectionId", inspectionId);

			logger.logInfo(methodName + "|" + " Finish");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		}

		return "sentTask";
	}

	private void setAllViolationsAsSent() {

		try {

			DomainDataView domainDataStatusSent = psa.getDomainDataByValue(
					WebConstants.VIOLATION_STATUS,
					WebConstants.VIOLATION_STATUS_SENT);
			Long statusId = domainDataStatusSent.getDomainDataId();

			for (int i = 0; i < violationView.size(); i++) {
				Long violationId = violationView.get(i).getViolationId();

				psa.setViolationStatus(violationId, statusId);
			}

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String addAction() {

		String methodName = "addAction";
		logger.logInfo(methodName + "|" + " Start");

		setRequestParam("unitId", unitId);
		setRequestParam("inspectionId", inspectionId);

		ViolationView selectedViolation = (ViolationView) dataTable
				.getRowData();

		setRequestParam("violationId", selectedViolation.getViolationId());
		setRequestParam("violationDate", selectedViolation
				.getViolationDateString());

		logger.logInfo(methodName + "|" + " Finish");

		return "violationAction";
	}

	public String deleteViolation() {

		String methodName = "deleteViolation";
		logger.logInfo(methodName + "|" + " Start");

		ViolationView violation = (ViolationView) dataTable.getRowData();

		try {

			psa.deleteInspectionViolation(violation.getViolationId());

			getInspectionViolation();

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);
		}

		logger.logInfo(methodName + "|" + " Finish");
		return "";
	}
    @SuppressWarnings( "unchecked" )
	private void loadCombos() {

		String METHOD_NAME = "loadCombos()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");
		// TODO Auto-generated method stub	
		try {

			List<DomainDataView> domainDataListType = loadViolationTypes();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put(WebConstants.VIOLATION_TYPE,
							domainDataListType);

			List<DomainDataView> domainDataListCategory = loadViolationCategory();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put(WebConstants.VIOLATION_CATEGORY,
							domainDataListCategory);
			
			List<DomainDataView> domainDataListStatus = loadViolationStatus();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put(WebConstants.VIOLATION_STATUS,
							domainDataListStatus);
			getUnitList();

		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME + " method completed Succesfully");
	}
	
	public List<DomainDataView> loadViolationStatus() {

		String METHOD_NAME = "loadViolationStatus()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		List<DomainDataView> domainDataList = null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa
					.getDomainDataByDomainTypeName(WebConstants.VIOLATION_STATUS);

			//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return domainDataList;
	}
	
	public List loadSelectedUnitList(List lst) {

		String METHOD_NAME = "loadSelectedUnitList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		List domainDataList = null;
		
		try {

			if(lst != null ){
				
				for(int i=0;i<lst.size(); i++){
					
					InspectionUnitView iuv = (InspectionUnitView) lst.get(i);
					
					
					
				}
				
			}
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa
					.getDomainDataByDomainTypeName(WebConstants.VIOLATION_STATUS);

			//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return domainDataList;
	}

	public List<DomainDataView> loadViolationTypes() {

		String METHOD_NAME = "loadViolationTypes()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		List<DomainDataView> domainDataList = null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa
					.getDomainDataByDomainTypeName(WebConstants.VIOLATION_TYPE);

			//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return domainDataList;
	}

	public List<DomainDataView> loadViolationCategory() {

		String METHOD_NAME = "loadViolationCategory()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		List<DomainDataView> domainDataList = null;
		try {

			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa
					.getDomainDataByDomainTypeName(WebConstants.VIOLATION_CATEGORY);

			//	domainDataList.get(0);

		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return domainDataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getUnitRefNo() {
		return unitRefNo;
	}

	public void setUnitRefNo(String unitRefNo) {
		this.unitRefNo = unitRefNo;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getViolationType() {
		if(viewRootMap.containsKey("VIOLATION_TYPE"))
		   violationType = (String)viewRootMap.get("VIOLATION_TYPE");
		return violationType;
	}

	public void setViolationType(String violationType) {
		this.violationType = violationType;
		
		if(this.violationType!=null)
			viewRootMap.put("VIOLATION_TYPE", this.violationType);
	}

	public String getDamageAmount() {
		return damageAmount;
	}

	public void setDamageAmount(String damageAmount) {
		this.damageAmount = damageAmount;
	}

	public String getViolationCategory() {
		if(viewRootMap.containsKey("VIOLATION_CATEGORY"))
			violationCategory = (String)viewRootMap.get("VIOLATION_CATEGORY");
		
		return violationCategory;
	}

	public void setViolationCategory(String violationCategory) {
		this.violationCategory = violationCategory;
		
		if(this.violationCategory!=null)
			viewRootMap.put("VIOLATION_CATEGORY", this.violationCategory);
	}

	public List<SelectItem> getTypeList() {

		String METHOD_NAME = "getTypeList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			typeList.clear();

			List<DomainDataView> getCategoryList = (ArrayList<DomainDataView>) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get(WebConstants.VIOLATION_TYPE);
			for (int i = 0; i < getCategoryList.size(); i++) {
				DomainDataView ddv = (DomainDataView) getCategoryList.get(i);
				SelectItem item = null;
				if (isEnglishLocale)
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescEn());
				else
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescAr());

				typeList.add(item);
			}
			Collections.sort(typeList,ListComparator.LIST_COMPARE);
		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}

		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return typeList;
	}

	public void setTypeList(List<SelectItem> typeList) {
		this.typeList = typeList;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);

		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);

		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}

	public List<SelectItem> getCategoryList() {

		String METHOD_NAME = "getCategoryList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			categoryList.clear();

			List<DomainDataView> categoryComboList = (ArrayList<DomainDataView>) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get(WebConstants.VIOLATION_CATEGORY);
			for (int i = 0; i < categoryComboList.size(); i++) {
				DomainDataView ddv = (DomainDataView) categoryComboList.get(i);
				SelectItem item = null;
				if (isEnglishLocale)
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescEn());
				else
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescAr());

				categoryList.add(item);
			}
			Collections.sort(categoryList,ListComparator.LIST_COMPARE);
		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}

		logger.logInfo(METHOD_NAME + " method completed Succesfully");

		return categoryList;
	}

	private String getLoggedInUser() {
		String methodName = "getLoggedInUser";
		logger.logInfo(methodName + "|" + " Start");

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		logger.logInfo(methodName + "|" + " Finish");
		return loggedInUser;
	}

	public String getDateFormat() {
		String methodName = "getDateFormat";
		logger.logInfo(methodName + "|" + " Start");

		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters
				.getParameter(WebConstants.SHORT_DATE_FORMAT);

		logger.logInfo(methodName + "|" + " Finish");
		return dateFormat;
	}

	public void setCategoryList(List<SelectItem> categoryList) {
		this.categoryList = categoryList;
	}

	public List<ViolationView> getViolationView() {

		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes()
				.containsKey("violationView"))

			violationView = (List<ViolationView>) FacesContext
					.getCurrentInstance().getViewRoot().getAttributes().get(
							"violationView");

		return violationView;
	}

	public void setViolationView(List<ViolationView> violationView) {
		this.violationView = violationView;
	}

	public void setViolationDate(String violationDate) {
		this.violationDate = violationDate;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public String getViolationDate() {
		return violationDate;
	}

	public Date getViolationDateRich() {
		if(viewRootMap.containsKey("VIOLATION_DATE")&&viewRootMap.get("VIOLATION_DATE").toString().length()>0)
			violationDateRich  = (Date) viewRootMap.get("VIOLATION_DATE");
		return violationDateRich;
	}

	public void setViolationDateRich(Date violationDateRich) {
		this.violationDateRich = violationDateRich;
		
		if(this.violationDateRich!=null)
			viewRootMap.put("VIOLATION_DATE", this.violationDateRich);
		else
			viewRootMap.put("VIOLATION_DATE","");
	}

	public Long getContractUnitId() {
		return contractUnitId;
	}

	public void setContractUnitId(Long contractUnitId) {
		this.contractUnitId = contractUnitId;
	}

	public Long getInspectionId() {
		return inspectionId;
	}

	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public String getUnitStatus() {
		return unitStatus;
	}

	public void setUnitStatus(String unitStatus) {
		this.unitStatus = unitStatus;
	}

	public boolean isFromList() {
		return fromList;
	}

	public void setFromList(boolean fromList) {
		this.fromList = fromList;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public List<SelectItem> getStatusList() {
		
		String METHOD_NAME = "getStatusList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			
			statusList.clear();

			List<DomainDataView> getCategoryList = (ArrayList<DomainDataView>) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get( WebConstants.VIOLATION_STATUS );
			
			for (int i = 0; i < getCategoryList.size(); i++) {
				
				DomainDataView ddv = (DomainDataView) getCategoryList.get(i);
				SelectItem item = null;
				if (getIsEnglishLocale())
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescEn());
				else if(getIsArabicLocale())
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescAr());

				else
					item = new SelectItem(ddv.getDomainDataId().toString(), ddv
							.getDataDescEn());

				statusList.add(item);
			}
			Collections.sort(statusList,ListComparator.LIST_COMPARE);
		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}

		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		
		return statusList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}

	public String getViolationStatus() {
		if(viewRootMap.containsKey("VIOLATION_STATUS"))
			violationStatus = (String)viewRootMap.get("VIOLATION_STATUS");  
		
		return violationStatus;
	}

	public void setViolationStatus(String violationStatus) {
		this.violationStatus = violationStatus;
		
		if(this.violationStatus !=null)
			viewRootMap.put("VIOLATION_STATUS", this.violationStatus);
	}

	public String getViolationUnit() {
		if(viewRootMap.containsKey("VIOLATION_UNIT"))
			violationUnit =  (String) viewRootMap.get("VIOLATION_UNIT");
		
		return violationUnit;
	}

	public void setViolationUnit(String violationUnit) {
		this.violationUnit = violationUnit;
		
		if(this.violationUnit!=null)
			viewRootMap.put("VIOLATION_UNIT", this.violationUnit);
	}

	public List<SelectItem> getSelectedUnitList() {
		return selectedUnitList;
	}

	public void setSelectedUnitList(List<SelectItem> selectedUnitList) {
		this.selectedUnitList = selectedUnitList;
	}

	public String getContractNumber() {
		if(viewRootMap.containsKey("CONTRACT_NUMBER"))
			contractNumber = (String)viewRootMap.get("CONTRACT_NUMBER");
		
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
		
		if(this.contractNumber!=null)
           viewRootMap.put("CONTRACT_NUMBER", this.contractNumber);			
		
	}

	public String getTenantName() {
		if(viewRootMap.containsKey("TENANT_NAME"))
			tenantName = (String)viewRootMap.get("TENANT_NAME");
		
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
		
		if(this.tenantName!=null)
			viewRootMap.put("TENANT_NAME", this.tenantName);
	}

	public List<SelectItem> getUnitList() {
		
		String METHOD_NAME = "getUnitList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			
			unitList.clear();
		if(!viewRootMap.containsKey("FROM_VIOLATION_CONTRACT"))	{
			if ( viewRootMap.containsKey("SESSION_INSPECTION_UNITS") &&  viewRootMap.get("SESSION_INSPECTION_UNITS") != null  ) {
				
				List selUnitList = (List) viewRootMap.get("SESSION_INSPECTION_UNITS");
				
				if(selUnitList != null ){
					
					for(int i=0;i<selUnitList.size(); i++){
						
						InspectionUnitView iuv = (InspectionUnitView) selUnitList.get(i);
						
						String value = iuv.getUnitView().getUnitId().toString();
						ispectionUnitMap.put(value,iuv);
						SelectItem item = null;
						item = new SelectItem(value, iuv
									.getUnitView().getUnitNumber());
						if( item != null )
							unitList.add(item);
						
					}
					
					if(viewRootMap.containsKey("UNIT_LIST_FOR"))
						unitList = (List<SelectItem>)viewRootMap.get("UNIT_LIST_FOR"); 
					
					viewRootMap.put("INSPECTION_UNIT_MAP", ispectionUnitMap);
				}	
			}
		}else{
			
			if ( viewRootMap.containsKey("SESSION_INSPECTION_UNITS") &&  viewRootMap.get("SESSION_INSPECTION_UNITS") != null  ) {
		
		List selUnitList = (List) viewRootMap.get("SESSION_INSPECTION_UNITS");
		
		if(selUnitList != null ){
			
			for(int i=0;i<selUnitList.size(); i++){
				
				UnitView uv = (UnitView) selUnitList.get(i);
				
				String value = uv.getUnitId().toString();
				SelectItem item = null;
				item = new SelectItem(value, uv.getUnitNumber());
				if( item != null )
					unitList.add(item);
				
			}
			
			if(viewRootMap.containsKey("UNIT_LIST_FOR"))
				unitList = (List<SelectItem>)viewRootMap.get("UNIT_LIST_FOR"); 
			
		}	
	}
		if(viewRootMap.containsKey("VIOLATION_VIEW_FROM_SEARCH")){
		 ViolationView vV = (ViolationView)	viewRootMap.get("VIOLATION_VIEW_FROM_SEARCH");
			SelectItem item = null;
			item = new SelectItem(vV.getUnitId().toString(), vV.getUnitNumber());
			if( item != null )
				unitList.add(item);
		
		if(viewRootMap.containsKey("UNIT_LIST_FOR"))
			unitList = (List<SelectItem>)viewRootMap.get("UNIT_LIST_FOR"); 
		
	
		}
			
	}

		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}

		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return unitList;
	}

	public void setUnitList(List<SelectItem> unitList) {
		this.unitList = unitList;
		
		if(this.unitList!=null)
			viewRootMap.put("UNIT_LIST_FOR",this.unitList);
	}

	public String addViolationToInspection()
	{
		errorMessages  = new ArrayList<String>();
	      
		if(viewRootMap.containsKey("EDIT_VIEW")){
			if(!putViolationObjectInSession((ViolationView)viewRootMap.get("EDIT_VIEW"))){
			 return "addViolationToInspection";	
			}
		viewRootMap.remove("EDIT_VIEW");
		}
		else{
			if(!putViolationObjectInSession(null)){
				return "addViolationToInspection";	    
			}
		}     
	        
		return "addViolationToInspection";
	    }
	

	
	private Boolean putViolationObjectInSession(ViolationView vV)
	{
		String methodName="putSelectedUnitsInSession";
		ViolationView violationViewToAdd;
		DomainDataView domainDataView = new DomainDataView();
		ViolationCategoryView vCV = new ViolationCategoryView();
		ViolationTypeView vTV = new ViolationTypeView();

		logger.logInfo(methodName + "|" + " Start");
		errorMessages = new ArrayList<String>();
		
		if(vV!=null){
			 violationViewToAdd =vV;
		 violationViewDataList.remove(violationViewToAdd);    
		}
		else
		 violationViewToAdd = new ViolationView();

		try {

			if (!hasErrors()) {
				if(getViolationStatus()!=null&& !getViolationStatus().equals("-1") )	
				domainDataView = psa.getDomainDataByDomainDataId( Long.valueOf(getViolationStatus()));
				if(domainDataView.getDomainDataId()!=null)
				{
				violationViewToAdd.setStatusId(domainDataView.getDomainDataId());
				violationViewToAdd.setStatusEn(domainDataView.getDataDescEn());
				violationViewToAdd.setStatusAr(domainDataView.getDataDescAr());
				}
			

				if(getViolationCategory()!=null && !getViolationCategory().equals("-1"))
				{
				vCV = usa.getViolationCategoryById(Long.parseLong(getViolationCategory()));
				violationViewToAdd.setCategoryId(vCV.getViolationCategoryId());
				violationViewToAdd.setCategoryEn(vCV.getDescriptionEn());
				violationViewToAdd.setCategoryAr(vCV.getDescriptionAr());
				}
				if(getViolationType()!=null && !getViolationType().equals("-1"))
				{
				vTV = usa.getViolationTypeById(Long.parseLong(getViolationType()));
				violationViewToAdd.setTypeId(vTV.getViolationTypeId());
				violationViewToAdd.setTypeAr(vTV.getDescriptionAr());
				violationViewToAdd.setTypeEn(vTV.getDescriptionEn());
				}
			
		
			
				violationViewToAdd.setUpdatedBy(getLoggedInUser());
				violationViewToAdd.setUpdatedOn(new Date());
				violationViewToAdd.setCreatedBy(getLoggedInUser());
				violationViewToAdd.setCreatedOn(new Date());
				violationViewToAdd.setViolationDate(violationDateRich);
				violationViewToAdd.setDescription(voilationDescription);
				violationViewToAdd.setViolationDateString( DateUtil.dateToOraString( violationDateRich  ));
				violationViewToAdd.setViolationActionIdList(getViolationActions());
				violationViewToAdd.setUnitId( Long.parseLong(getViolationUnit() ));
						
				
				SelectItem sitem = null;
				String unitNumber = "" ;
			for(int i=0; i< getUnitList().size(); i++ )
			{
				sitem = unitList.get(i);
				if(sitem.getValue().equals(violationUnit)){
					unitNumber = sitem.getLabel();
					viewRootMap.put("unitForNotification", unitNumber);
				}
			}
		if(!viewRootMap.containsKey("FROM_VIOLATION_CONTRACT")&&!viewRootMap.containsKey("FROM_VIOLATION_SEARCH")){		
			if(viewRootMap.containsKey("INSPECTION_UNIT_MAP"))
			{
				   ispectionUnitMap =(HashMap) viewRootMap.get("INSPECTION_UNIT_MAP");
					if(ispectionUnitMap.containsKey(violationUnit))
					{
						InspectionUnitView iUV = (InspectionUnitView)ispectionUnitMap.get(violationUnit);
						violationViewToAdd.setContractNumber(iUV.getContractNumber());
						violationViewToAdd.setTenantName(iUV.getTenantName());
						violationViewToAdd.setContractUnitId(iUV.getContractUnitId());
					}
			 }
		}else{
			  if(viewRootMap.containsKey("VIOLATION_CONTRACT_VIEW")){
				  ContractView contView =(ContractView) viewRootMap.get("VIOLATION_CONTRACT_VIEW");
					
				  violationViewToAdd.setContractNumber(contView.getContractNumber());
				  violationViewToAdd.setTenantName(setTenantName(contView));
				  violationViewToAdd.setContractUnitId(contView.getContractUnitView().iterator().next().getContractUnitId());
					
				 }
			  if(viewRootMap.containsKey("FROM_VIOLATION_SEARCH")){
				  violationViewToAdd.setContractNumber(getContractNumber());
				  violationViewToAdd.setTenantName(getTenantName());
				  if(viewRootMap.containsKey("INSPECTION_ID_FROM_SEARCH"))
				  violationViewToAdd.setInspectionId((Long)viewRootMap.get("INSPECTION_ID"));
				  
				  //violationViewToAdd.setContractUnitId(contView.getContractUnitView().iterator().next().getContractUnitId());
			  }
		  }
			
				
			if(viewRootMap.containsKey("INSPECTION_ID")){
				    violationViewToAdd.setInspectionId((Long)viewRootMap.get("INSPECTION_ID"));
			     }
				violationViewToAdd.setUnitNumber(unitNumber); 
				violationViewToAdd.setRecordStatus(1L);
				violationViewToAdd.setIsDeleted(0L);
				violationViewToAdd.setCreatedOn(new Date());
				violationViewToAdd.setCreatedBy(getLoggedInUser());
				violationViewToAdd.setUpdatedOn(new Date());
				violationViewToAdd.setUpdatedBy(getLoggedInUser());
				
				violationViewDataList.add(violationViewToAdd);
			     viewRootMap.put("VIOLATION_DATA_LIST",violationViewDataList);
			     getViolationViewDataList();

				clearAll();
             
				
				
			}else{
				return false;
			}

			logger.logInfo(methodName + "|" + " Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName + "|" + "Error Occured:" + e);

		}


				return true;
	}
	
	public String sendToParentWindow(String javaScript)
	{
		String methodName="sendToParentWindow";
        final String viewId = "/AddViolationPopup.jsf";
		
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =javaScript;
        
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		
		return "";
	}
	
	public void loadViolationUnit(){
		
 if(!viewRootMap.containsKey("FROM_VIOLATION_CONTRACT")){
		if(viewRootMap.containsKey("INSPECTION_UNIT_MAP")){
		   ispectionUnitMap =(HashMap) viewRootMap.get("INSPECTION_UNIT_MAP");
			if(ispectionUnitMap.containsKey(violationUnit)){
			InspectionUnitView iUV = (InspectionUnitView)ispectionUnitMap.get(violationUnit);
			setContractNumber(iUV.getContractNumber());
			setTenantName(iUV.getTenantName());
			}
		 }
  }else{
	  if(viewRootMap.containsKey("VIOLATION_CONTRACT_VIEW")){
		  ContractView contView =(ContractView) viewRootMap.get("VIOLATION_CONTRACT_VIEW");
			
			setContractNumber(contView.getContractNumber());
			setTenantName(setTenantName(contView));
			
		 }
  }
}

	public HtmlDataTable getDataTableViolation() {
		return dataTableViolation;
	}

	public void setDataTableViolation(HtmlDataTable dataTableViolation) {
		this.dataTableViolation = dataTableViolation;
	}
	
	public List<ViolationView> getViolationViewDataList() {
		if ( viewRootMap.get("VIOLATION_DATA_LIST") != null)
			violationViewDataList = (ArrayList<ViolationView>) viewRootMap.get("VIOLATION_DATA_LIST");

		return violationViewDataList;
	}

	public void setViolationViewDataList(List<ViolationView> violationViewDataList) {
		this.violationViewDataList = violationViewDataList;
		
		if(this.violationViewDataList!=null)
			viewRootMap.put("VIOLATION_DATA_LIST", this.violationActions);
		
	}
	
	public String saveViolation(){
	errorMessages  = new ArrayList<String>();	
	String javaScriptText ="window.opener.document.forms[0].submit();" +
    		               "window.close();"; 
	try {	
	// when editing violation 
	  if(viewRootMap.containsKey("EDIT_THIS_VIOLATION")){
		  
		  if(viewRootMap.containsKey("EDIT_VIEW")){
				if(!putViolationObjectInSession((ViolationView)viewRootMap.get("EDIT_VIEW"))){
				return "saveViolation";	
				}
				viewRootMap.remove("EDIT_VIEW");
				}
		  psa.addViolation(violationViewDataList);
	  	  getFacesContext().getExternalContext().getSessionMap().put("NEW_VIOLATION_VIEW", violationViewDataList);
	  	  getFacesContext().getExternalContext().getSessionMap().put("EDIT_DONE", true);
	  }
   //when adding violation
   if(violationViewDataList!=null && violationViewDataList.size()>0
	  && !getFacesContext().getExternalContext().getSessionMap().containsKey("EDIT_DONE")){
	  psa.addViolation(violationViewDataList);
    	getFacesContext().getExternalContext().getSessionMap().put("NEW_VIOLATION_VIEW", violationViewDataList);
    	getFacesContext().getExternalContext().getSessionMap().put("SAVE_DONE", true);
    	notifyTenant();
	
	}//pressing save btn with out adding violation 
   else if(!getFacesContext().getExternalContext().getSessionMap().containsKey("EDIT_DONE")){
		errorMessages.add(getBundle().getString(
				MessageConstants.ViolationDetails.MSG_ADD_VIOLATION));
		return "saveViolation";
	}
        
    sendToParentWindow(javaScriptText);
    
    
	} catch (PimsBusinessException e) {
		e.printStackTrace();
	}
		
	return "saveViolation";
	}
	
	private final void loadViolationActionList() {
		String methodName = "loadServiceTypeList()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> violationActionDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.VIOLATION_ACTION);

			if (violationActionList == null)
				violationActionList = new HashMap<String, String>();

			for (DomainDataView acrionType : violationActionDDL)
				getViolationActionList().put(
						getIsEnglishLocale() ? acrionType.getDataDescEn()
								: acrionType.getDataDescAr(),
								  acrionType.getDomainDataId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	public Map<String, String> getViolationActionList() {
		if(viewRootMap.get("VIOLATION_ACTION_LIST")!=null)
			violationActionList = (Map<String,String>)viewRootMap.get("VIOLATION_ACTION_LIST");
		
		return violationActionList;
	}

	public void setViolationActionList(Map<String, String> violationActionList) {
		this.violationActionList = violationActionList;
	}

	public List<String> getViolationActions() {
		if(viewRootMap.containsKey("VIOLATION_ACTION")&&viewRootMap.get("VIOLATION_ACTION").toString().length()>0)
			violationActions = (List<String>)viewRootMap.get("VIOLATION_ACTION");
		return violationActions;
	}

	public void setViolationActions(List<String> violationActions) {
		this.violationActions = violationActions;
		
		if(this.violationActions!=null)
			viewRootMap.put("VIOLATION_ACTION", this.violationActions);
		else
			viewRootMap.put("VIOLATION_ACTION", "");
	}
	
	public String btnDeleteViolation_Click(){
		
		
		   violationViewDataList.remove((ViolationView) dataTableViolation.getRowData());
		   
		   
		return "btnDeleteViolation_Click";
	}
	
	
	public String btnEditViolation_Click(){
		
		ViolationView vV =  (ViolationView) dataTableViolation.getRowData();
		   //violationViewDataList.remove((ViolationView) dataTableViolation.getRowData());
		   
		   putViolationViewValueInControl(vV);
		
		   viewRootMap.put("EDIT_VIEW", vV);
		   
		return "btnDeleteViolation_Click";
	}
	
	public void  putViolationViewValueInControl(ViolationView vV){
		if(vV.getUnitId()!=null)
		setViolationUnit(vV.getUnitId().toString());
		
		if(vV.getStatusId()!=null)
		setViolationStatus(vV.getStatusId().toString());

		if(vV.getCategoryId()!=null)
		{	
		setViolationCategory(vV.getCategoryId().toString());
		
		List<SelectItem> violationTypeList = new ArrayList<SelectItem>(0);
		violationTypeList=getViolationTypeByCategory(vV.getCategoryId());
    	viewRootMap.put("violationTypeList", violationTypeList);
		}
		
		if(vV.getTypeId()!=null)
		setViolationType(vV.getTypeId().toString());
		
		setTenantName(vV.getTenantName());
		setContractNumber(vV.getContractNumber());
		setViolationDateRich(vV.getViolationDate());
		setViolationActions(vV.getViolationActionIdList());
		setVoilationDescription(vV.getDescription());
		
		
	}

	public Boolean getRenderedDiv() {
		return renderedDiv;
	}

	public void setRenderedDiv(Boolean renderedDiv) {
		this.renderedDiv = renderedDiv;
	}

	public Boolean getEnableActionOnly() {
		return enableActionOnly;
	}

	public void setEnableActionOnly(Boolean enableActionOnly) {
		this.enableActionOnly = enableActionOnly;
	}

	private String setTenantName(ContractView contractView) {
		String name = "";
		String  METHOD_NAME = "setTenantName|";		
        logger.logInfo(METHOD_NAME+" started...");
		if(contractView.getTenantView() != null){
			
			if(contractView.getTenantView().getFirstName() != null)
				name += contractView.getTenantView().getFirstName() + " ";
			if(contractView.getTenantView().getMiddleName() != null)
				name += contractView.getTenantView().getMiddleName() + " ";
			if(contractView.getTenantView().getLastName() != null)
				name += contractView.getTenantView().getLastName();
				
		}
        logger.logInfo(METHOD_NAME+" ended...");
        
        return name;
	}
	public void  putViolationViewValueInControlForViolationSearch(ViolationView vV){

		if(vV.getUnitId()!=null)
		setViolationUnit(vV.getUnitId().toString());
		
		if(vV.getStatusId()!=null)
		setViolationStatus(vV.getStatusId().toString());

		
		if(vV.getCategoryId()!=null)
		{	
		setViolationCategory(vV.getCategoryId().toString());
		
		List<SelectItem> violationTypeList = new ArrayList<SelectItem>(0);
		violationTypeList=getViolationTypeByCategory(vV.getCategoryId());
    	viewRootMap.put("violationTypeList", violationTypeList);
		}
		
		if(vV.getTypeId()!=null)
		setViolationType(vV.getTypeId().toString());
		
		setTenantName(vV.getTenantName());
		setContractNumber(vV.getContractNumber());
		setViolationDateRich(vV.getViolationDate());
		setVoilationDescription(vV.getDescription());
		setViolationActions(vV.getViolationActionIdList());
		setViolationUnit(vV.getUnitId().toString());
		
		
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}


	public String getVoilationDescription() {
		if(viewRootMap.containsKey("VIOLATION_DESCRIPTION"))
			voilationDescription = viewRootMap.get("VIOLATION_DESCRIPTION").toString();
		return voilationDescription;
	}


	public void setVoilationDescription(String voilationDescription) {
		this.voilationDescription = voilationDescription;
		if(this.voilationDescription!=null)
			viewRootMap.put("VIOLATION_DESCRIPTION", this.voilationDescription);
	}
	public void violationCategorySelected(ValueChangeEvent event)
	{
		Long violationCategoryId = -1L;
		try {
			violationCategoryId = new Long((String) event.getNewValue());
		} catch (NumberFormatException e) {
			logger.LogException("violationCategorySelected|Error Occured", e);
		}
		
		List<SelectItem> violationTypeList = new ArrayList<SelectItem>(0);
		
		if (violationCategoryId!=-1)
			violationTypeList=getViolationTypeByCategory(violationCategoryId);
			
		viewRootMap.put("violationTypeList", violationTypeList);
	}
	
	public List<SelectItem> getViolationTypeByCategory(Long id)
	{
	
			ApplicationBean applicationBean=new ApplicationBean();
			return applicationBean.getViolationTypeListByCategory(id);
	}


	public List<SelectItem> getViolationTypes() 
	{
		if(viewRootMap.get("violationTypeList")!=null)
		violationTypes = (List<SelectItem>) viewRootMap.get("violationTypeList");
		return violationTypes;
	}


	public void setViolationTypes(List<SelectItem> violationTypes) {
		this.violationTypes = violationTypes;
	}
	
	private void notifyTenant()
	{
		try
		{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		ContractView contView = null;
		if(viewRootMap.containsKey("VIOLATION_CONTRACT_VIEW"))
			  contView =(ContractView) viewRootMap.get("VIOLATION_CONTRACT_VIEW");
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		String contractNo = this.getContractNumber();
		if(contView != null)
		{
			if(contView.getTenantView()!=null)
			{
				Long tenantId=contView.getTenantView().getPersonId();
				PersonView tenant = psAgent.getPersonInformation(tenantId);
				List<ContactInfo> contactInfoList = getContactInfoList(tenant);
				String unitNumber = (String)viewRootMap.get("unitForNotification");
				eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
				eventAttributesValueMap.put("UNIT_NO", unitNumber);
				eventAttributesValueMap.put("TENANT_NAME", tenant.getPersonFullName());
				
				generateNotification(WebConstants.Notification_MetaEvents.Event_Violation_Added, contactInfoList, eventAttributesValueMap,null);
			}
		}
		}
		catch (PimsBusinessException e)
		{
			logger.logInfo("notifyTenant crashed...");
			e.printStackTrace();
		
		}
	}
		
	
//	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
//	{
//		final String METHOD_NAME = "generateNotification()";
//		boolean success = false;
//		
//		try
//		{
//			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
//			if ( recipientList != null && recipientList.size() > 0 )
//			{
//				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
//				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
//				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
//					event.setValue( eventAttributesValueMap );
//				if ( notificationType != null )
//					notificationProvider.fireEvent(event, recipientList, notificationType);
//				else
//					notificationProvider.fireEvent(event, recipientList);
//				
//			}
//			success = true;
//			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
//		}
//		catch (Exception exception)
//		{
//			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
//		}
//		
//		return success;
//	}
	
	
	
}
