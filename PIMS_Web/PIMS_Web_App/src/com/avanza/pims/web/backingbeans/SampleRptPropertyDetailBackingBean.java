package com.avanza.pims.web.backingbeans;
import java.util.ArrayList;
import java.util.List;

import com.avanza.pims.report.POJOReports.POJOReportDataSource;
import com.avanza.pims.report.POJOReports.POJOReportSpecification;
import com.avanza.pims.report.criteria.PropertyDetailReportCriteria;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;

import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;

public class SampleRptPropertyDetailBackingBean extends AbstractController
{	

	private static final long serialVersionUID = 3124991600424588667L;
	private PropertyDetailReportCriteria propertyDetailReportCriteria;
	private Logger logger;
	private String propertyId = "";

	public SampleRptPropertyDetailBackingBean() 
	{
		logger = Logger.getLogger(SampleRptPropertyDetailBackingBean.class);
//		if(CommonUtil.getIsEnglishLocale())
//			propertyDetailReportCriteria = new PropertyDetailReportCriteria(ReportConstant.Report.PROPERTY_DETAIL_EN, ReportConstant.Processor.PROPERTY_DETAIL,CommonUtil.getLoggedInUser());
//		else
//			propertyDetailReportCriteria = new PropertyDetailReportCriteria(ReportConstant.Report.PROPERTY_DETAIL_AR, ReportConstant.Processor.PROPERTY_DETAIL,CommonUtil.getLoggedInUser());
		
	}
	
	private PropertyView getDummyProperty() throws PimsBusinessException
	{
		Long propertyId = (this.propertyId !=null && !this.propertyId.equals("") )? Long.parseLong(this.propertyId) : new Long(1) ;
		PropertyServiceAgent psa = new PropertyServiceAgent();
		PropertyView pv =  psa.getPropertyByID(propertyId);
		return pv;
	}
	public String cmdView_Click() {
		
		try 
		{
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			PropertyView pv = getDummyProperty();	
			List<PropertyView> pvList = new ArrayList<PropertyView>(0);
			pvList.add(pv);
			
			POJOReportSpecification reportSpecification = new POJOReportSpecification(ReportConstant.Report.SAMPLE_PROPERTY_DETAIL_EN,CommonUtil.getLoggedInUser());
			POJOReportDataSource propertyDs = new POJOReportDataSource(
																	   ReportConstant.POJOReportsDataSourceConstants.SAMPLE_PROPERTY_DETAIL_ALIAS_PROPERTY,
																	   ReportConstant.POJOReportsDataSourceConstants.SAMPLE_PROPERTY_DETAIL_QUALIFIED_NAME_PROPERTY,
																	   PropertyView.class,
																	   pvList);
			POJOReportDataSource floorDs = new POJOReportDataSource(
																	   ReportConstant.POJOReportsDataSourceConstants.SAMPLE_PROPERTY_DETAIL_ALIAS_FLOOR,
																	   ReportConstant.POJOReportsDataSourceConstants.SAMPLE_PROPERTY_DETAIL_QUALIFIED_NAME_FLOOR,
																	   FloorView.class,
																	   pv.getFloorView());
			
			reportSpecification.addPojoReportDataSource(propertyDs);
			reportSpecification.addPojoReportDataSource(floorDs);
			
			request.getSession().setAttribute(ReportConstant.Keys.POJO_REPORT_DATASOURCE_KEY, reportSpecification);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_POJO_REPORT_JSP + "');");
			
		}
		catch (PimsBusinessException e) 
		{		
			e.printStackTrace();
		}
    	
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	

	public void init() {
		super.init();		
	}
	

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	
}
