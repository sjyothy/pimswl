package com.avanza.pims.web.backingbeans.botcontract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionContractAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class BOTContractListBacking extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected List<String> errorMessages = new ArrayList<String>();
	private interface Keys {
		public static String CONTRACT_LIST = "CONTRACT_LIST";
		public static String RECORD_SIZE = "RECORD_SIZE";
		public static String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static String CONTRACT_TYPE = "CONTRACT_TYPE";
	}

	private final static Logger logger = Logger
			.getLogger(BOTContractListBacking.class);
	ContractView contractView = new ContractView(); 
	@SuppressWarnings("unchecked")
	private final Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<ContractView> contractList=new ArrayList<ContractView>(0);

	// binded attributes with search criteria components
	private String serviceType;
	private String contractNumber;
	private String contractStatus;
	private String contractorNumber;
	private String contractorName;
	private String projectName;
	private String projectNumber;
	
	private String vendorCategory;
	private String contractType;
	
	// attributes used for a data grid
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	CommonUtil commonUtil = new CommonUtil();
	// UI component attributes
	private HtmlDataTable dataTable;
	private List<SelectItem> statusList = new ArrayList<SelectItem>();
	HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();;
	@SuppressWarnings("unchecked")
	public void init() {
		logger.logInfo("Stepped into the init method");

		try {
			if (!isPostBack()) {
				String contract_Type="";
				if(request.getParameter("type")!=null)
					contract_Type=request.getParameter("type").toString();
				else
				   contract_Type=WebConstants.BOT_CONTRACT;  
				List<DomainDataView> ddl = CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);
				DomainDataView ddvMaintenance = CommonUtil.getIdFromType(ddl,contract_Type);
				viewMap.put(Keys.CONTRACT_TYPE, ddvMaintenance);
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception e) {
			logger.logError("init crashed due to:" + e.getStackTrace());
		}
	}
	public String btnAddBOT()
	{
	  return "addBOT";	
		
	}
	// searches all the service contracts in the database according to the
	// criteria specified by the user on the screen
	@SuppressWarnings("unchecked")
	public void searchContracts() {
		String METHOD_NAME = "searchContracts()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");
		errorMessages = new ArrayList<String>(0);
		try {
			
			
			HashMap methodArgs = new HashMap(0);
			DomainDataView ddv = new DomainDataView();
			if(viewMap.containsKey(Keys.CONTRACT_TYPE))
			  ddv = (DomainDataView)viewMap.get(Keys.CONTRACT_TYPE);
			
				
			contractView.setContractTypeId(ddv.getDomainDataId());
            boolean hasSearchParamProvided=false;
			if(contractNumber!=null && contractNumber.trim().length()>0)
			{
			 contractView.setContractNumber(contractNumber);
			 hasSearchParamProvided=true;
			}
			if(contractStatus!=null && contractStatus.trim().length()>0 &&!contractStatus.equals("-1"))
			{
				contractView.setStatus(new Long(contractStatus));
			    hasSearchParamProvided=true;
		    }
			if(contractorNumber!=null && contractorNumber.trim().length()>0)
			{
				methodArgs.put("CONTRACTOR_NUMBER",contractorNumber);
				hasSearchParamProvided=true;
			}
			if(contractorName!=null && contractorName.trim().length()>0)
			{
					  methodArgs.put("CONTRACTOR_NAME",contractorName);
				     hasSearchParamProvided=true;
			}
			if(serviceType!=null  && !serviceType.equals("-1"))
			{
				methodArgs.put("CONTRACTOR_TYPE",serviceType);
				hasSearchParamProvided=true;
			}
			
			if(projectName!=null && projectName.trim().length()>0)
			{
				  methodArgs.put("PROJECT_NAME",projectName);
				  hasSearchParamProvided=true;
			}
			if(projectNumber!=null && projectNumber.trim().length()>0)
			{
				  methodArgs.put("PROJECT_NUMBER",projectName);
				  hasSearchParamProvided=true;
			 }
			
			
			if(hasSearchParamProvided)
			{
			ConstructionContractAgent cca = new ConstructionContractAgent();
			
			contractList = cca.getConstructionContracts(contractView, methodArgs);
			}
			else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			recordSize = contractList.size();
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;

			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;

			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			
			viewMap.put(Keys.RECORD_SIZE, recordSize);
			viewMap.put(Keys.PAGINATOR_MAX_PAGES, paginatorMaxPages);
			viewMap.put(Keys.CONTRACT_LIST, contractList);

			logger.logInfo(METHOD_NAME + " method completed Succesfully");
		} catch (PimsBusinessException e) {
			
			logger.LogException(METHOD_NAME +"|Exception Occured", e);
            errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	// resets the UI components of the search screen
	public void reset() {
		contractNumber = contractStatus = contractorNumber = projectName =  "";
	}

	// attribute accessors follows below
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getStatusList() {

		String METHOD_NAME = "getStatusList()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			statusList.clear();

			List<DomainDataView> statusComboList = (ArrayList<DomainDataView>) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get(WebConstants.CONTRACT_STATUS);
			for (int i = 0; i < statusComboList.size(); i++) {
				DomainDataView ddv = (DomainDataView) statusComboList.get(i);
				SelectItem item = null;
				item = new SelectItem(ddv.getDomainDataId().toString(),
						getIsEnglishLocale() ? ddv.getDataDescEn() : ddv
								.getDataDescAr());

				statusList.add(item);
			}

		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}

		logger.logInfo(METHOD_NAME + " method completed Succesfully");
		return statusList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	public String getContractorNumber() {
		return contractorNumber;
	}

	public void setContractorNumber(String contractorNumber) {
		this.contractorNumber = contractorNumber;
	}

	@SuppressWarnings("unchecked")
	public List<ContractView> getContractList() {
		if (viewMap.containsKey(Keys.CONTRACT_LIST))
			contractList = (List<ContractView>) viewMap.get(Keys.CONTRACT_LIST);

		return contractList;
	}

	public void setContractList(List<ContractView> contractList) {
		this.contractList = contractList;
	}

	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);
		if (recordSize == null)
			recordSize = 0;

		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getServiceType() {
		
		return serviceType;
	}

	@SuppressWarnings("unchecked")
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	


	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	public String imgEditContract_Click()
	{
		String methodName ="imgEditContract_Click";
		logger.logInfo(methodName+"|Start");
		ContractView cView = (ContractView)dataTable.getRowData();
		setRequestParam(WebConstants.CONSTRUCTION_CONTRACT_ID, cView.getContractId());
		String navigateTo = getNavigateTo();
			
		logger.logInfo(methodName+"|Finish");
		return navigateTo ;
	}

	private String getNavigateTo() {
		String navigateTo="";
		DomainDataView domainDataView = (DomainDataView)viewMap.get(Keys.CONTRACT_TYPE);
		if(domainDataView.getDataValue().compareTo(WebConstants.BOT_CONTRACT)==0)
		  navigateTo ="EditBOTContract";
		if(domainDataView.getDataValue().compareTo(WebConstants.ISTISNAA_CONTRACT)==0)
		  navigateTo ="EditIstisnaaContract";
		return navigateTo;
		
	}
	public String imgManageContract_Click()
	{
		logger.logInfo("imgManageContract_Click started...");
		String taskType = "";
		String navigateTo ="";
		try{
	   		ContractView cView =(ContractView)dataTable.getRowData();
	   		Long requestId =getRequestIdForContractId(cView.getContractId());
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
				navigateTo = getNavigateTo();
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
				return null;
	   		}
	   		logger.logInfo("imgManageContract_Click  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("imgManageContract_Click  crashed ", exception);
		}	
		return navigateTo;
		
		
	}
	private Long getRequestIdForContractId(Long contractId)throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_CONSTRUCTION_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(contractId);
		rv.setContractView(cv);
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
		  return ((RequestView)requestViewList.get(0)).getRequestId(); 
		}
		return null;
    }
	public String getErrorMessages()
	{
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : errorMessages) 
				{
					messageList += "<LI>" +message + "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	public String getContractorName() {
		return contractorName;
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}

	public String getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	public String getContractType() {
		if (viewMap.get(Keys.CONTRACT_TYPE) != null) {
			DomainDataView ddv = (DomainDataView) viewMap.get(Keys.CONTRACT_TYPE);
			contractType = getIsEnglishLocale() ? ddv.getDataDescEn() : ddv.getDataDescAr();
		}


		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getVendorCategory() {
		return vendorCategory;
	}

	public void setVendorCategory(String vendorCategory) {
		this.vendorCategory = vendorCategory;
	}

}