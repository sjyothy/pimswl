package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import com.avanza.core.util.Logger;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class Header extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(AbstractController.class);
	private List<String> errorMessages = new ArrayList<String>();
	private String img_blackframeleft1;
	private String img_topheaderright;
	private String img_topheaderleft;
	
	@Override
	public void init() 
	{
		super.init();
	}
	private BPMWorklistClient getBPMClientInstance() throws IOException
	{
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
		
		return bpmWorkListClient;
	}
	public boolean getIsTaskPresent()
	{
		return true;//viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	}
	public String onLogOut()
	{
		
		return "logout";
	}
	
	public void onReleaseTask()
	{
		try
		{
			errorMessages.clear();
			String user = getLoggedInUserId();
			UserTask userTask =   null;
			if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
				userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			} 
			
			BPMWorklistClient bpmWorkListClient = getBPMClientInstance();
			if(bpmWorkListClient.releaseTask(userTask, user))
			{
				userTask.setAccquiredby(null);
				userTask.setIsAccquiredByUser(false);
				CommonUtil.updateRequestTaskAcquiredBy( userTask,null );
			}
			//loadDataList();
//			infoMessage =  Comm("TaskList.messages.releaseSuccess");
			
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseSuccess"));
		}
		catch(PIMSWorkListException pimsExp)
		{
			logger.LogException("Release method crashed due to:",pimsExp);
			if(pimsExp.getExceptionCode() == ExceptionCodes.TASK_IS_NOT_ACQUIRED)
			{
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureNotClaimed"));
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
			}
		}
		catch(Exception exp)
		{
			logger.LogException("Release method crashed due to:",exp);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	
	public void onGetAlerts()
	{
		try
		{
			errorMessages.clear();
			String user = getLoggedInUserId();
			
		}
		
		catch(Exception exp)
		{
			logger.LogException("Error Occured:",exp);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	public String getLoggedInUserName() {
		return  getLoggedInUserObj().getSecondaryFullName();
	
	}
	public String getImg_blackframeleft1() {
		return "../"+ResourceUtil.getInstance().getPathProperty("img_blackframeleft1");
	}
	public void setImg_blackframeleft1(String img_blackframeleft1) {
		this.img_blackframeleft1 = img_blackframeleft1;
	}
	public String getImg_topheaderright() {
		return "../"+ResourceUtil.getInstance().getPathProperty("img_topheaderright");
	}
	public String getImg_topheaderleft() {
		return "../"+ResourceUtil.getInstance().getPathProperty("img_topheaderleft");
	}
	public String getErrorMessages() {
		String messageList = "";
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : errorMessages) {
				messageList = message;
			}
		}
		return (messageList);

	}
	
}
