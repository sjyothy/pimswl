package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;

public class EvaluationRequestHistoryTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(EvaluationRequestHistoryTabBacking.class);
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	HtmlDataTable requestHistoryTable=new HtmlDataTable();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	List<EvaluationApplicationFilterView> oldEvaluationRequest=new ArrayList<EvaluationApplicationFilterView>(0);
	 public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}
	public void openEvaluationRequestPopup()
	{
		String METHOD_NAME = "openEvaluationRequestPopup()";
		logger.logInfo(METHOD_NAME + " started...");
	try{
			EvaluationApplicationFilterView reqView=(EvaluationApplicationFilterView)requestHistoryTable.getRowData();
	   		Long requestId = reqView.getRequestId();
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
	   		sessionMap.put("IS_POPUP", true);
	   		sessionMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE, WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_VIEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
	   		executeJavascript("openEvaluationPopup();");
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}		
	}
	public List<EvaluationApplicationFilterView> getOldEvaluationRequest() 
	{
		if(viewMap.containsKey("REQUEST_VIEW_LIST"))
			{
				oldEvaluationRequest=(List<EvaluationApplicationFilterView>) viewMap.get("REQUEST_VIEW_LIST");
				if(oldEvaluationRequest!=null)
					viewMap.put("RECORD_SIZE", oldEvaluationRequest.size());
			}
		return oldEvaluationRequest;
	}
	public void setOldEvaluationRequest(
			List<EvaluationApplicationFilterView> oldEvaluationRequest) {
		this.oldEvaluationRequest = oldEvaluationRequest;
	}
	public HtmlDataTable getRequestHistoryTable() {
		return requestHistoryTable;
	}
	public void setRequestHistoryTable(HtmlDataTable requestHistoryTable) {
		this.requestHistoryTable = requestHistoryTable;
	}
	public void executeJavascript(String javascript) {
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);		
		} 
		catch (Exception exception) 
		{			
			logger.LogException(" executing JavaScript CRASHED --- ", exception);
		}
	}
	public Integer getPaginatorMaxPages() 
	{
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public Integer getPaginatorRows() 
	{
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public Integer getRecordSize() 
	{
		if(viewMap.containsKey("RECORD_SIZE"))
			recordSize=(Integer) viewMap.get("RECORD_SIZE");
		return recordSize;
	}
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
     
}

