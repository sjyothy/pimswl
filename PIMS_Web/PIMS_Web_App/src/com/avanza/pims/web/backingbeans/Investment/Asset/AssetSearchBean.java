package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.investment.AssetService;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.AssetView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.pims.ws.vo.SectorView;
import com.avanza.pims.ws.vo.SubSectorView;
import com.avanza.pims.ws.vo.ViolationView;

public class AssetSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(AssetSearchBean.class);
	private List<AssetView> dataList;
	AssetView assetView = new AssetView();
	AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
	private HtmlDataTable tbl_AssetSearch;
	private AssetView dataItem = new AssetView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	private String pageMode;
	private List<Long> alreadySelectedAssetList;
	
	private HtmlSelectOneMenu htmlSelectOneAssetClassStatus   = new HtmlSelectOneMenu();	
	
	
	private HtmlInputText htmlAssetNumber  = new HtmlInputText();
	private HtmlInputText htmlAssetName  = new HtmlInputText();
	private HtmlInputText htmlAssetSymbol  = new HtmlInputText();
	private HtmlInputText htmlAssetMarketName  = new HtmlInputText();
	
	private HtmlSelectOneMenu htmlSelectOneAssetStatus  = new HtmlSelectOneMenu();					
	private HtmlSelectOneMenu htmlSelectOneAssetCurrency  = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneAssetClass  = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneAssetSubClass  = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneAssetSector  = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneAssetSubSector   = new HtmlSelectOneMenu();
	
	
	private HtmlCalendar htmlStartDate = new HtmlCalendar();
	private HtmlCalendar htmlEndDate = new HtmlCalendar();
	
	private List<SelectItem> assetSubClassList = new ArrayList<SelectItem>();
	private List<SelectItem> assetSubSectorList = new ArrayList<SelectItem>();
	List<SelectItem> assetStatusList = new ArrayList<SelectItem>();


	
	// Constructors

	/** default constructor */
	public AssetSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
		pageMode = WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_SEARCH;
		alreadySelectedAssetList = new ArrayList<Long>(0);
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public AssetView getDataItem() {
		return dataItem;
	}

	public void setDataItem(AssetView dataItem) {
		this.dataItem = dataItem;
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/ViolationSearch.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               //"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        {
        	javaScriptText=extraJavaScript;
        }
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}


	


	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		try {

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();		
		
		String emptyValue = "-1";	
		
		Object assetNumber = htmlAssetNumber.getValue(); 
		Object assetName = htmlAssetName.getValue();
		Object assetSymbol = htmlAssetSymbol.getValue();
		Object assetMarketName = htmlAssetMarketName.getValue();
		
		Object assetStatus =  htmlSelectOneAssetStatus.getValue();					
		Object assetCurrency = htmlSelectOneAssetCurrency.getValue();
		Object assetClass = htmlSelectOneAssetClass.getValue();
		Object assetSubClass = htmlSelectOneAssetSubClass.getValue();
		Object assetSector =  htmlSelectOneAssetSector.getValue();
		Object assetSubSector = htmlSelectOneAssetSubSector.getValue();
		Object assetDateFrom =  htmlStartDate.getValue();
		Object assetDateTo = htmlEndDate.getValue();
		
		
		
		if(assetNumber != null && !assetNumber.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_NUMBER, assetNumber.toString());
		}
		if(assetName != null && !assetName.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_NAME, assetName.toString());
		}
		if(assetSymbol != null && !assetSymbol.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_SYMBOL, assetSymbol.toString());
		}
		if(assetMarketName != null && !assetMarketName.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_MARKET_NAME, assetMarketName.toString());
		}
		if(assetDateFrom != null && !assetDateFrom.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_DATE_FROM, assetDateFrom);
		}
		if(assetDateTo != null && !assetDateTo.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_DATE_TO, assetDateTo);
		}
		
		
		
		if(assetStatus != null && !assetStatus.toString().trim().equals("") && !assetStatus.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_STATUS, assetStatus.toString());
		}
		if(assetCurrency != null && !assetCurrency.toString().trim().equals("") && !assetCurrency.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_CURRENCY, assetCurrency.toString());
		}		
		if(assetClass != null && !assetClass.toString().trim().equals("") && !assetClass.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_CLASS, assetClass.toString());
		}		
		if(assetSubClass != null && !assetSubClass.toString().trim().equals("") && !assetSubClass.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_SUB_CLASS, assetSubClass.toString());
		}		
		if(assetSector != null && !assetSector.toString().trim().equals("") && !assetSector.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_SECTOR, assetSector.toString());
		}		
		if(assetSubSector != null && !assetSubSector.toString().trim().equals("") && !assetSubSector.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.ASSET_SUB_SECTOR, assetSubSector.toString());
		}		
		
	    if(assetStatus == null || assetStatus.toString().trim().equals("") || assetStatus.toString().trim().equals(emptyValue))
			
		{
			ArrayList<Long> statuses = new ArrayList<Long>();
		    List<SelectItem> StatusList =  getAssetStatusList();
			for(SelectItem item :StatusList)
			{
				Object Stat = item.getValue();
				
				if(Stat != null && Stat.toString().trim().length()>0)
					statuses.add(Long.parseLong(Stat.toString()));					
				
			}
			
			if(statuses.size() >0)
				searchCriteria.put("ASSET_STATUS_IN",statuses);
	     }
		
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
		Object assetNumber = htmlAssetNumber.getValue(); 
		Object assetName = htmlAssetName.getValue();
		Object assetSymbol = htmlAssetSymbol.getValue();
		Object assetMarketName = htmlAssetMarketName.getValue();
		
		Object assetStatus =  htmlSelectOneAssetStatus.getValue();					
		Object assetCurrency = htmlSelectOneAssetCurrency.getValue();
		Object assetClass = htmlSelectOneAssetClass.getValue();
		Object assetSubClass = htmlSelectOneAssetSubClass.getValue();
		Object assetSector =  htmlSelectOneAssetSector.getValue();
		Object assetSubSector = htmlSelectOneAssetSubSector.getValue();
		Object assetDateFrom =  htmlStartDate.getValue();
		Object assetDateTo = htmlEndDate.getValue();
		
		
		if(StringHelper.isNotEmpty(assetNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetName.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetSymbol.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetMarketName.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetDateFrom.toString()))
			return true;
		if(StringHelper.isNotEmpty(assetDateTo.toString()))
			return true;
		
		if(StringHelper.isNotEmpty(assetStatus.toString()) && !assetStatus.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(assetCurrency.toString()) && !assetCurrency.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(assetClass.toString()) && !assetClass.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(assetSubClass.toString()) && !assetSubClass.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(assetSector.toString()) && !assetSector.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(assetSubSector.toString()) && !assetSubSector.toString().equals("-1"))
			return true;
		
		
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		logger.logInfo(methodName + "|" + "Start::");
		try 
		{
		//	if(atleastOneCriterionEntered()){
				loadDataList();
				if(viewRootMap.get("SESSION_ASSET_SEARCH_LIST")!=null){
					List<AssetClassView> assetClassList = (List<AssetClassView>) viewRootMap.get("SESSION_ASSET_SEARCH_LIST");
					if(assetClassList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
	//		}
//			else{
//				errorMessages.clear();
//				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
//			}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		logger.logInfo(methodName + "|" + "Finish::");
		return "";
	}

	
   private void loadDataList() throws Exception
   {
	   String methodName = "btnSearch_Click";
	   logger.logInfo(methodName + "|" + "Start::");
	   Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	   HashMap searchMap= getSearchCriteria();
		
	   if(viewRootMap.containsKey("SESSION_ASSET_SEARCH_LIST"))
		   viewRootMap.remove("SESSION_ASSET_SEARCH_LIST");
	   
        // new function right here   	   
		List<AssetView> assetViewList =assetServiceAgent.getAllAsset(searchMap,alreadySelectedAssetList);
		viewRootMap.put("SESSION_ASSET_SEARCH_LIST",assetViewList);
		dataList=assetViewList;
	   
		  if(dataList!=null)
				recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
	        
		
		logger.logInfo(methodName + "|" + "Finish::");
   }
	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) 
			{
				
		    }
			
			updateValuesFromMaps();
			
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}
    
	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		HashMap searchMap;
		String methodName="prerender"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
		  if(sessionMap.containsKey("FROM_STATUS")  && sessionMap.get("FROM_STATUS").toString().equals("1"))
		  {
			  sessionMap.remove("FROM_STATUS");
			  loadDataList();
			    
		  }
		   
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		  logger.logInfo(methodName + "|" + "Finish...");
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY );
			LoadAssetStatuses();
		}
		else if ( viewRootMap.get( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewRootMap.get( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY );
		}
		
		
		
		// ALREADY SELECTED PORTFOLIO LIST
		if ( sessionMap.get( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST) != null )
		{
			alreadySelectedAssetList = (List<Long>) sessionMap.get( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST );
			sessionMap.remove( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST );
		}
		else if ( viewRootMap.get( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST ) != null )
		{
			alreadySelectedAssetList = (List<Long>) viewRootMap.get( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST );
		}
		
		
		
				
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewRootMap.put( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY, pageMode );
		}
	
		// ALREADY SELECTED PORTFOLIO LIST
		if ( alreadySelectedAssetList != null )
		{
			viewRootMap.put( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST, alreadySelectedAssetList );
		}
	
	}
	
	public void onPopulateAssetSubClass() 
	{	
				
		boolean isLocaleEnglish = getIsEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		AssetClassView assetClassView = new AssetClassView();
		assetClassView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
		assetClassView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );		
		List<AssetClassView> assetClassViewList = new ArrayList<AssetClassView>(0); 
		
		if(htmlSelectOneAssetClass.getValue().toString()!=null && !htmlSelectOneAssetClass.getValue().toString().equals(-1))
		{	
			try 
			{
				assetClassViewList = assetServiceAgent.getAssetSubClasses(Long.parseLong(htmlSelectOneAssetClass.getValue().toString()));
			}
			catch (Exception e) 
			{
				logger.LogException("getCurrencyList()", e);
			}
			
			if ( assetClassViewList != null )		
				for ( AssetClassView aCV : assetClassViewList )			
					assetSubClassList.add( new SelectItem(aCV.getAssetClassId().toString(), isLocaleEnglish ? aCV.getAssetClassNameEn() : aCV.getAssetClassNameAr() ) );
			
			Collections.sort(assetSubClassList, ListComparator.LIST_COMPARE);
			viewRootMap.put("ASSET_SUB_CLASS_NEW", assetSubClassList);
		}	
		
		
	}

	
	public void onPopulateAssetSubSector() 
	{	
				
		boolean isLocaleEnglish = getIsEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		List<SubSectorView> subSectorViewList = new ArrayList<SubSectorView>(0);
		assetSubSectorList = new ArrayList<SelectItem>(0);
		
	  
		if(htmlSelectOneAssetSector.getValue().toString()!=null && !htmlSelectOneAssetSector.getValue().toString().equals(-1))
		{	
			try 
			{
				subSectorViewList = assetServiceAgent.getSubSectorList(Long.parseLong(htmlSelectOneAssetSector.getValue().toString()));
			}
			catch (Exception e) 
			{
				logger.LogException("getCurrencyList()", e);
			}
			
			if ( subSectorViewList != null )		
				for ( SubSectorView sSV : subSectorViewList )			
					assetSubSectorList.add( new SelectItem(sSV.getSubSectorId().toString(), isLocaleEnglish ? sSV.getSectorNameEn() : sSV.getSectorNameAr() ) );
			
			Collections.sort(assetSubSectorList, ListComparator.LIST_COMPARE);
			
			viewRootMap.put("ASSET_SUB_SECTOR_NEW", assetSubSectorList);
		}	
	  
	}
	

	public List<AssetView> getDataList() {
		dataList =null;
		if (dataList == null) 
		{
			if(viewRootMap.containsKey("SESSION_ASSET_SEARCH_LIST"));
			dataList=(ArrayList)viewRootMap.get("SESSION_ASSET_SEARCH_LIST");
		}
		return dataList;

	}

	public void setDataList(List<AssetView> dataList) {
		this.dataList = dataList;
	}

	
	public String getLocale() 
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() 
	{
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}

	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}



	public String btnAddAsset_Click(){
	
		return "addAsset";
	}



	public HtmlInputText getHtmlAssetNumber() {
		return htmlAssetNumber;
	}

	public void setHtmlAssetNumber(HtmlInputText htmlAssetNumber) {
		this.htmlAssetNumber = htmlAssetNumber;
	}


	
	
	public String cmdView_Click() 
	{
		String path = null;
		assetView = (AssetView)tbl_AssetSearch.getRowData();
		sessionMap.put( WebConstants.ASSET.ASSET_VIEW , assetView);
		sessionMap.put(WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY, WebConstants.ASSET.ASSET_PAGE_MODE_VIEW);
		sessionMap.put(WebConstants.ASSET.ASSET_PAGE_MODE_KEY, WebConstants.ASSET.ASSET_PAGE_MODE_POPUP_VIEW_ONLY);
		openPopup("openAssetManagePopup();");
		return path;
	}
	
	
	public String cmdEdit_Click(){
		assetView = (AssetView)tbl_AssetSearch.getRowData();
		sessionMap.put( WebConstants.ASSET.ASSET_VIEW , assetView);
		sessionMap.put(WebConstants.ASSET.ASSET_PAGE_MODE_KEY, WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE);
		return "addAsset";
	}

	public String cmdDelete_Click()
	{
		try {
		
		AssetView assetView = (AssetView) tbl_AssetSearch.getRowData();
		assetView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
		Integer index =  tbl_AssetSearch.getRowIndex();
		viewRootMap.put("delete", index);
		assetServiceAgent.updateAsset(assetView);
		
		index = (Integer) viewRootMap.get("delete");
		if(viewRootMap.containsKey("SESSION_ASSET_SEARCH_LIST"))
		{
		 List<AssetView> tempList = (List<AssetView>) viewRootMap.get("SESSION_ASSET_SEARCH_LIST");
		 tempList.remove(index.intValue());
         dataList = tempList;
         viewRootMap.put("SESSION_ASSET_SEARCH_LIST",dataList);
        if(dataList!=null)
        {
			recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
         }
         
	     infoMessages.add(CommonUtil.getBundleMessage("assetManage.msg.deleteRecord"));
		}
		
		} 
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "cmdDelete_Click"; 
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsSearchMode()
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_SEARCH) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Boolean getIsMultiSelectPopupMode()
	{
		Boolean isMultiSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP ) )
		{ 
			isMultiSelectPopupMode = true;
		}	
			
		
		return isMultiSelectPopupMode;
	}
	
	public Boolean getIsSingleSelectPopupMode() 
	{
		Boolean isSingleSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP ) )
		{
		 
			isSingleSelectPopupMode = true;
		}	
		
		return isSingleSelectPopupMode;
	}
	
	

	public String onSingleSelect()
	{
		AssetView selectedAsset = (AssetView) tbl_AssetSearch.getRowData();
		sessionMap.put( WebConstants.ASSET.ASSET_VIEW , selectedAsset );
		executeJavascript("closeAndPassSelected();");
		return null;
	}
	
	public String onMultiSelect()
	{
		List<AssetView> selectedAssetList = new ArrayList<AssetView>(0);
		
		if ( dataList != null )
		{			
			for ( AssetView assetView : dataList )
			{
				if ( assetView.getIsSelected()!=null &&  assetView.getIsSelected() )
				{
					selectedAssetList.add( assetView );
				}
			}			
		}
		
		if ( selectedAssetList.size() == 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.NO_PORTFOLIO_SELECTED_ERROR ) );
		}
		else
		{
			sessionMap.put( WebConstants.ASSET.ASSET_VIEW_LIST , selectedAssetList );
			executeJavascript("closeAndPassSelected();");
		}
		
		return null;
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
	}
	 private void LoadAssetStatuses()
     {
    	 List<SelectItem> allowedAssetStatuses = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.ASSET_STATUS);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(pageMode.equals(WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP) && ( !(dd.getDataValue().equals(WebConstants.ASSET_STATUS_ACTIVE)))  )
    			   isAdd = false;
    			 if(pageMode.equals(WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP) && ( !(dd.getDataValue().equals(WebConstants.ASSET_STATUS_ACTIVE)))  )
      			   isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedAssetStatuses.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedAssetStatuses!= null && allowedAssetStatuses.size()>1)
			 Collections.sort(allowedAssetStatuses,ListComparator.LIST_COMPARE);
		
    	 viewRootMap.put("ALLOWED_ASSET_STATUSES",allowedAssetStatuses);
     }
	
	public HtmlSelectOneMenu getHtmlSelectOneAssetClassStatus() {
		return htmlSelectOneAssetClassStatus;
	}

	public void setHtmlSelectOneAssetClassStatus(
			HtmlSelectOneMenu htmlSelectOneAssetClassStatus) {
		this.htmlSelectOneAssetClassStatus = htmlSelectOneAssetClassStatus;
	}

	public HtmlInputText getHtmlAssetName() {
		return htmlAssetName;
	}

	public void setHtmlAssetName(HtmlInputText htmlAssetName) {
		this.htmlAssetName = htmlAssetName;
	}

	public HtmlInputText getHtmlAssetSymbol() {
		return htmlAssetSymbol;
	}

	public void setHtmlAssetSymbol(HtmlInputText htmlAssetSymbol) {
		this.htmlAssetSymbol = htmlAssetSymbol;
	}

	public HtmlInputText getHtmlAssetMarketName() {
		return htmlAssetMarketName;
	}

	public void setHtmlAssetMarketName(HtmlInputText htmlAssetMarketName) {
		this.htmlAssetMarketName = htmlAssetMarketName;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetStatus() {
		return htmlSelectOneAssetStatus;
	}

	public void setHtmlSelectOneAssetStatus(
			HtmlSelectOneMenu htmlSelectOneAssetStatus) {
		this.htmlSelectOneAssetStatus = htmlSelectOneAssetStatus;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetCurrency() {
		return htmlSelectOneAssetCurrency;
	}

	public void setHtmlSelectOneAssetCurrency(
			HtmlSelectOneMenu htmlSelectOneAssetCurrency) {
		this.htmlSelectOneAssetCurrency = htmlSelectOneAssetCurrency;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetClass() {
		return htmlSelectOneAssetClass;
	}

	public void setHtmlSelectOneAssetClass(HtmlSelectOneMenu htmlSelectOneAssetClass) {
		this.htmlSelectOneAssetClass = htmlSelectOneAssetClass;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetSubClass() {
		return htmlSelectOneAssetSubClass;
	}

	public void setHtmlSelectOneAssetSubClass(
			HtmlSelectOneMenu htmlSelectOneAssetSubClass) {
		this.htmlSelectOneAssetSubClass = htmlSelectOneAssetSubClass;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetSector() {
		return htmlSelectOneAssetSector;
	}

	public void setHtmlSelectOneAssetSector(
			HtmlSelectOneMenu htmlSelectOneAssetSector) {
		this.htmlSelectOneAssetSector = htmlSelectOneAssetSector;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAssetSubSector() {
		return htmlSelectOneAssetSubSector;
	}

	public void setHtmlSelectOneAssetSubSector(
			HtmlSelectOneMenu htmlSelectOneAssetSubSector) {
		this.htmlSelectOneAssetSubSector = htmlSelectOneAssetSubSector;
	}

	public HtmlCalendar getHtmlStartDate() {
		return htmlStartDate;
	}

	public void setHtmlStartDate(HtmlCalendar htmlStartDate) {
		this.htmlStartDate = htmlStartDate;
	}

	public HtmlCalendar getHtmlEndDate() {
		return htmlEndDate;
	}

	public void setHtmlEndDate(HtmlCalendar htmlEndDate) {
		this.htmlEndDate = htmlEndDate;
	}

	public HtmlDataTable getTbl_AssetSearch() {
		return tbl_AssetSearch;
	}

	public void setTbl_AssetSearch(HtmlDataTable tbl_AssetSearch) {
		this.tbl_AssetSearch = tbl_AssetSearch;
	}

	public List<SelectItem> getAssetSubClassList() {
		if(viewRootMap.containsKey("ASSET_SUB_CLASS_NEW"))
			assetSubClassList = (List<SelectItem>)viewRootMap.get("ASSET_SUB_CLASS_NEW");
		return assetSubClassList;
	}

	public void setAssetSubClassList(List<SelectItem> assetSubClassList) {
		this.assetSubClassList = assetSubClassList;		
	
	}

	public List<SelectItem> getAssetSubSectorList() {	
		if(viewRootMap.containsKey("ASSET_SUB_SECTOR_NEW"))
			assetSubSectorList = (List<SelectItem>)viewRootMap.get("ASSET_SUB_SECTOR_NEW");
		return assetSubSectorList;
	}

	public void setAssetSubSectorList(List<SelectItem> assetSubSectorList) {
		this.assetSubSectorList = assetSubSectorList;
	}

	public List<Long> getAlreadySelectedAssetList() {
		return alreadySelectedAssetList;
	}

	public void setAlreadySelectedAssetList(List<Long> alreadySelectedAssetList) {
		this.alreadySelectedAssetList = alreadySelectedAssetList;
	}

	public List<SelectItem> getAssetStatusList() {
		List<SelectItem> allowedAssetStatuses = new ArrayList<SelectItem>(); 	 
   	    if(viewRootMap.containsKey("ALLOWED_ASSET_STATUSES"))
   	    	allowedAssetStatuses = (List<SelectItem>) viewRootMap.get("ALLOWED_ASSET_STATUSES");
		return allowedAssetStatuses;
	}

	public void setAssetStatusList(List<SelectItem> assetStatusList) {
		this.assetStatusList = assetStatusList;
	}
}