package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlOutputLabel;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.construction.project.ProjectService;
import com.avanza.pims.ws.vo.DesignNoteView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ProjectDesignApprovalDetailsView;
import com.avanza.pims.ws.vo.ProjectDesignView;
import com.avanza.pims.ws.vo.ProjectDetailsView;
import com.avanza.pims.ws.vo.ProjectView;
import com.collaxa.cube.engine.sensor.sa.publisher.toplink.model.Domain;

public class ApproveProjectDesign extends AbstractController {
	/**
	 * 
	 */
	
	private String PROJECT_DESIGN_LIST = "PROJECT_DESIGN_LIST";
	private String PROJECT_DESIGN_NOTES_LIST = "PROJECT_DESIGN_NOTES_LIST";
	private String PROJECT_VIEW = "PROJECT_VIEW";	
	private final String DEFAULT_COMBO_VALUE ="-1";
	
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private final String PAGE_MODE_EDIT ="PAGE_MODE_EDIT";
	private final String PAGE_MODE ="PAGE_MODE";
	private String pageMode="default";
	
	private final String noteOwner = WebConstants.ProjectFollowUp.PROJECT_FOLLOW_UP;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_PROJECT_FOLLOW_UP;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_APPROVE_PROJECT_DESIGN;
	
	private HtmlDataTable dataTableDesignNotes = new HtmlDataTable();
	private HtmlDataTable dataTableProjectDesign = new HtmlDataTable();
	private HtmlOutputLabel lblPresentedTo=new HtmlOutputLabel();
	private HtmlInputText htmlInputProjectNumber = new HtmlInputText();
	private HtmlInputText htmlConsultantName = new HtmlInputText();
	private HtmlInputText htmlPresentedTo = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneDesignType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneDesignStatus = new HtmlSelectOneMenu();
	private HtmlCalendar htmlCalendarPresentedDate = new HtmlCalendar();
	private HtmlSelectOneMenu htmlSelectOnePresentedTo = new HtmlSelectOneMenu();
	
	private HtmlCommandButton addDesignNote = new HtmlCommandButton();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private DomainDataView ddvPresentedToOthers;
	private transient Logger logger = Logger.getLogger(ApproveProjectDesign.class);
	
	private final String TAB_DESIGN_NOTES = "designNotesTab";
	private final String TAB_DESIGN_HISTORY = "designHistoryTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
		
	private String heading="";

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	
	public ProjectView getProjectView() {
		ProjectView projectView = null;
		if(viewMap.containsKey(PROJECT_VIEW))
		{
			 Object obj = viewMap.get(PROJECT_VIEW);
			 if(obj != null)
				 projectView = (ProjectView)obj;
		}		
		return projectView;
	}

	public void setProjectView(ProjectView projectView) {
		viewMap.put(PROJECT_VIEW, projectView);
	}
	
	@SuppressWarnings("unchecked")
	public List<ProjectDesignView> getprojectDesignViewList() {		 
		 List<ProjectDesignView> dataList  = new ArrayList<ProjectDesignView>();
		 if(viewMap.containsKey(PROJECT_DESIGN_LIST))
		 {
			 dataList = (List<ProjectDesignView>) viewMap.get(PROJECT_DESIGN_LIST);		 
		 }
		return dataList;
	}	
	public void setprojectDesignViewList(List<ProjectDesignView> projectDesignViewList) {
		viewMap.put(PROJECT_DESIGN_LIST,projectDesignViewList);
		if(projectDesignViewList!=null)
			viewMap.put("designHistoryRecordSize", projectDesignViewList.size());
	}
	
	public void setDesignNoteViewList(List<DesignNoteView> designNoteViewList) {
		viewMap.put(PROJECT_DESIGN_NOTES_LIST,designNoteViewList);
		if(designNoteViewList!=null)
			viewMap.put("designNotesRecordSize", designNoteViewList.size());
	}
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	@SuppressWarnings("unchecked")
	public List<DesignNoteView> getDesignNoteViewList() {		
		 List<DesignNoteView> dataList  = new ArrayList<DesignNoteView>();
		 if(viewMap.containsKey(PROJECT_DESIGN_NOTES_LIST))
		 {
			 dataList = (List<DesignNoteView>) viewMap.get(PROJECT_DESIGN_NOTES_LIST);		 
		 }
		return dataList;
	}
	
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long projectId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			if(userTask!=null)
			{
				viewMap.put(TASK_LIST_USER_TASK,userTask);
				
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.UserTasks.PROJECT_ID)!=null)
				{
					projectId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.PROJECT_ID));
					viewMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
				}
				String taskType = userTask.getTaskType();
				
				if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_APPROVE_PROJECT_DESIGN))
				{
					pageMode = PAGE_MODE_EDIT;
				}
				
				canAddAttachmentsAndComments(true);
				viewMap.put(PAGE_MODE, pageMode);
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
				
				ProjectView pv = new ProjectView();
				pv.setProjectId(projectId);
				setProjectView(pv);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
//			UserTask temp = new UserTask();
//			Map<String,String> taskAtr = new HashMap<String, String>();
//			taskAtr.put(WebConstants.UserTasks.PROJECT_ID,"8");
//			temp.setTaskAttributes(taskAtr);
//			temp.setTaskType(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_APPROVE_PROJECT_DESIGN);
//			sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,temp);
			
			if(!isPostBack())
			{			
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				checkTask();
				viewMap.put("showApprove", true);
				viewMap.put("showSave", true);
				viewMap.put("showGridActions", true);
				loadProjectDetails();
				LoadProjectDesignNotes();
				LoadProjectDesignHistory();	
				htmlPresentedTo.setRendered(false);
				lblPresentedTo.setRendered(false);
				ddvPresentedToOthers=CommonUtil.getDomainDataFromId(CommonUtil.getDomainDataListForDomainType(WebConstants.DomainTypes.PROJECT_DESIGN_PRESENTED_TO), 300118L);
				viewMap.put("PRSENTED_TO_DDV", ddvPresentedToOthers);
			}
			else
			{
				if(sessionMap.containsKey("DESIGN_NOTE_ADDED"))
				{
					LoadProjectDesignNotes();
					sessionMap.remove("DESIGN_NOTE_ADDED");
//					infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_ADD_SUCCESS);
				}
			}
			
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public String approve() {
		logger.logInfo("approve() started...");
		Long projectId=null;
		try {
				if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
					projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
				if(projectId!=null){ 
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					if(completeTask(TaskOutcome.APPROVE)){
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.DESIGN_APPROVED, projectId);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_APPROVE_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_VIEW);
						canAddAttachmentsAndComments(false);
						viewMap.remove("showApprove");
						viewMap.remove("showSave");
						viewMap.remove("showGridActions");
					}
					else{
						errorMessages.clear();
						errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_APPROVE_FAILURE));
					}
				}
					
			logger.logInfo("approve() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("approve() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_APPROVE_FAILURE));
		}
		return "approve";
	}
	
	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome){
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		try {
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = CommonUtil.getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, taskOutcome);
				}
			logger.logInfo("completeTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.logError("completeTask() crashed");
			success = false;
		}
		return success;
	}
	
	private void LoadProjectDesignHistory()	
	{
		ProjectView pv = getProjectView();
		if(pv != null && pv.getProjectId() != null)
		{
			try
			{
				ProjectService ps = new ProjectService();
				List<ProjectDesignView> projectDesignViewList =   ps.getDesignsForProject(pv.getProjectId());
				setprojectDesignViewList(projectDesignViewList);
				getprojectDesignViewList();
			}
			catch(Exception exp)
			{
				logger.LogException("Failed to load project design notes", exp);
			}
		}
	}
	
	private void loadProjectDetails() throws Exception	
	{
		logger.logInfo("loadProjectDetails() started...");
		
		ProjectView pv = getProjectView();
		if(pv != null && pv.getProjectId() != null)
		{
			try
			{
				ProjectService projectService = new ProjectService();
				ProjectDetailsView projectDetailsView = projectService.getProjectDetailsById(pv.getProjectId(), CommonUtil.getArgMap());
				ProjectDesignApprovalDetailsView projectDesignApprovalDetailsView = projectService.getProjectDetailsForDesignApprovalByProjectId(pv.getProjectId(), CommonUtil.getArgMap());
				if(projectDesignApprovalDetailsView!=null){
					viewMap.put("projectNumber", projectDesignApprovalDetailsView.getProjectNumber());
					viewMap.put("consultantName", projectDesignApprovalDetailsView.getConsultantName());
					viewMap.put("consultantId", projectDesignApprovalDetailsView.getConsultantId());
				}
				else{
					viewMap.put("projectNumber", projectDetailsView.getProjectNumber());
					viewMap.remove("showApprove");
					viewMap.remove("showSave");
					viewMap.remove("showGridActions");
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_NO_CONSULTANT_CONTRACT));
					viewMap.put("consultantNotFound", true);
					viewMap.put("errorMessages",errorMessages);
					canAddAttachmentsAndComments(false);
				}
				logger.logInfo("loadProjectDetails() completed successfully!!!");
			}
			catch(Exception exp)
			{
				logger.LogException("loadProjectDetails() crashed: Failed to load project number and consultant name!", exp);
				throw exp;
			}
		}
	}
	
	private void LoadProjectDesignNotes()	
	{
		ProjectView pv = getProjectView();
		if(pv != null && pv.getProjectId() != null)
		{
			try
			{
				ProjectService ps = new ProjectService();
				List<DesignNoteView> designNoteViewList =   ps.getDesignNotesForProject(pv.getProjectId());
				setDesignNoteViewList(designNoteViewList);
			}
			catch(Exception exp)
			{
				logger.LogException("Failed to load project design notes", exp);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public String deleteProjectDesignNote()	{
		logger.logInfo("deleteProjectDesignNote() started...");
		try
		{	
			if(dataTableDesignNotes.isRowAvailable())
			{
				Object obj = dataTableDesignNotes.getRowData();
				DesignNoteView designNoteView = (DesignNoteView) obj;
				AddDesignNote addDesignNote = new AddDesignNote();
				designNoteView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
				addDesignNote.persistProjectDesignNote(designNoteView);
				LoadProjectDesignNotes();
				infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_DELETE_SUCCESS);
			}			
	        logger.logInfo("deleteProjectDesignNote() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("deleteProjectDesignNote() crashed ", exception);
		}
		return "";
    }
	
	@SuppressWarnings("unchecked")
	public void openAddDesignNotePopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openAddDesignNotePopup() started...");
		try
		{	
			if(dataTableDesignNotes.isRowAvailable())
			{
				Object obj = dataTableDesignNotes.getRowData();
				DesignNoteView designNoteView = (DesignNoteView) obj;
				sessionMap.put(WebConstants.DESIGN_NOTE_VIEW, designNoteView);		
			}
			ProjectView pv = getProjectView();			
			sessionMap.put(WebConstants.PROJECT_VIEW, pv);			
			String javaScriptText = "javascript:showAddDesignNotePopup();";	 
			RegisterJavaScript(javaScriptText);
	        logger.logInfo("openAddDesignNotePopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAddDesignNotePopup() crashed ", exception);
		}
    }
	
	private void RegisterJavaScript(String javaScriptText )
	{
		final String viewId = "/approveProjectDesign.jsp"; 
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);  
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	public Boolean validateForApprove() throws Exception{
	    logger.logInfo("validateForApprove() started...");
	    Boolean validated = true;
	    try
		{
	    	List<ProjectDesignView> projectDesignViewList = getprojectDesignViewList();
	    	if(projectDesignViewList.isEmpty())
	    	{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_NO_APPROVED_DESIGN));
				validated = false;
			}
	    	/*else{
	    		DomainDataView domainDataView = new UtilityServiceAgent().getDomainDataByValue(WebConstants.de, domainDataValue)
		    	for(ProjectDesignView projectDesignView: projectDesignViewList){
		    		if(==null || presentedTo.toString().equals("") || presentedTo.toString().equals(DEFAULT_COMBO_VALUE) )
					{
						errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.PRESENTED_TO));
						validated = false;
						break;
					}	
		    	}
	    	}*/
			
			
			logger.logInfo("validateForApprove() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForApprove() crashed ", exception);
			throw exception;
		}
		return validated;
	}

	public ProjectDesignView validateForSave() throws Exception{
	    logger.logInfo("validateForSave() started...");
	    Boolean validated = true;
	    ProjectDesignView projectDesignView = new ProjectDesignView();
	    try
		{
	    	Object presentedDate = htmlCalendarPresentedDate.getValue();
	    	Object designType = htmlSelectOneDesignType.getValue();
	    	Object designStatus = htmlSelectOneDesignStatus.getValue();
	    	Object presentedTo = htmlSelectOnePresentedTo.getValue();
	    	if(viewMap.containsKey("PRSENTED_TO_DDV"))
				ddvPresentedToOthers=(DomainDataView) viewMap.get("PRSENTED_TO_DDV");
	    	if(designType==null || designType.toString().equals("") || designType.toString().equals(DEFAULT_COMBO_VALUE))
			{
				errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_TYPE));
				validated = false;
			}
			else
				projectDesignView.setDesignTypeId(Long.parseLong(designType.toString()));
			
			if(designStatus==null || designStatus.toString().equals("") || designStatus.toString().equals(DEFAULT_COMBO_VALUE))
			{
				errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_STATUS));
				validated = false;
			}
			else
				projectDesignView.setDesignStatusId(Long.parseLong(designStatus.toString()));

			
			if(presentedDate==null || presentedDate.toString().equals(""))
			{
				errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.PRESENTED_DATE));
				validated = false;
			}
			else
				projectDesignView.setPresentingDate((Date)presentedDate);
						
			if(presentedTo==null || presentedTo.toString().equals("") || presentedTo.toString().equals(DEFAULT_COMBO_VALUE) )
			{
				errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.PRESENTED_TO));
				validated = false;
			}
			else if( ddvPresentedToOthers !=null && ddvPresentedToOthers.getDomainDataId()!=null && presentedTo.toString().compareTo(ddvPresentedToOthers.getDomainDataId().toString())==0)
			{
				if(htmlPresentedTo.getValue()==null || htmlPresentedTo.getValue().toString().equals(""))
					{
						errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.PRESENTED_TO));
						validated = false;
					}
				else
					{	
						projectDesignView.setPresentedToEn(htmlPresentedTo.getValue().toString());
						projectDesignView.setPresentedToAr(htmlPresentedTo.getValue().toString());
						projectDesignView.setPresentedToOther(htmlPresentedTo.getValue().toString());
						projectDesignView.setPresentedTo(Long.parseLong(presentedTo.toString()));
					}
			}
			else
				projectDesignView.setPresentedTo(Long.parseLong(presentedTo.toString()));
			
			if(!AttachmentBean.mandatoryDocsValidated())
	    	{	    		
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;				
	    	}   
			
			logger.logInfo("validateForSave() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForSave() crashed ", exception);
			throw exception;
		}
		if(validated)
			return projectDesignView;
		else
			return null;
	}
	
	public String saveProjectDesign()
	{
		logger.logInfo("saveProjectDesign() started...");
    	try{    	
    		ProjectDesignView projectDesignView = validateForSave();
    		if(projectDesignView != null)
    		{    			
    			String loggedInUser = CommonUtil.getLoggedInUser();
    			ProjectService ps = new ProjectService();    			
    			ProjectView pv = getProjectView();
    			projectDesignView.setProjectId(pv.getProjectId());
    			
    			projectDesignView.setUpdatedBy(loggedInUser);
    			projectDesignView.setUpdatedOn(new Date());
    			if(projectDesignView.getProjectDesignId() == null)
    			{
    				projectDesignView.setCreatedBy(loggedInUser);
    				projectDesignView.setCreatedOn(new Date());
    				projectDesignView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
    				projectDesignView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
    			}
    			ps.persistProjectDesign(projectDesignView);
    			LoadProjectDesignHistory();
    			
    			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_SAVE_SUCCESS);
    		}
    			
    		logger.logInfo("saveProjectDesign() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_SAVE_FAILURE));
    		logger.LogException("saveProjectDesign() crashed ",exception);
    	}
    	catch(Exception exception){
    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.ApproveDesign.MSG_DESIGN_SAVE_FAILURE));
    		logger.LogException("saveProjectDesign() crashed ",exception);
    	}
        return "saveProjectDesign";
	}

	@SuppressWarnings("unchecked")
	public void openProjectViewPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openProjectViewPopup() started...");
		try
		{
			ProjectView projectView = getProjectView();
			if(projectView.getProjectId()!=null){
				sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectView.getProjectId());
		   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_POPUP_VIEW); 

		   		FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:openProjectViewPopup();";
		
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
	        logger.logInfo("openProjectViewPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openProjectViewPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void openConsultantViewPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openConsultantViewPopup() started...");
		try
		{
			if(getConsultantId()!=null){
		   		FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:showPersonPopup("+ getConsultantId() +");";
		
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
	        logger.logInfo("openConsultantViewPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openConsultantViewPopup() crashed ", exception);
		}
    }
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		
		pageMode = (String) viewMap.get(PAGE_MODE);
		if(pageMode==null)
			pageMode = PAGE_MODE_VIEW;
		
		logger.logInfo("pageMode : %s", pageMode);
		
		if(viewMap.containsKey("infoMessage")){
			infoMessage = (String) viewMap.get("infoMessage");
			if(viewMap.containsKey("secondPrerenderInfoCall")){
				viewMap.remove("infoMessage");
				viewMap.remove("secondPrerenderInfoCall");
			}
			viewMap.put("secondPrerenderCall", true);
		}
		if(viewMap.containsKey("errorMessages")){
			errorMessages = (List<String>) viewMap.get("errorMessages");
			if(viewMap.containsKey("secondPrerenderErrorCall")){
				viewMap.remove("errorMessages");
				if(viewMap.containsKey("consultantNotFound"))
					viewMap.remove("secondPrerenderErrorCall");
			}
			viewMap.put("secondPrerenderCall", true);
		}
		
		String projectNumber = (String) viewMap.get("projectNumber");
		htmlInputProjectNumber.setValue(projectNumber);
		
		String consultantName = (String) viewMap.get("consultantName");
		htmlConsultantName.setValue(consultantName);
	}	

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen==null)
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }
	
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * @param heading
	 *            the heading to set
	 */
	public void setHeading(String heading) {
		this.heading = heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}



	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	
	public Integer getDesignHistoryRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		recordSize = (Integer) viewMap.get("designHistoryRecordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	
	public Integer getDesignNotesRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		recordSize = (Integer) viewMap.get("designNotesRecordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public Long getConsultantId() {
		Long consultantId = (Long) viewMap.get("consultantId");
		return consultantId;
	}
	
	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public HtmlInputText getHtmlInputProjectNumber() {
		return htmlInputProjectNumber;
	}

	public void setHtmlInputProjectNumber(HtmlInputText htmlInputProjectNumber) {
		this.htmlInputProjectNumber = htmlInputProjectNumber;
	}

	public HtmlInputText getHtmlConsultantName() {
		return htmlConsultantName;
	}

	public void setHtmlConsultantName(HtmlInputText htmlConsultantName) {
		this.htmlConsultantName = htmlConsultantName;
	}

	public HtmlSelectOneMenu getHtmlSelectOneDesignType() {
		return htmlSelectOneDesignType;
	}

	public void setHtmlSelectOneDesignType(HtmlSelectOneMenu htmlSelectOneDesignType) {
		this.htmlSelectOneDesignType = htmlSelectOneDesignType;
	}

	public HtmlSelectOneMenu getHtmlSelectOneDesignStatus() {
		return htmlSelectOneDesignStatus;
	}

	public void setHtmlSelectOneDesignStatus(HtmlSelectOneMenu htmlSelectOneDesignStatus) {
		this.htmlSelectOneDesignStatus = htmlSelectOneDesignStatus;
	}

	public HtmlCalendar getHtmlCalendarPresentedDate() {
		return htmlCalendarPresentedDate;
	}

	public void setHtmlCalendarPresentedDate(HtmlCalendar htmlCalendarPresentedDate) {
		this.htmlCalendarPresentedDate = htmlCalendarPresentedDate;
	}

	public HtmlSelectOneMenu getHtmlSelectOnePresentedTo() {
		return htmlSelectOnePresentedTo;
	}

	public void setHtmlSelectOnePresentedTo(HtmlSelectOneMenu htmlSelectOnePresentedTo) {
		this.htmlSelectOnePresentedTo = htmlSelectOnePresentedTo;
	}

	public HtmlDataTable getDataTableDesignNotes() {
		return dataTableDesignNotes;
	}

	public void setDataTableDesignNotes(HtmlDataTable dataTableDesignNotes) {
		this.dataTableDesignNotes = dataTableDesignNotes;
	}

	public HtmlDataTable getDataTableProjectDesign() {
		return dataTableProjectDesign;
	}

	public void setDataTableProjectDesign(HtmlDataTable dataTableProjectDesign) {
		this.dataTableProjectDesign = dataTableProjectDesign;
	}

	public HtmlCommandButton getAddDesignNote() {
		return addDesignNote;
	}

	public void setAddDesignNote(HtmlCommandButton addDesignNote) {
		this.addDesignNote = addDesignNote;
	}
	
	public Boolean getShowApprove() {
		if(!viewMap.containsKey("showApprove"))
			return false;
		return true;
	}
	
	public Boolean getShowSave() {
		if(!viewMap.containsKey("showSave"))
			return false;
		return true;
	}
	
	public Boolean getShowGridActions() {
		if(!viewMap.containsKey("showGridActions"))
			return false;
		return true;
	}
	public void displayFieldForPresentedTo(ValueChangeEvent e)
	{
		if(viewMap.containsKey("PRSENTED_TO_DDV"))
		{
		ddvPresentedToOthers=(DomainDataView) viewMap.get("PRSENTED_TO_DDV");
		if(e.getNewValue()!=null && e.getNewValue().toString().compareTo(ddvPresentedToOthers.getDomainDataId().toString())==0)
			{
				htmlPresentedTo.setRendered(true);
				lblPresentedTo.setRendered(true);
			}
		else
			{
				htmlPresentedTo.setRendered(false);
				lblPresentedTo.setRendered(false);
			}
		}
	}

	public HtmlInputText getHtmlPresentedTo() {
		return htmlPresentedTo;
	}

	public void setHtmlPresentedTo(HtmlInputText htmlPresentedTo) {
		this.htmlPresentedTo = htmlPresentedTo;
	}

	public DomainDataView getDdvPresentedToOthers() {
		return ddvPresentedToOthers;
	}

	public void setDdvPresentedToOthers(DomainDataView ddvPresentedToOthers) {
		this.ddvPresentedToOthers = ddvPresentedToOthers;
	}

	public HtmlOutputLabel getLblPresentedTo() {
		return lblPresentedTo;
	}

	public void setLblPresentedTo(HtmlOutputLabel lblPresentedTo) {
		this.lblPresentedTo = lblPresentedTo;
	}
}
