package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.EmailValidator;
import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.SecUserUserGroupId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorSearch.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BusinessActivityView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContactReferenceView;
import com.avanza.pims.ws.vo.ContractorTypeView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.ui.util.ResourceUtil;

public class ContractorAdd extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(ContractorAdd.class);
	private boolean isAdded=false;

	public interface Keys {
		public String CONTRACTOR_TYPE_LIST = "CONTRACTOR_TYPE_LIST";
		public String LICENSE_SOURCE_LIST = "LICENSE_SOURCE_LIST";
		public String BUSINESS_ACTIVITY_LIST = "BUSINESS_ACTIVITY_LIST";
		public String SERVICE_TYPE_LIST = "SERVICE_TYPE_LIST";

		public String CONTRACTOR_STATUS_LIST = "CONTRACTOR_STATUS_LIST";
		public String CONTACT_INFO_LIST = "CONTACT_INFO_LIST";
		public String CONTACT_REFERENCE_LIST = "CONTACT_REFERENCE_LIST";
		public String CONTACT_INFO_CREATED_BY = "CONTACT_INFO_CREATED_BY";
		public String CONTACT_INFO_CREATED_ON = "CONTACT_INFO_CREATED_ON";
		public String SELECTED_CONTACT_INFO_ROW_INDEX = "SELECTED_CONTACT_INFO_ROW_INDEX";
		public String CONTACT_INFO_ROW_INDEX = "CONTACT_INFO_ROW_INDEX";
		public String CONTACT_INFO_ROW_ID = "CONTACT_INFO_ROW_ID";
		public String CR_CONTACT_INFO_CREATED_BY = "CR_CONTACT_INFO_CREATED_BY";
		public String CR_CONTACT_INFO_CREATED_ON = "CR_CONTACT_INFO_CREATED_ON";
		public String CONTACT_REFERENCE_CREATED_BY = "CONTACT_REFERENCE_CREATED_BY";
		public String CONTACT_REFERENCE_CREATED_ON = "CONTACT_REFERENCE_CREATED_ON";
		public String SELECTED_CONTACT_REF_ROW_INDEX = "SELECTED_CONTACT_REF_ROW_INDEX";
		public String CONTACT_REF_ROW_INDEX = "CONTACT_REF_ROW_INDEX";

		public String COUNTRY_LIST = "COUNTRY_LIST";
		public String STATE_LIST = "STATE_LIST";
		public String CITY_LIST = "CITY_LIST";
		public String CR_STATE_LIST = "CR_STATE_LIST";
		public String CR_CITY_LIST = "CR_CITY_LIST";
		
		public String VIEW_MODE = "viewMode";
		public String POPUP_MODE = "popup";

		public String CONTRACTOR_STATUS = "CONTRACTOR_STATUS";
		public String CONTRACTOR_COMMERCIAL_NAME = "CONTRACTOR_COMMERCIAL_NAME";
		public String CONTRACTOR_NAME_AR = "CONTRACTOR_NAME_AR";
		public String CONTRACTOR_NAME_EN = "CONTRACTOR_NAME_EN";
		public String CONTRACTOR_TYPE = "CONTRACTOR_TYPE";
		public String CONTRACTOR_LICENSE_NUMBER = "CONTRACTOR_LICENSE_NUMBER";
		public String CONTRACTOR_LICENSE_SOURCE = "CONTRACTOR_LICENSE_SOURCE";
		public String CONTRACTOR_ISSUE_DATE = "CONTRACTOR_ISSUE_DATE";
		public String CONTRACTOR_EXPIRY_DATE = "CONTRACTOR_EXPIRY_DATE";
		public String CONTRACTOR_BUSINESS_ACTIVITIES = "CONTRACTOR_BUSINESS_ACTIVITIES";
		public String CONTRACTOR_SERVICE_TYPES = "CONTRACTOR_SERVICE_TYPES";
		public String MODE_POPUP = "MODE_POPUP";
		public static final String CONTRACTOR_TYPE_ID = "contractorTypeId";
		public String CONTACT_INFO_RECORD_SIZE="CONTACT_INFO_RECORD_SIZE";
		public String CONTACT_REF_RECORD_SIZE="CONTACT_REF_RECORD_SIZE";
		
		
	}

	HttpServletRequest request = (HttpServletRequest) FacesContext
			.getCurrentInstance().getExternalContext().getRequest();
	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<String> errorMessages;
	private List<String> infoMessages;

	// UI component attributes
	private HtmlDataTable contactInfoDataTable = new HtmlDataTable();
	private HtmlDataTable contactRefDataTable = new HtmlDataTable();

	// attributes binded with UI Components to save their values entered by the
	// user - Contractor Details Tab
	private String grpNumber;
	private String contractorStatus;
	private String commercialName;
	private String contractorNameAr;
	private String contractorNameEn;
	private String contractorType;
	private String licenseNumber;
	private String licenseSource;
	private Date issueDate;
	private Date expiryDate;
	private List<String> businessActivities;
	private List<String> serviceTypes;

	private String contactInfoCreatedBy;
	private String contactInfoCreatedOn;
	private String contactInfoRowId;
	private String crContactInfoCreatedBy;
	private String crContactInfoCreatedOn;
	private String contactRefCreatedBy;
	private String contactRefCreatedOn;

	// attributes binded with UI Components to save their values entered by the
	// user - Address Details Tab
	private String address1;
	private String address2;
	private String street;
	private String postCode;
	private String country;
	private String state;
	private String city;
	private String homePhone;
	private String officePhone;
	private String fax;
	private String email;

	// attributes binded with UI Components to save their values entered by the
	// user - Contact Persons Tab
	private String crTitleId;
	private String crFirstName;
	private String crMiddleName;
	private String crLastName;
	private String crAddress1;
	private String crAddress2;
	private String crDesignation;
	private String crStreet;
	private String crPostCode;
	private String crCountryId;
	private String crStateId;
	private String crCityId;
	private String crHomePhone;
	private String crOfficePhone;
	private String crMobileNumber;
	private String crFax;
	private String crEmail;
	private Long personId;
	
	private Integer contactInfoPaginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer contactInfoRecordSize = 0;
	private Integer contactRefPaginatorMaxPages = 0;
	private Integer contactRefRecordSize = 0;

	// lists of UI components goes here
	private Map<String, String> contractorStatusList;
	private Map<String, String> contractorTypeList;
	private Map<String, String> licenseSourceList;
	private Map<String, String> businessActivityList;
	private Map<String, String> serviceTypeList;
	private List<ContactInfoView> contactInfoList;
	private List<ContactReferenceView> contactReferencesList;

	private Map<String, String> countryList;
	private Map<String, String> stateList;
	private Map<String, String> cityList;
	private Map<String, String> CRStateList;
	private Map<String, String> CRCityList;
	
	private HtmlSelectOneMenu cmbContractorType=new HtmlSelectOneMenu();
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_CONTRACTOR;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CONTRACTOR;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CONTRACTOR;
	
	
	HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		contactInfoList = new ArrayList<ContactInfoView>();
		contactReferencesList = new ArrayList<ContactReferenceView>();

		try {
			if (!isPostBack()) {
				if (request.getParameter(Keys.VIEW_MODE) != null)
					viewMap.put(Keys.VIEW_MODE, request.getParameter(
							Keys.VIEW_MODE).toString());
				if(requestParam.getParameter("MODE_POPUP")!=null){
					viewMap.put(Keys.MODE_POPUP,requestParam.getParameter("MODE_POPUP"));
					
				}
				
				if (request.getParameter(Keys.CONTRACTOR_TYPE_ID) != null 
					&&  !request.getParameter(Keys.CONTRACTOR_TYPE_ID).equals("")){
					viewMap.put(Keys.CONTRACTOR_TYPE_ID, request.getParameter(
							Keys.CONTRACTOR_TYPE_ID).toString());
	
					//bcoz set value by default 
				    cmbContractorType.setDisabled(true);
				    
				}
	

				canAddAttachmentsAndComments(true);
				loadAttachmentsAndComments();
				loadMapValues();
				
				
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}

	@SuppressWarnings("unchecked")
	private void loadAttachmentsAndComments() {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,
				WebConstants.PROCEDURE_TYPE_EXTEND_REQUEST);

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_EXTEND_REQUEST;

		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.ExtendApplication.EXTEND_REQUEST);
	}

	// fills the maps that is populated in the UI components
	@SuppressWarnings("unchecked")
	private void loadMapValues() {
		loadContractorStatus();
		viewMap.put(Keys.CONTRACTOR_STATUS_LIST, contractorStatusList);

		loadContractorTypes();
		viewMap.put(Keys.CONTRACTOR_TYPE_LIST, contractorTypeList);

		loadLicenseSources();
		viewMap.put(Keys.LICENSE_SOURCE_LIST, licenseSourceList);

		loadBusinessActivityList();
		viewMap.put(Keys.BUSINESS_ACTIVITY_LIST, businessActivityList);

		loadServiceTypeList();
		viewMap.put(Keys.SERVICE_TYPE_LIST, serviceTypeList);

		loadCountry();
		viewMap.put(Keys.COUNTRY_LIST, countryList);

		cityList = new HashMap<String, String>();
		viewMap.put(Keys.CITY_LIST, cityList);

		stateList = new HashMap<String, String>();
		viewMap.put(Keys.STATE_LIST, stateList);
		
		CRStateList = new HashMap<String, String>();
		viewMap.put(Keys.CR_STATE_LIST, CRStateList);
		
		CRCityList = new HashMap<String, String>();
		viewMap.put(Keys.CR_CITY_LIST, CRCityList);
	}

	// loads the contractor type list
	private final void loadContractorStatus() {
		String methodName = "loadContractorStatus()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> contractorStatusDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.Contractor.CONTRACTOR_STATUS);

			if (contractorStatusList == null)
				contractorStatusList = new HashMap<String, String>();

			for (DomainDataView contractorStatus : contractorStatusDDL)
				getContractorStatusList().put(
						getIsEnglishLocale() ? contractorStatus.getDataDescEn()
								: contractorStatus.getDataDescAr(),
						contractorStatus.getDomainDataId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the contractor type list
	private final void loadContractorTypes() {
		String methodName = "loadContractorTypes()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<ContractorTypeView> contractTypeViewList = csa
					.getAllContractorType();

			if (contractorTypeList == null)
				contractorTypeList = new HashMap<String, String>();

			for (ContractorTypeView contractTypeView : contractTypeViewList){
				if(viewMap.get(Keys.CONTRACTOR_TYPE_ID)==null || 
				  (viewMap.get(Keys.CONTRACTOR_TYPE_ID)!=null && contractTypeView.getContractorTypeId().compareTo(new Long(viewMap.get(Keys.CONTRACTOR_TYPE_ID).toString()))==0L))
						getContractorTypeList().put(
								getIsEnglishLocale() ? contractTypeView
										.getDescriptionEn() : contractTypeView
										.getDescriptionAr(),
								contractTypeView.getContractorTypeId().toString());
					this.setContractorType(contractTypeView.getContractorTypeId().toString());
					}

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the license source list
	private final void loadLicenseSources() {
		String methodName = "loadLicenseSources()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> licenseSourceDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.LicenseSource.LICENSE_SOURCE);

			if (licenseSourceList == null)
				licenseSourceList = new HashMap<String, String>();

			for (DomainDataView licenseSource : licenseSourceDDL)
				getLicenseSourceList().put(
						getIsEnglishLocale() ? licenseSource.getDataDescEn()
								: licenseSource.getDataDescAr(),
						licenseSource.getDomainDataId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the business activity list
	private final void loadBusinessActivityList() {
		String methodName = "loadBusinessActivityList()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<BusinessActivityView> businessActivityViewList = csa
					.getAllBusinessActivities();

			if (businessActivityList == null)
				businessActivityList = new HashMap<String, String>();

			for (BusinessActivityView businessActivityView : businessActivityViewList)
				businessActivityList.put(getIsEnglishLocale() ? businessActivityView.getDescriptionEn()
										: businessActivityView.getDescriptionAr(),
								          businessActivityView.getBusinessActivityId().toString());
			
			setBusinessActivityList(businessActivityList);

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the business activity list
	private final void loadServiceTypeList() {
		String methodName = "loadServiceTypeList()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> serviceTypeDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);

			if (serviceTypeList == null)
				serviceTypeList = new HashMap<String, String>();

			for (DomainDataView serviceType : serviceTypeDDL)
				getServiceTypeList().put(
						getIsEnglishLocale() ? serviceType.getDataDescEn()
								: serviceType.getDataDescAr(),
						serviceType.getDomainDataId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the country list
	private final void loadCountry() {
		String methodName = "loadCountry()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<RegionView> regionViewList = psa.getCountry();

			if (countryList == null)
				countryList = new HashMap<String, String>();

			for (RegionView rV : regionViewList)
				getCountryList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads all the states in the list
	public final void loadState() {
		String methodName = "loadState";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			getStateList().clear();
			getCityList().clear();
			 Long selectedCountry = new Long(this.getCountry());
						
			if(selectedCountry != -1)
			{
				Set<RegionView> regionViewList = psa.getState(selectedCountry);
			

			for (RegionView rV : regionViewList)
				this.getStateList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV.getDescriptionAr(),
						rV.getRegionId().toString());

			}
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads all the cities in the list
	public final void loadCity() {
		String methodName = "loadCity";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			Set<RegionView> regionViewList = null;
			getCityList().clear();

			Long selectedState = new Long(this.getState());
			if(selectedState != -1)
			{
				regionViewList = psa.getCity(selectedState);
				
				if (regionViewList != null)
				for (RegionView rV : regionViewList)
					getCityList().put(
							getIsEnglishLocale() ? rV.getDescriptionEn() : rV
									.getDescriptionAr(),
							rV.getRegionId().toString());
			}

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);
		}

	}
	
	
	public void loadContactRefState(ValueChangeEvent event) {
		
 		Long CRcountry = -1L;
 		String methodName = "loadContactRefState";
		logger.logInfo(methodName + "|" + "Start");
 		try {
 			CRcountry = new Long((String) event.getNewValue());
 			getCRStateList().clear();
 			getCRCityList().clear();
	
	
 		if (CRcountry!=-1)
 			{
 			PropertyServiceAgent psa = new PropertyServiceAgent();
 			Set<RegionView> regionViewList = psa.getState(CRcountry);
			

			for (RegionView rV : regionViewList)
				this.getCRStateList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

 			setCrCountryId(CRcountry.toString());	
				
 			}
		
 		this.setCRStateList(CRStateList);
 		viewMap.put(Keys.CR_STATE_LIST, CRStateList);
	}	
 		catch (NumberFormatException e) {
 			logger.LogException("", e);
 			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
 		}
 		
 		catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);
		}
 		
 }	


 	public void loadContactRefCity(ValueChangeEvent event) {
	
 		Long CRstate = -1L;
		String methodName = "loadCRCity";
		logger.logInfo(methodName + "|" + "Start");
 		try {
 			CRstate = new Long((String) event.getNewValue());
 			getCRCityList().clear();
	
 		if (CRstate!=-1)
 			{
 			PropertyServiceAgent psa = new PropertyServiceAgent();
 			Set<RegionView> regionViewList = psa.getCity(CRstate);
 			
 			if (regionViewList != null)
				for (RegionView rV : regionViewList)
					getCRCityList().put(
							getIsEnglishLocale() ? rV.getDescriptionEn() : rV
									.getDescriptionAr(),
							rV.getRegionId().toString());
 			
 			setCrStateId(CRstate.toString());
 			}
	
 		this.setCRCityList(CRCityList);
 		viewMap.put(Keys.CR_CITY_LIST, CRCityList);
 	}
 		catch (NumberFormatException e) {
 			logger.LogException("", e);
 			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
 		}
 		
 		catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);
 		}
 	
 	}	


	

	@SuppressWarnings("unchecked")
	public void addContactInfo() {
		String methodName = "addContactInfo()";
		logger.logInfo(methodName + "|" + "Start");
		ContactInfoView contactInfoView = new ContactInfoView();
		List<ContactInfoView> contactInfoViewList = getContactInfoList();

		DateFormat df = new SimpleDateFormat(getDateFormat());
		try {
			if (isContactInfoValid())
			{
				contactInfoView.setAddress1(this.getAddress1().trim());
				contactInfoView.setAddress2(this.getAddress2().trim());

				if (getCountry() != null && getCountry().length() > 0
						&& !getCountry().equals("-1"))
					contactInfoView.setCountryId(new Long(getCountry()));

				if (getState() != null && getState().length() > 0
						&& !getState().equals("-1"))
					contactInfoView.setStateId(new Long(getState()));

				if (getCity() != null && getCity().length() > 0
						&& !getCity().equals("-1"))
					contactInfoView.setCityId(new Long(getCity()));

				if (getEmail() != null && getEmail().trim().length() > 0)
					contactInfoView.setEmail(getEmail().trim());

				if (getFax() != null && getFax().trim().length() > 0)
					contactInfoView.setFax(getFax());

				if (getHomePhone() != null
						&& getHomePhone().trim().length() > 0)
					contactInfoView.setHomePhone(getHomePhone());

				if (getOfficePhone() != null
						&& getOfficePhone().trim().length() > 0)
					contactInfoView.setOfficePhone(getOfficePhone());

				if (getPostCode() != null && getPostCode().trim().length() > 0)
					contactInfoView.setPostCode(getPostCode());

				if (getStreet() != null && getStreet().trim().length() > 0)
					contactInfoView.setStreet(getStreet());

				contactInfoView.setUpdatedBy(getLoggedInUser());
				contactInfoView.setUpdatedOn(new Date());

				if (getContactInfoCreatedBy() != null
						&& getContactInfoCreatedBy().trim().length() > 0)
					contactInfoView.setCreatedBy(getContactInfoCreatedBy());
				else
					contactInfoView.setCreatedBy(getLoggedInUser());

				if (getContactInfoCreatedOn() != null
						&& getContactInfoCreatedOn().trim().length() > 0)
					contactInfoView.setCreatedOn(df
							.parse(getContactInfoCreatedOn()));
				else
					contactInfoView.setCreatedOn(new Date());

				contactInfoView.setIsDeleted(0L);
				contactInfoView.setRecordStatus(1L);
				int rowId = -1;
				if (viewMap.containsKey(Keys.SELECTED_CONTACT_INFO_ROW_INDEX)
						&& viewMap.get(Keys.SELECTED_CONTACT_INFO_ROW_INDEX) != null) {
					rowId = getContactInfoFromListForRowId(contactInfoViewList,
							new Long(viewMap.get(
									Keys.SELECTED_CONTACT_INFO_ROW_INDEX)
									.toString()), contactInfoView);
					contactInfoView.setRowId(new Long(viewMap.get(
							Keys.SELECTED_CONTACT_INFO_ROW_INDEX).toString()));
					viewMap.remove(Keys.SELECTED_CONTACT_INFO_ROW_INDEX);
				}

				if (rowId >= 0) {
					contactInfoViewList.set(rowId, contactInfoView);
				} else {
					if (viewMap.containsKey(Keys.CONTACT_INFO_ROW_INDEX)
							&& viewMap.get(Keys.CONTACT_INFO_ROW_INDEX) != null)
						contactInfoView.setRowId(new Long(viewMap.get(
								Keys.CONTACT_INFO_ROW_INDEX).toString()) + 1);
					else
						contactInfoView.setRowId(new Long(0));

					viewMap.put(Keys.CONTACT_INFO_ROW_INDEX, contactInfoView
							.getRowId());
					contactInfoViewList.add(contactInfoView);
				}

				viewMap.put(Keys.CONTACT_INFO_LIST, contactInfoViewList);
				contactInfoPaging(contactInfoViewList.size());
				clearContactInfoFields();
			}

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|" + "Error Occured", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public void removeContactInfo() {
		String methodName = "removeContactInfo()";
		logger.logInfo(methodName + "|" + "Start");

		ContactInfoView contactInfoView = (ContactInfoView) contactInfoDataTable
				.getRowData();
		List<ContactInfoView> newContactInfoViewList = new ArrayList<ContactInfoView>();
		List<ContactInfoView> oldContactInfoViewList = new ArrayList<ContactInfoView>();

		if (viewMap.containsKey(Keys.CONTACT_INFO_LIST))
			oldContactInfoViewList = (List<ContactInfoView>) viewMap
					.get(Keys.CONTACT_INFO_LIST);

		for (ContactInfoView contactInfoView2 : oldContactInfoViewList) {
			if (contactInfoView2.getRowId().equals(contactInfoView.getRowId()))
				contactInfoView.setIsDeleted(new Long(1));
			else
				newContactInfoViewList.add(contactInfoView2);
		}

		viewMap.put(Keys.CONTACT_INFO_LIST, newContactInfoViewList);
		contactInfoPaging(newContactInfoViewList.size());
		logger.logInfo(methodName + "|" + "Finish");
	}
	
	public void contactInfoPaging(Integer recordSize)
	{
		this.setContactInfoRecordSize(recordSize);	
		paginatorRows = getPaginatorRows();
		contactInfoPaginatorMaxPages = recordSize / paginatorRows;

		if ((recordSize % paginatorRows) > 0)
			contactInfoPaginatorMaxPages++;

		if (contactInfoPaginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			contactInfoPaginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;


	}

	@SuppressWarnings("unchecked")
	public void editContactInfo() {
		ContactInfoView contactInfoView = (ContactInfoView) contactInfoDataTable
				.getRowData();
		viewMap.put(Keys.SELECTED_CONTACT_INFO_ROW_INDEX, contactInfoView
				.getRowId());

		DateFormat df = new SimpleDateFormat(getDateFormat());

		setContactInfoRowId(contactInfoView.getRowId().toString());
		setAddress1(contactInfoView.getAddress1());
		setAddress2(contactInfoView.getAddress2());

		if (contactInfoView.getCountryId() != null)
			setCountry(contactInfoView.getCountryId().toString());
		if (contactInfoView.getStateId() != null)
			setState(contactInfoView.getStateId().toString());
		if (contactInfoView.getCityId() != null)
			setCity(contactInfoView.getCityId().toString());
		if (contactInfoView.getEmail() != null
				&& contactInfoView.getEmail().length() > 0)
			setEmail(contactInfoView.getEmail());
		if (contactInfoView.getFax() != null
				&& contactInfoView.getFax().length() > 0)
			setFax(contactInfoView.getFax());
		if (contactInfoView.getHomePhone() != null
				&& contactInfoView.getHomePhone().length() > 0)
			setHomePhone(contactInfoView.getHomePhone());
		if (contactInfoView.getOfficePhone() != null
				&& contactInfoView.getOfficePhone().length() > 0)
			setOfficePhone(contactInfoView.getOfficePhone());
		if (contactInfoView.getPostCode() != null
				&& contactInfoView.getPostCode().length() > 0)
			setPostCode(contactInfoView.getPostCode());
		if (contactInfoView.getStreet() != null
				&& contactInfoView.getStreet().length() > 0)
			setStreet(contactInfoView.getStreet());

		setContactInfoCreatedBy(contactInfoView.getCreatedBy());
		setContactInfoCreatedOn(df.format(contactInfoView.getCreatedOn()));
	}

	// validates the contact info tab and checks if there is any error in the
	// form filled by the user
	private boolean isContactInfoValid() {
		boolean hasErrors = false;

		if (getAddress1() == null || getAddress1().trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_ADDRESS_INFO_REQ)));
			hasErrors = true;
		}

		EmailValidator emailValidator = EmailValidator.getInstance();
		if (getEmail() != null && getEmail().trim().length() > 0
				&& !emailValidator.isValid(getEmail())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_INVALID_EMAIL)));
			hasErrors = true;
		}

		if (getEmail() == null || getEmail().trim().length() <= 0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_EMAIL_REQUIRED)));
			hasErrors = true;
		}

		
		return !hasErrors;
	}

	// clears all the values of the UI components in contact info tab
	private final void clearContactInfoFields() {
		setAddress1("");
		setAddress2("");
		setCountry("-1");
		setState("-1");
		setCity("-1");
		setEmail("");
		setFax("");
		setHomePhone("");
		setOfficePhone("");
		setPostCode("");
		setStreet("");
		setContactInfoCreatedBy("");
		setContactInfoCreatedOn("");
		setContactInfoRowId("");
	}

	@SuppressWarnings("unchecked")
	public void addContactReference() {
		String methodName = "addContactReference";
		logger.logInfo(methodName + "|" + "Start");

		ContactReferenceView contactReferenceView = new ContactReferenceView();
		ContactInfoView contactInfoView = new ContactInfoView();
		contactReferenceView.setContactInfoView(contactInfoView);
		List<ContactReferenceView> contactRefViewList = getContactReferencesList();
		DateFormat df = new SimpleDateFormat(getDateFormat());

		try {
			if (isContactReferencesValid()) {
				if (getCrFirstName() != null
						&& getCrFirstName().trim().length() > 0)
					contactReferenceView.setFirstName(getCrFirstName().trim());

				if (getCrMiddleName() != null
						&& getCrMiddleName().trim().length() > 0)
					contactReferenceView
							.setMiddleName(getCrMiddleName().trim());

				if (getCrLastName() != null
						&& getCrLastName().trim().length() > 0)
					contactReferenceView.setLastName(getCrLastName().trim());

				if (getCrTitleId() != null
						&& getCrTitleId().trim().length() > 0)
					contactReferenceView.setTitle((getCrTitleId()
							.trim()));

				contactReferenceView
						.setFullName((getCrFirstName() != null
								&& getCrFirstName().trim().length() > 0 ? getCrFirstName()
								.trim()
								: "")
								+ " "
								+ (getCrMiddleName() != null
										&& getCrMiddleName().trim().length() > 0 ? getCrMiddleName()
										.trim()
										: "")
								+ " "
								+ (getCrLastName() != null
										&& getCrLastName().trim().length() > 0 ? getCrLastName()
										.trim()
										: ""));

				contactReferenceView.getContactInfoView().setAddress1(
						getCrAddress1().trim());
				contactReferenceView.getContactInfoView().setAddress2(
						getCrAddress2().trim());

				if (getCrDesignation() != null
						&& getCrDesignation().trim().length() > 0)
					contactReferenceView.getContactInfoView().setDesignation(
							getCrDesignation());

				if (getCrCountryId() != null && getCrCountryId().length() > 0
						&& !getCrCountryId().equals("-1"))
					contactReferenceView.getContactInfoView().setCountryId(
							new Long(getCrCountryId()));

				if (getCrStateId() != null && getCrStateId().length() > 0
						&& !getCrStateId().equals("-1"))
					contactReferenceView.getContactInfoView().setStateId(
							new Long(getCrStateId()));

				if (getCrCityId() != null && getCrCityId().length() > 0
						&& !getCrCityId().equals("-1"))
					contactReferenceView.getContactInfoView().setCityId(
							new Long(getCrCityId()));

				if (getCrEmail() != null && getCrEmail().trim().length() > 0)
					contactReferenceView.getContactInfoView().setEmail(
							getCrEmail().trim());

				if (getCrFax() != null && getCrFax().trim().length() > 0)
					contactReferenceView.getContactInfoView()
							.setFax(getCrFax());

				if (getCrHomePhone() != null
						&& getCrHomePhone().trim().length() > 0)
					contactReferenceView.getContactInfoView().setHomePhone(
							getCrHomePhone());

				if (getCrOfficePhone() != null
						&& getCrOfficePhone().trim().length() > 0)
					contactReferenceView.getContactInfoView().setOfficePhone(
							getCrOfficePhone());

				if (getCrMobileNumber() != null
						&& getCrMobileNumber().trim().length() > 0)
					contactReferenceView.getContactInfoView().setMobileNumber(
							getCrMobileNumber());

				if (getCrPostCode() != null
						&& getCrPostCode().trim().length() > 0)
					contactReferenceView.getContactInfoView().setPostCode(
							getCrPostCode());

				if (getCrStreet() != null && getCrStreet().trim().length() > 0)
					contactReferenceView.getContactInfoView().setStreet(
							getCrStreet());

				contactReferenceView.getContactInfoView().setUpdatedBy(
						getLoggedInUser());
				contactReferenceView.getContactInfoView().setUpdatedOn(
						new Date());

				if (getCrContactInfoCreatedBy() != null
						&& getCrContactInfoCreatedBy().trim().length() > 0)
					contactReferenceView.getContactInfoView().setCreatedBy(
							getCrContactInfoCreatedBy());
				else
					contactReferenceView.getContactInfoView().setCreatedBy(
							getLoggedInUser());

				if (getCrContactInfoCreatedOn() != null
						&& getCrContactInfoCreatedOn().trim().length() > 0)
					contactReferenceView.getContactInfoView().setCreatedOn(
							df.parse(getCrContactInfoCreatedOn()));
				else
					contactReferenceView.getContactInfoView().setCreatedOn(
							new Date());

				if (getContactRefCreatedBy() != null
						&& getContactRefCreatedBy().trim().length() > 0)
					contactReferenceView.setCreatedBy(getContactRefCreatedBy());
				else
					contactReferenceView.setCreatedBy(getLoggedInUser());

				if (getContactRefCreatedOn() != null
						&& getContactRefCreatedOn().trim().length() > 0)
					contactReferenceView.setCreatedOn(df
							.parse(getContactRefCreatedOn()));
				else
					contactReferenceView.setCreatedOn(new Date());

				contactReferenceView.setUpdatedOn(new Date());
				contactReferenceView.setUpdatedBy(getLoggedInUser());
				contactReferenceView.getContactInfoView().setIsDeleted(0L);
				contactReferenceView.getContactInfoView().setRecordStatus(1L);
				contactReferenceView.setIsDeleted(0L);
				contactReferenceView.setRecordStatus(1L);

				int rowId = -1;
				if (viewMap.containsKey(Keys.SELECTED_CONTACT_REF_ROW_INDEX)
						&& viewMap.get(Keys.SELECTED_CONTACT_REF_ROW_INDEX) != null) {
					rowId = getContactRefFromListForRowId(contactRefViewList,
							new Long(viewMap.get(
									Keys.SELECTED_CONTACT_REF_ROW_INDEX)
									.toString()), contactReferenceView);
					contactReferenceView.setRowId(new Long(viewMap.get(
							Keys.SELECTED_CONTACT_REF_ROW_INDEX).toString()));
					viewMap.remove(Keys.SELECTED_CONTACT_REF_ROW_INDEX);
				}

				if (rowId >= 0) {
					contactRefViewList.set(rowId, contactReferenceView);
				} else {
					if (viewMap.containsKey(Keys.CONTACT_REF_ROW_INDEX)
							&& viewMap.get(Keys.CONTACT_REF_ROW_INDEX) != null)
						contactReferenceView.setRowId(new Long(viewMap.get(
								Keys.CONTACT_REF_ROW_INDEX).toString()) + 1);
					else
						contactReferenceView.setRowId(new Long(0));

					viewMap.put(Keys.CONTACT_REF_ROW_INDEX,
							contactReferenceView.getRowId());
					contactRefViewList.add(contactReferenceView);
				}

				viewMap.put(Keys.CONTACT_REFERENCE_LIST, contactRefViewList);
				contactRefPaging(contactRefViewList.size());
				clearContactReferencesFields();
			}

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|" + "Error Occured", ex);
		}
	}

	// validates the person contacts tab and checks if there is any error in the
	// form filled by the user
	private boolean isContactReferencesValid() {
		boolean hasErrors = false;

		if (getCrTitleId() == null || getCrTitleId().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_TITLE)));
			hasErrors = true;
		}

		if (getCrFirstName() == null || getCrFirstName().trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_FIRST_NAME)));
			hasErrors = true;
		}

		if (getCrLastName() == null || getCrLastName().trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_LAST_NAME)));
			hasErrors = true;
		}

		if (getCrAddress1() == null || getCrAddress1().trim().length() <= 0) {
			errorMessages
					.add(ResourceUtil
							.getInstance()
							.getProperty(
									(MessageConstants.Person.MSG_CONTACT_REF_ADDRESS_INFO_REQ)));
			hasErrors = true;
		}

		EmailValidator emailValidator = EmailValidator.getInstance();
		if (getCrEmail() != null && getCrEmail().trim().length() > 0
				&& !emailValidator.isValid(getCrEmail())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_INVALID_EMAIL)));
			hasErrors = true;
		}

		if (getCrEmail() == null || getCrEmail().trim().length() <= 0
				&& !emailValidator.isValid(getCrEmail())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_EMAIL_REQUIRED)));
			hasErrors = true;
		}

		
		
		return !hasErrors;
	}

	// clears all the values of the UI components in contact references tab
	private final void clearContactReferencesFields() {
		setCrFirstName("");
		setCrMiddleName("");
		setCrLastName("");
		setCrAddress1("");
		setCrAddress2("");
		setCrDesignation("");
		setCrCountryId("-1");
		setCrStateId("-1");
		setCrCityId("-1");
		setCrEmail("");
		setCrFax("");
		setCrHomePhone("");
		setCrOfficePhone("");
		setCrMobileNumber("");
		setCrPostCode("");
		setCrStreet("");
		setCrContactInfoCreatedBy((""));
		setCrContactInfoCreatedOn("");
		setContactRefCreatedBy("");
		setContactRefCreatedOn("");
		setCrTitleId("-1");
	}

	@SuppressWarnings("unchecked")
	public void removeContactReference() {
		String methodName = "removeContactReference()";
		logger.logInfo(methodName + "|" + "Start");

		ContactReferenceView contactRefView = (ContactReferenceView) contactRefDataTable
				.getRowData();
		List<ContactReferenceView> newContactRefViewList = new ArrayList<ContactReferenceView>();
		List<ContactReferenceView> oldContactREFViewList = getContactReferencesList();

		for (ContactReferenceView contactRefView2 : oldContactREFViewList) {
			if (contactRefView2.getRowId().compareTo(contactRefView.getRowId()) == 0)
				contactRefView2.setIsDeleted(new Long(1));
			else
				newContactRefViewList.add(contactRefView2);
		}

		viewMap.put(Keys.CONTACT_REFERENCE_LIST, newContactRefViewList);
		contactRefPaging(newContactRefViewList.size());
		logger.logInfo(methodName + "|" + "Finish");
	}

	
	public void contactRefPaging(Integer recordSize)
	{
		this.setContactRefRecordSize(recordSize);	
		paginatorRows = getPaginatorRows();
		contactRefPaginatorMaxPages = recordSize / paginatorRows;

		if ((recordSize % paginatorRows) > 0)
			contactRefPaginatorMaxPages++;

		if (contactRefPaginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			contactRefPaginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;


	}

	
	@SuppressWarnings("unchecked")
	public void editContactReference() {
		ContactReferenceView contactRefView = (ContactReferenceView) contactRefDataTable
				.getRowData();
		viewMap.put(Keys.SELECTED_CONTACT_REF_ROW_INDEX, contactRefView
				.getRowId());
		DateFormat df = new SimpleDateFormat(getDateFormat());

		this.setContactInfoRowId(contactRefView.getRowId().toString());
		this.setCrAddress1(contactRefView.getContactInfoView().getAddress1());
		this.setCrAddress2(contactRefView.getContactInfoView().getAddress2());

		if (contactRefView.getContactInfoView().getCountryId() != null)
			setCrCountryId(contactRefView.getContactInfoView().getCountryId()
					.toString());
		
		
		this.setCRStateList(populateStateList(contactRefView.getContactInfoView().getCountryId()));
		this.setCRCityList(populateCityList(contactRefView.getContactInfoView().getStateId()));
		
		if (contactRefView.getContactInfoView().getStateId() != null)
			setCrStateId(contactRefView.getContactInfoView().getStateId()
					.toString());

		if (contactRefView.getContactInfoView().getCityId() != null)
			setCrCityId(contactRefView.getContactInfoView().getCityId()
					.toString());

		if (contactRefView.getContactInfoView().getEmail() != null
				&& contactRefView.getContactInfoView().getEmail().length() > 0)
			setCrEmail(contactRefView.getContactInfoView().getEmail());

		if (contactRefView.getContactInfoView().getFax() != null
				&& contactRefView.getContactInfoView().getFax().length() > 0)
			setCrFax(contactRefView.getContactInfoView().getFax());

		if (contactRefView.getContactInfoView().getHomePhone() != null
				&& contactRefView.getContactInfoView().getHomePhone().length() > 0)
			setCrHomePhone(contactRefView.getContactInfoView().getHomePhone());

		if (contactRefView.getContactInfoView().getOfficePhone() != null
				&& contactRefView.getContactInfoView().getOfficePhone()
						.length() > 0)
			setCrOfficePhone(contactRefView.getContactInfoView()
					.getOfficePhone());

		if (contactRefView.getContactInfoView().getMobileNumber() != null
				&& contactRefView.getContactInfoView().getMobileNumber()
						.length() > 0)
			setCrMobileNumber(contactRefView.getContactInfoView()
					.getMobileNumber());

		if (contactRefView.getContactInfoView().getPostCode() != null
				&& contactRefView.getContactInfoView().getPostCode().length() > 0)
			setCrPostCode(contactRefView.getContactInfoView().getPostCode());

		if (contactRefView.getContactInfoView().getStreet() != null
				&& contactRefView.getContactInfoView().getStreet().length() > 0)
			setCrStreet(contactRefView.getContactInfoView().getStreet());

		if (contactRefView.getContactInfoView().getDesignation() != null
				&& contactRefView.getContactInfoView().getDesignation()
						.length() > 0)
			setCrDesignation(contactRefView.getContactInfoView()
					.getDesignation());

		setCrContactInfoCreatedBy(contactRefView.getContactInfoView()
				.getCreatedBy());
		setCrContactInfoCreatedOn(df.format(contactRefView.getContactInfoView()
				.getCreatedOn()));
		setContactRefCreatedBy(contactRefView.getCreatedBy());
		setContactRefCreatedOn(df.format(contactRefView.getCreatedOn()));
		setCrFirstName(contactRefView.getFirstName());
		setCrMiddleName(contactRefView.getMiddleName());
		setCrLastName(contactRefView.getLastName());

		if (contactRefView.getTitleId() != null)
			setCrTitleId(contactRefView.getTitleId().toString());
	}

	
	
	public Map<String, String> populateStateList(Long countryId) {
		
		Map<String, String> states = new HashMap<String, String>();
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			Set<RegionView> regionViewList = pSAgent.getState(countryId);
			for (RegionView regionView : regionViewList) {
				states.put(getIsEnglishLocale() ? regionView.getDescriptionEn() : regionView
								.getDescriptionAr(),
								regionView.getRegionId().toString());
			}
		} catch (Exception e) {
			logger.LogException("", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	
		}
		return states;
	}
	
	
	public Map<String, String> populateCityList(Long stateId) {
		
		Map<String, String> cities = new HashMap<String, String>();
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			Set<RegionView> regionViewList = pSAgent.getCity(stateId);
			for (RegionView regionView : regionViewList) {
				cities.put(getIsEnglishLocale() ? regionView.getDescriptionEn() : regionView
						.getDescriptionAr(),
						regionView.getRegionId().toString());
			}
		} catch (Exception e) {
			logger.LogException("", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return cities;
	}
	
	// saves the contractor with all the values specified by the user
	@SuppressWarnings("unchecked")
	public void saveContractor() {
		String METHOD_NAME = "saveContractor()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			if (!isContractorDetailsValid() || !isContactInfoListValid() || !isPersonContactsValid())
				return;

			ContractorView contractorView = new ContractorView();
			Date date = new Date();
			if (contractorView.getPersonId() == null) {
				contractorView.setCreatedOn(date);
				contractorView.setCreatedBy(getLoggedInUser());
			}
			// TODO: include this when finalized
			if(this.getPersonId() != null)
				contractorView.setPersonId(this.getPersonId());
			contractorView.setGrpNumber(grpNumber);
			contractorView.setStatusId(contractorStatus);
			contractorView.setContractorStatusId(Convert.toLong(contractorStatus));
			contractorView.setContractorNameEn(contractorNameEn);
			contractorView.setContractorNameAr(contractorNameAr);
			contractorView.setCommercialName(commercialName);
			contractorView.setContractorTypeId(contractorType);
			contractorView.setLicenseNumber(licenseNumber);
			contractorView.setLicenseSource(licenseSource);
			contractorView.setLicenseIssueDate(issueDate);
			contractorView.setLicenseExpiryDate(expiryDate);
			contractorView.setBusinessActivities(businessActivities);
			contractorView.setServiceTypes(serviceTypes);
			contractorView.setContactInfos(getContactInfoList());
			if(viewMap.get("loginId") != null)
				contractorView.setLoginId((String)viewMap.get("loginId"));
			
			Set<ContactInfoView> contactInfoViewSet = new HashSet<ContactInfoView>(0);
			contactInfoViewSet.addAll(getContactInfoList());
			contractorView.setContactInfoViewSet(contactInfoViewSet);

			contractorView.setUpdatedBy(getLoggedInUser());
			contractorView.setUpdatedOn(date);

			Set<ContactReferenceView> contactReferenceViewSet = new HashSet<ContactReferenceView>();
			for (ContactReferenceView contactReferenceView : getContactReferencesList())
				contactReferenceViewSet.add(contactReferenceView);

			contractorView.setContactReferences(contactReferenceViewSet);

			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			if(contractorView.getPersonId()!=null)
				setAdded(false);
			else
				setAdded(true);
			contractorView = csa.addContractor(contractorView, businessActivities, serviceTypes);
			this.setPersonId(contractorView.getPersonId());
			viewMap.put("savedContractor", contractorView);
			
			if(contractorView.getPersonId()!=null)
			{
				CommonUtil.loadAttachmentsAndComments(contractorView.getPersonId().toString());
				CommonUtil.saveAttachments(contractorView.getPersonId());
				CommonUtil.saveComments(contractorView.getPersonId(), noteOwner);
			}
			
			
			if(viewMap.containsKey("loginId"))
			{
				this.infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_ID_GENERATED));
				viewMap.remove("loginId");
			}
			else if(isAdded())
				this.infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Contractor.MSG_ADDED_SUCESS));
			else
				this.infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Contractor.MSG_UPDATED_SUCESS));
			//clearFields();
			
			logger.logInfo(METHOD_NAME + " method completed Succesfully");
		} catch (Exception e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
	}

	// validates the contactor details tab and checks if there is any error in
	// the
	// form filled by the user
	private boolean isContractorDetailsValid() {
		boolean isValid = true;

		if (getContractorStatus() == null
				|| getContractorStatus().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_STATUS_REQ)));
			isValid = false;
		}

		if (getContractorNameEn() == null
				|| getContractorNameEn().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_NAME_EN_REQ)));
			isValid = false;
		}

		if (getContractorNameAr() == null
				|| getContractorNameAr().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_NAME_AR_REQ)));
			isValid = false;
		}

		if (getCommercialName() == null
				|| getCommercialName().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_COMMERCIAL_NAME_REQ)));
			isValid = false;
		}
		
		
		if (getContractorType() == null
				|| getContractorType().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_TYPE_REQ)));
			isValid = false;
		}

		if (getLicenseNumber() == null
				|| getLicenseNumber().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_LICENSE_NUMBER_REQ)));
			isValid = false;
		}

		if (getLicenseSource() == null
				|| getLicenseSource().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_LICENSE_SOURCE_REQ)));
			isValid = false;
		}

//		if (getIssueDate() == null ) {
//			errorMessages.add(ResourceUtil.getInstance().getProperty(
//					(MessageConstants.Contractor.MSG_ISSUE_DATE_REQ)));
//			isValid = false;
//		}
//
//		if (getExpiryDate() == null ) {
//			errorMessages.add(ResourceUtil.getInstance().getProperty(
//					(MessageConstants.Contractor.MSG_EXPIRY_DATE_REQ)));
//			isValid = false;
//		}

	/*	if (getBusinessActivities() == null
				|| getBusinessActivities().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_BUSINESS_ACTIVITY_REQ)));
			isValid = false;
		}

		if (getServiceTypes() == null || getServiceTypes().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_SERVICE_TYPE_REQ)));
			isValid = false;
		}*/

		return isValid;
	}

	// validates the address details tab, checks if the user has added atleast one record or not
	private boolean isContactInfoListValid() {
		boolean isValid = true;

		if (getContactInfoList() == null
				|| getContactInfoList().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_ADDRESS_DETAILS_REQ)));
			isValid = false;
		}

		return isValid;
	}

	// validates the address details tab, checks if the user has added atleast one record or not
	private boolean isPersonContactsValid() {
		boolean isValid = true;

		if (getContactReferencesList() == null
				|| getContactReferencesList().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTACT_REFERENCES_REQ)));
			isValid = false;
		}

		return isValid;
	}
	
	
	private void clearFields()
	{
		setGrpNumber("");
		setContractorStatus("-1");
		setContractorNameEn("");
		setContractorNameAr("");
		setCommercialName("");
		setContractorType("");
		setLicenseNumber("");
		setLicenseSource("-1");
		setIssueDate(null);
		setExpiryDate(null);
		List<String> emptyList = new ArrayList<String>();
		setBusinessActivities(emptyList);
		setServiceTypes(emptyList);
		getContactInfoList().clear();
		getContactReferencesList().clear();
		setContactRefRecordSize(0);
		setContactInfoRecordSize(0);
		
		clearContactInfoFields();
		clearContactReferencesFields();
		List<DocumentView> clearAttachments = new ArrayList<DocumentView>();
		viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS,clearAttachments);
		viewMap.put(WebConstants.Attachment.OTHER_DOCS,clearAttachments);
	}

	// Accessors starts from here
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public String getGrpNumber() {
		if(viewMap.containsKey("groupNumber") && viewMap.get("groupNumber") != null)
			grpNumber = (String)viewMap.get("groupNumber");
			
			return grpNumber;
	}

	public void setGrpNumber(String grpNumber) {
		this.grpNumber = grpNumber;
		
		if (this.grpNumber != null)
			viewMap.put("groupNumber", this.grpNumber);
	}

	// public String getContractorNumber() {
	// return contractorNumber;
	// }
	//
	// public void setContractorNumber(String contractorNumber) {
	// this.contractorNumber = contractorNumber;
	// }

	public String getCommercialName() {
		if (viewMap.get(Keys.CONTRACTOR_COMMERCIAL_NAME) != null)
			commercialName = (String) viewMap
					.get(Keys.CONTRACTOR_COMMERCIAL_NAME);

		return commercialName;
	}

	@SuppressWarnings("unchecked")
	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;

		if (this.commercialName != null)
			viewMap.put(Keys.CONTRACTOR_COMMERCIAL_NAME, this.commercialName);
	}

	public String getContractorNameAr() {
		if (viewMap.get(Keys.CONTRACTOR_NAME_AR) != null)
			contractorNameAr = (String) viewMap.get(Keys.CONTRACTOR_NAME_AR);

		return contractorNameAr;
	}

	@SuppressWarnings("unchecked")
	public void setContractorNameAr(String contractorNameAr) {
		this.contractorNameAr = contractorNameAr;

		if (this.contractorNameAr != null)
			viewMap.put(Keys.CONTRACTOR_NAME_AR, this.contractorNameAr);
	}

	public String getContractorNameEn() {
		if (viewMap.get(Keys.CONTRACTOR_NAME_EN) != null)
			contractorNameEn = (String) viewMap.get(Keys.CONTRACTOR_NAME_EN);

		return contractorNameEn;
	}

	@SuppressWarnings("unchecked")
	public void setContractorNameEn(String contractorNameEn) {
		this.contractorNameEn = contractorNameEn;

		if (this.contractorNameEn != null)
			viewMap.put(Keys.CONTRACTOR_NAME_EN, this.contractorNameEn);
	}

	public String getContractorType() {
		if (viewMap.get(Keys.CONTRACTOR_TYPE) != null)
			contractorType = (String) viewMap.get(Keys.CONTRACTOR_TYPE);

		return contractorType;
	}

	@SuppressWarnings("unchecked")
	public void setContractorType(String contractorType) {
		this.contractorType = contractorType;

		if (this.contractorType != null)
			viewMap.put(Keys.CONTRACTOR_TYPE, this.contractorType);
	}

	public String getLicenseNumber() {
		if (viewMap.get(Keys.CONTRACTOR_LICENSE_NUMBER) != null)
			licenseNumber = (String) viewMap
					.get(Keys.CONTRACTOR_LICENSE_NUMBER);

		return licenseNumber;
	}

	@SuppressWarnings("unchecked")
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;

		if (this.licenseNumber != null)
			viewMap.put(Keys.CONTRACTOR_LICENSE_NUMBER, this.licenseNumber);
	}

	public String getLicenseSource() {
		if (viewMap.get(Keys.CONTRACTOR_LICENSE_SOURCE) != null)
			licenseSource = (String) viewMap
					.get(Keys.CONTRACTOR_LICENSE_SOURCE);

		return licenseSource;
	}

	@SuppressWarnings("unchecked")
	public void setLicenseSource(String licenseSource) {
		this.licenseSource = licenseSource;

		if (this.licenseSource != null)
			viewMap.put(Keys.CONTRACTOR_LICENSE_SOURCE, this.licenseSource);
	}

	// public String getCity() {
	// return city;
	// }
	//
	// public void setCity(String city) {
	// this.city = city;
	// }

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getContractorTypeList() {
		if (viewMap.get(Keys.CONTRACTOR_TYPE_LIST) != null)
			contractorTypeList = (Map<String, String>) viewMap
					.get(Keys.CONTRACTOR_TYPE_LIST);

		return contractorTypeList;
	}

	public void setContractorTypeList(Map<String, String> contractorTypeList) {
		this.contractorTypeList = contractorTypeList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getLicenseSourceList() {
		if (viewMap.get(Keys.LICENSE_SOURCE_LIST) != null)
			licenseSourceList = (Map<String, String>) viewMap
					.get(Keys.LICENSE_SOURCE_LIST);

		return licenseSourceList;
	}

	public void setLicenseSourceList(Map<String, String> licenseSourceList) {
		this.licenseSourceList = licenseSourceList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCRCityList() {
		if (viewMap.get(Keys.CR_CITY_LIST) != null)
			CRCityList = (Map<String, String>) viewMap.get(Keys.CR_CITY_LIST);

		return CRCityList;
	}

	public void setCRCityList(Map<String, String> CRCityList) {
		this.CRCityList = CRCityList;
		if(this.CRCityList != null)
			viewMap.put(Keys.CR_CITY_LIST, this.CRCityList);
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getCityList() {
		if (viewMap.get(Keys.CITY_LIST) != null)
			cityList = (Map<String, String>) viewMap.get(Keys.CITY_LIST);

		return cityList;
	}

	public void setCityList(Map<String, String> cityList) {
		this.cityList = cityList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getStateList() {
		if (viewMap.get(Keys.STATE_LIST) != null)
			stateList = (Map<String, String>) viewMap.get(Keys.STATE_LIST);

		return stateList;
	}

	public void setStateList(Map<String, String> stateList) {
		this.stateList = stateList;
	}

	
	@SuppressWarnings("unchecked")
	public Map<String, String> getCRStateList() {
		if (viewMap.get(Keys.CR_STATE_LIST) != null)
			CRStateList = (Map<String, String>) viewMap.get(Keys.CR_STATE_LIST);

		return CRStateList;
	}

	public void setCRStateList(Map<String, String> CRStateList) {
		this.CRStateList = CRStateList;
		if(this.CRStateList != null)
			viewMap.put(Keys.CR_STATE_LIST, this.CRStateList);
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getCountryList() {
		if (viewMap.get(Keys.COUNTRY_LIST) != null)
			countryList = (Map<String, String>) viewMap.get(Keys.COUNTRY_LIST);

		return countryList;
	}

	public void setCountryList(Map<String, String> countryList) {
		this.countryList = countryList;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean getIsViewModePopUp() {
		return viewMap.get(Keys.VIEW_MODE) != null
				&& viewMap.get(Keys.VIEW_MODE).toString().trim()
						.equalsIgnoreCase(Keys.POPUP_MODE);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getBusinessActivityList() {
		if (viewMap.get(Keys.BUSINESS_ACTIVITY_LIST) != null)
			businessActivityList = (Map<String, String>) viewMap
					.get(Keys.BUSINESS_ACTIVITY_LIST);

		return businessActivityList;
	}

	public void setBusinessActivityList(Map<String, String> businessActivityList) {
		this.businessActivityList = businessActivityList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getServiceTypeList() {
		if (viewMap.get(Keys.SERVICE_TYPE_LIST) != null)
			serviceTypeList = (Map<String, String>) viewMap
					.get(Keys.SERVICE_TYPE_LIST);

		return serviceTypeList;
	}

	public void setServiceTypeList(Map<String, String> serviceTypeList) {
		this.serviceTypeList = serviceTypeList;
	}

	public Date getIssueDate() {
		if (viewMap.get(Keys.CONTRACTOR_ISSUE_DATE) != null)
			issueDate = (Date) viewMap.get(Keys.CONTRACTOR_ISSUE_DATE);

		return issueDate;
	}

	@SuppressWarnings("unchecked")
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;

		if (this.issueDate != null)
			viewMap.put(Keys.CONTRACTOR_ISSUE_DATE, this.issueDate);
	}

	public Date getExpiryDate() {
		if (viewMap.get(Keys.CONTRACTOR_EXPIRY_DATE) != null)
			expiryDate = (Date) viewMap.get(Keys.CONTRACTOR_EXPIRY_DATE);

		return expiryDate;
	}

	@SuppressWarnings("unchecked")
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;

		if (this.expiryDate != null)
			viewMap.put(Keys.CONTRACTOR_EXPIRY_DATE, this.expiryDate);
	}

	@SuppressWarnings("unchecked")
	public List<String> getBusinessActivities() {
		if (viewMap.get(Keys.CONTRACTOR_BUSINESS_ACTIVITIES) != null)
			businessActivities = (List<String>) viewMap
					.get(Keys.CONTRACTOR_BUSINESS_ACTIVITIES);

		return businessActivities;
	}

	@SuppressWarnings("unchecked")
	public void setBusinessActivities(List<String> businessActivities) {
		this.businessActivities = businessActivities;

		if (this.businessActivities != null)
			viewMap.put(Keys.CONTRACTOR_BUSINESS_ACTIVITIES,
					this.businessActivities);
	}

	@SuppressWarnings("unchecked")
	public List<String> getServiceTypes() {
		if (viewMap.get(Keys.CONTRACTOR_SERVICE_TYPES) != null)
			serviceTypes = (List<String>) viewMap
					.get(Keys.CONTRACTOR_SERVICE_TYPES);

		return serviceTypes;
	}

	@SuppressWarnings("unchecked")
	public void setServiceTypes(List<String> serviceTypes) {
		this.serviceTypes = serviceTypes;

		if (this.serviceTypes != null)
			viewMap.put(Keys.CONTRACTOR_SERVICE_TYPES, this.serviceTypes);
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@SuppressWarnings("unchecked")
	public List<ContactInfoView> getContactInfoList() {
		if (viewMap.get(Keys.CONTACT_INFO_LIST) != null)
			contactInfoList = (List<ContactInfoView>) viewMap
					.get(Keys.CONTACT_INFO_LIST);

		return contactInfoList;
	}

	public void setContactInfoList(List<ContactInfoView> contactInfoList) {
		this.contactInfoList = contactInfoList;
	}

	public String getCrTitleId() {
		return crTitleId;
	}

	public void setCrTitleId(String crTitleId) {
		this.crTitleId = crTitleId;
	}

	public String getCrFirstName() {
		return crFirstName;
	}

	public void setCrFirstName(String crFirstName) {
		this.crFirstName = crFirstName;
	}

	public String getCrMiddleName() {
		return crMiddleName;
	}

	public void setCrMiddleName(String crMiddleName) {
		this.crMiddleName = crMiddleName;
	}

	public String getCrLastName() {
		return crLastName;
	}

	public void setCrLastName(String crLastName) {
		this.crLastName = crLastName;
	}

	public String getCrAddress2() {
		return crAddress2;
	}

	public void setCrAddress2(String crAddress2) {
		this.crAddress2 = crAddress2;
	}

	public String getCrStreet() {
		return crStreet;
	}

	public void setCrStreet(String crStreet) {
		this.crStreet = crStreet;
	}

	public String getCrPostCode() {
		return crPostCode;
	}

	public void setCrPostCode(String crPostCode) {
		this.crPostCode = crPostCode;
	}

	public String getCrCountryId() {
		return crCountryId;
	}

	public void setCrCountryId(String crCountryId) {
		this.crCountryId = crCountryId;
	}

	public String getCrStateId() {
		return crStateId;
	}

	public void setCrStateId(String crStateId) {
		this.crStateId = crStateId;
	}

	public String getCrCityId() {
		return crCityId;
	}

	public void setCrCityId(String crCityId) {
		this.crCityId = crCityId;
	}

	public String getCrHomePhone() {
		return crHomePhone;
	}

	public void setCrHomePhone(String crHomePhone) {
		this.crHomePhone = crHomePhone;
	}

	public String getCrOfficePhone() {
		return crOfficePhone;
	}

	public void setCrOfficePhone(String crOfficePhone) {
		this.crOfficePhone = crOfficePhone;
	}

	public String getCrFax() {
		return crFax;
	}

	public void setCrFax(String crFax) {
		this.crFax = crFax;
	}

	public String getCrEmail() {
		return crEmail;
	}

	public void setCrEmail(String crEmail) {
		this.crEmail = crEmail;
	}

	public String getCrAddress1() {
		return crAddress1;
	}

	public void setCrAddress1(String crAddress1) {
		this.crAddress1 = crAddress1;
	}

	@SuppressWarnings("unchecked")
	public List<ContactReferenceView> getContactReferencesList() {
		if (viewMap.get(Keys.CONTACT_REFERENCE_LIST) != null)
			contactReferencesList = (List<ContactReferenceView>) viewMap
					.get(Keys.CONTACT_REFERENCE_LIST);

		return contactReferencesList;
	}

	public void setContactReferencesList(
			List<ContactReferenceView> contactReferencesList) {
		this.contactReferencesList = contactReferencesList;
	}

	private final String getDateFormat() {
		return SystemParameters.getInstance().getParameter(
				WebConstants.SHORT_DATE_FORMAT);
	}

	private final String getLoggedInUser() {
		String methodName = "getLoggedInUser()";
		logger.logInfo(methodName + "|" + " Start");

		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		String loggedInUser = null;

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		logger.logInfo(methodName + "|" + " Finish");
		return loggedInUser;
	}

	public String getContactInfoCreatedBy() {
		if (viewMap.containsKey(Keys.CONTACT_INFO_CREATED_BY))
			contactInfoCreatedBy = viewMap.get(Keys.CONTACT_INFO_CREATED_BY)
					.toString();

		return contactInfoCreatedBy;
	}

	@SuppressWarnings("unchecked")
	public void setContactInfoCreatedBy(String contactInfoCreatedBy) {
		this.contactInfoCreatedBy = contactInfoCreatedBy;
		if (this.contactInfoCreatedBy != null)
			viewMap
					.put(Keys.CONTACT_INFO_CREATED_BY,
							this.contactInfoCreatedBy);
	}

	public String getContactInfoCreatedOn() {
		if (viewMap.containsKey(Keys.CONTACT_INFO_CREATED_ON))
			contactInfoCreatedOn = viewMap.get(Keys.CONTACT_INFO_CREATED_ON)
					.toString();

		return contactInfoCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void setContactInfoCreatedOn(String contactInfoCreatedOn) {
		this.contactInfoCreatedOn = contactInfoCreatedOn;

		if (this.contactInfoCreatedOn != null)
			viewMap
					.put(Keys.CONTACT_INFO_CREATED_ON,
							this.contactInfoCreatedOn);

	}

	private final int getContactInfoFromListForRowId(
			List<ContactInfoView> CIList, Long rowId, ContactInfoView ciView) {
		for (int i = 0; i < CIList.size(); i++) {
			ContactInfoView contactInfoView = (ContactInfoView) CIList.get(i);
			if (contactInfoView.getRowId().compareTo(rowId) == 0) {
				ciView.setContactInfoId(contactInfoView.getContactInfoId());
				ciView.setPersonContactInfoId(contactInfoView
						.getPersonContactInfoId());
				return i;
			}
		}

		return -1;
	}

	public String getContactInfoRowId() {
		if (viewMap.containsKey(Keys.CONTACT_INFO_ROW_ID))
			contactInfoRowId = viewMap.get(Keys.CONTACT_INFO_ROW_ID).toString();

		return contactInfoRowId;
	}

	@SuppressWarnings("unchecked")
	public void setContactInfoRowId(String contactInfoRowId) {
		this.contactInfoRowId = contactInfoRowId;

		if (this.contactInfoRowId != null)
			viewMap.put(Keys.CONTACT_INFO_ROW_ID, this.contactInfoRowId);
	}

	public HtmlDataTable getContactInfoDataTable() {
		return contactInfoDataTable;
	}

	public void setContactInfoDataTable(HtmlDataTable contactInfoDataTable) {
		this.contactInfoDataTable = contactInfoDataTable;
	}

	public String getCrContactInfoCreatedBy() {
		if (viewMap.containsKey(Keys.CR_CONTACT_INFO_CREATED_BY))
			crContactInfoCreatedBy = viewMap.get(Keys.CONTACT_INFO_CREATED_BY)
					.toString();
		return crContactInfoCreatedBy;
	}

	@SuppressWarnings("unchecked")
	public void setCrContactInfoCreatedBy(String crContactInfoCreatedBy) {
		this.crContactInfoCreatedBy = crContactInfoCreatedBy;
		if (this.crContactInfoCreatedBy != null)
			viewMap.put(Keys.CONTACT_INFO_CREATED_BY,
					this.crContactInfoCreatedBy);
	}

	public String getCrContactInfoCreatedOn() {
		if (viewMap.containsKey(Keys.CR_CONTACT_INFO_CREATED_ON))
			crContactInfoCreatedOn = viewMap.get(
					Keys.CR_CONTACT_INFO_CREATED_ON).toString();
		return crContactInfoCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void setCrContactInfoCreatedOn(String crContactInfoCreatedOn) {
		this.crContactInfoCreatedOn = crContactInfoCreatedOn;
		if (this.crContactInfoCreatedOn != null)
			viewMap.put(Keys.CR_CONTACT_INFO_CREATED_ON,
					this.crContactInfoCreatedOn);
	}

	public String getContactRefCreatedBy() {
		if (viewMap.containsKey(Keys.CONTACT_REFERENCE_CREATED_BY))
			contactRefCreatedBy = viewMap
					.get(Keys.CONTACT_REFERENCE_CREATED_BY).toString();
		return contactRefCreatedBy;
	}

	@SuppressWarnings("unchecked")
	public void setContactRefCreatedBy(String contactRefCreatedBy) {
		this.contactRefCreatedBy = contactRefCreatedBy;
		if (this.contactRefCreatedBy != null)
			viewMap.put(Keys.CONTACT_REFERENCE_CREATED_BY,
					this.contactRefCreatedBy);
	}

	public String getContactRefCreatedOn() {
		if (viewMap.containsKey(Keys.CONTACT_REFERENCE_CREATED_ON))
			contactRefCreatedOn = viewMap
					.get(Keys.CONTACT_REFERENCE_CREATED_ON).toString();
		return contactRefCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void setContactRefCreatedOn(String contactRefCreatedOn) {
		this.contactRefCreatedOn = contactRefCreatedOn;
		if (this.contactRefCreatedOn != null)
			viewMap.put(Keys.CONTACT_REFERENCE_CREATED_ON,
					this.contactRefCreatedOn);
	}

	public int getContactRefFromListForRowId(List<ContactReferenceView> CRList,
			Long rowId, ContactReferenceView crv) {
		for (int i = 0; i < CRList.size(); i++) {
			ContactReferenceView contactRefView = (ContactReferenceView) CRList
					.get(i);
			if (contactRefView.getRowId().compareTo(rowId) == 0) {
				crv.setContactReferenceId(contactRefView
						.getContactReferenceId());
				crv.getContactInfoView().setContactInfoId(
						contactRefView.getContactInfoView().getContactInfoId());
				return i;
			}
		}

		return -1;
	}

	public HtmlDataTable getContactRefDataTable() {
		return contactRefDataTable;
	}

	public void setContactRefDataTable(HtmlDataTable contactRefDataTable) {
		this.contactRefDataTable = contactRefDataTable;
	}

	@SuppressWarnings("unchecked")
	private void canAddAttachmentsAndComments(boolean canAdd) {
		viewMap.put("canAddAttachment", canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public String getCrMobileNumber() {
		return crMobileNumber;
	}

	public void setCrMobileNumber(String crMobileNumber) {
		this.crMobileNumber = crMobileNumber;
	}

	public String getCrDesignation() {
		return crDesignation;
	}

	public void setCrDesignation(String crDesignation) {
		this.crDesignation = crDesignation;
	}

	public String getContractorStatus() {
		if (viewMap.get(Keys.CONTRACTOR_STATUS) != null)
			contractorStatus = (String) viewMap.get(Keys.CONTRACTOR_STATUS);

		return contractorStatus;
	}

	@SuppressWarnings("unchecked")
	public void setContractorStatus(String status) {
		this.contractorStatus = status;

		if (this.contractorStatus != null)
			viewMap.put(Keys.CONTRACTOR_STATUS, this.contractorStatus);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getContractorStatusList() {
		if (viewMap.get(Keys.CONTRACTOR_STATUS_LIST) != null)
			contractorStatusList = (Map<String, String>) viewMap
					.get(Keys.CONTRACTOR_STATUS_LIST);

		return contractorStatusList;
	}

	public void setContractorStatusList(Map<String, String> contractorStatusList) {
		this.contractorStatusList = contractorStatusList;
	}

	public boolean getIsModePopUp(){
		
		boolean isModePopUp = true;
		
		if(!viewMap.containsKey(Keys.MODE_POPUP)){
			isModePopUp = false;
		}
		
		return isModePopUp;
	}
	
	public String btnBack_Click(){
		
		if(this.getIsModePopUp())
			setRequestParam(ContractorSearch.Keys.VIEW_MODE, ContractorSearch.Keys.POPUP_MODE);
		else
			setRequestParam(ContractorSearch.Keys.VIEW_MODE, ContractorSearch.Keys.VIEW_MODE);
			
		setRequestParam(ContractorSearch.Keys.CONTRACTOR_TYPE_ID, viewMap.get(Keys.CONTRACTOR_TYPE_ID));
		
		return "AddContractorScreen";
	}

	public HtmlSelectOneMenu getCmbContractorType() {
		return cmbContractorType;
	}

	public void setCmbContractorType(HtmlSelectOneMenu cmbContractorType) {
		this.cmbContractorType = cmbContractorType;
	}
	
	
	public Integer getContactInfoPaginatorMaxPages() {
		return contactInfoPaginatorMaxPages;
	}

	public void setContactInfoPaginatorMaxPages(Integer paginatorMaxPages) {
		this.contactInfoPaginatorMaxPages = contactInfoPaginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getContactInfoRecordSize() {
		if (viewMap.get(Keys.CONTACT_INFO_RECORD_SIZE) != null)
			contactInfoRecordSize = (Integer) viewMap.get(Keys.CONTACT_INFO_RECORD_SIZE);

		return contactInfoRecordSize == null ? 0 : contactInfoRecordSize;
	}

	public void setContactInfoRecordSize(Integer contactInfoRecordSize) {
		this.contactInfoRecordSize = contactInfoRecordSize;
		if(this.contactInfoRecordSize != null)
			viewMap.put(Keys.CONTACT_INFO_RECORD_SIZE, this.contactInfoRecordSize);
	}

	public Integer getContactRefRecordSize() {
		if (viewMap.get(Keys.CONTACT_REF_RECORD_SIZE) != null)
			contactRefRecordSize = (Integer) viewMap.get(Keys.CONTACT_REF_RECORD_SIZE);

		return contactRefRecordSize == null ? 0 : contactRefRecordSize;
	}

	public void setContactRefRecordSize(Integer contactRefRecordSize) {
		this.contactRefRecordSize = contactRefRecordSize;
		if(this.contactRefRecordSize != null)
			viewMap.put(Keys.CONTACT_REF_RECORD_SIZE, this.contactRefRecordSize);
	}
	
	public Integer getContactRefPaginatorMaxPages() {
		return contactRefPaginatorMaxPages;
	}

	public void setContactRefPaginatorMaxPages(Integer paginatorMaxPages) {
		this.contactRefPaginatorMaxPages = contactRefPaginatorMaxPages;
	}

	public Long getPersonId() {
		if(viewMap.containsKey("personId") && viewMap.get("personId") != null)
			this.personId = (Long)viewMap.get("personId");
		return this.personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
		if(this.personId != null)
			viewMap.put("personId",this.personId);
	}
	
	
	public void generateLoginId() throws PimsBusinessException
	{
		ContractorView contractor = (ContractorView)viewMap.get("savedContractor");
		try
		{
		
		if(contractor.getPersonId() != null)
		{
			UserDbImpl user = new UserDbImpl();
			
			user.setLoginId(contractor.getPersonId().toString());
			user.setFullName(contractor.getCommercialName());
			if(this.getIsEnglishLocale())
				user.setFirstName(contractor.getContractorNameEn());
			else
				user.setFirstName(contractor.getContractorNameAr());
			user.setPolicyId("admin");
			user.setStatusId("0");
			user.setSystemUser(true);	
				
			
			Set<ContactInfoView> tempList = new HashSet<ContactInfoView>();
			tempList = contractor.getContactInfoViewSet();
			String firstEmail = "";
			for(ContactInfoView cInfo : tempList)
			{
				if(cInfo.getEmail() != null)
				{
					firstEmail = cInfo.getEmail();
					break;
				}
				
			}
			
			user.setEmailUrl(firstEmail);
			
			Random randomNumer = new Random();
			Integer pin = randomNumer.nextInt(90000)+10000;
			user.setPassword(pin.toString());
			
			SecurityManager.persistUser(user);
			
			//adding user group
			
			SecUserGrp internetUserGrp = getUserGroup();
			if(internetUserGrp != null)
			{
				deleteGroupBinding(user.getLoginId(), internetUserGrp.getUserGroup().getUserGroupId());
				SecUserUserGroupId complexKey = new SecUserUserGroupId();
				GroupUserBinding groupUserBinding = new GroupUserBinding();
				complexKey.setUserGroupId(internetUserGrp.getUserGroup().getUserGroupId());
				complexKey.setLoginId(user.getLoginId());
				groupUserBinding.setUserGroupBinding(complexKey);
				SecurityManager.persistBinding(groupUserBinding);
				
			}
			
			viewMap.put("loginId", user.getLoginId());
			contractor.setLoginId(user.getLoginId());
			saveContractor();
			
			//makeEntryInOID();
			sendActivationEmail(user,contractor);
		} //if(person.getPersonId() != null)
		}//try
		catch(Exception e)
		{	
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
				
	}
	
	private SecUserGrp getUserGroup()
	{
		Search query = new Search();
		query.clear();
		query.addFrom(UserGroup.class);
		List<UserGroup> allUserGroups =  SecurityManager.findGroup(query);
		List<UserGroup> userGroups = null;
		HashMap<String, UserGroup> userGroupsMap = new HashMap<String, UserGroup>(0);		
		SecUserGrp secUserGroup = new SecUserGrp();
		
		for (UserGroup ug : allUserGroups) 
		{
			if(ug.getUserGroupId().compareTo(WebConstants.UserGroups.INTERNET_USERS_GROUP) == 0)
			{
				
				secUserGroup.setUserGroup(ug);
				return secUserGroup;
				
			}
		}
		return null;
		
	}
	
	private void deleteGroupBinding(String loginId, 
			String userGroupId) {

		GroupUserBinding groupUserBinding = null;
		try{	
			groupUserBinding = EntityManager.getBroker().findById(GroupUserBinding.class, new SecUserUserGroupId(userGroupId, loginId));
			if(groupUserBinding!=null)
				EntityManager.getBroker().delete(groupUserBinding);
		}
		catch(Throwable t){
			t.printStackTrace();
		}
	}
	
	
	private void makeEntryInOID()
	{
		
	}
	
	private void sendActivationEmail(UserDbImpl user, ContractorView contractor)
	{
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		if (user != null)
			contactInfoList.add(new ContactInfo(contractor.getPersonId().toString(), contractor.getCellNumber(), null, user.getEmailUrl()));
		
		UtilityService uService = new UtilityService();
		//String link = ResourceUtil.getInstance().getProperty(MessageConstants.ACTIVATION_LINK);
		String link = uService.getActivationLink();
		String encryptedUID = "?UID=";
		encryptedUID += new Cryptographer().encrypt(contractor.getPersonId().toString());
		link += encryptedUID;
		String type = "&T=";
		link += type;
		link += new Cryptographer().encrypt("contractor");
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		eventAttributesValueMap.put("USER_NAME", user.getFirstName());
		eventAttributesValueMap.put("PIN", user.getPassword());
		eventAttributesValueMap.put("ACTIVATION_LINK", link);
		generateNotification(WebConstants.Notification_MetaEvents.Event_Activation_Email, contactInfoList, eventAttributesValueMap,null);
	}
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		final String METHOD_NAME = "generateNotification()";
		boolean success = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
		
		return success;
	}

	public Boolean getIsGenerateLogin()
	{
		ContractorView contractor = (ContractorView)viewMap.get("savedContractor");
		if(contractor != null)
		{
			if(contractor.getPersonId() != null && contractor.getLoginId() == null)
				return true;
		}
		return false;
		
	}

	public boolean isAdded() {
		return isAdded;
	}

	public void setAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}


	
	
}