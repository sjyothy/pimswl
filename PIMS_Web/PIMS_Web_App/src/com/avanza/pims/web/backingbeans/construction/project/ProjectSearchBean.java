package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant.ProjectFollowUp;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab.ViewRootKeys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.ui.util.ResourceUtil;

public class ProjectSearchBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	public interface Keys {
		public static String PAGE_MODE="PAGE_MODE";
		public static String PAGE_MODE_SELECT_ONE_POPUP  = "MODE_SELECT_ONE_POPUP";
		public static String PAGE_MODE_SELECT_MANY_POPUP  = "MODE_SELECT_MANY_POPUP";
		public static String PAGE_MODE_FULL  = "MODE_FULL";
		public static String PROJECTS_LIST = "PROJECTS_LIST";
		public static String RECORD_SIZE = "RECORD_SIZE";
		public static String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static String CONTRACT_TYPE = "CONTRACT_TYPE";
		public static String ALLOWED_STATUSES = "ALLOWED_STATUSES";
		public static String ALLOWED_TYPES = "ALLOWED_TYPES";
	}

	private final static Logger logger = Logger.getLogger(ProjectSearchBean.class);

	@SuppressWarnings("unchecked")
	private final Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	ProjectServiceAgent projectServiceAgent = new ProjectServiceAgent();

	// binded attributes with search criteria components
	
	// attributes used for a data grid
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;

	// UI component attributes
	private String projectName;
	private String projectNumber;
	private String projectType;
	private String projectStatus;
	private ProjectView projectView =new ProjectView();
	private Map extraSearchCriteriaMap =new HashMap();
	private List<ProjectView> projectDataList = new ArrayList<ProjectView>(0);
	private HtmlDataTable dataTable;
	private HtmlSelectOneMenu cmbProjectType = new HtmlSelectOneMenu();
	// added by hammad.ahmed
	private String studyTypeId;
	private String projectPurposeId;
	private String propertyTypeId;
	private String propertyOwnershipId;
	private String projectSizeId;
	private String projectCategoryId;
	private String landNumber;
	private String projectDescription;
	private String  CONTEXT="context";
	List<SelectItem> projectStatusesList = new ArrayList<SelectItem>(0); 
	List<SelectItem> projectTypeList = new ArrayList<SelectItem>(0);
	
	//
	
	HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	@SuppressWarnings("unchecked")
	public void init() {
		logger.logInfo("Stepped into the init method");

		try {
			if (!isPostBack()) 
			{
				if(request.getParameter(Keys.PAGE_MODE)!=null)
					viewMap.put(Keys.PAGE_MODE, request.getParameter(Keys.PAGE_MODE).toString());
				else
					viewMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_FULL);
				if(request.getParameter(Keys.ALLOWED_TYPES)!=null)
				{
					DomainDataView ddView = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Project.PROJECT_TYPE), request.getParameter(Keys.ALLOWED_TYPES).toString());
							viewMap.put(Keys.ALLOWED_TYPES, ddView );
					
				}
				if(request.getParameter(Keys.ALLOWED_STATUSES)!=null)
					viewMap.put(Keys.ALLOWED_STATUSES, request.getParameter(Keys.ALLOWED_STATUSES));
				loadProjectStatuses();
				viewMap.put(Keys.RECORD_SIZE,0);
				
				
				if (request.getParameter(CONTEXT)!=null )
		    	 {
	    			viewMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
		    	 }
				LoadProjectStatuses();
				LoadProjectType();
			}
			if(viewMap.containsKey(Keys.ALLOWED_TYPES))
			{
				DomainDataView ddView = (DomainDataView)viewMap.get(Keys.ALLOWED_TYPES);
				projectType = ddView.getDomainDataId().toString();
				cmbProjectType.setDisabled(true);
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception e) {
			logger.logError("init crashed due to:" + e.getStackTrace());
		}
	}
	@Override
    public void prerender() 
    {
		if(viewMap.containsKey(Keys.ALLOWED_TYPES))
		{
			DomainDataView ddView = (DomainDataView)viewMap.get(Keys.ALLOWED_TYPES);
			projectType = ddView.getDomainDataId().toString();
			cmbProjectType.setDisabled(true);
		}
		
    }
	public void loadProjectStatuses()
	{
		boolean isAdd=true;
		List<SelectItem> allowedProjectStatuses = new ArrayList<SelectItem>();
		List<DomainDataView> ddvProjectStatuses =  (ArrayList<DomainDataView>)CommonUtil.getDomainDataListForDomainType(WebConstants.Project.PROJECT_STATUS);
		String projectStatusToShow = "";
		if(viewMap.containsKey(Keys.ALLOWED_STATUSES))
			projectStatusToShow = viewMap.get(Keys.ALLOWED_STATUSES).toString();
		
		for (DomainDataView domainDataView : ddvProjectStatuses) {
			 if(projectStatusToShow.length()>0 && !projectStatusToShow.contains(domainDataView.getDataValue()) )
				isAdd = false;
			 if(isAdd)
			 {
				 SelectItem item = null;
				 
				 if(getIsEnglishLocale())
					 item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn());
				 else
					 item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr());
				 
				 allowedProjectStatuses.add(item);
			 }
			  isAdd = true;
			 
		}
		Collections.sort(allowedProjectStatuses,ListComparator.LIST_COMPARE);
		
		viewMap.put("projectStatusesList",allowedProjectStatuses);
	}
	
	public boolean getIsPageModeSelectOnePopUp()
	{
		boolean isPageModeSelectOnePopUp= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_SELECT_ONE_POPUP))
				isPageModeSelectOnePopUp= true;
			else
				isPageModeSelectOnePopUp= false;
		}
		else
			isPageModeSelectOnePopUp =false;
		return isPageModeSelectOnePopUp;
	}
	
	public boolean getIsFullMode()
	{
		boolean isFullMode= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_FULL))
				isFullMode= true;
			else
				isFullMode= false;
		}
		else
			isFullMode = false;
		return isFullMode;
	}
	
	public boolean getIsViewModePopUp()
	{
		if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
			return true;
		else
			return false;
		
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public boolean getIsPageModeSelectManyPopUp()
	{
		boolean  isPageModeSelectManyPopUp= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_SELECT_MANY_POPUP))
				isPageModeSelectManyPopUp= true;
			else
				isPageModeSelectManyPopUp= false;
		}
		else
			isPageModeSelectManyPopUp =false;
		return isPageModeSelectManyPopUp;
	}

	private boolean anySearchCriteriaEntered()
	{
		if(projectName!=null  && projectName.trim().length()>0)
			return true;
		if(projectNumber!=null  && projectNumber.trim().length()>0)
			return true;
		if(projectStatus!=null  && !projectStatus.equals("-1"))
			return true;
		if(projectType!=null  && !projectType.equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(studyTypeId) && !studyTypeId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(projectPurposeId) && !projectPurposeId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(propertyTypeId) && !propertyTypeId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(propertyOwnershipId) && !propertyOwnershipId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(projectSizeId) && !projectSizeId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(projectCategoryId) && !projectCategoryId.equals("0"))
			return true;
		if(StringHelper.isNotEmpty(landNumber))
			return true;
		if(StringHelper.isNotEmpty(projectDescription))
			return true;
		return false;
	}
	private void populateSearchCriteria()
	{
		if(projectName!=null  && projectName.trim().length()>0)
		   projectView.setProjectName(projectName);
		if(projectNumber!=null  && projectNumber.trim().length()>0)
			projectView.setProjectNumber(projectNumber);
		if(projectStatus!=null  && !projectStatus.equals("-1"))
			projectView.setProjectStatusId(new Long(projectStatus));
		else
		{
			ArrayList<Long> statuses = new ArrayList<Long>();
		    List<SelectItem> StatusList =  getProjectStatusesList();
			for(SelectItem item :StatusList)
			{
				Object Stat = item.getValue();
				
				if(Stat != null && Stat.toString().trim().length()>0)
					statuses.add(Long.parseLong(Stat.toString()));					
				
			}
			
			if(statuses.size() >0)
				extraSearchCriteriaMap.put("PROJECT_STATUS_IN",statuses);
	     }
		if(projectType!=null  && !projectType.equals("-1"))
			projectView.setProjectTypeId(new Long(projectType));
		else
		{
			ArrayList<Long> types = new ArrayList<Long>();
		    List<SelectItem> typeList =  getProjectTypeList();
			for(SelectItem item :typeList)
			{
				Object typ = item.getValue();
				
				if(typ != null && typ.toString().trim().length()>0)
					types.add(Long.parseLong(typ.toString()));					
				
			}
			
			if(types.size() >0)
				extraSearchCriteriaMap.put("PROJECT_TYPE_IN",types);
	     }
		
		
		if(StringHelper.isNotEmpty(studyTypeId) && !studyTypeId.equals("0"))
			projectView.setStudyTypeId(Long.valueOf(studyTypeId));
		if(StringHelper.isNotEmpty(projectPurposeId) && !projectPurposeId.equals("0"))
			projectView.setProjectPurposeId(Long.valueOf(projectPurposeId));
		if(StringHelper.isNotEmpty(propertyTypeId) && !propertyTypeId.equals("0"))
			projectView.setPropertyTypeId(Long.valueOf(propertyTypeId));
		if(StringHelper.isNotEmpty(propertyOwnershipId) && !propertyOwnershipId.equals("0"))
			projectView.setPropertyOwnershipId(Long.valueOf(propertyOwnershipId));
		if(StringHelper.isNotEmpty(projectSizeId) && !projectSizeId.equals("0"))
			projectView.setProjectSizeId(Long.valueOf(projectSizeId));
		if(StringHelper.isNotEmpty(projectCategoryId) && !projectCategoryId.equals("0"))
			projectView.setProjectCategoryId(Long.valueOf(projectCategoryId));
		if(StringHelper.isNotEmpty(landNumber))
			projectView.setLandNumber(landNumber);
		if(StringHelper.isNotEmpty(projectDescription))
			projectView.setProjectDescription(projectDescription);
	}
	
	public void btnSearch_Click()
	{
		errorMessages = new ArrayList<String>(0);
		String methodName = "btnSearch_Click";
		ProjectServiceAgent psa  = new ProjectServiceAgent();
		try {
			logger.logInfo(methodName+"|Start");
			if(anySearchCriteriaEntered())
			{
				populateSearchCriteria();
				projectDataList = psa.getProjects(projectView,extraSearchCriteriaMap);
				 viewMap.put(Keys.PROJECTS_LIST, projectDataList );
				if(projectDataList != null)
				 viewMap.put(Keys.RECORD_SIZE,projectDataList.size());
				else
				 viewMap.put(Keys.RECORD_SIZE,0);
				
				if(projectDataList==null || projectDataList.isEmpty())
				{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
				}
			}
			else 
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			logger.logInfo(methodName+"|Finish");
		} catch (Exception e) {
                logger.LogException(methodName+"|Exception Occured", e);
                errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	 private void LoadProjectStatuses()
     {
    	 List<SelectItem> allowedProjectStatuses = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.Project.PROJECT_STATUS);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(getIsContextConstructionContract() && ( !(dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_ON_CONTRACT) 
    					                                  || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_NEW) 
    					                                  || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_UNDER_CONTRACTUAL_PROCESS)
    					                                  || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_SUBMITTED)
    					                                  || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_DESIGN_APPROVED))  ))
    			   isAdd = false;
    			 
    			 if(getIsContextConstructionTender() && ( !(dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_ON_CONTRACT) 
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_NEW) 
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_UNDER_CONTRACTUAL_PROCESS)
                                                            || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_SUBMITTED)
                                                        		   || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_DESIGN_APPROVED))  ))
                   isAdd = false;
    			 
    			 if(getIsContextBOTContract() && ( !(dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_ON_CONTRACT) 
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_SUBMITTED) 
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_UNDER_CONTRACTUAL_PROCESS)
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_SUBMITTED)
                                                           || dd.getDataValue().equals(WebConstants.Project.PROJECT_STATUS_DESIGN_APPROVED))  ))
                   isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedProjectStatuses.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedProjectStatuses!= null && allowedProjectStatuses.size()>1)
			 Collections.sort(allowedProjectStatuses,ListComparator.LIST_COMPARE);
		
    	 viewMap.put("ALLOWED_PROJECT_STATUSES",allowedProjectStatuses);
     }
	
	 
	 private void LoadProjectType()
     {
    	 List<SelectItem> allowedProjectTypes = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.Project.PROJECT_TYPE);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(getIsContextConstructionContract() && ( !(dd.getDataValue().equals(WebConstants.Project.PROJECT_TYPE_CONSTRUCTION)))  )
    				 isAdd = false;
    			 else if(getIsContextBOTContract() && ( !(dd.getDataValue().equals(WebConstants.Project.PROJECT_TYPE_BOT)))  )
    				 isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedProjectTypes.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedProjectTypes!= null && allowedProjectTypes.size()>1)
			 Collections.sort(allowedProjectTypes,ListComparator.LIST_COMPARE);
		
    	 viewMap.put("ALLOWED_PROJECT_TYPE",allowedProjectTypes);
     }
	 
	 public boolean getIsContextConstructionContract()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ProjectSearchContext.CONSTRUCTION_CONTRACT))
			{
				return  true;
			}
			else
				return false;
			
		}
	 
	 public boolean getIsContextConstructionTender()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ProjectSearchContext.CONSTRUCTION_TENDER))
			{
				return  true;
			}
			else
				return false;
			
		}
	 public boolean getIsContextBOTContract()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ProjectSearchContext.BOT_CONTRACT))
			{
				return  true;
			}
			else
				return false;
			
		}
	 
	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);
		if (recordSize == null)
			recordSize = 0;

		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}


	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public List<ProjectView> getProjectDataList() {
		   if(viewMap.get(Keys.PROJECTS_LIST)!=null)
			 projectDataList = (ArrayList<ProjectView>)viewMap.get(Keys.PROJECTS_LIST);
		return projectDataList;
	}

	public void setProjectDataList(List<ProjectView> projectDataList) {
		this.projectDataList = projectDataList;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}
	public String getInfoMessage() {
		return infoMessage;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getStudyTypeId() {
		return studyTypeId;
	}
	public void setStudyTypeId(String studyTypeId) {
		this.studyTypeId = studyTypeId;
	}
	public String getProjectPurposeId() {
		return projectPurposeId;
	}
	public void setProjectPurposeId(String projectPurposeId) {
		this.projectPurposeId = projectPurposeId;
	}
	public String getPropertyTypeId() {
		return propertyTypeId;
	}
	public void setPropertyTypeId(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	public String getPropertyOwnershipId() {
		return propertyOwnershipId;
	}
	public void setPropertyOwnershipId(String propertyOwnershipId) {
		this.propertyOwnershipId = propertyOwnershipId;
	}
	public String getProjectSizeId() {
		return projectSizeId;
	}
	public void setProjectSizeId(String projectSizeId) {
		this.projectSizeId = projectSizeId;
	}
	public String getProjectCategoryId() {
		return projectCategoryId;
	}
	public void setProjectCategoryId(String projectCategoryId) {
		this.projectCategoryId = projectCategoryId;
	}
	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	
	// by hammad.ahmed
	
	public String onEdit(){
		String METHOD_NAME = "onEdit()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ProjectView projectView=(ProjectView)dataTable.getRowData();
	   		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_DRAFT);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "ProjectDetails";
	}

	@SuppressWarnings("unchecked")
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			ProjectView projectView=(ProjectView)dataTable.getRowData();
	   		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_VIEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "ProjectDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onDelete(){
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ProjectView projectView=(ProjectView)dataTable.getRowData();
	   		Long projectId = projectView.getProjectId();
	   		Boolean success = projectServiceAgent.deleteProject(projectId, CommonUtil.getLoggedInUser());
	   		if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_DELETE_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   			projectDataList = (List<ProjectView>) viewMap.get(Keys.PROJECTS_LIST);
	   	    	ProjectView temp = projectDataList.remove(index);
	   	    	viewMap.put(Keys.PROJECTS_LIST, projectDataList );
	   	    	viewMap.put(Keys.RECORD_SIZE, projectDataList.size());
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_DELETE_FAILURE));
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_DELETE_FAILURE));
		}	
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String onCancel(){
		String METHOD_NAME = "onCancel()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		ProjectView projectView=(ProjectView)dataTable.getRowData();
	   		Long projectId = projectView.getProjectId();
	   		Boolean success = projectServiceAgent.cancelProject(projectId, CommonUtil.getLoggedInUser());
	   		if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_CANCEL_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   			projectDataList = (List<ProjectView>) viewMap.get(Keys.PROJECTS_LIST);
	   	    	ProjectView temp = projectDataList.remove(index);
	   	    	DomainDataView ddv = new UtilityServiceAgent().getDomainDataByValue(ProjectFollowUp.PROJECT_STATUS, ProjectFollowUp.PROJECT_STATUS_CANCELLED);
	   	    	if(ddv!=null){
	   	    		temp.setProjectStatusId(ddv.getDomainDataId());
	   	    		temp.setProjectStatusEn(ddv.getDataDescEn());
	   	    		temp.setProjectStatusAr(ddv.getDataDescAr());
	   	    		temp.setShowDelete(false);
	   	    		temp.setShowCancel(false);
	   	    		temp.setShowEdit(false);
	   	    	}
	   	    	projectDataList.add(index, temp);
	   	    	viewMap.put(Keys.PROJECTS_LIST, projectDataList );
	   	    	viewMap.put(Keys.RECORD_SIZE, projectDataList.size());
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_CANCEL_FAILURE));
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_CANCEL_FAILURE));
		}	
		return "";
	}
	
	public String onAddProject()
    {
    	logger.logInfo("onAddProject() started...");
    	try{
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_NEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onAddProject() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddProject() crashed ",exception);
    	}
        return "ProjectDetails";
    }
	
	public String onAcceptDesigns()
    {
    	logger.logInfo("onAcceptDesigns() started...");
    	try{
    		ProjectView projectView =(ProjectView)dataTable.getRowData();
    		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_NEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onAcceptDesigns() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAcceptDesigns() crashed ",exception);
    	}
        return "DesignDetails";
    }
	
	
	public String onFollowUp()
    {
    	logger.logInfo("onFollowUp() started...");
    	try{
    		ProjectView projectView =(ProjectView)dataTable.getRowData();
    		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_FOLLOW_UP);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onFollowUp() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onFollowUp() crashed ",exception);
    	}
        return "ProjectDetails";
    }

	public String onHandover()
    {
    	logger.logInfo("onHandover() started...");
    	try{
    		ProjectView projectView =(ProjectView)dataTable.getRowData();
    		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_HANDOVER);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onHandover() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onHandover() crashed ",exception);
    	}
        return "ProjectDetails";
    }
	
	public String onInitialAcceptance()
    {
    	logger.logInfo("onInitialAcceptance() started...");
    	try{
    		ProjectView projectView =(ProjectView)dataTable.getRowData();
    		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_INITIAL_RECEIVING);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onInitialAcceptance() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onInitialAcceptance() crashed ",exception);
    	}
        return "ProjectDetails";
    }
	
	public String onFinalAcceptance()
    {
    	logger.logInfo("onFinalAcceptance() started...");
    	try{
    		ProjectView projectView =(ProjectView)dataTable.getRowData();
    		Long projectId = projectView.getProjectId();
	   		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
    		sessionMap.put(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE, WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_FINAL_RECEIVING);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_PROJECT_SEARCH);
    		logger.logInfo("onFinalAcceptance() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onFinalAcceptance() crashed ",exception);
    	}
        return "ProjectDetails";
    }
	//
	public List<SelectItem> getProjectStatusesList() {
		 List<SelectItem> allowedProjectStatuses = new ArrayList<SelectItem>(); 	 
    	 if(viewMap.containsKey("ALLOWED_PROJECT_STATUSES"))
    		 allowedProjectStatuses = (List<SelectItem>) viewMap.get("ALLOWED_PROJECT_STATUSES");
    	 return allowedProjectStatuses;
	}
	public void setProjectStatusesList(List<SelectItem> projectStatusesList) {
		this.projectStatusesList = projectStatusesList;
	}

	public HtmlSelectOneMenu getCmbProjectType() {
		return cmbProjectType;
	}
	public void setCmbProjectType(HtmlSelectOneMenu cmbProjectType) {
		this.cmbProjectType = cmbProjectType;
	}
	public List<SelectItem> getProjectTypeList() {
		List<SelectItem> allowedProjectType = new ArrayList<SelectItem>(); 	 
   	 if(viewMap.containsKey("ALLOWED_PROJECT_TYPE"))
   		allowedProjectType = (List<SelectItem>) viewMap.get("ALLOWED_PROJECT_TYPE");
		return allowedProjectType;
	}
	public void setProjectTypeList(List<SelectItem> projectTypeList) {
		this.projectTypeList = projectTypeList;
	}
	
}