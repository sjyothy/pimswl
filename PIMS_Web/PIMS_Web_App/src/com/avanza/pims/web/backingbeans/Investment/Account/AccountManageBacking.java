package com.avanza.pims.web.backingbeans.Investment.Account;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AccountServiceAgent;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AccountView;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;

public class AccountManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	private String pageMode;
	private String viewMode;
	private final String noteOwner = WebConstants.Attachment.EXTERNAL_ID_ACCOUNT;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_ACCOUNT;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_ACCOUNT;
	
	private AccountView accountView;
	private AccountServiceAgent accountServiceAgent;
	
		
	public AccountManageBacking() 
	{
		
		logger = Logger.getLogger(AccountManageBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		commonUtil = new CommonUtil();
		pageMode = WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_ADD_ACCOUNT;
		viewMode = new String();
		accountView = new AccountView();
		accountServiceAgent = new AccountServiceAgent();
		
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			if(!isPostBack())
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			
			updateValuesFromMaps();
			showHideAttachmentsAndCommentsBtn();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE          
		if ( sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY) != null )
		{
		 pageMode = (String) sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY);
		 sessionMap.remove( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY ) != null )
		{
		 pageMode = (String) viewMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY);
		}
		                     
		if ( sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY);
		 sessionMap.remove( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY );			
		}
		else if ( viewMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) viewMap.get( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY );
		}
		
		// ACCOUNT VIEW
		if ( sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_VIEW ) != null )
		{
		 accountView = (AccountView) sessionMap.get( WebConstants.ACCOUNT.ACCOUNT_VIEW );
		 sessionMap.remove( WebConstants.ACCOUNT.ACCOUNT_VIEW);
		 
		 CommonUtil.loadAttachmentsAndComments(accountView.getAccountId().toString());
		}
		else if ( viewMap.get( WebConstants.ACCOUNT.ACCOUNT_VIEW )  != null )
		{
		 accountView = (AccountView) viewMap.get( WebConstants.ACCOUNT.ACCOUNT_VIEW ) ;
		}
		
			
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{                 
			viewMap.put( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		if ( viewMode != null )
		{                
			viewMap.put( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY, viewMode );
		}
		
		
		// ACCOUNT VIEW
		if ( accountView != null )
		{
			viewMap.put( WebConstants.ACCOUNT.ACCOUNT_VIEW, accountView );
		}

	
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "accountSearch";
		return path;
	}
	public String onReset() 
	{
		String path = null;		
		path = "accountManage";
		try {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
		response.sendRedirect("accountManage.jsf");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{ if(checkMissingField())
		  {
			    
				isSuccess = saveAccountView(accountView);
				
				
				CommonUtil.loadAttachmentsAndComments(accountView.getAccountId() + "");
				CommonUtil.saveAttachments(accountView.getAccountId());
				CommonUtil.saveComments(accountView.getAccountId(),noteOwner);
				
				
				if(viewMap.containsKey("DUPLICATE_ACCOUNT_NUMBER"))
				{
					updateValuesToMaps();
					errorMessages.add( CommonUtil.getBundleMessage("accountSearch.msg.duplicateNumber") );
					viewMap.remove("DUPLICATE_ACCOUNT_NUMBER");
				}
				if(viewMap.containsKey("DUPLICATE_GRP_ACCOUNT_NUMBER"))
				{
					updateValuesToMaps();
					errorMessages.add( CommonUtil.getBundleMessage("accountSearch.msg.duplicateGRPNumber") );
					viewMap.remove("DUPLICATE_GRP_ACCOUNT_NUMBER");
				}
				
				if(viewMap.containsKey("UPDATE_DONE") && isSuccess)
	            {
	            	updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("accountManage.msg.recordUpdate") );
					viewMap.remove("UPDATE_DONE");
	            }
	            else if (isSuccess)
				{              
	            	pageMode = WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_UPDATE_ACCOUNT; 
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("accountManage.msg.recordSave") );
				}
		 }    
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.error") );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveAccountView( AccountView accountView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		Boolean duplicate  = false;
		
		if ( accountView.getAccountId() == null )
		{
			if(accountServiceAgent.checkDuplicateAccountNumber(accountView, false))
			{
		      viewMap.put("DUPLICATE_ACCOUNT_NUMBER", true);
		      return isSuccess;
			}
			if(accountServiceAgent.checkDuplicateGRPAccountNumber(accountView, false))
			{
		      viewMap.put("DUPLICATE_GRP_ACCOUNT_NUMBER", true);
		      return isSuccess;
			}
			
			accountView.setCreatedBy( CommonUtil.getLoggedInUser() );
			accountView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			
		    isSuccess = accountServiceAgent.addAccount(accountView);
		}
		else
		{
			if(accountServiceAgent.checkDuplicateAccountNumber(accountView, true))
			{
		      viewMap.put("DUPLICATE_ACCOUNT_NUMBER", true);
		      return isSuccess;
			}
			if(accountServiceAgent.checkDuplicateGRPAccountNumber(accountView, true))
			{
		      viewMap.put("DUPLICATE_GRP_ACCOUNT_NUMBER", true);
		      return isSuccess;
			}
			
			accountView.setUpdatedBy( CommonUtil.getLoggedInUser() );			
			isSuccess = accountServiceAgent.updateAccount(accountView);
			viewMap.put("UPDATE_DONE", true);
		}
		
		return isSuccess;
	}
	
	

	public boolean checkMissingField()
	{
		boolean check = true;
		
		if(accountView.getAccountNumber()==null || accountView.getAccountNumber().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.accountNumber") );
			check = false;
		}
		if(accountView.getAccountStatusId()==null || accountView.getAccountStatusId().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.accountStatus") );
			check = false;
		}
		
		if(accountView.getAccountName()==null || accountView.getAccountName().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.accountName") );
			check = false;
		}
		if(accountView.getAccountTypeId()==null || accountView.getAccountTypeId().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.accoutType") );
			check = false;
		}
		if(accountView.getGRPAccountNumber()==null || accountView.getGRPAccountNumber().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("accountManage.msg.GRPAccountNumber") );
			check = false;
		}
		return check;
	}
   

	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
                                          		
		if ( pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_UPDATE_ACCOUNT) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT) )
			isViewMode = true;
		
		return isViewMode;
	}

	public String getIsViewReadOnly() 
	{
		String readOnly = "";
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsEditReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsStatusReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT)
			 || pageMode.equalsIgnoreCase(WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_ADD_ACCOUNT)	)
			readOnly = "READONLY";
		else if(pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_UPDATE_ACCOUNT ))
			readOnly = "";
	
			
		return readOnly;
	}
	
	public void showHideAttachmentsAndCommentsBtn(){
		if ( pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_ADD_ACCOUNT ) )
		    canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY) )
		    canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_UPDATE_ACCOUNT) )
	    	canAddAttachmentsAndComments(true);
	    if ( viewMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT) )
	    	canAddAttachmentsAndComments(false);
	}
	
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(noteOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	private void canAddAttachmentsAndComments(boolean canAdd)
	{
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}


	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}


	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	
	// Shiraz Code Start Here 
	
	public void onAddMember(){
	
	final String viewId = "/studyManage.jsf";

	FacesContext facesContext = FacesContext.getCurrentInstance();

	// This is the proper way to get the view's url
	ViewHandler viewHandler = facesContext.getApplication()
			.getViewHandler();
	String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	String javaScriptText = "javascript:SearchMemberPopup();";

	// Add the Javascript to the rendered page's header for immediate
	// execution
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,
			AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}


	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

	public AccountView getAccountView() {
		return accountView;
	}

	public void setAccountView(AccountView accountView) {
		this.accountView = accountView;
	}

	public AccountServiceAgent getAccountServiceAgent() {
		return accountServiceAgent;
	}

	public void setAccountServiceAgent(AccountServiceAgent accountServiceAgent) {
		this.accountServiceAgent = accountServiceAgent;
	}
	

}

