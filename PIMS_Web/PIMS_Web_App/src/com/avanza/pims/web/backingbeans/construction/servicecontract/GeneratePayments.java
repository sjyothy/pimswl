package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
public class GeneratePayments extends AbstractController {

   public interface Keys {
		
		
		public static final String GRP_ACCOUNT_ID ="grpAccountId";
		public static final String TOTAL_CONTRACT_VALUE="totalValue";
		public static final String CONTRACT_START_DATE="contractStartDate";
		public static final String CONTRACT_END_DATE="contractEndDate";
		public static final String PAYMENT_PERIODS="paymentPeriods";
		public static final String CONTRACT_DURATION="contractDuration";
		public static final String PAYMENT_TYPE="paymentType";
		public static final String OWNER_SHIP_TYPE="ownerShipType";
	}
   CommonUtil commonUtil = new CommonUtil();
	public String amount;
	public Double deposit;
	public String numberOfCashPayments;
	public String numberOfChqPayemts;
	public String totalNumberOfPayments;
	public Date startDate;
	public Date endDate;
	public Date today;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private List<SelectItem> paymentPeriodList = new ArrayList<SelectItem>();
	private String selectOnePaymentPeriod;
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	public String dateformat = "";
	private HtmlDataTable dataTable;
	private ResourceBundle bundle;
	Map sessionMap;
	private ServiceContractAgent csa = new ServiceContractAgent();
	private List<String> errorMessages = new ArrayList();
	private static Logger logger = Logger.getLogger(GeneratePayments.class);
	HttpServletRequest request =
			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	
	
	public LocaleInfo getLocaleInfo()
	{
	    	
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	
	     return localeInfo;
		
	}
	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}	
	
	public String getErrorMessages() {
		
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
				{
					messageList += "<LI>"+ message+ " <br></br>" ;
			    }
			messageList = messageList + "</UL></B></FONT>\n";
		}
		
		return messageList;
	}
    Map viewRootMap ;
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public void init() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		sessionMap =context.getExternalContext().getSessionMap();
		viewRootMap = context.getViewRoot().getAttributes();
		
		if (!isPostBack())
		{
			try {
			
				//Setting the Values from Request
				if(request.getParameter(Keys.GRP_ACCOUNT_ID)!=null)
					viewRootMap.put(Keys.GRP_ACCOUNT_ID, request.getParameter(Keys.GRP_ACCOUNT_ID));
				if(request.getParameter(Keys.TOTAL_CONTRACT_VALUE)!=null)
				{
				    viewRootMap.put(Keys.TOTAL_CONTRACT_VALUE, request.getParameter(Keys.TOTAL_CONTRACT_VALUE));
				    amount = viewRootMap.get(Keys.TOTAL_CONTRACT_VALUE).toString();
				}
				if(request.getParameter(Keys.CONTRACT_START_DATE)!=null)
					viewRootMap.put(Keys.CONTRACT_START_DATE, request.getParameter(Keys.CONTRACT_START_DATE));
				if(request.getParameter(Keys.CONTRACT_END_DATE)!=null)
					viewRootMap.put(Keys.CONTRACT_END_DATE, request.getParameter(Keys.CONTRACT_END_DATE));
				if(request.getParameter(Keys.PAYMENT_TYPE)!=null)
					viewRootMap.put(Keys.PAYMENT_TYPE, request.getParameter(Keys.PAYMENT_TYPE));
				if(request.getParameter(Keys.OWNER_SHIP_TYPE)!=null && request.getParameter(Keys.OWNER_SHIP_TYPE).toString().length()>0)
					viewRootMap.put(Keys.OWNER_SHIP_TYPE, request.getParameter(Keys.OWNER_SHIP_TYPE));
				today = new Date();
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				startDate = df.parse( viewRootMap.get(Keys.CONTRACT_START_DATE).toString());
				endDate   = df.parse( viewRootMap.get(Keys.CONTRACT_END_DATE).toString()); 
				List<DomainDataView> ddvList = commonUtil.getDomainDataListForDomainType(WebConstants.PaymentPeriods.PAYMENT_PERIOD_TYPE);
				viewRootMap.put(Keys.PAYMENT_PERIODS, ddvList );
				
				if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
	    		{
	    			viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
	    			                sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	    			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	    		}
				setContractDuration();
				
			} catch (Exception e) {

				logger.LogException("init|Error Occured", e);
			}
			
		}
		
		getIsEnglishLocale();
	}

	private void setContractDuration() {
		int dateDiffDays=1;
		dateDiffDays=CommonUtil.getDaysBetween(startDate, endDate);
		
		if(dateDiffDays>=365)
			viewRootMap.put(Keys.CONTRACT_DURATION, WebConstants.PaymentPeriods.YEARLY);
		else if(dateDiffDays>=180)
			viewRootMap.put(Keys.CONTRACT_DURATION, WebConstants.PaymentPeriods.HALF_YEARLY);
		else if(dateDiffDays>=90)
			viewRootMap.put(Keys.CONTRACT_DURATION, WebConstants.PaymentPeriods.QUARTERLY);
		else 
			viewRootMap.put(Keys.CONTRACT_DURATION, WebConstants.PaymentPeriods.MONTHLY);
	}
	private String getContractDuration() {
		
			return viewRootMap.get(Keys.CONTRACT_DURATION).toString();
		
	}
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	
	public boolean getIsArabicLocale()
	{
		
		
		isArabicLocale = !getIsEnglishLocale();
		
		
		
		return isArabicLocale;
	}
	
	public String getDateformat() {
		
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		return dateFormat;
	}
	
	public String generatePayments()
	{
		try {
			
			if (!hasErrors())
			{
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Date contractStartDate = df.parse( viewRootMap.get(Keys.CONTRACT_START_DATE).toString());
				Date contractEndDate   = df.parse( viewRootMap.get(Keys.CONTRACT_END_DATE).toString());
				String desc = getBundle().getString(MessageConstants.GeneratePayments.RENT_DESCRIPTION);
				String grpAccountNumberId =viewRootMap.get(Keys.GRP_ACCOUNT_ID).toString();
				Long paymentType =new Long(viewRootMap.get(Keys.PAYMENT_TYPE).toString());
				Long durationofContract = new Long(CommonUtil.getDaysBetween(contractStartDate,contractEndDate));
				String ownerShipType ="";
				if(viewRootMap.containsKey(Keys.OWNER_SHIP_TYPE))
				   ownerShipType = viewRootMap.get(Keys.OWNER_SHIP_TYPE).toString();
				paymentSchedules = csa.generateServiceContractPayments(new Double(viewRootMap.get(Keys.TOTAL_CONTRACT_VALUE).toString()),durationofContract,
						               selectOnePaymentPeriod,paymentType,startDate,desc,grpAccountNumberId,ownerShipType);
				List psList = new ArrayList<PaymentScheduleView>(0);
				for (PaymentScheduleView paymentSchedule : paymentSchedules) {
					paymentSchedule.setCreatedBy(CommonUtil.getLoggedInUser());
					
					psList.add(paymentSchedule);
					viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST, psList);
				}
				
				
			}
			
		}
		 catch (PimsBusinessException e) {

				logger.LogException("generatePayments|Error Occured", e);
			}
		catch (Exception e) {

			logger.LogException("generatePayments|Error Occured", e);
		}
		
		
		return "";
	}
	
	public String sendToContract()
	{
		if (viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) != null)
			paymentSchedules = (ArrayList<PaymentScheduleView>) viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
		
		if (viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) != null)
			paymentSchedules.addAll( (ArrayList<PaymentScheduleView>) viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST));
		
		
		
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		
		if(sessionMap.containsKey("OLD_LIST_OF_PAYMENT"))
		{
		  sessionMap.remove("OLD_LIST_OF_PAYMENT");
		  sessionMap.put("PUT_OLD_LIST_OF_PAYMENT", true);
		}
		
		//Close and Submit ////
		closeAndSubmit();
		///////////////////////
		
		return "";
	}
	
	public String closeAndSubmit()
	{
		String methodName="closeAndSubmit";
        final String viewId = "/GeneratePayments.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =" window.opener.document.forms[0].submit();" +
        					    "window.close();";
        
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	private boolean hasErrors()throws Exception
	{
		String methodName="hasErrors";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean isError=false;
		
		errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date contractStartDate = df.parse( viewRootMap.get(Keys.CONTRACT_START_DATE).toString());
		Date contractEndDate   = df.parse( viewRootMap.get(Keys.CONTRACT_END_DATE).toString());
    	
    	if (startDate == null)
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_DATE));
			isError = true;
    	}
    	else if(startDate.compareTo(contractStartDate)<0)
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_PAYMENTSTARTDATE_LESSTHAN_CONTRACTSTARTDATE));
			isError = true;
    	}
    	else if(startDate.compareTo(contractEndDate)>0)
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_PAYMENTSTARTDATE_GREATERTHAN_CONTRACTENDDATE));
			isError = true;
    	}
    	
    	return isError;
	}
	
	public ResourceBundle getBundle() {
		
		String methodName="getBundle";
    	logger.logInfo(methodName+"|"+" Start");

        if(getIsArabicLocale())

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

        else

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

        logger.logInfo(methodName+"|"+" Finish");
        
        return bundle;

	}
	
	private ArrayList<PaymentScheduleView> dataList ;
	
	public ArrayList<PaymentScheduleView> getDataList() {
		return dataList;
	}
	public void setDataList(ArrayList<PaymentScheduleView> dataList) {
		this.dataList = dataList;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
	public String getNumberOfCashPayments() {
		return numberOfCashPayments;
	}
	public void setNumberOfCashPayments(String numberOfCashPayments) {
		this.numberOfCashPayments = numberOfCashPayments;
	}
	public String getNumberOfChqPayemts() {
		return numberOfChqPayemts;
	}
	public void setNumberOfChqPayemts(String numberOfChqPayemts) {
		this.numberOfChqPayemts = numberOfChqPayemts;
	}
	public String getTotalNumberOfPayments() {
		return totalNumberOfPayments;
	}
	public void setTotalNumberOfPayments(String totalNumberOfPayments) {
		this.totalNumberOfPayments = totalNumberOfPayments;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public Date getToday() {
		return today;
	}
	public void setToday(Date today) {
		this.today = today;
	}
	public List<PaymentScheduleView> getPaymentSchedules() {
		
		if (viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) != null)
			paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
		
		return paymentSchedules;
	}
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	public boolean isArabicLocale() {
		return isArabicLocale;
	}
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	public boolean isEnglishLocale() {
		return isEnglishLocale;
	}
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	
	public TimeZone getTimeZone()

	{
	       return TimeZone.getDefault();
	}
    private boolean chkDuration(String domainDataValue)
    {
    	boolean isCorrectDuration=false;
    	if(getContractDuration().equals(WebConstants.PaymentPeriods.YEARLY))
    		return true;
    	else if(getContractDuration().equals(WebConstants.PaymentPeriods.HALF_YEARLY) && !domainDataValue.equals(WebConstants.PaymentPeriods.YEARLY))
    	{
    	  return true;	
    	}
    	else if(getContractDuration().equals(WebConstants.PaymentPeriods.QUARTERLY) && !domainDataValue.equals(WebConstants.PaymentPeriods.YEARLY) && !domainDataValue.equals(WebConstants.PaymentPeriods.HALF_YEARLY))
    	{
    	  return true;	
    	}
    	else if(getContractDuration().equals(WebConstants.PaymentPeriods.MONTHLY) && domainDataValue.equals(WebConstants.PaymentPeriods.MONTHLY) )
    	{
    	  return true;	
    	}

    	return isCorrectDuration;
    }
	public List<SelectItem> getPaymentPeriodList() {
		
		List<DomainDataView> ddvList = new ArrayList<DomainDataView>();
		SelectItem item = null;
		
		if(viewRootMap.containsKey("paymentPeriodList"))
			paymentPeriodList = (ArrayList<SelectItem>) viewRootMap.get("paymentPeriodList");
		else
		{
			if(viewRootMap.containsKey(Keys.PAYMENT_PERIODS))
			 ddvList = (ArrayList<DomainDataView>) viewRootMap.get(Keys.PAYMENT_PERIODS);
			for (DomainDataView domainDataView : ddvList) {
				if(chkDuration(domainDataView.getDataValue()))
				{
				   if(getIsEnglishLocale())
			         item = new SelectItem(domainDataView.getDataValue(),domainDataView.getDataDescEn());
				   else if(!getIsEnglishLocale())
				     item = new SelectItem(domainDataView.getDataValue(),domainDataView.getDataDescAr());
				   
				   paymentPeriodList.add(item);
				   viewRootMap.put("paymentPeriodList", paymentPeriodList);
				}
			}
		}
		return paymentPeriodList;
	}

	public void setPaymentPeriodList(List<SelectItem> paymentPeriodList) {
		this.paymentPeriodList = paymentPeriodList;
	}

	public String getSelectOnePaymentPeriod() {
		return selectOnePaymentPeriod;
	}

	public void setSelectOnePaymentPeriod(String selectOnePaymentPeriod) {
		this.selectOnePaymentPeriod = selectOnePaymentPeriod;
	}
}
