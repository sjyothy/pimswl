package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.SectorView;
import com.avanza.pims.ws.vo.SubSectorView;

public class SubSectorManageBacking extends AbstractController 
{
	private static final long serialVersionUID = 848255665840078643L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private String pageMode;
	private SubSectorView subSectorView;
	
	private AssetServiceAgent assetServiceAgent;
	
	@SuppressWarnings("unchecked")
	public SubSectorManageBacking() 
	{
		logger = Logger.getLogger( SubSectorManageBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		pageMode = WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_ADD;
		subSectorView = new SubSectorView();
		
		assetServiceAgent = new AssetServiceAgent();
	}
	
	@Override
	public void init() 
	{		
		final String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = ( String ) sessionMap.get( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = ( String ) viewMap.get( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY );
		}
		
		
		// SUB SECTOR VIEW
		if ( sessionMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW ) != null  )
		{
			subSectorView = (SubSectorView) sessionMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW );
			sessionMap.remove( WebConstants.SubSector.SUB_SECTOR_VIEW );
		}
		else if ( viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW ) != null )
		{
			subSectorView = ( SubSectorView ) viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW );
		}
		
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY , pageMode );
		}
		
		
		// SUB SECTOR VIEW
		if ( subSectorView != null )
		{
			viewMap.put( WebConstants.SubSector.SUB_SECTOR_VIEW , subSectorView );
		}
	}
	
	public String onSave()
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{	
			if( validate() )
			{	
				SectorView sectorView = new SectorView();
				sectorView.setSectorId( Long.parseLong( subSectorView.getParentSectorId() ) );				
				sectorView = assetServiceAgent.getSectorList( sectorView ).get( 0 );
				subSectorView.setSectorView( sectorView );
				
				isSuccess = saveSubSector( subSectorView );
				
				if( isSuccess )
				{
					pageMode = WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_EDIT;
					updateValuesToMaps();					
					successMessages.add( CommonUtil.getBundleMessage( "subSector.add.success" ) );
					logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
				}
			}	
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( "subSector.add.error" ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveSubSector(SubSectorView subSectorView) throws PimsBusinessException 
	{	
		Boolean isSuccess = false;
		
		if ( subSectorView.getSubSectorId() == null )
		{
			// Add Sub Sector
			subSectorView.setCreatedBy( CommonUtil.getLoggedInUser() );
			subSectorView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = assetServiceAgent.addSubSector( subSectorView );
		}
		else
		{
			// Update Sub Sector
			subSectorView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = assetServiceAgent.updateSubSector( subSectorView );
		}
		
		return isSuccess;
	}

	private boolean validate() 
	{
		boolean valid = true;
		
		if ( subSectorView.getSectorNameEn() == null || subSectorView.getSectorNameEn().equalsIgnoreCase("") )
		{
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage( "commons.required", CommonUtil.getBundleMessage( "subSector.name.en" ) ) );			
		}
		
		if ( subSectorView.getSectorNameAr() == null || subSectorView.getSectorNameAr().equalsIgnoreCase("") )
		{
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage( "commons.required", CommonUtil.getBundleMessage( "subSector.name.ar" ) ) );			
		}
		
		if ( subSectorView.getDescriptionEn() == null || subSectorView.getDescriptionEn().equalsIgnoreCase("") )
		{
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage( "commons.required", CommonUtil.getBundleMessage( "paymentConfiguration.descriptionEn" ) ) );			
		}
		
		if ( subSectorView.getDescriptionAr() == null || subSectorView.getDescriptionAr().equalsIgnoreCase("") )
		{
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage( "commons.required", CommonUtil.getBundleMessage( "paymentConfiguration.descriptionAr" ) ) );			
		}
		
		if ( subSectorView.getParentSectorId() == null || subSectorView.getParentSectorId().equalsIgnoreCase("") )
		{
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage( "commons.required", CommonUtil.getBundleMessage( "assetSearch.assetSector" ) ) );
		}
		
		return valid;
	}

	public String onCancel()
	{
		return "subSectorSearch";
	}
	
	public Boolean getIsAddMode()
	{
		Boolean isAddMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_ADD ) )
			isAddMode = true;
		
		return isAddMode;
	}
	
	public Boolean getIsEditMode()
	{
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_EDIT ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode()
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_VIEW ) )
			isViewMode = true;
		
		return isViewMode;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public SubSectorView getSubSectorView() {
		return subSectorView;
	}

	public void setSubSectorView(SubSectorView subSectorView) {
		this.subSectorView = subSectorView;
	}

	public AssetServiceAgent getAssetServiceAgent() {
		return assetServiceAgent;
	}

	public void setAssetServiceAgent(AssetServiceAgent assetServiceAgent) {
		this.assetServiceAgent = assetServiceAgent;
	}
}
