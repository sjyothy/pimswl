package com.avanza.pims.web.backingbeans.grp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.GRPServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.DisReqBeneficiary;
import com.avanza.pims.entity.DisbursementDetails;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FinancialTransactionView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;

public class GRPPosting extends AbstractController{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7963518626119821328L;
	//Initialization
	//Commons
	private Boolean isAPSearch ;
	private Boolean isPostedAPSearch;
	private HtmlCommandButton btnAddSupplier = new HtmlCommandButton();;
	private Boolean isPostedARSearch;
	private Boolean markUnMarkAll;
	private HashMap<Long, Long> paymentIdMap=new HashMap<Long, Long>();
	private HashMap<String, String> fTrxToBeStopedMap=new HashMap<String, String>();
	
	GRPServiceAgent grpServiceAgent = new GRPServiceAgent();
	FinancialTransactionView financialTransactionView = new FinancialTransactionView();
	SystemParameters parameters = SystemParameters.getInstance();
	private transient Logger logger = Logger.getLogger(GRPPosting.class);
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil = new CommonUtil();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private String DEFAULT_SORT_FIELD = "paymentTypeId";
	
	private String hdnTenantId;
	private String personTenant;
	private String TENANT_INFO="TENANTINFO";
	private HtmlInputHidden parentHidden = new HtmlInputHidden();
	List<PaymentScheduleView> uniquePendingPayments=new ArrayList<PaymentScheduleView>();
	List<String> fTrxToBeStopedList=new ArrayList<String>();
	//List
	private HtmlDataTable transactionsGrid = new HtmlDataTable();
	private List<FinancialTransactionView> transactionsDataList;
	
	
	//Search Fields
	private HtmlInputText paymentReceiptId = new HtmlInputText();
	private HtmlInputText propertyRefNo = new HtmlInputText();
	private HtmlInputText propertyName = new HtmlInputText();
	private HtmlInputText requestRefNo = new HtmlInputText();
	private HtmlSelectBooleanCheckbox isPosted = new HtmlSelectBooleanCheckbox();
	
	
	private HtmlSelectOneMenu htmlSelectOnePaymentMethods = new HtmlSelectOneMenu();
	private HtmlCalendar htmlCalendarReceiptDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarReceiptDateTo = new HtmlCalendar();
	private HtmlCalendar htmlCalendarContractFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarContractTo = new HtmlCalendar();
	private HtmlCalendar htmlPostingDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlPostingDateTo = new HtmlCalendar();
	private HtmlInputText htmlInputTextUnitNumber = new HtmlInputText();
	private HtmlInputText htmlInputTextTenantName = new HtmlInputText();
	private HtmlInputText htmlInputTextGRPRcptNumber = new HtmlInputText();
	private HtmlInputText htmlInputTextPostedBy = new HtmlInputText();
	private HtmlInputText htmlInputTextBankName = new HtmlInputText();
	private HtmlInputText htmlAuctionNumber = new HtmlInputText();
	private HtmlInputText htmlInputTextchequeNumber = new HtmlInputText();
	private HtmlInputText txtCostCenter	= new HtmlInputText();
	private HtmlInputText htmlInheritanceFileNumber = new HtmlInputText();
	private HtmlInputText htmlInheritanceFilePerson = new HtmlInputText();
	private HtmlInputText contractRefNo = new HtmlInputText();

	private HtmlInputText batchNumber = new HtmlInputText();
	private HtmlInputText GRPAccountNumber = new HtmlInputText();
	//Search Menu
	private HtmlSelectOneMenu paymentTypeMenu = new HtmlSelectOneMenu();
	
	List<SelectItem> paymentTypeList = new ArrayList<SelectItem>();	
	
	private HtmlSelectOneMenu directionTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu ownerShipMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu transactionTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu finTrxMenu = new HtmlSelectOneMenu();




	//Command Buttons
	private HtmlCommandButton searchButton = new HtmlCommandButton();
	private HtmlCommandButton clearButton = new HtmlCommandButton();
	private HtmlCommandButton postButton = new HtmlCommandButton();
	UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	public HtmlInputText getTxtCostCenter() {
		return txtCostCenter;
	}
	public void setTxtCostCenter(HtmlInputText txtCostCenter) {
		this.txtCostCenter = txtCostCenter;
	}
	//Life Cycle Implementation
	@SuppressWarnings("unchecked")
	public void init()
	{
		super.init();
	
		
		try 
		{
			setIsAPSearch(false);
			setIsPostedAPSearch(false);
			setIsPostedARSearch(false);
			if( !isPostBack()  )
			{
				setRowsPerPage(100);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
		        setSortItemListAscending(true);
		        btnAddSupplier.setRendered(false);
				
			}
			
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.GRP.IS_POSTED)&&
					getFacesContext().getExternalContext().getSessionMap().get(WebConstants.GRP.IS_POSTED)!=null){
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.GRP.IS_POSTED);
				transactionsDataList = new ArrayList<FinancialTransactionView>();
				getFacesContext().getViewRoot().getAttributes().put(WebConstants.GRP.TRANSACTIONS_LIST,transactionsDataList);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.GRP.MSG_TRANSACTIONS_POSTED_SUCCESSFULLY));

			}
			fillPaymentType();
		}
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.LogException("GRP POSTING INIT CRASHED", e);
		} 
	}
	@SuppressWarnings("unchecked")
	private void fillPaymentType() throws PimsBusinessException 
	{
		List<PaymentTypeView> listPaymentType = null;
		if(!viewMap.containsKey("LIST_PAYMENT_TYPE") || viewMap.get("LIST_PAYMENT_TYPE")==null)
		{
			listPaymentType= utilityServiceAgent.getAllPaymentTypes();
			viewMap.put("LIST_PAYMENT_TYPE",listPaymentType);
		}
		if(listPaymentType!=null)
		{
			for(PaymentTypeView paymentTypeView : listPaymentType){
				if(isEnglishLocale())
					paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn()));
				else
					paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr()));
			}
			viewMap.put("LIST_PAYMENT_TYPE",paymentTypeList);
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void postData()
	{

		try
		{
				logger.logDebug("postData|Start");
				btnAddSupplier.setRendered(false);
				fTrxToBeStopedMap.clear();
				List<FinancialTransactionView> listOfFinancialTransaction = new ArrayList<FinancialTransactionView>();
				List<FinancialTransactionView> tempListOfFinancialTransaction = new ArrayList<FinancialTransactionView>();
				listOfFinancialTransaction=getTransactionsDataList();
				List<String> stopedTransactionList= new ArrayList<String>();
				for(FinancialTransactionView ftView: listOfFinancialTransaction)
				{
					 if( ftView.getSelected()==null || !ftView.getSelected() ){ continue;}
					 if( isCreditMemoTrx(ftView) ) {continue;};
					 isMEMSAPSupplierPresent(ftView);
					 if( !isAllRowsDisbursedFromMatureRequest(ftView) ){break;}
					 
					 tempListOfFinancialTransaction.add(ftView);
					 if( ftView.getTrxTypeDataValue().equals(WebConstants.TransactionType.TRX_TYPE_GL_INTERFACE_DEBIT ) )
					 {
						HashMap<String,Object> searchCriteria = getSearchCriteria();
						
						searchCriteria.put(Constant.GRP_POSTING_CRITERIA.COLL_TRX_ID,ftView.getCollectionTrxId());
						searchCriteria.put(Constant.GRP_POSTING_CRITERIA.TRX_TYPE,WebConstants.TransactionType.TRX_TYPE_GL_INTERFACE_CREDIT_ID);
						tempListOfFinancialTransaction.addAll( grpServiceAgent.getTransactionsList( searchCriteria,null,null, null, null, true) );
					 }
					
				}
				
				if(viewMap.get("STOPED_TRANSACTION_LIST") != null && ((List<String>)viewMap.get("STOPED_TRANSACTION_LIST")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.get("STOPED_TRANSACTION_LIST");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosteing.TrxCantBrPosted")+stopedTransactionList);
					viewMap.remove("STOPED_TRANSACTION_LIST");
				}
				
				if(viewMap.get("CREDIT_MEMO_TRANS") != null && ((List<String>)viewMap.get("CREDIT_MEMO_TRANS")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.remove("CREDIT_MEMO_TRANS");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add("Credit Memo are temporarily disabled:"+stopedTransactionList);
					
					return;
				}
				if(viewMap.get("SUPPLIER_NOT_PRESENT") != null && ((List<String>)viewMap.get("SUPPLIER_NOT_PRESENT")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.remove("SUPPLIER_NOT_PRESENT");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosteing.TrxSpecifySuppliier")+stopedTransactionList);
					btnAddSupplier.setRendered(true);
					return;
				}
				if( viewMap.get("isAllRowsDisbursedFromMatureRequest" ) != null )
				{
					errorMessages = new ArrayList<String>(0);
					List<String> ftList = (ArrayList<String>)viewMap.remove("isAllRowsDisbursedFromMatureRequest" );
					String reqNum  ="";
					for (String trxNumber: ftList) {
						reqNum += trxNumber+",";
					}
					reqNum = reqNum.substring(0,reqNum.length()-1);
					String msg = java.text.MessageFormat.format(
																ResourceUtil.getInstance().getProperty("grpPosting.msg.disburseAllDisbursements"),
																reqNum
																);
					errorMessages.add( msg );
					return;
				}
				if(tempListOfFinancialTransaction != null && tempListOfFinancialTransaction.size()>0)
				{
					getFacesContext().getExternalContext().getSessionMap().put("LIST_OF_FINANCIAL_TRANSACTION_VIEW", tempListOfFinancialTransaction);
					parentHidden.setValue(true);
					String javaScriptText = "javascript:showPostingVerification();";
					openPopup(javaScriptText);
				}
				logger.logDebug("postData|Finish");
		}
		catch(Exception e)
		{
			logger.LogException( "postData", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void postAllData()
	{

		try
		{
				logger.logDebug("postAllData|Start");
				btnAddSupplier.setRendered(false);
				fTrxToBeStopedMap.clear();
				
				
				HashMap<String,Object> searchCriteria = getSearchCriteria();
				List<FinancialTransactionView> listOfFinancialTransaction = grpServiceAgent.getTransactionsList( searchCriteria,null,null, null, null, true);

				List<FinancialTransactionView> tempListOfFinancialTransaction = new ArrayList<FinancialTransactionView>();
				List<String> stopedTransactionList= new ArrayList<String>();

				
				for(FinancialTransactionView ftView: listOfFinancialTransaction)
				{
					//if not new then dont process it for posting.
					if(ftView.getTransactionStatusId().compareTo(10801L)!= 0)break;
					 if( isCreditMemoTrx(ftView) ) {continue;};
					 isMEMSAPSupplierPresent(ftView);
					 if( !isAllRowsDisbursedFromMatureRequest(ftView) ){break;}
					 tempListOfFinancialTransaction.add(ftView);
					 if( ftView.getTrxTypeDataValue().equals(WebConstants.TransactionType.TRX_TYPE_GL_INTERFACE_DEBIT ) )
					 {
						searchCriteria = getSearchCriteria();
						searchCriteria.put(Constant.GRP_POSTING_CRITERIA.COLL_TRX_ID,ftView.getCollectionTrxId());
						searchCriteria.put(Constant.GRP_POSTING_CRITERIA.TRX_TYPE,WebConstants.TransactionType.TRX_TYPE_GL_INTERFACE_CREDIT_ID);
						tempListOfFinancialTransaction.addAll( grpServiceAgent.getTransactionsList( searchCriteria,null,null, null, null, true) );
					 }
					
				}
				
				if(viewMap.get("STOPED_TRANSACTION_LIST") != null && ((List<String>)viewMap.get("STOPED_TRANSACTION_LIST")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.get("STOPED_TRANSACTION_LIST");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosteing.TrxCantBrPosted")+stopedTransactionList);
					viewMap.remove("STOPED_TRANSACTION_LIST");
				}
				if(viewMap.get("CREDIT_MEMO_TRANS") != null && ((List<String>)viewMap.get("CREDIT_MEMO_TRANS")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.remove("CREDIT_MEMO_TRANS");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add("Credit Memo are temporarily disabled:"+stopedTransactionList);
					
					return;
				}
				if(viewMap.get("SUPPLIER_NOT_PRESENT") != null && ((List<String>)viewMap.get("SUPPLIER_NOT_PRESENT")).size()>0)
				{
					stopedTransactionList=(List<String>) viewMap.remove("SUPPLIER_NOT_PRESENT");
					errorMessages = new ArrayList<String>(0);
					errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosteing.TrxSpecifySuppliier")+stopedTransactionList);
					btnAddSupplier.setRendered(true);
					return;
				}
				if( viewMap.get("isAllRowsDisbursedFromMatureRequest" ) != null )
				{
					errorMessages = new ArrayList<String>(0);
					List<String> ftList = (ArrayList<String>)viewMap.remove("isAllRowsDisbursedFromMatureRequest" );
					String reqNum  ="";
					for (String trxNumber: ftList) {
						reqNum += trxNumber+",";
					}
					reqNum = reqNum.substring(0,reqNum.length()-1);
					String msg = java.text.MessageFormat.format(
																ResourceUtil.getInstance().getProperty("grpPosting.msg.disburseAllDisbursements"),
																reqNum
																);
					errorMessages.add( msg );
					return;
				}
				if(tempListOfFinancialTransaction != null && tempListOfFinancialTransaction.size()>0)
				{
					getFacesContext().getExternalContext().getSessionMap().put("LIST_OF_FINANCIAL_TRANSACTION_VIEW", tempListOfFinancialTransaction);
					parentHidden.setValue(true);
					String javaScriptText = "javascript:showPostingVerification();";
					openPopup(javaScriptText);
				}
				logger.logDebug("postAllData|Finish");
		}
		catch(Exception e)
		{
			logger.LogException( "postAllData", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onAddGRPSuppliers() {
		try
		{
			
				List<Long> ids = new ArrayList<Long>();
				List<FinancialTransactionView> listOfFinancialTransaction=getTransactionsDataList();
				for(FinancialTransactionView ftView: listOfFinancialTransaction)
				{
					if(ftView.getSelected()!=null&&ftView.getSelected())
					{
					 ids.add(ftView.getDisbursementDetailId()  );
					}
				}
				Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
				sessionMap.put( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS,ids); 
				String javaScriptText = " javaScript:openAddGrpSuppliersPopup( );";				
				openPopup(javaScriptText);		
		}
		catch(Exception e)
		{
			logger.LogException( "onAddGRPSuppliers", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" ) 
	private boolean isMEMSAPSupplierPresent(FinancialTransactionView ftView) throws Exception
	{
		logger.logDebug("isMEMSAPSupplierPresent|Start");
		fTrxToBeStopedList = new ArrayList<String>();
		if( ftView.getTrxType().compareTo( getTransactionTypeApInvoice().getDomainDataId() )==0  && ftView.getIsMems() != null && 
			ftView.getIsMems().equals("1") && 
			ftView.getDisbursementReqBenId()!= null  	
		  )
		{
			DisReqBeneficiary reqBen = EntityManager.getBroker().findById( DisReqBeneficiary.class,ftView.getDisbursementReqBenId() );
			if( reqBen.getGrpSupplierName() == null  )
			{
				fTrxToBeStopedList.add(ftView.getTrxNumber());
				
			}
			if ( fTrxToBeStopedList.size() > 0  )
			{
				viewMap.put("SUPPLIER_NOT_PRESENT",fTrxToBeStopedList);
				logger.logDebug("isMEMSAPSupplierPresent|finish");
				return false;
			}
		}
		logger.logDebug("isMEMSAPSupplierPresent|finish");
		return true;
	}
	
	@SuppressWarnings( "unchecked" ) 
	private boolean isCreditMemoTrx(FinancialTransactionView ftView) throws Exception
	{
		logger.logDebug("isCreditMemoTrx|Start");
		fTrxToBeStopedList = new ArrayList<String>();
		if( 
				ftView.getTrxType().compareTo( WebConstants.TransactionType.TRX_TYPE_CREDIT_MEMO_INVOICE_ID )==0  
		  )
		{
			fTrxToBeStopedList.add(ftView.getTrxNumber());
			if ( fTrxToBeStopedList.size() > 0  )
			{
				viewMap.put("CREDIT_MEMO_TRANS",fTrxToBeStopedList);
				logger.logDebug("isCreditMemoTrx|finish");
				return true;
			}
		}
		logger.logDebug("isCreditMemoTrx|finish");
		return false;
	}

	
	@SuppressWarnings( "unchecked" ) 
	private boolean isAllRowsDisbursedFromMatureRequest(FinancialTransactionView ftView) throws Exception
	{
		logger.logDebug("isAllRowsDisbursedFromMatureRequest|Start");
		fTrxToBeStopedList = new ArrayList<String>();
		if( 
				ftView.getRequestType() != null && ftView.getRequestType().compareTo(	32L ) == 0
		  )
		{
			//Only for bank transfer we need to check if all banktransfers related disbursements are disbursed
			if(ftView.getPaymentMethodId().compareTo( Constant.PaymentMethod.CHEQUE_ID ) == 0 )
				return true;
				
			List<DisbursementDetails> undisbursedDisbursements = DisbursementService.getUndisbursedDisbursementDetailsForRequestId( 
																																	ftView.getRequestRefNo(),
																																	ftView.getPaymentMethodId() 
																																  );
			if( undisbursedDisbursements  != null  && undisbursedDisbursements.size()> 0 )
			{
				fTrxToBeStopedList.add(ftView.getTrxNumber());
				
			}
			if ( fTrxToBeStopedList.size() > 0  )
			{
				viewMap.put("isAllRowsDisbursedFromMatureRequest",fTrxToBeStopedList);
				return false;
			}
		}
		logger.logDebug("isAllRowsDisbursedFromMatureRequest|Finish");
		return true;
	}
	@SuppressWarnings( "unchecked" ) 
	private boolean isPendingPaymentsFound(FinancialTransactionView ftView) 
	{
		boolean isFound=false;
		List<PaymentScheduleView> pendingContractPaymnentsList=new ArrayList<PaymentScheduleView>();
		List<PaymentScheduleView> pendingRequestPaymnentsList=new ArrayList<PaymentScheduleView>();
		try
		{ 
			//For trx Type Ap Invoice no need to check pending payments
			if( ftView.getTrxType().compareTo( getTransactionTypeApInvoice().getDomainDataId() )==0 )
			{
				return false;
			}
			else
			{
				pendingContractPaymnentsList=new GRPServiceAgent().getPendingContractPayments(ftView.getContractNumber());
				pendingRequestPaymnentsList=new GRPServiceAgent().getPendingRequestPayments(ftView.getRequestRefNo());
				
				if((pendingContractPaymnentsList != null && pendingContractPaymnentsList.size()>0)||(pendingRequestPaymnentsList != null && pendingRequestPaymnentsList.size()>0))
				{
					if(!fTrxToBeStopedMap.containsKey(ftView.getTrxNumber()))
					{
						fTrxToBeStopedList.add(ftView.getTrxNumber());
						fTrxToBeStopedMap.put(ftView.getTrxNumber(), ftView.getTrxNumber());
						viewMap.put("STOPED_TRANSACTION_LIST",fTrxToBeStopedList);
					}
					isFound=true;
				}
				else
					isFound=false;
			}
		}
		catch (PimsBusinessException e)
		{
			logger.LogException(" isPendingPaymentsFound() crashed |" ,e);
		}
		return isFound;
		
	}
	@SuppressWarnings("unchecked")
	public DomainDataView getTransactionTypeApInvoice()
	{
		DomainDataView ddAPInvoice = null;
		if( !viewMap.containsKey( WebConstants.TransactionType.TRX_TYPE_AP_INVOICE ) )
		{
		ddAPInvoice = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.TransactionType.TRX_TYPE), WebConstants.TransactionType.TRX_TYPE_AP_INVOICE);
		viewMap.put(WebConstants.TransactionType.TRX_TYPE_AP_INVOICE, ddAPInvoice);
		}
		else
			ddAPInvoice = (DomainDataView)viewMap.get( WebConstants.TransactionType.TRX_TYPE_AP_INVOICE ) ; 
		
		return ddAPInvoice;
		
		
	}
	@SuppressWarnings("unchecked")
	public List<FinancialTransactionView> getTransactionsDataList() throws Exception 
	{
		if(viewMap.containsKey(WebConstants.GRP.TRANSACTIONS_LIST))
			transactionsDataList = (List<FinancialTransactionView>) viewMap.get(WebConstants.GRP.TRANSACTIONS_LIST);

		if(transactionsDataList == null)
			transactionsDataList = new ArrayList<FinancialTransactionView>();
		

		return transactionsDataList;
	}
	public void setTransactionsDataList(List<FinancialTransactionView> transactionsDataList) {
		this.transactionsDataList = transactionsDataList;
	}
	
	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();
		
		String emptyValue = "0";		
		
		Object bacthNum = batchNumber.getValue();
		Object paymentRcpt =  paymentReceiptId.getValue();
		Object propertyRefNum = propertyRefNo.getValue();
		Object contractRefNum = contractRefNo.getValue();
		Object requestNum = requestRefNo.getValue();
		Object grpAccountNum = GRPAccountNumber.getValue();
		Object paymentType = paymentTypeMenu.getValue();
		Object ownershipType = ownerShipMenu.getValue();
		Object directionType = directionTypeMenu.getValue();		
		Object paymentmethod = htmlSelectOnePaymentMethods.getValue();
		Object rcptDateFrom = htmlCalendarReceiptDateFrom.getValue();
		Object rcptDateTo = htmlCalendarReceiptDateTo.getValue();
		Object contractDateFrom = htmlCalendarContractFrom.getValue();
		Object contractDateTo = htmlCalendarContractTo.getValue();
		Object postingDateFrom = htmlPostingDateFrom.getValue();
		Object postingDateTo = htmlPostingDateTo.getValue();
		Object unitNumber = htmlInputTextUnitNumber.getValue();
		Object tenantName = htmlInputTextTenantName.getValue();
		Object grpRcptNumber = htmlInputTextGRPRcptNumber.getValue();
		Object postedBy = htmlInputTextPostedBy.getValue();
		Object bankName = htmlInputTextBankName.getValue();	
		Object chequeNumber = htmlInputTextchequeNumber.getValue();
		Object propertyName = this.propertyName.getValue();	
		Object trxtype = this.getTransactionTypeMenu().getValue();
		Object finTrxStatus = this.getFinTrxMenu().getValue();
		Object auctionNumber = htmlAuctionNumber.getValue();
		Object inheritanceFileNumber = htmlInheritanceFileNumber.getValue();
		Object inheritanceFilePerson = htmlInheritanceFilePerson.getValue();
		Object costCenter=txtCostCenter.getValue();
		//searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.IS_INVOICE,0L);
		if(costCenter !=null && costCenter.toString().trim().length()>0)
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.LOCATION, costCenter.toString().trim());
		}
		if(chequeNumber !=null && chequeNumber.toString().trim().length()>0)
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.CHEQUE_NUMBER, chequeNumber.toString().trim());
		}
		if( auctionNumber!=null && auctionNumber.toString().trim().length()>0 )
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.AUCTION_NUM, auctionNumber.toString());
			
		}
		if( inheritanceFileNumber!=null && inheritanceFileNumber.toString().trim().length()>0 )
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.INH_FILE_NO, inheritanceFileNumber.toString());
			
		}
		if( inheritanceFilePerson!=null && inheritanceFilePerson.toString().trim().length()>0 )
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.INH_FILE_PERSON, inheritanceFilePerson.toString());
			
		}
		if(finTrxStatus !=null && finTrxStatus.toString().trim().length()>0 && !finTrxStatus.toString().equals("0"))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.GRP_STATUS, new Long (finTrxStatus.toString()));
			
		}
		if(trxtype !=null && trxtype.toString().trim().length()>0 && !trxtype.toString().equals("0"))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.TRX_TYPE, new Long (trxtype.toString()));
			
		}
		if(bacthNum != null && !bacthNum.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.BATCH_NUMBER, bacthNum.toString());			
		}
		if(paymentRcpt != null && !paymentRcpt.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.PAYMENT_RCPT, paymentRcpt.toString());			
		}
		if(propertyRefNum != null && !propertyRefNum.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.PROPERTY_NUM, propertyRefNum.toString());			
		}
		if(contractRefNum != null && !contractRefNum.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.CONTRACT_NUM, contractRefNum.toString());			
		}
		if(requestNum != null && !requestNum.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.REQUEST_NUM, requestNum.toString());			
		}
		if(grpAccountNum != null && !grpAccountNum.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.GRP_ACC_NUM, grpAccountNum.toString());			
		}
		if(paymentType != null && !paymentType.toString().trim().equals("") && !paymentType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.PAYMENT_TYPE, paymentType.toString());
		}
		if(ownershipType != null && !ownershipType.toString().trim().equals("") && !ownershipType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.OWNERSHIP_TYPE, ownershipType.toString());
		}
		if(directionType != null && !directionType.toString().trim().equals("") && !directionType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.DIRECTION_TYPE, directionType.toString());
		}
		
		
		
		if(paymentmethod != null && !paymentmethod.toString().trim().equals("") && !paymentmethod.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.PAYMENT_METHOD, paymentmethod.toString());
		}
		if(rcptDateFrom != null && !rcptDateFrom.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.RCPT_DATE_FROM, rcptDateFrom);
		}		
		if(rcptDateTo != null && !rcptDateTo.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.RCPT_DATE_TO,rcptDateTo);
		}		
		if(contractDateFrom != null && !contractDateFrom.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.CONTRACT_DATE_FROM, contractDateFrom);
		}		
		if(contractDateTo != null && !contractDateTo.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.CONTRACT_DATE_TO,contractDateTo);
		}		
		if(postingDateFrom != null && !postingDateFrom.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.POSTING_DATE_FROM, postingDateFrom);
		}		
		if(postingDateTo != null && !postingDateTo.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.POSTING_DATE_TO,postingDateTo);
		}				
		if(unitNumber != null && !unitNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.UNIT_NUMBER, unitNumber.toString());			
		}
		if(tenantName != null && !tenantName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.TENANT_NAME, tenantName.toString());			
		}
		if(grpRcptNumber != null && !grpRcptNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.GRP_RCPT_NUMBER, grpRcptNumber.toString());			
		}
		if(postedBy != null && !postedBy.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.POSTED_BY, postedBy.toString());			
		}
		if(bankName != null && !bankName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.BANK_NAME, bankName.toString());			
		}
		if(propertyName != null && !propertyName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.GRP_POSTING_CRITERIA.PROPERTY_NAME, propertyName.toString());			
		}
		
		return searchCriteria;
	}
	public void chkMarkUnMark_Changed()
	{
		
		try
		{
			for (FinancialTransactionView obj : this.getTransactionsDataList()) 
			{
				if( obj.getShowChkBox() != null && obj.getShowChkBox().longValue()==1  )
				   obj.setSelected( this.getMarkUnMarkAll() );
			}
		}
		catch(Exception e)
		{
			logger.LogException( "postData", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void searchTransactions()
	{
	  btnAddSupplier.setRendered(false);
	  setFirstRow( 0 );	
	  doSearchItemList();

	}
	
	
	@SuppressWarnings( "unchecked" )
	public void doSearchItemList()
	{
		
		String methodName = "doSearchItemList|";
		logger.logInfo( methodName +"Start"  );
		errorMessages = new ArrayList<String>();
		List<FinancialTransactionView> tempList = new ArrayList<FinancialTransactionView>( 0 );	
		if(transactionsDataList==null)
			transactionsDataList = new ArrayList<FinancialTransactionView>();
		try 
		{
		    	
			this.setMarkUnMarkAll( false );
			HashMap<String,Object> searchCriteria = getSearchCriteria();
			int totalRows =  grpServiceAgent.searchFinTranGetTotalNumberOfRecords(searchCriteria );
			setTotalRows(totalRows);
			doPagingComputations();
			
			tempList = grpServiceAgent.getTransactionsList( searchCriteria,getRowsPerPage(),
					getCurrentPage(), getSortField(), isSortItemListAscending() , true);
			
			viewMap.put(WebConstants.GRP.TRANSACTIONS_LIST,  tempList);
			recordSize = totalRows;
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			
			
			logger.logInfo( methodName +"Finish"  );	
		} 
		catch ( Exception e ) 
		{
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("searchTransactions", e);
		}
			
		
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	public HtmlDataTable getTransactionsGrid() {
		return transactionsGrid;
	}
	public void setTransactionsGrid(HtmlDataTable transactionsGrid) {
		this.transactionsGrid = transactionsGrid;
	}
	public HtmlInputText getPaymentReceiptId() {
		return paymentReceiptId;
	}
	public void setPaymentReceiptId(HtmlInputText paymentReceiptId) {
		this.paymentReceiptId = paymentReceiptId;
	}
	public HtmlInputText getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(HtmlInputText batchNumber) {
		this.batchNumber = batchNumber;
	}
	public HtmlInputText getGRPAccountNumber() {
		return GRPAccountNumber;
	}
	public void setGRPAccountNumber(HtmlInputText accountNumber) {
		GRPAccountNumber = accountNumber;
	}
	public HtmlSelectOneMenu getPaymentTypeMenu() {
		return paymentTypeMenu;
	}
	public void setPaymentTypeMenu(HtmlSelectOneMenu paymentTypeMenu) {
		this.paymentTypeMenu = paymentTypeMenu;
	}
	public HtmlSelectOneMenu getOwnerShipMenu() {
		return ownerShipMenu;
	}
	public void setOwnerShipMenu(HtmlSelectOneMenu ownerShipMenu) {
		this.ownerShipMenu = ownerShipMenu;
	}
	public HtmlSelectOneMenu getTransactionTypeMenu() {
		return transactionTypeMenu;
	}
	public void setTransactionTypeMenu(HtmlSelectOneMenu transactionTypeMenu) {
		this.transactionTypeMenu = transactionTypeMenu;
	}
	public HtmlCommandButton getSearchButton() {
		return searchButton;
	}
	public void setSearchButton(HtmlCommandButton searchButton) {
		this.searchButton = searchButton;
	}
	public HtmlCommandButton getClearButton() {
		return clearButton;
	}
	public void setClearButton(HtmlCommandButton clearButton) {
		this.clearButton = clearButton;
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}

	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}




	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	public HtmlCommandButton getPostButton() {
		return postButton;
	}
	public void setPostButton(HtmlCommandButton postButton) {
		this.postButton = postButton;
	}
	private void openPopup(String javaScriptText)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentTypeList() {
		if (viewMap.containsKey("LIST_PAYMENT_TYPE")
				&& viewMap.get("LIST_PAYMENT_TYPE") != null)
			paymentTypeList = (ArrayList<SelectItem>)viewMap.get("LIST_PAYMENT_TYPE");
		
		
			return paymentTypeList;
		
		
	}
	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public HtmlInputHidden getParentHidden() {
		return parentHidden;
	}
	public void setParentHidden(HtmlInputHidden parentHidden) {
		this.parentHidden = parentHidden;
	}
	public HtmlInputText getPropertyRefNo() {
		return propertyRefNo;
	}
	public void setPropertyRefNo(HtmlInputText propertyRefNo) {
		this.propertyRefNo = propertyRefNo;
	}
	public HtmlInputText getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(HtmlInputText propertyName) {
		this.propertyName = propertyName;
	}
	public HtmlInputText getRequestRefNo() {
		return requestRefNo;
	}
	public void setRequestRefNo(HtmlInputText requestRefNo) {
		this.requestRefNo = requestRefNo;
	}
	public HtmlInputText getContractRefNo() {
		return contractRefNo;
	}
	public void setContractRefNo(HtmlInputText contractRefNo) {
		this.contractRefNo = contractRefNo;
	}
	public HtmlSelectBooleanCheckbox getIsPosted() {
		return isPosted;
	}
	public void setIsPosted(HtmlSelectBooleanCheckbox isPosted) {
		this.isPosted = isPosted;
	}
	public HtmlSelectOneMenu getDirectionTypeMenu() {
		return directionTypeMenu;
	}
	public void setDirectionTypeMenu(HtmlSelectOneMenu directionTypeMenu) {
		this.directionTypeMenu = directionTypeMenu;
	}
	public Boolean getIsAPSearch() {
		return isAPSearch;
	}
	public void setIsAPSearch(Boolean isAPSearch) {
		this.isAPSearch = isAPSearch;
	}
	public Boolean getIsPostedAPSearch() {
		return isPostedAPSearch;
	}
	public void setIsPostedAPSearch(Boolean isPostedAPSearch) {
		this.isPostedAPSearch = isPostedAPSearch;
	}
	public Boolean getIsPostedARSearch() {
		return isPostedARSearch;
	}
	public void setIsPostedARSearch(Boolean isPostedARSearch) {
		this.isPostedARSearch = isPostedARSearch;
	}
	public HtmlCalendar getHtmlCalendarContractFrom() {
		return htmlCalendarContractFrom;
	}
	public void setHtmlCalendarContractFrom(HtmlCalendar htmlCalendarContractFrom) {
		this.htmlCalendarContractFrom = htmlCalendarContractFrom;
	}
	public HtmlCalendar getHtmlCalendarContractTo() {
		return htmlCalendarContractTo;
	}
	public void setHtmlCalendarContractTo(HtmlCalendar htmlCalendarContractTo) {
		this.htmlCalendarContractTo = htmlCalendarContractTo;
	}
	public HtmlInputText getHtmlInputTextUnitNumber() {
		return htmlInputTextUnitNumber;
	}
	public void setHtmlInputTextUnitNumber(HtmlInputText htmlInputTextUnitNumber) {
		this.htmlInputTextUnitNumber = htmlInputTextUnitNumber;
	}
	public HtmlInputText getHtmlInputTextTenantName() {
		return htmlInputTextTenantName;
	}
	public void setHtmlInputTextTenantName(HtmlInputText htmlInputTextTenantName) {
		this.htmlInputTextTenantName = htmlInputTextTenantName;
	}
	public HtmlCalendar getHtmlPostingDateFrom() {
		return htmlPostingDateFrom;
	}
	public void setHtmlPostingDateFrom(HtmlCalendar htmlPostingDateFrom) {
		this.htmlPostingDateFrom = htmlPostingDateFrom;
	}
	public HtmlCalendar getHtmlPostingDateTo() {
		return htmlPostingDateTo;
	}
	public void setHtmlPostingDateTo(HtmlCalendar htmlPostingDateTo) {
		this.htmlPostingDateTo = htmlPostingDateTo;
	}
	public HtmlInputText getHtmlInputTextGRPRcptNumber() {
		return htmlInputTextGRPRcptNumber;
	}
	public void setHtmlInputTextGRPRcptNumber(
			HtmlInputText htmlInputTextGRPRcptNumber) {
		this.htmlInputTextGRPRcptNumber = htmlInputTextGRPRcptNumber;
	}
	public HtmlInputText getHtmlInputTextPostedBy() {
		return htmlInputTextPostedBy;
	}
	public void setHtmlInputTextPostedBy(HtmlInputText htmlInputTextPostedBy) {
		this.htmlInputTextPostedBy = htmlInputTextPostedBy;
	}
	public HtmlCalendar getHtmlCalendarReceiptDateFrom() {
		return htmlCalendarReceiptDateFrom;
	}
	public void setHtmlCalendarReceiptDateFrom(
			HtmlCalendar htmlCalendarReceiptDateFrom) {
		this.htmlCalendarReceiptDateFrom = htmlCalendarReceiptDateFrom;
	}
	public HtmlCalendar getHtmlCalendarReceiptDateTo() {
		return htmlCalendarReceiptDateTo;
	}
	public void setHtmlCalendarReceiptDateTo(HtmlCalendar htmlCalendarReceiptDateTo) {
		this.htmlCalendarReceiptDateTo = htmlCalendarReceiptDateTo;
	}
	public HtmlSelectOneMenu getHtmlSelectOnePaymentMethods() {
		return htmlSelectOnePaymentMethods;
	}
	public void setHtmlSelectOnePaymentMethods(
			HtmlSelectOneMenu htmlSelectOnePaymentMethods) {
		this.htmlSelectOnePaymentMethods = htmlSelectOnePaymentMethods;
	}
	public HtmlInputText getHtmlInputTextBankName() {
		return htmlInputTextBankName;
	}
	public void setHtmlInputTextBankName(HtmlInputText htmlInputTextBankName) {
		this.htmlInputTextBankName = htmlInputTextBankName;
	}
	public HtmlSelectOneMenu getFinTrxMenu() {
		return finTrxMenu;
	}
	public void setFinTrxMenu(HtmlSelectOneMenu finTrxMenu) {
		this.finTrxMenu = finTrxMenu;
	}
	public Boolean getMarkUnMarkAll() {
		return markUnMarkAll;
	}
	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
	}
	public HtmlInputText getHtmlAuctionNumber() {
		return htmlAuctionNumber;
	}
	public void setHtmlAuctionNumber(HtmlInputText htmlAuctionNumber) {
		this.htmlAuctionNumber = htmlAuctionNumber;
	}
	public List<String> getFTrxToBeStopedList() {
		return fTrxToBeStopedList;
	}
	public void setFTrxToBeStopedList(List<String> trxToBeStopedList) {
		fTrxToBeStopedList = trxToBeStopedList;
	}
	public HashMap<String, String> getFTrxToBeStopedMap() {
		return fTrxToBeStopedMap;
	}
	public void setFTrxToBeStopedMap(HashMap<String, String> trxToBeStopedMap) {
		fTrxToBeStopedMap = trxToBeStopedMap;
	}
	public HtmlInputText getHtmlInputTextchequeNumber() {
		return htmlInputTextchequeNumber;
	}
	public void setHtmlInputTextchequeNumber(HtmlInputText htmlInputTextchequeNumber) {
		this.htmlInputTextchequeNumber = htmlInputTextchequeNumber;
	}
	public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}
	@Override
    public void prerender() 
    {
    	
        String methodName="prerender";
    	try
    	{
    		   super.prerender();
		    	FillTenantInfo();
		    	if(viewMap.containsKey(TENANT_INFO))
			    {
				   PersonView tenantViewRow=(PersonView)viewMap.get(TENANT_INFO);
			       //tenantRefNum=tenantViewRow.getTenantNumber();
			       String tenantNames="";
			        if(tenantViewRow.getPersonFullName()!=null)
			        tenantNames=tenantViewRow.getPersonFullName();
			        if(tenantNames.trim().length()<=0)
			        	 tenantNames=tenantViewRow.getCompanyName();
			        htmlInputTextTenantName.setValue(tenantNames);
			       
			    }
		    	   
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured:",ex);		
    	}
    }
	private void FillTenantInfo() throws PimsBusinessException{

    	String methodName="FillTenantInfo";
    	logger.logInfo(methodName+"| Start");
    	PersonView tenantView;
    	if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(this.getHdnTenantId()));
    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
    		if(tenantsList.size()>0)
    			viewMap.put(TENANT_INFO,tenantsList.get(0));
    			
    	}
    	
    	logger.logInfo(methodName+"| Finish");
		
	}
	public String getHdnTenantId() {
		if(viewMap.containsKey("hdnTenantId") && viewMap.get("hdnTenantId")!=null)
			hdnTenantId = viewMap.get("hdnTenantId").toString();
		return hdnTenantId;
	}
	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
		if(this.hdnTenantId!=null)
			viewMap.put("hdnTenantId", this.hdnTenantId);
	}
	public HtmlInputText getHtmlInheritanceFileNumber() {
		return htmlInheritanceFileNumber;
	}
	public void setHtmlInheritanceFileNumber(HtmlInputText htmlInheritanceFileNumber) {
		this.htmlInheritanceFileNumber = htmlInheritanceFileNumber;
	}
	public HtmlInputText getHtmlInheritanceFilePerson() {
		return htmlInheritanceFilePerson;
	}
	public void setHtmlInheritanceFilePerson(HtmlInputText htmlInheritanceFilePerson) {
		this.htmlInheritanceFilePerson = htmlInheritanceFilePerson;
	}
	public HtmlCommandButton getBtnAddSupplier() {
		return btnAddSupplier;
	}
	public void setBtnAddSupplier(HtmlCommandButton btnAddSupplier) {
		this.btnAddSupplier = btnAddSupplier;
	}

}
