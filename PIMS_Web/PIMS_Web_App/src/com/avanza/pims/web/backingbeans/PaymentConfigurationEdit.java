package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.NOLTypeView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.ui.util.ResourceUtil;


public class PaymentConfigurationEdit extends AbstractController
{
	public static class Messages
	{
		public static String PROCEDURE_TYPE_REQUIRED = "paymentConfiguration.ErrorMessages.ProcedureTypeRequired";
		public static String PAYMENT_TYPE_REQUIRED = "paymentConfiguration.ErrorMessages.PaymentTypeRequired";
		public static String DESCRIPTION_EN_REQUIRED = "paymentConfiguration.ErrorMessages.DescriptionEnRequired";
		public static String DESCRIPTION_AR_REQUIRED = "paymentConfiguration.ErrorMessages.DescriptionArRequired";		
		public static String PAYMENT_SUB_TYPE_TYPE_REQUIRED = "paymentConfiguration.ErrorMessages.PaymentSubTypeTypeRequired";
		public static String PAYMENT_NATURE_REQUIRED = "paymentConfiguration.ErrorMessages.PaymentNatureRequired";
		public static String PAYMENT_AMOUNT_REQUIRED = "paymentConfiguration.ErrorMessages.PaymentAmountRequired";
		public static String PAYMENT_PERCENTAGE_REQUIRED = "paymentConfiguration.ErrorMessages.PaymentPercentageRequired";
		public static String INVALID_PAYMENT_AMOUNT = "paymentConfiguration.ErrorMessages.InvalidPaymentAmount";
		public static String INVALID_PAYMENT_Percentage = "paymentConfiguration.ErrorMessages.InvalidPaymentPercentage";
		public static String ERROR_ON_SAVE = "paymentConfiguration.ErrorMessages.ERRORONSAVE";
		
	}
	private transient Logger logger = Logger.getLogger( PaymentConfigurationEdit.class );	
	/**
	 * 
	 */
	
	public PaymentConfigurationEdit(){
		
	}
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	
	HtmlSelectOneMenu cmbProcedureType = new HtmlSelectOneMenu();
	HtmlSelectOneMenu cmbPaymentType = new HtmlSelectOneMenu();
	HtmlSelectOneMenu cmbCondition = new HtmlSelectOneMenu();
	HtmlSelectOneMenu cmbPaymentSubType = new HtmlSelectOneMenu();
	HtmlSelectOneMenu cmbBasedOnType = new HtmlSelectOneMenu();
	HtmlInputText procedureType = new HtmlInputText();
	HtmlInputText paymentDescriptionEn = new HtmlInputText();
	HtmlInputText paymentDescriptionAr = new HtmlInputText();
	HtmlInputText paymentMinAmount = new HtmlInputText();
	HtmlInputText paymentMaxAmount = new HtmlInputText();
	HtmlInputText paymentAmount = new HtmlInputText();
	HtmlSelectOneRadio radioPaymentNature = new HtmlSelectOneRadio();
	HtmlInputHidden paymentConfigurationId = new HtmlInputHidden();
	
	private String selectedBasedOn;
	private String selectedDepositType;
	
	private boolean isNOLSelected = false;
	private String nolPaymentSubTypecss ="";
	private String percentBasedOnCss ="";
	private String percentAmountOnCss ="";
	private boolean isFixed = false;
	
	
	private final String FIXED_VALUE = "FIXED";
	private final String PERCENTAGE = "PERCENT";
	private final String PAYMNET_TYPE_LIST = "PAYMNET_TYPE_LIST";
	private final String PAYMENT_SUB_TYPE_LIST = "PAYMENT_SUB_TYPE_LIST";
	private final String PAYMENT_NATURE_LIST = "PAYMENT_NATURE_LIST";
	
	
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	
	public boolean getIsNOLSelected() {
		boolean returnVal  = true;
		if(cmbProcedureType.getValue()!= null && cmbProcedureType.getValue().equals(WebConstants.PROCEDURE_NO_OBJECTION_LETTER_ID))
		{
			returnVal = false;			
		}			
		return returnVal;
	}
	public boolean getIsFixed() {
		
		boolean returnVal  = false;		
		if(radioPaymentNature.getValue()!= null && radioPaymentNature.getValue().equals(FIXED_VALUE))
		{
			returnVal = true;			
		}			
		return returnVal;
	}
	public String getNolPaymentSubTypecss() {
		String css = "";
		
		if(getIsNOLSelected())
		{
			css = "READONLY";			
		}		
		return css;		
	}
	public String getPercentBasedOnCss() {		
		String css = "";
		
		if(getIsFixed())
		{
			css = "READONLY";			
		}		
		return css;		
	}
	
	public String getPercentAmountOnCss() {
		String css = "A_RIGHT_NUM";
		if(getIsFixed())
		{
			css = "READONLY";
		}
		return css;
	}
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentTypeList() {
		List<SelectItem> cboList = (List<SelectItem>)viewMap.get(PAYMNET_TYPE_LIST);		
		if(cboList==null) {
			UtilityServiceAgent usa = new UtilityServiceAgent();
			try {
				cboList = new ArrayList<SelectItem>();
				List<PaymentTypeView> paymentTypeViewList = usa.getAllPaymentTypes();
				for(PaymentTypeView paymentTypeView: paymentTypeViewList)
				{
					String textVal = "";
					if(getIsEnglishLocale())
						textVal = paymentTypeView.getDescriptionEn();
					else
						textVal = paymentTypeView.getDescriptionAr();
					
					cboList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),textVal));
				}				
			} catch (PimsBusinessException e) {
				
				logger.logError("getPaymentTypeList crashed :::::::::::::",e);
			}
			
			viewMap.put(PAYMNET_TYPE_LIST, cboList);
		}

		return cboList;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentSubTypeList() {
		List<SelectItem> cboList = (List<SelectItem>)viewMap.get(PAYMENT_SUB_TYPE_LIST);
		if(cboList==null) {
			UtilityServiceAgent usa = new UtilityServiceAgent();
			try {
				cboList = new ArrayList<SelectItem>();
				List<NOLTypeView> nOLTypeViewList = usa.getAllNOLTypes();
				for(NOLTypeView nolTypeView: nOLTypeViewList)
				{
					String textVal = "";
					if(getIsEnglishLocale())
						textVal = nolTypeView.getDescriptionEn();
					else
						textVal = nolTypeView.getDescriptionAr();
					
					cboList.add(new SelectItem(nolTypeView.getNolTypeId().toString(),textVal));
				}				
			} catch (PimsBusinessException e) {
				
				logger.logError("getPaymentSubTypeList crashed  :::::::::::::",e);
			}
			
			viewMap.put(PAYMENT_SUB_TYPE_LIST, cboList);
		}

		return cboList;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentNatureList() {
		List<SelectItem> cboList = (List<SelectItem>)viewMap.get(PAYMENT_NATURE_LIST);
		SelectItem item = null;
		String itemLabel = "";
		if(cboList==null) {
			itemLabel = getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.FIXED_DEPOSIT);
			cboList = new ArrayList<SelectItem>();
			item = new SelectItem(FIXED_VALUE,itemLabel);
			cboList.add(item);
			itemLabel = getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.PERCENT);
			item = new SelectItem(PERCENTAGE,itemLabel);
			cboList.add(item);
		}
		viewMap.put(PAYMENT_NATURE_LIST, cboList);

		return cboList;
	}
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
		{			
		  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
		  loggedInUser = user.getLoginId();
		}
		
		return loggedInUser;
	}
	
	@SuppressWarnings("unchecked")
	public void savePaymentConfiguration(){	
		
		logger.logInfo("savePaymentConfiguration() started...");
		ResourceUtil resourceUtil = ResourceUtil.getInstance();
		try{
			
			if(validateData())
			{
				String loggedInUser = getLoggedInUser();				
				Object paymentDescriptionEnVal = paymentDescriptionEn.getValue();
	    		Object paymentDescriptionArVal = paymentDescriptionAr.getValue();
	    		Object cmbPaymentTypeVal = cmbPaymentType.getValue();
	    		Object cmbProcedureTypeVal = cmbProcedureType.getValue();
	    		Object cmbConditionVal = cmbCondition.getValue();
	    		Object cmbPaymentSubTypeVal = cmbPaymentSubType.getValue();
	    		Object radioPaymentNatureVal = radioPaymentNature.getValue();
	    		Object cmbBasedOnTypeVal = cmbBasedOnType.getValue();
	    		Object paymentMinAmountVal = paymentMinAmount.getValue();
	    		Object paymentMaxAmountVal = paymentMaxAmount.getValue();
	    		Object paymentAmountVal = paymentAmount.getValue();
	    		Object paymentConfigIdVal = paymentConfigurationId.getValue();
	    		boolean isAdd = true;
	    		
	    		Long paymentConfgiurationId = null; 
	    		Long paymentTypeId = Long.parseLong(cmbPaymentTypeVal.toString());
	    		Long procedureTypeId = Long.parseLong(cmbProcedureTypeVal.toString());	    		
	    		String createdBy = loggedInUser;
	    		String updatedBy = loggedInUser;
	    		Double amount = Double.parseDouble(paymentAmountVal.toString());
	    		Long basedOnTypeId = null;
	    		Long conditionId = null;
	    		Long nolTypeId = null;
	    		Date createdOn = new Date();
	    		Date updatedOn = new Date();
	    		String descriptionEn = paymentDescriptionEnVal.toString();
	    		String descriptionAr = paymentDescriptionArVal.toString();
	    		Double maxAmount = null;
	    		Double minAmount = null;
	    		Long isDeleted = WebConstants.DEFAULT_IS_DELETED;
	    		Long isFixed = null;
	    		Long recordStatus = WebConstants.DEFAULT_RECORD_STATUS;
	    		
				PaymentConfigurationView pcv = new PaymentConfigurationView();
				
				if(paymentConfigIdVal != null && !paymentConfigIdVal.toString().equals(""))
					{
					paymentConfgiurationId = Long.parseLong(paymentConfigIdVal.toString());
					isAdd = false;
					}
				else
				{ 
					isAdd = true;
				}
				
				if(cmbBasedOnTypeVal != null && !cmbBasedOnTypeVal.toString().equals("-1"))
				{
					basedOnTypeId = Long.parseLong(cmbBasedOnTypeVal.toString());
				}
				if(cmbConditionVal != null && !cmbConditionVal.toString().equals("-1"))
				{
					conditionId = Long.parseLong(cmbConditionVal.toString());
				}
				if(cmbPaymentSubTypeVal != null && !cmbPaymentSubTypeVal.toString().equals("-1"))
				{
					nolTypeId = Long.parseLong(cmbPaymentSubTypeVal.toString());
				}
				if(paymentMinAmountVal != null && !paymentMinAmountVal.toString().trim().equals(""))
				{
					minAmount = Double.parseDouble(paymentMinAmountVal.toString());
				}
				if(paymentMaxAmountVal != null && !paymentMaxAmountVal.toString().trim().equals(""))
				{
					maxAmount = Double.parseDouble(paymentMaxAmountVal.toString());
				}
				if(radioPaymentNatureVal.toString().equals(FIXED_VALUE))
					isFixed = new Long(1);
				else
					isFixed = new Long(0);
				
				pcv.setPaymentConfigurationId(paymentConfgiurationId);
				pcv.setAmount(amount);
				pcv.setBasedOnTypeId(basedOnTypeId);
				pcv.setConditionId(conditionId);
				pcv.setCreatedBy(createdBy);
				pcv.setCreatedOn(createdOn);
				pcv.setDescriptionAr(descriptionAr);
				pcv.setDescriptionEn(descriptionEn);
				pcv.setIsDeleted(isDeleted);
				pcv.setIsFixed(isFixed);
				pcv.setUpdatedOn(updatedOn);
				pcv.setUpdatedBy(updatedBy);
				pcv.setMaxAmount(maxAmount);
				pcv.setMinAmount(minAmount);
				pcv.setNolTypeId(nolTypeId);
				pcv.setPaymentTypeId(paymentTypeId);
				pcv.setProcedureTypeId(procedureTypeId); 
				pcv.setRecordStatus(recordStatus);
				
				UtilityServiceAgent usa = new UtilityServiceAgent();
				
				pcv = usa.persistPaymentConfiguration(pcv);				
				
				if(pcv == null)
				{
					errorMessages.clear();
					errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.ERROR_ON_SAVE));
					
				}
				
				else
				{
				
	   		     FacesContext facesContext = FacesContext.getCurrentInstance();
	   		     sessionMap.put("PAYMENT_CONFIG_POPUP_RESULT", isAdd);
	   		     sessionMap.put("PAYMENT_CONFIG_POPUP_RESULT_PROC_ID", procedureTypeId);
		         String javaScriptText = "javascript:closeWindowSubmit();";
		        
		         // Add the Javascript to the rendered page's header for immediate execution
		         AddResource addResource = AddResourceFactory.getInstance(facesContext);
		         addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
				}
			}
			
			logger.logInfo("savePaymentConfiguration() completed successfully!!!");
		}
    	/*catch(PimsBusinessException exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}*/
    	catch(Exception exception){
    		logger.LogException("savePaymentConfiguration() crashed ",exception);
    		errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.ERROR_ON_SAVE));
    	}
    	
	}
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	public String getInvalidMessage(String field){
		logger.logInfo("getInvalidMessage(String) started...");
		StringBuffer message = new StringBuffer("");
		
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}
	
	public Number getNumberValue(String doubleStr, String pattern) throws Exception{
		logger.logInfo("getNumberValue started...");
		Number numberVal = null;
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
			logger.logInfo("getNumberValue completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("getNumberValue crashed!!!", exception);
			throw exception;
		}
		return numberVal;
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}
	
	public Boolean validateData()
	{
		logger.logInfo("validateData() started...");
		Boolean validated = true;
		
    	try{
    		ResourceUtil resourceUtil = ResourceUtil.getInstance();
    		
    		Object paymentDescriptionEnVal = paymentDescriptionEn.getValue();
    		Object paymentDescriptionArVal = paymentDescriptionAr.getValue();
    		Object cmbPaymentTypeVal = cmbPaymentType.getValue();
    		Object cmbProcedureTypeVal = cmbProcedureType.getValue();
    		Object cmbConditionVal = cmbCondition.getValue();
    		Object cmbPaymentSubTypeVal = cmbPaymentSubType.getValue();
    		Object radioPaymentNatureVal = radioPaymentNature.getValue();
    		Object cmbBasedOnTypeVal = cmbBasedOnType.getValue();
    		Object paymentMinAmountVal = paymentMinAmount.getValue();
    		Object paymentMaxAmountVal = paymentMaxAmount.getValue();
    		Object paymentAmountVal = paymentAmount.getValue();
    		
    		errorMessages.clear();
    		
    		if(cmbProcedureTypeVal == null)
    		{
    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PROCEDURE_TYPE_REQUIRED));
    		}    		
    		if(cmbPaymentTypeVal == null)
    		{
    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PAYMENT_TYPE_REQUIRED));
    		}
    		
    		if(paymentDescriptionEnVal == null || paymentDescriptionEnVal.toString().equals("") )
    		{
    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.DESCRIPTION_EN_REQUIRED));
    		}
    		if(paymentDescriptionArVal == null || paymentDescriptionArVal.toString().equals("") )
    		{
    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.DESCRIPTION_AR_REQUIRED));
    		}
    		if(cmbProcedureTypeVal != null && cmbProcedureTypeVal.toString().equals(WebConstants.PROCEDURE_NO_OBJECTION_LETTER_ID) )
    		{
    			if(cmbPaymentSubTypeVal == null || cmbPaymentSubTypeVal.toString().equals("-1"))
        		{
    				errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PAYMENT_SUB_TYPE_TYPE_REQUIRED));
        		}    			
    		}
    		if(radioPaymentNatureVal == null)
    		{
    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PAYMENT_NATURE_REQUIRED));
    		}    		
    		
    		if(radioPaymentNatureVal != null)
    		{
    			if(paymentAmountVal == null)
        		{
    				if(radioPaymentNatureVal.toString().equals(FIXED_VALUE))
    					errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PAYMENT_AMOUNT_REQUIRED));
    				else
    					errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.PAYMENT_PERCENTAGE_REQUIRED));
        		}    			
    		}

    			
    		String pattern = getNumberFormat();    		
    		
    		if(radioPaymentNatureVal != null)
    		{    			
    		
    		if(radioPaymentNatureVal.toString().equals(FIXED_VALUE)){
	    		Number fixedPrice = 0;
	    		try{
	    			String fixedValStr = paymentAmountVal.toString();
	    			fixedPrice = getNumberValue(fixedValStr,pattern);
	    			String fixedStr = fixedValStr.replace(',', '0');
	    			char temp = fixedStr.toLowerCase().charAt(fixedStr.length()-1);
	    			if(temp>='a' && temp<='z')
	    				throw new Exception();
	    			Double dbl = new Double(fixedStr);
	    		} catch(Exception e){
	    				    			
	    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.INVALID_PAYMENT_AMOUNT));	    			
	    		}
    		
	    		if(fixedPrice.doubleValue()<0D)
	    		{	    		
	    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.INVALID_PAYMENT_AMOUNT));		    		
	    		}	    		
    		}
    		else {
    			
	    		Number percent = 0;
	    		try{
	    			String fixedValStr = paymentAmountVal.toString();
	    			percent = getNumberValue(fixedValStr,pattern);
	    			String fixedStr = fixedValStr.replace(',', '0');
	    			char temp = fixedStr.toLowerCase().charAt(fixedStr.length()-1);
	    			if(temp>='a' && temp<='z')
	    				throw new Exception();
	    			Double dbl = new Double(fixedStr);
	    		} catch(Exception e){
	    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.INVALID_PAYMENT_Percentage));  	
	    		}
    		
	    		if(percent.doubleValue()<0D)
	    		{
	    			errorMessages.add(resourceUtil.getProperty(PaymentConfigurationEdit.Messages.INVALID_PAYMENT_Percentage));
	    		}
	    		
    		}
    		}
    		if(errorMessages.size() > 0)
    			validated = false;
	        logger.logInfo("validateData() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("validateData() crashed ", exception);
		}
		return validated;
	}
	
	
    public void cancel(ActionEvent event) {
    	logger.logInfo("cancel() started...");
        FacesContext facesContext = FacesContext.getCurrentInstance();

        String javaScriptText = "window.close();";
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        logger.logInfo("cancel() completed successfully!!!");
    }

    public void done(ActionEvent event) {
    	logger.logInfo("done() started...");
    	try{
    		
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	
	        String javaScriptText = "window.closeWindowSubmit();";
	        
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("done() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("done() crashed ", exception);
		}
    }
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
    
	public void radioValueChanged(ValueChangeEvent event){
		try {
			
//			if(event.getNewValue().toString().equals(FIXED_VALUE))
//			{
//				paymentMinAmount.setDisabled(true);
//				paymentMaxAmount.setDisabled(true);
//				cmbBasedOnType.setDisabled(true);	
//				
//				paymentMinAmount.setReadonly(true);
//				paymentMaxAmount.setReadonly(true);
//				cmbBasedOnType.setReadonly(true);	
//			}
//			else
//			{
//				paymentMinAmount.setDisabled(false);
//				paymentMaxAmount.setDisabled(false);
//				cmbBasedOnType.setDisabled(false);	
//			}
			
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	public void procedureTypeChanged(ValueChangeEvent event){
		try {
			
//			if(event.getNewValue().toString().equals(WebConstants.PROCEDURE_NO_OBJECTION_LETTER_ID))
//			{
//				cmbPaymentSubType.setDisabled(false);	
//			}	
//			else
//			{
//				cmbPaymentSubType.setDisabled(true);	
//			}			
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	public void comboValueChanged(ValueChangeEvent event){
		try {
			setValues();
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>COMBO");
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{		
				setValues();				
			}
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	public void setValues(){
		
		PaymentConfigurationView pcv = (PaymentConfigurationView) sessionMap.get("SELECTED_PAYMENT_CONFIGURATION_VO");
		
		if(pcv.getProcedureTypeId() != null)
			cmbProcedureType.setValue(pcv.getProcedureTypeId().toString());
		
		if(pcv.getPaymentTypeId() != null)
			cmbPaymentType.setValue(pcv.getPaymentTypeId().toString());
		
		if(pcv.getConditionId() != null)
			cmbCondition.setValue(pcv.getConditionId().toString());
		
		if(pcv.getDescriptionAr() != null)
			paymentDescriptionAr.setValue(pcv.getDescriptionAr().toString());
		
		if(pcv.getDescriptionEn() != null)
			paymentDescriptionEn.setValue(pcv.getDescriptionEn());
		
		if(pcv.getNolTypeId() != null)
			cmbPaymentSubType.setValue(pcv.getNolTypeId().toString());
		
		if(pcv.getIsFixed()!= null)
		{
			if(pcv.getIsFixed().toString().equals("1"))
				radioPaymentNature.setValue(FIXED_VALUE);
			else
				radioPaymentNature.setValue(PERCENTAGE);
		}
			
		if(pcv.getBasedOnTypeId() != null)
		{
			cmbBasedOnType.setValue(pcv.getBasedOnTypeId().toString());
		}
		
		if(pcv.getAmount() != null)
			paymentAmount.setValue(pcv.getAmount());
		
		if(pcv.getMinAmount() != null)
			paymentMinAmount.setValue(pcv.getMinAmount());
		
		if(pcv.getMaxAmount() != null)
			paymentMaxAmount.setValue(pcv.getMaxAmount());		
		
		if(pcv.getPaymentConfigurationId() != null)
			paymentConfigurationId.setValue(pcv.getPaymentConfigurationId());
	}
	
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		
		applyMode();
	}

	private void applyMode(){
		String radioMode = (String)viewMap.get("RADIO_MODE");
		if(radioMode!=null){
			
			}
			else{

			}
		}
		
	
	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}


	

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}


	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public void setDataList(List<AuctionUnitView> dataList) {
		if(dataList!=null)
			viewMap.put(WebConstants.CHOSEN_AUCTION_UNITS,dataList);
	}

	public Double getMaxDeposit() {
		Double maxDeposit = (Double)viewMap.get("maxDeposit");
		return maxDeposit;
	}


	public void setMaxDeposit(Double maxDeposit) {
		if(maxDeposit!=null)
			viewMap.put("maxDeposit" , maxDeposit);
	}


	public Double getMinDeposit() {
		Double minDeposit = (Double)viewMap.get("minDeposit");
		return minDeposit;
	}


	public void setMinDeposit(Double minDeposit) {
		if(minDeposit!=null)
			viewMap.put("minDeposit" , minDeposit);
	}


	public String getSelectedDepositType() {
		return selectedDepositType;
	}


	public void setSelectedDepositType(String selectedDepositType) {
		this.selectedDepositType = selectedDepositType;
	}


	
	public String getSelectedBasedOn() {
		return selectedBasedOn;
	}


	public void setSelectedBasedOn(String selectedBasedOn) {
		this.selectedBasedOn = selectedBasedOn;
	}

	public Map<String, PaymentConfigurationView> getPaymentConfigMap() {
		Map<String,PaymentConfigurationView> paymentConfigMap = (Map<String,PaymentConfigurationView>)viewMap.get("PaymentConfigMap");
		return paymentConfigMap;
	}


	public void setPaymentConfigMap(Map<String, PaymentConfigurationView> paymentConfigMap) {
		if(paymentConfigMap!=null)
			viewMap.put("PaymentConfigMap",paymentConfigMap);
	}	

	

	public HtmlSelectOneMenu getCmbProcedureType() {
		return cmbProcedureType;
	}

	public void setCmbProcedureType(HtmlSelectOneMenu cmbProcedureType) {
		this.cmbProcedureType = cmbProcedureType;
	}

	public HtmlSelectOneMenu getCmbPaymentType() {
		return cmbPaymentType;
	}

	public void setCmbPaymentType(HtmlSelectOneMenu cmbPaymentType) {
		this.cmbPaymentType = cmbPaymentType;
	}

	public HtmlInputText getPaymentDescriptionEn() {
		return paymentDescriptionEn;
	}

	public void setPaymentDescriptionEn(HtmlInputText paymentDescriptionEn) {
		this.paymentDescriptionEn = paymentDescriptionEn;
	}

	public HtmlInputText getPaymentDescriptionAr() {
		return paymentDescriptionAr;
	}

	public void setPaymentDescriptionAr(HtmlInputText paymentDescriptionAr) {
		this.paymentDescriptionAr = paymentDescriptionAr;
	}

	public HtmlInputText getPaymentMinAmount() {
		return paymentMinAmount;
	}

	public void setPaymentMinAmount(HtmlInputText paymentMinAmount) {
		this.paymentMinAmount = paymentMinAmount;
	}

	public HtmlInputText getPaymentMaxAmount() {
		return paymentMaxAmount;
	}

	public void setPaymentMaxAmount(HtmlInputText paymentMaxAmount) {
		this.paymentMaxAmount = paymentMaxAmount;
	}

	public HtmlSelectOneMenu getCmbCondition() {
		return cmbCondition;
	}

	public void setCmbCondition(HtmlSelectOneMenu cmbCondition) {
		this.cmbCondition = cmbCondition;
	}

	public HtmlSelectOneMenu getCmbPaymentSubType() {
		return cmbPaymentSubType;
	}

	public void setCmbPaymentSubType(HtmlSelectOneMenu cmbPaymentSubType) {
		this.cmbPaymentSubType = cmbPaymentSubType;
	}

	public HtmlSelectOneMenu getCmbBasedOnType() {
		return cmbBasedOnType;
	}

	public void setCmbBasedOnType(HtmlSelectOneMenu cmbBasedOnType) {
		this.cmbBasedOnType = cmbBasedOnType;
	}
	public HtmlSelectOneRadio getRadioPaymentNature() {
		return radioPaymentNature;
	}
	public void setRadioPaymentNature(HtmlSelectOneRadio radioPaymentNature) {
		this.radioPaymentNature = radioPaymentNature;
	}
	public HtmlInputText getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(HtmlInputText paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public void setPercentBasedOnCss(String percentBasedOnCss) {
		this.percentBasedOnCss = percentBasedOnCss;
	}
	public void setPercentAmountOnCss(String percentAmountOnCss) {
		this.percentAmountOnCss = percentAmountOnCss;
	}

	public void setIsFixed(boolean isFixed) {
		this.isFixed = isFixed;
	}
	public void seIstNOLSelected(boolean isNOLSelected) {
		this.isNOLSelected = isNOLSelected;
	}
	
	public void setNolPaymentSubTypecss(String nolPaymentSubTypecss) {
		this.nolPaymentSubTypecss = nolPaymentSubTypecss;
	}
	public HtmlInputHidden getPaymentConfigurationId() {
		return paymentConfigurationId;
	}
	public void setPaymentConfigurationId(HtmlInputHidden paymentConfigurationId) {
		this.paymentConfigurationId = paymentConfigurationId;
	}	
}