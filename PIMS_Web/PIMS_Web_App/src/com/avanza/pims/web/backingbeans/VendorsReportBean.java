package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.hibernate.Criteria;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.VendorsReportCriteria;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.RegionView;

public class VendorsReportBean extends AbstractController{

	private VendorsReportCriteria reportCriteria; 
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private String stateId;
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	Map viewRootMap=getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	public VendorsReportBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new VendorsReportCriteria(ReportConstant.Report.VENDORS_REPORT_EN, ReportConstant.Processor.VENDORS_REPORT, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new VendorsReportCriteria(ReportConstant.Report.VENDORS_REPORT_AR, ReportConstant.Processor.VENDORS_REPORT, CommonUtil.getLoggedInUser());
	}

	public VendorsReportCriteria getReportCriteria() {
		return reportCriteria;
	}
	public void setReportCriteria(VendorsReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}
	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}
	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	 private static Logger logger = Logger.getLogger("VendorsReportBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	private boolean isArabicLocale;
	private boolean isEnglishLocale;

	public String btnPrint_Click() {	
		
		    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
				return "";
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public void init() {
		super.init();
			loadState();
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
			}
	}
	
	

	
	public void loadState(/*ValueChangeEvent vce*/)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "UAE";
		List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,CommonUtil.getIsArabicLocale());
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (CommonUtil.getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
			  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}	
	
	
	
	
	
	public void  loadCity(){
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();

		if(viewRootMap.containsKey("cityList")){
			viewRootMap.remove("cityList");
			cityList.clear();
		}

		try 
		{

			Set<RegionView> regionViewList = null;
			if(reportCriteria.getEmirate()!=null && new Long(reportCriteria.getEmirate()).compareTo(new Long(-1))!=0)
			{	
				regionViewList =  psa.getCity(new Long(reportCriteria.getEmirate()));
			sessionMap.put(WebConstants.SESSION_CITY,regionViewList);


			Iterator itrator = regionViewList.iterator();

			for(int i=0;i<regionViewList.size();i++)
			{
				RegionView rV=(RegionView)itrator.next();
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
				}
				else 
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				this.getCityList().add(item);


			}
			

			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
			logger.logInfo(methodName+"|"+"Finish");
		 }	
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);

		}

	}
	 public List<SelectItem> getCityList() {
		 if(viewRootMap.containsKey("cityList")){
			 //cityList.clear();
			 cityList= (List<SelectItem>) (viewRootMap.get("cityList"));

		 }
		 return cityList;	

	 }

	 public void setCityList(List<SelectItem> cityList) {
		 this.cityList = cityList;

	 }
	 public boolean getIsArabicLocale() {
		 isArabicLocale = !getIsEnglishLocale();
		 return isArabicLocale;
	 }

	 public boolean getIsEnglishLocale() {

		 String method="getIsEnglishLocale";

		 logger.logInfo(method+"|"+"Start...");
		 WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		 LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		 isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		 logger.logInfo(method+"|"+"Finish...");
		 return isEnglishLocale;
	 }
	
	
}

