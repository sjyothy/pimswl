package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
public class ContractPersonList extends AbstractController
{
	
	private HtmlDataTable dataTable;
	private HtmlDataTable propspectivePersonDataTable;
	private String firstName  ;
	private String lastName ;
	private Long personTypeId;
	private Long nationalityId;
	private String passportNumber;
	private String residenseVisaNumber;
	private String personalSecCardNo;
	private String drivingLicenseNumber;
	private String socialSecNumber;
	private String cellNumber;
	private String designation;
	private boolean showProspectiveGrid;
	//private Calendar occupiedTillDate = new Calendar();
	private PersonView personView = new PersonView();
	String controlName;
	String hdndisplaycontrolName;
	String controlForId;
	String requestForPersonType;
	String displayStylePersonTypeCombo;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    List<SelectItem> personTypeList =new ArrayList<SelectItem>();
	public String selectManyPersonType;
	String sessionName; 
	//We have to look for the list population for facilities
	private List<String> errorMessages;
    private boolean isSelectOnePerson;
    private boolean isSelectManyPerson;
	private PersonView dataItem = new PersonView();
	private PersonView prospectiveDataItem = new PersonView();	
	private static Logger logger = Logger.getLogger( ContractPersonList.class );
	private List<PersonView> dataList = new ArrayList<PersonView>();
	private List<PersonView> prospectivePersonDataList = new ArrayList<PersonView>();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;


	public String getSelectManyPersonType()
    {
		return selectManyPersonType;
	}

	public void setSelectManyPersonType(String selectManyPersonType) {
		this.selectManyPersonType = selectManyPersonType;
	}

	@Override 
	 public void init() 
     {
		String methodName="init";
    	logger.logInfo(methodName+"|"+"Start");
    	 super.init();
    	 try
    	 {
    		 
    		 if(!isPostBack())
    		 {
    			   
    			   HttpServletRequest request =
        			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    			   
    			   
    			   if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PERSON));
        		      sessionMap.remove(WebConstants.SESSION_CONTRACT_PERSON);
        		      
        		 if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().containsKey("persontype") && 
        				 FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("persontype")!=null 
        		    )
        		 {
        			 requestForPersonType=FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("persontype").toString();
        			//Load For Combos;
        			getSessionName();
        			loadPersonTypeCombo();
        			
        		 }
        			 
        		 else if(request.getParameter("persontype")!=null)
        		 {
        			 requestForPersonType=request.getParameter("persontype").toString();
        			//Load For Combos;
        			getSessionName();
        			loadPersonTypeCombo();
        			
        		 }
        		 if(request.getParameter("displaycontrolname")!=null)
        			controlName=request.getParameter("displaycontrolname").toString();
        		 if(request.getParameter("hdndisplaycontrolname")!=null)
         			hdndisplaycontrolName=request.getParameter("hdndisplaycontrolname").toString();
         		
        		 if(request.getParameter("hdncontrolname")!=null)
         			controlForId=request.getParameter("hdncontrolname").toString();
        		// loadDataList();
        		
            	
    		 }
    		 getSessionName();
    		 logger.logInfo(methodName+"|"+"Finish");
    		 
    	 }
    	 catch(Exception es)
    	 {
    		 logger.logError(methodName+"|"+"Exception::"+es);
    		 
    	 }
	        
	 }
	  private void getSessionName()
	  {
		  if(requestForPersonType!=null && requestForPersonType.length()>0)
		  {
		     if(requestForPersonType.equals(WebConstants.OCCUPIER))
	 			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER;
			 else if(requestForPersonType.equals(WebConstants.PARTNER))
	     			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER;
			 else if(requestForPersonType.equals(WebConstants.SPONSOR))
  			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_SPONSOR;
			 else if(requestForPersonType.equals(WebConstants.MANAGER))
  			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_MANAGER;
		  
		  }
		  
	  }
	  public void preprocess() 
	    {
		 super.preprocess();
		 requestForPersonType=requestForPersonType;
		 getSessionName();
	    }
	  public void prerender() 
	    {
		 super.prerender();
		 getSessionName();
		 getshowProspectiveGrid();
	    }


	 private void loadPersonTypeCombo() 
	 {
		 String methodName="loadPersonTypeCombo";
		 logger.logInfo(methodName+"|"+"Start");
		 PropertyServiceAgent psa=new PropertyServiceAgent();
		 try
		 {
			 if(!sessionMap.containsKey(WebConstants.PERSON_TYPE) )
			 {
				 
				 List<DomainDataView> domainDataList=psa.getDomainDataByDomainTypeName(WebConstants.PERSON_TYPE);
				 sessionMap.put(WebConstants.PERSON_TYPE, domainDataList);
				 
			 }
			 logger.logInfo(methodName+"|"+"Finish");
		 }
		 catch(Exception ex)
		 {
			 logger.logError(methodName+"|"+"Exception ::"+ex);
			 
		 }
		 
	 }
	 
	public String btnAdd_Click()
	{
		String methodName="btnAdd_Click";
		logger.logInfo(methodName+"|"+"Start");
				logger.logInfo(methodName+"|"+"Finish");
				setRequestParam("PERSON_TYPE", requestForPersonType);
		return "addPerson";
		
	}
	public String openPopUp()
	{
		String methodName="openPopUp";
        final String viewId = "/ContractPersonList.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText = "window.opener.document.forms[0].submit();" +
                                "window.close();";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public void sendInfoToParent()
	{
		 if(requestForPersonType.equals(WebConstants.OCCUPIER))
 			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER;
		 else if(requestForPersonType.equals(WebConstants.PARTNER))
     			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER;
		List<PersonView> personViewList = (ArrayList<PersonView>)sessionMap.get(WebConstants.SESSION_CONTRACT_PERSON);
		List<PersonView> newpersonViewList= new ArrayList<PersonView>();
		List<PersonView> alreadySelectedPersonsList =new ArrayList<PersonView>();
		if(sessionMap.containsKey(sessionName))
		{
			alreadySelectedPersonsList = (ArrayList<PersonView>)sessionMap.get(sessionName);	
		}
		
		for (PersonView personView : personViewList) 
		{
		  if(personView.getSelected() && !personAlreadyInSession(personView))
		  {
			  newpersonViewList.add(personView);
		  }
		}
		if(alreadySelectedPersonsList.size()>0)
			newpersonViewList.addAll(alreadySelectedPersonsList);
		sessionMap.put(sessionName, newpersonViewList);
		openPopUp();
	}
	private boolean personAlreadyInSession(PersonView editpersonView) 
	{
	    String methodName="personAlreadyInSession";
	    boolean isPersonInSession=false;
		List<PersonView> previouspersonViewList =new ArrayList<PersonView>();
		logger.logInfo(methodName+"|"+"Start");
		try
		{
			if(sessionMap.containsKey(sessionName))
			{
			 	previouspersonViewList= (ArrayList)sessionMap.get(sessionName);
			 	for(int i=0;i<previouspersonViewList.size();i++)
		        {
		             PersonView previousPreviousView=(PersonView) previouspersonViewList.get(i);
		              //logger.logInfo(methodName+"|"+"previousPaidFacilityView.getFacilityId():::"+previousPaidFacilityView.getFacilityId());
					  //logger.logInfo(methodName+"|"+"Selected paidFacilitiesView.getFacilityId():::"+paidFacilitiesView.getFacilityId());
					  //If previous List has same item with id  as new View 
		              //then discard it else add that item to new List.    
					 if(previousPreviousView.getPersonId().toString().equals(editpersonView.getPersonId().toString()))
		             {
		            	isPersonInSession=true;
		              
		             }
		         }
			}
			logger.logInfo(methodName+"|"+"Finish");
			
		}
        catch(Exception e)
        {
             logger.logError(methodName +"|"+"Exception Occured::"+e);
             
        }

        return isPersonInSession;



     }
	public List<PersonView> getPropertyInquiryDataList()
	{
		String methodName="getPropertyInquiryDataList";
		
		logger.logDebug(methodName+"|"+"Start");
		
		if (dataList == null || dataList.size()==0)
		{
			dataList= loadDataList(); 
		}
		logger.logDebug(methodName+"|"+"Finish");
		return dataList;
	}
	public List<PersonView> getProspectivePersonDataList()
	{
        String methodName="getProspectivePersonDataList";
		
		logger.logInfo(methodName+"|"+"Start");
		
		if (sessionMap.containsKey(sessionName))
		{
			prospectivePersonDataList= (ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PERSON); 
		
	         
		}
		logger.logInfo(methodName+"|"+"Finsih");
		return prospectivePersonDataList;
	}
	public String btnAddPersonToProspectiveSession_Click()
	{
		String methodName="btnAddPersonToProspectiveSession_Click";
		logger.logDebug(methodName+"|"+"Start");
		try 
		{
			
			 if(requestForPersonType.equals(WebConstants.OCCUPIER))
	 			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER;
			 else if(requestForPersonType.equals(WebConstants.PARTNER))
	     			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER;
		addNewItemsInSession();
		logger.logDebug(methodName+"|"+"Finish");
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Error",ex);
			
		}
		showProspectiveGrid=true;
		return "";
	}
	public boolean getshowProspectiveGrid()
	{
		
        String methodName="getshowProspectiveGrid";
		logger.logDebug(methodName+"|"+"Start");
		logger.logDebug(methodName+"|"+"showProspectiveGrid:"+showProspectiveGrid);
		showProspectiveGrid=false;
		logger.logDebug(methodName+"|"+"sessionName:"+sessionName);
		if(sessionName!=null)
		{
		if(sessionMap.containsKey(sessionName) )
			showProspectiveGrid=true;
		}
		
		logger.logDebug(methodName+"|"+"showProspectiveGrid:"+showProspectiveGrid);
		logger.logDebug(methodName+"|"+"Finish");
		return showProspectiveGrid;
		
	}
	private void addNewItemsInSession() throws Exception
	{
	    String methodName="addNewItemsInSession";
		List<PersonView> previouspersonViewList =new ArrayList<PersonView>();
		List<PersonView> newpersonViewList =new ArrayList<PersonView>();
		logger.logInfo(methodName+"|"+"Start");
		try
		{
			PersonView editpersonView=(PersonView)dataTable.getRowData();
			if(sessionMap.containsKey(sessionName))
			{
			 	previouspersonViewList= (ArrayList)sessionMap.get(sessionName);
			 	for(int i=0;i<previouspersonViewList.size();i++)
		        {
		             PersonView previousPreviousView=(PersonView) previouspersonViewList.get(i);
		              //logger.logInfo(methodName+"|"+"previousPaidFacilityView.getFacilityId():::"+previousPaidFacilityView.getFacilityId());
					  //logger.logInfo(methodName+"|"+"Selected paidFacilitiesView.getFacilityId():::"+paidFacilitiesView.getFacilityId());
					  //If previous List has same item with id  as new View 
		              //then discard it else add that item to new List.    
					 if(!previousPreviousView.getPersonId().toString().equals(editpersonView.getPersonId().toString()))
		             {
		            	 newpersonViewList.add(previousPreviousView);
		              
		             }
		         }
			}
			
			newpersonViewList.add(editpersonView);
			
			sessionMap.put(sessionName, newpersonViewList);
					
			logger.logInfo(methodName+"|"+"Finish");
		}
        catch(Exception e)
        {
             logger.logError(methodName +"|"+"Exception Occured::"+e);
             throw e;
        }

	



     }
	public String doBid() {
//		errorMessages = new ArrayList<String>();
/*		if (getCustomerId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
			
*/ 
		return "";
		}
	
	public String doSearch (){

        

        //InquiryManager inquiryManager = new InquiryManager(); 

        String methodName="doSearch";
		logger.logInfo(methodName+"|"+"Start");


                    try {
                    	
                    
                            if (firstName !=null && !firstName.equals("")){
                            
                            	personView.setFirstName(firstName.trim());                           	
                             	
                            }
                            if (lastName !=null && !lastName.equals("")){
                            	
                            	personView.setLastName(lastName.trim());
                            }
                    
	                        if(personalSecCardNo!=null && !personalSecCardNo.equals(""))
	                        {
	                        	personView.setPersonalSecCardNo(personalSecCardNo);
	                        	
	                        }
	                        
	                        if(passportNumber!=null && !passportNumber.equals(""))
	                        {
	                        	personView.setPassportNumber(passportNumber);
	                        	
	                        }
	                        if(residenseVisaNumber!=null && !residenseVisaNumber.equals(""))
	                        {
	                        	personView.setResidenseVisaNumber(residenseVisaNumber);
	                        	
	                        }
	                        if(drivingLicenseNumber!=null && !drivingLicenseNumber.equals(""))
	                        {
	                        	personView.setDrivingLicenseNumber(drivingLicenseNumber);
	                        	
	                        }
	                        if(socialSecNumber!=null && !socialSecNumber.equals(""))
	                        {
	                        	personView.setSocialSecNumber(socialSecNumber);
	                        	
	                        }
		                    dataList.clear();
		                    if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PERSON))
		                    	sessionMap.remove(WebConstants.SESSION_CONTRACT_PERSON);
		                    loadDataList();

		            		logger.logInfo(methodName+"|"+"Finsih");

	                    }

                    catch (Exception e){


                		logger.logError(methodName+"|"+"Start");
       

                    }

                return "";

     }

	
	public List<PersonView> loadDataList() 
	{
		 String methodName="loadDataList";
		 List<PersonView> list = new ArrayList();

			logger.logInfo(methodName+"|"+"Start");
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		try
		{
 			   
	           if(!sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PERSON))
	           {
	        	   PropertyServiceAgent myPort = new PropertyServiceAgent();
	        	   List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
	   			   for(int i=0;i<domainDataList.size();i++)
	   			   {
	   			     DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
	   				 if(domainDataView.getDataValue().equals( requestForPersonType)   )
	   				 {
	   					personView.setPersonTypeId(domainDataView.getDomainDataId());
	   					break;
	   				 }
	   			   }
		            
	   			   list =  myPort.getPersonInformation(personView);
	   			if(list!=null)
					recordSize = list.size();
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize/paginatorRows;
				if((recordSize%paginatorRows)>0)
					paginatorMaxPages++;
				if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewMap.put("paginatorMaxPages", paginatorMaxPages);
		        
		           sessionMap.put(WebConstants.SESSION_CONTRACT_PERSON, list);
	                
	   			   
	           }
	           else
	        	   list=(ArrayList<PersonView>)sessionMap.get(WebConstants.SESSION_CONTRACT_PERSON);
               
	           
	           dataList =list;

	   		logger.logInfo(methodName+"|"+"Finish");

	    }
		catch (Exception ex) 
		{

			logger.logError(methodName+"|"+"loadDataList");

	    }
		return list;
		
	}
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) ||
		(errorMessages.size() == 0)) {
		messageList = "";
		} else {
		messageList = "<FONT COLOR=RED><B><UL>\n";
		for(String message: errorMessages) {
		messageList = messageList + "<LI>" + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
		}

    public boolean getisSelectOnePerson()
    {
    	String methodName="getisSelectOnePerson";

		logger.logInfo(methodName+"|"+"Start");

    	if(requestForPersonType!=null && !requestForPersonType.equals(""))
    	{
    		if(requestForPersonType.equals(WebConstants.SPONSOR)||requestForPersonType.equals(WebConstants.MANAGER))
    			isSelectOnePerson=true;
    	}

		logger.logInfo(methodName+"|"+"Finish");

    	return isSelectOnePerson;
    }
    public boolean getisSelectManyPerson()
    {
    	String methodName="isSelectManyPerson";

		logger.logInfo(methodName+"|"+"Start");

    	if(requestForPersonType!=null && !requestForPersonType.equals(""))
    	{
    		if(requestForPersonType.equals(WebConstants.OCCUPIER)||requestForPersonType.equals(WebConstants.PARTNER))
    			isSelectManyPerson=true;
    		else
    			isSelectManyPerson=false;
    		
    	}
    	else
		isSelectManyPerson=true;
    	
		logger.logInfo(methodName+"|"+"Finish");

    	return isSelectManyPerson;
    }

	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Long getPersonTypeId() {
		return personTypeId;
	}



	public void setPersonTypeId(Long personTypeId) {
		this.personTypeId = personTypeId;
	}



	public Long getNationalityId() {
		return nationalityId;
	}



	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}



	public String getPassportNumber() {
		return passportNumber;
	}



	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}



	public String getResidenseVisaNumber() {
		return residenseVisaNumber;
	}



	public void setResidenseVisaNumber(String residenseVisaNumber) {
		this.residenseVisaNumber = residenseVisaNumber;
	}



	public String getPersonalSecCardNo() {
		return personalSecCardNo;
	}



	public void setPersonalSecCardNo(String personalSecCardNo) {
		this.personalSecCardNo = personalSecCardNo;
	}



	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}



	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}



	public String getSocialSecNumber() {
		return socialSecNumber;
	}



	public void setSocialSecNumber(String socialSecNumber) {
		this.socialSecNumber = socialSecNumber;
	}



	public PersonView getPersonView() {
		return personView;
	}



	public void setPersonView(PersonView personView) {
		this.personView = personView;
	}



	public PersonView getDataItem() {
		return dataItem;
	}



	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}



	public List<PersonView> getDataList() {
		return dataList;
	}



	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlForId() {
		return controlForId;
	}
	public void setControlForId(String controlForId) {
		this.controlForId = controlForId;
	}
	public String getRequestForPersonType() {
		return requestForPersonType;
	}
	public void setRequestForPersonType(String requestForPersonType) {
		this.requestForPersonType = requestForPersonType;
	}
	public String getDisplayStylePersonTypeCombo() {
		if(requestForPersonType!=null && !requestForPersonType.equals(""))
			displayStylePersonTypeCombo="display:inline";
		else
			displayStylePersonTypeCombo="display:none";
		return displayStylePersonTypeCombo;
	}
	public void setDisplayStylePersonTypeCombo(String displayStylePersonTypeCombo) {
		this.displayStylePersonTypeCombo = displayStylePersonTypeCombo;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		isEnglishLocale =  view.getLocale().toString().equals("en");
		return isEnglishLocale;
	}
	
	public List<SelectItem> getPersonTypeList() 
	{
        String methodName="getPersonTypeList";
		logger.logInfo(methodName+"|"+"Start");

		personTypeList.clear();
		if(!sessionMap.containsKey(WebConstants.PERSON_TYPE))
			loadPersonTypeCombo();
		else
		{
			List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
			for(int i=0;i<domainDataList.size();i++)
			{
				SelectItem item =null;
				DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
				System.out.println(domainDataView.getDataValue());
				if(domainDataView.getDataValue().equals( requestForPersonType)   )
				{
					if(getIsEnglishLocale())  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn() );
					else  
					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr() );
					  personTypeList.add(item);
				}
				
				
			}
		}

		logger.logInfo(methodName+"|"+"Finish");

		return personTypeList;
	}

	public HtmlDataTable getPropspectivePersonDataTable() {
		return propspectivePersonDataTable;
	}

	public void setPropspectivePersonDataTable(
			HtmlDataTable propspectivePersonDataTable) {
		this.propspectivePersonDataTable = propspectivePersonDataTable;
	}

	public PersonView getProspectiveDataItem() {
		return prospectiveDataItem;
	}

	public void setProspectiveDataItem(PersonView prospectiveDataItem) {
		this.prospectiveDataItem = prospectiveDataItem;
	}

	public String getHdndisplaycontrolName() {
		return hdndisplaycontrolName;
	}

	public void setHdndisplaycontrolName(String hdndisplaycontrolName) {
		this.hdndisplaycontrolName = hdndisplaycontrolName;
	}

	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	


	

	
	
}
