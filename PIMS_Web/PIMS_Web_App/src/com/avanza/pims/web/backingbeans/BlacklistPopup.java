package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.context.portlet.SessionMap;
import org.apache.myfaces.custom.checkbox.HtmlCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.ViolationType;
import com.avanza.pims.proxy.pimsinspectionbpelproxy.PIMSInspectionBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorAdd.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BlackListView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationCategoryView;
import com.avanza.pims.ws.vo.ViolationTypeView;
import com.avanza.pims.ws.vo.ViolationView;
import com.crystaldecisions.report.web.shared.ErrorManager;


public class BlacklistPopup extends AbstractController 
{
	FacesContext context = FacesContext.getCurrentInstance();
	Map viewRootMap = context.getViewRoot().getAttributes();
	Map sessionMap= context.getExternalContext().getSessionMap();
	CommonUtil commUtil=new CommonUtil();
	Logger logger = Logger.getLogger(BlacklistPopup.class);
	private List<String> errorMessages;
	private List<String> successMessages;
	private String blacklistDesc;
	private HtmlCheckbox chkBoxBL=new HtmlCheckbox();
	private HtmlInputTextarea BLDesc=new HtmlInputTextarea();
	private HtmlCommandButton btnSave=new HtmlCommandButton();
	private Long tenantId;
	private boolean disabled=false;
	PersonView person = new PersonView();

	private boolean blacklist;
	PropertyServiceAgent psa=new PropertyServiceAgent();
	@Override
	public void init() 
	{
		try {
			super.init();
			errorMessages = new ArrayList<String>(0);
			successMessages = new ArrayList<String>(0);
			if (sessionMap.get("TENANT_ID") != null) {
				tenantId = (Long) sessionMap.get("TENANT_ID");
				person = psa.getTenantById(tenantId);
				viewRootMap.put("PERSON_VIEW", person);
				sessionMap.remove("TENANT_ID");
				if(person.getIsBlacklisted()==1L)
					{
						viewRootMap.put("BLACKLISTED", true);
						List<BlackListView> blvList=new ArrayList<BlackListView>(0);
						BlackListView blv=new BlackListView();
						blvList=psa.getBlackListViewByPersonId(person.getPersonId());
						blv=blvList.get(blvList.size()-1);
						viewRootMap.put("BLACKLIST_DESC",blv.getRemarks());
						viewRootMap.put("BLACKLIST",true);
						btnSave.setRendered(false);
					}
				else 
					viewRootMap.put("BLACKLISTED",false);
			}
		} 
		catch (Exception e) 
		{
			logger.LogException("init() Crashed :", e) ;
		}
			
	}
	public void save()
	{
		try 
		{
			PersonView person = new PersonView();
			BlackListView blv=new BlackListView();
			
			person=(PersonView) viewRootMap.get("PERSON_VIEW");
			if(!hasError())
			{
				if(blacklist==true)
				person.setIsBlacklisted(1L);
				person=psa.addPerson(person);
				blv.setBlackListId(null);
				blv.setCreatedBy(getLoggedInUser());
				blv.setCreatedOn(new Date());
				blv.setUpdatedBy(getLoggedInUser());
				blv.setUpdatedOn(new Date());
				blv.setIsDeleted(0L);
				blv.setRecordStatus(1L);
				blv.setRemarks(this.blacklistDesc);
				blv.setPersonId(person.getPersonId());
				psa.addBlackList(blv);
				btnSave.setRendered(false);
				viewRootMap.put("BLACKLISTED", true);
				successMessages.add(getBundle().getString(MessageConstants.CommonMessage.RECORD_SAVED));
				logger.logInfo("save() successfully Completed :");
			}
		} 
		catch (Exception e)
		{
			logger.LogException("save() Crashed :" , e);
		}
	}
	private String getLoggedInUser() {
		String methodName = "getLoggedInUser";
		logger.logInfo(methodName + "|" + " Start");

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		logger.logInfo(methodName + "|" + " Finish");
		return loggedInUser;
	}
	private boolean hasError()
	{
		boolean error=false;
		if(blacklist==true)
		{
			if(blacklistDesc==null || blacklistDesc.trim().length()<=0)
			 {
				errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_BLACKLIST_REASON_REQUIRED));
				error=true;
			 }
		}
		if(blacklistDesc!=null && blacklistDesc.trim().length()>0)
			{
				if(blacklist!=true)
					{
						errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_BLACKLIST_CHECK));
						error= true;
					}
			}
		if(blacklist!=true && (blacklistDesc==null || blacklistDesc.trim().length()<=0))
				{
					errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_BLACKLIST_CHECK_DESC));
					error= true;
				}
		if(blacklistDesc.trim().length()>=100)
			{
				errorMessages.add(getBundle().getString(MessageConstants.CommonMessage.BLACKLIST_DESC_TOO_LONG));
				error= true;
			}
			return error;
			
	}
 public ResourceBundle getBundle()
 {
		 ResourceBundle bundle;
		             bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());
         return bundle;
 }
	public String getErrorMessages()
	{
		  return commUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getSuccessMessages()
	{
		return commUtil.getErrorMessages(successMessages);
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getBlacklistDesc()
	{
		if(viewRootMap.get("BLACKLIST_DESC")!=null)
			blacklistDesc=(String) viewRootMap.get("BLACKLIST_DESC");
		return blacklistDesc;
	}
	public void setBlacklistDesc(String blacklistDesc) {
		this.blacklistDesc = blacklistDesc;
	}
	public Long getTenantId()
	{
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public boolean isBlacklist() 
	{
		if(viewRootMap.get("BLACKLIST")!=null)
			blacklist=(Boolean) viewRootMap.get("BLACKLIST");
		return blacklist;
	}
	public void setBlacklist(boolean blacklist) {
		this.blacklist = blacklist;
	}
	public PersonView getPerson()
	{
		if(viewRootMap.get("PERSON_VIEW")!=null)
			person=(PersonView) viewRootMap.get("PERSON_VIEW");
		return person;
	}
	public void setPerson(PersonView person) {
		this.person = person;
	}
	public HtmlCheckbox getChkBoxBL() {
		return chkBoxBL;
	}
	public void setChkBoxBL(HtmlCheckbox chkBoxBL) {
		this.chkBoxBL = chkBoxBL;
	}
	public HtmlInputTextarea getBLDesc() {
		return BLDesc;
	}
	public void setBLDesc(HtmlInputTextarea desc) {
		BLDesc = desc;
	}
	public boolean isDisabled()
	{
		if(viewRootMap.get("BLACKLISTED")!=null)
		{
			disabled=(Boolean) viewRootMap.get("BLACKLISTED");
		}
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}
	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}
}
