package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ExtendRequestDetailsView;


public class ExtendRequestDetailsTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ExtendRequestDetailsTabBacking.class);

	private ExtendRequestDetailsView requestDetailsTabView = new ExtendRequestDetailsView();
	/*	private String applicationNumber;
	private String applicationStatus;
	private Date applicationDate;
	private String applicationSource;
	private String applicationSourceId;
	private String contractorName;
	private String contractorType;
	private String contractorTypeId;
	private String tenderNumber;
	private String tenderDescription;
	private Date tenderStartDate;
	private Date tenderEndDate;
	private String reasons;*/
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	 @Override 
	 public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(sessionMap.containsKey("FROM_CONSTRUCTION_TENDER"))
			{
				viewMap.put("FROM_CONSTRUCTION_TENDER", sessionMap.get("FROM_CONSTRUCTION_TENDER"));
				sessionMap.remove("FROM_CONSTRUCTION_TENDER");
			}	
//			populateFromView();
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 		if(isPostBack()){
	 		if(!viewMap.containsKey("count"))
	 		{
	 			populateView();
	 			viewMap.put("count", 1);
	 		}
	 		else{
	 			viewMap.remove("count");
	 		}
 		}
 		
 	}

	public Boolean getReadonlyMode(){
		Boolean extendRequestDetailsReadonlyMode = (Boolean)viewMap.get("extendRequestDetailsReadonlyMode");
		if(extendRequestDetailsReadonlyMode==null)
			extendRequestDetailsReadonlyMode = false;
		return extendRequestDetailsReadonlyMode;
	}

	public String getApplicationNumber() {
		String temp = (String)viewMap.get("applicationNumber");
		return temp;
	}

	public void setApplicationNumber(String applicationNumber) {
		if(StringHelper.isNotEmpty(applicationNumber))
			viewMap.put("applicationNumber" , applicationNumber);
	}

	public String getApplicationStatus() {
		String temp = (String)viewMap.get("applicationStatus");
		return temp;
	}

	public void setApplicationStatus(String applicationStatus) {
		if(applicationStatus!=null)
			viewMap.put("applicationStatus" , applicationStatus);
	}

	public String getApplicationDate() {
		String temp = (String)viewMap.get("applicationDate");
		return temp;
	}

	public void setApplicationDate(String applicationDate) {
		if(applicationDate!=null)
			viewMap.put("applicationDate" , applicationDate);
	}

	public String getApplicationSource() {
		String temp = (String)viewMap.get("applicationSource");
		return temp;
	}

	public void setApplicationSource(String applicationSource) {
		if(applicationSource!=null)
			viewMap.put("applicationSource" , applicationSource);
	}

	public String getApplicationSourceId() {
		String temp = (String)viewMap.get("applicationSourceId");
		return temp;
	}

	public void setApplicationSourceId(String applicationSourceId) {
		if(applicationSourceId!=null)
			viewMap.put("applicationSourceId" , applicationSourceId);
	}

	public String getContractorName() {
		String temp = (String)viewMap.get("contractorName");
		return temp;
	}

	public void setContractorName(String contractorName) {
		if(contractorName!=null)
			viewMap.put("contractorName" , contractorName);
	}

	public String getContractorType() {
		String temp = (String)viewMap.get("contractorType");
		return temp;
	}

	public void setContractorType(String contractorType) {
		if(contractorType!=null)
			viewMap.put("contractorType" , contractorType);
	}

	public String getContractorTypeId() {
		String temp = (String)viewMap.get("contractorTypeId");
		return temp;
	}

	public void setContractorTypeId(String contractorTypeId) {
		if(contractorTypeId!=null)
			viewMap.put("contractorTypeId" , contractorTypeId);
	}

	public String getTenderNumber() {
		String temp = (String)viewMap.get("tenderNumber");
		return temp;
	}

	public void setTenderNumber(String tenderNumber) {
		if(tenderNumber!=null)
			viewMap.put("tenderNumber" , tenderNumber);
	}

	public String getTenderType() {
		String temp = (String)viewMap.get("tenderType");
		return temp;
	}

	public void setTenderType(String tenderType) {
		if(tenderType!=null)
			viewMap.put("tenderType" , tenderType);
	}

	public String getTenderTypeId() {
		String temp = (String)viewMap.get("tenderTypeId");
		return temp;
	}

	public void setTenderTypeId(String tenderTypeId) {
		if(tenderTypeId!=null)
			viewMap.put("tenderTypeId" , tenderTypeId);
	}

	
	public String getTenderStartDate() {
		String temp = (String)viewMap.get("tenderStartDate");
		return temp;
	}

	public void setTenderStartDate(String tenderStartDate) {
		if(tenderStartDate!=null)
			viewMap.put("tenderStartDate" , tenderStartDate);
	}

	public String getTenderEndDate() {
		String temp = (String)viewMap.get("tenderEndDate");
		return temp;
	}

	public void setTenderEndDate(String tenderEndDate) {
		if(tenderEndDate!=null)
			viewMap.put("tenderEndDate" , tenderEndDate);
	}

	public Date getTenderEndDateVal() {
		Date endDateVal = (Date)viewMap.get("endDateVal");
		return endDateVal;
	}

	public void setTenderEndDateVal(Date endDateVal) {
		if(endDateVal!=null)
			viewMap.put("endDateVal" , endDateVal);
	}
	
	public String getReasons() {
		String temp = (String)viewMap.get("reasons");
		return temp;
	}

	public void setReasons(String reasons) {
		if(reasons!=null)
			viewMap.put("reasons" , reasons);
	}

	public String getApplicationStatusId() {
		String temp = (String)viewMap.get("applicationStatusId");
		return temp;
	}
	
	public void setApplicationStatusId(String applicationStatusId) {
		if(applicationStatusId!=null)
			viewMap.put("applicationStatusId" , applicationStatusId);
	}
	
	public String getContractorId() {
		String temp = (String)viewMap.get("contractorId");
		return temp;
	}
	
	public void setContractorId(String contractorId) {
		if(contractorId!=null)
			viewMap.put("contractorId" , contractorId);
	}
	
	public Long getRequestId() {
		Long temp = (Long)viewMap.get("requestId");
		return temp;
	}
	
	public void setRequestId(Long requestId) {
		if(requestId!=null)
			viewMap.put("requestId" , requestId);
	}
	
	public String getTenderId() {
		String temp = (String)viewMap.get("tenderId");
		return temp;
	}
	
	public void setTenderId(String tenderId) {
		if(tenderId!=null)
			viewMap.put("tenderId" , tenderId);
	}
	
	
	private ExtendRequestDetailsView populateView(){
		requestDetailsTabView.setApplicationDate(getApplicationDate());
		requestDetailsTabView.setApplicationNumber(getApplicationNumber());
		requestDetailsTabView.setApplicationSource(getApplicationSource());
		requestDetailsTabView.setApplicationSourceId(getApplicationSourceId());
		requestDetailsTabView.setApplicationStatus(getApplicationStatus());
		requestDetailsTabView.setApplicationStatusId(getApplicationStatusId());
		requestDetailsTabView.setContractorId(getContractorId());
		requestDetailsTabView.setContractorName(getContractorName());
		requestDetailsTabView.setContractorType(getContractorType());
		requestDetailsTabView.setContractorTypeId(getContractorTypeId());
		requestDetailsTabView.setReasons(getReasons());
		requestDetailsTabView.setRequestId(getRequestId());
		requestDetailsTabView.setTenderEndDate(getTenderEndDate());
		requestDetailsTabView.setTenderId(getTenderId());
		requestDetailsTabView.setTenderNumber(getTenderNumber());
		requestDetailsTabView.setTenderStartDate(getTenderStartDate());
		requestDetailsTabView.setTenderType(getTenderType());
		requestDetailsTabView.setTenderTypeId(getTenderTypeId());
		
		viewMap.put("requestDetailsTabView" , requestDetailsTabView);
		return requestDetailsTabView;
	}
	
	private void populateFromView(){
		requestDetailsTabView = (ExtendRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EXTEND_REQUEST_DETAILS_TAB_VIEW);
		if(requestDetailsTabView!=null){
			setApplicationDate(requestDetailsTabView.getApplicationDate());
			setApplicationNumber(requestDetailsTabView.getApplicationNumber());
			setApplicationSource(requestDetailsTabView.getApplicationSource());
			setApplicationSourceId(requestDetailsTabView.getApplicationSourceId());
			setApplicationStatus(requestDetailsTabView.getApplicationStatus());
			setApplicationStatusId(requestDetailsTabView.getApplicationStatusId());
			setContractorId(requestDetailsTabView.getContractorId());
			setContractorName(requestDetailsTabView.getContractorName());
			setContractorType(requestDetailsTabView.getContractorType());
			setContractorTypeId(requestDetailsTabView.getContractorTypeId());
			setReasons(requestDetailsTabView.getReasons());
			setRequestId(requestDetailsTabView.getRequestId());
			setTenderEndDate(requestDetailsTabView.getTenderEndDate());
			setTenderId(requestDetailsTabView.getTenderId());
			setTenderNumber(requestDetailsTabView.getTenderNumber());
			setTenderStartDate(requestDetailsTabView.getTenderStartDate());
			setTenderType(requestDetailsTabView.getTenderType());
			setTenderTypeId(requestDetailsTabView.getTenderTypeId());
		}
	}
	
	public String onSearchTender() 
	{
		String path = null;
		if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
		openPopup("showSearchConstructionTenderPopUp();");
		else
		openPopup("showSearchTenderPopUp();");
		
		
		
		
		return path;
	}
	
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}

	public ExtendRequestDetailsView getRequestDetailsTabView() {
		return requestDetailsTabView;
	}

	public void setRequestDetailsTabView(
			ExtendRequestDetailsView requestDetailsTabView) {
		this.requestDetailsTabView = requestDetailsTabView;
	}

}

