package com.avanza.pims.web.backingbeans;

import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.BlackListView;

public class BlacklistSearchBackingBean extends AbstractController {
	
	public String tenantFirstName;
	public String contractorFirstName;
	public String addedBy;
	public List<BlackListView> blaclistViews;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private HtmlDataTable dataTable;
	
	private static Logger logger = Logger.getLogger(BlacklistSearchBackingBean.class);
	
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		
		try {
			
			blaclistViews = psa.searchBlacklist(new HashMap());
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String delete() {
		
		try {
			BlackListView entryToBeRemoved = (BlackListView) dataTable.getRowData();
			psa.deleteTenantFromBlackList(entryToBeRemoved.getBlackListId());
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	
	public String search() {
		
		HashMap blacklistMap = new HashMap();
		
		try {
			
			if (tenantFirstName != null && tenantFirstName.length()>0)
				blacklistMap.put("tenantFirstName", tenantFirstName);
			
			if (contractorFirstName != null && contractorFirstName.length()>0)
				blacklistMap.put("contractorFirstName", contractorFirstName);
			
			if (addedBy != null && addedBy.length()>0)
				blacklistMap.put("addedBy", addedBy);
			
			blaclistViews = psa.searchBlacklist(blacklistMap);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String getTenantFirstName() {
		return tenantFirstName;
	}

	public void setTenantFirstName(String tenantFirstName) {
		this.tenantFirstName = tenantFirstName;
	}

	public String getContractorFirstName() {
		return contractorFirstName;
	}

	public void setContractorFirstName(String contractorFirstName) {
		this.contractorFirstName = contractorFirstName;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public String getDateFormat()
	{
		String methodName="getDateFormat";
    	logger.logInfo(methodName+"|"+" Start");
		
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		
		logger.logInfo(methodName+"|"+" Finish");
		return dateFormat;
	}
	
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);

		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);

		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");


		
		return isEnglishLocale;
	}

	public List<BlackListView> getBlaclistViews() {
		return blaclistViews;
	}

	public void setBlaclistViews(List<BlackListView> blaclistViews) {
		this.blaclistViews = blaclistViews;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

}
