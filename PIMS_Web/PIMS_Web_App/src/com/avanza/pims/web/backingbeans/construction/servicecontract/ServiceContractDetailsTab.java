package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorSearch;
import com.avanza.pims.web.backingbeans.construction.tendermanagement.TenderSearchBacking;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BankView;



public class ServiceContractDetailsTab extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ServiceContractDetailsTab.class);
     public interface Page_Mode {
		
    	public static final String EDIT="PAGE_MODE_EDIT";
 		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
 		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
 		public static final String COMPLETE="PAGE_MODE_COMPLETE";
 		public static final String POPUP="PAGE_MODE_POPUP";
 		public static final String VIEW="PAGE_MODE_VIEW";
 		public static final String ACTIVE="PAGE_MODE_ACTIVE";
		
	}
    private HtmlGraphicImage imgSearchTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgRemoveTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchContractor=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewContractor=new HtmlGraphicImage();
    private HtmlGraphicImage imgRemoveContractor=new HtmlGraphicImage();
    private List<SelectItem> banks = new ArrayList<SelectItem>();
    
    private Date contractStartDate;
    org.richfaces.component.html.HtmlCalendar startDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    
    private Date contractEndDate;
    org.richfaces.component.html.HtmlCalendar endDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    
    
    
    private String contractNo;
	private HtmlInputText txtContractNo;
	
	private String contractStatus;
	private HtmlInputText txtContractStatus;
	
	private String contractType;
	private HtmlInputText txtContractType;
	
	private HtmlInputText txtTenderNum=new HtmlInputText();
	private String tenderNum;
	
	private HtmlInputText txtTenderDesc=new HtmlInputText();
	private String tenderDesc;
	
	private HtmlInputText txtContractorNum=new HtmlInputText();
	private String contractorNum;
	
	private HtmlInputText txtContractorName=new HtmlInputText();
	private String contractorName;
	
	private HtmlInputText txtTotalAmount=new HtmlInputText();
	private String totalAmount;
	
	private String selectOneBank;
	private HtmlSelectOneMenu cmbBank=new HtmlSelectOneMenu();
	private HtmlInputText txtGuaranteePercentage=new HtmlInputText();
	private String guaranteePercentage;
	
	private HtmlInputText txtGuaranteeAmont=new HtmlInputText();
	private String guaranteeAmont;
	protected String contractorScreenQueryStringPopUpMode;
	protected String contractorScreenQueryStringViewMode;
	public String getContractorScreenQueryStringPopUpMode() {
		
		return ContractorSearch.Keys.MODE_SELECT_ONE_POPUP;
	}

	

	public String getContractorScreenQueryStringViewMode() {
		return ContractorSearch.Keys.PAGE_MODE;
	}

	
	private void loadBanksList(){
		try{
			List<BankView> bankViewList = new ArrayList();
			banks = new ArrayList();
			bankViewList = new  UtilityServiceAgent().getBanksList();
			for(int index=0;index<bankViewList.size();index++){
				
				banks.add(new SelectItem(bankViewList.get(index).getBankId().toString(),
						                 getIsEnglishLocale()?bankViewList.get(index).getBankEn():bankViewList.get(index).getBankAr()));
			}
			viewRootMap.put(WebConstants.BANKS_LIST, banks);
			
		}
		catch(Exception e){
			logger.LogException("loadBanksList|Error Occured", e);
		}
	}
	
	private String pageMode;
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap=context.getViewRoot().getAttributes();
	 @Override 
	 public void init() 
     {
    	 super.init();
    	 
			sessionMap=context.getExternalContext().getSessionMap();
			this.setContractType(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_TYPE).toString());
			//String requestNumber=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_NUMBER).toString();
    	
	 }
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 		if(!isPostBack())
 		{
 			if(!viewRootMap.containsKey(WebConstants.BANKS_LIST))
 				loadBanksList();
 		}
 	}
 	public void enableDisableControls(String pageMode)
 	{
 		
 		if(pageMode.equals(Page_Mode.ACTIVE) || pageMode.equals(Page_Mode.COMPLETE) 
 				|| pageMode.equals(Page_Mode.LEGAL_REVIEW_REQUIRED) || pageMode.equals(Page_Mode.VIEW))
 		{
			imgSearchTender.setRendered(false);
			imgSearchContractor.setRendered(false);
			txtTotalAmount.setDisabled(true);
			txtGuaranteePercentage.setDisabled(true);
			startDateCalendar.setDisabled(true);
			endDateCalendar.setDisabled(true);
			cmbBank.setDisabled(true);
 		}
 		if(pageMode.equals(Page_Mode.EDIT) || pageMode.equals(Page_Mode.APPROVAL_REQUIRED))
 		{
			imgSearchTender.setRendered(false);
 		}
 		
 	}
 	
 	public LocaleInfo getLocaleInfo()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		
	    return localeInfo;
		
	}
 	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
 	public Boolean getIsArabicLocale()
	{
    	
		return CommonUtil.getIsArabicLocale();
	}
 	public Boolean getIsEnglishLocale()
	{
    	
		return CommonUtil.getIsEnglishLocale();
	}
		public String getDateFormat()
		{
	    	
			return CommonUtil.getDateFormat();
		}
		public TimeZone getTimeZone()
		{
			 return CommonUtil.getTimeZone();
			
		}

		public HtmlGraphicImage getImgSearchTender() {
			return imgSearchTender;
		}

		public void setImgSearchTender(HtmlGraphicImage imgSearchTender) {
			this.imgSearchTender = imgSearchTender;
		}

		public HtmlGraphicImage getImgViewTender() {
			return imgViewTender;
		}

		public void setImgViewTender(HtmlGraphicImage imgViewTender) {
			this.imgViewTender = imgViewTender;
		}

		public HtmlGraphicImage getImgRemoveTender() {
			return imgRemoveTender;
		}

		public void setImgRemoveTender(HtmlGraphicImage imgRemoveTender) {
			this.imgRemoveTender = imgRemoveTender;
		}

		public HtmlGraphicImage getImgSearchContractor() {
			return imgSearchContractor;
		}

		public void setImgSearchContractor(HtmlGraphicImage imgSearchContractor) {
			this.imgSearchContractor = imgSearchContractor;
		}

		public HtmlGraphicImage getImgViewContractor() {
			return imgViewContractor;
		}

		public void setImgViewContractor(HtmlGraphicImage imgViewContractor) {
			this.imgViewContractor = imgViewContractor;
		}

		public HtmlGraphicImage getImgRemoveContractor() {
			return imgRemoveContractor;
		}

		public void setImgRemoveContractor(HtmlGraphicImage imgRemoveContractor) {
			this.imgRemoveContractor = imgRemoveContractor;
		}

		public Date getContractStartDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_START_DATE))
				contractStartDate= (Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
			return contractStartDate;
		}

		public void setContractStartDate(Date contractStartDate) {
			this.contractStartDate = contractStartDate;
			if(this.contractStartDate    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_START_DATE, this.contractStartDate );
		}

		public org.richfaces.component.html.HtmlCalendar getStartDateCalendar() {
			return startDateCalendar;
		}

		public void setStartDateCalendar(
				org.richfaces.component.html.HtmlCalendar startDateCalendar) {
			this.startDateCalendar = startDateCalendar;
		}

		public Date getContractEndDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_END_DATE))
				contractEndDate=(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);

			return contractEndDate;
		}

		public void setContractEndDate(Date contractEndDate) {
			this.contractEndDate = contractEndDate;
			if(this.contractEndDate    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_END_DATE, this.contractEndDate );
		}

		public org.richfaces.component.html.HtmlCalendar getEndDateCalendar() {
			return endDateCalendar;
		}

		public void setEndDateCalendar(
				org.richfaces.component.html.HtmlCalendar endDateCalendar) {
			this.endDateCalendar = endDateCalendar;
		}

		public String getContractNo() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_NUM))
				contractNo=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_NUM).toString();

			return contractNo;
		}

		public void setContractNo(String contractNo) {
			this.contractNo = contractNo;
			if(this.contractNo    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_NUM, this.contractNo );
		}

		public HtmlInputText getTxtContractNo() {
			return txtContractNo;
		}

		public void setTxtContractNo(HtmlInputText txtContractNo) {
			this.txtContractNo = txtContractNo;
		}

		public String getContractStatus() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_STATUS))
				contractStatus=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_STATUS).toString();

			return contractStatus;
		}

		public void setContractStatus(String contractStatus) {
			this.contractStatus = contractStatus;
			if(this.contractStatus    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_STATUS, this.contractStatus );
		}

		public HtmlInputText getTxtContractStatus() {
			return txtContractStatus;
		}

		public void setTxtContractStatus(HtmlInputText txtContractStatus) {
			this.txtContractStatus = txtContractStatus;
		}

		public String getContractType() {
			 if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_TYPE))
				contractType=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_TYPE).toString();

			return contractType;
		}

		public void setContractType(String contractType) {
			this.contractType = contractType;
			if(this.contractType    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE, this.contractType ); 
		}

		public HtmlInputText getTxtContractType() {
			return txtContractType;
		}

		public void setTxtContractType(HtmlInputText txtContractType) {
			this.txtContractType = txtContractType;
		}

		public HtmlInputText getTxtTenderDesc() {
			return txtTenderDesc;
		}

		public void setTxtTenderDesc(HtmlInputText txtTenderDesc) {
			this.txtTenderDesc = txtTenderDesc;
		}

		public String getTenderDesc() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.TENDER_DESC))
				tenderDesc=viewRootMap.get(WebConstants.ContractDetailsTab.TENDER_DESC).toString();

			return tenderDesc;
		}

		public void setTenderDesc(String tenderDesc) {
			this.tenderDesc = tenderDesc;
			if(this.tenderDesc    !=null)
							viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_DESC, this.tenderDesc );
		}

		public HtmlInputText getTxtContractorNum() {
			return txtContractorNum;
		}

		public void setTxtContractorNum(HtmlInputText txtContractorNum) {
			this.txtContractorNum = txtContractorNum;
		}

		public String getContractorNum() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACTOR_NUM))
				contractorNum=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACTOR_NUM).toString();
			return contractorNum;
		}

		public void setContractorNum(String contractorNum) {
			this.contractorNum = contractorNum;
			if(this.contractorNum   !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM, this.contractorNum);
		}

		public HtmlInputText getTxtContractorName() {
			return txtContractorName;
		}

		public void setTxtContractorName(HtmlInputText txtContractorName) {
			this.txtContractorName = txtContractorName;
		}

		public String getContractorName() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACTOR_NAME))
				contractorName=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACTOR_NAME).toString();
			return contractorName;
		}

		public void setContractorName(String contractorName) {
			this.contractorName = contractorName;
			if(this.contractorName    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME, this.contractorName );
		}

		public String getSelectOneBank() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.BANK_ID))
				selectOneBank=viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).toString();
			return selectOneBank;
		}

		public void setSelectOneBank(String selectOneBank) {
			this.selectOneBank = selectOneBank;
			if(this.selectOneBank   !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.BANK_ID, this.selectOneBank);
		}

		public HtmlInputText getTxtGuaranteePercentage() {
			return txtGuaranteePercentage;
		}

		public void setTxtGuaranteePercentage(HtmlInputText txtGuaranteePercentage) {
			this.txtGuaranteePercentage = txtGuaranteePercentage;
		}

		public String getGuaranteePercentage() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT))
				guaranteePercentage=viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString();
			return guaranteePercentage;
		}

		public void setGuaranteePercentage(String guaranteePercentage) {
			this.guaranteePercentage = guaranteePercentage;
			if(this.guaranteePercentage    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT, this.guaranteePercentage );
		}

		public HtmlInputText getTxtGuaranteeAmont() {
			return txtGuaranteeAmont;
		}

		public void setTxtGuaranteeAmont(HtmlInputText txtGuaranteeAmont) {
			this.txtGuaranteeAmont = txtGuaranteeAmont;
		}

		public String getGuaranteeAmont() {
			
			Double contractAmount = null;
			Double percent = null;
			Double guarantee = null ;
			if(this.getTotalAmount()!=null && this.getTotalAmount().trim().length()>0 && this.getGuaranteePercentage()!=null && this.getGuaranteePercentage().trim().length()>0)
			{
			    contractAmount = new Double(this.getTotalAmount().toString());
				percent = new Double(this.getGuaranteePercentage().toString());
				guarantee=percent*contractAmount/100;
				guaranteeAmont = guarantee.toString();
			}
			return guaranteeAmont;
		}

		public void setGuaranteeAmont(String guaranteeAmont) {
			this.guaranteeAmont = guaranteeAmont;
			if(this.guaranteeAmont    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.BANK_GUARANTEE_AMOUNT, this.guaranteeAmont );
		}

		public HtmlInputText getTxtTenderNum() {
			return txtTenderNum;
		}

		public void setTxtTenderNum(HtmlInputText txtTenderNum) {
			this.txtTenderNum = txtTenderNum;
		}

		public String getTenderNum() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.TENDER_NUM))
				tenderNum=viewRootMap.get(WebConstants.ContractDetailsTab.TENDER_NUM).toString();
			return tenderNum;
		}

		public void setTenderNum(String tenderNum) {
			this.tenderNum = tenderNum;
			if(this.tenderNum    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM , this.tenderNum );
		}

		public HtmlInputText getTxtTotalAmount() {
			return txtTotalAmount;
		}

		public void setTxtTotalAmount(HtmlInputText txtTotalAmount) {
			this.txtTotalAmount = txtTotalAmount;
		}

		public String getTotalAmount() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT))
				totalAmount=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString();
			return totalAmount;
		}

		public void setTotalAmount(String totalAmount) {
			this.totalAmount = totalAmount;
			if(this.totalAmount    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT, this.totalAmount );
		}

		public List<SelectItem> getBanks() {
			if(viewRootMap.containsKey(WebConstants.BANKS_LIST))
				banks = (ArrayList<SelectItem>) viewRootMap.get(WebConstants.BANKS_LIST);
			else
				loadBanksList();
			return banks;
		}

		public void setBanks(List<SelectItem> banks) {
			this.banks = banks;
		}



		public HtmlSelectOneMenu getCmbBank() {
			return cmbBank;
		}



		public void setCmbBank(HtmlSelectOneMenu cmbBank) {
			this.cmbBank = cmbBank;
		}

		public String getTenderSearchKey_NotBindedWithContract() {
			return TenderSearchBacking.Keys.NOT_BINDED_WITH_CONTRACT;
		}

		

		public String getTenderSearchKey_tenderType() {
			return TenderSearchBacking.Keys.TENDER_TYPE;
		}

		

		public String getTenderSearchKey_WinnerContractorPresent() {
			return TenderSearchBacking.Keys.WINNER_CONTRACTOR_PRESENT;
		}




	
}
