package com.avanza.pims.web.backingbeans.Investment.Opportunities;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import oracle.tip.pc.services.translation.importer.ccb.Usage;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.pimsstrategicplan.PIMSStrategicPlanBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Portfolio;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.Investment.Opportunities.StrategicPlanSearch.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.investment.InvestmentOpportunityService;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RelatedStudiesView;
import com.avanza.pims.ws.vo.StrategicPlanPortfolioView;
import com.avanza.pims.ws.vo.StrategicPlanStudyView;
import com.avanza.pims.ws.vo.StrategicPlanView;
import com.avanza.pims.ws.vo.StudyDetailView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class StrategicPlanDetailsBacking extends AbstractController {

	private transient Logger logger = Logger.getLogger(StrategicPlanDetailsBacking.class);	
	private HtmlDataTable dataTable = new HtmlDataTable();
	private final String TAB_PLAN_DETAILS = "strategicPlanDetailsTab";
	private final String TAB_PORTFOLIOS = "portfoliosTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";	
	private final String SEND_FOR_APPROVAL = "SEND_FOR_APPROVAL";	
	
	private final String noteOwner = WebConstants.StrategicPlan.STRATEGIC_PLAN;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_STRATEGIC_PLAN;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_STRATEGIC_PLAN;
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	// attributes used for a data grid
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	
	private HtmlInputText htmlInputPlanNameEn = new HtmlInputText();
	private HtmlInputText htmlInputPlanNameAr = new HtmlInputText();	
	private HtmlInputTextarea htmlInputPlanDescription = new HtmlInputTextarea();
	private HtmlCalendar htmlCalendarCreationDate = new HtmlCalendar();
	private HtmlCalendar htmlCalendarEndDate = new HtmlCalendar();
	private HtmlCommandButton addPortfolioButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton sendForApprovalButton = new HtmlCommandButton();
	private HtmlCommandButton approveButton = new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	private HtmlCommandButton closeButton = new HtmlCommandButton();
	private HtmlCommandButton activateButton = new HtmlCommandButton();
	
	private String readonlyStyleClass = "";
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_DRAFT ="PAGE_MODE_DRAFT";
	private final String PAGE_MODE_DRAFT_DONE ="PAGE_MODE_DRAFT_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private final String PAGE_MODE_CLOSE ="PAGE_MODE_CLOSE";
	private final String PAGE_MODE_CLOSE_DONE ="PAGE_MODE_CLOSE_DONE";
	private final String PAGE_MODE_APPROVAL ="PAGE_MODE_APPROVAL";
	private final String PAGE_MODE_APPROVAL_DONE ="PAGE_MODE_APPROVAL_DONE";
	private final String PAGE_MODE_REVIEW ="PAGE_MODE_REVIEW";
	private final String PAGE_MODE_REVIEW_DONE ="PAGE_MODE_REVIEW_DONE";
	private String pageMode="default";
	
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_NEW;	
	private String heading="";	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private List<StrategicPlanStudyView> strategicPlanStudyViewList = new ArrayList<StrategicPlanStudyView>(0);
	private HtmlDataTable strategicPlanStudyDataTable = new HtmlDataTable();
	
	public static void main(String args[] )
	{
		try 
		{
			ApplicationLoader.load("D:\\AvanzaCVS\\AMAF\\EDGE\\Implementation artifacts\\Application\\PIMS_Web_App\\resources\\av-conf.xml");			
			Thread.sleep(5000);
			InvestmentOpportunityService ios = new InvestmentOpportunityService();			
			StrategicPlanView view = new StrategicPlanView();								
			HashMap<String, Object> map = new HashMap<String, Object>();
			List<StrategicPlanView> strategicPlanViewList =  ios.getStrategicPlansByCriteria(map);			
			System.out.println("Plan view size "+ strategicPlanViewList.size());
		}
		catch (InterruptedException e) 
		{
			
			e.printStackTrace();
		}
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);
		if (recordSize == null)
			recordSize = 0;

		return recordSize;
	}
	public Boolean getShowSaveButton()
	{
		pageMode = (String) viewMap.get(PAGE_MODE);
		if(pageMode==null)
			pageMode = PAGE_MODE_VIEW;
		
		if(pageMode.equals(PAGE_MODE_NEW) || pageMode.equals(PAGE_MODE_DRAFT))
		{
			return true;
		}
		
		return false;
	}
	public Boolean getShowSendForApprovalButton()
	{
		pageMode = (String) viewMap.get(PAGE_MODE);
		boolean visible=false;
		if(pageMode==null)
			pageMode = PAGE_MODE_VIEW;
		
		if(pageMode.equals(PAGE_MODE_NEW) || pageMode.equals(PAGE_MODE_DRAFT))
		{
			if(viewMap.get(SEND_FOR_APPROVAL)!=null)
			{
				visible=(Boolean) viewMap.get(SEND_FOR_APPROVAL);
				return true;
			}
		}
		
		return false;
	}
	public Boolean getCanAddPortfolio()
	{
		Boolean canAddPortfolio = (Boolean)viewMap.get("canAddPortfolio");
		if(canAddPortfolio==null)
			canAddPortfolio = false;
		return canAddPortfolio;
	}
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}
	public void deletePortfolio(ActionEvent evt)
	{
		try
		{
			if(dataTable.isRowAvailable())
			{
				StrategicPlanPortfolioView strategicPlanPortfolioView = (StrategicPlanPortfolioView)  dataTable.getRowData();
				int index = dataTable.getRowIndex();
				
				if(strategicPlanPortfolioView.getStrategicPlanPortfolioId() != null)
				{
					strategicPlanPortfolioView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					strategicPlanPortfolioView.setUpdatedBy(CommonUtil.getLoggedInUser());
					strategicPlanPortfolioView.setUpdatedOn(new Date());
					List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList =  getDeletedStrategicPlanPortfolios();
					strategicPlanPortfolioViewList.add(strategicPlanPortfolioView);
					setDeletedStrategicPlanPortfolios(strategicPlanPortfolioViewList);
				}				
				List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList= getStrategicPlanView().getStrategicPlanPortfolioViewList();
				strategicPlanPortfolioViewList.remove(index);	
				
				if(strategicPlanPortfolioViewList != null)
					 viewMap.put(Keys.RECORD_SIZE,strategicPlanPortfolioViewList.size());
				else
					viewMap.put(Keys.RECORD_SIZE,0);
				
				infoMessage=CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PORTFOLIO_DELETED_SUCCESS);
			}
		}
		catch (Exception exception) 
		{
			logger.LogException("deletePortfolio() crashed ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PORTFOLIO_DELETED_FAIL));
		}
		
	}
	//TODO
	@SuppressWarnings("unchecked")
	public void showPortfolioPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("showPortfolioPopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showPortfolioPopup();";
	        List<Long> alreadySelectedPortfolioList = new ArrayList<Long>();
	        sessionMap.put(  WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY,   WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP  );
	        StrategicPlanView strategicPlanView = getStrategicPlanView();
	        List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList = strategicPlanView.getStrategicPlanPortfolioViewList();
	        if(strategicPlanPortfolioViewList != null && strategicPlanPortfolioViewList.size() > 0)
	        {
	        	 for(StrategicPlanPortfolioView strategicPlanPortfolioView: strategicPlanPortfolioViewList)
	 	        {
	 	        	alreadySelectedPortfolioList.add(strategicPlanPortfolioView.getPortfolioId());
	 	        }
	        }	       
	        if(alreadySelectedPortfolioList.size() >0)
	        	sessionMap.put( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST , alreadySelectedPortfolioList );
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("showPortfolioPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("showPortfolioPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public Boolean validatedForReviewStrategicPlan() 
    {
	    logger.logInfo("validatedForReviewStrategicPlan() started...");
	    Boolean validated = true;
	    try
		{
	    	StrategicPlanView strategicPlanView = getStrategicPlanView();
	    	errorMessages.clear();	 
	    	
	    	String planReviewRemarks = strategicPlanView.getStrategicPlanReviewRemarks();
	    	if(planReviewRemarks== null || StringHelper.isEmpty(planReviewRemarks))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.REVIEW_REMARKS_REQUIRED));
	        }
	    	
	    	Date planReviewDate = strategicPlanView.getStrategicPlanReviewDate();
	    	if(planReviewDate== null || StringHelper.isEmpty(planReviewDate.toString()))
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.REVIEW_DATE_REQUIRED));
	        }
			logger.logInfo("validatedForReviewStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForReviewStrategicPlan() crashed ", exception);
		}
		return validated;
	}
	@SuppressWarnings("unchecked")
	public Boolean validatedForCloseStrategicPlan() 
    {
	    logger.logInfo("validatedForCloseStrategicPlan() started...");
	    Boolean validated = true;
	    try
		{
	    	StrategicPlanView strategicPlanView = getStrategicPlanView();
	    	errorMessages.clear();	 
	    	
	    	String planClosingRemarks = strategicPlanView.getStrategicPlanClosingRemarks();
	    	if(planClosingRemarks== null || StringHelper.isEmpty(planClosingRemarks))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.CLOSING_REMARKS_REQUIRED));
	        }
	    	
	    	Date planClosingDate = strategicPlanView.getStrategicPlanClosingDate();
	    	if(planClosingDate== null || StringHelper.isEmpty(planClosingDate.toString()))
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.CLOSING_DATE_REQUIRED));
	        }
			logger.logInfo("validatedForCloseStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForCloseStrategicPlan() crashed ", exception);
		}
		return validated;
	}
	@SuppressWarnings("unchecked")
	public Boolean validatedForApproveStrategicPlan() 
    {
	    logger.logInfo("validatedForApproveStrategicPlan() started...");
	    Boolean validated = true;
	    try
		{
	    	StrategicPlanView strategicPlanView = getStrategicPlanView();
	    	errorMessages.clear();	 
	    	
	    	String planApproveRejectRemarks = strategicPlanView.getStrategicPlanApproveRejectRemarks();
	    	if(planApproveRejectRemarks== null || StringHelper.isEmpty(planApproveRejectRemarks))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.ACTION_REMARKS_REQUIRED));
	        }
	    	
	    	Date planApproveRejectDate = strategicPlanView.getStrategicPlanApproveRejectDate();
	    	if(planApproveRejectDate== null || StringHelper.isEmpty(planApproveRejectDate.toString()))
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.ACTION_DATE_REQUIRED));
	        }
			logger.logInfo("validatedForApproveStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForApproveStrategicPlan() crashed ", exception);
		}
		return validated;
	}
	@SuppressWarnings("unchecked")
	public Boolean validatedForSendStrategicPlanForApproval() 
    {
	    logger.logInfo("validatedForSendStrategicPlanForApproval() started...");
	    Boolean validated = true;
	    try
		{
	    	StrategicPlanView strategicPlanView = getStrategicPlanView();
	    	List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList = strategicPlanView.getStrategicPlanPortfolioViewList();
	    	
	    	
	    	if(strategicPlanPortfolioViewList==null || strategicPlanPortfolioViewList.size() == 0)
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.ACTION_ATLEAST_ONE_PORTFOLIO_REQUIRED));
	        	tabPanel.setSelectedTab(TAB_PORTFOLIOS);
	        }	    	
	    	
			logger.logInfo("validatedForSendStrategicPlanForApproval() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForSendStrategicPlanForApproval() crashed ", exception);
		}
		return validated;
	}
    @SuppressWarnings("unchecked")
	public Boolean validatedForNewStrategicPlan() 
    {
	    logger.logInfo("validatedForNewStrategicPlan() started...");
	    Boolean validated = true;
	    try
		{
	    	StrategicPlanView strategicPlanView = getStrategicPlanView();	    	    
	    		   
	    	String planNameEn = strategicPlanView.getStrategicPlanNameEn();
	    	if(StringHelper.isEmpty(planNameEn) || planNameEn.trim().length()<=0)
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.PLAN_NAME_EN_REQUIRED));
	        }
	    	
	    	String planNameAr = strategicPlanView.getStrategicPlanNameAr();
	    	if(StringHelper.isEmpty(planNameAr)|| planNameAr.trim().length()<=0)
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.PLAN_NAME_AR_REQUIRED));
	        }	    	
	    	
	    	Date creationDate = strategicPlanView.getStrategicPlanCreationDate();
	    	if(creationDate==null)
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.CREATION_DATE_REQUIRED));
	        }
	    	Date endDate = strategicPlanView.getStrategicPlanEndDate();
	    	if(endDate==null)
	        {
	    		validated = false;
	    		errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.END_DATE_REQUIRED));
	        }
	    	
	    	if(!validated)
	    		tabPanel.setSelectedTab(TAB_PLAN_DETAILS);
	    	
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		if(validated)
	    			tabPanel.setSelectedTab(TAB_ATTACHMENTS);
				validated = false;
	    	}

			logger.logInfo("validatedForNewStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForNewStrategicPlan() crashed ", exception);
		}
		return validated;
	}
	    
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			Long strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
			if(strategicPlanId!=null)
			{
				InvestmentOpportunityService ios = new  InvestmentOpportunityService();				
				CommonUtil.loadAttachmentsAndComments(strategicPlanId + "");
				StrategicPlanView strategicPlanView = ios.getStrategicPlanId(strategicPlanId);	
				
				pageMode = (String) viewMap.get(PAGE_MODE);
				if(pageMode==null)
					pageMode = PAGE_MODE_VIEW;
				
				if(pageMode.equals(PAGE_MODE_APPROVAL))
				{
					strategicPlanView.setStrategicPlanApproveRejectDate(new Date());
				}
				else if(pageMode.equals(PAGE_MODE_CLOSE))
				{
					strategicPlanView.setStrategicPlanClosingDate(new Date());
				}
				else if(pageMode.equals(PAGE_MODE_REVIEW))
				{
					strategicPlanView.setStrategicPlanReviewDate(new Date());
				}
				setStrategicPlanView(strategicPlanView);
			}
			
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	public String getStrategicPlanViewStatus()
	{
		String status = "";
		
		StrategicPlanView strategicPlanView = getStrategicPlanView();
		if(strategicPlanView != null && strategicPlanView.getStatusId()!=null)
			status = strategicPlanView.getStatusId().toString();
		
		return status;
	}
	public void setStrategicPlanViewStatus(String status)
	{
		StrategicPlanView strategicPlanView = getStrategicPlanView();
		if(strategicPlanView != null )
			strategicPlanView.setStatusId(Long.parseLong(status));
	}	
	public List<StrategicPlanPortfolioView> getDeletedStrategicPlanPortfolios()
	{
		List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList = null;
		if(viewMap.containsKey(WebConstants.StrategicPlan.DELETED_STRATEGIC_PLAN_PORTFOLIOS))
			strategicPlanPortfolioViewList = (List<StrategicPlanPortfolioView>) viewMap.get(WebConstants.StrategicPlan.DELETED_STRATEGIC_PLAN_PORTFOLIOS);
		else
			strategicPlanPortfolioViewList = new ArrayList<StrategicPlanPortfolioView>();
		
		return strategicPlanPortfolioViewList;
	}
	public void setDeletedStrategicPlanPortfolios( List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList )
	{
		viewMap.put(WebConstants.StrategicPlan.DELETED_STRATEGIC_PLAN_PORTFOLIOS, strategicPlanPortfolioViewList );
	}
	public StrategicPlanView getStrategicPlanView()
	{
		StrategicPlanView strategicPlanView = null;
		if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW))
			strategicPlanView = (StrategicPlanView) viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW);
		else
		{
			strategicPlanView = new StrategicPlanView();	
			UtilityServiceAgent usa = new UtilityServiceAgent();
			DomainDataView dd;
			try 
			{
				dd = usa.getDomainDataByValue(WebConstants.StrategicPlansStatus.STRATEGIC_PLAN_STATUS, WebConstants.StrategicPlansStatus.STRATEGIC_PLAN_STATUS_NEW);
				strategicPlanView.setStatusId(dd.getDomainDataId());
				strategicPlanView.setStatusEn(dd.getDataDescEn());
				strategicPlanView.setStatusAr(dd.getDataDescAr());
			}
			catch (PimsBusinessException exception) 
			{
				
				logger.LogException("getStrategicPlanView crashed: ", exception);
			}
			
			setStrategicPlanView(strategicPlanView);
		}
		
		return strategicPlanView;
	}
	public void setStrategicPlanView(StrategicPlanView strategicPlanView)
	{
		viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW, strategicPlanView);
	}
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		
		try
		{
			if(!isPostBack())
			{
				if(sessionMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID))
				{
					Long strategcPlanId = (Long) sessionMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
					viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, strategcPlanId);					
					sessionMap.remove(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
				}
				if(sessionMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA))
				{					
					viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA, sessionMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA));					
					sessionMap.remove(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA);
				}
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				setMode();
				setValues();
			}
			else
			
			{
				try
				{
					if(sessionMap.containsKey(WebConstants.Portfolio.PORTFOLIO_VIEW_LIST))
					{
						String loggedInUser = CommonUtil.getLoggedInUser();
						StrategicPlanView strategicPlanView = getStrategicPlanView();
						List<StrategicPlanPortfolioView> strategicPlanPortfolioViewList = strategicPlanView.getStrategicPlanPortfolioViewList();
						if(strategicPlanPortfolioViewList == null)
							strategicPlanPortfolioViewList = new ArrayList<StrategicPlanPortfolioView>();
						List<PortfolioView> selectedPortfolioList = (List<PortfolioView>) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST );
						for(PortfolioView portfolioView : selectedPortfolioList )
						{
							StrategicPlanPortfolioView strategicPlanPortfolioView = new StrategicPlanPortfolioView();
							strategicPlanPortfolioView.setCreatedBy(loggedInUser);
							strategicPlanPortfolioView.setUpdatedBy(loggedInUser);
							strategicPlanPortfolioView.setUpdatedOn(new Date());
							strategicPlanPortfolioView.setCreatedOn(new Date());
							strategicPlanPortfolioView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
							strategicPlanPortfolioView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
							strategicPlanPortfolioView.setPortfolio(portfolioView);
							strategicPlanPortfolioView.setPortfolioId(portfolioView.getPortfolioId());						
							strategicPlanPortfolioViewList.add(strategicPlanPortfolioView);
						}
						strategicPlanView.setStrategicPlanPortfolioViewList(strategicPlanPortfolioViewList);
						sessionMap.remove( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST );
						
						if(strategicPlanPortfolioViewList != null)
							 viewMap.put(Keys.RECORD_SIZE,strategicPlanPortfolioViewList.size());
						else
							viewMap.put(Keys.RECORD_SIZE,0);
						
						infoMessage=CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PORTFOLIO_ADD_SUCCESS);
					}					
					
				}
				catch(Exception exp)
				{
					logger.LogException("Failed to add portfolios in the strategic plan crashed ", exp);
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PORTFOLIO_ADD_FAIL));
				}
					
			}
			updateValuesFromMaps();
			pageMode = (String) viewMap.get(PAGE_MODE);
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
	
	public Boolean getShowApprovalStrategicPlanPanel()
	{	
		if (pageMode.equalsIgnoreCase(PAGE_MODE_APPROVAL))
			return true;
		return false;
	}
	
	public Boolean getShowCloseStrategicPlanPanel()
	{
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CLOSE))
			return true;
		return false;
	}	
	public Boolean getShowReviewPlanPanel()
	{
		if (pageMode.equalsIgnoreCase(PAGE_MODE_REVIEW))
			return true;
		return false;
	}	
	
	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try 
		{
			// from followUp application search screen
			if(sessionMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE))
			{
				String planDetailsMode = (String) sessionMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE);
				sessionMap.remove(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE);
				
				canAddAttachmentsAndComments(true);
				viewMap.put("planDetailsReadonlyMode", true);
				viewMap.put("asterisk", "");				
				
				if(planDetailsMode.equals(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_NEW))
				{
					pageMode = PAGE_MODE_NEW;					
					viewMap.put("planDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_NEW);
				}
				else if(planDetailsMode.equals(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_DRAFT))
				{
					pageMode = PAGE_MODE_DRAFT;
					viewMap.put("planDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MODIFY);
				}
				else if(planDetailsMode.equals(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_VIEW))
				{
					pageMode = PAGE_MODE_VIEW;										
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_VIEW);
				}
				else if(planDetailsMode.equals(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_CLOSE))
				{
					pageMode = PAGE_MODE_CLOSE;										
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_CLOSE);
				}
				
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
				viewMap.put(PAGE_MODE, pageMode);
				
			}
			// FROM TASK LIST
			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
				checkTask();
			}
			else
			{
				canAddAttachmentsAndComments(true);
				pageMode = PAGE_MODE_NEW;
				viewMap.put("planDetailsReadonlyMode", false);
				viewMap.put("asterisk", "*");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_NEW);
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
				viewMap.put(PAGE_MODE, pageMode);
			}
			// set back page
			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long planId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.UserTasks.STRATEGIC_PLAN_ID)!=null)
				{
					planId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.STRATEGIC_PLAN_ID));
					viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, planId);
				}
				String taskType = userTask.getTaskType();
				
				if(taskType.equals(WebConstants.UserTasks.StrategicPlan.TASK_TYPE_APPROVE))
				{
					pageMode = PAGE_MODE_APPROVAL;					
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MANAGE);					
				}
				else if(taskType.equals(WebConstants.UserTasks.StrategicPlan.TASK_TYPE_REVIEW))
				{
					pageMode = PAGE_MODE_REVIEW;					
					viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_REVIEW);
				}
				viewMap.put("planDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
				viewMap.put(PAGE_MODE, pageMode);
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_VIEW;
			
			logger.logInfo("pageMode : %s", pageMode);
			heading = getHeading();
			
			String selectedTab = (String) viewMap.get("selectedTab");
			if(StringHelper.isNotEmpty(selectedTab))
				tabPanel.setSelectedTab(selectedTab);			
			
			viewMap.put("planDetailsReadonlyMode", false);
			canAddAttachmentsAndComments(true);
			
			if(pageMode.equals(PAGE_MODE_NEW))
			{				
				readonlyStyleClass = "";
				htmlInputPlanNameAr.setReadonly(false);
				htmlInputPlanNameEn.setReadonly(false);
				htmlInputPlanDescription.setReadonly(false);
				htmlCalendarCreationDate.setDisabled(false);
				addPortfolioButton.setRendered(true);				
				
				viewMap.put("asterisk", "*");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_NEW);
			}
			else if(pageMode.equals(PAGE_MODE_DRAFT))
			{			
				readonlyStyleClass = "";
				htmlInputPlanNameAr.setReadonly(false);
				htmlInputPlanNameEn.setReadonly(false);
				htmlInputPlanDescription.setReadonly(false);
				htmlCalendarCreationDate.setDisabled(false);
				addPortfolioButton.setRendered(true);				
				viewMap.put("asterisk", "*");
				saveButton.setRendered(true);
				sendForApprovalButton.setRendered(true);
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MODIFY);
			}
			else if(pageMode.equals(PAGE_MODE_DRAFT_DONE))
			{
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				saveButton.setRendered(false);
				sendForApprovalButton.setRendered(false);
				viewMap.put("asterisk", "");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MODIFY);
			}
			else if(pageMode.equals(PAGE_MODE_REVIEW))
			{
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				activateButton.setRendered(true);
				viewMap.put("asterisk", "");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_REVIEW);
			}
			else if(pageMode.equals(PAGE_MODE_REVIEW_DONE))
			{
				viewMap.put("planDetailsReadonlyMode", true);
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				activateButton.setRendered(false);
				canAddAttachmentsAndComments(false);
				viewMap.put("asterisk", "");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_REVIEW);
			}
			else if(pageMode.equals(PAGE_MODE_APPROVAL))
			{
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				viewMap.put("asterisk", "");				
				approveButton.setRendered(true);
				rejectButton.setRendered(true);
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MANAGE);
			}
			else if(pageMode.equals(PAGE_MODE_APPROVAL_DONE))
			{
				readonlyStyleClass = "READONLY";
				viewMap.put("planDetailsReadonlyMode", true);
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				viewMap.put("asterisk", "");
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				canAddAttachmentsAndComments(false);
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_MANAGE);
			}
			else if(pageMode.toString().equals(PAGE_MODE_CLOSE))
			{
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				viewMap.put("asterisk", "");
				closeButton.setRendered(true);
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_CLOSE);
			}
			else if(pageMode.toString().equals(PAGE_MODE_CLOSE_DONE))
			{
				readonlyStyleClass = "READONLY";
				viewMap.put("planDetailsReadonlyMode", true);
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);
				viewMap.put("asterisk", "");
				closeButton.setRendered(false);
				canAddAttachmentsAndComments(false);
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_CLOSE);
			}
			else if(pageMode.toString().equals(PAGE_MODE_VIEW))
			{
				viewMap.put("planDetailsReadonlyMode", true);
				readonlyStyleClass = "READONLY";
				htmlInputPlanNameAr.setReadonly(true);
				htmlInputPlanNameEn.setReadonly(true);
				htmlInputPlanDescription.setReadonly(true);
				htmlCalendarCreationDate.setDisabled(true);
				addPortfolioButton.setRendered(false);	
				canAddAttachmentsAndComments(false);
				viewMap.put("asterisk", "");
				viewMap.put(HEADING, WebConstants.StrategicPlan.Headings.STRATEGIC_PLAN_MODE_VIEW);
			}
			
			viewMap.remove("selectedTab");			
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}	
		
	@SuppressWarnings("unchecked")
	public String approveStrategicPlan() {
		logger.logInfo("approveStrategicPlan() started...");
		Long strategicPlanId=0L;
		Boolean success = false;
		try 
		{			
			if(validatedForApproveStrategicPlan())
			{
				String returnVal ="";
				String returnValFromSave = saveStrategicPlan();				
				if(returnValFromSave.equals("true"))
				{
					StrategicPlanView strategicPlanView =  getStrategicPlanView(); 
					CommonUtil.saveRemarksAsComments(strategicPlanView.getStrategicPlanId(), strategicPlanView.getStrategicPlanApproveRejectRemarks(), noteOwner,strategicPlanView.getStrategicPlanApproveRejectDate());
					
					if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID))			
						strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
					
					CommonUtil.loadAttachmentsAndComments(strategicPlanId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(strategicPlanId);
					Boolean commentSuccess = CommonUtil.saveComments(strategicPlanId, noteOwner);
	
					infoMessage = "";
					errorMessages.clear();
					if(completeTask(TaskOutcome.APPROVE))
					{				
						infoMessage = CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_APPROVE_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_APPROVAL_DONE);
					}
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_APPROVE_FAIL));
				}
			}
			logger.logInfo("approveStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("approveStrategicPlan() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_APPROVE_FAIL));
		}
		return "completeFollowUp";
	}
	@SuppressWarnings("unchecked")
	public String rejectStrategicPlan()
	{
		logger.logInfo("rejectStrategicPlan() started...");
		Long strategicPlanId=0L;
		Boolean success = false;
		try 
		{
		
			if(validatedForApproveStrategicPlan())
			{				
				String returnValFromSave = saveStrategicPlan();				
				if(returnValFromSave.equals("true"))
				{
					StrategicPlanView strategicPlanView =  getStrategicPlanView(); 
					CommonUtil.saveRemarksAsComments(strategicPlanView.getStrategicPlanId(), strategicPlanView.getStrategicPlanApproveRejectRemarks(), noteOwner,strategicPlanView.getStrategicPlanApproveRejectDate());
					
					if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID))			
						strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
					
					CommonUtil.loadAttachmentsAndComments(strategicPlanId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(strategicPlanId);
					Boolean commentSuccess = CommonUtil.saveComments(strategicPlanId, noteOwner);
	
					infoMessage = "";
					errorMessages.clear();
					if(completeTask(TaskOutcome.REJECT))
					{						
						infoMessage = CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REJECT_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_APPROVAL_DONE);
					}
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REJECT_FAIL));
				}
			}
			logger.logInfo("rejectStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("rejectStrategicPlan() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REJECT_FAIL));
		}
		return "completeFollowUp";
	}
	@SuppressWarnings("unchecked")
	public String reviewStrategicPlan()
	{
		logger.logInfo("reviewStrategicPlan() started...");
		Long strategicPlanId=0L;
		Boolean success = false;
		try 
		{			
			if(validatedForReviewStrategicPlan())
			{				
				String returnValFromSave = saveStrategicPlan();				
				if(returnValFromSave.equals("true"))
				{
					StrategicPlanView strategicPlanView =  getStrategicPlanView(); 
					CommonUtil.saveRemarksAsComments(strategicPlanView.getStrategicPlanId(), strategicPlanView.getStrategicPlanReviewRemarks(), noteOwner,strategicPlanView.getStrategicPlanReviewDate());
					
					if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID))			
						strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
					
					CommonUtil.loadAttachmentsAndComments(strategicPlanId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(strategicPlanId);
					Boolean commentSuccess = CommonUtil.saveComments(strategicPlanId, noteOwner);
	
					infoMessage = "";
					errorMessages.clear();
					if(completeTask(TaskOutcome.OK))
					{						
						infoMessage = CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REVIEWED_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_REVIEW_DONE);
					}
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REVIEWED_FAIL));
				}
			}
			logger.logInfo("rejectStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("reviewStrategicPlan() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_REVIEWED_FAIL));
		}
		return "completeFollowUp";
	}
	@SuppressWarnings("unchecked")
	public String closeStrategicPlan()
	{
		logger.logInfo("reviewStrategicPlan() started...");
		Long strategicPlanId=0L;
		Boolean success = false;
		try 
		{			
			if(validatedForCloseStrategicPlan())
			{				
				String returnValFromSave = saveStrategicPlan();
				if(returnValFromSave.equals("true"))
				{
					StrategicPlanView strategicPlanView =  getStrategicPlanView(); 
					CommonUtil.saveRemarksAsComments(strategicPlanView.getStrategicPlanId(), strategicPlanView.getStrategicPlanClosingRemarks(), noteOwner,strategicPlanView.getStrategicPlanClosingDate());
					
					if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID))			
						strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);
					
					CommonUtil.loadAttachmentsAndComments(strategicPlanId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(strategicPlanId);
					Boolean commentSuccess = CommonUtil.saveComments(strategicPlanId, noteOwner);
	
					infoMessage = "";
					errorMessages.clear();
					infoMessage = CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_CLOSE_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_CLOSE_DONE);
				}
				else
				{
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_CLOSE_FAIL));
				}
			}
			logger.logInfo("rejectStrategicPlan() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("reviewStrategicPlan() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_CLOSE_FAIL));
		}
		return "completeFollowUp";
	}
	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception{
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		try {
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = CommonUtil.getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, taskOutcome);
				}
			logger.logInfo("completeTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.logError("completeTask() crashed");
			success = false;
			throw exception;
		}
		return success;
	}
	
	@SuppressWarnings("unchecked")
	public String sendStrategicPlanForApproval()
	{
		String returnVal ="";		
		boolean crashed = false;
		try
		{
			
			boolean isvalidatedForSendStrategicPlanForApproval = validatedForSendStrategicPlanForApproval();
			boolean isvalidatedForNewStrategicPlan = validatedForNewStrategicPlan();
			if(isvalidatedForSendStrategicPlanForApproval && isvalidatedForNewStrategicPlan)
			{
				String isSuccess = saveStrategicPlan();
				if(isSuccess.equals("true"))
				{
					invokeBpel();
					errorMessages.clear();
					infoMessage = "";
					infoMessage= CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_SEND_FOR_APPROVAL_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_DRAFT_DONE);
				}				
			}
		}
		catch (Exception exception) 
		{
			crashed = true;
			logger.LogException("sendStrategicPlanForApproval() crashed ", exception);
		}
		finally
		{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_SEND_FOR_APPROVAL_FAIL));
		}
		return returnVal;		
	}
	
	public Boolean invokeBpel()
	{
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try 
		{		
			PIMSStrategicPlanBPELPortClient bpelPortClient= new PIMSStrategicPlanBPELPortClient();
			StrategicPlanView strategicPlanView = getStrategicPlanView();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.StrategicPlan.BPEL_END_POINT);
			bpelPortClient.setEndpoint(endPoint);
			String userId = CommonUtil.getLoggedInUser();
			bpelPortClient.initiate(strategicPlanView.getStrategicPlanId(), userId, null, null);			
			logger.logInfo("invokeBpel() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
		}		
		return success;
	}

	@SuppressWarnings("unchecked")
	public String saveStrategicPlan()
	{		
		String returnVal = "false";
		logger.logInfo("saveStrategicPlan() started...");		
		Boolean crashed = false;
		Boolean firstTime = false;
		try {
			
			infoMessage ="";
			errorMessages.clear();
			
			if(validatedForNewStrategicPlan())
			{
								
				InvestmentOpportunityService ios = new InvestmentOpportunityService();				
				String loggedInUser = CommonUtil.getLoggedInUser();
				
				List<StrategicPlanPortfolioView> deletedStrategicPlanPortfolioViewList =  getDeletedStrategicPlanPortfolios();
				StrategicPlanView strategicPlanView = getStrategicPlanView();	
				
				if(deletedStrategicPlanPortfolioViewList != null && deletedStrategicPlanPortfolioViewList.size() >0)
					strategicPlanView.setDeletedStrategicPlanPortfolioViewList(deletedStrategicPlanPortfolioViewList);
				
				strategicPlanView.setUpdatedBy(loggedInUser);
				strategicPlanView.setUpdatedOn(new Date());
				if(strategicPlanView.getStrategicPlanId() == null)
				{
					firstTime = true;
					strategicPlanView.setCreatedBy(loggedInUser);
					strategicPlanView.setCreatedOn(new Date());
					strategicPlanView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
					strategicPlanView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);					
				}
				strategicPlanView = ios.persistStrategicPlan(strategicPlanView);		
				setStrategicPlanView(strategicPlanView);				
				 
				Long strategicPlanId = strategicPlanView.getStrategicPlanId();
				viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, strategicPlanId);
				 
				saveStrategicPlanStudyViewList( strategicPlanView );
				updateValuesToMaps();
				
				CommonUtil.loadAttachmentsAndComments(strategicPlanId.toString());
				
				Boolean attachSuccess = CommonUtil.saveAttachments(strategicPlanId);
				Boolean commentSuccess = CommonUtil.saveComments(strategicPlanId, noteOwner);				
				
				returnVal = "true";
				if(firstTime)
				{
					infoMessage=CommonUtil.getParamBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_CREATED_SUCCESSFULLY, strategicPlanView.getStrategicPlanNumber());
					viewMap.put(PAGE_MODE, PAGE_MODE_DRAFT);
				}
				else
				{
					infoMessage=CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_MODIFIED_SUCCESSFULLY);
				}	
				viewMap.put(SEND_FOR_APPROVAL,true);
			}
			logger.logInfo("saveStrategicPlan() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("saveStrategicPlan() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("saveStrategicPlan() crashed ", exception);
		}
		finally
		{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_FAILED_SAVE));
		}
		return returnVal;
	}
	
	private void saveStrategicPlanStudyViewList( StrategicPlanView strategicPlanView ) throws PimsBusinessException 
	{	
		if ( strategicPlanStudyViewList != null && strategicPlanStudyViewList.size() > 0 )
		{
			InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
			
			for ( StrategicPlanStudyView strategicPlanStudyView : strategicPlanStudyViewList )
			{
				if ( strategicPlanStudyView.getStrategicPlanStudyId() == null )
				{
					strategicPlanStudyView.setStrategicPlanView( strategicPlanView );
					strategicPlanStudyView.setCreatedBy( CommonUtil.getLoggedInUser() );
					strategicPlanStudyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					
					investmentOpportunityServiceAgent.addStrategicPlanStudyView( strategicPlanStudyView );					
				}
				else
				{	
					strategicPlanStudyView.setStrategicPlanView( strategicPlanView );					
					strategicPlanStudyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					
					investmentOpportunityServiceAgent.updateStrategicPlanStudyView( strategicPlanStudyView );
				}
			}
		}
	}

	public Integer getStrategicPlanStudyViewListRecordSize()
	{
		int size = 0;
		
		return size;
	}
	
	public String onAddStudies() 
	{
		String path = null;
		sessionMap.put( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY, WebConstants.Study.STUDY_SEARCH_PAGE_MODE_POPUP_MULTI );
		String javascript = "openStudySearchPopup();";
		openPopup(javascript);
		return path;
	}
	
	@SuppressWarnings("unchecked")
	public String receiveSelectedStudies()
	{
		if ( sessionMap.get( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST ) != null )
		{
			// Selected studies from search studies popup
			List<StudyDetailView> selectedStudyDetailView = (List<StudyDetailView>) sessionMap.get( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST );
			sessionMap.remove( WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST );			
			
			Map<Long, StudyDetailView> alreadyContainedStudyMap = new HashMap(0);		

			// All those studies which already exits in the list should not be added again
			for ( StrategicPlanStudyView currentStrategicPlanStudyView : strategicPlanStudyViewList )
			{
				if ( currentStrategicPlanStudyView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				{
					alreadyContainedStudyMap.put( currentStrategicPlanStudyView.getStudyDetailView().getStudyId(), currentStrategicPlanStudyView.getStudyDetailView() );
				}
			}
			
			for ( StudyDetailView currentStudyDetailView : selectedStudyDetailView )
			{
				if ( alreadyContainedStudyMap.get( currentStudyDetailView.getStudyId() ) == null )
				{
					
					StrategicPlanStudyView newStrategicPlanStudyView = new StrategicPlanStudyView();
					newStrategicPlanStudyView.setStudyDetailView( currentStudyDetailView );
					newStrategicPlanStudyView.setCreatedBy( CommonUtil.getLoggedInUser() );
					newStrategicPlanStudyView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					newStrategicPlanStudyView.setCreatedOn( new Date() );
					newStrategicPlanStudyView.setUpdatedOn( new Date() );
					newStrategicPlanStudyView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
					newStrategicPlanStudyView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
					strategicPlanStudyViewList.add( newStrategicPlanStudyView );
				}
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW_LIST) != null )
		{
			strategicPlanStudyViewList = (List<StrategicPlanStudyView>) viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW_LIST);
		}
		else if ( viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID) != null )
		{
			InvestmentOpportunityService ios = new InvestmentOpportunityService();
			
			Long strategicPlanId = (Long)viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID);			
			StrategicPlanView strategicPlanView = ios.getStrategicPlanId(strategicPlanId);
			
			StrategicPlanStudyView strategicPlanStudyView = new StrategicPlanStudyView();
			strategicPlanStudyView.setStrategicPlanView(strategicPlanView);
			strategicPlanStudyView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			strategicPlanStudyView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			
			strategicPlanStudyViewList = ios.getStrategicPlanStudyViewList(strategicPlanStudyView);
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps() 
	{
		if ( strategicPlanStudyViewList != null )
		{
			viewMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_STUDY_VIEW_LIST, strategicPlanStudyViewList);
		}
	}

	public String onStudyDelete() 
	{
		String path = null;
		StrategicPlanStudyView selectedStrategicPlanStudyView = (StrategicPlanStudyView) strategicPlanStudyDataTable.getRowData();
		if ( selectedStrategicPlanStudyView.getStrategicPlanStudyId() == null )
			strategicPlanStudyViewList.remove( selectedStrategicPlanStudyView );	
		else
			selectedStrategicPlanStudyView.setIsDeleted( WebConstants.IS_DELETED_TRUE );		
		updateValuesToMaps();		
		return path;
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}


	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(StringHelper.isEmpty(backScreen))
					backScreen = "home";
				
				if(viewMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA))
				{					
					sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA, viewMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA));					
					viewMap.remove(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA);
				}
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }

    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}

	public Boolean getShowCancel(){
		return true;
	}
	
	public Boolean getShowSaveProject() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return true;
		return false;
	}
	
	public Boolean getPlanDetailsReadonlyMode(){
		Boolean planDetailsReadonlyMode = (Boolean)viewMap.get("planDetailsReadonlyMode");
		if(planDetailsReadonlyMode==null)
			planDetailsReadonlyMode = false;
		return planDetailsReadonlyMode;
	}

	
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	private String getProjectTypeKey() throws PimsBusinessException{
		String dataValue = "";
		String projectTypeId = (String)viewMap.get("projectTypeId");
		if(StringHelper.isNotEmpty(projectTypeId)){
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			DomainDataView domainDataView = utilityServiceAgent.getDomainDataByDomainDataId(Convert.toLong(projectTypeId));
			dataValue = domainDataView.getDataValue();
		}
		return dataValue;
	}
	private Boolean designApprovalRequired() throws PimsBusinessException{
		String propertyType = getProjectTypeKey();
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_CONSTRUCTION))
			return true;
		return false;
	}
	
	private Boolean handoverRequired() throws PimsBusinessException{
		String propertyType = getProjectTypeKey();
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_CONSTRUCTION))
			return true;
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_BOT))
			return true;
		return false;
	}
	
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		String remarks = (String)viewMap.get("remarks");
		return (remarks==null)?"":remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		if(remarks!=null)
			viewMap.put("remarks", remarks);
	}

	/**
	 * @return the followUpRemarks
	 */
	public String getFollowUpRemarks() {
		String followUpRemarks = (String)viewMap.get("followUpRemarks");
		return (followUpRemarks==null)?"":followUpRemarks;
	}

	/**
	 * @param followUpRemarks the followUpRemarks to set
	 */
	public void setFollowUpRemarks(String followUpRemarks) {
		if(followUpRemarks!=null)
			viewMap.put("followUpRemarks", followUpRemarks);
	}

	/**
	 * @return the initialReceivingDate
	 */
	public Date getInitialReceivingDate() {
		Date initialReceivingDate = (Date)viewMap.get("initialReceivingDate");
		return initialReceivingDate;
	}

	/**
	 * @param initialReceivingDate the initialReceivingDate to set
	 */
	public void setInitialReceivingDate(Date initialReceivingDate) {
		if(initialReceivingDate!=null)
			viewMap.put("initialReceivingDate", initialReceivingDate);
	}

	/**
	 * @return the finalReceivingDate
	 */
	public Date getFinalReceivingDate() {
		Date finalReceivingDate = (Date)viewMap.get("finalReceivingDate");
		return finalReceivingDate;
	}

	/**
	 * @param finalReceivingDate the finalReceivingDate to set
	 */
	public void setFinalReceivingDate(Date finalReceivingDate) {
		if(finalReceivingDate!=null)
			viewMap.put("finalReceivingDate", finalReceivingDate);
	}

	/**
	 * @return the handOverDate
	 */
	public Date getHandOverDate() {
		Date handOverDate = (Date)viewMap.get("handOverDate");
		return handOverDate;
	}

	public void setHandOverDate(Date handOverDate) {
		if(handOverDate!=null)
			viewMap.put("handOverDate", handOverDate);
	}

	/**
	 * @return the followUpDate
	 */
	public Date getFollowUpDate() {
//		Date followUpDate = (Date)viewMap.get("followUpDate");
		Date followUpDate = new Date();
		return followUpDate;
	}

	/**
	 * @param followUpDate the followUpDate to set
	 */
	public void setFollowUpDate(Date followUpDate) {
		if(followUpDate!=null)
			viewMap.put("followUpDate", followUpDate);
	}

	public Date getActualEndDate() {
		Date actualEndDate = (Date)viewMap.get("actualEndDate");
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		if(actualEndDate!=null)
			viewMap.put("actualEndDate", actualEndDate);
	}
	
	public Date getActualStartDate() {
		Date actualStartDate = (Date)viewMap.get("actualStartDate");
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		if(actualStartDate!=null)
			viewMap.put("actualStartDate", actualStartDate);
	}

	public Boolean getActualStartDateReadOnly() {
		Boolean actualStartDateReadOnly = (Boolean)viewMap.get("actualStartDateReadOnly");
		return actualStartDateReadOnly==null?false:actualStartDateReadOnly;
	}
	
	public Boolean getActualEndDateReadOnly() {
		Boolean actualEndDateReadOnly = (Boolean)viewMap.get("actualEndDateReadOnly");
		return actualEndDateReadOnly==null?false:actualEndDateReadOnly;
	}

	/**
	 * @return the newMilestoneId
	 */
	public String getNewMilestoneId() {
		String newMilestoneId = (String)viewMap.get("newMilestoneId");
		return newMilestoneId;
	}
	/**
	 * @param newMilestoneId the newMilestoneId to set
	 */
	public void setNewMilestoneId(String newMilestoneId) {
		if(newMilestoneId!=null)
			viewMap.put("newMilestoneId", newMilestoneId);
	}
	
	public String getCompletionPercent() {
		String completionPercent = (String)viewMap.get("completionPercent");
		return completionPercent;
	}

	public void setCompletionPercent(String completionPercent) {
		if(completionPercent!=null)
			viewMap.put("completionPercent", completionPercent);
	}

	public String getLastAchievedMilestone() {
		String lastAchievedMilestone = (String)viewMap.get("lastAchievedMilestone");
		return lastAchievedMilestone;
	}

	public void setLastAchievedMilestone(String lastAchievedMilestone) {
		if(lastAchievedMilestone!=null)
			viewMap.put("lastAchievedMilestone", lastAchievedMilestone);
	}

	public String getNewCompletionPercent() {
		String newCompletionPercent = (String)viewMap.get("newCompletionPercent");
		return newCompletionPercent;
	}

	public void setNewCompletionPercent(String newCompletionPercent) {
		if(newCompletionPercent!=null)
			viewMap.put("newCompletionPercent", newCompletionPercent);
	}	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public String getReadonlyStyleClass() {
		return readonlyStyleClass;
	}
	public void setReadonlyStyleClass(String readonlyStyleClass) {
		this.readonlyStyleClass = readonlyStyleClass;
	}
	public HtmlInputText getHtmlInputPlanNameEn() {
		return htmlInputPlanNameEn;
	}
	public void setHtmlInputPlanNameEn(HtmlInputText htmlInputPlanNameEn) {
		this.htmlInputPlanNameEn = htmlInputPlanNameEn;
	}
	public HtmlInputText getHtmlInputPlanNameAr() {
		return htmlInputPlanNameAr;
	}
	public void setHtmlInputPlanNameAr(HtmlInputText htmlInputPlanNameAr) {
		this.htmlInputPlanNameAr = htmlInputPlanNameAr;
	}
	public HtmlInputTextarea getHtmlInputPlanDescription() {
		return htmlInputPlanDescription;
	}
	public void setHtmlInputPlanDescription(
			HtmlInputTextarea htmlInputPlanDescription) {
		this.htmlInputPlanDescription = htmlInputPlanDescription;
	}
	public HtmlCalendar getHtmlCalendarCreationDate() {
		return htmlCalendarCreationDate;
	}
	public void setHtmlCalendarCreationDate(HtmlCalendar htmlCalendarCreationDate) {
		this.htmlCalendarCreationDate = htmlCalendarCreationDate;
	}
	public HtmlCommandButton getAddPortfolioButton() {
		return addPortfolioButton;
	}
	public void setAddPortfolioButton(HtmlCommandButton addPortfolioButton) {
		this.addPortfolioButton = addPortfolioButton;
	}
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	public HtmlCommandButton getSendForApprovalButton() {
		return sendForApprovalButton;
	}
	public void setSendForApprovalButton(HtmlCommandButton sendForApprovalButton) {
		this.sendForApprovalButton = sendForApprovalButton;
	}
	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}
	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}
	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}
	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}
	public HtmlCommandButton getCloseButton() {
		return closeButton;
	}
	public void setCloseButton(HtmlCommandButton closeButton) {
		this.closeButton = closeButton;
	}
	public HtmlCommandButton getActivateButton() {
		return activateButton;
	}
	public void setActivateButton(HtmlCommandButton activateButton) {
		this.activateButton = activateButton;
	}
	public List<StrategicPlanStudyView> getStrategicPlanStudyViewList() {
		return strategicPlanStudyViewList;
	}
	public void setStrategicPlanStudyViewList(
			List<StrategicPlanStudyView> strategicPlanStudyViewList) {
		this.strategicPlanStudyViewList = strategicPlanStudyViewList;
	}
	public HtmlDataTable getStrategicPlanStudyDataTable() {
		return strategicPlanStudyDataTable;
	}
	public void setStrategicPlanStudyDataTable(
			HtmlDataTable strategicPlanStudyDataTable) {
		this.strategicPlanStudyDataTable = strategicPlanStudyDataTable;
	}

	public HtmlCalendar getHtmlCalendarEndDate() {
		return htmlCalendarEndDate;
	}

	public void setHtmlCalendarEndDate(HtmlCalendar htmlCalendarEndDate) {
		this.htmlCalendarEndDate = htmlCalendarEndDate;
	}
	
	
}