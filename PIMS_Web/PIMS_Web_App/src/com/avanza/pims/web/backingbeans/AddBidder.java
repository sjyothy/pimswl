package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;






import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.InquiryManager;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Inquiry;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
/*import com.avanza.pims.web.tempentity.SecUserGroupData;*/
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.entity.Property;
import com.avanza.ui.util.ResourceUtil;

public class AddBidder extends AbstractController {

	private static Logger logger = Logger.getLogger( AddBidder.class );
	
	private String pageMode;
	private String PAGE_MODE_ADD="ADD";
	private String PAGE_MODE_EDIT="EDIT";
	private HtmlDataTable dataTable;
	private String personId = "";
	private String firstName = "";
	private String middleName = "";
	private String lastName = "";
	private String gender1 = "";
	private Long personType;
	private String designation;
	private String bidderId;
	private String contactInfoId;
    private String passportNumber;
    private Date passportIssueDate;
  //  private Long passportIssuePlaceId;
    private Date passportExpiryDate;
    private String residenseVisaNumber;
    private Date residenseVidaExpDate;
  //  private Long residenseVisaIssuePlaceId;
    private String personalSecCardNo;
    private String drivingLicenseNumber;
    private String socialSecNumber;
	
	//private String gender = "";
	private String cellNo = "";
	private Date dateOfBirth;
	private String address1 = ""; 
	private String address2 = "";
	private String street;
	private String postCode;
	
	private boolean isPageModeAdd;
	/*private String city;
	private String state;
	private String country;*/
	private String homePhone;
	private String officePhone;
	private String fax; 
	private String email;

	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu genderSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu titleSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu nationalitySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu personTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu residenseVisaIssuePlaceSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu passportIssuePlaceSelectMenu = new HtmlSelectOneMenu();
	private HtmlInputHidden hiddenStateValue = new HtmlInputHidden();
	private HtmlInputHidden hiddenCityValue = new HtmlInputHidden();
	
	String selectOneTitle;
	String selectOneCountry;
	String selectOneState ;
	String selectOneCity ;
	String selectOneNationality;

	
	private PersonView bidderView  = new PersonView();
	private ContactInfoView contactInfoView = new ContactInfoView();
	private BidderView bidderViewEdit  = new BidderView();
	private ContactInfoView contactInfoViewEdit = new ContactInfoView();
	
	
	List<SelectItem> title= new ArrayList();
	List<SelectItem> residenseVisaIssuePlace= new ArrayList();
	List<SelectItem> passportIssuePlace= new ArrayList();
	
	
/*	List<SelectItem> personType= new ArrayList();*/
	List<SelectItem> city= new ArrayList();
	List<SelectItem> state= new ArrayList();
	List<SelectItem> country= new ArrayList();
	List<SelectItem> gender= new ArrayList();
	List<SelectItem> nationality= new ArrayList();
	
	private List<SelectItem> countries = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>();
	private List<SelectItem> cities = new ArrayList<SelectItem>();
	
	private Map<Long,RegionView> countryMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> stateMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> cityMap = new HashMap<Long,RegionView>();
	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
//	public Unit dataItem = new Unit();
	
	FacesContext context=getFacesContext();
    Map sessionMap;
    CommonUtil commonUtil = new CommonUtil();
	
    Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    
    private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	public AddBidder (){
		//loadCityList();
		//loadStateList();
		//loadCountryList();
		
		
	}
	
	
	
	
	public void init() 
    {
   	
   	 super.init();
   	 try
   	 {
   		 System.out.println("Testing");
    sessionMap=context.getExternalContext().getSessionMap();
    loadCountries();
	loadStateMap();
   
    if(!isPostBack()){
    	       
    	errorMessages = new ArrayList<String>();
		successMessages=new ArrayList<String>();
    	
    		PropertyServiceAgent psa = new PropertyServiceAgent();
    		List<BidderView> bidderViewList = new ArrayList<BidderView>();
    	
    	   Map  request=context.getExternalContext().getRequestMap();
           if(request.get("bidderId")!=null && !request.get("bidderId").toString().trim().equals("")){
    		 bidderId=request.get("bidderId").toString().trim();
    		 bidderViewEdit.setBidderId(new Long(bidderId));
    		 bidderViewEdit= null;// psa.getBidderById(new Long(bidderId));
    	
    		 if(bidderViewEdit.getCreatedBy()!=null){
              	  viewMap.put("CreatedBy", bidderViewEdit.getCreatedBy());}
              	  if(bidderViewEdit.getCreatedOn()!=null){
              	  viewMap.put("CreatedOn", bidderViewEdit.getCreatedOn());}
    		 
    		 
    	    pageMode=PAGE_MODE_EDIT;	
    	    putBidderViewValueInControl();
    	}
    	else
    	{
    		pageMode=PAGE_MODE_ADD;
    		
    	}
    		
   	 	loadCombos();
   	
     }
   		 
   	 }
   	 catch(Exception es)
   	 {
   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
   		 System.out.println(es);
   		 
   	 }
	        
	 }
	
	private boolean putBidderViewValueInControl() throws ParseException{
		
		String methodName = "putControlValuesInView";
		DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
		
	       boolean isSucceed=true;
	      
	       
	       
	       
	
	      
	     if(	bidderViewEdit.getTitleId()!=null ){
	       	
	    	 selectOneTitle=bidderViewEdit.getTitleId().toString();
	       }
	
	       if (	bidderViewEdit.getFirstName() != null)
	      {
	    	   firstName = bidderViewEdit.getFirstName();
		   }
	
	       if (bidderViewEdit.getMiddleName() != null)
	       {
	    	   middleName = bidderViewEdit.getMiddleName();
	 	   }

	       if (bidderViewEdit.getLastName()!= null ) {
	    	   lastName= bidderViewEdit.getLastName();
	        }
	


		if (bidderViewEdit.getGender() != null) {
			gender1 = bidderViewEdit.getGender().toString();

		}
		
		
		 
	       if (bidderViewEdit.getDateOfBirth()!= null )
	     {
	    	   dateOfBirth = bidderViewEdit.getDateOfBirth();
		   }
		 if (bidderViewEdit.getCellNumber() != null )
	     {
			 cellNo =  bidderViewEdit.getCellNumber() ;
		   }
	
		 if (bidderViewEdit.getPassportNumber()!= null)
	     {
			 passportNumber=  bidderViewEdit.getPassportNumber();
		   }
		 if (bidderViewEdit.getPassportIssueDate() != null)
	     {
			 passportIssueDate= bidderViewEdit.getPassportIssueDate(); 
		   }
		 if (bidderViewEdit.getPassportExpiryDate() != null )
	     {
			passportExpiryDate = bidderViewEdit.getPassportExpiryDate();
		   }
	    if (bidderViewEdit.getResidenseVisaNumber() != null)
	     {
	    	residenseVisaNumber = bidderViewEdit.getResidenseVisaNumber();
		   }
		 if (bidderViewEdit.getResidenseVisaExpDate()!= null)
	     {
			 residenseVidaExpDate = bidderViewEdit.getResidenseVisaExpDate();
		   }
		 if (bidderViewEdit.getPersonalSecCardNo() != null )
	     {
			 personalSecCardNo = bidderViewEdit.getPersonalSecCardNo();
		   }
		 if (bidderViewEdit.getDrivingLicenseNumber()!= null)
	     {
			 drivingLicenseNumber= bidderViewEdit.getDrivingLicenseNumber();
		   }
		 if (bidderViewEdit.getSocialSecNumber() != null )
	     {
			 socialSecNumber=bidderViewEdit.getSocialSecNumber();
		   }

		 if (bidderViewEdit.getNationalityId()!=null){
		 		
			 selectOneNationality= bidderViewEdit.getNationalityId().toString();
		 	}
	

			if (	bidderViewEdit.getContactInfoView() !=null){
				
				contactInfoViewEdit =  bidderViewEdit.getContactInfoView();
				
				contactInfoId =  contactInfoViewEdit.getContactInfoId().toString();
			}
			
		 if (contactInfoViewEdit.getAddress1()!= null ) {
			 address1= contactInfoViewEdit.getAddress1();

		}
		
		if (contactInfoViewEdit.getAddress2() != null) {
			address2= contactInfoViewEdit.getAddress2();

		}
	

		if (contactInfoViewEdit.getStreet() != null ) {
			street= contactInfoViewEdit.getStreet();

		}
		if (contactInfoViewEdit.getPostCode() != null ) {
			postCode= contactInfoViewEdit.getPostCode();

		}
		if (contactInfoViewEdit.getHomePhone() != null ) {
			homePhone =contactInfoViewEdit.getHomePhone();

		}
		if (contactInfoViewEdit.getOfficePhone()!= null ) {
			officePhone= contactInfoViewEdit.getOfficePhone();

		}
		if (contactInfoViewEdit.getFax() != null ) {
			fax = contactInfoViewEdit.getFax();

		}
		if (contactInfoViewEdit.getEmail() != null ) {
			email= contactInfoViewEdit.getEmail();

		}
			
		if (contactInfoViewEdit.getCountryId()!=null ){
			selectOneCountry= contactInfoViewEdit.getCountryId().toString();
			hiddenStateValue.setValue(selectOneCountry);
			}
		if (contactInfoViewEdit.getStateId() !=null ){
			 selectOneState = contactInfoViewEdit.getStateId().toString();
			}
		if (contactInfoViewEdit.getCityId() !=null ){
			selectOneCity = contactInfoViewEdit.getCityId().toString();
		}
	      return isSucceed;
	}
		
	
	
	 private void loadCombos() throws PimsBusinessException
     {
    	 
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	   loadTitleList();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
  loadCountry();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
    	   loadState();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
    	   loadCity();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    
    		  
    	   loadGenderList();
    	 loadRegion();
     }
	 
	 
	 private void  loadState(){
		 
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_STATE))
				{
			          Set<RegionView> regionViewList =  psa.getState(new Long(selectOneCountry));
			          sessionMap.put(WebConstants.SESSION_STATE,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		   
	   }
	 private void  loadCity(){
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_CITY))
				{
			          Set<RegionView> regionViewList =  psa.getCity(new Long(selectOneState));
			          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		   
	   }
	 
	 private void  loadRegion(){
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_REGION))
				{
			          List<RegionView> regionViewList =  psa.getRegion();
			          sessionMap.put(WebConstants.SESSION_REGION,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				System.out.println("Print"+ex);
			}
		   
	   }
	 
	 private void loadCountry()
	 {
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_COUNTRY))
				{
			         List<RegionView> regionViewList =  psa.getCountry();
			          sessionMap.put(WebConstants.SESSION_COUNTRY,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			
		 
	 }
	 private void loadTitleList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.TITLES);
    	 sessionMap.put(WebConstants.TITLES, ddl);
    
     }
	 
	
	
	public List<PropertyInquiryData> getPropertyInquiryDataList() {
		if (dataList == null) {
		//loadDataList(); // Preload by lazy loading.
		}
		return dataList;
	}
	

	public String getFirstName (){
		return this.firstName;
	}
	public void setFirstName (String firstName){
		this.firstName = firstName;
	}

	public String getMiddleName (){
		return this.middleName;
	}
	public void setMiddleName (String middleName){
		this.middleName = middleName;
	}
	
	public String getLastName (){
		return this.lastName;
	}
	public void setLastName (String lastName){
		this.lastName = lastName;
	}
	
	public String getCellNo (){
		return this.cellNo;
	}
	public void setCellNo (String cellNo){
		this.cellNo = cellNo;
	}
	
	public String getAddress1 (){
		return this.address1;
	}
	public void setAddress1 (String address1){
		this.address1 = address1;
	}
	
	public String getAddress2 (){
		return this.address2;
	}
	public void setAddress2 (String address2){
		this.address2 = address2;
	}
	
	public String getStreet (){
		return this.street;
	}
	public void setStreet (String street){
		this.street = street;
	}
	
	public String getPostCode (){
		return this.postCode;
	}
	public void setPostCode (String postCode){
		this.postCode = postCode;
	}
	
	public String getHomePhone (){
		return this.homePhone;
	}
	public void setHomePhone (String homePhone){
		this.homePhone = homePhone;
	}
	
	public String getOfficePhone (){
		return this.officePhone;
	}
	public void setOfficePhone (String officePhone){
		this.officePhone = officePhone;
	}
	
	public String getFax (){
		return this.fax;
	}
	public void setFax (String fax){
		this.fax = fax;
	}
	
	public String getEmail (){
		return this.email;
	}
	public void setEmail (String email){
		this.email = email;
	}
	

	
	
	
	
	public String doBid() {
		errorMessages = new ArrayList<String>();
		if (getPersonId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
	}
	Inquiry inquiryPOJO  = new Inquiry();
	InquiryManager inquiryManager = new InquiryManager(); 
	List<Unit> inquiryUnit = new ArrayList();
	
	
	public String btnBack_Click()
	{
		return "BackScreen";
		
	}
	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}

	public String getSuccessMessages() {
		
		return commonUtil.getErrorMessages(this.successMessages);
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	private void loadGenderList() {

	    gender = new ArrayList ();
	    
	    gender.add(new SelectItem("0", "Male"));
	    gender.add(new SelectItem("1", "Female"));
	    

		for (int i = 0; i < gender.size(); i++) {

			getGenderSelectMenu().setValue(new SelectItem(gender.get(i)));
		}

	  }
	
	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}

	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}

	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}

	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}

	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}

	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}

	public List<SelectItem> getCity() {
		city.clear();
		
		try {
			selectOneState = (String)hiddenCityValue.getValue();
			
			if(selectOneState != null && !selectOneState.equals("-1"))
			{
				Set<RegionView> regionViewList=getRegionChildren(new Long(selectOneState)); 
				Iterator<RegionView> iter=regionViewList.iterator();
				while(iter.hasNext())
				  {
					  RegionView rV=(RegionView)iter.next();
					  
				      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				      city.add(item);
				  }
			
			}
		}catch (Exception e){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return city;
	}

	public void setCity(List<SelectItem> city) {
		this.city = city;
	}

	public List<SelectItem> getState() {
		state.clear();
		
		selectOneCountry= (String)hiddenStateValue.getValue();
		
		try {
			if(selectOneCountry!=null &&  !selectOneCountry.equals("-1"))
			{
				
				
				 
				
				//loadState();
				Set<RegionView> regionViewList=getRegionChildren(new Long(selectOneCountry)); 
				Iterator<RegionView> iter=regionViewList.iterator();
				while(iter.hasNext())
				  {
					  RegionView rV=(RegionView)iter.next();
					  
				      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				      state.add(item);
				  }
			}
		}catch (Exception e)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Error.."+e);
		}
		
		return state;
	}

	public Set<RegionView> getRegionChildren(long parentRegionId)
	{
	    loadRegion();
	    List<RegionView> list = (ArrayList)sessionMap.get(WebConstants.SESSION_REGION);
		Set<RegionView> region=new HashSet<RegionView>();
	    
		for(int i=0;i<list.size();i++)
	    {
	     RegionView regionView =(RegionView)list.get(i);
	      if(regionView.getRegionId().equals(parentRegionId)){
	    	  region=regionView.getRegions();
	      break;}
	    	
	    	
	    }
	return region;
	}
	public List<RegionView> getRegionCountry()
	{
	    loadRegion();
	    List<RegionView> list = (ArrayList)sessionMap.get(WebConstants.SESSION_REGION);
		List<RegionView> region=new ArrayList<RegionView>();
		String regionCountry ;
		DomainData domaindata;
		UtilityManager utilitymanager = new UtilityManager(); 
		regionCountry= "REGION TYPE COUNTRY";
		   
		   try {
			domaindata= utilitymanager.getDomainDataByValue(regionCountry);
		
	    
		for(int i=0;i<list.size();i++)
	    {
	     RegionView regionView =(RegionView)list.get(i);
	      if(regionView.getRegionTypeId().equals(domaindata.getDomainDataId()))
	    	  region.add(regionView);
	    	
	    	
	    }
		
		   }catch (Exception e) {
			   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	return region;
	}
	public void setState(List<SelectItem> state) {
		this.state = state;
	}

	
	public void methodValueChange(ValueChangeEvent vce){
	 selectOneCountry = (String)vce.getNewValue();
	 selectOneCountry= (String)hiddenStateValue.getValue();
	    getState();

	 
	 
	}
	
	
	public void methodValueChangeState(ValueChangeEvent vce){
		 selectOneState = (String)	vce.getNewValue();
		 selectOneState = (String)hiddenCityValue.getValue();
		   //iP.getState();
		 getCity();
		 
		 }
	
	public List<SelectItem> getCountry() {
		country.clear();
		try 
		{
					//loadCountry();
					System.out.println("hiddenStateValue"+hiddenStateValue.getValue());
		            List<RegionView> regionViewList=getRegionCountry(); 
		            for(int i=0;i<regionViewList.size();i++)
		            {
			          RegionView rV=(RegionView)regionViewList.get(i);
		              SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn().toString());
		              country.add(item);
		            }
		}catch (Exception e){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Error::"+e);
			
		}
		return country;
	}

	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}


    public HtmlSelectOneMenu getGenderSelectMenu() {
		return genderSelectMenu;
	}
    public void setGenderSelectMenu(HtmlSelectOneMenu genderSelectMenu) {
		this.genderSelectMenu = genderSelectMenu;
	}

public List<SelectItem> getGender() {
		return gender;
	}
public void setGender(List<SelectItem> gender) {
		this.gender = gender;
	}

public String getPersonId() {
	return personId;
}
public void setPersonId(String personId) {
	this.personId = personId;
}

public String getGender1() {
	return gender1;
}

public void setGender1(String gender1) {
	this.gender1 = gender1;
}

public Long getPersonType() {
	return personType;
}

public void setPersonType(Long personType) {
	this.personType = personType;
}

public String btnAdd_Click() throws Exception {
	
	String methodName = "btnAdd_Click";
	String eventOutCome = "failure";
	String s1;
	logger.logInfo(methodName +" Starts::::::::::::::::::::::");
	errorMessages = new ArrayList<String>();
	successMessages=new ArrayList<String>();
	try {
 
		if(putControlValuesInView())
		{
			
		PropertyServiceAgent psa  =new PropertyServiceAgent();	
		
		logger.logInfo(methodName +" AddRecordStarts::::::::::::::::::::::");     
			psa.addBidder(bidderView);
		logger.logInfo(methodName +" AddRecordEnds::::::::::::::::::::::");	
		

			
	    	successMessages.add( ResourceUtil.getInstance().getProperty("common.messages.Save"));
		
			
			eventOutCome = "success";
			
			logger.logInfo(methodName +" Ends::::::::::::::::::::::");
		}

	} catch (Exception ex) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.logInfo(methodName +" Crashed::::::::::::::::::::::");
		ex.printStackTrace();
	}

	//return eventOutCome;
	return "BackScreen1";

}

private boolean putControlValuesInView() throws ParseException {
	
	String methodName = "putControlValuesInView";
	DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
	
       boolean isSucceed=true;
       errorMessages.clear();
       successMessages.clear();
       String message;
       logger.logInfo(methodName +" Starts::::::::::::::::::::::");
    try{   
       if (pageMode.equals("EDIT")){
    	   //bidderView.setBidderId(new Long(bidderId));
       }
      
       if(selectOneTitle!=null && !selectOneTitle.equals("") && !selectOneTitle.equals("-1")){
       	
       	bidderView.setTitleId(new Long(selectOneTitle));
       }
       else
   	{
    	   message = ResourceUtil.getInstance().getProperty("person.message.title");   
   		errorMessages.add(message);
   		isSucceed=false;
   		return isSucceed;
   	}
       
       
       if (firstName != null && !firstName.equals(""))
      {
		bidderView.setFirstName(firstName);
	   }
	else
	{ 
		message = ResourceUtil.getInstance().getProperty("person.message.firstName");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}
       if (middleName != null && !middleName.equals(""))
       {
 		bidderView.setMiddleName(middleName);
 	   }

       if (lastName != null && !lastName.equals("")) {
		bidderView.setLastName(lastName);
        }
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.lastName");
        errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}

	if (gender1 != null && ! gender1.equals("") && !gender1.equals("-1")) {
		bidderView.setGender(gender1);

	}
	
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.gender");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}	
	 if (dateOfBirth != null && !dateOfBirth.equals(""))
     {
		bidderView.setDateOfBirth(dateOfBirth);
	   }
	 if (cellNo != null && !cellNo.equals(""))
     {
		bidderView.setCellNumber(cellNo);
	   }

	 if (passportNumber != null && !passportNumber.equals(""))
     {
		bidderView.setPassportNumber(passportNumber);
	   }
	 if (passportIssueDate != null && !passportIssueDate.equals(""))
     {
		bidderView.setPassportIssueDate(passportIssueDate);
	   }
	 if (passportExpiryDate != null && !passportExpiryDate.equals(""))
     {
		bidderView.setPassportExpiryDate(passportExpiryDate);
	   }
	 if (residenseVisaNumber != null && ! residenseVisaNumber.equals(""))
     {
		bidderView.setResidenseVisaNumber(residenseVisaNumber);
	   }
	 if (residenseVidaExpDate != null && !residenseVidaExpDate.equals(""))
     {
		//bidderView.setResidenseVisaExpDate(residenseVidaExpDate);
	   }
	 if (personalSecCardNo != null && !personalSecCardNo.equals(""))
     {
		 bidderView.setPersonalSecCardNo(personalSecCardNo);
	   }

	 if (drivingLicenseNumber != null && ! drivingLicenseNumber.equals(""))
     {
		bidderView.setDrivingLicenseNumber(drivingLicenseNumber);
	   }
	 if (socialSecNumber != null && ! socialSecNumber.equals(""))
     {
		bidderView.setSocialSecNumber(socialSecNumber);
	   }

	 if (selectOneNationality !=null && !selectOneNationality.equals("")&& !selectOneNationality.equals("-1")){
	 		
	 		bidderView.setNationalityId(new Long(selectOneNationality));
	 	}
	 
	
	
	
	bidderView.setCreatedBy(getCreatedByUser(getLoggedInUser()));
	bidderView.setCreatedOn(getCreatedOnDate());
	bidderView.setUpdatedBy(getLoggedInUser());
	bidderView.setUpdatedOn(new Date());
	bidderView.setIsDeleted(new Long(0));
	bidderView.setRecordStatus(new Long(1));
	
	   if (pageMode.equals("EDIT")){
		   contactInfoView.setContactInfoId(new Long(contactInfoId));
       }
	
	if (address1 != null && ! address1.equals("")) {
		contactInfoView.setAddress1(address1);
	}
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.address1");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}	
	
	if (address2 != null && ! address2.equals("")) {
		contactInfoView.setAddress2(address2);

	}
/*	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.address2");
		errorMessages.add(message);
		isSucceed=false;
	}
*/
	if (street != null && ! street.equals("")) {
		contactInfoView.setStreet(street);

	}
	if (postCode != null && ! postCode.equals("")) {
		contactInfoView.setPostCode(postCode);

	}
	if (homePhone != null && ! homePhone.equals("")) {
		contactInfoView.setHomePhone(homePhone);

	}
	if (officePhone != null && ! officePhone.equals("")) {
		contactInfoView.setOfficePhone(officePhone);

	}
	if (fax != null && ! fax.equals("")) {
		contactInfoView.setFax(fax);

	}
	if (email != null && ! email.equals("")) {
		contactInfoView.setEmail(email);

	}
		
	if (selectOneCountry !=null && !selectOneCountry.equals("") && !selectOneCountry.equals("-1")){
			
			contactInfoView.setCountryId(new Long(selectOneCountry));
		}
	if (selectOneState !=null && !selectOneState.equals("")&& !selectOneState.equals("-1")){
			
			contactInfoView.setStateId(new Long(selectOneState));
		}
	if (selectOneCity !=null && !selectOneCity.equals("")&& !selectOneCity.equals("-1")){
		
		contactInfoView.setCityId(new Long(selectOneCity));
	}
	
	
	
	
	
	contactInfoView.setCreatedBy(getCreatedByUser(getLoggedInUser()));
	contactInfoView.setCreatedOn(getCreatedOnDate());
	contactInfoView.setUpdatedBy(getLoggedInUser());
	contactInfoView.setUpdatedOn(new Date());
	contactInfoView.setIsDeleted(new Long(0));
	contactInfoView.setRecordStatus(new Long(1));
	
	
	if (contactInfoView !=null){
		
		//bidderView.setContactInfoView(contactInfoView);
	}
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.contactInfo");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}	
	logger.logInfo(methodName +" Ends::::::::::::::::::::::");
   }catch (Exception e) {
	// TODO: handle exception
	   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	   logger.logInfo(methodName +" Crashed::::::::::::::::::::::");
	   e.printStackTrace();
}
      return isSucceed;
}
public HtmlSelectOneMenu getPersonTypeSelectMenu() {
	return personTypeSelectMenu;
}

public void setPersonTypeSelectMenu(HtmlSelectOneMenu personTypeSelectMenu) {
	this.personTypeSelectMenu = personTypeSelectMenu;
}

public Date getDateOfBirth() {
	return dateOfBirth;
}

public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

public String getDesignation() {
	return designation;
}

public void setDesignation(String designation) {
	this.designation = designation;
}

public String getPassportNumber() {
	return passportNumber;
}

public void setPassportNumber(String passportNumber) {
	this.passportNumber = passportNumber;
}

public Date getPassportIssueDate() {
	return passportIssueDate;
}

public void setPassportIssueDate(Date passportIssueDate) {
	this.passportIssueDate = passportIssueDate;
}

public Date getPassportExpiryDate() {
	return passportExpiryDate;
}

public void setPassportExpiryDate(Date passportExpiryDate) {
	this.passportExpiryDate = passportExpiryDate;
}

public String getResidenseVisaNumber() {
	return residenseVisaNumber;
}

public void setResidenseVisaNumber(String residenseVisaNumber) {
	this.residenseVisaNumber = residenseVisaNumber;
}

public Date getResidenseVidaExpDate() {
	return residenseVidaExpDate;
}

public void setResidenseVidaExpDate(Date residenseVidaExpDate) {
	this.residenseVidaExpDate = residenseVidaExpDate;
}

public String getPersonalSecCardNo() {
	return personalSecCardNo;
}

public void setPersonalSecCardNo(String personalSecCardNo) {
	this.personalSecCardNo = personalSecCardNo;
}

public String getDrivingLicenseNumber() {
	return drivingLicenseNumber;
}


public void setDrivingLicenseNumber(String drivingLicenseNumber) {
	this.drivingLicenseNumber = drivingLicenseNumber;
}

public String getSocialSecNumber() {
	return socialSecNumber;
}

public void setSocialSecNumber(String socialSecNumber) {
	this.socialSecNumber = socialSecNumber;
}

public List<SelectItem> getResidenseVisaIssuePlace() {
	return residenseVisaIssuePlace;
}

public void setResidenseVisaIssuePlace(List<SelectItem> residenseVisaIssuePlace) {
	this.residenseVisaIssuePlace = residenseVisaIssuePlace;
}

public List<SelectItem> getPassportIssuePlace() {
	return passportIssuePlace;
}


public void setPassportIssuePlace(List<SelectItem> passportIssuePlace) {
	this.passportIssuePlace = passportIssuePlace;
}

public HtmlSelectOneMenu getResidenseVisaIssuePlaceSelectMenu() {
	return residenseVisaIssuePlaceSelectMenu;
}
public void setResidenseVisaIssuePlaceSelectMenu(
		HtmlSelectOneMenu residenseVisaIssuePlaceSelectMenu) {
	this.residenseVisaIssuePlaceSelectMenu = residenseVisaIssuePlaceSelectMenu;
}

public HtmlSelectOneMenu getPassportIssuePlaceSelectMenu() {
	return passportIssuePlaceSelectMenu;
}


public void setPassportIssuePlaceSelectMenu(
		HtmlSelectOneMenu passportIssuePlaceSelectMenu) {
	this.passportIssuePlaceSelectMenu = passportIssuePlaceSelectMenu;
}

public String getSelectOneTitle() {
	return selectOneTitle;
}

public void setSelectOneTitle(String selectOneTitle) {
	this.selectOneTitle = selectOneTitle;
}




public List<SelectItem> getTitle() {
	title.clear();
	 try
	 {
	 if(sessionMap.containsKey(WebConstants.TITLES))
		 loadTitleList();
	 
	  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.TITLES );	 
	  for(int i=0;i<ddl.size();i++)
	  {
		  DomainDataView ddv=(DomainDataView)ddl.get(i);
	      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
	      title.add(item);
	  }
	  
	 }
	 catch(Exception ex)
	 {
		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	 }
	return title;
}

public void setTitle(List<SelectItem> title) {
	this.title = title;
}

public HtmlSelectOneMenu getTitleSelectMenu() {
	return titleSelectMenu;
}

public void setTitleSelectMenu(HtmlSelectOneMenu titleSelectMenu) {
	this.titleSelectMenu = titleSelectMenu;
}

public String getSelectOneCountry() {
	return selectOneCountry;
}

public void setSelectOneCountry(String selectOneCountry) {
	this.selectOneCountry = selectOneCountry;
}

public String getSelectOneState() {
	return selectOneState;
}

public void setSelectOneState(String selectOneState) {
	this.selectOneState = selectOneState;
}

public String getSelectOneCity() {
	return selectOneCity;
}

public void setSelectOneCity(String selectOneCity) {
	this.selectOneCity = selectOneCity;
}

public HtmlInputHidden getHiddenStateValue() {
	return hiddenStateValue;
}

public void setHiddenStateValue(HtmlInputHidden hiddenStateValue) {
	this.hiddenStateValue = hiddenStateValue;
}

public HtmlInputHidden getHiddenCityValue() {
	return hiddenCityValue;
}

public void setHiddenCityValue(HtmlInputHidden hiddenCityValue) {
	this.hiddenCityValue = hiddenCityValue;
}

public List<SelectItem> getNationality() {
	nationality.clear();
	try 
	{
				//loadCountry();
			//	System.out.println("hiddenStateValue"+hiddenStateValue.getValue());
	            List<RegionView> regionViewList=getRegionCountry(); 
	            for(int i=0;i<regionViewList.size();i++)
	            {
		          RegionView rV=(RegionView)regionViewList.get(i);
	              SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn().toString());
	              nationality.add(item);
	            }
	}catch (Exception e){
		
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		System.out.println("Error::"+e);
		
	}
	return nationality;
}
public void setNationality(List<SelectItem> nationality) {
	this.nationality = nationality;
}
public HtmlSelectOneMenu getNationalitySelectMenu() {
	return nationalitySelectMenu;
}

public void setNationalitySelectMenu(HtmlSelectOneMenu nationalitySelectMenu) {
	this.nationalitySelectMenu = nationalitySelectMenu;
}

public String getSelectOneNationality() {
	return selectOneNationality;
}

public void setSelectOneNationality(String selectOneNationality) {
	this.selectOneNationality = selectOneNationality;
}

public String getPageMode() {
	return pageMode;
}

public void setPageMode(String pageMode) {
	this.pageMode = pageMode;
}

public String getBidderId() {
	return bidderId;
}

public void setBidderId(String bidderId) {
	this.bidderId = bidderId;
}

public String getContactInfoId() {
	return contactInfoId;
}

public void setContactInfoId(String contactInfoId) {
	this.contactInfoId = contactInfoId;
}
public String getLocale(){
	WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getLanguageCode();
}

public String getDateFormat(){
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getDateFormat();
}
private String getLoggedInUser() {
	context = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession) context.getExternalContext()
			.getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
		UserDbImpl user = (UserDbImpl) session
				.getAttribute(WebConstants.USER_IN_SESSION);
		loggedInUser = user.getLoginId();
	}

	return loggedInUser;
}
public Date getCreatedOnDate( )throws Exception
{
	String methodName="getCreatedOnDate";
	SystemParameters parameters = SystemParameters.getInstance();
	
	DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	if(viewMap.containsKey("CreatedOn")
	  && viewMap.get("CreatedOn").toString()!=null 
	  && (viewMap.get("CreatedOn").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdOn.."+viewMap.get("CreatedOn").toString());
		return  df.parse(df.format(viewMap.get("CreatedOn")));
		
	}
	else
	{
		logger.logInfo(methodName+"|"+"createdOn.."+new Date());
		return  new Date();
	}
	 
	
}
public String getCreatedByUser(String loggedInUser)
{
	String methodName="getCreatedByUser";
	
  if(viewMap.containsKey("CreatedBy")
	 && viewMap.get("CreatedBy").toString()!=null 
	 && (viewMap.get("CreatedBy").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdBy.."+viewMap.get("CreatedBy").toString());
		return  viewMap.get("CreatedBy").toString();	
	}
  else
  {
		logger.logInfo(methodName+"|"+"createdBy.."+loggedInUser);
		return  loggedInUser;
  }
	 
	
}
public List<SelectItem> getCountries() {
	return countries;
}

public void setCountries(List<SelectItem> countries) {
	this.countries = countries;
}

public List<SelectItem> getStates() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	states = (List<SelectItem>) sessionMap.get("states");
	if(states==null)
		states = new ArrayList<SelectItem>();
	return states;
}

public void setStates(List<SelectItem> states) {
	this.states = states;
}

public List<SelectItem> getCities() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	cities = (List<SelectItem>) sessionMap.get("cities");
	if(cities==null)
		cities = new ArrayList<SelectItem>();
	return cities;
}

public void setCities(List<SelectItem> cities) {
	this.cities = cities;
}

public Map<Long, RegionView> getCountryMap() {
	return countryMap;
}

public void setCountryMap(Map<Long, RegionView> countryMap) {
	this.countryMap = countryMap;
}

public Map<Long, RegionView> getStateMap() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	stateMap = (Map<Long, RegionView>) sessionMap.get("stateMap");
	if(stateMap==null)
		stateMap = new HashMap<Long, RegionView>();
	return stateMap;
}

public void setStateMap(Map<Long, RegionView> stateMap) {
	this.stateMap = stateMap;
}

public Map<Long, RegionView> getCityMap() {
	return cityMap;
}

public void setCityMap(Map<Long, RegionView> cityMap) {
	this.cityMap = cityMap;
}

/**
 * @param isEnglishLocale the isEnglishLocale to set
 */
public void setEnglishLocale(boolean isEnglishLocale) {
	this.isEnglishLocale = isEnglishLocale;
}

/**
 * @param isArabicLocale the isArabicLocale to set
 */
public void setArabicLocale(boolean isArabicLocale) {
	this.isArabicLocale = isArabicLocale;
}

public boolean getIsArabicLocale()
{
	isArabicLocale = !getIsEnglishLocale();
	return isArabicLocale;
}

public boolean getIsEnglishLocale()
{
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	return isEnglishLocale;
}
private void loadCountries() {
	try {
		logger.logInfo("loadCountries() started...");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		countries = new ArrayList();
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		if(sessionMap.containsKey("countryMap"))
		{
			countryMap = (HashMap<Long, RegionView>) sessionMap.get("countryMap");
		}
		else{
			countryMap = new HashMap<Long, RegionView>();
			for(RegionView regView :regionViewList)
			{
				countryMap.put(regView.getRegionId(), regView);
			}
			sessionMap.put("countryMap",countryMap);
		}
		if(sessionMap.containsKey("countries"))
		{
			countries = (List<SelectItem>) sessionMap.get("countries");
		}
		else{	 
			for(RegionView regionView :regionViewList)
			{
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
			  countries.add(item);
			}
			sessionMap.put("countries",countries);
		}
		logger.logInfo("loadCountries() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadCountries() crashed ",exception);
	}


}

public void loadStateMap(){
	try {
		logger.logInfo("loadStateMap() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		stateMap = new HashMap<Long, RegionView>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> statesList = psa.getAllStates();
		for(RegionView regView :statesList)
		{
			stateMap.put(regView.getRegionId(), regView);
		}
		sessionMap.put("stateMap",stateMap);
		logger.logInfo("loadStateMap() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadStateMap() crashed ",exception);
	}
}

public void loadStates(ValueChangeEvent vce)  {
	try {
		logger.logInfo("loadStates() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		states = new ArrayList<SelectItem>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		String regionName = "";
		Long regionId = Convert.toLong(vce.getNewValue());
		RegionView temp = (RegionView)countryMap.get(regionId);
		if(temp!=null)
		{
			regionName = temp.getRegionName();
			List <RegionView> statesList = psa.getCountryStates(regionName,getIsArabicLocale());
			
			for(RegionView stateView:statesList)
			{
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
				else 
					item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
				states.add(item);
			}
			sessionMap.put("states",states);
		}
		else{
			sessionMap.remove("states");
		}
			
//		loadCities(vce);
		logger.logInfo("loadStates() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadStates() crashed ",exception);
	}
}

public void loadCities(ValueChangeEvent vce)  {
	try {
		logger.logInfo("loadCities() started...");
		cities = new ArrayList<SelectItem>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		String selectedStateName = "";
		Long regionId = Convert.toLong(vce.getNewValue());
		RegionView temp = (RegionView)stateMap.get(regionId);
		if(temp!=null)
		{
			selectedStateName = temp.getRegionName();
			List <RegionView> cityList = psa.getCountryStates(selectedStateName,getIsArabicLocale());
		
		
			for(RegionView cityView:cityList)
			{
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionEn());			  }
				else 
					item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionAr());	  
				cities.add(item);
			}
			sessionMap.put("cities",cities);
		}
		else{
			sessionMap.remove("cities");
		}
		logger.logInfo("loadCities() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadCities() crashed ",exception);
	}
}
}
