package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.ContactInfo;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Floor;
import com.avanza.pims.entity.Property;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyOwnerView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.ReceivePropertyFloorView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class DuplicateFloors extends AbstractController {
	private HtmlInputText floorPrefix = new HtmlInputText();
	private HtmlInputText startNumber = new HtmlInputText();
	private List<String> errorMessages=  new ArrayList<String>();
	private HtmlInputText endNumber = new HtmlInputText();
	private HtmlSelectBooleanCheckbox addWithUnits = new HtmlSelectBooleanCheckbox();
	private Boolean addWithUnitsVar = new Boolean(false);
	private HtmlCommandButton generateDuplicateFloors= new HtmlCommandButton();
	private HtmlCommandButton cancel= new HtmlCommandButton();
	private static Logger logger = Logger.getLogger(DuplicateFloors.class);
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	public HtmlSelectBooleanCheckbox getAddWithUnits() {
		
		return addWithUnits;
	}
	public void setAddWithUnits(HtmlSelectBooleanCheckbox addWithUnits) {
		this.addWithUnits = addWithUnits;
	}
	public Boolean getAddWithUnitsVar() {
		return addWithUnitsVar;
	}
	public void setAddWithUnitsVar(Boolean addWithUnitsVar) {
		this.addWithUnitsVar = addWithUnitsVar;
	}
	public HtmlInputText getEndNumber() {
		return endNumber;
	}
	public void setEndNumber(HtmlInputText endNumber) {
		this.endNumber = endNumber;
	}
	public HtmlInputText getFloorPrefix() {
		return floorPrefix;
	}
	public void setFloorPrefix(HtmlInputText floorPrefix) {
		this.floorPrefix = floorPrefix;
	}
	public HtmlInputText getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(HtmlInputText startNumber) {
		this.startNumber = startNumber;
	}
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();		
		if(getFacesContext().getExternalContext().getSessionMap().containsKey("FLOOR_VIEW_AS_PARAM"))
		{
			ReceivePropertyFloorView floorView = (ReceivePropertyFloorView)getFacesContext().getExternalContext().getSessionMap().get("FLOOR_VIEW_AS_PARAM");
			viewMap.put("FLOOR_VIEW_AS_PARAM",floorView);
			getFacesContext().getExternalContext().getSessionMap().remove("FLOOR_VIEW_AS_PARAM");
			if(floorView.getNoOfUnits()<=0)
				addWithUnits.setDisabled(true);
		}
		
	}
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}
	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	public HtmlCommandButton getCancel() {
		return cancel;
	}
	public void setCancel(HtmlCommandButton cancel) {
		this.cancel = cancel;
	} 
	public HtmlCommandButton getGenerateDuplicateFloors() {
		return generateDuplicateFloors;
	}
	public void setGenerateDuplicateFloors(HtmlCommandButton generateDuplicateFloors) {
		this.generateDuplicateFloors = generateDuplicateFloors;
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	private boolean validateFloors(){
		String method = "ValidateOwner";
		logger.logInfo(method + " started...");
		boolean isValid = true;	
		try{
			
			
			if(startNumber.getValue()!=null&&startNumber.getValue().toString().trim().length()>0){
			
				if(Integer.parseInt(startNumber.getValue().toString())<=0)throw new NumberFormatException();
				
		
		}
		else{
			isValid=false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_NO_FROM_REQUIRED));
		}
		}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_FROM));

		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_FROM));

		}
	try{
			
			
			if(endNumber.getValue()!=null&&endNumber.getValue().toString().trim().length()>0){

				if(Integer.parseInt(startNumber.getValue().toString())<=0) throw new NumberFormatException();
			
			}
	else{
		isValid=false;
		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_NO_TO_REQUIRED));
	}
	}
		catch (NumberFormatException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_TO));

		}
		catch (IllegalArgumentException e) {
			isValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_FLOOR_NUMBER_TO));

		}
		return isValid;
	}

	public void duplicateFloors(){
		String method = "generatePropertyUnits";
		logger.logInfo(method + " started...");

		try
		{			
			if(validateFloors())
			{
			
				ReceivePropertyFloorView floorView = (ReceivePropertyFloorView) viewMap.get("FLOOR_VIEW_AS_PARAM");	
			
				String loggedInUser = getLoggedInUser();
				Long propertyId = Convert.toLong(floorView.getPropertyId());
				int noOfFloors = Convert.toInteger(endNumber.getValue())-Convert.toInteger(startNumber.getValue());
				if(noOfFloors>0 || Convert.toInteger(endNumber.getValue())==Convert.toInteger(startNumber.getValue())){
					getFacesContext().getExternalContext().getSessionMap().remove("FLOOR_VIEW_AS_PARAM");
				int floorNumberFrom = Convert.toInteger(startNumber.getValue());
	
				int floorNumberTo = floorNumberFrom + noOfFloors;
	//			String floorNamePrefix = inputTextFloorNamePrefix.getValue().toString();
				String floorNumberPrefix = floorPrefix.getValue().toString();
							
				ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();
				PropertyServiceAgent psAgent = new PropertyServiceAgent();
				UnitView unitView;
				ArrayList<FloorView> propertyFloorViews = new ArrayList<FloorView>();
				
				Double minSeq= 0D;
				Double maxSeq= 0D;
				
				minSeq = floorView.getFloorSequence();			 
				maxSeq = psAgent.getMinimumFloorSequence(propertyId,minSeq);
				 if(maxSeq == null || maxSeq < ReceiveProperty.minimumFloorSequence )
					 maxSeq = ReceiveProperty.maximumFloorSequence;
				 
			 
			 Double steps = (maxSeq - minSeq) / (floorNumberTo -floorNumberFrom + 2);
			
			 
			for(int index = floorNumberFrom; index <= floorNumberTo; index ++ ) 
			{
				FloorView propertyFloorView = new FloorView();
				propertyFloorView.setCreatedBy(loggedInUser);
				propertyFloorView.setCreatedOn(new Date());
				propertyFloorView.setFloorNumber(floorNumberPrefix+index);
				propertyFloorView.setFloorName(	floorNumberPrefix+index);
				propertyFloorView.setFloorNo(Double.valueOf(index));
				propertyFloorView.setFloorSeperator(floorView.getFloorSeperator());
				propertyFloorView.setFloorPrefix(floorPrefix.getValue().toString());
				propertyFloorView.setIsDeleted(new Long(0));
//				propertyFloorView.setFloorId(floorView.getFloorId());
				propertyFloorView.setRecordStatus(new Long(1));
				propertyFloorView.setPropertyId(propertyId);
				propertyFloorView.setTypeId(floorView.getTypeId());
				propertyFloorView.setUpdatedBy(loggedInUser);
				propertyFloorView.setUpdatedOn(new Date());
				
				Double floorSequence = minSeq + steps;
				propertyFloorView.setFloorSequence(floorSequence);
				minSeq = floorSequence;
				 
				 
				if(addWithUnitsVar!=null&&addWithUnitsVar){
				Iterator itUnit = floorView.getUnitView().iterator();
				
				while(itUnit.hasNext()){
					ReceivePropertyUnitView receivePropertyUnitView = (ReceivePropertyUnitView)itUnit.next();
					UnitView unitViewTemp = new UnitView();
					unitViewTemp.setCreatedBy(receivePropertyUnitView.getCreatedBy());
					unitViewTemp.setCreatedOn(receivePropertyUnitView.getCreatedOn());
					unitViewTemp.setUnitTypeId(receivePropertyUnitView.getUnitTypeId());
					unitViewTemp.setFloorNumber(receivePropertyUnitView.getFloorNumber());
					unitViewTemp.setInvestmentTypeId(receivePropertyUnitView.getInvestmentTypeId());
					unitViewTemp.setIsDeleted(receivePropertyUnitView.getIsDeleted());
					unitViewTemp.setNoOfBath(receivePropertyUnitView.getNoOfBath());
					unitViewTemp.setNoOfBed(receivePropertyUnitView.getNoOfBed());
					unitViewTemp.setNoOfLiving(receivePropertyUnitView.getNoOfLiving());
					unitViewTemp.setRentValue(receivePropertyUnitView.getRentValue());
					unitViewTemp.setRequiredPrice(receivePropertyUnitView.getRequiredPrice());
					unitViewTemp.setUnitArea(receivePropertyUnitView.getUnitArea());
					unitViewTemp.setIncludeFloorNumber(receivePropertyUnitView.getIncludeFloorNumber());
					if(receivePropertyUnitView.getIncludeFloorNumber()!=null&&
							receivePropertyUnitView.getIncludeFloorNumber().toString().equals("1")){
						
					}
					int unitNo = receivePropertyUnitView.getUnitNo().intValue();
					String unitPrefix = (receivePropertyUnitView.getUnitPrefix()!=null && receivePropertyUnitView.getUnitPrefix().trim().length()>0)?
							             receivePropertyUnitView.getUnitPrefix():"";
					/*if(receivePropertyUnitView.getUnitSeperator()==null)
						unitViewTemp.setUnitNumber(propertyFloorView.getFloorNumber()+"0"+unitNo);
					*///if(receivePropertyUnitView.getUnitSeperator()!=null)
						unitViewTemp.setUnitNumber(propertyFloorView.getFloorNumber()+unitPrefix+unitNo);
					
					unitViewTemp.setUnitNo(receivePropertyUnitView.getUnitNo());
					unitViewTemp.setUnitPrefix(receivePropertyUnitView.getUnitPrefix());
//					unitViewTemp.setUnitSeperator(receivePropertyUnitView.getUnitSeperator());
					unitViewTemp.setUnitSideTypeId(receivePropertyUnitView.getUnitSideTypeId());
					unitViewTemp.setUpdatedBy(receivePropertyUnitView.getUpdatedBy());
					unitViewTemp.setUpdatedOn(receivePropertyUnitView.getUpdatedOn());
					unitViewTemp.setUsageTypeId(receivePropertyUnitView.getUsageTypeId());
					unitViewTemp.setStatusId(receivePropertyUnitView.getStatusId());
					propertyFloorView.getUnitView().add(unitViewTemp);
				}
				
					
				
				}
				propertyFloorViews.add(propertyFloorView);
			}
			
			propertyFloorViews = psAgent.generateFloors(propertyId,propertyFloorViews);
	
			if(propertyFloorViews!= null && propertyFloorViews.size() >0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ALREADY_EXISTS));                
				 for(FloorView fv: propertyFloorViews) 
				 {
					 {
						 errorMessages.add(fv.getFloorNumber()+" ; "+fv.getFloorName());
						 logger.logInfo("Floor Number: "+floorView.getFloorNumber()+" Floor Name: "+fv.getFloorName() );
					 }
				 }
			}
			else
			{				
				getFacesContext().getExternalContext().getSessionMap().put("FLOORS_DATA_LIST_FROM_POPUP",true);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				String javaScriptText = "javascript:window.opener.document.forms[0].submit();window.close();";
				// Add the Javascript to the rendered page's header for immediate execution
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
				}
			}			
        }

		}


		catch(Exception ex)
		{
			logger.logError("Crashed while generating floors:",ex);
			ex.printStackTrace();

		}
	}
	 public String getErrorMessages() {
		 return CommonUtil.getErrorMessages(errorMessages);
	 }


	 public void setErrorMessages(List<String> errorMessages) {
		 this.errorMessages = errorMessages;
	 }

}
