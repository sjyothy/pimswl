package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;


import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.UnitView;

public class UnitEvaluationDetailsTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(UnitEvaluationDetailsTabBacking.class);
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	UnitView unitView=new UnitView();
	
	private String unitUsage;
	private String unitType;
	private String investmentType;
	private String unitSide;
	private String unitStatus;
	private String unitState;
	private String unitNumber;
	private String propertyName;
	private String propertyNumber;
	private String endowedName;
	private String floorNumber;
	private String noOfBeds;
	private String dewaAccNo;
	private String unitDesc;
	private String unitRentAmount;
	private boolean popUp=false;
		
	public void init()
	{
		logger.logInfo("init() started...");
		super.init();
		try
		{
			setValuesToMap();
			if(sessionMap.containsKey("IS_POPUP"))
			{
				popUp=(Boolean) sessionMap.get("IS_POPUP");
				sessionMap.remove("IS_POPUP");
			}
			else if(viewMap.containsKey("IS_POPUP"))
				popUp=(Boolean) viewMap.get("IS_POPUP");
		}
		catch (Exception e) 
		{
			logger.LogException("init() Crashed ", e) ;
		}
		logger.logInfo("init() Finished...");
	}
	@SuppressWarnings("unchecked")
	private void setValuesToMap() 
	{
		if(viewMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT) && viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT) != null)
		{
			unitView=(UnitView) viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);
			
			if(unitView.getUnitId()!=null)
				viewMap.put("UNIT_ID",unitView.getUnitId());
			if(unitView.getUsageTypeId()!=null)
				viewMap.put("USAGE_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getUsageTypeEn():unitView.getUsageTypeAr());
			if(unitView.getStatusId()!=null)
				viewMap.put("UNIT_STATUS",CommonUtil.getIsEnglishLocale()?unitView.getStatusEn():unitView.getStatusAr());
			if(unitView.getUnitNumber()!=null)
				viewMap.put("UNIT_NUMBER",unitView.getUnitNumber());
			if(unitView.getUsageTypeId()!=null)
				viewMap.put("UNIT_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getUnitTypeEn():unitView.getUnitTypeAr());
			if(unitView.getInvestmentTypeId()!=null)
				viewMap.put("INVESTMENT_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getInvestmentTypeEn():unitView.getInvestmentTypeAr());
			if(unitView.getUnitSideTypeId()!=null)
				viewMap.put("UNIT_SIDE",CommonUtil.getIsEnglishLocale()?unitView.getUnitSideTypeEn():unitView.getUnitSideTypeAr());
			if(unitView.getStateEn()!=null || unitView.getStateAr()!=null)
				viewMap.put("UNIT_STATE",CommonUtil.getIsEnglishLocale()?unitView.getStateEn():unitView.getStateAr());
			if(unitView.getPropertyCommercialName()!=null)
				viewMap.put("PROPERTY_NAME",unitView.getPropertyCommercialName());
			if(unitView.getPropertyNumber()!=null)
				viewMap.put("PROPERTY_NUMBER",unitView.getPropertyNumber());
			if(unitView.getPropertyEndowedName()!=null)
				viewMap.put("ENDOWED_NAME",unitView.getPropertyEndowedName());
			if(unitView.getFloorNumber()!=null)
				viewMap.put("FLOOR_NUMBER",unitView.getFloorNumber());
			if(unitView.getNoOfBed()!=null)
				viewMap.put("NO_OF_BEDS",unitView.getNoOfBed());
			if(unitView.getDewaNumber()!=null)
				viewMap.put("DEWA_ACC_NO",unitView.getDewaNumber());
			if(unitView.getUnitDesc()!=null)
				viewMap.put("UNIT_DESC",unitView.getUnitDesc());
			if(unitView.getRentValue()!=null)
				viewMap.put("UNIT_RENT_AMOUNT",unitView.getRentValue());
		}
		
		//put remaining values
	
	}
	
	public String openUnitsPopUp()
	{
		String methodName="openUnitsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
        //WebConstants.UNIT
 
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText ="var screen_width = 1024;"+
        "var screen_height = 450;"+
        "var popup_width = screen_width-200;"+
        "var popup_height = screen_height;"+
        "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"+
        "window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
        openPopUp("UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP", javaScriptText);
          
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/LeaseContract.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public UnitView getUnitView() 
	{
		return unitView;
	}
	public void setUnitView(UnitView unitView) 
	{
		this.unitView = unitView;
	}
	public String getUnitUsage() 
	{
		if(viewMap.containsKey("USAGE_TYPE"))
			unitUsage=viewMap.get("USAGE_TYPE").toString();
		return unitUsage;
	}
	public void setUnitUsage(String unitUsage) {
		this.unitUsage = unitUsage;
	}
	public String getUnitType() 
	{
		if(viewMap.containsKey("UNIT_TYPE"))
			unitType=viewMap.get("UNIT_TYPE").toString();
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getInvestmentType()
	{
		if(viewMap.containsKey("INVESTMENT_TYPE"))
			investmentType=viewMap.get("INVESTMENT_TYPE").toString();
		return investmentType;
	}
	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}
	public String getUnitSide() 
	{
		if(viewMap.containsKey("UNIT_SIDE"))
			unitSide=viewMap.get("UNIT_SIDE").toString();
		return unitSide;
	}
	public void setUnitSide(String unitSide) {
		this.unitSide = unitSide;
	}
	public String getUnitStatus() 
	{
		if(viewMap.containsKey("UNIT_STATUS"))
			unitStatus=viewMap.get("UNIT_STATUS").toString();
		return unitStatus;
	}
	public void setUnitStatus(String unitStatus) {
		this.unitStatus = unitStatus;
	}
	public String getUnitState()
	{	
		if(viewMap.containsKey("UNIT_STATE"))
			unitState=viewMap.get("UNIT_STATE").toString();
		return unitState;
	}
	public void setUnitState(String unitState) {
		this.unitState = unitState;
	}
	public String getUnitNumber() 
	{
		if(viewMap.containsKey("UNIT_NUMBER"))
			unitNumber=viewMap.get("UNIT_NUMBER").toString();
		return unitNumber;
	}
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}
	public String getPropertyName() 
	{
		if(viewMap.containsKey("PROPERTY_NAME"))
			propertyName=viewMap.get("PROPERTY_NAME").toString();
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyNumber() 
	{
		if(viewMap.containsKey("PROPERTY_NUMBER"))
			propertyNumber=viewMap.get("PROPERTY_NUMBER").toString();
		return propertyNumber;
	}
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}
	public String getEndowedName() 
	{
		if(viewMap.containsKey("ENDOWED_NAME"))
			endowedName=viewMap.get("ENDOWED_NAME").toString();
		return endowedName;
	}
	public void setEndowedName(String endowedName) {
		this.endowedName = endowedName;
	}
	public String getFloorNumber() 
	{
		if(viewMap.containsKey("FLOOR_NUMBER"))
			floorNumber=viewMap.get("FLOOR_NUMBER").toString();
		return floorNumber;
	}
	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}
	public String getNoOfBeds() 
	{
		if(viewMap.containsKey("NO_OF_BEDS"))
			noOfBeds= viewMap.get("NO_OF_BEDS").toString();
		return noOfBeds;
	}
	public void setNoOfBeds(String noOfBeds) {
		this.noOfBeds = noOfBeds;
	}
	public String getDewaAccNo() 
	{
		if(viewMap.containsKey("DEWA_ACC_NO"))
			dewaAccNo=viewMap.get("DEWA_ACC_NO").toString();
		return dewaAccNo;
	}
	public void setDewaAccNo(String dewaAccNo) {
		this.dewaAccNo = dewaAccNo;
	}
	public String getUnitDesc() 
	{
		if(viewMap.containsKey("UNIT_DESC"))
			unitDesc=viewMap.get("UNIT_DESC").toString();
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public String getUnitRentAmount() 
	{
		if(viewMap.containsKey("UNIT_RENT_AMOUNT"))
			unitRentAmount=viewMap.get("UNIT_RENT_AMOUNT").toString();
		return unitRentAmount;
	}
	public void setUnitRentAmount(String unitRentAmount) {
		this.unitRentAmount = unitRentAmount;
	}
	public boolean isPopUp() 
	{
		if(viewMap.containsKey("IS_POPUP"))
			popUp=(Boolean) viewMap.get("IS_POPUP");
		return popUp;
	}
	public void setPopUp(boolean popUp) {
		this.popUp = popUp;
	}
	public boolean getRequestAvailable()
	{
		boolean isAvailable = false;
		if(viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID) != null)
			isAvailable = true;
		return isAvailable;	
	}
}

