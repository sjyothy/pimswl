package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.TenderFilterView;


public class ConstructionTenderSearchBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(TenderSearchBacking.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
    public interface Keys {
    	public static final String 	WINNER_CONTRACTOR_PRESENT="WINNER_CONTRACTOR_PRESENT";
		public static final String NOT_BINDED_WITH_CONTRACT="NOT_BINDED_WITH_CONTRACT";
		public static final String TENDER_TYPE="TENDER_TYPE";
		public static final String DD_TENDER_TYPE="DD_TENDER_TYPE";
		
	}
	private TenderFilterView searchFilterView = new TenderFilterView();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	
	private TenderFilterView dataItem = new TenderFilterView();
	private List<TenderFilterView> dataList = new ArrayList<TenderFilterView>();
	
	private HtmlSelectOneMenu tenderTypeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu tenderStatusCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu constructionServiceTypeCombo = new HtmlSelectOneMenu();	
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private HtmlDataTable dataTable;
	private String  CONTEXT="context";
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	List<SelectItem> tenderStatusList = new ArrayList<SelectItem>();
	List<SelectItem> constructionServiceTypeList = new ArrayList<SelectItem>();
	
	
	String VIEW_MODE="VIEW_MODE";
	
	
	public ConstructionTenderSearchBacking(){
		
	}
	

	/*
	 * Methods
	 */
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		return CommonUtil.getLoggedInUser();
	}
	
	@SuppressWarnings("unchecked")
	public String onManage(){
		String METHOD_NAME = "onManage()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, tenderId);
	   		sessionMap.put(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_MANAGE);	   		
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONSTRUCTION_TENDER_SEARCH);	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "constructionTenderManage";
	}
	
	public String onReceiveProposal(){
		String METHOD_NAME = "onReceiveProposal()";
		String tenderTypeId = null;
		String taskType = "";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put(WebConstants.Tender.TENDER_ID, tenderId);
	   		sessionMap.put(WebConstants.Tender.TENDER_MODE, WebConstants.Tender.TENDER_MODE_RECEIVE_PROPOSAL);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TENDER_SEARCH);
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "constructionTenderReceiveProposal";
	}
	
	public String onOpenProposal(){
		String METHOD_NAME = "onOpenProposal()";
		String tenderTypeId = null;
		String taskType = "";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put(WebConstants.Tender.TENDER_ID, tenderId);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TENDER_SEARCH);
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "constructionTenderOpenProposal";
	}
	
	public String onPurchaseTender() {
		
		String METHOD_NAME = "onPurchaseTender()";		
		
		logger.logInfo(METHOD_NAME + " started...");
		
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, tenderId );
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TENDER_SEARCH);	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "constructionTenderPurchaseDocument";
	}

	public String getBundleMessage(String key){
    	return CommonUtil.getBundleMessage(key);
    }
	
	@SuppressWarnings("unchecked")
	public String onReopen(){
		String METHOD_NAME = "onReopen()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		Boolean success = constructionServiceAgent.setTenderAsReopened(tenderId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.Tender.MSG_TENDER_REOPEN_SUCCESS);
	   			/*int index = dataTable.getRowIndex();
	   	    	dataList = (List<TenderFilterView>) viewMap.get("tenderList");
	   	    	
	   	    	ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
	   			DomainDataView reOpenedStatusDD = appBean.getDomainData(Constant.TENDER_STATUS, Constant.TENDER_STATUS_REOPENED);
	   			if(reOpenedStatusDD!=null){
	   				TenderFilterView temp = dataList.remove(index);
	   				String tenderStatus = "";
	   				tenderStatus = getIsEnglishLocale()?reOpenedStatusDD.getDataDescEn():reOpenedStatusDD.getDataDescAr();
	   				temp.setTenderStatus(tenderStatus);
	   				dataList.add(index, temp);
	   				viewMap.put("tenderList", dataList);
	   			}
	   	    	
	   	    	viewMap.put("tenderList",dataList);*/
	   			loadTenderList();
	   		}
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.Tender.MSG_TENDER_REOPEN_FAILURE));
		}	
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String onCancel(){
		String METHOD_NAME = "onCancel()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		Boolean success = constructionServiceAgent.setTenderAsCancelled(tenderId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.Tender.MSG_TENDER_CANCEL_SUCCESS);
	   			/*int index = dataTable.getRowIndex();
	   	    	dataList = (List<TenderFilterView>) viewMap.get("tenderList");
	   	    	
	   	    	ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
	   			DomainDataView reOpenedStatusDD = appBean.getDomainData(Constant.TENDER_STATUS, Constant.TENDER_STATUS_CANCELLED);
	   			if(reOpenedStatusDD!=null){
	   				TenderFilterView temp = dataList.remove(index);
	   				String tenderStatus = "";
	   				tenderStatus = getIsEnglishLocale()?reOpenedStatusDD.getDataDescEn():reOpenedStatusDD.getDataDescAr();
	   				temp.setTenderStatus(tenderStatus);
	   				dataList.add(index, temp);
	   				viewMap.put("tenderList", dataList);
	   			}
	   	    	
	   	    	viewMap.put("tenderList",dataList);*/
	   			loadTenderList();
	   		}
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.Tender.MSG_TENDER_CANCEL_FAILURE));
		}	
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, tenderId);
	   		sessionMap.put(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_VIEW);	   		
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONSTRUCTION_TENDER_SEARCH);
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "constructionTenderManage";
	}
	
	@SuppressWarnings("unchecked")
	public String onEdit(){
		String METHOD_NAME = "onEdit()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		sessionMap.put(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, tenderId);
	   		sessionMap.put(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_EDIT);	   		
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONSTRUCTION_TENDER_SEARCH);	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "constructionTenderManage";
	}
	
	@SuppressWarnings("unchecked")
	public String onDelete(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
	   		TenderFilterView tenderFilterView=(TenderFilterView)dataTable.getRowData();
	   		Long tenderId = tenderFilterView.getTenderId();
	   		Boolean success = constructionServiceAgent.deleteTender(tenderId, getLoggedInUser());
	   		if(success){
	   			infoMessage = getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   	    	dataList = (List<TenderFilterView>) viewMap.get("tenderList");
	   	    	TenderFilterView temp = dataList.remove(index);
	   	    	viewMap.put("tenderList",dataList);
	   	    	viewMap.put("recordSize",dataList.size());
	   		}
	   		
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
			errorMessages.add(getBundleMessage(MessageConstants.ExtendApplication.MSG_REQUEST_DELETE_FAILURE));
		}	
		return "";
	}
	
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		
		/*if(viewMap.get(Keys.NOT_BINDED_WITH_CONTRACT)!=null)
			argMap.put("isNotBindedWithContract",true); 
		if(viewMap.get(Keys.WINNER_CONTRACTOR_PRESENT)!=null)
			argMap.put("isWinnerContractorPresent",true);*/
		
		if(searchFilterView.getTenderStatusId()!=null  && searchFilterView.getTenderStatusId().equals("0"))
			
		{
			ArrayList<Long> statuses = new ArrayList<Long>();
		    List<SelectItem> StatusList =  getTenderStatusList();
			for(SelectItem item :StatusList)
			{
				Object Stat = item.getValue();
				
				if(Stat != null && Stat.toString().trim().length()>0)
					statuses.add(Long.parseLong(Stat.toString()));					
				
			}
			
			if(statuses.size() >0)
				argMap.put("TENDER_STATUS_IN",statuses);
	     }
       if(searchFilterView.getTenderTypeId()!=null  && searchFilterView.getTenderTypeId().equals("0"))
			
		{
			ArrayList<Long> types = new ArrayList<Long>();
		    List<SelectItem> typeList =  getConstructionServiceTypeList();
			for(SelectItem item :typeList)
			{
				Object typ = item.getValue();
				
				if(typ != null && typ.toString().trim().length()>0)
					types.add(Long.parseLong(typ.toString()));					
				
			}
			
			if(types.size() >0)
				argMap.put("TENDER_TYPE_IN",types);
	     }
			
		return argMap;
	}
	
	private void loadTenderList() throws PimsBusinessException{
		logger.logInfo("loadTenderList() started...");
    	try{
			dataList = new ArrayList<TenderFilterView>();
			Map argMap = getArgMap();
			argMap.put("PROJECT_IS_NOT_NULL",true);
			List<TenderFilterView> tenderViewList = constructionServiceAgent.getTenders(searchFilterView, argMap);
			viewMap.put("tenderList", tenderViewList);
			recordSize = tenderViewList.size();
			viewMap.put("recordSize", recordSize);
			if(tenderViewList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadTenderList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadTenderList() crashed ",exception);
		}
		catch(Exception exception){
			logger.LogException("loadTenderList() crashed ",exception);
		}
	}
	
	public Boolean validateFields() {
		if(StringHelper.isNotEmpty(searchFilterView.getTenderNumber()))
			return true;
		if(viewMap.get(Keys.DD_TENDER_TYPE)!=null ||( StringHelper.isNotEmpty(searchFilterView.getTenderTypeId()) && 
				!searchFilterView.getTenderTypeId().equals("0")))
		{
			if ( viewMap.get("TENDER_TYPE_ID") != null )
				searchFilterView.setTenderTypeId(viewMap.get("TENDER_TYPE_ID").toString());			
			return true;
				
		}
		if(searchFilterView.getTenderIssueDateFrom()!=null)
			return true;
		if(searchFilterView.getTenderIssueDateTo()!=null)
			return true;			
		if(searchFilterView.getTenderEndDateFrom()!=null)
			return true;
		if(searchFilterView.getTenderEndDateTo()!=null)
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getPropertyName()))
			return true;
		if(StringHelper.isNotEmpty(searchFilterView.getTenderDescription()))
			return true;
		
		if( StringHelper.isNotEmpty( searchFilterView.getTenderStatusId() ) && ! searchFilterView.getTenderStatusId().equalsIgnoreCase("0") )
			return true;
		
		if( StringHelper.isNotEmpty( searchFilterView.getConstructionServiceTypeId() ) && ! searchFilterView.getConstructionServiceTypeId().equalsIgnoreCase("0") )			
			return true;
		
		if( StringHelper.isNotEmpty( searchFilterView.getProjectNumber() ) )			
			return true;
		
		if( StringHelper.isNotEmpty( searchFilterView.getProjectName() ) )			
			return true;
		
		return false;
	}
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
				if(request.getParameter(VIEW_MODE)!=null)
					viewMap.put(VIEW_MODE, request.getParameter(VIEW_MODE).toString());
			  
			  // because now new status of winner selected in construction tender we do'not use these request parameters 
				/*if(request.getParameter(Keys.NOT_BINDED_WITH_CONTRACT)!=null)
					viewMap.put(Keys.NOT_BINDED_WITH_CONTRACT,request.getParameter(Keys.NOT_BINDED_WITH_CONTRACT));
				if(request.getParameter(Keys.TENDER_TYPE)!=null)
					viewMap.put(Keys.TENDER_TYPE,request.getParameter(Keys.TENDER_TYPE));
				if(request.getParameter(Keys.WINNER_CONTRACTOR_PRESENT)!=null)
					viewMap.put(Keys.WINNER_CONTRACTOR_PRESENT,request.getParameter(Keys.WINNER_CONTRACTOR_PRESENT));*/
				
				if (request.getParameter(CONTEXT)!=null )
		    	 {
	    			viewMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
		    	 }
				
				if(getIsContextTenderInquiry())
					searchFilterView.setTenderEndDateFrom(new Date());
				
				LoadTenderStatuses();
				LoadTenderType();
			}
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		CommonUtil commonUtil = new CommonUtil();
		DomainDataView ddView=null;
		if(viewMap.get(Keys.TENDER_TYPE)!=null )
		{
			tenderTypeCombo.setReadonly(true);
			if(viewMap.get(Keys.DD_TENDER_TYPE)==null )
			{
				ddView= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.Tender.TENDER_TYPE), 
					viewMap.get(Keys.TENDER_TYPE).toString());
				viewMap.put(Keys.DD_TENDER_TYPE,ddView);
			}
			else
				ddView =(DomainDataView)viewMap.get(Keys.DD_TENDER_TYPE); 
			
			searchFilterView.setTenderTypeId(ddView.getDomainDataId().toString());
			viewMap.put("TENDER_TYPE_ID",ddView.getDomainDataId().toString());
		}
	}
	
	 private void LoadTenderStatuses()
     {
    	 List<SelectItem> allowedTenderStatuses = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.Tender.TENDER_STATUS);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(getIsContextConstructionContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_TENDER_STATUS_WINNER_SELECTED)))  )
    			   isAdd = false;
    			 
    			 else if(getIsContextConsultantContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_TENDER_STATUS_WINNER_SELECTED)))  )
                   isAdd = false;
    			 
    			 else if(getIsContextBOTContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_TENDER_STATUS_WINNER_SELECTED)))  )
                   isAdd = false;
    			 else if(getIsContextTenderInquiry() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_TENDER_STATUS_APPROVED)))  )
                     isAdd = false;
    			 else if(getIsContextExtendApplication() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_TENDER_STATUS_APPROVED)))  )
                     isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedTenderStatuses.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedTenderStatuses!= null && allowedTenderStatuses.size()>1)
			 Collections.sort(allowedTenderStatuses,ListComparator.LIST_COMPARE);
		
    	 viewMap.put("ALLOWED_TENDER_STATUSES",allowedTenderStatuses);
     }
	
	 
	 private void LoadTenderType()
     {
    	 List<SelectItem> allowedTenderTypes = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.Tender.CONSTRUCTION_SERVICE_TYPE);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(getIsContextConstructionContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_SERVICE_TYPE_CONSTRUCTION)))  )
    				 isAdd = false;
    			 else if(getIsContextConsultantContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_SERVICE_TYPE_CONSULTATION)))  )
    				 isAdd = false;
    			 else if(getIsContextBOTContract() && ( !(dd.getDataValue().equals(WebConstants.CONSTRUCTION_SERVICE_TYPE_CONSTRUCTION)))  )
    				 isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedTenderTypes.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedTenderTypes!= null && allowedTenderTypes.size()>1)
			 Collections.sort(allowedTenderTypes,ListComparator.LIST_COMPARE);
		
    	 viewMap.put("ALLOWED_TENDER_TYPE",allowedTenderTypes);
     }
	
	 public boolean getIsContextConstructionContract()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ConstructionTenderSearchContext.CONSTRUCTION_CONTRACT))
			{
				return  true;
			}
			else
				return false;
			
		}
	 
	 public boolean getIsContextConsultantContract()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ConstructionTenderSearchContext.CONSULTANT_CONTRACT))
			{
				return  true;
			}
			else
				return false;
			
		}
	 public boolean getIsContextBOTContract()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ConstructionTenderSearchContext.BOT_CONTRACT))
			{
				return  true;
			}
			else
				return false;
			
		}
	 public boolean getIsContextTenderInquiry()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ConstructionTenderSearchContext.TENDER_INQUIRY))
			{
				return  true;
			}
			else
				return false;
			
		}
	 public boolean getIsContextExtendApplication()
		{
			
			if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ConstructionTenderSearchContext.EXTEND_APPLICATION))
			{
				return  true;
			}
			else
				return false;
			
		}
	@SuppressWarnings("unchecked")
	public String onAddTender()
    {
    	logger.logInfo("onAddTender() started...");
    	try{
    		sessionMap.put(WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, null);
	   		sessionMap.put(WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_NEW);	   		
    		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_CONSTRUCTION_TENDER_SEARCH);
    		logger.logInfo("onAddTender() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddTender() crashed ",exception);
    	}
        return "constructionTenderManage";
    }
	
    public String searchTenders()
    {
    	logger.logInfo("searchTenders() started...");
    	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    	Date currentDate = new Date();
    	try{
    		errorMessages = new ArrayList<String>();
    		if(validateFields())
    		{	
    			// when we r comming from tender inquiry need tender end day greater than today 
     		   if(getIsContextTenderInquiry())
     		   {
     			                                    
     			   if(searchFilterView.getTenderEndDateFrom()==null || 
     				((formatter.parse(formatter.format(searchFilterView.getTenderEndDateFrom()))).before(formatter.parse(formatter.format( currentDate))))) 
     			   {
     				   errorMessages.add(getBundleMessage("tenderSearch.msg.dateSelect"));
     				   return "Done";
     			   }
     			   
     		   }
    			loadTenderList();
    		}	
    		else
    			errorMessages.add(getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
    			
    		logger.logInfo("searchTenders() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchTenders() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchTenders() crashed ",exception);
    	}
        return "searchTenders";
    }
	
    
    	
	/*
	 * Setters / Getters
	 */
    

	/**
	 * @return the dataItem
	 */
	public TenderFilterView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(TenderFilterView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<TenderFilterView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<TenderFilterView>)viewMap.get("tenderList");
		if(dataList==null)
			dataList = new ArrayList<TenderFilterView>();
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<TenderFilterView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public TenderFilterView getSearchFilterView() {
		return searchFilterView;
	}


	public void setSearchFilterView(TenderFilterView searchFilterView) {
		this.searchFilterView = searchFilterView;
	}


	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }


	public HtmlSelectOneMenu getTenderTypeCombo() {
		return tenderTypeCombo;
	}


	public void setTenderTypeCombo(HtmlSelectOneMenu tenderTypeCombo) {
		this.tenderTypeCombo = tenderTypeCombo;
	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}

	public Boolean getIsViewModePopUp()
	 {
		if(viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE)!=null && viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase("popup"))
			return true;
		return false;
	 }


	public HtmlSelectOneMenu getTenderStatusCombo() {
		return tenderStatusCombo;
	}


	public void setTenderStatusCombo(HtmlSelectOneMenu tenderStatusCombo) {
		this.tenderStatusCombo = tenderStatusCombo;
	}


	public HtmlSelectOneMenu getConstructionServiceTypeCombo() {
		return constructionServiceTypeCombo;
	}


	public void setConstructionServiceTypeCombo(
			HtmlSelectOneMenu constructionServiceTypeCombo) {
		this.constructionServiceTypeCombo = constructionServiceTypeCombo;
	}


	public List<SelectItem> getTenderStatusList() {
		 List<SelectItem> allowedTenderStatuses = new ArrayList<SelectItem>(); 	 
    	 if(viewMap.containsKey("ALLOWED_TENDER_STATUSES"))
    		 allowedTenderStatuses = (List<SelectItem>) viewMap.get("ALLOWED_TENDER_STATUSES");
		return allowedTenderStatuses;
	}


	public void setTenderStatusList(List<SelectItem> tenderStatusList) {
		this.tenderStatusList = tenderStatusList;
	}


	public List<SelectItem> getConstructionServiceTypeList() {
		List<SelectItem> allowedTenderType = new ArrayList<SelectItem>(); 	 
	   	 if(viewMap.containsKey("ALLOWED_TENDER_TYPE"))
	   		allowedTenderType = (List<SelectItem>) viewMap.get("ALLOWED_TENDER_TYPE");
		return allowedTenderType;
	}


	public void setConstructionServiceTypeList(
			List<SelectItem> constructionServiceTypeList) {
		this.constructionServiceTypeList = constructionServiceTypeList;
	}
	
}

