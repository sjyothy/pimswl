package com.avanza.pims.web.backingbeans;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.custom.checkbox.HtmlCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.*;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;



public class InquiryApplicationSearch extends AbstractController
{
	private static Logger logger = Logger.getLogger( InquiryApplicationSearch.class );
	
	private HtmlDataTable dataTable;
	private Double unitAreaMin = null;
	private Double unitAreaMax = null;
	private Long noOfLivingMin = null;
	private Long noOfLivingMax = null;
	private String noOfBedsMax = null;
	private Long noOfBathMin = null;
	private Long noOfBathMax = null;
	
	private String floorNumber = null;
	
	//private Calendar occupiedTillDate = new Calendar();
	
	//private Date inquiryDate = new Date();
	
	
	private String unitId;
	private String createdBy;
	private String createdOn;
	
	
	
	
	private HtmlSelectOneMenu unitTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitUsageSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitInvestmentSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitSideSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu inquiryMethodSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu prioritySelectMenu= new HtmlSelectOneMenu();
	
	
	String selectOneUnitUsage;
	String selectOneUnitSide;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	
	List<SelectItem> unitType= new ArrayList();
	List<SelectItem> unitUsage= new ArrayList();
	List<SelectItem> unitInvestment= new ArrayList();
	List<SelectItem> unitSide= new ArrayList();
	
	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectBooleanCheckbox  checkBox=new HtmlSelectBooleanCheckbox();
	InquiryView inquiryView = new InquiryView();
	List<SelectItem> city= new ArrayList();
	List<SelectItem> state= new ArrayList();
	List<SelectItem> country= new ArrayList();
	
	private List<SelectItem> propertyAreaList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	
	private String countryId;
	private String stateId;
	private String cityId;
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	
	
	FacesContext context=FacesContext.getCurrentInstance();
	private ServletContext context1 = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    Map sessionMap;
    CommonUtil	commonUtil = new CommonUtil();
    Map viewRootMap=context.getViewRoot().getAttributes();
    Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    private List<String> errorMessages;
	private List<String> successMessages;
	
	private Date applicationDateFrom;
	private Date applicationDateTo;
	
	private String applicationNumber;
	private String inquirerName;
	private String cellPhone;
	private String personIdNumber;
	private String passportNumber;
	private String unitNumber ;
	private String propertyName;
	private String rentValueMin ;
	private String rentValueMax ;
	private String noOfBedsMin;
	
	private String selectOneApplicationStatus;
	private String selectOneApplicationMethod;
	private String selectOneApplicationPriority;
	private String selectOneUnitType;
	private String selectOnePropertyType;
	private String selectOnePropertyArea;
	
	private Map<String,List<SelectItem>> listMap = new HashMap<String,List<SelectItem>>();
	private List<SelectItem> emptyList = new ArrayList<SelectItem>();
	
     @Override 
	 public void init() 
     {
    	
    	 super.init();
    	 try
    	 {
    		 errorMessages = new ArrayList<String>();
    		 successMessages = new ArrayList<String>();
    		 System.out.println("Testing");
    		 loadPropertyArea();
    		 sessionMap=context.getExternalContext().getSessionMap();
    		 
    		 
    		if (!isPostBack()){
    			loadCombos();
    		 if (dataList == null || dataList.size()==0)
    		{
    			 //   noOfLivingMin=noOfLivingMin;
    			//dataList= loadDataList(); 
    		}
    	  } 
    	 }
    	 catch(Exception es)
    	 {
    		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		 
    	 }
	        
	 }
	 
     private void loadCombos() throws PimsBusinessException
     {
    	 
    	 if(!sessionMap.containsKey(WebConstants.UNIT_TYPE))
    	   loadUnitTypeList();
    	 if(!sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
      	   loadUnitSideTypeList();
    	 if(!sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
    	   loadUnitUsageList(); 
    	 
     }
     
     private void loadUnitTypeList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_TYPE);
    	 sessionMap.put(WebConstants.UNIT_TYPE, ddl);
    	 
    	 
     }
     private void loadUnitSideTypeList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_SIDE_TYPE);
    	 sessionMap.put(WebConstants.UNIT_SIDE_TYPE, ddl);
    	 
     }
     private void  loadUnitUsageList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);
    	 sessionMap.put(WebConstants.PROPERTY_USAGE, ddl);
    	 
     }  
	 
		//We have to look for the list population for facilities
	
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private InquiryView dataItem = new InquiryView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<InquiryView> dataList = new ArrayList<InquiryView>();
  
	//	public Unit dataItem = new Unit();
	
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
	public List<InquiryView> getPropertyInquiryDataList()
	{
		
		
		if (dataList == null || dataList.size()==0)
		{
			if(viewMap.containsKey("PERSONLIST"))	
				  dataList= (ArrayList)viewMap.get("PERSONLIST");
			//else
				//loadDataList();
		}
		return dataList;
	}

	
	
	public String doBid() {
//		errorMessages = new ArrayList<String>();
/*		if (getCustomerId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
			
*/ 
		return "";
		}
	
	public void doSearch (){
		logger.logInfo("doSearch Start:::::::::::::::::::::");
		DateFormat df  =new SimpleDateFormat(getDateFormat());
		try {
		if ( noOfBedsMin!=null && ! noOfBedsMin.equals(""))
		{
		inquiryView.setNoOfBedsMin(Long.parseLong(noOfBedsMin));
		}
		if ( noOfBedsMax !=null && !noOfBedsMax.equals(""))
		{
		inquiryView.setNoOfBedsMax(Long.parseLong(noOfBedsMax));
		}
		if (rentValueMin!=null && !rentValueMin.equals(""))
		{
		inquiryView.setRentValueMin(Double.parseDouble(rentValueMin));
		}
		if (rentValueMax!=null && !rentValueMax.equals(""))
		{
		inquiryView.setRentValueMax(Double.parseDouble(rentValueMax));
		}
		if(selectOneUnitType!=null && !selectOneUnitType.equals("")&& !selectOneUnitType.equals("-1") )
		{
		inquiryView.setUnitType(new Long(selectOneUnitType));
		}
		if(selectOneUnitUsage !=null && !selectOneUnitUsage.equals("")&& !selectOneUnitUsage.equals("-1") ){
		inquiryView.setUnitUsageTypeId(new Long(selectOneUnitUsage));
		}
		if (unitNumber!=null && !unitNumber.equals("")){
		inquiryView.setUnitNumber(unitNumber);
		}
		if (countryId !=null && ! countryId.equals("")&& ! countryId.equals("-1")){
		inquiryView.setCountryId(Long.parseLong(countryId));
		}
		if (stateId!=null && !stateId.equals("")&& !stateId.equals("-1")){
		inquiryView.setStateId(Long.parseLong(stateId));
		}
		if (cityId!=null && !cityId.equals("")&& !cityId.equals("-1")){
		inquiryView.setCityId(Long.parseLong(cityId));
		}
//new Criterian
		if (inquirerName!=null && !inquirerName.equals("")){
		inquiryView.setInquirerName(inquirerName);
		}		
		if (cellPhone!=null && !cellPhone.equals("")){
		inquiryView.setCellPhone(cellPhone);
		}
		if (applicationDateFrom!=null && !applicationDateFrom.equals("")){
		inquiryView.setInquiryDateFrom(applicationDateFrom);
		}
		if (applicationDateTo!=null && !applicationDateTo.equals("")){
		inquiryView.setInquiryDateTo(applicationDateTo);
		}
		if (personIdNumber!=null && !personIdNumber.equals("")){
		inquiryView.setPersonIdNumber(personIdNumber);
		}
		if (passportNumber!=null && !passportNumber.equals("")){
		inquiryView.setPassportNumber(passportNumber);
		}
		if (propertyName!=null && !propertyName.equals("")){
		inquiryView.setPropertyName(propertyName);
		}
		if (selectOnePropertyType!=null && !selectOnePropertyType.equals("")&& !selectOnePropertyType.equals("-1")){
		inquiryView.setPropertyType(Long.parseLong(selectOnePropertyType));
		}
		if (selectOnePropertyArea!=null && !selectOnePropertyArea.equals("")&& !selectOnePropertyArea.equals("-1")){
		inquiryView.setPropertyArea(Long.parseLong(selectOnePropertyArea));
		}
		if (selectOneApplicationMethod!=null && !selectOneApplicationMethod.equals("")&& !selectOneApplicationMethod.equals("-1")){
		inquiryView.setInquiryReceiveMethodId(Long.parseLong(selectOneApplicationMethod));
		}
		if (selectOneApplicationPriority!=null && !selectOneApplicationPriority.equals("")&& !selectOneApplicationPriority.equals("-1")){
		inquiryView.setPriorityId(Long.parseLong(selectOneApplicationPriority));
		}
		if (selectOneApplicationStatus!=null && !selectOneApplicationStatus.equals("")&& !selectOneApplicationStatus.equals("-1")){
		inquiryView.setApplicationStatusId(Long.parseLong(selectOneApplicationStatus));
		}
		if (applicationNumber!=null && !applicationNumber.equals("")){
		inquiryView.setApplicationNumber(applicationNumber);
		}
		
		dataList.clear();
		loadDataList();
		logger.logInfo("doSearch Ends:::::::::::::::::::::");

            }
    catch (Exception e){
    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	logger.logInfo("doSearch Crashed:::::::::::::::::::::");
        System.out.println("Exception doSearch"+e);       
   }

}
	
	public List<InquiryView> loadDataList() 
	{
		errorMessages = new ArrayList<String>();
    	successMessages = new ArrayList<String>();
		 String methodName="loadDataList";
		 logger.logInfo(methodName+" Starts::::::::::::::::::::::");
		 
		 List<InquiryView> list = new ArrayList();
		
		try
		{
			PropertyServiceAgent myPort = new PropertyServiceAgent();
	          // System.out.println("calling " + myPort.getEndpoint());
	            
	           list =  myPort.getInquiryPerson(inquiryView);
	         
	           Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				viewMap.put("PERSONLIST", list);
				recordSize = list.size();
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
	           
//	           sessionMap.put("UNIT_LIST", list);
		        dataList=(ArrayList<InquiryView>) list;
		        
               logger.logInfo("Record Found In Load Data List : %s",dataList.size());
		        
		        if (dataList.size()==0){
		        	
		        	successMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
		        	
		        }
	           
     //          dataList =list;
		        logger.logInfo(methodName+" Ends::::::::::::::::::::::");
	    }
		catch (Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logInfo(methodName+" Creashed::::::::::::::::::::::");
	        ex.printStackTrace();
	    }
		return list;
		
	}
	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}	
	public String getSuccessMessages() {
		return commonUtil.getErrorMessages(this.successMessages);
	}

	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }

	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Double getUnitAreaMin() {
		return unitAreaMin;
	}



	public void setUnitAreaMin(Double unitAreaMin) {
		this.unitAreaMin = unitAreaMin;
	}



	public Double getUnitAreaMax() {
		return unitAreaMax;
	}



	public void setUnitAreaMax(Double unitAreaMax) {
		this.unitAreaMax = unitAreaMax;
	}



	public Long getNoOfLivingMin() {
		return noOfLivingMin;
	}



	public void setNoOfLivingMin(Long noOfLivingMin) {
		this.noOfLivingMin = noOfLivingMin;
	}



	public Long getNoOfLivingMax() {
		return noOfLivingMax;
	}



	public void setNoOfLivingMax(Long noOfLivingMax) {
		this.noOfLivingMax = noOfLivingMax;
	}


	public Long getNoOfBathMin() {
		return noOfBathMin;
	}



	public void setNoOfBathMin(Long noOfBathMin) {
		this.noOfBathMin = noOfBathMin;
	}



	public Long getNoOfBathMax() {
		return noOfBathMax;
	}



	public void setNoOfBathMax(Long noOfBathMax) {
		this.noOfBathMax = noOfBathMax;
	}





	public HtmlSelectOneMenu getUnitTypeSelectMenu() {
		return unitTypeSelectMenu;
	}



	public void setUnitTypeSelectMenu(HtmlSelectOneMenu unitTypeSelectMenu) {
		this.unitTypeSelectMenu = unitTypeSelectMenu;
	}



	public HtmlSelectOneMenu getUnitUsageSelectMenu() {
		return unitUsageSelectMenu;
	}



	public void setUnitUsageSelectMenu(HtmlSelectOneMenu unitUsageSelectMenu) {
		this.unitUsageSelectMenu = unitUsageSelectMenu;
	}



	public HtmlSelectOneMenu getUnitInvestmentSelectMenu() {
		return unitInvestmentSelectMenu;
	}



	public void setUnitInvestmentSelectMenu(
			HtmlSelectOneMenu unitInvestmentSelectMenu) {
		this.unitInvestmentSelectMenu = unitInvestmentSelectMenu;
	}



	public HtmlSelectOneMenu getUnitSideSelectMenu() {
		return unitSideSelectMenu;
	}



	public void setUnitSideSelectMenu(HtmlSelectOneMenu unitSideSelectMenu) {
		this.unitSideSelectMenu = unitSideSelectMenu;
	}



	public HtmlSelectOneMenu getInquiryMethodSelectMenu() {
		return inquiryMethodSelectMenu;
	}



	public void setInquiryMethodSelectMenu(HtmlSelectOneMenu inquiryMethodSelectMenu) {
		this.inquiryMethodSelectMenu = inquiryMethodSelectMenu;
	}



	public HtmlSelectOneMenu getPrioritySelectMenu() {
		return prioritySelectMenu;
	}



	public void setPrioritySelectMenu(HtmlSelectOneMenu prioritySelectMenu) {
		this.prioritySelectMenu = prioritySelectMenu;
	}



	public List<SelectItem> getUnitType() {
		unitType.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitType.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			 
		 }
		
		
		return unitType;
	}



	public void setUnitType(List<SelectItem> unitType) {
		this.unitType = unitType;
	}



	public List<SelectItem> getUnitUsage() {
		unitUsage.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.PROPERTY_USAGE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitUsage.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }



		return unitUsage;
	}




	public void setUnitUsage(List<SelectItem> unitUsage) {
		this.unitUsage = unitUsage;
	}



	public List<SelectItem> getUnitInvestment() {
		return unitInvestment;
	}



	public void setUnitInvestment(List<SelectItem> unitInvestment) {
		this.unitInvestment = unitInvestment;
	}



	public List<SelectItem> getUnitSide() {
		unitSide.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_SIDE_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitSide.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }

		return unitSide;
	}



	public void setUnitSide(List<SelectItem> unitSide) {
		this.unitSide = unitSide;
	}




	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}



	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}



	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}



	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}



	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}



	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}



	public List<SelectItem> getCity() {
		return city;
	}



	public void setCity(List<SelectItem> city) {
		this.city = city;
	}



	public List<SelectItem> getState() {
		return state;
	}



	public void setState(List<SelectItem> state) {
		this.state = state;
	}



	public List<SelectItem> getCountry() {
		return country;
	}



	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}





	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public InquiryView getDataItem() {
		return dataItem;
	}



	public void setDataItem(InquiryView dataItem) {
		this.dataItem = dataItem;
	}



	public List<InquiryView> getDataList() {
		return dataList;
	}



	public void setDataList(List<InquiryView> dataList) {
		this.dataList = dataList;
	}



	

/*
	public Date getOccupiedTillDate() {
		return occupiedTillDate;
	}



	public void setOccupiedTillDate(Date occupiedTillDate) {
		this.occupiedTillDate = occupiedTillDate;
	}

	*/

	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;

	private boolean sortAscending = true;

	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}


	public void sortDataList(ActionEvent event) {


		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters -----------------------------------------------------------------------------------

	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------

	

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}



	public HtmlSelectBooleanCheckbox getCheckBox() {
		return checkBox;
	}



	public void setCheckBox(HtmlSelectBooleanCheckbox checkBox) {
		this.checkBox = checkBox;
	}

	public String getSelectOneUnitType() {
		return selectOneUnitType;
	}

	public void setSelectOneUnitType(String selectOneUnitType) {
		this.selectOneUnitType = selectOneUnitType;
	}

	public String getSelectOneUnitUsage() {
		return selectOneUnitUsage;
	}

	public void setSelectOneUnitUsage(String selectOneUnitUsage) {
		this.selectOneUnitUsage = selectOneUnitUsage;
	}

	public String getSelectOneUnitSide() {
		return selectOneUnitSide;
	}

	public void setSelectOneUnitSide(String selectOneUnitSide) {
		this.selectOneUnitSide = selectOneUnitSide;
	}
	public String btnBack_Click()
	{
		return "BackScreen";
		
	}

	public String getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public void cancel(ActionEvent event) {
		//logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        //clearSessionMap();
				//Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
				//sessionMap.remove(WebConstants.AUCTION_ID);
				//sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		     //   logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			//	logger.LogException("cancel() crashed ", exception);
			}
    }

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = 10;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public void loadPropertyArea()  {
		
		  String methodName="loadCountry"; 
			logger.logInfo(methodName+"|"+"Start");
			try {
				if(viewRootMap.containsKey("PropertyArea")){
					viewRootMap.remove("PropertyArea");
					propertyAreaList.clear();
				}
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getAllPropertyArea();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
			      this.getPropertyAreaList().add(item);
			     
			  }
			
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("PropertyArea", propertyAreaList);
			 logger.logInfo(methodName+"|"+"Finish");
			}catch (Exception e){
				logger.LogException(methodName+"|Error Occured ",e);
				
			}
		}
	  
/*		public void loadState()  {
			
			String methodName="loadState"; 
			logger.logInfo(methodName+"|"+"Start");
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			if(viewRootMap.containsKey("stateList")){
				viewRootMap.remove("stateList");
				stateList.clear();
			}
			
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(countryId)==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	
			
			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName);
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      this.getStateList().add(item);
			      
			      
			  }
			
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
			 logger.logInfo(methodName+"|"+"Finish");
			}catch (Exception e){
				
				logger.LogException(methodName+"|Error Occured ",e);
				
			}
		}
		
		public void  loadCity(){
			String methodName="loadCity"; 
			logger.logInfo(methodName+"|"+"Start"); 
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			if(viewRootMap.containsKey("cityList")){
				viewRootMap.remove("cityList");
				cityList.clear();
			}
				
				try 
				{
					
					 Set<RegionView> regionViewList = null;
					 if(stateId!=null && new Long(stateId).compareTo(new Long(-1))!=0)
					 regionViewList =  psa.getCity(new Long(stateId));
				          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
			  	    
					
					Iterator itrator = regionViewList.iterator();

					for(int i=0;i<regionViewList.size();i++)
					  {
						  RegionView rV=(RegionView)itrator.next();
						  SelectItem item;
						  if (getIsEnglishLocale())
						  {
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
						  else 
							 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
					      this.getCityList().add(item);
					      
					      
					  }
				
					 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
					 logger.logInfo(methodName+"|"+"Finish");
				}
				catch(Exception ex)
				{
					logger.LogException(methodName+"|Error Occured ",ex);
					
				}
			   
		   }*/
	
		public void setArabicLocale(boolean isArabicLocale) {
			this.isArabicLocale = isArabicLocale;
		}


		public void setEnglishLocale(boolean isEnglishLocale) {
			this.isEnglishLocale = isEnglishLocale;
		}
	
		public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
		public boolean getIsEnglishLocale()
		{

			WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
			return isEnglishLocale;
		}
	
		public List<SelectItem> getPropertyAreaList() {
			if(viewRootMap.containsKey("countryList")){
			
				propertyAreaList= (List<SelectItem>) (viewRootMap.get("PropertyArea"));
			}
			return propertyAreaList;
			
		}

		public void setPropertyAreaList(List<SelectItem> propertyAreaList) {
			this.propertyAreaList = propertyAreaList;
		}

		public List<SelectItem> getStateList() {
			if(viewRootMap.containsKey("stateList")){
				//stateList.clear();
				stateList= (List<SelectItem>) (viewRootMap.get("stateList"));
			}
			return stateList;	
			
			
		}

		public void setStateList(List<SelectItem> stateList) {
			this.stateList = stateList;
			
		}

		public List<SelectItem> getCityList() {
			if(viewRootMap.containsKey("cityList")){
				//cityList.clear();
				cityList= (List<SelectItem>) (viewRootMap.get("cityList"));
				
			}
			return cityList;	
			
		}

		public void setCityList(List<SelectItem> cityList) {
			this.cityList = cityList;
			
		}

		public String getCountryId() {
			return countryId;
		}

		public void setCountryId(String countryId) {
			this.countryId = countryId;
		}

		public String getStateId() {
			return stateId;
		}

		public void setStateId(String stateId) {
			this.stateId = stateId;
		}

		public String getCityId() {
			return cityId;
		}

		public void setCityId(String cityId) {
			this.cityId = cityId;
		}

		public String getNoOfBedsMin() {
			return noOfBedsMin;
		}

		public void setNoOfBedsMin(String noOfBedsMin) {
			this.noOfBedsMin = noOfBedsMin;
		}

		public String getNoOfBedsMax() {
			return noOfBedsMax;
		}

		public void setNoOfBedsMax(String noOfBedsMax) {
			this.noOfBedsMax = noOfBedsMax;
		}

		public String getRentValueMin() {
			return rentValueMin;
		}

		public void setRentValueMin(String rentValueMin) {
			this.rentValueMin = rentValueMin;
		}

		public String getRentValueMax() {
			return rentValueMax;
		}

		public void setRentValueMax(String rentValueMax) {
			this.rentValueMax = rentValueMax;
		}
		public String getDateFormat(){
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
		}
		
		
		public TimeZone getTimeZone()
		{
			 return TimeZone.getDefault();
			
		}
		
		public String generateNotification()
	    {
	    	   String methodName ="generateNotification";
	    	   errorMessages = new ArrayList<String>();
	    	   successMessages = new ArrayList<String>();
	    	   Boolean flage = false;
				
	    	      Boolean success = false;
	    	            try
	    	            {
	    	            	logger.logInfo(methodName+"|Start");
	    	            	
	    	            	List<InquiryView> inquiryViewList = new ArrayList<InquiryView>(0);
	    	            	List<InquiryView> inquiryViewSelectedList = new ArrayList<InquiryView>(0);
	    	            	List<Long> personIdList= new ArrayList<Long>(0);
	    	            	List<PersonView> personViewList = new ArrayList<PersonView>();
	    	            	PersonView personView = new PersonView();
	    	            	PropertyServiceAgent psa =  new PropertyServiceAgent();
	    	            	HashMap placeHolderMap = new HashMap();
	    	                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
	    	                NotificationFactory nsfactory = NotificationFactory.getInstance();
	    	                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
	    	                Event event = EventCatalog.getInstance().getMetaEvent(WebConstants.Notification_MetaEvents.Event_PropertyInquiry_InquiryOutCome).createEvent();
	    	        		
	    	            	if (viewMap.containsKey("PERSONLIST")) {
	    	        			inquiryViewList = (ArrayList<InquiryView>) viewMap.get("PERSONLIST");
	    	        			for (InquiryView iV : inquiryViewList) {
	    	        				if (iV.getSelectedEmail() != null && !iV.equals("")) {
	    	        					if (iV.getSelectedEmail()) {
	    	        						inquiryViewSelectedList.add(iV);
	    	        					}
	    	        				}
	    	        			}
	    	        		}
	    	            	  
	    	                  
	    	                 
	    	            	
	    	            	if(inquiryViewSelectedList.size()>0){
	    	            		for(InquiryView iV:inquiryViewSelectedList){
	    	            		personView =	psa.getPersonInformation(iV.getPersonView().getPersonId());
			    	            		if(personView!=null && iV.getUnitNumber()!=null){
			    	            		getNotificationPlaceHolder(event,iV,personView);
			    	            		personsEmailList = getEmailContactInfos((PersonView)personView);
		    	                        if(personsEmailList.size()>0)
		    	                              notifier.fireEvent(event, personsEmailList);
		    	                        flage  = true;
			    	            		}
    	                        
	    	            		}
	    	            	}else{
	    	            		errorMessages.add(getBundleMessage(MessageConstants.PersonList.NO_PERSON_SELECTED));
	    	            		return "";
	    	            	}
	    	            	
	    	            	 
	    	                 
	    	            	
	    	                     
	    	                  
	    	                  success  = true;
	    	                  logger.logInfo(methodName+"|Finish");
	    	            }
	    	            catch(Exception ex)
	    	            {
	    	                  logger.LogException(methodName+"|Finish", ex);
	    	            }
	    	            if(flage)
	    	            successMessages.add(getBundleMessage(MessageConstants.PersonList.NOTIFICATION_SEND));
	    	            
                        return "";
	    }
	    private List<ContactInfo> getEmailContactInfos(PersonView personView)
	    {
	    	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
	    	Iterator iter = personView.getContactInfoViewSet().iterator();
	    	HashMap previousEmailAddressMap = new HashMap();
	    	while(iter.hasNext())
	    	{
	    		ContactInfoView ciView = (ContactInfoView)iter.next();
	    		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
	    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
	    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	            {
	    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
	    			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
	    			//emailList.add(ciView);
	            }
	    	}
	    	return emailList;
	    }
		private void  getNotificationPlaceHolder(Event placeHolderMap,InquiryView inquiryView,PersonView perView)
		{
			String methodName = "getNotificationPlaceHolder";
			logger.logDebug(methodName+"|Start");
			//placeHolderMap.setValue("CONTRACT", contractView);
			placeHolderMap.setValue("PERSON_NAME",perView.getPersonFullName());
			placeHolderMap.setValue("UNIT_NUMBER",inquiryView.getUnitNumber());
			
			logger.logDebug(methodName+"|Finish");
			
		}
		
	    private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}

		public String getSelectOneApplicationStatus() {
			return selectOneApplicationStatus;
		}

		public void setSelectOneApplicationStatus(String selectOneApplicationStatus) {
			this.selectOneApplicationStatus = selectOneApplicationStatus;
		}

		public String getSelectOneApplicationMethod() {
			return selectOneApplicationMethod;
		}

		public void setSelectOneApplicationMethod(String selectOneApplicationMethod) {
			this.selectOneApplicationMethod = selectOneApplicationMethod;
		}

		public String getSelectOneApplicationPriority() {
			return selectOneApplicationPriority;
		}

		public void setSelectOneApplicationPriority(String selectOneApplicationPriority) {
			this.selectOneApplicationPriority = selectOneApplicationPriority;
		}

		public String getInquirerName() {
			return inquirerName;
		}

		public void setInquirerName(String inquirerName) {
			this.inquirerName = inquirerName;
		}

		public String getCellPhone() {
			return cellPhone;
		}

		public void setCellPhone(String cellPhone) {
			this.cellPhone = cellPhone;
		}


		public String getLocale() {
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode();
		}

		public Date getApplicationDateFrom() {
			return applicationDateFrom;
		}

		public void setApplicationDateFrom(Date applicationDateFrom) {
			this.applicationDateFrom = applicationDateFrom;
		}

		public String getPersonIdNumber() {
			return personIdNumber;
		}

		public void setPersonIdNumber(String personIdNumber) {
			this.personIdNumber = personIdNumber;
		}

		public String getPassportNumber() {
			return passportNumber;
		}

		public void setPassportNumber(String passportNumber) {
			this.passportNumber = passportNumber;
		}

		public String getPropertyName() {
			return propertyName;
		}

		public void setPropertyName(String propertyName) {
			this.propertyName = propertyName;
		}

		public String getSelectOnePropertyType() {
			return selectOnePropertyType;
		}

		public void setSelectOnePropertyType(String selectOnePropertyType) {
			this.selectOnePropertyType = selectOnePropertyType;
		}

		public String getSelectOnePropertyArea() {
			return selectOnePropertyArea;
		}

		public void setSelectOnePropertyArea(String selectOnePropertyArea) {
			this.selectOnePropertyArea = selectOnePropertyArea;
		}

		public Date getApplicationDateTo() {
			return applicationDateTo;
		}

		public void setApplicationDateTo(Date applicationDateTo) {
			this.applicationDateTo = applicationDateTo;
		}

		public String getApplicationNumber() {
			return applicationNumber;
		}

		public void setApplicationNumber(String applicationNumber) {
			this.applicationNumber = applicationNumber;
		}

		public List<SelectItem> getRequestStatusList() {
			Boolean isEnglishLocale = isEnglishLocale();
			
			if(isEnglishLocale) {
				List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_EN);
				if(cboList==null) {
					List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
					Iterator<DomainTypeView> itr = list.iterator();
					while( itr.hasNext() ) {
						DomainTypeView dtv = itr.next();
						if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
							Set<DomainDataView> dd = dtv.getDomainDatas();
							cboList = new ArrayList<SelectItem>();
							SelectItem item = null;
							for (DomainDataView ddv : dd) {
								if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED_AND_NOTIFIED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CLOSED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NOTIFIED)==0){
								item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
								cboList.add(item);
								}
							}
							break;
						}
					}
					if( cboList==null ) {
						return emptyList;
					}
					Collections.sort(cboList,ListComparator.LIST_COMPARE);
					listMap.put(WebConstants.REQUEST_STATUS_LIST_EN, cboList);
				}
				return cboList;
			} else {
				List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_AR);
				if(cboList==null) {
					List<DomainTypeView> list = (List<DomainTypeView>) context1.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
					Iterator<DomainTypeView> itr = list.iterator();
					while( itr.hasNext() ) {
						DomainTypeView dtv = itr.next();
						if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
							Set<DomainDataView> dd = dtv.getDomainDatas();
							cboList = new ArrayList<SelectItem>();
							SelectItem item = null;
							for (DomainDataView ddv : dd) {
								if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED_AND_NOTIFIED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CLOSED)==0
								 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NOTIFIED)==0){
								item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
								cboList.add(item);
								}
							}
							break;
						}
					}
					if( cboList==null ) {
						return emptyList;
					}
					Collections.sort(cboList,ListComparator.LIST_COMPARE);
					listMap.put(WebConstants.REQUEST_STATUS_LIST_AR, cboList);
				}
				return cboList;
			}
		}
		
		public Boolean isEnglishLocale() {
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		}
		
		public String onFindMatchingProperties() 
		{
			InquiryView selectedInquiryView = (InquiryView) dataTable.getRowData();
			sessionMap.put( WebConstants.INQUIRY, selectedInquiryView );
			return "inquiryMatchedProperties";
		}
}
