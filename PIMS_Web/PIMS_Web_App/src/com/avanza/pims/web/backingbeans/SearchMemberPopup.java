package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.UserView;
public class SearchMemberPopup extends AbstractController
{

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}
	@Override
	public void prerender() {
	// TODO Auto-generated method stub
		super.prerender();
	}


	private HtmlDataTable dataTable;
	private HtmlInputText userName  ;
	private HtmlInputText groupName ;
	private HtmlInputText fullName;
	private HtmlCommandButton selectButton;
	//private Calendar occupiedTillDate = new Calendar();
	private UserView userView = new UserView();
	private static Logger logger = Logger.getLogger(SearchMemberPopup.class);
	private String teamId;
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
//	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();



	@Override 
	public void init() 
	{
		super.init();

		HttpServletRequest request =
			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		if(getRequestParam(WebConstants.TEAM_NAME)!=null)
			viewMap.put(WebConstants.TEAM_NAME,getRequestParam(WebConstants.TEAM_NAME));
		 
		 if(request.getParameter("singleselectmode")!=null)
			 viewMap.put("singleselectmode","singleselectmode");
		
	
	}
    public boolean getIsSingleSelectMode()
    {
    	boolean isSingleSelectMode = false;
    	if( viewMap.containsKey("singleselectmode") &&  viewMap.get("singleselectmode").toString().trim().equals("singleselectmode"))
    		isSingleSelectMode = true;
    	return isSingleSelectMode;
    }

	//We have to look for the list population for facilities
	private List<String> errorMessages;
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private UserView dataItem = new UserView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<UserView> dataList = new ArrayList<UserView>();
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	
	public List<UserView> loadDataList() 
	{
		String methodName="loadDataList";
		List<UserView> list = new ArrayList();
		try
		{
			UtilityServiceAgent utilityServiceAgent= new UtilityServiceAgent();
			list =  utilityServiceAgent.getUsers(userView);
			dataList =list;
			viewMap.put("MembersList", dataList);
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return list;
	}
	public List<UserView> getUserDataList()
	{
		if(viewMap.containsKey("MembersList")){
			dataList = (ArrayList<UserView>)viewMap.get("MembersList");
		}
//		if (dataList == null || dataList.size()==0)
//		{
//			dataList= loadDataList(); 
//		}
		return dataList;
	}
	private TeamView getTeamView(List<UserView> membersList,String teamName){
		String methodName = "getTeamView";
		logger.logInfo(methodName+"|"+"Start..");
		TeamView teamView =new TeamView();
		Set<TeamMemberView> teamMemberSet=new HashSet<TeamMemberView>(0);
		TeamMemberView teamMemberView=new TeamMemberView();
		
		if(teamId!=null && teamId.length()>0)
		{
			teamView.setTeamId(new Long(teamId));	
		}
		//logger.logInfo(methodName+"|"+"teamName.."+teamName);
		//if(teamName!=null && teamName.length()>0)
		//{
		teamView.setTeamName(teamName);	
		//}
		//else
		//{
		//	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.InspectionDetails.MSG_REQ_TEAM_NAME));
		//}

		teamView.setCreatedOn(new Date());

		teamView.setCreatedBy(getLoggedInUser());

		teamView.setUpdatedOn(new Date());

		teamView.setUpdatedBy(getLoggedInUser());

		teamView.setIsDeleted(new Long(0));

		teamView.setRecordStatus(new Long(1));
		//adding members	

		if(membersList.size()<=0)
		{
			if(errorMessages==null)
				errorMessages =new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.InspectionDetails.MSG_REQ_TEAM_MEMBER));
		}
		for (UserView selectedMember : membersList) 
		{
			teamMemberView=new TeamMemberView();

			teamMemberView.setLoginId(selectedMember.getUserName());
			teamMemberView.setIsDeleted(new Long(0));
			teamMemberView.setRecordStatus(new Long(1));
			teamMemberView.setUpdatedOn(new Date());
			teamMemberView.setCreatedOn(new Date());
			teamMemberView.setCreatedBy(getLoggedInUser());
			teamMemberView.setUpdatedBy(getLoggedInUser());
			teamMemberSet.add(teamMemberView);

		}
		teamView.setTeamMembersView(teamMemberSet);
		return teamView;
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public void cmdSelectPerson_Click()
	{
		String methodName ="cmdSelectPerson_Click";
		logger.logInfo(methodName+"|Start");
		UserView userView = (UserView)dataTable.getRowData();
		String javaScriptText = "javascript:closeWindow();";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// Add the Javascript to the rendered page's header for immediate execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.USER_VIEW, userView);
		logger.logInfo(methodName+"|Finish");
		
	}
	public void editDataItem() {
	
		
		List<UserView> userViewList=new ArrayList<UserView>(0);
		List<UserView> userViewListTemp=new ArrayList<UserView>(0);
		if(viewMap.containsKey("MembersList")){
			userViewList=(ArrayList<UserView>)viewMap.get("MembersList"); 
			for (UserView userView : userViewList) 
			{
				if(userView.getSelected()!=null && userView.getSelected() )
				{
					userViewListTemp.add(userView);
				}
			}
		}
		String teamName="";
		if(viewMap.get(WebConstants.TEAM_NAME)!=null)
			teamName = viewMap.get(WebConstants.TEAM_NAME).toString();
		TeamView teamView = getTeamView(userViewListTemp,teamName);
		final String viewId = "/SearchMemberPopup.jsf"; 

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:closeWindow();";

		// Add the Javascript to the rendered page's header for immediate execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TEAM_VIEW, teamView);
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TEAM_DATA_LIST, userViewListTemp);
		
		
	}

	
	public String searchUser(){
		UtilityServiceAgent utilityServiceAgent =new UtilityServiceAgent();
		dataList = new ArrayList<UserView>();
		userView.setUserName(userName.getValue().toString());
		if(getIsArabicLocale()){
			userView.setGroupNameSecondary(groupName.getValue().toString());
			userView.setFullNameSecondary(fullName.getValue().toString());
		}
		else{
			userView.setGroupNamePrimary(groupName.getValue().toString());
			userView.setFullNamePrimary(fullName.getValue().toString());
		}
			
		loadDataList();
		return "searched";
	}




	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) ||
				(errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for(String message: errorMessages) {
				messageList = messageList + "<LI>" + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}



	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public UserView getDataItem() {
		return dataItem;
	}

	public void setDataItem(UserView dataItem) {
		this.dataItem = dataItem;
	}

	public List<UserView> getDataList() {
		return dataList;
	}
	
	public void setDataList(List<UserView> dataList) {
		this.dataList = dataList;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		  String method="getIsArabicLocale";
	    	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
					LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
					isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
			return isEnglishLocale;
	}
	public HtmlInputText getFullName() {
		return fullName;
	}
	public void setFullName(HtmlInputText fullName) {
		this.fullName = fullName;
	}
	public HtmlInputText getGroupName() {
		return groupName;
	}
	public void setGroupName(HtmlInputText groupName) {
		this.groupName = groupName;
	}
	public HtmlInputText getUserName() {
		return userName;
	}
	public void setUserName(HtmlInputText userName) {
		this.userName = userName;
	}
	public HtmlCommandButton getSelectButton() {
		return selectButton;
	}
	public void setSelectButton(HtmlCommandButton selectButton) {
		this.selectButton = selectButton;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}


}
