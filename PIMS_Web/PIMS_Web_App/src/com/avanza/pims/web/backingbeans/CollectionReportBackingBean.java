package com.avanza.pims.web.backingbeans;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.CollectionReportCriteria;

import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;


public class CollectionReportBackingBean extends AbstractController 
{

	 private static Logger logger = Logger.getLogger("CollectionReportBackingBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	CollectionReportCriteria collectionReportCriteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlSelectOneMenu  cmbReceiptBy = new HtmlSelectOneMenu();
	
	
	public CollectionReportBackingBean(){ 
		if(CommonUtil.getIsEnglishLocale())
			collectionReportCriteria = new CollectionReportCriteria(ReportConstant.Report.COLLECTION_REPORT_EN, ReportConstant.Processor.COLLECTION_REPORT, getLoggedInUserObj().getFullName());		
		else
			collectionReportCriteria = new CollectionReportCriteria(ReportConstant.Report.COLLECTION_REPORT_AR, ReportConstant.Processor.COLLECTION_REPORT, getLoggedInUserObj().getSecondaryFullName() );		
	}
	
		public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
//		collectionReportCriteria.setCriteriaReceiptBy( cmbReceiptBy.getTitle().toString() );
//		for (SelectItem item: ( ( ApplicationBean )getBean("pages$ApplicationBean") ).getUserNameList()) 
//		  if( item.getValue().toString().compareTo( collectionReportCriteria.getReceiptBy()  ) == 0 )
//		  {
//			  collectionReportCriteria.setCriteriaReceiptBy(  item.getLabel() );
//			  break;
//		  }
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, collectionReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public CollectionReportCriteria getCollectionReportCriteria() {
		return collectionReportCriteria;
	}
	public void setCollectionReportCriteria(
			CollectionReportCriteria collectionReportCriteria) {
		this.collectionReportCriteria = collectionReportCriteria;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlSelectOneMenu getCmbReceiptBy() {
		return cmbReceiptBy;
	}

	public void setCmbReceiptBy(HtmlSelectOneMenu cmbReceiptBy) {
		this.cmbReceiptBy = cmbReceiptBy;
	}
	
	
	
}
