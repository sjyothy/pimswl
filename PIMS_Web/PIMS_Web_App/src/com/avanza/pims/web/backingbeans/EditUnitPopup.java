package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class EditUnitPopup extends AbstractController{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Controls 
	private HtmlInputText floorNumber = new HtmlInputText();
	private String unitType = new String();
	private HtmlInputText propertyNumber = new HtmlInputText();
	private HtmlInputText accountNumber = new HtmlInputText();
	private String unitUsage = new String();
	private HtmlInputText unitNo = new HtmlInputText();
	private String investmentType = new String();
	private HtmlInputText unitStatus= new HtmlInputText();
	private String unitSide= new String();
	private String infoMessage = "";
	
	private HtmlInputText unitPrefix = new HtmlInputText();
	private String strUnitPrefix = new String();
	private HtmlInputText description = new HtmlInputText();
	private HtmlInputText areaInSquare = new HtmlInputText();
	private HtmlInputText bedrooms = new HtmlInputText();
	private HtmlInputText propCommercialName = new HtmlInputText();
	private HtmlInputText propAddress = new HtmlInputText();
	private HtmlInputText bathrooms = new HtmlInputText();
	private HtmlInputText livingRooms = new HtmlInputText();
	private HtmlInputText EWUtilityNo = new HtmlInputText();
	private HtmlInputText rent = new HtmlInputText();
	private HtmlInputText unitRemarks = new HtmlInputText();
	private HtmlInputText ejariRefNum = new HtmlInputText();
	
	private HtmlSelectBooleanCheckbox includeFloorNumber = new HtmlSelectBooleanCheckbox();
	private Boolean includeFloorNumberVar = new Boolean(false);
	private HtmlCommandButton updateButton = new HtmlCommandButton();
	private HtmlInputHidden hiddenUnitId = new HtmlInputHidden();
	private HtmlInputHidden hiddenFloorId = new HtmlInputHidden();
	private HtmlInputHidden hiddenPropertyId = new HtmlInputHidden();
	private HtmlInputHidden hiddenStatusId = new HtmlInputHidden();
	private HtmlSelectOneMenu unitInvestmentTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitInvestmentTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu unitTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu unitUsageTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitUsageTypeList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu unitSideMenu = new HtmlSelectOneMenu();
	private List<SelectItem> unitSideList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu selectedUnitStaus = new HtmlSelectOneMenu();
	private List<DomainDataView> unitStatusListDD = new ArrayList<DomainDataView>();
	private DomainDataView unitStatusVacantDD = new DomainDataView();
	private DomainDataView unitStatusRentedDD = new DomainDataView();
	
	//Maps i.e. ViewMaps RequestMap or SessionMap
	Map sessionMap  = getFacesContext().getExternalContext().getSessionMap();
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	
	//Constants
	private String UNIT_POPUP_VIEW = "UNIT_POPUP_VIEW" ;
	private String UNIT_ID = "UNIT_ID" ;
	private String READ_ONLY_MODE = "readOnlyMode" ;
	
	//Logger
	private static Logger logger = Logger.getLogger(EditUnitPopup.class);
	ApplicationBean applicationBean = new ApplicationBean();
	
	//Locale & Messages
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private List<String> errorMessages  = new ArrayList<String>();
	private List<String> successMessages  = new ArrayList<String>(); 
	
	public HtmlInputText getAreaInSquare() {
		return areaInSquare;
	}

	public void setAreaInSquare(HtmlInputText areaInSquare) {
		this.areaInSquare = areaInSquare;
	}

	public HtmlInputText getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(HtmlInputText bathrooms) {
		this.bathrooms = bathrooms;
	}

	public HtmlInputText getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(HtmlInputText bedrooms) {
		this.bedrooms = bedrooms;
	}

	public HtmlInputText getDescription() {
		return description;
	}

	public void setDescription(HtmlInputText description) {
		this.description = description;
	}

	public HtmlInputText getEWUtilityNo() {
		return EWUtilityNo;
	}

	public void setEWUtilityNo(HtmlInputText utilityNo) {
		EWUtilityNo = utilityNo;
	}

	public HtmlInputText getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(HtmlInputText floorNumber) {
		this.floorNumber = floorNumber;
	}

	public HtmlSelectBooleanCheckbox getIncludeFloorNumber() {
		return includeFloorNumber;
	}

	public void setIncludeFloorNumber(HtmlSelectBooleanCheckbox includeFloorNumber) {
		this.includeFloorNumber = includeFloorNumber;
	}

	public HtmlInputText getLivingRooms() {
		return livingRooms;
	}

	public void setLivingRooms(HtmlInputText livingRooms) {
		this.livingRooms = livingRooms;
	}

	public HtmlInputText getPropertyNumber() {
		return propertyNumber;
	}

	public void setPropertyNumber(HtmlInputText propertyNumber) {
		this.propertyNumber = propertyNumber;
	}

	public HtmlInputText getRent() {
		return rent;
	}

	public void setRent(HtmlInputText rent) {		
		this.rent = rent;		
	}

	public HtmlInputText getUnitNo() {
		return unitNo;
	}

	public void setUnitNo(HtmlInputText unitNo) {
		this.unitNo = unitNo;
	}

	public HtmlInputText getUnitRemarks() {
		return unitRemarks;
	}

	public void setUnitRemarks(HtmlInputText unitRemarks) {
		this.unitRemarks = unitRemarks;
	}

	public HtmlInputHidden getHiddenUnitId() {
		return hiddenUnitId;
	}

	public void setHiddenUnitId(HtmlInputHidden hiddenUnitId) {
		this.hiddenUnitId = hiddenUnitId;
	}

	public HtmlInputText getUnitPrefix() {
		return unitPrefix;
	}

	public void setUnitPrefix(HtmlInputText unitPrefix) {
		this.unitPrefix = unitPrefix;
	}


	public HtmlCommandButton getUpdateButton() {
		return updateButton;
	}

	public void setUpdateButton(HtmlCommandButton updateButton) {
		this.updateButton = updateButton;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init()
	{
		super.init();
		try 
		{
			HttpServletRequest request =
				 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			if(!isPostBack())
			{
				if(sessionMap.containsKey(UNIT_ID) && sessionMap.get(UNIT_ID)!=null)
				{
					HashMap unitSearchMap = new HashMap(0);
					viewMap.put(UNIT_ID, sessionMap.get(UNIT_ID));
					sessionMap.remove(UNIT_ID);
					unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID,new Long(viewMap.get(UNIT_ID).toString()));
					List<UnitView> unitList = new PropertyServiceAgent().getPropertyUnitsByCriteria(unitSearchMap);
					UnitView unitView = (UnitView)unitList.get(0);
					viewMap.put(UNIT_POPUP_VIEW,unitView );
					
				}
				else if(request.getParameter(UNIT_ID)!=null)
				{
					HashMap unitSearchMap = new HashMap(0);
					viewMap.put(UNIT_ID, request.getParameter(UNIT_ID).toString());
					sessionMap.remove(UNIT_ID);
					unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID,new Long(viewMap.get(UNIT_ID).toString()));
					List<UnitView> unitList = new PropertyServiceAgent().getPropertyUnitsByCriteria(unitSearchMap);
					UnitView unitView = (UnitView)unitList.get(0);
					viewMap.put(UNIT_POPUP_VIEW,unitView );
					
				}
				if((sessionMap.containsKey(READ_ONLY_MODE) && sessionMap.get(READ_ONLY_MODE)!=null)||request.getParameter(READ_ONLY_MODE)!= null)
				{
					viewMap.put(READ_ONLY_MODE,true);
				}
				
					
			}
			
		}
		catch (Exception e) 
		{
			logger.LogException("init|Error Occured", e);
		}
		
	}

	@Override
	public void preprocess() {
		super.preprocess();		
	}

	@Override
	@SuppressWarnings("unchecked")
	public void prerender() 
	{
		super.prerender();		
		if(sessionMap.containsKey(UNIT_POPUP_VIEW)&& sessionMap.get(UNIT_POPUP_VIEW)!=null)
		{
			viewMap.put(UNIT_POPUP_VIEW, sessionMap.get(UNIT_POPUP_VIEW));
			sessionMap.remove(UNIT_POPUP_VIEW);
		}
		if(!isPostBack())
		{
			if(viewMap.containsKey(UNIT_POPUP_VIEW)&&viewMap.get(UNIT_POPUP_VIEW)!=null)
			{
				
				UnitView unitView = (UnitView)viewMap.get(UNIT_POPUP_VIEW);
				fillValues(unitView);
			}
			unitStatusListDD = CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_STATUS);
			unitStatusVacantDD = CommonUtil.getIdFromType(unitStatusListDD, WebConstants.UNIT_VACANT_STATUS);
			unitStatusRentedDD = CommonUtil.getIdFromType(unitStatusListDD, WebConstants.UNIT_RENTED_STATUS);
			viewMap.put("VACANT_UNIT_DD", unitStatusVacantDD);
			viewMap.put("RENTED_UNIT_DD", unitStatusRentedDD);
			viewMap.put("UNIT_STATUS_DD_LIST", unitStatusListDD);
		}
		if(viewMap.containsKey(READ_ONLY_MODE))
		{
			propertyNumber.setReadonly(true);
			unitPrefix.setReadonly(true);
			unitNo.setReadonly(true);
			areaInSquare.setReadonly(true);
			bedrooms.setReadonly(true);
			livingRooms.setReadonly(true);
			bathrooms.setReadonly(true);
			EWUtilityNo.setReadonly(true);
			rent.setReadonly(true);
			description.setReadonly(true);
			unitRemarks.setReadonly(true);
			ejariRefNum.setReadonly(true);
			updateButton.setRendered(false);
		}

	}
	public Boolean getIsReadOnlyMode()
	{
		if(viewMap.containsKey(READ_ONLY_MODE))
			return true;
		else return false;
		
	}
	@SuppressWarnings("unchecked")
	private void fillValues(UnitView unitView)
	{
		 floorNumber.setValue(unitView.getFloorNumber());
		 hiddenUnitId.setValue(unitView.getUnitId().toString());
		 hiddenFloorId.setValue(unitView.getFloorView().getFloorId().toString());
		 hiddenStatusId.setValue(unitView.getStatusId().toString());
		 unitNo.setValue(unitView.getUnitNumber());
		 unitRemarks.setValue(unitView.getUnitRemarks());
		 ejariRefNum.setValue(unitView.getEjariReferenceNumber());
		 unitType = unitView.getUnitTypeId().toString();
		 hiddenStatusId.setValue(unitView.getStatusId().toString());
		 unitPrefix.setValue(unitView.getUnitPrefix());
		 description.setValue(unitView.getUnitDesc());
		 areaInSquare.setValue(unitView.getUnitArea());
		 bedrooms.setValue(unitView.getNoOfBed().toString());
		 livingRooms.setValue(unitView.getNoOfLiving().toString());
		 bathrooms.setValue(unitView.getNoOfBath().toString());
		 propCommercialName.setValue(unitView.getPropertyCommercialName());
		 propAddress.setValue(unitView.getUnitAddress());
		 propertyNumber.setValue(unitView.getPropertyNumber());
		 accountNumber.setValue(unitView.getAccountNumber());
		 if(unitView.getRentValue()!=null)
		   rent.setValue(unitView.getRentValue().toString());
		 if(unitView.getEWUtilitNo()!=null)
		    EWUtilityNo.setValue(unitView.getEWUtilitNo().toString());
		 if(unitView.getUnitSideTypeId()!=null)
			 unitSide=unitView.getUnitSideTypeId().toString();
		 if(unitView.getUsageTypeId()!=null)
			 unitUsage = unitView.getUsageTypeId().toString();
		 if(unitView.getInvestmentTypeId()!=null)
		 	investmentType = unitView.getInvestmentTypeId().toString();
		 if(unitView.getIncludeFloorNumber()!=null && unitView.getIncludeFloorNumber().toString().equals("1"))
			 this.setIncludeFloorNumberVar(true);
		 else
			 this.setIncludeFloorNumberVar(false);
		 if(getIsEnglishLocale())
			 unitStatus.setValue(unitView.getStatusEn());
		 else
			 unitStatus.setValue(unitView.getStatusAr());

		 ReceivePropertyUnitView dest = new ReceivePropertyUnitView();
		 transformUnitViewToReceivePropertyUnitView(dest, unitView);
		 viewMap.put("UNIT_VIEW", dest);
		

	 } 


	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the successMessages
	 */
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	/**
	 * @param successMessages the successMessages to set
	 */
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	@SuppressWarnings("unchecked")
	public Boolean getIncludeFloorNumberVar() {
		if(viewMap.containsKey("includeFloorNumberVar"))
			includeFloorNumberVar = Boolean.valueOf(viewMap.get("includeFloorNumberVar").toString());
		return includeFloorNumberVar;
	}

	@SuppressWarnings("unchecked")
	public void setIncludeFloorNumberVar(Boolean includeFloorNumberVar) {
		this.includeFloorNumberVar = includeFloorNumberVar;
		if(this.includeFloorNumberVar !=null)
			viewMap.put("includeFloorNumberVar", this.includeFloorNumberVar);
	}

	public HtmlInputHidden getHiddenStatusId() {
		return hiddenStatusId;
	}

	public void setHiddenStatusId(HtmlInputHidden hiddenStatusId) {
		this.hiddenStatusId = hiddenStatusId;
	}

	public List<SelectItem> getUnitInvestmentTypeList() {
		return unitInvestmentTypeList;
	}

	public void setUnitInvestmentTypeList(List<SelectItem> unitInvestmentTypeList) {
		this.unitInvestmentTypeList = unitInvestmentTypeList;
	}

	public HtmlSelectOneMenu getUnitInvestmentTypeMenu() {
		return unitInvestmentTypeMenu;
	}

	public void setUnitInvestmentTypeMenu(HtmlSelectOneMenu unitInvestmentTypeMenu) {
		this.unitInvestmentTypeMenu = unitInvestmentTypeMenu;
	}

	public List<SelectItem> getUnitSideList() {
		return unitSideList;
	}

	public void setUnitSideList(List<SelectItem> unitSideList) {
		this.unitSideList = unitSideList;
	}

	public HtmlSelectOneMenu getUnitSideMenu() {
		return unitSideMenu;
	}

	public void setUnitSideMenu(HtmlSelectOneMenu unitSideMenu) {
		this.unitSideMenu = unitSideMenu;
	}

	public HtmlInputText getUnitStatus() {
		return unitStatus;
	}

	public void setUnitStatus(HtmlInputText unitStatus) {
		this.unitStatus = unitStatus;
	}

	public List<SelectItem> getUnitTypeList() {
		return unitTypeList;
	}

	public void setUnitTypeList(List<SelectItem> unitTypeList) {
		this.unitTypeList = unitTypeList;
	}

	public HtmlSelectOneMenu getUnitTypeMenu() {
		return unitTypeMenu;
	}

	public void setUnitTypeMenu(HtmlSelectOneMenu unitTypeMenu) {
		this.unitTypeMenu = unitTypeMenu;
	}

	public List<SelectItem> getUnitUsageTypeList() {
		return unitUsageTypeList;
	}

	public void setUnitUsageTypeList(List<SelectItem> unitUsageTypeList) {
		this.unitUsageTypeList = unitUsageTypeList;
	}

	public HtmlSelectOneMenu getUnitUsageTypeMenu() {
		return unitUsageTypeMenu;
	}

	public void setUnitUsageTypeMenu(HtmlSelectOneMenu unitUsageTypeMenu) {
		this.unitUsageTypeMenu = unitUsageTypeMenu;
	}
	
	//ACtion Method
	public String updateUnit(){
		generatePropertyUnits();
		return "";		
	}
	@SuppressWarnings("unchecked")
	public String closePopup()
	{
		 sessionMap.put("UNIT_UPDATED_SUCCESSFULLY",true);
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 String javaScriptText = "javascript:closeWindow();";
		 AddResource addResource = AddResourceFactory.getInstance(facesContext);
		 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		return "";		
	}
	 private boolean validateUnitScreen() 
	 {

		 String method = "ValidateUnitScreen";
		 logger.logInfo(method + " started...");
		 boolean isValid = true;	
		 
		 if(selectedUnitStaus != null &&  selectedUnitStaus.getValue() != null && !selectedUnitStaus.getValue().toString().equals("-1"))
		 {	
			 if(!isValidateUnitStatus())
				 isValid = false;
		 }
		 
		 if(unitType==null||unitType.equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_TYPE_REQUIRED));
			 isValid = false;
		 }
		 if(unitSide==null||unitSide.equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_SIDE_REQUIRED));
			 isValid = false;
		 }
		 if(unitUsage==null||unitUsage.equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_USAGE_TYPE_REQUIRED));
			 isValid = false;
		 }
		 if(investmentType==null||investmentType.equals("-1")){
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_INVESTMENT_TYPE_REQUIRED));
			 isValid = false;
		 }
		 
		 Object accNumber = accountNumber.getValue();
		 if(accNumber==null||accNumber.toString().equals(""))
		 {
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_ACCOUNT_NUMBER_REQUIRED));
			 isValid = false;
		 }

		 try
		 {
			 if(areaInSquare.getValue()!=null&&areaInSquare.getValue().toString().trim().length()>0)
				 Double.parseDouble(areaInSquare.getValue().toString());
			 else
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_AREA_REQUIRED));
			 }
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_AREA));
		 }
		 catch (IllegalArgumentException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_AREA));
		 }
		 try
		 {
			 if(bedrooms.getValue()!=null&&bedrooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(bedrooms.getValue().toString());
			 else
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_BEDROOMS_REQUIRED));
			 }
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BEDROOMS));
		 }
		 catch (IllegalArgumentException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BEDROOMS));
		 }
		 try
		 {
			 if(livingRooms.getValue()!=null&&livingRooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(livingRooms.getValue().toString());
			 else
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_LIVINGROOMS_REQUIRED));
			 }
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LIVINGROOMS));
		 }
		 catch (IllegalArgumentException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_LIVINGROOMS));
		 }
		 try
		 {
			 if(bathrooms.getValue()!=null&&bathrooms.getValue().toString().trim().length()>0)
				 Double.parseDouble(bathrooms.getValue().toString());
			 else
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_BATHROOMS_REQUIRED));
			 }
		 }
		 catch (NumberFormatException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BATHROOMS));
		 }
		 catch (IllegalArgumentException e) 
		 {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_BATHROOMS));
		 }
		 try
		 {
			 if(rent.getValue()!=null&&rent.getValue().toString().trim().length()>0)
				 Double.parseDouble(rent.getValue().toString());
			 else
			 {
				 isValid = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_RENT_REQUIRED));

			 }
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
		 }
		 try{
			 if(EWUtilityNo.getValue()!=null&&EWUtilityNo.getValue().toString().trim().length()>0)
				 Double.parseDouble(EWUtilityNo.getValue().toString());
		
			 
			 //nf.parse(getUnitArea());
		 }
		 catch (NumberFormatException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_EW_UTILITY_NUMBER));
		 }
		 catch (IllegalArgumentException e) {
			 isValid = false;
			 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_EW_UTILITY_NUMBER));
		 }
		 return isValid;
	 }
	 
		private boolean isValidateUnitStatus()
		{
			boolean isValidated = true;
			UnitView unitView = new UnitView();
			if(viewMap.get("RENTED_UNIT_DD") != null)
				unitStatusRentedDD = (DomainDataView) viewMap.get("RENTED_UNIT_DD");
			
			if(viewMap.get("VACANT_UNIT_DD") != null)
				unitStatusVacantDD = (DomainDataView) viewMap.get("VACANT_UNIT_DD");
			
			if( viewMap.get(UNIT_POPUP_VIEW) != null )
				unitView = (UnitView)viewMap.get(UNIT_POPUP_VIEW);
			try
			{
			//if user is going to change the unit status from rented unit then perform this check
			if(unitView != null && unitView.getStatusId().compareTo(unitStatusRentedDD.getDomainDataId()) == 0 && 
				selectedUnitStaus != null && selectedUnitStaus.getValue() != null )
				{if(isAnyActiveContractWithUnit(unitView.getUnitId()))
					{
						errorMessages.add(CommonUtil.getBundleMessage("changeUnitStatus.errMsg.associatedWithContract"));
						isValidated = false;
					}
				}
			//if user is going to mark current unit as rented
			else if(selectedUnitStaus != null && selectedUnitStaus.getValue() != null && selectedUnitStaus.getValue().toString().compareTo(unitStatusRentedDD.getDomainDataId().toString())==0)
				{if(!isAnyActiveContractWithUnit(unitView.getUnitId()))
					{ 
					 errorMessages.add(CommonUtil.getBundleMessage("changeUnitStatus.errMsg.noContractAssociated"));
					 isValidated = false;
					}
				}
			}
			catch (PimsBusinessException e) 
			{
				logger.LogException("isValidateUnitStatus crashed | ", e);
			}
			return isValidated;
		}
 
		private boolean isAnyActiveContractWithUnit(Long unitId) throws PimsBusinessException 
		{
			
			return new PropertyService().isAnyActiveContractWithUnit(unitId);
		}

		private String getNumberFormat(){
			String methodName ="getNumberFormat";
			logger.logInfo(methodName+ " started");
			WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			logger.logInfo(methodName+" compelted");
			return localeInfo.getNumberFormat();
		}
		
		
		
		
       private void transformUnitViewToReceivePropertyUnitView(ReceivePropertyUnitView dest  ,UnitView source)
       {

    	   dest.setAccountNumber(source.getAccountNumber()) ;
    	   dest.setCreatedBy(source.getCreatedBy()) ;
    	   dest.setCreatedOn(source.getCreatedOn() );
    	   dest.setDewaNumber(source.getDewaNumber()) ;
    	   if(source.getEWUtilitNo()!=null)
    	      dest.setEwUtilityNumber(source.getEWUtilitNo().toString()) ;
    	   dest.setFloor(source.getFloor()) ;
    	   dest.setFloorId(source.getFloorId()) ;
    	   dest.setFloorName(source.getFloorName()) ;
    	   dest.setFloorNumber(source.getFloorNumber()) ;
    	   dest.setGracePeriod(source.getGracePeriod()) ;
    	   dest.setGracePeriodTypeAr(source.getGracePeriodTypeAr()) ;
    	   dest.setGracePeriodTypeEn(source.getGracePeriodTypeEn()) ;
    	   dest.setGracePeriodTypeId(source.getGracePeriodTypeId()) ;
    	   dest.setIncludeFloorNumber(source.getIncludeFloorNumber()) ;
    	   dest.setInvestmentTypeAr(source.getInvestmentTypeAr()) ;
    	   dest.setInvestmentTypeEn(source.getInvestmentTypeEn()) ;
    	   dest.setInvestmentTypeId(source.getInvestmentTypeId()) ;
    	   dest.setIsDeleted(source.getIsDeleted()) ;
    	   dest.setIsSelected(source.getIsSelected()) ;
    	   dest.setNoOfBath(source.getNoOfBath()) ;
    	   dest.setNoOfBed(source.getNoOfBed()) ;
    	   dest.setNoOfLiving(source.getNoOfLiving()) ;
    	   dest.setPropertyCommercialName(source.getPropertyCommercialName()) ;
    	   dest.setOwnerShipTypeId( source.getPropertyCategoryId() );
    	   dest.setPropertyNumber(source.getPropertyNumber()) ;
    	   dest.setRecordStatus(source.getRecordStatus()) ;
    	   dest.setRentProcessAr(source.getRentProcessAr()) ;
    	   dest.setRentProcessEn(source.getRentProcessEn()) ;
    	   dest.setRentProcessId(source.getRentProcessId()) ;
    	   dest.setRentValue(source.getRentValue()) ;
    	   dest.setRequiredPrice(source.getRequiredPrice()) ;
    	   dest.setSelected(source.getSelected()) ;
    	   dest.setMigrated(source.getMigrated());
    	   if(source.getStatusId()!=null)
    	      dest.setStatus(source.getStatusId().toString()) ;
    	   dest.setStatusAr(source.getStatusAr()) ;
    	   dest.setStatusEn(source.getStatusEn()) ;
    	   dest.setUsageTypeId(source.getUsageTypeId()) ;
    	   dest.setUsageTypeEn(source.getUsageTypeEn()) ;
    	   dest.setUsageTypeAr(source.getUsageTypeAr()) ;
    	   dest.setUpdatedOn(new Date()) ;
    	   dest.setUpdatedBy(getLoggedInUserId()) ;
    	   dest.setUnitTypeId(source.getUnitTypeId()) ;
    	   dest.setUnitTypeEn(source.getUnitTypeEn()) ;
    	   dest.setUnitTypeAr(source.getUnitTypeAr()) ;
    	   dest.setUnitType(source.getUnitTypeVal()) ;
    	   dest.setUnitSideTypeEn(source.getUnitSideTypeEn());
    	   dest.setUnitSideTypeAr(source.getUnitSideTypeAr());
    	   dest.setUnitSideTypeId(source.getUnitSideTypeId());
    	   dest.setUnitRemarks(source.getUnitRemarks());
    	   dest.setEjariReferenceNumber(source.getEjariReferenceNumber());
    	   dest.setUnitPrefix(source.getUnitPrefix());
    	 
    	   
    	    dest.setUnitNo(source.getUnitNo());
    	   dest.setUnitNumber(source.getUnitNumber());
    	   dest.setUnitName(source.getUnitName());
    	   dest.setUnitId(source.getUnitId());
    	   dest.setUnitDesc(source.getUnitDesc());
    	   dest.setUnitArea(source.getUnitArea());
    	   dest.setUnitAddress(source.getUnitAddress());
    	   dest.setStatusId(source.getStatusId());
    	   
       }
		
		@SuppressWarnings("unchecked")
		private void generatePropertyUnits()
		 {
 			 String method = "generatePropertyUnits";
			 logger.logInfo(method + " started...");

			 try
			 {
				 if(validateUnitScreen())
				 {
					 Long unitTypeId = Convert.toLong(getUnitType());			
					 Long unitSideId = Convert.toLong(getUnitSide());
					 Long usageTypeId= Convert.toLong(getUnitUsage());
					 Long investmentTypeId= Convert.toLong(getInvestmentType());
					 PropertyServiceAgent psAgent = new PropertyServiceAgent();
					 ReceivePropertyUnitView vo = (ReceivePropertyUnitView)viewMap.get("UNIT_VIEW"); 
					 if(unitPrefix.getValue()==null || unitPrefix.getValue().toString().trim().length()<=0)
						 unitPrefix.setValue("");
					 String unitIndex = (vo.getUnitNo() !=null)? Integer.toString(vo.getUnitNo().intValue()):"";
					if( vo.getMigrated() == null || vo.getMigrated().longValue() == 0l )
					{
						 if(this.getIncludeFloorNumberVar())
						 {
							 vo.setIncludeFloorNumber(1L);
							 FloorView floorView = new PropertyServiceAgent().getFloorById(Long.valueOf(hiddenFloorId.getValue().toString()));
							 if(floorView.getFloorNo()!=null)
							   vo.setUnitNumber(floorView.getFloorNumber()+unitPrefix.getValue().toString()+ unitIndex);
							 else
							   vo.setUnitNumber(unitPrefix.getValue().toString() + unitIndex);
						 }
						else
						{
							 vo.setIncludeFloorNumber(0L);
							 vo.setUnitNumber(unitPrefix.getValue().toString() + unitIndex);
						}
					}
					 
					if(selectedUnitStaus != null && selectedUnitStaus.getValue() != null &&  ! selectedUnitStaus.getValue().toString().equals("-1"))
						vo.setStatusId(Long.parseLong(selectedUnitStaus.getValue().toString()));
					else
						vo.setStatusId(Long.valueOf(hiddenStatusId.getValue().toString()));
					 vo.setUnitName(	unitPrefix.getValue().toString().trim()+unitNo.getValue().toString().trim());
					 vo.setUnitPrefix(unitPrefix.getValue().toString().trim());
					 vo.setIsDeleted(new Long(0));
					 vo.setRecordStatus(new Long(1));
					 vo.setFloorId(Long.valueOf(hiddenFloorId.getValue().toString()));
					 vo.setUnitId(Long.valueOf(hiddenUnitId.getValue().toString()));
					 vo.setUnitTypeId(unitTypeId);
					 vo.setUnitSideTypeId(unitSideId);
					 vo.setUsageTypeId(usageTypeId);
					 vo.setInvestmentTypeId(investmentTypeId);
					 vo.setUnitDesc(description.getValue().toString());
					 vo.setAccountNumber(accountNumber.getValue().toString());
					 if(areaInSquare.getValue()!=null)
						 vo.setUnitArea(Double.valueOf(areaInSquare.getValue().toString()));
					 if(bathrooms.getValue()!=null)
						 vo.setNoOfBath(Long.valueOf(bathrooms.getValue().toString()));
					 if(bedrooms.getValue()!=null)
						 vo.setNoOfBed(Long.valueOf(bedrooms.getValue().toString()));
					 if(livingRooms.getValue()!=null)
						 vo.setNoOfLiving(Long.valueOf(livingRooms.getValue().toString()));
					 if(getEWUtilityNo().getValue()!=null&&getEWUtilityNo().getValue().toString().trim().length()>0)
						 vo.setEwUtilityNumber(getEWUtilityNo().getValue().toString());
					 if(getRent().getValue()!=null)
					 {
						 vo.setRentValue(Double.valueOf(getRent().getValue().toString()));
						 vo.setRequiredPrice(Double.valueOf(getRent().getValue().toString()));
					 }
					 if(unitRemarks.getValue()!=null)
  					    vo.setUnitRemarks(unitRemarks.getValue().toString());
					 if(ejariRefNum.getValue() != null && ejariRefNum.getValue().toString().trim().length()>0)
					 {
						 vo.setEjariReferenceNumber(ejariRefNum.getValue().toString().trim());
					 }
					 if(hiddenUnitId.getValue()!=null && hiddenUnitId.getValue().toString().trim().length()>0)
						 vo.setUnitId(Long.valueOf(hiddenUnitId.getValue().toString()));
					
					 if (!psAgent.updatePropertyUnit(vo) )
						 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UNITS_ACCOUNT_NUM_ALREADY_EXISTS));
					 else
						 {
						 	UnitView unitView = (UnitView)viewMap.get(UNIT_POPUP_VIEW);
						 	infoMessage =  ResourceUtil.getInstance().getProperty(MessageConstants.UNITS_UPDATED_SUCCESSFULLY);
						 	if(selectedUnitStaus != null && selectedUnitStaus.getValue() != null && ! selectedUnitStaus.getValue().toString().equals("-1"))
						 		prepareLoggingMessage();
						 	if(  rent.getValue() != null && rent.getValue().toString().trim().length() > 0  )
						 	{
						 		prepareLoggingForUnitRentUpdated(unitView);
						 	}
						 	viewMap.put(UNIT_POPUP_VIEW ,psAgent.getUnitDetailsByUnitId(unitView.getUnitId() ) );
						 }
				 }
			 }

			 catch(Exception ex)
			 {
				 logger.LogException("Crashed while generating units:",ex);
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UNITS_UPDATE_GENERAL_ERROR));
			 }



		 }



		@SuppressWarnings( "unchecked" )
		public void onActivityLogTab()
		{
			try	
			{
	            RequestHistoryController rhc=new RequestHistoryController();
			    rhc.getAllRequestTasksForRequest( WebConstants.UNIT_STATUS,  hiddenUnitId.getValue().toString() );
			}
			catch(Exception ex)
			{
				logger.LogException("onActivityLogTab|Error Occured..",ex);
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			}
		}	

		private void prepareLoggingForUnitRentUpdated( UnitView  unitView ) throws Exception 
		{
			Double rentValue = Double.valueOf( rent.getValue().toString() );
			Locale localeAr = new Locale( "ar" );
			Locale localeEn = new Locale( "en" );
			ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
			ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
			
			String sysMsgEn = java.text.MessageFormat.format( 
																bundleEn.getString( "changeUnit.loggMsg.unitRentChanged"),
																unitNo.getValue().toString(),
																unitView.getRentValue(),
																rentValue 
															); 	
			String sysMsgAr = java.text.MessageFormat.format( 
																bundleAr.getString( "changeUnit.loggMsg.unitRentChanged"),
																unitNo.getValue().toString(),
																unitView.getRentValue(),
																rentValue
															);
			if( unitView.getRentValue().compareTo(rentValue)!=  0 )
			{
				
				NotesController.saveSystemNotesForRequest( WebConstants.UNIT_STATUS , sysMsgEn,sysMsgAr,Long.parseLong(hiddenUnitId.getValue().toString()));
			}
		}
		private void prepareLoggingMessage()throws PimsBusinessException
		{
			// TODO Auto-generated method stub
			Locale localeAr = new Locale( "ar" );
			Locale localeEn = new Locale( "en" );
			ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
			ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
			String oldStatusEn = "";
			String	newStatusEn = "";
			String oldStatusAr = "";
			String	newStatusAr = "";
			DomainDataView oldStatusDD = new DomainDataView();
			DomainDataView newStatusDD = new DomainDataView();
			oldStatusDD = getUnitStatusDDByStatusId(Long.parseLong(hiddenStatusId.getValue().toString()));
			newStatusDD = getUnitStatusDDByStatusId(Long.parseLong(selectedUnitStaus.getValue().toString()));
			
			oldStatusEn = oldStatusDD.getDataDescEn();
			oldStatusAr = oldStatusDD.getDataDescAr();
			newStatusEn = newStatusDD.getDataDescEn();
			newStatusAr = newStatusDD.getDataDescAr();
			
			String sysMsgEn = java.text.MessageFormat.format( bundleEn.getString( MessageConstants.UNIT_STATUS_CHANGED_LOGG ),
					 unitNo.getValue().toString(),
					 oldStatusEn,
					 newStatusEn); 	
			String sysMsgAr = java.text.MessageFormat.format( bundleAr.getString( MessageConstants.UNIT_STATUS_CHANGED_LOGG ),
					 unitNo.getValue().toString(),
					 oldStatusAr,
					 newStatusAr);
			 NotesController.saveSystemNotesForRequest( WebConstants.UNIT_STATUS , sysMsgEn,sysMsgAr,Long.parseLong(hiddenUnitId.getValue().toString()));
		}

		public HtmlInputHidden getHiddenFloorId() {
			return hiddenFloorId;
		}

		public void setHiddenFloorId(HtmlInputHidden hiddenFloorId) {
			this.hiddenFloorId = hiddenFloorId;
		}

		public HtmlInputHidden getHiddenPropertyId() {
			return hiddenPropertyId;
		}

		public void setHiddenPropertyId(HtmlInputHidden hiddenPropertyId) {
			this.hiddenPropertyId = hiddenPropertyId;
		}

		@SuppressWarnings("unchecked")
		public String getInvestmentType() {
			if( viewMap.get("investmentType") != null )
			{
				this.investmentType = viewMap.get("investmentType").toString();
			}
			return investmentType;
		}
		@SuppressWarnings("unchecked")
		public void setInvestmentType(String investmentType) {
			this.investmentType = investmentType;
			if(this.investmentType  != null && this.investmentType.trim().length() > 0 )
			{
				viewMap.put("investmentType",this.investmentType );
			}
		}

		@SuppressWarnings("unchecked")
		public String getUnitSide() {
			if( viewMap.get("unitSide") != null )
			{
				this.unitSide = viewMap.get("unitSide").toString();
			}
			return unitSide;
		}

		@SuppressWarnings("unchecked")
		public void setUnitSide(String unitSide) {
			this.unitSide = unitSide;
			if(this.unitSide != null && this.unitSide.trim().length() > 0 )
			{
				viewMap.put("unitSide",this.unitSide);
			}
		}
		@SuppressWarnings("unchecked")
		public String getUnitType() {
			if( viewMap.get("unitType") != null )
			{
				this.unitType = viewMap.get("unitType").toString();
			}
			return unitType;
		}
		@SuppressWarnings("unchecked")
		public void setUnitType(String unitType) {
			this.unitType = unitType;
			if(this.unitType != null && this.unitType .trim().length() > 0 )
			{
				viewMap.put("unitType",this.unitType );
			}
		}
		@SuppressWarnings("unchecked")
		public String getUnitUsage() {
			if( viewMap.get("unitUsage") != null )
			{
				this.unitUsage= viewMap.get("unitUsage").toString();
			}
			return unitUsage;
		}
		@SuppressWarnings("unchecked")
		public void setUnitUsage(String unitUsage) {
			this.unitUsage = unitUsage;
			if(this.unitUsage  != null && this.unitUsage.trim().length() > 0 )
			{
				viewMap.put("unitUsage",this.unitUsage);
			}
		}
		public HtmlInputText getPropCommercialName() {
			
			return propCommercialName;
		}
		public void setPropCommercialName(HtmlInputText propCommercialName) {
			this.propCommercialName = propCommercialName;
		}

		public HtmlInputText getPropAddress() {
			return propAddress;
		}

		public void setPropAddress(HtmlInputText propAddress) {
			this.propAddress = propAddress;
		}

		public HtmlInputText getAccountNumber() {
			return accountNumber;
		}

		public void setAccountNumber(HtmlInputText accountNumber) {
			this.accountNumber = accountNumber;
		}

		public String getInfoMessage() {
			return infoMessage;
		}

		public void setInfoMessage(String infoMessage) {
			this.infoMessage = infoMessage;
		}

		public HtmlSelectOneMenu getSelectedUnitStaus() {
			return selectedUnitStaus;
		}

		public void setSelectedUnitStaus(HtmlSelectOneMenu selectedUnitStaus) {
			this.selectedUnitStaus = selectedUnitStaus;
		}

		public List<DomainDataView> getUnitStatusListDD() {
			return unitStatusListDD;
		}

		public void setUnitStatusListDD(List<DomainDataView> unitStatusListDD) {
			this.unitStatusListDD = unitStatusListDD;
		}

		public DomainDataView getUnitStatusVacantDD() {
			return unitStatusVacantDD;
		}

		public void setUnitStatusVacantDD(DomainDataView unitStatusVacantDD) {
			this.unitStatusVacantDD = unitStatusVacantDD;
		}

		public DomainDataView getUnitStatusRentedDD() {
			return unitStatusRentedDD;
		}

		public void setUnitStatusRentedDD(DomainDataView unitStatusRentedDD) {
			this.unitStatusRentedDD = unitStatusRentedDD;
		}
		@SuppressWarnings("unchecked")
		private DomainDataView getUnitStatusDDByStatusId(Long id)
		{
			DomainDataView unit = new DomainDataView();
			if(viewMap.get("UNIT_STATUS_DD_LIST") != null)
			{
				unitStatusListDD = (List<DomainDataView>) viewMap.get("UNIT_STATUS_DD_LIST") ;
				for(DomainDataView unitDD : unitStatusListDD)
				{
					if(unitDD.getDomainDataId().compareTo(id) == 0)
					{
						unit = unitDD;
						break;
					}
				}
			}
			return unit;
		}

		public HtmlInputText getEjariRefNum() {
			return ejariRefNum;
		}

		public void setEjariRefNum(HtmlInputText ejariRefNum) {
			this.ejariRefNum = ejariRefNum;
		}
		
}
