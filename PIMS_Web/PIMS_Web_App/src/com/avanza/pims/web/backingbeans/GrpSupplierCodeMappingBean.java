package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;

import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.GrpSupplierCodeMapping;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.ui.util.ResourceUtil;

//TODO::

public class GrpSupplierCodeMappingBean extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private HtmlDataTable dataTable;

	private List<GrpSupplierCodeMapping> dataList = new ArrayList<GrpSupplierCodeMapping> ();
    private String selectOneSupplierName;
	@Override
	public void init() {
		super.init();

		if (!isPostBack()) 
		{
			//loadDatalist();

		}
	}

	

		public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	@SuppressWarnings("unchecked")
	public List<GrpSupplierCodeMapping> getDataList() {
		if (viewMap.get("GrpSupplierCodeMapping") != null)
			return (ArrayList<GrpSupplierCodeMapping>) viewMap.get("GrpSupplierCodeMapping");

		return dataList;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<GrpSupplierCodeMapping> dataList) 
	{

		this.dataList = dataList;
		if (this.dataList != null)
			viewMap.put("MEMS_NOL_TYPE_LIST", this.dataList);

	}

	@SuppressWarnings("unchecked")
	private void loadDatalist() 
	{

		try 
		{
			//Crit
			dataList = EntityManager.getBroker().findAll(GrpSupplierCodeMapping.class);
			if (this.dataList != null)
			{
				this.setDataList(this.dataList);
			}

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getUserGroups() crashed ", e);
		}
	}



	public String getSelectOneSupplierName() {
		return selectOneSupplierName;
	}



	public void setSelectOneSupplierName(String selectOneSupplierName) {
		this.selectOneSupplierName = selectOneSupplierName;
	}


}
