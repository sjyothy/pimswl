package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.ui.util.ResourceUtil;


public class SplitPaymentScheduleBacking  extends AbstractController
{
     /**
	 * 
	 */
	private static final long serialVersionUID = 6100425815097470767L;


	private List<String> errorMessages;
	private static Logger logger = Logger.getLogger( SplitPaymentScheduleBacking.class );
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings( "unchecked" )
	Map sessionMap;
	@SuppressWarnings( "unchecked" )
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	HttpServletRequest request =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	
	
	private PaymentScheduleView paymentScheduleView;
	private List<PaymentScheduleView> paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
	private HtmlDataTable dataTablePaymentSchedule = new HtmlDataTable();
	private HtmlSelectOneMenu htmlPaymentMethod = new HtmlSelectOneMenu();
	private HtmlCalendar htmlCalendar = new HtmlCalendar();
	private String selectedPaymentMode ="";	
	public String splitAmount;
	

	public String getSelectedPaymentMode() {
		return selectedPaymentMode;
	}

	public void setSelectedPaymentMode(String selectedPaymentMode) {
		this.selectedPaymentMode = selectedPaymentMode;
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	@SuppressWarnings( "unchecked" )
	    public void init() 
	    {
	    	logger.logInfo("init|Start");
	    	super.init();
	    	sessionMap =context.getExternalContext().getSessionMap();
	    	
	    	if(!viewRootMap.containsKey("PAYMENT_METHOD_LIST"))
	    	{
	    		PropertyServiceAgent psa =new PropertyServiceAgent();
	    		Map<Long, DomainDataView> paymentMethods = new HashMap<Long, DomainDataView>(0);
	    		try
	    		{
	    			List<DomainDataView> ddv= (ArrayList<DomainDataView>)psa.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_MODE);
	    			for(DomainDataView domainDataView : ddv )
	    				paymentMethods.put(domainDataView.getDomainDataId(), domainDataView);
	    			
	    			viewRootMap.put("PAYMENT_METHOD_LIST", paymentMethods);
	    		}
	    		catch(Exception exp)
	    		{
	    			//DO Nothing
	    		}
	    	}
	    	
	    	if(!isPostBack())
	    	{
	    		viewRootMap.put("PAYMENT_SCHDULE_SPLIT_LIST", new ArrayList<PaymentScheduleView>(0));
	    		if(sessionMap.containsKey("PAYMENT_SCHDULE_SPLIT"))
		    	{
		    		viewRootMap.put("PAYMENT_SCHDULE_SPLIT", sessionMap.remove("PAYMENT_SCHDULE_SPLIT"));
		    	}
	    		setDefaultValues();
	    	}
	    		
	    	
	    }
    @SuppressWarnings( "unchecked" )
	private boolean validate(boolean isDone)
	{
		boolean validated = true;
		errorMessages = new ArrayList<String>(0);
		
		if(isDone)
		{
			paymentScheduleViewList = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");
			if(paymentScheduleViewList==null || paymentScheduleViewList.size()==0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.atleastOneSplitPaymentRequired"));
				validated = false;
			}
			else
			{
				PaymentScheduleView psOld = getPaymentScheduleView();
				Double amount =  psOld.getAmount();
				for(PaymentScheduleView paymentScheduleView: paymentScheduleViewList)
				{
					amount -= paymentScheduleView.getAmount();
				}
				if(amount != 0)
				{
					errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.sumshouldBeEqual") + psOld.getAmount() );
					validated = false;
				}
					
			}
		}
		else
		{
			if(htmlPaymentMethod==null || htmlPaymentMethod.getValue() ==null || htmlPaymentMethod.getValue().toString().equals(""))
			{
				errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.methodRequired"));
				validated = false;
			}
			if(htmlCalendar==null || htmlCalendar.getValue() ==null || htmlCalendar.getValue().toString().equals(""))
			{
				errorMessages.add(CommonUtil.getBundleMessage(""));
				validated = false;
			}
			if(splitAmount==null || splitAmount.toString().equals(""))
			{
				errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.splitAmountIsRequired"));
				validated = false;
			}
			else
			{
				Double amount = new Double(0);
				try
				{
					amount = Double.parseDouble(splitAmount);
					
					if(amount <= 0)
					{
						errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.splitAmountShouldBeGTZero"));
						validated = false;		
					}
				}
				catch(NumberFormatException numberFormatException)
				{					 
					 errorMessages.add(CommonUtil.getBundleMessage("replaceCheque.errMsg.invalidSplitAmount"));
					 validated = false;					 
				}
				
			}
		}
		
		return validated;
	}
	@SuppressWarnings( "unchecked" )
	public void btnDone_Click()
    {
    	
    		try
    		{
    			if(validate(true))
    	    	{
		    		PaymentScheduleView psOld = getPaymentScheduleView();
		    		paymentScheduleViewList = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");
		    		PropertyService ps  = new PropertyService();
		    		ps.SplitPaymentSchedules(psOld, paymentScheduleViewList);
		    		sessionMap.put("PAYMENT_SCHDULE_SPLIT_DONE",psOld.getRequestId());
		    		executeJavaScript("window.opener.document.forms[0].submit();window.close();");
    		    }
    		}
    		catch(Exception exp)
    		{
    			errorMessages = new ArrayList<String>(0);
    			logger.logError("btnDone_Click|Failed to persist split payment schedules", exp);
        		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		}
    	
    
    }
	public void executeJavaScript(String javaScriptText) throws Exception 
	{
		AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
		addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);
	}
	@SuppressWarnings( "unchecked" )
	public void deleteRow()
    {

    	try
    	{
	    	logger.logInfo("deleteRow|Start...");
	    	paymentScheduleViewList = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");
	    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	    	paymentScheduleViewList.remove(psv);
	    	viewRootMap.put("PAYMENT_SCHDULE_SPLIT_LIST", paymentScheduleViewList);  
	    	setDefaultValues();
	    	logger.logInfo("deleteRow| Finish...");
    	}
    	catch (Exception e) 
    	{
			logger.LogException("deleteRow| crashed |",e);
    		errorMessages = new ArrayList<String>(0);
        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    }
	@SuppressWarnings( "unchecked" )
	public void btnAdd_Click()
    {
    	try
    	{
	    	if(validate(false))
	    	{
	    		logger.logInfo("btnAdd_Click|Start...");    	
		    	paymentScheduleViewList = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");	    	
		    	PaymentScheduleView psSplit = new PaymentScheduleView();	    	
		    	psSplit.setAmount(Double.parseDouble(splitAmount));
		    	psSplit.setPaymentDueOn((Date)htmlCalendar.getValue());
		    	psSplit.setPaymentModeId(Long.parseLong(htmlPaymentMethod.getValue().toString()));
		    	psSplit.setCreatedBy(getLoggedInUserId());
		    	psSplit.setUpdatedBy(getLoggedInUserId());
		    	if(viewRootMap.containsKey("PAYMENT_METHOD_LIST"))
		    	{
		    		DomainDataView dv = ((Map<Long, DomainDataView>) viewRootMap.get("PAYMENT_METHOD_LIST")).get(Long.parseLong(htmlPaymentMethod.getValue().toString()));
			    	psSplit.setPaymentModeEn(dv.getDataDescEn());
			    	psSplit.setPaymentModeAr(dv.getDataDescAr());
		    	}
		    	paymentScheduleViewList.add(psSplit);	    	
		    	viewRootMap.put("PAYMENT_SCHDULE_SPLIT_LIST", paymentScheduleViewList);
		    	setDefaultValues();
		    	logger.logInfo("btnAdd_Click|Finish...");
    	    }
    	}
    	catch (Exception e) 
    	{
			logger.LogException("btnAdd_Click| crashed |",e);
    		errorMessages = new ArrayList<String>(0);
        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    }
	@SuppressWarnings( "unchecked" )
	private void setDefaultValues()
	{
		PaymentScheduleView psOld = getPaymentScheduleView();
		if(psOld !=null)
		{
			htmlCalendar.setValue(psOld.getPaymentDueOn());
			Double amount = psOld.getAmount();
			htmlPaymentMethod.setValue(psOld.getPaymentModeId().toString());
			
			if(viewRootMap.containsKey("PAYMENT_SCHDULE_SPLIT_LIST"))
			{
				paymentScheduleViewList = (List<PaymentScheduleView>)viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");
				if(paymentScheduleViewList !=null && paymentScheduleViewList.size() >0)
				{
					for(PaymentScheduleView paymentScheduleView: paymentScheduleViewList)
					{
						amount -= paymentScheduleView.getAmount();
					}
					if(amount <0)
					{
						amount = new Double(0);
					}
				}
			}
			
			splitAmount = amount.toString();
		}
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
		
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

    @SuppressWarnings( "unchecked" )
	public PaymentScheduleView getPaymentScheduleView() 
	{
		paymentScheduleView = new PaymentScheduleView();
    	if(viewRootMap.containsKey("PAYMENT_SCHDULE_SPLIT"))
    	{
    		paymentScheduleView = (PaymentScheduleView) viewRootMap.get("PAYMENT_SCHDULE_SPLIT");
    	}
    	return paymentScheduleView;
	}
    @SuppressWarnings( "unchecked" )
	public void setPaymentScheduleView(PaymentScheduleView paymentScheduleView) {
		this.paymentScheduleView = paymentScheduleView;
	}
    @SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentScheduleViewList() 
	{
		if(viewRootMap.containsKey("PAYMENT_SCHDULE_SPLIT_LIST"))
				paymentScheduleViewList = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHDULE_SPLIT_LIST");
		return paymentScheduleViewList;
	}

	public void setPaymentScheduleViewList(
			List<PaymentScheduleView> paymentScheduleViewList) {
		this.paymentScheduleViewList = paymentScheduleViewList;
	}

	public String getSplitAmount() {
		return splitAmount;
	}

	public void setSplitAmount(String splitAmount) {
		this.splitAmount = splitAmount;
	}

	public HtmlSelectOneMenu getHtmlPaymentMethod() {
		return htmlPaymentMethod;
	}

	public void setHtmlPaymentMethod(HtmlSelectOneMenu htmlPaymentMethod) {
		this.htmlPaymentMethod = htmlPaymentMethod;
	}

	public HtmlCalendar getHtmlCalendar() {
		return htmlCalendar;
	}

	public void setHtmlCalendar(HtmlCalendar htmlCalendar) {
		this.htmlCalendar = htmlCalendar;
	}

}