package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.RequiredDocumentsView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.ui.util.ResourceUtil;

public class ContractFollowUp extends AbstractController {

	
	public String txtRequestId;
	public String txtcontractNum;
	public String txtStatus;
	public String txtStartDate;
	public String txtEndDate;
	public String createdOn;
	public String createdBy;
	public String txtTenantNumber;
	public String txtTenantName;
	public String txtContractType;
	public String txtTotalContractValue;
	public String txtunitRefNum;
	public String txtUnitRentValue;
	public String txtpropertyName;
	public String txtUnitType;

	public String txtNewTenantName;
	public String txtNewTenantId;
	public String txtNewTenantNumber;
	public String txtTenantId;
	public String txtContractId;
	SystemParameters parameters=SystemParameters.getInstance();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	
	//my variable;
	public String contractId;
	public String contractNumber;
	public String followUpTypeId;
	public Date startDate;
	public Date endDate;
	public String followUpStatusId;
	public String tenantId;
	public String renderInfo ;
	private HtmlCommandButton btnRenderedTenant = new HtmlCommandButton();
	private HtmlCommandButton btnRenderedContract = new HtmlCommandButton();
	HashMap followUpMap = new HashMap ();
	
	
	
	private static Logger logger = Logger.getLogger(ContractFollowUp.class);
	
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	private HtmlDataTable tbl_RequiredDocs;
	private RequiredDocumentsView rdDataItem=new RequiredDocumentsView();
	private List<RequiredDocumentsView> rdDataList=new ArrayList<RequiredDocumentsView>(0);
	private HtmlDataTable tbl_PaymentSchedule;
	private PaymentScheduleView psDataItem=new PaymentScheduleView();
	private List<PaymentScheduleView> psDataList=new ArrayList<PaymentScheduleView>(0);
	ContractView contractView=null;
	TenantView newTenantView=null;
	RequestView requestView=new RequestView();
	private List<String> errorMessages=new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	String pageMode="";
	SystemParameters param=SystemParameters.getInstance();
	private final String PAGE_MODE_ADD ="PAGE_MODE_ADD";
	private final String PAGE_MODE_UPDATE ="PAGE_MODE_UPDATE";
	private final String PAGE_MODE_UPDATE_APPROVE ="PAGE_MODE_UPDATE_APPROVE";
	private final String PAGE_MODE_UPDATE_APPROVE_COLLECTED ="PAGE_MODE_UPDATE_APPROVE_COLLECTED";
	private final String PAGE_MODE_APPROVE_REJECT ="PAGE_MODE_APPROVE_REJECT";
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnApprove = new HtmlCommandButton();
	private HtmlCommandButton btnReject = new HtmlCommandButton();
	private HtmlCommandButton btnPay = new HtmlCommandButton();
	private HtmlCommandButton btnComplete = new HtmlCommandButton();
	private HtmlCommandButton btnNewTenant = new HtmlCommandButton();
	CommonUtil commonUtil=new CommonUtil();
	// Constructors

	/** default constructor */
	public ContractFollowUp() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

   
	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		String message;
		logger.logInfo(methodName + "|" + "Start...");
		String eventOutCome = "failure";
		PropertyServiceAgent psa = new PropertyServiceAgent();
		try {
                
				if(!putControlValuesInViews())
				{
			
					psa.addFollowUp(followUpMap);
					
					if (contractId!=null && !contractId.equals("")){
					logger.logDebug(methodName + "|" + "Save Attachments...Start");
					saveAttachments(contractId.toString());
					logger.logDebug(methodName + "|" + "Save Attachments...Finish");
					
					logger.logDebug(methodName + "|" + "Save Comments...Start");
					saveComments(new Long(contractId));
					logger.logDebug(methodName + "|" + "Save Comments...Finish");
					}
					

					message = ResourceUtil.getInstance().getProperty("common.messages.Save");   
			   		successMessages.add(message);
					
			   		eventOutCome = "success";

				}
				logger.logInfo(methodName + "|" + "Finish...");
			} 
			catch (Exception e) 
			{
				logger.logError(methodName + "|" + "Exception Occured::" + e);
			}
		return eventOutCome;
	}



			
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		
		
		try {
			
			
		} catch (Exception ex) {
			logger.LogException(methodName + "|" + "Exception Occured::", ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
	    	
		}
	}
	public void saveComments(Long contratId) throws Exception
    {
    	String methodName="saveComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.CONTRACT;
    		logger.logInfo(methodName + "notesOwner..."+notesOwner);
	    	NotesController.saveNotes(notesOwner, contratId);

	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	public void saveAttachments(String contractId)
    {
    	String methodName="saveAtttachments";
    	logger.logInfo("|"+"Start..");
    	
    	String notesOwner = WebConstants.CONTRACT;
    	try
    	{

    		String repositoryId = WebConstants.ATTACHMENT_REPOSITORY_ID;
    		logger.logInfo("|"+"repositoryId.."+repositoryId);
	    	String fileSystemRepository = WebConstants.FILE_SYSTEM_REPOSITORY_BOUNCE_CHEQUE;
	    	logger.logInfo("|"+"fileSystemRepository.."+fileSystemRepository);
	    	String user = getLoggedInUser();
	    	logger.logInfo("|"+"user.."+user);
	    	String entityId = contractId;
	    	logger.logInfo("|"+"entityId.."+entityId);
	    	logger.logInfo("|"+"Saving attachment..START");
	    	AttachmentController.saveAttachments(repositoryId, fileSystemRepository, getLoggedInUser(), entityId);
	    	logger.logInfo("|"+"Saving attachment..complete");
    	}
    	catch(Exception ex)
    	{
    		logger.logInfo("|"+"Error Occured.."+ex);
    	}
    	logger.logInfo("|"+"Finish..");
    }

  public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		if (!isPostBack()){
			btnRenderedTenant.setRendered(false);
			btnRenderedContract.setRendered(false);
		}
     
		if (renderInfo!=null && renderInfo.equals("true")){
			btnRenderedTenant.setRendered(true);
			btnRenderedContract.setRendered(true);
		}
		
		try
		{
			
		   
		}
		catch(Exception ex)
		{
		//	logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		 // logger.logInfo(methodName + "|" + "Finish...");
	}



			

	
	private boolean putControlValuesInViews() throws PimsBusinessException
	{
		String methodName="putControlValuesInViews";
		boolean isError=false;
		String message;
		PropertyServiceAgent psa = new PropertyServiceAgent();
		ContractView contView = new ContractView();
		logger.logInfo(methodName+"|"+"Start..");
		
		List<DomainDataView> ddvList;
		DomainDataView ddv;
		RequestFieldDetailView requestFieldDetailView=new RequestFieldDetailView();
		 
		if (contractId !=null && !contractId.equals("")){
			
			followUpMap.put("contractId", new Long(contractId));
		}
	    
		if (followUpTypeId !=null && !followUpTypeId.equals("") && !followUpTypeId.equals("-1")){
			followUpMap.put("followUpTypeId",new Long(followUpTypeId));
		}
		else
	   	{
	    	message = ResourceUtil.getInstance().getProperty("contractFollowUp.msg.followUpType");   
	   		errorMessages.add(message);
	   		isError=true;
	   		return isError;
	   	}
		if (startDate !=null && !startDate.equals("")){
			followUpMap.put("startDate", startDate);
		}
		else
	   	{
	    	message = ResourceUtil.getInstance().getProperty("contractFollowUp.msg.startDate");   
	   		errorMessages.add(message);
	   		isError=true;
	   		return isError;
	   	}
		if (endDate !=null && !endDate.equals("")){
			followUpMap.put("endDate", endDate);
		}
		else
	   	{
	    	message = ResourceUtil.getInstance().getProperty("contractFollowUp.msg.endDate");   
	   		errorMessages.add(message);
	   		isError=true;
	   		return isError;
	   	}
		if (followUpStatusId !=null && !followUpStatusId.equals("") && !followUpStatusId.equals("-1")){
			followUpMap.put("followUpStatusId",new Long(followUpStatusId));
		}
		 
				
		
		logger.logInfo(methodName+"|"+"Finish..");
		return isError;
	}
	

	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);

	}
	public String getSuccessMessages() {
		return commonUtil.getErrorMessages(this.successMessages);

	}
	
	
	
	
	
	public String getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}


	public String getTxtcontractNum() {
		return txtcontractNum;
	}

	public void setTxtcontractNum(String txtcontractNum) {
		this.txtcontractNum = txtcontractNum;
	}

	public String getTxtStatus() {
		return txtStatus;
	}

	public void setTxtStatus(String txtStatus) {
		this.txtStatus = txtStatus;
	}

	public String getTxtStartDate() {
		return txtStartDate;
	}

	public void setTxtStartDate(String txtStartDate) {
		this.txtStartDate = txtStartDate;
	}

	public String getTxtEndDate() {
		return txtEndDate;
	}

	public void setTxtEndDate(String txtEndDate) {
		this.txtEndDate = txtEndDate;
	}

	public String getTxtTenantNumber() {
		return txtTenantNumber;
	}

	public void setTxtTenantNumber(String txtTenantNumber) {
		this.txtTenantNumber = txtTenantNumber;
	}

	public String getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(String txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public String getTxtContractType() {
		return txtContractType;
	}

	public void setTxtContractType(String txtContractType) {
		this.txtContractType = txtContractType;
	}

	public String getTxtTotalContractValue() {
		return txtTotalContractValue;
	}

	public void setTxtTotalContractValue(String txtTotalContractValue) {
		this.txtTotalContractValue = txtTotalContractValue;
	}

	public String getTxtUnitRentValue() {
		return txtUnitRentValue;
	}

	public void setTxtUnitRentValue(String txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}

	public String getTxtpropertyName() {
		return txtpropertyName;
	}

	public void setTxtpropertyName(String txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}

	public String getTxtUnitType() {
		return txtUnitType;
	}

	public void setTxtUnitType(String txtUnitType) {
		this.txtUnitType = txtUnitType;
	}

	public String getTxtNewTenantName() {
		return txtNewTenantName;
	}

	public void setTxtNewTenantName(String txtNewTenantName) {
		this.txtNewTenantName = txtNewTenantName;
	}

	public String getTxtNewTenantId() {
		return txtNewTenantId;
	}

	public void setTxtNewTenantId(String txtNewTenantId) {
		this.txtNewTenantId = txtNewTenantId;
	}

	public String getTxtNewTenantNumber() {
		return txtNewTenantNumber;
	}

	public void setTxtNewTenantNumber(String txtNewTenantNumber) {
		this.txtNewTenantNumber = txtNewTenantNumber;
	}

	public String getTxtTenantId() {
		return txtTenantId;
	}

	public void setTxtTenantId(String txtTenantId) {
		this.txtTenantId = txtTenantId;
	}

	public String getTxtContractId() {
		return txtContractId;
	}

	public void setTxtContractId(String txtContractId) {
		this.txtContractId = txtContractId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public HtmlDataTable getTbl_PaymentSchedule() {
		return tbl_PaymentSchedule;
	}

	public void setTbl_PaymentSchedule(HtmlDataTable tbl_PaymentSchedule) {
		this.tbl_PaymentSchedule = tbl_PaymentSchedule;
	}

	public PaymentScheduleView getPsDataItem() {
		return psDataItem;
	}

	public void setPsDataItem(PaymentScheduleView psDataItem) {
		this.psDataItem = psDataItem;
	}

	public List<PaymentScheduleView> getPsDataList() {
		if(context.getViewRoot().getAttributes().containsKey("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE"))
			psDataList=(ArrayList<PaymentScheduleView>)context.getViewRoot().getAttributes().get("CHANGE_TENANT_NAME_PAYMENT_SCHEDULE");
		return psDataList;
	}

	public void setPsDataList(List<PaymentScheduleView> psDataList) {
		this.psDataList = psDataList;
	}



	public String getTxtRequestId() {
		return txtRequestId;
	}

	public void setTxtRequestId(String txtRequestId) {
		this.txtRequestId = txtRequestId;
	}

	public HtmlDataTable getTbl_RequiredDocs() {
		return tbl_RequiredDocs;
	}

	public void setTbl_RequiredDocs(HtmlDataTable tbl_RequiredDocs) {
		this.tbl_RequiredDocs = tbl_RequiredDocs;
	}

	public RequiredDocumentsView getRdDataItem() {
		return rdDataItem;
	}

	public void setRdDataItem(RequiredDocumentsView rdDataItem) {
		this.rdDataItem = rdDataItem;
	}

	public List<RequiredDocumentsView> getRdDataList() {
		if(sessionMap.containsKey("SESSION_CHNG_TENANT_REQ_DOC"))
			rdDataList= (ArrayList<RequiredDocumentsView>)sessionMap.get("SESSION_CHNG_TENANT_REQ_DOC");
		
		return rdDataList;
	}

	public void setRdDataList(List<RequiredDocumentsView> rdDataList) {
		this.rdDataList = rdDataList;
	}

	public String getPageMode() {
		pageMode= context.getViewRoot().getAttributes().get("pageMode").toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnPay() {
		return btnPay;
	}

	public void setBtnPay(HtmlCommandButton btnPay) {
		this.btnPay = btnPay;
	}

	public HtmlCommandButton getBtnNewTenant() {
		return btnNewTenant;
	}

	public void setBtnNewTenant(HtmlCommandButton btnNewTenant) {
		this.btnNewTenant = btnNewTenant;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getFollowUpTypeId() {
		return followUpTypeId;
	}

	public void setFollowUpTypeId(String followUpTypeId) {
		this.followUpTypeId = followUpTypeId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFollowUpStatusId() {
		return followUpStatusId;
	}

	public void setFollowUpStatusId(String followUpStatusId) {
		this.followUpStatusId = followUpStatusId;
	}

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat(){
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String btnTenant_Click()
	{
		
		String methodName="btnTenant_Click";
		logger.logInfo(methodName+"|"+"Start..");
		context.getExternalContext().getRequestMap().put("tenantId", tenantId);
		context.getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		
		logger.logInfo(methodName+"|"+"Finish..");
		return "TENANT_VIEW";
	}
	
	public String btnContract_Click()
	{
		
		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		context.getExternalContext().getSessionMap().put("contractId", contractId);
		logger.logInfo(methodName+"|"+"Finish..");
		return "CONTRACT_VIEW";
	}

	public String getRenderInfo() {
		return renderInfo;
	}

	public void setRenderInfo(String renderInfo) {
		this.renderInfo = renderInfo;
	}

	public HtmlCommandButton getBtnRenderedContract() {
		return btnRenderedContract;
	}

	public void setBtnRenderedContract(HtmlCommandButton btnRenderedContract) {
		this.btnRenderedContract = btnRenderedContract;
	}

	public HtmlCommandButton getBtnRenderedTenant() {
		return btnRenderedTenant;
	}

	public void setBtnRenderedTenant(HtmlCommandButton btnRenderedTenant) {
		this.btnRenderedTenant = btnRenderedTenant;
	}

}
