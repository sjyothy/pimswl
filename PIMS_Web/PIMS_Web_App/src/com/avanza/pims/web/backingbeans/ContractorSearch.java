package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.constructioncontract.ConstructionContractDetailsTab;
import com.avanza.pims.web.backingbeans.construction.constructioncontract.ConstructionContractDetailsTab.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BusinessActivityView;
import com.avanza.pims.ws.vo.ContractorTypeView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class ContractorSearch extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(ContractorSearch.class);

	public interface Keys {
		public static final String CONTRACTORS_LIST = "CONTRACTORS_LIST";
		public static final String RECORD_SIZE = "RECORD_SIZE";
		public static final String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static final String CONTRACTOR_TYPE_LIST = "CONTRACTOR_TYPE_LIST";
		public static final String LICENSE_SOURCE_LIST = "LICENSE_SOURCE_LIST";
		public static final String COUNTRY_LIST = "COUNTRY_LIST";
		public static final String STATE_LIST = "STATE_LIST";
		public static final String CITY_LIST = "CITY_LIST";
		public static final String VIEW_MODE = "viewMode";
		public static final String PAGE_MODE = "pageMode";
		public static final String POPUP_MODE = "popup";
		public static final String CONTRACTOR_TYPE_ID = "contractorTypeId";
		public static final String CONTRACTOR_TYPE = "CONTRACTOR_TYPE";
		public static final String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
		public static final String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
		
	}

	HttpServletRequest request = (HttpServletRequest) FacesContext
			.getCurrentInstance().getExternalContext().getRequest();
	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<String> errorMessages;
	private List<String> infoMessages;

	// UI component attributes
	private HtmlDataTable dataTable;

	// configurable values of data table
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	// attributes binded with UI Components to save their values entered by the
	// user
	private String grpNumber;
	private String contractorNumber;
	private String commercialName;
	private String contractorNameAr;
	private String contractorNameEn;
	private String contractorType;
	private String officeNumber;
	private String licenseNumber;
	private String licenseSource;
	private String country;
	private String state;
	private String city;
	
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;

	private HtmlSelectOneMenu cmbContractorType=new HtmlSelectOneMenu();
	// lists of UI components goes here
	private Map<String, String> contractorTypeList;
	private Map<String, String> licenseSourceList;
	private Map<String, String> countryList;
	private Map<String, String> stateList;
	private Map<String, String> cityList;
	private List<ContractorView> contractorsList;

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		try {
			if (!isPostBack()) 
			{
				if (request.getParameter(Keys.VIEW_MODE) != null )
					viewMap.put(Keys.VIEW_MODE, request.getParameter(
							Keys.VIEW_MODE).toString());
				else if (getRequestParam(Keys.VIEW_MODE) != null)
					viewMap.put(Keys.PAGE_MODE, getRequestParam(
							Keys.VIEW_MODE));
				
				
				if (request.getParameter(Keys.CONTRACTOR_TYPE_ID) != null ){
					viewMap.put(Keys.CONTRACTOR_TYPE_ID, request.getParameter(
							Keys.CONTRACTOR_TYPE_ID).toString());
					//for contractor Type 
					//viewMap.put(Keys.CONTRACTOR_TYPE_ID,WebConstants.CONTRACTOR_TYPE_CONSULTANT);
					//bcoz set value by default 
				    cmbContractorType.setDisabled(true);
				}
				else if (getRequestParam(Keys.CONTRACTOR_TYPE_ID) != null ){
					viewMap.put(Keys.CONTRACTOR_TYPE_ID, getRequestParam(
							Keys.CONTRACTOR_TYPE_ID).toString());
					//for contractor Type 
					//viewMap.put(Keys.CONTRACTOR_TYPE_ID,WebConstants.CONTRACTOR_TYPE_CONSULTANT);
					//bcoz set value by default 
				    cmbContractorType.setDisabled(true);
				}
				
				if(request.getParameter(Keys.PAGE_MODE) != null)
					viewMap.put(Keys.PAGE_MODE,request.getParameter(Keys.PAGE_MODE).toString());
				
				loadMapValues();
				
				// FROM RECEIVE PROPOSAL SCREEN AS POPUP - Hammad
				String tenderId = (String) sessionMap.get(WebConstants.ReceiveTenderProposals.TENDER_ID_FOR_CONTRACTOR_SEARCH);
				sessionMap.remove( WebConstants.ReceiveTenderProposals.TENDER_ID_FOR_CONTRACTOR_SEARCH );
				if(StringHelper.isNotEmpty(tenderId))				
					viewMap.put(WebConstants.Tender.TENDER_ID, tenderId);
				//
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}

	// fills the maps that is populated in the UI components
	@SuppressWarnings("unchecked")
	private void loadMapValues() {
		loadContractorTypes();
		viewMap.put(Keys.CONTRACTOR_TYPE_LIST, contractorTypeList);

		loadLicenseSources();
		viewMap.put(Keys.LICENSE_SOURCE_LIST, licenseSourceList);

		loadCountry();
		viewMap.put(Keys.COUNTRY_LIST, countryList);

		cityList = new HashMap<String, String>();
		viewMap.put(Keys.CITY_LIST, cityList);

		stateList = new HashMap<String, String>();
		viewMap.put(Keys.STATE_LIST, stateList);
	}

	// loads the license source list
	private final void loadLicenseSources() {
		String methodName = "loadLicenseSources()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> licenseSourceDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.LicenseSource.LICENSE_SOURCE);

			if (licenseSourceList == null)
				licenseSourceList = new HashMap<String, String>();

			for (DomainDataView licenseSource : licenseSourceDDL)
				getLicenseSourceList().put(
						getIsEnglishLocale() ? licenseSource.getDataDescEn()
								: licenseSource.getDataDescAr(),
						licenseSource.getDomainDataId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads the country list
	private final void loadContractorTypes() {
		logger.logInfo("loadContractorTypes|Start");

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<ContractorTypeView> contractTypeViewList = csa
					.getAllContractorType();

			if (contractorTypeList == null)
				contractorTypeList = new HashMap<String, String>();

			
			for (ContractorTypeView contractTypeView : contractTypeViewList){
				
				getContractorTypeList().put(
						getIsEnglishLocale() ? contractTypeView
								.getDescriptionEn() : contractTypeView.getDescriptionAr(),contractTypeView.getContractorTypeId().toString());
				if( viewMap.get(Keys.CONTRACTOR_TYPE_ID)!=null && 
					contractTypeView.getContractorTypeId().longValue()== Long.parseLong( viewMap.get(Keys.CONTRACTOR_TYPE_ID).toString() )
				  )
				{
			     this.setContractorType(contractTypeView.getContractorTypeId().toString());}
     			} 
			    
			logger.logInfo("loadContractorTypes|Finish");
		} catch (Exception e) {
			logger.LogException("loadContractorTypes|Error Occured ", e);
		}
	}

	// loads the country list
	private final void loadCountry() {
		String methodName = "loadCountry()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<RegionView> regionViewList = psa.getCountry();

			if (countryList == null)
				countryList = new HashMap<String, String>();

			for (RegionView rV : regionViewList)
				getCountryList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads all the states in the list
	public final void loadState() {
		String methodName = "loadState";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			getStateList().clear();
			getCityList().clear();
			Long selectedCountry = new Long(this.getCountry());
			if(selectedCountry != -1)
			{
				Set<RegionView> regionViewList = psa.getState(selectedCountry);
			

			for (RegionView rV : regionViewList)
				this.getStateList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			}
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	

	// loads all the cities in the list
	public final void loadCity() {
		String methodName = "loadCity";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			Set<RegionView> regionViewList = null;
			if (state != null && !state.trim().equals("-1"))
				regionViewList = psa.getCity(new Long(state));

			getCityList().clear();

			for (RegionView rV : regionViewList)
				getCityList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);
		}

	}

	// searches the contractors according to the criteria specified by the user
	@SuppressWarnings("unchecked")
	public void searchContractors() {
		logger.logInfo("searchContractors |Start");

		try {
			ContractorView contractorView = new ContractorView();
			contractorView.setGrpNumber(grpNumber);
			contractorView.setContractorNumber(contractorNumber);
			contractorView.setCommercialName(commercialName);
			contractorView.setContractorNameAr(contractorNameAr);
			contractorView.setContractorNameEn(contractorNameEn);
			if( this.getContractorType()!= null && !this.getContractorType().equals( "-1" ) )
			{
			 contractorView.setContractorTypeId(this.getContractorType());
			}
			contractorView.setOfficeNumber(officeNumber);
			contractorView.setLicenseNumber(licenseNumber);
			contractorView.setLicenseSource(licenseSource);
			contractorView.setCountry(country);
			contractorView.setState(state);
			contractorView.setCity(city);

			if(viewMap.get(WebConstants.Tender.TENDER_ID)!=null)
				contractorView.setTenderId(Long.valueOf(viewMap.get(WebConstants.Tender.TENDER_ID).toString()));
			
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			contractorsList = csa.getContractors(contractorView,
					getLicenseSourceList());

			if(contractorsList.isEmpty())
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			
			recordSize = contractorsList.size();
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;

			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;

			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

			viewMap.put(Keys.RECORD_SIZE, recordSize);
			viewMap.put(Keys.PAGINATOR_MAX_PAGES, paginatorMaxPages);
			viewMap.put(Keys.CONTRACTORS_LIST, contractorsList);

			logger.logInfo("searchContractors|completed Succesfully");
		} catch (PimsBusinessException e) {
			logger.LogException(  " searchContractors|crashed due to:",e);
		}
	}

	// Accessors starts from here
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public String getGrpNumber() {
		return grpNumber;
	}

	public void setGrpNumber(String grpNumber) {
		this.grpNumber = grpNumber;
	}

	public String getContractorNumber() {
		return contractorNumber;
	}

	public void setContractorNumber(String contractorNumber) {
		this.contractorNumber = contractorNumber;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public String getContractorNameAr() {
		return contractorNameAr;
	}

	public void setContractorNameAr(String contractorNameAr) {
		this.contractorNameAr = contractorNameAr;
	}

	public String getContractorNameEn() {
		return contractorNameEn;
	}

	public void setContractorNameEn(String contractorNameEn) {
		this.contractorNameEn = contractorNameEn;
	}

	public String getContractorType() {
		if(viewMap.containsKey(Keys.CONTRACTOR_TYPE_ID))
			contractorType = viewMap.get(Keys.CONTRACTOR_TYPE_ID).toString();
		return contractorType;
	}

	public void setContractorType(String contractorType) {
		this.contractorType = contractorType;
		if(this.contractorType !=null)
		viewMap.put(Keys.CONTRACTOR_TYPE,this.contractorType);	
	}


	
	
	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getLicenseSource() {
		return licenseSource;
	}

	public void setLicenseSource(String licenseSource) {
		this.licenseSource = licenseSource;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	@SuppressWarnings("unchecked")
	public List<ContractorView> getContractorsList() {
		if (viewMap.get(Keys.CONTRACTORS_LIST) != null)
			contractorsList = (List<ContractorView>) viewMap
					.get(Keys.CONTRACTORS_LIST);

		return contractorsList;
	}

	public void setContractorsList(List<ContractorView> contractorsList) {
		this.contractorsList = contractorsList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getContractorTypeList() {
		if (viewMap.get(Keys.CONTRACTOR_TYPE_LIST) != null)
			contractorTypeList = (Map<String, String>) viewMap
					.get(Keys.CONTRACTOR_TYPE_LIST);

		return contractorTypeList;
	}

	public void setContractorTypeList(Map<String, String> contractorTypeList) {
		this.contractorTypeList = contractorTypeList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getLicenseSourceList() {
		if (viewMap.get(Keys.LICENSE_SOURCE_LIST) != null)
			licenseSourceList = (Map<String, String>) viewMap
					.get(Keys.LICENSE_SOURCE_LIST);

		return licenseSourceList;
	}

	public void setLicenseSourceList(Map<String, String> licenseSourceList) {
		this.licenseSourceList = licenseSourceList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCityList() {
		if (viewMap.get(Keys.CITY_LIST) != null)
			cityList = (Map<String, String>) viewMap.get(Keys.CITY_LIST);

		return cityList;
	}

	public void setCityList(Map<String, String> cityList) {
		this.cityList = cityList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getStateList() {
		if (viewMap.get(Keys.STATE_LIST) != null)
			stateList = (Map<String, String>) viewMap.get(Keys.STATE_LIST);

		return stateList;
	}

	public void setStateList(Map<String, String> stateList) {
		this.stateList = stateList;
	}

	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if (viewMap.get(Keys.RECORD_SIZE) != null)
			recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);

		return recordSize == null ? 0 : recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if(this.recordSize != null)
			viewMap.put(Keys.RECORD_SIZE, this.recordSize);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCountryList() {
		if (viewMap.get(Keys.COUNTRY_LIST) != null)
			countryList = (Map<String, String>) viewMap.get(Keys.COUNTRY_LIST);

		return countryList;
	}

	public void setCountryList(Map<String, String> countryList) {
		this.countryList = countryList;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean getIsViewModePopUp() {
		/*return viewMap.get(Keys.VIEW_MODE) != null
				&& viewMap.get(Keys.VIEW_MODE).toString().trim()
						.equalsIgnoreCase(Keys.POPUP_MODE);*/
		
		if(getIsPageModeSelectManyPopUp() || getIsPageModeSelectOnePopUp())
			return true;
		
		else
			return false;
		
		
	}
	
	
	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		
		if (viewMap.containsKey(Keys.PAGE_MODE)
				&& viewMap.get(Keys.PAGE_MODE) != null
				&& viewMap.get(Keys.PAGE_MODE).toString().trim()
						.equalsIgnoreCase(Keys.MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}
	
	
	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewMap.containsKey(Keys.PAGE_MODE)
				&& viewMap.get(Keys.PAGE_MODE) != null
				&& viewMap.get(Keys.PAGE_MODE).toString().trim()
						.equalsIgnoreCase(Keys.MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public HtmlSelectOneMenu getCmbContractorType() {
		return cmbContractorType;
	}

	public void setCmbContractorType(HtmlSelectOneMenu cmbContractorType) {
		this.cmbContractorType = cmbContractorType;
	}
	
	public Long getContractorTypeId(){
		if(viewMap.containsKey(Keys.CONTRACTOR_TYPE_ID))
		return Long.parseLong(viewMap.get(Keys.CONTRACTOR_TYPE_ID).toString());
		else
		return null;	
			
	}
	
	public String openAddContractorPage()
	{
		return "addContractor";
	}
	
	public String viewDetails()
	{
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		ContractorView contractorView = new ContractorView();
		contractorView = (ContractorView)dataTable.getRowData();
		
		if(contractorView != null)
		 {
			request.setAttribute(WebConstants.PAGE_MODE, "detail_mode");
			request.setAttribute(WebConstants.CONTRACTOR_DETAILS, contractorView);	
		 }		
		
		return "goToDetailsPage";
		
		/*String javaScriptText = "javascript:showContractorDetailPopUp();";
		openPopup(javaScriptText);
		return "";
*/		
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public String editDetails()
	{
		
		
		
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		ContractorView contractorView = new ContractorView();
		contractorView = (ContractorView)dataTable.getRowData();
		
		if(contractorView != null)
		 {
			request.setAttribute(WebConstants.PAGE_MODE, "edit_mode");
			request.setAttribute(WebConstants.CONTRACTOR_DETAILS, contractorView);	
		 }
		
		
		return "goToDetailsPage";
	}
	
	public void removeContractor(){
	
		ContractorView contractor = new ContractorView();
		ConstructionServiceAgent csa = new ConstructionServiceAgent();
		try
		{
		if(dataTable.getRowCount() > 0)
		{
			contractor = (ContractorView)dataTable.getRowData();
			contractor = csa.getContractorById(contractor.getPersonId(), licenseSourceList);
			contractor.setIsDeleted(1L);
			
			for(ContractorView cView : contractorsList)
			{
				if(cView.getPersonId().compareTo(contractor.getPersonId()) == 0 )
					{
						contractorsList.remove(cView);
						break;
					}
			}
			
			Date date = new Date();
			contractor.setUpdatedBy((String)CommonUtil.getLoggedInUser());
			contractor.setUpdatedOn(date);
			contractor.setCreatedBy(CommonUtil.getLoggedInUser());
			contractor.setCreatedOn(date);
			csa.addContractor(contractor, contractor.getBusinessActivities(), contractor.getServiceTypes());
			setRecordSize(contractorsList.size());
			
		}
		}
		
		catch (Exception e) {
			logger.LogException("removeContractor crashed ", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}		
		
				
	}
	
	public void sendManyContractorInfoToParent(javax.faces.event.ActionEvent event)
	{
		  List<ContractorView> selectedContractors = new ArrayList<ContractorView>(0);
		  if(contractorsList != null && contractorsList.size() > 0)
    		{
    			for(ContractorView cView:contractorsList )
    				if(cView.getIsSelected())
    				{
    					selectedContractors.add(cView);
    				}    		
    		}
    		 		
    		logger.logInfo("getSelectedContractors completed successfully!!!");
    	
		  if(!selectedContractors.isEmpty())
	      {	    	  		 
	    	  	sessionMap.put(WebConstants.MANY_CONTRACTORS_SELECTED, selectedContractors);
		        String javaScriptText = "javascript:closeWindowSubmit();";		
		        sendToParent(javaScriptText);
	      }
	      else
	      {
	        	errorMessages.clear();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.CONTRACTOR_NOT_SELECTED));
	      }	      
	
	}
	
	
	public String sendToParent(String javaScript)
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText =javaScript;
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		return "";
	}

	
	
}