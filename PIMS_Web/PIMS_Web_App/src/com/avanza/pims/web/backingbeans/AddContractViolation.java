package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.proxy.issueclearanceletter.PIMSIssueClearanceLetterBPELPortClient;
import com.avanza.pims.proxy.types.issueclearanceletter.org.xmlsoap.schemas.ws._2003._03.addressing.AttributedURI;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.BidderAttendanceView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;

public class AddContractViolation extends AbstractController {
	transient Logger logger = Logger.getLogger(AddContractViolation.class);
	private String hdnContractId;
	private String hdnContractNumber;
	private String hdnTenantId;
	
	private HtmlInputText contractNoText = new HtmlInputText();
	private HtmlInputText tenantNameText = new HtmlInputText();
	private HtmlInputText contractStartDateText = new HtmlInputText();
	private HtmlInputText txtpropertyName = new HtmlInputText();
	private HtmlInputText txtunitRefNum = new HtmlInputText();
	private HtmlInputText txtUnitRentValue = new HtmlInputText();
	private HtmlInputText contractTypeText = new HtmlInputText();
	private HtmlInputText tenantNumberType = new HtmlInputText();
	private HtmlInputText contractEndDateText = new HtmlInputText();
	private HtmlInputText txtpropertyType = new HtmlInputText();
	private HtmlInputText txtUnitType = new HtmlInputText();
	private HtmlCommandLink populateContract = new HtmlCommandLink();
	
	private List<ViolationView> violationViewDataList = new ArrayList<ViolationView>();
	private HtmlDataTable dataTableViolation = new HtmlDataTable();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton addViolationButton = new HtmlCommandButton();
	private HtmlCommandLink editLink = new HtmlCommandLink();
	private HtmlCommandLink editActionLink = new HtmlCommandLink();
	private HtmlCommandLink deleteLink = new HtmlCommandLink();
	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;	
	private boolean enableDisableCommandLink;
	
	private List<String> successMessages;
	private List<String> errorMessages;
	
	CommonUtil commUtil  = new CommonUtil();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap= context.getExternalContext().getSessionMap();;
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	ContractView contractView = new ContractView();
	PropertyServiceAgent psa = new PropertyServiceAgent();

	private HtmlTabPanel richTabPanel = new HtmlTabPanel();

	
	
	public void init(){
		super.init();
		setEnableDisableCommandLink(true);
		if(viewMap.containsKey("NOT_RENDERED_COMMAND_LINK"))
		setEnableDisableCommandLink(false);
		
		try {
		
		if(!isPostBack()){
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			populateContract.setRendered(true);
			
			saveButton.setValue(ResourceUtil.getInstance().getProperty("commons.saveButton"));
		    //comming from contract Search 
			if(getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW)!=null){
				
				contractView = (ContractView)getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW);
				contractView  = getContractById(contractView.getContractId());
				populateContract();
				populateContract.setRendered(false);
				loadAttachmentsAndComments(contractView.getContractId());
				setViolationViewDataList(psa.getContractViolation(contractView.getContractUnitView()));
				viewMap.put("fromContractSearch", true);
			}else{
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				saveButton.setRendered(false);
				addViolationButton.setRendered(false);
			}
			
			
			
			
		}else{
		
		if(sessionMap.containsKey("NEW_VIOLATION_VIEW")){
			successMessages = new ArrayList<String>();
			populateContract.setRendered(false);
			setViolationViewDataList(psa.getContractViolation(((ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION")).getContractUnitView()));
			loadAttachmentsAndComments(((ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION")).getContractId());
			sessionMap.remove("NEW_VIOLATION_VIEW");
				if(sessionMap.containsKey("EDIT_DONE")){
					sessionMap.remove("EDIT_DONE");
					successMessages.add(CommonUtil.getBundleMessage("violation.msg.EditViolation"));
				}if(sessionMap.containsKey("SAVE_DONE")){
					sessionMap.remove("SAVE_DONE");
					successMessages.add(CommonUtil.getBundleMessage("violation.msg.SaveViolation"));
				}
		}
	}
		
		} catch (PimsBusinessException e) {
			logger.LogException("AddContractViolation init() Exception:::::::::::;", e);
		}
	}
	
	public void prerender(){
		super.prerender();
		
		try {
		if(hdnContractId!=null && !hdnContractId.equals("")&& hdnContractNumber!=null && !hdnContractNumber.equals("")
				&&hdnTenantId!=null && !hdnTenantId.equals("")){
			
			contractView  = getContractById(Long.parseLong(hdnContractId));
			populateContract();
			populateContract.setRendered(true);
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			saveButton.setRendered(true);
			addViolationButton.setRendered(true);
			loadAttachmentsAndComments(contractView.getContractId());
			setViolationViewDataList(psa.getContractViolation(contractView.getContractUnitView()));
			hdnContractId = null;
			hdnContractNumber = null;
			hdnTenantId = null;
		  }
		
		} catch (PimsBusinessException e) {
			logger.LogException("Exception in prerender:::::::::", e);
		}
	}
	
	
	public String btnContract_Click()
	{

		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		if(viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID)!=null 
				&& !viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).equals("")){
		context.getExternalContext().getSessionMap().put("contractId", viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).toString());
		context.getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		
		String javaScriptText="var screen_width = screen.width;"+
        "var screen_height = screen.height;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";
        //"window.open('LeaseContract.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=10,scrollbars=no,status=yes');";
       openPopUp("LeaseContract.jsf",javaScriptText);
		logger.logInfo(methodName+"|"+"Finish..");
		
		//return "CONTRACT_VIEW";
		}
		return "";
	}
	public String btnTenant_Click() {
		String methodName = "btnTenant_Click";
		logger.logInfo(methodName + "|Start");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		BidderAttendanceView dataItem = new BidderAttendanceView();
		if(viewMap.get("TENANT_ID")!=null
				&& !viewMap.get("TENANT_ID").equals("")){
		logger.logDebug(methodName + "|Person Id::" + dataItem.getBidderId());
		setRequestParam(WebConstants.PERSON_ID, dataItem.getBidderId());
		setRequestParam("viewMode", "popup");
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+viewMap.get("TENANT_ID").toString()+")";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
		}
		logger.logInfo(methodName + "|Finish");
		return "";

	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		final String viewId = "/CLRequest.jsp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+1024+',height='+450+',left=0,top=40,scrollbars=yes,status=yes');";
      if (extraJavaScript!=null&& extraJavaScript.trim().length()>0)      
    		javaScriptText=extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	public String getErrorMessages() 
	{
		
	   return commUtil.getErrorMessages(errorMessages);
		
	}
	public String getSuccessMessages() 
	{
	
		return commUtil.getErrorMessages(successMessages);
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public String getHdnContractId() {
		return hdnContractId;
	}

	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}

	public String getHdnContractNumber() {
		return hdnContractNumber;
	}

	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}

	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}

	public HtmlInputText getContractNoText() {
		return contractNoText;
	}

	public void setContractNoText(HtmlInputText contractNoText) {
		this.contractNoText = contractNoText;
	}

	public HtmlInputText getTenantNameText() {
		return tenantNameText;
	}

	public void setTenantNameText(HtmlInputText tenantNameText) {
		this.tenantNameText = tenantNameText;
	}

	public HtmlInputText getContractStartDateText() {
		return contractStartDateText;
	}

	public void setContractStartDateText(HtmlInputText contractStartDateText) {
		this.contractStartDateText = contractStartDateText;
	}

	public HtmlInputText getTxtpropertyName() {
		return txtpropertyName;
	}

	public void setTxtpropertyName(HtmlInputText txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}

	public HtmlInputText getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(HtmlInputText txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}

	public HtmlInputText getTxtUnitRentValue() {
		return txtUnitRentValue;
	}

	public void setTxtUnitRentValue(HtmlInputText txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}

	public HtmlInputText getContractTypeText() {
		return contractTypeText;
	}

	public void setContractTypeText(HtmlInputText contractTypeText) {
		this.contractTypeText = contractTypeText;
	}

	public HtmlInputText getTenantNumberType() {
		return tenantNumberType;
	}

	public void setTenantNumberType(HtmlInputText tenantNumberType) {
		this.tenantNumberType = tenantNumberType;
	}

	public HtmlInputText getContractEndDateText() {
		return contractEndDateText;
	}

	public void setContractEndDateText(HtmlInputText contractEndDateText) {
		this.contractEndDateText = contractEndDateText;
	}

	public HtmlInputText getTxtpropertyType() {
		return txtpropertyType;
	}

	public void setTxtpropertyType(HtmlInputText txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
	}

	public HtmlInputText getTxtUnitType() {
		return txtUnitType;
	}

	public void setTxtUnitType(HtmlInputText txtUnitType) {
		this.txtUnitType = txtUnitType;
	}

	public HtmlCommandLink getPopulateContract() {
		return populateContract;
	}

	public void setPopulateContract(HtmlCommandLink populateContract) {
		this.populateContract = populateContract;
	}

	public HtmlTabPanel getRichTabPanel() {
		return richTabPanel;
	}

	public void setRichTabPanel(HtmlTabPanel richTabPanel) {
		this.richTabPanel = richTabPanel;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getClStatus(){
		CommonUtil comUtil = new CommonUtil();
		DomainDataView ddv= comUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
	 		       WebConstants.CONTRACT_STATUS_ACTIVE
	              );
		
	  return ddv.getDomainDataId().toString();
	}
	
	   private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	    {
	    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
	    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
			Iterator<DomainTypeView> itr = list.iterator();
			//Iterates for all the domain Types
			while( itr.hasNext() ) 
			{
				DomainTypeView dtv = itr.next();
				if( dtv.getTypeName().compareTo(domainType)==0 ) 
				{
					Set<DomainDataView> dd = dtv.getDomainDatas();
					//Iterates over all the domain datas
					for (DomainDataView ddv : dd) 
					{
					  ddvList.add(ddv);	
					}
					break;
				}
			}
	    	
	    	return ddvList;
	    }

	   
	  public String  doSave(){
		
		  try { 
		  if(viewMap.containsKey("Contract_VIEW_FOR_ADD_VIOLATION")){
			  saveComments(((ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION")).getContractId());
			  saveAttachments(((ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION")).getContractId());
			  saveButton.setRendered(false);
			  addViolationButton.setRendered(false);
			  populateContract.setRendered(false);
			  setEnableDisableCommandLink(false);
			  viewMap.put("NOT_RENDERED_COMMAND_LINK", true);
			  
			  viewMap.put("canAddAttachment",false);
			  viewMap.put("canAddNote", false); 
		  }
		  
		  } catch (Exception e) {
				
				logger.LogException("doSave Crashed::::::::::;;", e);
			}
		  
		  return "";
	  }
	  
		private ContractView getContractById(Long contractId) throws PimsBusinessException
		{
			String methodName="getContractById";
			ContractView contView = new ContractView();
			ArrayList<ContractView> list;
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
			try
			{
				contView.setContractId(contractId);
				
				HashMap contractHashMap=new HashMap();
				contractHashMap.put("contractView",contView);
				
				SystemParameters parameters = SystemParameters.getInstance();			
				String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
				contractHashMap.put("dateFormat",dateFormat);
				PropertyServiceAgent psa =new PropertyServiceAgent();
				logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
				List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
				logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
				if(contractViewList.size()>0)
				contView=(ContractView)contractViewList.get(0);
				viewMap.put("Contract_VIEW_FOR_ADD_VIOLATION", contView);
				viewMap.put("TENANT_ID", contView.getTenantView().getPersonId());
				viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contView.getContractId());
				
			}
			catch(Exception ex)
			{
				logger.logError(methodName+"|"+"Error occured:"+ex);
			}
			return contView;
		}
		
		public String populateContract(){
			String  METHOD_NAME = "populateContractFromPopUp|";		
	        logger.logInfo(METHOD_NAME+"started...");

	        try{
					if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
					{
						//Setting the Contract/Tenant Info
						contractNoText.setValue(contractView.getContractNumber());
						setTenantName(contractView);
						contractTypeText.setValue(contractView.getContractTypeEn());
						contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
						contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
	                    
						//Added by shiraz for populate required field in the new screen
						if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
						{ 
							List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
							contractUnitViewList.addAll(contractView.getContractUnitView());
							ContractUnitView contractUnitView =contractUnitViewList.get(0);
							logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
							txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
							
							logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
						    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
						    
						    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
						    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
						    
						    
						    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
						    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
						
						    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
						    txtUnitRentValue.setValue(contractUnitView.getUnitView().getRentValue()!=null?contractUnitView.getUnitView().getRentValue().toString():"");
						
						}
					}

				logger.logInfo(METHOD_NAME+"completed successfully...");
			}
			catch (Exception e) {			
				logger.LogException(METHOD_NAME + "crashed...", e);
			}
			return "";
		}
		
		private void setTenantName(ContractView contractView) {
			String  METHOD_NAME = "setTenantName|";		
	        logger.logInfo(METHOD_NAME+" started...");
			if(contractView.getTenantView() != null){
				String name = "";
				if(contractView.getTenantView().getFirstName() != null)
					name += contractView.getTenantView().getFirstName() + " ";
				if(contractView.getTenantView().getMiddleName() != null)
					name += contractView.getTenantView().getMiddleName() + " ";
				if(contractView.getTenantView().getLastName() != null)
					name += contractView.getTenantView().getLastName();
				tenantNameText.setValue(name);
				
				if(contractView.getTenantView().getResidenseVisaNumber()!=null &&
						!contractView.getTenantView().getResidenseVisaNumber().equals(""))
				tenantNumberType.setValue(contractView.getTenantView().getResidenseVisaNumber());
				else
				tenantNumberType.setValue(contractView.getTenantView().getSocialSecNumber());	
			}
	        logger.logInfo(METHOD_NAME+" ended...");
		}
		
		public String getStringFromDate(Date dateVal){
			String pattern = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(pattern);
			String dateStr = "";
			try{
				if(dateVal!=null)
					dateStr = formatter.format(dateVal);
			}
			catch (Exception exception) {
				exception.printStackTrace();
			}
			return dateStr;
		}
		
		public String getDateFormat(){
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
	    }

		public HtmlCommandButton getSaveButton() {
			return saveButton;
		}

		public void setSaveButton(HtmlCommandButton saveButton) {
			this.saveButton = saveButton;
		}

		public HtmlDataTable getDataTableViolation() {
			return dataTableViolation;
		}

		public void setDataTableViolation(HtmlDataTable dataTableViolation) {
			this.dataTableViolation = dataTableViolation;
		}
		
		public List<ViolationView> getViolationViewDataList() {
			if (viewMap.containsKey("VIOLATION_DATA_LIST")&&viewMap.get("VIOLATION_DATA_LIST").toString().length()>0)
				violationViewDataList = (ArrayList<ViolationView>)viewMap.get("VIOLATION_DATA_LIST");
			
			
			return violationViewDataList;
		}

		public void setViolationViewDataList(List<ViolationView> violationViewDataList) {
			this.violationViewDataList = violationViewDataList;

			if (this.violationViewDataList != null&& this.violationViewDataList.size() > 0)
				viewMap.put("VIOLATION_DATA_LIST", this.violationViewDataList);
			else
				viewMap.put("VIOLATION_DATA_LIST", "");
		}
		
		public void showAddViolationPopup() {
			
			ContractView contView = new ContractView();
			List<UnitView> unitViewList  = new ArrayList<UnitView>();
			
			contView = (ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION");

			sessionMap.put("VIOLATION_CONTRACT_VIEW",(ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION"));
			sessionMap.put("FROM_VIOLATION_CONTRACT",true);
			
			Iterator<ContractUnitView>  iteratorConUnitView =  contView.getContractUnitView().iterator();
			
			while (iteratorConUnitView.hasNext() ) {
				
				ContractUnitView conUnitView = iteratorConUnitView.next();
				unitViewList.add(conUnitView.getUnitView());
				
			}

			sessionMap.put("SESSION_INSPECTION_UNITS",unitViewList);

			final String viewId = "/addContractViolation.jsf";

			FacesContext facesContext = FacesContext.getCurrentInstance();

			// This is the proper way to get the view's url
			ViewHandler viewHandler = facesContext.getApplication()
					.getViewHandler();
			String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			String javaScriptText = "javascript:showAddViolationPopup();";

			// Add the Javascript to the rendered page's header for immediate
			// execution
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
		}
		
	public String btnAddViolationAction_Click(){
		
		
		ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		
		sessionMap.put("VIOLATION_VIEW_FOR_EDIT", vV);
		sessionMap.put("ADD_VIOLATION_ACTION", true);
		
		ContractView contView = new ContractView();
		List<UnitView> unitViewList  = new ArrayList<UnitView>();
		
		contView = (ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION");

		sessionMap.put("VIOLATION_CONTRACT_VIEW",(ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION"));
		sessionMap.put("FROM_VIOLATION_CONTRACT",true);
		
		Iterator<ContractUnitView>  iteratorConUnitView =  contView.getContractUnitView().iterator();
		
		while (iteratorConUnitView.hasNext() ) {
			
			ContractUnitView conUnitView = iteratorConUnitView.next();
			unitViewList.add(conUnitView.getUnitView());
			
		}

		sessionMap.put("SESSION_INSPECTION_UNITS",unitViewList);
		
		
		final String viewId = "/inspectionDetails.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showAddViolationPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		
		return "AddViolationAction";
		
	}
	public void btnBlackList_Click()
	{
		
	try {
			ViolationView vV = (ViolationView) dataTableViolation.getRowData();
			ContractView contract=psa.getContract(vV.getContractNumber(), getDateFormat());
			if(contract.getTenantView()!=null)
			{
			Long tenantId=contract.getTenantView().getPersonId();
			sessionMap.put("TENANT_ID",tenantId);
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "javascript:showBlacklistPopup();";

			// Add the Javascript to the rendered page's header for immediate
			// execution
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,	AddResource.HEADER_BEGIN, javaScriptText);
			}
		}
		catch (Exception e) 
		{
			logger.LogException("btnBlackList_Click Crashed :",e);
		}
	}
	
	public String btnEditViolation_Click(){
	
		
		ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		
		sessionMap.put("VIOLATION_VIEW_FOR_EDIT", vV);
		
		ContractView contView = new ContractView();
		List<UnitView> unitViewList  = new ArrayList<UnitView>();
		
		contView = (ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION");

		sessionMap.put("VIOLATION_CONTRACT_VIEW",(ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION"));
		sessionMap.put("FROM_VIOLATION_CONTRACT",true);
		
		Iterator<ContractUnitView>  iteratorConUnitView =  contView.getContractUnitView().iterator();
		
		while (iteratorConUnitView.hasNext() ) {
			
			ContractUnitView conUnitView = iteratorConUnitView.next();
			unitViewList.add(conUnitView.getUnitView());
			
		}

		sessionMap.put("SESSION_INSPECTION_UNITS",unitViewList);
		
		
		final String viewId = "/inspectionDetails.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showAddViolationPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		
		return "EditViolation";
		
		
	}
	
	public String btnDeleteViolation_Click(){
	
		
		ViolationView vV = (ViolationView) dataTableViolation.getRowData();
		Boolean flage = false;
		successMessages = new ArrayList<String>();
		try {
			flage = psa.setInspectionViolationToDeleted(vV.getViolationId());
			
			if(flage){
			setViolationViewDataList(psa.getContractViolation(((ContractView)viewMap.get("Contract_VIEW_FOR_ADD_VIOLATION")).getContractUnitView()));
			successMessages.add(CommonUtil.getBundleMessage("violation.msg.DeleteViolation"));
			}
			
		} catch (PimsBusinessException e) {
			logger.LogException("Exception in btnDeleteViolation_Click::::::::::::::", e);
		}	
			
		return "EditViolation";
		
		
	}
	
	
	public void loadAttachmentsAndComments(Long contractId){
		
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_CONTRACTVIOLATION;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", externalId);
		
		if(contractId!=null){
		String entityId = contractId.toString();
		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
		viewMap.put("entityId", entityId);
		}
		
	}
	public Boolean saveAttachments(Long contractId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(contractId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	public void saveComments(Long contractId) throws Exception
    {
    	String methodName="saveComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.Attachment.EXTERNAL_ID_CONTRACTVIOLATION;
	    	NotesController.saveNotes(notesOwner, contractId);

	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }

	public HtmlCommandButton getAddViolationButton() {
		return addViolationButton;
	}

	public void setAddViolationButton(HtmlCommandButton addViolationButton) {
		this.addViolationButton = addViolationButton;
	}

	public HtmlCommandLink getEditLink() {
		return editLink;
	}

	public void setEditLink(HtmlCommandLink editLink) {
		this.editLink = editLink;
	}

	public HtmlCommandLink getEditActionLink() {
		return editActionLink;
	}

	public void setEditActionLink(HtmlCommandLink editActionLink) {
		this.editActionLink = editActionLink;
	}

	public HtmlCommandLink getDeleteLink() {
		return deleteLink;
	}

	public void setDeleteLink(HtmlCommandLink deleteLink) {
		this.deleteLink = deleteLink;
	}

	public boolean isEnableDisableCommandLink() {
		if(viewMap.containsKey("ENABLE_DISABLE_COMMAND"))
		 enableDisableCommandLink = (Boolean)viewMap.get("ENABLE_DISABLE_COMMAND");
		
		return enableDisableCommandLink;
	}

	public void setEnableDisableCommandLink(boolean enableDisableCommandLink) {
		this.enableDisableCommandLink = enableDisableCommandLink;
		
		
	}
	// added for pagination bar
	public Integer getRecordSize() {
		if (viewMap.containsKey("VIOLATION_DATA_LIST")&& viewMap.get("VIOLATION_DATA_LIST").toString().length()>0 ) {
			violationViewDataList = (ArrayList<ViolationView>) viewMap.get("VIOLATION_DATA_LIST");
			return violationViewDataList.size();
		} else {
			return 0;
		}
	}
	
	public String doCancel()
	{
		if(viewMap.containsKey("fromContractSearch") && (Boolean)viewMap.get("fromContractSearch"))
		{
			sessionMap.put("preserveSearchCriteria", true);
			return "contractSearch";
		}
		else
			return "";
		
	}

	
	
}

