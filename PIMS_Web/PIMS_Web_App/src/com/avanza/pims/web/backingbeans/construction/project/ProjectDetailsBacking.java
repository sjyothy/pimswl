package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.security.SecurityManager;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.pimsprojectfollowup.proxy.PIMSProjectFollowupBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.construction.project.ProjectService;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ProjectDetailsView;
import com.avanza.pims.ws.vo.ProjectFollowUpActivityView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.StudyDetailView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;

public class ProjectDetailsBacking extends AbstractController {

	private transient Logger logger = Logger.getLogger(ProjectDetailsBacking.class);

	private transient ProjectServiceAgent projectServiceAgent = new ProjectServiceAgent();
	
	private final String TAB_PROJECT_DETAILS = "projectDetailsTab";
	private final String TAB_PROJECT_MILSTONES = "projectMilestonesTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private final String TAB_PAYMENT = "projectPaymentTab";
	private final String TAB_CONTRACTS = "contractsTab";
	private final String TAB_TENDERS = "tendersTab";
	
	private final String noteOwner = WebConstants.ProjectFollowUp.PROJECT_FOLLOW_UP;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_PROJECT_FOLLOW_UP;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_PROJECT_FOLLOW_UP;
	private boolean showPropertiesTab=false;
	private boolean showAddPropertyButtton=false;
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlInputTextarea remarksField = new HtmlInputTextarea();
	private HtmlInputTextarea followUpremarksField = new HtmlInputTextarea();

	private List<ProjectFollowUpActivityView> projectFollowupActivityList=new ArrayList<ProjectFollowUpActivityView>(0);

	String hiddenStudyId = "";
	String hiddenLoadStudy = "";
	
	String hiddenPropertyId = "";
	String hiddenLoadProperty = "";
	
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_DRAFT ="PAGE_MODE_DRAFT";
	private final String PAGE_MODE_DRAFT_DONE ="PAGE_MODE_DRAFT_DONE";
	private final String PAGE_MODE_HANDOVER ="PAGE_MODE_HANDOVER";
	private final String PAGE_MODE_HANDOVER_DONE ="PAGE_MODE_HANDOVER_DONE";
	private final String PAGE_MODE_FOLLOW_UP ="PAGE_MODE_FOLLOW_UP";
	private final String PAGE_MODE_FOLLOW_UP_DONE ="PAGE_MODE_FOLLOW_UP_DONE";
	private final String PAGE_MODE_INITIAL_RECEIVING ="PAGE_MODE_INITIAL_RECEIVING";
	private final String PAGE_MODE_INITIAL_RECEIVING_DONE ="PAGE_MODE_INITIAL_RECEIVING_DONE";
	private final String PAGE_MODE_FINAL_RECEIVING ="PAGE_MODE_FINAL_RECEIVING";
	private final String PAGE_MODE_FINAL_RECEIVING_DONE ="PAGE_MODE_FINAL_RECEIVING_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private final String PAGE_MODE_POPUP_VIEW ="PAGE_MODE_POPUP_VIEW";
	private final String PAGE_MODE_SUBMIT ="PAGE_MODE_SUBMIT";
	
	private String pageMode="default";
	
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_NEW;
	
	private String heading="";

	private String completionPercent;
	private String lastAchievedMilestone;
	private String newCompletionPercent;
	private String newMilestoneId;
	private Date followUpDate;
	private String followUpRemarks;

	private Date initialReceivingDate;
	private Date finalReceivingDate;
	private Date handOverDate;
	private String remarks;
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	private List<DomainDataView> projectStatusList=new ArrayList<DomainDataView>(0);
	private List<DomainDataView> propertyTypeList=new ArrayList<DomainDataView>(0);
	private List<DomainDataView> propertyStatusList=new ArrayList<DomainDataView>(0);
	private DomainDataView projectStatusFollowUpDone=new DomainDataView();
	private DomainDataView propertyStatusRecieved=new DomainDataView();
	private DomainDataView propertyStatusUnderRecieving=new DomainDataView();
	private DomainDataView propertyTypeLand=new DomainDataView();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
    @SuppressWarnings("unchecked")
	public Boolean validatedForNewProject() {
	    logger.logInfo("validatedForNewProject() started...");
	    Boolean validated = true;
	    try
		{
	    	errorMessages.clear();
//	    	viewMap.put("relatedStudyId", "1");
//	    	String relatedStudyId = (String)viewMap.get("relatedStudyId");
//	    	if(StringHelper.isEmpty(relatedStudyId))
//	        {
//	    		validated = false;
//	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.RELATED_STUDY));
////				return validated;
//	        }
	    	
//	    	viewMap.put("propertyId", "1");
	    	String propertyId = (String)viewMap.get("propertyId");
	    	if(StringHelper.isEmpty(propertyId))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROPERTY));
//				return validated;
	        }
	    	
	    	String projectName = (String)viewMap.get("projectName");
	    	if(StringHelper.isEmpty(projectName))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_NAME));
//				return validated;
	        }
	    	
	    	String projectTypeId = (String)viewMap.get("projectTypeId");
	    	if(projectTypeId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_TYPE));
//				return validated;
	        }

	    	String projectSizeId = (String)viewMap.get("projectSizeId");
	    	if(projectSizeId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_SIZE));
//				return validated;
	        }
	    	
	    	String projectPurposeId = (String)viewMap.get("projectPurposeId");
	    	if(projectPurposeId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_PURPOSE));
//				return validated;
	        }
	    	
	    	String projectCategoryId = (String)viewMap.get("projectCategoryId");
	    	if(projectCategoryId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_CATEGORY));
//				return validated;
	        }
	    	
	    	String projectEstimatedCost = (String)viewMap.get("projectEstimatedCost");
	    	if(StringHelper.isEmpty(projectEstimatedCost))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.ESTIMATED_COST));
//				return validated;
	        }
	    	else{
	    		try{
	    			String opStr = projectEstimatedCost.replace(',', '0');
	    			Double temp = new Double(opStr);
		    	}
				catch(NumberFormatException nfe){
					validated = false;
					errorMessages.add(CommonUtil.getInvalidMessage(WebConstants.ProjectFollowUp.Tab.ESTIMATED_COST));
				}
	    	}
	    	
	    	Boolean datesNotNull = true;
	    	
	    	Date expectedStartDate = (Date)viewMap.get("expectedStartDate");
	    	if(expectedStartDate==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_EXP_START_DATE));
//				return validated;
	        }
	    	
	    	Date expectedEndDate = (Date)viewMap.get("expectedEndDate");
	    	if(expectedEndDate==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_EXP_END_DATE));
//				return validated;
	        }
	    	
	    	if(expectedStartDate!=null && expectedEndDate!=null && expectedStartDate.after(expectedEndDate)){
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_EXP_DATES_INVALID));
	    	}
	    	
	    	String projectDescription = (String)viewMap.get("projectDescription");
	    	if(StringHelper.isEmpty(projectDescription))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROJECT_DESCRIPTION));
//				return validated;
	        }
	    	
	    	/*String landNumber = (String)viewMap.get("landNumber");
	    	if(StringHelper.isEmpty(landNumber))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.Tab.PROPERTY_LAND_NUMBER));
				return validated;
	        }*/
//				viewMap.put("propertyTypeId", projectDetailsTabView.getPropertyTypeId().toString());
//				viewMap.put("propertyOwnershipId", projectDetailsTabView.getPropertyOwnershipId().toString());
	    	if(!validated)
	    		tabPanel.setSelectedTab(TAB_PROJECT_DETAILS);
	    	
	    	if(getProjectMilestoneList().isEmpty()){
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_MILESTONE_REQUIRED));
	    		if(validated)
	    			tabPanel.setSelectedTab(TAB_PROJECT_MILSTONES);
				validated = false;
	    	}
	    	else if(datesNotNull){
	    		List<ProjectMileStoneView> projectMilestones = getProjectMilestoneList();
	    		for(ProjectMileStoneView projectMileStoneView: projectMilestones){
	    			if(projectMileStoneView.getCompletionDate().after(expectedEndDate)){
	    				validated = false;
	    				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_MILESTONE_DATE_INVALID, CommonUtil.getStringFromDate(expectedEndDate)));
	    				break;
	    			}
	    			if(projectMileStoneView.getCompletionDate().before(expectedStartDate)){
	    				validated = false;
	    				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_MILESTONE_DATE_INVALID_2, CommonUtil.getStringFromDate(expectedStartDate)));
	    				break;
	    			}
	    		}
    		}
	    	
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		if(validated)
	    			tabPanel.setSelectedTab(TAB_ATTACHMENTS);
				validated = false;
//				return validated;
	    	}

			logger.logInfo("validatedForNewProject() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForNewProject() crashed ", exception);
		}
		return validated;
	}
	
    @SuppressWarnings("unchecked")
	public Boolean validatedForHandOver() {
	    logger.logInfo("validatedForHandOver() started...");
	    Boolean validated = true;
	    try
		{
        	errorMessages.clear();
	    	if(getHandOverDate()==null)
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.HANDOVER_LOCATION_DATE));
	        }
//	    	else if(getHandOverDate().before(getActualStartDate())){
//				validated = false;
//				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PARAM_DATE_BEFORE_START_DATE, CommonUtil.getBundleMessage(WebConstants.ProjectFollowUp.RequiredFields.HANDOVER_LOCATION_DATE)));
//			}
	    	
	    	if(StringHelper.isEmpty(getRemarks()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        }
	    	
	    	if(!atleastOneContractActive()){
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_ATLEAST_ONE_CONTRACT_ACTIVE));
	    	}
			logger.logInfo("validatedForHandOver() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForHandOver() crashed ", exception);
		}
		return validated;
	}
    
    @SuppressWarnings("unchecked")
	public Boolean validatedForFollowUp() {
	    logger.logInfo("validatedForFollowUp() started...");
	    Boolean validated = true;
	    try
		{
	    	errorMessages.clear();
	    	if(StringHelper.isEmpty(getNewCompletionPercent()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.NEW_COMPLETION_PERCENT));
	        }
	    	else
	        {
    			try{
    				Long percent = Long.parseLong(getNewCompletionPercent());
    				if(!(percent>0 && percent<=100)){
    		    		validated = false;
    		    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_PERCENT_OUT_OF_RANGE));    		        	
    				}
    			}
    			catch(NumberFormatException nfe){
    				validated = false;
    				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_PERCENT_OUT_OF_RANGE));
    			}
	        }
	    	
	    	if(getNewMilestoneId().equals("0"))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.NEW_ACHIEVED_MILESTONE));
	        }
	    	Boolean datesNotNull = true;
	    	
	    	if(getFollowUpDate()==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.FOLLOW_UP_DATE));
	        }
	    	
	    	if(getActualStartDate()==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.ACTUAL_START_DATE));
	        }
	    	
	    	if(datesNotNull && getFollowUpDate().before(getActualStartDate())){
				validated = false;
				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PARAM_DATE_BEFORE_START_DATE, CommonUtil.getBundleMessage(WebConstants.ProjectFollowUp.RequiredFields.FOLLOW_UP_DATE)));
			}
	    	
	    	if(StringHelper.isEmpty(getFollowUpRemarks()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        }
	    	
			logger.logInfo("validatedForFollowUp() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForFollowUp() crashed ", exception);
		}
		return validated;
	}
    
    @SuppressWarnings("unchecked")
	public Boolean validatedForInitialReceiving() {
	    logger.logInfo("validatedForInitialReceiving() started...");
	    Boolean validated = true;
	    try
		{
	    	errorMessages.clear();
	    	Boolean datesNotNull = true;
	    	if(getActualStartDate()==null)
	    		datesNotNull = false;
	    	
	    	if(getInitialReceivingDate()==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.INITIAL_RECEIVING_DATE));
	        }
	    	else if(datesNotNull && getInitialReceivingDate().before(getActualStartDate())){
    			validated = false;
				errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PARAM_DATE_BEFORE_START_DATE, CommonUtil.getBundleMessage(WebConstants.ProjectFollowUp.RequiredFields.INITIAL_RECEIVING_DATE)));
	    	}
	    	
	    	if(getActualEndDate()==null)
	        {
	    		validated = false;
	    		datesNotNull = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.ACTUAL_END_DATE));
	        }
	    	else if(datesNotNull && getActualEndDate().before(getActualStartDate())){
	    			validated = false;
	    			errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PARAM_DATE_BEFORE_START_DATE, CommonUtil.getBundleMessage(WebConstants.ProjectFollowUp.RequiredFields.ACTUAL_END_DATE)));
	    	}
	    	
	    	
	    	if(StringHelper.isEmpty(getRemarks()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        }
	    	if(!propertyValidated())
	    	{
	    		validated=false;
	    	}
	    	
			logger.logInfo("validatedForInitialReceiving() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForInitialReceiving() crashed ", exception);
		}
		return validated;
	}
    
    private boolean propertyValidated()
    {
    	boolean validated=false;
    	if(viewMap.get("propertyTypeId")!=null)
    	{
    		Long propertyTypeId;
    		boolean propertyRecieved=false;
    		propertyTypeId=Long.parseLong(viewMap.get("propertyTypeId").toString());
    		if(viewMap.get("PROPERTY_TYPE_LAND")!=null)
    		{
    			propertyTypeLand=(DomainDataView) viewMap.get("PROPERTY_TYPE_LAND");
    			//if propertyType is "Land"
    			if(propertyTypeId.compareTo(propertyTypeLand.getDomainDataId())==0)
    			{
    				//if there are some associated properties
    				//So check is there any received property?
    				//if yes then allow user to go ahead else stop user for IniAccp:
    				if(viewMap.get("ASSOCIATED_PROPERTIES")!=null)
    				{
    					List<PropertyView> propertiesList=new ArrayList<PropertyView>(0);
    					propertiesList=(List<PropertyView>) viewMap.get("ASSOCIATED_PROPERTIES");
    					propertyStatusRecieved=(DomainDataView) viewMap.get("RECEIVED_STATUS");
	    					for(PropertyView pv:propertiesList)
	    					{
	    						if(pv.getStatusId().compareTo(propertyStatusRecieved.getDomainDataId())==0)
    							{
	    							propertyRecieved=true;
	    							break;
    							}
	    					}
    					if(!propertyRecieved)
    						{
    							validated=false;
    							errorMessages.add(CommonUtil.getBundleMessage("errorMsg.NoPropertyReceived"));
    						}
    					else
    						validated=true;
    				}
    				//if there is no associated properties.
    				//so the restrict person for initial acceptance.
    				else if(!propertyRecieved)
					{
						validated=false;
						errorMessages.add(CommonUtil.getBundleMessage("errorMsg.NoPropertyReceived"));
					}
					else
						validated=true;
    			}
    			else
    				validated=true;
    		}
    	}
		return validated;
	}

	@SuppressWarnings("unchecked")
	public Boolean validatedForFinalReceiving() {
	    logger.logInfo("validatedForFinalReceiving() started...");
	    Boolean validated = true;
	    try
		{
	    	errorMessages.clear();

	    	Boolean datesNotNull = true;
	    	if(getActualStartDate()==null)
	    		datesNotNull = false;
	    	
	    	if(getFinalReceivingDate()==null)
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.FINAL_RECEIVING_DATE));
	        }
	    	else if(datesNotNull && getFinalReceivingDate().before(getActualStartDate())){
    			validated = false;
    			errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PARAM_DATE_BEFORE_START_DATE, CommonUtil.getBundleMessage(WebConstants.ProjectFollowUp.RequiredFields.FINAL_RECEIVING_DATE)));
	    	}

	    	
	    	if(StringHelper.isEmpty(getRemarks()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Commons.REMARKS));
	        }
	    	
	    	
//	    	if(!allContractsTerminated()){
//	    		validated = false;
//	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_ALL_CONTRACTS_SHUD_BE_TERMINATED));
//	    	}
	    	
	    	List<String> contracts= null;
	    	try
	    	{
	    		contracts = getProjectContractsWithPendingPayments();
	    	}
	    	catch(Exception exp)
	    	{
	    		validated = false;
	    		logger.LogException("getProjectContractsWithPendingPayments() crashed ", exp);
	    	}
	    	if(contracts!=null && contracts.size()>0 )
	    	{
	    		validated = false;
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_CONTRACTS_WITH_PENDING_PAYMENTS));
	    	}
	    	
			logger.logInfo("validatedForFinalReceiving() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForFinalReceiving() crashed ", exception);
		}
		return validated;
	}
    
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
			if(projectId!=null){
				CommonUtil.loadAttachmentsAndComments(projectId + "");
				ProjectDetailsView projectDetailsTabView = projectServiceAgent.getProjectDetailsById(projectId, CommonUtil.getArgMap());	
				viewMap.put(WebConstants.ProjectFollowUp.PROJECT_VIEW, projectDetailsTabView);
				viewMap.put("PROJECT_VIEW_FOR_RECEIVING_PROPERTY", projectDetailsTabView);
				populateProjectDetailsTab();
				populateProjectMilestonesTab();
				
				if(pageMode.equals(PAGE_MODE_FOLLOW_UP)){
					ProjectMileStoneView lastAchievedMilestone = projectServiceAgent.getLastAchievedMilestone(projectId);
					if(lastAchievedMilestone!=null){
						setCompletionPercent(lastAchievedMilestone.getCompletionPercentage().toString());
						setLastAchievedMilestone(lastAchievedMilestone.getMileStoneName());
						viewMap.put("lastFollowUpActivityId", lastAchievedMilestone.getLastFollowUpActivityId());
					}
				}
				populateProjectContractsTab();
				populateProjectTendersTab();
				populateProjectPaymentsTab();
				populatePropertiesDetailsTab();
			}
			
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	private void populatePropertiesDetailsTab() throws PimsBusinessException
	{
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null)
		{
			List<PropertyView> propertiesList =new ProjectServiceAgent().getAllPropertiesByPropjectId(projectDetailsTabView.getProjectId());
			if(propertiesList!=null && propertiesList.size()>0){
				viewMap.put("PROPERTY_VIEW_LIST", propertiesList);
				viewMap.put("SHOW_PROPERTIES_TAB", true);
			}
		}
	}

	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
			if(!isPostBack())
			{
				projectStatusList=CommonUtil.getDomainDataListForDomainType(WebConstants.Project.PROJECT_STATUS);
				for(DomainDataView ddv :projectStatusList)
				{
					if(ddv.getDataValue().compareTo("FOLLOW_UP_DONE")==0)
						{
							projectStatusFollowUpDone=ddv;
							viewMap.put("FOLLOW_UP_DONE_STATUS", projectStatusFollowUpDone);
							break;
						}
				}
				
				propertyStatusList=CommonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_STATUS);
				for(DomainDataView ddv :propertyStatusList)
				{
					if(ddv.getDataValue().compareTo("RECEIVED")==0)
					{
						propertyStatusRecieved=ddv;
						viewMap.put("RECEIVED_STATUS", propertyStatusRecieved);
						break;
					}
					if(ddv.getDataValue().compareTo("UNDER_RECEIVING")==0)
					{
						propertyStatusUnderRecieving=ddv;
						viewMap.put("UNDER_RECEIVING", propertyStatusUnderRecieving);
						break;
					}
				}
				
				propertyTypeList=CommonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_TYPE);
				for(DomainDataView ddv:propertyTypeList)
				{
					if(ddv.getDataValue().toString().compareToIgnoreCase("LAND")==0)
						{
							propertyTypeLand=ddv;
							viewMap.put("PROPERTY_TYPE_LAND", propertyTypeLand);
							break;
						}
				}
				if(propertyTypeList!=null && propertyTypeList.size()>0)
					viewMap.put("PROPERTY_TYPE_LIST", propertyTypeList);
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				setMode();
				setValues();
			}
			else{
				if(sessionMap.containsKey("milestoneAddSuccess") && sessionMap.containsKey(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST)){
					List<ProjectMileStoneView> projectMilestoneList = (List<ProjectMileStoneView>)sessionMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
					if(projectMilestoneList==null)
						projectMilestoneList = new ArrayList<ProjectMileStoneView>();
					viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, projectMilestoneList);
					sessionMap.remove("milestoneAddSuccess");
					sessionMap.remove(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
					infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_MILESTONE_ADD_SUCCESS);
				}
				else if(sessionMap.containsKey("milestoneEditSuccess") && sessionMap.containsKey(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW)){
					ProjectMileStoneView projectMileStoneView = (ProjectMileStoneView) sessionMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
					Boolean milestoneEditSuccess = (Boolean) sessionMap.get("milestoneEditSuccess");
					
					Integer indexObj = (Integer) viewMap.get("editMilestoneIndex");
					if(indexObj!=null){
						int index = indexObj.intValue();
						List<ProjectMileStoneView> projectMilestoneList = getProjectMilestoneList();
						if(projectMilestoneList!=null){
							projectMilestoneList.remove(index);
							projectMilestoneList.add(index, projectMileStoneView);
							viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, projectMilestoneList);
							infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_MILESTONE_EDIT_SUCCESS);
						}
					}
//					if(milestoneEditSuccess){	
//					
//					}
//					else
//						errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_MILESTONE_EDIT_FAILURE));
					
					sessionMap.remove("milestoneEditSuccess");
					sessionMap.remove(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
				}
				
				
				
				StudyDetailView selectedStudyDetailView = (StudyDetailView) sessionMap.get(WebConstants.Study.SELECTED_STUDY_DETAIL);
				sessionMap.remove(WebConstants.Study.SELECTED_STUDY_DETAIL);
				if(selectedStudyDetailView!=null){
					viewMap.put(WebConstants.Study.SELECTED_STUDY_DETAIL, selectedStudyDetailView);
					if(selectedStudyDetailView.getDescription()!=null)
						viewMap.put("studyDescription", selectedStudyDetailView.getDescription());
					if(selectedStudyDetailView.getStudyId()!=null)
						viewMap.put("relatedStudyId", selectedStudyDetailView.getStudyId().toString());
					if(selectedStudyDetailView.getStudyTitle()!=null)
						viewMap.put("relatedStudy", selectedStudyDetailView.getStudyTitle());
				}
			}
			pageMode = (String) viewMap.get(PAGE_MODE);

			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
	
	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			
//			UserTask temp = new UserTask();
//			Map<String,String> taskAtr = new HashMap<String, String>();
//			taskAtr.put(WebConstants.UserTasks.PROJECT_ID,"8");
//			temp.setTaskAttributes(taskAtr);
//			temp.setTaskType(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_HAND_OVER_PROJECT_LOCATION);
//			sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,temp);
			///
			
			// from followUp application search screen
			if(sessionMap.get(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE)!=null)
			{
				String projectDetailsMode = (String) sessionMap.get(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE);
				sessionMap.remove(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE);
				Long projectId = (Long)sessionMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
				sessionMap.remove(WebConstants.ProjectFollowUp.PROJECT_ID);
				if(projectId!=null)
					viewMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
				
				canAddAttachmentsAndComments(true);
				viewMap.put("projectDetailsReadonlyMode", true);
				viewMap.put("asterisk", "");
				viewMap.put("canAddMilestone", false);
				
				if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_NEW)){
					pageMode = PAGE_MODE_NEW;
					viewMap.put("projectDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put("canAddMilestone", true);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_NEW);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_DRAFT)){
					pageMode = PAGE_MODE_DRAFT;
					viewMap.put("projectDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put("canAddMilestone", true);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_DRAFT);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_SUBMIT)){
					pageMode = PAGE_MODE_SUBMIT;
					viewMap.put("projectDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put("canAddMilestone", true);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_SUBMIT);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_HANDOVER)){
					pageMode = PAGE_MODE_HANDOVER;
					viewMap.put("handOverReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_HANDOVER);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_FOLLOW_UP)){
					pageMode = PAGE_MODE_FOLLOW_UP;
					viewMap.put("followUpReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_FOLLOW_UP);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_INITIAL_RECEIVING)){
					pageMode = PAGE_MODE_INITIAL_RECEIVING;
					viewMap.put("initialReceivingReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_INITIAL_RECEIVING);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_FINAL_RECEIVING)){
					pageMode = PAGE_MODE_FINAL_RECEIVING;
					viewMap.put("finalReceivingReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_FINAL_RECEIVING);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_VIEW)){
					pageMode = PAGE_MODE_VIEW;
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_VIEW);
					canAddAttachmentsAndComments(false);
				}
				else if(projectDetailsMode.equals(WebConstants.ProjectFollowUp.PROJECT_DETAILS_MODE_POPUP_VIEW)){
					pageMode = PAGE_MODE_POPUP_VIEW;
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_VIEW);
					canAddAttachmentsAndComments(false);
				}
				
				viewMap.put(PAGE_MODE, pageMode);
				
			}
			// FROM TASK LIST
			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
				checkTask();
			}
			// set back page
			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long projectId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.UserTasks.PROJECT_ID)!=null)
				{
					projectId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.PROJECT_ID));
					viewMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
				}
				String taskType = userTask.getTaskType();
//				if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_APPROVE_PROJECT_DESIGN))
//					completeTask(TaskOutcome.APPROVE);
				
				if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_FOLLOW_UP_PROJECT))
				{
					pageMode = PAGE_MODE_FOLLOW_UP;
					viewMap.put("followUpReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_FOLLOW_UP);
					viewMap.put("showCompleteFollowUp", true);
				}
				else if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_HAND_OVER_PROJECT_LOCATION))
				{
					pageMode = PAGE_MODE_HANDOVER;
					viewMap.put("handOverReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_HANDOVER);
				}
				else if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_PROJECT_INITIAL_ACCEPTANCE))
				{
					pageMode = PAGE_MODE_INITIAL_RECEIVING;
					viewMap.put("initialReceivingReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_INITIAL_RECEIVING);
				}
				else if(taskType.equals(WebConstants.UserTasks.ProjectFollowUp.TASK_TYPE_PROJECT_FINAL_ACCEPTANCE))
				{
					pageMode = PAGE_MODE_FINAL_RECEIVING;
					viewMap.put("finalReceivingReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_FINAL_RECEIVING);
				}

				viewMap.put("projectDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
				viewMap.put(PAGE_MODE, pageMode);
				setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_VIEW;
			
			logger.logInfo("pageMode : %s", pageMode);
			heading = getHeading();
			
			String selectedTab = (String) viewMap.get("selectedTab");
			if(StringHelper.isNotEmpty(selectedTab))
				tabPanel.setSelectedTab(selectedTab);
			viewMap.remove("selectedTab");
			
			// for projectDetailsTab start
			loadProperty();
			// for projectDetailsTab end
			
			if(viewMap.containsKey("infoMessage")){
				infoMessage = (String) viewMap.get("infoMessage");
				if(viewMap.containsKey("secondPrerenderInfoCall")){
					viewMap.remove("infoMessage");
					viewMap.remove("secondPrerenderInfoCall");
				}
				viewMap.put("secondPrerenderInfoCall", true);
			}
			if(viewMap.containsKey("errorMessages")){
				errorMessages = (List<String>) viewMap.get("errorMessages");
				if(viewMap.containsKey("secondPrerenderErrorCall")){
					viewMap.remove("errorMessages");
					viewMap.remove("secondPrerenderErrorCall");
				}
				viewMap.put("secondPrerenderErrorCall", true);
			}
			
			remarksField.setReadonly(getHandOverReadonlyMode() || getFollowUpReadonlyMode() || getInitialReceivingReadonlyMode() || getFinalReceivingReadonlyMode());
			followUpremarksField.setReadonly(getFollowUpReadonlyMode());
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadStudy(){
		try{
			logger.logInfo("loadStudy started...");
			Boolean studyLoaded = (Boolean) viewMap.get("studyLoaded");
			studyLoaded = (studyLoaded==null)?false:studyLoaded;
			Boolean loadStudy = hiddenLoadStudy.equals("true");
			if(StringHelper.isNotEmpty(hiddenStudyId) && (!studyLoaded || loadStudy)){
				viewMap.put("studyId", hiddenStudyId);
				/*ProjectDetailsView propertyView = projectServiceAgent.getEvaluationApplicationPropertyById(Long.parseLong(hiddenStudyId), getArgMap());
				if(propertyView.getPropertyId()!=null)
					viewMap.put("propertyId", propertyView.getPropertyId());
				if(propertyView.getApplicationStatus()!=null)
					viewMap.put("applicationStatus", propertyView.getApplicationStatus());*/

				viewMap.put("studyLoaded", true);
				hiddenLoadStudy = "false";
			}
			logger.logInfo("loadStudy completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadStudy crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadProperty(){
		try{
			logger.logInfo("loadProperty started...");
			Boolean propertyLoaded = (Boolean) viewMap.get("propertyLoaded");
			propertyLoaded = (propertyLoaded==null)?false:propertyLoaded;
			Boolean loadProperty = hiddenLoadProperty.equals("true");
			if(StringHelper.isNotEmpty(hiddenPropertyId) && (!propertyLoaded || loadProperty)){
				viewMap.put("propertyId", hiddenPropertyId);
				PropertyView propertyView = projectServiceAgent.getProjectFollowPropertyId(Long.parseLong(hiddenPropertyId), CommonUtil.getArgMap());
				//Edited by Kamran:
				viewMap.put("propertyView", propertyView);
				
				if(propertyView.getPropertyId()!=null)
					viewMap.put("propertyId", propertyView.getPropertyId().toString());
				if(propertyView.getEndowedName()!=null)
					viewMap.put("propertyName", propertyView.getEndowedName());
				else
				{
					if(propertyView.getCommercialName()!=null)
						viewMap.put("propertyName", propertyView.getCommercialName());
				}
				if(getIsEnglishLocale()){
					if(propertyView.getPropertyTypeEn()!=null)
						viewMap.put("propertyType", propertyView.getPropertyTypeEn());
					if(propertyView.getCategoryTypeEn()!=null)
						viewMap.put("propertyOwnership", propertyView.getCategoryTypeEn());
				}
				else{
					if(propertyView.getPropertyTypeAr()!=null)
						viewMap.put("propertyType", propertyView.getPropertyTypeAr());
					if(propertyView.getCategoryTypeAr()!=null)
						viewMap.put("propertyOwnership", propertyView.getCategoryTypeAr());
				}
				
				if(propertyView.getTypeId()!=null)
					viewMap.put("propertyTypeId", propertyView.getTypeId().toString());
				
				if(propertyView.getCategoryId()!=null)
					viewMap.put("propertyOwnershipId", propertyView.getCategoryId().toString());
				
				if(propertyView.getLandNumber()!=null)
					viewMap.put("landNumber", propertyView.getLandNumber());
					
				viewMap.put("propertyLoaded", true);
				hiddenLoadProperty = "false";
			}
			logger.logInfo("loadProperty completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadProperty crashed...", ex);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	/*
	 * Button Click Action Handlers
	 */

	@SuppressWarnings("unchecked")
	public String saveHandOver() {
		logger.logInfo("saveHandOver() started...");
		Long projectId=0L;
		Boolean success = false;
		try {
				if(validatedForHandOver()){
					if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
						projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
					
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					ProjectDetailsView handOverView = new ProjectDetailsView();
					handOverView.setProjectId(projectId);
					handOverView.setHandOverDate(getHandOverDate());
					success = projectServiceAgent.saveProjectHandOver(handOverView, CommonUtil.getArgMap());
					
					if(success && completeTask(TaskOutcome.OK)){
						CommonUtil.saveRemarksAsComments(projectId, getRemarks(), noteOwner);
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.HANDOVER_DONE, projectId);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_HAND_OVER_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_HANDOVER_DONE);
						viewMap.put("handOverReadonlyMode", true);
						canAddAttachmentsAndComments(false);
					}
				}
			logger.logInfo("saveHandOver() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveHandOver() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_HAND_OVER_FAILURE));
		}
		return "saveHandOver";
	}
	
	@SuppressWarnings("unchecked")
	public String saveFollowUp() {
		logger.logInfo("saveFollowUp() started...");
		Long projectId=0L;
		Boolean success = false;
		try {
				if(validatedForFollowUp()){
					ProjectFollowUpActivityView projectFollowUpActivityView = new ProjectFollowUpActivityView();
					updateProjectFollowUpActivityViewToSave(projectFollowUpActivityView);
					if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
						projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
					
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					Map argMap = CommonUtil.getArgMap();
					if(viewMap.containsKey("lastFollowUpActivityId")){
						Long lastFollowUpActivityId =  (Long) viewMap.get("lastFollowUpActivityId");
						argMap.put("lastFollowUpActivityId", lastFollowUpActivityId);
					}
					success = projectServiceAgent.saveProjectFollowUpActivity(projectFollowUpActivityView, argMap);
					if(success){
						CommonUtil.saveRemarksAsComments(projectId, getFollowUpRemarks(), noteOwner);
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.FOLLOWUP_PERFORMED, projectId);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FOLLOW_UP_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_FOLLOW_UP_DONE);
						viewMap.put("followUpReadonlyMode", true);
						canAddAttachmentsAndComments(false);
						projectFollowupActivityList=getProjectFollowupActivityList();
						for(ProjectMileStoneView pmsVU:getProjectMilestoneList())
						{
							if(getNewMilestoneId().compareTo(pmsVU.getProjectMileStoneId().toString())==0)
								projectFollowUpActivityView.setNewAchievedProjectMilestoneName(pmsVU.getMileStoneName());
						}
						projectFollowupActivityList.add(projectFollowUpActivityView);
						viewMap.put("PROJECT_FOLLWOUP_ACTIVITY_LIST",projectFollowupActivityList);
						viewMap.put("followUpRecordSize", projectFollowupActivityList.size());
					}
				}
			logger.logInfo("saveFollowUp() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveFollowUp() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FOLLOW_UP_FAILURE));
		}
		return "saveFollowUp";
	}
	
	@SuppressWarnings("unchecked")
	public String completeFollowUp() {
		logger.logInfo("completeFollowUp() started...");
		Long projectId=0L;
		Boolean success = false;
		try {
				if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
					projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
				
				CommonUtil.loadAttachmentsAndComments(projectId.toString());
				Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
				Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);

				if(completeTask(TaskOutcome.OK)){
					CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.FOLLOWUP_DONE, projectId);
					errorMessages.clear();
					infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FOLLOW_UP_COMPLETE_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_FOLLOW_UP_DONE);
					viewMap.put("followUpReadonlyMode", true);
					canAddAttachmentsAndComments(false);
					viewMap.remove("showCompleteFollowUp");
				}
			logger.logInfo("completeFollowUp() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("completeFollowUp() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FOLLOW_UP_COMPLETE_FAILURE));
		}
		return "completeFollowUp";
	}
	//Check the project status here, it should be Follow up done
	//then check the property type, if it is land then open the recieve property screen to add new
	//receive newly constructed property, in case property type is building then allow the user to 
	//edit the selected property
	@SuppressWarnings("unchecked")
	public String saveInitialReceiving() {
		logger.logInfo("saveInitialReceiving() started...");
		Long projectId=0L;
		Boolean success = false;
		try {
				if(validatedForInitialReceiving()){
					if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
						projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
					
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					ProjectDetailsView initialReceivingView = new ProjectDetailsView();
					initialReceivingView.setProjectId(projectId);
					initialReceivingView.setInitialReceivingDate(getInitialReceivingDate());
					initialReceivingView.setActualEndDate(getActualEndDate());
					success = projectServiceAgent.saveProjectInitialAcceptance(initialReceivingView, CommonUtil.getArgMap());
					
					if(success && completeTask(TaskOutcome.OK)){
						CommonUtil.saveRemarksAsComments(projectId, getRemarks(), noteOwner);
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.INITIAL_ACCEPTANCE_DONE, projectId);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_INITIAL_RECEIVING_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_INITIAL_RECEIVING_DONE);
						viewMap.put("initialReceivingReadonlyMode", true);
						canAddAttachmentsAndComments(false);
					}
				}
			logger.logInfo("saveInitialReceiving() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveInitialReceiving() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_INITIAL_RECEIVING_FAILURE));
		}
		return "saveInitialReceiving";
	}
	
	@SuppressWarnings("unchecked")
	public String saveFinalReceiving() {
		logger.logInfo("saveFinalReceiving() started...");
		Long projectId=0L;
		Boolean success = false;
		try {
				if(validatedForFinalReceiving()){
					if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))			
						projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
					
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					ProjectDetailsView finalReceivingView = new ProjectDetailsView();
					finalReceivingView.setProjectId(projectId);
					finalReceivingView.setFinalReceivingDate(getFinalReceivingDate());
					success = projectServiceAgent.saveProjectFinalAcceptance(finalReceivingView, CommonUtil.getArgMap());
					
					if(success && completeTask(TaskOutcome.OK)){
						CommonUtil.saveRemarksAsComments(projectId, getRemarks(), noteOwner);
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.FINAL_ACCEPTANCE_DONE, projectId);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FINAL_RECEIVING_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_FINAL_RECEIVING_DONE);
						viewMap.put("finalReceivingReadonlyMode", true);
						canAddAttachmentsAndComments(false);
					}
				}
			logger.logInfo("saveFinalReceiving() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveFinalReceiving() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_FINAL_RECEIVING_FAILURE));
		}
		return "saveFinalReceiving";
	}
	
	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception{
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		try {
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = CommonUtil.getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, taskOutcome);
				}
			logger.logInfo("completeTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.logError("completeTask() crashed");
			success = false;
			throw exception;
		}
		return success;
	}
	
	public Boolean invokeBpel(Long projectId) {
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try {
			PIMSProjectFollowupBPELPortClient bpelPortClient= new PIMSProjectFollowupBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.ProjectFollowUp.BPEL_END_POINT);
			bpelPortClient.setEndpoint(endPoint);
			String userId = CommonUtil.getLoggedInUser();
			
			bpelPortClient.initiate(projectId, userId, designApprovalRequired(), handoverRequired(), null, null);

			logger.logInfo("invokeBpel() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
		}
		finally{
			if(!success)
			{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SUBMIT_FAILURE));
			}
		}
		return success;
	}
	
	@SuppressWarnings("unchecked")
	public void updateProjectDetailsViewToSave(ProjectDetailsView projectDetailsView) throws Exception{
		logger.logInfo("updateProjectDetailsViewToSave() started...");
		try {
				projectDetailsView.setProjectId((Long)viewMap.get("projectId"));
				projectDetailsView.setProjectNumber((String)viewMap.get("projectNumber"));
				projectDetailsView.setProjectStatusId(Convert.toLong((String)viewMap.get("projectStatusId")));
				projectDetailsView.setStudyId(Convert.toLong((String)viewMap.get("relatedStudyId")));
				projectDetailsView.setProjectName((String)viewMap.get("projectName"));
				projectDetailsView.setProjectTypeId(Convert.toLong((String)viewMap.get("projectTypeId")));
				projectDetailsView.setProjectPurposeId(Convert.toLong((String)viewMap.get("projectPurposeId")));
				projectDetailsView.setProjectSizeId(Convert.toLong((String)viewMap.get("projectSizeId")));
				projectDetailsView.setPropertyOwnershipId(Convert.toLong((String)viewMap.get("propertyOwnershipId")));
				projectDetailsView.setExpectedStartDate((Date)viewMap.get("expectedStartDate"));
				projectDetailsView.setExpectedEndDate((Date)viewMap.get("expectedEndDate"));
				projectDetailsView.setProjectCategoryId(Convert.toLong((String)viewMap.get("projectCategoryId")));
				String projectEstimatedCost = (String)viewMap.get("projectEstimatedCost");
				Double projectEstimatedCostVal = CommonUtil.getDoubleValue(projectEstimatedCost);
				projectDetailsView.setProjectEstimatedCost(projectEstimatedCostVal);
				projectDetailsView.setProjectDescription((String)viewMap.get("projectDescription"));
				projectDetailsView.setPropertyId(Convert.toLong((String)viewMap.get("propertyId")));
				projectDetailsView.setPropertyName((String)viewMap.get("propertyName"));
				projectDetailsView.setProjectMilestones(getProjectMilestoneList());
				
			logger.logInfo("updateProjectDetailsViewToSave() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("updateProjectDetailsViewToSave() crashed ", exception);
			throw exception;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateProjectFollowUpActivityViewToSave(ProjectFollowUpActivityView projectFollowUpActivityView) throws Exception{
		logger.logInfo("updateProjectFollowUpActivityViewToSave() started...");
		try {
			projectFollowUpActivityView.setProjectId((Long)viewMap.get("projectId"));
			projectFollowUpActivityView.setNewCompletionPercentage(Convert.toLong(getNewCompletionPercent()));
			projectFollowUpActivityView.setNewAchievedProjectMilestoneId(Convert.toLong(getNewMilestoneId()));
			projectFollowUpActivityView.setFollowupDate(getFollowUpDate());
			projectFollowUpActivityView.setRemark(getFollowUpRemarks());
			projectFollowUpActivityView.setActualProjectStartDate(getActualStartDate());
			projectFollowUpActivityView.setIsLastFollowUp(true);
				
			logger.logInfo("updateProjectFollowUpActivityViewToSave() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("updateProjectFollowUpActivityViewToSave() crashed ", exception);
			throw exception;
		}
	}
	public String saveProject()
	{
		return saveAndSubmitProject(false);
	}
	public String submitProject()
	{
		return saveAndSubmitProject(true);
	}
	@SuppressWarnings("unchecked")
	public String saveAndSubmitProject(boolean isCalledFromSubmit) {
		logger.logInfo("saveProject() started...");
		Boolean firstTime = false;
		Boolean crashed = false;
		try {
			
			if(validatedForNewProject()){
				ProjectDetailsView projectDetailsView = new ProjectDetailsView();
				Long projectId = (Long)viewMap.get("projectId");
				 
				if(projectId!=null){
					projectDetailsView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
					projectDetailsView.setProjectId(projectId);
				}
				else
					firstTime = true;
				
				updateProjectDetailsViewToSave(projectDetailsView);
				ProjectDetailsView projDetailsView = projectServiceAgent.saveProject(projectDetailsView, CommonUtil.getArgMap());
				viewMap.put(WebConstants.ProjectFollowUp.PROJECT_VIEW, projDetailsView);
				populateProjectDetailsTab();
				populateProjectMilestonesTab();
				 
				if(projDetailsView.getProjectId()!=null){
					projectId = projDetailsView.getProjectId();
					viewMap.put(WebConstants.ProjectFollowUp.PROJECT_ID, projectId);
					
					CommonUtil.loadAttachmentsAndComments(projectId.toString());
					
					Boolean attachSuccess = CommonUtil.saveAttachments(projectId);
					Boolean commentSuccess = CommonUtil.saveComments(projectId, noteOwner);
					
					errorMessages.clear();
//					firstTime = true;
						
						infoMessage=CommonUtil.getParamBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_CREATE_SUCCESS, projDetailsView.getProjectNumber());
						if(isCalledFromSubmit)
						{
							projectId = (Long) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
							if(projectId != null)
							 new ProjectService().setProjectAsSubmitted(projectId, getLoggedInUserId());
							invokeBpel(projectId);
							
							CommonUtil.saveSystemComments(noteOwner, "project.event.projectSubmitted", projectId);
							viewMap.put(PAGE_MODE, PAGE_MODE_DRAFT_DONE);
							infoMessage=CommonUtil.getBundleMessage("project.successMsg.projectSubmitted");
						}
						
						
						else
						{
							CommonUtil.saveSystemComments(noteOwner, MessageConstants.ProjectFollowUp.HistoryComments.PROJECT_CREATED, projectId);
							infoMessage=CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_SAVE_SUCCESS);
							viewMap.put(PAGE_MODE, PAGE_MODE_DRAFT);
						}
							
							
							viewMap.put("projectDetailsReadonlyMode", true);
							viewMap.put("asterisk", "");
							canAddAttachmentsAndComments(false);
							viewMap.put("canAddMilestone", false);
					
//					tabPanel.setSelectedTab(TAB_PROJECT_DETAILS);
				}
				 
			}
			logger.logInfo("saveProject() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("saveProject() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("saveProject() crashed ", exception);
		}
		finally{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectFollowUp.MSG_PROJECT_SAVE_FAILURE));
		}
		return "";
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(StringHelper.isEmpty(backScreen))
					backScreen = "home";
				if(isPopupViewMode()){
					FacesContext facesContext = FacesContext.getCurrentInstance();
			        String javaScriptText = "window.close();";
			        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
			        AddResource addResource = AddResourceFactory.getInstance(facesContext);
			        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
				}
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }

	@SuppressWarnings("unchecked")
	private void populateProjectMilestonesTab(){
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null){
			List<ProjectMileStoneView> projectMilestoneList = projectDetailsTabView.getProjectMilestones(); 
			if(projectMilestoneList!=null){
				viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, projectMilestoneList);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void populateProjectContractsTab(){
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null){
			List<ContractView> contractViewList = projectDetailsTabView.getContractViewList();
			viewMap.put(ProjectContractsTabBacking.ViewRootKeys.PROJECT_ID,projectDetailsTabView.getProjectId());
			if(contractViewList!=null){
				viewMap.put(WebConstants.ProjectContractsTab.PROJECT_CONTRACTS, contractViewList);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void populateProjectTendersTab(){
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null){
			List<TenderView> tenderViewList = projectDetailsTabView.getTenderViewList(); 
			if(tenderViewList!=null){
				viewMap.put(WebConstants.ProjectTendersTab.PROJECT_TENDERS, tenderViewList);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void populateProjectPaymentsTab(){
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null){
			List<PaymentScheduleView> paymentScheduleViewList = projectDetailsTabView.getPaymentScheduleViewList(); 
			if(paymentScheduleViewList!=null){
				viewMap.put(WebConstants.ProjectPaymentsTab.PROJECT_PAYMENTS, paymentScheduleViewList);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void populateProjectDetailsTab(){
		ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.ProjectFollowUp.PROJECT_VIEW);
		if(projectDetailsTabView!=null)
		{
		try
		{	
			//Edited by Kamran Ahmed:if the property is other than Land then provide the edit option.
			PropertyView propertyView;
			if(projectDetailsTabView.getPropertyId()!=null)
				{
					propertyView=new PropertyServiceAgent().getPropertyByID(projectDetailsTabView.getPropertyId());
					viewMap.put("propertyView", propertyView);
				}
			//Edited by Kamran Ahmed:
			if(projectDetailsTabView.getProjectId()!=null)
			{
				viewMap.put("projectId", projectDetailsTabView.getProjectId());
				List<PropertyView> propertyList=new ProjectServiceAgent().getAllPropertiesByPropjectId(projectDetailsTabView.getProjectId());
				if(propertyList!=null && propertyList.size()>0)
					viewMap.put("ASSOCIATED_PROPERTIES", propertyList);
			}
			if(projectDetailsTabView.getProjectNumber()!=null)
				viewMap.put("projectNumber", projectDetailsTabView.getProjectNumber());
			if(projectDetailsTabView.getProjectStatus()!=null)
				viewMap.put("projectStatus", projectDetailsTabView.getProjectStatus());
			if(projectDetailsTabView.getProjectStatusId()!=null)
				viewMap.put("projectStatusId", projectDetailsTabView.getProjectStatusId().toString());
			if(projectDetailsTabView.getStudyDescription()!=null)
				viewMap.put("studyDescription", projectDetailsTabView.getStudyDescription());
			if(projectDetailsTabView.getStudyId()!=null)
				viewMap.put("relatedStudyId", projectDetailsTabView.getStudyId().toString());
			if(projectDetailsTabView.getStudyTitle()!=null)
				viewMap.put("relatedStudy", projectDetailsTabView.getStudyTitle());
			if(projectDetailsTabView.getProjectName()!=null)
				viewMap.put("projectName", projectDetailsTabView.getProjectName());
			if(projectDetailsTabView.getProjectTypeId()!=null)
				viewMap.put("projectTypeId", projectDetailsTabView.getProjectTypeId().toString());
			if(projectDetailsTabView.getProjectType()!=null)
				viewMap.put("projectType", projectDetailsTabView.getProjectType());
			if(projectDetailsTabView.getPropertyType()!=null)
				viewMap.put("propertyType", projectDetailsTabView.getPropertyType());
			if(projectDetailsTabView.getPropertyTypeId()!=null)
				viewMap.put("propertyTypeId", projectDetailsTabView.getPropertyTypeId().toString());
			if(projectDetailsTabView.getProjectPurpose()!=null)
				viewMap.put("projectPurpose", projectDetailsTabView.getProjectPurpose());
			if(projectDetailsTabView.getProjectPurposeId()!=null)
				viewMap.put("projectPurposeId", projectDetailsTabView.getProjectPurposeId().toString());
			if(projectDetailsTabView.getProjectSize()!=null)
				viewMap.put("projectSize", projectDetailsTabView.getProjectSize());
			if(projectDetailsTabView.getProjectSizeId()!=null)
				viewMap.put("projectSizeId", projectDetailsTabView.getProjectSizeId().toString());
			if(projectDetailsTabView.getPropertyOwnership()!=null)
				viewMap.put("propertyOwnership", projectDetailsTabView.getPropertyOwnership());
			if(projectDetailsTabView.getPropertyOwnershipId()!=null)
				viewMap.put("propertyOwnershipId", projectDetailsTabView.getPropertyOwnershipId().toString());
			if(projectDetailsTabView.getExpectedStartDate()!=null)
				viewMap.put("expectedStartDate", projectDetailsTabView.getExpectedStartDate());
			if(projectDetailsTabView.getExpectedEndDate()!=null)
				viewMap.put("expectedEndDate", projectDetailsTabView.getExpectedEndDate());
			if(projectDetailsTabView.getProjectCategory()!=null)
				viewMap.put("projectCategory", projectDetailsTabView.getProjectCategory());
			if(projectDetailsTabView.getProjectCategoryId()!=null)
				viewMap.put("projectCategoryId", projectDetailsTabView.getProjectCategoryId().toString());
			if(projectDetailsTabView.getProjectEstimatedCost()!=null)
				viewMap.put("projectEstimatedCost", projectDetailsTabView.getProjectEstimatedCost().toString());
			if(projectDetailsTabView.getLandNumber()!=null)
				viewMap.put("landNumber", projectDetailsTabView.getLandNumber());
			if(projectDetailsTabView.getProjectDescription()!=null)
				viewMap.put("projectDescription", projectDetailsTabView.getProjectDescription());
			if(projectDetailsTabView.getPropertyId()!=null)
				viewMap.put("propertyId", projectDetailsTabView.getPropertyId().toString());
			if(projectDetailsTabView.getPropertyName()!=null)
				viewMap.put("propertyName", projectDetailsTabView.getPropertyName());
			if(projectDetailsTabView.getActualStartDate()!=null){
				viewMap.put("actualStartDate", projectDetailsTabView.getActualStartDate());
				viewMap.put("actualStartDateReadOnly", true);
			}
			if(projectDetailsTabView.getActualEndDate()!=null){
				viewMap.put("actualEndDate", projectDetailsTabView.getActualEndDate());
				viewMap.put("actualEndDateReadOnly", true);
			}
				
		}
			catch (PimsBusinessException e) 
			{
				logger.LogException("populateProjectDetailsTab() Crashed : ", e);
			}
		}
	}
	
	public void requestHistoryTabClick()
	{
		logger.logInfo("requestHistoryTabClick started...");
		try	
		{
			RequestHistoryController rhc=new RequestHistoryController();
			Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
			if(projectId!=null && projectId!=0L)
				rhc.getAllRequestTasksForRequest(noteOwner, projectId.toString());
			
			logger.logInfo("requestHistoryTabClick completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestHistoryTabClick crashed...",ex);
		}
	}

    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public Boolean getShowActionPanel(){
		return getShowHandOverActionPanel() || getShowInitialReceivingActionPanel() || getShowFinalReceivingActionPanel(); 
	}
	
	public Boolean getShowHandOverActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_HANDOVER))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_HANDOVER_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowFollowUpActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowInitialReceivingActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_INITIAL_RECEIVING))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_INITIAL_RECEIVING_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowFinalReceivingActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FINAL_RECEIVING))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FINAL_RECEIVING_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowProjectDetailsTab(){
		return true;
	}

	public Boolean getShowProjectMilestonesTab(){
		return true;
	}

	public Boolean getShowProjectPaymentTab(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT_DONE))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		return true;
	}
	
	public Boolean getShowContractsTab(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT_DONE))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		return true;
	}
	
	public Boolean getShowTendersTab(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT_DONE))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		return true;
	}
	
	public Boolean getShowHistory(){
//		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
//			return false;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return false;
		return true;
	}
	
	public Boolean getShowCancel(){
		return true;
	}
	
	public Boolean getShowSaveProject() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return true;
		return false;
	}
	
	public Boolean getShowSaveHandOver() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_HANDOVER))
			return true;
		return false;
	}

	public Boolean getShowSaveFollowUp() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FOLLOW_UP))
			return true;
		return false;
	}
	
	public Boolean getShowCompleteFollowUp() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if(viewMap.containsKey("showCompleteFollowUp"))
			return true;
		return false;
	}

	public Boolean getShowSaveInitial() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_INITIAL_RECEIVING))
			return true;
		return false;
	}
	
	public Boolean getShowSaveFinal() {
//		if (pageMode.equalsIgnoreCase("default"))
//			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_FINAL_RECEIVING))
			return true;
		return false;
	}

	public Boolean getProjectDetailsReadonlyMode(){
		Boolean projectDetailsReadonlyMode = (Boolean)viewMap.get("projectDetailsReadonlyMode");
		if(projectDetailsReadonlyMode==null)
			projectDetailsReadonlyMode = false;
		return projectDetailsReadonlyMode;
	}

	public Boolean getFollowUpReadonlyMode(){
		Boolean followUpReadonlyMode = (Boolean)viewMap.get("followUpReadonlyMode");
		return (followUpReadonlyMode==null)?false:followUpReadonlyMode;
	}
	
	public Boolean getHandOverReadonlyMode(){
		Boolean handOverReadonlyMode = (Boolean)viewMap.get("handOverReadonlyMode");
		return (handOverReadonlyMode==null)?false:handOverReadonlyMode;
	}
	
	public Boolean getInitialReceivingReadonlyMode(){
		Boolean initialReceivingReadonlyMode = (Boolean)viewMap.get("initialReceivingReadonlyMode");
		return (initialReceivingReadonlyMode==null)?false:initialReceivingReadonlyMode;
	}
	
	public Boolean getFinalReceivingReadonlyMode(){
		Boolean finalReceivingReadonlyMode = (Boolean)viewMap.get("finalReceivingReadonlyMode");
		return (finalReceivingReadonlyMode==null)?false:finalReceivingReadonlyMode;
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	private String getProjectTypeKey() throws PimsBusinessException{
		String dataValue = "";
		String projectTypeId = (String)viewMap.get("projectTypeId");
		if(StringHelper.isNotEmpty(projectTypeId)){
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			DomainDataView domainDataView = utilityServiceAgent.getDomainDataByDomainDataId(Convert.toLong(projectTypeId));
			dataValue = domainDataView.getDataValue();
		}
		return dataValue;
	}
	
	private Boolean atleastOneContractActive() throws PimsBusinessException{
		Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
		return projectServiceAgent.atleastOneContractActive(projectId);
	}
	
	private Boolean allContractsTerminated() throws PimsBusinessException{
		Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
		return projectServiceAgent.allContractsTerminated(projectId);
	}
	private List<String> getProjectContractsWithPendingPayments() throws PimsBusinessException{
		Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
		return projectServiceAgent.getProjectContractsWithPendingPayments(projectId);
	}	
	private Boolean designApprovalRequired() throws PimsBusinessException{
		String propertyType = getProjectTypeKey();
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_CONSTRUCTION))
			return true;
		return false;
	}
	
	private Boolean handoverRequired() throws PimsBusinessException{
		String propertyType = getProjectTypeKey();
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_CONSTRUCTION))
			return true;
		if(propertyType.equals(WebConstants.Project.PROJECT_TYPE_BOT))
			return true;
		return false;
	}
	
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	/**
	 * @return the hiddenStudyId
	 */
	public String getHiddenStudyId() {
		return hiddenStudyId;
	}

	/**
	 * @param hiddenStudyId the hiddenStudyId to set
	 */
	public void setHiddenStudyId(String hiddenStudyId) {
		this.hiddenStudyId = hiddenStudyId;
	}

	/**
	 * @return the hiddenLoadStudy
	 */
	public String getHiddenLoadStudy() {
		return hiddenLoadStudy;
	}

	/**
	 * @param hiddenLoadStudy the hiddenLoadStudy to set
	 */
	public void setHiddenLoadStudy(String hiddenLoadStudy) {
		this.hiddenLoadStudy = hiddenLoadStudy;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		String remarks = (String)viewMap.get("remarks");
		return (remarks==null)?"":remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		if(remarks!=null)
			viewMap.put("remarks", remarks);
	}

	/**
	 * @return the followUpRemarks
	 */
	public String getFollowUpRemarks() {
		String followUpRemarks = (String)viewMap.get("followUpRemarks");
		return (followUpRemarks==null)?"":followUpRemarks;
	}

	/**
	 * @param followUpRemarks the followUpRemarks to set
	 */
	public void setFollowUpRemarks(String followUpRemarks) {
		if(followUpRemarks!=null)
			viewMap.put("followUpRemarks", followUpRemarks);
	}

	/**
	 * @return the initialReceivingDate
	 */
	public Date getInitialReceivingDate() {
		Date initialReceivingDate = (Date)viewMap.get("initialReceivingDate");
		return initialReceivingDate;
	}

	/**
	 * @param initialReceivingDate the initialReceivingDate to set
	 */
	public void setInitialReceivingDate(Date initialReceivingDate) {
		if(initialReceivingDate!=null)
			viewMap.put("initialReceivingDate", initialReceivingDate);
	}

	/**
	 * @return the finalReceivingDate
	 */
	public Date getFinalReceivingDate() {
		Date finalReceivingDate = (Date)viewMap.get("finalReceivingDate");
		return finalReceivingDate;
	}

	/**
	 * @param finalReceivingDate the finalReceivingDate to set
	 */
	public void setFinalReceivingDate(Date finalReceivingDate) {
		if(finalReceivingDate!=null)
			viewMap.put("finalReceivingDate", finalReceivingDate);
	}

	/**
	 * @return the handOverDate
	 */
	public Date getHandOverDate() {
		Date handOverDate = (Date)viewMap.get("handOverDate");
		return handOverDate;
	}

	public void setHandOverDate(Date handOverDate) {
		if(handOverDate!=null)
			viewMap.put("handOverDate", handOverDate);
	}

	/**
	 * @return the followUpDate
	 */
	public Date getFollowUpDate() {
//		Date followUpDate = (Date)viewMap.get("followUpDate");
		Date followUpDate = new Date();
		return followUpDate;
	}

	/**
	 * @param followUpDate the followUpDate to set
	 */
	public void setFollowUpDate(Date followUpDate) {
		if(followUpDate!=null)
			viewMap.put("followUpDate", followUpDate);
	}

	public Date getActualEndDate() {
		Date actualEndDate = (Date)viewMap.get("actualEndDate");
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		if(actualEndDate!=null)
			viewMap.put("actualEndDate", actualEndDate);
	}
	
	public Date getActualStartDate() {
		Date actualStartDate = (Date)viewMap.get("actualStartDate");
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		if(actualStartDate!=null)
			viewMap.put("actualStartDate", actualStartDate);
	}

	public Boolean getActualStartDateReadOnly() {
		Boolean actualStartDateReadOnly = (Boolean)viewMap.get("actualStartDateReadOnly");
		return actualStartDateReadOnly==null?false:actualStartDateReadOnly;
	}
	
	public Boolean getActualEndDateReadOnly() {
		Boolean actualEndDateReadOnly = (Boolean)viewMap.get("actualEndDateReadOnly");
		return actualEndDateReadOnly==null?false:actualEndDateReadOnly;
	}

	/**
	 * @return the newMilestoneId
	 */
	public String getNewMilestoneId() {
		String newMilestoneId = (String)viewMap.get("newMilestoneId");
		return newMilestoneId;
	}

	/**
	 * @param newMilestoneId the newMilestoneId to set
	 */
	public void setNewMilestoneId(String newMilestoneId) {
		if(newMilestoneId!=null)
			viewMap.put("newMilestoneId", newMilestoneId);
	}
	
	public List<ProjectMileStoneView> getProjectMilestoneList() {
		List<ProjectMileStoneView> projectMilestoneList = (List<ProjectMileStoneView>)viewMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
		if(projectMilestoneList==null)
			projectMilestoneList = new ArrayList<ProjectMileStoneView>();
		return projectMilestoneList;
	}

	public List<SelectItem> getProjectMilstoneSelectList() {
		List<SelectItem> projectMilstoneSelectList = (List<SelectItem>) viewMap.get("projectMilstoneSelectList");
		List<ProjectMileStoneView> projectMilestoneList = getProjectMilestoneList();
		List<SelectItem> emptyList = new ArrayList<SelectItem>();
		if(projectMilestoneList==null)
			return emptyList;
		if(projectMilstoneSelectList==null) {
			projectMilstoneSelectList = new ArrayList<SelectItem>();
			SelectItem item = new SelectItem();
			for (ProjectMileStoneView milestone : projectMilestoneList) {
				item = new SelectItem(milestone.getProjectMileStoneId().toString(), milestone.getMileStoneName());
				projectMilstoneSelectList.add(item);
			}
			viewMap.put("projectMilstoneSelectList", projectMilstoneSelectList);
		}
		return projectMilstoneSelectList;
	}

	public String getCompletionPercent() {
		String completionPercent = (String)viewMap.get("completionPercent");
		return completionPercent;
	}

	public void setCompletionPercent(String completionPercent) {
		if(completionPercent!=null)
			viewMap.put("completionPercent", completionPercent);
	}

	public String getLastAchievedMilestone() {
		String lastAchievedMilestone = (String)viewMap.get("lastAchievedMilestone");
		return lastAchievedMilestone;
	}

	public void setLastAchievedMilestone(String lastAchievedMilestone) {
		if(lastAchievedMilestone!=null)
			viewMap.put("lastAchievedMilestone", lastAchievedMilestone);
	}

	public String getNewCompletionPercent() {
		String newCompletionPercent = (String)viewMap.get("newCompletionPercent");
		return newCompletionPercent;
	}

	public void setNewCompletionPercent(String newCompletionPercent) {
		if(newCompletionPercent!=null)
			viewMap.put("newCompletionPercent", newCompletionPercent);
	}

	public String getReadonlyStyleClassFollowUp(){
		if(pageMode.equals(PAGE_MODE_FOLLOW_UP))
			return "";
		return WebConstants.READONLY_STYLE_CLASS;
	}
	
	public String getHiddenLoadProperty() {
		return hiddenLoadProperty;
	}

	public void setHiddenLoadProperty(String hiddenLoadProperty) {
		this.hiddenLoadProperty = hiddenLoadProperty;
	}

	public String getHiddenPropertyId() {
		return hiddenPropertyId;
	}

	public void setHiddenPropertyId(String hiddenPropertyId) {
		this.hiddenPropertyId = hiddenPropertyId;
	}

	public HtmlInputTextarea getRemarksField() {
		return remarksField;
	}

	public void setRemarksField(HtmlInputTextarea remarksField) {
		this.remarksField = remarksField;
	}

	/**
	 * @return the followUpremarksField
	 */
	public HtmlInputTextarea getFollowUpremarksField() {
		return followUpremarksField;
	}

	/**
	 * @param followUpremarksField the followUpremarksField to set
	 */
	public void setFollowUpremarksField(HtmlInputTextarea followUpremarksField) {
		this.followUpremarksField = followUpremarksField;
	}
	
	public boolean isDraftMode(){
		return pageMode.equals(PAGE_MODE_DRAFT);
	}
	public boolean isNewMode(){
		return pageMode.equals(PAGE_MODE_NEW);
	}
	
	public boolean isPopupViewMode(){
		return pageMode.equals(PAGE_MODE_POPUP_VIEW);
	 }

	public List<ProjectFollowUpActivityView> getProjectFollowupActivityList() 
	{
		if(viewMap.containsKey("PROJECT_FOLLWOUP_ACTIVITY_LIST"))
			projectFollowupActivityList=(List<ProjectFollowUpActivityView>) viewMap.get("PROJECT_FOLLWOUP_ACTIVITY_LIST");
		return projectFollowupActivityList;
	}

	public void setProjectFollowupActivityList(
			List<ProjectFollowUpActivityView> projectFollowupActivityList) {
		this.projectFollowupActivityList = projectFollowupActivityList;
	}
	public void executeJavascript(String javascript) {
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);		
		} 
		catch (Exception exception) 
		{			
			logger.LogException(" executing JavaScript CRASHED --- ", exception);
		}
	}

	public List<DomainDataView> getProjectStatusList() {
		return projectStatusList;
	}

	public void setProjectStatusList(List<DomainDataView> projectStatusList) {
		this.projectStatusList = projectStatusList;
	}

	public DomainDataView getProjectStatusFollowUpDone() {
		return projectStatusFollowUpDone;
	}

	public void setProjectStatusFollowUpDone(
			DomainDataView projectStatusFollowUpDone) {
		this.projectStatusFollowUpDone = projectStatusFollowUpDone;
	}

	public List<DomainDataView> getPropertyTypeList() {
		return propertyTypeList;
	}

	public void setPropertyTypeList(List<DomainDataView> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}

	public List<DomainDataView> getPropertyStatusList() {
		return propertyStatusList;
	}

	public void setPropertyStatusList(List<DomainDataView> propertyStatusList) {
		this.propertyStatusList = propertyStatusList;
	}

	public DomainDataView getPropertyStatusRecieved() {
		return propertyStatusRecieved;
	}

	public void setPropertyStatusRecieved(DomainDataView propertyStatusRecieved) {
		this.propertyStatusRecieved = propertyStatusRecieved;
	}

	public DomainDataView getPropertyStatusUnderRecieving() {
		return propertyStatusUnderRecieving;
	}

	public void setPropertyStatusUnderRecieving(
			DomainDataView propertyStatusUnderRecieving) {
		this.propertyStatusUnderRecieving = propertyStatusUnderRecieving;
	}

	public boolean isShowPropertiesTab() 
	{
		if(viewMap.get("SHOW_PROPERTIES_TAB")!=null)
			showPropertiesTab=(Boolean) viewMap.get("SHOW_PROPERTIES_TAB");
		return showPropertiesTab;
	}

	public void setShowPropertiesTab(boolean isShowPropertiesTab) {
		this.showPropertiesTab = isShowPropertiesTab;
	}
	public String openReceiveProperty()
	{
		sessionMap.put("PROJECT_VIEW_FOR_RECEIVING_PROPERTY",viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY"));
		return "receiveProperty";
	}

	public boolean isShowAddPropertyButtton()
	{
		Long id;
		if(viewMap.get("propertyTypeId")!=null)
		{
		propertyTypeLand=(DomainDataView) viewMap.get("PROPERTY_TYPE_LAND");
		id=Long.parseLong(viewMap.get("propertyTypeId").toString());
		if(id.compareTo(propertyTypeLand.getDomainDataId())==0
		  && (viewMap.containsKey(HEADING)) && viewMap.get(HEADING).equals(WebConstants.ProjectFollowUp.Headings.PROJECT_DETAILS_MODE_INITIAL_RECEIVING))
			showAddPropertyButtton=true;
		else	
			showAddPropertyButtton=false;
		}
		return showAddPropertyButtton;
	}

	public void setShowAddPropertyButtton(boolean showAddPropertyButtton) {
		this.showAddPropertyButtton = showAddPropertyButtton;
	}

}