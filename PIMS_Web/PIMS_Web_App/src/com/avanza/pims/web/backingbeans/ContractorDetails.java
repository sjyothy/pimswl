package com.avanza.pims.web.backingbeans;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.EmailValidator;
import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.SecUserUserGroupId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ContractorAdd.Keys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

import com.avanza.pims.ws.vo.BusinessActivityView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContactReferenceView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.ui.util.ResourceUtil;

public class ContractorDetails extends AbstractController 
{
	
	
	private String styleClass;
	private Boolean isViewMode = false;
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	private Boolean isAddressShow;
	private Boolean isContactPersonShow;
	private Boolean closed=true;
	private Boolean isContactPersonClosed= true;
	private Integer contactPersonRecordSize = 0;
	private Integer paginatorRows = 0;
	private Integer	contactReferenceRecordSize=0;
	private Integer contactReferencePaginatorMaxPages=0;
	private Integer	contactInfoRecordSize=0;
	private Integer contactInfoPaginatorMaxPages=0;
	
	private ContractorView contractorView ;
	private ContractorView refView ;
	private ContactInfoView contactInfoView;
	private RegionView regionView=new RegionView();
	private ContactReferenceView contactReferenceView;
	private PropertyServiceAgent pSAgent = new PropertyServiceAgent(); 
	private Map<String, String> licenseSourceList;
	private List<String> serviceTypeList = new ArrayList<String>();
	private String strContractorStatusId;
	private List<String> businessAcitvitiesList = new ArrayList<String>();
	private List<String> errorMessages;
	private List<String> infoMessages;
	
	private String countryId; //holds the value of the selected country
	private String cityId; //holds the value of the selected area
	private String stateId; //holds the value of the selected emirate/state
	private String CRcountryId; //holds the value of the selected country
	private String CRstateId; //holds the value of the selected area
	private String CRCityId; //holds the value of the selected emirate/state
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>(); //to be displayed in combo
	private List<SelectItem> cities = new ArrayList<SelectItem>();  //to be displayed in combo
	private List<SelectItem> CRStateList = new ArrayList<SelectItem>();
	private List<SelectItem> CRCityList = new ArrayList<SelectItem>();
	
	private String crTitleId;
	private String crAddress1;
	private String crAddress2;
	private String crDesignation;
	private String crStreet;
	private String crPostCode;
	private String crHomePhone;
	private String crOfficePhone;
	private String crMobileNumber;
	private String crFax;
	private String crEmail;
	private Long personId;
	
	
	private Map<String, String> allBusinessActivityList =new HashMap<String, String>();
	private Map<String, String> allServiceTypesList =new HashMap<String, String>();
	private List<ContactInfoView> contactInfoList;
	private List<ContactReferenceView> contactReferencesList;
	
	private HtmlDataTable contactInfoDataTable = new HtmlDataTable();
	private HtmlDataTable contactRefDataTable = new HtmlDataTable();
	private List<ContactReferenceView> cRefToBeDeleted=new ArrayList<ContactReferenceView>(0);
	private List<ContactInfoView> cInfoToBeDeleted=new ArrayList<ContactInfoView>(0);
	
	private Map<String,Object> viewRootMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private final Logger logger = Logger.getLogger(ContractorDetails.class);
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_CONTRACTOR;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CONTRACTOR;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CONTRACTOR;
	private final String POPUP_MODE = "popUp";
	private final String PAGE_MODE = "pageMode";
	
	
	
	
	public ContractorDetails()
	{
		contractorView = new ContractorView();
		refView = new ContractorView();
		contactInfoView=new ContactInfoView();
		contactReferenceView=new ContactReferenceView();
		contactReferenceView.setContactInfoView(new ContactInfoView());
		contactInfoList = new ArrayList<ContactInfoView>();
		contactReferencesList = new ArrayList<ContactReferenceView>();
		this.isAddressShow = false;
		this.isContactPersonShow = false;
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

	}
	
	
	
	
	public void init()
	{
		super.init();
		errorMessages.clear();

		
		if (!isPostBack())
		{
			
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			
			if(request.getParameter(PAGE_MODE) != null)
				{
					viewRootMap.put(PAGE_MODE,request.getParameter(PAGE_MODE).toString());
					if(getIsViewModePopUp())
						setModeToView();
					
				}
			
			
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			
			if(request.getAttribute(WebConstants.CONTRACTOR_DETAILS) != null || 
					(viewRootMap.containsKey(WebConstants.CONTRACTOR_DETAILS)&&
							viewRootMap.get(WebConstants.CONTRACTOR_DETAILS)!=null))
			{
				if(request.getAttribute(WebConstants.CONTRACTOR_DETAILS) != null)
				{
					contractorView = (ContractorView) request.getAttribute(WebConstants.CONTRACTOR_DETAILS);
					request.removeAttribute(WebConstants.CONTRACTOR_DETAILS);
				}
				else
					contractorView = (ContractorView) viewRootMap.get(WebConstants.CONTRACTOR_DETAILS);
				
			
				setContractorView(contractorView);
			}
			
			if(sessionMap.get(WebConstants.CONTRACTOR_DETAILS) != null)
				{
					contractorView = (ContractorView) sessionMap.get(WebConstants.CONTRACTOR_DETAILS);
					sessionMap.remove(WebConstants.CONTRACTOR_DETAILS);
					setContractorView(contractorView);
				}
				
			
			if(request.getAttribute(WebConstants.PAGE_MODE) != null)
				{
					if(request.getAttribute(WebConstants.PAGE_MODE).toString().compareToIgnoreCase("detail_mode") == 0)
						{
							setModeToView();
						}
					else if(request.getAttribute(WebConstants.PAGE_MODE).toString().compareToIgnoreCase("edit_mode") == 0)
						{
							setModeToEdit();
												
						}
					
					
				}
			
			
			
			loadLicenseSourceList();
			getContractorInfo();
			CommonUtil.loadAttachmentsAndComments(contractorView.getPersonId().toString());
			

	    }
		
	
		
	}
	
	
	
	
	public void setModeToView()
	{
		viewMap.put("canAddAttachment",false);
		viewMap.put("canAddNote",false);
		viewRootMap.put(WebConstants.PAGE_MODE,true);
		
	}
	
	public void setModeToEdit()
	{
		viewRootMap.put("isAddressShow", true);
		viewRootMap.put("isContactPersonShow",true);
		viewRootMap.put("closed", false);
		viewRootMap.put("isContactPersonClosed", false);
		viewMap.put("canAddAttachment",true);
		viewMap.put("canAddNote",true);
		viewRootMap.put(WebConstants.PAGE_MODE,false);
	}

	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
		
		this.getContractorView();
	}
	
	
 private void loadLicenseSourceList() {
		
		String methodName = "loadLicenseSources()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> licenseSourceDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.LicenseSource.LICENSE_SOURCE);

			if (licenseSourceList == null)
				licenseSourceList = new HashMap<String, String>();

			for (DomainDataView licenseSource : licenseSourceDDL)
				getLicenseSourceList().put(
						(getIsEnglishLocale() ? licenseSource.getDataDescEn()
								: licenseSource.getDataDescAr()),licenseSource.getDomainDataId().toString());
			
			viewRootMap.put("licenses", licenseSourceList);

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}	
	}	
 
 
 
 
 private void getContractorInfo()
 {
	 try
	 {
			
		contractorView = this.getContractorView();
	
		ConstructionServiceAgent cSa = new ConstructionServiceAgent();
		refView = (ContractorView)cSa.getContractorById(contractorView.getPersonId(), getLicenseSourceList());
		//strContractorStatusId= refView.getContractorStatusId().toString();
		viewRootMap.put("refView", refView);
			
			loadBusinessActivityList();
			loadServiceTypesList();
			
			getContactList();
			getContactPersonList();
			
			if(!this.getIsViewMode())
			{
			 loadAllBusinessActivitiesList();
			 loadAllServiceTypesList();
			}

			
				
	
	 }
		catch (Exception e) {
			logger.LogException("getContractorInfo ", e);
		}
	 
 }
 
 
	private void getContactList()
	{	//ContractorView 
		refView=(ContractorView)viewRootMap.get("refView");
		try
		{
			if(refView.getContactInfoViewSet()!=null && refView.getContactInfoViewSet().size()>0)
			{
				logger.logInfo("Getting iterator for ContactInfoView ");
				Iterator iter=refView.getContactInfoViewSet().iterator();
				while(iter.hasNext())
				{	ContactInfoView contactInfoView1=new ContactInfoView();
					contactInfoView1=(ContactInfoView)iter.next();
					
						if (contactInfoView1.getIsDeleted()==0L) 
						{
							
													
							regionView = pSAgent.getRegionViewByRegionId(contactInfoView1.getCountryId());
							if (isEnglishLocale)
								contactInfoView1.setCountry(regionView
										.getDescriptionEn());
							else
								contactInfoView1.setCountry(regionView
										.getDescriptionAr());
							regionView = pSAgent.getRegionViewByRegionId(contactInfoView1
											.getStateId());
							if (isEnglishLocale)
								contactInfoView1.setState(regionView
										.getDescriptionEn());
							else
								contactInfoView1.setState(regionView
										.getDescriptionAr());
							regionView = pSAgent.getRegionViewByRegionId(contactInfoView1
											.getCityId());
							if (isEnglishLocale)
								contactInfoView1.setCity(regionView
										.getDescriptionEn());
							else
								contactInfoView1.setCity(regionView
										.getDescriptionAr());
							contactInfoList.add(contactInfoView1);
						}
				}
			}	
			viewRootMap.put("contactInfoList", contactInfoList);
			contactInfoPaging(contactInfoList.size());
		}
			
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}	
	
	
	private void getContactPersonList()
	{	try
		{	
			refView=(ContractorView)viewRootMap.get("refView");
						
			if(refView.getContactReferences()!=null && refView.getContactReferences().size()>0)
			{
				logger.logInfo("Getting iterator for ContactReferenceView ");
				Iterator iter=refView.getContactReferences().iterator();
				while(iter.hasNext())
				{	ContactReferenceView contactReferenceView=new ContactReferenceView();
					contactReferenceView=(ContactReferenceView)iter.next();
					
							if (contactReferenceView.getIsDeleted()==0L) 
							{
														
								regionView = pSAgent.getRegionViewByRegionId(contactReferenceView
													.getContactInfoView().getCountryId());
									if (isEnglishLocale)
										contactReferenceView.getContactInfoView()
												.setCountry(regionView.getDescriptionEn());
									else
										contactReferenceView.getContactInfoView()
												.setCountry(regionView.getDescriptionAr());
									
									regionView =pSAgent.getRegionViewByRegionId(contactReferenceView
													.getContactInfoView().getStateId());
									if (isEnglishLocale)
										contactReferenceView.getContactInfoView().setState(
												regionView.getDescriptionEn());
									else
										contactReferenceView.getContactInfoView().setState(
												regionView.getDescriptionAr());
									
									PersonView pView = new PersonView();
									pView = pSAgent.getPersonInformation(contactReferenceView.getTenantId());
									
									
									if (isEnglishLocale)
										contactReferenceView.setNationality(pView.getNationalityEn());
									else
										contactReferenceView.setNationality(pView.getNationalityAr());
									
									if (isEnglishLocale)
										contactReferenceView.setContractorType(pView.getPersonTypeEn());
									else
										contactReferenceView.setContractorType(pView.getPersonTypeAr());								
										
									
									contactReferencesList.add(contactReferenceView);
							}
					
				}
			}	
			viewRootMap.put("refPersonList", contactReferencesList);
			contactRefPaging(contactReferencesList.size());
		}	
			catch (Exception e) 
			{
				logger.LogException("getContractRefernces crashed due to ", e);
				e.printStackTrace();
			} 
	
	}	
 
 
 
 
 
 
 
private void loadBusinessActivityList()
 {
	 if(viewRootMap.containsKey("refView") && viewRootMap.get("refView") != null)
		 refView = (ContractorView)viewRootMap.get("refView");
	 
	try{
		if(refView != null)
	 {
		 List<String> businessActivies = new ArrayList<String>();
		 List<BusinessActivityView> businessActivityView = new ArrayList<BusinessActivityView>();
		 businessActivies=refView.getBusinessActivities();
		
		if(businessActivies != null)
		{
			viewRootMap.put("businessActivies", businessActivies);
			if(businessAcitvitiesList == null)
				businessAcitvitiesList = new ArrayList<String>();
			
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			businessActivityView= csa.getAllBusinessActivities();
			for(BusinessActivityView bav:businessActivityView)
			{	
				for(String ba: businessActivies)
				{
					if(bav.getBusinessActivityId()==(Long.parseLong(ba)))
					{
						if(getIsViewMode())
						{
							if(getIsEnglishLocale())
								businessAcitvitiesList.add(bav.getDescriptionEn());
							else
								businessAcitvitiesList.add(bav.getDescriptionAr());
						}
						else
						{
							businessAcitvitiesList.add(bav.getBusinessActivityId().toString());
						}							
							
							
					}
					
				}
			}
			setBusinessAcitvitiesList(businessAcitvitiesList);
			//viewRootMap.put("businessAcitvitiesList", businessAcitvitiesList);
		}
	 }
	}
	catch (Exception e) {
		logger.LogException("load business activites crashed ", e);
		errorMessages.clear(); 
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
 }
 
 
 private void loadServiceTypesList()
 {
	 if(viewRootMap.containsKey("refView") && viewRootMap.get("refView") != null)
		 refView = (ContractorView)viewRootMap.get("refView");
	  
	   try{
		   
		 if(refView != null)
		 {
	 
			List<String> serviceTypes = new ArrayList<String>();
			serviceTypes = refView.getServiceTypes();
			
			if(serviceTypeList == null)
				serviceTypeList = new ArrayList<String>();
		
			if(serviceTypes != null)
			{
				CommonUtil commonUtil = new CommonUtil();
				List<DomainDataView> serviceTypeDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);
			
				for (DomainDataView serviceType : serviceTypeDDL)
				{
					for(String sType : serviceTypes)
					{
						if(serviceType.getDomainDataId().compareTo(Long.parseLong(sType))==0)
						{
							if(getIsViewMode())
							{
								if(getIsEnglishLocale())
									serviceTypeList.add(serviceType.getDataDescEn());
								else
									serviceTypeList.add(serviceType.getDataDescAr());
							}
							else
								serviceTypeList.add(serviceType.getDomainDataId().toString());
								
			
					     }
				   }
				}
				setServiceTypeList(serviceTypeList);
				//viewRootMap.put("ServiceTypeList", serviceTypeList);
			}
			
	   }
	   }
	   catch (Exception e) {
			logger.LogException("load serviceTypes crashed ", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
	 
 }
 
 
 
private final void loadAllBusinessActivitiesList() {
		

		String methodName = "loadAllBusinessActivityList()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			ConstructionServiceAgent csa = new ConstructionServiceAgent();
			List<BusinessActivityView> businessActivityViewList = csa
					.getAllBusinessActivities();

			if (allBusinessActivityList == null)
				allBusinessActivityList = new HashMap<String, String>();

			for (BusinessActivityView businessActivityView : businessActivityViewList)
				getAllBusinessActivityList()
						.put(
								getIsEnglishLocale() ? businessActivityView
										.getDescriptionEn()
										: businessActivityView
												.getDescriptionAr(),
								businessActivityView.getBusinessActivityId()
										.toString());
			
			viewRootMap.put("allBusinessActivityList", allBusinessActivityList);

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}		
		
			
			
	}
	
	
	
private void loadAllServiceTypesList()
	{
		
		String methodName = "loadAllServiceTypeList()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			CommonUtil commonUtil = new CommonUtil();
			List<DomainDataView> serviceTypeDDL = commonUtil
					.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);

			if (allServiceTypesList == null)
				allServiceTypesList = new HashMap<String, String>();

			for (DomainDataView serviceType : serviceTypeDDL)
				getAllServiceTypesList().put(
						getIsEnglishLocale() ? serviceType.getDataDescEn()
								: serviceType.getDataDescAr(),
						serviceType.getDomainDataId().toString());
			

			viewRootMap.put("allServiceTypesList", allServiceTypesList);

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}		
		
	}
 

public void viewContactInfo()
{
	try {
		isAddressShow = true;
		viewRootMap.put("closed", false);
		viewRootMap.put("isAddressShow", isAddressShow);
				
		contactInfoView = new ContactInfoView();
		contactInfoView = (ContactInfoView) contactInfoDataTable.getRowData();
		
		if(contactInfoView.getRowId() == null)
			contactInfoView.setRowId(contactInfoView.getContactInfoId());
		
		viewRootMap.put("SELECTED_ROW_ID", contactInfoView.getRowId());
		
		RegionView  regionView = pSAgent.getRegionViewByRegionId(contactInfoView.getCityId());
		if (getIsEnglishLocale())
			contactInfoView.setCity(regionView.getDescriptionEn());
		else
			contactInfoView.setCity(regionView.getDescriptionAr());
		regionView = pSAgent.getRegionViewByRegionId(contactInfoView.getStateId());
		if (getIsEnglishLocale())
			contactInfoView.setState(regionView.getDescriptionEn());
		else
			contactInfoView.setState(regionView.getDescriptionAr());
	
		regionView = pSAgent.getRegionViewByRegionId(contactInfoView.getCountryId());
		if (getIsEnglishLocale())
			contactInfoView.setCountry(regionView.getDescriptionEn());
		else
			contactInfoView.setCountry(regionView.getDescriptionAr());

		
		if(contactInfoView.getCountryId() != -1)
		{
			stateList=getStateList(contactInfoView.getCountryId());
			this.setStates(stateList);
		}
		
		if(contactInfoView.getStateId() != -1)
		{
			cityList=getCityList(contactInfoView.getStateId());
			this.setCities(cityList);
		}
		
		
		
		if(contactInfoView.getCountryId() != null)
			viewRootMap.put("countryId", contactInfoView.getCountryId());
		if(contactInfoView.getCityId() != null)
			viewRootMap.put("cityId", contactInfoView.getCityId());
		if(contactInfoView.getStateId() != null)
			viewRootMap.put("stateId", contactInfoView.getStateId());
		viewRootMap.put("contactInfoView", contactInfoView);
	} 
	catch (Exception e)
	{
		e.printStackTrace();
	}	
}


public void viewContactRefInfo()
{
	try {
		isContactPersonShow = true;
		viewRootMap.put("isContactPersonClosed", false);
		viewRootMap.put("isContactPersonShow", isContactPersonShow);
				
		contactReferenceView = new ContactReferenceView();
		contactReferenceView = (ContactReferenceView) contactRefDataTable.getRowData();
		if(contactReferenceView.getRowId() == null)
			contactReferenceView.setRowId(contactReferenceView.getContactReferenceId());
		
		viewRootMap.put("SELECTED_REF_ROW_ID", contactReferenceView.getRowId());
		
		
		
		RegionView  regionView = pSAgent.getRegionViewByRegionId(contactReferenceView.getContactInfoView().getCityId());
		if (getIsEnglishLocale())
			contactReferenceView.getContactInfoView().setCity(regionView.getDescriptionEn());
		else
			contactReferenceView.getContactInfoView().setCity(regionView.getDescriptionAr());
		regionView = pSAgent.getRegionViewByRegionId(contactReferenceView.getContactInfoView().getStateId());
		if (getIsEnglishLocale())
			contactReferenceView.getContactInfoView().setState(regionView.getDescriptionEn());
		else
			contactReferenceView.getContactInfoView().setState(regionView.getDescriptionAr());
		
		regionView = pSAgent.getRegionViewByRegionId(contactReferenceView.getContactInfoView().getCountryId());
		if (getIsEnglishLocale())
			contactReferenceView.getContactInfoView().setCountry(regionView.getDescriptionEn());
		else
			contactReferenceView.getContactInfoView().setCountry(regionView.getDescriptionAr());
		
		if(contactReferenceView.getContactInfoView().getCountryId() != -1)
		{
			stateList=getStateList(contactReferenceView.getContactInfoView().getCountryId());
			this.setCRStateList(stateList);
			viewRootMap.put("CRStateList", stateList);
		}
		
		if(contactReferenceView.getContactInfoView().getStateId() != -1)
		{
			cityList=getCityList(contactReferenceView.getContactInfoView().getStateId());
			this.setCRCityList(cityList);
			viewRootMap.put("CRCityList", cityList);
		}
		
		
		if(contactReferenceView.getContactInfoView().getCountryId() != null)
			viewRootMap.put("CRCountryId", contactReferenceView.getContactInfoView().getCountryId());
		if(contactReferenceView.getContactInfoView().getCityId() != null)
			viewRootMap.put("CRCityId", contactReferenceView.getContactInfoView().getCityId());
		if(contactReferenceView.getContactInfoView().getStateId() != null)
			viewRootMap.put("CRStateId", contactReferenceView.getContactInfoView().getStateId());
		viewRootMap.put("contactReferenceView", contactReferenceView);
	} 
	catch (Exception e)
	{
		e.printStackTrace();
	}	
}

public void updateContactInfo()
{
	try
	{
		ContactInfoView	CInfo =new ContactInfoView();
		CInfo= getContactInfoView();
		Long contactInfoViewId=null;
		RegionView myRegionView=new RegionView();
		if(viewRootMap.get("contactInfoView")!=null)
		{
			contactInfoViewId=((ContactInfoView)viewRootMap.get("contactInfoView")).getContactInfoId();
			CInfo.setContactInfoId(contactInfoViewId);
		}
		if(contactInfoValidData(CInfo))
		{
			if(viewRootMap.containsKey("countryId"))
			{
				CInfo.setCountryId(Convert.toLong(viewRootMap.get("countryId")));
				CInfo.setCountryIdString((viewRootMap.get("countryId")).toString());
				myRegionView = pSAgent.getRegionViewByRegionId(CInfo.getCountryId());
				if(isEnglishLocale)
					CInfo.setCountry(myRegionView.getDescriptionEn());
				else
					CInfo.setCountry(myRegionView.getDescriptionAr());
									 
			}
			if(viewRootMap.containsKey("stateId"))
				{
					CInfo.setStateId(Convert.toLong(viewRootMap.get("stateId")));
					CInfo.setStateIdString((viewRootMap.get("stateId")).toString());
					myRegionView = pSAgent.getRegionViewByRegionId(CInfo.getStateId());
					if(isEnglishLocale)
						CInfo.setState(myRegionView.getDescriptionEn());
					else
						CInfo.setState(myRegionView.getDescriptionAr());
				}
			if(viewRootMap.containsKey("cityId"))
				{
					CInfo.setCityId(Convert.toLong(viewRootMap.get("cityId")));
					CInfo.setCityIdString((viewRootMap.get("cityId")).toString());
					myRegionView = pSAgent.getRegionViewByRegionId(CInfo.getCityId());
					if(isEnglishLocale)
						CInfo.setCity(myRegionView.getDescriptionEn());
					else
						CInfo.setCity(myRegionView.getDescriptionAr());
				}
						
							
			
			int rowId = -1;
			if (viewRootMap.containsKey("SELECTED_ROW_ID")
					&& viewRootMap.get("SELECTED_ROW_ID") != null)
			{
				rowId = getContactInfoFromListForRowId(contactInfoList,
						new Long(viewRootMap.get("SELECTED_ROW_ID")
								.toString()), CInfo);
				CInfo.setRowId(new Long(viewRootMap.get("SELECTED_ROW_ID").toString()));
				viewRootMap.remove("SELECTED_ROW_ID");
			}

			if (rowId >= 0)
			{
				contactInfoList.set(rowId, CInfo);
			}
			else 
			{
				if (viewRootMap.containsKey("ROW_ID")
						&& viewRootMap.get("ROW_ID") != null)
					CInfo.setRowId(new Long(viewRootMap.get("ROW_ID").toString()) + 1);
				else
					CInfo.setRowId(new Long(0));

				viewRootMap.put("ROW_ID", CInfo
						.getRowId());
				contactInfoList.add(CInfo);
			}
				
			clearFields();	
			viewRootMap.put("contactInfoList", contactInfoList);
			contactInfoPaging(contactInfoList.size());
	}
	}
	catch ( Exception e )
	{
		logger.LogException("updateAddress", e);
	}
	
}

	private final int getContactInfoFromListForRowId(
			List<ContactInfoView> CIList, Long rowId, ContactInfoView ciView) 
	{
		for (int i = 0; i < CIList.size(); i++) 
		{
			ContactInfoView contactInfoView = (ContactInfoView) CIList.get(i);
			if (contactInfoView.getRowId().compareTo(rowId) == 0)
			{
				ciView.setContactInfoId(contactInfoView.getContactInfoId());
				ciView.setPersonContactInfoId(contactInfoView
						.getPersonContactInfoId());
				return i;
			}
		}

		return -1;
	}

	private void clearFields()
	{
		contactInfoView = new ContactInfoView();
		contactInfoView.setAddress1("");
		contactInfoView.setAddress2("");
		this.setCountryId("-1");
		this.setStateId("-1");
		this.setCityId("-1");
		contactInfoView.setEmail("");
		contactInfoView.setFax("");
		contactInfoView.setHomePhone("");
		contactInfoView.setOfficePhone("");
		contactInfoView.setPostCode("");
		contactInfoView.setStreet("");
		
		List<SelectItem> emptyList = new ArrayList<SelectItem>();
		this.setStates(emptyList);
		this.setCities(emptyList);
		
		viewRootMap.put("contactInfoView",contactInfoView);
	
	}
	
	private boolean contactInfoValidData(ContactInfoView CInfo)
	{
		
		boolean isValid = true;
		errorMessages.clear();
		if(CInfo.getAddress1() == null || CInfo.getAddress1().trim().length() <= 0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_ADDRESS_INFO_REQ)));
			isValid = false;
		}
		
		EmailValidator emailValidator = EmailValidator.getInstance();
		if (CInfo.getEmail() != null && CInfo.getEmail().trim().length() > 0
				&& !emailValidator.isValid(CInfo.getEmail())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_INVALID_EMAIL)));
			isValid = false;
		}
		if (CInfo.getEmail() == null || CInfo.getEmail().trim().length() <= 0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_EMAIL_REQUIRED)));
			isValid = false;
			
		}
		
		return isValid;
	}
	
public void updateContactReference()
{
	try
	{
		ContactReferenceView cRInfo =new ContactReferenceView();		
		cRInfo= getContactReferenceView();
		Long contactReferenceViewId=null;
		RegionView myRegionView = new RegionView();
		if(viewRootMap.get("contactReferenceView")!=null)
		{
			contactReferenceViewId=((ContactReferenceView)viewRootMap.get("contactReferenceView")).getContactReferenceId();
			cRInfo.setContactReferenceId(contactReferenceViewId);
		}
		if(contactReferencesValidData(cRInfo))
		{
		if(viewRootMap.containsKey("CRCountryId"))
			{
				cRInfo.getContactInfoView().setCountryId(Convert.toLong(viewRootMap.get("CRCountryId")));
				cRInfo.getContactInfoView().setCountryIdString((viewRootMap.get("CRCountryId")).toString());
				myRegionView = pSAgent.getRegionViewByRegionId(cRInfo.getContactInfoView().getCountryId());
				if(isEnglishLocale)
					cRInfo.getContactInfoView().setCountry(myRegionView.getDescriptionEn());
				else
					cRInfo.getContactInfoView().setCountry(myRegionView.getDescriptionAr());
				
			}
			if(viewRootMap.containsKey("CRStateId"))
				{	
					cRInfo.getContactInfoView().setStateId(Convert.toLong(viewRootMap.get("CRStateId")));
					cRInfo.getContactInfoView().setStateIdString((viewRootMap.get("CRStateId")).toString());
					myRegionView =pSAgent.getRegionViewByRegionId(cRInfo.getContactInfoView().getStateId());
					if(isEnglishLocale)
						cRInfo.getContactInfoView().setState(myRegionView.getDescriptionEn());
					else
					cRInfo.getContactInfoView().setState(myRegionView.getDescriptionAr());
				}
			if(viewRootMap.containsKey("CRCityId"))
				{
					cRInfo.getContactInfoView().setCityId(Convert.toLong(viewRootMap.get("CRCityId")));
					cRInfo.getContactInfoView().setCityIdString((viewRootMap.get("CRCityId")).toString());
					myRegionView =pSAgent.getRegionViewByRegionId(cRInfo.getContactInfoView().getCityId());
					if(isEnglishLocale)
						cRInfo.getContactInfoView().setCity(myRegionView.getDescriptionEn());
					else
					cRInfo.getContactInfoView().setCity(myRegionView.getDescriptionAr());
				}
			

			int rowId = -1;
			if (viewRootMap.containsKey("SELECTED_REF_ROW_ID")
					&& viewRootMap.get("SELECTED_REF_ROW_ID") != null) {
				rowId = getContactRefFromListForRowId(contactReferencesList,
						new Long(viewRootMap.get("SELECTED_REF_ROW_ID")
								.toString()), cRInfo);
				cRInfo.setRowId(new Long(viewRootMap.get("SELECTED_REF_ROW_ID").toString()));
				viewRootMap.remove("SELECTED_REF_ROW_ID");
			}

			if (rowId >= 0) {
				contactReferencesList.set(rowId, cRInfo);
			} else {
				if (viewRootMap.containsKey("REFERENCE_ROW_INDEX")
						&& viewRootMap.get("REFERENCE_ROW_INDEX") != null)
					cRInfo.setRowId(new Long(viewRootMap.get("REFERENCE_ROW_INDEX").toString()) + 1);
				else
					cRInfo.setRowId(new Long(0));

				viewRootMap.put("REFERENCE_ROW_INDEX",cRInfo.getRowId());
				contactReferencesList.add(cRInfo);
			}
			
			clearContactReferencesFields();
			viewRootMap.put("refPersonList", contactReferencesList);
			contactRefPaging(contactReferencesList.size());
		}	
	}
	catch ( Exception e )
	{
		logger.LogException("updateRefPerson", e);
	}
	
}

	public int getContactRefFromListForRowId(List<ContactReferenceView> CRList,
			Long rowId, ContactReferenceView crv) {
		for (int i = 0; i < CRList.size(); i++) {
			ContactReferenceView contactRefView = (ContactReferenceView) CRList
					.get(i);
			if (contactRefView.getRowId().compareTo(rowId) == 0) {
				crv.setContactReferenceId(contactRefView
						.getContactReferenceId());
				crv.getContactInfoView().setContactInfoId(
						contactRefView.getContactInfoView().getContactInfoId());
				return i;
			}
		}
	
		return -1;
	}
	private void clearContactReferencesFields()
	{
		contactReferenceView = new ContactReferenceView();
		contactReferenceView.setContactInfoView(new ContactInfoView());
		contactReferenceView.setFirstName("");
		contactReferenceView.setMiddleName("");
		contactReferenceView.setLastName("");
		contactReferenceView.getContactInfoView().setAddress1("");
		contactReferenceView.getContactInfoView().setAddress2("");
		contactReferenceView.getContactInfoView().setDesignation("");
		this.setCRcountryId("-1");
		this.setCRstateId("-1");
		this.setCRCityId("-1");
		contactReferenceView.getContactInfoView().setEmail("");
		contactReferenceView.getContactInfoView().setFax("");
		contactReferenceView.getContactInfoView().setHomePhone("");
		contactReferenceView.getContactInfoView().setOfficePhone("");
		contactReferenceView.getContactInfoView().setMobileNumber("");
		contactReferenceView.getContactInfoView().setPostCode("");
		contactReferenceView.getContactInfoView().setStreet("");
		contactReferenceView.setTitle("-1");
		
		List<SelectItem> emptyList = new ArrayList<SelectItem>();
		this.setCRStateList(emptyList);
		this.setCRCityList(emptyList);
		
		viewRootMap.put("contactReferenceView",contactReferenceView);
	}

	
	private boolean contactReferencesValidData(ContactReferenceView cRInfo) {
		boolean isValid = true;
		errorMessages.clear();

		if (cRInfo.getFirstName() == null || cRInfo.getFirstName().trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_FIRST_NAME)));
			isValid = false;
		}

		if (cRInfo.getLastName() == null || cRInfo.getLastName().trim().length() <= 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_LAST_NAME)));
			isValid = false;
		}

		if (cRInfo.getContactInfoView().getAddress1() == null || cRInfo.getContactInfoView().getAddress1().trim().length() <= 0) {
			errorMessages
					.add(ResourceUtil
							.getInstance()
							.getProperty(
									(MessageConstants.Person.MSG_CONTACT_REF_ADDRESS_INFO_REQ)));
			isValid = false;
		}

		EmailValidator emailValidator = EmailValidator.getInstance();
		if (cRInfo.getContactInfoView().getEmail() != null && cRInfo.getContactInfoView().getEmail().trim().length() > 0
				&& !emailValidator.isValid(cRInfo.getContactInfoView().getEmail())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_CONTACT_REF_INVALID_EMAIL)));
			isValid = false;
		}
		
		if (cRInfo.getContactInfoView().getEmail() == null || cRInfo.getContactInfoView().getEmail().trim().length() <= 0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Person.MSG_EMAIL_REQUIRED)));
			isValid = false;
			
		}

		return isValid;
	}

	
	public void save()
	{
		String methodName="save";
		infoMessages=new ArrayList<String>(0);
    	errorMessages = new ArrayList<String>(0);
    	try {
		    logger.logInfo(methodName+"| Start..");
            
		    if (!isContractorDetailsValid() || !isContactInfoListValid() || !isContactPersonsValid())
				return;
		    
		    putControlValuesInView(this.getContractorView());
	   		ContractorView contractorToSave=(ContractorView)viewRootMap.get(WebConstants.CONTRACTOR_DETAILS);
	   		contractorToSave.setBusinessActivities(this.getBusinessAcitvitiesList());
	   		contractorToSave.setServiceTypes(this.getServiceTypeList());
	   		
	   		ConstructionServiceAgent csa = new ConstructionServiceAgent();
	   		csa.addContractor(contractorToSave, getBusinessAcitvitiesList(), getServiceTypeList());

			if(contractorToSave.getPersonId()!=null)
			{
				CommonUtil.loadAttachmentsAndComments(contractorToSave.getPersonId().toString());
				CommonUtil.saveAttachments(contractorToSave.getPersonId());
				CommonUtil.saveComments(contractorToSave.getPersonId(), noteOwner);
			}
	   		
			viewRootMap.put(WebConstants.CONTRACTOR_DETAILS, contractorToSave);
			String successMessage="";
			
			if(viewMap.containsKey("loginId"))
			{
				this.infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Person.MSG_ID_GENERATED));
				viewMap.remove("loginId");
			}
			else
			   infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Contractor.MSG_UPDATED_SUCESS));
			
			logger.logInfo(methodName+"| Finish..");
			
		} catch (Exception e)
		{

			logger.LogException(methodName+"| Error Occured..",e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}		
	
	}
	
	
	
	private boolean isContractorDetailsValid() {
		boolean isValid = true;
		contractorView = this.getContractorView();
		if (contractorView.getStatusId() == null
				|| contractorView.getStatusId().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_STATUS_REQ)));
			isValid = false;
		}

		if (contractorView.getContractorNameEn() == null
				|| contractorView.getContractorNameEn().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_NAME_EN_REQ)));
			isValid = false;
		}

		if (contractorView.getContractorNameAr() == null
				|| contractorView.getContractorNameAr().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_NAME_AR_REQ)));
			isValid = false;
		}

		if (contractorView.getCommercialName() == null
				|| contractorView.getCommercialName().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_COMMERCIAL_NAME_REQ)));
			isValid = false;
		}
		
		
		if (contractorView.getContractorTypeId() == null
				|| contractorView.getContractorTypeId().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_TYPE_REQ)));
			isValid = false;
		}

		if (contractorView.getLicenseNumber() == null
				|| contractorView.getLicenseNumber().trim().length() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_LICENSE_NUMBER_REQ)));
			isValid = false;
		}

		if (contractorView.getLicenseSourceId() == null
				|| contractorView.getLicenseSourceId().trim().equals("-1")) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_LICENSE_SOURCE_REQ)));
			isValid = false;
		}

		if (contractorView.getLicenseIssueDate() == null ) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_ISSUE_DATE_REQ)));
			isValid = false;
		}

		if (contractorView.getLicenseExpiryDate() == null ) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_EXPIRY_DATE_REQ)));
			isValid = false;
		}

		return isValid;
	}

	// validates the address details tab, checks if the user has added atleast one record or not
	private boolean isContactInfoListValid() {
		boolean isValid = true;

		if (getContactInfoList() == null
				|| getContactInfoList().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTRACTOR_ADDRESS_DETAILS_REQ)));
			isValid = false;
		}

		return isValid;
	}

	// validates the address details tab, checks if the user has added atleast one record or not
	private boolean isContactPersonsValid() {
		boolean isValid = true;

		if (getContactReferencesList() == null
				|| getContactReferencesList().size() == 0) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					(MessageConstants.Contractor.MSG_CONTACT_REFERENCES_REQ)));
			isValid = false;
		}

		return isValid;
	}

	
	
	
	private void putControlValuesInView(ContractorView contractorToSave)
	{	ContractorView currentContractorViewView=(ContractorView)viewRootMap.get("refView");
		if(currentContractorViewView.getContractorStatusId()!=null && !currentContractorViewView.getContractorStatusId().equals("-1"))
			contractorToSave.setContractorStatusId(new Long(currentContractorViewView.getContractorStatusId()));
		
		
		if(viewRootMap.get("loginId") != null)
			contractorToSave.setLoginId((String)viewRootMap.get("loginId"));
		putNotNullFieldsInView(contractorToSave);
		putContactInfoListInView(contractorToSave);
		putContactReferenceListInView(contractorToSave);

	}
	private void putNotNullFieldsInView(ContractorView contractorToSave)
	{
		Date date = new Date();
		
		contractorToSave.setUpdatedBy((String)CommonUtil.getLoggedInUser());
		contractorToSave.setUpdatedOn(date);
		contractorToSave.setCreatedBy(CommonUtil.getLoggedInUser());
		contractorToSave.setCreatedOn(date);
			
		viewRootMap.put("saveView", contractorToSave);
	}

	
	private void putContactInfoListInView(ContractorView contractorToSave) 
	{
		//For Contact Info/////
		
		contractorToSave = (ContractorView)viewRootMap.get("saveView");	
		Set<ContactInfoView> ciViewSet=new HashSet<ContactInfoView>(0);
		List<ContactInfoView> ciList=(List<ContactInfoView>)viewRootMap.get("contactInfoList");
		ciViewSet.addAll(ciList);
		Date date = new Date();
		for(ContactInfoView contInfoView : ciViewSet )
		{
			contInfoView.setCreatedBy(CommonUtil.getLoggedInUser());
			contInfoView.setCreatedOn(date);
			contInfoView.setUpdatedBy((String)CommonUtil.getLoggedInUser());
			contInfoView.setUpdatedOn(date);
			contInfoView.setIsDeleted(new Long(0));
			contInfoView.setRecordStatus(new Long(1));
			
		}
		
		if(viewRootMap.get("cInfoToBeDeleted")!=null)
    		
		{	
			cInfoToBeDeleted=(List<ContactInfoView>)viewRootMap.get("cInfoToBeDeleted");
			for(ContactInfoView civ:cInfoToBeDeleted)
				ciViewSet.add(civ);
		}
		
		contractorToSave.setContactInfoViewSet(ciViewSet);
		viewRootMap.put("saveView", contractorToSave);
		
	}
	
	private void putContactReferenceListInView(ContractorView contractorToSave) {
		contractorToSave = (ContractorView)viewRootMap.get("saveView");
		if(viewRootMap.containsKey("refPersonList"))
		{
			List<ContactReferenceView> contactRefList=(List<ContactReferenceView>)viewRootMap.get("refPersonList");
			if(contactRefList!=null && contactRefList.size()>0)
			{
			Set<ContactReferenceView> crViewSet=new HashSet<ContactReferenceView>(0);
			crViewSet.addAll(contactRefList);
			Date date = new Date();
			for(ContactReferenceView crView : crViewSet)
			{
				crView.setCreatedBy(CommonUtil.getLoggedInUser());
				crView.setCreatedOn(date);
				crView.setUpdatedBy(CommonUtil.getLoggedInUser());
				crView.setUpdatedOn(date);
				crView.setIsDeleted(0L);
				crView.setRecordStatus(1L);
				crView.getContactInfoView().setCreatedBy(CommonUtil.getLoggedInUser());
				crView.getContactInfoView().setCreatedOn(date);
				crView.getContactInfoView().setUpdatedBy(CommonUtil.getLoggedInUser());
				crView.getContactInfoView().setUpdatedOn(date);
				crView.getContactInfoView().setIsDeleted(0L);
				crView.getContactInfoView().setRecordStatus(1L);
			}
			
    		if(viewRootMap.get("cRefToBeDeleted")!=null)
	    		
    		{	
    			cRefToBeDeleted=(List<ContactReferenceView>)viewRootMap.get("cRefToBeDeleted");
    			for(ContactReferenceView crv:cRefToBeDeleted)
    				crViewSet.add(crv);
    		}
			
			
			contractorToSave.setContactReferences(crViewSet);
			}
		}
		viewRootMap.put(WebConstants.CONTRACTOR_DETAILS, contractorToSave);
	}

	
	
	public void removeContactInfo()
	{
		ContactInfoView cInfo=new ContactInfoView(); 
		if(contactInfoDataTable.getRowCount() > 0)
		{
			cInfo = (ContactInfoView) contactInfoDataTable.getRowData();
				
			if(cInfo.getContactInfoId() == null)
				contactInfoList.remove(cInfo);
			else
			{
				cInfo.setIsDeleted(1L);
				cInfoToBeDeleted.add(cInfo);
				viewRootMap.put("cInfoToBeDeleted",cInfoToBeDeleted);
				
				for(ContactInfoView cView: contactInfoList)
				 {

					if(cView.getContactInfoId() != null)
					{
						if(cView.getContactInfoId().compareTo(cInfo.getContactInfoId()) == 0 )
						{
							contactInfoList.remove(cView);
							break;
						}
					}
				 }

			}
						
		
		}
		
		
		viewRootMap.put("contactInfoList", contactInfoList);
		contactInfoPaging(contactInfoList.size());
	}
	
	
	
	public void removeContactReference()
	{
		ContactReferenceView CRInfo=new ContactReferenceView(); 
		
		if(contactRefDataTable.getRowCount() > 0)
		{
			CRInfo = (ContactReferenceView) contactRefDataTable.getRowData();
			
			if(CRInfo.getContactReferenceId() == null)
				contactReferencesList.remove(CRInfo);
			else
			{	
				CRInfo.setIsDeleted(1L);
				cRefToBeDeleted.add(CRInfo);
				viewRootMap.put("cRefToBeDeleted",cRefToBeDeleted);
				
			 for(ContactReferenceView cView: contactReferencesList)
			 {
				if(cView.getContactReferenceId() != null)
				{
					if(cView.getContactReferenceId().compareTo(CRInfo.getContactReferenceId()) == 0 )
					{
						contactReferencesList.remove(cView);
							break;
					}
				}
			 }
			}

			
	}
		viewRootMap.put("refPersonList", contactReferencesList);
		contactRefPaging(contactReferencesList.size());
		
	}
	public void loadState(ValueChangeEvent event) {
	
		Long selectedCountry = -1L;
		try {
			selectedCountry = new Long((String) event.getNewValue());
		} catch (NumberFormatException e) {
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
	
		if (selectedCountry!=-1)
			{
				stateList=getStateList(selectedCountry);
				setCountryId(selectedCountry.toString());
			}
		
		this.setStates(stateList);
		viewRootMap.put("countryId", selectedCountry.toString());
		viewRootMap.put("states", stateList);
}


	public void loadCities(ValueChangeEvent event)
	{
		Long selectedState = -1L;
		try {
			selectedState = new Long((String) event.getNewValue());
		} catch (NumberFormatException e) {
			logger.LogException("", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		if (selectedState!=-1)
			cityList=getCityList(selectedState);
		
		this.setCities(cityList);
		viewRootMap.put("stateId", selectedState.toString());
		viewRootMap.put("cities", cityList);
	}



 	public void loadContactRefState(ValueChangeEvent event) {
	
 		Long CRcountry = -1L;
 		try {
 			CRcountry = new Long((String) event.getNewValue());
 		} catch (NumberFormatException e) {
 			logger.LogException("", e);
 			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
 		}
	
	
 		if (CRcountry!=-1)
 			{
 				CRStateList = getStateList(CRcountry);
 				setCRcountryId(CRcountry.toString());
 				
 			}
		
 		this.setCRStateList(CRStateList);
 		viewRootMap.put("CRCountryId", CRcountry.toString());
 		viewRootMap.put("CRStateList", CRStateList);
 }	


 	public void loadContactRefCity(ValueChangeEvent event) {
	
 		Long CRstate = -1L;
 		try {
 			CRstate = new Long((String) event.getNewValue());
 		} catch (NumberFormatException e) {
 			logger.LogException("", e);
 			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
 		}
	
 		if (CRstate!=-1)
 			{
 				CRCityList=getCityList(CRstate);
 				setCRstateId(CRstate.toString());
 			}
	
 		this.setCRCityList(CRCityList);
 		viewRootMap.put("CRStateId", CRstate.toString());
 		viewRootMap.put("CRCityList", CRCityList);
 }	

 	
	public void contactInfoPaging(Integer recordSize)
	{
		this.setContactInfoRecordSize(recordSize);	
		paginatorRows = getPaginatorRows();
		contactInfoPaginatorMaxPages = recordSize / paginatorRows;

		if ((recordSize % paginatorRows) > 0)
			contactInfoPaginatorMaxPages++;

		if (contactInfoPaginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			contactInfoPaginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;


	}

	
	public void contactRefPaging(Integer recordSize)
	{
		this.setContactReferenceRecordSize(recordSize);	
		paginatorRows = getPaginatorRows();
		contactReferencePaginatorMaxPages = recordSize / paginatorRows;

		if ((recordSize % paginatorRows) > 0)
			contactReferencePaginatorMaxPages++;

		if (contactReferencePaginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			contactReferencePaginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;


	}
 	
 	
 	

 	public List<SelectItem> getCRStateList() {
 		if(viewRootMap.containsKey("CRStateList"))
 			CRStateList = ( ArrayList<SelectItem> )viewRootMap.get("CRStateList");
	
 		if(CRStateList == null)
 			CRStateList = new ArrayList<SelectItem>();
	 
 		return CRStateList;
 	}

 	public void setCRStateList(List<SelectItem> stateList) {
 		CRStateList = stateList;
 		viewRootMap.put("CRStateList", CRStateList);
 	}

 	public List<SelectItem> getCRCityList() {
 		if(viewRootMap.containsKey("CRCityList"))
 			CRCityList = ( ArrayList<SelectItem> )viewRootMap.get("CRCityList");
		
 		if(CRCityList == null)
 			CRCityList = new ArrayList<SelectItem>();
	 
	 
 		return CRCityList;
 	}

 	public void setCRCityList(List<SelectItem> cityList) {
 		CRCityList = cityList;
 		viewRootMap.put("CRCityList", CRCityList);
 		}	

 
 
	public String back()
	{
		return "back";
	}
	
	
	public void close()
	{	
		viewRootMap.put("AddressViewMode", false);
		viewRootMap.put("closed", true);
	}
	
	public void contactReferenceClose()
	{	
		viewRootMap.put("isContactPersonShow",false);
		viewRootMap.put("isContactPersonClosed", true);
	}
	
	public List<String> getBusinessAcitvitiesList()
	{	if(viewRootMap.containsKey("businessAcitvitiesList"))
			businessAcitvitiesList=(List<String>)viewRootMap.get("businessAcitvitiesList");
		return businessAcitvitiesList;
	}

	public void setBusinessAcitvitiesList(List<String> businessAcitvitiesList) {
		this.businessAcitvitiesList = businessAcitvitiesList;
		viewRootMap.put("businessAcitvitiesList",this.businessAcitvitiesList);
	}
	
	
	public List<String> getServiceTypeList() {
		if(viewRootMap.containsKey("ServiceTypeList")){
			
			serviceTypeList = (List<String>)viewRootMap.get("ServiceTypeList");
		}
		return serviceTypeList;
	}

	public void setServiceTypeList(List<String> serviceTypeList) {
		this.serviceTypeList = serviceTypeList;
		viewRootMap.put("ServiceTypeList",this.serviceTypeList);
	}
	
	public Map<String, String> getLicenseSourceList() {
		if (viewRootMap.get("licenses") != null)
			licenseSourceList = (Map<String, String>) viewRootMap
					.get("licenses");

		return licenseSourceList;
	}

	public void setLicenseSourceList(Map<String, String> licenseSourceList) {
		this.licenseSourceList = licenseSourceList;
	}
	
	
	public Map<String, String> getAllBusinessActivityList()
	{
		if (viewRootMap.get("allBusinessActivityList") != null)
			allBusinessActivityList = (Map<String, String>) viewRootMap.get("allBusinessActivityList");
		return allBusinessActivityList;
	}
	
	public void setAllBusinessActivityList(
			Map<String, String> allBusinessActivityList) {
		this.allBusinessActivityList = allBusinessActivityList;
	}
	

	public Map<String, String> getAllServiceTypesList()
	{
		if (viewRootMap.get("allServiceTypesList") != null)
			allServiceTypesList = (Map<String, String>) viewRootMap.get("allServiceTypesList");
		
		if(allServiceTypesList == null)
			allServiceTypesList = new HashMap<String,String>();
		
		return allServiceTypesList;
	}
	
	public void setAllServiceTypesList(
			Map<String, String> ServiceTypesList) {
		this.allServiceTypesList = allServiceTypesList;
	}
	
	
	public String getStyleClass() {
		
		if(this.getIsViewMode())
			styleClass = "READONLY";
		else
			styleClass = "A_LEFT_SMALL";
		
		
		return styleClass;
	}


	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}


	public Boolean getIsViewMode() {
		isViewMode = false;
		if(viewRootMap.containsKey(WebConstants.PAGE_MODE))
		{
			isViewMode = (Boolean)viewRootMap.get(WebConstants.PAGE_MODE);
		}
		return isViewMode;
	}


	public void setIsViewMode(Boolean isViewMode) {
		this.isViewMode = isViewMode;
	}


	public Boolean getIsEnglishLocale() {
		isEnglishLocale = CommonUtil.getIsEnglishLocale();
		return isEnglishLocale;
	}


	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}


	public Boolean getIsArabicLocale() {
		isArabicLocale = CommonUtil.getIsArabicLocale();
		return isArabicLocale;
	}


	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}


	public ContractorView getContractorView() {
		
		if(viewRootMap.containsKey(WebConstants.CONTRACTOR_DETAILS) && viewRootMap.get(WebConstants.CONTRACTOR_DETAILS) != null)
			contractorView = (ContractorView) viewRootMap.get(WebConstants.CONTRACTOR_DETAILS);
		
		return contractorView;
	}


	public void setContractorView(ContractorView contractorView) {
		this.contractorView = contractorView;
		
		if(this.contractorView!=null)
		 viewRootMap.put(WebConstants.CONTRACTOR_DETAILS, this.contractorView);
	}
	
	
	public Boolean getIsAddressShow()
	{	if(viewRootMap.get("isAddressShow")!=null)
			isAddressShow=Convert.toBoolean(viewRootMap.get("isAddressShow"));
		
		return isAddressShow;
	}

	public void setAddressShow(Boolean isAddressShow) {
		this.isAddressShow = isAddressShow;
	}
	
	public Boolean getIsContactPersonShow()
	{
		if(viewRootMap.get("isContactPersonShow")!=null)
			isContactPersonShow=Convert.toBoolean(viewRootMap.get("isContactPersonShow"));
		
		return isContactPersonShow;
	}

	public void setContactPersonShow(Boolean isContactPersonShow) 
	{
		this.isContactPersonShow = isContactPersonShow;
	}
	
	public Boolean getIsClosed() 
	{	if(viewRootMap.get("closed")!=null)
			closed=Convert.toBoolean(viewRootMap.get("closed"));
		
	return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}
	
	public Integer getContactPersonRecordSize() {
		return contactPersonRecordSize;
	}


	public void setContactPersonRecordSize(Integer contactPersonRecordSize) {
		this.contactPersonRecordSize = contactPersonRecordSize;
	}


	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getContactReferencePaginatorMaxPages() {
		return contactReferencePaginatorMaxPages;
	}


	public void setContactReferencePaginatorMaxPages(
			Integer contactReferencePaginatorMaxPages) {
		this.contactReferencePaginatorMaxPages = contactReferencePaginatorMaxPages;
	}


	public Integer getContactInfoPaginatorMaxPages() {
		return contactInfoPaginatorMaxPages;
	}


	public void setContactInfoPaginatorMaxPages(Integer contactInfoPaginatorMaxPages) {
		this.contactInfoPaginatorMaxPages = contactInfoPaginatorMaxPages;
	}
	
	public void setContactInfoList(List<ContactInfoView> contactInfoList) {
		this.contactInfoList = contactInfoList;
	}

	public List<ContactInfoView> getContactInfoList()
	{ if(viewRootMap.containsKey("contactInfoList"))
		contactInfoList=(List<ContactInfoView>)viewRootMap.get("contactInfoList");
		return contactInfoList;
	}
	
	
	
	public List<ContactReferenceView> getContactReferencesList()
	{	if(viewRootMap.containsKey("refPersonList"))
		contactReferencesList=(List<ContactReferenceView>)viewRootMap.get("refPersonList");
		return contactReferencesList;
	}

	public void setContactReferencesList(List<ContactReferenceView> referencedPersonList) {
		this.contactReferencesList = referencedPersonList;
	}



	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}



	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}



	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}



	public HtmlDataTable getContactInfoDataTable() {
		return contactInfoDataTable;
	}



	public void setContactInfoDataTable(HtmlDataTable contactInfoDataTable) {
		this.contactInfoDataTable = contactInfoDataTable;
	}



	public HtmlDataTable getContactRefDataTable() {
		return contactRefDataTable;
	}



	public void setContactRefDataTable(HtmlDataTable contactRefDataTable) {
		this.contactRefDataTable = contactRefDataTable;
	}	
	
	
	public List<SelectItem> getCityList(Long stateId) {
		
		try {
			Set<RegionView> regionViewList = pSAgent.getCity(stateId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				cityList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return cityList;
	}
	


	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}


	
	public List<SelectItem> getStateList(Long countryId) {
		
		try {
			Set<RegionView> regionViewList = pSAgent.getState(countryId);
			for (RegionView regionView : regionViewList) {
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel(regionView.getRegionName());
				selectItem.setValue(regionView.getRegionId().toString());
				stateList.add(selectItem);
			}
		} catch (Exception e) {
			logger.LogException("", e);
		}
		return stateList;
	}



	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}
		
	
	
	
	public String getCountryId()
	{
		if(viewRootMap.containsKey("countryId"))
			countryId=viewRootMap.get("countryId").toString();
		return countryId;
	}



	public void setCountryId(String countryId) 
	{
		this.countryId = countryId;
		if(this.countryId != null)
			viewRootMap.put("countryId", this.countryId);

	}



	public String getStateId()
	{
		if(viewRootMap.containsKey("stateId"))
			stateId=viewRootMap.get("stateId").toString();
		return stateId;
	}



	public void setStateId(String stateId) 
	{
		this.stateId = stateId;
		if(this.stateId != null)
			viewRootMap.put("stateId",this.stateId);
			
	}



	public String getCityId() 
	{
		if(viewRootMap.containsKey("cityId"))
			cityId=viewRootMap.get("cityId").toString();
		
		return cityId;
	}



	public void setCityId(String cityId)
	{
		this.cityId = cityId;
		if(this.cityId != null)
			viewRootMap.put("cityId",this.cityId);
			

	}
	

	
	public List<SelectItem> getStates() {
		states = (List<SelectItem>) viewRootMap.get("states");
		if (states == null)
			states = new ArrayList<SelectItem>();
		return states;
	}

	public void setStates(List<SelectItem> states) {
		
		this.states = states;
		if(this.states != null)
			viewRootMap.put("states", stateList);
	}




	public List<SelectItem> getCities() {
		cities = (List<SelectItem>) viewRootMap.get("cities");
		if (cities == null)
			cities = new ArrayList<SelectItem>();
		return cities;
	}

	public void setCities(List<SelectItem> cities) {
		this.cities = cities;
		if(this.cities != null)
			viewRootMap.put("cities", cityList);
	}

	
	
	public ContactInfoView getContactInfoView() 
	{	if(viewRootMap.get("contactInfoView")!=null)
		contactInfoView=(ContactInfoView)viewRootMap.get("contactInfoView");
		return contactInfoView;
	}

	public void setContactInfoView(ContactInfoView contactInfoView) {
		this.contactInfoView = contactInfoView;
		if(this.contactInfoView!=null)
			viewRootMap.put("contactInfoView",this.contactInfoView);
	}
	
	
	public ContactReferenceView getContactReferenceView() 
	{	if(viewRootMap.get("contactReferenceView")!=null)
		contactReferenceView=(ContactReferenceView)viewRootMap.get("contactReferenceView");
		return contactReferenceView;
	}

	public void setContactReferenceView(ContactReferenceView contactReferenceView) {
		this.contactReferenceView = contactReferenceView;
		if(this.contactReferenceView != null)
			viewRootMap.put("contactReferenceView",contactReferenceView);
	}
	
	
	public String getCRcountryId() {
		if(viewRootMap.containsKey("CRCountryId"))
			CRcountryId=viewRootMap.get("CRCountryId").toString();
		
		return CRcountryId;
	}
	public void setCRcountryId(String rcountryId) {
		CRcountryId = rcountryId;
		if(CRcountryId != null)
			viewRootMap.put("CRCountryId", CRcountryId);
			
	}

	public String getCRstateId() {
		
		if(viewRootMap.containsKey("CRStateId"))
			CRstateId=viewRootMap.get("CRStateId").toString();
		return CRstateId;
	}

	public void setCRstateId(String CrstateId) {
		CRstateId = CrstateId;
		if(CRstateId != null)
			viewRootMap.put("CRStateId", CrstateId);
			
	}

	public String getCRCityId() {
		if(viewRootMap.containsKey("CRCityId"))
			CRCityId=viewRootMap.get("CRCityId").toString();
		return CRCityId;
	}

	public void setCRCityId(String cityId) {
		CRCityId = cityId;
		if(CRCityId != null)
			viewRootMap.put("CRCityId", CRCityId);
	}
	
	public String getCrTitleId() {
		return crTitleId;
	}

	public void setCrTitleId(String crTitleId) {
		this.crTitleId = crTitleId;
	}



	public String getCrAddress1() {
		return crAddress1;
	}



	public void setCrAddress1(String crAddress1) {
		this.crAddress1 = crAddress1;
	}



	public String getCrAddress2() {
		return crAddress2;
	}



	public void setCrAddress2(String crAddress2) {
		this.crAddress2 = crAddress2;
	}



	public String getCrDesignation() {
		return crDesignation;
	}



	public void setCrDesignation(String crDesignation) {
		this.crDesignation = crDesignation;
	}



	public String getCrStreet() {
		return crStreet;
	}



	public void setCrStreet(String crStreet) {
		this.crStreet = crStreet;
	}



	public String getCrPostCode() {
		return crPostCode;
	}



	public void setCrPostCode(String crPostCode) {
		this.crPostCode = crPostCode;
	}



	public String getCrHomePhone() {
		return crHomePhone;
	}



	public void setCrHomePhone(String crHomePhone) {
		this.crHomePhone = crHomePhone;
	}



	public String getCrOfficePhone() {
		return crOfficePhone;
	}



	public void setCrOfficePhone(String crOfficePhone) {
		this.crOfficePhone = crOfficePhone;
	}



	public String getCrMobileNumber() {
		return crMobileNumber;
	}



	public void setCrMobileNumber(String crMobileNumber) {
		this.crMobileNumber = crMobileNumber;
	}



	public String getCrFax() {
		return crFax;
	}



	public void setCrFax(String crFax) {
		this.crFax = crFax;
	}



	public String getCrEmail() {
		return crEmail;
	}



	public void setCrEmail(String crEmail) {
		this.crEmail = crEmail;
	}



	public Boolean getIsContactPersonClosed() {
		if(viewRootMap.get("isContactPersonClosed")!=null)
			isContactPersonClosed = Convert.toBoolean(viewRootMap.get("isContactPersonClosed"));
		return isContactPersonClosed;     
	}

	

	public void setIsContactPersonClosed(Boolean isContactPersonClosed) {
		this.isContactPersonClosed = isContactPersonClosed;
	}



	public String getStrContractorStatusId() {
		return strContractorStatusId;
	}



	public void setStrContractorStatusId(String strContractorStatusId) {
		this.strContractorStatusId = strContractorStatusId;
	}
	
	
	public List<ContactReferenceView> getCRefToBeDeleted()
	{	if(viewRootMap.containsKey("cRefToBeDeleted") && viewRootMap.get("cRefToBeDeleted")!=null)
		{
		cRefToBeDeleted=(List<ContactReferenceView>)viewRootMap.get("cRefToBeDeleted");
		}
		return cRefToBeDeleted;
	}

	public void setCRefToBeDeleted(List<ContactReferenceView> cRefToBeDeleted) {
		this.cRefToBeDeleted = cRefToBeDeleted;
	}

	public List<ContactInfoView> getCInfoToBeDeleted()
	{
		if(viewRootMap.containsKey("cInfoToBeDeleted") && viewRootMap.get("cInfoToBeDeleted")!=null)
		{
			cInfoToBeDeleted=(List<ContactInfoView>)viewRootMap.get("cInfoToBeDeleted");
		}
		return cInfoToBeDeleted;
	}

	public void setCInfoToBeDeleted(List<ContactInfoView> cInfoToBeDeleted) {
		this.cInfoToBeDeleted = cInfoToBeDeleted;
	}
	
	public Integer getContactInfoRecordSize() {
		if (viewRootMap.get("recordSize") != null)
			contactInfoRecordSize = (Integer) viewRootMap.get("recordSize");

		return contactInfoRecordSize == null ? 0 : contactInfoRecordSize;
	}

	public void setContactInfoRecordSize(Integer contactInfoRecordSize) {
		this.contactInfoRecordSize = contactInfoRecordSize;
		if(this.contactInfoRecordSize != null)
			viewRootMap.put("recordSize", this.contactInfoRecordSize);
	}




	public Integer getContactReferenceRecordSize() {
		return contactReferenceRecordSize;
	}


	

	public void setContactReferenceRecordSize(Integer contactReferenceRecordSize) {
		this.contactReferenceRecordSize = contactReferenceRecordSize;
	}
	
	public boolean getIsViewModePopUp() {
		return viewRootMap.get(PAGE_MODE) != null
				&& viewRootMap.get(PAGE_MODE).toString().trim()
						.equalsIgnoreCase(POPUP_MODE);
		
		
	}
	
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	
	public void generateLoginId() throws PimsBusinessException
	{
		ContractorView contractor=(ContractorView)viewRootMap.get("refView");
		try
		{
		
		if(contractor.getPersonId() != null)
		{
			UserDbImpl user = new UserDbImpl();
			
			user.setLoginId(contractor.getPersonId().toString());
			user.setFullName(contractor.getCommercialName());
			if(this.getIsEnglishLocale())
				user.setFirstName(contractor.getContractorNameEn());
			else
				user.setFirstName(contractor.getContractorNameAr());
			user.setPolicyId("admin");
			user.setStatusId("0");
			user.setSystemUser(true);	
				
			
			Set<ContactInfoView> tempList = new HashSet<ContactInfoView>();
			tempList = contractor.getContactInfoViewSet();
			String firstEmail = "";
			for(ContactInfoView cInfo : tempList)
			{
				if(cInfo.getEmail() != null)
				{
					firstEmail = cInfo.getEmail();
					break;
				}
				
			}
			
			user.setEmailUrl(firstEmail);
			
			Random randomNumer = new Random();
			Integer pin = randomNumer.nextInt(90000)+10000;
			user.setPassword(pin.toString());
			
			SecurityManager.persistUser(user);
			
			//adding user group
			
			SecUserGrp internetUserGrp = getUserGroup();
			if(internetUserGrp != null)
			{
				deleteGroupBinding(user.getLoginId(), internetUserGrp.getUserGroup().getUserGroupId());
				SecUserUserGroupId complexKey = new SecUserUserGroupId();
				GroupUserBinding groupUserBinding = new GroupUserBinding();
				complexKey.setUserGroupId(internetUserGrp.getUserGroup().getUserGroupId());
				complexKey.setLoginId(user.getLoginId());
				groupUserBinding.setUserGroupBinding(complexKey);
				SecurityManager.persistBinding(groupUserBinding);
				
			}
			
			viewMap.put("loginId", user.getLoginId());
			contractor.setLoginId(user.getLoginId());
			save();
			
			//makeEntryInOID();
			sendActivationEmail(user,contractor);
		} //if(person.getPersonId() != null)
		}//try
		catch(Exception e)
		{	
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
				
	}
	
	private SecUserGrp getUserGroup()
	{
		Search query = new Search();
		query.clear();
		query.addFrom(UserGroup.class);
		List<UserGroup> allUserGroups =  SecurityManager.findGroup(query);
		List<UserGroup> userGroups = null;
		HashMap<String, UserGroup> userGroupsMap = new HashMap<String, UserGroup>(0);		
		SecUserGrp secUserGroup = new SecUserGrp();
		
		for (UserGroup ug : allUserGroups) 
		{
			if(ug.getUserGroupId().compareTo(WebConstants.UserGroups.INTERNET_USERS_GROUP) == 0)
			{
				
				secUserGroup.setUserGroup(ug);
				return secUserGroup;
				
			}
		}
		return null;
		
	}
	
	private void deleteGroupBinding(String loginId, 
			String userGroupId) {

		GroupUserBinding groupUserBinding = null;
		try{	
			groupUserBinding = EntityManager.getBroker().findById(GroupUserBinding.class, new SecUserUserGroupId(userGroupId, loginId));
			if(groupUserBinding!=null)
				EntityManager.getBroker().delete(groupUserBinding);
		}
		catch(Throwable t){
			t.printStackTrace();
		}
	}
	
	
	private void makeEntryInOID()
	{
		
	}
	
	private void sendActivationEmail(UserDbImpl user, ContractorView contractor)
	{
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		if (user != null)
			contactInfoList.add(new ContactInfo(contractor.getPersonId().toString(), contractor.getCellNumber(), null, user.getEmailUrl()));
		String link = ResourceUtil.getInstance().getProperty(MessageConstants.ACTIVATION_LINK);
		String encryptedUID = new Cryptographer().encrypt(contractor.getPersonId().toString());
		link += encryptedUID;
		String type = "&T=";
		link += type;
		link += new Cryptographer().encrypt("contractor");
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		eventAttributesValueMap.put("USER_NAME", user.getFirstName());
		eventAttributesValueMap.put("PIN", user.getPassword());
		eventAttributesValueMap.put("ACTIVATION_LINK", link);
		generateNotification(WebConstants.Notification_MetaEvents.Event_Activation_Email, contactInfoList, eventAttributesValueMap,null);
	}
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		final String METHOD_NAME = "generateNotification()";
		boolean success = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
		
		return success;
	}

	public Boolean getIsGenerateLogin()
	{
		ContractorView contractor=(ContractorView)viewRootMap.get(WebConstants.CONTRACTOR_DETAILS);
		if(contractor != null)
		{
			if(contractor.getPersonId() != null && contractor.getLoginId() == null)
				return true;
		}
		return false;
		
	}


}
