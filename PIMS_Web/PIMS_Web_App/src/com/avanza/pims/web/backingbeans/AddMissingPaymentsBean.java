package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.ui.util.ResourceUtil;

public class AddMissingPaymentsBean extends AbstractController
{
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger( AddMissingPaymentsBean.class );
	@SuppressWarnings("unchecked")
	private Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();;
	@SuppressWarnings("unchecked")
	private List<String> errorMessages; 
	private List<String> successMessages;
	private Map sessionMap =FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private List<BounceChequeView> bounceChequeList = new ArrayList<BounceChequeView>();
	private BounceChequeView bounceChequeView = new BounceChequeView();
	private HtmlDataTable dataTable ;
	
	private final String CONTRACT_VIEW = "CONTRACT_VIEW";
	private final String BOUNCE_CHEQUE_VIEW = "BOUNCE_CHEQUE_VIEW";
	private final String IS_CHEQUE = "IS_CHEQUE";
	private final String IS_BANK_TRASNFER = "IS_BANK_TRASNFER";
	private final String BANK_VIEW_LIST = "BANK_VIEW_LIST";
	private final String BOUNCED_CHEQUE_VIEW = "BOUNCED_CHEQUE_VIEW";
	private final String PAYMENT_TYPE_LIST = "PAYMENT_TYPE_LIST";
	private final String PAYMENT_MODE_DD = "PAYMENT_MODE_DD";
	private final String PAYMENT_SCH_STATUS_DD = "PAYMENT_SCH_STATUS_DD";
	private final String CHEQUE_TYPE_DD = "CHEQUE_TYPE_DD";
	private final String BOUNCE_CHEQUE_VIEW_LIST = "BOUNCE_CHEQUE_VIEW_LIST";
	private final String DIRCTION_TYPE_INBOUND = "DIRCTION_TYPE_INBOUND";
	
	
	
//	private final String PAYMENT_MODE_LIST_DD = "PAYMENT_MODE_LIST_DD";
	
	//fields for controls start
	private String paymentType;
	private String paymentMode;
	private Date paymentDueDate;//user extract it from GRP
	private String receiptNumber;
	private String  paymentAmount;
	private String paymentStatus;
	private String grpReceiptId;
	private String bankName;
	private String chequeOrTransferNumber;
	private String trxNumber;
	private String accountNumber;
	private String chequeType;
	private String description;
	//fields for controls end
	
	private boolean isCheque;
	private boolean isBankTransfer;
	private ContractView contractView = new ContractView();
	private List<DomainDataView> paymentModeDDList = new ArrayList<DomainDataView>();
	private List<PaymentTypeView> paymentTypeList= new ArrayList<PaymentTypeView>();
	private List<DomainDataView> paymentSchStatusDDList = new ArrayList<DomainDataView>();
	private List<DomainDataView> chequeTypeDDList = new ArrayList<DomainDataView>();
	
	private DomainDataView paymentModeChequeDD = new DomainDataView();
	private DomainDataView paymentModeBTransferDD = new DomainDataView();
	private HtmlSelectOneMenu htmlPaymentMode = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlPaymentType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlPaymentStatus = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlBankName = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlChequeType = new HtmlSelectOneMenu();
	private List<BankView> bankList = new ArrayList<BankView>();
	
	private DomainDataView directionTypeInbound = new DomainDataView();
	
	public AddMissingPaymentsBean()
	{
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		try
		{
		
		if(!isPostBack())
		{
			paymentModeDDList = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
			paymentTypeList = new UtilityServiceAgent().getAllPaymentTypes();
			paymentSchStatusDDList = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);
			directionTypeInbound = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.DIRECTION_TYPE),WebConstants.DIRECTION_TYPE_INBOUND);
			chequeTypeDDList = CommonUtil.getDomainDataListForDomainType(WebConstants.CHEQUE_TYPE);
			
			paymentModeChequeDD = (DomainDataView) CommonUtil.getIdFromType(paymentModeDDList, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
			paymentModeBTransferDD = (DomainDataView) CommonUtil.getIdFromType(paymentModeDDList, WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);
			
			if( sessionMap.containsKey(CONTRACT_VIEW) && sessionMap.get(CONTRACT_VIEW) != null)
			{
				viewMap.put(CONTRACT_VIEW, sessionMap.get(CONTRACT_VIEW));
				sessionMap.remove(CONTRACT_VIEW);
			}
			
			try 
			{
				bankList = new PropertyServiceAgent().getAllBank();
			} 
			catch (PimsBusinessException e) 
			{
				logger.LogException("init method crashed due to : ", e);
			}
			setValuesToMap();
		}
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("init crashed | ",e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getValuesFromMap()
	{
		if( viewMap.get(CONTRACT_VIEW) != null)
			contractView =	(ContractView) viewMap.get(CONTRACT_VIEW);
		
		if(viewMap.get(BOUNCE_CHEQUE_VIEW) != null)
			bounceChequeView = (BounceChequeView) viewMap.get(BOUNCE_CHEQUE_VIEW);
		
//		if(viewMap.get(PAYMENT_MODE_LIST_DD) != null)
//			paymentModeDDList = (List<DomainDataView>) viewMap.get(PAYMENT_MODE_LIST_DD); 
		
		if(viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE) != null)
			paymentModeChequeDD = (DomainDataView) viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
			
		if(viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER) != null)
			paymentModeBTransferDD = (DomainDataView) viewMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);
		
		if(viewMap.get(BANK_VIEW_LIST) != null)
			bankList = (List<BankView>) viewMap.get(BANK_VIEW_LIST) ;
		
		if(viewMap.get(BOUNCED_CHEQUE_VIEW) != null)
			bounceChequeView = (BounceChequeView) viewMap.get(BOUNCED_CHEQUE_VIEW); 
		
		if(viewMap.get(PAYMENT_TYPE_LIST) != null)
			paymentTypeList = (List<PaymentTypeView>) viewMap.get(PAYMENT_TYPE_LIST);
		
		if(viewMap.get(PAYMENT_MODE_DD) != null)
			paymentModeDDList = (List<DomainDataView>) viewMap.get(PAYMENT_MODE_DD);
		
		if(viewMap.get(PAYMENT_SCH_STATUS_DD) != null)
			paymentSchStatusDDList = (List<DomainDataView>) viewMap.get(PAYMENT_SCH_STATUS_DD) ;
		
		if(viewMap.get(CHEQUE_TYPE_DD) != null)
			chequeTypeDDList = (List<DomainDataView>) viewMap.get(CHEQUE_TYPE_DD);
		
		if(viewMap.get(BOUNCE_CHEQUE_VIEW_LIST) != null)
			bounceChequeList = (List<BounceChequeView>) viewMap.get(BOUNCE_CHEQUE_VIEW_LIST);
		
		if(viewMap.get(DIRCTION_TYPE_INBOUND) != null)
			directionTypeInbound = (DomainDataView) viewMap.get(DIRCTION_TYPE_INBOUND); 
	}
	@SuppressWarnings("unchecked")
	public void setValuesToMap()
	{
		if(bounceChequeView != null )
			viewMap.put(BOUNCE_CHEQUE_VIEW, bounceChequeView);
		
//		if(paymentModeDDList != null && paymentModeDDList.size() > 0)
//			viewMap.put(PAYMENT_MODE_LIST_DD, paymentModeDDList);
		
		if(directionTypeInbound != null && directionTypeInbound.getDomainDataId() != null) 
			viewMap.put(DIRCTION_TYPE_INBOUND, directionTypeInbound);
		
		if(paymentModeChequeDD != null && paymentModeChequeDD.getDomainDataId() != null)
			viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE, paymentModeChequeDD);
		
		if(paymentModeBTransferDD != null && paymentModeBTransferDD.getDomainDataId() != null)
			viewMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER, paymentModeBTransferDD);
		
		if(contractView != null && contractView.getContractId() != null)
			viewMap.put(CONTRACT_VIEW, contractView);
		
		if(bankList != null && bankList.size() > 0)
			viewMap.put(BANK_VIEW_LIST, bankList);
		
		if(bounceChequeView != null && bounceChequeView.isFilled())
			viewMap.put(BOUNCED_CHEQUE_VIEW, bounceChequeView);
		
		if(paymentTypeList != null && paymentTypeList.size() > 0)
			viewMap.put(PAYMENT_TYPE_LIST, paymentTypeList);
		
		if(paymentModeDDList != null && paymentModeDDList.size() > 0)
			viewMap.put(PAYMENT_MODE_DD, paymentModeDDList);
		
		if(paymentSchStatusDDList != null && paymentSchStatusDDList.size() > 0)
			viewMap.put(PAYMENT_SCH_STATUS_DD, paymentSchStatusDDList);
		
		if(chequeTypeDDList != null && chequeTypeDDList.size() > 0)
			viewMap.put(CHEQUE_TYPE_DD, chequeTypeDDList);
		
		if(bounceChequeList != null && bounceChequeList.size() > 0)
			viewMap.put(BOUNCE_CHEQUE_VIEW_LIST, bounceChequeList);
	}

	@Override
	public void prerender()
	{
		super.prerender();
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public List<BounceChequeView> getBounceChequeList() {
		return bounceChequeList;
	}
	public void setBounceChequeList(List<BounceChequeView> bounceChequeList) {
		this.bounceChequeList = bounceChequeList;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public Integer getRecordSize() {
		return recordSize;
	}
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	public String btnViewContractClick()
	{
		
		String methodName="btnViewContractClick";
		logger.logInfo(methodName+"|"+"Start..");
		ContractView contractView = new ContractView(); 
		
		if(viewMap.get("CONTRACT_VIEW") != null)
			contractView = (ContractView) viewMap.get("CONTRACT_VIEW"); 
			
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		
		String javaScriptText="var screen_width = 1024;"+
        "var screen_height = 450;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=10,scrollbars=no,status=yes');";
        openPopUp("LeaseContract.jsf",javaScriptText);
		
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        
		logger.logInfo(methodName+"|"+"Start..");
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-10)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
        addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);
        

		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	private boolean hasErrors() 
	{
		errorMessages = new ArrayList<String>();
		boolean hasError = false;
		getValuesFromMap();
		
		if(getDescription() == null || getDescription().length() <= 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideDescription"));
			hasError  = true;
		}
		
		if(htmlPaymentType.getValue() == null || htmlPaymentType.getValue().toString().equals("-1"))
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectPaymentType"));
			hasError = true;
		}
		
		if(htmlPaymentMode.getValue() ==null || htmlPaymentMode.getValue().toString().equals("-1"))
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectPaymentMethod"));
			hasError = true;
		}
		if(htmlPaymentMode.getValue().toString().equals(paymentModeChequeDD.getDomainDataId().toString()))
		{
			setCheque(true);
			setBankTransfer(false);
			if(htmlBankName.getValue().toString().equals("-1"))
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectBank"));
				hasError  = true;
			}
			
			if(getChequeOrTransferNumber() == null || getChequeOrTransferNumber().length() <= 0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideChequeNTransfer"));
				hasError  = true;
			}
//			Made optional as directed by Mohammad
//			if(getAccountNumber() == null || getAccountNumber().length() <= 0)
//			{
//				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideAccountNumber"));
//				hasError  = true;
//			}	
			
		}
		else if(htmlPaymentMode.getValue().toString().equals(paymentModeBTransferDD.getDomainDataId().toString()))
		{
			setCheque(false);
			setBankTransfer(true);
			if(htmlBankName.getValue().toString().equals("-1"))
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectBank"));
				hasError  = true;
			}
			
			if(getChequeOrTransferNumber() == null || getChequeOrTransferNumber().length() <= 0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideChequeNTransfer"));
				hasError  = true;
			}
		}
		try
		{
			if(getPaymentAmount() == null || getPaymentAmount().toString().length() <= 0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideAmount"));
				hasError  = true;
			}
			else if(Double.parseDouble(getPaymentAmount().toString()) <= 0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideCorrectAmount"));
				hasError  = true;
			}
				
		}
		catch (NumberFormatException e)
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideCorrectAmount"));
			hasError  = true;
		}
		
		try
		{
			if(getGrpReceiptId() != null && getGrpReceiptId().trim().length() > 0)
			{
				Long grpRcptId = Long.parseLong(getGrpReceiptId().toString());
				hasError  = false;
			}
				
		}
		catch (NumberFormatException e)
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideCorrectgrpReceiptId"));
			hasError  = true;
		}
		
		
		if(htmlPaymentStatus.getValue() == null || htmlPaymentStatus.getValue().toString().equals("-1"))
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectPaymentStatus"));
			hasError  = true;
		}
		
		if(getReceiptNumber() == null || getReceiptNumber().length() <= 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideGrpReceiptNumber"));
			hasError  = true;
		}
		
		if(paymentDueDate == null)
		{
			errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.provideDate"));
			hasError  = true;
		}
		setValuesToMap();
		return hasError;
	}
	public BounceChequeView getBounceChequeView() {
		return bounceChequeView;
	}
	public void setBounceChequeView(BounceChequeView bounceChequeView) {
		this.bounceChequeView = bounceChequeView;
	}
	public String getPaymentType() 
	{
		return paymentType;
	}
	public void setPaymentType(String paymentType) 
	{
		this.paymentType = paymentType;
	}
	public String getPaymentMode()
	{
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getGrpReceiptId() {
		return grpReceiptId;
	}
	public void setGrpReceiptId(String grpReceiptId) {
		this.grpReceiptId = grpReceiptId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeOrTransferNumber() {
		return chequeOrTransferNumber;
	}
	public void setChequeOrTransferNumber(String chequeOrTransferNumber) {
		this.chequeOrTransferNumber = chequeOrTransferNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getChequeType() {
		return chequeType;
	}
	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}
	public void paymentModeChanged()
	{
		getValuesFromMap();
		if(htmlPaymentMode.getValue() != null && htmlPaymentMode.getValue().toString().equals(paymentModeChequeDD.getDomainDataId().toString()))
		{
			setCheque(true);
			setBankTransfer(false);
		}
		else if(htmlPaymentMode.getValue() != null && htmlPaymentMode.getValue().toString().equals(paymentModeBTransferDD.getDomainDataId().toString()))
		{
			setCheque(false);
			setBankTransfer(true);
		}
		else
		{
			setCheque(false);
			setBankTransfer(false);
		}
			
	}
	public boolean isCheque() 
	{
		if(viewMap.get(IS_CHEQUE) != null)
			isCheque = (Boolean) viewMap.get(IS_CHEQUE);
		return isCheque;
	}
	@SuppressWarnings("unchecked")
	public void setCheque(boolean isCheque) 
	{
		viewMap.put(IS_CHEQUE,isCheque);
		this.isCheque = isCheque;
	}
	public boolean isBankTransfer() 
	{
		if(viewMap.get(IS_BANK_TRASNFER) != null)
			isBankTransfer = (Boolean) viewMap.get(IS_BANK_TRASNFER);
		return isBankTransfer;
	}
	@SuppressWarnings("unchecked")
	public void setBankTransfer(boolean isBankTransfer) 
	{
		viewMap.put(IS_BANK_TRASNFER,isBankTransfer);
		this.isBankTransfer = isBankTransfer;
	}
	public List<DomainDataView> getPaymentModeDDList() {
		return paymentModeDDList;
	}
	public void setPaymentModeDDList(List<DomainDataView> paymentModeDDList) {
		this.paymentModeDDList = paymentModeDDList;
	}
	public ContractView getContractView() {
		return contractView;
	}
	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}
	public DomainDataView getPaymentModeChequeDD() {
		return paymentModeChequeDD;
	}
	public void setPaymentModeChequeDD(DomainDataView paymentModeChequeDD) {
		this.paymentModeChequeDD = paymentModeChequeDD;
	}
	public DomainDataView getPaymentModeBTransferDD() {
		return paymentModeBTransferDD;
	}
	public void setPaymentModeBTransferDD(DomainDataView paymentModeBTransferDD) {
		this.paymentModeBTransferDD = paymentModeBTransferDD;
	}
	public HtmlSelectOneMenu getHtmlPaymentMode() {
		return htmlPaymentMode;
	}
	public void setHtmlPaymentMode(HtmlSelectOneMenu htmlPaymentMode) {
		this.htmlPaymentMode = htmlPaymentMode;
	}
	@SuppressWarnings("unchecked")
	public void btnAddPaymentContractClick()
	{
		getValuesFromMap();
		DomainDataView ddv = new DomainDataView();
		PaymentTypeView pmntVU= new PaymentTypeView();
		BounceChequeView bchequeVU = new BounceChequeView();
		try 
		{
				if(!hasErrors())
				{
						bchequeVU.setPaymentTypeId(Long.parseLong(htmlPaymentType.getValue().toString()));
						pmntVU = getPaymentTypeById(Long.parseLong(htmlPaymentType.getValue().toString()), paymentTypeList);
						bchequeVU.setPaymentTypeDescriptionEn(pmntVU.getDescriptionEn());
						bchequeVU.setPaymentTypeDescriptionAr(pmntVU.getDescriptionAr());
						
						bchequeVU.setPaymentModeId(Long.parseLong(htmlPaymentMode.getValue().toString()));
						bchequeVU.setGrpMethodId(getPaymentMehtodIdResembleToDDId(bchequeVU.getPaymentModeId()));
						ddv = getDomainDataById(bchequeVU.getPaymentModeId(),paymentModeDDList);
						bchequeVU.setPaymentModeEn(ddv.getDataDescEn());
						bchequeVU.setPaymentModeAr(ddv.getDataDescAr());
						
						bchequeVU.setPaymentDueOn(getPaymentDueDate());
						
						bchequeVU.setStatusId(Long.parseLong(htmlPaymentStatus.getValue().toString()));
						ddv = getDomainDataById(Long.parseLong(htmlPaymentStatus.getValue().toString()), paymentSchStatusDDList);
						bchequeVU.setStatusEn(ddv.getDataDescEn());
						bchequeVU.setStatusAr(ddv.getDataDescAr());
						
						bchequeVU.setAmount(Double.parseDouble(getPaymentAmount()));
						bchequeVU.setReceiptNumber(getReceiptNumber());
						bchequeVU.setGrpReceiptId(getGrpReceiptId());
						
						bchequeVU.setDirectionTypeId(directionTypeInbound.getDomainDataId());
						if(htmlPaymentMode.getValue().toString().equals(paymentModeChequeDD.getDomainDataId().toString()) || htmlPaymentMode.getValue().toString().equals(paymentModeBTransferDD.getDomainDataId().toString()) )
						{
							bchequeVU.setBankId(Long.parseLong(htmlBankName.getValue().toString()));
							bchequeVU.setBankNameEn(getBankNameById(bchequeVU.getBankId()));
							bchequeVU.setBankNameAr(getBankNameById(bchequeVU.getBankId()));
							bchequeVU.setChequeNumber(getChequeOrTransferNumber());
						}
						
						if(htmlPaymentMode.getValue().toString().equals(paymentModeChequeDD.getDomainDataId().toString()))
						{
							bchequeVU.setChequeTypeId(Long.parseLong(htmlChequeType.getValue().toString()));
							ddv = getDomainDataById(Long.parseLong(htmlChequeType.getValue().toString()), chequeTypeDDList);
							
							bchequeVU.setChequeTypeEn(ddv.getDataDescEn());
							bchequeVU.setChequeTypeAr(ddv.getDataDescAr());
							bchequeVU.setAccountNumber(getAccountNumber());
							bchequeVU.setCheque(true);
							bchequeVU.setBankTransfer(false);
						}
						else if(htmlPaymentMode.getValue().toString().equals(paymentModeBTransferDD.getDomainDataId().toString()))
						{
							bchequeVU.setCheque(false);
							bchequeVU.setBankTransfer(true);
						}
						bchequeVU.setDescription(getDescription());
					
						bchequeVU.setOwnerShipTypeId(new PropertyService().getOwnershipTypeIdByContractId(contractView.getContractId()));
						bchequeVU.setContractView(contractView);
						bchequeVU.setLoggedInUser(CommonUtil.getLoggedInUser());
						bchequeVU.setGrpTrxNumber( getTrxNumber().trim() );
						new PropertyService().persistMigratedPayments(bchequeVU);
						
						successMessages = new ArrayList<String>();
						successMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.succesMsg.paymentAdded"));
						
						if(viewMap.get(BOUNCE_CHEQUE_VIEW_LIST) != null)
							bounceChequeList = (List<BounceChequeView>) viewMap.get(BOUNCE_CHEQUE_VIEW_LIST);
						bounceChequeList.add(bchequeVU);
						setValuesToMap();
						clearFields();
		
					
			} 
		}

			 catch (Exception e) 
			 {
					logger.LogException("btnAddPaymentContractClick crashed due to : ", e);
					errorMessages = new ArrayList<String>(0);
		    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    		
				}
		}
	
	public void clearFields() 
	{
		htmlPaymentType.setValue(null);
		htmlBankName.setValue(null);
		htmlChequeType.setValue(null);
		htmlPaymentMode.setValue(null);
		htmlPaymentStatus.setValue(null);
		setPaymentDueDate(null);
        setTrxNumber("");
		setReceiptNumber("");
		setPaymentAmount("");
		setGrpReceiptId("");
		setDescription("");
		setAccountNumber("");
		setChequeOrTransferNumber("");
	}
	public HtmlSelectOneMenu getHtmlPaymentType() {
		return htmlPaymentType;
	}
	public void setHtmlPaymentType(HtmlSelectOneMenu htmlPaymentType) {
		this.htmlPaymentType = htmlPaymentType;
	}
	public HtmlSelectOneMenu getHtmlPaymentStatus() {
		return htmlPaymentStatus;
	}
	public void setHtmlPaymentStatus(HtmlSelectOneMenu htmlPaymentStatus) {
		this.htmlPaymentStatus = htmlPaymentStatus;
	}
	public HtmlSelectOneMenu getHtmlBankName() {
		return htmlBankName;
	}
	public void setHtmlBankName(HtmlSelectOneMenu htmlBankName) {
		this.htmlBankName = htmlBankName;
	}
	public HtmlSelectOneMenu getHtmlChequeType() {
		return htmlChequeType;
	}
	public void setHtmlChequeType(HtmlSelectOneMenu htmlChequeType) {
		this.htmlChequeType = htmlChequeType;
	}
	public String getBankNameById(Long bankId)
	{
		getValuesFromMap();
		String bankName="";
		if(bankList != null && bankList.size() > 0)
		{
			for(BankView bank : bankList)
			{
				if(bank.getBankId().compareTo(bankId)==0)
				{
					if(CommonUtil.getIsEnglishLocale())
						bankName = bank.getBankEn();
					else
						bankName = bank.getBankAr();
					break;
				}
			}
		}
		return bankName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<DomainDataView> getPaymentSchStatusDDList() {
		return paymentSchStatusDDList;
	}
	public void setPaymentSchStatusDDList(
			List<DomainDataView> paymentSchStatusDDList) {
		this.paymentSchStatusDDList = paymentSchStatusDDList;
	}
	public List<DomainDataView> getChequeTypeDDList() {
		return chequeTypeDDList;
	}
	public void setChequeTypeDDList(List<DomainDataView> chequeTypeDDList) {
		this.chequeTypeDDList = chequeTypeDDList;
	}
	
	private DomainDataView getDomainDataById(Long id, List<DomainDataView> ddList)
	{
		DomainDataView ddv = new DomainDataView();
		if(ddList != null && ddList.size() > 0)
		{
			for(DomainDataView dd :ddList)
			{
				if(dd.getDomainDataId().compareTo(id) == 0)
				{
					ddv= dd;
					break;
				}
			}
		}
		return ddv;
	}
	private PaymentTypeView getPaymentTypeById(Long id, List<PaymentTypeView> pmntList)
	{
		PaymentTypeView pmntVU = new PaymentTypeView();
		if(pmntList != null && pmntList.size() > 0)
		{
			for(PaymentTypeView pmnt:pmntList)
			{
				if(pmnt.getPaymentTypeId().compareTo(id)==0)
				{
					pmntVU= pmnt;
					break;
				}
			}
		}
		return pmntVU;
	}
	public List<PaymentTypeView> getPaymentTypeList() {
		return paymentTypeList;
	}
	public void setPaymentTypeList(List<PaymentTypeView> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	private Long getPaymentMehtodIdResembleToDDId(Long id)
	{
		Long methodId = 0L;
		if(id.compareTo(34001L)==0)
			methodId = 1L;
		if(id.compareTo(34002L)==0)
			methodId = 2L;
		if(id.compareTo(34003L)==0)
			methodId = 3L;
		return methodId;
	}
	public DomainDataView getDirectionTypeInbound() {
		return directionTypeInbound;
	}
	public void setDirectionTypeInbound(DomainDataView directionTypeInbound) {
		this.directionTypeInbound = directionTypeInbound;
	}
	public String getTrxNumber() {
		return trxNumber;
	}
	public void setTrxNumber(String trxNumber) {
		this.trxNumber = trxNumber;
	}
	}
