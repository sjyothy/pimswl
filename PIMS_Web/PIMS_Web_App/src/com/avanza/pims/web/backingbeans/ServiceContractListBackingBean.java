package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab.ViewRootKeys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class ServiceContractListBackingBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected List<String> errorMessages = new ArrayList<String>();
	private interface Keys {
		public static String CONTRACT_LIST = "CONTRACT_LIST";
		public static String STATUS_LIST = "STATUS_LIST";
		public static String RECORD_SIZE = "RECORD_SIZE";
		public static String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static String CONTRACT_TYPE = "CONTRACT_TYPE";
		public static String CONTRACT_TYPE_LIST = "CONTRACT_TYPE_LIST";
		public static String CONTEXT="context";
		
		
	}
	public static String PROPERTY_COMM_NAME="PROPERTY_COMM_NAME";
	public static String PROPERTY_ID="PROPERTY_ID";
	public static String UNIT_ID="UNIT_ID";
	public static String UNIT_NUMBER="UNIT_NUMBER";
	private final static Logger logger = Logger
			.getLogger(ServiceContractListBackingBean.class);

	@SuppressWarnings("unchecked")
	private final Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private List<ContractView> contractList;

	// binded attributes with search criteria components
	private String serviceType;
	private String contractNumber;
	private String contractStatus;
	private String contractorNumber;
	private String propertyName;
	private String ownershipType;
	private String unitNumber;
	private String unitType;
	private String selectOneContractType;
	
	// attributes used for a data grid
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;

	private HtmlInputText txtPropertyName = new HtmlInputText();
	private HtmlInputText txtUnitNumber = new HtmlInputText();
	private String VIEW_MODE_POPUP="VIEW_MODE_POPUP";

	
	
	// UI component attributes
	private HtmlDataTable dataTable;
	private List<SelectItem> statusList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu cmbContractType = new HtmlSelectOneMenu();
	HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();;
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	


	@SuppressWarnings("unchecked")
	public void init() {
		logger.logInfo("Stepped into the init method");

		try 
		{
			if (!isPostBack()) 
			{
				String contract_Type="";
				if(request.getParameter("type")!=null)
					contract_Type=request.getParameter("type").toString();
				 if(request.getParameter("viewMode")!=null)
					 viewMap.put(VIEW_MODE_POPUP, "TRUE");
				 if(request.getParameter("singleselectmode")!=null)
					 viewMap.put("singleselectmode", "true");
				 if (request.getParameter(ServiceContractListBackingBean.Keys.CONTEXT)!=null )
	    			viewMap.put(ServiceContractListBackingBean.Keys.CONTEXT, 
	    					    request.getParameter(ServiceContractListBackingBean.Keys.CONTEXT).toString());
			     if(sessionMap.containsKey(PROPERTY_COMM_NAME)&& sessionMap.get(PROPERTY_COMM_NAME)!=null)
			     {
			    		 viewMap.put(PROPERTY_COMM_NAME,sessionMap.get(PROPERTY_COMM_NAME).toString());
			    		 sessionMap.remove(PROPERTY_COMM_NAME);
			     }
			     if(sessionMap.containsKey(UNIT_NUMBER)&& sessionMap.get(UNIT_NUMBER)!=null)
			     {
			    		 viewMap.put(UNIT_NUMBER,sessionMap.get(UNIT_NUMBER).toString());
			    		 sessionMap.remove(UNIT_NUMBER);
			     }
			     if(sessionMap.containsKey("PROPERTY_ID") && sessionMap.get("PROPERTY_ID")!=null)
			     {
			    	 viewMap.put("PROPERTY_ID", sessionMap.get("PROPERTY_ID"));
			    	 sessionMap.remove("PROPERTY_ID");
			     }
			     if(sessionMap.containsKey("REQUEST_DATE") && sessionMap.get("REQUEST_DATE")!=null)
			     {
			    	 viewMap.put("REQUEST_DATE", sessionMap.get("REQUEST_DATE"));
			    	 sessionMap.remove("REQUEST_DATE");
			     }
			     
				 loadStatus();
				 loadServiceContractTypeList(contract_Type);
			}
			logger.logInfo("init |method completed successfully!");
		} 
		catch (Exception e) 
		{
			logger.LogException("init |Error Occured:" ,e);
		}
	}

	
	@SuppressWarnings("unchecked")
	private void loadServiceContractTypeList(String contract_Type)
	{
		 DomainDataView ddvMaintenance = null;
		 List<SelectItem> serviceContractList = new ArrayList<SelectItem>(0);
		 List<DomainDataView> ddl = CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE);
		 if(contract_Type!=null && contract_Type.length()>0)
		 {
		   ddvMaintenance = CommonUtil.getIdFromType(ddl,contract_Type);
		   viewMap.put(Keys.CONTRACT_TYPE, ddvMaintenance);
		 }
         for (DomainDataView domainDataView : ddl) 
         {
        	 boolean isServiceContract = false;
        	 if(domainDataView.getDataValue().compareTo(WebConstants.MAINTENANCE_CONTRACT)==0 ||
        		domainDataView.getDataValue().compareTo(WebConstants.INSURANCE_CONTRACT)==0 ||	 
        		domainDataView.getDataValue().compareTo(WebConstants.SECURITY_CONTRACT)==0 )
        		 isServiceContract = true;
        	 if(isServiceContract)
        	 {
	        	 boolean isAdd = true;
	        	 if(ddvMaintenance!=null && ddvMaintenance.getDomainDataId().compareTo(domainDataView.getDomainDataId())!=0)
	        		 isAdd =false;
        	 
	        	 if(isAdd)
	        	 {
	        		SelectItem item = new SelectItem(domainDataView.getDomainDataId().toString() ,
	        				   getIsEnglishLocale() ?domainDataView.getDataDescEn():domainDataView.getDataDescAr()); 
                     serviceContractList.add(item);	        		 
	        	 
  	        	 }
        	 }
        	
		 }
    	 viewMap.put(Keys.CONTRACT_TYPE_LIST, serviceContractList);
	}
	@SuppressWarnings("unchecked")
	public List<DomainDataView> getServiceContractTypeList()
	{
		return (ArrayList<DomainDataView>)viewMap.get(Keys.CONTRACT_TYPE_LIST);
		
	}
	public String getSelectOneContractType() 
	{
		if(viewMap.containsKey(Keys.CONTRACT_TYPE))
			selectOneContractType = ((DomainDataView)viewMap.get(Keys.CONTRACT_TYPE)).getDomainDataId().toString();
		return selectOneContractType;
	}


	public void setSelectOneContractType(String selectOneContractType) {
		this.selectOneContractType = selectOneContractType;
	}
	@SuppressWarnings("unchecked")
    private void loadStatus()
    {
		List<DomainDataView> statusComboList = (ArrayList<DomainDataView>) CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
		for (int i = 0; i < statusComboList.size(); i++) 
		{
			boolean isAdd = true;
			DomainDataView ddv = (DomainDataView) statusComboList.get(i);
			if(getIsContextMaintanceRequest() && ddv.getDataValue().compareTo(WebConstants.CONTRACT_STATUS_ACTIVE)!=0)
			    isAdd = false;
			if(isAdd)
			{
				SelectItem item = null;
				item = new SelectItem(ddv.getDomainDataId().toString(),getIsEnglishLocale() ? ddv.getDataDescEn() : ddv.getDataDescAr());
				statusList.add(item);
				
			}
			
		}
		viewMap.put(ServiceContractListBackingBean.Keys.STATUS_LIST, statusList);

    }
	@Override
	public void afterApplyRequestValues()
	{
		if(getIsContextMaintanceRequest())
		{
			propertyName = viewMap.get(PROPERTY_COMM_NAME).toString();
			txtPropertyName.setReadonly(true);
			if(viewMap.containsKey(UNIT_NUMBER))
			{
			 unitNumber = viewMap.get(UNIT_NUMBER).toString();
			 txtUnitNumber.setReadonly(true);
			}
		}
		
	}
	@Override
	@SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		if(getIsContextMaintanceRequest())
		{
			propertyName = viewMap.get(PROPERTY_COMM_NAME).toString();
			txtPropertyName.setReadonly(true);
			if(viewMap.containsKey(UNIT_NUMBER))
			{
			 unitNumber = viewMap.get(UNIT_NUMBER).toString();
			 txtUnitNumber.setReadonly(true);
			}
			
		}
		if(viewMap.containsKey(Keys.CONTRACT_TYPE))
			cmbContractType.setDisabled(true);
				
		
	}
	public boolean getIsContextMaintanceRequest()
	{
		
		if(viewMap.containsKey(ServiceContractListBackingBean.Keys.CONTEXT) && 
		   viewMap.get(ServiceContractListBackingBean.Keys.CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.ServiceContractSearchContext.MAINTENANCE_REQUEST))
		{
			return  true;
		}
		else
			return false;
		
	}

	
	// searches all the service contracts in the database according to the
	// criteria specified by the user on the screen
	@SuppressWarnings("unchecked")
	public void searchContracts() {
		String METHOD_NAME = "searchContracts()";
		logger.logInfo("Stepped into the " + METHOD_NAME + " method");

		try {
			HashMap arguments = new HashMap(0);
			//DomainDataView ddv = (DomainDataView)viewMap.get(Keys.CONTRACT_TYPE);
			ContractView contractView = new ContractView();
			contractView.setContractNumber(contractNumber);
			contractView.setContractorNumber(contractorNumber);
			arguments.put("PROPERTY_NAME",propertyName);
			
			if(getIsContextMaintanceRequest() && viewMap.containsKey("PROPERTY_ID"))
			{
				arguments.put("PROPERTY_ID",viewMap.get("PROPERTY_ID").toString());
			}
			if(getIsContextMaintanceRequest() && viewMap.containsKey("REQUEST_DATE"))
				arguments.put("REQUEST_DATE",viewMap.get("REQUEST_DATE"));
			
			contractView.setStatus(new Long(this.contractStatus));
			if(!this.ownershipType.equals("-1"))
			{
				UnitView uv= new UnitView();
				uv.setPropertyCategoryId(new Long(this.ownershipType));
				ContractUnitView cuView = new ContractUnitView();
				cuView.setUnitView(uv);
				contractView.setContractUnit(cuView);
			}
			List<Long> statusIdsIn = new ArrayList<Long>(0);
			for (SelectItem item : getStatusList())
			{
				statusIdsIn.add(new Long(item.getValue().toString()));
				
			}
			arguments.put("STATUS_ID_IN",statusIdsIn);
			contractView.setUnitTypeId(new Long(this.unitType));
			contractView.setUnitNumber(unitNumber);
			if(this.getSelectOneContractType()!=null && this.getSelectOneContractType().trim().length()>0 
					&& !this.getSelectOneContractType().equals("-1"))
            contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
			ServiceContractAgent sca = new ServiceContractAgent();
			contractList = sca.getServiceContracts(contractView,arguments);

			recordSize = contractList.size();
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;

			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;

			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

			viewMap.put(Keys.RECORD_SIZE, recordSize);
			viewMap.put(Keys.PAGINATOR_MAX_PAGES, paginatorMaxPages);
			viewMap.put(Keys.CONTRACT_LIST, contractList);

			logger.logInfo(METHOD_NAME + " method completed Succesfully");
		} catch (PimsBusinessException e) {
			logger.logError(METHOD_NAME + " crashed due to:"
					+ e.getStackTrace());
		}
	}
	
	public boolean getIsViewModePopUp()
	{
		if(viewMap.containsKey(VIEW_MODE_POPUP))
			return true;
		else
			return false;
	}
	public boolean getIsSingleSelectMode()
	{
		if(viewMap.containsKey("singleselectmode"))
			return true;
		else
			return false;
	}
	
	// resets the UI components of the search screen
	public void reset() {
		contractNumber = contractStatus = contractorNumber = propertyName = ownershipType = unitNumber = unitType = selectOneContractType="";
	}

	// attribute accessors follows below
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getStatusList() 
	{

		statusList= (ArrayList<SelectItem>)viewMap.get(ServiceContractListBackingBean.Keys.STATUS_LIST);
		return statusList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	public String getContractorNumber() {
		return contractorNumber;
	}

	public void setContractorNumber(String contractorNumber) {
		this.contractorNumber = contractorNumber;
	}

	@SuppressWarnings("unchecked")
	public List<ContractView> getContractList() {
		if (viewMap.containsKey(Keys.CONTRACT_LIST))
			contractList = (List<ContractView>) viewMap.get(Keys.CONTRACT_LIST);

		return contractList;
	}

	public void setContractList(List<ContractView> contractList) {
		this.contractList = contractList;
	}

	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);
		if (recordSize == null)
			recordSize = 0;

		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getServiceType() {
		if (viewMap.get(Keys.CONTRACT_TYPE) != null) {
			DomainDataView ddv = (DomainDataView) viewMap.get(Keys.CONTRACT_TYPE);
			serviceType = getIsEnglishLocale() ? ddv.getDataDescEn() : ddv.getDataDescAr();
		}

		return serviceType;
	}

	@SuppressWarnings("unchecked")
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	public void cmdSelectContract_Click()
	{
		
		ContractView contractView  = (ContractView)dataTable.getRowData();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getSessionMap().put(WebConstants.CONTRACT, contractView);
		
        String javaScriptText = "window.opener.populateContract("+contractView.getContractId()+");" +
                                "window.close();";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
     	
	}
	public String imgEditContract_Click()
	{
		String methodName ="imgEditContract_Click";
		logger.logInfo(methodName+"|Start");
		ContractView cView = (ContractView)dataTable.getRowData();
		setRequestParam(WebConstants.SERVICE_CONTRACT_ID, cView.getContractId());
		String navigateTo = getNavigateTo(cView.getContractTypeId());
			
		logger.logInfo(methodName+"|Finish");
		return navigateTo ;
	}

	private String getNavigateTo(Long typeId) {
		String navigateTo="";
		
			DomainDataView ddView = CommonUtil.getDomainDataFromId(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE),
					                 typeId);
			if(ddView!=null )
			{
				if(ddView.getDataValue().equals(WebConstants.SECURITY_CONTRACT))
						navigateTo ="EditSecurityContract"; 
				else if(ddView.getDataValue().equals(WebConstants.MAINTENANCE_CONTRACT))
						navigateTo ="EditMaintenanceContract";
				else if(ddView.getDataValue().equals(WebConstants.INSURANCE_CONTRACT))
						navigateTo ="EditInsuranceContract";
				
			}
				
			
		
		return navigateTo;
		
	}
	public String imgManageContract_Click()
	{
		logger.logInfo("imgManageContract_Click started...");
		String taskType = "";
		String navigateTo ="";
		try{
	   		ContractView cView =(ContractView)dataTable.getRowData();
	   		Long requestId =getRequestIdForContractId(cView.getContractId());
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				taskType = userTask.getTaskType();
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
				navigateTo = getNavigateTo(cView.getContractTypeId());
	   		}
	   		else
	   		{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
				return null;
	   		}
	   		logger.logInfo("imgManageContract_Click  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHORIZED_FOR_TASK));
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
	   			errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.USER_NOT_AUTHENTIC));
	   		}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.BPELMessages.GENERAL_EXCEPTION_MESSAGE));
			}
		}
		catch(PimsBusinessException ex)
		{
   			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		catch (Exception exception) {
			logger.LogException("imgManageContract_Click  crashed ", exception);
		}	
		return navigateTo;
		
		
	}
	private Long getRequestIdForContractId(Long contractId)throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_MAINTENANCE_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(contractId);
		rv.setContractView(cv);
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
		  return ((RequestView)requestViewList.get(0)).getRequestId(); 
		}
		return null;
    }
	public String getErrorMessages()
	{
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : errorMessages) 
				{
					messageList += "<LI>" +message + "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	public HtmlInputText getTxtPropertyName() {
		return txtPropertyName;
	}

	public void setTxtPropertyName(HtmlInputText txtPropertyName) {
		this.txtPropertyName = txtPropertyName;
	}

	public HtmlInputText getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(HtmlInputText txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}


	public HtmlSelectOneMenu getCmbContractType() {
		return cmbContractType;
	}


	public void setCmbContractType(HtmlSelectOneMenu cmbContractType) {
		this.cmbContractType = cmbContractType;
	}
}