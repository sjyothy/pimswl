package com.avanza.pims.web.backingbeans;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.taglibs.standard.tag.common.core.SetSupport;

import com.avanza.core.util.Logger;
import com.avanza.pims.report.message.resource.ReportMessageResource;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.Investment.Asset.AssetSearchBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.MessageResourceView;


public class MessageResourceManageBacking extends AbstractController {

	
	MessageResourceView messageResourceView;
	MessageResourceView messageResourceViewForSearch;
	List<MessageResourceView> messageResourceViewList;
	List<MessageResourceView> messageResourceViewListPermenent;
	private HtmlDataTable tbl_MessageResource;
	private static Logger logger ;
	private Map<String,Object> viewMap;
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	public List<String> infoMessages ;
	public String impMessage ;
	
	public MessageResourceManageBacking() 
	{
		messageResourceView = new MessageResourceView();
		messageResourceViewForSearch = new MessageResourceView();
		messageResourceViewListPermenent = new ArrayList<MessageResourceView>();
		messageResourceViewList = new ArrayList<MessageResourceView>();
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		logger = Logger.getLogger(MessageResourceManageBacking.class);
		infoMessages = new ArrayList<String>();
		impMessage = new String();
	}
	
	
	
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		String METHOD_NAME = "init()";
		try
		{
			infoMessages.add("");
		 updateValuesFromMaps();
		}catch (Exception e) {
		logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
		}
	}
	
	
	private void populateMessageResource()
	{
		
		Iterator iterator =   ReportMessageResource.getMessageResourceEn().getKeys();
		
		while (iterator.hasNext()) 
		{
			messageResourceView = new MessageResourceView();
			String key = iterator.next().toString();
			String messageEn =  ReportMessageResource.getPropertyEn(key);
			String messageAr =  ReportMessageResource.getPropertyAr(key);
			messageResourceView.setMessageKey(key);
			messageResourceView.setMessageDesEn(messageEn);
			messageResourceView.setMessageDesAr(messageAr);
		
			messageResourceViewList.add(messageResourceView);
			messageResourceViewListPermenent.add(messageResourceView);
		}
		
		if(messageResourceViewList !=null && messageResourceViewList.size()>0 )
		{
			viewMap.put("recordSize", messageResourceViewList.size());
			
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
		}
		
	}

	private void updateValuesFromMaps() throws Exception
	{
		
		// Message Resource List
		if ( viewMap.get( WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST) != null )
		{
		  messageResourceViewList = (List<MessageResourceView>) viewMap.get( WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST);
		 			
		}//for the first time only 
		else
		{
			populateMessageResource();	
		}
		if(viewMap.get(WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST_PERMENANT)!=null)
		{
			messageResourceViewListPermenent = (List<MessageResourceView>) viewMap.get( WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST_PERMENANT);
		}
		if(!viewMap.containsKey("IMPORTANT_MESSAGE") )
		{
			viewMap.put("IMPORTANT_MESSAGE", CommonUtil.getBundleMessage("messageResource.impMessage"));
		}
		updateValuesToMaps();
	}
	
	private void updateValuesToMaps() 
	{
		// Message Resource List
		if ( messageResourceViewList != null )
		{
			viewMap.put( WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST, messageResourceViewList );
		}
		if ( messageResourceViewListPermenent != null )
		{
			viewMap.put( WebConstants.MESSAGE_RESOURCE.MESSAGE_RESOURCE_LIST_PERMENANT, messageResourceViewListPermenent );
		}
	}
	
	public String onApplyChanges ()
	{
		String METHOD_NAME = "onApplyChanges()";		
	
	try{	
		infoMessages.add("");
		for(MessageResourceView mRV : messageResourceViewList)
		{
			if(mRV.getIsChange()!=null && mRV.getIsChange().equalsIgnoreCase("1"))
			{
				ReportMessageResource.getMessageResourceEn().setProperty(mRV.getMessageKey(), mRV.getMessageDesEn());
				ReportMessageResource.getMessageResourceAr().setProperty(mRV.getMessageKey(), mRV.getMessageDesAr());
				mRV.setIsChange("0");
			}
		}
		ReportMessageResource.getMessageResourceEn().save();
		ReportMessageResource.getMessageResourceAr().save();
		//ReportMessageResource.getPropertiesEn().save();
		
		Class type = ResourceBundle.class;
		Field cacheList = type.getDeclaredField("cacheList");
		cacheList.setAccessible(true);
		((Map)cacheList.get(ResourceBundle.class)).clear();
		
		infoMessages.add(CommonUtil.getBundleMessage("messageResource.successMsg"));
	  }catch (Exception e) 
	    {
		  logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);  
	   }
	  
	  return "";
	}
	public String onChange()
	{

		MessageResourceView mRV = (MessageResourceView)tbl_MessageResource.getRowData();
		mRV.setIsChange("1");
		//updateValuesToMaps();
		return "";
	}
	
	public String onSearch()
	{ 
		messageResourceViewList.clear();
				
		   
			for (MessageResourceView mRV: messageResourceViewListPermenent)
			{
				if( ( messageResourceViewForSearch.getMessageKey()== null ||
						 messageResourceViewForSearch.getMessageKey().equals(""))
					
					&& ( messageResourceViewForSearch.getMessageDesEn()== null ||
						 messageResourceViewForSearch.getMessageDesEn().equals(""))
						
					&& ( messageResourceViewForSearch.getMessageDesAr()== null ||
						 messageResourceViewForSearch.getMessageDesAr().equals("")) )
				{
					messageResourceViewList.add( mRV );
					continue;
				}
				
				if( messageResourceViewForSearch.getMessageKey()!=null && 
						! messageResourceViewForSearch.getMessageKey().equals("")
						&& StringUtils.countMatches( mRV.getMessageKey(), messageResourceViewForSearch.getMessageKey() ) > 0 )
				{
					messageResourceViewList.add( mRV );
					continue;
				}
				
				if( messageResourceViewForSearch.getMessageDesEn()!=null && 
						! messageResourceViewForSearch.getMessageDesEn().equals("")
						&& StringUtils.countMatches( mRV.getMessageDesEn(), messageResourceViewForSearch.getMessageDesEn() ) > 0 )
				{
					messageResourceViewList.add( mRV );
					continue;
				}
				
				
				if( messageResourceViewForSearch.getMessageDesAr()!=null && 
						! messageResourceViewForSearch.getMessageDesAr().equals("")
						&& StringUtils.countMatches( mRV.getMessageDesAr(), messageResourceViewForSearch.getMessageDesAr() ) > 0 )
				{
					messageResourceViewList.add( mRV );
					continue;
				}
				
			}
		  	
		viewMap.put("recordSize", messageResourceViewList.size());
		updateValuesToMaps();
		return "";
		
	}

	public MessageResourceView getMessageResourceView() {
		return messageResourceView;
	}



	public void setMessageResourceView(MessageResourceView messageResourceView) {
		this.messageResourceView = messageResourceView;
	}



	public List<MessageResourceView> getMessageResourceViewList() {
		return messageResourceViewList;
	}



	public void setMessageResourceViewList(
			List<MessageResourceView> messageResourceViewList) {
		this.messageResourceViewList = messageResourceViewList;
	}



	public HtmlDataTable getTbl_MessageResource() {
		return tbl_MessageResource;
	}



	public void setTbl_MessageResource(HtmlDataTable tbl_MessageResource) {
		this.tbl_MessageResource = tbl_MessageResource;
	}
	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}



	public MessageResourceView getMessageResourceViewForSearch() {
		return messageResourceViewForSearch;
	}



	public void setMessageResourceViewForSearch(
			MessageResourceView messageResourceViewForSearch) {
		this.messageResourceViewForSearch = messageResourceViewForSearch;
	}



	public List<MessageResourceView> getMessageResourceViewListPermenent() {
		return messageResourceViewListPermenent;
	}



	public void setMessageResourceViewListPermenent(
			List<MessageResourceView> messageResourceViewListPermenent) {
		this.messageResourceViewListPermenent = messageResourceViewListPermenent;
	}



	public String getInformationMessages() {
		return CommonUtil.getSuccessMessages( infoMessages );
	}




	public String getImpMessage() {
		if(viewMap.containsKey("IMPORTANT_MESSAGE"))
			impMessage  = viewMap.get("IMPORTANT_MESSAGE").toString(); 
		return impMessage;
	}



	public void setImpMessage(String impMessage) {
		this.impMessage = impMessage;
	}


}
