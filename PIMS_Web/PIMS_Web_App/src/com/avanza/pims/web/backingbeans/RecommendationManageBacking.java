package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.RecommendationView;

public class RecommendationManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = -7035488591733684038L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private RecommendationView recommendationView;

	public RecommendationManageBacking() 
	{
		logger = Logger.getLogger( RecommendationManageBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		recommendationView = new RecommendationView();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			recommendationView.setRecommendationDate( new Date() );
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// RECOMMENDATION VIEW
		if( sessionMap.get( WebConstants.Study.RECOMMENDATION ) != null )
		{
			recommendationView = (RecommendationView) sessionMap.get( WebConstants.Study.RECOMMENDATION );			
			sessionMap.remove( WebConstants.Study.RECOMMENDATION );
		}
		else if ( viewMap.get( WebConstants.Study.RECOMMENDATION ) != null )
		{
			recommendationView = (RecommendationView) viewMap.get( WebConstants.Study.RECOMMENDATION );
		}
		
		updateValuesToMaps();
	}
	
	private void updateValuesToMaps()
	{
		// RECOMMENDATION VIEW
		if ( recommendationView != null )
		{	
			viewMap.put( WebConstants.Study.RECOMMENDATION, recommendationView );			
		}
	}
	
	public String onSave() 
	{
		String path = null;		
		sessionMap.put( WebConstants.Study.RECOMMENDATION, recommendationView );		
		executeJavascript( "callOpenerFn(); closeWindow();" );		
		return path;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	@Override
	public void preprocess() {		
		super.preprocess();
	}
	
	@Override
	public void prerender() {		
		super.prerender();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {		
		return CommonUtil.getErrorMessages( successMessages );
		
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public RecommendationView getRecommendationView() {
		return recommendationView;
	}

	public void setRecommendationView(RecommendationView recommendationView) {
		this.recommendationView = recommendationView;
	}
}
