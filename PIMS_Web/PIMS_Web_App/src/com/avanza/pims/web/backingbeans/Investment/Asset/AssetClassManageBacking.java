package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;

public class AssetClassManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	private String pageMode;
	private String viewMode;
	private final String noteOwner = WebConstants.Attachment.EXTERNAL_ID_ASSET_CLASS;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_ASSET_CLASS;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_ASSET_CLASS;
	
	private AssetClassView assetClassView;
	private AssetServiceAgent assetServiceAgent;
	private List<AssetClassView> assetSubClassViewList;
	private HtmlDataTable assetSubClassDataTable;
		
	public AssetClassManageBacking() 
	{
		
		logger = Logger.getLogger(AssetClassManageBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		commonUtil = new CommonUtil();
		pageMode = WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_ADD_ASSET_CLASS;
		viewMode = new String();
		assetClassView = new AssetClassView();
		assetServiceAgent = new AssetServiceAgent();
		assetSubClassViewList = new ArrayList<AssetClassView>();
		assetSubClassDataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			if(!isPostBack())
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			DomainDataView ddvAssetStatusActive = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.ASSET_CLASS_STATUS), WebConstants.ASSET_CLASS_STATUS_ACTIVE);
			assetClassView.setStatusIdForUI( String.valueOf( ddvAssetStatusActive.getDomainDataId() ) );
			updateValuesFromMaps();
			showHideAttachmentsAndCommentsBtn();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY) != null )
		{
		 pageMode = (String) sessionMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY);
		 sessionMap.remove( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY ) != null )
		{
		 pageMode = (String) viewMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY );
		}
		
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) sessionMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY);
		 sessionMap.remove( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY );			
		}
		else if ( viewMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) viewMap.get( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY );
		}
		
		// ASSET CLASS VIEW
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW ) != null )
		{
		 assetClassView = (AssetClassView) sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW );
		 sessionMap.remove( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW );
		 
		 CommonUtil.loadAttachmentsAndComments(assetClassView.getAssetClassId().toString());
		 
		 assetSubClassViewList = assetServiceAgent.getAssetSubClasses(assetClassView.getAssetClassId());
		 
		}
		else if ( viewMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW )  != null )
		{
		 assetClassView = (AssetClassView) viewMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW ) ;
		}
		
		//ASSET SUB CLASS LIST
		if(viewMap.get(WebConstants.ASSET_CLASS.ASSET_SUB_CLASS_VIEW_LIST)!=null)
		{
		 assetSubClassViewList = (List<AssetClassView>)viewMap.get(WebConstants.ASSET_CLASS.ASSET_SUB_CLASS_VIEW_LIST);	
		}
			
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		if ( viewMode != null )
		{
			viewMap.put( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_KEY, viewMode );
		}
		
		
		// ASSET CLASS VIEW
		if ( assetClassView != null )
		{
			viewMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW, assetClassView );
		}
		
		if(assetSubClassViewList !=null && assetSubClassViewList.size()>0)
		{
			viewMap.put( WebConstants.ASSET_CLASS.ASSET_SUB_CLASS_VIEW_LIST,assetSubClassViewList);
		}
	
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "assetClassesSearch";
		return path;
	}
	public String onReset() 
	{
		String path = null;		
		path = "assetClassManage";
		try {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
		response.sendRedirect("assetClassManage.jsf");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{ if(checkMissingField())
		  {
				isSuccess = saveAssetClassView(assetClassView);
				isSuccess = saveAssetSubClassViewList(assetSubClassViewList);
				
				CommonUtil.loadAttachmentsAndComments(assetClassView.getAssetClassId() + "");
				CommonUtil.saveAttachments(assetClassView.getAssetClassId());
				CommonUtil.saveComments(assetClassView.getAssetClassId(),noteOwner);
				
				
	            if(viewMap.containsKey("UPDATE_DONE"))
	            {
	            	updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("assetManage.update.success") );
					viewMap.remove("UPDATE_DONE");
	            }
	            else if (isSuccess)
				{
	            	pageMode = WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS; 
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("assetManage.save.success") );
				}
		 }    
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_SAVE_ERROR) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveAssetClassView( AssetClassView assetClassView ) throws PimsBusinessException
	{
		Boolean isSuccess = false;		
		
		if ( assetClassView.getAssetClassId() == null )
		{
			assetClassView.setCreatedBy( CommonUtil.getLoggedInUser() );
			assetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			assetClassView.setAssetClassLevel(WebConstants.ASSET_CLASS_PARENT_LEVEL);
		    isSuccess = assetServiceAgent.addAssetClass(assetClassView);
		}
		else
		{
			assetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );			
			isSuccess = assetServiceAgent.updateAssetClass(assetClassView);
			viewMap.put("UPDATE_DONE", true);
		}
		
		return isSuccess;
	}
	
	private Boolean saveAssetSubClassViewList( List<AssetClassView> assetSubViewList) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( assetSubViewList != null && assetSubViewList.size() > 0 )
		{
			for ( AssetClassView currentAssetClassView : assetSubViewList )
			{
				if ( currentAssetClassView.getAssetClassId() == null )
				{
					// Add
					currentAssetClassView.setCreatedBy( CommonUtil.getLoggedInUser() );
					currentAssetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					currentAssetClassView.setAssetClassLevel(WebConstants.ASSET_CLASS_CHILD_LEVEL);
					currentAssetClassView.setAssetClassView(assetClassView);					
					isSuccess = assetServiceAgent.addAssetSubClass(currentAssetClassView);
				}
				else 
				{
					// Update
					currentAssetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = assetServiceAgent.updateAssetSubClass(currentAssetClassView);
				}				
			}
		}
		else
		{
			isSuccess = true;
		}
		
		return isSuccess;
	}

	public boolean checkMissingField()
	{
		boolean check = true;
		
		if(assetClassView.getAssetClassNameAr()==null || assetClassView.getAssetClassNameAr().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.nameAr") );
			check = false;
		}
		if(assetClassView.getAssetClassNameEn()==null || assetClassView.getAssetClassNameEn().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.nameEn") );
			check = false;
		}
		
		if(assetClassView.getStatusIdForUI()==null || assetClassView.getStatusIdForUI().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetClass.msg.status") );
			check = false;
		}
		
		return check;
	}
   
	
	public String onAddSubClass() 
	{
		String path = null;
		openPopup("openAssetSubClassManagePopup();");
		return path;
	}
	
	
	public String cmdEditSubClass_Click()
	{
		String path = null;
		
		AssetClassView selectedAssetSubClass = (AssetClassView) assetSubClassDataTable.getRowData();		
		selectedAssetSubClass.setIsForUpdate( true );
		selectedAssetSubClass.setStoredHashCode( selectedAssetSubClass.hashCode() );
		sessionMap.put( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW, selectedAssetSubClass);
		openPopup("openAssetSubClassManagePopup();");
		updateValuesToMaps();
		return path;
	}
	
public String receiveAssetSubClass()
	{	
		if ( sessionMap.get(  WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW) != null )
		{
			AssetClassView receivedAssetSubClass = (AssetClassView) sessionMap.get(  WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW );			
			
			sessionMap.remove( WebConstants.ASSET_SUB_CLASS.ASSET_SUB_CLASS_VIEW);
			
			if ( receivedAssetSubClass.getIsForUpdate() )
			{
				for ( AssetClassView currentAssetSubClassView : assetSubClassViewList  )
				{
					if ( receivedAssetSubClass.getStoredHashCode() != null && currentAssetSubClassView.getStoredHashCode()!=null 
						 && receivedAssetSubClass.getStoredHashCode().equals( currentAssetSubClassView.getStoredHashCode() ) )
					{	
						// Already in the list
						currentAssetSubClassView.setAssetClassNameEn(receivedAssetSubClass.getAssetClassNameEn());
						currentAssetSubClassView.setAssetClassNameAr(receivedAssetSubClass.getAssetClassNameAr());
						currentAssetSubClassView.setDescription(receivedAssetSubClass.getDescription());
						currentAssetSubClassView.setStatusId(receivedAssetSubClass.getStatusId());
						currentAssetSubClassView.setStatusIdForUI(receivedAssetSubClass.getStatusId().toString());
						currentAssetSubClassView.setStatusEn(receivedAssetSubClass.getStatusEn());
						currentAssetSubClassView.setStatusAr(receivedAssetSubClass.getStatusAr());
						currentAssetSubClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
						currentAssetSubClassView.setUpdatedOn( new Date() );
						currentAssetSubClassView.setIsForUpdate( false );
						currentAssetSubClassView.setStoredHashCode( null );
					}
				}
			}
			else
			{
				// Not in the list
				receivedAssetSubClass.setAssetClassView(assetClassView);
				receivedAssetSubClass.setCreatedBy( CommonUtil.getLoggedInUser() );
				receivedAssetSubClass.setUpdatedBy( CommonUtil.getLoggedInUser() );
				receivedAssetSubClass.setCreatedOn( new Date() );
				receivedAssetSubClass.setUpdatedOn( new Date() );
				receivedAssetSubClass.setIsDeleted( Constant.DEFAULT_IS_DELETED );
				receivedAssetSubClass.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
				assetSubClassViewList.add( receivedAssetSubClass );
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String cmdDeleteSubClass_Click() 
	{	
		String path = null;
		
		AssetClassView selectedAssetSubClass = (AssetClassView) assetSubClassDataTable.getRowData();
		
		if ( selectedAssetSubClass.getAssetClassId() == null )
			assetSubClassViewList.remove( selectedAssetSubClass);
		else
			selectedAssetSubClass.setIsDeleted(1L);
		
		updateValuesToMaps();
		
		return path;
	}
	
	
	
	public Integer getSubClassesRecordSize()
	{
		int size = 0;
		
		for ( AssetClassView assetClsView :  assetSubClassViewList )
		{
			if ( assetClsView.getIsDeleted().equals( Constant.DEFAULT_IS_DELETED ) )
				size++;
		}
		return size;
	}

	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS) )
			isViewMode = true;
		
		return isViewMode;
	}

	public String getIsViewReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsEditReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsStatusReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS)
			 || pageMode.equalsIgnoreCase(WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_ADD_ASSET_CLASS)	)
			readOnly = "READONLY";
		else if(pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS ))
			readOnly = "";
	
			
		return readOnly;
	}
	
	public void showHideAttachmentsAndCommentsBtn(){
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_ADD_ASSET_CLASS ) )
		    canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY ) )
		    canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_UPDATE_ASSET_CLASS ) )
	    	canAddAttachmentsAndComments(true);
	    if ( viewMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_VIEW_ASSET_CLASS) )
	    	canAddAttachmentsAndComments(false);
	}
	
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET_CLASS.ASSET_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY ) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(noteOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.Attachment.EXTERNAL_ID_ASSET_CLASS);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_ASSET_CLASS;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.Attachment.EXTERNAL_ID_ASSET_CLASS);
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}


	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}


	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	
	// Shiraz Code Start Here 
	
	public void onAddMember(){
	
	final String viewId = "/studyManage.jsf";

	FacesContext facesContext = FacesContext.getCurrentInstance();

	// This is the proper way to get the view's url
	ViewHandler viewHandler = facesContext.getApplication()
			.getViewHandler();
	String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	String javaScriptText = "javascript:SearchMemberPopup();";

	// Add the Javascript to the rendered page's header for immediate
	// execution
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,
			AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}


	public AssetClassView getAssetClassView() {
		return assetClassView;
	}

	public void setAssetClassView(AssetClassView assetClassView) {
		this.assetClassView = assetClassView;
	}

	public AssetServiceAgent getAssetServiceAgent() {
		return assetServiceAgent;
	}

	public void setAssetServiceAgent(AssetServiceAgent assetServiceAgent) {
		this.assetServiceAgent = assetServiceAgent;
	}


	public HtmlDataTable getAssetSubClassDataTable() {
		return assetSubClassDataTable;
	}

	public void setAssetSubClassDataTable(HtmlDataTable assetSubClassDataTable) {
		this.assetSubClassDataTable = assetSubClassDataTable;
	}

	public List<AssetClassView> getAssetSubClassViewList() {
		return assetSubClassViewList;
	}

	public void setAssetSubClassViewList(List<AssetClassView> assetSubClassViewList) {
		this.assetSubClassViewList = assetSubClassViewList;
	}

	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}
	

}

