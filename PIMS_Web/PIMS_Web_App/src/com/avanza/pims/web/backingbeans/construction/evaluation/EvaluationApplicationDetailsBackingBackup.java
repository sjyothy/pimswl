package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.pimsevaluaterequest.PIMSPropertyEvaluationRequestBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.RequestDetail;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;
import com.avanza.pims.ws.vo.EvaluationView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SiteVisitResultsView;
import com.avanza.pims.ws.vo.TeamMemberView;
import com.avanza.pims.ws.vo.TeamView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;


public class EvaluationApplicationDetailsBackingBackup extends AbstractController
{
	private transient Logger logger = Logger.getLogger(EvaluationApplicationDetailsBackingBackup.class);

	private transient RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	private transient ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private transient PropertyServiceAgent propertyServiceAgent  = new PropertyServiceAgent();
	
	private final String TAB_REQUEST_DETAILS = "requestDetailsTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private final String TAB_SITE_VISIT_TEAM = "siteVisitTeamTab";
	private final String TAB_SITE_VISIT_RESULTS = "siteVisitResultsTab";
	
	private final String noteOwner = WebConstants.EvaluationApplication.EVALUATION_REQUEST;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_EVALUATION_REQUEST;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_EVALUATION_REQUEST;
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	private SiteVisitResultsView siteVisitView = new SiteVisitResultsView(); 
	private EvaluationView evaluationView = new EvaluationView();
	
	String hiddenPropertyId = "";
	String hiddenLoadProperty = "";
	
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_DRAFT ="PAGE_MODE_DRAFT";
	private final String PAGE_MODE_DRAFT_DONE ="PAGE_MODE_DRAFT_DONE";
	private final String PAGE_MODE_EVALUATION ="PAGE_MODE_EVALUATION";
	private final String PAGE_MODE_EVALUATION_DONE ="PAGE_MODE_EVALUATION_DONE";
	private final String PAGE_MODE_SITE_VISIT ="PAGE_MODE_SITE_VISIT";
	private final String PAGE_MODE_SITE_VISIT_DONE ="PAGE_MODE_SITE_VISIT_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private String pageMode="default";
	
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_NEW;
	
	private String heading="";

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private UploadedFile selectedFile;
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
    
	///////////////////// TEAM MEMBER TAB /////////////////////////
	private HtmlDataTable teamMemberDataTable = new HtmlDataTable();
	private List<UserView> siteVisitTeamList = new ArrayList<UserView>();
	private Integer teamMemberRecordSize = 0;
	///////////////////////////////////////////////////////////////
	
	
    public Integer getTeamMemberRecordSize() {
    	Integer recordSize = 0;
    	
    	if(getSiteVisitTeamList()!=null)
    	recordSize = getSiteVisitTeamList().size();
    	
		return recordSize;
	}

	public void setTeamMemberRecordSize(Integer teamMemberRecordSize) {
		this.teamMemberRecordSize = teamMemberRecordSize;
	}

	public UploadedFile getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(UploadedFile selectedFile) {
		this.selectedFile = selectedFile;
	}

	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}

	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}

	/////////////////////// TEAM MEMBER TAB LOGIC //////////////////
	
	public Boolean getCanAddMember(){
		Boolean canAddMember = (Boolean)viewMap.get("canAddMember");
		if(canAddMember==null)
			canAddMember = false;
		return canAddMember;
	}

	@SuppressWarnings("unchecked")
	public void openTeamMemberPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openTeamMemberPopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showTeamMemberPopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openTeamMemberPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openTeamMemberPopup() crashed ", exception);
		}
    }
	
	public String siteVisitTeamTabClick()
	{
		logger.logInfo("siteVisitTeamTabClick() started...");
		loadSiteVisitTeamList();
		logger.logInfo("validatedForEvaluation() completed successfully!!!");
		
		return "";
	}
	
	private void loadSiteVisitResultList() throws PimsBusinessException
	{
		logger.logInfo("loadSiteVisitResultList() started...");
	
		List<SiteVisitResultsView> siteVisitResultList = new ArrayList<SiteVisitResultsView>();
		Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
		if(requestId!=null)
			siteVisitResultList = constructionServiceAgent.getSiteVisitResults(requestId);
		viewMap.put(WebConstants.SiteVisit.VISIT_RESULT_LIST, siteVisitResultList);
		viewMap.put("siteVisitResultsRecordSize", siteVisitResultList.size());
	
		logger.logInfo("loadSiteVisitResultList() completed successfully!!!");
	}
	
	private void loadEvaluationResults() throws PimsBusinessException
	{
		logger.logInfo("loadEvaluationResults() started...");
	
		Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
		if(requestId!=null){
			evaluationView = constructionServiceAgent.getEvaluationResults(requestId);
			evaluationView.setPropertyValueStr(CommonUtil.getFormattedDoubleString(evaluationView.getPropertyValue(),CommonUtil.getDoubleFormat()));
		}
		viewMap.put(WebConstants.EvaluationApplication.EVALUATION_RESULT, evaluationView);
	
		logger.logInfo("loadEvaluationResults() completed successfully!!!");
	}
	
	private void loadSiteVisitTeamList()
	{
		logger.logInfo("loadSiteVisitTeamList() started...");
		if(!viewMap.containsKey(WebConstants.TeamMembers.TEAM_MEMBER_LIST))
		{
			Map<String, TeamMemberView> teamMemberMap = new  HashMap<String, TeamMemberView>(0);
			List<UserView> userViewList = new ArrayList<UserView>();
			TeamView teamView = getTeamView();
			TeamMemberView tmv = null;
			if (teamView != null && teamView.getTeamId()!=null) {
				Iterator it = teamView.getTeamMembersView().iterator();
				while (it.hasNext()) {
					tmv = (TeamMemberView) it.next();
					if(tmv.getIsDeleted().compareTo(0L)==0)
					{
						UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
						UserView userView = new UserView();
						userView.setUserName(tmv.getLoginId());
						userView.setTeamMemberId(tmv.getTeamMemberId());
						teamMemberMap.put(tmv.getLoginId(), tmv);
						try {
							List<UserView> temp = utilityServiceAgent.getUsers(userView);
							if(!temp.isEmpty())
								userView = temp.get(0);
							    userView.setTeamMemberId(tmv.getTeamMemberId());
						} catch (PimsBusinessException e) {
							logger.logError("Error in getting user : utilityServiceAgent.getUsers(userView); crashed");
						}
						userViewList.add(userView);
					}
				}
				viewMap.put("teamMemberMap", teamMemberMap);
				viewMap.put(WebConstants.TeamMembers.TEAM_MEMBER_LIST, userViewList);
				viewMap.put("teamMemberRecordSize", userViewList.size());
			}
		}
		logger.logInfo("loadSiteVisitTeamList() completed successfully!!!");
	}
	
	
	private void loadTeamDataList()
	{
		String sMethod="loadReceivingTeamDataList";
		Boolean isAdd = true;
		

			if(sessionMap.get(WebConstants.TEAM_DATA_LIST)!=null)
			{
				// check duplication 
			   if(getSiteVisitTeamList()!=null && getSiteVisitTeamList().size()>0)
			   {
				   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
				   tempReceivingTeam = (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST);
				   sessionMap.remove(WebConstants.TEAM_DATA_LIST);
				   for(UserView uV:tempReceivingTeam)
				   {
					   for(UserView uV1: getSiteVisitTeamList())
					   {
						   isAdd = true;
						   if(uV.getUserName().equals(uV1.getUserName()))
						   {
							 isAdd = false;
							 break;
						   }
					   }
					
					  if(isAdd)
					  {
						  uV.setTeamMemberIsDeleted(0L);
						  getSiteVisitTeamList().add(uV);
					  }
				   }
				   viewMap.put(WebConstants.TeamMembers.TEAM_MEMBER_LIST,getSiteVisitTeamList());
				   
			   } // for the first time when no member present 
			   else{
				   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
					for(UserView uV: (List<UserView>)sessionMap.get(WebConstants.TEAM_DATA_LIST))
					{
						 uV.setTeamMemberIsDeleted(0L);
						  tempReceivingTeam.add(uV);
					}
				    sessionMap.remove(WebConstants.TEAM_DATA_LIST);	
				    setSiteVisitTeamList(tempReceivingTeam);
			   }
			}
  	   }
	
	public String deleteMember() {
		logger.logInfo("deleteMember() started...");
		Map<String, TeamMemberView> teamMemberMap = new  HashMap<String, TeamMemberView>(0);
		if(viewMap.get("teamMemberMap")!=null)
		{
		teamMemberMap = (Map<String, TeamMemberView>) viewMap.get("teamMemberMap");
		}
		UserView receivingTeamView = (UserView) teamMemberDataTable.getRowData();
		try {
			List<UserView> userlst = getSiteVisitTeamList();
			if (userlst != null && !userlst.isEmpty()) {
				userlst.remove(receivingTeamView);
				
				// if member already present 
				if(receivingTeamView.getTeamMemberId()!=null)
				constructionServiceAgent.deleteTeamMember(receivingTeamView.getUserName(), getTeamView().getTeamId()
						                                  , getArgMap(),receivingTeamView.getTeamMemberId());
				if(teamMemberMap !=null && teamMemberMap.containsKey(receivingTeamView.getUserName()))
				{
					teamMemberMap.remove(receivingTeamView.getUserName());
				}
			}
			setSiteVisitTeamList(userlst);
			if(viewMap.containsKey("teamView"))
			viewMap.remove("teamView");

			logger.logInfo("deleteMember() completed successfully!!!");
		} catch (Exception e) {
			logger.LogException("deleteMember() creashed...", e);
		}
		return "delete";
	}
	
	/////////////////////////////////////////////////////////
	
	
	@SuppressWarnings("unchecked")
	public Boolean validatedForEvaluation() {
	    logger.logInfo("validatedForEvaluation() started...");
	    Boolean validated = true;
	    try
		{
	    	if(evaluationView.getEvaluationDate()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.EVALUATION_DATE));
				return validated;
	        }
	    	
	    	if(StringHelper.isEmpty(evaluationView.getEngineerName()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.EVALUATION_ENGINEER));
				return validated;
	        }

	    	if(StringHelper.isEmpty(evaluationView.getPropertyValueStr()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.PROPERTY_VALUE));
				return validated;
	        }
	    	
	    	if(StringHelper.isNotEmpty(evaluationView.getPropertyValueStr()))
	        {
	    		try{
		    		String doubleStr = evaluationView.getPropertyValueStr();
		    		for(int index=0; index<doubleStr.length(); index++){
		    			char digit = doubleStr.charAt(index);
		    			if(!((digit>='0' && digit<='9') || (digit==',') || (digit=='.')))
		    				throw new NumberFormatException();
		    		}
		    		Double propertyValue = CommonUtil.getDoubleValue(doubleStr);
		    		evaluationView.setPropertyValue(propertyValue);
	    		}
	    		catch(NumberFormatException nfe){
		    		validated = false;
		        	errorMessages.clear();
		        	errorMessages.add(CommonUtil.getInvalidMessage(WebConstants.Evaluation.PROPERTY_VALUE));
					return validated;
	    		}
	        }
	    	
	    	if(fileUploadCtrl.getUploadedFile()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
				return validated;
	        }
	    	
			logger.logInfo("validatedForEvaluation() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForEvaluation() crashed ", exception);
		}
		return validated;
	}
    
    @SuppressWarnings("unchecked")
	public Boolean validatedForNewRequest() {
	    logger.logInfo("validatedForNewRequest() started...");
	    Boolean validated = true;
	    try
		{
	    	String evaluationPurposeId = (String)viewMap.get("evaluationPurposeId");
	    	if(evaluationPurposeId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.EvaluationApplication.Tab.EVALUATION_PURPOSE));
				return validated;
	        }
	    	
	    	if(StringHelper.isEmpty(hiddenPropertyId))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_SELECT_PROPERTY));
				return validated;
	        }
	    	else
	    		viewMap.put("propertyId", hiddenPropertyId);

	    	String requestDescription = (String)viewMap.get("requestDescription");
	    	if(StringHelper.isEmpty(requestDescription))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.EvaluationApplication.Tab.DESCRIPTION));
				return validated;
	        }
	    	
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;
				return validated;
	    	}

			logger.logInfo("validatedForNewRequest() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForNewRequest() crashed ", exception);
		}
		return validated;
	}
	
    @SuppressWarnings("unchecked")
	public Boolean validatedForSiteVisit() {
	    logger.logInfo("validatedForSiteVisit() started...");
	    Boolean validated = true;
	    try
		{
	    	if(siteVisitView.getVisitDate()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.SiteVisit.VISIT_DATE));
				return validated;
	        }
	    	viewMap.put("siteVisitDate", siteVisitView.getVisitDate());
	    	
	    	if(StringHelper.isEmpty(siteVisitView.getResult()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.SiteVisit.VISIT_RESULT));
				return validated;
	        }
	    	viewMap.put("siteVisitResult", siteVisitView.getResult());
	    	
	    	if(getSiteVisitTeamList().isEmpty())
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SiteVisit.Messages.TEAM_MEMBER_REQUIRED));
				return validated;
	        }
	    	
			logger.logInfo("validatedForSiteVisit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForSiteVisit() crashed ", exception);
		}
		return validated;
	}
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			if(requestId!=null)
				CommonUtil.loadAttachmentsAndComments(requestId + "");
			EvaluationRequestDetailsView requestDetailsTabView = requestServiceAgent.getEvaluationRequestById(requestId, getArgMap());	
			viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
			populateRequestDetailsTab();
			
			if(pageMode.equals(PAGE_MODE_SITE_VISIT)){
				loadSiteVisitTeamList();
				viewMap.put("selectedTab", TAB_SITE_VISIT_TEAM);
			}
			else if(pageMode.equals(PAGE_MODE_EVALUATION)){
				loadSiteVisitResultList();
				loadSiteVisitTeamList();
//				viewMap.put("selectedTab", TAB_SITE_VISIT_RESULTS);
			}
			else if(pageMode.equals(PAGE_MODE_VIEW)){
				loadSiteVisitResultList();
				loadSiteVisitTeamList();
				loadEvaluationResults();
			}
				
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (PimsBusinessException e) {
			logger.LogException("setValues() crashed ", e);
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	private void getRequestTeam(){
		logger.logInfo("getRequestTeam() started...");
		try {
			Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			if(requestId!=null){
				TeamView teamView = constructionServiceAgent.getRequestTeam(requestId);
				viewMap.put("teamView", teamView);
			}
			logger.logInfo("getRequestTeam() completed successfully!!!");
		} catch (PimsBusinessException e) {
			logger.LogException("getRequestTeam() crashed ", e);
		}
	}
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
			if(!isPostBack())
			{
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				setMode();
				setValues();
			}
			else{
	
				loadTeamDataList();
			}

			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
	
	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {

			
			// from evaluation application search screen
			if(sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE)!=null)
			{
				String extendRequestMode = (String) sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE);
				sessionMap.remove(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE);
				Long requestId = (Long)sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				sessionMap.remove(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				if(requestId!=null)
					viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
				
				canAddAttachmentsAndComments(true);
				viewMap.put("evaluationRequestDetailsReadonlyMode", true);
				viewMap.put("asterisk", "");
				
				if(extendRequestMode.equals(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_NEW)){
					pageMode = PAGE_MODE_NEW;
					viewMap.put("evaluationRequestDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_NEW);
				}
				else if(extendRequestMode.equals(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_DRAFT)){
					pageMode = PAGE_MODE_DRAFT;
					viewMap.put("evaluationRequestDetailsReadonlyMode", false);
					viewMap.put("asterisk", "*");
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_DRAFT);
				}
				else if(extendRequestMode.equals(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_SITE_VISIT)){
					pageMode = PAGE_MODE_SITE_VISIT;
					viewMap.put("canAddMember", true);
					viewMap.put("siteVisitReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_SITE_VISIT);
				}
				//
				else if(extendRequestMode.equals(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_EVALUATE)){
					pageMode = PAGE_MODE_EVALUATION;
					viewMap.put("evaluationReadonlyMode", false);
					viewMap.put("canAddMember", false);
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_EVALUATE);
				}
				//
				else if(extendRequestMode.equals(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_VIEW)){
					pageMode = PAGE_MODE_VIEW;
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_VIEW);
					canAddAttachmentsAndComments(false);
					viewMap.put("siteVisitReadonlyMode", true);
					viewMap.put("evaluationReadonlyMode", true);
				}
				
				viewMap.put(PAGE_MODE, pageMode);
				
			}
			// FROM TASK LIST
			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
				checkTask();
			}
			// set back page
			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void setAllUnitsAsUnSelected(List<AuctionUnitView> oldAllSelectedUnitList){
		for(AuctionUnitView auctionUnitView:oldAllSelectedUnitList){
			auctionUnitView.setSelected(false);
		}
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	

	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long requestId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)!=null)
				{
					requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
					viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
				}
				String taskType = userTask.getTaskType();
				if(taskType.equals(WebConstants.UserTasks.EvaluatePropertyRequest.TASK_TYPE_EVALUATE))
				{
					pageMode = PAGE_MODE_EVALUATION;
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_EVALUATE);
					viewMap.put("evaluationReadonlyMode", false);
				}
				else if(taskType.equals(WebConstants.UserTasks.EvaluatePropertyRequest.TASK_TYPE_SITE_VISIT))
				{
					pageMode = PAGE_MODE_SITE_VISIT;
					viewMap.put("canAddMember", true);
					viewMap.put("siteVisitReadonlyMode", false);
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_SITE_VISIT);
				}

				viewMap.put("evaluationRequestDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
				viewMap.put(PAGE_MODE, pageMode);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_VIEW;
			
			logger.logInfo("pageMode : %s", pageMode);
			heading = getHeading();
			
			String selectedTab = (String) viewMap.get("selectedTab");
			if(StringHelper.isNotEmpty(selectedTab))
				tabPanel.setSelectedTab(selectedTab);
			viewMap.remove("selectedTab");
			
			if(pageMode.equals(PAGE_MODE_SITE_VISIT_DONE)){
				if(viewMap.get("siteVisitDate")!=null)
					siteVisitView.setVisitDate((Date)viewMap.get("siteVisitDate"));
				if(viewMap.get("siteVisitResult")!=null)
					siteVisitView.setResult((String)viewMap.get("siteVisitResult"));
			}
			
			if(pageMode.equals(PAGE_MODE_VIEW)){
				evaluationView = (EvaluationView) viewMap.get(WebConstants.EvaluationApplication.EVALUATION_RESULT);
			}
			// for evaluationRequestTab start
			loadProperty();
			// for evaluationRequestTab end
				
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadProperty(){
		try{
			logger.logInfo("loadProperty started...");
			Boolean propertyLoaded = (Boolean) viewMap.get("propertyLoaded");
			propertyLoaded = (propertyLoaded==null)?false:propertyLoaded;
			Boolean loadProperty = hiddenLoadProperty.equals("true");
			if(StringHelper.isNotEmpty(hiddenPropertyId) && (!propertyLoaded || loadProperty)){
				viewMap.put("propertyId", hiddenPropertyId);
				EvaluationRequestDetailsView propertyView = requestServiceAgent.getEvaluationApplicationPropertyById(Long.parseLong(hiddenPropertyId), getArgMap());
				if(propertyView.getPropertyId()!=null)
					viewMap.put("propertyId", propertyView.getPropertyId());
				if(propertyView.getApplicationStatus()!=null)
					viewMap.put("applicationStatus", propertyView.getApplicationStatus());
				if(propertyView.getPropertyType()!=null)
					viewMap.put("propertyType", propertyView.getPropertyType());
				if(propertyView.getPropertyStatus()!=null)
					viewMap.put("propertyStatus", propertyView.getPropertyStatus());
				if(propertyView.getPropertyName()!=null)
					viewMap.put("propertyName", propertyView.getPropertyName());
				if(propertyView.getEmirate()!=null)
					viewMap.put("emirate", propertyView.getEmirate());
				if(propertyView.getLocationArea()!=null)
					viewMap.put("locationArea", propertyView.getLocationArea());
				if(propertyView.getLandNumber()!=null)
					viewMap.put("landNumber", propertyView.getLandNumber());
				if(propertyView.getRequestId()!=null)
					viewMap.put("requestId", propertyView.getRequestId());
				if(propertyView.getLandArea()!=null)
					viewMap.put("landArea", propertyView.getLandArea());
				if(propertyView.getAddress()!=null)
					viewMap.put("address", propertyView.getAddress());
				if(propertyView.getFloorCount()!=null)
					viewMap.put("floorCount", propertyView.getFloorCount());
				if(propertyView.getConstructionDate()!=null)
					viewMap.put("constructionDate", propertyView.getConstructionDate());
				if(viewMap.containsKey("EVALUATION_BY_ID"))
				{
					Long id;
					id=(Long) viewMap.get("EVALUATION_BY_ID");
					if(id.compareTo(2L)==0)
						viewMap.put("SHOW_UNIT_TYPE_DETAILS", true);
					else
						viewMap.put("SHOW_UNIT_TYPE_DETAILS", false);
				}
				
				viewMap.put("propertyLoaded", true);
				hiddenLoadProperty = "false";
			}
			logger.logInfo("loadProperty completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadProperty crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	/*
	 * Button Click Action Handlers
	 */

	@SuppressWarnings("unchecked")
	public String saveSiteVisit() {
		logger.logInfo("saveSiteVisit() started...");
		Long requestId=0L;
		Boolean success = false;
		try {
				if(validatedForSiteVisit()){
					if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))			
						requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
					
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
					Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
					siteVisitView.setRequestId(requestId);
					siteVisitView.setTeam(prepareTeamView(getSiteVisitTeamList()));
					success = constructionServiceAgent.saveSiteVisitResults(siteVisitView, getArgMap());
					
					if(viewMap.containsKey("teamView"))
					viewMap.remove("teamView");
					viewMap.remove(WebConstants.TeamMembers.TEAM_MEMBER_LIST);
					loadSiteVisitTeamList();
					
					
					if(success && completeTask(TaskOutcome.OK)){
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SITE_VISIT_DONE);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_SITE_VISIT_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_SITE_VISIT_DONE);
						viewMap.put("siteVisitReadonlyMode", true);
						viewMap.put("canAddMember", false);
						canAddAttachmentsAndComments(false);
					}
					
				}
			logger.logInfo("saveSiteVisit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveSiteVisit() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_SITE_VISIT_FAILURE));
		}
		return "saveSiteVisit";
	}
	
	
	
	private TeamView prepareTeamView(List<UserView> listUserView) throws PimsBusinessException {
		logger.logInfo("prepareTeamView started...");
		TeamView teamView = new TeamView();
		Set<TeamMemberView> teamMemberSet = new HashSet<TeamMemberView>(0);
		TeamMemberView teamMemberView = new TeamMemberView();
		
		TeamView currentTeam = getTeamView();
		
		if(currentTeam.getTeamId()!= null)
			teamView.setTeamId(currentTeam.getTeamId());
		

		teamView.setTeamName("Evaluation Team");
		teamView.setCreatedOn(new Date());
		teamView.setCreatedBy(CommonUtil.getLoggedInUser());
		teamView.setUpdatedOn(new Date());
		teamView.setUpdatedBy(CommonUtil.getLoggedInUser());
		teamView.setIsDeleted(new Long(0));
		teamView.setRecordStatus(new Long(1));
		Map<String, TeamMemberView> teamMemberMap = (Map<String, TeamMemberView>) viewMap.get("teamMemberMap");
		for (UserView teamMember : listUserView) {
			if(teamMemberMap !=null && teamMemberMap.containsKey(teamMember.getUserName())){
				teamMemberView = teamMemberMap.get(teamMember.getUserName()); 
			}
			else{
				teamMemberView = new TeamMemberView();
				teamMemberView.setLoginId(teamMember.getUserName());
				teamMemberView.setIsDeleted(new Long(0));
				teamMemberView.setRecordStatus(new Long(1));
				teamMemberView.setUpdatedOn(new Date());
				teamMemberView.setCreatedOn(new Date());
				teamMemberView.setCreatedBy(CommonUtil.getLoggedInUser());
				teamMemberView.setUpdatedBy(CommonUtil.getLoggedInUser());
			}
				teamMemberSet.add(teamMemberView);
			
		}
		
		teamView.setTeamMembersView(teamMemberSet);
		
		logger.logInfo("prepareTeamView completed successfully...");
		return teamView;
	}
	
	@SuppressWarnings("unchecked")
	public String saveEvaluation() {
		logger.logInfo("saveEvaluation() started...");
		Long requestId=0L;
		Boolean success = false;
		try {
				if(validatedForEvaluation()){
					if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))			
						requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
					
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
					Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
					evaluationView.setRequestId(requestId);
					success = constructionServiceAgent.saveEvaluation(evaluationView, getArgMap());
					
					StringBuilder documentTitle = new StringBuilder(evaluationView.getEngineerName());
					documentTitle = documentTitle.append(" ").append(CommonUtil.getStringFromDate(evaluationView.getEvaluationDate())).append(" ").append(viewMap.get("applicationNumber").toString());
					DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), documentTitle.toString(), null);
					Boolean attachReportSuccess = documentView!=null; 
					if(success && completeTask(TaskOutcome.OK)){
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_PROPERTY_EVALUATED);
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_EVALUATE_SUCCESS);
						viewMap.put(PAGE_MODE, PAGE_MODE_EVALUATION_DONE);
						viewMap.put("evaluationReadonlyMode", true);
						canAddAttachmentsAndComments(false);
						
					}
					if(!attachReportSuccess)
						errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAILURE));
				}
			logger.logInfo("saveEvaluation() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveEvaluation() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_EVALUATE_FAILURE));
		}
		return "saveEvaluation";
	}
	
	@SuppressWarnings("unchecked")
	public String cancelEvaluation() {
		logger.logInfo("cancelEvaluation() started...");
		Long requestId=0L;
		Boolean success = false;
		try {
				if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))			
					requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
				Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
				
				if(completeTask(TaskOutcome.CANCEL)){
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_CANCELED);
					errorMessages.clear();
					infoMessage = CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_CANCEL_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_EVALUATION_DONE);
					viewMap.put("evaluationReadonlyMode", true);
					canAddAttachmentsAndComments(false);
				}
			logger.logInfo("cancelEvaluation() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("cancelEvaluation() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_EVALUATE_FAILURE));
		}
		return "cancelEvaluation";
	}
	
	@SuppressWarnings("unchecked")
	public String doSiteVisit() {
		logger.logInfo("doSiteVisit() started...");
		Long requestId=0L;
		try {
				if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))			
					requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
				Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
				
				if(completeTask(TaskOutcome.SEND_FOR_SITE_VISIT)){
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_ON_SITE_VISIT);
					errorMessages.clear();
					infoMessage = CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_ON_SITE_VISIT_SUCCESS);
					viewMap.put(PAGE_MODE, PAGE_MODE_EVALUATION_DONE);
					viewMap.put("evaluationReadonlyMode", true);
					canAddAttachmentsAndComments(false);
				}
			logger.logInfo("doSiteVisit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("doSiteVisit() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_ON_SITE_VISIT_FAILURE));
		}
		return "doSiteVisit";
	}
	
	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome) {
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		try {
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = CommonUtil.getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, taskOutcome);
//					saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
				}
			logger.logInfo("completeTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.logError("completeTask() crashed");
			success = false;
		}
		return success;
	}
	
	public Boolean invokeBpel(Long requestId) {
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try {
			PIMSPropertyEvaluationRequestBPELPortClient bpelPortClient= new PIMSPropertyEvaluationRequestBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.EvaluationApplication.EVALUATION_REQUEST_BPEL_END_POINT);
			String userId = CommonUtil.getLoggedInUser();
			bpelPortClient.setEndpoint(endPoint);
			bpelPortClient.initiate(requestId, userId, null, null);

			logger.logInfo("invokeBpel() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
		}
		finally{
			if(!success)
			{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SUBMIT_FAILURE));
			}
		}
		return success;
	}
	
	@SuppressWarnings("unchecked")
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	
	@SuppressWarnings("unchecked")
	public String saveRequest() {
		logger.logInfo("saveRequest() started...");
		String success = "false";
		Boolean crashed = false;
		Boolean firstTime = false;
		try {
//			fillExtendRequestDetailsView();
			if(validatedForNewRequest()){
				EvaluationRequestDetailsView evaluationRequestDetailsView = new EvaluationRequestDetailsView();
				Long requestId = (Long)viewMap.get("requestId");
				 
				if(requestId!=null){
					evaluationRequestDetailsView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
					evaluationRequestDetailsView.setRequestId(requestId);
				}
					 
				 String evaluationPurposeId = (String)viewMap.get("evaluationPurposeId");
				 evaluationRequestDetailsView.setEvaluationPurposeId(evaluationPurposeId);
				 //This propertyId is uses when evaluation has done on property(by Wholeproperty/UnitType)
				 if(viewMap.get("propertyId")!=null)
				 {
					 String propertyId = (String)viewMap.get("propertyId");
					 evaluationRequestDetailsView.setPropertyId(Long.valueOf(propertyId));
					 
				 }
//				 For unit evaluation
//				 if(viewMap.get("unitId")!=null)
//				 {
//					 String unitId = (String)viewMap.get("unitId");
////					 evaluationRequestDetailsView.setPropertyId(Long.valueOf(unitId));
//				 }	
				 
				 String requestDescription = (String)viewMap.get("requestDescription");
				 evaluationRequestDetailsView.setRequestDescription(requestDescription);
				 
				 String applicationStatusId = (String)viewMap.get("applicationStatusId");
				 evaluationRequestDetailsView.setApplicationStatusId(applicationStatusId);
//				 TODO:
				 
				 if(evaluationRequestDetailsView.getRequestId()==null)
					 firstTime = true;
 
				 evaluationRequestDetailsView = requestServiceAgent.saveEvaluationRequest(evaluationRequestDetailsView, getArgMap());
				 viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, evaluationRequestDetailsView);
				 populateRequestDetailsTab();
				 
				 if(evaluationRequestDetailsView.getRequestId()!=null){
					requestId = evaluationRequestDetailsView.getRequestId();
					viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
					
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					
					Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
					Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
					
					if(attachSuccess && commentSuccess)
						success = "true";
					
					errorMessages.clear();
					
					if(firstTime){
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_CREATED);
						infoMessage=CommonUtil.getParamBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_CREATE_SUCCESS, evaluationRequestDetailsView.getApplicationNumber());
					}
					else{
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SAVED);
						infoMessage=CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SAVE_SUCCESS);							
					}
					viewMap.put(PAGE_MODE, PAGE_MODE_DRAFT_DONE);
					viewMap.put("evaluationRequestDetailsReadonlyMode", true);
					viewMap.put("asterisk", "");
					canAddAttachmentsAndComments(false);

					if(firstTime)
						invokeBpel(requestId);
			   }
				 
			}
			else
				success = "validation";
			logger.logInfo("saveRequest() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("saveRequest() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("saveRequest() crashed ", exception);
		}
		finally{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SAVE_FAILURE));
		}
		return success;
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(StringHelper.isEmpty(backScreen))
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return "evaluationApplicationSearch";
    }

	@SuppressWarnings("unchecked")
	private void populateRequestDetailsTab() throws PimsBusinessException
	{
		EvaluationRequestDetailsView requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
		 List<RequestDetailView> requestDetailsList=new ArrayList<RequestDetailView>(0); 
		if(requestDetailsTabView!=null){
			if(requestDetailsTabView.getApplicationDate()!=null)
				viewMap.put("applicationDate", requestDetailsTabView.getApplicationDate());
			if(requestDetailsTabView.getApplicationNumber()!=null)
				viewMap.put("applicationNumber", requestDetailsTabView.getApplicationNumber());
			if(requestDetailsTabView.getEvaluationPurpose()!=null)
			{
				viewMap.put("evaluationPurpose", requestDetailsTabView.getEvaluationPurpose());
			    viewMap.put("evaluationPurposeId",requestDetailsTabView.getEvaluationPurposeId());
			}
			if(requestDetailsTabView.getEvaluationPurposeId()!=null)
				viewMap.put("evaluationPurposeId", requestDetailsTabView.getEvaluationPurposeId());
			if(requestDetailsTabView.getPropertyId()!=null)
			{
				viewMap.put("propertyId", requestDetailsTabView.getPropertyId());
			    hiddenPropertyId = requestDetailsTabView.getPropertyId().toString();
			    Long requestId;
			    if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))
			    	{
			    		requestId=(Long) viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			    		RequestView rv=new RequestView();
			    		
			    		requestDetailsList=requestServiceAgent.getRequestDetailByRequestId(requestId);
			    		if(requestDetailsList!=null && requestDetailsList.size()>0)
			    			{
			    				RequestDetail rd=new RequestDetail();
			    				if(rd.getUnit()!=null)
			    					{
			    						viewMap.put("evaluationOn",CommonUtil.getBundleMessage("property"));
			    						viewMap.put("evaluationOnId","1");
			    					}
			    				else
				    				{
				    					viewMap.put("evaluationOn",CommonUtil.getBundleMessage("cancelContract.tab.unit"));
			    						viewMap.put("evaluationOnId","2");
				    				}
			    					
			    			}
			    		else
	    				{
			    			viewMap.put("evaluationOn",CommonUtil.getBundleMessage("property"));
    						viewMap.put("evaluationOnId","1");
	    				}
			    	}
			}    
			if(requestDetailsTabView.getApplicationStatus()!=null)
				viewMap.put("applicationStatus", requestDetailsTabView.getApplicationStatus());
			if(requestDetailsTabView.getApplicationStatusId()!=null)
				viewMap.put("applicationStatusId", requestDetailsTabView.getApplicationStatusId());
			if(requestDetailsTabView.getPropertyType()!=null)
				viewMap.put("propertyType", requestDetailsTabView.getPropertyType());
			if(requestDetailsTabView.getPropertyName()!=null)
				viewMap.put("propertyName", requestDetailsTabView.getPropertyName());
			if(requestDetailsTabView.getPropertyStatus()!=null)
				viewMap.put("propertyStatus", requestDetailsTabView.getPropertyStatus());
			if(requestDetailsTabView.getEmirate()!=null)
				viewMap.put("emirate", requestDetailsTabView.getEmirate());
			if(requestDetailsTabView.getLocationArea()!=null)
				viewMap.put("locationArea", requestDetailsTabView.getLocationArea());
			if(requestDetailsTabView.getLandNumber()!=null)
				viewMap.put("landNumber", requestDetailsTabView.getLandNumber());
			if(requestDetailsTabView.getRequestId()!=null)
				viewMap.put("requestId", requestDetailsTabView.getRequestId());
			if(requestDetailsTabView.getLandArea()!=null)
				viewMap.put("landArea", requestDetailsTabView.getLandArea());
			if(requestDetailsTabView.getAddress()!=null)
				viewMap.put("address", requestDetailsTabView.getAddress());
			if(requestDetailsTabView.getFloorCount()!=null)
				viewMap.put("floorCount", requestDetailsTabView.getFloorCount());
			if(requestDetailsTabView.getConstructionDate()!=null)
				viewMap.put("constructionDate", requestDetailsTabView.getConstructionDate());
			if(requestDetailsTabView.getRequestDescription()!=null)
				viewMap.put("requestDescription", requestDetailsTabView.getRequestDescription());
		}
	}
	
	public Boolean saveSystemComments(String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
	    	if(requestId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(noteOwner, sysNote, requestId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	
	public void requestHistoryTabClick()
	{
		logger.logInfo("requestHistoryTabClick started...");
		try	
		{
			RequestHistoryController rhc=new RequestHistoryController();
			Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			if(requestId!=null && requestId!=0L)
				rhc.getAllRequestTasksForRequest(noteOwner, requestId.toString());
			
			logger.logInfo("requestHistoryTabClick completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestHistoryTabClick crashed...",ex);
		}
	}

    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	
	public Boolean getShowSiteVisitActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_SITE_VISIT))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_SITE_VISIT_DONE))
			return true;
		return false;
	}
	
	public Boolean getShowEvaluationActionPanel(){
		if (pageMode.equalsIgnoreCase("default"))
			{
				viewMap.put("SHOW_EVALUALTION_BY", true);
				return true;
			}
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			{
				viewMap.put("SHOW_EVALUALTION_BY", true);
				return true;
			}
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION_DONE))
			{
				viewMap.put("SHOW_EVALUALTION_BY", true);
				return true;
			}
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			{
				viewMap.put("SHOW_EVALUALTION_BY", true);
				return true;
			}
		
			viewMap.put("SHOW_EVALUALTION_BY", false);
			return false;
	}
	
	public Boolean getShowHistory(){
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION_DONE))
			return true;
		return true;
	}
	
	public Boolean getShowSiteVisitResults(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			return true;
		return false;
	}
	
	public Boolean getShowSiteVisitTeam(){
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_SITE_VISIT))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_SITE_VISIT_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION_DONE))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_VIEW))
			return true;
		return false;
	}
	
	public Boolean getShowCancel(){
		return true;
	}
	
	public Boolean getShowSaveRequest() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_DRAFT))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
			return true;
		return false;
	}
	
	public Boolean getShowSaveEvaluation() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			return true;
		return false;
	}
	
	public Boolean getShowSiteVisit() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EVALUATION))
			return true;
		return false;
	}

	public Boolean getShowSaveSiteVisit() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_SITE_VISIT))
			return true;
		return false;
	}

	
	public Boolean getReadonlyMode(){
		Boolean evaluationRequestDetailsReadonlyMode = (Boolean)viewMap.get("evaluationRequestDetailsReadonlyMode");
		if(evaluationRequestDetailsReadonlyMode==null)
			evaluationRequestDetailsReadonlyMode = false;
		return evaluationRequestDetailsReadonlyMode;
	}

	public Boolean getEvaluationReadonlyMode(){
		Boolean evaluationReadonlyMode = (Boolean)viewMap.get("evaluationReadonlyMode");
		return (evaluationReadonlyMode==null)?false:evaluationReadonlyMode;
	}
	
	public Boolean getSiteVisitReadonlyMode(){
		Boolean siteVisitReadonlyMode = (Boolean)viewMap.get("siteVisitReadonlyMode");
		return (siteVisitReadonlyMode==null)?false:siteVisitReadonlyMode;
	}
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHiddenPropertyId() {
		return hiddenPropertyId;
	}

	public void setHiddenPropertyId(String hiddenPropertyId) {
		this.hiddenPropertyId = hiddenPropertyId;
	}

	public EvaluationView getEvaluationView() {
		return evaluationView;
	}

	public void setEvaluationView(EvaluationView evaluationView) {
		this.evaluationView = evaluationView;
	}

	public String getHiddenLoadProperty() {
		return hiddenLoadProperty;
	}

	public void setHiddenLoadProperty(String hiddenLoadProperty) {
		this.hiddenLoadProperty = hiddenLoadProperty;
	}

	public SiteVisitResultsView getSiteVisitView() {
		return siteVisitView;
	}

	public void setSiteVisitView(SiteVisitResultsView siteVisitView) {
		this.siteVisitView = siteVisitView;
	}

	public List<UserView> getSiteVisitTeamList() {
		siteVisitTeamList = (List<UserView>)viewMap.get(WebConstants.TeamMembers.TEAM_MEMBER_LIST);
		if(siteVisitTeamList==null)
			siteVisitTeamList = new ArrayList<UserView>();
		return siteVisitTeamList;
	}

	public void setSiteVisitTeamList(List<UserView> siteVisitTeamList) {
		this.siteVisitTeamList = siteVisitTeamList;
		if(this.siteVisitTeamList!=null)
			viewMap.put(WebConstants.TeamMembers.TEAM_MEMBER_LIST,this.siteVisitTeamList);			
	}

	public HtmlDataTable getTeamMemberDataTable() {
		return teamMemberDataTable;
	}

	public void setTeamMemberDataTable(HtmlDataTable teamMemberDataTable) {
		this.teamMemberDataTable = teamMemberDataTable;
	}
	
	public TeamView getTeamView(){
		if(!viewMap.containsKey("teamView")){
			getRequestTeam();
		}
		TeamView teamView = (TeamView)viewMap.get("teamView");
		return teamView;
	}

}

