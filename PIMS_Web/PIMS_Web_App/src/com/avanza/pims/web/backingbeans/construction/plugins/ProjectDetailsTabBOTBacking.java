package com.avanza.pims.web.backingbeans.construction.plugins;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.context.FacesContext;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.project.ProjectSearchBean;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.pims.ws.vo.PropertyView;



public class ProjectDetailsTabBOTBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ProjectDetailsTabBOTBacking.class);
    
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap=context.getViewRoot().getAttributes();
    private HtmlGraphicImage  imgSearchProject = new HtmlGraphicImage();
    private HtmlGraphicImage  imgSearchProperty = new HtmlGraphicImage();
    private HtmlCommandButton btnAddProperty  = new HtmlCommandButton();
    private String projectName;
    private String projectNumber;
    private String projectStatus ;
    private String projectType ; 
    private String projectEstimatedCost ;
    private PropertyView propertyView ;
    
    
    
    
    @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
			sessionMap=context.getExternalContext().getSessionMap();

			
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
     
    
    public boolean getIsSearchPropertyShow() {
    	
    	
    	if(viewRootMap.containsKey("procedureType")&& 
    			viewRootMap.get("procedureType").toString().trim().equals(WebConstants.PROCEDURE_TYPE_ISTISNAA_CONTRACT) )
    		return true;
    	else
    		return false;
    }

    public void populateProjectDetails(ProjectView projectView,String tenderId,PropertyView propertyView)
    {
    	
        if(tenderId!=null && tenderId.trim().length()>0)
        	imgSearchProject.setRendered(false);
        if(projectView!=null && projectView.getProjectId()!=null)
        {
	    	this.setProjectNumber(projectView.getProjectNumber());
	    	this.setProjectName(projectView.getProjectName());
	    	this.setProjectStatus(projectView.getProjectStatusId().toString());
	    	this.setProjectType(projectView.getProjectTypeId().toString());
	    	DecimalFormat df = new DecimalFormat();
	    	this.setProjectEstimatedCost(df.format( projectView.getProjectEstimatedCost()));
	    	this.setProjectNumber(projectView.getProjectNumber());
        }
    	this.setPropertyView(propertyView);
    
    	
    }
    @SuppressWarnings("unchecked")
    public void clearProjectDetails()
    {
        
        	imgSearchProject.setRendered(true);
    	this.setProjectNumber("");
    	this.setProjectName("");
    	this.setProjectStatus("");
    	this.setProjectType("");
    	this.setProjectEstimatedCost("");
    	this.setProjectNumber("");
    	this.setProjectEstimatedCost("");
    	this.setPropertyView(null);
    	viewRootMap.remove(ServiceContractPaymentSchTab.ViewRootKeys.PROJECT_ID);
    	
    	
    	
    }
 	public String getProjectName() {
 		if(viewRootMap.containsKey(WebConstants.ProjectDetailsTab.PROJECT_NAME))
 			projectName=viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_NAME).toString();

		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
		if(this.projectName   !=null)
			viewRootMap.put(WebConstants.ProjectDetailsTab.PROJECT_NAME, this.projectName);
	}

	public String getProjectNumber() {
		if(viewRootMap.containsKey(WebConstants.ProjectDetailsTab.PROJECT_NUMBER))
			projectNumber=viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_NUMBER).toString();

		return projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
		if(this.projectNumber   !=null)
			viewRootMap.put(WebConstants.ProjectDetailsTab.PROJECT_NUMBER, this.projectNumber);
	}

	public String getProjectStatus() {
		if(viewRootMap.containsKey(WebConstants.ProjectDetailsTab.PROJECT_STATUS))
			projectStatus=viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_STATUS).toString();

		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
		if(this.projectStatus   !=null)
			viewRootMap.put(WebConstants.ProjectDetailsTab.PROJECT_STATUS, this.projectStatus);
	}

	public String getProjectType() {
		if(viewRootMap.containsKey(WebConstants.ProjectDetailsTab.PROJECT_TYPE))
			projectType=viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_TYPE).toString();

		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
		if(this.projectType   !=null)
			viewRootMap.put(WebConstants.ProjectDetailsTab.PROJECT_TYPE, this.projectType);
	}

	public String getProjectEstimatedCost() {
		if(viewRootMap.containsKey(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST))
			projectEstimatedCost=viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST).toString();

		return projectEstimatedCost;
	}

	public void setProjectEstimatedCost(String projectEstimatedCost) {
		this.projectEstimatedCost = projectEstimatedCost;
		if(this.projectEstimatedCost   !=null)
			viewRootMap.put(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST, this.projectEstimatedCost);
	}

	public LocaleInfo getLocaleInfo()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    return localeInfo;
		
	}
 	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
 	public Boolean getIsArabicLocale()
	{
    	
		return CommonUtil.getIsArabicLocale();
	}
 	public Boolean getIsEnglishLocale()
	{
    	
		return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
	    	
			return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
			 return CommonUtil.getTimeZone();
			
	}
	public String getProjectKey_PageMode()
    {
        	
        	return ProjectSearchBean.Keys.PAGE_MODE;
        	
    }
    public String getProjectKey_PageModeSelectOnPopUp()
    {
        	
        	return ProjectSearchBean.Keys.PAGE_MODE_SELECT_ONE_POPUP;
        	
    }

    public String getProjectKey_AllowedType()
    {
        	
        	return ProjectSearchBean.Keys.ALLOWED_TYPES;
        	
    }
    public String getProjectValue_AllowedTypes()
    {
        	
        	return WebConstants.Project.PROJECT_TYPE_BOT;
        	
    }
	public String getProjectKey_AllowedStatus()
    {
        	
        	return ProjectSearchBean.Keys.ALLOWED_STATUSES;
        	
    }
    public String getProjectValue_AllowedStatuses()
    {
        	
        	return WebConstants.Project.PROJECT_STATUS_NEW+","+WebConstants.Project.PROJECT_STATUS_UNDER_CONTRACTUAL_PROCESS+","+WebConstants.Project.PROJECT_STATUS_ON_CONTRACT;
        	
    }



	public HtmlGraphicImage getImgSearchProject() {
		return imgSearchProject;
	}



	public void setImgSearchProject(HtmlGraphicImage imgSearchProject) {
		this.imgSearchProject = imgSearchProject;
	}



	public HtmlCommandButton getBtnAddProperty() {
		return btnAddProperty;
	}



	public void setBtnAddProperty(HtmlCommandButton btnAddProperty) {
		this.btnAddProperty = btnAddProperty;
	}



	public PropertyView getPropertyView() {
		return propertyView;
	}



	public void setPropertyView(PropertyView propertyView) {
		this.propertyView = propertyView;
	}



	public HtmlGraphicImage getImgSearchProperty() {
		return imgSearchProperty;
	}



	public void setImgSearchProperty(HtmlGraphicImage imgSearchProperty) {
		this.imgSearchProperty = imgSearchProperty;
	}
	
	

	
}
