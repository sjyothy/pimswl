package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class FileBlockingCommentsBean extends AbstractController {

	private static Logger logger = Logger.getLogger(FileBlockingCommentsBean.class);
	
	private static final String BLOCKING_FILE_COMMENTS = "BLOCKING_FILE_COMMENTS";
	private static final String UNBLOCKING_FILE_COMMENTS = "UNBLOCKING_FILE_COMMENTS";
	private String  FILE_TO_BLOCKED = "FILE_TO_BLOCKED";
	private String  FILE_TO_UNBLOCKED = "FILE_TO_UNBLOCKED";
	HtmlInputTextarea txtAreaComments = new HtmlInputTextarea();
	private NotesBean notes;
	protected List<String> errorMessages = new ArrayList<String>();
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public void sendNotesToParent()
	{
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();

		if(txtAreaComments.getValue() != null && StringHelper.isNotEmpty(txtAreaComments.getValue().toString())){
			Object comments = txtAreaComments.getValue();
			if(comments!=null && !comments.toString().trim().equals("") && sessionMap.containsKey(FILE_TO_BLOCKED))
			{
				sessionMap.put(BLOCKING_FILE_COMMENTS, comments.toString());
			}
			else if(comments!=null && !comments.toString().trim().equals("") && sessionMap.containsKey(FILE_TO_UNBLOCKED))
			{
				sessionMap.put(UNBLOCKING_FILE_COMMENTS, comments.toString());
			}
			executeJavascript("sendNotesToParent()");
		}
		else{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
		}
		return;
	}
	
	
	
	public void init() {
		super.init();
	}


	public HtmlInputTextarea getTxtAreaComments() {
		return txtAreaComments;
	}

	public void setTxtAreaComments(HtmlInputTextarea txtAreaComments) {
		this.txtAreaComments = txtAreaComments;
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )	

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
}
