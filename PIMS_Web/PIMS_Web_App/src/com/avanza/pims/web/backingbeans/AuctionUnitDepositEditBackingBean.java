package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.ui.util.ResourceUtil;


public class AuctionUnitDepositEditBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionUnitDepositEditBackingBean.class);
	/**
	 * 
	 */
	
	public AuctionUnitDepositEditBackingBean(){
		
	}
	private AuctionUnitView auctionUnitView = new AuctionUnitView();

	private String PAGE_MODE= "PageMode";
	private String PAGE_MODE_OPENING_PRICE= "OpeningPrice";
	private String PAGE_MODE_AUCTION_UNIT_DEPOSIT_AMOUNT= "DepositAmount";
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	private AuctionUnitView dataItem = new AuctionUnitView();
	private List<AuctionUnitView> dataList = new ArrayList<AuctionUnitView>();

	private HtmlDataTable dataTable = new HtmlDataTable();
	
	private String readonlyStyleClassFixed = "A_RIGHT_NUM";
	private String readonlyStyleClassPerc = "A_RIGHT_NUM";
	
	private String openingPrice = "";
	private String depositAmount = "";
	
	private String fixedValue;
	private String percentage;
	private Double basedOnAmount;
	
	private Double minDeposit;	
	private Double maxDeposit;	
	
	
	HtmlOutputLabel maxDepositLabel = new HtmlOutputLabel();
	HtmlOutputLabel openingPriceLabel = new HtmlOutputLabel();
	HtmlOutputLabel depositAmountLabel = new HtmlOutputLabel();
	HtmlInputText fixedValueField = new HtmlInputText();
	HtmlInputText percentageField = new HtmlInputText();
	HtmlInputText basedOnAmountField = new HtmlInputText();
	HtmlInputText depositAmountField = new HtmlInputText();
	HtmlInputText minDepositField = new HtmlInputText();
	HtmlInputText maxDepositField = new HtmlInputText();
	HtmlSelectOneRadio depositTypeRadio = new HtmlSelectOneRadio();
	HtmlSelectOneMenu basedOnCombo = new HtmlSelectOneMenu();
	
	private String selectedBasedOn;
	private String selectedDepositType;
	private final String FIXED_VALUE = "FIXED";
	private final String PERCENTAGE = "PERCENT";
	
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDepositTypeList() {
		List<SelectItem> cboList = (List<SelectItem>)viewMap.get("depositTypeList");
		SelectItem item = null;
		String itemLabel = "";
		if(cboList==null) {
			itemLabel = getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.FIXED_DEPOSIT);
			cboList = new ArrayList<SelectItem>();
			item = new SelectItem(FIXED_VALUE,itemLabel);
			cboList.add(item);
			itemLabel = getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.PERCENT);
			item = new SelectItem(PERCENTAGE,itemLabel);
			cboList.add(item);
		}
		viewMap.put("depositTypeList", cboList);

		return cboList;
	}

	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
		{			
		  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
		  loggedInUser = user.getLoginId();
		}
		
		return loggedInUser;
	}
	
	
	@SuppressWarnings("unchecked")
	public void saveAuctionUnitPrices(){
		/*Long auctionUnitId = 0L;
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();*/
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		logger.logInfo("saveAuctionUnitPrices() started...");
		
		Boolean isOPeningPriceMode = getIsOPeningPriceMode();
		try{
			if(validatePrices())
			{
				Double depAmt = 0D;

	   		    List<AuctionUnitView> selectedAuctionUnits = (List<AuctionUnitView>)viewMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
	   		    String radioMode = (String)viewMap.get("RADIO_MODE");   		    
	   		    
	   		    
	   		    if(selectedAuctionUnits!=null && radioMode!=null){
					if(radioMode.equals(FIXED_VALUE)){
						depAmt = (Double)viewMap.get(FIXED_VALUE);
						if(depAmt!=null)
							for(AuctionUnitView auctionUnitView: selectedAuctionUnits){
								if(!isOPeningPriceMode)
									auctionUnitView.setDepositAmount(depAmt);
								else
									auctionUnitView.setOpeningPrice(depAmt);
							}
					}
					else if(radioMode.equals(PERCENTAGE)){
						
						Double minAmount = 0D;
			   		    Double maxAmount = 0D;
			   		    
			   		    if(minDepositField.getValue() != null && !minDepositField.getValue().toString().equals(""))
			   		    {
			   		    	minAmount = Double.parseDouble(minDepositField.getValue().toString()); 
			   		    }
			   		    
			   		    if(maxDepositField.getValue() != null && !maxDepositField.getValue().toString().equals(""))
			   		    {
			   		    	maxAmount = Double.parseDouble(maxDepositField.getValue().toString()); 
			   		    }
			   		    
						Double percentage = 0.0D;
			   		    String basedOn = (String) viewMap.get("basedOn");
			   		    if(basedOn!=null){
			   		    	percentage = (Double)viewMap.get(PERCENTAGE);
			   		    	percentage /= 100.00 ;
			   		    }
						if(basedOn!=null && percentage!=null){
							if(basedOn.equals(WebConstants.BASED_ON_ANNUAL_RENT)){
								for(AuctionUnitView auctionUnitView: selectedAuctionUnits){
									depAmt = auctionUnitView.getRentValue();
									depAmt = depAmt*percentage;
									if(depAmt>maxAmount)
										depAmt = maxAmount;
									else if(depAmt<minAmount)
										depAmt = minAmount;
									if(!isOPeningPriceMode)
										auctionUnitView.setDepositAmount(depAmt);
									else
										auctionUnitView.setOpeningPrice(depAmt);
								}
							}
							else if(basedOn.equals(WebConstants.BASED_ON_OPENING_AMOUNT)){
								for(AuctionUnitView auctionUnitView: selectedAuctionUnits){
									depAmt = auctionUnitView.getOpeningPrice();
									depAmt = depAmt*percentage;
									if(depAmt>maxAmount)
										depAmt = maxAmount;
									else if(depAmt<minAmount)
										depAmt = minAmount;
									if(!isOPeningPriceMode)
										auctionUnitView.setDepositAmount(depAmt);
									else
										auctionUnitView.setOpeningPrice(depAmt);
								}
							}
						}
					}
				}
	   		    
	   		    sessionMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedAuctionUnits);
	   		    
	   		    if(!getIsOPeningPriceMode())
	   		    	sessionMap.put("POPUP_MODE",WebConstants.DEPOSIT_AMOUNT);
	   		    else
	   		    	sessionMap.put("POPUP_MODE",WebConstants.OPENINIG_PRICE);
	   		    
	   		    FacesContext facesContext = FacesContext.getCurrentInstance();
	   		
		        String javaScriptText = "javascript:closeWindowSubmit();";
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			
			logger.logInfo("saveAuctionUnitPrices() completed successfully!!!");
		}
    	/*catch(PimsBusinessException exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}*/
    	catch(Exception exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}
    	
	}
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	public String getInvalidMessage(String field){
		logger.logInfo("getInvalidMessage(String) started...");
		StringBuffer message = new StringBuffer("");
		
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}
	
	public Number getNumberValue(String doubleStr, String pattern) throws Exception{
		logger.logInfo("getNumberValue started...");
		Number numberVal = null;
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
			logger.logInfo("getNumberValue completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("getNumberValue crashed!!!", exception);
			throw exception;
		}
		return numberVal;
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}
	
	public Boolean validatePrices(){
		logger.logInfo("validatePrices() started...");
		Boolean validated = true;
		
    	try{
    		String pattern = getNumberFormat();
    		String radioMode = (String)viewMap.get("RADIO_MODE");
    		Boolean validateFixedPrice = false;
    		Boolean validatePercentage = false;

    		if(radioMode!=null){
    			if(radioMode.equals(FIXED_VALUE)){
    				validateFixedPrice = true;
    			}
    			else if(radioMode.equals(PERCENTAGE)){
    				validatePercentage = true;
    			}
    		}    		
    		
    		if(validateFixedPrice){
	    		Number fixedPrice = 0;
	    		try{
	    			String fixedValStr = getFixedValue();
	    			fixedPrice = getNumberValue(fixedValStr,pattern);
	    			String fixedStr = fixedValStr.replace(',', '0');
	    			char temp = fixedStr.toLowerCase().charAt(fixedStr.length()-1);
	    			if(temp>='a' && temp<='z')
	    				throw new Exception();
	    			Double dbl = new Double(fixedStr);
	    		} catch(Exception e){
	    			errorMessages.clear();
	    			validated = false;
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.FIXED_DEPOSIT));
	    			return validated;
	    		}
    		
	    		if(fixedPrice.doubleValue()<0D)
	    		{
	    			errorMessages.clear();
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.FIXED_DEPOSIT));
	    			validated = false;
	    			return validated;
	    		}
	    		viewMap.put(FIXED_VALUE, fixedPrice.doubleValue());
    		}
    		else if(validatePercentage){
        		String basedOn = (String)basedOnCombo.getValue();
    			if(basedOn==null || basedOn.equals("none")){
    				errorMessages.clear();
	    			validated = false;
	    			errorMessages.add(getInvalidMessage(MessageConstants.PrepareAuction.BASED_ON_NOT_SELECTED));
	    			return validated;
    			}
    			
	    		Number percent = 0;
	    		try{
	    			String fixedValStr = getPercentage();
	    			percent = getNumberValue(fixedValStr,pattern);
	    			String fixedStr = fixedValStr.replace(',', '0');
	    			char temp = fixedStr.toLowerCase().charAt(fixedStr.length()-1);
	    			if(temp>='a' && temp<='z')
	    				throw new Exception();
	    			Double dbl = new Double(fixedStr);
	    		} catch(Exception e){
	    			errorMessages.clear();
	    			validated = false;
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.PERCENT));
	    			return validated;
	    		} 		
    		
	    		if(percent.doubleValue()<0D)
	    		{
	    			errorMessages.clear();
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.PERCENT));
	    			validated = false;
	    			return validated;
	    		}
	    		viewMap.put(PERCENTAGE, percent.doubleValue());
    		}
    		
	        logger.logInfo("validatePrices() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("validatePrices() crashed ", exception);
		}
		return validated;
	}
	
	
    public void cancel(ActionEvent event) {
    	logger.logInfo("cancel() started...");
        FacesContext facesContext = FacesContext.getCurrentInstance();

        String javaScriptText = "window.close();";
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        logger.logInfo("cancel() completed successfully!!!");
    }

    public void done(ActionEvent event) {
    	logger.logInfo("done() started...");
    	try{
    		
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	
	        String javaScriptText = "window.closeWindowSubmit();";
	        
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("done() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("done() crashed ", exception);
		}
    }
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
    
	public void radioValueChanged(ValueChangeEvent event){
		try {
			setValues();
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RADIO");
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	public void comboValueChanged(ValueChangeEvent event){
		try {
			setValues();
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>COMBO");
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
	private void getPaymentConfig(){
		logger.logInfo("getPaymentConfig() started...");
		try
		{
			UtilityServiceAgent usa = new UtilityServiceAgent();
			PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
			paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_AUCTION_DEPOSIT_ID);
			paymentConfigurationView.setProcedureTypeKey(WebConstants.PROCEDURE_TYPE_PREPARE_AUCTION);
			List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
			viewMap.put("PaymentConfigList", paymentConfigList);
			logger.logInfo("getPaymentConfig() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getPaymentConfig() crashed ", exception);
		}
	}

	private void generatePaymentConfigMap(){
		logger.logInfo("generatePaymentConfigMap() started...");
		try
		{
			List<PaymentConfigurationView> paymentConfigList = (List<PaymentConfigurationView>)viewMap.get("PaymentConfigList");
			Map<String,PaymentConfigurationView> paymentConfigMap = new HashMap<String, PaymentConfigurationView>(0);
			if(paymentConfigList!=null && paymentConfigList.size()>0){
				for(PaymentConfigurationView temp: paymentConfigList){
					if(temp.getBasedOnTypeKey()!=null)
						paymentConfigMap.put(temp.getBasedOnTypeKey(),temp);
					else
						paymentConfigMap.put(FIXED_VALUE,temp);
				}
				viewMap.put("PaymentConfigMap", paymentConfigMap);
			}
			
			logger.logInfo("generatePaymentConfigMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("generatePaymentConfigMap() crashed ", exception);
		}
	}
	
	public boolean getIsOPeningPriceMode(){
		 
		 if(viewMap.containsKey(PAGE_MODE) && viewMap.get(PAGE_MODE)!=null && viewMap.get(PAGE_MODE).toString().trim().equalsIgnoreCase(PAGE_MODE_OPENING_PRICE))
			 return true;
		 else 
			 return false;
	 }
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{				
				HttpServletRequest request =
					 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
				
				
				if(request.getParameter(PAGE_MODE)!=null)
				 {
					 viewMap.put(PAGE_MODE, request.getParameter(PAGE_MODE).toString());					  
				 }
				 else if(getRequestParam(PAGE_MODE)!=null)
				 {
					 viewMap.put(PAGE_MODE, getRequestParam(PAGE_MODE).toString());					  
				 }
				
				depositTypeRadio.setValue(FIXED_VALUE);
				viewMap.put("RADIO_MODE", FIXED_VALUE);	
				
				if(!getIsOPeningPriceMode())
				{
					getPaymentConfig();
					generatePaymentConfigMap();					
				}		

				if(sessionMap.containsKey(WebConstants.CHOSEN_AUCTION_UNITS))
				{
					List<AuctionUnitView> selectedAuctionUnits = (List<AuctionUnitView>)sessionMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
					viewMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedAuctionUnits);
					sessionMap.remove(WebConstants.CHOSEN_AUCTION_UNITS);
				}
				setValues();
				
			}
			
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	public void setValues(){		
		
			String depositCalculationType = (String)depositTypeRadio.getValue();
			Map<String, PaymentConfigurationView> paymentConfigMap = getPaymentConfigMap();
			if(depositCalculationType!=null){
				if(depositCalculationType.equals(FIXED_VALUE))
				{
					viewMap.put("RADIO_MODE", FIXED_VALUE);
		
					Double amount = new Double(0);
					Double minAmount = new Double(0);
					Double maxAmount = new Double(0);
					
					if(paymentConfigMap != null && paymentConfigMap.containsKey(FIXED_VALUE))
					{
						PaymentConfigurationView fixedAmountConfiguration = paymentConfigMap.get(FIXED_VALUE);
						amount = fixedAmountConfiguration.getAmount();
						minAmount =fixedAmountConfiguration.getMinAmount(); 
						maxAmount = fixedAmountConfiguration.getMaxAmount();
					}
					//setFixedValue(amount.toString());
					//setMinDeposit(minAmount);
					minDepositField.setValue(minAmount);
					maxDepositField.setValue(maxAmount);					
				}
				else if(depositCalculationType.equals(PERCENTAGE)){
					viewMap.put("RADIO_MODE", PERCENTAGE);
					String basedOn = (String)basedOnCombo.getValue();
					if(basedOn!=null && !basedOn.equals("none")){
						
						if(basedOn.equals(WebConstants.BASED_ON_ANNUAL_RENT)){
							viewMap.put("basedOn", WebConstants.BASED_ON_ANNUAL_RENT);
						}
						else if(basedOn.equals(WebConstants.BASED_ON_OPENING_AMOUNT)){
							viewMap.put("basedOn", WebConstants.BASED_ON_OPENING_AMOUNT);
						}
						
						if(paymentConfigMap != null && paymentConfigMap.containsKey(basedOn))
						{
							PaymentConfigurationView percentConfiguration = paymentConfigMap.get(basedOn);
							
							if(getPercentage()==null)
								setPercentage(percentConfiguration.getAmount().toString());
							else
								setPercentage(percentageField.getValue().toString());
							
							//setMinDeposit(percentConfiguration.getMinAmount());
							//setMaxDeposit(percentConfiguration.getMaxAmount());
							minDepositField.setValue(percentConfiguration.getMinAmount());
							maxDepositField.setValue(percentConfiguration.getMaxAmount());									
						}
					}
				}
			}		
	}
	
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		
		applyMode();
	}

	private void applyMode(){
		String radioMode = (String)viewMap.get("RADIO_MODE");
		if(radioMode!=null){
			basedOnAmountField.setRendered(false);
			depositAmountField.setRendered(false);
			
			if(radioMode.equals(FIXED_VALUE)){
				readonlyStyleClassFixed = "A_RIGHT_NUM";
				readonlyStyleClassPerc = "A_RIGHT_NUM_READONLY"; 
				fixedValueField.setDisabled(false);
				percentageField.setDisabled(true);
				basedOnAmountField.setDisabled(true);
				depositAmountField.setDisabled(true);				
				basedOnCombo.setDisabled(true);
				//minDepositField.setReadonly(false);
				maxDepositField.setReadonly(false);
			}
			else if(radioMode.equals(PERCENTAGE)){
				readonlyStyleClassFixed = "A_RIGHT_NUM_READONLY";
				readonlyStyleClassPerc = "A_RIGHT_NUM";
				fixedValueField.setDisabled(true);
				percentageField.setDisabled(false);
				basedOnAmountField.setDisabled(false);
				depositAmountField.setDisabled(false);				
				basedOnCombo.setDisabled(false);
			}
			else{

			}
		}
		
	}
	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		isEnglishLocale =  view.getLocale().toString().equals("en");
		return isEnglishLocale;
	}


	/**
	 * @return the auctionUnitView
	 */
	public AuctionUnitView getAuctionUnitView() {
		return auctionUnitView;
	}


	/**
	 * @param auctionUnitView the auctionUnitView to set
	 */
	public void setAuctionUnitView(AuctionUnitView auctionUnitView) {
		this.auctionUnitView = auctionUnitView;
	}



	/**
	 * @return the openingPrice
	 */
	public String getOpeningPrice() {
		return openingPrice;
	}


	/**
	 * @param openingPrice the openingPrice to set
	 */
	public void setOpeningPrice(String openingPrice) {
		this.openingPrice = openingPrice;
	}


	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}


	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}


	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}


	public AuctionUnitView getDataItem() {
		return dataItem;
	}


	public void setDataItem(AuctionUnitView dataItem) {
		this.dataItem = dataItem;
	}


	public List<AuctionUnitView> getDataList() {
		dataList = (List<AuctionUnitView>)viewMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
		return dataList;
	}


	public void setDataList(List<AuctionUnitView> dataList) {
		if(dataList!=null)
			viewMap.put(WebConstants.CHOSEN_AUCTION_UNITS,dataList);
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}


	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public HtmlOutputLabel getOpeningPriceLabel() {
		return openingPriceLabel;
	}


	public void setOpeningPriceLabel(HtmlOutputLabel openingPriceLabel) {
		this.openingPriceLabel = openingPriceLabel;
	}


	public HtmlOutputLabel getDepositAmountLabel() {
		return depositAmountLabel;
	}


	public void setDepositAmountLabel(HtmlOutputLabel depositAmountLabel) {
		this.depositAmountLabel = depositAmountLabel;
	}


	public HtmlInputText getFixedValueField() {
		return fixedValueField;
	}


	public void setFixedValueField(HtmlInputText fixedValueField) {
		this.fixedValueField = fixedValueField;
	}


	public HtmlInputText getPercentageField() {
		return percentageField;
	}


	public void setPercentageField(HtmlInputText percentageField) {
		this.percentageField = percentageField;
	}


	public HtmlInputText getBasedOnAmountField() {
		return basedOnAmountField;
	}


	public void setBasedOnAmountField(HtmlInputText basedOnAmountField) {
		this.basedOnAmountField = basedOnAmountField;
	}


	public HtmlSelectOneMenu getBasedOnCombo() {
		return basedOnCombo;
	}


	public void setBasedOnCombo(HtmlSelectOneMenu basedOnCombo) {
		this.basedOnCombo = basedOnCombo;
	}


	public HtmlInputText getDepositAmountField() {
		return depositAmountField;
	}


	public void setDepositAmountField(HtmlInputText depositAmountField) {
		this.depositAmountField = depositAmountField;
	}


	public HtmlOutputLabel getMaxDepositLabel() {
		return maxDepositLabel;
	}


	public void setMaxDepositLabel(HtmlOutputLabel maxDepositLabel) {
		this.maxDepositLabel = maxDepositLabel;
	}


	public HtmlInputText getMinDepositField() {
		return minDepositField;
	}


	public void setMinDepositField(HtmlInputText minDepositField) {
		this.minDepositField = minDepositField;
	}


	public HtmlInputText getMaxDepositField() {
		return maxDepositField;
	}


	public void setMaxDepositField(HtmlInputText maxDepositField) {
		this.maxDepositField = maxDepositField;
	}
	
	public String getSelectedDepositType() {
		return selectedDepositType;
	}


	public void setSelectedDepositType(String selectedDepositType) {
		this.selectedDepositType = selectedDepositType;
	}


	public HtmlSelectOneRadio getDepositTypeRadio() {
		return depositTypeRadio;
	}


	public void setDepositTypeRadio(HtmlSelectOneRadio depositTypeRadio) {
		this.depositTypeRadio = depositTypeRadio;
	}


	public String getFixedValue() {
		return fixedValue;
	}


	public void setFixedValue(String fixedValue) {
		this.fixedValue = fixedValue;
	}


	public String getPercentage() {
		return percentage;
	}


	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}


	public String getSelectedBasedOn() {
		return selectedBasedOn;
	}


	public void setSelectedBasedOn(String selectedBasedOn) {
		this.selectedBasedOn = selectedBasedOn;
	}


	public Double getBasedOnAmount() {
		return basedOnAmount;
	}


	public void setBasedOnAmount(Double basedOnAmount) {
		this.basedOnAmount = basedOnAmount;
	}

	public Map<String, PaymentConfigurationView> getPaymentConfigMap() {
		Map<String,PaymentConfigurationView> paymentConfigMap = (Map<String,PaymentConfigurationView>)viewMap.get("PaymentConfigMap");
		return paymentConfigMap;
	}


	public void setPaymentConfigMap(Map<String, PaymentConfigurationView> paymentConfigMap) {
		if(paymentConfigMap!=null)
			viewMap.put("PaymentConfigMap",paymentConfigMap);
	}


	public String getReadonlyStyleClassFixed() {
		return readonlyStyleClassFixed;
	}


	public void setReadonlyStyleClassFixed(String readonlyStyleClassFixed) {
		this.readonlyStyleClassFixed = readonlyStyleClassFixed;
	}


	public String getReadonlyStyleClassPerc() {
		return readonlyStyleClassPerc;
	}


	public void setReadonlyStyleClassPerc(String readonlyStyleClassPerc) {
		this.readonlyStyleClassPerc = readonlyStyleClassPerc;
	}


}