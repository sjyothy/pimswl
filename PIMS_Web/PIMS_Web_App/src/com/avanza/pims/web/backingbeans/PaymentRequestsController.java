package com.avanza.pims.web.backingbeans;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.memsrecievesharesbpel.proxy.MEMSReceiveMatureSharesBPELPortClient;
import com.avanza.pims.bpel.proxy.MEMSBlockingBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.entity.Request;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SessionDetailView;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

/**
 * @author farhan.amin
 *
 */
@SuppressWarnings("unchecked")
public class PaymentRequestsController extends AbstractMemsBean {
	
	private static final long serialVersionUID = 1L;
	private static final Long INVALID_STATUS = -1L;
	private static final String BENEF_NO_COST_CENTER_MAP			= "BENEF_NO_COST_CENTER_MAP";
	private static final String DELETED_DISBURSEMENT	= "DELETED";
	private static final String DISBURSEMENTS			= "DISB_DETAILS";
	private static final String BENENEFICIARY_LIST 		= "BENENEFICIARY_LIST";
	private static final String IS_BENEF_SRCH			= "BeneficiarySearch";	
	private static final String PROCEDURE_TYPE 			= "procedureType";
	private static final String EXTERNAL_ID 			= "externalId";
	private static final String CSS_READONLY			= "READONLY";
	private static final String IS_UPDATE				= "IS_UPDATE";
	private static final String IS_FILE_ASSOCIATED		= "IsFileAssociated";
	private static final String CMB_DEFAULT_VAL			= "-1";
	private static final String PERSON_BALANCE_MAP		= "personBalance";
	private static final String PERSON_BLOCKING_MAP			= "personBlocking";
	private final String SESSION_DETAIL_VIEW_LIST = "SESSION_DETAIL_VIEW_LIST";
	private static final String ALL_USER_GROUPS 			= "AllUserGroups";
	private static final String USER_IN_GROUP 				= "UserInGroup";
	@SuppressWarnings("unused")
	private static final String SELECTED_TAB			= "SelectedTab";
	@SuppressWarnings("unused")
	private static final String TAB_ID_APP				= "applicationTab";
	private static final String TAB_ID_DETAILS			= "disbDetailsTab";
	private static final String TAB_ID_SESSION_DETAIL	= "sessionTab";
	private static final String TAB_ID_FINANCE_TAB		= "financeTab";
	private static final String TAB_ID_REVIEW_TAB		= "reviewTab";
		 
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnRequestId;
	private String totalDisbursementCount;
	private HtmlInputText blockingBalance = new HtmlInputText();
	protected HtmlTabPanel richPanel = new HtmlTabPanel();
	private HtmlInputText txtAmount = new HtmlInputText();
	private HtmlInputTextarea txtDescription = new HtmlInputTextarea();
	private HtmlSelectOneMenu cmbPaymentCategories = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbBeneficiaryList = new HtmlSelectOneMenu();
	private HtmlInputText txtFileNumber = new HtmlInputText();
	private HtmlDataTable disbursementTable = new HtmlDataTable();
	private HtmlSelectOneMenu cmbPaymentSource = new HtmlSelectOneMenu();
	private HtmlInputText txtExternalPartyName = new HtmlInputText();
	private HtmlInputText txtBalance = new HtmlInputText();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlFileType = new HtmlInputText();
	private HtmlInputText htmlFileStatus  = new HtmlInputText();
	private HtmlInputText htmlFileDate = new HtmlInputText();
    private HtmlInputTextarea htmlRFC = new HtmlInputTextarea();
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    private static long STATUS_FINANCE_REJECTED_ID=90056l;
	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlFileType() {
		return htmlFileType;
	}

	public void setHtmlFileType(HtmlInputText htmlFileType) {
		this.htmlFileType = htmlFileType;
	}

	public HtmlInputText getHtmlFileStatus() {
		return htmlFileStatus;
	}

	public void setHtmlFileStatus(HtmlInputText htmlFileStatus) {
		this.htmlFileStatus = htmlFileStatus;
	}

	public HtmlInputText getHtmlFileDate() {
		return htmlFileDate;
	}

	public void setHtmlFileDate(HtmlInputText htmlFileDate) {
		this.htmlFileDate = htmlFileDate;
	}

	public HtmlInputText getTxtBalance() {
		return txtBalance;
	}

	public void setTxtBalance(HtmlInputText txtBalance) {
		this.txtBalance = txtBalance;
	}

	public HtmlInputText getTxtExternalPartyName() {
		return txtExternalPartyName;
	}

	public void setTxtExternalPartyName(HtmlInputText txtExternalPartyName) {
		this.txtExternalPartyName = txtExternalPartyName;
	}

	public HtmlSelectOneMenu getCmbPaymentSource() {
		return cmbPaymentSource;
	}

	public void setCmbPaymentSource(HtmlSelectOneMenu cmbPaymentSource) {
		this.cmbPaymentSource = cmbPaymentSource;
	}

	@Override
	public void init() {
		super.init();		
		try {		
	    	viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_TYPE_MEMS_PAYMENTS);
	    	viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_MEMS_PAYMENTS);
	    	
	    	if( !isPostBack() ) {	    		
	    		
	    		ApplicationBean application = new ApplicationBean();
	    		
		    	List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_NEW, CommonUtil.
		    						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_APPROVED, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVED).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_REJECTED, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_REJECTED).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_CANCELED, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_CANCELED).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_REVIEW_REQUIRED).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_REVIEW_DONE, CommonUtil.
						getIdFromType(ddvList, WebConstants.REQUEST_STATUS_REVIEW_DONE).getDomainDataId());
		    	
		    	viewMap.put(WebConstants.REQUEST_STATUS_JUDGE_DONE, CommonUtil.
		    			getIdFromType(ddvList, WebConstants.REQUEST_STATUS_JUDGE_DONE).getDomainDataId());

		    	
		    	viewMap.put(WebConstants.MemsPaymentCategories.PAYMENT_CATEGORY,
		    			application.getDomainDataList(WebConstants.MemsPaymentCategories.PAYMENT_CATEGORY));
		    	
		    	List<SelectItem> source = application.getDomainDataList(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		    	Long aidId = getDomainDataId(WebConstants.PaymentSrcType.DOMAIN_KEY, WebConstants.PaymentSrcType.AID);
		    	
		    	for(SelectItem item : source) {
		    		if( item.getValue().equals(aidId.toString())) {
		    			source.remove(item);
		    			break;
		    		}
		    	}
		    	viewMap.put(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE,
		    			application.getDomainDataList(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE));
		    	
		    	List<DisbursementDetailsView> toDeleteList = new ArrayList<DisbursementDetailsView>();
				viewMap.put(DELETED_DISBURSEMENT, toDeleteList);
														    		 
				RequestView reqView = (RequestView ) getRequestMap().get(WebConstants.REQUEST_VIEW);
				
				InheritanceFileView inheritanceFile = (InheritanceFileView) getRequestMap()
													.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				
				BeneficiaryGridView beneficiaryView = (BeneficiaryGridView) getRequestMap()
															.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
				
				UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				if( null!=userTask ) {
					sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK);
				}
				
				handleFinanceTabVisibility();
				
				if( null!=reqView ) {
					initFromRequestSearch(reqView);
				} else if ( null!=inheritanceFile ) {
					initFromInheritanceFileSearch(inheritanceFile);
				} else if ( null!=beneficiaryView ) {
					initFromBeneficiarySearch(beneficiaryView);
				} else if ( null!=userTask ) {
					initFromUserTask(userTask);
				}
				//loadUserGroupsInViewRoot();
				//setUserInGroup();
				
	    	}
	    	if( 
	    		( 
	    		  getIsRejectedResubmitted() ||
	    		  getIsSuspended()
	    		)
	    		&&  getRequestView().getMemsNolReason() != null 
	    	  )
			{
				errorMessages.add(getRequestView().getMemsNolReason() );
				
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	


	private void initFromRequestSearch(RequestView reqView) throws Exception 
	{
		setRequestView(reqView);
		Long inhFileId = reqView.getInheritanceFileId();
		refreshTabs(reqView);
		//TODO: if its not from request search populate with the beneficiary combo box 
		if( null!=inhFileId ) 
		{
			loadBeneficiaryList(inhFileId);
			viewMap.put(IS_FILE_ASSOCIATED, "TRUE");
		}
		else 
		{
			//the request is not associated with the inheritance file
			// there should be only one disbursement and one request beneficiary
			List<DisbursementDetailsView> view = getDisbursementDetails(); 
			if( null!=view && 1==view.size()) 
			{
				List<DisbursementRequestBeneficiaryView> reqBenef = view.get(0).getRequestBeneficiaries();				
				DisbursementRequestBeneficiaryView next = reqBenef.get( 0);
				BeneficiaryGridView gridView = new BeneficiaryGridView();
				gridView.setPersonName(next.getName());
				gridView.setBeneficiaryPersonId(next.getPersonId());
				loadBeneficiaryList(gridView);
						

			}
		}
		loadAttachmentsAndComments(reqView.getRequestId());
		loadInhFileParamTextBoxes(reqView.getInheritanceFileId()!=null?reqView.getInheritanceFileId():null);
		showTab(getRequestView().getStatusId());
	}
	
	private void showTab(final Long statusId) 
	{
		if( statusId.equals(getStatusApprovalRequiredId())   || statusId.equals( STATUS_FINANCE_REJECTED_ID ) ) 
		{
			richPanel.setSelectedTab( TAB_ID_DETAILS);
		}
		else if( statusId.equals( getStatusApprovedId())  || statusId.equals( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID) ) 
		{
			richPanel.setSelectedTab( TAB_ID_FINANCE_TAB);
		}
		else if ( statusId.equals( getStatusJudgeSessionReqId()) || statusId.equals( getStatusJudgeSessionDoneId()) ) 
		{
			richPanel.setSelectedTab( TAB_ID_SESSION_DETAIL);
		}
		else if ( statusId.equals( getStatusReviewReqId()  )   || statusId.equals( getStatusReviewDoneId() )) 
		{
			richPanel.setSelectedTab( TAB_ID_REVIEW_TAB);
		}
		
	}
	
	private void initFromUserTask(UserTask userTask) throws Exception 
	{
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		Map taskAttributes =  userTask.getTaskAttributes();				
		if( null!=taskAttributes.get(WebConstants.UserTasks.REQUEST_ID) ) 
		{			
			Long reqId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			RequestService reqSrv = new RequestService();
			RequestView requestView= reqSrv.getRequestById(reqId);					
			initFromRequestSearch(requestView);
			loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
		}
	}
 
	private void refreshTabs(RequestView reqView) {
		if( null==reqView && null==reqView.getApplicantView()) {
			throw new IllegalArgumentException();		
		}
		
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, reqView.getRequestDate());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, reqView.getRequestNumber());
		
		PersonView applicant = reqView.getApplicantView();
		viewMap.put(WebConstants.LOCAL_PERSON_ID, applicant.getPersonId());
		
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,applicant.getPersonId());
		
		//TODO: I am getting it null when saving
		//FIXME proper fix required 
		if( null!=applicant.getPersonFullName() ) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME, applicant.getPersonFullName());
		}
		
		if( null!=applicant.getCellNumber() ) { 
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, applicant.getCellNumber());
		}
				
		String desc = reqView.getDescription();
		
		if( null!=desc && 0!=desc.trim().length()) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, reqView.getDescription());
		}
		
		if( CommonUtil.getIsEnglishLocale()) {			
			//viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, applicant.getPersonTypeEn());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusEn());
		} 
		else if( CommonUtil.getIsArabicLocale() ) {
			//viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, applicant.getPersonTypeAr());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusAr());
		}	
		
		if( !getIsNewStatus() ) { 
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");			
		}
		
		List<DisbursementDetailsView> detailsView = reqView.getDisubrsementDetailsView();
		if( null==detailsView ) {
			detailsView = new ArrayList<DisbursementDetailsView>();
		}
		
		if( getIsApprovalRequired() || getIsJudgeSessionDone()) {
			cmbPaymentSource.setDisabled(true);
			txtExternalPartyName.setDisabled(true);
			cmbPaymentSource.setStyleClass(CSS_READONLY);
			txtExternalPartyName.setStyleClass(CSS_READONLY);
			cmbPaymentSource.setValue(CMB_DEFAULT_VAL);
		}
		setDisbursementDetails(detailsView);
//		viewMap.put(DISBURSEMENTS, detailsView);
		
		if( getIsApproved() || getIsDisbursementRequired() ) 
		{
			populateFinanceData();
		}				
		handleAttachmentCommentTabStatus(reqView);		
	}


	private void loadUserGroupsInViewRoot() throws Exception {
		if (!viewMap.containsKey(ALL_USER_GROUPS)) {
			List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
			List<SelectItem> listUserGrps = new ArrayList<SelectItem>(
					allUserGrps.size());
			for (UserGroup singleGrp : allUserGrps) {
				listUserGrps.add(new SelectItem(singleGrp.getUserGroupId(),
												isEnglishLocale()? singleGrp.getPrimaryName(): singleGrp.getSecondaryName()));
			}
			Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
			viewMap.put(ALL_USER_GROUPS, listUserGrps);
		}
	}

	public List<SelectItem> getAllUserGroups() {
		if (viewMap.containsKey(ALL_USER_GROUPS)) {
			return (List<SelectItem>) viewMap.get(ALL_USER_GROUPS);
		}
		return new ArrayList<SelectItem>(0);
	}

	@Override
	public void prerender() 
	{
		super.prerender();
		try
		{
			readParams();
			getErrorMessages();
			if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS) ) {
				List<String> sucMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);
				successMessages.addAll(sucMsgs);
				viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);
			}
			if (viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS)) {				
				List<String> errMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);					
				errorMessages.addAll(errMsgs);
				viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);
			}
			if(viewMap.containsKey(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES))
			{
				errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES) );
				viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES);
				
			}
			if(viewMap.containsKey(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES))
			{
				errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES) );
				viewMap.remove(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES);
				
			}
			if (  getIsReviewRequired()  )
			{
				viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			}
			if( 	!isTaskAvailable() && 
					( 
					  getIsApprovalRequired() || getIsReviewRequired() || 
					  getIsReviewed() || getIsFinanceRejected() || getIsDisbursementRequired() || 
					  getIsJudgeSessionDone() || getIsJudgeSessionRequired() 
					)
			 )
			{
				if( viewMap.remove("fetchTaskNotified") == null  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
					viewMap.put("fetchTaskNotified", true);
				}
			}
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in prerender()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public Boolean getIsUserInGroup() {
		String userInGrp = ((String) viewMap.get(USER_IN_GROUP));
		if (null == userInGrp) {
			return false;
		}
		return userInGrp.equals("TRUE");
	}
	public void setUserInGroup() {
		if (getIsReviewRequired()) {
			User loggedInUser = getLoggedInUserObj();
			if (loggedInUser.hasUserGroup(getRequestView().getMemsNolReviewGrpId())) {
				viewMap.put(USER_IN_GROUP, "TRUE");
			}
		}
	}

	private void readParams() throws Exception
	{
		if( hdnPersonId!=null && !hdnPersonId.equals("")) {						
			RequestView reqView = getRequestView();
			PersonView applicant = reqView.getApplicantView();			
			if( null!=applicant) {
				applicant.setPersonId(Long.parseLong(hdnPersonId));
			} else {
				applicant = new PersonView();
				applicant.setPersonId(Long.parseLong(hdnPersonId));
				reqView.setApplicantView(applicant); 				
			}					
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}
		
		if( hdnPersonName!=null && !hdnPersonName.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
		}
		
		if( null!=hdnCellNo && !hdnCellNo.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, hdnCellNo);
		}
		
		if( null!=hdnPersonType && !hdnPersonType.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, hdnPersonType);
		}
		
	}
	
	public void setRequestView(RequestView reqView) {
		@SuppressWarnings("unused")
		RequestView oldView = (RequestView) viewMap.remove(WebConstants.REQUEST_VIEW);
		oldView = null;
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
	}
	
	public RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}
	private void storeBeneficiaryBalanceAmountInMap( Long beneficiaryId ) throws Exception
	   {
		   if(  !getBalanceMap().containsKey(beneficiaryId  ) )
		   {
//		    PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
			Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
			Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryId,getRequestView().getInheritanceFileId());
//			Double amount =  PersonalAccountTrxService .getAmount( beneficiaryId  );
			personBalanceMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
			handleBeneficiaryChange();				
		   }
		   
	   }
	
	private void storeBeneficiaryBlockingAmountInMap( Long beneficiaryId ) throws Exception
	   {
		   if(  !getBlockingMap().containsKey(beneficiaryId  ) )
		   {
		    
			Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
			Double amount =  BlockingAmountService.getBlockingAmount(  beneficiaryId , null, null, null, null );
			personBlockingMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
			handleBeneficiaryChange();				
		   }
		   
	   }
	public void initFromBeneficiarySearch(BeneficiaryGridView beneficiaryGridView) throws Exception {
		logger.logInfo("[initFromBeneficiarySearch() .. Starts]");
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();
			
		Collections.sort(disbDetails, new ListComparator("createdBy",false));							
		setTotalRows(disbDetails.size());
		
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date());				
		reqView.setCreatedOn(new Date());
		
		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.PAYMENT_REQUEST_ID);
		reqView.setStatusId(INVALID_STATUS);		
		reqView.setDisubrsementDetailsView(disbDetails);
		setDisbursementDetails(disbDetails);	
//		viewMap.put(DISBURSEMENTS, disbDetails);
		setRequestView(reqView);
		cmbBeneficiaryList.setValue(beneficiaryGridView.getBeneficiaryPersonId().toString());
		if( beneficiaryGridView.getCostCenter() != null && beneficiaryGridView.getCostCenter().trim().length() > 0 )
		{
			storeBeneficiaryBalanceAmountInMap(beneficiaryGridView.getBeneficiaryPersonId());
		}
		storeBeneficiaryBlockingAmountInMap(beneficiaryGridView.getBeneficiaryPersonId());
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		loadBeneficiaryList(beneficiaryGridView);
		logger.logInfo("[initFromBeneficiarySearch() .. Ends]");
	}
	
	public void initFromInheritanceFileSearch(InheritanceFileView inheritanceFileView) throws Exception {		
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();
			
		Collections.sort(disbDetails, new ListComparator("createdBy",false));							
		setTotalRows(disbDetails.size());
		
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date());				
		reqView.setCreatedOn(new Date());
		
		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.PAYMENT_REQUEST_ID);
		reqView.setStatusId(INVALID_STATUS);		
		reqView.setDisubrsementDetailsView(disbDetails);
		
		setDisbursementDetails(disbDetails);
//		viewMap.put(DISBURSEMENTS, disbDetails);
					
		Long fileId = inheritanceFileView.getInheritanceFileId();
		reqView.setInheritanceFileId(fileId);
		
				
		reqView.setInheritanceFileView(inheritanceFileView);		
		setRequestView(reqView);	
				
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		loadBeneficiaryList(fileId);
		loadInhFileParamTextBoxes(fileId);
		viewMap.put(IS_FILE_ASSOCIATED, "TRUE");				
	}
	
	private void loadInhFileParamTextBoxes(Long fileId) throws Exception{
			if (fileId != null) 
			{
				InheritanceFileView inhFile = new UtilityService()
						.getInheritanceFileById(fileId);
				htmlFileNumber.setValue(inhFile.getFileNumber());
				DomainDataView domainData = new UtilityService().getDomainDataByDomainDataId(inhFile.getStatusId() );
				DomainDataView domainDataFileType = new UtilityService().getDomainDataByDomainDataId(new Long( inhFile.getFileTypeId() ) );
				
				PropertyService service = new PropertyService();
//				PersonView personView=service.getPersonById( new Long (inhFile.getFilePersonId() ) );
//				
//				@SuppressWarnings("unused")
//				String completeName = (personView.getFirstName()!=null  ? personView.getFirstName():"" )
//										+(personView.getMiddleName()!=null  ? personView.getMiddleName():"" )
//										 + (personView.getLastName()!=null  ? personView.getLastName():"");
				
				txtFileOwner.setValue( inhFile.getFilePerson() );
				if (CommonUtil.getIsEnglishLocale()) 
				{
					htmlFileStatus.setValue(domainData.getDataDescEn());
					htmlFileType.setValue(domainDataFileType.getDataDescEn());
					htmlFileDate.setValue(inhFile.getCreatedOn());
//					htmlFilePerson.setValue(completeName); 
					
				} else {
					htmlFileStatus.setValue(domainData.getDataDescAr());
					htmlFileType.setValue(domainDataFileType.getDataDescAr());
					htmlFileDate.setValue(inhFile.getCreatedOn());
					
					
//					htmlFilePerson.setValue(completeName); 
				}

			}

	}
	@SuppressWarnings ("unchecked")	
	private void loadBeneficiaryList(BeneficiaryGridView beneficiaryGridView) throws Exception 
	{
		Map<Long, String> benefNoCostCenterMap = new HashMap<Long, String>();
		ArrayList<SelectItem> beneficiary = new ArrayList<SelectItem>();
		beneficiary.add(  
				          new SelectItem( beneficiaryGridView.getBeneficiaryPersonId().toString(), 
				        		          beneficiaryGridView.getPersonName()
				        		         )
		               );
//		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Double amount =0.0d;
		if( beneficiaryGridView.getCostCenter() != null &&  beneficiaryGridView.getCostCenter().trim().length() >0  )
		{
			amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryGridView.getBeneficiaryPersonId(),getRequestView().getInheritanceFileId());
	//		Double amount = PersonalAccountTrxService .getAmount( beneficiaryGridView.getBeneficiaryPersonId());
			personBalanceMap.put( beneficiaryGridView.getBeneficiaryPersonId(), amount);			
	//		accountWS = null;
			txtBalance.setValue( amount.toString());
		}
		else
		{
			benefNoCostCenterMap.put(beneficiaryGridView.getBeneficiaryPersonId() ,beneficiaryGridView.getBeneficiaryName()  );
		}
		Map<Long, Double> personBlockBalanceMap = new HashMap<Long, Double>();
		Double personBlockBalanceAmount = BlockingAmountService.getBlockingAmount( beneficiaryGridView.getBeneficiaryPersonId(), null, null, null, null );
		personBlockBalanceMap.put( beneficiaryGridView.getBeneficiaryPersonId(), personBlockBalanceAmount);
		blockingBalance.setValue( personBlockBalanceAmount.toString()  );
		
		Double requestAmount = DisbursementService.getRequestedAmount(beneficiaryGridView.getBeneficiaryPersonId(), getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null);
		requestedAmount.setValue( requestAmount );
		
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		availableBalance.setValue(   oneDForm.format( amount- (personBlockBalanceAmount+requestAmount)) );
		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put( PERSON_BLOCKING_MAP, personBlockBalanceMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiary);
	}
	@SuppressWarnings ("unchecked")	
	private void loadBeneficiaryList(final Long fileId) throws Exception 
	{
		InheritedAssetService inheritedAssetWS = new InheritedAssetService();
		List<BeneficiariesPickListView> beneficiaresPickList = inheritedAssetWS.getBeneficiariesView(fileId);
		
		ArrayList<SelectItem> beneficiaries = new ArrayList<SelectItem>();
		//PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Map<Long, String> benefNoCostCenterMap = new HashMap<Long, String>();
		Map<Long, Double> personBlockBalanceMap = new HashMap<Long, Double>();
		for(BeneficiariesPickListView singleBeneficiary : beneficiaresPickList) 
		{
			
			if(  singleBeneficiary.getFromTakharuj().compareTo(1L)==0 || 
				(singleBeneficiary.getIsMinor() != null && singleBeneficiary.getIsMinor().longValue() == 1l)
			   )
			{
				continue;
			}
			
			
			SelectItem selItem = new SelectItem( singleBeneficiary.getPersonId().toString(),singleBeneficiary.getFullName()  );
			beneficiaries.add( selItem);
			if(	singleBeneficiary.getCostCenter() == null )
			{
				benefNoCostCenterMap.put(singleBeneficiary.getPersonId(),singleBeneficiary.getFullName() ); 		
			}
			else
			{
				Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(singleBeneficiary.getPersonId(),getRequestView().getInheritanceFileId());
	//			Double amount = PersonalAccountTrxService.getAmount( singleBeneficiary.getPersonId());
				personBalanceMap.put( singleBeneficiary.getPersonId(), amount);
				
				Double personBlockBalanceAmount = BlockingAmountService.getBlockingAmount( singleBeneficiary.getPersonId(), null, null, null, null );
				personBlockBalanceMap.put( singleBeneficiary.getPersonId(), personBlockBalanceAmount);
			}
			
			
		}			
//		accountWS = null;
		
		

		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
		viewMap.put( PERSON_BLOCKING_MAP, personBlockBalanceMap);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiaries);
	}
	@SuppressWarnings ("unchecked")	
	public void handleBeneficiaryChange() 
	{
	  try
	  {
		 if( cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL)
				 || 
				 !validateCostCenter(cmbBeneficiaryList.getValue().toString()) 	 		 
		 ) 
		 {
				txtBalance.setValue( "" );			
				txtAmount.setValue( "" );
				blockingBalance.setValue( "" );
				availableBalance.setValue("");
				requestedAmount.setValue("");
				return;
		 }
		 Map<Long, Double> balanceMap = getBalanceMap();
		 Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
		 if( !balanceMap.containsKey( beneficiaryId) ){return;}
		 Double amount = balanceMap.get( beneficiaryId);
		 txtBalance.setValue( amount.toString());
		 
		 Map<Long, Double> blockingMap = getBlockingMap();
		 Double blockingMapAmount = 0.00d;
		 if( blockingMap.containsKey( beneficiaryId) ) 
		 {
			blockingMapAmount = blockingMap.get( beneficiaryId);
			blockingBalance.setValue( blockingMapAmount.toString());
		 }
		 Double requestAmount = DisbursementService.getRequestedAmount(beneficiaryId, getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null);
		 requestedAmount.setValue( requestAmount );
			
		 DecimalFormat oneDForm = new DecimalFormat("#.00");
		 availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
		 if(getReadOnlyAmount())
		 {
		  txtAmount.setValue(availableBalance.getValue());
		 }
	  }
	  catch(Exception ex) 
	  {
		 logger.LogException("[Exception occured in handleBeneficiaryChange()]", ex);
	  }
		 
	}
	@SuppressWarnings ("unchecked")	
	private void handleFinanceTabVisibility()throws Exception 
	{
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_PAYMENT_TYPE, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_REASON, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_ITEMS, "");
		viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_DIS_SRC, "");		
	}
	@SuppressWarnings ("unchecked")
	public void handleAttachmentCommentTabStatus(RequestView view) {
		if( view.getStatusId().equals(getStatusCompleteId())) {
			viewMap.put("canAddAttachment", false);
			viewMap.put("canAddNote", false);
		} else {
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
		}
	}
	@SuppressWarnings ("unchecked")
	public void loadAttachmentsAndComments(Long requestId){		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = viewMap.get(EXTERNAL_ID).toString();
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if( null!=requestId ) {
	    	String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
	/* validators STARTS here */
	
	private boolean validateComplete() {
		if( !viewMap.containsKey(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE) ) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.ERR_COMPLETE));
			return false;
		} 
		return true;
	}
	
	private boolean validateApprove() throws Exception
	{
		errorMessages = new ArrayList<String>();
		List<DisbursementDetailsView> ddView = getDisbursementDetails();
		boolean hasBalanceError = false;
		for(DisbursementDetailsView singleView : ddView) 
		{
			if( StringHelper.isEmpty(singleView.getExternalPartyName()) &&
					(null==singleView.getPaymentDetails() || 0==singleView.getPaymentDetails().size())) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_PAYMENT_DT_REQ));
				richPanel.setSelectedTab( TAB_ID_DETAILS);
				return false;				
			}
			
			for(DisbursementRequestBeneficiaryView singleBeneficiary : singleView.getRequestBeneficiaries()) 
			{
			     
					 int validateBalanceCode = validateBalance(  getBalanceMap().get( singleBeneficiary.getPersonId() ) ,
																 getBlockingMap().get( singleBeneficiary.getPersonId() ),
																 DisbursementService.getRequestedAmount(singleBeneficiary.getPersonId() , getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
							 									 singleBeneficiary.getAmount(),
																 singleBeneficiary.getPersonId(),
																 singleView.getBeneficiariesName(),
																 getRequestView().getInheritanceFileId()
					 										   ) ;
					 if( !hasBalanceError )
					 {
					  hasBalanceError = validateBalanceCode > 0 ;
					 }
			}
			
			
		}
		if ( hasBalanceError )
		{
			return false;
		}
		return true;
	}
	
	private boolean validateSubmitQuery()
	{
		boolean bValid=true;
		if( 0==getDisbursementDetails().size() ) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_ONE_BENEF_REQ));
			bValid=false;
		}				
		logger.logInfo("[validateSubmitQuery().. Ends]");
		return bValid;
	}
	private boolean validateCostCenter(String personId) throws Exception 
	{
		Map<Long, String> map  =  (HashMap<Long, String>) viewMap.get( BENEF_NO_COST_CENTER_MAP );
		if ( map.containsKey( new Long( personId ) ) )
		{
			String message = map.get( new Long( personId ) );
			errorMessages.add( 
								java.text.MessageFormat.format(
																CommonUtil.getBundleMessage("disburseFinance.msg.associateCostCenter"),
																 (" "+message)
															  )
							    
							 );
			return false;
		}
		return true;
	}
	
	private boolean validateInput() {
		
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			richPanel.setSelectedTab(TAB_ID_APP);
			return false;
		}
		
		if( getRequestView().getDisubrsementDetailsView()== null || 0==getRequestView().getDisubrsementDetailsView().size() && 
			0==getToDeleteList().size()) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_ONE_BENEF_REQ));
			richPanel.setSelectedTab(TAB_ID_DETAILS);
			return false;
		}
		
		return true;
	}
	
	public void showInheritanceFilePopup() {
		try	 {			
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, getRequestView().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			String javaScriptText ="var screen_width = 900;"+
            	"var screen_height =600;"+
            	"window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    sendToParent( javaScriptText);		    		    
		}
		catch (Exception ex)  {
			logger.LogException("[Exception occured in showInheritanceFilePopup()]", ex);
			ex.printStackTrace();
		}
	}
		
	private boolean validateBeneficiariesInput() throws Exception 
	{
		
		if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_REQ));
			return false;
		}
		
		if( null==txtDescription.getValue() || 0==txtDescription.getValue().toString().trim().length()) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs
																	.ERR_DESC_REQ));
			return false;
		}
		 Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
		 if ( !validateCostCenter(beneficiaryId.toString()) )
		 {
			 return false;
		 }
		 int validateBalanceCode = validateBalance(
				 									 getBalanceMap().get( beneficiaryId ) ,
													 getBlockingMap().get( beneficiaryId ),
													 DisbursementService.getRequestedAmount(beneficiaryId , getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
													 Double.parseDouble(txtAmount.getValue().toString().trim()),
													 beneficiaryId,
													 null,
													 getRequestView().getInheritanceFileId()
		   										  ) ;

		 if ( validateBalanceCode != 0 )
		 {
			 return false;
		 }
			 
		if( null==getIsUpdate() ) {
			List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
			for(DisbursementDetailsView singleDetailView : dDetailsView) {
				if(singleDetailView.getPersonId().toString().equals(cmbBeneficiaryList.getValue().toString())) {
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_ALREADY_EXISTS));
					return false;					
				}
			}
		}
		return true;					
	}
	

		
	public boolean validateBeneficiaryWhenApprovalRequired()throws Exception 
	{
		if( null==getIsUpdate() && (getIsApprovalRequired() || getIsJudgeSessionDone()) ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_SEL_BENEFICIARY));
			return false;
		}		
		
		if( null==cmbPaymentSource.getValue() || cmbPaymentSource.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_PAYMENT_SRC_REQ));
			return false;
		}
		if ( null==txtAmount.getValue() || 0==txtAmount.getValue().toString().trim().length() ) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_AMNT_REQ));
			return false;
		} 
		else
		{ 
			try 
			{
				
				 Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString() );
				 int validateBalanceCode = validateBalance(
						 									 getBalanceMap().get( beneficiaryId ) ,
															 getBlockingMap().get( beneficiaryId ),
															 DisbursementService.getRequestedAmount(beneficiaryId, getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
															 Double.parseDouble(txtAmount.getValue().toString().trim()),
															 beneficiaryId,
															 null,
															 getRequestView().getInheritanceFileId()
				   										  ) ;
				 if ( validateBalanceCode != 0 )
				 {
					 return false;
				 }
					
				
			} catch(NumberFormatException ex) {
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_AMNT_NUM));
				return false;
			}
		}	
		Long extId = getDomainDataId(WebConstants.PaymentSrcType.DOMAIN_KEY, WebConstants.PaymentSrcType.EXTERNAL_PARTY);
		if( extId.toString().equals(cmbPaymentSource.getValue().toString()) ) {
			
			if( null==txtExternalPartyName.getValue()
									|| 0==txtExternalPartyName.getValue().toString().trim().length() ) {
				errorMessages.add(CommonUtil.getBundleMessage(
										MessageConstants.MemsPaymentDisbursementMsgs.ERR_EXTERNAL_NAME_REQ));
				return false;													
			}
		}
		return true;
	}
	
	/* validators ENDS here */
	
	
	
	/* USER Actions here */
	public void updateBeneficiaryWhenApprovalRequired() 
	{
		try 
		{
			if( validateBeneficiaryWhenApprovalRequired() ) 
			{
				DisbursementDetailsView reqBenef = getIsUpdate();
				if( null!=reqBenef ) 
				{
					Iterator<DisbursementRequestBeneficiaryView> iterator = reqBenef.getRequestBeneficiaries().iterator();
					DisbursementRequestBeneficiaryView next = iterator.next();
					double amnt = Double.parseDouble(txtAmount.getValue().toString());
					next.setAmount(amnt);
					reqBenef.setAmount(amnt);
					reqBenef.setOriginalAmount(amnt);
					reqBenef.setDescription(txtDescription.getValue().toString());

					reqBenef.setSrcType(new Long(cmbPaymentSource.getValue().toString()));
					DomainDataView domainDataView = getDomainDataView(WebConstants.PaymentSrcType.DOMAIN_KEY, 
																					reqBenef.getSrcType());
					reqBenef.setSrcTypeAr(domainDataView.getDataDescAr());
					reqBenef.setSrcTypeEn(domainDataView.getDataDescEn());
					
					if( reqBenef.getSrcType().equals( WebConstants.PaymentSrcType.EXTERNAL_PARTY_ID )) 
					{
						reqBenef.setPaidTo(WebConstants.PAID_TO_EXTERNAL_PARTY);
						reqBenef.setExternalPartyName(txtExternalPartyName.getValue().toString());
					} 
					else 
					{
						reqBenef.setExternalPartyName("");
						reqBenef.setPaidTo(WebConstants.PAID_TO_BENEFICIARY);
					}										
				}
				resetNonMaturedSharesViewToDetault();
				viewMap.remove(IS_UPDATE);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_ADD_SUCC));				
			} 
			setTotalRows(getDisbursementDetails().size());
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in updateBeneficiaryWhenApprovalRequired()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	private boolean hasUnblockError ( boolean hasSelected ) throws Exception
	{
		boolean hasError = false;
		if( !hasSelected )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "disbursement.msg.selectAny" )	);
			hasError = true;
		}
		return hasError;
	}
	
	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeUnblockBPEL(Request request) throws Exception, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSBlockingBPEL" );
		 MEMSBlockingBPELPortClient port=new MEMSBlockingBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		Integer.parseInt( request.getRequestId().toString()),
				 		CommonUtil.getLoggedInUser(),
				 		"0", 
				 		null, 
				 		null
				 	   );
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			Long requestId = -1l;
			if( getRequestView().getRequestId() != null )
			{
				requestId  = getRequestView().getRequestId();
			}
			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+
								  														getRequestView().getInheritanceFileId()+"','"+ 
								  														requestId+
								  												 "');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+getRequestView().getInheritanceFileId()+"');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onUnblockSelected()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			List<DisbursementDetailsView> selectedDetails = new ArrayList<DisbursementDetailsView>(); 
			for (DisbursementDetailsView dtView : getDisbursementDetails()) 
			{
              if( dtView.getSelected() == null || !dtView.getSelected() ) { continue; }
              selectedDetails.add( dtView );	 
			}
			if( hasUnblockError( selectedDetails.size()  >  0 ) ) return;
			
	        ApplicationContext.getContext().getTxnContext().beginTransaction();
	        
	        
			BlockingAmountService blockService = new BlockingAmountService();
			RequestView reqView = getRequestView();
			
			Request unblockingRequest = blockService.populateUnBlockingRequestFromDisbursementView(
																									 reqView, 
																									 selectedDetails,
																									 getLoggedInUserId()
																								  );
			invokeUnblockBPEL( unblockingRequest );
            String msg =  ResourceUtil.getInstance().getProperty( "disbursement.msg.unblockingRequestSaved" );			
            msg =  java.text.MessageFormat.format( 
		            								msg, 
		            								unblockingRequest.getRequestNumber()
		            							  );	
            
			successMessages.add( msg );
	        ApplicationContext.getContext().getTxnContext().commit();
		}
		catch( Exception e)
		{
			logger.LogException( " onUnblockSelected --- EXCEPTION --- ", e );
	        ApplicationContext.getContext().getTxnContext().rollback();
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(	ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );

		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}	
  
	@SuppressWarnings("unchecked")
	public void saveBeneficiary()
	{
		try 
		{
			if( validateBeneficiariesInput() ) 
			{
				DisbursementDetailsView reqBenef = getIsUpdate();
				if( null!=reqBenef ) 
				{
					Iterator<DisbursementRequestBeneficiaryView> iterator = reqBenef.getRequestBeneficiaries().iterator();
					DisbursementRequestBeneficiaryView next = iterator.next();
					double amnt = Double.parseDouble(txtAmount.getValue().toString());
					next.setAmount(amnt);
					reqBenef.setAmount(amnt);
					reqBenef.setOriginalAmount(amnt);
					reqBenef.setDescription(txtDescription.getValue().toString());
					viewMap.remove(IS_UPDATE);
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_ADD_SUCC));	
				}
				else 
				{
					String beneficiaryName = "";
					List<SelectItem> beneficiaryList = this.getBeneficiaryList();
					for(SelectItem singleItem : beneficiaryList) 
					{
						if(singleItem.getValue().toString().equals(cmbBeneficiaryList.getValue().toString())) 
						{
							beneficiaryName = singleItem.getLabel();
						}
					}
					List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
					DisbursementDetailsView defView = getDefaultDisbDetailView();
					double amnt = Double.parseDouble(txtAmount.getValue().toString());
					defView.setAmount(amnt);
					defView.setOriginalAmount(amnt);
					defView.setDescription(txtDescription.getValue().toString());
					List<DisbursementRequestBeneficiaryView> requestBeneficiaries = defView.getRequestBeneficiaries();
					if( null==requestBeneficiaries ) 
					{
						requestBeneficiaries = new ArrayList<DisbursementRequestBeneficiaryView>();
						defView.setRequestBeneficiaries(requestBeneficiaries);
					}
					DisbursementRequestBeneficiaryView benfView = new DisbursementRequestBeneficiaryView();
					benfView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
					benfView.setName(beneficiaryName);
					benfView.setAmount(amnt);
					benfView.setPersonId(Long.parseLong(cmbBeneficiaryList.getValue().toString()));
					requestBeneficiaries.add(benfView);
					dDetailsView.add(defView);					
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_UPDATE_SUCC));					
				}
				setTotalRows(getDisbursementDetails().size());
				setDisbursementDetails( getDisbursementDetails() );
				resetNonMaturedSharesViewToDetault();
			}
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in saveBeneficiary()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private void resetNonMaturedSharesViewToDetault() 
	{
		txtDescription.setValue("");
		txtAmount.setValue("");
		cmbBeneficiaryList.setDisabled(false);
		cmbBeneficiaryList.setValue(CMB_DEFAULT_VAL);
		txtBalance.setValue( "" );
		blockingBalance.setValue( "" );
		availableBalance.setValue("");
		requestedAmount.setValue("");
		if( getIsApprovalRequired() || getIsJudgeSessionDone() ) 
		{
			txtExternalPartyName.setValue("");
			cmbPaymentSource.setDisabled(true);
			cmbPaymentSource.setStyleClass(CSS_READONLY);
			cmbPaymentSource.setValue(CMB_DEFAULT_VAL);	
		}
	}

	public void saveApplication() {
		try {
			Long oldStatusId = getRequestView().getStatusId();
			Long statusId = getRequestView().getStatusId();
			statusId = getStatusNewId();
			if( !oldStatusId.equals( INVALID_STATUS) &&
				oldStatusId.equals(  getStatusRejectedResubmittedId() )
			 )
			{
				statusId = getStatusRejectedResubmittedId();
			}
			
			if( doSaveApplication( statusId  ) ) 
			{
				
				if( oldStatusId.equals( INVALID_STATUS)) 
				{
					CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.
																	REQUEST_CREATED, getRequestView().getRequestId());
				}
				refreshTabs(getRequestView());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.
																	MemsCommonRequestMsgs.SUCC_REQ_SAVE));
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in saveApplication()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private DisbursementDetailsView getIsUpdate() {
		return (DisbursementDetailsView) viewMap.get(IS_UPDATE);
	}
	
	
	public void onPrint()
    {
    	try
    	{
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	public void submitQuery() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			Long oldStatusId = getRequestView().getStatusId();
			if( validateSubmitQuery() && doSaveApplication(getStatusApprovalRequiredId())) 
			{
				if( oldStatusId.equals( INVALID_STATUS) ) 
				{
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
														MessageConstants.RequestEvents.REQUEST_CREATED,
														getRequestView().getRequestId());
				}
				refreshTabs(getRequestView());
				invokeBpel(getRequestView().getRequestId());
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, 
																					getRequestView().getRequestId());
				CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.
																	MemsCommonRequestMsgs.SUCC_REQ_SUBMIT));
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in submitQuery()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[submitQuery() .. Ends");
	}
	
	public void resubmit() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			if( validateSubmitQuery() &&  doSaveApplication( getStatusApprovalRequiredId() ) ) 
			{
				completeTask(TaskOutcome.OK);
				refreshTabs(getRequestView());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SUBMIT));
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, 
																							getRequestView().getRequestId());
			}
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in resubmit()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void deleteBeneficiary() {
		try {
			DisbursementDetailsView detailView = (DisbursementDetailsView) disbursementTable.getRowData();
			if( null!=detailView ) {
				if( null!=detailView.getDisDetId() ) {
					detailView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					getToDeleteList().add(detailView);
				} 
				if( getDisbursementDetails().remove(detailView)) {
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_DELETE_SUCC));
				}
				setDisbursementDetails(getDisbursementDetails());
			}			
		} catch(Exception ex) {
			logger.logInfo("[Exception occured in deleteBeneficiary()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private boolean hasRejectionError()
	{
		if( htmlRFC.getValue()== null || htmlRFC.getValue().toString().trim().length()  <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "socialResearchRecommendation.msg.rejectionReasonRequired" ));
			return true;
		}
		return false;
	}
	private boolean hasRejectFinanceError()throws Exception
	{
	
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		java.util.Date lowerBound = (java.util.Date)df.parse("7/11/2013 23:59:59");
		
		if(getRequestView().getCreatedOn().before( lowerBound )  )
		{
			
			errorMessages.add(CommonUtil.getBundleMessage("disburseRecMatFinance.msg.rejectionErrorDate"));
			richPanel.setSelectedTab(TAB_ID_FINANCE_TAB	);
			return true;
		}
		FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
		
		List<DisbursementDetailsView> ddView = tabFinanceController.getDisbDetails();
		
		for(DisbursementDetailsView singleView : ddView) 
		{
			if(  singleView.getIsDisbursed().longValue()==1l  || !tabFinanceController.getRenderDisburse()) 
			{
				errorMessages.add(CommonUtil.getBundleMessage("disburseFinance.msg.rejectionError"));
				richPanel.setSelectedTab(TAB_ID_FINANCE_TAB	);
				return true;				
			}
		}
		if( hasRejectionError() )
		{
			return true;
		}	
		return false;
	}
	public void rejectFinance() 
	{
		try 
		{
			errorMessages = new ArrayList<String>();
			if( hasRejectFinanceError() )
				return; 
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				if(	doSaveApplication( STATUS_FINANCE_REJECTED_ID )	)
				{
					completeTask( TaskOutcome.REJECT);
					//refreshTabs( reqView);
					refreshTabs(getRequestView());
					CommonUtil.saveSystemComments( WebConstants.REQUEST,"disbursementRequest.event.financeRejected", getRequestView().getRequestId());
					if( htmlRFC.getValue() != null && htmlRFC.getValue().toString().trim().length() >0 )
					{
						CommonUtil.saveRemarksAsComments(getRequestView().getRequestId(), 
								                         htmlRFC.getValue().toString().trim(), 
								                         WebConstants.REQUEST);
					}
					successMessages.add(CommonUtil.getBundleMessage("disbursementRequest.msg.financeRejected" ));
				}
            
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in rejectFinance()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void reject() 
	{
		errorMessages=new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			if( hasRejectionError() )
				return;
			
			List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
			Long disRejected = CommonUtil.getDomainDataId(WebConstants.DisbursementStatus.DOMAIN_KEY, 
																	WebConstants.DisbursementStatus.REJECTED);
			for(DisbursementDetailsView singleDetail : disbursementDetails) {
				singleDetail.setStatus(disRejected);
			}
			if( doSaveApplication( getStatusRejectedResubmittedId() ) ) 
			{
				completeTask(TaskOutcome.REJECT);
				refreshTabs(getRequestView());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REJECT));
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_RESUBMITTED, 
																							getRequestView().getRequestId());
				if( htmlRFC.getValue() != null && htmlRFC.getValue().toString().trim().length() >0 )
				{
					CommonUtil.saveRemarksAsComments(getRequestView().getRequestId(), 
							                         htmlRFC.getValue().toString().trim(), 
							                         WebConstants.REQUEST);
				}
				
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in reject()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void cancelled() {
		try {
			List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
			Long disRejected = CommonUtil.getDomainDataId(WebConstants.DisbursementStatus.DOMAIN_KEY, 
																	WebConstants.DisbursementStatus.REJECTED);
			for(DisbursementDetailsView singleDetail : disbursementDetails) {
				singleDetail.setStatus(disRejected);
			}
			if( doSaveApplication(getStatusCancelledId() ) ) {
				completeTask(TaskOutcome.REJECT);
				refreshTabs(getRequestView());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.RequestEvents.REQUEST_CANCELED));
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_CANCELED, 
																							getRequestView().getRequestId());
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in cancelled()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( cmbReviewGroup.getValue() == null || StringHelper.isEmpty( cmbReviewGroup.getValue().toString().trim() ) || 
			this.cmbReviewGroup.getValue().toString().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( htmlRFC.getValue() == null || StringHelper.isEmpty( htmlRFC.getValue().toString().trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}


	public void review() {
		try {
			if( !isReviewValidated() )
				return ;
			UtilityService utilityService = new UtilityService();
			if( doSaveApplication(getStatusReviewReqId())) {
				refreshTabs(getRequestView());
				ReviewRequestView reviewRequestView = new ReviewRequestView();
				reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
				reviewRequestView.setCreatedOn( new Date() );
				reviewRequestView.setRfc( htmlRFC.getValue().toString().trim() );
				reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
				reviewRequestView.setRequestId( getRequestView().getRequestId() );
				utilityService.persistReviewRequest( reviewRequestView );				
		
				completeTask(TaskOutcome.SEND_FOR_RECOMMENDATIONS);
				htmlRFC.setValue("");
				cmbReviewGroup.setValue("-1");
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW, 
						getRequestView().getRequestId());
				
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
										MemsPaymentDisbursementMsgs.REVIEW_REQUIRED));
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo("[review() .. Ends]");
	}
	
	public void reviewDone() {
		try {
			UtilityService utilityService = new UtilityService();
			ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			utilityService.persistReviewRequest( reviewRequestView );
			
			
			if( doSaveApplication( getStatusReviewDoneId() )  ) {
				completeTask(TaskOutcome.OK);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_REVIEWED, 
						getRequestView().getRequestId());
				refreshTabs(getRequestView());
			}
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
											MemsPaymentDisbursementMsgs.REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}
	
	public void judge() {
		try {
			if( doSaveApplication(getStatusJudgeSessionReqId())) {
				refreshTabs(getRequestView());
				completeTask(TaskOutcome.LEGAL_DEPT_REVIEW);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_PAYMENT_SENT_FOR_JUDGE, 
						getRequestView().getRequestId());
				
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
										MemsPaymentDisbursementMsgs.SUCC_JUDGE_REVIEW));
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in judge()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void judgeDone() {
		try {
			List<SessionDetailView> sessionDetailViewList = (List<SessionDetailView>) viewMap.get( 
															WebConstants.Session.SESSION_DETAIL_VIEW_LIST ); 	
			UtilityService utilityService = new UtilityService();
			utilityService.persistJudgeSession(sessionDetailViewList.toArray(new 
										SessionDetailView[sessionDetailViewList.size()]));
			if( doSaveApplication(getStatusJudgeSessionDoneId()) ) {
				completeTask(TaskOutcome.OK);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_PAYMENT_JUDGE_DONE, 
						getRequestView().getRequestId());
				refreshTabs(getRequestView());
			}
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
											MemsPaymentDisbursementMsgs.SUCC_JUDGE_REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in judgeDone()]", ex);
		}
	}
	
	public void complete() {
		try {
			if( !validateComplete() ) {return;}
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				doSaveApplication( WebConstants.REQUEST_STATUS_COMPLETE_ID );
				refreshTabs(getRequestView());
				completeTask(TaskOutcome.OK);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED, 
										getRequestView().getRequestId());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.
															MemsCommonRequestMsgs.SUCC_REQ_COMPLETE));
		} catch(Exception ex) {
			logger.LogException("[Exception occured in complete()]", ex);			
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private boolean validateSuspension() throws Exception
	{
		boolean validate = true;
		if( htmlRFC.getValue()== null || htmlRFC.getValue().toString().trim().length()  <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "request.msg.suspensionReasonRequired" ));
			validate =false ;
		}
		
		
		return validate;
	}
	public void onSuspended()
	{
		try 
		{
			RequestView reqView= getRequestView();
			
			if( validateSuspension() && doSaveApplication( WebConstants.REQUEST_STATUS_SUSPENDED_ID ) ) 
			{				

				CommonUtil.saveSystemComments( WebConstants.REQUEST,
											   "request.msg.requestSuspended",
											   reqView.getRequestId()
											  );
				refreshTabs( reqView);
				successMessages.add( CommonUtil.getBundleMessage( "request.msg.requestSuspended") );
			}
			
		} 
		
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onSuspended()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void approve() 
	{
		errorMessages = new ArrayList<String>();
		try 
		{
			if( !validateApprove() ) 
			{
				getErrorMessages();
				return;
			}
				Long disbRequired = CommonUtil.getDomainDataId(WebConstants.DisbursementStatus.DOMAIN_KEY, 
																	WebConstants.DisbursementStatus.DIS_REQ);
				Long disbursedId = CommonUtil.getDomainDataId(WebConstants.DisbursementStatus.DOMAIN_KEY, 
																	WebConstants.DisbursementStatus.DISBURSED);
				List<DisbursementDetailsView> details = getDisbursementDetails();				
				boolean bForward = false;				
				for(DisbursementDetailsView singleDetail : details)
				{
					String externalParty = singleDetail.getExternalPartyName();
					if( StringHelper.isEmpty(externalParty)) 
					{
						singleDetail.setStatus(disbRequired);
						bForward = true;
					} else 
					{
						singleDetail.setStatus(disbursedId);
						singleDetail.setDisbursedBy(CommonUtil.getLoggedInUser());
						singleDetail.setIsDisbursed( WebConstants.DISBURSED );
					}
				}				
				
				if( bForward ) 
				{
					doSaveApplication( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID );
					CommonUtil.saveSystemComments(	
													WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_APPROVED, 
													getRequestView().getRequestId()
												 );
					completeTask(TaskOutcome.FORWARD);
				} 
				else 
				{
					doSaveApplication(getStatusCompleteId());
					CommonUtil.saveSystemComments(
													WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED, 
													getRequestView().getRequestId()
												 );
					completeTask(TaskOutcome.APPROVE);
				}
				
				refreshTabs(getRequestView());				
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.
																	MemsCommonRequestMsgs.SUCC_REQ_APPROVE));
		} catch(Exception ex) {
			
			logger.LogException("[Exception occured in approve]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void editPaymentDetails() {
		try {

		DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();
		List<DisbursementRequestBeneficiaryView> reqBeneficiaryList = detailData.getRequestBeneficiaries();
		DisbursementRequestBeneficiaryView next = reqBeneficiaryList.get( 0);				
		sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, detailData);					
		String javaScriptText = " javaScript:openPaymentDetailsPopUp( 'pageMode=MODE_SELECT_MANY_POPUP&"
								+ WebConstants.PERSON_ID+ "="+ next.getPersonId().toString()+"');";		
		sendToParent(javaScriptText);
	} catch(Exception ex) {
		logger.LogException("[Exception occured in editPaymentDetails]", ex);
		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}

	}
	
	public String sendToParent(String javaScript) {
		final String viewId = "/SearchAssets.jsp";	
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);	
		return "";
	}
	
	public void editBeneficiary() 
	{
		try
		{
		
		DisbursementDetailsView selectedRow = (DisbursementDetailsView) disbursementTable.getRowData();
		if( null!=selectedRow ) 
		{
			txtAmount.setValue( selectedRow.getAmount().toString());
			txtDescription.setValue( selectedRow.getDescription());
			cmbBeneficiaryList.setValue( selectedRow.getPersonId().toString());
			cmbBeneficiaryList.setDisabled( true);
			storeBeneficiaryBalanceAmountInMap( selectedRow.getPersonId() );
			Map<Long, Double> balanceMap = getBalanceMap();
			Double amount=balanceMap.get( new Long( selectedRow.getPersonId()));
			txtBalance.setValue( amount );
			
			storeBeneficiaryBlockingAmountInMap( selectedRow.getPersonId() );
			Double blockingMapAmount = getBlockingMap().get( new Long( selectedRow.getPersonId()));
			blockingBalance.setValue( blockingMapAmount );
			
			Double requestAmount = DisbursementService.getRequestedAmount(selectedRow.getPersonId(), getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null);
			requestedAmount.setValue( requestAmount );
			
			DecimalFormat oneDForm = new DecimalFormat("#.00");
			availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
			if( !txtBalance.getValue().equals(txtAmount.getValue() ) )
			{
			  txtAmount.setValue( availableBalance.getValue().toString() );
			}
			//if the request status is approval required two more controls are displayed and should be updated
			if( getIsApprovalRequired() || getIsJudgeSessionDone() || getIsFinanceRejected()  ) 
			{				
				cmbPaymentSource.setValue(selectedRow.getSrcType().toString());
				cmbPaymentSource.setDisabled(false);
				cmbPaymentSource.setStyleClass("");
				txtExternalPartyName.setValue(selectedRow.getExternalPartyName());						
			}
			
			viewMap.put(IS_UPDATE, selectedRow);
		}
		}
		catch( Exception e )
		{
    		errorMessages = new ArrayList<String>(0);
    		logger.LogException("[editBeneficiary() .. |Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	
	public void handlePaymentSrcChange() {
		logger.logInfo("[handlePaymentSrcChange() .. Starts]");
		Long srcTypeId = getDomainDataId(WebConstants.PaymentSrcType.DOMAIN_KEY, 
														WebConstants.PaymentSrcType.EXTERNAL_PARTY);
		if( srcTypeId.toString().equals(cmbPaymentSource.getValue().toString())) {			
			txtExternalPartyName.setDisabled(false);
			txtExternalPartyName.setReadonly(false);
			txtExternalPartyName.setStyleClass("");		
			DisbursementDetailsView selected = getIsUpdate();
			if( null!=selected ) {
				txtExternalPartyName.setValue(selected.getExternalPartyName());
			}
		} else {
			txtExternalPartyName.setValue("");
			txtExternalPartyName.setDisabled(true);
			txtExternalPartyName.setReadonly(true);
			txtExternalPartyName.setStyleClass(CSS_READONLY);
		}
		logger.logInfo("[handlePaymentSrcChange() .. Ends]");
	}
	
	public void loadPaymentDetails(ActionEvent evt) {
		
		try
		{
				DisbursementDetailsView view = (DisbursementDetailsView) sessionMap.get(WebConstants.LOAD_PAYMENT_DETAILS);
				sessionMap.remove(WebConstants.LOAD_PAYMENT_DETAILS);
				boolean bFinanceTab = false;
				if( sessionMap.containsKey( WebConstants.FinanceTabPublishKeys.IS_FINANCE)) 
				{
					sessionMap.remove( WebConstants.FinanceTabPublishKeys.IS_FINANCE);
					bFinanceTab = true;
				}
				if( bFinanceTab ) 
				{																						
					FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
					if( null!=tabFinanceController ) 
					{
						tabFinanceController.loadPaymentDetails( view);
					}
				} 
				else 
				{
						PaymentCriteriaView criteriaView = view.getPaymentCriteriaView();
						List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
						
						for (DisbursementDetailsView dtView : disbursementDetails)
						{
								if (!dtView.getDisDetId().equals(view.getDisDetId())) { continue; }
								
								
								List<PaymentDetailsView> listPaymentDetailsView = dtView.getPaymentDetails();
								if( null==listPaymentDetailsView ) {
									listPaymentDetailsView = new ArrayList<PaymentDetailsView>();
									dtView.setPaymentDetails( listPaymentDetailsView);
								}
								
								PaymentDetailsView paymentView = null;
								if( 0<listPaymentDetailsView.size() ) 
								{
									paymentView = listPaymentDetailsView.get( 0);					
								}
								else 
								{
									paymentView = new PaymentDetailsView();
									paymentView.setStatus( WebConstants.NOT_DISBURSED);
									paymentView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
								}
								
								paymentView.setAccountNo(criteriaView.getAccountNumber());
								paymentView.setAmount(criteriaView.getAmount());
								paymentView.setBankId(criteriaView.getBankId());
								if (null != criteriaView.getPaymentMode()) 
								{
									paymentView.setPaymentMethod(new Long(criteriaView.getPaymentMode()));
								}
								if( null!=criteriaView.getPaidTo()) {
									paymentView.setPaidTo( new Long( criteriaView.getPaidTo()));
									paymentView.setOthers( null );
								}
								else if ( criteriaView.getOther() != null && criteriaView.getOther().trim().length() >0 )
								{
									paymentView.setOthers(criteriaView.getOther().trim() );
									paymentView.setPaidTo(  null );
								}
								paymentView.setAmount(dtView.getAmount());
								paymentView.setRefDate(criteriaView.getDate());
								paymentView.setReferenceNo(criteriaView.getReferenceNumber());
								if( 0==listPaymentDetailsView.size() )
								{
									listPaymentDetailsView.add( paymentView);
								}
						}
						
						setDisbursementDetails(disbursementDetails);
				}
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
		
		} catch (Exception ex) {
			logger.LogException("[loadPaymentDetails |Exception|", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	
	private boolean doSaveApplication(final Long statusId) throws Exception
	{
				if( !validateInput() ) {return false;}
				
				RequestView reqView = getRequestView();
				reqView.setRequestDate(new Date());
				reqView.setOldStatusId(reqView.getStatusId());
				reqView.setStatusId( statusId);
				if( (statusId.equals( 90037l )  || statusId.equals( 90059l ) ) && 
					htmlRFC.getValue() !=null && htmlRFC.getValue().toString().trim().length() >0)
				{
					reqView.setMemsNolReason( htmlRFC.getValue().toString().trim() );
				}
				int totalSize = getToDeleteList().size() + getDisbursementDetails().size();				
				List<DisbursementDetailsView> totalDisbursement = new ArrayList<DisbursementDetailsView>(totalSize);
				totalDisbursement.addAll(getDisbursementDetails());
				totalDisbursement.addAll(getToDeleteList());
				reqView.setDisubrsementDetailsView(totalDisbursement);				
				RequestView updatedView = saveOrUpdate(reqView);
				
				//inserting maturity date into person
				enterMaturityDate();
				
				setRequestView(updatedView);
				List<DisbursementDetailsView> dDetailsView = updatedView.getDisubrsementDetailsView();
				if( null==dDetailsView ) {
					dDetailsView = new ArrayList<DisbursementDetailsView>();
										
				}
				setDisbursementDetails(dDetailsView);
				setTotalRows(dDetailsView.size());
				saveAttachmentsAndComments(updatedView.getRequestId());
				return true;
	}

	private void enterMaturityDate()throws Exception {
		if ( this.getRequestView() != null && this.getRequestView().getRequestId()!= null)
		{
			new RequestService().enterMaturityDate(this.getRequestView().getRequestId(), getDisbursementDetails() );
		}
	}
	
	private RequestView saveOrUpdate(RequestView reqView) throws Exception {
		RequestService reqWS = new RequestService();
		return reqWS.saveOrUpdate(reqView);
	}
	
	private void insertMaturityDate(List<PersonView> personView,Date maturityDate)
	{
		RequestService requestService = new RequestService();
		requestService.updatePersonForMaturityDate(personView,maturityDate);
	}
	
	
	/** The function is used to enable or disable the CONTROLS ON THE DISBURSEMENT DETAILS TAB 
	 *  The control will only be enabled when the status of the request is NEW, APPROVAL_REQUIRED 
	 *  or REVIEW ( evaluated ).
	 * @author Farhan Muhammad Amin
	 * @return true if the controls are to be displayed in read only mode
	 */
	public Boolean getReadOnlyMode() {
		return  getIsApproved() || getIsDisbursementRequired();			
	}
	
	public Boolean getReadOnlyAmount() {
		return  !(getIsReviewDone() || getIsApprovalRequired() || getIsJudgeSessionDone());			
	}
	public String getAmountReadOnlyClass() {
		if( getReadOnlyAmount() )
		{
			return CSS_READONLY;
		}
		return "";
	}
	
	/** The function is used in the EL to set the READONLY class template to the desired controls
	 * @return the CSS class for the read only mode if the controls are to be rendered in read only mode
	 */
	
	public String getReadOnlyClass() {
		if( getReadOnlyMode() ) {
			return CSS_READONLY;
		}
		return "";
	}
	
	
	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}
	
	public Boolean getIsFromBeneficiarySearch() {
		if( !viewMap.containsKey(IS_BENEF_SRCH) ) {
			return Boolean.valueOf(false);
		}
		return (Boolean) viewMap.get(IS_BENEF_SRCH);
	}
	
	
		
	public Boolean getIsReadOnly() {
		return true;
	}
	
	public String getStyleClass() {
		return "";
	}
	
	private Long getStatusNewId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_NEW) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_NEW);
	}
	
	private Long getStatusJudgeSessionDoneId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_JUDGE_DONE) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_JUDGE_DONE);
	}
	
	private Long getStatusJudgeSessionReqId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ) ) {
			return INVALID_STATUS;
		}
		viewMap.put( WebConstants.Session.SESSION_DETAIL_TAB_MODE_KEY,WebConstants.Session.SESSION_DETAIL_TAB_MODE_UPDATABLE  );
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ);
	}
	private Long getStatusReviewReqId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED);
	}
	private Long getStatusReviewDoneId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_DONE) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_DONE);
	}
	
	private Long getStatusRejectedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED);
	}
	private Long getStatusRejectedResubmittedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT))
		{
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
	}
	
	private Long getStatusCancelledId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_CANCELED))
		{
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_CANCELED);
	}
	
	public Long getStatusApprovedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVED);
	}
	
	public Long getStatusCompleteId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_COMPLETE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
	}
	
	public Long getStatusApprovalRequiredId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);		
	}
	
	public Long getSatusRejectedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED);		
	}
	
	public Long getStatusReviewRequiredId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_EVALUATION_REQUIRED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_EVALUATION_REQUIRED);		
	}
	
	public Long getStatusReviewedId() {
		
		return WebConstants.REQUEST_STATUS_REVIEW_DONE_ID;		
	}
	
	public List<SelectItem> getPaymentSource() {
		if( viewMap.containsKey(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE) ) {
			return (List<SelectItem>) viewMap.get(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getPaymentCategories() {
		if( viewMap.containsKey(WebConstants.MemsPaymentCategories.PAYMENT_CATEGORY) ) {
			return (List<SelectItem>) viewMap.get(WebConstants.MemsPaymentCategories.PAYMENT_CATEGORY);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public Boolean getIsInvalidStatus() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(INVALID_STATUS);
	}
	public Boolean getIsRejectedResubmitted() {
		String dataValue = this.getRequestView().getStatusDataValue();
		
		return  ! this.getRequestView().getStatusId().equals(INVALID_STATUS) &&
		        dataValue.equals( WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT  );				
	}
	public Boolean getIsCancelled() {
		String dataValue = this.getRequestView().getStatusDataValue();
		
		return  !this.getRequestView().getStatusId().equals(INVALID_STATUS) &&
				dataValue.equals( WebConstants.REQUEST_STATUS_CANCELED );				
	}
	public Boolean getIsReviewed() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusReviewedId());				
	}
	
	public Boolean getIsDisbursementRequired() 
	{
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID );
	}
	
	public Boolean getIsSuspended() {
		return this.getRequestView() != null &&  this.getRequestView().getStatusId().equals( WebConstants.REQUEST_STATUS_SUSPENDED_ID );
	}
	
	public Boolean getIsApproved() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusApprovedId());
	}
	
	public Boolean getIsApprovalRequired() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusApprovalRequiredId());				
	}
	public Boolean getIsFinanceRejected() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.longValue()== STATUS_FINANCE_REJECTED_ID;				
	}
	public Boolean getIsJudgeSessionDone() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusJudgeSessionDoneId());
	}
	
	public Boolean getIsJudgeSessionRequired() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals(getStatusJudgeSessionReqId());
	}
	
	public Boolean getIsReviewRequired() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals( getStatusReviewReqId() );
	}
	
	public Boolean getIsReviewDone() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals( getStatusReviewDoneId() );
	}
	
	public Boolean getIsComplete() {
		Long reqId = this.getRequestView().getStatusId();
		return reqId.equals( 9005L );
	}
	
	
	public Boolean getIsNewStatus() {
		Long reqId = this.getRequestView().getStatusId();
		return (reqId.equals(INVALID_STATUS)) || (reqId.equals(getStatusNewId()));
	}
	
	public HtmlSelectOneMenu getCmbPaymentCategories() {
		return cmbPaymentCategories;
	}

	public void setCmbPaymentCategories(HtmlSelectOneMenu cmbPaymentCategories) {
		this.cmbPaymentCategories = cmbPaymentCategories;
	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}
	
	public String getPaymentCatCssClass() {
		if( getIsNewStatus() ) {
			return StringHelper.EMPTY;
		}
		return CSS_READONLY;		 
	}
	
	public String getFileNumber() {
		return getRequestView().getInheritanceFileView().getFileNumber();
	}

	public List<SelectItem> getBeneficiaryList() {
		if( viewMap.containsKey(BENENEFICIARY_LIST) ) {
			return (List<SelectItem>) viewMap.get(BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public HtmlSelectOneMenu getCmbBeneficiaryList() {
		return cmbBeneficiaryList;
	}

	public void setCmbBeneficiaryList(HtmlSelectOneMenu cmbBeneficiaryList) {
		this.cmbBeneficiaryList = cmbBeneficiaryList;
	}	
	@SuppressWarnings("unused")	
	public void setDisbursementDetails(List<DisbursementDetailsView> list) {

		double sumDisbursements = 0.0d;
		viewMap.put(DISBURSEMENTS, list);
		for (DisbursementDetailsView disbursementDetailsView : list) 
		{
			if( disbursementDetailsView.getIsDeleted().compareTo( 1L ) == 0 )
				continue;
			sumDisbursements += disbursementDetailsView.getAmount(); 
		}
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		setTotalDisbursementCount( oneDForm.format( sumDisbursements ) );
	}
	
	public List<DisbursementDetailsView> getDisbursementDetails() {
		if( viewMap.containsKey(DISBURSEMENTS)) {
			return (List<DisbursementDetailsView>) viewMap.get(DISBURSEMENTS);
		}
		return new ArrayList<DisbursementDetailsView>();
	}
	
	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}
	
	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}

	public HtmlInputTextarea getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(HtmlInputTextarea txtDescription) {
		this.txtDescription = txtDescription;
	}

	public HtmlInputText getTxtAmount() {
		return txtAmount;
	}

	public void setTxtAmount(HtmlInputText txtAmount) {
		this.txtAmount = txtAmount;
	}
	
	private DisbursementDetailsView getDefaultDisbDetailView() {
		DisbursementDetailsView ddView = new DisbursementDetailsView();
		ddView.setCreatedBy(CommonUtil.getLoggedInUser());
		ddView.setCreatedOn(new Date());
		ddView.setUpdatedBy(CommonUtil.getLoggedInUser());
		ddView.setUpdatedOn(new Date());
		ddView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		ddView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		ddView.setIsDisbursed(WebConstants.NOT_DISBURSED);
		ddView.setStatus(WebConstants.DisbursementStatus.NEW_ID);
		ddView.setSrcType(WebConstants.PaymentSrcType.SELF_ACCOUNT_ID );
		ddView.setHasInstallment( WebConstants.HAS_INSTALLMENT_DEAFULT);
		
		DomainDataView domainData = getDomainDataView(WebConstants.PaymentSrcType.DOMAIN_KEY, 
														ddView.getSrcType() );
		
		ddView.setSrcTypeAr(domainData.getDataDescAr());
		ddView.setSrcTypeEn(domainData.getDataDescEn());
		
		ddView.setIsDisbursed(0L);					
		return ddView;
	}
	
	public List<DisbursementDetailsView> getToDeleteList() {
		if( viewMap.containsKey(DELETED_DISBURSEMENT) ) {
			return (List<DisbursementDetailsView>) viewMap.get(DELETED_DISBURSEMENT);
		}
		return new ArrayList<DisbursementDetailsView>(0);
	}
	
	public DomainDataView getDomainDataView(final String domainKey, final Long domainSubKeyId) {
		List<DomainDataView> ddv = CommonUtil.getDomainDataListForDomainType(domainKey);
		return CommonUtil.getDomainDataFromId(ddv, domainSubKeyId);
	}
	
	public Long getDomainDataId(String domainKey, String domainSubKey) {
		List<DomainDataView> ddv = CommonUtil.getDomainDataListForDomainType(domainKey);
		return CommonUtil.getIdFromType(ddv, domainSubKey).getDomainDataId(); 
	}
	
	public Boolean getIsFileAssociated() {
		if( viewMap.containsKey(IS_FILE_ASSOCIATED) ) {
			return ((String)viewMap.get(IS_FILE_ASSOCIATED)).equals("TRUE");		
		}		
		return false;
	}
	
	private void saveAttachmentsAndComments(Long reqId) throws PimsBusinessException {
		loadAttachmentsAndComments(reqId);
		CommonUtil.saveAttachments(reqId);
		String noteOwner = (String)viewMap.get("noteowner");
		CommonUtil.saveComments(reqId,noteOwner);
	}	
	
	private Map<Long, Double> getBalanceMap() {
		if( viewMap.containsKey( PERSON_BALANCE_MAP)) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BALANCE_MAP);
		}
		return new HashMap<Long, Double>();
	}
	
	private Map<Long, Double> getBlockingMap() {
		if( viewMap.containsKey( PERSON_BLOCKING_MAP )) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BLOCKING_MAP  );
		}
		return new HashMap<Long, Double>();
	}
	@SuppressWarnings("unchecked")
	private Boolean invokeBpel(Long requestId) {
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try {
			MEMSReceiveMatureSharesBPELPortClient bpelClient = new MEMSReceiveMatureSharesBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.MemsBPELEndPoints.MEMS_MATURE_SHARES_END_POINT);
			bpelClient.setEndpoint( endPoint);
			String userId = CommonUtil.getLoggedInUser();		
			bpelClient.initiate( requestId, userId, null, null);
			logger.logInfo("invokeBpel() completed successfully!!!");		
		} catch(PIMSWorkListException exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
		} catch(Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
		}
		finally {
			if(!success) {		
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	private Boolean completeTask(TaskOutcome taskOutcome) throws Exception {
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
        logger.logInfo("Contextpath is: " + contextPath);
        String loggedInUser = CommonUtil.getLoggedInUser();
        if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        } else {
        	userTask = getIncompleteRequestTasks();
       }
		BPMWorklistClient client = new BPMWorklistClient(contextPath);
		logger.logInfo("UserTask is: " + userTask.getTaskType());
		if(userTask!=null){
			client.completeTask(userTask, loggedInUser, taskOutcome);
		}
		logger.logInfo("completeTask() completed successfully!!!");
		return success;
	}
	
	public UserTask getIncompleteRequestTasks() {
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";		 
		try {
	   		Long requestId = getRequestView().getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();	   		
	   		if( null!=taskId ) {	   		
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);				 
				taskType = userTask.getTaskType();
				logger.logInfo("Task Type is:" + taskType);
				return userTask;
	   		} else {	   		
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		} catch(PIMSWorkListException ex) {				
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {			
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC) {			
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else {			
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		} catch (Exception exception) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		return null;
	}				
	
	public void populateFinanceData() {
		try
		{
			FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
			if( null!=tabFinanceController ) 
			{
				tabFinanceController.handleTabChange();
			}
		}
		catch (Exception ex) {
				logger.LogException( "[Exception occured in populateFinanceData()]", ex);
		}
	}
	
	public void getRequestHistory() {		
		logger.logInfo("[getRequestHistory() .. Starts]");    			
		try	{
    		Long reqId = getRequestView().getRequestId();
    		if ( null!=reqId && !reqId.equals(INVALID_STATUS) ) {
    			RequestHistoryController rhc=new RequestHistoryController();
    			rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, reqId.toString());
    		}
    	} catch(Exception ex) {
    		logger.logInfo("[Exception occured in getRequestHistory()]", ex);
    		ex.printStackTrace();
    	}    	
		logger.logInfo("[getRequestHistory() .. Ends]");	
	}
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}

	public HtmlTabPanel getRichPanel() {
		return richPanel;
	}

	public void setRichPanel(HtmlTabPanel richPanel) {
		this.richPanel = richPanel;
	}
	public void openAssociateCostCenters() 
    {
    	final String methodName = "openAssociateCostCenters";
    	try
    	{
    	
    	DisbursementDetailsView gridViewRow = ( DisbursementDetailsView ) disbursementTable.getRowData();
		String javaScriptText = "javaScript:openAssociateCostCenters();";
        sessionMap.put( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW, gridViewRow );
        if( gridViewRow.getRequestBeneficiaries() != null &&  gridViewRow.getRequestBeneficiaries().size() > 0 )
        {
        	
        	sessionMap.put( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW, gridViewRow.getRequestBeneficiaries().get( 0 ) );
        	
        }
		sendToParent( javaScriptText );
    	}
    	catch( Exception e )
    	{
    		logger.LogException( methodName +"Error Occured:", e );
    		errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

    	}
	}
	public void onMessageFromAssociateCostCenter()
	{
		try
		{
			DisbursementDetailsView message = (DisbursementDetailsView) sessionMap.get(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);				
			sessionMap.remove(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);
			List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
			for (DisbursementDetailsView dtView : disbursementDetails) 
			{
				if(dtView.getId() == message.getId() )
				{
				  disbursementDetails.remove( dtView  );
				  disbursementDetails.add( message );
				  successMessages.add(CommonUtil.getBundleMessage( "commons.associateCostCenterDoneSuccessfully" ));
				  break;
				}
                								
				
			}
			setDisbursementDetails( disbursementDetails );
		}
		catch( Exception e)
		{
			logger.LogException( " [onMessageFromAssociateCostCenter --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}	

	public void openAvailableBankTransfersPopUp(javax.faces.event.ActionEvent event)
	{
		if( !cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL)) 
		{
			Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
			
			String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 250;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('AvailableBankTransfers.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.PERSON_ID
				+ "="
				+ beneficiaryId
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
			sendToParent(javaScriptText);
		} 
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.AVAILABLE_BALANCE_ERROR_BENEF_MISSING));
		}
		
	}
	
    @SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) {
    	
    	DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
		String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 550;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.PERSON_ID
				+ "="
				+ gridViewRow.getPersonId()
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sendToParent(javaScriptText);

	}

	public HtmlInputTextarea getHtmlRFC() {
		return htmlRFC;
	}

	public void setHtmlRFC(HtmlInputTextarea htmlRFC) {
		this.htmlRFC = htmlRFC;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public HtmlInputText getBlockingBalance() {
		return blockingBalance;
	}

	public void setBlockingBalance(HtmlInputText blockingBalance) {
		this.blockingBalance = blockingBalance;
	}
	
	@SuppressWarnings("unchecked")
	public String getTotalDisbursementCount() {
		if( viewMap.get("totalDisbursementCount") != null )
		{
			 totalDisbursementCount = viewMap.get("totalDisbursementCount").toString();
		}
		return totalDisbursementCount;
	}

	@SuppressWarnings("unchecked")
	public void setTotalDisbursementCount(String totalDisbursementCount) {
		this.totalDisbursementCount = totalDisbursementCount;
		viewMap.put("totalDisbursementCount",this.totalDisbursementCount);
		
	}
}

