package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.CoreException;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.core.web.context.impl.ViewContextImpl;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.EvacuationServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ChequeWithdrawlReportCriteria;
import com.avanza.pims.report.criteria.SettlementReportCriteria;
import com.avanza.pims.report.processor.SettlementReportProcessor;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.EvacuationService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractForm;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentBean;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings( "unchecked" )
public class SettlementBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2349243306136322685L;
	private static final Logger logger = Logger.getLogger(SettlementBean.class);
	private static final String MODE_SETTLE_PAYMENT_REQ = "SETTLE_PAYMENT_REQ";
	private static final String MODE_READONLY = "MODE_READONLY";
	private boolean isParentTransferContract = false;
	private boolean isParentContractList = false;
	private static final String MODE_DEFAULT = MODE_SETTLE_PAYMENT_REQ;
	
	private static final String EVENT_SETTLEMENT = "Event.Evacuation.Settlement";
	private static final String BEAN_PAYMENTS_RETURN_CHEQUES = "paymentsBeanReturn";
	private static final String BEAN_PAYMENTS = "paymentsBean";
	private static final String BEAN_PAYMENTS_OTHER = "otherpaymentsBean";
	private static final String BEAN_CONTRACT = "contractBean";
	private static final String CONTRACT_DURATION = "contract_Duration";
	private static final String EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION = "EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION";
	 
	private List<String> errorMessages=new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	private String pageMode = MODE_DEFAULT;
	private HtmlCalendar evacuationClnder=new HtmlCalendar();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer recordSizeReturnCheque = 0;
	private Integer recordSizeOtherPayments= 0;
	
	private transient EvacuationServiceAgent evacuationService = new EvacuationServiceAgent();
	private transient PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private transient CommonUtil commonUtil = new CommonUtil();
	private ContractView contractView;
	private transient HtmlDataTable paymentTable;
	private transient HtmlDataTable paymentChequestToReturnTable;
	private transient HtmlDataTable paymentTableOther;

	private SettlementReportCriteria reportCriteria;
	private ChequeWithdrawlReportCriteria withDrawlCriteria ;
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private boolean isTransferSettlementTask;
	private boolean isCancelContractSettlementTask;
	private HtmlCommandButton btnRejectBtn;
	public SettlementBean()
	{
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_EN,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		else
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_AR,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		if(getLoggedInUser().getSecondaryFullName()!= null && getLoggedInUser().getSecondaryFullName().length()>0)
			withDrawlCriteria = new ChequeWithdrawlReportCriteria(ReportConstant.Report.CHEQUE_WITHDRAWL_REPORT, ReportConstant.Processor.CHEQUE_WITHDRAWL_REPORT, getLoggedInUser().getSecondaryFullName());
		else
			withDrawlCriteria = new ChequeWithdrawlReportCriteria(ReportConstant.Report.CHEQUE_WITHDRAWL_REPORT, ReportConstant.Processor.CHEQUE_WITHDRAWL_REPORT, getLoggedInUser().getFullName());
	}
	
	public boolean isReadOnly(){
		if (pageMode.equalsIgnoreCase(MODE_READONLY))
			return true;
		return false;
	}
	
	public boolean isFromTaskList(){
		UserTask userTask = (UserTask) viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
		if (userTask != null)
			return true;
		return false;
	}
	
	public boolean isShowAddPayment(){
		//if(pageMode==null)
			//return false;
		//if (pageMode.equalsIgnoreCase(MODE_DEFAULT) && viewMap.containsKey("canAddPayment"))
			return true;
		//return false;
	}
	
	public boolean isShowCollectPayment(){
		//if(pageMode==null)
			//return false;
		//if (pageMode.equalsIgnoreCase(MODE_DEFAULT) && viewMap.containsKey("canCollectPayment"))
			return true;
		//return false;
	}
	
	public boolean isShowCancelPayment(){
		if(pageMode==null)
			return false;
		if (pageMode.equalsIgnoreCase(MODE_DEFAULT) && viewMap.containsKey("canCancelPayment"))
			return true;
		return false;
	}
	
	public boolean isShowPrintSettlement(){
		if (viewMap.containsKey("canPrintSettlement"))
			return true;
		return false;
	}
	
	public boolean isShowPrintReceipt(){
		if (viewMap.containsKey("canPrintReceipt"))
			return true;
		return false;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public void init() {

		logger.logDebug("init|Start");
		super.init();
		
	    if(getLoggedInUserObj().getPermission("Pims.Settlement.EvacuationOptions")!=null)
	    	evacuationClnder.setDisabled(false);
	    else
	    	evacuationClnder.setDisabled(true);
	   
	    
		initContext();
		
		logger.logDebug("init|isPostBack()=" + isPostBack());
		
		errorMessages=new ArrayList<String>(0);
		successMessages=new ArrayList<String>(0);
		if (viewContext != null && viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		try 
		{
			//Added to identify whether the parent is TransferContract
			if(webContext.getAttribute("parentPage") != null)
			{
				String parentPage = webContext.getAttribute("parentPage");
				viewContext.setAttribute("parentPage", parentPage);
				webContext.removeAttribute("parentPage");
			}
			if(sessionMap.containsKey("FROM_CANCEL_CONTRACT_BEAN"))
			{
			  viewMap.put("FROM_CANCEL_CONTRACT_BEAN", true);
			  sessionMap.remove("FROM_CANCEL_CONTRACT_BEAN"); 
			}
			if(viewContext.getAttribute("parentPage") != null && viewContext.getAttribute("parentPage").equals("TransferContract"))			
				isParentTransferContract = true;
			else if(viewContext.getAttribute("parentPage") != null && viewContext.getAttribute("parentPage").equals("contractList"))
			{
			  this.setParentContractList(true);
			}
			if (!isPostBack()) 
			{
				loadAttachmentsAndComments(null);
				RequestView requestView =null;
				String applicantId ="";
				contractView = getContract();
				ContractView contractTempView = propertyServiceAgent.getContract(contractView.getContractNumber(), getDateFormat());
				Long ownershipTypeId = getOwnerShipTypeId(contractTempView);
				viewMap.put(WebConstants.OWNWERSHIP_TYPE_ID,ownershipTypeId );
				if(webContext.getAttribute("requestView")!=null)
				{
					requestView = webContext.getAttribute("requestView");// put to session
					applicantId = requestView.getApplicantView().getPersonId().toString();
					if(StringHelper.isNotEmpty(applicantId))
					{
						PropertyServiceAgent psa=new PropertyServiceAgent();
			    		PersonView pv=new PersonView();
			    		pv.setPersonId(new Long(applicantId));
			    		List<PersonView> personsList =  psa.getPersonInformation(pv);
			    		if(!personsList.isEmpty())
			    		{
			    			pv = personsList.get(0);
			    			viewContext.setAttribute( WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
			    		}
					}
				}
				if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				{
					 UserTask userTask = (UserTask) webContext.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
					 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.CANCEL_CONTRACT_TASK_LIST, userTask);
					 getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				}
				webContext.removeAttribute("contractView");
				webContext.removeAttribute("requestView");
				webContext.removeAttribute("pageMode");
				if (requestView != null)
				{
					viewContext.setAttribute("requestView", requestView);
				    loadAttachmentsAndComments(requestView.getRequestId());
				}
				viewContext.setAttribute("contractView", contractView);
				viewContext.setAttribute("pageMode", pageMode);
				viewContext.setAttribute("applicantId", applicantId);
				viewContext.setAttribute(BEAN_CONTRACT, this.createBeanFromContract( contractView ));
				if (this.contractView.getSettlementDate() != null) 
				{
					calculateSettlementForFirstTime();
				}
				else
				{
					this.populatePaymentsTab(contractView.getContractId());
				}
			}
			else
			{
				//IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
                if( sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
             		sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
                )
                {
                	List<PaymentScheduleView>psvList = new ArrayList<PaymentScheduleView>( 0 );
                	for (PaymentScheduleView psv : ( ArrayList<PaymentScheduleView> )sessionMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE ) ) 
                	{
                		psv.setContractId(this.getContractBean().getContract().getContractId());
                		psvList.add(psv);
					}
             	    new UtilityServiceAgent().persistPaymentSchedule(psvList);
             	    sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
             	    this.populatePaymentsTab(this.getContractBean().getContract().getContractId());
                }
                else if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) 
   		    		 &&  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null
   		    		 && (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
   		    	    )
   			 	{
   	    		       sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
   	    		       CollectPayments();
   	    		       this.populatePaymentsTab(this.getContractBean().getContract().getContractId());
   			 	}
			}
			logger.logDebug("init|pageMode = %s", pageMode);
			logger.logDebug("init|Finish");
		} 
		catch (Exception ex) 
		{
			logger.LogException("init|Exception Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void btnPrintSettlement_Click()
    {
		
		contractView = (ContractView)viewContext.getAttribute("contractView");
		ContractForm contractForm = this.getContractBean();
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		if (contractForm.getEvacDate() != null && contractForm.getEvacDate().compareTo(locale.getDateFormated(contractForm.getContractStartDate())) >= 0 && contractForm.getEvacDate().compareTo(locale.getDateFormated(contractForm.getContractExpiryDate())) <= 0)
			{
			contractView.setSettlementDate(contractForm.getEvacDate());
			webContext.setAttribute("contractView", contractView);
			String javaScriptText ="javaScript:printSettlement();";
		    AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
		    addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
			
			
			}
		else
			errorMessages.add(new String(ResourceUtil.getInstance().getProperty("settlement.message.evacmessage")));
		
		
    }
	private void CollectPayments()
	{
		String methodName = "CollectPayments";
		logger.logInfo(methodName+"|Start");
		successMessages = new ArrayList<String>(0);
		successMessages.clear();
		errorMessages = new ArrayList<String>(0);
		errorMessages.clear();
		try
		{
			 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     prv = psa.collectPayments(prv);
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
		     //CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext());
		     logger.logInfo(methodName + "|" + "Finish...");
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured..."+ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private ContractView getContract() {
		
		ContractView contractView = webContext.getAttribute("contractView");
		if (contractView == null) {
			contractView = (ContractView) getRequestParam(WebConstants.Contract.CONTRACT_VIEW);
		}
		return contractView;
	}
    
	private void initContext() {
		webContext = ApplicationContext.getContext().get(WebContext.class);
    	requestContext = ApplicationContext.getContext().get(RequestContext.class);
    	viewContext = ApplicationContext.getContext().get(ViewContext.class);

    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	webContext = new WebContextImpl(req.getSession());
    	
    	requestContext = new RequestContextImpl(req);
    	if (getFacesContext().getViewRoot() != null) {
    		viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
    		ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
    	}
    	ApplicationContext.getContext().add(WebContext.class.getName(), webContext);
    	ApplicationContext.getContext().add(RequestContext.class.getName(), requestContext);
		if (viewContext != null && viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().containsKey("SETTLE_PAGE_MODE"))
		{  
			pageMode =MODE_READONLY;
			viewContext.setAttribute("pageMode",pageMode );
		}
	}
	
	private ContractForm createBeanFromContract(ContractView contractView) throws PimsBusinessException,Exception
	{
		
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		ContractForm retVal = new ContractForm();
		retVal.setContract(contractView);
		retVal.setContractNo(contractView.getContractNumber());
		retVal.setContractStartDate(contractView.getStartDateString());
		retVal.setContractType(english?contractView.getContractTypeEn():contractView.getContractTypeAr());
		retVal.setStatus(english?contractView.getStatusEn():contractView.getStatusAr());
		retVal.setContractExpiryDate(contractView.getEndDateString());
		retVal.setRentalAmount(contractView.getRentAmount());
		if (contractView.getSettlementDate() != null)
		{
			retVal.setEvacDate(new Date(contractView.getSettlementDate().getTime()));
		}
		if(contractView.getTenantView() != null) 
		{
			retVal.setTenantId(contractView.getTenantView().getPersonId()!=null?contractView.getTenantView().getPersonId().toString():"");
		    retVal.setTenantName(contractView.getTenantView().getPersonFullName());
		    retVal.setGrpCustomerNumber(contractView.getTenantView().getGrpCustomerNo());
		}
		if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0) 
		{ 
			List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
			contractUnitViewList.addAll(contractView.getContractUnitView());
			ContractUnitView contractUnitView = contractUnitViewList.get(0);

			retVal.setUnitType(english ? contractUnitView.getUnitView().getUnitTypeEn():contractUnitView.getUnitView().getUnitTypeAr());
			retVal.setPropertyName(contractUnitView.getUnitView().getPropertyCommercialName());
			retVal.setUnitRefNo(contractUnitView.getUnitView().getUnitNumber());
			retVal.setOwnerShipTypeId(contractUnitView.getUnitView().getPropertyCategoryId());
            retVal.setGrpUnitCostCenter(contractUnitView.getUnitView().getAccountNumber());		
		}	
		
		return retVal;
	}
	@SuppressWarnings("unchecked")
	private void populateReturnChequesTab()
	{
		String methodName ="populateReturnChequesTab";
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		List<PaymentScheduleView> payments = new ArrayList<PaymentScheduleView>(0);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		List<PaymentBean> pbList = new ArrayList<PaymentBean>(0);
		if(viewMap.containsKey("paymentSchedules"))
			payments = (ArrayList<PaymentScheduleView>)viewMap.get("paymentSchedules");
		ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
		DomainDataView collectedStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_COLLECTED);
		
		java.text.DateFormat df= new SimpleDateFormat("dd/MM/yyyy"); 		
		for (PaymentScheduleView view : payments) {
				
			//boolean isUnRealised = (view.getStatusId().compareTo(collectedStatus.getDomainDataId() )==0);
			try
			{
			
			if (view.getStatusId().compareTo(collectedStatus.getDomainDataId())==0 &&
				view.getPaymentDueOn()!=null 
				&& df.parse(df.format(view.getPaymentDueOn())).compareTo(this.getContractBean().getEvacDate())> 0 && view.getPaymentReceipts() != null
				) {
				for (PaymentReceiptView receiptView : view.getPaymentReceipts()) {
					if (receiptView.getPaymentReceiptDetails() != null) {
						
						for (PaymentReceiptDetailView detailView : receiptView.getPaymentReceiptDetails()) {
							HashMap prtIdMap = detailView.getPaymentReceiptTermId();
							
					  if(prtIdMap.containsKey(view.getPaymentReceiptTermId()) && 
							  detailView.getPaymentMethod().getPaymentMethodId().compareTo(WebConstants.PAYMENT_METHOD_CHEQUE_ID)==0  )
					   {
						
							PaymentBean pb2 = new PaymentBean();
							pb2.setRecieptNo(receiptView.getReceiptNumber());
							pb2.setHasWithDrawPrinted( view.getHasWithDrawPrinted() );
							pb2.setWithDrawPrintedBy( view.getWithDrawPrintedBy() );				
							pb2.setDueDate(locale.getShortDateFormated(view.getPaymentDueOn()));
							pb2.setStatusId(view.getStatusId());
							if (detailView.getPaymentMethod() != null && StringHelper.isNotEmpty(detailView.getMethodRefNo()))
							{
								pb2.setPaymentMethod(detailView.getPaymentMethod().getDescription() + "/" + detailView.getMethodRefNo());
								pb2.setChequeNumber(detailView.getMethodRefNo());
							}
							else if (detailView.getPaymentMethod() != null)
							{
								pb2.setPaymentMethod(detailView.getPaymentMethod().getDescription());
								pb2.setStatus(english?detailView.getMethodStatusEn():detailView.getMethodStatusAr());
							}
							
							if (StringHelper.isEmpty(pb2.getStatus()))
								pb2.setStatus(english?view.getStatusEn():view.getStatusAr());
							if (StringHelper.isEmpty(pb2.getStatus()))
								pb2.setStatus("N/A");
							
							pb2.setAmountVal(detailView.getAmount());
							pb2.setPaymentScheduleView(view);
							pb2.setSelected(false);
							pbList.add(pb2);
						
						break;
						}
						}
						
					
					
					}
				
				
				}
			}
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|Error Occured::",ex);
			}
		}
		viewContext.setAttribute(BEAN_PAYMENTS_RETURN_CHEQUES, pbList); 
		recordSizeReturnCheque = pbList.size();
		viewMap.put("recordSizeReturnCheque", recordSizeReturnCheque);
	}
	@SuppressWarnings("unchecked")
	private void populatePaymentsTab(Long contractId) throws PimsBusinessException,Exception 
	{

		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		List<PaymentBean> otherPaymentBeans = new ArrayList<PaymentBean>();
		List<PaymentScheduleView> payments = null;
		ContractForm contractForm = this.getContractBean();
		contractForm.setTotalDepositAmount(0.0);
		contractForm.setTotalRealizedAmount(0.0);
		contractForm.setTotalUnrealizedAmount(0.0);
		contractForm.setOtherUnpaidfines(0.0);
		RequestView requestView = viewContext.getAttribute("requestView");// put to session
		if( !isParentTransferContract() && !this.isParentContractList()  && requestView != null && requestView.getRequestId() != null )
		{
		 Double maintenanceFines = new EvacuationService().getDamageAmountFromInspectionViolations( requestView.getRequestId() ); 
		 contractForm.setOtherUnpaidfines( maintenanceFines );
		 contractForm.setOtherUnpaidfines( contractForm.getOtherUnpaidfines() + contractForm.getUnpaidfinesEnteredByUser() );
		}
		ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
		DomainDataView pendingStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_PENDING);
		DomainDataView realisedStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_REALISED);
		DomainDataView canceledStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_CANCELED);
		DomainDataView replacedStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_REPLACED);
		DomainDataView errorStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_ERROR);
		DomainDataView collectedStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_COLLECTED);
		DomainDataView returnedStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, WebConstants.PAYMENT_SCHEDULE_STATUS_RETURNED);
		DomainDataView collectedReturnStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_STATUS_COLLECTED_RETURN);
		DomainDataView paymentModeCash = appBean.getDomainData(WebConstants.PAYMENT_SCHEDULE_MODE,WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		DomainDataView paymentModeCheque = appBean.getDomainData(WebConstants.PAYMENT_SCHEDULE_MODE,WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
		DomainDataView paymentModeBankTransfer = appBean.getDomainData(WebConstants.PAYMENT_SCHEDULE_MODE,WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER);
		try 
		{
			payments = evacuationService.getPaymentSchedules(new Long(contractId));
		} 
		catch (PimsBusinessException e) 
		{
			logger.LogException("Error:CancelContract while populating the paymentsTab", e);
		}
		
		if (payments != null) 
		{
			viewMap.put("paymentSchedules",payments);
			if(contractForm.getEvacDate()!= null)
			 populateReturnChequesTab();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			for (PaymentScheduleView view : payments) 
			{

				PaymentBean pb = new PaymentBean();
				pb.setAmountVal(view.getAmount());
				if (view.getPaymentDueOn() != null)
					pb.setDueDate(locale.getShortDateFormated(view.getPaymentDueOn()));
				pb.setPaymentType(english?view.getTypeEn():view.getTypeAr());
				pb.setPaymentTypeId(view.getTypeId());
				pb.setPaymentScheduleView(view);
				pb.setPaymentMethod(english?view.getPaymentModeEn():view.getPaymentModeAr());
				pb.setHasWithDrawPrinted( view.getHasWithDrawPrinted() );
				pb.setWithDrawPrintedBy( view.getWithDrawPrintedBy() );
				if( view.getPaymentModeId()!=null )
				{
					if( view.getPaymentModeId().longValue()== paymentModeCash.getDomainDataId().longValue() )
					pb.setPaymentMethodId(WebConstants.PAYMENT_METHOD_CASH_ID);
					else if( view.getPaymentModeId().longValue()== paymentModeCheque.getDomainDataId().longValue() )
						pb.setPaymentMethodId(WebConstants.PAYMENT_METHOD_CHEQUE_ID );
					else if( view.getPaymentModeId().longValue()== paymentModeBankTransfer.getDomainDataId().longValue() )
						pb.setPaymentMethodId(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID );
				}
				pb.setStatusId(view.getStatusId());
				if(StringHelper.isEmpty(pb.getPaymentMethod()))
					pb.setPaymentMethod("N/A");
				pb.setRecieptNo(view.getPaymentReceiptNumber());
				if(StringHelper.isEmpty(pb.getRecieptNo()))
					pb.setRecieptNo("N/A");
				pb.setStatus(english?view.getStatusEn():view.getStatusAr());
				if (view.getPaymentReceipts() == null || view.getPaymentReceipts().isEmpty()) 
				{
					addToList(paymentBeans, otherPaymentBeans,view, pb,pendingStatus);	
					this.calculatePaymentValues(pb,pendingStatus ,realisedStatus, canceledStatus, replacedStatus, errorStatus, collectedStatus,returnedStatus);
				}
				else if (view.getPaymentReceipts() != null) 
				{
					for (PaymentReceiptView receiptView : view.getPaymentReceipts()) 
					{
						PaymentBean pb2 = new PaymentBean();
						pb2.setAmount(pb.getAmount());
						pb2.setDueDate(pb.getDueDate());
						pb2.setPaymentType(pb.getPaymentType());
						pb2.setPaymentTypeId(pb.getPaymentTypeId());
						pb2.setStatusId(pb.getStatusId());
						pb2.setPaymentScheduleView(pb.getPaymentScheduleView());
						if(StringHelper.isEmpty(pb.getPaymentMethod()))
							pb.setPaymentMethod("N/A");
						pb2.setStatus(pb.getStatus());
						
					
						
						if (StringHelper.isNotEmpty(receiptView.getReceiptNumber()))
							pb2.setRecieptNo(receiptView.getReceiptNumber());
						pb = pb2;
						if (receiptView.getPaymentReceiptDetails() == null || receiptView.getPaymentReceiptDetails().isEmpty()) 
						{
							addToList(paymentBeans, otherPaymentBeans,view, pb,pendingStatus);	
							this.calculatePaymentValues(pb,pendingStatus , realisedStatus, canceledStatus, replacedStatus, errorStatus, collectedStatus,returnedStatus);
						}
						else if (receiptView.getPaymentReceiptDetails() != null) 
						{
							
							for (PaymentReceiptDetailView detailView : receiptView.getPaymentReceiptDetails()) 
							{
								HashMap prtIdMap = detailView.getPaymentReceiptTermId();
								
								if(prtIdMap.containsKey(view.getPaymentReceiptTermId()) )
								{
									pb2 = new PaymentBean();
									pb2.setRecieptNo(receiptView.getReceiptNumber());
									pb2.setAmount(pb.getAmount());
									pb2.setDueDate(pb.getDueDate());
									pb2.setPaymentType(pb.getPaymentType());
									pb2.setPaymentTypeId(pb.getPaymentTypeId());
									pb2.setStatusId(pb.getStatusId());
									pb2.setPaymentScheduleView(pb.getPaymentScheduleView());
									pb2.setRecieptNo(pb.getRecieptNo());
									
									if (detailView.getPaymentMethod() != null && StringHelper.isNotEmpty(detailView.getMethodRefNo()))
									{
										pb2.setPaymentMethod(detailView.getPaymentMethod().getDescription() + "/" + detailView.getMethodRefNo());
										pb2.setPaymentMethodId(detailView.getPaymentMethod().getPaymentMethodId());
										pb2.setChequeNumber(detailView.getMethodRefNo());
										String paymentDue = df.format(view.getPaymentDueOn());
										try
										{
										if(    this.isParentContractList() && 
										       (view.getStatusId().compareTo(collectedStatus.getDomainDataId())==0 ||
										           view.getStatusId().compareTo(pendingStatus.getDomainDataId())==0 )
												&& this.getContractBean().getEvacDate()!=null &&  
												df.parse(paymentDue).compareTo(this.getContractBean().getEvacDate())<= 0 )
										{
											pb2.setStatus(english?realisedStatus.getDataDescEn():realisedStatus.getDataDescAr());
											pb2.setStatusId(realisedStatus.getDomainDataId());
										}
										}catch(Exception ex)
										{
											
										}
									}
									else if (detailView.getPaymentMethod() != null)
									{
										pb2.setPaymentMethod(detailView.getPaymentMethod().getDescription());
										pb2.setPaymentMethodId(detailView.getPaymentMethod().getPaymentMethodId());
										// TODO KAzim Add method status Id
										pb2.setStatus(english?detailView.getMethodStatusEn():detailView.getMethodStatusAr());
									}
									
									if (StringHelper.isEmpty(pb2.getStatus()))
										pb2.setStatus(pb.getStatus());
									if (StringHelper.isEmpty(pb2.getStatus()))
										pb2.setStatus("N/A");
									
									
									//This line is changed bcz more than 1 PS can bind with one PRD
									//pb2.setAmountVal(detailView.getAmount());
									//pb2.setAmountVal(detailView.getAmount());
									pb2.setAmountVal(view.getAmount());
									pb = pb2;
									addToList(paymentBeans, otherPaymentBeans,view, pb,pendingStatus);	
									this.calculatePaymentValues(pb,pendingStatus , realisedStatus, canceledStatus, replacedStatus, errorStatus, collectedStatus,returnedStatus);
									break;
								}
							}
							
						}
						
					}
				}
			}
			contractForm.setTotalPaidAmount(contractForm.getTotalRealizedAmount() + contractForm.getTotalDepositAmount() );
			contractForm.setTotalUnpaidAmount(contractForm.getTotalEvacFines() + contractForm.getOtherUnpaidfines() + contractForm.getTotalUnrealizedAmount());
		}
		viewContext.setAttribute(BEAN_PAYMENTS, paymentBeans);
		viewContext.setAttribute(BEAN_PAYMENTS_OTHER, otherPaymentBeans);
		recordSize = paymentBeans.size();
		viewMap.put("recordSize", recordSize);
	}

	private void addToList(List<PaymentBean> paymentBeans,
			List<PaymentBean> otherPaymentBeans, PaymentScheduleView view,
			PaymentBean pb,DomainDataView pendingStatus) {
		if(isPaymentTypeOtherPayments(view))
		{ 
		  if(pb.getStatusId().compareTo(pendingStatus.getDomainDataId())==0)
			  pb.getPaymentScheduleView().setShowSelect(true);
			  
			  otherPaymentBeans.add(pb);
		}
		else 
			paymentBeans.add(pb);
	}

	private boolean  isPaymentTypeOtherPayments(PaymentScheduleView psv)
	{
		//All payments other then rent and depost will be shown in other payments tab
		if(psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_DEPOSIT_ID)!=0 &&
			psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)!=0 )
		  return true;
		
		return false;
	}
	@SuppressWarnings("unchecked")	
	private void calculatePaymentValues(PaymentBean pb,DomainDataView pendingStatus, DomainDataView realisedStatus, 
			           DomainDataView canceledStatus, DomainDataView replacedStatus, DomainDataView errorStatus, DomainDataView collectedStatus,
			           DomainDataView returnedStatus) 
	{
		ContractForm contractForm = this.getContractBean();
		
		if( pb.getStatusId().compareTo( realisedStatus.getDomainDataId() ) == 0 ||
			pb.getStatusId().compareTo( collectedStatus.getDomainDataId() ) == 0 ||
			pb.getStatusId().compareTo( pendingStatus.getDomainDataId() ) == 0
		  )
		{
			boolean isUnRealised =false;
			//If cash and its status is collected then isUnRealised=false(i.e collected cash is always considered realised)
			//If status is pending then isUnRealised=true or
			// if cheque and status is not realised then isUnRealised=true
			if(  pb.getStatusId().compareTo( pendingStatus.getDomainDataId() ) == 0 ||
			  (  pb.getPaymentMethodId() != null && 
				 pb.getPaymentMethodId().longValue()==( Constant.PaymentMethod.CHEQUE_ID )&& 
			     pb.getStatusId().compareTo( realisedStatus.getDomainDataId() )!=0 ) 
			  )       
				isUnRealised = true;
			if( pb.getPaymentTypeId().compareTo( Constant.PAYMENT_TYPE_RENT_ID )==0 )
			{
				if ( !isUnRealised )
					contractForm.setTotalRealizedAmount( contractForm.getTotalRealizedAmount() + pb.getAmountVal() );
				else  
					contractForm.setTotalUnrealizedAmount( contractForm.getTotalUnrealizedAmount() + pb.getAmountVal() );
			}
			if( pb.getPaymentTypeId() != null )
			{
				if ( pb.getPaymentTypeId()!=null && pb.getPaymentTypeId().compareTo( ( Constant.PAYMENT_TYPE_DEPOSIT_ID ) )==0)
					contractForm.setTotalDepositAmount( contractForm.getTotalDepositAmount() + pb.getAmountVal() );
				else if ( isUnRealised && ( pb.getPaymentTypeId() != null && 
						                    pb.getPaymentTypeId().compareTo( Constant.PAYMENT_TYPE_FINE_ID ) == 0  )  )				
					contractForm.setOtherUnpaidfines( contractForm.getOtherUnpaidfines() + pb.getAmountVal() );
			}
		}
	}
	
	public void onPaymentDetail(ActionEvent event)
	{
		logger.logDebug("SettlementBean:onPaymentDetail() called.");
		PaymentBean paymentBean = (PaymentBean) this.paymentTable.getRowData();
		PaymentScheduleView psview = paymentBean.getPaymentScheduleView();
		if (psview.getPaymentReceipts() != null && !psview.getPaymentReceipts().isEmpty()) 
		{
			webContext.setAttribute(WebConstants.PAYMENT_RECEIPT_VIEW, psview.getPaymentReceipts().get(0));
			AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
			String javaScript = "javascript:openPopupReceivePayment();";
			addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScript);
		}
	}
	
	public void onCalculateSettlement( ) {
		logger.logDebug("SettlementBean:onCalculateSettlement() called.");
		if (this.validateInput()) {
			this.calculateSettlement(false, null);			
		}
	}
	public void onAddEvacFines(ActionEvent event)
	{
		logger.logDebug("SettlementBean:onAddEvacFines() called.");
		if ( this.validateInput() ) 
			this.calculateSettlement(true, this.getContractBean().getTotalEvacFines());
	}
	
	private void calculateSettlement(boolean addEvacFinesFromScreen, Double evacFines) 
	{
		String methodName = "calculateSettlement|";
		ContractForm contractForm = this.getContractBean();
		try 
		{
			logger.logInfo( methodName + "Start");
			
			ApplicationBean appBean = ( ApplicationBean ) getValue( "#{pages$ApplicationBean}" ) ;
			LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );
			Date endDate = contractForm.getEvacDate();
			Date expDate = locale.getDateFormated( contractForm.getContractExpiryDate() );
			//String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration( WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR );
			Double amountGrandTotal = 0D;
			getActualRentPeriod( contractForm );
			Double contractDuration = new Double( getContractDurationDays( contractForm ) );
			logger.logInfo( methodName + "contractDuration:%s",contractDuration);
			contractForm.setActualRentalAmount( Math.ceil( ( contractForm.getTotalDays()/contractDuration )  * contractForm.getRentalAmount() ) );
			if( endDate.compareTo( expDate ) > 0 )
				calculateForExtendedPeriod( contractForm );
			
			//For transfer Contract no evacuation fines will be calculated
			if( isParentTransferContract )
				contractForm.setTotalEvacFines( new Double( 0 ) );
			//Calculate other anulation fines
			else if ( !addEvacFinesFromScreen && this.getExempted().compareTo(0L)==0 ) 
			{ 
				Double totalEvacFines = 0.0D;
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Date formattedEvacDate = df.parse(df.format(endDate));
				Date formattedExpiryDate = df.parse(df.format(expDate));
				// if evac date is before expiry date then calculate fine
				if( formattedEvacDate.compareTo( formattedExpiryDate ) < 0 )
				  totalEvacFines += calculateFineForEvacDateBeforeExpiryDate( contractForm, appBean, contractDuration.toString() );

				// if request date is not before specified months than calculate fine
				else
				  totalEvacFines += calculateFineForRequestNotBeforeSpecifedMonths( contractForm, appBean, expDate, contractDuration.toString() );
				contractForm.setTotalEvacFines( (double) ( ( (long ) ( totalEvacFines * 100 ) ) / 100.00 ) );
			} 
			else if (evacFines == null || this.getExempted().compareTo(1L) == 0)
				contractForm.setTotalEvacFines(0.0D);
			this.populatePaymentsTab(contractForm.getContract().getContractId());
			//Now calculating the Grand totals
			amountGrandTotal =        (contractForm.getActualRentalAmount() - contractForm.getTotalPaidAmount()) +
									  (contractForm.getTotalEvacFines() + contractForm.getOtherUnpaidfines());
			if (amountGrandTotal > 0) 
			{
				contractForm.setTotalRefundAmount(0.0);
				contractForm.setTotalDueAmount(amountGrandTotal);
			}
			else 
			{
				contractForm.setTotalRefundAmount( Math.ceil( Math.abs( amountGrandTotal) ));
				contractForm.setTotalDueAmount(0.0);
			}
			if( !isParentContractList )
			{
			 Map<String,Double> map = new HashMap<String, Double>();
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");				
			 getCreditMemo(map, contractForm, df);
			}
			contractForm.setSettlementCalculated(true);
			logger.logInfo( methodName + "Finish");
		} catch (Throwable t)
		{
			contractForm.setSettlementCalculated(false);
			logger.LogException("Exception when calculating the Settlement Values for contractId:%s", t, contractForm.getContract().getContractId());
		}
	}

	private void getActualRentPeriod(ContractForm contractForm ) throws Exception 
	{
		String methodName = "getActualRentPeriod|";
	    logger.logInfo( methodName + "Start");
		
		LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );
		// Calculate Actual Rent Period( Months/Days) Start
		Integer[] months_days=  CommonUtil.calculateMonthsDaysBetween( locale.getDateFormated( contractForm.getContractStartDate() ), 
				                                   contractForm.getEvacDate() );
		contractForm.setMonths( months_days[0] );
		contractForm.setDays( months_days[1] );
		 // Calculate Actual Rent Period( Months/Days) end
		contractForm.setTotalDays( new Long( ( contractForm.getMonths() * 30 ) )+ new Long( contractForm.getDays() ) );
		logger.logInfo( methodName + "Finish|Months stayed:%s|Days stayed:%s|Total Days:%s|", contractForm.getMonths(),
				 contractForm.getDays(), contractForm.getTotalDays() );
	}
	private void calculateForExtendedPeriod( ContractForm contractForm ) throws Exception
	{
		String methodName = "calculateForExtendedPeriod|";
		logger.logInfo( methodName +"Start" );
		LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );

		contractForm.setRentalAmountExtendedDuration(  
		                                             contractForm.getActualRentalAmount( )	- contractForm.getRentalAmount()	
		                                            );
		Integer[] months_days=  CommonUtil.calculateMonthsDaysBetween( locale.getDateFormated( contractForm.getContractExpiryDate() ), 
                                                             contractForm.getEvacDate() );
        contractForm.setExtendedMonths( months_days[0] );
        //SUBTRACTING 1 BECAUSE SUB FROM EXPIRY TILL EVAC DATE NOT FROM EXP+1 TILL EVAC THEREFORE REMOVING 1 EXTRA DAY OF EXPIRY
        //WHICH IS INCLUDED IN THIS CALCULATION
        
        contractForm.setExtendedDays( months_days[1] -1 );
        logger.logInfo( methodName +"Finish|Extended Months:%s|Extended Days:%s", contractForm.getExtendedMonths(),
        		                                                                                contractForm.getExtendedDays() );
	}
	private void calculateSettlementForFirstTime() 
	{
        
		ContractForm contractForm = this.getContractBean();
		try 
		{
			RequestView requestView =  new RequestServiceAgent().getRequestById(getRequestId());
			
			LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );
			Date endDate = contractForm.getEvacDate();
			Date expDate = locale.getDateFormated( contractForm.getContractExpiryDate() );
			//String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration( WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR );
			Double amountGrandTotal = 0D;
			getActualRentPeriod( contractForm );
			Double contractDuration = new Double( getContractDurationDays( contractForm ) );
			contractForm.setActualRentalAmount( Math.ceil( ( contractForm.getTotalDays() / contractDuration  ) * contractForm.getRentalAmount() ) );
	        //if evac date is greater then expiry date then calculate rent from 
			if( endDate.compareTo( expDate ) > 0 )
			{
				calculateForExtendedPeriod( contractForm );
			}
			//For transfer Contract no evacuation fines will be calculated
			if(isParentTransferContract)
			{
				contractForm.setTotalEvacFines( new Double( 0 ) );
			}
			else
		    {
		    	if( requestView.getEvacuationFine() != null && requestView.getEvacuationFine().doubleValue() > 0d )
		    	{
				    contractForm.setTotalEvacFines( requestView.getEvacuationFine() );
		    	}
		    	else
		    	{
		    		contractForm.setTotalEvacFines( 0.0d );
		    	}
				if(requestView.getIsExempted()!=null && requestView.getIsExempted().compareTo(1L)==0)
				{
				   this.setExempted(1L);
				   contractForm.setTotalEvacFines(0.0D);
				}
				else
				{
				   this.setExempted(0L);
				}
			
			}
			
			this.populatePaymentsTab(contractForm.getContract().getContractId());
			
			if(this.getExempted().compareTo(0L)==0 )
			{
			  //Now calculating the Grand totals
			  amountGrandTotal =        (contractForm.getActualRentalAmount() - contractForm.getTotalPaidAmount()) +
									  (contractForm.getTotalEvacFines() + contractForm.getOtherUnpaidfines());
			}
			else if( this.getExempted().compareTo(1L)==0  )
			{
				//Now calculating the Grand totals
				amountGrandTotal =        (contractForm.getActualRentalAmount() - contractForm.getTotalPaidAmount()) +
										  (contractForm.getOtherUnpaidfines());
			}
			if (amountGrandTotal > 0) 
			{
				contractForm.setTotalRefundAmount(0.0);
				contractForm.setTotalDueAmount(amountGrandTotal);
			}
			else 
			{
				contractForm.setTotalRefundAmount(Math.ceil (  Math.abs(amountGrandTotal ) ));
				contractForm.setTotalDueAmount(0.0);
			}
			
			if( !isParentContractList )
			{
			 Map<String,Double> map = new HashMap<String, Double>();
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");				
			 getCreditMemo(map, contractForm, df);
			}
			contractForm.setSettlementCalculated(true);
		} 
		catch (Throwable t)
		{
			contractForm.setSettlementCalculated(false);
			logger.LogException("Exception when calculating the Settlement Values for contractId:%s", t, contractForm.getContract().getContractId());
		}
	}
	@SuppressWarnings("unchecked")
	private Double calculateFineForEvacDateBeforeExpiryDate(ContractForm contractForm, ApplicationBean appBean, String daysInYear) throws ParseException
	{
		Double fine = 0.0D;
		Double paymentConfigurationAmount= 0.0D;

		
			DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
			DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
			if (contractForm.getContract().getContractTypeId().equals(comercialType.getDomainDataId()) )
			{
				PaymentConfigurationView commercial = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_COMMERCIAL);
				if (commercial!=null )
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
					if(commercial.getIsFixed().compareTo(0L)==0) 
					  paymentConfigurationAmount = commercial.getAmount();
				    else
					  return commercial.getAmount();
				}
			}
			else if (contractForm.getContract().getContractTypeId().equals(residentalType.getDomainDataId()) )
			{
				PaymentConfigurationView residental = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_RESIDENTIAL);
				if(residental!=null)
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
					if(residental.getIsFixed().compareTo(0L)==0) 
					   paymentConfigurationAmount = residental.getAmount();
				    else
				       return residental.getAmount();
						
				}
			}
			fine = Math.ceil ((30.0/new Double(daysInYear)) * paymentConfigurationAmount * contractForm.getRentalAmount());
	    return fine;
	}
    @SuppressWarnings("unchecked")
	private Double calculateFineForRequestNotBeforeSpecifedMonths(ContractForm contractForm, ApplicationBean appBean, Date expDate,String daysInYear) throws ParseException,Exception 
	{
		int months;
		Integer[] months_days;
		Double totalEvacFines  = 0.0;
		Double totalFineMonths = 0.0D;
		
		RequestView requestView = viewContext.getAttribute("requestView");
		if(requestView != null && requestView.getRequestId()!= null )
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date formattedRequestDate = df.parse( df.format( requestView.getRequestDate() ) );
			Date formattedExpiryDate  = df.parse( df.format( expDate ) );
		     if( formattedRequestDate.compareTo( formattedExpiryDate ) <= 0 )
		     {
				months_days= new Integer[]{0,0};
				months_days = CommonUtil.calculateMonthsDaysBetween(formattedRequestDate, expDate );
				months = months_days[0];
			    if(months>=2)
			    	totalEvacFines=0.0;
			    else
			    {
					DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
					DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
					if (contractForm.getContract().getContractTypeId().equals(comercialType.getDomainDataId()) )
					{
						
						PaymentConfigurationView commercial = getPaymentConfig(Constant.ANNULMENT_FINE_COMMERCIAL);
						if (commercial!=null)
						{
						    viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
							totalFineMonths = commercial.getAmount();
						}
					}
					else if (contractForm.getContract().getContractTypeId().equals(residentalType.getDomainDataId()) )
					{
						PaymentConfigurationView residental = getPaymentConfig(Constant.ANNULMENT_FINE_RESIDENTIAL);
						if(residental!=null)
						{
					  	 viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
						 totalFineMonths = residental.getAmount();
						}
					}
					totalEvacFines = Math.ceil ((30.0/new Double(daysInYear)) * totalFineMonths * contractForm.getRentalAmount());
					

			    	
			    }
		     }
			
		}
		
		
		return totalEvacFines;
	}
    @SuppressWarnings( "unchecked" )
    private Integer getContractDurationDays( ContractForm contractForm  )throws Exception
    {
    	Integer[] months_days = getContractDuration( contractForm );
    	return ( months_days[0] * 30 ) + months_days[1];
    	
    }
    @SuppressWarnings( "unchecked" )
    private Integer[] getContractDuration ( ContractForm contractForm ) throws Exception
    {
    	if( viewMap.get( CONTRACT_DURATION ) == null )
    	{
    		LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );
    		viewMap.put( CONTRACT_DURATION , CommonUtil.calculateMonthsDaysBetween(locale.getDateFormated( contractForm.getContractStartDate() ),
    				                                                    locale.getDateFormated( contractForm.getContractExpiryDate() ) ) );
    	}   
    		return ( Integer[] )viewMap.get( CONTRACT_DURATION );
    	
    }
		@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId){
		
		if( !isReadOnly())
		{
		viewMap.put("canAddAttachment",true);
		viewMap.put("canAddNote", true);
		}		
		if(requestId!=null)
		{
		  viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, requestId.toString());
		  viewMap.put("entityId", requestId.toString());
		}
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, Constant.PROCEDURE_TYPE_EVACUATION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
	}
	private void saveAttachmentComments(Long requestId, String sysNote) throws Exception
	{
		saveAttachments(requestId);
		saveComments(requestId);
		saveSystemComments(requestId, sysNote);
	}
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null)
		    	success = CommonUtil.updateDocuments();
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveSystemComments(Long requestId, String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	
	    	if(requestId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(notesOwner, sysNote, requestId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
    public void tabAttachmentsComments_Click()
	{
		
		RequestView requestView = viewContext.getAttribute("requestView");
		 if(requestView!= null && requestView.getRequestId()!=null)
		 {
			 loadAttachmentsAndComments(requestView.getRequestId());
		 }
		
	}
	public void btnOpenRequest_Click()
	{
		String methodName = "btnOpenRequest_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	RequestView requestView = viewContext.getAttribute("requestView");
    	
		ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW, requestView);
    	AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
    	if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_TRANSFER_CONTRACT)==0)
    	   addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, "openTransferRequest();");
    	else if(requestView.getRequestTypeId().compareTo(WebConstants.REQUEST_TYPE_TERMINATE_CONTRACT)==0)
           addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, "openCancelRequest();");

    	logger.logInfo(methodName+"|"+" Finish...");
	}
	public void btnAddPayments_Click()
    {
    	String methodName = "btnAddPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PropertyServiceAgent psa =new PropertyServiceAgent();
    	List<PaymentScheduleView> paymentScheduleList = new ArrayList<PaymentScheduleView>();
    	String psStatusInReq="";
    	try
    	{
    		
    		
			String arg1 = WebConstants.PAYMENT_STATUS_IN_REQUEST;
			String arg2 = WebConstants.PAYMENT_TYPE_FEES_ID.toString() ;//+","+ WebConstants.PAYMENT_TYPE_FINE_ID.toString();
			String arg3 = viewMap.get(WebConstants.OWNWERSHIP_TYPE_ID).toString();
			psStatusInReq = WebConstants.PAYMENT_SCHEDULE_PENDING;
		    FacesContext facesContext = FacesContext.getCurrentInstance();
		    String javaScriptText ="var screen_width = 1024;"+
            "var screen_height = 450;"+
            "window.open('PaymentSchedule.jsf?"+WebConstants.PAYMENT_STATUS_IN_REQUEST+"="+psStatusInReq+"&"+
            PaymentSchedule.Keys.PAYMENT_TYPES+"="+arg2 +
            "&ownerShipTypeId="+arg3+
            "','_blank','width='+(screen_width-400)+',height='+(screen_height)+',left=200,top=250,scrollbars=yes,status=yes');";
				// Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);


	        logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
	
	private PaymentConfigurationView getPaymentConfig(String conditionKey){
		logger.logInfo("getPaymentConfig() started...");
		PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
		
		try
		{
			UtilityServiceAgent usa = new UtilityServiceAgent();
			//paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_FINE_ID);
			paymentConfigurationView.setConditionKey(conditionKey);
			paymentConfigurationView.setProcedureTypeKey(Constant.PROCEDURE_TYPE_EVACUATION);
			List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
			if(!paymentConfigList.isEmpty())
				paymentConfigurationView = paymentConfigList.get(0);
			else
				paymentConfigurationView = null;
				
			logger.logInfo("getPaymentConfig() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getPaymentConfig() crashed ", exception);
		}
		
		return paymentConfigurationView; 
	}

	private boolean validateInput() {
		ContractForm contractForm = this.getContractBean();
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		if (contractForm.getEvacDate() != null )
			return true;
		else
			errorMessages.add(new String(ResourceUtil.getInstance().getProperty("settlement.message.evacDate")));
			
		return false;
	}
	
	public String onPrintSettlement(){
		successMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.SETTLEMENT_PRINTED));
		return "";
	}
	
	
	
	private boolean hasSaveSettlementErrors()throws Exception
	{
		String methodName ="hasSaveSettlementErrors|";
		logger.logInfo(methodName +"Start");
		boolean hasSaveSettlementErrors =false;
		ContractForm contractForm = this.getContractBean();
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		//List<PaymentBean> otherPaymentsList = (ArrayList<PaymentBean>)this.getOtherPayments();
	//	DomainDataView ddvPending =CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_PENDING);
		if(contractForm.getSettlementCalculated()==null||!contractForm.getSettlementCalculated())
		{
			this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.settlementmsg"));
			hasSaveSettlementErrors = true; 
		}
		if(contractForm.getEvacDate()==null|| 
				df.parse(df.format(dateNow)).compareTo(df.parse(df.format(contractForm.getEvacDate())))<0)
		{
			this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.todayDateGreaterOrEqualEvacuationDate"));
			hasSaveSettlementErrors = true; 
		}
		if(getPaymentsChequesToBeReturned().size()>0)
		{
			for(PaymentBean pmtBean : getPaymentsChequesToBeReturned())
			{
				if( pmtBean.getHasWithDrawPrinted() == null || pmtBean.getHasWithDrawPrinted().trim().length() <= 0 || 
					pmtBean.getHasWithDrawPrinted().equals("0")
				  )
					{
					this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.withdrawlLetterNotPrinted"));
					hasSaveSettlementErrors = true; 
					break;
					}
			}
		}
//		Map<String,Double> map  = new HashMap<String, Double>();
//		try
//		{
//		   getCreditMemo( map, contractForm, df);
//		   if( contractForm.getCreditMemo() != null && contractForm.getGrpUnAppliedAmount() != null &&
//			   contractForm.getCreditMemo() > contractForm.getGrpUnAppliedAmount())
//		   {
//			   hasSaveSettlementErrors = true;
//			   this.errorMessages.add(  java.text.MessageFormat.format
//					                     (
//					                      ResourceUtil.getInstance().getProperty("settlement.msg.CreditMemoGreaterThanUnAppliedAmount"),
//					                      contractForm.getCreditMemo(),
//					                      contractForm.getGrpInvoiceAmount(),
//					                      contractForm.getGrpUnAppliedAmount()
//					                     ) 
//					                  );
//		   }
//		}
//		catch(Exception e)
//		{
//			logger.LogException(methodName +"Error Occured:",e);
//			hasSaveSettlementErrors = true;
//		}
		logger.logInfo(methodName +"Finish");
		return hasSaveSettlementErrors;
		
	}

	private void getCreditMemo( Map<String,Double> map ,ContractForm contractForm,DateFormat df) throws ParseException, Exception 
	{
		String methodName = "getCreditMemo";
		
		if( df.parse( contractForm.getContractExpiryDate() ).compareTo( df.parse( df.format( contractForm.getEvacDate() ) ) ) >0 )
		{
		   EvacuationServiceAgent esa = new EvacuationServiceAgent();
		   Long ownershipTypeId = new Long( viewMap.get(WebConstants.OWNWERSHIP_TYPE_ID).toString() );
		   logger.logInfo( methodName +"ownerShiptTypeId:%s", ownershipTypeId );
		   DomainDataView ddMinor = getDDOwnerShipTypeMinor();
		   //Since for minor owner ship type no credit memo is posted to grp therefore no credit memo validation 
		   //is required
		   if( ddMinor.getDomainDataId().longValue() != ownershipTypeId )
		   {
			   RequestView requestView = viewContext.getAttribute("requestView");
			   logger.logInfo( methodName +"Contract Unit does not belong to Minor:%s");
			   try
			   {
				
				   map =  esa.getAmountsForCreditMemoForContractBeforeExpiryValidation( contractForm, requestView.getRequestId(),
						                                                                                   ownershipTypeId, contractForm.getEvacDate() );
				   contractForm.setCreditMemo( map.get("absCreditMemo").doubleValue() );
				   contractForm.setGrpInvoiceAmount( map.get("invoiceAmount").doubleValue() );
				   contractForm.setGrpAppliedAmount( map.get("amountApplied").doubleValue() );
				   contractForm.setGrpUnAppliedAmount( contractForm.getGrpInvoiceAmount()- contractForm.getGrpAppliedAmount());
				   
			   }
			   catch (PimsBusinessException e)
			   {
				   logger.LogException("init|Exception Occured::", e);
				   if(e.getExceptionCode()!=null &&  e.getExceptionCode().compareTo(ExceptionCodes.GRP_AR_IVOICE_NOT_PRESENT)==0)
				   {
					   this.errorMessages.add(java.text.MessageFormat.format(
							     ResourceUtil.getInstance().getProperty("settlement.msg.ArInvoiceNotPresent"),e.getExceptionMessage()));
				   }
				   else
				   {
					   
						errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				   }
				   throw e;
					   
			   }
		   }
			else
			{
				 map.put("absCreditMemo", 0d );
				 map.put("invoiceAmount", 0d );
				 map.put("amountApplied", 0d );
				
			}
		}
	}
	@SuppressWarnings("unchecked")
	public String onSave()
	{

		logger.logDebug("SettlementBean.onSave() - called"); 
		ContractForm contractForm = this.getContractBean();
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		RequestView requestView = viewContext.getAttribute("requestView");
		Long requestNo = 0L;
		ApplicationContext applicationContext = ApplicationContext.getContext();
		try 
		{
			if ( !hasSaveSettlementErrors() ) 
			{
				    applicationContext.getTxnContext().beginTransaction();
				    this.calculateSettlement(false, null);
				    saveSettlementQuery();
					HashMap argMap = new HashMap();
					argMap.put("paymentScheduleDescription", CommonUtil.getBundleMessage(WebConstants.CancelContract.PAYMENT_SCHEDULE_DESC));
					if(contractForm.getTotalDueAmount().compareTo(0D)>0 && (contractForm.getActualRentalAmount() - contractForm.getTotalPaidAmount())>0)
					  argMap.put("totalDueRent", (contractForm.getActualRentalAmount() - contractForm.getTotalPaidAmount()));
					if(contractForm.getTotalEvacFines()!=null && contractForm.getTotalEvacFines().compareTo(0D)>0 )
					argMap.put("totalEvacuationFines", contractForm.getTotalEvacFines());
					if(contractForm.getTotalEvacFines().longValue()>0l && viewMap.containsKey(EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION))
						argMap.put("evacfinepaymenttype", viewMap.get(EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION));
					Long ownershipTypeId = new Long(viewMap.get(WebConstants.OWNWERSHIP_TYPE_ID).toString());
					evacuationService.addFinalPayment(
														contractForm.getEvacDate(), 
														contractForm.getContract().getContractId(), 
														contractForm.getTotalDueAmount(), 
														contractForm.getTotalRefundAmount(), 
														user.getLoginId(), 
														(requestView == null)? null : requestView.getRequestId(), 
														argMap,
														ownershipTypeId,
														contractForm
													  );
					 new RequestServiceAgent().updateRequestEvacuation(
							 											contractForm.getTotalEvacFines(), 
							 											this.getExempted(), 
							 											CommonUtil.getLoggedInUser(), getRequestId()
							 										   );
					if (requestView != null && !isParentTransferContract) 
					{
						generateNotification(user);
						Boolean success = completeTask(user.getLoginId());
						if(success)
						{
							Long requestId = requestView.getRequestId();
							try 
							{
								if(requestId!=null)
								{
									saveAttachmentComments(requestId, MessageConstants.RequestEvents.REQUEST_SETTLEMENT_DONE);
									generateNotification(EVENT_SETTLEMENT);
								}
							}
							catch (Exception e) 
							{
								logger.LogException("Exception in saving system comments...", e);
							}
						}
						else
						{
							errorMessages.clear();
							errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
							return "";
						}
					}
					if(isParentTransferContract)
					{
						UserTask userTask = (UserTask) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
						if (userTask != null)
						{
							String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
							BPMWorklistClient client = new BPMWorklistClient(contextPath);
							client.addTaskComment(userTask, CommonUtil.getLoggedInUser(), "SETTLED");
							userTask.getTaskComments().add("SETTLED");
							saveAttachmentComments( requestView.getRequestId(), MessageConstants.RequestEvents.REQUEST_SETTLEMENT_DONE);
							webContext.setAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
							HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
							response.sendRedirect("transferContract.jsf");
						}
					}
				applicationContext.getTxnContext().commit();
				this.populatePaymentsTab(contractForm.getContract().getContractId());
				viewContext.setAttribute("pageMode", MODE_READONLY);
				this.pageMode = viewContext.getAttribute("pageMode");
				this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestforward"));
				sessionMap.put("SETTLE_MESSAGE", "cancelContract.message.requestforward");
			  }
			} catch (Throwable t) 
			{
				applicationContext.getTxnContext().rollback();
				logger.LogException("Error occoured while Saving the Settlement Request.", t);
				throw new CoreException("Error occoured while Saving the Settlement Request.", t);
			} finally 
			{
				applicationContext.getTxnContext().release();
			}
		 
		return "";
	}
	
	public boolean isFromCancelContract()
	{
		if(viewMap.containsKey("FROM_CANCEL_CONTRACT_BEAN"))
			return true;
		else
			return false;
	}
	
	public String onRejectRequest()
	{
	try{	
		if(isCancelContractSettlementTask())
			rejectCancelSettlementTask();
		else if(isTransferSettlementTask())
			rejectTransferSettlementTask();
	 }catch (Exception exception) 
	  {
		logger.LogException("onRejectRequest() crashed ", exception);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }
		
		return "";
	}
	
	private void rejectTransferSettlementTask() throws Exception{
		Long requestNo = 0L;
		
		if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		RequestView requestView = viewContext.getAttribute("requestView");
		new RequestService().setRequestStatus(requestView.getRequestId(), WebConstants.REQUEST_STATUS_SUGGESTED_RENT_APPROVED, CommonUtil.getLoggedInUser());
		if (requestView != null)
			requestNo = requestView.getRequestId();

		if (completeTask(CommonUtil.getLoggedInUser(), TaskOutcome.REJECT))
			if (requestNo != null)
				saveSystemComments(requestNo,
						MessageConstants.RequestEvents.REQUEST_REJECTED);

		if (requestNo != null && requestNo > 0) {
			viewContext.setAttribute("pageMode", MODE_READONLY);
			this.pageMode = viewContext.getAttribute("pageMode");
			this.successMessages.add(CommonUtil.getBundleMessage("successMsg.sentForVerification"));
			btnRejectBtn.setRendered(false);
		}
	}

	private void rejectCancelSettlementTask()throws Exception {

		Long requestNo = 0L;

		if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		RequestView requestView = viewContext.getAttribute("requestView");
		requestNo = updateEvacReq(CommonUtil.getLoggedInUser(),
				Constant.REQUEST_STATUS_APPROVAL_REQUIRED, requestView
						.getRequestId());
		if (requestView != null)
			requestNo = requestView.getRequestId();

		if (completeTask(CommonUtil.getLoggedInUser(), TaskOutcome.REJECT))
			if (requestNo != null)
				saveSystemComments(requestNo,
						MessageConstants.RequestEvents.REQUEST_REJECTED);

		if (requestNo != null && requestNo > 0) {
			viewContext.setAttribute("pageMode", MODE_READONLY);
			this.pageMode = viewContext.getAttribute("pageMode");
			this.successMessages.add(ResourceUtil.getInstance().getProperty(
					"cancelContract.message.requestRequest"));
		}
	}

	public void onEvacFinesExempted(ActionEvent event)
	{
		if ( this.validateInput() ) 
		{
			this.setExempted(1L);
			this.calculateSettlement(true, this.getContractBean().getTotalEvacFines());	
		}
      
	}
	public void onEvacFinesApplied(ActionEvent event)
	{
		if ( this.validateInput() ) 
		{
			this.setExempted(0L);
			this.calculateSettlement(true, this.getContractBean().getTotalEvacFines());	
        }
	}
	
	public void evacDateChanged(ValueChangeEvent event){
		viewMap.put("dateChanged", true);
		ContractForm contractForm = this.getContractBean();
		contractForm.setSettlementCalculated(false);
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	private boolean completeTask(String loginId){
		boolean success = false;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			UserTask userTask = (UserTask) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			bpmWorkListClient.completeTask(userTask, loginId, TaskOutcome.OK);
			success = true;
		} catch (Throwable t) {
			logger.LogException("Exception while completing task from BPEL.", t);
		}
		return success;
	}
	
	private boolean completeTask(String loginId, TaskOutcome taskOutcome ){
		boolean success = false;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			UserTask userTask = (UserTask) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			bpmWorkListClient.completeTask(userTask, loginId, taskOutcome);
			success = true;
		} catch (Throwable t) {
			logger.LogException("Exception while completing task from BPEL.", t);
		}
		return success;
	}

	private void generateNotification(User user) {
		
		NotificationFactory nsfactory = NotificationFactory.getInstance();
		NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<ContactInfo> contactInfo = new ArrayList<ContactInfo>();
		
		this.contractView = this.getContractBean().getContract();
		
		PersonView tenantView =  this.contractView.getTenantView();
//		contactInfo.add(new ContactInfo(user.getLoginId(), tenantView.getCellNumber(), null, tenantView.getContactInfoView().getEmail()));
		
		dataMap.put("TENANT_NAME", tenantView.getPersonFullName());
		dataMap.put("CONTRACT_NO", this.contractView.getContractNumber());
		dataMap.put("CONTRACT", this.contractView);
		
		// TODO correct from request details backing bean
//		notifier.fireEvent(EVENT_SETTLEMENT, dataMap, contactInfo);
	}
	
    private List<ContactInfo> getEmailContactInfos(PersonView personView)
    {
    	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
    	Iterator iter = personView.getContactInfoViewSet().iterator();
    	HashMap previousEmailAddressMap = new HashMap();
    	while(iter.hasNext())
    	{
    		ContactInfoView ciView = (ContactInfoView)iter.next();
    		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
            {
    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
    			emailList.add(new ContactInfo(CommonUtil.getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
    			//emailList.add(ciView);
            }
    	}
    	return emailList;
    }

    
	private Boolean generateNotification(String eventName)
    {
		String methodName ="generateNotification";
    	Boolean success = false;
		try
		{
			logger.logInfo(methodName+"|Start");
			contractView = viewContext.getAttribute("contractView");
			List<ContactInfo> tenantsEmailList    = new ArrayList<ContactInfo>(0);
			List<ContactInfo> applicantsEmailList = new ArrayList<ContactInfo>(0);
			NotificationFactory nsfactory = NotificationFactory.getInstance();
			NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
			Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();	
	    	getNotificationPlaceHolder(event);			    	
			if(contractView.getTenantView()!=null){
	    	{
				tenantsEmailList = getEmailContactInfos(contractView.getTenantView());
				if(tenantsEmailList.size()>0)
					notifier.fireEvent(event, tenantsEmailList);
	    	}	
				
				String hdnApplicantId = viewContext.getAttribute("applicantId");
				String hdnTenantId = "";
				if(contractView.getTenantView().getPersonId()!=null)
					hdnTenantId = (String)contractView.getTenantView().getPersonId().toString();
				
				if(StringHelper.isNotEmpty(hdnTenantId) && StringHelper.isNotEmpty(hdnApplicantId) && !hdnTenantId.equals(hdnApplicantId))
		    	{
				    applicantsEmailList = getEmailContactInfos((PersonView)viewContext.getAttribute(WebConstants.ApplicationDetails.APPLICANT_VIEW));
				    if(applicantsEmailList.size()>0)
				    	notifier.fireEvent(event, applicantsEmailList);
		    	}
			}
			success  = true;
			logger.logInfo(methodName+"|Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Finish", ex);
		}
		return success;
    }
	
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		placeHolderMap.setValue("CONTRACT", contractView);
		placeHolderMap.setValue("TENANT_NAME",contractView.getTenantView().getPersonFullName());
		placeHolderMap.setValue("CONTRACT_NO",contractView.getContractNumber());
		logger.logDebug(methodName+"|Finish");
	}

	private Long updateEvacReq(String loginId, String reqStatus, Long requestId) {
		Long requestNo = 0L;
		try {
			if (this.getContractBean() != null) {
				requestNo = evacuationService.updateEvacuationRequest(loginId, reqStatus, null, requestId);
				this.successMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.success"));
			} else {
				logger.logError("Error occoured the ContractView is null while adding Evacuation Request.");
			}
		} catch (PimsBusinessException e) {
			if (this.getContractBean() != null && this.getContractBean().getContract() != null) {
				logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, this.getContractBean().getContract().getContractId(), reqStatus);
				throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, this.getContractBean().getContract().getContractId(), reqStatus);
			}
			logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
			throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
		}
		return requestNo;
	}

	public String onPrintReceipt(){
		successMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.RECEIPT_PRINTED));
		return "";
	}

	public String onCollectPayment(){
		return openReceivePaymentsPopUp();
	}
	
	public String onCancelPayment(){
		return "";
	}
	
	public String openReceivePaymentsPopUp()
	{
    	logger.logInfo("openReceivePaymentsPopUp started...");
    	
		successMessages.clear();
		errorMessages.clear();

		try{
			List<PaymentBean> paymentBeans = viewContext.getAttribute(BEAN_PAYMENTS_OTHER);
			
	        List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
	        //DomainDataView pendingStatus = appBean.getDomainData(Constant.PAYMENT_SCHEDULE_STATUS, Constant.PAYMENT_SCHEDULE_PENDING);
			for (int i=0; i<paymentBeans.size(); i++)
			{
				if (paymentBeans.get(i).getSelected()!=null&&paymentBeans.get(i).getSelected())
					paymentsForCollection.add(paymentBeans.get(i).getPaymentScheduleView());
			}
			sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
			//			if(paymentBeans!=null)
            //				createReceiptPayment(paymentBeans);
	        if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION)!=null)
	    	{
		        //List<PaymentScheduleView> paymentsForCollection = (List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
				//sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentsForCollection);
				
				PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
		        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	        
		        FacesContext facesContext = FacesContext.getCurrentInstance();
		        String extrajavaScriptText ="var screen_width = 1024;"+
                                            "var screen_height = 450;"+
                                            "window.open('receivePayment.jsf','_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=0,top=10,scrollbars=no,status=no');";

		
				// Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, extrajavaScriptText);
	    	}
	        logger.logInfo("openReceivePaymentsPopUp completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("openReceivePaymentsPopUp crashed...", ex);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		return "";
	}

	@Override
	public void preprocess(){
		super.preprocess();
		
    	initContext();
	}
	

	public String getErrorMessages() {
		
		return CommonUtil.getErrorMessages(errorMessages);
	}
	

	public String getSuccessMessages() {
		StringBuilder sb = new StringBuilder();
		for (String msg: this.successMessages)
			sb.append(msg).append("\n");
		
		return sb.toString();
	}
	
	
	public ContractForm getContractBean() {
		return viewContext.getAttribute(BEAN_CONTRACT);
	}
	
	
	public List<PaymentBean> getPayments(){
		return viewContext.getAttribute(BEAN_PAYMENTS);
	}
	public List<PaymentBean> getPaymentsChequesToBeReturned(){
		List<PaymentBean> payments = (ArrayList<PaymentBean>) viewContext.getAttribute(BEAN_PAYMENTS_RETURN_CHEQUES);
		
		
		
		return payments ;
	}
	public List<PaymentBean> getOtherPayments(){
		List<PaymentBean> payments = (ArrayList<PaymentBean>) viewContext.getAttribute(BEAN_PAYMENTS_OTHER);
		
		if(payments!=null )
		viewMap.put("recordSizeOtherPayments",payments.size());
		
		
		return payments ;
	}
	
	public Long getRequestId(){
		if (viewContext!= null && viewContext.getAttribute("requestView") != null)
			return ((RequestView) viewContext.getAttribute("requestView")).getRequestId();
		else
			return 0L;
	}

	public HtmlDataTable getPaymentTable() {
		return paymentTable;
	}

	public void setPaymentTable(HtmlDataTable paymentTable) {
		this.paymentTable = paymentTable;
	}
	
	@Override
	public void prerender(){
		super.prerender();
		this.pageMode = viewContext.getAttribute("pageMode");
	}

	public boolean isParentTransferContract() {
		return isParentTransferContract;
	}

	public void setParentTransferContract(boolean isParentTransferContract) {
		this.isParentTransferContract = isParentTransferContract;
	}
	
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.SETTLEMENT_RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	private Long getOwnerShipTypeId(ContractView cvo){
		 Iterator iter=cvo.getContractUnitView().iterator();
  	  while(iter.hasNext())
  	  {
  		  
  		  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
  		  return contractUnitView.getUnitView().getPropertyCategoryId();
  	  }
  	  return null;
	}

	public boolean isParentContractList() {
		
		if(viewMap.containsKey("isParentContractList"))
			isParentContractList = (Boolean)viewMap.get("isParentContractList");
		return isParentContractList;
	}

	public void setParentContractList(boolean isParentContractList) {
		this.isParentContractList = isParentContractList;
		viewMap.put("isParentContractList", this.isParentContractList );
	}

	public HtmlDataTable getPaymentChequestToReturnTable() {
		return paymentChequestToReturnTable;
	}

	public void setPaymentChequestToReturnTable(
			HtmlDataTable paymentChequestToReturnTable) {
		this.paymentChequestToReturnTable = paymentChequestToReturnTable;
	}

	public Integer getRecordSizeReturnCheque() {
		recordSizeReturnCheque = (Integer)viewMap.get("recordSizeReturnCheque");
		return recordSizeReturnCheque;
	}

	public HtmlDataTable getPaymentTableOther() {
		return paymentTableOther;
	}

	public void setPaymentTableOther(HtmlDataTable paymentTableOther) {
		this.paymentTableOther = paymentTableOther;
	}

	public Integer getRecordSizeOtherPayments() {
		recordSizeOtherPayments = (Integer)viewMap.get("recordSizeOtherPayments");
		
		return recordSizeOtherPayments;
	}
	
	public void printReport()
	{
		String methodName = "printReport|";
		logger.logDebug( methodName + "Start");
		putScreenValuesInCriteriaObject();
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		logger.logDebug( methodName + "Finish");

		
	}

	private void putScreenValuesInCriteriaObject() 
	{
		String methodName = "putScreenValuesInCriteriaObject|";
		logger.logDebug( methodName + "Start" );
		this.contractView = this.getContractBean().getContract();
		RequestView req = viewContext.getAttribute( "requestView" );
		if( req!= null && req.getRequestId() != null )
			reportCriteria.setRequestId( req.getRequestId() );
		else
			reportCriteria.setRequestId( null );
		reportCriteria.setContractNumber( this.getContractBean().getContractNo() );
		reportCriteria.setTenantName( this.getContractBean().getTenantName() );
		reportCriteria.setGrpCustomerNumber( this.getContractBean().getGrpCustomerNumber() );
		reportCriteria.setCostCenter( this.getContractBean().getGrpUnitCostCenter() );
		reportCriteria.setContractStartDate( contractView.getStartDateString() );
		reportCriteria.setContractEndDate( contractView.getEndDateString() );
		reportCriteria.setEvacuationDateString( getStringFromDate( this.getContractBean().getEvacDate() ) );
		reportCriteria.setActualRentalAmount( this.getContractBean().getActualRentalAmount() );
		reportCriteria.setDays( this.getContractBean().getDays() );
		reportCriteria.setMonths( this.getContractBean().getMonths() );
		reportCriteria.setOtherUnpaidfines( this.getContractBean().getOtherUnpaidfines() );
		reportCriteria.setTotalDepositAmount( this.getContractBean().getTotalDepositAmount() );
		reportCriteria.setTotalDueAmount( this.getContractBean().getTotalDueAmount() );
		reportCriteria.setTotalEvacFines( this.getContractBean().getTotalEvacFines() );
		reportCriteria.setTotalPaidAmount( this.getContractBean().getTotalPaidAmount() );
		reportCriteria.setTotalRealizedAmount( this.getContractBean().getTotalRealizedAmount() );
		reportCriteria.setTotalRefundAmount( this.getContractBean().getTotalRefundAmount() );
		reportCriteria.setTotalUnpaidAmount( this.getContractBean().getTotalUnpaidAmount() );
		reportCriteria.setTotalUnrealizedAmount( this.getContractBean().getTotalUnrealizedAmount() );
		reportCriteria.setContractRentalAmount( this.getContractBean().getRentalAmount() );
		reportCriteria.setCreditMemo( this.getContractBean().getCreditMemo() != null ?this.getContractBean().getCreditMemo() :0 );
		List<PaymentBean> tempPayments;
		tempPayments = this.getPayments();
		for( PaymentBean payment : tempPayments )
			payment.setCategory( ResourceUtil.getInstance().getProperty( MessageConstants.RENT_PAYMENTS ) );
		reportCriteria.getAllPayments().addAll( tempPayments );
		tempPayments = this.getOtherPayments();
		for( PaymentBean payment : tempPayments )
			payment.setCategory( ResourceUtil.getInstance().getProperty( MessageConstants.OTHER_PAYMENTS ) );
		reportCriteria.getAllPayments().addAll( tempPayments );
		tempPayments = this.getPaymentsChequesToBeReturned();
		for( PaymentBean payment : tempPayments )
		{
			//payment.setCategory("Cheques to be Returned");
			if( payment.getPaymentType()  == null )
				payment.setPaymentType( "" );
			payment.setCategory( ResourceUtil.getInstance().getProperty( MessageConstants.RETURNED_PAYMENTS ) );
		}
		reportCriteria.getAllPayments().addAll( tempPayments );
		logger.logDebug( methodName + "Finish" );
	}
	private void saveSettlementQuery() throws Exception
	{
		SettlementReportProcessor proc = new SettlementReportProcessor();
		putScreenValuesInCriteriaObject();
		proc.createAndSaveSettlementQuery( reportCriteria );
	}
	private void openPopup(String javaScriptText) 
	{
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	public Long getExempted() 
	{
		Long temp = 0L;
		if( viewMap.get("isExempted")!=null )
		temp = (Long)viewMap.get("isExempted");
		
		return temp;
	}

	public void setExempted(Long isExempted) 
	{
		if(isExempted !=null)
			viewMap.put("isExempted" , isExempted);
	}

	public HtmlCalendar getEvacuationClnder() {
		return evacuationClnder;
	}

	public void setEvacuationClnder(HtmlCalendar evacuationClnder) {
		this.evacuationClnder = evacuationClnder;
	}
	private UserDbImpl getLoggedInUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		UserDbImpl user=new UserDbImpl();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) 
			 user = (UserDbImpl) session	.getAttribute(WebConstants.USER_IN_SESSION);

		return user;
	}
	public void printWithdrawlLetter()
	{
	try{
		List<PaymentScheduleView> selectedPmtSch= new ArrayList<PaymentScheduleView>();
		HashMap financialAccConfMap= new HashMap<Long, String>();
		String chequeString ="";
		List<PaymentBean> newPbList = new ArrayList<PaymentBean>();
		if(getPaymentsChequesToBeReturned().size()>0)
		{
			for(PaymentBean pmtBean : getPaymentsChequesToBeReturned())
			{
				if( pmtBean.getSelected() !=null && pmtBean.getSelected())
					{
						chequeString += pmtBean.getChequeNumber()+",";
						selectedPmtSch.add(pmtBean.getPaymentScheduleView());
						pmtBean.setHasWithDrawPrinted("1");
						pmtBean.setWithDrawPrintedBy(CommonUtil.getLoggedInUser());
						newPbList.add( pmtBean );
					}
				
			}
		}
		errorMessages= new ArrayList<String>();
		if(selectedPmtSch !=null && selectedPmtSch.size()>0)
		{
			String queryInCondition=" IN (";
			for(PaymentScheduleView pSch : selectedPmtSch)
			{
				if(pSch.getPaymentScheduleId() != null)
					queryInCondition = queryInCondition.concat(pSch.getPaymentScheduleId()+","); 
			}

			queryInCondition=queryInCondition.substring(0,queryInCondition.length()-1);
			queryInCondition += ")";
			withDrawlCriteria.setInCriteria(queryInCondition);
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, withDrawlCriteria);
			
			RequestView requestView = new RequestView();
			
			if(viewContext.getAttribute("requestView") != null)
				requestView = viewContext.getAttribute("requestView"); 
				
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			Locale localeAr = new Locale( "ar" );
		    Locale localeEn = new Locale( "en" );
			chequeString = chequeString.substring(0, chequeString.length()-1);
			ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
			ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
			String sysMsgEn = java.text.MessageFormat.format( bundleEn.getString( MessageConstants.WITH_DRAWL_LETTER_PRINTED_FOR_CHEQUES),chequeString);
			String sysMsgAr = java.text.MessageFormat.format( bundleAr.getString( MessageConstants.WITH_DRAWL_LETTER_PRINTED_FOR_CHEQUES),chequeString);
			NotesController.saveSystemNotesForRequest(WebConstants.REQUEST,sysMsgEn,sysMsgAr, requestView.getRequestId());
			new UtilityService().updatePaymentWithDrawlFlags(selectedPmtSch, CommonUtil.getLoggedInUser());
			//viewContext.setAttribute(BEAN_PAYMENTS_RETURN_CHEQUES, newPbList);
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage("report.chequeWithDrawlLetter.errorMsg"));
		}
		
		logger.logDebug("printWithdrawlLetter" + "Finish");		
		
	}
	catch (Exception e) 
	{
		logger.LogException("printWithdrawlLetter() crashed |" ,e);
		errorMessages = new ArrayList<String>(0);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	}


  public boolean getShowCreditMemo()
  {
	  ContractForm form = this.getContractBean();
	  DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	  errorMessages = new ArrayList<String>();
	  try
	  {
		  if( !isParentContractList && form != null &&
			  df.parse( form.getContractExpiryDate() ).compareTo( df.parse( df.format( form.getEvacDate() ) ) ) > 0 &&
			  !isMinorContract()
			  
		     )	
		    
		  {
			  
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	  }
	  catch( Exception e )
	  {
		 logger.LogException("getShowCreditMemo|Exception Occured::", e );
		 errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 return false;
	  }
  }
  private boolean isMinorContract()
  {
	  DomainDataView ddMinor = getDDOwnerShipTypeMinor();
	if( ddMinor.getDomainDataId().longValue() == new Long ( viewMap.get( WebConstants.OWNWERSHIP_TYPE_ID ).toString() ) )
	{
		return true;
	}
	return false;
  }
  private DomainDataView getDDOwnerShipTypeMinor()
  {
	  if( viewMap.get( "ddOwnerShipTypeMinor" ) == null )
	  {
	   DomainDataView ddMinor = CommonUtil.getIdFromType(
              CommonUtil.getDomainDataListForDomainType( WebConstants.OWNER_TYPE ),
              WebConstants.OwnerShipType.MINOR 
              );
	   viewMap.put( "ddOwnerShipTypeMinor", ddMinor );
	   return ddMinor;
	  }
	  else
	  {
		  return ( DomainDataView )viewMap.get( "ddOwnerShipTypeMinor" );
	  }
  }


public void setSettlementTask(boolean isSettlementTask) {
	this.isTransferSettlementTask = isSettlementTask;
}

public boolean isTransferSettlementTask() {
	if(viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST) != null){
		UserTask task = (UserTask) viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
		if(task.getTaskType().compareTo(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_SETTLE_TRANSFER_CONTRACT) == 0){
			return true;
		}
	}
	return false;
}

public void setTransferSettlementTask(boolean isTransferSettlementTask) {
	this.isTransferSettlementTask = isTransferSettlementTask;
}

public boolean isCancelContractSettlementTask() {
	if(viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST) != null){
		UserTask task = (UserTask) viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
		if(task.getTaskType().compareTo(WebConstants.CancelContract_TaskTypes.TerminateContractSettlement) == 0){
			return true;
		}
	}
	return false;
}

public void setCancelContractSettlementTask(
		boolean isCancelContractSettlementTask) {
	this.isCancelContractSettlementTask = isCancelContractSettlementTask;
}

public HtmlCommandButton getBtnRejectBtn() {
	return btnRejectBtn;
}

public void setBtnRejectBtn(HtmlCommandButton btnRejectBtn) {
	this.btnRejectBtn = btnRejectBtn;
}
}


