package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.ComplaintService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ComplaintDetailsView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class ComplaintsBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(ComplaintsBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "APPROVAL_REQUIRED";
	private static final String PAGE_MODE_FM_TASK = "FM_TASK";
	private static final String PAGE_MODE_PM_TASK = "PM_TASK";
	private static final String PAGE_MODE_SITE_VISIT = "SITE_VISIT";
	private static final String PAGE_MODE_REVIEW_REQUIRED= "REVIEW_REQUIRED";
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DETAILS   = "detailsTab";
	private static final String TAB_REVIEW  = "reviewTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPropertyId;
    private String hdnContractId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String selectOneCollectedByUser;
    private String txtRemarks;
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    private ComplaintService complaintService = new ComplaintService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private RequestView requestView ;
	HttpServletRequest request;
	public String getHdnContractId() {
		return hdnContractId;
	}

	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}

	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public ComplaintsBean(){
		
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_TYPE_COMPLAINTS);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_COMPLAINTS);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if(request.getParameter("requestId")!=null)
		{
				long requestId = Long.valueOf( request.getParameter("requestId").toString() );
				requestView = new RequestView();
				requestView.setRequestId(requestId);
				getDataFromRequestSearch();
		} 
		else if(request.getParameter("complaintId")!= null)
		{
			long complaintId = Long.valueOf( request.getParameter("complaintId").toString() );
			getRequestDetails(null, complaintId);
		} 
		
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
			requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		    getDataFromRequestSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender(){
	
//		if ( getPageMode().equals(PAGE_MODE_APPROVAL_REQUIRED) && 
//				 viewMap.get("collectionCompleted" ) != null 
//			   )
//			{
//				setPageMode(PAGE_MODE_VIEW);
//			}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.REQUEST_TYPE_COMPLAINT_ID );

		ComplaintDetailsView complaintDetailsView = new ComplaintDetailsView();
		complaintDetailsView.setCreatedOn(new Date());
		complaintDetailsView.setCreatedBy(  getLoggedInUserId()  );
		complaintDetailsView.setUpdatedBy(  getLoggedInUserId()  );
		complaintDetailsView.setUpdatedOn(new Date());
		complaintDetailsView.setStatus( WebConstants.ComplaintStatus.NEW_ID );
		complaintDetailsView.setIsDeleted("0");
		complaintDetailsView.setType("1");
		requestView.setComplaintDetailsView(complaintDetailsView);

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if(request.getParameter("pageMode")!=null)
		{
			setPageMode( request.getParameter("pageMode").toString() );
			return;
		}
		
		
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		
		if( requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) == 0|| 
			requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_COMPLETED_BY_FM ) == 0 || 
			requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_COMPLETED_BY_PM ) == 0 ||
			(
				requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 &&
				(
				  
						requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID ) == 0 ||
						requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_COMPLETED_BY_FM_ID ) == 0 || 
						requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_COMPLETED_BY_PM_ID ) == 0 
				)
			)
		  )
		{
			setPageMode( PAGE_MODE_APPROVAL_REQUIRED);
			tabPanel.setSelectedTab(TAB_DETAILS );
		}
		else if(requestView.getStatusDataValue().compareTo(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED ) == 0 )
		{
			setPageMode( PAGE_MODE_REVIEW_REQUIRED );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab( TAB_REVIEW );
		}
		else if(requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_SITE_VISIT_REQUIRED ) == 0 )
		{
			setPageMode(PAGE_MODE_SITE_VISIT );
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if(
				requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_WITH_FM) == 0 ||
				requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_VISIT_DONE) == 0 ||
				(
					requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 && 	
					requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_WITH_FM_ID) == 0 		
				)
		       )
		{
			setPageMode(PAGE_MODE_FM_TASK );
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		
		else if(
					requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_WITH_PM) == 0 || 
					(
						requestView.getStatusDataValue() .compareTo(WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 && 	
						requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_WITH_PM_ID) == 0 		
					)
				)
		{
			setPageMode(PAGE_MODE_PM_TASK );
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if (getStatusCompleted())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		
		
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromRequestSearch()throws Exception
	{
		
		if( this.requestView !=null && requestView.getRequestId() != null)
		{
		  getRequestDetails( requestView.getRequestId(),null );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id ,null);
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( Long requestId,Long complaintId ) throws Exception 
	{
		requestView = complaintService.getComplaintRequestView( complaintId ,requestId );
		
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( requestId );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromPropertySearch()
    {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
		   updateValuesFromMap();
	       if(hdnPropertyId != null)
	 	   {
	    	   com.avanza.pims.entity.Property property = EntityManager.getBroker().findById(
	    			   																			com.avanza.pims.entity.Property.class, 
	    			   																			new Long(hdnPropertyId)
	    			   																		);
	    	   this.requestView.getComplaintDetailsView().setPropertyId( property.getPropertyId() );
	    	   this.requestView.getComplaintDetailsView().setPropertyNumber( property.getPropertyNumber() );
	    	   this.requestView.getComplaintDetailsView().setUnitId(null);
	    	   this.requestView.getComplaintDetailsView().setUnitNumber(null);
	    	   this.requestView.getComplaintDetailsView().setPropertyId(null);
	    	   this.requestView.getComplaintDetailsView().setPropertyNumber(null);
	 	   }
	       updateValuesToMap();
    	}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromPropertySearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
 	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromUnitSearch()
    {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
	       if(sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT))
	 	   {
	    	   UnitView unitView = ((UnitView)sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT));
	    	   this.requestView.getComplaintDetailsView().setUnitId(unitView.getUnitId());
	    	   this.requestView.getComplaintDetailsView().setUnitNumber(unitView.getUnitNumber());
	    	   requestView.getComplaintDetailsView().setPropertyId( unitView.getPropertyId() );
			   requestView.getComplaintDetailsView().setPropertyNumber( unitView.getPropertyNumber() );
			   if( unitView.getStatusId().compareTo(WebConstants.UnitStatus.RENTED_ID) == 0)
			   {
				   ContractView contract  = UtilityService.getLatestActiveContractViewFromUnitId( unitView.getUnitId() );
				   hdnContractId = contract.getContractId().toString();
				   requestView.getComplaintDetailsView().setContractId( contract.getContractId() );
				   requestView.getComplaintDetailsView().setContractNumber( contract.getContractNumber() );
			   }
	 	   }
	       updateValuesToMap();
    	}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromUnitSearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
 	}
    	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromContractSearch()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			
			Contract contract = EntityManager.getBroker().findById(Contract.class, Long.valueOf( hdnContractId ) );
			requestView.getComplaintDetailsView().setContractId( contract.getContractId() );
			requestView.getComplaintDetailsView().setContractNumber( contract.getContractNumber() );
			Unit unit = contract.getContractUnits().iterator().next().getUnit();
			requestView.getComplaintDetailsView().setUnitNumber( unit.getUnitNumber() );
			requestView.getComplaintDetailsView().setUnitId( unit.getUnitId() );
			requestView.getComplaintDetailsView().setPropertyId( unit.getFloor().getProperty().getPropertyId() );
			requestView.getComplaintDetailsView().setPropertyNumber( unit.getFloor().getProperty().getPropertyNumber() );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromContractSearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
//		if( requestView.getDonationCollectedBy() ==null || requestView.getDonationCollectedBy().equals( "-1" )  ) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage("donationRequest.msg.collectedByRequired"));
//			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
//			return true;
//		}
//		
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}


	@SuppressWarnings( "unchecked" )
	public String onInitiateMaintenanceRequest()
	{
		try	
		{
			updateValuesFromMap();
			sessionMap.put("complaintDetails", requestView.getComplaintDetailsView() );
			updateValuesToMap();
			return "Maintenance";
		}
		catch(Exception ex)
		{
			logger.LogException("onInitiateMaintenanceRequest|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		return "";
	}
	

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			
			saveRequestInTransaction();
			
			getRequestDetails( requestView.getRequestId(),null );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty(  MessageConstants.RequestEvents.REQUEST_SAVED ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
	
		if(status!=null) requestView.setStatusId(status);
		requestView = complaintService.persist(requestView);
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
//			 SystemParameters parameters = SystemParameters.getInstance();
//			 String endPoint= parameters.getParameter( "MEMSDonationRequestBPEL" );
//			 MEMSDonationRequestBPELPortClient port=new MEMSDonationRequestBPELPortClient();
//			 port.setEndpoint(endPoint);
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
//			 
//			 port.initiate(
//					 		CommonUtil.getLoggedInUser(),
//					 		Integer.parseInt( requestView.getRequestId().toString()),
//					 		requestView.getRequestNumber(), 
//					 		null, 
//					 		null
//					 	   );
			 notifyMaintenanceRequest( WebConstants.Notification_MetaEvents.Event_Complaint_Submit );
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void notifyMaintenanceRequest(String notificationType)throws Exception
	{
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
			PersonView applicant=psAgent.getPersonInformation( requestView.getApplicantView().getPersonId() );
	    	if( applicant.getPersonFullName() == null )
	    	{
	    		eventAttributesValueMap.put("APPLICANT_NAME", applicant.getFullNameEn());
	    	}
	    	else
	    	{
	    		eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName() );
	    	}
			DateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
			eventAttributesValueMap.put("DATE", format.format( new Date() ) );
			eventAttributesValueMap.put("REQUEST_NUMBER", requestView.getRequestNumber());
			if( applicant != null )
			{
				contactInfoList = getContactInfoList(applicant);
			}
			if(contactInfoList != null )
			{
				generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
			}
	}
				
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 requestView.setOldStatusId(  requestView.getStatusId() );
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId(),null );
		 
		 setTaskOutCome(TaskOutcome.OK);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSiteVisitRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_SITE_VISIT_REQUIRED_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_SITE_VISIT_REQ;
		String event  =  MessageConstants.RequestEvents.REQUEST_SITE_VISIT_REQ;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(requestView.getStatusId() );
			 
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.OK);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSiteVisitRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSiteVisitDone()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_VISIT_DONE_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_SITE_VISIT_DONE;
		String event  =  MessageConstants.RequestEvents.REQUEST_SITE_VISIT_DONE;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
//			 requestView.setOldStatusId(  requestView.getStatusId() );
			 if(requestView.getOldStatusId() != null )
			 {
				 persistRequest( requestView.getOldStatusId() );
			 }
			 else
			 {
				 persistRequest( status );
			 }
			 setTaskOutCome(TaskOutcome.OK);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSiteVisitDone--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_COMPLETE_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 notifyMaintenanceRequest(WebConstants.Notification_MetaEvents.Event_Complaint_Complete );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSentToPM()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_WITH_PM_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_TO_PM;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_TO_PM;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.OK);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSentToPM--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDoneByPM()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_COMPLETED_BY_PM_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_DONE_BY_PM;
		String event  =  MessageConstants.RequestEvents.REQUEST_DONE_BY_PM;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.OK);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onDoneByPM--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSentToFM()
	{
		errorMessages 	=  new ArrayList<String>();
		successMessages =  new ArrayList<String>();
		Long status   	=  WebConstants.REQUEST_STATUS_WITH_FM_ID;
		String msg    	=  MessageConstants.RequestEvents.REQUEST_SENT_TO_FM;
		String event  	=  MessageConstants.RequestEvents.REQUEST_SENT_TO_FM;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.OK);
			 getRequestDetails( requestView.getRequestId(),null );
			 saveCommentsAttachment( event );
			 updateValuesToMap();
			 
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSentToPM--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onDoneByFM()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_COMPLETED_BY_FM_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_DONE_BY_FM;
		String event  =  MessageConstants.RequestEvents.REQUEST_DONE_BY_FM;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 requestView.setOldStatusId(  requestView.getStatusId() );
			 persistRequest( status );
			 
			 setTaskOutCome(TaskOutcome.APPROVE);
			 getRequestDetails( requestView.getRequestId(),null );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onDoneByFM--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onReviewed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 

			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 	
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			 utilityService.persistReviewRequest( reviewRequestView );
//			 requestView.setStatus(status)
			 if( requestView.getOldStatusId() != null )
			 {
				 persistRequest( requestView.getOldStatusId()  );
			 }
			 else
			 {
				 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
			 }
			 getRequestDetails( requestView.getRequestId(),null );
			 
			 setTaskOutCome(TaskOutcome.OK);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onReviewed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	public boolean hasReviewErrors() throws Exception
	{
		if( null==cmbReviewGroup.getValue() || cmbReviewGroup.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return true;
		}
		
		
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onReviewRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		try	
		{	updateValuesFromMap();	
			if( hasSaveErrors() || hasReviewErrors() || !hasReasonProvided()){ return; }
			 
			 
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = new ReviewRequestView();
			 reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			 reviewRequestView.setCreatedOn( new Date() );
			 reviewRequestView.setRfc( txtRemarks.trim() );
			 reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
			 reviewRequestView.setRequestId( requestView.getRequestId() );
		
			 try 
			 {
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
					 utilityService.persistReviewRequest( reviewRequestView );
					 requestView.setOldStatusId(requestView.getStatusId() );
					 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID);
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}

			 getRequestDetails( requestView.getRequestId(),null );
			 setTaskOutCome(TaskOutcome.FORWARD);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
//			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}

	@SuppressWarnings( "unchecked" )
	public String getPageTitle() 
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("complaint.heading"));
		return pageTitle;
	}

	@SuppressWarnings( "unchecked" )
	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	@SuppressWarnings( "unchecked" )
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowFMTaskButtons()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_FM_TASK ) 
			 )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowPMTaskButtons()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_PM_TASK ) 
			 )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewTaskButtons()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_REVIEW_REQUIRED ) 
			 )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSiteVisitTaskButtons()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_SITE_VISIT) 
			 )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowBusinessManagerTaskButtons()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED) 
			 )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}

	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public String getSelectOneCollectedByUser() {
		return selectOneCollectedByUser;
	}

	public void setSelectOneCollectedByUser(String selectOneCollectedByUser) {
		this.selectOneCollectedByUser = selectOneCollectedByUser;
	}
	
	

}