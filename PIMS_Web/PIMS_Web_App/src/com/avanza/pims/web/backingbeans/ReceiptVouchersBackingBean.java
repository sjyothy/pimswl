package com.avanza.pims.web.backingbeans;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ReceiptVouchersReportCriteria;
import com.avanza.pims.report.criteria.TenantDetailsReportCriteria;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class ReceiptVouchersBackingBean extends AbstractController
{
	private static final long serialVersionUID = -2408114848530306168L;
	Logger logger;
	private ReceiptVouchersReportCriteria receiptVouchersReportCriteria; 

	
	public ReceiptVouchersBackingBean()
	{	logger = Logger.getLogger(ReceiptVouchersBackingBean.class);
		if(CommonUtil.getIsEnglishLocale())
			receiptVouchersReportCriteria=new ReceiptVouchersReportCriteria(ReportConstant.Report.RECEIPT_VOUCHERS_REPORT_EN, ReportConstant.Processor.RECEIPT_VOUCHERS_REPORT,CommonUtil.getLoggedInUser());
		else    
			receiptVouchersReportCriteria=new ReceiptVouchersReportCriteria(ReportConstant.Report.RECEIPT_VOUCHERS_REPORT_AR, ReportConstant.Processor.RECEIPT_VOUCHERS_REPORT,CommonUtil.getLoggedInUser());
	}
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, receiptVouchersReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) 
	{
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		    } 
		catch (Exception exception)
		{
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public ReceiptVouchersReportCriteria getReceiptVouchersReportCriteria() {
		return receiptVouchersReportCriteria;
	}
	public void setReceiptVouchersReportCriteria(
			ReceiptVouchersReportCriteria receiptVouchersReportCriteria) {
		this.receiptVouchersReportCriteria = receiptVouchersReportCriteria;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
}