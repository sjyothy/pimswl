package com.avanza.pims.web.backingbeans;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PropertyOccupancyDetailsReportCriteria;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class PropertyOccupancyDetailsBacking extends AbstractController
{

	private static final long serialVersionUID = -8398868717270581295L;
	private Logger logger;
	PropertyOccupancyDetailsReportCriteria propertyOccupancyDetailsReportCriteria;
	
	public PropertyOccupancyDetailsBacking() 
	{
		logger = Logger.getLogger(PropertyOccupancyDetailsBacking.class);
			
		if(CommonUtil.getIsEnglishLocale())
			propertyOccupancyDetailsReportCriteria = new PropertyOccupancyDetailsReportCriteria(ReportConstant.Report.PROPERTY_OCCUPANCY_DETAILS_REPORT_EN, ReportConstant.Processor.PROPERTY_OCCUPANCY_DETAILS_REPORT,getLoggedInUserObj().getFullName());
		else
			propertyOccupancyDetailsReportCriteria = new PropertyOccupancyDetailsReportCriteria(ReportConstant.Report.PROPERTY_OCCUPANCY_DETAILS_REPORT_AR, ReportConstant.Processor.PROPERTY_OCCUPANCY_DETAILS_REPORT,getLoggedInUserObj().getSecondaryFullName());
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertyOccupancyDetailsReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public PropertyOccupancyDetailsReportCriteria getPropertyOccupancyDetailsReportCriteria() {
		return propertyOccupancyDetailsReportCriteria;
	}

	public void setPropertyOccupancyDetailsReportCriteria(
			PropertyOccupancyDetailsReportCriteria propertyOccupancyDetailsReportCriteria) {
		this.propertyOccupancyDetailsReportCriteria = propertyOccupancyDetailsReportCriteria;
	}
}
