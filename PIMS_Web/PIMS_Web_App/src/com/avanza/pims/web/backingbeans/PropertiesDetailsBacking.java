package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PropertyView;;

public class PropertiesDetailsBacking extends AbstractController{

	private static Logger logger = Logger.getLogger(PropertiesDetailsBacking.class);
	static Map viewRootMap=FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	static Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();;
	private List<PropertyView> propertiesList=new ArrayList<PropertyView>(0);
	private HtmlDataTable propertiesTable=new HtmlDataTable();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	public void init() 
	{
		super.init();
	}
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(viewRootMap.containsKey("recordSize"))
		recordSize=Integer.parseInt(viewRootMap.get("recordSize").toString() );
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	public List<PropertyView> getPropertiesList() 
	{
		if(viewRootMap.get("PROPERTY_VIEW_LIST")!=null)
			{
				propertiesList=(List<PropertyView>) viewRootMap.get("PROPERTY_VIEW_LIST");
				if(propertiesList!=null )
					viewRootMap.put("recordSize",propertiesList.size());
			}
		return propertiesList;
	}
	public void setPropertiesList(List<PropertyView> propertiesList) {
		this.propertiesList = propertiesList;
	}
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return localeInfo.getDateFormat();
		
	}
	public Boolean getEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	public String onOpenProperty()
	{
		PropertyView property;
		property=(PropertyView)propertiesTable.getRowData();
		 sessionMap.put(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY, property);
//		  sessionMap.put("PARENT_SCREEN", "PROJECT");
		  return "openPropertyInEditMode";
	}
	public HtmlDataTable getPropertiesTable() {
		return propertiesTable;
	}
	public void setPropertiesTable(HtmlDataTable propertiesTable) {
		this.propertiesTable = propertiesTable;
	}
}
