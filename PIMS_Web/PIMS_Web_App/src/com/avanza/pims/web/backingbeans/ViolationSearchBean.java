package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ViolationView;

public class ViolationSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	public String selectedType;
	public String selectedStatus;
	public String selectedCategory;
	public Date violationDate;
	public String txtunitRefNum;
	public String txtcontractNum;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(ViolationSearchBean.class);
	private List<ViolationView> dataList;
	InspectionView inspectionView = new InspectionView();
	private HtmlDataTable tbl_violationSearch;
	private InspectionViolationView dataItem = new InspectionViolationView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;

	private HtmlSelectOneMenu htmlSelectOneViolationCategory = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneViolationStatus = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneViolationType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneViolationAction = new HtmlSelectOneMenu();
	private HtmlInputText htmlInspectionNumber = new HtmlInputText();
	private HtmlInputText htmlInspectorName = new HtmlInputText();
	private HtmlInputText htmlPropertyName = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOnePropertyType = new HtmlSelectOneMenu();
	private HtmlInputText htmlContractNumber = new HtmlInputText();	
	private HtmlInputText htmlTenantName = new HtmlInputText();
	private HtmlInputText htmlUnitNumber = new HtmlInputText();
	
	// Constructors

	/** default constructor */
	public ViolationSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public InspectionViolationView getDataItem() {
		return dataItem;
	}

	public void setDataItem(InspectionViolationView dataItem) {
		this.dataItem = dataItem;
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/ViolationSearch.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               //"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        {
        	javaScriptText=extraJavaScript;
        }
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public String cmdAction_Click()
	{
		String methodName ="cmdAction_Click";
		logger.logInfo(methodName + "|" + "Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        sessionMap.put("violation",tbl_violationSearch.getRowData());
        
        openPopUp("ViolationActionPopUp.jsf","");
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public String cmdStatus_Click()
	{
		String methodName ="cmdStatus_Click";
		logger.logInfo(methodName + "|" + "Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        sessionMap.put("violation",tbl_violationSearch.getRowData());

        openPopUp("ViolationStatusPopUp.jsf","");
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	
	
	
	
	public String cmdPayFine_Click()
	{
		String methodName ="cmdStatus_Click";
		logger.logInfo(methodName + "|" + "Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViolationView violationView=(ViolationView)tbl_violationSearch.getRowData();
        List<PaymentScheduleView> paymentList=new ArrayList<PaymentScheduleView>();
        if(violationView.getPaymentScheduleView()!=null && violationView.getPaymentScheduleView().size()>0)
        	paymentList.addAll(violationView.getPaymentScheduleView());
        Double amount=new Double(0);
        logger.logInfo(methodName + "|" + "Iterating paymentList to calculate Amount..");
        for (PaymentScheduleView paymentScheduleView : paymentList) 
        {
        	
        	amount+=paymentScheduleView.getAmount();
        	
		}
        logger.logInfo(methodName + "|" + "Amount="+amount);
        PaymentScheduleView paymentScheduleViewRow=(PaymentScheduleView)paymentList.get(0);
        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
        paymentReceiptView.setAmount(amount);
        paymentReceiptView.setModeId(paymentScheduleViewRow.getPaymentModeId());
        paymentReceiptView.setPaymentScheduleView(paymentScheduleViewRow);
        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
        String extrajavaScriptText ="var screen_width = screen.width;"+
        "var screen_height = screen.height;"+
        "window.open('receivePayment.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";

        openPopUp("receivePayment.jsf",extrajavaScriptText);


		logger.logInfo(methodName+"|"+"Finish..");
		return "";
		
		
	}
	public void violationCategorySelected(ValueChangeEvent event)
	{
		Long violationCategoryId = -1L;
		try 
		{
			violationCategoryId = new Long((String) event.getNewValue());
		} catch (NumberFormatException e) 
		{
			logger.LogException("", e);
		}
		
		List<SelectItem> violationTypeList = new ArrayList<SelectItem>(0);
		
		if (violationCategoryId!=-1)
			violationTypeList=getViolationTypeByCategory(violationCategoryId);
			
		viewRootMap.put("violationTypeList", violationTypeList);
		
	}
	public List<SelectItem> getViolationTypes() 
	{
		List<SelectItem> violationTypes = new ArrayList<SelectItem>();
		if(viewRootMap.get("violationTypeList")!=null)
		violationTypes = (List<SelectItem>) viewRootMap.get("violationTypeList");
		return violationTypes;
	}
	public List<SelectItem> getViolationTypeByCategory(Long id)
	{	
			ApplicationBean applicationBean=new ApplicationBean();
			return applicationBean.getViolationTypeListByCategory(id);
	}
	private HashMap putControlValuesInView() throws Exception {

		String methodName = "putControlValuesInView";
		logger.logInfo(methodName + "|" + "Start");
		boolean isError=true;
		HashMap searchMap=new HashMap();
		try {
			
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
    		
		   logger.logInfo(methodName + "|" + "ViolationDate:"+violationDate);
		   if(violationDate!=null )
		   {
			   
			   searchMap.put("violationDate",df.format(violationDate));
		   }
		   logger.logInfo(methodName + "|" + "Is Deleted:0");
		    	   searchMap.put("isDeleted",0);
		   logger.logInfo(methodName + "|" + "txtcontractNum:"+txtcontractNum);
               if(txtcontractNum!=null && txtcontractNum.length()>0)
            	   searchMap.put("contractNum", txtcontractNum);
           logger.logInfo(methodName + "|" + "txtunitRefNum:"+txtunitRefNum);
               if(txtunitRefNum!=null && txtunitRefNum.length()>0)
            	   searchMap.put("unitNum", txtunitRefNum);
           logger.logInfo(methodName + "|" + "Category:"+selectedCategory);
               if(selectedCategory!=null && !selectedCategory.equals("-1"))
            	   searchMap.put("violationCategory", selectedCategory);
           logger.logInfo(methodName + "|" + "Status:"+selectedStatus);
               if(selectedStatus!=null && !selectedStatus.equals("-1"))
            	   searchMap.put("violationStatus", selectedStatus);
           logger.logInfo(methodName + "|" + "Type:"+selectedType);
               if(selectedType!=null && !selectedType.equals("-1"))
            	   searchMap.put("violationType", selectedType);
               
               
		 } catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
			throw e;

		}

		logger.logInfo(methodName + "|" + "Finsih");
		return searchMap;
	}

	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		try {

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();		
		
		String emptyValue = "-1";	
		
		Object violationCategory = htmlSelectOneViolationCategory.getValue();
		Object violationStatus = htmlSelectOneViolationStatus.getValue();
		Object violationType = htmlSelectOneViolationType.getValue();
		Object violationAction = htmlSelectOneViolationAction.getValue();		
		Object inspectionNumber = htmlInspectionNumber.getValue();		
		Object inspectorName = htmlInspectorName.getValue();
		Object propertyName = htmlPropertyName.getValue();
		Object propertyType = htmlSelectOnePropertyType.getValue();
		Object contractNumber = htmlContractNumber.getValue();
		Object tenantName = htmlTenantName.getValue();		
		Object unitNumber = htmlUnitNumber.getValue();
		
		if(violationCategory != null && !violationCategory.toString().trim().equals("") && !violationCategory.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.VIOLATION_CATEGORY, violationCategory.toString());
		}
		if(violationStatus != null && !violationStatus.toString().trim().equals("") && !violationStatus.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.VIOLATION_STATUS, violationStatus.toString());
		}
		if(violationType != null && !violationType.toString().trim().equals("") && !violationType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.VIOLATION_TYPE, violationType.toString());
		}
		if(violationAction != null && !violationAction.toString().trim().equals("") && !violationAction.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.VIOLATION_ACTION, violationAction.toString());
		}		
		if(inspectionNumber != null && !inspectionNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.INSPECTION_VISIT_NUMBER, inspectionNumber.toString());
		}			
		if(inspectorName != null && !inspectorName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.INSPECTOR_NAME, inspectorName.toString());
		}		
		if(propertyName != null && !propertyName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.PROPERTY_NAME, propertyName.toString());
		}		
		if(propertyType != null && !propertyType.toString().trim().equals("") && !propertyType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.PROPERTY_TYPE, propertyType.toString());
		}
		if(contractNumber != null && !contractNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.CONTRACT_NUMBER, contractNumber.toString());
		}
		if(tenantName != null && !tenantName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.TENANT_NAME, tenantName.toString());
		}
		if(unitNumber != null && !unitNumber.toString().trim().equals("") && !unitNumber.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.UNIT_NUMBER, unitNumber.toString());
		}
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
		Object violationCategory = htmlSelectOneViolationCategory.getValue();
		Object violationStatus = htmlSelectOneViolationStatus.getValue();
		Object violationType = htmlSelectOneViolationType.getValue();
		Object violationAction = htmlSelectOneViolationAction.getValue();		
		Object propertyType = htmlSelectOnePropertyType.getValue();
		Object inspectionNumber = htmlInspectionNumber.getValue();		
		Object inspectorName = htmlInspectorName.getValue();
		Object propertyName = htmlPropertyName.getValue();
		Object contractNumber = htmlContractNumber.getValue();
		Object tenantName = htmlTenantName.getValue();		
		Object unitNumber = htmlUnitNumber.getValue();
		
		
		if(StringHelper.isNotEmpty(violationCategory.toString()) && !violationCategory.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(violationStatus.toString()) && !violationStatus.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(violationType.toString()) && !violationType.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(violationAction.toString()) && !violationAction.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(propertyType.toString()) && !propertyType.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(inspectionNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(inspectorName.toString()))
			return true;
		if(StringHelper.isNotEmpty(propertyName.toString()))
			return true;
		if(StringHelper.isNotEmpty(contractNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(tenantName.toString()))
			return true;
		if(StringHelper.isNotEmpty(unitNumber.toString()))
			return true;
		
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		logger.logInfo(methodName + "|" + "Start::");
		try 
		{
			if(atleastOneCriterionEntered()){
				loadDataList();
				if(viewRootMap.get("SESSION_VIOLATION_SEARCH_LIST")!=null){
					List<ViolationView> violationViewList = (List<ViolationView>) viewRootMap.get("SESSION_VIOLATION_SEARCH_LIST");
					if(violationViewList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
			}
			else{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		logger.logInfo(methodName + "|" + "Finish::");
		return "";
	}

	
   private void loadDataList() throws Exception
   {
	   String methodName = "btnSearch_Click";
	   logger.logInfo(methodName + "|" + "Start::");
	   Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	   HashMap searchMap= getSearchCriteria();
		PropertyServiceAgent psa = new PropertyServiceAgent();
	   if(viewRootMap.containsKey("SESSION_VIOLATION_SEARCH_LIST"))
		   viewRootMap.remove("SESSION_VIOLATION_SEARCH_LIST");
		List<ViolationView> violationViewList =psa.getAllInspectionViolation(searchMap,"dd/MM/yyyy");
		viewRootMap.put("SESSION_VIOLATION_SEARCH_LIST",violationViewList);
		dataList=violationViewList;
		  if(dataList!=null)
				recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
	        
		
		logger.logInfo(methodName + "|" + "Finish::");
   }
	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) {
							}
			else
			{
				   if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
		 	       {
		 		     CollectPayment();
		 		     
		 	       }
				   
				   if(sessionMap.containsKey("NEW_VIOLATION_VIEW")){
					   infoMessages = new ArrayList<String>();
					   List<ViolationView> newViolationList = (List<ViolationView>) sessionMap.get("NEW_VIOLATION_VIEW");
						sessionMap.remove("NEW_VIOLATION_VIEW");
						if(!newViolationList.isEmpty()){
							ViolationView tempView = newViolationList.get(0);
							if(sessionMap.containsKey("EDIT_DONE")){
								
								Integer index = (Integer) viewRootMap.get("index");
								List<ViolationView> tempList = getDataList();
								tempList.remove(index.intValue());
								tempList.add(index.intValue(), tempView);
								viewRootMap.put("SESSION_VIOLATION_SEARCH_LIST", tempList);
								sessionMap.remove("EDIT_DONE");
								infoMessages.add(CommonUtil.getBundleMessage("violation.msg.EditViolation"));
							}
						}
					}
				
			}
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}
    private void CollectPayment()throws Exception
    {
    	String methodName="CollectPayment"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
	    	 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     psa.addPaymentReceiptForViolation(prv);
		     loadDataList();
		     logger.logInfo(methodName + "|" + "Finish...");
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured..."+ex);
			throw ex;
		}
    }
	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		HashMap searchMap;
		String methodName="prerender"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
		  if(sessionMap.containsKey("FROM_STATUS")  && sessionMap.get("FROM_STATUS").toString().equals("1"))
		  {
			  sessionMap.remove("FROM_STATUS");
			  loadDataList();
			    
		  }
		   
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		  logger.logInfo(methodName + "|" + "Finish...");
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	
	public String getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}


	public String getTxtcontractNum() {
		return txtcontractNum;
	}

	public void setTxtcontractNum(String txtcontractNum) {
		this.txtcontractNum = txtcontractNum;
	}

	public List<ViolationView> getDataList() {
		dataList =null;
		if (dataList == null) 
		{
			if(viewRootMap.containsKey("SESSION_VIOLATION_SEARCH_LIST"));
			dataList=(ArrayList)viewRootMap.get("SESSION_VIOLATION_SEARCH_LIST");
		}
		return dataList;

	}

	public void setDataList(List<ViolationView> dataList) {
		this.dataList = dataList;
	}

	public String getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public HtmlDataTable getTbl_violationSearch() {
		return tbl_violationSearch;
	}

	public void setTbl_violationSearch(HtmlDataTable tbl_violationSearch) {
		this.tbl_violationSearch = tbl_violationSearch;
	}

	public Date getViolationDate() {
		return violationDate;
	}

	public void setViolationDate(Date violationDate) {
		this.violationDate = violationDate;
	}
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlSelectOneMenu getHtmlSelectOneViolationCategory() {
		return htmlSelectOneViolationCategory;
	}

	public void setHtmlSelectOneViolationCategory(
			HtmlSelectOneMenu htmlSelectOneViolationCategory) {
		this.htmlSelectOneViolationCategory = htmlSelectOneViolationCategory;
	}

	public HtmlSelectOneMenu getHtmlSelectOneViolationStatus() {
		return htmlSelectOneViolationStatus;
	}

	public void setHtmlSelectOneViolationStatus(
			HtmlSelectOneMenu htmlSelectOneViolationStatus) {
		this.htmlSelectOneViolationStatus = htmlSelectOneViolationStatus;
	}

	public HtmlSelectOneMenu getHtmlSelectOneViolationType() {
		return htmlSelectOneViolationType;
	}

	public void setHtmlSelectOneViolationType(
			HtmlSelectOneMenu htmlSelectOneViolationType) {
		this.htmlSelectOneViolationType = htmlSelectOneViolationType;
	}

	public HtmlSelectOneMenu getHtmlSelectOneViolationAction() {
		return htmlSelectOneViolationAction;
	}

	public void setHtmlSelectOneViolationAction(
			HtmlSelectOneMenu htmlSelectOneViolationAction) {
		this.htmlSelectOneViolationAction = htmlSelectOneViolationAction;
	}

	public HtmlInputText getHtmlInspectionNumber() {
		return htmlInspectionNumber;
	}

	public void setHtmlInspectionNumber(HtmlInputText htmlInspectionNumber) {
		this.htmlInspectionNumber = htmlInspectionNumber;
	}

	public HtmlInputText getHtmlInspectorName() {
		return htmlInspectorName;
	}

	public void setHtmlInspectorName(HtmlInputText htmlInspectorName) {
		this.htmlInspectorName = htmlInspectorName;
	}

	public HtmlInputText getHtmlPropertyName() {
		return htmlPropertyName;
	}

	public void setHtmlPropertyName(HtmlInputText htmlPropertyName) {
		this.htmlPropertyName = htmlPropertyName;
	}

	public HtmlSelectOneMenu getHtmlSelectOnePropertyType() {
		return htmlSelectOnePropertyType;
	}

	public void setHtmlSelectOnePropertyType(
			HtmlSelectOneMenu htmlSelectOnePropertyType) {
		this.htmlSelectOnePropertyType = htmlSelectOnePropertyType;
	}

	public HtmlInputText getHtmlContractNumber() {
		return htmlContractNumber;
	}

	public void setHtmlContractNumber(HtmlInputText htmlContractNumber) {
		this.htmlContractNumber = htmlContractNumber;
	}

	public HtmlInputText getHtmlTenantName() {
		return htmlTenantName;
	}

	public void setHtmlTenantName(HtmlInputText htmlTenantName) {
		this.htmlTenantName = htmlTenantName;
	}

	public HtmlInputText getHtmlUnitNumber() {
		return htmlUnitNumber;
	}

	public void setHtmlUnitNumber(HtmlInputText htmlUnitNumber) {
		this.htmlUnitNumber = htmlUnitNumber;
	}
   public String btnEditViolation_Click(){
	   
	 ViolationView vV = (ViolationView)  tbl_violationSearch.getRowData();
	 
	 Integer index = tbl_violationSearch.getRowIndex();
	 viewRootMap.put("index", index);
	 
	 sessionMap.put("VIOLATION_VIEW_FROM_SEARCH", vV);
	 
		final String viewId = "/ViolationSearch.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:showAddViolationPopup();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		
		return "EditViolation";
	
   }
	
   public String btnAddViolation_Click(){
		 
		 return "AddViolation";
		
	   }

}