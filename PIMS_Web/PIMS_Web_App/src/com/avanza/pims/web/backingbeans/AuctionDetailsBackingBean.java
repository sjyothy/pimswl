package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.column.HtmlSimpleColumn;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.bpel.proxy.pimsprepareauction.PIMSPrepareAuctionBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.AdvertisementCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionAdvertisementView;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionVenueView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.ui.util.ResourceUtil;

public class AuctionDetailsBackingBean extends AbstractController {
	/**
	 * 
	 */

	public AuctionDetailsBackingBean() {
	}
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;	
	
	private transient Logger logger = Logger.getLogger(AuctionDetailsBackingBean.class);

	private transient PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	
	private final String TAB_AUCTION_UNITS = "auctionUnitsTab";
	private final String TAB_AUCTION_DETAILS = "auctionDetailsTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private String NEXT_TAB = "NEXT_TAB";
	private String BACK_TAB = "BACK_TAB";
	private String BACK_CLICK = "BACK_CLICK";
	private String NEXT_CLICK = "NEXT_CLICK";
	
	private final String AUCTION_VIEW = "AUCTION_VIEW";
	private final String ADVERTISEMENT_VIEW = "ADVERTISEMENT_VIEW";
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	public String pageMode="";
	private final String PAGE_MODE ="PAGE_MODE";
	private final String PAGE_MODE_NEW ="PAGE_MODE_NEW";
	private final String PAGE_MODE_DRAFT ="PAGE_MODE_DRAFT";
	private final String PAGE_MODE_DRAFT_DONE ="PAGE_MODE_DRAFT_DONE";
	private final String PAGE_MODE_APPROVE ="PAGE_MODE_APPROVE";
	private final String PAGE_MODE_APPROVE_DONE ="PAGE_MODE_APPROVE_DONE";
	private final String PAGE_MODE_PUBLISH ="PAGE_MODE_PUBLISH";
	private final String PAGE_MODE_PUBLISH_DONE ="PAGE_MODE_PUBLISH_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private final String SHOW_CANCEL_BUTTON = "SHOW_CANCEL_BUTTON";
	
	private final String NOTIFICATION_EVENT_PUBLISH_ADVERTISEMENT = "Event.PrepareAuction.PublishAdvertisement";
	
	private Boolean canSelectVenue = false; // set when advertisement is approved
	private Boolean advertisementMode = false; // set when advertisement tab is to be displayed
	private Boolean auctionViewMode = false; // set when auction is in view-only mode else editable
	private Boolean auctionFullViewMode = false; // set when auction is in view-only mode with advertisement view also
	private Boolean canApproveAdvertisement = false; // set when advertisement draft status is READY_FOR_APPROVAL
	private Boolean canRequestForPublication = false; // set when advertisement draft status is APPROVED
	private Boolean canPublishAdvertisement = false; // set when advertisement draft status is SENT_FOR_PUBLICATION
	private Boolean advertisementSaveMode = false; // set when advertisement is in draft status and can be modified
	private Boolean conductAuctionMode = false; // set when advertisement is in draft status and can be modified
	private Boolean dateVenueSelected = false;
	private Boolean auctionCreateMode = false;
	private Boolean auctionEditMode = false;
	private Boolean renderAuctionNumber = false;
	private String message="";
	private String selectedTab="";
	private String auctionReqStartDateTimeString="";
	private String auctionReqEndDateTimeString="";
	private String advertisementDateString="";
	
	private Boolean canCompleteTask = false;
	private Boolean taskCompleted = false;
	private Boolean fullViewMode = false;
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	private String heading="";
	private String completeButtonLabel="";

	private String hhStart = "";
	private String mmStart = "";
	private String hhEnd = "";
	private String mmEnd = "";
	
	private String readonlyStyleClass = "";
	private String numericReadonlyStyleClass = "";
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	private AuctionView auctionView = new AuctionView();
	private AuctionAdvertisementView advertisementView = new AuctionAdvertisementView();

	private AuctionUnitView dataItem = new AuctionUnitView();
	private List<AuctionUnitView> dataList = new ArrayList<AuctionUnitView>();

	private HtmlDataTable dataTable = new HtmlDataTable();
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = "";
	private Boolean sortAscending = true;
	
	private Boolean readOnlyMode;

	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton auctionDetailsSaveButton = new HtmlCommandButton();
	private HtmlCommandButton auctionDetailsCancelButton = new HtmlCommandButton();
	private HtmlCommandButton auctionUnitsSaveButton = new HtmlCommandButton();
	private HtmlCommandButton auctionUnitsCancelButton = new HtmlCommandButton();
	private HtmlCommandButton approveButton = new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	private HtmlCommandButton publishButton = new HtmlCommandButton();
	private HtmlCommandButton submitButton = new HtmlCommandButton();
	private HtmlCommandButton addAuctionUnitsButton = new HtmlCommandButton();
	private HtmlCommandButton selectVenueButton = new HtmlCommandButton();
	private HtmlCommandButton applyOpeningPriceButton = new HtmlCommandButton();
	private HtmlCommandButton applyDepositButton = new HtmlCommandButton();
	private HtmlSimpleColumn selectColumn = new HtmlSimpleColumn(); 
	private HtmlSimpleColumn actionColumn = new HtmlSimpleColumn();
	private HtmlInputText auctionNumberField = new HtmlInputText();
	private HtmlInputText auctionStatusField = new HtmlInputText();
	private HtmlInputText auctionDescriptionField = new HtmlInputText();
	private HtmlInputText auctionVenueField = new HtmlInputText();
	private HtmlInputText outbiddingValueField = new HtmlInputText();
	private HtmlInputText confiscationPercentField = new HtmlInputText();
	private HtmlInputText gracePeriodField = new HtmlInputText();
	private HtmlSelectOneMenu hhStartField = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu mmStartField = new HtmlSelectOneMenu();
	
	private HtmlCalendar auctionDateField = new HtmlCalendar();
	private HtmlCalendar publicationDateField = new HtmlCalendar();
	private HtmlCalendar registrationStartDateField = new HtmlCalendar();
	private HtmlCalendar registrationEndDateField = new HtmlCalendar();
	
	private HtmlOutputLabel mandatoryLabel = new HtmlOutputLabel();	
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	
	private Boolean generateNotification(String eventName){
		Boolean success = false;
		try{
			logger.logInfo("generateNotification started...");
			
//			auctionView = getAuctionView();
			logger.logInfo("User : %s | Auction Number : %s ", getLoggedInUser(), getAuctionNumber());
			logger.logInfo("Event Name : %s ", eventName);
			NotificationFactory nsfactory = NotificationFactory.getInstance();
			NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
			Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();	
			getNotificationPlaceHolder(event);
			List<ContactInfo> contactInfo = new ArrayList<ContactInfo>();
			
			String group = new PropertyServiceAgent().getSystemConfigurationValueByKey(WebConstants.AMAF_IT_AND_MEDIA_SECTION_SEC_USER_GROUP);
			
			if(group != null && !group.equals(""))
			{
				Set<User> users =  SecurityManager.getGroup(group).getUsers();		
				
				for(User user : users)
				{			 
				 contactInfo.add(new ContactInfo(getLoggedInUser(), null, null, user.getEmailUrl()));			 
				}
				
				if(contactInfo.size()>0)
					notifier.fireEvent(event, contactInfo);
			}
			success = true;
			logger.logInfo("generateNotification completed successfully...");
		}
		catch(Exception exception){
			logger.LogException("generateNotification crashed...",exception);
		}
		return success;
	}
	
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		
		StringBuilder stringBuilder = new StringBuilder(getStringFromDate(getAuctionDate()));
	    stringBuilder = stringBuilder.append(" ").append(getHhStart()).append(":").append(getMmStart());
	    
	    List<AuctionUnitView> temp = new ArrayList<AuctionUnitView>();
	    if(viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
			temp = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
	    
	    placeHolderMap.setValue("AUCTION_NO", getAuctionNumber());
	    placeHolderMap.setValue("AUCTION_DESC", getAuctionDescription());
	    placeHolderMap.setValue("AUCTION_DATE_TIME", stringBuilder.toString());
		placeHolderMap.setValue("AUCTION_VENUE", getAuctionVenue());
		placeHolderMap.setValue("PUB_DATE", getStringFromDate(getAdvertisementDate()));
		placeHolderMap.setValue("REG_START_DATE", getStringFromDate(getRequestStartDate()));
		placeHolderMap.setValue("REG_END_DATE", getStringFromDate(getRequestEndDate()));
		
		stringBuilder = new StringBuilder();
		stringBuilder = stringBuilder.append(temp.size()).append(" units:</br></br>");
		for(AuctionUnitView auctionUnitView:temp){
			stringBuilder = stringBuilder.append("Unit Number: ").append(auctionUnitView.getUnitNumber()).append("</br>");
			stringBuilder = stringBuilder.append("Property Number: ").append(auctionUnitView.getPropertyNumber()).append("</br>");
			stringBuilder = stringBuilder.append("Property Commercial Name: ").append(auctionUnitView.getPropertyCommercialName()).append("</br>");
			stringBuilder = stringBuilder.append("Unit Type: ").append(auctionUnitView.getUnitTypeEn()).append("</br>");
			stringBuilder = stringBuilder.append("Unit Usage: ").append(auctionUnitView.getUnitUsageTypeEn()).append("</br>");
			stringBuilder = stringBuilder.append("Unit Opening Price: ").append(auctionUnitView.getOpeningPrice()).append("</br>");
			stringBuilder = stringBuilder.append("Unit Deposit Amount: ").append(auctionUnitView.getDepositAmount()).append("</br>");
			stringBuilder = stringBuilder.append("Emirate: ").append(auctionUnitView.getEmirateEn()).append("</br>");
			stringBuilder = stringBuilder.append("</br>");
		}
		
		placeHolderMap.setValue("AUCTION_UNITS", stringBuilder.toString());
		
		logger.logDebug(methodName+"|Finish");
	}
	
	public String saveCommentsButton()
    {
		Boolean success = false;
    	String methodName="saveCommentsButton";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	Long auctionId = 0L;
	    	if(viewMap.containsKey(WebConstants.AUCTION_ID))			
				auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
	    	success = saveComments(auctionId);
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success+"";
    }	
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.AUCTION;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public void applyOpeningPriceToUnits(ActionEvent event)
    {
    	try{
	    	logger.logInfo("applyOpeningPriceToUnits started...");
	    	Integer count = setSelectedAuctionUnits();
	    	if (count>0)
	    	{
	    		sessionMap.put("POPUP_MODE", WebConstants.OPENINIG_PRICE);
				
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:showPriceEditPopup();";
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	    	}
	    	else
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.NO_UNIT_SELECTED_TO_APPLY_OPENING_PRICE));
	    	}
	    	logger.logInfo("applyOpeningPriceToUnits completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException("applyOpeningPriceToUnits crashed ", throwable);
		}
    }
	
	public void applyDepositToUnits(ActionEvent event)
    {
    	try{
	    	logger.logInfo("applyDepositToUnits started...");
	    	Integer count = setSelectedAuctionUnits();
	    	if (count>0)
	    	{
	    		sessionMap.put("POPUP_MODE", WebConstants.DEPOSIT_AMOUNT);
				
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:showDepositEditPopup();";
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	    	}
	    	else
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.NO_UNIT_SELECTED_TO_APPLY_DEPOSIT));
	    	}
	    	logger.logInfo("applyDepositToUnits completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException("applyDepositToUnits crashed ", throwable);
		}
    }
	
	public Integer setSelectedAuctionUnits(){
		List<AuctionUnitView> allUnits = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
		List<AuctionUnitView> selectedUnits = new ArrayList<AuctionUnitView>(0);
		Integer count = 0;
		if(allUnits!=null && !allUnits.isEmpty()){
			for(AuctionUnitView auctionUnitView:allUnits){
				if(auctionUnitView.getSelected()){
					selectedUnits.add(auctionUnitView);
					count++;
				}
			}
			if(count>0)
				sessionMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedUnits);
		}
		return count;
	}
	
/*	public void saveComments(ActionEvent event)
    {
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	Long auctionId = -1L;
	    	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	    	if (sessionMap.containsKey(WebConstants.AUCTION_ID))
	    	{
	    		auctionId = (Long) sessionMap.get(WebConstants.AUCTION_ID);
		    	String notesOwner = WebConstants.AUCTION;
		    	NotesController.saveNotes(notesOwner, auctionId);
		    	errorMessages.clear();
				infoMessage= getBundleMessage(WebConstants.PropertyKeys.Commons.NOTES_ADDED);
	    	}
	    	else
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NOTES_FAILURE_MESSAGE));
	    	}
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    }
*/	
	/*public void saveAttachments(ActionEvent event)
    {
    	String methodName="saveAtttachments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	Long auctionId = -1L;
	    	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	    	if (sessionMap.containsKey(WebConstants.AUCTION_ID))
	    	{
	    		auctionId = (Long) sessionMap.get(WebConstants.AUCTION_ID);
		    	String repositoryId = "pimsDb";
		    	String fileSystemRepository = "auctionRepository";
		    	String user = getLoggedInUser();
		    	String entityId = auctionId.toString();
		    	AttachmentController.saveAttachments(repositoryId, fileSystemRepository, getLoggedInUser(), entityId);
		    	errorMessages.clear();
				infoMessage= getBundleMessage(WebConstants.PropertyKeys.Commons.ATTACHMENTS_ADDED);
	    	}
	    	else
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.ATTACHMENT_FAILURE_MESSAGE));
	    	}
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    }*/
	
	public String saveAttachmentsButton()
    {
		Boolean success = false;
    	String methodName="saveAttachmentsButton";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	Long auctionId = 0L;
	    	if(viewMap.containsKey(WebConstants.AUCTION_ID))			
				auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
	    	success = saveAttachments(auctionId);
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success+"";
    }	
	
	/*public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveAtttachments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String repositoryId = WebConstants.ATTACHMENT_REPOSITORY_ID;
	    	String fileSystemRepository = WebConstants.FILE_SYSTEM_REPOSITORY_AUCTION;
	    	String user = getLoggedInUser();
	    	String entityId = referenceId.toString();
	    	AttachmentController.saveAttachments(repositoryId, fileSystemRepository, getLoggedInUser(), entityId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
			logger.LogException(methodName + " crashed ", throwable);
		}
    	
    	return success;
    }*/
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	
	public String getInvalidMessage(String field){
		
		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}

	
	
    public String getFieldRequiredMessage(String field){
		StringBuffer message = new StringBuffer("");
		logger.logInfo("getFieldRequiredMessage(String) started...");
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.IS_REQUIRED));
		logger.logInfo("getFieldRequiredMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getFieldRequiredMessage(String) crashed ", exception);
		}
		return message.toString();
	}

    public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
    
    @SuppressWarnings("unchecked")
    public void selectAuctionUnit(ValueChangeEvent event){
    	int index = dataTable.getRowIndex();
    	AuctionUnitView temp = (AuctionUnitView)dataTable.getRowData();
//    	errorMessages.add("Selected:" + dataTable.getRowIndex() + " " + temp.getUnitNumber() + " " + temp.getSelected() + " " + event.getNewValue().toString());
    	
    	dataList = (List<AuctionUnitView>) viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
    	AuctionUnitView auctionUnitView = dataList.remove(index);
    	auctionUnitView.setSelected((Boolean)event.getNewValue());
    	dataList.add(index,auctionUnitView);
    	viewMap.put(WebConstants.SELECTED_AUCTION_UNITS,dataList);
    }
    
    public void textChanged(ValueChangeEvent event){
    	canCompleteTask = false;
    }
    
    
    @SuppressWarnings("unchecked")
	public Boolean advertisementFieldsValidated() {
	    logger.logInfo("advertisementFieldsValidated() started...");
	    Boolean validated = true;
		errorMessages = new ArrayList<String>();
		DateFormat formatter = new SimpleDateFormat(WebConstants.DATE_FORMAT);
		Date startDate;
		Date endDate;
	    try
		{
	    	auctionView = getAuctionView();
	    	auctionView.setRequestStartDate(getRequestStartDate());
	    	auctionView.setRequestEndDate(getRequestEndDate());
	    	
	    	
	    	startDate = auctionView.getRequestStartDate();
			endDate = auctionView.getRequestEndDate();
	    	
	    	advertisementDateString = getStringFromDate(advertisementView.getAdvertisementDate());
			if (advertisementDateString.equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Advertisement.PUBLICATION_DATE));
				 validated = false;
				 return validated;
			}
			auctionReqStartDateTimeString = getStringFromDate(startDate);
			if (auctionReqStartDateTimeString.equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.REQUEST_START_DATE));
				 validated = false;
				 return validated;
			}
			else if (isInvalidDate(auctionReqStartDateTimeString)) {
				 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.REQUEST_START_DATE));
				 validated = false;
				 return validated;
			}
			auctionReqEndDateTimeString = getStringFromDate(endDate);
			if (auctionReqEndDateTimeString.equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.REQUEST_END_DATE));
				 validated = false;
				 return validated;
			}
			else if (isInvalidDate(auctionReqEndDateTimeString)) {
				 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.REQUEST_START_DATE));
				 validated = false;
				 return validated;
			}
			else
			{
				if(endDate.before(startDate))
				{
					errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.REQUEST_DATES_INVALID));
					validated = false;
					return validated;
				}
			}	
			logger.logInfo("advertisementFieldsValidated() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("advertisementFieldsValidated() crashed ", exception);
		}
		return validated;
	}
    
    /**
     * Sets the binded field values to the corresponsing auctionView fields
     */
    private void fillAuctionView(){
    	auctionView = getAuctionView();
    	if(auctionView==null)
    		auctionView = new AuctionView();
    	auctionView.setAuctionNumber(getAuctionNumber());
    	auctionView.setAuctionStatus(getAuctionStatus());
    	auctionView.setAuctionTitle(getAuctionDescription());
    	auctionView.setConfiscationPercent(getConfiscationPercent());
    	auctionView.setOutBiddingValue(getOutBiddingValue());
    	AuctionVenueView auctionVenue = (AuctionVenueView)viewMap.get("auctionVenueView");
    	if(auctionVenue==null){
    		Long auctionVenueId = (Long) viewMap.get(WebConstants.AUCTION_VENUE_ID);
    		if(auctionVenueId!=null){
    			auctionVenue = new AuctionVenueView();
        		auctionVenue.setVenueId(auctionVenueId);	
    		}
    	}
    	
    	auctionView.setAuctionVenue(auctionVenue);
    	auctionView.setAuctionDateVal(getAuctionDate());
    	auctionView.setRequestStartDate(getRequestStartDate());
    	auctionView.setRequestEndDate(getRequestEndDate());
    	StringBuilder stringBuilder = new StringBuilder(getHhStart());
    	stringBuilder = stringBuilder.append(":").append(getMmStart());
    	auctionView.setAuctionStartTime(stringBuilder.toString());
		if (viewMap.containsKey(ADVERTISEMENT_VIEW))
			advertisementView = (AuctionAdvertisementView) viewMap.get(ADVERTISEMENT_VIEW);
    	if(advertisementView==null)
    		advertisementView = new AuctionAdvertisementView();
    	advertisementView.setAdvertisementDate(getAdvertisementDate());
    	auctionView.setAuctionAdvertisement(advertisementView);
    	
    	setAuctionView(auctionView);
    }
    
    private Boolean validateUnitsTab(){
    	logger.logInfo("validateUnitsTab() started...");
	    Boolean validated = true;
    	List<AuctionUnitView> temp = new ArrayList<AuctionUnitView>();
    	try
 		{
	    	if(viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
				temp = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
	    	
			if(temp.size()<=0)
			{
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.ATLEAST_ONE_UNIT));
				validated = false;
				//return validated;
			}
			
			for(AuctionUnitView auctionUnitView: temp){
				if( (auctionUnitView.getDepositAmount()==0D) || (auctionUnitView.getOpeningPrice()==0D)){
					errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.AUCTION_UNIT_AMOUNTS_NOT_ENTERED));
					validated = false;
					break;
					//return validated;
				}
				if( (auctionUnitView.getOpeningPrice()>auctionUnitView.getRentValue())){
					errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.MSG_OPENING_PRICE_INVALID));
					validated = false;
					break;
					//return validated;
				}
			}
			
			logger.logInfo("validateUnitsTab() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateUnitsTab() crashed ", exception);
		}
		return validated;
    }
    
    private Boolean validateAuctionDetailsTab(){
    	logger.logInfo("validateAuctionDetailsTab() started...");
    	Boolean validated = true;
	    Date startDate = null;
		Date endDate = null;		
		DateFormat formatter = new SimpleDateFormat(WebConstants.DATE_FORMAT);
		fillAuctionView();
		auctionView = getAuctionView();
		advertisementView = auctionView.getAuctionAdvertisement();
		
		String auctionTitle = auctionView.getAuctionTitle();
		
	    try
		{
    	
	    	if (auctionTitle==null || auctionTitle.trim().equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.TITLE));
				 validated = false;
				 //return validated;
			}
			
	    	Date todayDate = new Date();
			Date auctionDateVal = auctionView.getAuctionDateVal();
			Date requestEndDate = auctionView.getRequestEndDate();
			String auctionDate = getStringFromDate(auctionDateVal);
	//		String auctionStartTime = hhStart+":"+mmStart;
			String auctionStartTime = auctionView.getAuctionStartTime();
			String auctionEndTime = hhEnd+":"+mmEnd;
			Integer hhStartVal = 0;
			Integer mmStartVal = 0;
			Integer hhEndVal = 0;
			Integer mmEndVal = 0;
			
			hhStart = getHhStart();
			mmStart = getMmStart();
			
			startDate = auctionView.getRequestStartDate();
			endDate = auctionView.getRequestEndDate();
			
			if(auctionView.getAuctionVenue()!=null){
				String name = auctionView.getAuctionVenue().getVenueName();
				if(name==null || name.equals(""))
				{
					errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.VENUE));
					validated = false;
					//return validated;
				}
			}
			else{
				errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.VENUE));
				validated = false;
				//return validated;
			}
			
			if(auctionView.getOutbiddingValue()==null){
				errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.OUTBIDDING_VALUE));
				validated = false;
				//return validated;
			}
			else if( auctionView.getOutbiddingValue()!= null && (Double.compare(auctionView.getOutbiddingValue(), 0) < 0))			
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_OUTBIDDING_VALUE_INVALID));
				validated = false;
			}

			/*String auctionDateString = auctionView.getAuctionDate();
			auctionDateString = auctionDateString.trim();*/
			
			if (auctionDate==null || auctionDate.equals("")){
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.DATE));
				 validated = false;
				 //return validated;
			}
			else{
				
				if(auctionDateVal.before(todayDate))
				{
					 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_DATE_INVALID));
					 validated = false;
				}
				
				auctionDateVal = formatter.parse(auctionDate);
			}
			
			if(auctionStartTime==null || auctionStartTime.equals(":")){
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.START_TIME));
				 validated = false;
				 //return validated;
			}
			/*if(auctionEndTime==null || auctionEndTime.equals(":")){
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.END_TIME));
				 validated = false;
				 return validated;
			}
			*/
				
			if( ((hhStart.trim().length()>0) && (mmStart.trim().length()==0)) || ((mmStart.trim().length()>0) && (hhStart.trim().length()==0)) )
			{
				 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.START_TIME));
				 validated = false;
				 //return validated;
			}
			/*if( ((hhEnd.trim().length()>0) && (mmEnd.trim().length()==0)) || ((mmEnd.trim().length()>0) && (hhEnd.trim().length()==0)) )
			{
				 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.END_TIME));
				 validated = false;
				 return validated;
			}*/
			else{
				try{
					hhStartVal = Integer.parseInt(hhStart);
					mmStartVal = Integer.parseInt(mmStart);
				}
				catch(NumberFormatException numberFormatException){
					 logger.LogException("Invalid Start Time... (Problem in parsing start time)", numberFormatException);
					 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.START_TIME));
					 validated = false;
					 //return validated;
				}
			}
			advertisementDateString = getStringFromDate(advertisementView.getAdvertisementDate());
			if (advertisementDateString.equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Advertisement.PUBLICATION_DATE));
				 validated = false;
				 //return validated;
			}
			
			if(auctionView.getConfiscationPercent()==null){
				errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.CONFISCATION_PERCENTAGE));
				validated = false;
				//return validated;
			}			
			else if( auctionView.getConfiscationPercent()!= null && (Double.compare(auctionView.getConfiscationPercent(), 100) > 0 || Double.compare(auctionView.getConfiscationPercent(), 0) < 0))			
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_CONFISCATION_AMOUNT_INVALID));
				validated = false;
			}
			
			auctionReqEndDateTimeString = getStringFromDate(endDate);
			auctionReqStartDateTimeString = getStringFromDate(startDate);
			
			if (auctionReqEndDateTimeString.equals("")) {
				 errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.REQUEST_END_DATE));
				 validated = false;
				 //return validated;
			}			
			else
			{
				//TODO:Commented for UAT Need to open for production
//				if(endDate.before(todayDate))
//				{
//					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_REQUEST_END_DATE_INVALID));
//					validated = false;
//				}				
			}	
			
			if (auctionReqStartDateTimeString.equals("")) {
				errorMessages.add(getFieldRequiredMessage(WebConstants.PropertyKeys.Auction.REQUEST_START_DATE));
				validated = false;
				 //return validated;
			}			
			else
			{
				//TODO:Commented for UAT Need to open for production
//				if(startDate.before(todayDate))
//				{
//					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_REQUEST_START_DATE_INVALID));
//					validated = false;
//				}				
			}			
			
			if( !auctionReqEndDateTimeString.equals("") && !auctionReqStartDateTimeString.equals("")  && endDate.before(startDate))
			{
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.REQUEST_DATES_INVALID));
				validated = false;
				//return validated;
			}
			auctionDateVal = auctionView.getAuctionDateVal();
			requestEndDate = auctionView.getRequestEndDate();
			
			if(auctionDateVal!=null && requestEndDate!=null && auctionDateVal.before(requestEndDate))
			{				
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.AUCTION_DATE_INVALID));
				validated = false;
				//return validated;
			}
			
			
			
			/*try{
				hhEndVal = Integer.parseInt(hhEnd);
				mmEndVal = Integer.parseInt(mmEnd);
			}
			catch(NumberFormatException numberFormatException){
				 logger.LogException("Invalid End Time... (Problem in parsing end time)", numberFormatException);
				 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.END_TIME));
				 validated = false;
				 return validated;
			}*/
			
			/*Long timeStartVal = Convert.toLong(hhStartVal*60 + mmStartVal);
			Long timeEndVal = Convert.toLong(hhEndVal*60 + mmEndVal);
			
			if(timeEndVal<timeStartVal)
			{
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.TIME_INVALID));
				validated = false;
				return validated;
			}*/
			
			if(validated){
				auctionView.setAuctionStartTime(hhStart+":"+mmStart);
				auctionView.setAuctionEndTime("23:00");
	//			auctionView.setAuctionEndTime(hhEnd+":"+mmEnd);
			}
			logger.logInfo("validateAuctionDetailsTab() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateAuctionDetailsTab() crashed ", exception);
		}
		return validated;
    }
    
    @SuppressWarnings("unchecked")
	public Boolean fieldsValidated() {
	    logger.logInfo("fieldsValidated() started...");
	    Boolean validated = true;
	    Boolean unitsTabValidated = true;
	    Boolean detailsTabValidated = true;
	    Boolean documentsTabValidated = true;
		fillAuctionView();
		
	    try
		{
	    	errorMessages.clear();
	    	String currentTab = (String)tabPanel.getSelectedTab();	    		    	
	    	
	    	detailsTabValidated = validateAuctionDetailsTab();
	    	unitsTabValidated =  validateUnitsTab();	    	
	    	documentsTabValidated = validateMandatoryDocuments();
	    	
	    	if(currentTab!=null && currentTab.equals(TAB_AUCTION_DETAILS)){
	    		if(detailsTabValidated)
	    		{
	    			if(!unitsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_AUCTION_UNITS);
	    			}
	    			else if(!documentsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_ATTACHMENTS);
	    			}
	    		}							
	    	}
	    	else if(currentTab!=null && currentTab.equals(TAB_AUCTION_UNITS)){	    		
	    		
	    		if(unitsTabValidated)
	    		{
	    			if(!detailsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_AUCTION_DETAILS);
	    			}
	    			else if(!documentsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_ATTACHMENTS);
	    			}
	    		}				
	    	}
	    	else if(currentTab!=null && currentTab.equals(TAB_ATTACHMENTS)){	    		
				
	    		if(documentsTabValidated)
	    		{
	    			if(!detailsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_AUCTION_DETAILS);
	    			}
	    			else if(!unitsTabValidated)
	    			{
	    				tabPanel.setSelectedTab(TAB_AUCTION_UNITS);
	    			}
	    		}				
	    	}
	    	else
	    	{
	    		if(!detailsTabValidated)
    			{
    				tabPanel.setSelectedTab(TAB_AUCTION_DETAILS);
    			}
    			else if(!unitsTabValidated)
    			{
    				tabPanel.setSelectedTab(TAB_AUCTION_UNITS);
    			}
    			else if(!documentsTabValidated)
    			{
    				tabPanel.setSelectedTab(TAB_ATTACHMENTS);
    			}
	    		
	    	}	    	

	    	validated = unitsTabValidated && detailsTabValidated && documentsTabValidated;   	
	    	
			logger.logInfo("fieldsValidated() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("fieldsValidated() crashed ", exception);
			validated = false;		
		}
		return validated;
	}
	
    private Boolean validateMandatoryDocuments()
    {
    	Boolean validated = true;
    	try
		{
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{	    		
	    		errorMessages.add(getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;				
	    	}    	
		}
    	catch (Exception exception) {
			logger.LogException("fieldsValidated() crashed ", exception);
			validated = false;		
		}
    	return validated;
    }
	public Boolean isInvalidDate(String dateString){
		logger.logInfo("isInvalidDate() started...");
		Boolean invalid = false;
		try
		{
			
	        logger.logInfo("isInvalidDate() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("isInvalidDate() crashed ", exception);
		}
		return invalid;
	}
	
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			checkAuctionAvailability();
			checkAdvertisementAvailability();
			
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void selectVenue(javax.faces.event.ActionEvent event){
		logger.logInfo("selectVenue() started...");
		try
		{
	        final String viewId = "/auctionDetails.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String javaScriptText = "javascript:showVenuePopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("selectVenue() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("selectVenue() crashed ", exception);
		}
    }
	
	
	@SuppressWarnings("unchecked")
	public void openUnitPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openUnitPopup() started...");
		try
		{
			List<AuctionUnitView> allUnits = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.ALREADY_SELECTED_AUCTION_UNITS, allUnits);			
			
	        final String viewId = "/auctionDetails.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	        String javaScriptText = "javascript:showUnitPopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openUnitPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openUnitPopup() crashed ", exception);
		}
    }
	
	/*@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}*/
	
	@SuppressWarnings("unchecked")
	private String getLoggedInUser()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = "";
		if(user!=null)
			{
				loginId = user.getLoginId();
				if( user.getEmailUrl() !=null && user.getEmailUrl().trim().length()> 0 )
				viewMap.put("loggedInUserEmail",user.getEmailUrl());
			}
		return loginId;
	}
	
	
	@SuppressWarnings("unchecked")
	public void checkAuctionAvailability(){
		logger.logInfo("checkAuctionAvailability() started...");
		DateFormat formatter = new SimpleDateFormat(getDateFormat());
		try{
//				if(!viewMap.containsKey("auctionLoaded"))
//				{
//					Long auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
//					auctionView = propertyServiceAgent.getAuctionById(auctionId);
					auctionView = getAuctionView();
					
					if(auctionView.getAuctionNumber()!=null)
					{
						if(getAuctionNumber()==null || !getAuctionNumber().equals(auctionView.getAuctionNumber()))
							setAuctionNumber(auctionView.getAuctionNumber());
					}
					
					if(auctionView.getAuctionStatus()!=null)
					{
						if(getAuctionStatus()==null || !getAuctionStatus().equals(auctionView.getAuctionStatus()))
							setAuctionStatus(auctionView.getAuctionStatus());
					}
					
					if(auctionView.getAuctionTitle()!=null)
					{
						if(getAuctionDescription()==null || !getAuctionDescription().equals(auctionView.getAuctionTitle()))
							setAuctionDescription(auctionView.getAuctionTitle());
					}
					
					if(auctionView.getOutbiddingValue()!=null)
					{
						if(getOutBiddingValue()==null || !getOutBiddingValue().equals(auctionView.getOutbiddingValue()))
							setOutBiddingValue(auctionView.getOutbiddingValue());
					}
					
					if(auctionView.getConfiscationPercent()!=null)
					{
						if(getConfiscationPercent()==null || !getConfiscationPercent().equals(auctionView.getConfiscationPercent()))
							setConfiscationPercent(auctionView.getConfiscationPercent());
					}
					
					if(auctionView.getAuctionVenue()!=null && auctionView.getAuctionVenue().getVenueName()!=null)
					{
						if(getAuctionVenue()==null || !getAuctionVenue().equals(auctionView.getAuctionVenue().getVenueName())){
							setAuctionVenue(auctionView.getAuctionVenue().getVenueName());
						}
					}
					
					if(auctionView.getAuctionDateVal()!=null){
						if(getAuctionDate()==null || !getAuctionDate().equals(auctionView.getAuctionDateVal()))
							setAuctionDate(auctionView.getAuctionDateVal());
					}
					
					if(auctionView.getAuctionAdvertisement()!=null && auctionView.getAuctionAdvertisement().getAdvertisementDate()!=null)
					{
						viewMap.put(ADVERTISEMENT_VIEW,auctionView.getAuctionAdvertisement());
						if(getAdvertisementDate()==null || !getAdvertisementDate().equals(auctionView.getAuctionAdvertisement().getAdvertisementDate())){
							setAdvertisementDate(auctionView.getAuctionAdvertisement().getAdvertisementDate());
						}
					}
					
					if(auctionView.getRequestStartDate()!=null)
					{
						if(getRequestStartDate()==null || !getRequestStartDate().equals(auctionView.getRequestStartDate())){
							setRequestStartDate(auctionView.getRequestStartDate());
							auctionReqStartDateTimeString = formatter.format(auctionView.getRequestStartDate());
						}
					}
					if(auctionView.getRequestEndDate()!=null){
						if(getRequestEndDate()==null || !getRequestEndDate().equals(auctionView.getRequestEndDate())){
							setRequestEndDate(auctionView.getRequestEndDate());
							auctionReqEndDateTimeString = formatter.format(auctionView.getRequestEndDate());
						}
					}
					
					if(auctionView.getAuctionStartTime()!=null){
						String startTime = auctionView.getAuctionStartTime();
						if((startTime!=null) && !startTime.equals(":")){
							StringTokenizer st = new StringTokenizer(startTime,":");
							if(st.hasMoreTokens())
								setHhStart(st.nextToken());
							if(st.hasMoreTokens())
								setMmStart(st.nextToken());
						}
					}
					if(auctionView.getAuctionEndTime()!=null){
						String endTime = auctionView.getAuctionEndTime();
						if((endTime!=null) && !endTime.equals(":")){
							StringTokenizer st = new StringTokenizer(endTime,":");
						
							if(st.hasMoreTokens())
								hhEnd= st.nextToken();
							if(st.hasMoreTokens())
								mmEnd= st.nextToken();
						}
					}
//					viewMap.put(AUCTION_VIEW, auctionView);
//				}	
				
				if(!viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
				{
					if(auctionView.getAuctionUnits()!=null)
						{
						 //viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,auctionView.getAuctionUnits());
						 PersistAuctionUnitsInView(auctionView.getAuctionUnits());
						}
				}
				
				if(viewMap.containsKey(WebConstants.AUCTION_VENUE_ID))
				 {
					 AuctionVenueView auctionVenue = (AuctionVenueView)viewMap.get("auctionVenueView");
					 Long auctionVenueId = Convert.toLong(viewMap.get(WebConstants.AUCTION_VENUE_ID));
					// if(auctionVenue==null)
					 {
						 auctionVenue = propertyServiceAgent.getAuctionVenueById(auctionVenueId);
						 viewMap.put("auctionVenueView",auctionVenue);
						 setAuctionVenue(auctionVenue.getVenueName());
					 }
					 auctionView.setAuctionVenue(auctionVenue);
				 }
				
				logger.logInfo("checkAuctionAvailability() completed successfully...");
			}
			catch (Exception exception) {
				logger.LogException("checkAuctionAvailability() crashed ", exception);
			}
		}
	
	public void PersistAuctionUnitsInView(List<AuctionUnitView> auctionUnits)
	{
		viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,auctionUnits);
		if(auctionUnits != null)
		 {
			 	recordSize = auctionUnits.size();
			 
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize/paginatorRows;
				if((recordSize%paginatorRows)>0)
					paginatorMaxPages++;
				if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewMap.put("paginatorMaxPages", paginatorMaxPages);
		 }
	}
	public void checkAdvertisementAvailability(){
		logger.logInfo("checkAdvertisementAvailability() started...");
		DateFormat formatter = new SimpleDateFormat(WebConstants.DATE_FORMAT);
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			if (viewMap.containsKey(ADVERTISEMENT_VIEW))
			{
				advertisementView = (AuctionAdvertisementView) viewMap.get(ADVERTISEMENT_VIEW);
			}
			if(advertisementView.getAdvertisementDate()!=null){
				advertisementDateString = formatter.format(advertisementView.getAdvertisementDate());
				viewMap.put("advertisementDate",advertisementView.getAdvertisementDate());
			}
				logger.logInfo("checkAdvertisementAvailability() completed successfully...");
			}
			catch (Exception exception) {
				logger.LogException("checkAdvertisementAvailability() crashed ", exception);
			}
		}
	
	/*
	 * JSF Lifecycle methods
	 */
	@Override
	@SuppressWarnings("unchecked")
	
	/*public void init(){
		UtilityServiceAgent utilServiceAgent = new UtilityServiceAgent();
		String temp;
		try {
			temp = utilServiceAgent.getProcedureTaskGroups("PREPARE_FOR_AUCTION", "APPROVE_AUCTION");
			System.out.println(temp);
			} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PropertyServiceAgent utilServiceAgent = new PropertyServiceAgent();
		Boolean temp;
		try {
			temp = utilServiceAgent.setAuctionAsReadyForApproval(36L, "admin");
			System.out.println(temp);
			} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
	
	
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
			setMode();
			setValues();
			
			if(!isPostBack())
			{
//				populateTabMaps();
//				setFeeConfig();
				loadAttachmentsAndComments();
			}

//			if(viewMap.containsKey("FileListData"))
//				System.out.println();
//			viewMap.remove("FileListData");
//			loadAttachmentsAndComments();
			
//			AttachmentController ac = new AttachmentController();
//			List<AttachmentBean> temp = ac.getFilesList();
//			for(AttachmentBean attachmentBean: temp){
//				System.out.println(attachmentBean.getFileName());
//				Document doc = attachmentBean.getDoc();
//				System.out.println(doc.getDocumentId());
//			}
			
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}
	
	
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			String mode = (String)getFacesContext().getExternalContext().getRequestParameterMap().get("mode");
			///
//			UserTask temp = new UserTask();
//			Map<String,String> taskAtr = new HashMap<String, String>();
//			taskAtr.put(WebConstants.UserTasks.AUCTION_ID,36L+"");
//			temp.setTaskAttributes(taskAtr);
//			temp.setTaskType(WebConstants.UserTasks.PublishAuction.TASK_TYPE_NAME);
//			sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,temp);
//			setRequestParam(WebConstants.BACK_SCREEN,WebConstants.BACK_SCREEN_TASK_LIST);
//			setRequestParam(WebConstants.Contract.CONTRACT_VIEW,null);
//			mode = "create";
//			sessionMap.put(WebConstants.FROM_SEARCH,"hello");
//			sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
			///
			
			// create new auction
			if(mode!=null && mode.equals("create"))
			{
				clearAll();
				viewMap.put(PAGE_MODE, PAGE_MODE_NEW);
				viewMap.put("readOnlyMode",false);
				viewMap.put("asterisk", "*");
				viewMap.put(SHOW_CANCEL_BUTTON, false);
				UtilityServiceAgent usa = new UtilityServiceAgent();
				DomainDataView ddv = usa.getDomainDataByValue(WebConstants.AUCTION_STATUS, WebConstants.AUCTION_DRAFT_STATUS);
				if(ddv!=null){
					if(getIsEnglishLocale())
						setAuctionStatus(ddv.getDataDescEn());
					else
						setAuctionStatus(ddv.getDataDescAr());
				}
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
			}
			
			// from auction search screen
			else if(sessionMap.containsKey(WebConstants.FROM_SEARCH))
			{
				viewMap.put(SHOW_CANCEL_BUTTON, true);
				
				Long auctionId = (Long)sessionMap.get(WebConstants.AUCTION_ID);
				if(auctionId!=null){
					viewMap.put(WebConstants.AUCTION_ID, auctionId);
					auctionView = propertyServiceAgent.getAuctionById(auctionId);
					setAuctionView(auctionView);
					AuctionVenueView auctionVenueView = auctionView.getAuctionVenue();
					if(auctionVenueView!=null && auctionVenueView.getVenueId()!=null)
						viewMap.put(WebConstants.AUCTION_VENUE_ID,auctionVenueView.getVenueId());
				}
				if(sessionMap.get(WebConstants.VIEW_MODE)!=null){
					String viewMode = sessionMap.get(WebConstants.VIEW_MODE).toString();
					if(viewMode.equals(WebConstants.EDIT_MODE)){
						pageMode = PAGE_MODE_DRAFT;
						viewMap.put("asterisk", "*");
						viewMap.put("canAddAttachment",true);
						viewMap.put("canAddNote", true);
					}
					else{
						pageMode = PAGE_MODE_VIEW;
						viewMap.put("asterisk", "");
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
					}
					
					viewMap.put(PAGE_MODE,pageMode);
					sessionMap.remove(WebConstants.VIEW_MODE);
				}
				sessionMap.remove(WebConstants.FROM_SEARCH);
			}
			// FROM TASK LIST
			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
				viewMap.put(SHOW_CANCEL_BUTTON, true);
				checkTask();
				viewMap.put("readOnlyMode",true);
				viewMap.put("asterisk", "");
			}
//			else{
//				clearAll();
//				viewMap.put(PAGE_MODE, PAGE_MODE_VIEW);
//			}
			
			/*if(viewMap.containsKey(AUCTION_VIEW)){
				AuctionView auction = (AuctionView)viewMap.get(AUCTION_VIEW);
				
				Long auctionId = auction.getAuctionId();
				if(auctionId!=null)
					viewMap.put(WebConstants.AUCTION_ID,auctionId);
			}*/
			
			// auction venue selected from popup
			if(sessionMap.containsKey(WebConstants.AUCTION_VENUE_ID)){
				Long venueId = (Long)sessionMap.get(WebConstants.AUCTION_VENUE_ID);
				if(venueId!=null)
				{
					viewMap.put(WebConstants.AUCTION_VENUE_ID,venueId);
					viewMap.put("auctionVenue", sessionMap.get(WebConstants.AUCTION_VENUE_NAME).toString());
				}
				sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
				sessionMap.remove(WebConstants.AUCTION_VENUE_NAME);
			}
			
			// new auction units selected from popup			
			if(sessionMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS)){
				List<AuctionUnitView> allSelectedUnitList = (List<AuctionUnitView>)sessionMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
				List<AuctionUnitView> oldAllSelectedUnitList = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
				if(oldAllSelectedUnitList==null)
					oldAllSelectedUnitList= new ArrayList<AuctionUnitView>();
				if(allSelectedUnitList!=null){
					oldAllSelectedUnitList.addAll(allSelectedUnitList);
					setAllUnitsAsUnSelected(oldAllSelectedUnitList);
					//viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,oldAllSelectedUnitList);
					PersistAuctionUnitsInView(oldAllSelectedUnitList);
				}
				sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			}	

			// after deposit / opening price is set from respective popups
			if(sessionMap.containsKey(WebConstants.CHOSEN_AUCTION_UNITS)){
				List<AuctionUnitView> modifiedAuctionUnits = (List<AuctionUnitView>)sessionMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
				List<AuctionUnitView> oldAllSelectedUnitList = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
				
				String popupMode = (String)sessionMap.get("POPUP_MODE");
				if(popupMode!=null){
						
					for(AuctionUnitView auctionUnit: modifiedAuctionUnits)
					{
						for(AuctionUnitView oldAuctionUnit: oldAllSelectedUnitList){
							if(auctionUnit.getUnitId().compareTo(oldAuctionUnit.getUnitId())==0)
							{
								if(popupMode.equals(WebConstants.DEPOSIT_AMOUNT))
									oldAuctionUnit.setDepositAmount(auctionUnit.getDepositAmount());
								else if(popupMode.equals(WebConstants.OPENINIG_PRICE))
									oldAuctionUnit.setOpeningPrice(auctionUnit.getOpeningPrice());
								else{
									oldAuctionUnit.setDepositAmount(auctionUnit.getDepositAmount());
									oldAuctionUnit.setOpeningPrice(auctionUnit.getOpeningPrice());
								}
							}
						}
					}
					//viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,oldAllSelectedUnitList);
					PersistAuctionUnitsInView(oldAllSelectedUnitList);
				}
				
				sessionMap.remove(WebConstants.CHOSEN_AUCTION_UNITS);
				sessionMap.remove("POPUP_MODE");
			}	
			
			if(getRequestParam(WebConstants.BACK_SCREEN)!=null)
			{
				String backScreen = (String) getRequestParam(WebConstants.BACK_SCREEN);
				viewMap.put(WebConstants.BACK_SCREEN, backScreen);
				setRequestParam(WebConstants.BACK_SCREEN,null);
			}	
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void setAllUnitsAsUnSelected(List<AuctionUnitView> oldAllSelectedUnitList){
		for(AuctionUnitView auctionUnitView:oldAllSelectedUnitList){
			auctionUnitView.setSelected(false);
		}
	}
	
	public void setDates(){
		viewMap.put("advertisementDate",advertisementView.getAdvertisementDate());
		viewMap.put("requestStartDate",auctionView.getRequestStartDate());
		viewMap.put("requestEndDate",auctionView.getRequestEndDate());
	}
	
	public String getDatesOnTabChange(){
		advertisementView.setAdvertisementDate((Date)viewMap.get("advertisementDate"));
		auctionView.setRequestStartDate((Date)viewMap.get("requestStartDate"));
		auctionView.setRequestEndDate((Date)viewMap.get("requestEndDate"));
		return "";
	}
	
	public Number getNumberValue(String doubleStr, String pattern){
		Number numberVal = null;
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return numberVal;
	}
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	public Date getDateFromString(String dateStr){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		Date dateVal = null;
		try{
			if(dateStr.length()==pattern.length())
				dateVal = formatter.parse(dateStr);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateVal;
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	
	@SuppressWarnings("unchecked")
	public void setFeeConfig(){
		logger.logInfo("setFeeConfig() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		String maxAmount = "0.00";
		String decimalFormat = getNumberFormat();
		try{
			FeeConfigurationView feeConfig = propertyServiceAgent.getAuctionDepositFeeConfig();
			maxAmount = getFormattedDoubleString(feeConfig.getMaxAmount(),decimalFormat);
			sessionMap.put(WebConstants.MAX_DEPOSIT_AMOUNT, maxAmount);
			logger.logInfo("setFeeConfig() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setFeeConfig() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_PREPARE_AUCTION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_AUCTION;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.AUCTION);
		
		if(viewMap.containsKey(WebConstants.AUCTION_ID)){
			Long auctionId = (Long) viewMap.get(WebConstants.AUCTION_ID);
	    	String entityId = auctionId.toString();
			
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
	/*
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		UserTask task = null;
		Long auctionId = 107L;
		Long auctionAdvertsiementId = 36L;
		try{
				sessionMap.put(WebConstants.AUCTION_ID, auctionId);
				
//				sessionMap.put(WebConstants.ADVERTISEMENT_ID, auctionAdvertsiementId);
				
				sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
				sessionMap.put(WebConstants.ADVERTISEMENT_MODE,WebConstants.ADVERTISEMENT_MODE);
				sessionMap.put(WebConstants.ADVERTISEMENT_ACTION,WebConstants.ADVERTISEMENT_DRAFT_MODE);
			
				
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	*/
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long auctionId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(WebConstants.PrepareAuction.TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.containsKey(WebConstants.UserTasks.AUCTION_ID))
				{
					auctionId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.AUCTION_ID));
					viewMap.put(WebConstants.AUCTION_ID, auctionId);
					auctionView = propertyServiceAgent.getAuctionById(auctionId);
					setAuctionView(auctionView);
				}
				
				/*if(taskAttributes.containsKey(WebConstants.UserTasks.ADVERTISEMENT_ID))
				{
					auctionAdvertisementId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.ADVERTISEMENT_ID));
					viewMap.put(WebConstants.ADVERTISEMENT_ID, auctionAdvertisementId);
				}*/
				
				String taskType = userTask.getTaskType();
				if(taskType.equals(WebConstants.UserTasks.ApproveAuction.TASK_TYPE_NAME))
				{
					pageMode = PAGE_MODE_APPROVE;
				}
				else if(taskType.equals(WebConstants.UserTasks.PublishAuction.TASK_TYPE_NAME))
				{
					pageMode = PAGE_MODE_PUBLISH;
				}
				
				viewMap.put(PAGE_MODE, pageMode);
				
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
				
				viewMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			sessionMap.remove(WebConstants.ROW_ID);
//			sessionMap.remove(WebConstants.VIEW_MODE);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.SELECT_VENUE_MODE);
//			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
//			sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
			sessionMap.remove(WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.ALREADY_SELECTED_AUCTION_UNITS);
//			sessionMap.remove(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void clearAll(){
		logger.logInfo("clearAll() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			auctionView = new AuctionView();
			advertisementView = new AuctionAdvertisementView();
			
			sessionMap.remove(WebConstants.ROW_ID);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.MAX_DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
			sessionMap.remove(WebConstants.FROM_SEARCH);
			
			logger.logInfo("clearAll() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearAll() crashed ", exception);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
//		setTabs();
		applyMode();
//		tabPanel.setSelectedTab(TAB_COMMENTS);
//		tabPanel.setSelectedTab(TAB_AUCTION_UNITS);
	}

	private void setFieldsReadonly(Boolean readonly){
		auctionNumberField.setReadonly(readonly);
		auctionStatusField.setReadonly(readonly);
		auctionDescriptionField.setReadonly(readonly);
		auctionVenueField.setReadonly(readonly);
		outbiddingValueField.setReadonly(readonly);
		confiscationPercentField.setReadonly(readonly);
		gracePeriodField.setReadonly(readonly);
		hhStartField.setReadonly(readonly);
		mmStartField.setReadonly(readonly);
		auctionDateField.setDisabled(readonly);
		publicationDateField.setDisabled(readonly);
		registrationStartDateField.setDisabled(readonly);
		registrationEndDateField.setDisabled(readonly);
		mandatoryLabel.setRendered(!readonly);
		selectColumn.setRendered(!readonly);
		actionColumn.setRendered(!readonly);
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			Boolean showCancelButton = (Boolean)viewMap.get(SHOW_CANCEL_BUTTON);
			if(showCancelButton==null)
				showCancelButton = false;
			cancelButton.setRendered(showCancelButton);
			
			pageMode = (String)viewMap.get(PAGE_MODE);
			if(pageMode!=null)
			{
				logger.logInfo("Page Mode : %s",pageMode);
				
				if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW))
				{
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.CREATE_HEADING);
					setFieldsReadonly(false);
					applyDepositButton.setRendered(true);
					applyOpeningPriceButton.setRendered(true);
					selectVenueButton.setRendered(true);
					auctionDetailsSaveButton.setRendered(true);
					auctionDetailsCancelButton.setRendered(false);
//					auctionUnitsSaveButton.setRendered(true);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(true);
					selectColumn.setRendered(true); 
					addAuctionUnitsButton.setRendered(true);
					readonlyStyleClass = "";
					numericReadonlyStyleClass = "A_RIGHT_NUM";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_DRAFT))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.CREATE_HEADING);
					setFieldsReadonly(false);
					applyDepositButton.setRendered(true);
					applyOpeningPriceButton.setRendered(true);
					selectVenueButton.setRendered(true);
					auctionDetailsSaveButton.setRendered(true);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(true);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(true);
					selectColumn.setRendered(true); 
					addAuctionUnitsButton.setRendered(true);
					readonlyStyleClass = "";
					numericReadonlyStyleClass = "A_RIGHT_NUM";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_DRAFT_DONE))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.CREATE_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_APPROVE))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.APPROVE_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(true);
					rejectButton.setRendered(true);
					publishButton.setRendered(false);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_APPROVE_DONE))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.APPROVE_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_PUBLISH))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.PUBLISH_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(true);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_PUBLISH_DONE))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.PUBLISH_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_VIEW))
				{	
					heading = getBundleMessage(WebConstants.PropertyKeys.Auction.VIEW_HEADING);
					setFieldsReadonly(true);
					applyDepositButton.setRendered(false);
					applyOpeningPriceButton.setRendered(false);
					selectVenueButton.setRendered(false);
					auctionDetailsSaveButton.setRendered(false);
					auctionDetailsCancelButton.setRendered(true);
//					auctionUnitsSaveButton.setRendered(false);
//					auctionUnitsCancelButton.setRendered(true);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					publishButton.setRendered(false);
					submitButton.setRendered(false);
					selectColumn.setRendered(false); 
					addAuctionUnitsButton.setRendered(false);
					readonlyStyleClass = "READONLY";
					numericReadonlyStyleClass = "A_RIGHT_NUM_READONLY";
				}
			}
			else{
				viewMap.put(PAGE_MODE,PAGE_MODE_VIEW);
			}
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	private void setTabs(){
		String nextTab = (String)viewMap.get(NEXT_TAB);
		String backTab = (String)viewMap.get(BACK_TAB);
		if(viewMap.get(NEXT_CLICK)!=null && nextTab!=null)
			tabPanel.setSelectedTab(nextTab);
		if(viewMap.get(BACK_CLICK)!=null && backTab!=null)
			tabPanel.setSelectedTab(backTab);
		viewMap.remove(BACK_CLICK);
		viewMap.remove(NEXT_CLICK);
	}
	
	/*
	 * Button Click Action Handlers
	 */

	@SuppressWarnings("unchecked")
	public String saveAdvertisement() {
		logger.logInfo("saveAdvertisement() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		Long auctionId = 0L;
		Boolean firstTime = false;
		String success = "false";
		try {
			if(advertisementFieldsValidated()){
				
				 if(advertisementView.getAuctionAdvertisementId()==null)
				 {
					 advertisementView.setCreatedBy(getLoggedInUser());
					 advertisementView.setCreatedOn(new Date());
					 advertisementView.setUpdatedBy(getLoggedInUser());
					 advertisementView.setUpdatedOn(new Date());
					 advertisementView.setRecordStatus(1L);
					 advertisementView.setIsDeleted(0L);
					 firstTime = true;
				 }
				 else{
					 advertisementView.setUpdatedBy(getLoggedInUser());
					 advertisementView.setUpdatedOn(new Date());
					 firstTime = false;
				 }
//				 String dateFormat = getDateFormat();
//				 DateFormat formatter = new SimpleDateFormat(dateFormat);
//				 advertisementView.setAdvertisementDate(getDateFromString(advertisementDateString));
//				 auctionView.setRequestStartDate(getDateFromString(auctionReqStartDateTimeString));
//				 auctionView.setRequestEndDate(getDateFromString(auctionReqEndDateTimeString));
				 //advertisementView.setAuctionView(auctionView);
				 advertisementView.setAuctionId(auctionView.getAuctionId());
				 if(viewMap.containsKey(WebConstants.AUCTION_ID))			
						auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
				 advertisementView = propertyServiceAgent.addAuctionAdvertisement(auctionId, advertisementView);
				 Long auctionAdvertisementId = advertisementView.getAuctionAdvertisementId();
				if(auctionAdvertisementId<0){
					errorMessages.clear();
					String msg = getBundleMessage(WebConstants.PropertyKeys.Advertisement.Messages.ADVERTISEMENT_CREATE_ERROR);
					errorMessages.add(msg);
				}
				else{	
					Boolean attachSuccess = saveAttachments(auctionId);
					Boolean commentSuccess = saveComments(auctionId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
					if(success.equals("true"))
					{
						UserTask userTask = null;
						sessionMap.put(WebConstants.ADVERTISEMENT_ID,auctionAdvertisementId);
						String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
				        logger.logInfo("Contextpath is: " + contextPath);
				        String loggedInUser = getLoggedInUser();
				        if(viewMap.containsKey(WebConstants.PrepareAuction.TASK_LIST_USER_TASK))	
							userTask = (UserTask) viewMap.get(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
						BPMWorklistClient client = new BPMWorklistClient(contextPath);
						
						if(userTask!=null){
							logger.logInfo("UserTask is: " + userTask.getTaskType());
							List<String> comments = userTask.getTaskComments();
							if(comments!=null)
								if(comments.isEmpty() && !sessionMap.containsKey("commentsAdded"))
								{
									userTask.getTaskComments().add(auctionAdvertisementId.toString());
									client.addTaskComment(userTask, loggedInUser, auctionAdvertisementId.toString());
									viewMap.put(WebConstants.PrepareAuction.TASK_LIST_USER_TASK,userTask);
									sessionMap.put("commentsAdded", "commentsAdded");
								}
						}
						
						String msg = "";
						errorMessages.clear();
						infoMessage="";
						if(firstTime)
							msg = getBundleMessage(WebConstants.PropertyKeys.Advertisement.Messages.ADVERTISEMENT_CREATE_SUCCESS);
						else
							msg = getBundleMessage(WebConstants.PropertyKeys.Advertisement.Messages.ADVERTISEMENT_SAVE_SUCCESS);
	
						infoMessage=msg;
						canCompleteTask = true;
						sessionMap.put(WebConstants.CAN_COMPLETE_TASK,WebConstants.CAN_COMPLETE_TASK);
					}
			   }
			}
			else{
				success = "validation";
			}
			logger.logInfo("saveAdvertisement() completed successfully!!!");
		}
		catch(PIMSWorkListException exception){
			 logger.LogException("saveAdvertisement() crashed ",exception);
		}
		catch (Exception exception) {
			logger.LogException("saveAdvertisement() crashed ", exception);
		}
		return success;
	}
	

	@SuppressWarnings("unchecked")
	public String approveAuction() {
		logger.logInfo("approveAuction() started...");
		UserTask userTask = null;
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		String success = "false";
		Long auctionId = 0L;
		try {
				if(viewMap.containsKey(WebConstants.AUCTION_ID))			
					auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
				Boolean attachSuccess = saveAttachments(auctionId);
				Boolean commentSuccess = saveComments(auctionId);
				
				if(attachSuccess && commentSuccess)
					success = "true";
		        
				if(success.equals("true"))
				{
					String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
			        logger.logInfo("Contextpath is: " + contextPath);
			        String loggedInUser = getLoggedInUser();
			        if(viewMap.containsKey(WebConstants.PrepareAuction.TASK_LIST_USER_TASK))	
						userTask = (UserTask) viewMap.get(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
					BPMWorklistClient client = new BPMWorklistClient(contextPath);
					logger.logInfo("UserTask is: " + userTask.getTaskType());
					if(userTask!=null){
//						client.addTaskComment(userTask, loggedInUser, auctionAdvertisementId.toString());
						client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
						saveSystemComments(MessageConstants.RequestEvents.AUCTION_APPROVED);
						
						Boolean notificationSuccess = generateNotification(NOTIFICATION_EVENT_PUBLISH_ADVERTISEMENT);
						errorMessages.clear();
						if(!notificationSuccess)
						{
							errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PrepareAuction.MSG_NOTFICATION_FAILURE));
						}
					}
					
					errorMessages.clear();
					sendNotification();
					infoMessage = getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_APPROVE_SUCCESS);
					clearSessionMap();
					viewMap.put(PAGE_MODE, PAGE_MODE_APPROVE_DONE);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
					setAsterisk("");
				}
				else{
					errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_APPROVE_FAILURE));
				}
			/*}*/
			logger.logInfo("approveAuction() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("approveAuction() crashed ", exception);
		}
		return "approveAuction";
	}

	@SuppressWarnings("unchecked")
	public String rejectAuction() {
		logger.logInfo("rejectAuction() started...");
		UserTask userTask = null;
		String success = "false";
		Long auctionId = 0L;
		try {
			if(viewMap.containsKey(WebConstants.AUCTION_ID))			
				auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
			Boolean attachSuccess = saveAttachments(auctionId);
			Boolean commentSuccess = saveComments(auctionId);
			
			if(attachSuccess && commentSuccess)
				success = "true";
	        
			if(success.equals("true"))
			{
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = getLoggedInUser();
		        if(viewMap.containsKey(WebConstants.PrepareAuction.TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				 logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
//					client.addTaskComment(userTask, loggedInUser, auctionAdvertisementId.toString());
					client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);
					saveSystemComments(MessageConstants.RequestEvents.AUCTION_REJECTED);
				}
				clearSessionMap();
				errorMessages.clear();
				infoMessage = getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_REJECT_SUCCESS);
				viewMap.put(PAGE_MODE, PAGE_MODE_APPROVE_DONE);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				setAsterisk("");
			}
			else{
				errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_REJECT_FAILURE));
			}
			logger.logInfo("rejectAuction() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("rejectAuction() crashed ", exception);
		}
		return "rejectAuction";
	}

	@SuppressWarnings("unchecked")
	public String requestForPublication() {
		logger.logInfo("requestForPublication() started...");
		Long auctionAdvertisementId=-2L;
		UserTask userTask = null;
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		String success = "false";
		Long auctionId = 0L;
		try {
			if(viewMap.containsKey(WebConstants.AUCTION_ID))			
				auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
			if(viewMap.containsKey(WebConstants.ADVERTISEMENT_ID))			
				auctionAdvertisementId = (Long)viewMap.get(WebConstants.ADVERTISEMENT_ID);
			
				if(canRequestForPublication)
				{
					String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
			        logger.logInfo("Contextpath is: " + contextPath);
			        String loggedInUser = getLoggedInUser();
			        
			        Boolean attachSuccess = saveAttachments(auctionId);
					Boolean commentSuccess = saveComments(auctionId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
			        
					if(success.equals("true"))
					{
			        
				        if(viewMap.containsKey(WebConstants.PrepareAuction.TASK_LIST_USER_TASK))	
							userTask = (UserTask) viewMap.get(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
						BPMWorklistClient client = new BPMWorklistClient(contextPath);
						 logger.logInfo("UserTask is: " + userTask.getTaskType());
						if(userTask!=null){
	//						client.addTaskComment(userTask, loggedInUser, auctionAdvertisementId.toString());
							client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
						}
						
						errorMessages.clear();
						infoMessage="";
						infoMessage = getBundleMessage(WebConstants.PropertyKeys.Advertisement.Messages.ADVERTISEMENT_REQUEST_PUBLICATION_TASK_COMPLETE);
						taskCompleted = true;
						viewMap.put(WebConstants.TASK_COMPLETED,taskCompleted);
						clearSessionMap();
						
						sessionMap.put(WebConstants.FULL_VIEW_MODE,WebConstants.FULL_VIEW_MODE);
						sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
						init();
					}
					else{
						errorMessages.clear();
						errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Advertisement.Messages.ADVERTISEMENT_PUBLISH_TASK_CANNOT_COMPLETE));
					}
				}

			logger.logInfo("requestForPublication() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("requestForPublication() crashed ", exception);
		}
		return "requestForPublication";
	}
	public void printAdvertisment()
	{
		AdvertisementCriteria advertisementCriteira;
		Long auctionId=null;
		if(viewMap.containsKey(WebConstants.AUCTION_ID))			
			auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
	
		if(getIsEnglishLocale())
			advertisementCriteira=new AdvertisementCriteria(ReportConstant.Report.ADVERTISEMENT_REPORT_EN, ReportConstant.Processor.ADVERTISEMENT_REPORT,CommonUtil.getLoggedInUser());
		else
			advertisementCriteira=new AdvertisementCriteria(ReportConstant.Report.ADVERTISEMENT_REPORT_AR, ReportConstant.Processor.ADVERTISEMENT_REPORT,CommonUtil.getLoggedInUser());
		advertisementCriteira.setAuctionId(auctionId);
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY,advertisementCriteira);
		String javaScript = "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');";
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript);			
		
	}
	@SuppressWarnings("unchecked")
	public String publishAuction() {
		logger.logInfo("publishAuction() started...");
		Long auctionId=0L;
		UserTask userTask = null;
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		String success = "false";
		try {
				if(viewMap.containsKey(WebConstants.AUCTION_ID))			
					auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
			
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = getLoggedInUser();
		        
		        Boolean attachSuccess = saveAttachments(auctionId);
				Boolean commentSuccess = saveComments(auctionId);
				
				if(attachSuccess && commentSuccess)
					success = "true";
		        
				if(success.equals("true"))
				{
			        if(viewMap.containsKey(WebConstants.PrepareAuction.TASK_LIST_USER_TASK))	
						userTask = (UserTask) viewMap.get(WebConstants.PrepareAuction.TASK_LIST_USER_TASK);
					BPMWorklistClient client = new BPMWorklistClient(contextPath);
					 logger.logInfo("UserTask is: " + userTask.getTaskType());
					if(userTask!=null)
					{
						saveSystemComments(MessageConstants.RequestEvents.AUCTION_PUBLISHED);
						client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
						printAdvertisment();
						infoMessage = getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_PUBLISH_SUCCESS);
						sendNotification();
						clearSessionMap();
						viewMap.put(PAGE_MODE, PAGE_MODE_PUBLISH_DONE);
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
						setAsterisk("");
					}
					else{
						errorMessages.clear();
						errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_PUBLISH_FAILURE));
					}
				}
				else
				{
					errorMessages.add(getBundleMessage(MessageConstants.PrepareAuction.MSG_AUCTION_PUBLISH_FAILURE));
				}
			logger.logInfo("publishAuction() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("publishAuction() crashed ", exception);
		}
		return "publishAuction";
	}
	
	@SuppressWarnings("unchecked")
	public String removeAuctionUnit() {
		logger.logInfo("removeAuctionUnit() started...");

		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		List<AuctionUnitView> allSelectedUnitList = new ArrayList<AuctionUnitView>();
        if(viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
        	allSelectedUnitList = (List<AuctionUnitView>)viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
    	try{
    		if((!allSelectedUnitList.isEmpty()) && (allSelectedUnitList!=null))
    		{
    			AuctionUnitView removedUnit = allSelectedUnitList.remove(dataTable.getRowIndex());
    			Long id = removedUnit.getAuctionUnitId();
    			if(id!=null)
    			{
    				propertyServiceAgent.removeAuctionUnit(removedUnit);
    			}
    		}
    		//viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,allSelectedUnitList);
    		PersistAuctionUnitsInView(allSelectedUnitList);
    		
		logger.logInfo("removeAuctionUnit() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("addAuctionUnits() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("removeAuctionUnit() crashed ", exception);
		}
		return "removeAuctionUnit";
	}

	
	@SuppressWarnings("unchecked")
	public void editAuctionUnit(ActionEvent event){
		logger.logInfo("editAuctionUnit() started...");
		try {
			AuctionUnitView auctionUnitView=(AuctionUnitView)dataTable.getRowData();
			List<AuctionUnitView> selectedUnits = new ArrayList<AuctionUnitView>();
			selectedUnits.add(auctionUnitView);
			sessionMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedUnits);
			 
			sessionMap.put("POPUP_MODE", "BOTH");
				
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "javascript:showPriceEditPopup();";
			
			// Add the Javascript to the rendered page's header for immediate execution
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			 
			logger.logInfo("editAuctionUnit() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("editAuctionUnit() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("editAuctionUnit() crashed ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public String saveAuction() {
		logger.logInfo("saveAuction() started...");
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		Boolean firstTime = false;
		String success = "false";
		try {
			if(fieldsValidated()){
				
				 if(auctionView.getAuctionId()==null)
				 {
					 firstTime = true;
					 auctionView.setCreatedBy(getLoggedInUser());
					 auctionView.setCreatedOn(new Date());
					 auctionView.setUpdatedBy(getLoggedInUser());
					 auctionView.setUpdatedOn(new Date());
					 auctionView.setRecordStatus(1L);
					 auctionView.setIsDeleted(0L);
				 }
				 else{
					 firstTime = false;
					 auctionView.setUpdatedBy(getLoggedInUser());
					 auctionView.setUpdatedOn(new Date());
				 }
 
				 advertisementView = auctionView.getAuctionAdvertisement();
				 if(advertisementView.getAuctionAdvertisementId()==null)
				 {
					 advertisementView.setCreatedBy(getLoggedInUser());
					 advertisementView.setCreatedOn(new Date());
					 advertisementView.setUpdatedBy(getLoggedInUser());
					 advertisementView.setUpdatedOn(new Date());
					 advertisementView.setRecordStatus(1L);
					 advertisementView.setIsDeleted(0L);
				 }
				 else{
					 advertisementView.setUpdatedBy(getLoggedInUser());
					 advertisementView.setUpdatedOn(new Date());
				 }
				 
				 if(auctionView.getAuctionVenue()!=null)
					 if(auctionView.getAuctionVenue().getVenueName()==null)
					 	 auctionView.setAuctionVenue(null);
				 if(viewMap.containsKey(WebConstants.AUCTION_VENUE_ID))
				 {
					 Long auctionVenueId = Convert.toLong(viewMap.get(WebConstants.AUCTION_VENUE_ID));
					 AuctionVenueView auctionVenue = propertyServiceAgent.getAuctionVenueById(auctionVenueId);
					 auctionView.setAuctionVenue(auctionVenue);
				 }
				 if(viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
						dataList = (List<AuctionUnitView>) viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);	 
				
				 if(dataList.isEmpty())
					 dataList = null;
				 else
				 {
					 for(AuctionUnitView auctionUnit: dataList){
						 if(auctionUnit.getCreatedBy()==null)
							 auctionUnit.setCreatedBy(getLoggedInUser());
						 if(auctionUnit.getCreatedOn()==null)
							 auctionUnit.setCreatedOn(new Date());
						 auctionUnit.setUpdatedBy(getLoggedInUser());
						 auctionUnit.setUpdatedOn(new Date());
					 }
				 }
				 auctionView.setAuctionUnits((ArrayList<AuctionUnitView>)dataList);
				 
				 auctionView = propertyServiceAgent.createAuction(auctionView);
				 setAuctionView(auctionView);
				 setAuctionNumber(auctionView.getAuctionNumber());
				 
				 Long auctionId = auctionView.getAuctionId();
				if(auctionId<0){
					success = "false";
					errorMessages.clear();
					String msg = getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.AUCTION_NUMBER_EXISTS);
					errorMessages.add(msg);
				}
				else{	
					//viewMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,(List<AuctionUnitView>)auctionView.getAuctionUnits());
					PersistAuctionUnitsInView(auctionView.getAuctionUnits());
					viewMap.put(WebConstants.AUCTION_ID,auctionId);
					
					loadAttachmentsAndComments();
					
					Boolean attachSuccess = saveAttachments(auctionId);
					Boolean commentSuccess = saveComments(auctionId);
					
					if(attachSuccess && commentSuccess)
						success = "true";
					
					errorMessages.clear();
					infoMessage="";
					String msg="";
					if(firstTime){
						msg = getBundleMessage(WebConstants.PropertyKeys.Auction.AUCTION_CREATE_SUCCESS_MESSAGE) + " " + auctionView.getAuctionNumber();
						saveSystemComments(MessageConstants.RequestEvents.AUCTION_CREATED);
						//NotesController.saveSystemNotesForRequest(WebConstants.AUCTION,MessageConstants.RequestEvents.AUCTION_CREATED, auctionId);
					}
					else{
						msg = getBundleMessage(WebConstants.PropertyKeys.Auction.AUCTION_SAVE_SUCCESS_MESSAGE);
						saveSystemComments(MessageConstants.RequestEvents.AUCTION_SAVED);
						//NotesController.saveSystemNotesForRequest(WebConstants.AUCTION,MessageConstants.RequestEvents.AUCTION_CREATED, auctionId);
						
					}
					infoMessage=msg;
					canCompleteTask = true;
					sessionMap.put(WebConstants.CAN_COMPLETE_TASK,WebConstants.CAN_COMPLETE_TASK);
					
//					nextButton();
					/*sessionMap.put(WebConstants.VIEW_MODE,WebConstants.EDIT_MODE);
					init();*/
//					init();
			   }
			}
			else
				success = "validation";
			logger.logInfo("saveAuction() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("saveAuction() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("saveAuction() crashed ", exception);
		}
		return success;
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen==null)
					backScreen = "home";
				clearAll();
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }

	public String done() 
	{
		logger.logInfo("done() started...");
		Long auctionId=0L;
		Long auctionAdvertisementId=-4L;
		UserTask userTask = null;
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		Boolean crashed = false;
		String success = "false";
		try {
				success = saveAuction();
				if(viewMap.containsKey(WebConstants.AUCTION_ID))			
					auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
//				if(viewMap.containsKey(WebConstants.ADVERTISEMENT_ID))			
//					auctionAdvertisementId = (Long)viewMap.get(WebConstants.ADVERTISEMENT_ID);
				if(success.equals("true"))
				{
					PIMSPrepareAuctionBPELPortClient bpelPortClient= new PIMSPrepareAuctionBPELPortClient();
					SystemParameters parameters = SystemParameters.getInstance();
//					String endPoint = bpelPortClient.getEndpoint();
					String endPoint = parameters.getParameter(WebConstants.PrepareAuction.PREPARE_AUCTION_BPEL_END_POINT);
					String userId = getLoggedInUser();
					bpelPortClient.setEndpoint(endPoint);
					bpelPortClient.initiate(auctionId, userId, null, null);
					saveSystemComments(MessageConstants.RequestEvents.AUCTION_SENT_FOR_APPROVAL);
					errorMessages.clear();
					infoMessage="";
					infoMessage = getBundleMessage(WebConstants.PropertyKeys.Auction.AUCTION_CREATED_MESSAGE);
					taskCompleted = true;
					viewMap.put(WebConstants.TASK_COMPLETED,taskCompleted);
					sendNotification();
					clearSessionMap();

					viewMap.put(PAGE_MODE,PAGE_MODE_DRAFT_DONE);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
					setAsterisk("");
									}
				else if(success.equals("false")){
					errorMessages.clear();
					errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Messages.CREATE_AUCTION_TASK_CANNOT_COMPLETE));
				}
				else{
					taskCompleted = false;
					viewMap.put(WebConstants.TASK_COMPLETED,taskCompleted);
				}
			logger.logInfo("done() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			crashed = true;
			logger.LogException("done() crashed ", exception);    
		}
		catch (Exception exception) {
			crashed = true;
			logger.LogException("done() crashed ", exception);
		}
		finally{
			if(crashed)
			{
				errorMessages.clear();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.Messages.TASK_CRASH_MSG));
			}
		}
		return "bpelInvoked";
	}

	
	public Boolean saveSystemComments(String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	
	    	String notesOwner = WebConstants.AUCTION;
	    	Long auctionId = (Long) viewMap.get(WebConstants.AUCTION_ID);
	    	
	    	if(auctionId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(notesOwner, sysNote, auctionId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	
	public void requestHistoryTabClick()
	{
		logger.logInfo("requestHistoryTabClick started...");
		Long auctionId = 0L;
		try	
		{
			RequestHistoryController rhc=new RequestHistoryController();
			if(viewMap.containsKey(WebConstants.AUCTION_ID))
			{
				auctionId = (Long) viewMap.get(WebConstants.AUCTION_ID);
			}
			if(auctionId!=null && auctionId!=0L)
				rhc.getAllRequestTasksForRequest(WebConstants.AUCTION, auctionId.toString());
			
			logger.logInfo("requestHistoryTabClick completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestHistoryTabClick crashed...",ex);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void loadViewAttachmentList(ActionEvent event){
		logger.logInfo("loadViewAttachmentList started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		Long auctionId = -1L;
		try {
			if (viewMap.containsKey(WebConstants.AUCTION_ID))
	    	{
	    		auctionId = (Long) viewMap.get(WebConstants.AUCTION_ID);
		    	String repositoryId = "pimsDb";
		    	String fileSystemRepository = "auctionRepository";
		    	String user = getLoggedInUser();
		    	String entityId = auctionId.toString();
		    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				viewMap.put("repositoryId", repositoryId);
				viewMap.put("fsRepositoryId", fileSystemRepository);
				viewMap.put("externalId", user);
				viewMap.put("associatedObjectId", entityId);

				errorMessages.clear();
				infoMessage= getBundleMessage(WebConstants.PropertyKeys.Commons.ATTACHMENTS_ADDED);
	    	}
	    	else
	    	{
	    		errorMessages.clear();
	    		errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.ATTACHMENT_FAILURE_MESSAGE));
	    	}
			
			logger.logInfo("loadViewAttachmentList completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("loadViewAttachmentList crashed ", exception);
			exception.printStackTrace();
		}
	}
	
	public String ok() {
		logger.logInfo("ok() started...");
		try {
			clearSessionMap();
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			logger.logInfo("ok() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("ok() crashed ", exception);
			exception.printStackTrace();
		}
		return "done";
	}

	/*
	 * Setters / Getters
	 */

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
		auctionView = (AuctionView)viewMap.get(AUCTION_VIEW);
		if(auctionView==null)
			auctionView = new AuctionView();
		return auctionView;
	}

	/**
	 * @param auctionView
	 *            the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
		if(auctionView!=null)
			viewMap.put(AUCTION_VIEW,auctionView);
	}

	/**
	 * @return the dataItem
	 */
	public AuctionUnitView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem
	 *            the dataItem to set
	 */
	public void setDataItem(AuctionUnitView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<AuctionUnitView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList
	 *            the dataList to set
	 */
	public void setDataList(List<AuctionUnitView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the canSelectVenue
	 */
	public Boolean getCanSelectVenue() {
		return canSelectVenue;
	}

	/**
	 * @param canSelectVenue
	 *            the canSelectVenue to set
	 */
	public void setCanSelectVenue(Boolean canSelectVenue) {
		this.canSelectVenue = canSelectVenue;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable
	 *            the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount
	 *            the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}


	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending
	 *            the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the advertisementMode
	 */
	public Boolean getAdvertisementMode() {
		return advertisementMode;
	}

	/**
	 * @param advertisementMode
	 *            the advertisementMode to set
	 */
	public void setAdvertisementMode(Boolean advertisementMode) {
		this.advertisementMode = advertisementMode;
	}

	/**
	 * @return the auctionViewMode
	 */
	public Boolean getAuctionViewMode() {
		return auctionViewMode;
	}

	/**
	 * @param auctionViewMode
	 *            the auctionViewMode to set
	 */
	public void setAuctionViewMode(Boolean auctionViewMode) {
		this.auctionViewMode = auctionViewMode;
	}

	/**
	 * @return the canApproveAdvertisement
	 */
	public Boolean getCanApproveAdvertisement() {
		return canApproveAdvertisement;
	}

	/**
	 * @param canApproveAdvertisement
	 *            the canApproveAdvertisement to set
	 */
	public void setCanApproveAdvertisement(Boolean canApproveAdvertisement) {
		this.canApproveAdvertisement = canApproveAdvertisement;
	}

	/**
	 * @return the canRequestForPublication
	 */
	public Boolean getCanRequestForPublication() {
		return canRequestForPublication;
	}

	/**
	 * @param canRequestForPublication
	 *            the canRequestForPublication to set
	 */
	public void setCanRequestForPublication(Boolean canRequestForPublication) {
		this.canRequestForPublication = canRequestForPublication;
	}

	/**
	 * @return the canPublishAdvertisement
	 */
	public Boolean getCanPublishAdvertisement() {
		return canPublishAdvertisement;
	}

	/**
	 * @param canPublishAdvertisement
	 *            the canPublishAdvertisement to set
	 */
	public void setCanPublishAdvertisement(Boolean canPublishAdvertisement) {
		this.canPublishAdvertisement = canPublishAdvertisement;
	}

	/**
	 * @return the advertisementSaveMode
	 */
	public Boolean getAdvertisementSaveMode() {
		return advertisementSaveMode;
	}

	/**
	 * @param advertisementSaveMode
	 *            the advertisementSaveMode to set
	 */
	public void setAdvertisementSaveMode(Boolean advertisementSaveMode) {
		this.advertisementSaveMode = advertisementSaveMode;
	}

	/**
	 * @return the advertisementView
	 */
	public AuctionAdvertisementView getAdvertisementView() {
		return advertisementView;
	}

	/**
	 * @param advertisementView
	 *            the advertisementView to set
	 */
	public void setAdvertisementView(AuctionAdvertisementView advertisementView) {
		this.advertisementView = advertisementView;
	}

	/**
	 * @return the conductAuctionMode
	 */
	public Boolean getConductAuctionMode() {
		return conductAuctionMode;
	}

	/**
	 * @param conductAuctionMode
	 *            the conductAuctionMode to set
	 */
	public void setConductAuctionMode(Boolean conductAuctionMode) {
		this.conductAuctionMode = conductAuctionMode;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the dateVenueSelected
	 */
	public Boolean getDateVenueSelected() {
		return dateVenueSelected;
	}

	/**
	 * @param dateVenueSelected
	 *            the dateVenueSelected to set
	 */
	public void setDateVenueSelected(Boolean dateVenueSelected) {
		this.dateVenueSelected = dateVenueSelected;
	}
	
	
	public String getErrorMessages() {
		/*String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);*/
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * @param heading
	 *            the heading to set
	 */
	public void setHeading(String heading) {
		this.heading = heading;
	}

	/**
	 * @return the auctionCreateMode
	 */
	public Boolean getAuctionCreateMode() {
		return auctionCreateMode;
	}

	/**
	 * @param auctionCreateMode
	 *            the auctionCreateMode to set
	 */
	public void setAuctionCreateMode(Boolean auctionCreateMode) {
		this.auctionCreateMode = auctionCreateMode;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}


	/**
	 * @return the auctionReqStartDateTimeString
	 */
	public String getAuctionReqStartDateTimeString() {
		return auctionReqStartDateTimeString;
	}


	/**
	 * @param auctionReqStartDateTimeString the auctionReqStartDateTimeString to set
	 */
	public void setAuctionReqStartDateTimeString(
			String auctionReqStartDateTimeString) {
		this.auctionReqStartDateTimeString = auctionReqStartDateTimeString;
	}


	/**
	 * @return the auctionReqEndDateTimeString
	 */
	public String getAuctionReqEndDateTimeString() {
		return auctionReqEndDateTimeString;
	}


	/**
	 * @param auctionReqEndDateTimeString the auctionReqEndDateTimeString to set
	 */
	public void setAuctionReqEndDateTimeString(String auctionReqEndDateTimeString) {
		this.auctionReqEndDateTimeString = auctionReqEndDateTimeString;
	}


	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}


	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	
	@SuppressWarnings("unchecked")
	public List<AuctionUnitView> getAuctionUnitDataList() {
		 Integer rowId=0;
		 AuctionUnitView temp;
		 if(viewMap.containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
		 {
			 dataList = (List<AuctionUnitView>) viewMap.get(WebConstants.ALL_SELECTED_AUCTION_UNITS);		 
			 
		 }
		 //		 if(sessionMap.containsKey(WebConstants.EDIT_PRICE_MODE))
//		 {
//			 if(sessionMap.containsKey(WebConstants.ROW_ID))
//				 rowId =(Integer)sessionMap.get(WebConstants.ROW_ID);
//			 temp = (AuctionUnitView)dataList.get(rowId);
//			 if(sessionMap.containsKey(WebConstants.OPENINIG_PRICE))
//				 temp.setOpeningPrice((Double)sessionMap.get(WebConstants.OPENINIG_PRICE));
//			 if(sessionMap.containsKey(WebConstants.DEPOSIT_AMOUNT))
//			 temp.setDepositAmount((Double)sessionMap.get(WebConstants.DEPOSIT_AMOUNT));
//			 sessionMap.remove(WebConstants.EDIT_PRICE_MODE);
//		 }
		 
//		dataList = allSelectedAuctionUnitList;
//		sessionMap.put(WebConstants.ALL_SELECTED_AUCTION_UNITS,dataList);
		return dataList;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}



	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}



	/**
	 * @return the auctionEditMode
	 */
	public Boolean getAuctionEditMode() {
		return auctionEditMode;
	}



	/**
	 * @param auctionEditMode the auctionEditMode to set
	 */
	public void setAuctionEditMode(Boolean auctionEditMode) {
		this.auctionEditMode = auctionEditMode;
	}



	/**
	 * @return the canCompleteTask
	 */
	public Boolean getCanCompleteTask() {
		return canCompleteTask;
	}



	/**
	 * @param canCompleteTask the canCompleteTask to set
	 */
	public void setCanCompleteTask(Boolean canCompleteTask) {
		this.canCompleteTask = canCompleteTask;
	}



	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}



	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}



	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}



	/**
	 * @return the taskCompleted
	 */
	public Boolean getTaskCompleted() {
		if(viewMap.containsKey(WebConstants.TASK_COMPLETED))
			taskCompleted = (Boolean) viewMap.get(WebConstants.TASK_COMPLETED);
		return taskCompleted;
	}


	/**
	 * @param taskCompleted the taskCompleted to set
	 */
	public void setTaskCompleted(Boolean taskCompleted) {
		this.taskCompleted = taskCompleted;
	}



	/**
	 * @return the advertisementDateString
	 */
	public String getAdvertisementDateString() {
		return advertisementDateString;
	}



	/**
	 * @param advertisementDateString the advertisementDateString to set
	 */
	public void setAdvertisementDateString(String advertisementDateString) {
		this.advertisementDateString = advertisementDateString;
	}


	/**
	 * @return the renderAuctionNumber
	 */
	public Boolean getRenderAuctionNumber() {
		return renderAuctionNumber;
	}



	/**
	 * @param renderAuctionNumber the renderAuctionNumber to set
	 */
	public void setRenderAuctionNumber(Boolean renderAuctionNumber) {
		this.renderAuctionNumber = renderAuctionNumber;
	}



	/**
	 * @return the auctionFullViewMode
	 */
	public Boolean getAuctionFullViewMode() {
		return auctionFullViewMode;
	}



	/**
	 * @param auctionFullViewMode the auctionFullViewMode to set
	 */
	public void setAuctionFullViewMode(Boolean auctionFullViewMode) {
		this.auctionFullViewMode = auctionFullViewMode;
	}

	/**
	 * @return the selectedTab
	 */
	public String getSelectedTab() {
		return selectedTab;
	}

	/**
	 * @param selectedTab the selectedTab to set
	 */
	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	/**
	 * @return the completeButtonLabel
	 */
	public String getCompleteButtonLabel() {
		return completeButtonLabel;
	}

	/**
	 * @param completeButtonLabel the completeButtonLabel to set
	 */
	public void setCompleteButtonLabel(String completeButtonLabel) {
		this.completeButtonLabel = completeButtonLabel;
	}


	/**
	 * @return the fullViewMode
	 */
	public Boolean getFullViewMode() {
		return fullViewMode;
	}


	/**
	 * @param fullViewMode the fullViewMode to set
	 */
	public void setFullViewMode(Boolean fullViewMode) {
		this.fullViewMode = fullViewMode;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}



	public Date getAdvertisementDate() {
		Date temp = (Date)viewMap.get("advertisementDate");
		return temp;
	}


	public void setAdvertisementDate(Date advertisementDate) {
		if(advertisementDate!=null)
			viewMap.put("advertisementDate" , advertisementDate);
	}


	public Date getRequestStartDate() {
		Date temp = (Date)viewMap.get("requestStartDate");
		return temp;
	}


	public void setRequestStartDate(Date requestStartDate) {
		if(requestStartDate!=null)
			viewMap.put("requestStartDate" , requestStartDate);
	}


	public Date getRequestEndDate() {
		Date temp = (Date)viewMap.get("requestEndDate");
		return temp;
	}


	public void setRequestEndDate(Date requestEndDate) {
		if(requestEndDate!=null)
			viewMap.put("requestEndDate" , requestEndDate);
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}


	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getPageMode() {
		pageMode = (String)viewMap.get(PAGE_MODE);
		return pageMode;
	}


	public void setPageMode(String pageMode) {
		String temp = (String)viewMap.get(PAGE_MODE);
		if(temp!=null)
			this.pageMode = temp;
	}


	public String getAuctionNumber() {
		String temp = (String)viewMap.get("auctionNumber");
		return temp;
	}


	public void setAuctionNumber(String auctionNumber) {
		if(auctionNumber!=null)
			viewMap.put("auctionNumber" , auctionNumber);
	}

	public Double getConfiscationPercent() {
		Double temp = (Double)viewMap.get("confiscationPercent");
		return temp;
	}

	public void setConfiscationPercent(Double confiscationPercent) {
		if(confiscationPercent!=null)
			viewMap.put("confiscationPercent" , confiscationPercent);
	}

	public Double getOutBiddingValue() {
		Double temp = (Double)viewMap.get("outBiddingValue");
		return temp;
	}

	public void setOutBiddingValue(Double outBiddingValue) {
		if(outBiddingValue!=null)
			viewMap.put("outBiddingValue" , outBiddingValue);
	}
	
	public String getAuctionDescription() {
		String temp = (String)viewMap.get("auctionDescription");
		return temp;
	}


	public void setAuctionDescription(String auctionDescription) {
		if(auctionDescription!=null)
			viewMap.put("auctionDescription" , auctionDescription);
	}


	public String getAuctionVenue() {
		String temp = (String)viewMap.get("auctionVenue");
		return temp;
	}


	public void setAuctionVenue(String auctionVenue) {
		if(auctionVenue!=null)
			viewMap.put("auctionVenue" , auctionVenue);
	}

	public Date getAuctionDate() {
		Date temp = (Date)viewMap.get("auctionDate");
		return temp;
	}


	public void setAuctionDate(Date auctionDate) {
		if(auctionDate!=null)
			viewMap.put("auctionDate" , auctionDate);
	}
	
	public String getAuctionStatus() {
		String temp = (String)viewMap.get("auctionStatus");
		return temp;
	}

	public void setAuctionStatus(String auctionStatus) {
		if(auctionStatus!=null)
			viewMap.put("auctionStatus" , auctionStatus);
	}

	/**
	 * @return the hhStart
	 */
	public String getHhStart() {
		String temp = (String)viewMap.get("hhStart");
		return temp;
	}


	/**
	 * @param hhStart the hhStart to set
	 */
	public void setHhStart(String hhStart) {
		if(hhStart!=null)
			viewMap.put("hhStart" , hhStart);
	}


	/**
	 * @return the mmStart
	 */
	public String getMmStart() {
		String temp = (String)viewMap.get("mmStart");
		return temp;
	}


	/**
	 * @param mmStart the mmStart to set
	 */
	public void setMmStart(String mmStart) {
		if(mmStart!=null)
			viewMap.put("mmStart" , mmStart);
	}


	/**
	 * @return the hhEnd
	 */
	public String getHhEnd() {
		return hhEnd;
	}


	/**
	 * @param hhEnd the hhEnd to set
	 */
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}


	/**
	 * @return the mmEnd
	 */
	public String getMmEnd() {
		return mmEnd;
	}


	/**
	 * @param mmEnd the mmEnd to set
	 */
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}


	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}


	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}


	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}


	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}


	public HtmlSimpleColumn getSelectColumn() {
		return selectColumn;
	}


	public void setSelectColumn(HtmlSimpleColumn selectColumn) {
		this.selectColumn = selectColumn;
	}


	public HtmlCommandButton getAuctionDetailsSaveButton() {
		return auctionDetailsSaveButton;
	}


	public void setAuctionDetailsSaveButton(
			HtmlCommandButton auctionDetailsSaveButton) {
		this.auctionDetailsSaveButton = auctionDetailsSaveButton;
	}


	public HtmlCommandButton getAuctionDetailsCancelButton() {
		return auctionDetailsCancelButton;
	}


	public void setAuctionDetailsCancelButton(
			HtmlCommandButton auctionDetailsCancelButton) {
		this.auctionDetailsCancelButton = auctionDetailsCancelButton;
	}


	public HtmlCommandButton getAuctionUnitsSaveButton() {
		return auctionUnitsSaveButton;
	}


	public void setAuctionUnitsSaveButton(HtmlCommandButton auctionUnitsSaveButton) {
		this.auctionUnitsSaveButton = auctionUnitsSaveButton;
	}


	public HtmlCommandButton getAuctionUnitsCancelButton() {
		return auctionUnitsCancelButton;
	}


	public void setAuctionUnitsCancelButton(
			HtmlCommandButton auctionUnitsCancelButton) {
		this.auctionUnitsCancelButton = auctionUnitsCancelButton;
	}


	public HtmlCommandButton getPublishButton() {
		return publishButton;
	}


	public void setPublishButton(HtmlCommandButton publishButton) {
		this.publishButton = publishButton;
	}


	public HtmlCommandButton getSubmitButton() {
		return submitButton;
	}


	public void setSubmitButton(HtmlCommandButton submitButton) {
		this.submitButton = submitButton;
	}


	public Boolean getReadOnlyMode() {
		readOnlyMode = (Boolean) viewMap.get("readOnlyMode");
		if(readOnlyMode==null)
			readOnlyMode = true;
		return readOnlyMode;
	}


	public void setReadOnlyMode(Boolean readOnlyMode) {
		this.readOnlyMode = readOnlyMode;
	}


	public HtmlInputText getAuctionNumberField() {
		return auctionNumberField;
	}


	public void setAuctionNumberField(HtmlInputText auctionNumberField) {
		this.auctionNumberField = auctionNumberField;
	}


	public HtmlInputText getAuctionStatusField() {
		return auctionStatusField;
	}


	public void setAuctionStatusField(HtmlInputText auctionStatusField) {
		this.auctionStatusField = auctionStatusField;
	}


	public HtmlInputText getAuctionDescriptionField() {
		return auctionDescriptionField;
	}


	public void setAuctionDescriptionField(HtmlInputText auctionDescriptionField) {
		this.auctionDescriptionField = auctionDescriptionField;
	}


	public HtmlInputText getAuctionVenueField() {
		return auctionVenueField;
	}


	public void setAuctionVenueField(HtmlInputText auctionVenueField) {
		this.auctionVenueField = auctionVenueField;
	}


	public HtmlInputText getOutbiddingValueField() {
		return outbiddingValueField;
	}


	public void setOutbiddingValueField(HtmlInputText outbiddingValueField) {
		this.outbiddingValueField = outbiddingValueField;
	}


	public HtmlInputText getConfiscationPercentField() {
		return confiscationPercentField;
	}


	public void setConfiscationPercentField(HtmlInputText confiscationPercentField) {
		this.confiscationPercentField = confiscationPercentField;
	}


	public HtmlInputText getGracePeriodField() {
		return gracePeriodField;
	}


	public void setGracePeriodField(HtmlInputText gracePeriodField) {
		this.gracePeriodField = gracePeriodField;
	}


	public HtmlCalendar getAuctionDateField() {
		return auctionDateField;
	}


	public void setAuctionDateField(HtmlCalendar auctionDateField) {
		this.auctionDateField = auctionDateField;
	}


	public HtmlCalendar getPublicationDateField() {
		return publicationDateField;
	}


	public void setPublicationDateField(HtmlCalendar publicationDateField) {
		this.publicationDateField = publicationDateField;
	}


	public HtmlCalendar getRegistrationStartDateField() {
		return registrationStartDateField;
	}


	public void setRegistrationStartDateField(
			HtmlCalendar registrationStartDateField) {
		this.registrationStartDateField = registrationStartDateField;
	}


	public HtmlCalendar getRegistrationEndDateField() {
		return registrationEndDateField;
	}


	public void setRegistrationEndDateField(HtmlCalendar registrationEndDateField) {
		this.registrationEndDateField = registrationEndDateField;
	}


	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}


	public void setAsterisk(String asterisk) {
		if(asterisk!=null)
			viewMap.put("asterisk", asterisk);
	}


	public HtmlOutputLabel getMandatoryLabel() {
		return mandatoryLabel;
	}


	public void setMandatoryLabel(HtmlOutputLabel mandatoryLabel) {
		this.mandatoryLabel = mandatoryLabel;
	}


	public HtmlSelectOneMenu getHhStartField() {
		return hhStartField;
	}


	public void setHhStartField(HtmlSelectOneMenu hhStartField) {
		this.hhStartField = hhStartField;
	}


	public HtmlSelectOneMenu getMmStartField() {
		return mmStartField;
	}


	public void setMmStartField(HtmlSelectOneMenu mmStartField) {
		this.mmStartField = mmStartField;
	}

	public HtmlCommandButton getAddAuctionUnitsButton() {
		return addAuctionUnitsButton;
	}

	public void setAddAuctionUnitsButton(HtmlCommandButton addAuctionUnitsButton) {
		this.addAuctionUnitsButton = addAuctionUnitsButton;
	}

	public HtmlCommandButton getSelectVenueButton() {
		return selectVenueButton;
	}

	public void setSelectVenueButton(HtmlCommandButton selectVenueButton) {
		this.selectVenueButton = selectVenueButton;
	}

	public HtmlCommandButton getApplyOpeningPriceButton() {
		return applyOpeningPriceButton;
	}

	public void setApplyOpeningPriceButton(HtmlCommandButton applyOpeningPriceButton) {
		this.applyOpeningPriceButton = applyOpeningPriceButton;
	}

	public HtmlCommandButton getApplyDepositButton() {
		return applyDepositButton;
	}

	public void setApplyDepositButton(HtmlCommandButton applyDepositButton) {
		this.applyDepositButton = applyDepositButton;
	}

	public HtmlSimpleColumn getActionColumn() {
		return actionColumn;
	}

	public void setActionColumn(HtmlSimpleColumn actionColumn) {
		this.actionColumn = actionColumn;
	}

	public String getReadonlyStyleClass() {
		return readonlyStyleClass;
	}

	public void setReadonlyStyleClass(String readonlyStyleClass) {
		this.readonlyStyleClass = readonlyStyleClass;
	}

	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public String getNumericReadonlyStyleClass() {
		return numericReadonlyStyleClass;
	}

	public void setNumericReadonlyStyleClass(String numericReadonlyStyleClass) {
		this.numericReadonlyStyleClass = numericReadonlyStyleClass;
	}

	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	private void sendNotification()
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		AuctionView aucView = this.getAuctionView();
		try
		{
			if( aucView.getAuctionId() != null && viewMap.containsKey("loggedInUserEmail") && viewMap.get("loggedInUserEmail") != null )
			{
				List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
				String email = (String)viewMap.get("loggedInUserEmail"); 
				contactInfoList.add(new ContactInfo(getLoggedInUserId(), null, null, email));
				eventAttributesValueMap.put("AUCTION_NUMBER", aucView.getAuctionNumber());
				eventAttributesValueMap.put("AUCTION_VENUE", aucView.getAuctionVenue().getVenueName());
				eventAttributesValueMap.put("AUCTION_DATE", aucView.getAuctionDate());
				eventAttributesValueMap.put("APPLICANT_NAME", aucView.getUpdatedBy());
				
				if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW))
					generateNotification(WebConstants.Notification_MetaEvents.Event_Auction_Prepared, contactInfoList, eventAttributesValueMap, null);
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_APPROVE))
					generateNotification(WebConstants.Notification_MetaEvents.Event_Auction_Approved, contactInfoList, eventAttributesValueMap, null);
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_PUBLISH))
					generateNotification(WebConstants.Notification_MetaEvents.Event_Auction_Complete, contactInfoList, eventAttributesValueMap, null);
			}
		}
		catch (Exception e)
		{
			logger.logInfo("sendNotification crashed...");
			e.printStackTrace();
		
		}
	}
	
	
//	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
//	{
//		final String METHOD_NAME = "generateNotification()";
//		boolean success = false;
//		
//		try
//		{
//			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
//			if ( recipientList != null && recipientList.size() > 0 )
//			{
//				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
//				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
//				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
//					event.setValue( eventAttributesValueMap );
//				if ( notificationType != null )
//					notificationProvider.fireEvent(event, recipientList, notificationType);
//				else
//					notificationProvider.fireEvent(event, recipientList);
//				
//			}
//			success = true;
//			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
//		}
//		catch (Exception exception)
//		{
//			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
//		}
//		
//		return success;
//	}
	
	public void tabRequestHistory_Click() { 
		String methodName="tabRequestHistory_Click";
		logger.logInfo(methodName+"|"+"Start..");
		Long auctionId = 0L;
		try	
		{
		  RequestHistoryController rhc=new RequestHistoryController();
		  if(viewMap.containsKey(WebConstants.AUCTION_ID)) {	
			  auctionId = (Long) viewMap.get(WebConstants.AUCTION_ID);
		  }
		  
		  if(auctionId!=null) {
			  rhc.getAllRequestTasksForRequest(WebConstants.AUCTION, auctionId.toString());
		  }  
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured..",ex);
		}
		logger.logInfo(methodName+"|"+"Finish..");
	 }

	

}

/*<h:inputHidden id="bidderName" value="#{pages$AuctionRequest.hiddenBidderName}"></h:inputHidden>
<h:inputText id="bidderNameInputText" style="width: 200px; height: 16px" readonly="true" value="#{pages$AuctionRequest.hiddenBidderName}"></h:inputText>
value="#{pages$auctionDetails.adadvertisementView.advertisementDate}"
value="#{pages$auctionDetails.auctionView.requestStartDate}"
value="#{pages$auctionDetails.auctionView.requestEndDate}"
*/