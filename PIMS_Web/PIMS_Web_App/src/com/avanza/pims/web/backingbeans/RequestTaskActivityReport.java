package com.avanza.pims.web.backingbeans;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.RequestTaskActivityCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;


public class RequestTaskActivityReport extends AbstractController 
{

	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger("RequestTaskActivityReport");
	RequestTaskActivityCriteria criteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	
	protected List<String> errorMessages;
	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public RequestTaskActivityReport()
	{ 
			criteria = new RequestTaskActivityCriteria(
														"RequestPerformanceReport.rpt", 
														"com.avanza.pims.report.processor.RequestTaskActivityProcessor", 
														getLoggedInUserObj().getSecondaryFullName() 
													   );		
	}
	
	private Boolean hasErrors ()throws Exception
	{
		boolean hasError  =false;
		if( criteria.getFromDate() == null || criteria.getToDate() == null)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("requestTaskActivityReport.msg.dateRangeRequired"));
			hasError = true;
		}
		return hasError;
	}
	public void cmdView_Click() 
	{
		try
		{
			errorMessages = new ArrayList<String>(0);
			if( hasErrors() ) return;
		
			
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			
			if( 
					criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_RENEW_CONTRACT.toString() ) ||
					criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_LEASE_CONTRACT.toString() )
			  )  
			{
				
				criteria.setReportName("ReqTaskContractSubReport.rpt");
			}
			else if( 
					 criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_REPLACE_CHEQUE.toString() ) ||
					 criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_PAY_BOUNCE_CHEQUE.toString() ) 
			       )
			{
				criteria.setReportName("ReqTaskReplaceSubReport.rpt");
			}
			else if( criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_TERMINATE_CONTRACT .toString() ) )
			{
				criteria.setReportName("ReqTaskTerminateSubReport.rpt");
			}
			else if( criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_TRANSFER_CONTRACT.toString() ) )
			{
				criteria.setReportName("ReqTaskTransferSubReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_NOL.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_CLEARANCE_LETTER.toString() )
				  )
			{
				criteria.setReportName("ReqTaskPIMSSubReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( WebConstants.MemsRequestType.INHERITANCE_FILE.toString() ) 
		  	   )  
			{
				criteria.setReportName("RequestTaskInhFileReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT.toString() ) 
			  	   )
			{
				criteria.setReportName("RequestTaskCollectionSubReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.REQUEST_TYPE_MAINTENANCE_REQUEST.toString() ) 
			  	   )
			{
				criteria.setReportName("ReqTaskMaintenanceSubReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.PAYMENT_REQUEST_ID.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.PAY_VENDOR.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.INVESTMENT_REQUEST_ID.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.REVIEW_CONFIRM_DISBURSEMENTS_ID.toString() ) 
			  	   )
			{
				criteria.setReportName("ReqTaskMEMSPaymentsSubReport.rpt");
			}
			else if( 
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.BLOCKING_REQUEST.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.UNBLOCKING_REQUEST.toString() ) ||
					criteria.getRequestTypeId().equals( Constant.MemsRequestType.NOL_REQUEST_ID.toString() ) 
			  	   )
			{
				criteria.setReportName("ReqTaskMEMSMiscellaneousReport.rpt");
			}
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, criteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("cmdView_Click|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
		
	}
	
	private void openPopup(String javaScriptText) {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getLocale() 
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() 
	{
		WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public RequestTaskActivityCriteria getRequestTaskActivityCriteria() {
		return criteria;
	}
	public void setRequestTaskActivityCriteria(
			RequestTaskActivityCriteria RequestTaskActivityCriteria) {
		this.criteria = RequestTaskActivityCriteria;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public RequestTaskActivityCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(RequestTaskActivityCriteria criteria) {
		this.criteria = criteria;
	}
	
}
