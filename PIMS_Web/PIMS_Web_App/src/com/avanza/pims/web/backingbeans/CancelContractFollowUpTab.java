package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlPanelGrid;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.pims.ws.vo.RequestView;

public class CancelContractFollowUpTab extends AbstractController{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(CancelContractFollowUpTab.class);
	@SuppressWarnings("unchecked")
	Map viewRootMap=FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	private HtmlDataTable requestHistoryDataTable;
	private List<FollowUpView> list = new ArrayList<FollowUpView>(0);
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private NotesVO notesDataItem = new NotesVO();
	private String txtRemarks;
    private final String  fpList= "FOLLOW_UP_LIST"; 
    protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings( "unchecked" )
	public List<FollowUpView> getList() {
		if( viewRootMap.get( fpList )!=null )
		  list = ( ArrayList<FollowUpView> ) viewRootMap.get( fpList );
		return list;
	}
	@SuppressWarnings( "unchecked" )
	public void setList(List<FollowUpView> list) {
		this.list = list;
		if( this.list != null )
	    	viewRootMap.put(fpList, this.list);
	}
	
	@SuppressWarnings( "unchecked" )
	public void btnSaveFollowUp_Click()
	{
		String methodName = "btnSaveFollowUp_Click";
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			logger.logInfo(methodName +"|Start");
			{
				 FollowUpView followUpView = new FollowUpView();
				 setFollowUpView(followUpView );
				 followUpView = rsa.addFollowup(followUpView);
				 list = this.getList();
				 list.add(followUpView);
				 this.setList(list);
				 this.setTxtRemarks("");
				 viewRootMap.put("RECORD_SIZE", this.getList().size());
				 logger.logInfo(methodName+"|"+" Saving Notes...Start");
			}
			logger.logInfo(methodName +"|Finish");
		}
		catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured..",e);
			
		}
		
	}
	@SuppressWarnings( "unchecked" )
	private void setFollowUpView(FollowUpView followUpView)
	{
			List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
		    ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpType.FOLLOW_UP_TYPE);
			DomainDataView ddFollowUpType = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpType.CUSTOM); 
	        ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS);
	        DomainDataView ddFollowUpStatus = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpStatus.CLOSE);
			followUpView.setRemarks(this.getTxtRemarks());
	        followUpView.setRequestId( ( ( RequestView )ApplicationContext.getContext().get(ViewContext.class).getAttribute("requestView")).getRequestId()  );
	        followUpView.setFollowupDate(new Date());
	        followUpView.setCreatedBy(getLoggedInUserId());
	        followUpView.setUpdatedBy(getLoggedInUserId());
	        followUpView.setCreatedByFullNameEn( getLoggedInUserObj().getFullName() );
	        followUpView.setCreatedByFullNameAr( getLoggedInUserObj().getSecondaryFullName() );
	        followUpView.setCreatedOn(new Date());
	        followUpView.setUpdatedOn(new Date());
	        followUpView.setStartDate( new Date()); 
	        followUpView.setFollowUpTypeId(ddFollowUpType.getDomainDataId()); // Will not be used but setting the value due to not null
	        followUpView.setStatusId(ddFollowUpStatus.getDomainDataId() ); // Will not be used but setting the value due to not null
	        followUpView.setIsDeleted(0L);
	        followUpView.setRecordStatus(1L);
		
	}
     @SuppressWarnings( "unchecked" )
	 public void getAllFollowUpsForRequest(Long requestId) throws PimsBusinessException,Exception
	    {
	    	String methodName="getAllFollowUpsForRequest|";
			logger.logInfo(methodName+"Start...");
			if( viewRootMap.containsKey( fpList ) )
				viewRootMap.remove(fpList);
			if( viewRootMap.containsKey("recordSize") )
			    viewRootMap.remove("recordSize");
			this.list = new ArrayList<FollowUpView>( 0 );
	        if( requestId!=null  )
	        {
	        	this.setList( new RequestServiceAgent().getFollowupsByRequest(requestId ) );
		        int recordSize = this.list.size();
		        viewRootMap.put("recordSize", recordSize);
	        }
	        logger.logInfo(methodName+"Finish...");
	    	
	    }
	 @Override
		public void prerender() {
			super.prerender();
			String methodName="prerender"; 
			  logger.logInfo(methodName + "|" + "Finish...");
		}
	 @Override
		public void init() 
	   {
			super.init();
			String methodName="init"; 
			  logger.logInfo(methodName + "|" + "Finish...");
		}
	


	 public HtmlDataTable getRequestHistoryDataTable() {
		return requestHistoryDataTable;
	}
	public void setRequestHistoryDataTable(HtmlDataTable requestHistoryDataTable) {
		this.requestHistoryDataTable = requestHistoryDataTable;
	}
	
	public NotesVO getRequestTasksDataItem() {
		return notesDataItem;
	}
	public void setRequestTasksDataItem(NotesVO requestTasksDataItem) {
		this.notesDataItem = requestTasksDataItem;
	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return localeInfo.getDateFormat();
		
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(viewRootMap.containsKey("recordSize"))
		recordSize=Integer.parseInt(viewRootMap.get("recordSize").toString() );
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	@SuppressWarnings( "unchecked" )
	public String getTxtRemarks() {
		if( viewRootMap.get("txtRemarks")!=null )
			txtRemarks = viewRootMap.get("txtRemarks").toString();
		return txtRemarks;
	}
	@SuppressWarnings( "unchecked" )
	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
		if(this.txtRemarks.trim() != null )
			viewRootMap.put("txtRemarks", this.txtRemarks.trim());
	}

	
	
	
}
