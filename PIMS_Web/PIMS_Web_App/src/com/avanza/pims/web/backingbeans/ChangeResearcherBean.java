package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.entity.ResearcherChangeHistory;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.InheritanceFileView;

public class ChangeResearcherBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String RESEARCHER_LIST = "RESEARCHER_LIST";
	private static Logger logger = Logger.getLogger(ChangeResearcherBean.class);
	
	
	Map  sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private Map<String,String> researcherIdNameMap= new HashMap<String, String>();
	
	
	HtmlInputTextarea txtAreaComments = new HtmlInputTextarea();
	private HtmlSelectOneMenu cboResearcher = new HtmlSelectOneMenu();
	
	
	protected List<String> errorMessages = new ArrayList<String>();
	private List<SelectItem> researcherList ;

	private ResearcherChangeHistory researcherChangeHistory = new ResearcherChangeHistory();
	private InheritanceFileView file = new InheritanceFileView();
	
	
	
	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		try
		{
			super.init();
			if( !isPostBack() )
			{
				setFile( ( InheritanceFileView )sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW ) );
				List<SelectItem> groupUserList= new CommonUtil().getResearcherGroup();
				if(groupUserList  != null && groupUserList.size() > 0)
				{
					for (SelectItem selectItem : groupUserList) 
					{
						researcherIdNameMap.put( selectItem.getValue().toString() , selectItem.getLabel().toString() );
						
					}
					setResearcherIdNameMap(researcherIdNameMap);
					setResearcherList( groupUserList );
				}
				
			}
		}
		catch (Exception e) 
		{
			logger.LogException("init crashed", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	private void executeJavascript(String javascript) throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
	
	private boolean hasErrors() throws Exception
	{
	  boolean hasErrors = false;
	  if(cboResearcher.getValue() == null || cboResearcher.getValue().toString().equals("-1"))
	  {
			errorMessages.add( CommonUtil.getBundleMessage("changeResearcher.researcherRequired") );
			hasErrors = true;
	  }
	  if(txtAreaComments.getValue() == null || txtAreaComments.getValue().toString().trim().length() <=0 )
	  {
			errorMessages.add( CommonUtil.getBundleMessage("changeResearcher.commentsRequired") );
			hasErrors = true;
	  }
	  
	  return hasErrors;
	}

	@SuppressWarnings("unchecked")
	public void onDone()
	{
		try
		{
			file = getFile();
			if( hasErrors() ){return;}
			
			Long fileId = getFile().getInheritanceFileId() ;
			researcherIdNameMap = getResearcherIdNameMap();
			String researcherId = cboResearcher.getValue().toString();
			String newResearcherName = researcherIdNameMap.get( researcherId  ).toString();
			String oldResearcherName ="";
			if( file.getResearcher()!=null && researcherIdNameMap.get(  file.getResearcher() ) != null)
			{
			 oldResearcherName = researcherIdNameMap.get(  file.getResearcher() ).toString();
			 researcherChangeHistory.setOldResearcher(file.getResearcher() );
			}
			researcherChangeHistory.setCreatedBy( getLoggedInUserId() );
			researcherChangeHistory.setCreatedOn( new Date() );
			researcherChangeHistory.setInheritanceFileId( fileId );
			researcherChangeHistory.setNewResearcher(researcherId.toString());
			
			researcherChangeHistory.setReason( txtAreaComments.getValue().toString().trim() );
			
			changeInTransaction( newResearcherName,oldResearcherName  );
			sessionMap.put(WebConstants.InheritanceFile.RESEARCHER_ID,researcherId );
			sessionMap.put(WebConstants.InheritanceFile.RESEARCHER_NAME,researcherId );
			executeJavascript("closeWindowSubmit()");
			
		}
		catch (Exception e) 
		{
			logger.LogException("onDone crashed", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}


	private void changeInTransaction( String newResearcherName,String oldResearcherName )throws Exception 
	{
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			InheritanceFileService service = new InheritanceFileService();
			service.changeFileResearcher( researcherChangeHistory );
	
			CommonUtil.saveSystemComments( WebConstants.InheritanceFile.NOTES_OWNER, 
					                       "mems.inheritanceFile.event.ResearcherChanged",
					                       file.getInheritanceFileId(),
					                       oldResearcherName,
					                       newResearcherName 
					                      );
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	


	public HtmlInputTextarea getTxtAreaComments() {
		return txtAreaComments;
	}

	public void setTxtAreaComments(HtmlInputTextarea txtAreaComments) {
		this.txtAreaComments = txtAreaComments;
	}
	
	@SuppressWarnings( "unchecked" )	
	public String getErrorMessages() 
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}


	public ResearcherChangeHistory getResearcherChangeHistory() 
	{
		if( viewMap.get( "researcherChangeHistory" ) != null )
		{
			researcherChangeHistory = ( ResearcherChangeHistory )viewMap.get( "researcherChangeHistory" );  
		}
		return researcherChangeHistory;
	}

	@SuppressWarnings( "unchecked" )
	public void setResearcherChangeHistory(
			ResearcherChangeHistory researcherChangeHistory) 
	{
		
		this.researcherChangeHistory = researcherChangeHistory;
		if( this.researcherChangeHistory != null )
		{
			 viewMap.put( "researcherChangeHistory" , this.researcherChangeHistory ); 
		}
	}

	@SuppressWarnings( "unchecked" )
	public InheritanceFileView getFile() 
	{
		if( viewMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW ) != null )
		{
			file = ( InheritanceFileView )viewMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW );  
		}
		return file;
	}

	@SuppressWarnings( "unchecked" )
	public void setFile(InheritanceFileView file) 
	{
		this.file = file;
		if( this.file != null )
		{
			 viewMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW , this.file  ); 
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getResearcherList() 
	{
		if(viewMap.get(RESEARCHER_LIST)  != null)
		{
			return (List<SelectItem>) viewMap.get(RESEARCHER_LIST);						
		}
		return researcherList;
	}

	@SuppressWarnings("unchecked")
	public void setResearcherList(List<SelectItem> researcherList)
	{
		this.researcherList = researcherList;
		if ( this.researcherList != null )
		{
			viewMap.put(RESEARCHER_LIST,this.researcherList);
		}
	}


	public HtmlSelectOneMenu getCboResearcher() {
		return cboResearcher;
	}


	public void setCboResearcher(HtmlSelectOneMenu cboResearcher) 
	{
		this.cboResearcher = cboResearcher;
	}


	@SuppressWarnings( "unchecked" )
	public Map<String, String> getResearcherIdNameMap() 
	{
		if( viewMap.get( "researcherIdNameMap" ) != null )
		{
			researcherIdNameMap = ( Map<String, String> )viewMap.get( "researcherIdNameMap" ); 
		}
		return researcherIdNameMap;
	}

	@SuppressWarnings( "unchecked" )
	public void setResearcherIdNameMap(Map<String, String> researcherIdNameMap) 
	{
		this.researcherIdNameMap = researcherIdNameMap;
		if( this.researcherIdNameMap != null  )
		{
		  viewMap.put( "researcherIdNameMap",this.researcherIdNameMap );
		}
	}
}
