package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ProcedureDocument;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;

public class ProcedureDocuments extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(ProcedureDocuments.class);

	public static class Keys {
		public static final String PROCEDURE_DOCUMENTS_LIST = "PROCEDURE_DOCUMENTS_LIST";
		public static final String PROCEDURE_TYPE = "PROCEDURE_TYPE";
		public static final String PROCEDURE_TYPE_ID = "PROCEDURE_TYPE_ID";
		public static final String PROCEDURE_DOCUMENT_TYPE = "PROCEDURE_DOCUMENT_TYPE";
		public static final String PROCEDURE_DOCUMENT_DESCRIPTION = "PROCEDURE_DOCUMENT_DESCRIPTION";
		public static final String PROCEDURE_DOCUMENT_OBJ = "PROCEDURE_DOCUMENT_OBJ";
	}

	@SuppressWarnings("unchecked")
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap();
	@SuppressWarnings("unchecked")
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
			.getAttributes();

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private HtmlDataTable dataTable;

	private List<String> errorMessages;
	private List<String> infoMessages;

	private String selectOneProcedureType;

	private List<DocumentView> documentsViewList;

	public void init() {
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();

		if (sessionMap.get("reloadProcedureDocuments") != null) {
			sessionMap.remove("reloadProcedureDocuments");
			ProcedureDocument procedureTask = (ProcedureDocument) sessionMap
					.get("procedureDocument");

			if (procedureTask != null)
				selectOneProcedureType = procedureTask.getProcedureTypeId()
						.toString();
			else
				selectOneProcedureType = (String) sessionMap
						.get(Keys.PROCEDURE_TYPE_ID);

			loadProcedureDocuments();
		}
	}

	// this method is called when user selects on procedure type and click the
	// search icon. It then loads all the documents of that procedure type.
	public String btnSearch_Click() {
		String sMethod = "btnSearch_Click";
		logger.logInfo(sMethod + " Starts::::::::::::::");
		loadProcedureDocuments();
		logger.logInfo(sMethod + " Ends::::::::::::::");

		return "";
	}

	public boolean getIsSelectedProcedureTypeIsNOL()
	{
		boolean flag = false;
		
		try
		{
			if(selectOneProcedureType != null && !selectOneProcedureType.trim().equals(""))
			{
				Long procedureTypeId = Long.parseLong(selectOneProcedureType);
				UtilityManager utilityManager = new UtilityManager();
				DomainData domainDataProcedureType_NOL = null;
				if(viewMap.containsKey("DD_PROCEDURE_TYPE_NOL"))
				{
					domainDataProcedureType_NOL = (DomainData) viewMap.get("DD_PROCEDURE_TYPE_NOL");
				}					
				else
				{
					domainDataProcedureType_NOL = utilityManager.getDomainDataByValue(WebConstants.PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_NO_OBJECTION_LETTER);
					viewMap.put("DD_PROCEDURE_TYPE_NOL", domainDataProcedureType_NOL);
				}
									
				if(domainDataProcedureType_NOL.getDomainDataId().compareTo(procedureTypeId) == 0)
					flag = true;
			}		
		}
		catch(Exception ex)
		{}
		return flag;
	}
	// loads all the documents that are mapped to the procedure selected by the
	// user from the combo-box
	@SuppressWarnings("unchecked")
	private void loadProcedureDocuments() {
		String sMethod = "loadProcedureDocuments()";
		logger.logInfo(sMethod + " Starts::::::::::::::");

		try {
			if (documentsViewList == null)
				documentsViewList = new ArrayList<DocumentView>();
			else if (documentsViewList.size() > 0)
				documentsViewList.clear();

			Long procedureTypeId = Long.parseLong(selectOneProcedureType);

			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			UtilityManager utilityManager = new UtilityManager();
			DomainData domainData = utilityManager
					.getDomainDataByDomainDataId(procedureTypeId);

			documentsViewList = utilityServiceAgent
					.getProcedureDocuments(domainData.getDataValue());

			viewMap.put(Keys.PROCEDURE_DOCUMENTS_LIST, documentsViewList);

			logger.logInfo("Record Found In Load Data List : %s",
					documentsViewList.size());

			if (documentsViewList.size() == 0) {
				errorMessages = new ArrayList<String>();
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}

			recordSize = documentsViewList.size();
			paginatorRows = getPaginatorRows();
			if (paginatorRows > 0)
				paginatorMaxPages = recordSize / paginatorRows;
			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;

			logger.logInfo(sMethod + " Ends::::::::::::::");
		} catch (Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logInfo(sMethod + " Crashed::::::::::::::");
			logger.LogException(sMethod + " Crashed::::::::::::::", ex);
		}
	}

	@SuppressWarnings("unchecked")
	// opens a pop-up to edit the selected procedure document
	public void editProcedureDocument(ActionEvent event) {
		logger.logInfo("editProcedureDocument() started...");

		try {
			DocumentView selectedDocument = (DocumentView) dataTable
					.getRowData();

			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			DomainDataView ddv = utilityServiceAgent
					.getDomainDataByDomainDataId(Long
							.parseLong(selectOneProcedureType));

			sessionMap.put(ProcedureDocuments.Keys.PROCEDURE_DOCUMENT_OBJ, selectedDocument
					.getProcedureDocument());
			sessionMap.put(Keys.PROCEDURE_TYPE, getIsEnglishLocale() ? ddv
					.getDataDescEn() : ddv.getDataDescAr());
			sessionMap.put(Keys.PROCEDURE_TYPE_ID, selectOneProcedureType);
			sessionMap.put(Keys.PROCEDURE_DOCUMENT_TYPE, selectedDocument
					.getDocTypeId().toString());
			sessionMap.put(Keys.PROCEDURE_DOCUMENT_DESCRIPTION,
					selectedDocument.getProcDocDescription());
			sessionMap.put("NOL_TYPE_ID",
					selectedDocument.getNolTypeId());
			sessionMap.put("PROCEDURE_DOCUMENT_ID",
					selectedDocument.getProcedureDocumentId());
			sessionMap.put("FILE_TYPE_ID",
					selectedDocument.getSubProcedureId());	
			openPopup();
			logger.logInfo("editProcedureDocument() finished...");
		} catch (Exception exception) {
			logger.LogException("editProcedureDocument() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	// deletes the selected procedure documents making it un-mandatory for the
	// procedure
	public void deleteProcedureDocument(ActionEvent event) {
		logger.logInfo("deleteProcedureDocument() started...");

		try {
			DocumentView selectedDocument = (DocumentView) dataTable
					.getRowData();

			com.avanza.pims.entity.ProcedureDocument procedureDocument = EntityManager.getBroker().findById(com.avanza.pims.entity.ProcedureDocument.class, selectedDocument.getProcedureDocumentId());
			procedureDocument.setIsDeleted(WebConstants.IS_DELETED_TRUE);
			procedureDocument.setUpdatedBy(getLoggedInUserId());
			procedureDocument.setUpdatedOn(new Date());

			EntityManager.getBroker().update(procedureDocument);
			loadProcedureDocuments();

			logger.logInfo("deleteProcedureDocument() finished...");
		} catch (Exception exception) {
			logger
					.LogException("deleteProcedureDocument() crashed ",
							exception);
		}
	}

	// to add new document a new popup is loaded within this method
	@SuppressWarnings("unchecked")
	public void addNewDocument() {
		logger.logInfo("addNewDocument() started...");

		try {
			UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
			DomainDataView ddv = utilityServiceAgent
					.getDomainDataByDomainDataId(Long
							.parseLong(selectOneProcedureType));

			sessionMap.put(Keys.PROCEDURE_TYPE, getIsEnglishLocale() ? ddv
					.getDataDescEn() : ddv.getDataDescAr());
			sessionMap.put(Keys.PROCEDURE_TYPE_ID, selectOneProcedureType);

			openPopup();
			logger.logInfo("addNewDocument() finished...");
		} catch (Exception exception) {
			logger.LogException("addNewDocument() crashed ", exception);
		}
	}

	// opens a pop-up in order to add a new document to the select procedure
	private void openPopup() {
		logger.logInfo("openPopup() started...");

		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "javascript:showProcedureDocumentPopup();";

			// Add the Javascript to the rendered page's header for
			// immediate execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	// saves all the documents of the selected procedure
	public void saveDocuments() {
		// TODO: Lots of code here
	}

	// attributes accessors

	@SuppressWarnings("unchecked")
	public List<DocumentView> getGridDataList() {
		if (documentsViewList == null || documentsViewList.size() == 0)
			documentsViewList = viewMap
					.containsKey(Keys.PROCEDURE_DOCUMENTS_LIST) ? (ArrayList<DocumentView>) viewMap
					.get(Keys.PROCEDURE_DOCUMENTS_LIST)
					: new ArrayList<DocumentView>();

		return documentsViewList;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public String getSelectOneProcedureType() {
		return selectOneProcedureType;
	}

	public void setSelectOneProcedureType(String selectOneProcedureType) {
		this.selectOneProcedureType = selectOneProcedureType;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("EN");
	}

	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage() started...");
		String message = "";

		try {
			message = ResourceUtil.getInstance().getProperty(key);
			logger.logInfo("getBundleMessage() completed successfully!!!");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage() crashed ", exception);
		}

		return message;
	}

	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get("recordSize");
		return recordSize == null ? 0 : recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

}
