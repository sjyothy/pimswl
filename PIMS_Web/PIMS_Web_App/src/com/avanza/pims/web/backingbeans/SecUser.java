package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.SecUserUserGroupId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.JSFUtil;
import com.avanza.ui.util.ResourceUtil;


public class SecUser extends AbstractController{
	
	private transient Logger logger = Logger.getLogger(SecUser.class);
	
	private String loginId;
	private String userStatus;
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private boolean admin;	
	
	private String secPolicy;
	private HtmlInputHidden editMode = new HtmlInputHidden();
	private String fullName;
	private String fullNameSec;
	private String firstName;
	private String lastName;
	private String middleName;
	private String emailUrl;
	private HtmlDataTable dataTable;
	private UserDbImpl dataItem;	
	
	List<SelectItem> loginIdList = new ArrayList<SelectItem>(0);

	private HtmlInputHidden listOfGroupsBelongsToUser= new HtmlInputHidden();
	private HtmlInputHidden listOfGroupsNotBelongsToUser= new HtmlInputHidden();
	private HtmlInputText fullNameTextBox = new HtmlInputText();
	private HtmlInputText fullNameSecTextBox = new HtmlInputText();
	private HtmlCommandButton addButton = new HtmlCommandButton();
	private HtmlCommandButton delButton =new HtmlCommandButton();
	private HtmlInputText firstNameTextBox = new HtmlInputText();
	private HtmlInputText loginIDTextBox = new HtmlInputText();
	private HtmlInputText lastNameTextBox = new HtmlInputText();
	private HtmlInputText middleNameTextBox = new HtmlInputText();
	private HtmlInputText emailUrlTextBox = new HtmlInputText();

	private HtmlSelectBooleanCheckbox adminCheck = new HtmlSelectBooleanCheckbox();

	private HtmlSelectOneMenu loginIdSelectOneMenu = new HtmlSelectOneMenu();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton clearButton = new HtmlCommandButton();


	private HtmlSelectOneMenu statusSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu policySelectMenu = new HtmlSelectOneMenu();
	List<SelectItem> policies = new ArrayList();
	
	Map sessionMap;
    Map viewRootMap;
	
	
	
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	/** default constructor */

	@Override
	public void init() 
	{
		super.init();
		
		sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();	       
		viewRootMap= getFacesContext().getViewRoot().getAttributes();
				
		UserDbImpl objUser = null;
		
		if (sessionMap.containsKey(WebConstants.User.EDIT_USER))
		{
			objUser =  (UserDbImpl)sessionMap.remove(WebConstants.User.EDIT_USER);
			setUser(objUser);
		}		
						
		if(!isPostBack())
		{
			loadLoginIdList();
			LoadGroups();	
		
			// If edit mode
			if(objUser != null)
			{
				setUserValues(objUser);
			}
		}
		
	}	
	
	private void setUserValues(UserDbImpl objUser)
	{		
		firstNameTextBox.setValue(objUser.getFirstName());			
		middleNameTextBox.setValue(objUser.getMiddleName());
		lastNameTextBox.setValue(objUser.getLastName());
		fullNameTextBox.setValue(objUser.getFullName());			
		fullNameSecTextBox.setValue(objUser.getSecondaryFullName());
		middleNameTextBox.setValue(objUser.getMiddleName());
		loginIDTextBox.setValue(objUser.getLoginId());
		loginId = objUser.getLoginId();
		emailUrlTextBox.setValue(objUser.getEmailUrl());
		userStatus =objUser.getStatusId();
		
		if(objUser.isSystemUser())
			adminCheck.setSelected(true);
		else
			adminCheck.setSelected(false);
	
	}
	private UserDbImpl getUser()
	{
		UserDbImpl user = null;
		
		if(viewRootMap.containsKey(WebConstants.User.EDIT_USER))
			user = (UserDbImpl) viewRootMap.get(WebConstants.User.EDIT_USER);
			
		return user;
	}
	private void setUser(UserDbImpl user)
	{
		viewRootMap.put(WebConstants.User.EDIT_USER,user);
	}
	private boolean isEditMode()
	{
		if(getUser() == null)
			return false;
		else
			return true;
	}
	
	public List<SecUserGrp> getGroups()
	{		
		List<SecUserGrp> secUserGroupsList = null;
		
		if(viewRootMap.containsKey("USER_GROUPS"))
			secUserGroupsList = (List<SecUserGrp>) viewRootMap.get("USER_GROUPS");
		else
			secUserGroupsList = new ArrayList<SecUserGrp>(0);
		
		return secUserGroupsList;
	}
	public void setGroups(List<SecUserGrp> groups)
	{
		viewRootMap.put("USER_GROUPS",groups);
	}
	private void LoadGroups()
	{
		List<SecUserGrp> secUserGroups = new ArrayList<SecUserGrp>(0);		
		
		Search query = new Search();
		query.clear();
		query.addFrom(UserGroup.class);
		List<UserGroup> allUserGroups =  SecurityManager.findGroup(query);
		List<UserGroup> userGroups = null;
		HashMap<String, UserGroup> userGroupsMap = new HashMap<String, UserGroup>(0);		
		
		if(isEditMode())
		{
			User user =  getUser();
			userGroups = user.getUserGroups();
			if(userGroups != null && userGroups.size() > 0)
			{
				for(UserGroup ug : userGroups )
					userGroupsMap.put(ug.getUserGroupId(), ug);
			}			
		}
		
		for (UserGroup ug : allUserGroups) 
		{
			SecUserGrp secUserGroup = new SecUserGrp();
			secUserGroup.setUserGroup(ug);
			secUserGroup.setUserGroup(ug);	
			
			if(userGroupsMap.containsKey( ug.getUserGroupId()) )				
				secUserGroup.setIsAssignedToUser(true);				
			else
				secUserGroup.setIsAssignedToUser(false);
			
			secUserGroups.add(secUserGroup);
		}
		
		setGroups(secUserGroups);
		viewRootMap.put("SECUSERSCREEN_USER_GROUPS_HASHMAP", userGroupsMap);
	}
	
	public Integer getRecordSize() 
	{
		Integer recordSize= 0 ;
		
		List<SecUserGrp> secUserGroups = getGroups();
		if(secUserGroups != null)
			recordSize = secUserGroups.size();
		
		return recordSize;
	}
	public Integer getPaginatorRows() 
	{		
		return  WebConstants.RECORDS_PER_PAGE;
	}

	public Integer getPaginatorMaxPages() {
		
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() 
	{
	}
	public String cancel() 
	{
		return "cancel";
	}
	
	private void deleteGroupBinding(String loginId, 
			String userGroupId) {

		GroupUserBinding groupUserBinding = null;
		try{	
			groupUserBinding = EntityManager.getBroker().findById(GroupUserBinding.class, new SecUserUserGroupId(userGroupId, loginId));
			if(groupUserBinding!=null)
				EntityManager.getBroker().delete(groupUserBinding);
		}
		catch(Throwable t){
			t.printStackTrace();
		}
	}
	
	private boolean VerifySave()
	{
		boolean result = true;		
		Object fullName = fullNameTextBox.getValue();
		Object firstName = firstNameTextBox.getValue();
		
		if (loginId == null || loginId.toString().trim().equals("") || loginId.toString().trim().equals("-1")	)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.User.LOGIN_ID_REQUIRED));
			result = false;
		}
		if (fullName == null || fullName.toString().trim().equals("") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.User.USER_FULLNAME_REQUIRED));
			result = false;
		}
		if (firstName == null || firstName.toString().trim().equals("") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.User.USER_FIRSTNAME_REQUIRED));
			result = false;
		}
		
		return result;
	}
	public String saveUser() 
	{ 
		String returnVal = ""; 
		try
		{
			if(VerifySave())
			{
				HashMap<String, UserGroup> userGroupsMap = (HashMap<String, UserGroup>) viewRootMap.get("SECUSERSCREEN_USER_GROUPS_HASHMAP");
				UserDbImpl objUser = null;
				boolean isEditMode = isEditMode();
				JSFUtil objUtil = new JSFUtil();
				
				if( isEditMode )
					objUser = getUser();
				else
				{
					objUser = new UserDbImpl();		
					objUser.setLoginId( loginId.toLowerCase() );
				}
				
				GroupUserBinding groupUserBinding = new GroupUserBinding();				
				
				objUser.setEmailUrl(emailUrlTextBox.getValue().toString());
				objUser.setFirstName(firstNameTextBox.getValue().toString());
				objUser.setMiddleName(middleNameTextBox.getValue().toString());
				objUser.setFullName(fullNameTextBox.getValue().toString());
				objUser.setLastName(lastNameTextBox.getValue().toString());
				objUser.setSecondaryFullName(fullNameSecTextBox.getValue().toString());	
				objUser.setPolicyId("admin");
				objUser.setSystemUser(adminCheck.isSelected());			
				objUser.setStatusId(userStatus);			
//				objUser.setCreatedBy(getLoggedInUserId() );
//				objUser.setUpdatedBy(getLoggedInUserId() );
				objUser.setRecordStatus( 1 );
//				objUser.setUpdatedOn( new Date() );
//				objUser.setCreatedOn( new Date() );
				SecurityManager.persistUser(objUser);
				objUser.setRecordStatus( 1 );
				EntityManager.getBroker().update(objUser);
				setUser(objUser);
		
				List<SecUserGrp> secUserGroupsList = getGroups();
				for(SecUserGrp secUserGroup: secUserGroupsList)
				{				
					if(secUserGroup.getIsAssignedToUser())
					{
						SecUserUserGroupId complexKey = new SecUserUserGroupId();
						groupUserBinding.setUserGroupBinding(complexKey);
						groupUserBinding.getUserGroupBinding().setUserGroupId(secUserGroup.getUserGroup().getUserGroupId());
						groupUserBinding.getUserGroupBinding().setLoginId(objUser.getLoginId());
						//System.out.println("========ADDING-----USER: "+ objUser.getLoginId()+"   GROUP ID: "+secUserGroup.getUserGroup().getUserGroupId());
						if(!userGroupsMap.containsKey(secUserGroup.getUserGroup().getUserGroupId()))
						{
							groupUserBinding.setCreatedBy(null);
							groupUserBinding.setCreatedOn(null);
							groupUserBinding.setUpdatedBy(null);
							groupUserBinding.setUpdatedOn(null);
							SecurityManager.persistBinding(groupUserBinding);	
							userGroupsMap.put(secUserGroup.getUserGroup().getUserGroupId(),secUserGroup.getUserGroup());
						}
					}
					else
					{
						//System.out.println("========DELETING----USER: "+ objUser.getLoginId()+"   GROUP ID: "+secUserGroup.getUserGroup().getUserGroupId());
						this.deleteGroupBinding(objUser.getLoginId(), secUserGroup.getUserGroup().getUserGroupId());	
						userGroupsMap.remove(secUserGroup.getUserGroup().getUserGroupId());
					}
				}				
				
				viewRootMap.put("SECUSERSCREEN_USER_GROUPS_HASHMAP",userGroupsMap);
				
				if(isEditMode)
					infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.User.USER_UPDATED));
				else
					infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.User.USER_INSERTED));
			}
		}
		catch (Exception exp)
		{			
			logger.LogException("Failed to save the user due to: ", exp);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return returnVal;
	}

	public List<SelectItem> getLoginIdList() 
	{
		
		if(viewRootMap.containsKey("LOGIN_ID_LIST"))
		{
			loginIdList = (List<SelectItem>) viewRootMap.get("LOGIN_ID_LIST");
		}
		else
			loginIdList = new ArrayList<SelectItem>(0);
		
		return loginIdList;
	}

	public void setLoginIdList(List<SelectItem> loginIdList) 
	{
		viewRootMap.put("LOGIN_ID_LIST", loginIdList);
	}
	@SuppressWarnings( "unchecked" )
	private void loadLoginIdList() {

		//Here we need to change the source of data
		//with the list of POJO
		
		loginIdList = new ArrayList<SelectItem>(0);
		List<User> userList ;		
		
		Search query = new Search();
		query.clear();
		query.addFrom(UserDbImpl.class);

		userList = SecurityManager.findAuthorizeableUser(query);

		if(userList == null)
			userList = new ArrayList<User>(0);
		
		if(isEditMode())
		{
			userList.add(getUser());
		}
		for (User user: userList) 
		{
			String label = "";
			
			if(getIsEnglishLocale())
				label = ((user.getFullName()==null || user.getFullName().length()<=0 )? "": user.getFullName()+ " - ") + user.getLoginId().toLowerCase();
			else
				label = user.getLoginId().toLowerCase() + ((user.getFullName()==null || user.getFullName().length()<=0 )? "": " - "+ user.getFullName() );
			
			loginIdList.add(new SelectItem(user.getLoginId(),label));	
		}
		Collections.sort( loginIdList, ListComparator.LIST_COMPARE );
		setLoginIdList(loginIdList);
	}
	private void showCombo(){
		loginIdSelectOneMenu.setRendered(true);
		loginIDTextBox.setRendered(false);
	}
	private void hideCombo(){
		loginIdSelectOneMenu.setRendered(false);
		loginIDTextBox.setRendered(true);
	}
	
	private void disablePanel(){
		fullNameTextBox.setDisabled(true);
		firstNameTextBox.setDisabled(true);
		middleNameTextBox.setDisabled(true);
		lastNameTextBox.setDisabled(true);
		emailUrlTextBox.setDisabled(true);
		adminCheck.setReadonly(true);
		loginIdSelectOneMenu.setDisabled(true);
		saveButton.setDisabled(true);
		clearButton.setDisabled(true);
		statusSelectMenu.setDisabled(true);
		policySelectMenu.setDisabled(true);
		fullNameSecTextBox.setDisabled(true);		
		addButton.setDisabled(true);
		delButton.setDisabled(true);
		loginIDTextBox.setDisabled(true);		
	}
	private void enablePanel(){
		fullNameTextBox.setDisabled(false);
		firstNameTextBox.setDisabled(false);
		middleNameTextBox.setDisabled(false);
		lastNameTextBox.setDisabled(false);
		emailUrlTextBox.setDisabled(false);
		adminCheck.setReadonly(false);
		loginIdSelectOneMenu.setDisabled(false);
		saveButton.setDisabled(false);
		clearButton.setDisabled(false);
		statusSelectMenu.setDisabled(false);
		policySelectMenu.setDisabled(false);
		fullNameSecTextBox.setDisabled(false);		
		addButton.setDisabled(false);
		delButton.setDisabled(false);
		loginIDTextBox.setDisabled(false);
	}
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false; 

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}  
	
	public List<SelectItem> getStatus() 
	{
		List<SelectItem> status = null;
		
		if(viewRootMap.containsKey("USER_STATUS"))
			status = (List<SelectItem>) viewRootMap.get("USER_STATUS");
		else
			status = new ArrayList<SelectItem>(0);
		
		return status;
	}	
	public void setStatus(List<SelectItem> status) 
	{
		viewRootMap.put("USER_STATUS", status);
	}	
	private void loadStatusList() 
	{
		try
		{
			List<SelectItem> status = new ArrayList();	
//			UtilityServiceAgent utilityServiceAgent=new UtilityServiceAgent();
//			List<DomainDataView> ddv= (ArrayList)utilityServiceAgent.getDomainDataByDomainTypeName(WebConstants.User.USER_STATUS);
//			for (int i = 0; i < ddv.size(); i++) 
//			{
//				if (getIsEnglishLocale())
//					status.add(new SelectItem(ddv.get(i).getDomainDataId().toString(),ddv.get(i).getDataDescEn()));
//				else
//					status.add(new SelectItem(ddv.get(i).getDomainDataId().toString(),ddv.get(i).getDataDescAr()));						
//
//			}				
			status.add(new SelectItem("1", "Active"));
			status.add(new SelectItem("0","In Active"));
			setStatus(status);
		}
		catch (Exception e)
		{
			logger.LogException("Failed to load statuses", e);
		}
	}

	public User getDataItem() {
		return dataItem;
	}

	public void setDataItem(UserDbImpl dataItem) {
		this.dataItem = dataItem;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getEmailUrl() {
		return emailUrl;
	}

	public void setEmailUrl(String emailUrl) {
		this.emailUrl = emailUrl;
	}

	public HtmlInputText getEmailUrlTextBox() {
		return emailUrlTextBox;
	}

	public void setEmailUrlTextBox(HtmlInputText emailUrlTextBox) {
		this.emailUrlTextBox = emailUrlTextBox;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public HtmlInputText getFirstNameTextBox() {
		return firstNameTextBox;
	}

	public void setFirstNameTextBox(HtmlInputText firstNameTextBox) {
		this.firstNameTextBox = firstNameTextBox;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullNameSec() {
		return fullNameSec;
	}

	public void setFullNameSec(String fullNameSec) {
		this.fullNameSec = fullNameSec;
	}

	public HtmlInputText getFullNameSecTextBox() {
		return fullNameSecTextBox;
	}

	public void setFullNameSecTextBox(HtmlInputText fullNameSecTextBox) {
		this.fullNameSecTextBox = fullNameSecTextBox;
	}

	public HtmlInputText getFullNameTextBox() {
		return fullNameTextBox;
	}

	public void setFullNameTextBox(HtmlInputText fullNameTextBox) {
		this.fullNameTextBox = fullNameTextBox;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public HtmlInputText getLastNameTextBox() {
		return lastNameTextBox;
	}

	public void setLastNameTextBox(HtmlInputText lastNameTextBox) {
		this.lastNameTextBox = lastNameTextBox;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public HtmlInputText getMiddleNameTextBox() {
		return middleNameTextBox;
	}

	public void setMiddleNameTextBox(HtmlInputText middleNameTextBox) {
		this.middleNameTextBox = middleNameTextBox;
	}


	public HtmlSelectOneMenu getPolicySelectMenu() {
		return policySelectMenu;
	}

	public void setPolicySelectMenu(HtmlSelectOneMenu policySelectMenu) {
		this.policySelectMenu = policySelectMenu;
	}

	public String getSecPolicy() {
		return secPolicy;
	}

	public void setSecPolicy(String secPolicy) {
		this.secPolicy = secPolicy;
	}

	public HtmlSelectOneMenu getStatusSelectMenu() {
		return statusSelectMenu;
	}
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isAdmin() {

		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public HtmlSelectBooleanCheckbox getAdminCheck() {
		return adminCheck;
	}

	public void setAdminCheck(HtmlSelectBooleanCheckbox adminCheck) {
		this.adminCheck = adminCheck;
	}

	public List<SelectItem> getPolicies() {
		return policies;
	}

	public void setPolicies(List<SelectItem> policies) {
		this.policies = policies;
	}	

	public HtmlSelectOneMenu getLoginIdSelectOneMenu() {
		return loginIdSelectOneMenu;
	}

	public void setLoginIdSelectOneMenu(HtmlSelectOneMenu loginIdSelectOneMenu) {
		this.loginIdSelectOneMenu = loginIdSelectOneMenu;
	}
	public HtmlInputHidden getListOfGroupsBelongsToUser() {
		return listOfGroupsBelongsToUser;
	}

	public void setListOfGroupsBelongsToUser(
			HtmlInputHidden listOfGroupsBelongsToUser) {
		this.listOfGroupsBelongsToUser = listOfGroupsBelongsToUser;
	}

	public HtmlInputHidden getListOfGroupsNotBelongsToUser() {
		return listOfGroupsNotBelongsToUser;
	}

	public void setListOfGroupsNotBelongsToUser(
			HtmlInputHidden listOfGroupsNotBelongsToUser) {
		this.listOfGroupsNotBelongsToUser = listOfGroupsNotBelongsToUser;
	}

	public HtmlInputHidden getEditMode() {
		return editMode;
	}

	public void setEditMode(HtmlInputHidden editMode) {
		this.editMode = editMode;
	}

	public HtmlCommandButton getClearButton() {
		return clearButton;
	}

	public void setClearButton(HtmlCommandButton clearButton) {
		this.clearButton = clearButton;
	}

	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}

	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}

	public HtmlCommandButton getAddButton() {
		return addButton;
	}

	public void setAddButton(HtmlCommandButton addButton) {
		this.addButton = addButton;
	}

	public HtmlCommandButton getDelButton() {
		return delButton;
	}

	public void setDelButton(HtmlCommandButton delButton) {
		this.delButton = delButton;
	}	

	public HtmlInputText getLoginIDTextBox() {
		return loginIDTextBox;
	}

	public void setLoginIDTextBox(HtmlInputText loginIDTextBox) {
		this.loginIDTextBox = loginIDTextBox;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}
}