package com.avanza.pims.web.backingbeans;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ReceiptReportCriteria;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;


public class ReceiptReportBackingBean extends AbstractController 
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("CollectionReportBackingBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	ReceiptReportCriteria receiptReportCriteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlSelectOneMenu  cmbReceiptBy = new HtmlSelectOneMenu();
	private List<SelectItem> ownerShipTypeList = new ArrayList<SelectItem>();
	private List<String> selectedOwnerShipTypeList = new ArrayList<String>();
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
	
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
				
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		loadPropertyOwnershipTypes();
	}
	public void loadPropertyOwnershipTypes() throws Exception
	{

		List<DomainData> ddList = UtilityService.getDomainDataPOJOById(WebConstants.OwnerShipType.KEY_ID);
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean isEnglishLocale=localeInfo.getLanguageCode().equalsIgnoreCase("en");
		selectedOwnerShipTypeList = new ArrayList<String>();
		for (DomainData domainData : ddList) 
		{
			String domainDataId =domainData.getDomainDataId().toString();
			SelectItem item = new SelectItem(
												domainDataId, 
												isEnglishLocale?domainData.getDataDescEn():domainData.getDataDescAr()
											);
			
			ownerShipTypeList.add(item);
			if( domainData.getDomainDataId().compareTo(WebConstants.OwnerShipType.THIRD_PARTY_ZAWAYA_ID) != 0)
			{
				selectedOwnerShipTypeList.add( domainDataId );
			}
							

				
			}
		setOwnerShipTypeList(ownerShipTypeList);
		}
	
	public ReceiptReportBackingBean()
	{ 
			receiptReportCriteria = new ReceiptReportCriteria(ReportConstant.Report.RECEIPT_REPORT, ReportConstant.Processor.RECEIPT_REPORT, getLoggedInUserObj().getFullName());		
	}
	
	public String cmdView_Click() 
	{
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		String criteriaOwnerShipTypes = "";
		if(selectedOwnerShipTypeList != null && selectedOwnerShipTypeList.size()>0)
		{
			
			criteriaOwnerShipTypes +=selectedOwnerShipTypeList.toString();
		}
		receiptReportCriteria.setOwnerShipTypes( criteriaOwnerShipTypes.replace("[", "").replace("]",""));
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, receiptReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	public String printCancelledReceipts() 
	{
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		receiptReportCriteria.setCancelledReceipt(true);
		String criteriaOwnerShipTypes = "";
		if(selectedOwnerShipTypeList != null && selectedOwnerShipTypeList.size()>0)
		{
			
			criteriaOwnerShipTypes +=selectedOwnerShipTypeList.toString();
		}
		receiptReportCriteria.setOwnerShipTypes( criteriaOwnerShipTypes.replace("[", "").replace("]",""));
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, receiptReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlSelectOneMenu getCmbReceiptBy() {
		return cmbReceiptBy;
	}

	public void setCmbReceiptBy(HtmlSelectOneMenu cmbReceiptBy) {
		this.cmbReceiptBy = cmbReceiptBy;
	}

	public ReceiptReportCriteria getReceiptReportCriteria() {
		return receiptReportCriteria;
	}

	public void setReceiptReportCriteria(ReceiptReportCriteria receiptReportCriteria) {
		this.receiptReportCriteria = receiptReportCriteria;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getOwnerShipTypeList() {
		if( viewMap.get("ownerShipTypeList")!= null)
		{
			this.ownerShipTypeList =	(ArrayList<SelectItem> )viewMap.get("ownerShipTypeList");
		}
		return ownerShipTypeList;
	}
	@SuppressWarnings("unchecked")
	public void setOwnerShipTypeList(List<SelectItem> ownerShipTypeList) {
		
		this.ownerShipTypeList = ownerShipTypeList;
		if(this.ownerShipTypeList != null)
		{
		viewMap.put("ownerShipTypeList",this.ownerShipTypeList );
		}
	}

	public List<String> getSelectedOwnerShipTypeList() {
		return selectedOwnerShipTypeList;
	}

	public void setSelectedOwnerShipTypeList(List<String> selectedOwnerShipTypeList) {
		this.selectedOwnerShipTypeList = selectedOwnerShipTypeList;
	}
	
	
	
}
