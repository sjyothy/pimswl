package com.avanza.pims.web.backingbeans.Investment.RevenuesAndExpenses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;


import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AccountServiceAgent;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RevenuesAndExpensesServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.investment.AssetService;
import com.avanza.pims.ws.vo.AccountView;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExpenseAndRevenueView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.OrderView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.pims.ws.vo.ViolationView;

public class RevenuesAndExpensesSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(RevenuesAndExpensesSearchBean.class);
	private CommonUtil commonUtil = new CommonUtil();
	private List<ExpenseAndRevenueView> dataList;
	ExpenseAndRevenueView expenseAndRevenueView = new ExpenseAndRevenueView();
	RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent = new RevenuesAndExpensesServiceAgent();
	private HtmlDataTable tbl_RevenueAndExpenseSearch;
	private ExpenseAndRevenueView dataItem = new ExpenseAndRevenueView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	
	
	private HtmlSelectOneMenu htmlSelectOneStatus = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectNumber = new HtmlSelectOneMenu();
	
	
	
	private HtmlInputText htmlPortfolio = new HtmlInputText();
	private HtmlInputText htmlMarker = new HtmlInputText();
	private HtmlInputText htmlLog = new HtmlInputText();
	
	private HtmlCalendar htmlDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlDateTo = new HtmlCalendar();
	
	
	
	


	
	// Constructors

	/** default constructor */
	public RevenuesAndExpensesSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/ViolationSearch.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               //"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        {
        	javaScriptText=extraJavaScript;
        }
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	
	public String btnAddRevenue_Click() {
		String methodName = "btnAddRevenue_Click";
		try {
         
		    sessionMap.put("ADD_REVENUE", true);	 
		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "addRevenueAndExpense";
	}
	
	public String btnAddExpenses_Click() {
		String methodName = "btnAddExpenses_Click";
		try {
			sessionMap.put("ADD_EXPENSE", true);
		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "addRevenueAndExpense";
	}
	
	

	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();		
		
		String emptyValue = "-1";	
		
		
		
		Object number = htmlSelectNumber.getValue();
		Object protfolio = htmlPortfolio.getValue();
		Object marker = htmlMarker.getValue();
		Object log = htmlLog.getValue();



		Object status = htmlSelectOneStatus.getValue();
		Object type = htmlSelectOneType.getValue();

		Object dateFrom = htmlDateFrom.getValue();
		Object dateTo = htmlDateTo.getValue();
		
		
		if(protfolio != null && !protfolio.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.PROTFOLIO, protfolio.toString());
		}
    	if(marker != null && !marker.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.MARKER, marker.toString());
		}
		if(log != null && !log.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.LOG, log.toString());
		}
		
		
		if(number != null && !number.toString().trim().equals("") && !number.toString().trim().equals(emptyValue) )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.NUMBER, number.toString());
		}
		if(status != null && !status.toString().trim().equals("") && !status.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.STATUS, status.toString());
		}
		if(type != null && !type.toString().trim().equals("") && !type.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.TYPE, type.toString());
		}	
		
		
		if(dateFrom != null && !dateFrom.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.DATE_FROM, dateFrom);
		}
		if(dateTo != null && !dateTo.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.DATE_TO, dateTo);
		}
		
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
	/*	Object accountNumber = htmlAccountNumber.getValue();
		Object accountName = htmlAccountName.getValue();
	 	Object GRPAccountNumber = htmlGRPAccountNumber.getValue();
	//	Object GRPAccountName = htmlGRPAccountName.getValue();


		Object accountStatus = htmlSelectOneAccountStatus.getValue();
		Object accountType = htmlSelectOneAccountType.getValue();
		
		
		
		if(StringHelper.isNotEmpty(accountNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(accountName.toString()))
			return true;
		if(StringHelper.isNotEmpty(GRPAccountNumber.toString()))
			return true;
	//	if(StringHelper.isNotEmpty(GRPAccountName.toString()))
	//		return true;
		
		if(StringHelper.isNotEmpty(accountStatus.toString()) && !accountStatus.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(accountType.toString()) && !accountType.toString().equals("-1"))
			return true;
*/		
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		logger.logInfo(methodName + "|" + "Start::");
		try 
		{
		 //if(atleastOneCriterionEntered())
		//	{
				loadDataList();
				if(viewRootMap.get("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST")!=null){
					List<AssetClassView> assetClassList = (List<AssetClassView>) viewRootMap.get("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST");
					if(assetClassList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
		//	}
		//	else{
		//		errorMessages.clear();
		//		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
		//	}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		logger.logInfo(methodName + "|" + "Finish::");
		return "";
	}

	
   private void loadDataList() throws Exception
   {
	   String methodName = "btnSearch_Click";
	   logger.logInfo(methodName + "|" + "Start::");
	   Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	   HashMap searchMap= getSearchCriteria();
		
	   if(viewRootMap.containsKey("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST"))
		   viewRootMap.remove("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST");
	   
        // new function right here   	   
		List<ExpenseAndRevenueView> expenseAndRevenuesViewList =revenuesAndExpensesServiceAgent.getAllRevenueAndExpenses(searchMap);
		viewRootMap.put("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST",expenseAndRevenuesViewList);
		dataList=expenseAndRevenuesViewList;
	   
		  if(dataList!=null)
				recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
	        
		
		logger.logInfo(methodName + "|" + "Finish::");
   }
	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) 
			{
				
				
				
				
		    }
			

			
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}
    
	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		HashMap searchMap;
		String methodName="prerender"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
		  if(sessionMap.containsKey("FROM_STATUS")  && sessionMap.get("FROM_STATUS").toString().equals("1"))
		  {
			  sessionMap.remove("FROM_STATUS");
			  loadDataList();
			    
		  }
		   
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		  logger.logInfo(methodName + "|" + "Finish...");
	}

	

	public List<ExpenseAndRevenueView> getDataList() {
		dataList =null;
		if (dataList == null) 
		{
			if(viewRootMap.containsKey("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST"));
			dataList=(ArrayList)viewRootMap.get("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST");
		}
		return dataList;

	}

	public void setDataList(List<ExpenseAndRevenueView> dataList) {
		this.dataList = dataList;
	}

	


	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public String btnAddAccount_Click(){
	
		return "accountManage";
	}

	
	
	public String cmdView_Click() 
	{
		String path = null;
		expenseAndRevenueView = (ExpenseAndRevenueView)tbl_RevenueAndExpenseSearch.getRowData();
		sessionMap.put( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW, expenseAndRevenueView);
		sessionMap.put(WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY, WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW);
		sessionMap.put(WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY, WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_POPUP_VIEW_ONLY);
		
		DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_REVENUE);
		if(expenseAndRevenueView.getExpRevTypeId().compareTo(ddvTypeRevenue.getDomainDataId().toString())==0)
		{
			sessionMap.put("ADD_REVENUE", true);
		}else
		{
			sessionMap.put("ADD_EXPENSE", true);
		}
		openPopup("openExpenseAndRevenuePopup();");
		
		return path;
	}
	
	
	public String cmdEdit_Click(){
		
		expenseAndRevenueView = (ExpenseAndRevenueView)tbl_RevenueAndExpenseSearch.getRowData();
		sessionMap.put( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW, expenseAndRevenueView);   
		sessionMap.put(WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY, WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE);
		
		DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_REVENUE);
		
		if(expenseAndRevenueView.getExpRevTypeId().compareTo(ddvTypeRevenue.getDomainDataId().toString())==0)
		{
			sessionMap.put("ADD_REVENUE", true);
		}else
		{
			sessionMap.put("ADD_EXPENSE", true);
		}
		
		return "addRevenueAndExpense";
	}

	public String cmdDelete_Click()
	{
   try {
		
		expenseAndRevenueView = (ExpenseAndRevenueView)tbl_RevenueAndExpenseSearch.getRowData();
		expenseAndRevenueView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
		Integer index =  tbl_RevenueAndExpenseSearch.getRowIndex();
		viewRootMap.put("delete", index);
		revenuesAndExpensesServiceAgent.updateRevenueAndExpense(expenseAndRevenueView);
		
		index = (Integer) viewRootMap.get("delete");
		if(viewRootMap.containsKey("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST"))
		{
		 List<ExpenseAndRevenueView> tempList = (List<ExpenseAndRevenueView>) viewRootMap.get("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST");
		 tempList.remove(index.intValue());
         dataList = tempList;
         viewRootMap.put("SESSION_EXPENSE_AND_REVENUE_SEARCH_LIST",dataList);
        if(dataList!=null)
        {
			recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
         }
         
	     infoMessages.add(CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.deleteRecord"));
		}
		
		} 
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "cmdDelete_Click"; 
	}
	
	
	
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	
	
	public String getLocale() 
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() 
	{
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}



	public HtmlSelectOneMenu getHtmlSelectOneStatus() {
		return htmlSelectOneStatus;
	}

	public void setHtmlSelectOneStatus(HtmlSelectOneMenu htmlSelectOneStatus) {
		this.htmlSelectOneStatus = htmlSelectOneStatus;
	}

	public HtmlSelectOneMenu getHtmlSelectOneType() {
		return htmlSelectOneType;
	}

	public void setHtmlSelectOneType(HtmlSelectOneMenu htmlSelectOneType) {
		this.htmlSelectOneType = htmlSelectOneType;
	}
	

	public HtmlInputText getHtmlPortfolio() {
		return htmlPortfolio;
	}

	public void setHtmlPortfolio(HtmlInputText htmlPortfolio) {
		this.htmlPortfolio = htmlPortfolio;
	}

	public HtmlInputText getHtmlMarker() {
		return htmlMarker;
	}

	public void setHtmlMarker(HtmlInputText htmlMarker) {
		this.htmlMarker = htmlMarker;
	}

	public HtmlInputText getHtmlLog() {
		return htmlLog;
	}

	public void setHtmlLog(HtmlInputText htmlLog) {
		this.htmlLog = htmlLog;
	}

	public HtmlCalendar getHtmlDateFrom() {
		return htmlDateFrom;
	}

	public void setHtmlDateFrom(HtmlCalendar htmlDateFrom) {
		this.htmlDateFrom = htmlDateFrom;
	}

	public HtmlCalendar getHtmlDateTo() {
		return htmlDateTo;
	}

	public void setHtmlDateTo(HtmlCalendar htmlDateTo) {
		this.htmlDateTo = htmlDateTo;
	}

	public HtmlDataTable getTbl_RevenueAndExpenseSearch() {
		return tbl_RevenueAndExpenseSearch;
	}

	public void setTbl_RevenueAndExpenseSearch(
			HtmlDataTable tbl_RevenueAndExpenseSearch) {
		this.tbl_RevenueAndExpenseSearch = tbl_RevenueAndExpenseSearch;
	}

	public ExpenseAndRevenueView getDataItem() {
		return dataItem;
	}

	public void setDataItem(ExpenseAndRevenueView dataItem) {
		this.dataItem = dataItem;
	}

	public RevenuesAndExpensesServiceAgent getRevenuesAndExpensesServiceAgent() {
		return revenuesAndExpensesServiceAgent;
	}

	public void setRevenuesAndExpensesServiceAgent(
			RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent) {
		this.revenuesAndExpensesServiceAgent = revenuesAndExpensesServiceAgent;
	}

	public HtmlSelectOneMenu getHtmlSelectNumber() {
		return htmlSelectNumber;
	}

	public void setHtmlSelectNumber(HtmlSelectOneMenu htmlSelectNumber) {
		this.htmlSelectNumber = htmlSelectNumber;
	}
	
	
}