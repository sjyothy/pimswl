package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.hsqldb.lib.HashMap;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.MemsNolTypeUsrGrp;
import com.avanza.pims.entity.MemsNolTypes;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.vo.MemsNolTypeView;
import com.avanza.ui.util.ResourceUtil;

//TODO::

public class MemsNolTypeGroupsMapping extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private HtmlDataTable dataTable;
	private HtmlSelectOneMenu selectOneGroup = new HtmlSelectOneMenu();
	private HtmlCommandButton btnAddGroup = new HtmlCommandButton();

	private ArrayList<MemsNolTypeView> dataList = new ArrayList();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private List<SelectItem> selectUserGroups = new ArrayList<SelectItem>();
	private String selectOneUserGroup;

	private List<UserGroup> memsNolTypeGroups = new ArrayList<UserGroup>();

	@Override
	public void init() {
		super.init();

		if (!isPostBack()) {
			getUserGroups();
			// fillDataTable();

		}
	}

	private void fillDataTable() {
		setMemsNolTypeGroups(getSecUserGroups());

	}

	public void prerender() {
		super.prerender();
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlSelectOneMenu getSelectOneGroup() {
		return selectOneGroup;
	}

	public void setSelectOneGroup(HtmlSelectOneMenu selectOneGroup) {
		this.selectOneGroup = selectOneGroup;
	}

	public ArrayList<MemsNolTypeView> getDataList() {
		if (viewMap.get("MEMS_NOL_TYPE_LIST") != null)
			return (ArrayList<MemsNolTypeView>) viewMap
					.get("MEMS_NOL_TYPE_LIST");

		return dataList;
	}

	public void setDataList(ArrayList<MemsNolTypeView> dataList) {

		this.dataList = dataList;
		if (this.dataList != null)
			viewMap.put("MEMS_NOL_TYPE_LIST", this.dataList);

	}

	private void getUserGroups() {
		logger.logInfo("getUserGroups() started...");

		try {
			MemsNolTypeView selectedMemsNolTypeView = (MemsNolTypeView) sessionMap
					.get("selectedMemsNolTypeView");

			selectUserGroups.clear();
			memsNolTypeGroups.clear();

			Set memsNolTypeUserGroupMappingSet = selectedMemsNolTypeView
					.getMemsNolTypeUserGroupMappings();

			HashMap tempHashmap = new HashMap();

			for (Object object : memsNolTypeUserGroupMappingSet) {

				MemsNolTypeUsrGrp memsNolTypeUserGroupMapping = (MemsNolTypeUsrGrp) object;
				tempHashmap.put(memsNolTypeUserGroupMapping.getUserGroupId(),
						memsNolTypeUserGroupMapping);
			}

			for (UserGroup userGroup : getSecUserGroups()) {

				if (tempHashmap.containsKey(userGroup.getUserGroupId())) {
					memsNolTypeGroups.add(userGroup);
				} else {
					selectUserGroups.add(new SelectItem(userGroup
							.getUserGroupId(), userGroup.getPrimaryName()));
				}

			}
			viewMap.put("memsNolTypeGroups", memsNolTypeGroups);
			viewMap.put("selectUserGroups", selectUserGroups);

			if (this.dataList != null)
				this.setDataList(this.dataList);

			recordSize = memsNolTypeGroups.size();
			viewMap.put("recordSize", recordSize);

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getUserGroups() crashed ", e);
		}
	}

	public void addGroup() {

		String value = selectOneUserGroup;

		MemsNolTypeView selectedMemsNolTypeView = (MemsNolTypeView) sessionMap
				.get("selectedMemsNolTypeView");

		MemsNolTypes memsNolType = EntityManager.getBroker().findById(
				MemsNolTypes.class, selectedMemsNolTypeView.getMemsNolTypeId());

		MemsNolTypeUsrGrp newGroup = new MemsNolTypeUsrGrp();
		// newGroup.setCreatedBy("admin");
		// newGroup.setCreatedOn(new Date());
		newGroup.setMemsNolTypes(memsNolType);
		// newGroup.setUpdatedBy("admin");
		// newGroup.setUpdatedOn(new Date());
		newGroup.setUserGroupId(value);

		if (value != null)
			EntityManager.getBroker().add(newGroup);

		reload();

	}

	public void deleteGroup() {
		logger.logInfo("deleteGroup() started...");

		MemsNolTypeView selectedMemsNolTypeView = (MemsNolTypeView) sessionMap
				.get("selectedMemsNolTypeView");

		MemsNolTypes memsNolType = EntityManager.getBroker().findById(
				MemsNolTypes.class, selectedMemsNolTypeView.getMemsNolTypeId());

		UserGroup selectedUserGroup = (UserGroup) dataTable.getRowData();

		MemsNolTypeUsrGrp memsNolTypeUserGroupMapping = null;

		for (Object obj : selectedMemsNolTypeView
				.getMemsNolTypeUserGroupMappings()) {
			memsNolTypeUserGroupMapping = (MemsNolTypeUsrGrp) obj;
			if (memsNolTypeUserGroupMapping.getMemsNolTypes()
					.getMemsNolTypeId() == selectedMemsNolTypeView
					.getMemsNolTypeId()
					&& memsNolTypeUserGroupMapping.getUserGroupId()
							.equalsIgnoreCase(
									selectedUserGroup.getUserGroupId())) {
				break;
			}
		}

		EntityManager.getBroker().delete(memsNolTypeUserGroupMapping);

		reload();
	}

	public void reload() {

		MemsNolTypeView selectedMemsNolTypeView = (MemsNolTypeView) sessionMap
				.get("selectedMemsNolTypeView");

		MemsNolTypes memsNolType = EntityManager.getBroker().findById(
				MemsNolTypes.class, selectedMemsNolTypeView.getMemsNolTypeId());
		selectedMemsNolTypeView.setMemsNolTypeUserGroupMappings(memsNolType
				.getMemsNolTypeUsrGrps());

		sessionMap.put("selectedMemsNolTypeView", selectedMemsNolTypeView);

		getUserGroups();
	}

	private List<UserGroup> getSecUserGroups() {
		logger.logInfo("getSecUserGroups() started...");

		List<UserGroup> userGroups = null;
		try {
			Search query = new Search();
			query.clear();
			query.addFrom(UserGroupDbImpl.class);
			userGroups = SecurityManager.findGroup(query);
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getSecUserGroups() crashed ", e);
		}

		return userGroups;
	}

	public void closeWindowAndSubmit() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();

		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:closeWindowSubmit();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
	}

	public List<SelectItem> getSelectUserGroups() {
		if (viewMap.get("selectUserGroups") != null)
			selectUserGroups = (List<SelectItem>) viewMap
					.get("selectUserGroups");
		return selectUserGroups;
	}

	public void setSelectUserGroups(List<SelectItem> selectUserGroups) {
		this.selectUserGroups = selectUserGroups;
	}

	public String getSelectOneUserGroup() {
		return selectOneUserGroup;
	}

	public void setSelectOneUserGroup(String selectOneUserGroup) {
		this.selectOneUserGroup = selectOneUserGroup;
	}

	public List<UserGroup> getMemsNolTypeGroups() {
		if (viewMap.get("memsNolTypeGroups") != null)
			memsNolTypeGroups = (List<UserGroup>) viewMap
					.get("memsNolTypeGroups");
		return memsNolTypeGroups;
	}

	public void setMemsNolTypeGroups(List<UserGroup> memsNolTypeGroups) {
		this.memsNolTypeGroups = memsNolTypeGroups;
	}

	public HtmlCommandButton getBtnAddGroup() {
		return btnAddGroup;
	}

	public void setBtnAddGroup(HtmlCommandButton btnAddGroup) {
		this.btnAddGroup = btnAddGroup;
	}

	public Integer getPaginatorMaxPages() {
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null)
			recordSize = (Integer) viewMap.get("recordSize");
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

}
