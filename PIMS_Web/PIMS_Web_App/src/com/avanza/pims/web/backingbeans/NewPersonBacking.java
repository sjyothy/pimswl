package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.validator.EmailValidator;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;


public class NewPersonBacking extends AbstractController
{
	private static Logger logger = Logger.getLogger(NewPersonBacking.class);
	private String tenantType="";
	private boolean company=false;
	private HtmlPanelGrid companyPanel=new HtmlPanelGrid();
	private HtmlPanelGrid individualPanel=new HtmlPanelGrid();
	private PersonView newPerson=new PersonView();
	private ContactInfoView newPersonCInfo=new ContactInfoView();
	FacesContext context=FacesContext.getCurrentInstance();
	Map sessionMap=context.getExternalContext().getSessionMap();
	private List<String> successMessages;
	private List<String> errorMessages;
	
	public NewPersonBacking() 
	{
		
	}
	public void init()
	{
		super.init();
		if(!isPostBack())
			{
				if(sessionMap.containsKey("PERSON_TO_EDIT"))
				{
					newPerson=(PersonView) sessionMap.get("PERSON_TO_EDIT");
					this.setNewPersonCInfo(newPerson.getContactInfoViewSet().iterator().next());
					sessionMap.remove("PERSON_TO_EDIT");
					if(newPerson.getIsCompany().compareTo(1L)==0)
						setTenantType("1");
					else
						setTenantType("0");
					if(newPerson.getIsCompany().compareTo(0L)==0)
						{
							individualPanel.setRendered(true);
							companyPanel.setRendered(false);
						}
					else
						{
							individualPanel.setRendered(false);
							companyPanel.setRendered(true);
						}
				}
				else
				{
					individualPanel.setRendered(true);
					companyPanel.setRendered(false);
				}	
			}
		
		
	}
	
	public String changeTenantType(ValueChangeEvent e)
	{
		newPerson=new PersonView();
		newPersonCInfo=new ContactInfoView();
		if(e.getNewValue().toString().compareTo("1")==0)
		{
			this.setCompany(true);
			companyPanel.setRendered(true);
			individualPanel.setRendered(false);
		}
		else
		{
			this.setCompany(false);
			companyPanel.setRendered(false);
			individualPanel.setRendered(true);
		}
		
		return "";
	}
	public String getTenantType() {
		return tenantType;
	}
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	public boolean isCompany() {
		return company;
	}
	public void setCompany(boolean company) {
		this.company = company;
	}
	public HtmlPanelGrid getCompanyPanel() {
		return companyPanel;
	}
	public void setCompanyPanel(HtmlPanelGrid companyPanel) {
		this.companyPanel = companyPanel;
	}
	public HtmlPanelGrid getIndividualPanel() {
		return individualPanel;
	}
	public void setIndividualPanel(HtmlPanelGrid individualPanel) {
		this.individualPanel = individualPanel;
	}
	public PersonView getNewPerson() {
		return newPerson;
	}
	public void setNewPerson(PersonView newPerson) {
		this.newPerson = newPerson;
	}
	public ContactInfoView getNewPersonCInfo() {
		return newPersonCInfo;
	}
	public void setNewPersonCInfo(ContactInfoView newPersonCInfo) {
		this.newPersonCInfo = newPersonCInfo;
	}
	public void sendPersonToCallerBean()
	{
		if(!hasError())
		{
		Set<ContactInfoView> contactInfoViewSet =  new HashSet<ContactInfoView>(0); 
		newPerson.setPersonId(null);
		if(newPersonCInfo!=null)
			{
				contactInfoViewSet.add(newPersonCInfo);
				newPerson.setContactInfoViewSet(contactInfoViewSet);
			}
		if(company==true)
			{
				newPerson.setIsCompany(1L);
				newPerson.setPersonFullName(newPerson.getCompanyName());
			}
		else
			{
				newPerson.setIsCompany(0L);
				newPerson.setPersonFullName(newPerson.getFirstName()+" "+newPerson.getMiddleName()+" "+newPerson.getLastName());
			}
		sessionMap.put("NEW_PERSON", newPerson);
		executeJavaScript("alertTheCallerBean();");
		}
	}
	public void executeJavaScript(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}	
	public boolean hasError()
	{
		boolean error=false;
		errorMessages=new ArrayList<String>();
		EmailValidator emailValidator = EmailValidator.getInstance();
		if(this.isCompany())
			{
				if(newPerson.getCompanyName()==null || newPerson.getCompanyName().trim().length()<=0)
					error=true;
				if(newPersonCInfo.getEmail()==null || newPersonCInfo.getEmail().trim().length()<=0 )
					error=true;
				if(newPerson.getCellNumber()==null || newPerson.getCellNumber().trim().length()<=0)
					error=true;
				if(newPerson.getCompanyName()==null || newPerson.getCompanyName().trim().length()<=0)
					error=true;
				if (newPersonCInfo.getEmail() != null && newPersonCInfo.getEmail().trim().length() > 0&& !emailValidator.isValid(newPersonCInfo.getEmail())) 
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_INVALID_EMAIL)));
					error=true;
				}	
			}
		else
			{
			if(newPerson.getFirstName()==null || newPerson.getFirstName().trim().length()<=0)
				error=true;
			if(newPerson.getLastName()==null || newPerson.getLastName().trim().length()<=0)
				error=true;
			if(newPerson.getCellNumber()==null || newPerson.getCellNumber().trim().length()<=0)
				error=true;
//			if (newPersonCInfo.getEmail() != null && newPersonCInfo.getEmail().trim().length() > 0&& !
//					emailValidator.isValid(newPersonCInfo.getEmail())) 
//			{
//				errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Person.MSG_INVALID_EMAIL)));
//				empty=true;
//			}	
		}
		if(error==true)
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Tenant.MSG_FIELD_REQUIRED));
		return error;
	}
	public String getSuccessMessages() {
		return CommonUtil.getSuccessMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
}
