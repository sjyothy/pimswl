package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.InvestmentUnitHistoryView;

public class PortfolioInvestmentUnitHistory extends AbstractController {
	
	private List<InvestmentUnitHistoryView> investmentUnitHistoryViewList = new ArrayList<InvestmentUnitHistoryView>(0);
	private Logger logger = Logger.getLogger( PortfolioInvestmentUnitHistory.class );
	private Map<String,Object> viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private HtmlDataTable dataTable;
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		
		try {
			super.init();
			
			if (!isPostBack()) {
				HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
				HttpSession session = request.getSession();


				if (session.getAttribute(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST)!=null) {
					investmentUnitHistoryViewList = (List<InvestmentUnitHistoryView>) session.getAttribute(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST);
					viewRootMap.put("recordSize",investmentUnitHistoryViewList.size());
					viewRootMap.put(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST, investmentUnitHistoryViewList);
					session.removeAttribute(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST); 
				}
			}
		}	
		catch (Exception e) {
			logger.LogException("init() crashed",e);
		}
	
	}

	@SuppressWarnings("unchecked")
	public List<InvestmentUnitHistoryView> getInvestmentUnitHistoryViewList() {
		
		if(viewRootMap.containsKey(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST) && viewRootMap.get(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST)!=null) {
			investmentUnitHistoryViewList =  (List<InvestmentUnitHistoryView>) viewRootMap.get(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST);
		}
		
		return investmentUnitHistoryViewList;
	}

	public void setInvestmentUnitHistoryViewList(
			List<InvestmentUnitHistoryView> investmentUnitHistoryViewList) {
		this.investmentUnitHistoryViewList = investmentUnitHistoryViewList;
	}
	
	public String getDateFormat() {
		String dateFormat = "dd/MM/yyyy hh:mm:ss aaa";
		return dateFormat;
	}
	
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();		
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	
	public Integer getRecordSize() {
		Integer recordSize = 0;
		 
		if(viewRootMap.get("recordSize")!=null) {
			recordSize = (Integer)viewRootMap.get("recordSize");
		}	
		return recordSize;
	}

	

}
