package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.RegionDetailsView;
import com.avanza.ui.util.ResourceUtil;


public class RegionDetailsBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(RegionDetailsBacking.class);

	private transient UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	
	private RegionDetailsView region = new RegionDetailsView(); 
	
	HtmlInputText countryEnText = new HtmlInputText();
	HtmlInputText countryArText = new HtmlInputText();
	HtmlInputText cityEnText = new HtmlInputText();
	HtmlInputText cityArText = new HtmlInputText();
	HtmlInputText areaEnText = new HtmlInputText();
	HtmlInputText areaArText = new HtmlInputText();
	
	private final String PAGE_MODE ="PAGE_MODE";
	private final String POPUP_MODE ="POPUP_MODE";
	private final String MENU_MODE ="MENU_MODE";
	private final String PAGE_MODE_COUNTRY = WebConstants.RegionDetails.PAGE_MODE_COUNTRY; // initial mode where only country fields will be visible
	private final String PAGE_MODE_CITY = WebConstants.RegionDetails.PAGE_MODE_CITY; // mode where country and city fields will be visible
	private final String PAGE_MODE_AREA = WebConstants.RegionDetails.PAGE_MODE_AREA; // mode where country, city and area fields will be visible
	private String pageMode="default";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.RegionDetails.Headings.MAIN;
	
	private String heading="";

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private HtmlDataTable regionDataTable = new HtmlDataTable();
	private List<RegionDetailsView> regionList = new ArrayList<RegionDetailsView>();
	private Integer regionRecordSize = 0;
	private Integer paginatorRows = 0;
	
	// Methods
	
	public void init() {
		logger.logInfo("init() started...");
		super.init();

		try
		{
//			if(!isPostBack())
//			{
			setMode();
			setValues();
//			}
//			else{
//
//			}

			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			///
//			sessionMap.remove(POPUP_MODE);
//			sessionMap.remove(PAGE_MODE);
//			sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_ID, 693L);
//	   		sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_MODE, WebConstants.RegionDetails.EVALUATION_REQUEST_MODE_EVALUATE);
//	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
			///

			// if popup mode i.e. add country, city or area
//			if(sessionMap.get(POPUP_MODE)!=null)
//				viewMap.put(POPUP_MODE, sessionMap.get(POPUP_MODE));
//			sessionMap.remove(POPUP_MODE);
//
//			if(sessionMap.get(PAGE_MODE)!=null)
//				viewMap.put(PAGE_MODE, sessionMap.get(PAGE_MODE));
//			sessionMap.remove(PAGE_MODE);
			//
			
			String level = (String)getFacesContext().getExternalContext().getRequestParameterMap().get("level");
			
			if(StringHelper.isNotEmpty(level)){
				viewMap.put(MENU_MODE, true);
				if(level.equals("1"))
					viewMap.put(PAGE_MODE, PAGE_MODE_COUNTRY);
				else if(level.equals("2"))
					viewMap.put(PAGE_MODE, PAGE_MODE_CITY);
				else
					viewMap.put(PAGE_MODE, PAGE_MODE_AREA);
			}
			
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_COUNTRY; 
			
			
			viewMap.put("asterisk", "");
			/*if(pageMode.equals(PAGE_MODE_COUNTRY)){
				viewMap.put(HEADING, WebConstants.RegionDetails.Headings.COUNTRIES);
			}	
			else if(pageMode.equals(PAGE_MODE_CITY)){
				viewMap.put(HEADING, WebConstants.RegionDetails.Headings.CITIES);
			}
			else if(pageMode.equals(PAGE_MODE_AREA)){
				viewMap.put(HEADING, WebConstants.RegionDetails.Headings.AREAS);
			}*/
			
			viewMap.put(PAGE_MODE, pageMode);
				
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			
			RegionDetailsView modifiedRegion = (RegionDetailsView) sessionMap.get("modifiedRegion");
			Boolean saveSuccess = (Boolean) sessionMap.get("saveSuccess");
			if(saveSuccess!=null && modifiedRegion!=null)
			{
				Integer rowIndex = (Integer) viewMap.get("rowIndex");
				if(rowIndex!=null){
					List<RegionDetailsView> regionList = getRegionList();
					RegionDetailsView removedRegion = regionList.remove(rowIndex.intValue());
					regionList.add(rowIndex.intValue(), modifiedRegion);
					setRegionList(regionList);
				}
			}
			
			sessionMap.remove("modifiedRegion");
			sessionMap.remove("saveSuccess");
			/*DomainTypeView domainType = utilityServiceAgent.getDomainTypeByTypeName(WebConstants.REGION_TYPE);
			List<DomainDataView> domainDataList = new ArrayList<DomainDataView>();
			if(domainType.getDomainDatas()!=null){
				domainDataList.addAll(domainType.getDomainDatas());
				for(DomainDataView ddv: domainDataList){
					if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_COUNTRY))
						region.setCountryDomainDataId(ddv.getDomainDataId());
					else if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_CITY))
						region.setCityDomainDataId(ddv.getDomainDataId());
					else if(ddv.getDataValue().equals(WebConstants.RegionLevels.REGION_TYPE_AREA))
						region.setAreaDomainDataId(ddv.getDomainDataId());
				}
			}*/
			
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}
	
	@SuppressWarnings("unchecked")
	public void openAddCountryPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openAddCountryPopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";

	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_COUNTRY);
			region.setCountryEn(null);
			region.setCountryAr(null);
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, region);
			List<RegionDetailsView> countryList = (List<RegionDetailsView>) viewMap.get("countryList");
			if(countryList!=null)
				sessionMap.put("countryList", countryList);
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openAddCountryPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAddCountryPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void openAddCityPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openAddCityPopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";
	
	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_CITY);
			region.setCityEn(null);
			region.setCityAr(null);
			Long countryId = (Long) viewMap.get("countryId");
			region.setCountryId(countryId);
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, viewMap.get("currentRegion"));
//			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, region);
			sessionMap.put("countryId", countryId);
			List<RegionDetailsView> cityList = (List<RegionDetailsView>) viewMap.get("cityList");
			if(cityList!=null)
				sessionMap.put("cityList", cityList);
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openAddCityPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAddCityPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void openAddAreaPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openAddAreaPopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";
	
	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_AREA);
			region.setAreaEn(null);
			region.setAreaAr(null);
			Long cityId = (Long) viewMap.get("cityId");
			region.setCityId(cityId);
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, region);
			List<RegionDetailsView> areaList = (List<RegionDetailsView>) viewMap.get("areaList");
			if(areaList!=null)
				sessionMap.put("areaList", areaList);
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openAddAreaPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAddAreaPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void deleteCountry(javax.faces.event.ActionEvent event){
		logger.logInfo("deleteCountry() started...");
		try
		{
			RegionDetailsView countryView = (RegionDetailsView)regionDataTable.getRowData();
			Long countryId = countryView.getCountryId();
			Boolean success = true;
			try{
				success = utilityServiceAgent.deleteRegion(countryId, CommonUtil.getLoggedInUser());
			}
			catch (PimsBusinessException pimsException) {
				success = false;
				logger.LogException("deleteCountry() crashed ", pimsException);
			}
			if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_SUCCESS);
	   			int index = regionDataTable.getRowIndex();
	   			List<RegionDetailsView> countryList = (List<RegionDetailsView>) viewMap.get("countryList");
	   			RegionDetailsView temp = countryList.remove(index);
	   	    	viewMap.put("countryList",countryList);
	   	    	viewMap.put("regionRecordSize",countryList.size());
	   	    	region.setCountryEn(null);
	   	    	region.setCountryAr(null);
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_FAILURE));
	   		
			logger.logInfo("deleteCountry() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("deleteCountry() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void deleteCity(javax.faces.event.ActionEvent event){
		logger.logInfo("deleteCity() started...");
		try
		{
			RegionDetailsView cityView = (RegionDetailsView)regionDataTable.getRowData();
			Long cityId = cityView.getCityId();
			Boolean success = true;
			try{
				success = utilityServiceAgent.deleteRegion(cityId, CommonUtil.getLoggedInUser());
			}
			catch (PimsBusinessException pimsException) {
				success = false;
				logger.LogException("deleteCity() crashed ", pimsException);
			}
			if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_SUCCESS);
	   			int index = regionDataTable.getRowIndex();
	   			List<RegionDetailsView> cityList = (List<RegionDetailsView>) viewMap.get("cityList");
	   			RegionDetailsView temp = cityList.remove(index);
	   	    	viewMap.put("cityList",cityList);
	   	    	viewMap.put("regionRecordSize",cityList.size());
	   	    	region.setCityEn(null);
	   	    	region.setCityAr(null);
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_FAILURE));
	   		
			logger.logInfo("deleteCity() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("deleteCity() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void deleteArea(javax.faces.event.ActionEvent event){
		logger.logInfo("deleteArea() started...");
		try
		{
			RegionDetailsView areaView = (RegionDetailsView)regionDataTable.getRowData();
			Long areaId = areaView.getAreaId();
			Boolean success = true;
			try{
				success = utilityServiceAgent.deleteRegion(areaId, CommonUtil.getLoggedInUser());
			}
			catch (PimsBusinessException pimsException) {
				success = false;
				logger.LogException("deleteArea() crashed ", pimsException);
			}
			if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_SUCCESS);
	   			int index = regionDataTable.getRowIndex();
	   			List<RegionDetailsView> areaList = (List<RegionDetailsView>) viewMap.get("areaList");
	   			RegionDetailsView temp = areaList.remove(index);
	   	    	viewMap.put("cityList",areaList);
	   	    	viewMap.put("regionRecordSize",areaList.size());
	   	    	region.setAreaEn(null);
	   	    	region.setAreaAr(null);
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_DELETE_FAILURE));
	   		
			logger.logInfo("deleteArea() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("deleteArea() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void editCountry(javax.faces.event.ActionEvent event){
		logger.logInfo("editCountry() started...");
		try
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";
	        
			RegionDetailsView countryView = (RegionDetailsView)regionDataTable.getRowData();
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, countryView);
	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_COUNTRY);
			sessionMap.put("editMode", true);
			viewMap.put("rowIndex", regionDataTable.getRowIndex());
			
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	   		
			logger.logInfo("editCountry() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editCountry() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void editCity(javax.faces.event.ActionEvent event){
		logger.logInfo("editCity() started...");
		try
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";
	        
			RegionDetailsView cityView = (RegionDetailsView)regionDataTable.getRowData();
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, cityView);
	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_CITY);
			sessionMap.put("editMode", true);
			
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	   		
			logger.logInfo("editCity() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editCity() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void editArea(javax.faces.event.ActionEvent event){
		logger.logInfo("editArea() started...");
		try
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddRegionPopup();";
	        
			RegionDetailsView areaView = (RegionDetailsView)regionDataTable.getRowData();
			sessionMap.put(WebConstants.RegionDetails.REGION_VIEW, areaView);
	        sessionMap.put(POPUP_MODE, true);
			sessionMap.put(PAGE_MODE, PAGE_MODE_AREA);
			sessionMap.put("editMode", true);
			
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	   		
			logger.logInfo("editArea() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editArea() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public String openCities(){
		logger.logInfo("openCities() started...");
		try
		{
			RegionDetailsView countryView = (RegionDetailsView)regionDataTable.getRowData();
//			region.setCountryEn(countryView.getCountryEn());
//			region.setCountryAr(countryView.getCountryAr());
			viewMap.put("countryEn", countryView.getCountryEn());
			viewMap.put("countryAr", countryView.getCountryAr());
			viewMap.put("countryId", countryView.getCountryId());
			
			viewMap.put("currentRegion", countryView);
			
			countryView.setRegionLevel(WebConstants.RegionLevels.CITY_LEVEL);
			List<RegionDetailsView> cities = utilityServiceAgent.getChildRegions(countryView.getCountryId());
			viewMap.put("cityList", cities);
			viewMap.put("regionRecordSize", cities.size());
	   		pageMode = PAGE_MODE_CITY;
	   		viewMap.put(PAGE_MODE, pageMode);
	        logger.logInfo("openCities() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openCities() crashed ", exception);
		}
		return "cities";
    }
	
	@SuppressWarnings("unchecked")
	public String openAreas(){
		logger.logInfo("openAreas() started...");
		try
		{
			RegionDetailsView cityView = (RegionDetailsView)regionDataTable.getRowData();
//			region.setCountryEn(cityView.getCountryEn());
//			region.setCountryAr(cityView.getCountryAr());
//			region.setCityEn(cityView.getCityEn());
//			region.setCityAr(cityView.getCityAr());
			viewMap.put("countryEn", cityView.getCountryEn());
			viewMap.put("countryAr", cityView.getCountryAr());
			viewMap.put("cityEn", cityView.getCityEn());
			viewMap.put("cityAr", cityView.getCityAr());
			viewMap.put("countryId", cityView.getCountryId());
			viewMap.put("cityId", cityView.getCityId());
			
			viewMap.put("currentRegion", cityView);
			
			cityView.setRegionLevel(WebConstants.RegionLevels.AREA_LEVEL);
			List<RegionDetailsView> areas = utilityServiceAgent.getChildRegions(cityView.getCityId());
			viewMap.put("areaList", areas);
			viewMap.put("regionRecordSize", areas.size());
	   		pageMode = PAGE_MODE_AREA;
	   		viewMap.put(PAGE_MODE, pageMode);
	   		
	        logger.logInfo("openAreas() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAreas() crashed ", exception);
		}
		return "areas";
    }

	@SuppressWarnings("unchecked")
	public String back(){
		logger.logInfo("back() started...");
		try
		{
			if(getIsCityMode()){
				pageMode = PAGE_MODE_COUNTRY;
				viewMap.put(HEADING, WebConstants.RegionDetails.Headings.COUNTRIES);
	    	}
	    	else if(getIsAreaMode()){
	    		pageMode = PAGE_MODE_CITY;
	    		viewMap.put(HEADING, WebConstants.RegionDetails.Headings.CITIES);
	    	}
			viewMap.put(PAGE_MODE, pageMode);
			viewMap.put("regionRecordSize", getRegionList().size());
			
	        logger.logInfo("back() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("back() crashed ", exception);
		}
		return "areas";
    }
	
	@SuppressWarnings("unchecked")
	public Boolean validateForSearch() {
	    logger.logInfo("validateForSearch() started...");
	    Boolean validated = false;
	    try
		{
	    	RegionDetailsView temp = (RegionDetailsView) viewMap.get("currentRegion");
	    	
	    	if(temp==null)
	    		temp = new RegionDetailsView();
	    	
	    	if(getIsCountryMode()){
		    	if(StringHelper.isNotEmpty(region.getCountryEn().trim()))
		    	{
		    		validated = true;
		    	}
		    	if(StringHelper.isNotEmpty(region.getCountryAr().trim()))
		    	{
		    		validated = true;
		    	}
		    	temp.setCountryEn(region.getCountryEn());
		    	temp.setCountryAr(region.getCountryAr());
	    	}
	    	else if(getIsCityMode()){
		    	if(StringHelper.isNotEmpty(region.getCityEn().trim()))
		    	{
		    		validated = true;
		    	}
		    	if(StringHelper.isNotEmpty(region.getCityAr().trim()))
		    	{
		    		validated = true;
		    	}
		    	temp.setCityEn(region.getCityEn());
		    	temp.setCityAr(region.getCityAr());
	    	}
	    	else if(getIsAreaMode()){
		    	if(StringHelper.isNotEmpty(region.getAreaEn().trim()))
		    	{
		    		validated = true;
		    	}
		    	if(StringHelper.isNotEmpty(region.getAreaAr().trim()))
		    	{
		    		validated = true;
		    	}
		    	temp.setAreaEn(region.getAreaEn());
		    	temp.setAreaAr(region.getAreaAr());
	    	}
	    	
	    	region = temp;
	    	
			logger.logInfo("validateForSearch() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForSearch() crashed ", exception);
		}
//		return validated;
		return true;
	}
    
	public String searchRegions()
    {
    	logger.logInfo("searchRegions() started...");
    	try{
    		if(validateForSearch()){
    			loadRegionList();
    		}
			else
    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
    			
    		logger.logInfo("searchRegions() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("searchAuctions() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("searchRegions() crashed ",exception);
    	}
        return "searchRegions";
    }
	
	private void loadRegionList() throws PimsBusinessException{
		logger.logInfo("loadRegionList() started...");
    	try{
    		regionList = new ArrayList<RegionDetailsView>();
			
			if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
				region.setRegionLevel(WebConstants.RegionLevels.COUNTRY_LEVEL);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
				region.setRegionLevel(WebConstants.RegionLevels.CITY_LEVEL);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
				region.setRegionLevel(WebConstants.RegionLevels.AREA_LEVEL);

    		Map argMap = getArgMap();
			regionList = utilityServiceAgent.getRegions(region, argMap);
			
			if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
				viewMap.put("countryList", regionList);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
				viewMap.put("cityList", regionList);
			else if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
				viewMap.put("areaList", regionList);
			
			regionRecordSize = regionList.size();
			viewMap.put("regionRecordSize", regionRecordSize);
			if(regionList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadRegionList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadRegionList() crashed ",exception);
			throw exception;
		}
		catch(Exception exception){
			logger.LogException("loadRequestList() crashed ",exception);
		}
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_COUNTRY;
			
			String countryEn = (String) viewMap.get("countryEn");
			String countryAr = (String) viewMap.get("countryAr");
			String cityEn = (String) viewMap.get("cityEn");
			String cityAr = (String) viewMap.get("cityAr");
			
			if(pageMode.equals(PAGE_MODE_COUNTRY)){
				countryEnText.setReadonly(false);
				countryArText.setReadonly(false);
			}
			else if(pageMode.equals(PAGE_MODE_CITY)){
				countryEnText.setReadonly(true);
				countryArText.setReadonly(true);
				cityEnText.setReadonly(false);
				cityArText.setReadonly(false);
				
				countryEnText.setValue(countryEn);
				countryArText.setValue(countryAr);
			}
			else if(pageMode.equals(PAGE_MODE_AREA)){
				countryEnText.setReadonly(true);
				countryArText.setReadonly(true);
				cityEnText.setReadonly(true);
				cityArText.setReadonly(true);
				areaEnText.setReadonly(false);
				areaArText.setReadonly(false);
				
				countryEnText.setValue(countryEn);
				countryArText.setValue(countryAr);
				cityEnText.setValue(cityEn);
				cityArText.setValue(cityAr);
			}
			
			logger.logInfo("pageMode : %s", pageMode);
			heading = getHeading();
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		argMap.put("editMode", false);
		return argMap;
	}
	

    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		if(pageMode.equals(PAGE_MODE_COUNTRY)){
			heading = WebConstants.RegionDetails.Headings.COUNTRIES;
		}	
		else if(pageMode.equals(PAGE_MODE_CITY)){
			heading = WebConstants.RegionDetails.Headings.CITIES;
		}
		else if(pageMode.equals(PAGE_MODE_AREA)){
			heading = WebConstants.RegionDetails.Headings.AREAS;
		}
		/*heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);*/
		return CommonUtil.getBundleMessage(heading);
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	public Boolean getShowCountry() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getShowCity() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getShowArea() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}
	
	public Boolean getIsCountryMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return true;
		return false;
	}

	
	public Boolean getIsCityMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return true;
		return false;
	}

	public Boolean getIsAreaMode() {
		if (pageMode.equalsIgnoreCase("default"))
			return true;
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return true;
		return false;
	}

	public Boolean getCountryReadonly() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			return false;
		return true;
	}
	
	public Boolean getCityReadonly() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			return false;
		return true;
	}
	
	public Boolean getAreaReadonly() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			return false;
		return true;
	}

	public Boolean getIsViewModePopUp(){
//		Boolean popupMode = (Boolean)viewMap.get(POPUP_MODE);
//		return (popupMode==null)?false:popupMode;
		return false;
	}

	public RegionDetailsView getRegion() {
		return region;
	}

	public void setRegion(RegionDetailsView region) {
		this.region = region;
	}

	public HtmlDataTable getRegionDataTable() {
		return regionDataTable;
	}

	public void setRegionDataTable(HtmlDataTable regionDataTable) {
		this.regionDataTable = regionDataTable;
	}

	public List<RegionDetailsView> getRegionList() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_COUNTRY))
			regionList = (List<RegionDetailsView>)viewMap.get("countryList");
		else if (pageMode.equalsIgnoreCase(PAGE_MODE_CITY))
			regionList = (List<RegionDetailsView>)viewMap.get("cityList");
		else if (pageMode.equalsIgnoreCase(PAGE_MODE_AREA))
			regionList = (List<RegionDetailsView>)viewMap.get("areaList");
		
		if(regionList==null)
			regionList = new ArrayList<RegionDetailsView>();
		
		regionRecordSize = regionList.size();
		return regionList;
	}

	public void setRegionList(List<RegionDetailsView> regionList) {
		this.regionList = regionList;
	}

	public Integer getRegionRecordSize() {
		regionRecordSize = (Integer) viewMap.get("regionRecordSize");
		regionRecordSize = regionRecordSize==null?0:regionRecordSize;
		return regionRecordSize;
	}

	public Boolean getIsMenuMode(){
		Boolean menuMode = (Boolean) viewMap.get(MENU_MODE);
		return menuMode==null?false:menuMode;
	}
	
	public String getReadonlyStyleClassCountry(){
		if(pageMode.equals(PAGE_MODE_COUNTRY))
			return "";
		return WebConstants.READONLY_STYLE_CLASS;
	}
	
	public String getReadonlyStyleClassCity(){
		if(pageMode.equals(PAGE_MODE_CITY))
			return "";
		return WebConstants.READONLY_STYLE_CLASS;
	}
	
	public String getReadonlyStyleClassArea(){
		if(pageMode.equals(PAGE_MODE_AREA))
			return "";
		return WebConstants.READONLY_STYLE_CLASS;
	}
	
	public void setRegionRecordSize(Integer regionRecordSize) {
		this.regionRecordSize = regionRecordSize;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	public HtmlInputText getCountryEnText() {
		return countryEnText;
	}

	public void setCountryEnText(HtmlInputText countryEnText) {
		this.countryEnText = countryEnText;
	}

	public HtmlInputText getCountryArText() {
		return countryArText;
	}

	public void setCountryArText(HtmlInputText countryArText) {
		this.countryArText = countryArText;
	}

	public HtmlInputText getCityEnText() {
		return cityEnText;
	}

	public void setCityEnText(HtmlInputText cityEnText) {
		this.cityEnText = cityEnText;
	}

	public HtmlInputText getCityArText() {
		return cityArText;
	}

	public void setCityArText(HtmlInputText cityArText) {
		this.cityArText = cityArText;
	}

	public HtmlInputText getAreaEnText() {
		return areaEnText;
	}

	public void setAreaEnText(HtmlInputText areaEnText) {
		this.areaEnText = areaEnText;
	}

	public HtmlInputText getAreaArText() {
		return areaArText;
	}

	public void setAreaArText(HtmlInputText areaArText) {
		this.areaArText = areaArText;
	}
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
}

