package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.soap.rpc.RPCConstants;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.PaymentSchedule;
import com.avanza.pims.web.backingbeans.PaymentSchedule.Keys;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;




public class ProjectContractsTabBacking extends AbstractController {
	
	
	public interface ViewRootKeys {
		
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String PAYMENT_DUE_ON_MULTIPLE="PAYMENT_DUE_ON_MULTIPLE";
		public static final String PROJECT_ID="PROJECT_ID";
		public static final String  DIRECTION_TYPE_INBOUND="directionTypeInbound";
		public static final String  OWNERSHIP_TYPE="ownerShipTypeId";
		public static final String  PS_STATUS_COLLECTED="psCollected";
		public static final String  ERROR_TOTAL_VALUE_NOT_PRESENT ="ERROR_TOTAL_VALUE_NOT_PRESENT";
		
	}

	private static Logger logger = Logger.getLogger(ProjectContractsTabBacking.class);
	
	private HtmlDataTable dataTable = new HtmlDataTable();		
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();	
	private List<String> errorMessages = new ArrayList<String>();
	private List<PaymentScheduleView> paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
	private String infoMessage = "";
	FacesContext context=FacesContext.getCurrentInstance();
	
	public ProjectContractsTabBacking (){
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
   	 	if(!isPostBack()){
   	 		
   	 	}
   	 	else{
   	 		
   	 	try{	
	   	    viewMap.put(ProjectContractsTabBacking.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE,true);
	   	      
	   	    if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
	          { 
	   	    	ContractView contractView = new ContractView();
	   	    	DomainDataView paymentSubType = new DomainDataView();
	   	    	DomainDataView paymentSchStatus = new DomainDataView();
	   	    	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	   	    	paymentSchStatus = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
	   	    	paymentScheduleViewList = (List<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	   	    	sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	   	    	
	   	    	
	   	    	if(sessionMap.containsKey("CONTRACT_VIEW_FOR_DETAIL"))
	   	    	{
	   	    		contractView = (ContractView) sessionMap.get("CONTRACT_VIEW_FOR_DETAIL");
	   	    		sessionMap.remove("CONTRACT_VIEW_FOR_DETAIL");
	   	    	}
	   	    	if(sessionMap.containsKey("PAYMENT_SUB_TYPE"))
	   	    	{
	   	    		paymentSubType	= (DomainDataView) sessionMap.get("PAYMENT_SUB_TYPE");
	   	    		sessionMap.remove("PAYMENT_SUB_TYPE");
	   	    	}
	   	    		

	   	    	
	   	    	
	   	    	for(PaymentScheduleView psv: paymentScheduleViewList)
	   	    	{
	   	    		if(contractView !=null && contractView.getContractId()!=null)
	   	    		{
	   	    			psv.setContractId(contractView.getContractId());
	   	    			psv.setContractNumber(contractView.getContractNumber());
	   	    			
	   	    			if(contractView.getRequestsView()!=null && contractView.getRequestsView().size()>0 )
	   	    			{
	   	    				Iterator<RequestView> requestViewIterator = contractView.getRequestsView().iterator();
	   	    				
	   	    				if(requestViewIterator.hasNext())
	   	    				{
	   	    				  psv.setRequestId(requestViewIterator.next().getRequestId());	
	   	    				}
	   	    				
	   	    			}
	   	    		}
	   	    		if(paymentSubType!=null && paymentSubType.getDomainDataId()!=null)
	   	    		{
	   	    			psv.setPaymentScheduleSubTypeId(paymentSubType.getDomainDataId());
	   	    			psv.setPaymentScheduleSubTypeEn(paymentSubType.getDataDescEn());
	   	    			psv.setPaymentScheduleSubTypeAr(paymentSubType.getDataDescAr());
	   	    		}
	   	    		if(paymentSchStatus!=null && paymentSchStatus.getDomainDataId()!=null)
	   	    		{
	   	    			psv.setStatusId(paymentSchStatus.getDomainDataId());
	   	    			psv.setStatusEn(paymentSchStatus.getDataDescEn());
	   	    			psv.setStatusAr(paymentSchStatus.getDataDescAr());
	   	    		}
	   	    		
	   	    		
	  	          propertyServiceAgent.addPaymentScheduleByPaymentSchedule(psv);
	   	    	}
	   	    	
	   	    	viewMap.put("PAYMENT_SCHEDULE_FORM_CONTRACT_DETAIL_TAB", paymentScheduleViewList);
	          }
   	 	  }catch (Exception e) {
		
		    }   
   	 	}	
   	 	
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}

	
	@SuppressWarnings("unchecked")
	public String onView()
	{
		logger.logInfo("onView() started...");
		try
		{
			ContractView  contractView = (ContractView) dataTable.getRowData();
			Long contractId = contractView.getContractId();
			if(contractId==null)
				contractId = 0L;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractId);
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);

			String javaScriptText="var screen_width = 1024;";
			javaScriptText+="var screen_height = 450;";
			javaScriptText+="window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
			WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
			WebConstants.VIEW_MODE+"="+
			WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
			"','_blank','width='+(screen_width-190)+',height='+(screen_height-10)+',left=0,top=10,scrollbars=no,status=yes');";
			openPopUp("LeaseContract.jsf",javaScriptText);

	        logger.logInfo("onView() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("onView() crashed ", exception);
		}
		return "";
    }
	
    public String onAddDesignPayment()
    {
    	
			String methodName ="onAddDesignPayment";
			logger.logInfo(methodName+"|"+"Start..");

			try	
	    	{
				
				String paymentDueOnMultipleQueryString="";
				String directionTypeInbound="0";
				String ownerShipTypeId="";
				String totalContractValue="";
				DomainDataView ddvPaymentSubtypeDesign = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_DESIGN);
				
				ContractView contractView = (ContractView) dataTable.getRowData();

   			    	
   			   if(contractView!=null && contractView.getDesignAmount()!=null)
   			   {   
   	   			   totalContractValue ="totalContractValue="+contractView.getDesignAmount().toString();
   	   			   
   	   			   sessionMap.put("CONTRACT_VIEW_FOR_DETAIL", contractView);
   	   			   sessionMap.put("PAYMENT_SUB_TYPE", ddvPaymentSubtypeDesign);
   			    
   			    
   			    
				String javaScript ="var screen_width = 1024;"+ 
					"var screen_height = 450;"+
	            "window.open('PaymentSchedule.jsf?" +
	            totalContractValue+
	            paymentDueOnMultipleQueryString+ownerShipTypeId+
	            "&directionType="+directionTypeInbound+"','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
				AddResource addResource = AddResourceFactory.getInstance(context);
		        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScript);
		        
   			   }
				
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);

	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");
	    	
	    	return null;

		}
    
    
    public String onAddSupervisionPayment()
    {
    	
			String methodName ="onAddDesignSupervision";
			logger.logInfo(methodName+"|"+"Start..");

			try	
	    	{
				
				String paymentDueOnMultipleQueryString="";
				String directionTypeInbound="0";
				String ownerShipTypeId="";
				String totalContractValue="";
				DomainDataView ddvPaymentSubtypeSupervision = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_SUB_TYPE), WebConstants.PAYMENT_SCHEDULE_SUB_TYPE_SUPERVISION);
				
				ContractView contractView = (ContractView) dataTable.getRowData();

   			    	
   			   if(contractView!=null && contractView.getSupervisionAmount()!=null)
   			   {   
   	   			   totalContractValue ="totalContractValue="+contractView.getSupervisionAmount().toString();
   	   			   
   	   			   sessionMap.put("CONTRACT_VIEW_FOR_DETAIL", contractView);
   	   			   sessionMap.put("PAYMENT_SUB_TYPE", ddvPaymentSubtypeSupervision);
   			    
   			    
   			    
				String javaScript ="var screen_width = 1024;"+ 
					"var screen_height = 450;"+
	            "window.open('PaymentSchedule.jsf?" +
	            totalContractValue+
	            paymentDueOnMultipleQueryString+ownerShipTypeId+
	            "&directionType="+directionTypeInbound+"','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
				AddResource addResource = AddResourceFactory.getInstance(context);
		        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScript);
		        
   			   }
				
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);

	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");
	    	
	    	return null;

		}
    
    public String onAddContractAmount()
    {
    	
			String methodName ="onAddContractAmount";
			logger.logInfo(methodName+"|"+"Start..");

			try	
	    	{
				
				String paymentDueOnMultipleQueryString="";
				String directionTypeInbound="0";
				String ownerShipTypeId="";
				String totalContractValue="";
				
				
				ContractView contractView = (ContractView) dataTable.getRowData();

   			    	
   			   if(contractView!=null && contractView.getTotalAmount()!=null)
   			   {   
   	   			   totalContractValue ="totalContractValue="+contractView.getTotalAmount().toString();
   	   			   
   	   			   sessionMap.put("CONTRACT_VIEW_FOR_DETAIL", contractView);
   	   			   
   			    
   			    
   			    
				String javaScript ="var screen_width = 1024;"+ 
					"var screen_height = 450;"+
	            "window.open('PaymentSchedule.jsf?" +
	            totalContractValue+
	            paymentDueOnMultipleQueryString+ownerShipTypeId+
	            "&directionType="+directionTypeInbound+"','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
				AddResource addResource = AddResourceFactory.getInstance(context);
		        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScript);
		        
   			   }
				
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.LogException(methodName+"|"+"Error Occured..",ex);

	    	}
	    	logger.logInfo(methodName+"|"+"Finish..");
	    	
	    	return null;

		}
    
    public Boolean getOnRenderedPaymentLink()
    {
    	Boolean flage = false;
    	
    	try{
    
			DomainDataView ddContractType = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE), WebConstants.CONSULTANT_CONTRACT);
		    ContractView contractView=(ContractView)dataTable.getRowData();
			
			if(contractView.getContractTypeId().equals(ddContractType.getDomainDataId()))
			flage = true;
    	
    	}catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    	
    	
    	return flage;
    	
    }
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		final String viewId = "/transferContract.jsf";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
		if(extraJavaScript!=null && extraJavaScript.length()>0)
			javaScriptText=extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public List<ContractView> getProjectContractViewList() 
	{
		List<ContractView> projectContractViewList = (List<ContractView>)viewMap.get(WebConstants.ProjectContractsTab.PROJECT_CONTRACTS);
		if(projectContractViewList==null)
			projectContractViewList = new ArrayList<ContractView>();
		viewMap.put("recordSize", projectContractViewList.size());
		return projectContractViewList;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	public Boolean getCanAddMilestone(){
		Boolean canAddMilestone = (Boolean)viewMap.get("canAddMilestone");
		if(canAddMilestone==null)
			canAddMilestone = false;
		return canAddMilestone;
	}
	
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}

	public List<PaymentScheduleView> getPaymentScheduleViewList() {
		return paymentScheduleViewList;
	}

	public void setPaymentScheduleViewList(
			List<PaymentScheduleView> paymentScheduleViewList) {
		this.paymentScheduleViewList = paymentScheduleViewList;
	}
	
}
