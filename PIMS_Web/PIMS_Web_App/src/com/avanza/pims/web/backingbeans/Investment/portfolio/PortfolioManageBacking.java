package com.avanza.pims.web.backingbeans.Investment.portfolio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AccountServiceAgent;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.business.services.PortfolioServiceAgent;
import com.avanza.pims.business.services.RevenuesAndExpensesServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AccountTypeView;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.AssetView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExpectedRateView;
import com.avanza.pims.ws.vo.ExpenseAndRevenueView;
import com.avanza.pims.ws.vo.InvestmentUnitHistoryView;
import com.avanza.pims.ws.vo.OrderView;
import com.avanza.pims.ws.vo.PortfolioAccountTypeView;
import com.avanza.pims.ws.vo.PortfolioAssetClassView;
import com.avanza.pims.ws.vo.PortfolioAssetView;
import com.avanza.pims.ws.vo.PortfolioEvaluationView;
import com.avanza.pims.ws.vo.PortfolioExpectedRateView;
import com.avanza.pims.ws.vo.PortfolioProfitView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.ui.util.ResourceUtil;

public class PortfolioManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = -1781852127764878406L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private PortfolioServiceAgent portfolioServiceAgent;
	private OrderServiceAgent orderServiceAgent;
	private RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent;
	private AccountServiceAgent accountServiceAgent;
	
	private String pageMode;
	private PortfolioView portfolioView;
	private List<OrderView> buyingOrderViewList;
	private List<OrderView> sellingOrderViewList;
	private List<ExpenseAndRevenueView> expenseAndRevenueViewList;
	private List<PortfolioAssetClassView> portfolioAssetClassViewList;
	private List<PortfolioAssetView> portfolioAssetViewList;
	private List<PortfolioAccountTypeView> portfolioAccountTypeViewList;
	private PortfolioAccountTypeView portfolioAccountTypeView;
	private List<PortfolioExpectedRateView> portfolioExpectedRateViewList;
	private PortfolioExpectedRateView portfolioExpectedRateView;
	private List<PortfolioProfitView> portfolioProfitViewList;
	private PortfolioProfitView portfolioProfitView;
	private List<PortfolioEvaluationView> portfolioEvaluationViewList;
	private PortfolioEvaluationView portfolioEvaluationView;
	private List<InvestmentUnitHistoryView> investmentUnitHistoryViewList;
	
	private SystemParameters systemParameters;
	private UserTask userTask;
	
	private HtmlDataTable buyingOrderDataTable;
	private HtmlDataTable sellingOrderDataTable;
	private HtmlDataTable assetClassesDataTable;
	private HtmlDataTable assetDataTable;
	private HtmlDataTable accountTypeDataTable;
	private HtmlDataTable expectedRateDataTable;
	private HtmlDataTable profitDataTable;
	private HtmlDataTable evaluationDataTable;
	private String readOnlyEvaluationValue;
	private Boolean disableEvaluationValue;
	private DomainDataView ddv_PorfolioStatusNew = new DomainDataView();
	private String readOnlyUnitValue;
	private Boolean disableUnitValue;
	
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_MANAGE_PORTFOLIO;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_PORTFOLIO_MANAGE;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_MANAGE_PORTFOLIO;
	
	@SuppressWarnings("unchecked")
	public PortfolioManageBacking() 
	{
		logger = Logger.getLogger( PortfolioManageBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		portfolioServiceAgent = new PortfolioServiceAgent();
		orderServiceAgent = new OrderServiceAgent();
		revenuesAndExpensesServiceAgent = new RevenuesAndExpensesServiceAgent();
		accountServiceAgent = new AccountServiceAgent();
		
		pageMode = WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_ADD;
		portfolioView = new PortfolioView();
		buyingOrderViewList = new ArrayList<OrderView>(0);
		sellingOrderViewList = new ArrayList<OrderView>(0);
		expenseAndRevenueViewList = new ArrayList<ExpenseAndRevenueView>(0);
		portfolioAssetClassViewList = new ArrayList<PortfolioAssetClassView>(0);
		portfolioAssetViewList = new ArrayList<PortfolioAssetView>(0);
		portfolioAccountTypeViewList = new ArrayList<PortfolioAccountTypeView>(0);
		portfolioAccountTypeView = new PortfolioAccountTypeView();
		portfolioExpectedRateViewList = new ArrayList<PortfolioExpectedRateView>(0);
		portfolioExpectedRateView = new PortfolioExpectedRateView();
		portfolioProfitViewList = new ArrayList<PortfolioProfitView>(0);
		investmentUnitHistoryViewList = new ArrayList<InvestmentUnitHistoryView>(0);
		portfolioProfitView = new PortfolioProfitView();
		
		portfolioEvaluationViewList = new ArrayList<PortfolioEvaluationView>(0);
		portfolioEvaluationView = new PortfolioEvaluationView();
		
		systemParameters = SystemParameters.getInstance();
		userTask = new UserTask();
		
		buyingOrderDataTable = new HtmlDataTable();
		sellingOrderDataTable = new HtmlDataTable();
		assetClassesDataTable = new HtmlDataTable();
		assetDataTable = new HtmlDataTable();
		accountTypeDataTable = new HtmlDataTable();
		expectedRateDataTable = new HtmlDataTable();
		profitDataTable = new HtmlDataTable();
		evaluationDataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);			
			super.init();
			DomainDataView ddvPortfolioStatusNew = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Portfolio.PORTFOLIO_STATUS), WebConstants.Portfolio.PORTFOLIO_STATUS_NEW);
			viewMap.put("DDV_PORTFOLIO_STATUS_NEW", ddvPortfolioStatusNew);
			portfolioView.setStatusId( String.valueOf( ddvPortfolioStatusNew.getDomainDataId() ) );			
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException,Exception
	{
		//IF from endowment file
		if( sessionMap.get( Constant.Endowments.ENDOWMENT ) != null )
		{
			getEndowmentDetailsFromSessionMap();
		}
		// USER TASK
		if ( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		}
		else if ( sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			
			portfolioView = portfolioServiceAgent.getPortfolio( new Long( userTask.getTaskAttributes().get( WebConstants.Portfolio.TASK_ATTRIBUTE_PORTFOLIO_ID ) ) );
			
			if ( userTask.getTaskAttributes().get( WebConstants.Portfolio.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.Portfolio.TASK_TYPE_APPROVE_REJECT_PORTFOLIO ) )				
				pageMode = WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_APPROVE_REJECT;
			else if ( userTask.getTaskAttributes().get( WebConstants.Portfolio.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.Portfolio.TASK_TYPE_CLOSE_PORTFOLIO ) )
				pageMode = WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_CLOSE;
		}
		
		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}
		else if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}
		
		
		// PORTFOLIO VIEW
		if ( sessionMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW ) != null )
		{		
			portfolioView = (PortfolioView) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW );
			sessionMap.remove( WebConstants.Portfolio.PORTFOLIO_VIEW );
			CommonUtil.loadAttachmentsAndComments(portfolioView.getPortfolioId().toString());
		}		
		else if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW ) != null )
		{
			portfolioView = (PortfolioView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW );
			if ( portfolioView.getPortfolioId() != null )
				CommonUtil.loadAttachmentsAndComments(portfolioView.getPortfolioId().toString());
		}
		
		
		// BUYING ORDER VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.BUYING_ORDER_VIEW_LIST ) != null )
		{
			buyingOrderViewList = (List<OrderView>) viewMap.get( WebConstants.Portfolio.BUYING_ORDER_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			OrderView orderView = new OrderView();
			orderView.setPortfolioView(portfolioView);						
			DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);
			orderView.setOrderTypeId( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) );
			orderView.setOrderTypeEn( ddvOrderTypeBuying.getDataDescEn() );
			orderView.setOrderTypeAr( ddvOrderTypeBuying.getDataDescAr() );
			orderView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			buyingOrderViewList = orderServiceAgent.getOrderList(orderView);
		}
		
		
		// SELLING ORDER VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.SELLING_ORDER_VIEW_LIST ) != null )
		{
			sellingOrderViewList = (List<OrderView>) viewMap.get( WebConstants.Portfolio.SELLING_ORDER_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			OrderView orderView = new OrderView();
			orderView.setPortfolioView(portfolioView);
			DomainDataView ddvOrderTypeSelling = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_SELLING);
			orderView.setOrderTypeId( String.valueOf( ddvOrderTypeSelling.getDomainDataId() ) );
			orderView.setOrderTypeEn( ddvOrderTypeSelling.getDataDescEn() );
			orderView.setOrderTypeAr( ddvOrderTypeSelling.getDataDescAr() );
			orderView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			sellingOrderViewList = orderServiceAgent.getOrderList(orderView);
		}
		
		
		// EXPENSE AND REVENUE VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.EXPENSE_REVENUE_VIEW_LIST ) != null )
		{
			expenseAndRevenueViewList = (List<ExpenseAndRevenueView>) viewMap.get( WebConstants.Portfolio.EXPENSE_REVENUE_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioNumber() != null && ! portfolioView.getPortfolioNumber().equalsIgnoreCase("") )
		{		
			HashMap criteriaMap = new HashMap(0);
			criteriaMap.put( WebConstants.REVENUES_AND_EXPENSES_SEARCH_CRITERIA.PROTFOLIO, portfolioView.getPortfolioNumber() );
			expenseAndRevenueViewList = revenuesAndExpensesServiceAgent.getAllRevenueAndExpenses( criteriaMap );
			setRealizedUnrealizedReturn();
		}
		
		// PORTFOLIO ASSET CLASS VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_ASSET_CLASS_VIEW_LIST ) != null )
		{
			portfolioAssetClassViewList = (List<PortfolioAssetClassView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_ASSET_CLASS_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioAssetClassView portfolioAssetClassView = new PortfolioAssetClassView();
			portfolioAssetClassView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioAssetClassView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioAssetClassView.setPortfolioView( portfolioView );			
			portfolioAssetClassViewList = portfolioServiceAgent.getPortfolioAssetClassList(portfolioAssetClassView);
		}
		
		// PORTFOLIO ASSET VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_ASSET_VIEW_LIST ) != null )
		{
			portfolioAssetViewList = (List<PortfolioAssetView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_ASSET_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioAssetView portfolioAssetView = new PortfolioAssetView();
			portfolioAssetView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioAssetView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioAssetView.setPortfolioView( portfolioView );
			portfolioAssetViewList = portfolioServiceAgent.getPortfolioAssetList(portfolioAssetView);
		}
		
		// PORTFOLIO ACCOUNT TYPE VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW_LIST ) != null )
		{
			portfolioAccountTypeViewList = (List<PortfolioAccountTypeView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioAccountTypeView portfolioAccountTypeView = new PortfolioAccountTypeView();
			portfolioAccountTypeView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioAccountTypeView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioAccountTypeView.setPortfolioView( portfolioView );
			portfolioAccountTypeViewList = portfolioServiceAgent.getPortfolioAccountTypeList( portfolioAccountTypeView );
		}
		
		// PORTFOLIO ACCOUNT TYPE VIEW 
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW ) != null )
		{
			portfolioAccountTypeView = (PortfolioAccountTypeView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW );
		}
		
		// PORTFOLIO EXPECTED RATE VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW_LIST ) != null )
		{
			portfolioExpectedRateViewList = (List<PortfolioExpectedRateView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioExpectedRateView portfolioExpectedRateView = new PortfolioExpectedRateView();
			portfolioExpectedRateView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioExpectedRateView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioExpectedRateView.setPortfolioView( portfolioView );
			portfolioExpectedRateViewList = portfolioServiceAgent.getPortfolioExpectedRateList( portfolioExpectedRateView );
		}
		// PORTFOLIO EXPECTED RATE VIEW
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW ) != null )
		{
			portfolioExpectedRateView = (PortfolioExpectedRateView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW );
		}
		
		// PORTFOLIO PROFIT VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW_LIST ) != null )
		{
			portfolioProfitViewList = (List<PortfolioProfitView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioProfitView portfolioProfitView = new PortfolioProfitView();
			portfolioProfitView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioProfitView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioProfitView.setPortfolioView( portfolioView );
			portfolioProfitViewList = portfolioServiceAgent.getPortfolioProfitList( portfolioProfitView );
		}
		// PORTFOLIO PROFIT VIEW
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW ) != null )
		{
			portfolioProfitView = (PortfolioProfitView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW );
		}
		
		// PORTFOLIO EVALUATION VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW_LIST ) != null )
		{
			portfolioEvaluationViewList = (List<PortfolioEvaluationView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW_LIST );
		}
		else if ( portfolioView != null && portfolioView.getPortfolioId() != null )
		{
			PortfolioEvaluationView portfolioEvaluationView = new PortfolioEvaluationView();
			portfolioEvaluationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			portfolioEvaluationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			portfolioEvaluationView.setPortfolioView( portfolioView );
			portfolioEvaluationViewList = portfolioServiceAgent.getPortfolioEvaluationList( portfolioEvaluationView );
		}
		// PORTFOLIO PROFIT VIEW
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW ) != null )
		{
			portfolioEvaluationView = (PortfolioEvaluationView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW );
		}
		
		
		// INVESTMENT UNIT HISTORY VIEW LIST
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST ) != null )	{
			investmentUnitHistoryViewList = (List<InvestmentUnitHistoryView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST );
		}else if ( portfolioView != null && portfolioView.getPortfolioId() != null )	{
			InvestmentUnitHistoryView investmentUnitHistoryView = new InvestmentUnitHistoryView();

			investmentUnitHistoryView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			investmentUnitHistoryView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			investmentUnitHistoryView.setPortfolioView( portfolioView );

			investmentUnitHistoryViewList = portfolioServiceAgent.getInvestmentUnitHistoryList(investmentUnitHistoryView);
		}

		
		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void getEndowmentDetailsFromSessionMap() throws Exception
	{
		Endowment endowment = (Endowment) sessionMap.remove(Constant.Endowments.ENDOWMENT);
		viewMap.put( Constant.EndowmentFile.ENDOWMENT_FILE_ID, sessionMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ID) );
		if( endowment.getPortfolioId() != null && endowment.getPortfolioId() > 0 )
		{
			portfolioView = portfolioServiceAgent.getPortfolio( endowment.getPortfolioId() );
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW , portfolioView);
			
			
			return;
		}
		
		portfolioView.setEndowmentId(  endowment.getEndowmentId().toString() );
		portfolioView.setEndowmentRef( endowment.getEndowmentNum() );
		portfolioView.setPortfolioNameEn(endowment.getEndowmentName());
		portfolioView.setPortfolioNameAr(endowment.getEndowmentName());
		if( endowment.getFielDetials1() != null && endowment.getFielDetials1().trim().length() > 0  )
		{
			portfolioView.setTotalUnits( endowment.getFielDetials1() );
		}
		if( endowment.getFielDetials2() != null && endowment.getFielDetials2().trim().length() > 0  )
		{
			portfolioView.setUnitPrice( endowment.getFielDetials2() );
		}
	}
	private void updateValuesToMaps() 
	{
		// USER TASK
		if ( userTask != null )
		{
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		}
		
		
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		
		
		// PORTFOLIO VIEW
		if ( portfolioView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW, portfolioView );
		}
		

		// BUYING ORDER VIEW LIST
		if ( buyingOrderViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.BUYING_ORDER_VIEW_LIST, buyingOrderViewList );
		}
		
		
		// SELLING ORDER LIST
		if ( sellingOrderViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.SELLING_ORDER_VIEW_LIST, sellingOrderViewList );
		}
		
		
		// EXPENSE AND REVENUE VIEW LIST
		if ( expenseAndRevenueViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.EXPENSE_REVENUE_VIEW_LIST, expenseAndRevenueViewList );
		}
		
		
		// PORTFOLIO ASSET CLASS VIEW LIST
		if ( portfolioAssetClassViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_ASSET_CLASS_VIEW_LIST, portfolioAssetClassViewList );
		}
		
		
		// PORTFOLIO ASSET VIEW LIST
		if ( portfolioAssetViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_ASSET_VIEW_LIST, portfolioAssetViewList );
		}
		
		// PORTFOLIO ACCOUNT TYPE VIEW LIST
		if ( portfolioAccountTypeViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW_LIST, portfolioAccountTypeViewList );
		}
		
		// PORTFOLIO ACCOUNT TYPE VIEW 
		if ( portfolioAccountTypeView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_ACCOUNT_TYPE_VIEW, portfolioAccountTypeView );
		}
		
		// PORTFOLIO EXPECTED RATE VIEW LIST
		if ( portfolioExpectedRateViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW_LIST, portfolioExpectedRateViewList );
		}
		
		// PORTFOLIO EXPECTED RATE VIEW
		if ( portfolioExpectedRateView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_EXPECTED_RATE_VIEW, portfolioExpectedRateView );
		}
		// PORTFOLIO PROFIT VIEW LIST
		if ( portfolioProfitViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW_LIST, portfolioProfitViewList );
		}
		
		// PORTFOLIO PROFIT VIEW
		if ( portfolioProfitView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_PROFIT_VIEW, portfolioProfitView );
		}
		
		// PORTFOLIO EVALUATION VIEW LIST
		if ( portfolioEvaluationViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW_LIST, portfolioEvaluationViewList );
		}
		
		// PORTFOLIO EVALUATION VIEW
		if ( portfolioEvaluationView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_EVALUATION_VIEW, portfolioEvaluationView );
		}
		
		// INVESTMENT UNIT HISTORY VIEW LIST
		if ( investmentUnitHistoryViewList != null ) {
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST, investmentUnitHistoryViewList );
		}
	}
	@SuppressWarnings("unused")
	public String onSave()
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{	
			if(validate())
			{	
				isSuccess = savePortfolio( portfolioView );
				isSuccess = savePortfolioAccountTypeList( portfolioAccountTypeViewList );
				isSuccess = savePortfolioExpectedRateList( portfolioExpectedRateViewList );
				isSuccess = savePortfolioProfitList( portfolioProfitViewList );
				isSuccess = savePortfolioEvaluationList( portfolioEvaluationViewList );
				isSuccess = savePortfolioAssetClassList ( portfolioAssetClassViewList );
				isSuccess = savePortfolioAssetList(portfolioAssetViewList);
				isSuccess = saveOrderList( buyingOrderViewList );
				isSuccess = saveOrderList( sellingOrderViewList );
				
				if( isSuccess )
				{
					pageMode = WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_EDIT;
					updateValuesToMaps();
					CommonUtil.loadAttachmentsAndComments(portfolioView.getPortfolioId().toString());
					Boolean attachSuccess = CommonUtil.saveAttachments(portfolioView.getPortfolioId());
					Boolean commentSuccess = CommonUtil.saveComments(portfolioView.getPortfolioId(), noteOwner);
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_SAVE_SUCCESS ) );
					logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
				}
			}	
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_SAVE_ERROR ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}

	@SuppressWarnings("unused")
	public String onBack()
	{
		
		try	
		{	
			 if( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ID ) != null )
			 {
				 sessionMap.put(
						 		Constant.EndowmentFile.ENDOWMENT_FILE_ID , 
						  		viewMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ID ) 
						  	   );
				 return "endowment";
			 }
			 
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_SAVE_ERROR ) );
			logger.LogException( "onBack --- CRASHED --- ", exception);			
		}
		
		return "";
	}

	private Boolean validate()
	{
		Boolean flage  = true;
		
		if(portfolioView.getPortfolioTypeId()==null ||  portfolioView.getPortfolioTypeId().equals("") || portfolioView.getPortfolioTypeId().equals("-1"))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioType"));
		  flage = false;
		}
		if(portfolioView.getCreationDate()==null ||  portfolioView.getCreationDate().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioCreationDate"));
		  flage = false;
		}
		if(portfolioView.getPortfolioNameEn()==null ||  portfolioView.getPortfolioNameEn().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioNameEn"));
		  flage = false;
		}
		if(portfolioView.getPortfolioNameAr()==null ||  portfolioView.getPortfolioNameAr().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioNameAr"));
		  flage = false;
		}
		if(portfolioView.getMarketSector()==null ||  portfolioView.getMarketSector().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioMarketSector"));
		  flage = false;
		}
		if(portfolioView.getAccountTypeId()==null ||  portfolioView.getAccountTypeId().equals("")|| portfolioView.getAccountTypeId().equals("-1"))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioAccountType"));
		  flage = false;
		}
		if(portfolioView.getPrincipalAmount()==null || !(portfolioView.getPrincipalAmount().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioPrincipalAmount"));
		  flage = false;
		}
		
		if ( portfolioView.getTotalUnits() == null || portfolioView.getTotalUnits().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.total.unit") ) );
			flage = false;
		}
		
		else
		{
		try{			
			
			Long.parseLong(portfolioView.getTotalUnits().toString());

			
			}
			catch (NumberFormatException e) {
				flage = false;
				errorMessages.add(ResourceUtil.getInstance().getProperty("portfolioManage.totalUnitsNumber"));

			}
			
		}
		
		
		if ( portfolioView.getOriginalAmount() == null || portfolioView.getOriginalAmount().equalsIgnoreCase("") )	{
			//For mandatory/required check
		}
		
		else
		{
		try{			
			
			Double.parseDouble(portfolioView.getOriginalAmount().toString());

			}
			catch (NumberFormatException e) {
				flage = false;
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.original.amount") ) );
			}
			
		}
		
		if ( portfolioView.getExchangeRate() == null || portfolioView.getExchangeRate().equalsIgnoreCase("") )	{
			//For mandatory/required check
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("payment.exchangeRate") ) );
			flage = false;
		}
		
		else
		{
		try{			
			
			Double.parseDouble(portfolioView.getExchangeRate().toString());

			}
			catch (NumberFormatException e) {
				flage = false;
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("payment.exchangeRate") ) );
			}
			
		}
		
		
		if ( portfolioView.getCurrencyId() == null || portfolioView.getCurrencyId().equalsIgnoreCase("") )	{
			//For mandatory/required check
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("assetSearch.currency") ) );
			flage = false;
		}
		
		else {
			if (portfolioView.getCurrencyId().equalsIgnoreCase("-1")) {
				flage = false;
			  	errorMessages.add(CommonUtil.getBundleMessage("assetManage.msg.assetCurrency"));
			}
		}	
		
		
		
		if ( portfolioView.getUnitPrice() == null || portfolioView.getUnitPrice().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.unit.price") ) );
			flage = false;
		}
		
		if ( portfolioView.getPortfolioFees() == null || portfolioView.getPortfolioFees().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.fees") ) );
			flage = false;
		}
		
		if ( portfolioView.getFundsDurationStartDate() == null )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("commons.startDate") ) );
			flage = false;
		}
		
//		if ( portfolioView.getFundsDurationEndDate() == null )
//		{
//			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("commons.endDate") ) );
//			flage = false;
//		}
		
       try{
			
		if(portfolioView.getPrincipalAmount()!=null && portfolioView.getPrincipalAmount().toString().trim().length()>0)
		Double.parseDouble(portfolioView.getPrincipalAmount().toString());

		}
		catch (NumberFormatException e) {
			flage = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty("portfolioManage.portfolioPrincipalAmountNumber"));

		}
		if(portfolioView.getBudgetRateOfReturn()==null || !(portfolioView.getBudgetRateOfReturn().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioRateOfReturn"));
		  flage = false;
		}
       try{
			
		if(portfolioView.getBudgetRateOfReturn()!=null && portfolioView.getBudgetRateOfReturn().toString().trim().length()>0)
		Double.parseDouble(portfolioView.getBudgetRateOfReturn().toString());

		}
		catch (NumberFormatException e) {
			flage = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty("portfolioManage.portfolioRateOfReturnNumber"));

		}
		if(portfolioView.getPortfolioDescEn()==null ||  portfolioView.getPortfolioDescEn().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioDescriptionEn"));
		  flage = false;
		}
		if(portfolioView.getPortfolioDescAr()==null ||  portfolioView.getPortfolioDescAr().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("portfolioManage.portfolioDescriptionAr"));
		  flage = false;
		}
		
		
		Double totalPercentage = 0.00D;
		for ( PortfolioAccountTypeView portfolioAccountTypeView : portfolioAccountTypeViewList )
		{
			if ( portfolioAccountTypeView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
			{	
				totalPercentage += portfolioAccountTypeView.getAccountPercentage();
			}
		}
		if ( ! totalPercentage.equals( 100D ) )
		{
			errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.percentage.equal"));
			flage = false;			
		}
		
		if ( getExpectedRateRecordSize() == 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage("expectedRate.not.found") );
			flage = false;
		}
		
		//CHANGE REQUEST - Asset List is now mandatory - as per required by portfolio detail report
		if (portfolioAssetViewList==null || portfolioAssetViewList.size()==0) {
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.assets.heading") ) );
			flage = false;
		}
		
		//CHANGE REQUEST - Asset Class List is now mandatory  - as per required by portfolio detail report
		if (portfolioAssetClassViewList==null || portfolioAssetClassViewList.size()==0) {
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.assetClasses.heading") ) );
			flage = false;
		}
		
		//CHANGE REQUEST - Evaluation List is now mandatory - as per required by portfolio detail report 
		if (portfolioEvaluationViewList==null || portfolioEvaluationViewList.size()==0) {
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.portfolio.evaluation") ) );
			flage = false;
		}

		
		
		
		return flage;
	}
	
	private Boolean savePortfolio(PortfolioView portfolioView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( portfolioView.getPortfolioId() == null )
		{
			// Add Portfolio
			portfolioView.setCreatedBy( CommonUtil.getLoggedInUser() );
			portfolioView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = portfolioServiceAgent.addPortfolio( portfolioView );
		}
		else
		{
			// Update Portfolio
			portfolioView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = portfolioServiceAgent.updatePortfolio( portfolioView );
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioAccountTypeList( List<PortfolioAccountTypeView> portfolioAccountTypeViewList ) throws PimsBusinessException 
	{
		Boolean isSuccess = false;
		
		for ( PortfolioAccountTypeView portfolioAccountTypeView : portfolioAccountTypeViewList )
		{
			if ( portfolioAccountTypeView.getPortfolioAccountTypeId() == null )
			{
				// Add PortfolioAccountType
				portfolioAccountTypeView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioAccountTypeView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioAccountTypeView.setPortfolioView( portfolioView );
				isSuccess = portfolioServiceAgent.addPortfolioAccountType( portfolioAccountTypeView );
				
			}
			else if ( portfolioAccountTypeView.getIsDeleted().equals( WebConstants.IS_DELETED_TRUE ) )
			{
				// Update PortfolioAccountType
				portfolioAccountTypeView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				isSuccess = portfolioServiceAgent.updatePortfolioAccountType( portfolioAccountTypeView );
			}
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioExpectedRateList( List<PortfolioExpectedRateView> portfolioExpectedRateViewList ) throws PimsBusinessException 
	{
		Boolean isSuccess = false;
		
		for ( PortfolioExpectedRateView portfolioExpectedRateView : portfolioExpectedRateViewList )
		{
			if ( portfolioExpectedRateView.getPortfolioExpectedRateId() == null )
			{
				// Add PortfolioExpectedRate
				portfolioExpectedRateView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioExpectedRateView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioExpectedRateView.setPortfolioView( portfolioView );
				isSuccess = portfolioServiceAgent.addPortfolioExpectedRate( portfolioExpectedRateView );
			}
			else if ( portfolioExpectedRateView.getIsDeleted().equals( WebConstants.IS_DELETED_TRUE ) )
			{
				// Update PortfolioExpectedRate
				portfolioExpectedRateView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				isSuccess = portfolioServiceAgent.updatePortfolioExpectedRate( portfolioExpectedRateView );
			}
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioProfitList( List<PortfolioProfitView> portfolioProfitViewList ) throws PimsBusinessException 
	{
		Boolean isSuccess = false;
		
		for ( PortfolioProfitView portfolioProfitView : portfolioProfitViewList )
		{
			if ( portfolioProfitView.getPortfolioProfitId() == null )
			{
				// Add PortfolioExpectedRate
				portfolioProfitView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioProfitView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioProfitView.setPortfolioView( portfolioView );
				isSuccess = portfolioServiceAgent.addPortfolioProfit( portfolioProfitView );
			}
			else if ( portfolioProfitView.getIsDeleted().equals( WebConstants.IS_DELETED_TRUE ) )
			{
				// Update PortfolioExpectedRate
				portfolioProfitView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				isSuccess = portfolioServiceAgent.updatePortfolioProfit( portfolioProfitView );
			}
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioEvaluationList( List<PortfolioEvaluationView> portfolioEvaluationViewList ) throws PimsBusinessException 
	{
		Boolean isSuccess = false;
		
		for ( PortfolioEvaluationView portfolioEvaluationView : portfolioEvaluationViewList )
		{
			if ( portfolioEvaluationView.getPortfolioEvaluationId() == null )
			{
				// Add PortfolioExpectedRate
				portfolioEvaluationView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioEvaluationView.setPortfolioView( portfolioView );
				isSuccess = portfolioServiceAgent.addPortfolioEvaluation( portfolioEvaluationView );
			}
			else if ( portfolioEvaluationView.getIsDeleted().equals( WebConstants.IS_DELETED_TRUE ) )
			{
				// Update PortfolioExpectedRate
				portfolioEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				isSuccess = portfolioServiceAgent.updatePortfolioEvaluation( portfolioEvaluationView );
			}
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioAssetClassList( List<PortfolioAssetClassView> portfolioAssetClassViewList ) throws PimsBusinessException 
	{	 
		Boolean isSuccess = false;
		
		for ( PortfolioAssetClassView portfolioAssetClassView : portfolioAssetClassViewList )
		{
			if ( portfolioAssetClassView.getPortfolioAssetClassId() == null )
			{
				// Add Portfolio Asset Class
				portfolioAssetClassView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioServiceAgent.addPortfolioAssetClass(portfolioAssetClassView);
				
			}
			else
			{
				// Update Portfolio Asset Class
				portfolioAssetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioServiceAgent.updatePortfolioAssetClass(portfolioAssetClassView);
			}
		}
		
		return isSuccess;
	}
	
	private Boolean savePortfolioAssetList( List<PortfolioAssetView> portfolioAssetViewList ) throws PimsBusinessException 
	{	 
		Boolean isSuccess = false;
		
		for ( PortfolioAssetView portfolioAssetView : portfolioAssetViewList )
		{
			if ( portfolioAssetView.getPortfolioAssetId() == null )
			{
				// Add Portfolio Asset
				portfolioAssetView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioServiceAgent.addPortfolioAsset(portfolioAssetView);
				
			}
			else
			{
				// Update Portfolio Asset
				portfolioAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioServiceAgent.updatePortfolioAsset(portfolioAssetView);
			}
		}
		
		return isSuccess;
	}
	
	private Boolean saveOrderList(List<OrderView> orderViewList) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( orderViewList != null && orderViewList.size() > 0 )
		{
			for ( OrderView orderView : orderViewList  )
			{
				if ( orderView.getOrderId() == null )
				{
					// Add Order
					orderView.setCreatedBy( CommonUtil.getLoggedInUser() );
					orderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					orderView.setPortfolioView( portfolioView );
					isSuccess = orderServiceAgent.addOrder( orderView );
				}
				else
				{
					// Update Order
					orderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = orderServiceAgent.updateOrder( orderView );
				}
			}
		}
		else
		{
			isSuccess = true;
		}
		
		return isSuccess;
	}
	
	public String onCancel()
	{
		return "portfolioSearch";
	}
	
//	public String onSend()
//	{
//		String METHOD_NAME = "onSend()";
//		String path = null;
//		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
//		
//		try	
//		{	
//			PIMSManageStudiesBPELPortClient portClient = new PIMSManageStudiesBPELPortClient();
//			// PIMSManagePortfolioBPELPortClient portClient = new PIMSManagePortfolioBPELPortClient();
//			portClient.setEndpoint( systemParameters.getParameter( WebConstants.Portfolio.MANAGE_PORTFOLIO_BPEL_END_POINT ) );
//			portClient.initiate( portfolioView.getPortfolioId(), CommonUtil.getLoggedInUser(), null, null );
//			onAfterBPEL();
//			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_REQUEST_SENT_SUCCESS ) );
//			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
//		}
//		catch (Exception exception) 
//		{				
//			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
//			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
//		}
//		
//		return path;
//	}
	
	public String onTaskApprove()
	{
		String METHOD_NAME = "onTaskApprove()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCloseTask(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_REQUEST_APPROVE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskReject()
	{
		String METHOD_NAME = "onTaskReject()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCloseTask(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_REQUEST_REJECT_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	
	public String onTaskClose()
	{
		String METHOD_NAME = "onTaskClose()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectCloseTask(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_REQUEST_CLOSE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskCancel() 
	{	
		return "taskList";
	}
	
	private void approveRejectCloseTask(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception 
	{
		String METHOD_NAME = "approveRejectCloseTask()";		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");				
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
	}
	
	
	private void onAfterBPEL() throws PimsBusinessException
	{
		portfolioView = portfolioServiceAgent.getPortfolio( portfolioView.getPortfolioId() );		
		pageMode = WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_VIEW;
		updateValuesToMaps();
	}
	
	public String onOrderAddSelling() 
	{
		OrderView newOrderView = new OrderView();
		
		DomainDataView ddvOrderTypeSelling = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_SELLING);
		newOrderView.setOrderTypeId( String.valueOf( ddvOrderTypeSelling.getDomainDataId() ) );
		newOrderView.setOrderTypeEn( ddvOrderTypeSelling.getDataDescEn() );
		newOrderView.setOrderTypeAr( ddvOrderTypeSelling.getDataDescAr() );
		
		DomainDataView ddvOrderStatusNew = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_STATUS), WebConstants.Order.ORDER_STATUS_NEW );			
		newOrderView.setOrderStatusId( String.valueOf( ddvOrderStatusNew.getDomainDataId() ) );
		newOrderView.setOrderStatusEn( ddvOrderStatusNew.getDataDescEn() );
		newOrderView.setOrderStatusAr( ddvOrderStatusNew.getDataDescAr() );
		
		newOrderView.setTransactionDate( new Date() );
		
		newOrderView.setPortfolioView( portfolioView );
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, newOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW_POPUP);
		
		executeJavascript("openOrderManagePopup();");
		
		return null;
	}
	
	public String onOrderAddBuying() 
	{
		OrderView newOrderView = new OrderView();
		
		DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);
		newOrderView.setOrderTypeId( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) );
		newOrderView.setOrderTypeEn( ddvOrderTypeBuying.getDataDescEn() );
		newOrderView.setOrderTypeAr( ddvOrderTypeBuying.getDataDescAr() );		
		
		DomainDataView ddvOrderStatusNew = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_STATUS), WebConstants.Order.ORDER_STATUS_NEW );			
		newOrderView.setOrderStatusId( String.valueOf( ddvOrderStatusNew.getDomainDataId() ) );
		newOrderView.setOrderStatusEn( ddvOrderStatusNew.getDataDescEn() );
		newOrderView.setOrderStatusAr( ddvOrderStatusNew.getDataDescAr() );
		
		newOrderView.setTransactionDate( new Date() );
		
		newOrderView.setPortfolioView( portfolioView );
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, newOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW_POPUP);
		
		executeJavascript("openOrderManagePopup();");
		
		return null;
	}
	
	public String onOrderReceive()
	{
		DomainDataView ddvOrderTypeSelling = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_SELLING);
		DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);		
		OrderView receivedOrderView = (OrderView) sessionMap.get( WebConstants.Order.ORDER_VIEW );		
		sessionMap.remove( WebConstants.Order.ORDER_VIEW );
		
		List<OrderView> orderViewList = null;		
		
		if ( ddvOrderTypeBuying != null && ddvOrderTypeSelling != null && receivedOrderView != null ) 
		{
			if ( receivedOrderView.getOrderTypeId().equalsIgnoreCase( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) ) )
				orderViewList = buyingOrderViewList;
			else if ( receivedOrderView.getOrderTypeId().equalsIgnoreCase( String.valueOf( ddvOrderTypeSelling.getDomainDataId() ) ) )
				orderViewList = sellingOrderViewList;
			
			if ( receivedOrderView.getStoredHashCode() == null )
			{
				// Add Order in the List Case
				receivedOrderView.setPortfolioView( portfolioView );
				receivedOrderView.setCreatedBy( CommonUtil.getLoggedInUser() );
				receivedOrderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				receivedOrderView.setCreatedOn( new Date() );
				receivedOrderView.setUpdatedOn( new Date() );
				receivedOrderView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				receivedOrderView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				orderViewList.add( receivedOrderView );
			}
			else
			{
				// Update Order in the List Case
				for ( OrderView orderView : orderViewList )
				{
					if ( orderView.getStoredHashCode() != null && orderView.getStoredHashCode().equals( receivedOrderView.getStoredHashCode() ) )
					{	
						orderView.setStoredHashCode( null );
						orderView.setTransactionDate( receivedOrderView.getTransactionDate() );
						orderView.setTransactionTimeHH( receivedOrderView.getTransactionTimeHH() );
						orderView.setTransactionTimeMM( receivedOrderView.getTransactionTimeMM() );
						orderView.setTransactionLogId( receivedOrderView.getTransactionLogId() );
						orderView.setTransactionLogEn( receivedOrderView.getTransactionLogEn() );
						orderView.setTransactionLogAr( receivedOrderView.getTransactionLogAr() );
						orderView.setCurrentValueNav( receivedOrderView.getCurrentValueNav() );
						orderView.setTotalAmountInvestReturn( receivedOrderView.getTotalAmountInvestReturn() );
						orderView.setTotalUnitsBoughtSold( receivedOrderView.getTotalUnitsBoughtSold() );
						orderView.setTotalUnits( receivedOrderView.getTotalUnits() );
						orderView.setTotalAmount( receivedOrderView.getTotalAmount() );
						orderView.setBudgetRateReturn( receivedOrderView.getBudgetRateReturn() );
						orderView.setAdditionalExpenses( receivedOrderView.getAdditionalExpenses() );
						orderView.setDescription( receivedOrderView.getDescription() );
						orderView.setActualReturn( receivedOrderView.getActualReturn() );						
					}
				}
			}
			
			
		}
		
		updateValuesToMaps();
		
		return null;
	}
	
	public String onOrderView() 
	{
		OrderView orderView = null;
		
		if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("buying") )
			orderView = (OrderView) buyingOrderDataTable.getRowData();
		else if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("selling") )
			orderView =	(OrderView) sellingOrderDataTable.getRowData();
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, orderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW_POPUP);
		
		executeJavascript("openOrderManagePopup();");
		
		return null;
	}
	
	public String onOrderEdit() 
	{
		OrderView orderView = null;
		
		if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("buying") )
			orderView = (OrderView) buyingOrderDataTable.getRowData();
		else if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("selling") )
			orderView =	(OrderView) sellingOrderDataTable.getRowData();
		
		orderView.setStoredHashCode( orderView.hashCode() );
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, orderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT_POPUP );
		
		executeJavascript("openOrderManagePopup();");
		
		updateValuesToMaps();
		
		return null;
	}
	
	public String onOrderDelete()
	{
		HtmlDataTable dataTable = null;
		List<OrderView> orderViewList = null;
		
		if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("buying") )		
		{
			dataTable = buyingOrderDataTable;
			orderViewList = buyingOrderViewList;
		}
		else if ( getExternalContext().getRequestParameterMap().get("orderType").toString().equalsIgnoreCase("selling") )
		{
			dataTable = sellingOrderDataTable;
			orderViewList = sellingOrderViewList;
		}
		
		OrderView selectedOrderView = (OrderView) dataTable.getRowData();
		
		if ( selectedOrderView.getOrderId() == null )
			orderViewList.remove( selectedOrderView );
		else
			selectedOrderView.setIsDeleted( 1L );
		
		updateValuesToMaps();
		
		return null;
	}
	
	public Double getTotalFees() 
	{
		Double totalFees = null;
		
		for ( OrderView orderView : buyingOrderViewList )
		{
			if ( orderView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
			{
				if ( totalFees != null && orderView.getTotalAmountInvestReturn()!=null && orderView.getTotalAmountInvestReturn().trim().length()>0 )				
					totalFees += Double.parseDouble(orderView.getTotalAmountInvestReturn());				
				else if(orderView.getTotalAmountInvestReturn()!=null && orderView.getTotalAmountInvestReturn().trim().length()>0)				
					totalFees = Double.parseDouble(orderView.getTotalAmountInvestReturn());				
			}
		}
		
		return totalFees;
	}
	
	private void setRealizedUnrealizedReturn() 
	{
		DomainDataView ddvExpenseRevenueStatusRealized = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_STATUS), WebConstants.EXPENSE_AND_REVENUE_STATUS_REALIZED );
		DomainDataView ddvExpenseRevenueStatusUnrealized = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_STATUS), WebConstants.EXPENSE_AND_REVENUE_STATUS_NOT_REALIZED );
		
		for ( ExpenseAndRevenueView expenseAndRevenueView : expenseAndRevenueViewList )
		{
			if ( expenseAndRevenueView.getStatusId().equalsIgnoreCase( ddvExpenseRevenueStatusRealized.getDomainDataId().toString() )  )
			{
				if ( portfolioView.getRealizedReturn() != null )				
					portfolioView.setRealizedReturn( portfolioView.getRealizedReturn() + expenseAndRevenueView.getActualValue() );				
				else				
					portfolioView.setRealizedReturn( expenseAndRevenueView.getActualValue() );							
			}
			else if ( expenseAndRevenueView.getStatusId().equalsIgnoreCase( ddvExpenseRevenueStatusUnrealized.getDomainDataId().toString() )  )
			{
				if ( portfolioView.getUnrealizedReturn() != null )				
					portfolioView.setUnrealizedReturn( portfolioView.getUnrealizedReturn() + expenseAndRevenueView.getActualValue() );				
				else				
					portfolioView.setUnrealizedReturn( expenseAndRevenueView.getActualValue() );				
			}
		}
	}
	
	public Boolean getIsApproveRejectMode() 
	{
		Boolean isApproveRejectMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_APPROVE_REJECT ) )
			isApproveRejectMode = true;
		
		return isApproveRejectMode;
	}
	
	public Boolean getIsPortfolioApproved() 
	{
		Boolean isPortfolioApproved = false;
		DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Portfolio.PORTFOLIO_STATUS), WebConstants.Portfolio.PORTFOLIO_STATUS_APPROVED);
		
		if ( portfolioView !=null && portfolioView.getPortfolioId()!=null && portfolioView.getStatusId().compareTo(ddv.getDomainDataId().toString())==0)
			isPortfolioApproved = true;
		
		return isPortfolioApproved;
	}
	
	public Boolean getShowBackButton()
	{
		Boolean show = false;
		
		if ( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ID)!= null )
		{
			show = true;
		}
			
		
		return show;
	}
	
	public Boolean getIsCloseMode()
	{
		Boolean isCloseMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_CLOSE ) )
			isCloseMode = true;
		
		return isCloseMode;
	}
	
	public Boolean getIsAddMode()
	{
		Boolean isAddMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_ADD ) )
			isAddMode = true;
		
		return isAddMode;
	}
	
	public Boolean getIsEditMode()
	{
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_EDIT ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode()
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_VIEW ) )
			isViewMode = true;
		
		return isViewMode;
	}
	
	public Integer getBuyingOrderListRecordSize() 
	{
		int size = 0;
		
		for ( OrderView orderView : buyingOrderViewList )
			if ( orderView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		
		return size;
	}
	
	public Integer getSellingOrderListRecordSize() 
	{
		int size = 0;
		
		for ( OrderView orderView : sellingOrderViewList )
			if ( orderView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		
		return size;
	}
	
	public String onAssetAdd()
	{
		List<Long> alreadySelectedAssetList = new ArrayList<Long>(0);
		
		for ( PortfolioAssetView portfolioAssetView : portfolioAssetViewList )
		{
			if ( portfolioAssetView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
			{
				alreadySelectedAssetList.add( portfolioAssetView.getAssetView().getAssetId() );
			}
		}
		
		sessionMap.put( WebConstants.ASSET.ALREADY_SELECTED_ASSET_VIEW_LIST , alreadySelectedAssetList );
		sessionMap.put( WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_KEY, WebConstants.ASSET.ASSET_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP );
		executeJavascript("openAssetSearchPopup();");
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String onAssetReceive()
	{
		if ( sessionMap.get( WebConstants.ASSET.ASSET_VIEW_LIST ) != null )
		{
			List<AssetView> selectedAssetViewList = (List<AssetView>) sessionMap.get( WebConstants.ASSET.ASSET_VIEW_LIST );
			sessionMap.remove( WebConstants.ASSET.ASSET_VIEW_LIST );
			
			for ( AssetView assetView : selectedAssetViewList )
			{
				PortfolioAssetView portfolioAssetView = new PortfolioAssetView();
				portfolioAssetView.setPortfolioView( portfolioView );
				portfolioAssetView.setAssetView( assetView );
				portfolioAssetView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetView.setCreatedOn( new Date() );				
				portfolioAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetView.setUpdatedOn( new Date() );
				portfolioAssetView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				portfolioAssetView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				portfolioAssetViewList.add( portfolioAssetView );
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String onAssetDelete()
	{
		PortfolioAssetView portfolioAssetView = (PortfolioAssetView) assetDataTable.getRowData();
		
		if ( portfolioAssetView.getPortfolioAssetId() == null )
			portfolioAssetViewList.remove( portfolioAssetView );
		else
			portfolioAssetView.setIsDeleted( 1L );
		
		updateValuesToMaps();
		
		return null;
	}
	
	public Integer getAssetRecordSize()
	{
		int size = 0;
		
		for ( PortfolioAssetView portfolioAssetView : portfolioAssetViewList )
		{
			if ( portfolioAssetView.getIsDeleted().equals( ( WebConstants.DEFAULT_IS_DELETED ) ) )
			{
				size++;
			}
		}
		
		return size;
	}
	
	public String onAssetClassesAdd() 
	{
		List<Long> alreadySelectedAssetClassViewList = new ArrayList<Long>(0);
		for ( PortfolioAssetClassView portfolioAssetClassView : portfolioAssetClassViewList )
		{
			if ( portfolioAssetClassView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				alreadySelectedAssetClassViewList.add( portfolioAssetClassView.getAssetClassView().getAssetClassId() );
		}
		sessionMap.put( WebConstants.ASSET_CLASS.ALREADY_SELECTED_ASSET_CLASS_VIEW_LIST, alreadySelectedAssetClassViewList );		
		sessionMap.put( WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_KEY, WebConstants.ASSET_CLASS.ASSET_CLASS_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP );
		executeJavascript("openAssetClassSearchPopup();");
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String onAssetClassesReceive() 
	{
		if ( sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW_LIST ) != null )
		{
			List<AssetClassView> selectedAssetClassViewList = (List<AssetClassView>) sessionMap.get( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW_LIST );
			sessionMap.remove( WebConstants.ASSET_CLASS.ASSET_CLASS_VIEW_LIST );
			
			for ( AssetClassView assetClassView : selectedAssetClassViewList )
			{
				PortfolioAssetClassView portfolioAssetClassView = new PortfolioAssetClassView();
				portfolioAssetClassView.setPortfolioView( portfolioView );
				portfolioAssetClassView.setAssetClassView( assetClassView );
				portfolioAssetClassView.setCreatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetClassView.setCreatedOn( new Date() );				
				portfolioAssetClassView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				portfolioAssetClassView.setUpdatedOn( new Date() );
				portfolioAssetClassView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				portfolioAssetClassView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				portfolioAssetClassViewList.add( portfolioAssetClassView );
			}
			
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String onAssetClassesDelete()
	{
		PortfolioAssetClassView portfolioAssetClassView = (PortfolioAssetClassView) assetClassesDataTable.getRowData();
		
		if ( portfolioAssetClassView.getPortfolioAssetClassId() == null )
			portfolioAssetClassViewList.remove( portfolioAssetClassView );
		else
			portfolioAssetClassView.setIsDeleted(1L);
		
		updateValuesToMaps();
		
		return null;
	}
	
	public Integer getAssetClassesRecordSize()
	{
		int size = 0;
		
		for ( PortfolioAssetClassView portfolioAssetClassView : portfolioAssetClassViewList )
		{
			if ( portfolioAssetClassView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		}
		
		return size;
	}
	
	public String onAccountTypeAdd()
	{
		try
		{
			if ( validateAccountTypeAdd() )
			{	
				AccountTypeView accountTypeView = new AccountTypeView();
				accountTypeView.setAccountTypeId( Long.parseLong( portfolioAccountTypeView.getStrSelectedAccountTypeId() ) );
				accountTypeView = accountServiceAgent.getAccountTypeList( accountTypeView ).get(0);
				
				PortfolioAccountTypeView newPortfolioAccountTypeView = new PortfolioAccountTypeView();
				newPortfolioAccountTypeView.setAccountTypeView( accountTypeView );
				newPortfolioAccountTypeView.setAccountPercentage( Double.parseDouble( portfolioAccountTypeView.getStrSelectedAccountPercentage() ) );
				newPortfolioAccountTypeView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				newPortfolioAccountTypeView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				portfolioAccountTypeViewList.add( newPortfolioAccountTypeView );
				
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException(" --- Exception Occurred --- ", e);
		}
		
		return null;
	}
	
	private boolean validateAccountTypeAdd() 
	{
		boolean valid = true;
		
		if ( portfolioAccountTypeView.getStrSelectedAccountTypeId() == null || portfolioAccountTypeView.getStrSelectedAccountTypeId().equalsIgnoreCase("") )
		{
			errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.accountType"));
			valid = false;
		}
		
		if ( portfolioAccountTypeView.getStrSelectedAccountPercentage() == null || portfolioAccountTypeView.getStrSelectedAccountPercentage().equalsIgnoreCase("") )
		{
			errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.percentage"));
			valid = false;
		}
		
		if ( valid && ( Double.parseDouble( portfolioAccountTypeView.getStrSelectedAccountPercentage() ) <= 0 ) )
		{
			errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.percentage.negative"));
			valid = false;
		}
		
		if ( valid )
		{
			Map<Long, Long> alreadyAddedAccountTypeId = new HashMap<Long, Long>();
			Double totalPercentage = Double.parseDouble( portfolioAccountTypeView.getStrSelectedAccountPercentage() );
			for ( PortfolioAccountTypeView portfolioAccountTypeView : portfolioAccountTypeViewList )
			{
				if ( portfolioAccountTypeView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				{
					alreadyAddedAccountTypeId.put( portfolioAccountTypeView.getAccountTypeView().getAccountTypeId(), portfolioAccountTypeView.getAccountTypeView().getAccountTypeId() );
					totalPercentage += portfolioAccountTypeView.getAccountPercentage();
				}
			}
			
			if ( alreadyAddedAccountTypeId.get( Long.parseLong( portfolioAccountTypeView.getStrSelectedAccountTypeId() ) ) != null )
			{	
				errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.accountType.exits"));
				valid = false;
			}
			
			if ( totalPercentage > 100 )
			{
				errorMessages.add(CommonUtil.getBundleMessage("accountType.add.error.percentage.greater"));
				valid = false;
			}
		}
		
		return valid;		
	}

	public String onAccountTypeDelete()
	{
		PortfolioAccountTypeView portfolioAccountTypeView = (PortfolioAccountTypeView) accountTypeDataTable.getRowData();
		
		if ( portfolioAccountTypeView.getPortfolioAccountTypeId() == null )
			portfolioAccountTypeViewList.remove( portfolioAccountTypeView );
		else
			portfolioAccountTypeView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		updateValuesToMaps();
		
		return null;
	}
	
	public Integer getAccountTypeRecordSize()
	{
		int size = 0;
		
		for ( PortfolioAccountTypeView portfolioAccountTypeView : portfolioAccountTypeViewList )
		{
			if ( portfolioAccountTypeView.getIsDeleted().equals( ( WebConstants.DEFAULT_IS_DELETED ) ) )
			{
				size++;
			}
		}
		
		return size;
	}
	
	public Integer getExpectedRateRecordSize()
	{
		int size = 0;
		
		for ( PortfolioExpectedRateView portfolioExpectedRateView : portfolioExpectedRateViewList )
		{
			if ( portfolioExpectedRateView.getIsDeleted().equals( ( WebConstants.DEFAULT_IS_DELETED ) ) )
			{
				size++;
			}
		}
		
		return size;
	}
	
	public Integer getProfitRecordSize()
	{
		int size = 0;
		
		for ( PortfolioProfitView portfolioProfitView : portfolioProfitViewList )
		{
			if ( portfolioProfitView.getIsDeleted().equals( ( WebConstants.DEFAULT_IS_DELETED ) ) )
			{
				size++;
			}
		}
		
		return size;
	}
	
	public Integer getEvaluationRecordSize()
	{
		int size = 0;
		
		for ( PortfolioEvaluationView portfolioEvaluationView : portfolioEvaluationViewList )
		{
			if ( portfolioEvaluationView.getIsDeleted().equals( ( WebConstants.DEFAULT_IS_DELETED ) ) )
			{
				size++;
			}
		}
		
		return size;
	}
	
	public void onExpectedRateChanged()
	{
		final String METHOD_NAME = "onExpectedRateChanged()";
		
		try 
		{
			portfolioExpectedRateView.setHasPeriod( false );
			portfolioExpectedRateView.setStrExpectedRatePeriod( null );
			portfolioExpectedRateView.setStrExpectedRateValue( null );
			
			portfolioExpectedRateView.setStrExpectedRateNote(null);
			portfolioExpectedRateView.setStrExpectedRateTimes(null);
			
			
			portfolioExpectedRateView.setHasNote(false);
			portfolioExpectedRateView.setHasValue(false);
			portfolioExpectedRateView.setHasTimes(false);
			
			String selectedExpectedRateId = (String) portfolioExpectedRateView.getStrExpectedRateId();
			
			if ( selectedExpectedRateId != null && ! selectedExpectedRateId.equalsIgnoreCase("") )
			{	
				ExpectedRateView expectedRateView = new ExpectedRateView();
				expectedRateView.setExpectedRateId( selectedExpectedRateId );
				expectedRateView = portfolioServiceAgent.getExpectedRateList( expectedRateView ).get(0);
				portfolioExpectedRateView.setHasPeriod( expectedRateView.getHasPeriod().equals( WebConstants.IS_DELETED_TRUE ) );
				
				portfolioExpectedRateView.setHasNote(WebConstants.IS_DELETED_TRUE.equals(expectedRateView.getHasNote()));
				portfolioExpectedRateView.setHasValue(WebConstants.IS_DELETED_TRUE.equals(expectedRateView.getHasValue()));
				portfolioExpectedRateView.setHasTimes(WebConstants.IS_DELETED_TRUE.equals(expectedRateView.getHasTimes()));				
				
			}
			
			updateValuesToMaps();
		} 
		catch ( Exception exception ) 
		{
			logger.LogException( METHOD_NAME + " --- Exception Occurred --- ", exception);
		}
	}
	public void onEvaluationTypeChanged()
	{
		
        final String METHOD_NAME = "onEvaluationTypeChanged()";
		
		try 
		{
			DomainDataView ddvPortfolioEvaluationTypePercentage = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PORTFOLIO_EVALUATION_TYPE), WebConstants.PORTFOLIO_EVALUATION_TYPE_PERCENTAGE);
			portfolioEvaluationView.setStrEvaluationValue( null );
			portfolioEvaluationView.setStrUnitValue( null );
			portfolioEvaluationView.setStrNumberOfUnit( null );
			
			String selectedEvaluationTypeId = (String) portfolioEvaluationView.getStrEvaluationTypeId();
			
			if ( selectedEvaluationTypeId != null && ! selectedEvaluationTypeId.equalsIgnoreCase("") )
			{	
				
				if(ddvPortfolioEvaluationTypePercentage.getDomainDataId().toString().equals(selectedEvaluationTypeId))
				{
					setReadOnlyEvaluationValue("READONLY");
					setDisableEvaluationValue(true);
					
					setReadOnlyUnitValue("");					
					setDisableUnitValue(false);
				}else
				{
					setReadOnlyEvaluationValue("");
					setDisableEvaluationValue(false);
					
					setReadOnlyUnitValue("READONLY");					
					setDisableUnitValue(true);
				}
			}
			
			updateValuesToMaps();
		} 
		catch ( Exception exception ) 
		{
			logger.LogException( METHOD_NAME + " --- Exception Occurred --- ", exception);
		}
		
	}
	
	public void onCalcEvaluationValue()
	{
		
		  final String METHOD_NAME = "onCalcEvaluationValue()";
			
			try 
			{
				DomainDataView ddvPortfolioEvaluationTypePercentage = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PORTFOLIO_EVALUATION_TYPE), WebConstants.PORTFOLIO_EVALUATION_TYPE_PERCENTAGE);
				portfolioEvaluationView.setStrEvaluationValue( null );
				
				// As it was a requirement to remove evaluation typeId and set it to FIXED in all cases
				portfolioEvaluationView.setStrEvaluationTypeId( "" +CommonUtil.getDomainDataId(WebConstants.PORTFOLIO_EVALUATION_TYPE, WebConstants.PORTFOLIO_EVALUATION_TYPE_FIXED));
				
				
				String selectedEvaluationTypeId = (String) portfolioEvaluationView.getStrEvaluationTypeId();
				
				if(validateCalculateEvaluationValue())
					if ( selectedEvaluationTypeId != null && ! selectedEvaluationTypeId.equalsIgnoreCase("") )
					{	
						
						if(ddvPortfolioEvaluationTypePercentage.getDomainDataId().toString().equals(selectedEvaluationTypeId))
						{
						  portfolioEvaluationView.setStrEvaluationValue( String.valueOf( (Double.parseDouble( portfolioEvaluationView.getStrUnitValue() ) * Double.parseDouble( portfolioEvaluationView.getStrNumberOfUnit())) ) );
													
						}
					}
				
				updateValuesToMaps();
			} 
			catch ( Exception exception ) 
			{
				logger.LogException( METHOD_NAME + " --- Exception Occurred --- ", exception);
			}
	}
	
	public Boolean validateCalculateEvaluationValue()
	{
		
		Boolean flage = true;
		
	   	
		if(portfolioEvaluationView.getStrUnitValue()!=null && portfolioEvaluationView.getStrNumberOfUnit()!=null)
		{
			try
			{
				Double.parseDouble( portfolioEvaluationView.getStrUnitValue() );	
			}catch (NumberFormatException e) {
				flage = false;
				errorMessages.add(CommonUtil.getBundleMessage("portfolioEvaluationDetails.error.msg.unitValue"));
			}
			
			try
			{
				Double.parseDouble( portfolioEvaluationView.getStrNumberOfUnit() );	
			}catch (NumberFormatException e) {
				flage = false;
				errorMessages.add(CommonUtil.getBundleMessage("portfolioEvaluationDetails.error.msg.noOfUnits"));
			}
			
		}else
		{
			flage = false;
		}
		
		
		return flage;
	}
	
	public String onExpectedRateAdd()
	{
		final String METHOD_NAME = "onExpectedRateAdd()"; 
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( validateExpectedRateAdd() )
			{	
				PortfolioExpectedRateView newPortfolioExpectedRateView = new PortfolioExpectedRateView();
				
				if (portfolioExpectedRateView.getStrExpectedRateValue()!=null && !"".equalsIgnoreCase(portfolioExpectedRateView.getStrExpectedRateValue())) {
					newPortfolioExpectedRateView.setExpectedRateValue( Double.parseDouble( portfolioExpectedRateView.getStrExpectedRateValue() ) );
					newPortfolioExpectedRateView.setStrExpectedRateValue( portfolioExpectedRateView.getStrExpectedRateValue() );
				} else {
					newPortfolioExpectedRateView.setExpectedRateValue(null);
					newPortfolioExpectedRateView.setStrExpectedRateValue(null);
				}
				
				if (portfolioExpectedRateView.getStrExpectedRateTimes()!=null && !"".equalsIgnoreCase(portfolioExpectedRateView.getStrExpectedRateTimes())) {
					newPortfolioExpectedRateView.setExpectedRateTimes( Double.parseDouble( portfolioExpectedRateView.getStrExpectedRateTimes() ) );
					newPortfolioExpectedRateView.setStrExpectedRateTimes( portfolioExpectedRateView.getStrExpectedRateTimes() );
				} else {
					newPortfolioExpectedRateView.setExpectedRateTimes(null);
					newPortfolioExpectedRateView.setStrExpectedRateTimes(null);
				}
				
				if (portfolioExpectedRateView.getStrExpectedRateNote()!=null && !"".equalsIgnoreCase(portfolioExpectedRateView.getStrExpectedRateNote())) {
					newPortfolioExpectedRateView.setExpectedRateNote(portfolioExpectedRateView.getStrExpectedRateNote());
					newPortfolioExpectedRateView.setStrExpectedRateNote(portfolioExpectedRateView.getStrExpectedRateNote());
				} else {
					newPortfolioExpectedRateView.setExpectedRateNote("");
					newPortfolioExpectedRateView.setStrExpectedRateNote("");
				}
				
				if (portfolioExpectedRateView.getStrExpectedRatePeriod() != null && ! portfolioExpectedRateView.getStrExpectedRatePeriod().equalsIgnoreCase("") ){
					newPortfolioExpectedRateView.setExpectedRatePeriod( Double.parseDouble( portfolioExpectedRateView.getStrExpectedRatePeriod() ) );
					newPortfolioExpectedRateView.setStrExpectedRatePeriod( portfolioExpectedRateView.getStrExpectedRatePeriod() );
				} else {
					newPortfolioExpectedRateView.setExpectedRatePeriod( null );
					newPortfolioExpectedRateView.setStrExpectedRatePeriod( null );
				}
				
				newPortfolioExpectedRateView.setCreatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioExpectedRateView.setCreatedOn( new Date() );
				newPortfolioExpectedRateView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioExpectedRateView.setUpdatedOn( new Date() );
				newPortfolioExpectedRateView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				newPortfolioExpectedRateView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				newPortfolioExpectedRateView.setPortfolioView( portfolioView );				
				
				ExpectedRateView expectedRateView = new ExpectedRateView();
				expectedRateView.setExpectedRateId( portfolioExpectedRateView.getStrExpectedRateId() );
				expectedRateView = portfolioServiceAgent.getExpectedRateList( expectedRateView ).get(0);
				newPortfolioExpectedRateView.setExpectedRateView( expectedRateView );
				newPortfolioExpectedRateView.setStrExpectedRateId( expectedRateView.getExpectedRateId() );
				
				newPortfolioExpectedRateView.setHasPeriod( portfolioExpectedRateView.getHasPeriod() );
				
				portfolioExpectedRateViewList.add( newPortfolioExpectedRateView );
				
				updateValuesToMaps();
			}
			
			logger.logInfo( METHOD_NAME + " --- COMPLETED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
		}
		
		return null;
	}
	
	private boolean validateExpectedRateAdd() 
	{
		boolean valid = true;
		
		if ( portfolioExpectedRateView.getStrExpectedRateId() == null || portfolioExpectedRateView.getStrExpectedRateId().equalsIgnoreCase( "" ) )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.expected.rate") ) );
			valid = false;
		}
		
		if (    portfolioExpectedRateView.getHasValue() && 
				"".equalsIgnoreCase(portfolioExpectedRateView.getStrExpectedRateValue()))	{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("commons.value") ) );
			valid = false;
		} else if (portfolioExpectedRateView.getHasValue()){
			try{
				Double val = Double.parseDouble( portfolioExpectedRateView.getStrExpectedRateValue() );
				if (val!=null && val < 0){
					errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("commons.value") ) );
					valid = false;
				}
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("commons.value") ) );
				valid = false;
			}
		}
		
		if ( portfolioExpectedRateView.getHasPeriod() && 
				( portfolioExpectedRateView.getStrExpectedRatePeriod() == null || portfolioExpectedRateView.getStrExpectedRatePeriod().equalsIgnoreCase( "" ) ) 
			)
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.period") ) );
			valid = false;
		
		} else if (portfolioExpectedRateView.getHasPeriod()){
			try{
				Double period = Double.parseDouble( portfolioExpectedRateView.getStrExpectedRatePeriod() );
				if (period!=null && period < 0){
					errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.period") ) );
					valid = false;
				}
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.period") ) );
				valid = false;
			}
			
		}
		
		if (portfolioExpectedRateView.getHasTimes() &&  
				(
						"".equalsIgnoreCase(portfolioExpectedRateView.getStrExpectedRateTimes())
						|| portfolioExpectedRateView.getStrExpectedRateTimes()==null )
				){
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.times") ) );
			valid = false;
		} else if (portfolioExpectedRateView.getHasTimes()){
			
			try{
				Double times = Double.parseDouble( portfolioExpectedRateView.getStrExpectedRateTimes() );
				if (times!=null && times < 0){
					errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.times") ) );
					valid = false;
				}
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.decimalValue.required", CommonUtil.getParamBundleMessage("portfolio.expected.rate.times") ) );
				valid = false;
			}
		}
		
		
		if ( valid )
		{
			for ( PortfolioExpectedRateView addedPortfolioExpectedRateView : portfolioExpectedRateViewList )
			{
				if ( addedPortfolioExpectedRateView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) && 
					 portfolioExpectedRateView.getStrExpectedRateId().equalsIgnoreCase(addedPortfolioExpectedRateView.getStrExpectedRateId()))						
				{
					valid = false;
					errorMessages.add( CommonUtil.getBundleMessage("expectedRate.add.error.exits.expectedRate") );
					break;
				}
			}
		}
		
		return valid;
	}
	
	public String onExpectedRateDelete()
	{
		PortfolioExpectedRateView portfolioExpectedRateView = (PortfolioExpectedRateView) expectedRateDataTable.getRowData();
		
		if ( portfolioExpectedRateView.getPortfolioExpectedRateId() == null )
			portfolioExpectedRateViewList.remove( portfolioExpectedRateView );
		else
			portfolioExpectedRateView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		updateValuesToMaps();
		
		return null;
	}
	
	public String onProfitAdd()
	{
		final String METHOD_NAME = "onProfitAdd()"; 
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( validateProfitAdd() )
			{	
				PortfolioProfitView newPortfolioProfitView = new PortfolioProfitView ();
				
				newPortfolioProfitView.setProfitValue( Double.parseDouble(portfolioProfitView.getStrProfitValue()) );
				newPortfolioProfitView.setProfitToDate( portfolioProfitView.getProfitToDate() );
				newPortfolioProfitView.setProfitFromDate( portfolioProfitView.getProfitFromDate() );
				
				
				
				newPortfolioProfitView.setCreatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioProfitView.setCreatedOn( new Date() );
				newPortfolioProfitView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioProfitView.setUpdatedOn( new Date() );
				newPortfolioProfitView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				newPortfolioProfitView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				newPortfolioProfitView.setPortfolioView( portfolioView );				
				
				
				
				portfolioProfitViewList.add( newPortfolioProfitView );
				
				updateValuesToMaps();
			}
			
			logger.logInfo( METHOD_NAME + " --- COMPLETED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
		}
		
		return null;
	}
	
	private boolean validateProfitAdd() 
	{
		boolean valid = true;
		
		if ( portfolioProfitView.getStrProfitValue() == null || portfolioProfitView.getStrProfitValue().equalsIgnoreCase( "" ) )
		{
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.portfolio.profit") ) );
			valid = false;
		}else
		{
			
			try{
				
				Double profitVal = Double.parseDouble( portfolioProfitView.getStrProfitValue() );
				
				if (profitVal!=null && profitVal < 0){
					errorMessages.add( CommonUtil.getBundleMessage("profit.add.error.required.invalidProfitValue") );
					valid = false;
				}
				
			}catch (NumberFormatException e) {
				
				errorMessages.add( CommonUtil.getBundleMessage("profit.add.error.required.invalidProfitValue") );
				valid = false;
			}
			
		}
		
		if ( portfolioProfitView.getProfitFromDate() == null  )
		{
			errorMessages.add( CommonUtil.getBundleMessage("profit.add.error.required.dateFrom") );
			valid = false;
		}
		
		if ( portfolioProfitView.getProfitToDate() == null  )
		{
			errorMessages.add( CommonUtil.getBundleMessage("profit.add.error.required.dateTo") );
			valid = false;
		}
		
		
		
		return valid;
	}
	
	public String onProfitDelete()
	{
		PortfolioProfitView portfolioProfitView = (PortfolioProfitView) profitDataTable.getRowData();
		
		if ( portfolioProfitView.getPortfolioProfitId() == null )
			portfolioProfitViewList.remove( portfolioProfitView );
		else
			portfolioProfitView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		updateValuesToMaps();
		
		return null;
	}
	
	public String onEvaluationAdd()
	{
		final String METHOD_NAME = "onEvaluationAdd()"; 
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
						
			Map<Long, DomainData> domainDataMap = new UtilityManager().getDomainDataMap();
			
			// As it was a requirement to remove evaluation typeId and set it to FIXED in all cases
			portfolioEvaluationView.setStrEvaluationTypeId( "" +CommonUtil.getDomainDataId(WebConstants.PORTFOLIO_EVALUATION_TYPE, WebConstants.PORTFOLIO_EVALUATION_TYPE_FIXED));
			
			if ( validateEvaluationAdd() )
			{	
				PortfolioEvaluationView newPortfolioEvaluationView = new PortfolioEvaluationView ();
				
				newPortfolioEvaluationView.setEvaluationTypeId( Long.parseLong( portfolioEvaluationView.getStrEvaluationTypeId() ) );
				newPortfolioEvaluationView.setEvaluationTypeEn(domainDataMap.get( Long.parseLong( portfolioEvaluationView.getStrEvaluationTypeId()) ).getDataDescEn());
				newPortfolioEvaluationView.setEvaluationTypeAr(domainDataMap.get( Long.parseLong(portfolioEvaluationView.getStrEvaluationTypeId()) ).getDataDescAr());
				
				if(portfolioEvaluationView.getStrUnitValue() !=null && !portfolioEvaluationView.getStrUnitValue().toString().equals(""))
					newPortfolioEvaluationView.setUnitValue( Double.parseDouble( portfolioEvaluationView.getStrUnitValue() ) );
				
				if(portfolioEvaluationView.getStrNumberOfUnit() !=null && !portfolioEvaluationView.getStrNumberOfUnit().toString().equals(""))
					newPortfolioEvaluationView.setNumberOfUnit( Double.parseDouble( portfolioEvaluationView.getStrNumberOfUnit() ) );
				
				if(portfolioEvaluationView.getStrEvaluationValue() !=null && !portfolioEvaluationView.getStrEvaluationValue().toString().equals(""))
					newPortfolioEvaluationView.setEvaluationValue( Double.parseDouble( portfolioEvaluationView.getStrEvaluationValue() ) );
				
				
				if( ( portfolioEvaluationView.getStrEvaluationValue() ==null ||  portfolioEvaluationView.getStrEvaluationValue().toString().trim().length()<=0 )&&  
				 	( portfolioEvaluationView.getStrNumberOfUnit() !=null && !portfolioEvaluationView.getStrNumberOfUnit().toString().equals("") )&&
				    ( portfolioEvaluationView.getStrUnitValue() !=null && !portfolioEvaluationView.getStrUnitValue().toString().equals(""))   )
					newPortfolioEvaluationView.setEvaluationValue(  Double.parseDouble( portfolioEvaluationView.getStrNumberOfUnit() ) *Double.parseDouble( portfolioEvaluationView.getStrUnitValue() ) );
				
				newPortfolioEvaluationView.setEvaluationQuarterId( Long.parseLong( portfolioEvaluationView.getStrEvaluationQuarterId() ) );
				newPortfolioEvaluationView.setEvaluationQuarterEn(domainDataMap.get( Long.parseLong(portfolioEvaluationView.getStrEvaluationQuarterId()) ).getDataDescEn());
				newPortfolioEvaluationView.setEvaluationQuarterAr(domainDataMap.get( Long.parseLong(portfolioEvaluationView.getStrEvaluationQuarterId()) ).getDataDescAr());
				newPortfolioEvaluationView.setEvaluationYear( Long.parseLong( portfolioEvaluationView.getStrEvaluationYear() ) );
				
				newPortfolioEvaluationView.setCreatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioEvaluationView.setCreatedOn( new Date() );
				newPortfolioEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				newPortfolioEvaluationView.setUpdatedOn( new Date() );
				newPortfolioEvaluationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				newPortfolioEvaluationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				newPortfolioEvaluationView.setPortfolioView( portfolioView );				
				
				
				
				portfolioEvaluationViewList.add( newPortfolioEvaluationView );
				
				updateValuesToMaps();
			}
			
			logger.logInfo( METHOD_NAME + " --- COMPLETED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
		}
		
		return null;
	}
	
	private boolean validateEvaluationAdd() {
		boolean valid = true;

		//DomainDataView ddvPortfolioEvaluationTypeFixed = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PORTFOLIO_EVALUATION_TYPE), WebConstants.PORTFOLIO_EVALUATION_TYPE_FIXED);
		//DomainDataView ddvPortfolioEvaluationTypePercentage = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PORTFOLIO_EVALUATION_TYPE), WebConstants.PORTFOLIO_EVALUATION_TYPE_PERCENTAGE);
		
		if ( portfolioEvaluationView.getStrEvaluationTypeId() == null || portfolioEvaluationView.getStrEvaluationTypeId().equalsIgnoreCase( "" ) )
		{
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.EvaluationType") );
			valid = false;
		}
		
		//if(portfolioEvaluationView.getStrEvaluationTypeId() !=null && !portfolioEvaluationView.getStrEvaluationTypeId().equalsIgnoreCase("")
		//		&& ddvPortfolioEvaluationTypeFixed.getDomainDataId().toString().equals(portfolioEvaluationView.getStrEvaluationTypeId().toString())  )
		//{

		if ( portfolioEvaluationView.getStrEvaluationValue() == null || portfolioEvaluationView.getStrEvaluationValue().equalsIgnoreCase( "" ) ) {
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.EvaluationValue") );
			valid = false;
		}
		else{

			try{

				Double.parseDouble( portfolioEvaluationView.getStrEvaluationValue() );

			}catch (NumberFormatException e) {

				errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidEvaluationValue") );
				logger.LogException(CommonUtil.getBundleMessage("evaluation.add.error.required.EvaluationValue") + "NumberFormatException", e);
				
				valid = false;
			}

		}	
		//}
		
		//if(portfolioEvaluationView.getStrEvaluationTypeId() !=null && !portfolioEvaluationView.getStrEvaluationTypeId().equalsIgnoreCase("")
		//		&& ddvPortfolioEvaluationTypePercentage.getDomainDataId().toString().equals(portfolioEvaluationView.getStrEvaluationTypeId().toString())  )
		//{
		if ( portfolioEvaluationView.getStrUnitValue() == null || portfolioEvaluationView.getStrUnitValue().equalsIgnoreCase( "" ) ) {
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.UnitValue") );
			valid = false;
		} 	else {
			try{
				
				Double unitVal = Double.parseDouble( portfolioEvaluationView.getStrUnitValue() );
				
				Double portUnitVal = Double.parseDouble( portfolioView.getUnitPrice() );
				
				if (portUnitVal!=null && portUnitVal>0) {
					if (unitVal!=null && unitVal <= 0){
						errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidUnitValue") );
						valid = false;
					}
				}
				
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidUnitValue") );
				logger.LogException(CommonUtil.getBundleMessage("evaluation.add.error.required.invalidUnitValue") + "NumberFormatException", e);
				valid = false;
			}
		}	
		
		if ( portfolioEvaluationView.getStrNumberOfUnit() == null || portfolioEvaluationView.getStrNumberOfUnit().equalsIgnoreCase( "" ) ) {
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.NumberOfUnit") );
			valid = false;
		}else {
			try{

				Double numberOfUnit = Double.parseDouble( portfolioEvaluationView.getStrNumberOfUnit() );
				
				Double totalUnits = Double.parseDouble( portfolioView.getTotalUnits() );
				
				if (totalUnits!=null && totalUnits>0) {
					if (numberOfUnit!=null && numberOfUnit <= 0){
						errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidNumberOfUnit") );
						valid = false;
					}
				}
				
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidNumberOfUnit") );
				logger.LogException(CommonUtil.getBundleMessage("evaluation.add.error.required.invalidNumberOfUnit") + "NumberFormatException", e);
				valid = false;
			}
		}
		//}
		
		if ( portfolioEvaluationView.getStrEvaluationQuarterId() == null || portfolioEvaluationView.getStrEvaluationQuarterId().equalsIgnoreCase( "" ) ) {
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.EvaluationQuarter") );
			valid = false;
		}
		
		if ( portfolioEvaluationView.getStrEvaluationYear() == null || portfolioEvaluationView.getStrEvaluationYear().equalsIgnoreCase( "" ) ) {
			errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.EvaluationYear") );
			valid = false;
		}  else { 
			try{
				Long.parseLong( portfolioEvaluationView.getStrEvaluationYear() );
			}catch (NumberFormatException e) {
				errorMessages.add( CommonUtil.getBundleMessage("evaluation.add.error.required.invalidEvaluationYear") );
				logger.LogException(CommonUtil.getBundleMessage("evaluation.add.error.required.invalidEvaluationYear") + "NumberFormatException", e);
				valid = false;
			}
		}
		
		
		return valid;
	}
	
	public String onEvaluationDelete()
	{
		PortfolioEvaluationView portfolioEvaluationView = (PortfolioEvaluationView) evaluationDataTable.getRowData();
		
		if ( portfolioEvaluationView.getPortfolioEvaluationId() == null )
			portfolioEvaluationViewList.remove( portfolioEvaluationView );
		else
			portfolioEvaluationView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		updateValuesToMaps();
		
		return null;
	}

	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public void showHideAttachmentAndCommentsBtn()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_ADD ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_EDIT ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_APPROVE_REJECT ) )
			canAddAttachmentsAndComments(false);
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_CLOSE ) )
			canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_VIEW ) )	
	    	canAddAttachmentsAndComments(false);
	    
		
	}
	
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return CommonUtil.getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public PortfolioView getPortfolioView() {
		return portfolioView;
	}

	public void setPortfolioView(PortfolioView portfolioView) {
		this.portfolioView = portfolioView;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public PortfolioServiceAgent getPortfolioServiceAgent() {
		return portfolioServiceAgent;
	}

	public void setPortfolioServiceAgent(PortfolioServiceAgent portfolioServiceAgent) {
		this.portfolioServiceAgent = portfolioServiceAgent;
	}
	
	public OrderServiceAgent getOrderServiceAgent() {
		return orderServiceAgent;
	}

	public void setOrderServiceAgent(OrderServiceAgent orderServiceAgent) {
		this.orderServiceAgent = orderServiceAgent;
	}

	public List<OrderView> getBuyingOrderViewList() {
		return buyingOrderViewList;
	}

	public void setBuyingOrderViewList(List<OrderView> buyingOrderViewList) {
		this.buyingOrderViewList = buyingOrderViewList;
	}

	public List<OrderView> getSellingOrderViewList() {
		return sellingOrderViewList;
	}

	public void setSellingOrderViewList(List<OrderView> sellingOrderViewList) {
		this.sellingOrderViewList = sellingOrderViewList;
	}

	public HtmlDataTable getBuyingOrderDataTable() {
		return buyingOrderDataTable;
	}

	public void setBuyingOrderDataTable(HtmlDataTable buyingOrderDataTable) {
		this.buyingOrderDataTable = buyingOrderDataTable;
	}

	public HtmlDataTable getSellingOrderDataTable() {
		return sellingOrderDataTable;
	}

	public void setSellingOrderDataTable(HtmlDataTable sellingOrderDataTable) {
		this.sellingOrderDataTable = sellingOrderDataTable;
	}

	public SystemParameters getSystemParameters() {
		return systemParameters;
	}

	public void setSystemParameters(SystemParameters systemParameters) {
		this.systemParameters = systemParameters;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public List<ExpenseAndRevenueView> getExpenseAndRevenueViewList() {
		return expenseAndRevenueViewList;
	}

	public void setExpenseAndRevenueViewList(
			List<ExpenseAndRevenueView> expenseAndRevenueViewList) {
		this.expenseAndRevenueViewList = expenseAndRevenueViewList;
	}

	public RevenuesAndExpensesServiceAgent getRevenuesAndExpensesServiceAgent() {
		return revenuesAndExpensesServiceAgent;
	}

	public void setRevenuesAndExpensesServiceAgent(
			RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent) {
		this.revenuesAndExpensesServiceAgent = revenuesAndExpensesServiceAgent;
	}

	public List<PortfolioAssetClassView> getPortfolioAssetClassViewList() {
		return portfolioAssetClassViewList;
	}

	public void setPortfolioAssetClassViewList(
			List<PortfolioAssetClassView> portfolioAssetClassViewList) {
		this.portfolioAssetClassViewList = portfolioAssetClassViewList;
	}

	public HtmlDataTable getAssetClassesDataTable() {
		return assetClassesDataTable;
	}

	public void setAssetClassesDataTable(HtmlDataTable assetClassesDataTable) {
		this.assetClassesDataTable = assetClassesDataTable;
	}

	public List<PortfolioAssetView> getPortfolioAssetViewList() {
		return portfolioAssetViewList;
	}

	public void setPortfolioAssetViewList(
			List<PortfolioAssetView> portfolioAssetViewList) {
		this.portfolioAssetViewList = portfolioAssetViewList;
	}

	public HtmlDataTable getAssetDataTable() {
		return assetDataTable;
	}

	public void setAssetDataTable(HtmlDataTable assetDataTable) {
		this.assetDataTable = assetDataTable;
	}

	public List<PortfolioAccountTypeView> getPortfolioAccountTypeViewList() {
		return portfolioAccountTypeViewList;
	}

	public void setPortfolioAccountTypeViewList(
			List<PortfolioAccountTypeView> portfolioAccountTypeViewList) {
		this.portfolioAccountTypeViewList = portfolioAccountTypeViewList;
	}

	public HtmlDataTable getAccountTypeDataTable() {
		return accountTypeDataTable;
	}

	public void setAccountTypeDataTable(HtmlDataTable accountTypeDataTable) {
		this.accountTypeDataTable = accountTypeDataTable;
	}

	public PortfolioAccountTypeView getPortfolioAccountTypeView() {
		return portfolioAccountTypeView;
	}

	public void setPortfolioAccountTypeView(
			PortfolioAccountTypeView portfolioAccountTypeView) {
		this.portfolioAccountTypeView = portfolioAccountTypeView;
	}

	public AccountServiceAgent getAccountServiceAgent() {
		return accountServiceAgent;
	}

	public void setAccountServiceAgent(AccountServiceAgent accountServiceAgent) {
		this.accountServiceAgent = accountServiceAgent;
	}

	public List<PortfolioExpectedRateView> getPortfolioExpectedRateViewList() {
		return portfolioExpectedRateViewList;
	}

	public void setPortfolioExpectedRateViewList(
			List<PortfolioExpectedRateView> portfolioExpectedRateViewList) {
		this.portfolioExpectedRateViewList = portfolioExpectedRateViewList;
	}

	public PortfolioExpectedRateView getPortfolioExpectedRateView() {
		return portfolioExpectedRateView;
	}

	public void setPortfolioExpectedRateView(
			PortfolioExpectedRateView portfolioExpectedRateView) {
		this.portfolioExpectedRateView = portfolioExpectedRateView;
	}

	public String getNoteOwner() {
		return noteOwner;
	}

	public String getExternalId() {
		return externalId;
	}

	public String getProcedureTypeKey() {
		return procedureTypeKey;
	}

	public HtmlDataTable getExpectedRateDataTable() {
		return expectedRateDataTable;
	}

	public void setExpectedRateDataTable(HtmlDataTable expectedRateDataTable) {
		this.expectedRateDataTable = expectedRateDataTable;
	}

	public HtmlDataTable getProfitDataTable() {
		return profitDataTable;
	}

	public void setProfitDataTable(HtmlDataTable profitDataTable) {
		this.profitDataTable = profitDataTable;
	}

	public List<PortfolioProfitView> getPortfolioProfitViewList() {
		return portfolioProfitViewList;
	}

	public void setPortfolioProfitViewList(
			List<PortfolioProfitView> portfolioProfitViewList) {
		this.portfolioProfitViewList = portfolioProfitViewList;
	}

	public PortfolioProfitView getPortfolioProfitView() {
		return portfolioProfitView;
	}

	public void setPortfolioProfitView(PortfolioProfitView portfolioProfitView) {
		this.portfolioProfitView = portfolioProfitView;
	}

	public List<PortfolioEvaluationView> getPortfolioEvaluationViewList() {
		return portfolioEvaluationViewList;
	}

	public void setPortfolioEvaluationViewList(
			List<PortfolioEvaluationView> portfolioEvaluationViewList) {
		this.portfolioEvaluationViewList = portfolioEvaluationViewList;
	}

	public PortfolioEvaluationView getPortfolioEvaluationView() {
		return portfolioEvaluationView;
	}

	public void setPortfolioEvaluationView(
			PortfolioEvaluationView portfolioEvaluationView) {
		this.portfolioEvaluationView = portfolioEvaluationView;
	}

	public HtmlDataTable getEvaluationDataTable() {
		return evaluationDataTable;
	}

	public void setEvaluationDataTable(HtmlDataTable evaluationDataTable) {
		this.evaluationDataTable = evaluationDataTable;
	}

	public String getReadOnlyEvaluationValue() {
		if(viewMap.containsKey("READONLY_EVALUATION_VALUE"))
			readOnlyEvaluationValue = viewMap.get("READONLY_EVALUATION_VALUE").toString();
		return readOnlyEvaluationValue;
	}

	public void setReadOnlyEvaluationValue(String readOnlyEvaluationValue) {
		this.readOnlyEvaluationValue = readOnlyEvaluationValue;
		
		if(this.readOnlyEvaluationValue!=null)
			viewMap.put("READONLY_EVALUATION_VALUE", this.readOnlyEvaluationValue);
		else
			viewMap.put("READONLY_EVALUATION_VALUE", "");
	}
	
	public String getReadOnlyUnitValue() {
		if(viewMap.containsKey("READONLY_UNIT_VALUE"))
			readOnlyUnitValue = viewMap.get("READONLY_UNIT_VALUE").toString();
		return readOnlyUnitValue;
	}

	public void setReadOnlyUnitValue(String readOnlyUnitValue) {
		this.readOnlyUnitValue = readOnlyUnitValue;
		
		if(this.readOnlyUnitValue!=null)
			viewMap.put("READONLY_UNIT_VALUE", this.readOnlyUnitValue);
		else
			viewMap.put("READONLY_UNIT_VALUE", "");
	}
	

	public Boolean getDisableEvaluationValue() {
		
		if(viewMap.containsKey("DISABLE_EVALUATION_VALUE"))
			disableEvaluationValue = (Boolean)viewMap.get("DISABLE_EVALUATION_VALUE");
		return disableEvaluationValue;
	}

	public void setDisableEvaluationValue(Boolean disableEvaluationValue) {
		this.disableEvaluationValue = disableEvaluationValue;
		
		if(this.disableEvaluationValue!=null)
			viewMap.put("DISABLE_EVALUATION_VALUE", this.disableEvaluationValue);
		else
			viewMap.put("DISABLE_EVALUATION_VALUE", false);
			
	}
	
public Boolean getDisableUnitValue() {
		
		if(viewMap.containsKey("DISABLE_UNIT_VALUE"))
			disableUnitValue = (Boolean)viewMap.get("DISABLE_UNIT_VALUE");
		return disableUnitValue;
	}

	public void setDisableUnitValue(Boolean disableUnitValue) {
		this.disableUnitValue = disableUnitValue;
		
		if(this.disableUnitValue!=null)
			viewMap.put("DISABLE_UNIT_VALUE", this.disableUnitValue);
		else
			viewMap.put("DISABLE_UNIT_VALUE", false);
			
	}
	
	public Boolean getIsStatusNew()
	{
		Boolean isNewStatus = false;
		if(viewMap.get("DDV_PORTFOLIO_STATUS_NEW") != null)
		{
			portfolioView = (PortfolioView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW);
			if(portfolioView != null && portfolioView.getPortfolioId() != null)
			{
				ddv_PorfolioStatusNew = (DomainDataView) viewMap.get("DDV_PORTFOLIO_STATUS_NEW"); 
				if(portfolioView.getStatusId().compareTo(ddv_PorfolioStatusNew.getDomainDataId().toString()) == 0)
					isNewStatus = true;
			}
			
		}
		return isNewStatus;
	}

	public DomainDataView getDdv_PorfolioStatusNew() {
		return ddv_PorfolioStatusNew;
	}

	public void setDdv_PorfolioStatusNew(DomainDataView ddv_PorfolioStatusNew) {
		this.ddv_PorfolioStatusNew = ddv_PorfolioStatusNew;
	}
	public void onApprove()
	{
		try{
		portfolioView = (PortfolioView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW);
		DomainDataView ddvPortfolioStatusApproved = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Portfolio.PORTFOLIO_STATUS), WebConstants.Portfolio.PORTFOLIO_STATUS_APPROVED);
		portfolioView.setStatusId(ddvPortfolioStatusApproved.getDomainDataId().toString());
		portfolioView.setStatusEn(ddvPortfolioStatusApproved.getDataDescEn());
		portfolioView.setStatusAr(ddvPortfolioStatusApproved.getDataDescAr());
		if(new PortfolioServiceAgent().updatePortfolio(portfolioView))
			{
				viewMap.put(WebConstants.Portfolio.PORTFOLIO_VIEW, portfolioView);
				successMessages.add( CommonUtil.getBundleMessage("portfolio.successMsg.porfolioApproved") );
			}
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("onApprove crashed", e);
		}
		
	}
	
	public String showInvestmentUnitHistory() {

		HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
		
		
		
		request.getSession().setAttribute(WebConstants.Portfolio.PORTFOLIO_INVESTMENT_UNIT_HISTORY_LIST, investmentUnitHistoryViewList);
		openPopup("openShowInvestmentUnitHistoryPopup('" + "portfolioInvestmentUnitHistory.jsf" + "');");
		return null;
	}

	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("openPopup() completed successfully!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public List<InvestmentUnitHistoryView> getInvestmentUnitHistoryViewList() {
		return investmentUnitHistoryViewList;
	}

	public void setInvestmentUnitHistoryViewList(
			List<InvestmentUnitHistoryView> investmentUnitHistoryViewList) {
		this.investmentUnitHistoryViewList = investmentUnitHistoryViewList;
	}
}
