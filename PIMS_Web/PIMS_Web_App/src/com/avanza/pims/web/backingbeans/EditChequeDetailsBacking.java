package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;


public class EditChequeDetailsBacking  extends AbstractController
{
     /**
	 * 
	 */
	private static final long serialVersionUID = 6100425815097470767L;
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	private static Logger logger = Logger.getLogger( EditChequeDetailsBacking.class );
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings( "unchecked" )
	Map sessionMap;
	@SuppressWarnings( "unchecked" )
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	
	
	//Data Fields------//
	private String chequeNumber="";
	private String selectedBankId;
	private String description="";
	private String amount;
	private String selectedPaymentMode="";
	private String chequeType;
	private Date chequeDate=new Date();
	private String paymentType="";
	private DomainDataView ddvGrpStatusPosted=new DomainDataView();
	//Date Fields End----//
	
	String dateFormat="";
	private BounceChequeView selectedCheque=new BounceChequeView();

	@SuppressWarnings( "unchecked" )
	    public void init() 
	    {
	    	String methodName="init";
	    	logger.logInfo(methodName +"|"+"Start");
	    	super.init();
	    	logger.logInfo(methodName +"|"+"isPostBack()="+isPostBack());
	    	sessionMap =context.getExternalContext().getSessionMap();
	    	if( !isPostBack() )
	    	{
	    		ddvGrpStatusPosted=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.GRP.GRP_TRANSACTION_STATUS), WebConstants.GRP.GRP_POSTED);
	    		viewRootMap.put(WebConstants.GRP.GRP_POSTED, ddvGrpStatusPosted);
	    		if(sessionMap.get("SELECTED_RECORD")!=null)
	    		{
	    			viewRootMap.put("SELECTED_RECORD", (BounceChequeView)sessionMap.get("SELECTED_RECORD"));
	    			sessionMap.remove("SELECTED_RECORD");
	    		}
	    		populateDataFieldsFromSelectedData();
	    		if(sessionMap.get("CONTRACT_END_DATE") != null){
	    			viewMap.put("CONTRACT_END_DATE", sessionMap.get("CONTRACT_END_DATE"));
	    			sessionMap.remove("CONTRACT_END_DATE");
	    		}
	    	}
	    	logger.logInfo( methodName +"|"+"Finish" );
	    }

	// Constructors

	/** default constructor */
	public EditChequeDetailsBacking() 
	{
		
	}

	public BounceChequeView getSelectedCheque() 
	{
		if(viewRootMap.get("SELECTED_RECORD")!=null)
			selectedCheque=(BounceChequeView) viewRootMap.get("SELECTED_RECORD");
		return selectedCheque;
	}

	public void setSelectedCheque(BounceChequeView selectedCheque) 
	{
		this.selectedCheque = selectedCheque;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}
	 public String getSuccessMessages()
		{
			String messageList="";
			if ((successMessages== null) || (successMessages.size() == 0)) 
				messageList = "";
			else
				for (String message : successMessages) 
						messageList +=  "<LI>" +message+ "<br></br>" ;
			return (messageList);
		}
	    public String getLocale(){
			LocaleInfo localeInfo = getLocaleInfo();
			return localeInfo.getLanguageCode();
		}
		
		public LocaleInfo getLocaleInfo()
		{
		    	
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		
		     return localeInfo;
			
		}
		@SuppressWarnings( "unchecked" )
		public void populateDataFieldsFromSelectedData()
		{
			selectedCheque=getSelectedCheque();
			if(selectedCheque.getMethodReferenceDate()!=null)
				viewRootMap.put("CHEQUE_DATE",selectedCheque.getMethodReferenceDate());
			if(selectedCheque.getChequeNumber()!=null)
				viewRootMap.put("CHEQUE_NUMBER",selectedCheque.getChequeNumber());
			if(selectedCheque.getAmount()!=null)
				viewRootMap.put("CHEQUE_AMOUNT",selectedCheque.getAmountDetails());
			if(selectedCheque.getBankId()!=null)
				viewRootMap.put("BANK_ID",selectedCheque.getBankId());
			if(selectedCheque.getChequeTypeId() != null )
				viewRootMap.put("CHEQUE_TYPE",selectedCheque.getChequeTypeId() );
			if(CommonUtil.getIsEnglishLocale())
				{
					if(selectedCheque.getPaymentTypeDescriptionEn()!=null)
						viewRootMap.put("PAYMENT_TYPE_DESCRIPTION",selectedCheque.getPaymentTypeDescriptionEn());
					if(selectedCheque.getPaymentModeEn()!=null)
						viewRootMap.put("PAYMENT_MODE",selectedCheque.getPaymentModeEn());
				}
			else
				{
					if(selectedCheque.getPaymentTypeDescriptionAr()!=null)
						viewRootMap.put("PAYMENT_TYPE_DESCRIPTION",selectedCheque.getPaymentTypeDescriptionAr());
					if(selectedCheque.getPaymentModeAr()!=null)
						viewRootMap.put("PAYMENT_MODE",selectedCheque.getPaymentModeAr());
				}
		}
		public void btnSave_Click()
		{
			try
			{
				errorMessages  = new ArrayList<String>( 0 );
				if( !hasError() )
				{
					if( !isPaymentPosted() )
					{	
						String newBankEn = "";
						String newBankAr = "";
						List<BankView> bankViewList = new UtilityServiceAgent().getBanksList();
						Long newBankId = Long.parseLong( this.getSelectedBankId() );
						for ( BankView bankView : bankViewList) 
						{
							if( newBankId.compareTo( bankView.getBankId() ) ==0 )
							{
								newBankEn = bankView.getBankEn();
								newBankAr = bankView.getBankAr();
							}
						}
						List<DomainDataView> chqTypeList = CommonUtil.getDomainDataListForDomainType(WebConstants.CHEQUE_TYPE);
						Long newChqType = Long.parseLong( this.getChequeType() );
						String newChqTypeEn="";
						String newChqTypeAr="";
						for (DomainDataView item : chqTypeList) 
						{
							if( newChqType.compareTo( item.getDomainDataId() ) ==0 )
							{
								newChqTypeEn = item.getDataDescEn();
								newChqTypeAr = item.getDataDescAr();
							}
						}
						successMessages.clear();
						if( isAnyChangePresent( this.getSelectedCheque() ) ) 
						{
						  DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
						  updateChequeDetails();
						  Locale localeAr = new Locale( "ar" );
						  Locale localeEn = new Locale( "en" );
						  ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
						  ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
			    		  String sysMsgEn = java.text.MessageFormat.format( bundleEn.getString( "msg.system.UpdateChequeDetails" ),
			    				                                            this.getSelectedCheque().getChequeNumber().trim(),
			    				                                            df.format( this.getSelectedCheque().getMethodReferenceDate() ),
			    				                                            this.getSelectedCheque().getBankNameEn(),
			    				                                            this.getSelectedCheque().getChequeTypeEn(),
			    				                                            this.getChequeNumber().trim(),
			    				                                            df.format( this.getChequeDate() ),
			    				                                            newBankEn,
			    				                                            newChqTypeEn
			    				                                            ); 	
			    		  String sysMsgAr = java.text.MessageFormat.format(   bundleAr.getString( "msg.system.UpdateChequeDetails" ),
											                                  this.getSelectedCheque().getChequeNumber().trim(),
											                                  df.format( this.getSelectedCheque().getMethodReferenceDate() ),
											                                  this.getSelectedCheque().getBankNameEn(),
											                                  this.getSelectedCheque().getChequeTypeAr(),
											                                  this.getChequeNumber().trim(),
											                                  df.format( this.getChequeDate() ),
											                                  newBankAr,
											                                  newChqTypeAr
											                                );
						  NotesController.saveSystemNotesForRequest( WebConstants.NOTES_OWNER_PAYMENT_RECEIPT , sysMsgEn,sysMsgAr, 
								                                     this.getSelectedCheque().getPaymentReceiptId() );
						  successMessages.add( CommonUtil.getBundleMessage( "grp.SuccessMsg.chequeHasBeenModified" ) );
						}
						else
						{
						 errorMessages.clear();
					     errorMessages.add(CommonUtil.getBundleMessage("grp.ErrMsg.noChange"));
					     return;
						}
					}
					else
						errorMessages.add(CommonUtil.getBundleMessage("grp.ErrMsg.chequeCantBeModified"));
				}
				else
					errorMessages.add(CommonUtil.getBundleMessage("grp.ErrMsg.allFieldsRequired"));
			}
			catch (Exception e) 
			{
				 logger.LogException(  "btnSave_Click|crashed |" ,e );
				 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}

		private void updateChequeDetails() throws Exception
		{
			if(this.getSelectedCheque().getPaymentReceiptDetailId()!=null)
			{
				HashMap<String,Object> map = new HashMap<String, Object>( 0 );
				map.put("bounceChequeView", this.getSelectedCheque() );
				map.put("newChequeDate",this.getChequeDate() );
				map.put("newChequeNumber",this.getChequeNumber().trim());  
				map.put("loggedInUser",CommonUtil.getLoggedInUser()) ;
				map.put("bank",this.getSelectedBankId() );
				map.put("chequeType",this.getChequeType() );
				
				new PropertyService().updateChequeDetails( map ); 
		
			}
				
		}


		private boolean isAnyChangePresent(BounceChequeView newCheque) throws Exception
		{
			boolean hasChanges=false;
			DateFormat df  = new SimpleDateFormat( "dd/MM/yyyy" );
			if( newCheque.getBankId().toString().compareTo( this.getSelectedBankId() ) == 0  &&
			    newCheque.getChequeNumber().compareTo( this.getChequeNumber().trim() ) == 0  &&
			    df.parse( df.format( newCheque.getMethodReferenceDate()) ).compareTo( df.parse( df.format( this.getChequeDate() ) ) )   == 0 &&
			    newCheque.getChequeTypeId().toString().equals( this.getChequeType() )
			    
			  )
				hasChanges=false;
			else
				hasChanges=true;
			
			return hasChanges;
		}

		private boolean hasError()
		{
			boolean hasError=false;
			if(viewMap.get("CONTRACT_END_DATE") != null && this.getChequeDate() != null){
				Date contractEndDate = (Date) viewMap.get("CONTRACT_END_DATE");
				if(contractEndDate.before(this.getChequeDate())){
					errorMessages.add(CommonUtil.getBundleMessage("leaseContractErrorMsg.payemtDueDate"));
					return  true;
				}
			}
			
			if( this.getSelectedBankId()==null ||this.getSelectedBankId().trim().length() <=0 ||
			    this.getChequeNumber()==null || this.getChequeNumber().trim().length() <= 0 ||
			    this.getChequeDate()==null
			  )
				hasError=true;
			
			else
				hasError=false;
			return hasError;
		}

		private boolean isPaymentPosted() throws PimsBusinessException
		{
			boolean posted=false;
			this.selectedCheque=getSelectedCheque();
			posted= new GRPService().isAnyPaymentPostedAgainstRecieptDetail(this.selectedCheque.getPaymentReceiptDetailId());
			return posted;
		}

		public String getChequeNumber() 
		{
			if(viewRootMap.get("CHEQUE_NUMBER")!=null)
				chequeNumber=viewRootMap.get("CHEQUE_NUMBER").toString();
			return chequeNumber;
		}
        @SuppressWarnings( "unchecked" ) 
		public void setChequeNumber(String chequeNumber) 
		{
			if(chequeNumber!=null)
				viewRootMap.put("CHEQUE_NUMBER", chequeNumber);
			this.chequeNumber = chequeNumber;
		}



		public String getSelectedPaymentMode() 
		{
			if(viewRootMap.get("PAYMENT_MODE")!=null)
				selectedPaymentMode=viewRootMap.get("PAYMENT_MODE").toString();
			return selectedPaymentMode;
		}

		public void setSelectedPaymentMode(String selectedPaymentMode)
		{
			this.selectedPaymentMode = selectedPaymentMode;
		}

		public String getDateFormat()
		{
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    	dateFormat= localeInfo.getDateFormat();
			return dateFormat;
		}

		public String getSelectedBankId() 
		{
			if(viewRootMap.get("BANK_ID")!=null)
				selectedBankId=viewRootMap.get("BANK_ID").toString();
			return selectedBankId;
		}

		public void setSelectedBankId(String selectedBankId) 
		{
			if(selectedBankId!=null)
				viewRootMap.put("BANK_ID", selectedBankId);
			this.selectedBankId = selectedBankId;
		}

		public String getAmount() 
		{
			if(viewRootMap.get("CHEQUE_AMOUNT")!=null)
				amount=viewRootMap.get("CHEQUE_AMOUNT").toString();
			return amount;
		}

		public void setAmount(String amount)
		{
			this.amount = amount;
		}
		public DomainDataView getDdvGrpStatusPosted()
		{
			if(viewRootMap.get(WebConstants.GRP.GRP_POSTED)!=null)
				ddvGrpStatusPosted=(DomainDataView) viewRootMap.get(WebConstants.GRP.GRP_POSTED);
			return ddvGrpStatusPosted;
		}

		public void setDdvGrpStatusPosted(DomainDataView ddvGrpStatusPosted) {
			this.ddvGrpStatusPosted = ddvGrpStatusPosted;
		}

		public Date getChequeDate() 
		{
			if(viewRootMap.get("CHEQUE_DATE")!=null)
				chequeDate=(Date) viewRootMap.get("CHEQUE_DATE");
			return chequeDate;
		}
		@SuppressWarnings( "unchecked" )
		public void setChequeDate(Date chequeDate) 
		{
			if(chequeDate!=null)
				viewRootMap.put("CHEQUE_DATE", chequeDate);
			this.chequeDate = chequeDate;
		}

		public String getPaymentType() {
			return paymentType;
		}

		public void setPaymentType(String paymentType) {
			this.paymentType = paymentType;
		}
       @SuppressWarnings( "unchecked" )
		public String getChequeType() {
			if( viewRootMap.containsKey( "CHEQUE_TYPE" ) )
				chequeType =viewRootMap.get( "CHEQUE_TYPE" ).toString(); 
			return chequeType;
		}
       @SuppressWarnings( "unchecked" )
		public void setChequeType(String chequeType) {
			
			this.chequeType = chequeType;
			if( this.chequeType != null )
				viewRootMap.put( "CHEQUE_TYPE", this.chequeType );
			
				
		}
}