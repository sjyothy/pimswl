package com.avanza.pims.web.backingbeans;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.Permission;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupPermissionBinding;
import com.avanza.core.security.db.GroupPermissionId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.Investment.Opportunities.StrategicPlanDetailsBacking;
import com.avanza.pims.web.backingbeans.Investment.Opportunities.StrategicPlanSearch.Keys;
import com.avanza.pims.web.controller.AbstractController;

import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.JSFUtil;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.StrategicPlanView;
import com.avanza.ui.util.ResourceUtil;


public class SecUserGroupListing extends AbstractController{

	// Fields
	private HtmlDataTable dataTable;

	private UserGroupDbImpl dataItem = new UserGroupDbImpl();

	private HtmlInputHidden dataItemId = new HtmlInputHidden();

	private String userGroupId;

	private String groupNamePrm;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String groupNameSec;
	private String userNameSec;
	private String userNamePrm;
	private List<String> messages = new ArrayList();
	private HtmlInputText userNamePriTextBox = new HtmlInputText();

	private HtmlInputText userNameSecTextBox = new HtmlInputText();
	private HtmlInputHidden listOfPermissionsForGroup= new HtmlInputHidden();
	private HtmlInputHidden listOfPermissionsNotForGroup= new HtmlInputHidden();

	private HtmlInputText groupNamePriTextBox = new HtmlInputText();
	private HtmlInputText groupNameSecTextBox = new HtmlInputText();
	private HtmlSelectManyListbox permissionsForGroupList = new HtmlSelectManyListbox();
	private HtmlSelectManyListbox permissionsNotForGroupList = new HtmlSelectManyListbox();
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private transient Logger logger = Logger.getLogger(StrategicPlanDetailsBacking.class);
	 

	private Set secUserUserGroups = new HashSet(0);
	List<SelectItem> permissionsForGroup = new ArrayList();
	List<SelectItem> permissionsNotForGroup = new ArrayList();

	private HtmlInputHidden addCount = new HtmlInputHidden();



	private List<UserGroup> dataList = new ArrayList();

	private String sortField = null;

	private boolean sortAscending = true;
	
	
	protected List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";	
	
	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessage() 
	{
		return infoMessage;
	}
	
	// Actions -----------------------------------------------------------------------------------
	public List<UserGroup> getSecGroupDataList() 
	{
		List<UserGroup> userGroupList = new ArrayList<UserGroup>(0);
		
		   if(viewRootMap.get("dataList")!=null)
			   userGroupList = (ArrayList<UserGroup>)viewRootMap.get("dataList");
			return userGroupList;
	}


	private void loadDataList() {

		Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

	//	if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("dataList")==null)
	//	{
			List<UserGroup> groupList = SecurityManager.findGroup(query);
		
			for(int i = 0;i<groupList.size();i++)
			{
			dataList.add(groupList.get(i));
			}
		//}
	} 
	

	private void loadDataList(Search query) {

		//Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("dataList")==null)
		{
			List<UserGroup> groupList = SecurityManager.findGroup(query);
		
			for(int i = 0;i<groupList.size();i++)
			{
			dataList.add(groupList.get(i));
			}
		}
	} 
		private void loadDataListAfterDeletion() {

			Search query = new Search();

			query.clear();

			query.addFrom(UserGroupDbImpl.class);

			
			List<UserGroup> groupList = SecurityManager.findGroup(query);
			
			for(int i = 0;i<groupList.size();i++){
				
				dataList.add(groupList.get(i));
			
			}	
			 
//			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//			recordSize = dataList.size();
//			viewRootMap.put("recordSize", this.getRecordSize());
			 Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		        loadDataList();
				viewRootMap.put("dataList", dataList);
				
//				return "";
			
//			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//			viewMap.put("dataList", dataList);
			recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);

	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters -----------------------------------------------------------------------------------

	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------


	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	/** default constructor */
	public SecUserGroupListing() {



		addCount.setValue(0);


	}


	// Property accessors

	public String getUserGroupId() {
		return this.userGroupId;
	}

	public void setUserGroupId(String userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getGroupNamePrm() {
		return this.groupNamePrm;
	}

	public void setGroupNamePrm(String groupNamePrm) {
		this.groupNamePrm = groupNamePrm;
	}

	public String getGroupNameSec() {
		return this.groupNameSec;
	}

	public void setGroupNameSec(String groupNameSec) {
		this.groupNameSec = groupNameSec;
	}

	public Set getSecUserUserGroups() {
		return this.secUserUserGroups;
	}

	public void setSecUserUserGroups(Set secUserUserGroups) {
		this.secUserUserGroups = secUserUserGroups;
	}

	public HtmlInputText getGroupNamePriTextBox() {
		return groupNamePriTextBox;
	}

	public void setGroupNamePriTextBox(HtmlInputText groupNamePriTextBox) {
		this.groupNamePriTextBox = groupNamePriTextBox;
	}

	public HtmlInputText getGroupNameSecTextBox() {
		return groupNameSecTextBox;
	}

	public void setGroupNameSecTextBox(HtmlInputText groupNameSecTextBox) {
		this.groupNameSecTextBox = groupNameSecTextBox;
	}



	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public HtmlInputHidden getDataItemId() {
		return dataItemId;
	}

	// Setters -----------------------------------------------------------------------------------

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public UserGroupDbImpl getDataItem() {
		return dataItem;
	}

	public void setDataItem(UserGroupDbImpl dataItem) {
		this.dataItem = dataItem;
	}

	public void setDataItemId(HtmlInputHidden dataItemId) {
		this.dataItemId = dataItemId;
	}





	public String reset() {

		loadPermissionsNotForGroupList();
		groupNameSecTextBox.setValue("");
		groupNamePriTextBox.setValue("");
		return "success";
	}

	


	public String searchGroup(){
	
		String methodName = "searchGroup";
		try
		{
			Search search = new Search();
			search.clear();
			search.addFrom(UserGroupDbImpl.class);
			
			Object grpPrimaryName =  groupNamePriTextBox.getValue();		
			Object grpSecondaryName = groupNameSecTextBox.getValue();
	
			if(grpPrimaryName != null && !grpPrimaryName.toString().equals(""))
			{				
				search.addCriterion(SimpleCriterion.like("primaryName", "%"+grpPrimaryName.toString()+"%", true));
			}
			
			if(grpSecondaryName != null && !grpSecondaryName.toString().equals(""))
			{
				search.addCriterion(SimpleCriterion.like("secondaryName", "%"+grpSecondaryName.toString()+"%", true));			
				
			}
				
			List<UserGroup> groupList = SecurityManager.findGroup(search);
			viewRootMap.put("dataList", groupList);
			if(groupList != null)
				viewRootMap.put("recordSize", recordSize);
			else
				viewRootMap.put("recordSize", 0);
			
			recordSize = dataList.size();
			
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);

		}
		catch (Exception e) 
		{
	        logger.LogException(methodName+"|Exception Occured", e);
	        errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
		return "searched";


	}







	public String saveGroup() {

		UserGroupDbImpl objGroup = new UserGroupDbImpl(groupNamePriTextBox.getValue().toString());
		GroupPermissionBinding groupPermissionBinding = new GroupPermissionBinding();
		objGroup.setPrimaryName( groupNamePriTextBox.getValue().toString());
		objGroup.setSecondaryName(groupNameSecTextBox.getValue().toString());


		//getPojo and populate it with the values in backing bean come from the Form
		SecurityManager.persistGroup(objGroup);

		ArrayList a = getPermissionList(getListOfPermissionsForGroup().getValue().toString());

		for(int i = 0;i<a.size();i++){


			GroupPermissionId objPermission = new GroupPermissionId();
			objPermission.setPermissionId(a.get(i).toString());
			objPermission.setAppId("Pims");
			objPermission.setUserGroupId(groupNamePriTextBox.getValue().toString());


			groupPermissionBinding.setGroupPermissionId(objPermission);
			//where to add groupID
			SecurityManager.persistBinding(groupPermissionBinding);
		}

		ArrayList b = getPermissionList(getListOfPermissionsForGroup().getValue().toString());
		for(int i = 0;i<b.size();i++){
			GroupPermissionId objPermission = new GroupPermissionId();
			objPermission.setPermissionId(a.get(i).toString());
			objPermission.setAppId("Pims");
			objPermission.setUserGroupId(groupNamePriTextBox.getValue().toString());


			groupPermissionBinding.setGroupPermissionId(objPermission);

			SecurityManager.deleteBinding(groupPermissionBinding);
		}






		//getPojo and populate it with the values in backing bean come from the Form

		ResourceBundle bundle = ResourceBundle
		.getBundle("com.avanza.pims.web.messageresource.Messages");
		String msg = "";

		msg = bundle.getString("commons.save");

		getFacesContext().addMessage(null, new FacesMessage(msg));

		return "success";

	}

	//Private Methods
	private ArrayList getPermissionList(String strPermissionListCommaSeperated){

		ArrayList<String> ret=new ArrayList();
		String strArr[]= strPermissionListCommaSeperated.split(",");
		for(int i = 0;i<strArr.length;i++){
			if (!ret.contains(strArr[i])) ret.add(strArr[i]);
		}




		return ret;


	}
	private void loadPermissionsNotForGroupList() {

		//Here we need to change the source of data
		//with the list of POJO	
		Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

		List<Permission> allGroupsList = SecurityManager.findPermission(query);


		permissionsNotForGroup = new ArrayList();

		for (int i = 0; i < allGroupsList.size(); i++) {
			permissionsNotForGroup.add(new SelectItem(allGroupsList.get(i).getPermissionId(),
					allGroupsList.get(i).getPrimaryName()));
			getPermissionsForGroupList().setValue(new SelectItem(permissionsNotForGroup.get(i)));
		}	



	}

	private void loadPermissionsForGroupList(UserGroupDbImpl userGroup) {
		Search query = new Search();

		query.clear();

		query.addFrom(UserGroupDbImpl.class);

		Set userGroupSet = userGroup.getGroupPermissionBindings();
		String strGroupId;
		Permission objPermission;
		List permissionList = new ArrayList();

		while(userGroupSet!=null&&userGroupSet.iterator().hasNext()){
			strGroupId = ((GroupPermissionBinding)userGroupSet.iterator().next()).getPermissionId();


			objPermission = SecurityManager.getPermission(strGroupId);
			permissionList.add(objPermission);

		}
		for (int i = 0; i < permissionList.size(); i++) {
			permissionsForGroup.add(new SelectItem(((UserGroup)permissionList.get(i)).getUserGroupId(),
					((UserGroup)permissionList.get(i)).getPrimaryName()));
			getPermissionsForGroupList().setValue(new SelectItem(permissionsForGroup.get(i)));
		}


		//Here we need to change the source of data
		//with the list of POJO

	}



	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public HtmlInputText getUserNamePriTextBox() {
		return userNamePriTextBox;
	}

	public void setUserNamePriTextBox(HtmlInputText userNamePriTextBox) {
		this.userNamePriTextBox = userNamePriTextBox;
	}

	public String getUserNamePrm() {
		return userNamePrm;
	}

	public void setUserNamePrm(String userNamePrm) {
		this.userNamePrm = userNamePrm;
	}

	public String getUserNameSec() {
		return userNameSec;
	}

	public void setUserNameSec(String userNameSec) {
		this.userNameSec = userNameSec;
	}

	public HtmlInputText getUserNameSecTextBox() {
		return userNameSecTextBox;
	}

	public void setUserNameSecTextBox(HtmlInputText userNameSecTextBox) {
		this.userNameSecTextBox = userNameSecTextBox;
	}

	public List<SelectItem> getPermissionsForGroup() {
		return permissionsForGroup;
	}

	public void setPermissionsForGroup(List<SelectItem> permissionsForGroup) {
		this.permissionsForGroup = permissionsForGroup;
	}

	public HtmlSelectManyListbox getPermissionsForGroupList() {
		return permissionsForGroupList;
	}

	public void setPermissionsForGroupList(
			HtmlSelectManyListbox permissionsForGroupList) {
		this.permissionsForGroupList = permissionsForGroupList;
	}

	public List<SelectItem> getPermissionsNotForGroup() {
		return permissionsNotForGroup;
	}

	public void setPermissionsNotForGroup(List<SelectItem> permissionsNotForGroup) {
		this.permissionsNotForGroup = permissionsNotForGroup;
	}

	public HtmlSelectManyListbox getPermissionsNotForGroupList() {
		return permissionsNotForGroupList;
	}

	public void setPermissionsNotForGroupList(
			HtmlSelectManyListbox permissionsNotForGroupList) {
		this.permissionsNotForGroupList = permissionsNotForGroupList;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		if(dataList==null)
		loadDataList();
		loadPermissionsNotForGroupList();

		super.init();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}

	public HtmlInputHidden getListOfPermissionsForGroup() {
		return listOfPermissionsForGroup;
	}

	public void setListOfPermissionsForGroup(
			HtmlInputHidden listOfPermissionsForGroup) {
		this.listOfPermissionsForGroup = listOfPermissionsForGroup;
	}

	public HtmlInputHidden getListOfPermissionsNotForGroup() {
		return listOfPermissionsNotForGroup;
	}

	public void setListOfPermissionsNotForGroup(
			HtmlInputHidden listOfPermissionsNotForGroup) {
		this.listOfPermissionsNotForGroup = listOfPermissionsNotForGroup;
	}
	

	public String editDataItem() {

		// Get selected MyData item to be edited.
		dataItem = (UserGroupDbImpl) dataTable.getRowData();
		setRequestParam(WebConstants.EDIT_PARAM,dataItem.getUserGroupId());       // Store the ID of the data item in hidden input element.
		 
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Group.GROUP_OBJECT, dataItem);
		    // Store the ID of the data item in hidden input element.


		return "edit";
	}
	public String setPermission() {

		// Get selected MyData item to be edited.
		dataItem = (UserGroupDbImpl) dataTable.getRowData();
		setRequestParam("View_Permission",dataItem.getUserGroupId());       // Store the ID of the data item in hidden input element.
		setRequestParam("groupObj",dataItem);   
		    // Store the ID of the data item in hidden input element.


		return "showPermission";
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object list = viewRootMap.get("dataList");
		if(list != null){
			return ((List<UserGroup>)list).size(); 
		}
		else{
			return 0;
		}
	}

	public String deleteGroup()
	{
		dataItem = (UserGroupDbImpl) dataTable.getRowData();
		if (dataItem.getUserGroupId().equals("Admin Group") || dataItem.getUserGroupId().equals("AMAF Inspection Team") || dataItem.getUserGroupId().equals("AMAF Rent Committee"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UserGroup.GROUP_DELETION_NOT_ALLOWED));
		}	
		else if (dataItem.getUsers()!=null && dataItem.getUsers().size()>0)
		{
	  		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UserGroup.GROUP_CANNOT_BE_DELETED));
		}	
		else
		{
		SecurityManager.deleteUserGroup(dataItem);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UserGroup.GROUP_DELETED));
		}
		loadDataListAfterDeletion();
		return "success";
	}


	public String getMessages() {
		return CommonUtil.getErrorMessages(messages);
	}


	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	
}