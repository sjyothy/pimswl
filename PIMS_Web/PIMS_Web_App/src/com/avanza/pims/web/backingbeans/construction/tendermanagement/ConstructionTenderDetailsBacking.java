package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.Date;
import java.util.Map;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.ConstructionTenderServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.project.ProjectSearchBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ConstructionTenderDetailsView;
import com.avanza.pims.ws.vo.DomainDataView;

public class ConstructionTenderDetailsBacking extends AbstractController {
	
	private static final long serialVersionUID = -8946871547370814258L;
	
	private Logger logger = Logger.getLogger(ConstructionTenderDetailsBacking.class);
	private Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private CommonUtil commonUtil = new CommonUtil();
	
	public HtmlInputText txtTenderNumber = new HtmlInputText();
	public HtmlSelectOneMenu ddnTenderType = new HtmlSelectOneMenu();
	public HtmlSelectOneMenu ddnConstructionServiceType = new HtmlSelectOneMenu();
	public HtmlSelectOneMenu ddnTenderStatus = new HtmlSelectOneMenu();
	public HtmlInputText txtProjectNumber = new HtmlInputText();
	public HtmlInputText txtProjectName = new HtmlInputText();
	public HtmlInputText txtProjectEstimatedCost = new HtmlInputText();
	public HtmlInputText txtTenderDocSellingPrice = new HtmlInputText();
	public HtmlCalendar calTenderIssueDate = new HtmlCalendar();
	public HtmlCalendar calTenderEndDate = new HtmlCalendar();
	public HtmlGraphicImage imgProjectSearch = new HtmlGraphicImage();
	public HtmlInputText txtTenderDescription = new HtmlInputText();
	
	private ConstructionTenderServiceAgent constructionTenderServiceAgent = new ConstructionTenderServiceAgent();
	private ConstructionTenderDetailsView constructionTenderDetailsView = new ConstructionTenderDetailsView();
	private String pageMode = WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_NEW;
	private Long constructionTenderId = null;

	public ConstructionTenderDetailsBacking() {
		
	}
	
	public void init() {
		String METHOD_NAME = "init()";
		logger.logInfo(METHOD_NAME + " --- started --- ");
		super.init();

		try	{			
			DomainDataView ddv = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(Constant.TENDER_STATUS), Constant.TENDER_STATUS_NEW);
			DomainDataView ddv2 = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.TENDER_TYPE_NEW), WebConstants.TENDER_TYPE_CONSTRUCTION);
			constructionTenderDetailsView.setTenderStatusId( String.valueOf( ddv.getDomainDataId() ) );
			constructionTenderDetailsView.setTenderTypeId(String.valueOf( ddv2.getDomainDataId() ));
			constructionTenderDetailsView.setTenderIssueDate(new Date());
			updateValuesFromMap();
			logger.logInfo(METHOD_NAME + " --- completed successfully --- ");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- crashed --- ", exception);
		}
	}
		
	public void preprocess() {
		super.preprocess();
	}
	
	public void prerender() {
		super.prerender();		
	}
	
	private void updateValuesFromMap() throws Exception {
		
		
		if ( viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY ) != null )		
			pageMode = (String) viewMap.get( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY );
		
		if ( viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID ) != null )		
			constructionTenderId = (Long) viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID );
				
		if ( viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW ) != null )
			constructionTenderDetailsView = (ConstructionTenderDetailsView) viewMap.get( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW );
		else if ( constructionTenderId != null )
			constructionTenderDetailsView = constructionTenderServiceAgent.getConstructionTenderDetailsViewById( constructionTenderId );
		
		changePageMode(pageMode);
	}
	
	public void changePageMode( String pageMode ) throws Exception {
		this.pageMode = pageMode;
		updateValuesToMap();
		updatePageForMode(this.pageMode);
	}
	
	private void updateValuesToMap() {		
		// Page Mode
		viewMap.put( WebConstants.ConstructionTender.Mode.CONSTRUCTION_TENDER_MODE_KEY, pageMode );
		
		// Construction Tender Id
		if ( constructionTenderId != null )
			viewMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_ID, constructionTenderId );
		
		// Construction Tender Detail View
		viewMap.put( WebConstants.ConstructionTender.CONSTRUCTION_TENDER_DETAIL_VIEW, constructionTenderDetailsView );
	}
	
	public String getProjectKey_PageMode() {
		return ProjectSearchBean.Keys.PAGE_MODE;
    }
    public String getProjectKey_PageModeSelectOnPopUp() {        	
    	return ProjectSearchBean.Keys.PAGE_MODE_SELECT_ONE_POPUP;        	
    }
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}

	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public ConstructionTenderDetailsView getConstructionTenderDetailsView() {
		return constructionTenderDetailsView;
	}

	public void setConstructionTenderDetailsView(
			ConstructionTenderDetailsView constructionTenderDetailsView) {
		this.constructionTenderDetailsView = constructionTenderDetailsView;
	}

	public ConstructionTenderServiceAgent getConstructionTenderServiceAgent() {
		return constructionTenderServiceAgent;
	}

	public void setConstructionTenderServiceAgent(
			ConstructionTenderServiceAgent constructionTenderServiceAgent) {
		this.constructionTenderServiceAgent = constructionTenderServiceAgent;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public Long getConstructionTenderId() {
		return constructionTenderId;
	}

	public void setConstructionTenderId(Long constructionTenderId) {
		this.constructionTenderId = constructionTenderId;
	}

	public HtmlInputText getTxtTenderNumber() {
		return txtTenderNumber;
	}

	public void setTxtTenderNumber(HtmlInputText txtTenderNumber) {
		this.txtTenderNumber = txtTenderNumber;
	}

	public HtmlSelectOneMenu getDdnTenderType() {
		return ddnTenderType;
	}

	public void setDdnTenderType(HtmlSelectOneMenu ddnTenderType) {
		this.ddnTenderType = ddnTenderType;
	}

	public HtmlSelectOneMenu getDdnConstructionServiceType() {
		return ddnConstructionServiceType;
	}

	public void setDdnConstructionServiceType(
			HtmlSelectOneMenu ddnConstructionServiceType) {
		this.ddnConstructionServiceType = ddnConstructionServiceType;
	}

	public HtmlSelectOneMenu getDdnTenderStatus() {
		return ddnTenderStatus;
	}

	public void setDdnTenderStatus(HtmlSelectOneMenu ddnTenderStatus) {
		this.ddnTenderStatus = ddnTenderStatus;
	}

	public HtmlInputText getTxtProjectNumber() {
		return txtProjectNumber;
	}

	public void setTxtProjectNumber(HtmlInputText txtProjectNumber) {
		this.txtProjectNumber = txtProjectNumber;
	}

	public HtmlInputText getTxtProjectName() {
		return txtProjectName;
	}

	public void setTxtProjectName(HtmlInputText txtProjectName) {
		this.txtProjectName = txtProjectName;
	}

	public HtmlInputText getTxtProjectEstimatedCost() {
		return txtProjectEstimatedCost;
	}

	public void setTxtProjectEstimatedCost(HtmlInputText txtProjectEstimatedCost) {
		this.txtProjectEstimatedCost = txtProjectEstimatedCost;
	}

	public HtmlInputText getTxtTenderDocSellingPrice() {
		return txtTenderDocSellingPrice;
	}

	public void setTxtTenderDocSellingPrice(HtmlInputText txtTenderDocSellingPrice) {
		this.txtTenderDocSellingPrice = txtTenderDocSellingPrice;
	}

	public HtmlCalendar getCalTenderIssueDate() {
		return calTenderIssueDate;
	}

	public void setCalTenderIssueDate(HtmlCalendar calTenderIssueDate) {
		this.calTenderIssueDate = calTenderIssueDate;
	}

	public HtmlCalendar getCalTenderEndDate() {
		return calTenderEndDate;
	}

	public void setCalTenderEndDate(HtmlCalendar calTenderEndDate) {
		this.calTenderEndDate = calTenderEndDate;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public HtmlGraphicImage getImgProjectSearch() {
		return imgProjectSearch;
	}

	public void setImgProjectSearch(HtmlGraphicImage imgProjectSearch) {
		this.imgProjectSearch = imgProjectSearch;
	}

	public HtmlInputText getTxtTenderDescription() {
		return txtTenderDescription;
	}

	public void setTxtTenderDescription(HtmlInputText txtTenderDescription) {
		this.txtTenderDescription = txtTenderDescription;
	}
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	

}
