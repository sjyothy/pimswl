package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class cancelRemarksPopupBean extends AbstractController 
{
	FacesContext context = FacesContext.getCurrentInstance();
	Map viewRootMap = context.getViewRoot().getAttributes();
	Map sessionMap= context.getExternalContext().getSessionMap();
	CommonUtil commUtil=new CommonUtil();
	Logger logger = Logger.getLogger(cancelRemarksPopupBean.class);
	private List<String> errorMessages=new ArrayList<String>();
	private String remarks;
	public void init() 
	{
		super.init();
	}
	public void saveRemarks()
	{
		
		if(remarks!=null && remarks.trim().length()>0)
			{
				if(remarks.length()>=250)
					errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.descriptionSize"));
				else 
				{
				   sessionMap.put("CANCEL_REMARKS", remarks.trim());
				   executeJavascript("closeAndSendRemarksToParent();");
				}  
			}
		else
			errorMessages.add(CommonUtil.getBundleMessage("commons.RequiredRemarks"));
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public void executeJavascript(String javascript) {
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);		
		} 
		catch (Exception exception) 
		{			
			logger.LogException(" executing JavaScript CRASHED --- ", exception);
		}
	}
	 public String getErrorMessages()
		{
			return CommonUtil.getErrorMessages(errorMessages);
		}
}