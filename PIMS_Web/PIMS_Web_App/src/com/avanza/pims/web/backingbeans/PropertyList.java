package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneListbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RegionView;

public class PropertyList extends AbstractController {

	Logger logger = Logger.getLogger(PropertyList.class);
	
	private static final long serialVersionUID = 8832446416083043942L;
	
	private HtmlSelectOneListbox ddlTypeId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlStatusId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlCategoryId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlUsageTypeId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlInvestmentPurposeId = new HtmlSelectOneListbox();
    private HtmlSelectOneListbox ddlOwnerShipTypeId = new HtmlSelectOneListbox();
    Map viewMap = getFacesContext().getViewRoot().getAttributes();
	private HtmlInputHidden hdnPropertyId = new HtmlInputHidden();
	
	private String pageMode;
	private String MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP"; 
	private String MODE_SELECT_ONE_POPUP ="MODE_SELECT_ONE_POPUP";
	private HtmlInputText txtPropertyNumber = new HtmlInputText();
	private HtmlInputText txtFinancialAccountNumber = new HtmlInputText();
    private HtmlInputText txtNoOfUnits = new HtmlInputText();
    private HtmlInputText txtEndowedName = new HtmlInputText();
    private HtmlInputText txtCommercialName = new HtmlInputText();
    private HtmlInputText txtLandNumber = new HtmlInputText();
    private HtmlInputText txtLandDeptCode = new HtmlInputText();
    private HtmlInputText txtMunicipalityPlanNo = new HtmlInputText();
    private HtmlInputText txtProjectNo = new HtmlInputText();
    private HtmlInputText txtLandArea = new HtmlInputText();
    private HtmlInputText txtBuiltInArea = new HtmlInputText();
    private HtmlInputText txtNoFloors = new HtmlInputText();
    private HtmlInputText txtNoOfMezzanineFloors = new HtmlInputText();
    private HtmlInputText txtNoOfParkingFloors = new HtmlInputText();
    private HtmlInputText txtNoOfFlats = new HtmlInputText();
    private HtmlInputText txtNoOfLifts = new HtmlInputText();
    private HtmlInputText txtDewaNumber = new HtmlInputText();
    private HtmlSelectOneMenu  cmbOwnerShipType = new HtmlSelectOneMenu();
    private HtmlDataTable dataTable = new HtmlDataTable();
	private PropertyView propertyBean = new PropertyView(); 
	private List<PropertyView> propertyViewList = new ArrayList<PropertyView>();

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String typeId;
	private String statusId;
	private String usageId;
	private String countryId;
	private String stateId;
	private String cityId;
	private String ownerShipTypeId;
	private String selectedCountryId;
	private String costCenter;
	private String landNumber;
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private String DEFAULT_SORT_FIELD = "propertyNumber";
	
	public String getLandNumber() {
		return landNumber;
	}
	public void setLandNumber(String landNumber) {
		this.landNumber = landNumber;
	}
	public PropertyList()
	{
		
	}
	@SuppressWarnings( "unchecked" )
	public String generateFloors()
	{
		
			getFacesContext().getExternalContext().getSessionMap()
			.put(WebConstants.GenerateFloors.PROPERTY_VIEW,(PropertyView)dataTable.getRowData());

			final String viewId = "/propertyList.jsf"; 

			FacesContext facesContext = FacesContext.getCurrentInstance();

			// This is the proper way to get the view's url
			ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
			String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			String javaScriptText = "javascript:GenerateFloorsPopup();";

			// Add the Javascript to the rendered page's header for immediate execution
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		
		
		return "";
	}
	
	public String generateUnits()
	{
		getFacesContext().getExternalContext().getSessionMap()
		.put(WebConstants.GenerateUnits.PROPERTY_VIEW,(PropertyView)dataTable.getRowData());
		
		final String viewId = "/propertyList.jsf";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
		.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		String javaScriptText = "javascript:GenerateUnitsPopup();";
		
		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		
		return "";
	}
	
	public String edit()
	{
		PropertyView propertyView = (PropertyView)dataTable.getRowData();
		
		getFacesContext().getExternalContext().getSessionMap()
		.put(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY,propertyView);
		
		
		return "editProperty";
	}
	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getUsageId() {
		return usageId;
	}
	public void setUsageId(String usageId) {
		this.usageId = usageId;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}	
	public PropertyView getPropertyBean() {
		return propertyBean;
	}

	public void setPropertyBean(PropertyView propertyBean) {
		this.propertyBean = propertyBean;
	}

	public HtmlSelectOneListbox getDdlTypeId() {
		return ddlTypeId;
	}

	public void setDdlTypeId(HtmlSelectOneListbox ddlTypeId) {
		this.ddlTypeId = ddlTypeId;
	}

	public HtmlSelectOneListbox getDdlStatusId() {
		return ddlStatusId;
	}

	public void setDdlStatusId(HtmlSelectOneListbox ddlStatusId) {
		this.ddlStatusId = ddlStatusId;
	}

	public HtmlSelectOneListbox getDdlCategoryId() {
		return ddlCategoryId;
	}

	public void setDdlCategoryId(HtmlSelectOneListbox ddlCategoryId) {
		this.ddlCategoryId = ddlCategoryId;
	}

	public HtmlSelectOneListbox getDdlUsageTypeId() {
		return ddlUsageTypeId;
	}

	public void setDdlUsageTypeId(HtmlSelectOneListbox ddlUsageTypeId) {
		this.ddlUsageTypeId = ddlUsageTypeId;
	}

	public HtmlSelectOneListbox getDdlInvestmentPurposeId() {
		return ddlInvestmentPurposeId;
	}

	public void setDdlInvestmentPurposeId(
			HtmlSelectOneListbox ddlInvestmentPurposeId) {
		this.ddlInvestmentPurposeId = ddlInvestmentPurposeId;
	}

	public HtmlInputHidden getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(HtmlInputHidden hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlInputText getTxtPropertyNumber() {
		return txtPropertyNumber;
	}

	public void setTxtPropertyNumber(HtmlInputText txtPropertyNumber) {
		this.txtPropertyNumber = txtPropertyNumber;
	}

	public HtmlInputText getTxtNoOfUnits() {
		return txtNoOfUnits;
	}

	public void setTxtNoOfUnits(HtmlInputText txtNoOfUnits) {
		this.txtNoOfUnits = txtNoOfUnits;
	}

	public HtmlInputText getTxtEndowedName() {
		return txtEndowedName;
	}

	public void setTxtEndowedName(HtmlInputText txtEndowedName) {
		this.txtEndowedName = txtEndowedName;
	}

	public HtmlInputText getTxtCommercialName() {
		return txtCommercialName;
	}

	public void setTxtCommercialName(HtmlInputText txtCommercialName) {
		this.txtCommercialName = txtCommercialName;
	}

	public HtmlInputText getTxtLandNumber() {
		return txtLandNumber;
	}

	public void setTxtLandNumber(HtmlInputText txtLandNumber) {
		this.txtLandNumber = txtLandNumber;
	}

	public HtmlInputText getTxtLandDeptCode() {
		return txtLandDeptCode;
	}

	public void setTxtLandDeptCode(HtmlInputText txtLandDeptCode) {
		this.txtLandDeptCode = txtLandDeptCode;
	}

	public HtmlInputText getTxtMunicipalityPlanNo() {
		return txtMunicipalityPlanNo;
	}

	public void setTxtMunicipalityPlanNo(HtmlInputText txtMunicipalityPlanNo) {
		this.txtMunicipalityPlanNo = txtMunicipalityPlanNo;
	}

	public HtmlInputText getTxtProjectNo() {
		return txtProjectNo;
	}

	public void setTxtProjectNo(HtmlInputText txtProjectNo) {
		this.txtProjectNo = txtProjectNo;
	}

	public HtmlInputText getTxtLandArea() {
		return txtLandArea;
	}

	public void setTxtLandArea(HtmlInputText txtLandArea) {
		this.txtLandArea = txtLandArea;
	}

	public HtmlInputText getTxtBuiltInArea() {
		return txtBuiltInArea;
	}

	public void setTxtBuiltInArea(HtmlInputText txtBuiltInArea) {
		this.txtBuiltInArea = txtBuiltInArea;
	}

	public HtmlInputText getTxtNoFloors() {
		return txtNoFloors;
	}

	public void setTxtNoFloors(HtmlInputText txtNoFloors) {
		this.txtNoFloors = txtNoFloors;
	}

	public HtmlInputText getTxtNoOfMezzanineFloors() {
		return txtNoOfMezzanineFloors;
	}

	public void setTxtNoOfMezzanineFloors(HtmlInputText txtNoOfMezzanineFloors) {
		this.txtNoOfMezzanineFloors = txtNoOfMezzanineFloors;
	}

	public HtmlInputText getTxtNoOfParkingFloors() {
		return txtNoOfParkingFloors;
	}

	public void setTxtNoOfParkingFloors(HtmlInputText txtNoOfParkingFloors) {
		this.txtNoOfParkingFloors = txtNoOfParkingFloors;
	}

	public HtmlInputText getTxtNoOfFlats() {
		return txtNoOfFlats;
	}

	public void setTxtNoOfFlats(HtmlInputText txtNoOfFlats) {
		this.txtNoOfFlats = txtNoOfFlats;
	}

	public HtmlInputText getTxtNoOfLifts() {
		return txtNoOfLifts;
	}

	public void setTxtNoOfLifts(HtmlInputText txtNoOfLifts) {
		this.txtNoOfLifts = txtNoOfLifts;
	}

	public HtmlInputText getTxtDewaNumber() {
		return txtDewaNumber;
	}

	public void setTxtDewaNumber(HtmlInputText txtDewaNumber) {
		this.txtDewaNumber = txtDewaNumber;
	}
	
	public String btnSearchAction() {
		logger.logInfo("Property List - SearchAction start ...");
		try 
		{
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			

		} catch(Exception e){
			logger.LogException(e.getMessage(), e);
		} finally {
			logger.logInfo("btnSaveAction ends ...");
		}
		return "success";
	}
	
	public String btnCancelAction() {
		return "success";
	}

	public HtmlInputText getTxtFinancialAccountNumber() {
		return txtFinancialAccountNumber;
	}

	public void setTxtFinancialAccountNumber(HtmlInputText txtFinancialAccountNumber) {
		this.txtFinancialAccountNumber = txtFinancialAccountNumber;
	}
	// Sorting actions ----------------------------------------------------------------------------

	public void doSearchItemList()
	{
		searchProperty();
	}
	@SuppressWarnings( "unchecked" )
	public String searchProperty()
	{
		try
		{
		
		if( this.getCostCenter()!=null && this.getCostCenter().trim().length() > 0  )
			this.propertyBean.setCostCenter(  this.getCostCenter() );
		
		if( this.getOwnerShipTypeId()!=null && !this.getOwnerShipTypeId().equals("0") )
			this.propertyBean.setCategoryId( new Long ( this.getOwnerShipTypeId() ) );
		if( this.getTypeId() != null && !this.getTypeId().equals("0") )
			this.propertyBean.setTypeId(new Long(this.getTypeId()));
		if(this.getStatusId() != null && !this.getStatusId().equals("0") )
			this.propertyBean.setStatusId(new Long(this.getStatusId()));
		if(this.getCityId() != null && !this.getCityId().equals("0") )
			this.propertyBean.setCityId(new Long(this.getCityId()));
		if(this.getUsageId() != null && !this.getUsageId().equals("0") )
			this.propertyBean.setUsageTypeId(new Long(this.getUsageId()));
		
		if (selectedCountryId!=null && !selectedCountryId.equals("0"))
			propertyBean.setCountryId(new Long(selectedCountryId));
		
		if (stateId!=null && !stateId.equals("0"))
			propertyBean.setStateId(new Long(stateId));
		
		HashMap searchCriteriaMap = new HashMap(); 
		if( viewMap.get( "context" ) != null  )
		{
			String context = viewMap.get( "context" ).toString();
			if( context.equals("endowment") )
			{
				searchCriteriaMap.put("emptyendid", 1 );
			}
		}
		
		PropertyService ps = new PropertyService();
		
		/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
		int totalRows =  ps.searchPropertyGetTotalNumberOfRecords(this.propertyBean);
		setTotalRows(totalRows);
		doPagingComputations();
        //////////////////////////////////////////////////////////////////////////////////////////////

        List<PropertyView> propertyViewList =  ps.searchProperty(this.propertyBean,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending(),searchCriteriaMap);       
        this.setPropertyViewList(propertyViewList);
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("propertyListRecordSize", totalRows);
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("propertyList", propertyViewList);
		
		}
		catch(PimsBusinessException pbe)
		{
		logger.logError("PimsBusinessException occured in Property List Backing Bean");	
		}
		catch(Exception e)
		{
		logger.logError("Exception occured in Property List Backing Bean");	
		}
		return "";
	}	
	public Integer getRecordSize() {
		
		Integer recordSize = 0;
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("propertyListRecordSize") != null)
			recordSize = (Integer) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("propertyListRecordSize");
		
		return recordSize;
	}
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() {		
		super.init();
			loadCountry();
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
		        setSortItemListAscending(true);
		        
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", new ArrayList<SelectItem>(0));
				if(request.getParameter("context")!=null )
				{
					 viewMap.put("context", request.getParameter("context").toString());
				}
	    		if(request.getParameter("pageMode")!=null&&
	    				request.getParameter("pageMode").equals(MODE_SELECT_ONE_POPUP)) 
      	    		viewMap.put("pageMode",MODE_SELECT_ONE_POPUP );
      	    	else if(request.getParameter("pageMode")!=null&&
      	    			request.getParameter("pageMode").equals(MODE_SELECT_MANY_POPUP))
      	    		viewMap.put("pageMode",MODE_SELECT_MANY_POPUP );
	    		if(viewMap.get("pageMode")==null)
	    			viewMap.put("pageMode","fullScreenMode");
	    		getIsViewModePopUp();
			}
	}
	public boolean getIsPageModeSelectOnePopUp()
	{
		if(viewMap.get("pageMode")!=null)
		{
			if(viewMap.get("pageMode").toString().equals(MODE_SELECT_ONE_POPUP))
				return true;
			else
				return false;
		}
		
		return false;
	}
	public boolean getIsViewModePopUp()
	{
		if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
			return true;
		else
			return false;
		
	}
	public boolean getIsPageModeSelectManyPopUp()
	{
		boolean  isPageModeSelectManyPopUp= true;
		if(viewMap.get("pageMode")!=null)
		{
			if(viewMap.get("pageMode").toString().equals(MODE_SELECT_MANY_POPUP))
				isPageModeSelectManyPopUp= true;
			else
				isPageModeSelectManyPopUp= false;
		}
		else
			isPageModeSelectManyPopUp =false;
		return isPageModeSelectManyPopUp;
	}

	
	public void loadCountry()  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      
			
		      this.getCountryList().add(item);
		  }
		
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("country", countryList);
		
		}catch (Exception e){
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}
	@SuppressWarnings( "unchecked" ) 
	public void loadState(ValueChangeEvent vce)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "";
		for (int i=0;i<this.getCountryList().size();i++)
		{
			if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
			{
				selectedCountryName = 	this.getCountryList().get(i).getLabel();
			}
		}	
		
		List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.LogException( "LoadState|Error Occured|",e);
			
		}
	}
	@SuppressWarnings("unchecked")
	public void  loadCity(ValueChangeEvent vce )
	{
		String methodName="loadCity"; 
		logger.logInfo(methodName+"|"+"Start"); 
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try 
		{
			 Set<RegionView> regionViewList = null;
			 if ( vce.getNewValue() != null && vce.getNewValue().toString().length()>0 && 
				  !vce.getNewValue().toString().equals( "0" )
			    )
			 {
				 regionViewList =  psa.getCity( new Long( vce.getNewValue().toString() ) );
				Iterator itrator = regionViewList.iterator();
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)itrator.next();
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  
					  }
					  else 
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      
					  cmbCityList.add(item);
				  }
				
				Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
			 }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", cmbCityList);
			 logger.logInfo(methodName+"|"+"Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured ",ex);
			
		}
	   }


	public String selectProperty(){
		PropertyView propertyView = (PropertyView)dataTable.getRowData();
		ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.UnitDetails.PROPERTY_VIEW, propertyView);		
		//start:adding inline Javascript to submit parent and close this window
		/////////////////////////////////////////////////////////////////////////////
		final String viewId = "/propertyListPopup.jsp"; 
		
        FacesContext facesContext = FacesContext.getCurrentInstance();        

        // This is the proper way to get the view's url
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);

        //Your Java script code here        
        
        String javaScriptText = "window.opener.document.forms[0].submit();\n"+
        						"window.close();"; 
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		//end:adding inline Javascript to submit parent and close this window
		return "";
	}
	
	public boolean getIsArabicLocale()
	{
		
		
		isArabicLocale = !getIsEnglishLocale();
		
		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		System.out.println("PropertyListBackingBean preprocess");
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		getIsViewModePopUp();
		if( viewMap.get( "context" ) != null  )
		{
			String context = viewMap.get( "context" ).toString();
			if( context.equals("endowment") )
			{
				this.setOwnerShipTypeId(  String.valueOf( WebConstants.OwnerShipType.ENDOWED_ID ) );
				cmbOwnerShipType.setDisabled(true);
			}
		}
	}

	public List<PropertyView> getPropertyViewList() {
		
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("propertyList") != null)
			propertyViewList = (List<PropertyView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("propertyList");
		
		return propertyViewList;
	}
	public void sendManyPropertyToParent()
	{
	       // UnitView unitViewRow=(UnitView)dataTable.getRowData();
	        
	        String javaScriptText ="window.opener.document.forms[0].submit();" +
	        		               "window.close();"; 
	        putSelectedUnitsInSession();
	        Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	        
	        AddResource addResource = AddResourceFactory.getInstance(getFacesContext());
	        addResource.addInlineScriptAtPosition(getFacesContext(), AddResource.HEADER_BEGIN, javaScriptText);
	        
	    }
	private void putSelectedUnitsInSession()
	{
		String methodName="putSelectedUnitsInSession";
		logger.logInfo(methodName+"|started...");
		try
		{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List <PropertyView> propertyViewList = (List<PropertyView>)viewMap.get("propertyList");
		String selectedUnits="";
		List<PropertyView> selectedPropertyList =new ArrayList<PropertyView>(0);
		for (PropertyView propertyView : propertyViewList) 
		{
			if(propertyView!=null && propertyView.getSelected()!=null && propertyView.getSelected())
				selectedPropertyList.add(propertyView);
			
		}
		getFacesContext().getExternalContext().getSessionMap().put("MANYPROPERTYINFO", selectedPropertyList);
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|ErrorOccured...",ex);
		}
		
	}
	

	public void setPropertyViewList(List<PropertyView> propertyView) {
		this.propertyViewList = propertyView;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public List<SelectItem> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public List<SelectItem> getStateList() {
		return stateList;
	}
	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}
	public String getSelectedCountryId() {
		return selectedCountryId;
	}
	public void setSelectedCountryId(String selectedCountryId) {
		this.selectedCountryId = selectedCountryId;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {		
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public Integer getPaginatorRows() {
		Integer paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String addProperty(){
		return "addProperty";
	}
	public HtmlSelectOneListbox getDdlOwnerShipTypeId() {
		return ddlOwnerShipTypeId;
	}
	public void setDdlOwnerShipTypeId(HtmlSelectOneListbox ddlOwnerShipTypeId) {
		this.ddlOwnerShipTypeId = ddlOwnerShipTypeId;
	}
	@SuppressWarnings("unchecked")
	public String getOwnerShipTypeId() {
		if(	viewMap.get("ownerShipTypeId") != null )
		{
			this.ownerShipTypeId = viewMap.get("ownerShipTypeId").toString();
		}
		return ownerShipTypeId;
	}
	@SuppressWarnings("unchecked")
	public void setOwnerShipTypeId(String ownerShipTypeId) {
		this.ownerShipTypeId = ownerShipTypeId;
		viewMap.put("ownerShipTypeId", this.ownerShipTypeId);
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	@SuppressWarnings("unchecked")
	public String openPropertyDetailsToAddRentAmount()
	{
			PropertyView propertyView = (PropertyView)dataTable.getRowData();
			
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY,propertyView);
			getFacesContext().getExternalContext().getSessionMap().put("ADD_RENT_AMOUNT_MODE",true);
			
			return "ChangePropertyUnitRent";
	}
	public HtmlSelectOneMenu getCmbOwnerShipType() {
		return cmbOwnerShipType;
	}
	public void setCmbOwnerShipType(HtmlSelectOneMenu cmbOwnerShipType) {
		this.cmbOwnerShipType = cmbOwnerShipType;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	
}
