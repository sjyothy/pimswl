package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationActionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationView;

public class ViolationStatusPopUpBean extends AbstractController {

	public String violationId;
	public String selectedStatus;
	public String description;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(ViolationStatusPopUpBean.class);
	private List<SelectItem> statusList=new ArrayList<SelectItem>();
	InspectionView inspectionView = new InspectionView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;

	// Constructors

	/** default constructor */
	public ViolationStatusPopUpBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		String method="getIsEnglishLocale";
		logger.logInfo(method+"|"+"Start...");
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		logger.logInfo(method+"|"+"Finish...");
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	


	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		}

	@Override
	public void preprocess() {
		super.preprocess();
		

	}

	@Override
	public void prerender() {
		String methodName="prerender";
		
		logger.logInfo(methodName + "|" + "Start");
		super.prerender();
		try {
			if (!isPostBack()) {
				if(sessionMap.containsKey("violation"))
				{
					ViolationView violationView=(ViolationView)sessionMap.get("violation");
					sessionMap.remove("violation");
					
					if(violationView.getStatusId()!=null)
					{
						logger.logInfo(methodName + "|Status:" + violationView.getStatusId().toString());
						selectedStatus=violationView.getStatusId().toString();
					}
					logger.logInfo(methodName + "|Description:" + violationView.getDescription());
					description=violationView.getDescription();
					logger.logInfo(methodName + "|ViolationId:" + violationView.getViolationId());
					violationId=violationView.getViolationId().toString();
				}
			}
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}

		System.out.println("prerender");
	}


	public void btnSave_Click()
	{
        String methodName="btnSave_Click";
		
		logger.logInfo(methodName + "|" + "Start");
		PropertyServiceAgent psa=new PropertyServiceAgent();
		try
		{
		  psa.setViolationStatus(new Long(violationId), new Long(selectedStatus));
		  //HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		  sessionMap.put("FROM_STATUS", "1");
		  
		  //request.setAttribute("FROM_STATUS", "1");
		  
		  CommonUtil.addJavaScript("/ViolationStatusPopUp.jsf","window.opener.document.forms[0].submit();window.close();");
		  
		
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured:"+ex);
			
		}
		
		
		logger.logInfo(methodName + "|" + "Finish");
		
		
	}
	
	
	
	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<SelectItem> getStatusList() {
		
		CommonUtil util=new CommonUtil();
		List<DomainDataView> ddvList= util.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
		statusList.clear();
		for (DomainDataView domainDataView : ddvList) 
		{
			if(!domainDataView.getDataValue().equals(WebConstants.VIOLATION_STATUS_REPORTED)&& 
					        !domainDataView.getDataValue().equals(WebConstants.VIOLATION_STATUS_ACTIONS_DEFINED) )
			{
				   SelectItem item =null;
					   if(getIsEnglishLocale())
					     item=new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn());
					   else
						   item=new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr());
				statusList.add(item);
			}
		}
		
		return statusList;
	}

	public void setStatusList(List<SelectItem> statusList) {
		this.statusList = statusList;
	}

	public String getViolationId() {
		return violationId;
	}

	public void setViolationId(String violationId) {
		this.violationId = violationId;
	}

}