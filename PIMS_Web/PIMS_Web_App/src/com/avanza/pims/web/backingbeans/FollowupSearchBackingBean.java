package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.FollowUp;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.FollowUpService;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.ui.util.ResourceUtil;


public class FollowupSearchBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(FollowupSearchBackingBean.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	/**
	 * 
	 */
	private ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private Map<String,Long> auctionStatusMap = new HashMap<String,Long>();
	
	public FollowupSearchBackingBean(){
		System.out.println("FollowupSearchBackingBean Constructor");
		
	}
	
	private String message;
	
	private AuctionView auctionView = new AuctionView();
	
	private AuctionView dataItem = new AuctionView();
	private List<FollowUpView> dataList = new ArrayList<FollowUpView>();
	
	private List<SelectItem> auctionStatuses = new ArrayList<SelectItem>();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String hhStart = "";
	private String mmStart = "";
	private String hhEnd = "";
	private String mmEnd = "";
	private List<String> errorMessages;
	private String infoMessage = "";
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private Boolean sortAscending = true;
	
	private HtmlInputText htmlInputFollowupNumber = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneFollowupAction = new HtmlSelectOneMenu();
	private HtmlCalendar htmlCalendarFollowupDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarFollowupDateTo = new HtmlCalendar();	
	private HtmlSelectOneMenu htmlSelectOnePaymentType = new HtmlSelectOneMenu();
	private HtmlInputText htmlInputPaymentAmount = new HtmlInputText();
	private HtmlCalendar htmlCalendarPaymentDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarPaymentDateTo = new HtmlCalendar();	
	private HtmlInputText htmlInputChequeNumber = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneBankName = new HtmlSelectOneMenu();
	private HtmlInputText htmlInputPropertyName = new HtmlInputText();
	private HtmlInputText htmlInputUnitNumber = new HtmlInputText();
	private HtmlInputText htmlInputContract = new HtmlInputText();
	private HtmlInputText htmlInputTenantName = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneFollowupStatus = new HtmlSelectOneMenu();
	
	
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	Map viewRootMap;
	String VIEW_MODE="VIEW_MODE";	
	private String  DEFAULT_SORT_FIELD = "followUpNumber";
	/*
	 * Methods
	 */
	
	public boolean getIsViewModePopUp(){
		 
		 if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase("popup"))
			 return true;
		 else 
			 return false;
	 }
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
//			sessionMap.remove("auctionList");
   		    /*sessionMap.remove(WebConstants.FROM_SEARCH);
			sessionMap.remove(WebConstants.AUCTION_STATUS_MAP);
			sessionMap.remove(WebConstants.ROW_ID);
			sessionMap.remove(WebConstants.FIRST_TIME);
			sessionMap.remove(WebConstants.VIEW_MODE);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.SELECT_VENUE_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
			sessionMap.remove(WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ID);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.FULL_VIEW_MODE);*/
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	@SuppressWarnings("unchecked")
	public List<FollowUpView> getFollowupsList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		dataList = (List<FollowUpView>)viewMap.get("followupsList");
		if(dataList==null)
			dataList = new ArrayList<FollowUpView>();
		return dataList;
	}
    private void LoadUnitStatuses()
    {
   	 List<SelectItem> allowedUnitStatuses = new ArrayList<SelectItem>(); 
   	 boolean isEnglish  = getIsEnglishLocale();
   	 UtilityManager um = new UtilityManager();
   	 try {
			
   		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.UNIT_STATUS);
   		 
   		 for(DomainData dd: domainDataList )
   		 {   			 
   			 boolean isAdd = true;
   			 
//   			 if(getIsContextAuctionUnits() && ( !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS))  )
//   				 isAdd = false;
//   			 
//   			 if(getIsContextNewLeaseContract() && ( !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS))  )
//   				 isAdd = false;    			 
   			 
   			 if(isAdd)
   			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedUnitStatuses.add(item);
   			 }    			 
   			 
   		 }
   		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
   	 
		if(allowedUnitStatuses!= null && allowedUnitStatuses.size()>1)
			 Collections.sort(allowedUnitStatuses,ListComparator.LIST_COMPARE);
		
   	 viewRootMap.put("ALLOWED_UNIT_STATUSES",allowedUnitStatuses);
    }
	private HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> criteria = new HashMap<String,Object>();
		
		String emptyValue = "-1";
		
		Object followupNumber = htmlInputFollowupNumber.getValue();
		Object followupAction  = htmlSelectOneFollowupAction.getValue();		
		Object followupDateFrom = htmlCalendarFollowupDateFrom.getValue();
		Object followupDateTo  = htmlCalendarFollowupDateTo.getValue();		
		Object paymentType = htmlSelectOnePaymentType.getValue();
		Object paymentAmount  = htmlInputPaymentAmount.getValue();		
		Object paymentDateFrom = htmlCalendarPaymentDateFrom.getValue();
		Object paymentDateTo  = htmlCalendarPaymentDateTo.getValue();	
		Object chequeNumber = htmlInputChequeNumber.getValue();
		Object bankId  = htmlSelectOneBankName.getValue();	
		Object propertyName = htmlInputPropertyName.getValue();
		Object unitNumber  = htmlInputUnitNumber.getValue();	
		Object contract = htmlInputContract.getValue();
		Object tenantName  = htmlInputTenantName.getValue();	
		Object statusId = htmlSelectOneFollowupStatus.getValue();
		
		if(statusId !=null && !statusId.toString().equals("") && !statusId.toString().equals("-1") )
		    criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.FOLLOWUP_STATUS_ID, statusId.toString());
		
		if(followupNumber !=null && !followupNumber.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.FOLLOWUP_NUMBER, followupNumber.toString());
		
		if(followupAction !=null && !followupAction.toString().equals("") && !followupAction.toString().trim().equals(emptyValue))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.FOLLOWUP_ACTION_ID, Long.parseLong(followupAction.toString()));
		
		if(followupDateFrom !=null && !followupDateFrom.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.FOLLOWUP_DATE_FROM, followupDateFrom);
		
		if(followupDateTo !=null && !followupDateTo.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.FOLLOWUP_DATE_TO, followupDateTo);
		
		if(paymentType !=null && !paymentType.toString().equals("") && !paymentType.toString().trim().equals(emptyValue))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.PAYMENT_TYPE_ID, Long.parseLong(paymentType.toString()));
		
		if(paymentAmount !=null && !paymentAmount.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.PAYMENT_AMOUNT, Double.parseDouble(paymentAmount.toString()));
		
		if(paymentDateFrom !=null && !paymentDateFrom.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.PAYMENT_DATE_FROM,paymentDateFrom);
		
		if(paymentDateTo !=null && !paymentDateTo.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.PAYMENT_DATE_TO,paymentDateTo);
		
		if(chequeNumber !=null && !chequeNumber.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.CHEQUE_NUMBER,chequeNumber.toString());
		
		if(bankId !=null && !bankId.toString().equals("") && !bankId.toString().trim().equals(emptyValue))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.BANK_ID, Long.parseLong(bankId.toString()));
		
		if(propertyName !=null && !propertyName.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.PROPERTY_NAME,propertyName.toString());
		
		if(unitNumber !=null && !unitNumber.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.UNIT_NUMBER, unitNumber.toString());
		
		if(contract !=null && !contract.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.CONTRACT_NUMBER,contract.toString());
		
		if(tenantName !=null && !tenantName.toString().equals(""))
			criteria.put(WebConstants.FOLLOWUP_SEARCH_CRITERIA.TENANT_NAME, tenantName.toString());
				
		
		return criteria;
	}
	@SuppressWarnings("unchecked")
	private void loadFollowupList() {
		logger.logInfo("loadFollowupList() started...");
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		auctionStatusMap = (Map<String,Long>) viewMap.get(WebConstants.AUCTION_STATUS_MAP);
		
		FollowUpService followUpService = new FollowUpService();

    	try
    	{		
			dataList = new ArrayList<FollowUpView>();
			HashMap<String,Object> searchCriteria =  getSearchCriteria();
			int totalRows =  followUpService.searchFollowUpTotalNumberOfRecords( searchCriteria );
			setTotalRows(totalRows);
			doPagingComputations();
						
			dataList = followUpService.getFollowUpsByCriteria( searchCriteria, getRowsPerPage(), getCurrentPage(), getSortField(), 
					                                           isSortItemListAscending() );
			viewMap.put( "followupsList", dataList );
			viewMap.put( "recordSize", totalRows );
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			if(dataList.isEmpty())
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			
			logger.logInfo("loadFollowupList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadFollowupList() crashed ",exception);
		}
		catch(Exception exception){
			logger.LogException("loadFollowupList() crashed ",exception);
		}
	}	
	
	/*
	 *  Validation Methods
	 */
	
	public String getInvalidMessage(String field){
		
		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}
	
	public Boolean validateFields() {
		errorMessages = new ArrayList<String>();
		String emptyValue = "-1";
		
		Object followupNumber = htmlInputFollowupNumber.getValue();
		Object followupAction  = htmlSelectOneFollowupAction.getValue();		
		Object followupDateFrom = htmlCalendarFollowupDateFrom.getValue();
		Object followupDateTo  = htmlCalendarFollowupDateTo.getValue();		
		Object paymentType = htmlSelectOnePaymentType.getValue();
		Object paymentAmount  = htmlInputPaymentAmount.getValue();		
		Object paymentDateFrom = htmlCalendarPaymentDateFrom.getValue();
		Object paymentDateTo  = htmlCalendarPaymentDateTo.getValue();	
		Object chequeNumber = htmlInputChequeNumber.getValue();
		Object bankId  = htmlSelectOneBankName.getValue();	
		Object propertyName = htmlInputPropertyName.getValue();
		Object unitNumber  = htmlInputUnitNumber.getValue();	
		Object contract = htmlInputContract.getValue();
		Object tenantName  = htmlInputTenantName.getValue();	
		Object status = htmlSelectOneFollowupStatus.getValue();
		if(status!=null && status.toString().length()>0 && !status.toString().equals("-1"))
			return true;
		if(followupNumber !=null && !followupNumber.toString().equals(""))
			return true;
		if(followupAction !=null && !followupAction.toString().equals("") && !followupAction.toString().trim().equals(emptyValue))
			return true;
		if(followupDateFrom !=null && !followupDateFrom.toString().equals(""))
			return true;
		if(followupDateTo !=null && !followupDateTo.toString().equals(""))
			return true;
		if(paymentType !=null && !paymentType.toString().equals("") && !paymentType.toString().trim().equals(emptyValue))
			return true;
		if(paymentAmount !=null && !paymentAmount.toString().equals(""))
			return true;
		if(paymentDateFrom !=null && !paymentDateFrom.toString().equals(""))
			return true;
		if(paymentDateTo !=null && !paymentDateTo.toString().equals(""))
			return true;
		if(chequeNumber !=null && !chequeNumber.toString().equals(""))
			return true;
		if(bankId !=null && !bankId.toString().equals("") && !bankId.toString().trim().equals(emptyValue))
			return true;
		if(propertyName !=null && !propertyName.toString().equals(""))
			return true;
		if(unitNumber !=null && !unitNumber.toString().equals(""))
			return true;
		if(contract !=null && !contract.toString().equals(""))
			return true;
		if(tenantName !=null && !tenantName.toString().equals(""))
			return true;
		return false;
	}
	
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();	
		
		try
		{
			if(!isPostBack())
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
		        setSortItemListAscending(true);
				clearSessionMap();
				loadStatusMap();
				
				HttpServletRequest request =
					 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
				
				boolean isPopup = false;
				
				if(request.getParameter("viewMode")!=null)
					 {
					   viewRootMap.put(VIEW_MODE, request.getParameter("viewMode").toString());
					   isPopup = true;
					 }
				 else if(getRequestParam("viewMode")!=null)
				 {
					 viewRootMap.put(VIEW_MODE, getRequestParam("viewMode").toString());
					 isPopup = true;
				 }
				
			}
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}

//		searchAuctions();
		System.out.println("FollowUpSearchBacking init");
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		System.out.println("FollowUpSearchBacking preprocess");
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		System.out.println("FollowUpSearchBacking prerender");
	}
	
	@SuppressWarnings( "unchecked" )
	private void loadStatusMap(){
		List<DomainTypeView> list = (ArrayList<DomainTypeView>) context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		while( itr.hasNext() ) {
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(WebConstants.Statuses.AUCTION_STATUS)==0 ) {
				Set<DomainDataView> dd = dtv.getDomainDatas();
				for (DomainDataView ddv : dd) {
					auctionStatusMap.put(ddv.getDataValue(),ddv.getDomainDataId());
				}
			}
		}
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.AUCTION_STATUS_MAP,auctionStatusMap);
	}
	
	/*
	 * Button Click Action Handlers 
	 */
	 
	public String auctionDetailsEditMode()
    {
    	logger.logInfo("auctionDetailsEditMode() started...");
    	try{
    		 clearSessionMap();
    		 AuctionView aucView=(AuctionView)dataTable.getRowData();
    		 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    		 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    		 sessionMap.put(WebConstants.VIEW_MODE,WebConstants.EDIT_MODE);
    		 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
    		 setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_AUCTION_SEARCH);
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
    		logger.logInfo("auctionDetailsEditMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsEditMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsEditMode() crashed ",exception);
    	}
        return "auctionDetailsEditMode";
    }
	
	
	public String auctionDetailsViewMode()
    {
    	logger.logInfo("auctionDetailsViewMode() started...");
    	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		auctionStatusMap = (Map<String,Long>) viewMap.get(WebConstants.AUCTION_STATUS_MAP);
		String viewScreen = "auctionDetailsViewMode";
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Long statusId = Long.parseLong(aucView.getAuctionStatusId());
    	 	sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
    	 	sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
    	 	if((statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0) 
    	 		|| (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_REFUND_APPROVED_STATUS))==0))
    	 	{
    	 		viewScreen = "auctionCompletedViewMode";
    	 	}
    	 	
   		logger.logInfo("auctionDetailsViewMode() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
    	}
        return viewScreen;
    }
	
	public String auctionDetailsFullViewMode()
    {
    	logger.logInfo("auctionDetailsViewMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.FULL_VIEW_MODE,WebConstants.FULL_VIEW_MODE);
    	 	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionDetailsViewMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
    	}
        return "auctionDetailsViewMode";
    }
	
	public String auctionDetailsConductMode()
    {
    	logger.logInfo("auctionDetailsConductMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.CONDUCT_AUCTION_MODE,WebConstants.CONDUCT_AUCTION_MODE);
    	  	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionDetailsConductMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsConductMode() crashed ",exception);
    	}
        return "auctionDetailsConductMode";
    }
	
	public String auctionDetailsRefundMode()
    {
    	logger.logInfo("auctionDetailsRefundMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	sessionMap.put("isRefundAuctionFinalMode", true);
    	 	
   		logger.logInfo("auctionDetailsRefundMode() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsRefundMode() crashed ",exception);
    	}
        return "auctionDetailsRefundMode";
    }
	
	public String auctionSelectDateVenueMode()
    {
    	logger.logInfo("auctionSelectDateVenueMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.SELECT_VENUE_MODE,WebConstants.SELECT_VENUE_MODE);
    	 	 sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
    	 	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionSelectDateVenueMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
    	}
        return "auctionSelectDateVenueMode";
    }
	
    public String searchFollowups()
    {
    	logger.logInfo("searchFollowups() started...");
    	try{
    		if(validateFields())
    		{
    			loadFollowupList();
    		}
    		else
    			errorMessages.add(getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
    			
    		logger.logInfo("searchFollowups() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("searchFollowups() crashed ",exception);
    	}
        return "searchFollowups";
    }
    public void doSearchItemList()
	{
    	logger.logInfo("doSearchItemList started...");
		try 
		{
			if(validateFields())
    			loadFollowupList();
			logger.logInfo("doSearchItemList Finished...");
		} 
		catch (Exception e) 
		{
			logger.LogException("doSearchItemList| crashed ", e);
		}
	}
	
    public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        clearSessionMap();
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		        logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }    
    	
	/*
	 * Setters / Getters
	 */
    
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
//		AuctionVenueView temp = new AuctionVenueView();
//		temp.setVenueName(auctionView.getVenueName());
//		auctionView.setAuctionVenue(temp);
		return auctionView;
	}

	/**
	 * @param auctionView the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
//		AuctionVenueView temp = new AuctionVenueView();
//		temp.setVenueName(auctionView.getVenueName());
//		auctionView.setAuctionVenue(temp);
		this.auctionView = auctionView;
	}

	/**
	 * @return the dataItem
	 */
	public AuctionView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(AuctionView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<FollowUpView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<FollowUpView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	

	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the auctionStatuses
	 */
	public List<SelectItem> getAuctionStatuses() {
		return auctionStatuses;
	}

	/**
	 * @param auctionStatuses the auctionStatuses to set
	 */
	public void setAuctionStatuses(List<SelectItem> auctionStatuses) {
		this.auctionStatuses = auctionStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(ServletContext context) {
		this.context = context;
	}

	/**
	 * @return the auctionStatusMap
	 */
	public Map<String, Long> getAuctionStatusMap() {
		return auctionStatusMap;
	}

	/**
	 * @param auctionStatusMap the auctionStatusMap to set
	 */
	public void setAuctionStatusMap(Map<String, Long> auctionStatusMap) {
		this.auctionStatusMap = auctionStatusMap;
	}

	/**
	 * @return the hhStart
	 */
	public String getHhStart() {
		return hhStart;
	}

	/**
	 * @param hhStart the hhStart to set
	 */
	public void setHhStart(String hhStart) {
		this.hhStart = hhStart;
	}

	/**
	 * @return the mmStart
	 */
	public String getMmStart() {
		return mmStart;
	}

	/**
	 * @param mmStart the mmStart to set
	 */
	public void setMmStart(String mmStart) {
		this.mmStart = mmStart;
	}

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}


	/**
	 * @return the hhEnd
	 */
	public String getHhEnd() {
		return hhEnd;
	}

	/**
	 * @param hhEnd the hhEnd to set
	 */
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}

	/**
	 * @return the mmEnd
	 */
	public String getMmEnd() {
		return mmEnd;
	}

	/**
	 * @param mmEnd the mmEnd to set
	 */
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}
	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	
	public String followUpDetailsMode(){
		logger.logInfo("followUpDetailsMode() started...");
		try {
			FollowUpView followUpView = (FollowUpView) dataTable.getRowData();
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.FollowUp.FOLLOW_UP_VIEW, followUpView);
			logger.logInfo("followUpDetailsMode() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("followUpDetailsMode() crashed ", exception);
		}
		return "returnedCheque";
	}
	
	public HtmlInputText getHtmlInputFollowupNumber() {
		return htmlInputFollowupNumber;
	}

	public void setHtmlInputFollowupNumber(HtmlInputText htmlInputFollowupNumber) {
		this.htmlInputFollowupNumber = htmlInputFollowupNumber;
	}

	public HtmlSelectOneMenu getHtmlSelectOneFollowupAction() {
		return htmlSelectOneFollowupAction;
	}

	public void setHtmlSelectOneFollowupAction(
			HtmlSelectOneMenu htmlSelectOneFollowupAction) {
		this.htmlSelectOneFollowupAction = htmlSelectOneFollowupAction;
	}

	public HtmlCalendar getHtmlCalendarFollowupDateFrom() {
		return htmlCalendarFollowupDateFrom;
	}

	public void setHtmlCalendarFollowupDateFrom(
			HtmlCalendar htmlCalendarFollowupDateFrom) {
		this.htmlCalendarFollowupDateFrom = htmlCalendarFollowupDateFrom;
	}

	public HtmlCalendar getHtmlCalendarFollowupDateTo() {
		return htmlCalendarFollowupDateTo;
	}

	public void setHtmlCalendarFollowupDateTo(
			HtmlCalendar htmlCalendarFollowupDateTo) {
		this.htmlCalendarFollowupDateTo = htmlCalendarFollowupDateTo;
	}

	public HtmlSelectOneMenu getHtmlSelectOnePaymentType() {
		return htmlSelectOnePaymentType;
	}

	public void setHtmlSelectOnePaymentType(
			HtmlSelectOneMenu htmlSelectOnePaymentType) {
		this.htmlSelectOnePaymentType = htmlSelectOnePaymentType;
	}

	public HtmlInputText getHtmlInputPaymentAmount() {
		return htmlInputPaymentAmount;
	}

	public void setHtmlInputPaymentAmount(HtmlInputText htmlInputPaymentAmount) {
		this.htmlInputPaymentAmount = htmlInputPaymentAmount;
	}

	public HtmlCalendar getHtmlCalendarPaymentDateFrom() {
		return htmlCalendarPaymentDateFrom;
	}

	public void setHtmlCalendarPaymentDateFrom(
			HtmlCalendar htmlCalendarPaymentDateFrom) {
		this.htmlCalendarPaymentDateFrom = htmlCalendarPaymentDateFrom;
	}

	public HtmlCalendar getHtmlCalendarPaymentDateTo() {
		return htmlCalendarPaymentDateTo;
	}

	public void setHtmlCalendarPaymentDateTo(HtmlCalendar htmlCalendarPaymentDateTo) {
		this.htmlCalendarPaymentDateTo = htmlCalendarPaymentDateTo;
	}

	public HtmlInputText getHtmlInputChequeNumber() {
		return htmlInputChequeNumber;
	}

	public void setHtmlInputChequeNumber(HtmlInputText htmlInputChequeNumber) {
		this.htmlInputChequeNumber = htmlInputChequeNumber;
	}

	public HtmlSelectOneMenu getHtmlSelectOneBankName() {
		return htmlSelectOneBankName;
	}

	public void setHtmlSelectOneBankName(HtmlSelectOneMenu htmlSelectOneBankName) {
		this.htmlSelectOneBankName = htmlSelectOneBankName;
	}

	public HtmlInputText getHtmlInputPropertyName() {
		return htmlInputPropertyName;
	}

	public void setHtmlInputPropertyName(HtmlInputText htmlInputPropertyName) {
		this.htmlInputPropertyName = htmlInputPropertyName;
	}

	public HtmlInputText getHtmlInputUnitNumber() {
		return htmlInputUnitNumber;
	}

	public void setHtmlInputUnitNumber(HtmlInputText htmlInputUnitNumber) {
		this.htmlInputUnitNumber = htmlInputUnitNumber;
	}

	public HtmlInputText getHtmlInputContract() {
		return htmlInputContract;
	}

	public void setHtmlInputContract(HtmlInputText htmlInputContract) {
		this.htmlInputContract = htmlInputContract;
	}

	public HtmlInputText getHtmlInputTenantName() {
		return htmlInputTenantName;
	}

	public void setHtmlInputTenantName(HtmlInputText htmlInputTenantName) {
		this.htmlInputTenantName = htmlInputTenantName;
	}

	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}
	
	public String getNumberFormat(){
    	return new CommonUtil().getNumberFormat();
    }

	public HtmlSelectOneMenu getHtmlSelectOneFollowupStatus() {
		return htmlSelectOneFollowupStatus;
	}

	public void setHtmlSelectOneFollowupStatus(
			HtmlSelectOneMenu htmlSelectOneFollowupStatus) {
		this.htmlSelectOneFollowupStatus = htmlSelectOneFollowupStatus;
	}
}


