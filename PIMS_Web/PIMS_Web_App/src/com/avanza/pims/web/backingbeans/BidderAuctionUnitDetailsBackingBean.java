package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlOutputLabel;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.BidderAttendanceView;
import com.avanza.pims.ws.vo.BidderUnitDetailView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.UnitView;


public class BidderAuctionUnitDetailsBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(BidderAuctionUnitDetailsBackingBean.class);
	/**
	 * 
	 */
	
	public BidderAuctionUnitDetailsBackingBean(){
		
	}
	
	private String infoMessage = "";
	private List<String> errorMessages = new ArrayList<String>();
	
	private BidderUnitDetailView dataItem = new BidderUnitDetailView();
	private List<BidderUnitDetailView> dataList = new ArrayList<BidderUnitDetailView>();
	private HtmlDataTable dataTable;
	
	private HtmlOutputLabel enterConfiscationHtmlLabel = new HtmlOutputLabel();
	private HtmlOutputLabel clickActionColumnHtmlLabel = new HtmlOutputLabel();
	private HtmlPanelGrid confiscationPanelGrid = new HtmlPanelGrid();
	private HtmlCommandButton btnSaveNewConfiscation = new HtmlCommandButton();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		// TODO Auto-generated method stub
		super.init();
		try{
		if(!isPostBack())
		{
			if(sessionMap.get("isRefundAuctionFinalMode")!=null)
			{
				Boolean isRefundAuctionFinalMode = (Boolean) sessionMap.get("isRefundAuctionFinalMode");
				viewMap.put("isRefundAuctionFinalMode", isRefundAuctionFinalMode);
				sessionMap.remove("isRefundAuctionFinalMode");
			}
			if(sessionMap.get("isRefundAuctionApprovalMode")!=null)
			{
				Boolean isRefundAuctionApprovalMode = (Boolean) sessionMap.get("isRefundAuctionApprovalMode");
				viewMap.put("isRefundAuctionApprovalMode", isRefundAuctionApprovalMode);
				sessionMap.remove("isRefundAuctionApprovalMode");
			}
			if(sessionMap.get("isConductMode")!=null)
			{
				Boolean isConductMode = (Boolean) sessionMap.get("isConductMode");
				viewMap.put("isConductMode", isConductMode);
				sessionMap.remove("isConductMode");
			}
			BidderAttendanceView bidderAttendanceView = (BidderAttendanceView) sessionMap.get(WebConstants.AuctionRefund.BIDDER_ATTENDANCE_VIEW); 
			if(bidderAttendanceView!=null)
			{
				viewMap.put(WebConstants.AuctionRefund.BIDDER_ATTENDANCE_VIEW, bidderAttendanceView);
				sessionMap.remove(WebConstants.AuctionRefund.BIDDER_ATTENDANCE_VIEW);
				List<BidderUnitDetailView> bidderUnitDetailViewList = bidderAttendanceView.getBidderUnitDetailViews(); 
				if(bidderUnitDetailViewList!=null)
				{
					List<BidderUnitDetailView> updatedUnitDetails = new ArrayList<BidderUnitDetailView>();
					for(BidderUnitDetailView BUDV : bidderUnitDetailViewList)
					{
						String replacingUnitNumber="";
						replacingUnitNumber = new PropertyService().getReplacingUnitNumberAgainstUnit(BUDV.getUnitId(),bidderAttendanceView.getBidderId(),bidderAttendanceView.getAuctionId());
						if(replacingUnitNumber !=null && replacingUnitNumber.length()>0)
							{
								BUDV.setShowSetNewConfiscAmount(false);
								BUDV.setReplacedWithUnitNumber(replacingUnitNumber);
							}
						updatedUnitDetails.add(BUDV);
					}
					viewMap.put(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST, updatedUnitDetails);
					viewMap.put("recordSize", updatedUnitDetails.size());
					setCanMoveToContract();
				}
			}
		}
		}
		catch (PimsBusinessException e) {
			logger.LogException("init() crashed |", e);
		}
		
	}

	private void setCanMoveToContract(){
		Boolean canMoveToContract = false;
		
		if(getIsConductMode()){
			int movableUnits = 0;
			int wonUnits = 0;
			int total = 0;
			
			List<BidderUnitDetailView> bidderUnitDetailViewList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
			if(bidderUnitDetailViewList!=null){
				total = bidderUnitDetailViewList.size();
				for(BidderUnitDetailView bidderUnitDetailView :bidderUnitDetailViewList){
					if(bidderUnitDetailView.getMovedToContract().equals("No") && bidderUnitDetailView.getIsExcluded().equals("No")){
						if(bidderUnitDetailView.getWonAuction().equals("Yes"))
							wonUnits++;
						else
							movableUnits++;
					}
				}
				if(wonUnits < total && movableUnits>0 && wonUnits>0)
					canMoveToContract = true;
			}
		}
		
		viewMap.put("canMoveToContract", canMoveToContract);
	}
	
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		
		applyMode();
	}
	
	private void applyMode(){
		if(getIsRefundAuctionApprovalMode()){
			if(viewMap.containsKey("showEnterConfiscationLabel")){
				enterConfiscationHtmlLabel.setRendered(true);
				clickActionColumnHtmlLabel.setRendered(false);	
				btnSaveNewConfiscation.setDisabled(false);
			}
			else{
				btnSaveNewConfiscation.setDisabled(true);
				enterConfiscationHtmlLabel.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(true);
			}
		}
		else
		{
			enterConfiscationHtmlLabel.setRendered(false);
			clickActionColumnHtmlLabel.setRendered(false);	
		}
	}
	
	
	public void markAsRefunded(javax.faces.event.ActionEvent event)
    {
    	logger.logInfo("markAsRefunded() started...");
    	Boolean success = true;
    	try{
    		BidderUnitDetailView bidderUnitDetailView=(BidderUnitDetailView)dataTable.getRowData();
    		int index = dataTable.getRowIndex();
    		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
    		success = pSAgent.markAsRefunded(bidderUnitDetailView.getRequestDetailId(), true, CommonUtil.getLoggedInUser());
 	
    		if(success){
    			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MARKED_AS_REFUNDED_SUCCESS);
    			bidderUnitDetailView.setHasRefunded("Yes");
    			List<BidderUnitDetailView> bidderUnitDetailViewList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
    			bidderUnitDetailViewList.remove(index);
    			bidderUnitDetailViewList.add(index,bidderUnitDetailView);
    			sessionMap.put("reloadBidders", true);
    		}
 	        logger.logInfo("markAsRefunded() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		success = false;
    		logger.LogException("markAsRefunded() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("markAsRefunded() crashed ",exception);
    		success = false;
    	}
    	finally{
    		if(!success){
    			errorMessages.clear();
        		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MARKED_AS_REFUNDED_FAILURE));
    		}
    	}
    }
	
	public void moveToContract(javax.faces.event.ActionEvent event)
    {
    	logger.logInfo("moveToContract() started...");
    	Boolean success = true;
    	try{
    		BidderUnitDetailView bidderUnitDetailView=(BidderUnitDetailView)dataTable.getRowData();
    		int index = dataTable.getRowIndex();
    		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
    		success = pSAgent.markAsMoveToContract(bidderUnitDetailView.getRequestDetailId(), 1L);
 	
    		if(success){
    			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MOVE_TO_CONTRACT_SUCCESS);
    			bidderUnitDetailView.setMovedToContract("Yes");
    			List<BidderUnitDetailView> bidderUnitDetailViewList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
    			bidderUnitDetailViewList.remove(index);
    			bidderUnitDetailViewList.add(index,bidderUnitDetailView);
    			sessionMap.put("reloadBidders", true);
    		}
 	        logger.logInfo("moveToContract() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		success = false;
    		logger.LogException("moveToContract() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("moveToContract() crashed ",exception);
    		success = false;
    	}
    	finally{
    		if(!success){
    			errorMessages.clear();
        		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MOVE_TO_CONTRACT_FAILURE));
    		}
    	}
    }
	@SuppressWarnings("unchecked")
	public void contractNotSigned(javax.faces.event.ActionEvent event)
    {
    	logger.logInfo("contractNotSigned() started...");
    	Boolean success = true;
    	try{
    		BidderUnitDetailView bidderUnitDetailView=(BidderUnitDetailView)dataTable.getRowData();
    		int index = dataTable.getRowIndex();
    		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
    		success = pSAgent.markAsContractNotSigned(bidderUnitDetailView.getRequestDetailId(), getLoggedInUserId());
 	
    		if(success)
    		{
    			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ConductAuction.CONTRACT_NOT_SIGNED_SUCCESS);
    			bidderUnitDetailView.setMovedToContract("No");
    			bidderUnitDetailView.setShowContractNotSignedIcon(false);
    			List<BidderUnitDetailView> bidderUnitDetailViewList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
    			bidderUnitDetailViewList.remove(index);
    			bidderUnitDetailViewList.add(index,bidderUnitDetailView);
    			sessionMap.put("reloadBidders", true);
    		}
 	        logger.logInfo("contractNotSigned() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		success = false;
    		logger.LogException("contractNotSigned() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("contractNotSigned() crashed ",exception);
    		success = false;
    	}
    	finally{
    		if(!success){
    			errorMessages.clear();
        		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.CONTRACT_NOT_SIGNED_FAILURE));
    		}
    	}
    }
	
	@SuppressWarnings("unchecked")
	public String btnSaveNewConfiscationClick()
	{
		String methodName ="btnSaveNewConfiscationClick";
		logger.logInfo(methodName + "|" + "Start");
		Boolean success = false;
		try
		{
			BidderUnitDetailView selectedBidderUnitDetailView = (BidderUnitDetailView)viewMap.get("selectedBidderUnitDetailView");
			Double depositAmount = selectedBidderUnitDetailView.getDepositAmount();
			Double newConfiscation = getNewConfiscation();
			if(newConfiscation<=depositAmount)
			{
				viewMap.remove("showEnterConfiscationLabel");
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
	    		success = pSAgent.setNewConfiscation(selectedBidderUnitDetailView.getRequestDetailId(), newConfiscation);
				
	    		if(success)
	    		{
					List<BidderUnitDetailView> bidderUnitDetailViewList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
					if(bidderUnitDetailViewList!=null && !bidderUnitDetailViewList.isEmpty()){
						bidderUnitDetailViewList.remove(selectedBidderUnitDetailView);
						Integer index = (Integer)viewMap.get("selectedIndex");
						if(index!=null)
						{
							selectedBidderUnitDetailView.setConfiscationAmount(newConfiscation);
							bidderUnitDetailViewList.add(index.intValue(),selectedBidderUnitDetailView);
							viewMap.put(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST,bidderUnitDetailViewList);
						}
					}
				
					viewMap.put("confiscationSaved", "confiscationSaved");
					infoMessage = CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_CONFISCATION_SAVE_SUCCESS);
					setNewConfiscation(0.0D);
					sessionMap.put("reloadBidders", true);
				}
				else
				{
					viewMap.remove("confiscationSaved");
					infoMessage="";
					errorMessages.clear(); 
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_CONFISCATION_SAVE_FAILURE));
				}
	    	}
			else
			{
				infoMessage="";
				errorMessages.clear(); 
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_CONFISCATION_VALIDATION));
			}
			logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " " + "crashed...",ex);

		}
		return "saveNewRentVal";
	}
	
	public String setSelectedColumn()
	{
		String methodName ="setSelectedColumn";
		logger.logInfo(methodName + "|" + "Start");
		try
		{
			BidderUnitDetailView selectedBidderUnitDetailView = (BidderUnitDetailView)dataTable.getRowData();
			viewMap.put("selectedIndex", dataTable.getRowIndex());
			setNewConfiscation(selectedBidderUnitDetailView.getConfiscationAmount());
			viewMap.put("selectedBidderUnitDetailView", selectedBidderUnitDetailView);
			viewMap.put("showEnterConfiscationLabel","showEnterConfiscationLabel");
			logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Error Occured::"+ex);

		}
		return "saveNewConfiscationAmount";
	}

	
	
	
	
	
	public String getInfoMessage() {
		return infoMessage;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	public String getErrorMessages() {
		if(errorMessages==null)
			errorMessages = new ArrayList<String>();
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public BidderUnitDetailView getDataItem() {
		return dataItem;
	}
	public void setDataItem(BidderUnitDetailView dataItem) {
		this.dataItem = dataItem;
	}
	public List<BidderUnitDetailView> getDataList() {
		dataList = (List<BidderUnitDetailView>) viewMap.get(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST);
		if(dataList==null)
			dataList = new ArrayList<BidderUnitDetailView>(0);
		return dataList;
	}
	public void setDataList(List<BidderUnitDetailView> dataList) {
		this.dataList = dataList;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	public Boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	public String getDateFormat()
	{
		SystemParameters parameters = SystemParameters.getInstance();
		return parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
	}
	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}
	
	public Boolean getCanMoveToContract(){
		if(viewMap.get("canMoveToContract")!=null){
			return (Boolean)viewMap.get("canMoveToContract");
		}
		return false;
	}

	public HtmlOutputLabel getClickActionColumnHtmlLabel() {
		return clickActionColumnHtmlLabel;
	}

	public void setClickActionColumnHtmlLabel(
			HtmlOutputLabel clickActionColumnHtmlLabel) {
		this.clickActionColumnHtmlLabel = clickActionColumnHtmlLabel;
	}

	public HtmlOutputLabel getEnterConfiscationHtmlLabel() {
		return enterConfiscationHtmlLabel;
	}

	public void setEnterConfiscationHtmlLabel(
			HtmlOutputLabel enterConfiscationHtmlLabel) {
		this.enterConfiscationHtmlLabel = enterConfiscationHtmlLabel;
	}
	public Double getNewConfiscation() {
		Double newConfiscation = (Double)viewMap.get("newConfiscation");
		return newConfiscation;
	}
	public void setNewConfiscation(Double newConfiscation) {
		if(newConfiscation!=null)
			viewMap.put("newConfiscation" , newConfiscation);
	}

	public HtmlPanelGrid getConfiscationPanelGrid() {
		return confiscationPanelGrid;
	}

	public void setConfiscationPanelGrid(HtmlPanelGrid confiscationPanelGrid) {
		this.confiscationPanelGrid = confiscationPanelGrid;
	}

	public HtmlCommandButton getBtnSaveNewConfiscation() {
		return btnSaveNewConfiscation;
	}

	public void setBtnSaveNewConfiscation(HtmlCommandButton btnSaveNewConfiscation) {
		this.btnSaveNewConfiscation = btnSaveNewConfiscation;
	}
	
	public Boolean getIsRefundAuctionFinalMode(){
		if(viewMap.get("isRefundAuctionFinalMode")!=null){
			return (Boolean)viewMap.get("isRefundAuctionFinalMode");
		}
		return false;
	}	
	public Boolean getIsRefundAuctionApprovalMode(){
		if(viewMap.get("isRefundAuctionApprovalMode")!=null){
			return (Boolean)viewMap.get("isRefundAuctionApprovalMode");
		}
		return false;
	}
	public Boolean getIsConductMode(){
		if(viewMap.get("isConductMode")!=null){
			return (Boolean)viewMap.get("isConductMode");
		}
		return false;
	}
	
}