package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.FinanceServiceAgent;
import com.avanza.pims.business.services.GRPServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.FinancialConfigurationView;
import com.avanza.pims.ws.vo.FollowUpActionsView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.XxAmafBankRemitAccountView;
import com.avanza.ui.util.ResourceUtil;


public class FinancialConfiguration extends AbstractController{
	
	
	
	FinanceServiceAgent financeServiceAgent = new FinanceServiceAgent();
	FinancialConfigurationView financialConfigurationView = new FinancialConfigurationView();
	SystemParameters parameters = SystemParameters.getInstance();
	private transient Logger logger = Logger.getLogger(FinancialConfiguration.class);
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil = new CommonUtil();

	private HtmlInputText bankRemitanceAccountName = new HtmlInputText();
	private HtmlInputText GRPAccountNumber = new HtmlInputText();
	private HtmlInputText actionNo= new HtmlInputText();
	private HtmlInputText description = new HtmlInputText();
	
	//Search Menu
	private HtmlSelectOneMenu paymentTypeMenu = new HtmlSelectOneMenu();
    private HtmlSelectOneMenu ownerShipMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu trxTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu trxTypeMenuDebit = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu paymentMethod = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu activityName = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu memoLine = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu receiptType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu bankRemitAccount = new HtmlSelectOneMenu();
	
	
	
	
	//Command Buttons
	private HtmlCommandButton searchButton = new HtmlCommandButton();
	private HtmlCommandButton clearButton = new HtmlCommandButton();

	List<SelectItem> paymentTypeList = new ArrayList<SelectItem>();	
	
	
	//Life Cycle Implementation
	@SuppressWarnings("unchecked")
	public void init()
	{
		super.init();
		
		try {
			
			if(!isPostBack())
			{
			  populatePaymentTypeList();
			  if(sessionMap.containsKey(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW) && 
					  sessionMap.get(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW)!=null )
			  {
			    viewMap.put(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW ,sessionMap.get(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW));
			    sessionMap.remove(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW );
			    beforeApplyRequestValues();
			  }
			  
			}
		}
		catch (PimsBusinessException e) {

			logger.LogException("Financial Configuration INIT CRASHED", e);
			
		} 
	}
	
	@Override 
	public void     beforeApplyRequestValues() 
	{
	 super.beforeApplyRequestValues();
	 if(!isPostBack() && viewMap.containsKey(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW ))
		{
			financialConfigurationView= (FinancialConfigurationView)viewMap.get(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW );
			if(financialConfigurationView.getBankRemitanceAccountName()!=null && financialConfigurationView.getBankRemitanceAccountName().length()>0)
			bankRemitanceAccountName.setValue(financialConfigurationView.getBankRemitanceAccountName());
			if(financialConfigurationView.getGrpAccountNumber()!=null && financialConfigurationView.getGrpAccountNumber().length()>0)
			GRPAccountNumber.setValue( financialConfigurationView.getGrpAccountNumber());
			paymentTypeMenu.setValue(financialConfigurationView.getPaymentTypeId().toString());
			ownerShipMenu.setValue(financialConfigurationView.getOwnerShipTypeId().toString());
			description.setValue(financialConfigurationView.getDescription());
			if(financialConfigurationView.getActionNo()!=null )
			    actionNo.setValue(financialConfigurationView.getActionNo().toString());
			if(financialConfigurationView.getTrxTypeName()!=null && financialConfigurationView.getTrxTypeName().length()>0)
    			trxTypeMenu.setValue(financialConfigurationView.getTrxTypeName());
			else
				trxTypeMenu.setValue("-1");
			if(financialConfigurationView.getTrxTypeNameDebit()!=null && financialConfigurationView.getTrxTypeNameDebit().length()>0)
    			trxTypeMenuDebit.setValue(financialConfigurationView.getTrxTypeNameDebit());
			else
				trxTypeMenuDebit.setValue("-1");
			if(financialConfigurationView.getPaymentMethodId()!=null )
				paymentMethod.setValue(financialConfigurationView.getPaymentMethodId().toString());
			if(financialConfigurationView.getXxAmafReceiptTypeId()  !=null  && financialConfigurationView.getXxAmafReceiptTypeId().length()>0  )
				receiptType.setValue(financialConfigurationView.getXxAmafReceiptTypeId().toString());
			if(financialConfigurationView.getMemoLineName() !=null )
				memoLine.setValue(financialConfigurationView.getMemoLineName().toString());
			if(financialConfigurationView.getActivityName() !=null && financialConfigurationView.getActivityName().length()>0 )
				activityName.setValue(financialConfigurationView.getActivityName() );
			bankRemitAccount.setValue(financialConfigurationView.getGrpPaymentMethod()+"_"+financialConfigurationView.getGrpAccountNumber());
			
		}
	}
	@Override
	@SuppressWarnings("unchecked")
	public void prerender()
	{
	 
	}
	@SuppressWarnings("unchecked")
	private void populatePaymentTypeList()throws PimsBusinessException 
	{
		UtilityServiceAgent usa = new UtilityServiceAgent();
		List<PaymentTypeView> listPaymentType = usa.getAllPaymentTypes();
		for(PaymentTypeView paymentTypeView : listPaymentType)
		{
			if(isEnglishLocale())
				paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn()));
			else
				paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr()));
		}
		Collections.sort(paymentTypeList, ListComparator.LIST_COMPARE);
		viewMap.put("PAYMENT_TYPE_LIST",paymentTypeList);
		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	
	public HtmlInputText getGRPAccountNumber() {
		return GRPAccountNumber;
	}
	public void setGRPAccountNumber(HtmlInputText accountNumber) {
		GRPAccountNumber = accountNumber;
	}
	public HtmlSelectOneMenu getPaymentTypeMenu() {
		return paymentTypeMenu;
	}
	public void setPaymentTypeMenu(HtmlSelectOneMenu paymentTypeMenu) {
		this.paymentTypeMenu = paymentTypeMenu;
	}
	public HtmlSelectOneMenu getOwnerShipMenu() {
		return ownerShipMenu;
	}
	public void setOwnerShipMenu(HtmlSelectOneMenu ownerShipMenu) {
		this.ownerShipMenu = ownerShipMenu;
	}
	public HtmlCommandButton getSearchButton() {
		return searchButton;
	}
	public void setSearchButton(HtmlCommandButton searchButton) {
		this.searchButton = searchButton;
	}
	public HtmlCommandButton getClearButton() {
		return clearButton;
	}
	public void setClearButton(HtmlCommandButton clearButton) {
		this.clearButton = clearButton;
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentTypeList() 
	{
		if(viewMap.containsKey("PAYMENT_TYPE_LIST")&&
				viewMap.get("PAYMENT_TYPE_LIST")!=null){
			paymentTypeList= ((List<SelectItem>)viewMap.get("PAYMENT_TYPE_LIST"));
	}
		
		return paymentTypeList;
	}
	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public HtmlInputText getBankRemitanceAccountName() {
		return bankRemitanceAccountName;
	}
	public void setBankRemitanceAccountName(HtmlInputText bankRemitanceAccountName) {
		this.bankRemitanceAccountName = bankRemitanceAccountName;
	}
	public String addConfiguration()
	{
		String methodName = "addConfiguration|";
		logger.logInfo(methodName+"Start");
		errorMessages = new ArrayList<String>(0);
		try 
		{
			if(isValidated())
			{
				setValues();		
				Boolean isAdded = financeServiceAgent.addFinancialAccConfiguration(financialConfigurationView);
				if(isAdded)
				{
				  successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_CONFIGURATION_SAVED_SUCCESSFULLY));
				  logger.logInfo(methodName+"Finish");
//				  return "Search";//This will navigate the user to search screen
				}
				else
				  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_CONFIGURATION_ALRTEADY_EXIST));
			}
			logger.logInfo(methodName+"Finish");
			
		} 
		catch (PimsBusinessException e) 
		{
			  logger.LogException(methodName+"Error ", e);
			  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		catch (Exception e) 
		{
			  logger.LogException(methodName+"Error ", e);
			  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	@SuppressWarnings("unchecked")
	private void setValues()
	{
		if(viewMap.containsKey(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW))
			financialConfigurationView =	(FinancialConfigurationView)viewMap.get(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW);
		else
		{
			financialConfigurationView.setCreatedBy(getLoggedInUser());
			financialConfigurationView.setCreatedOn(new Date());
			financialConfigurationView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			financialConfigurationView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		}
		financialConfigurationView.setBankRemitanceAccountName(bankRemitanceAccountName.getValue().toString());
		financialConfigurationView.setGrpAccountNumber(GRPAccountNumber.getValue().toString());
		financialConfigurationView.setPaymentTypeId(Long.valueOf(paymentTypeMenu.getValue().toString()));
		financialConfigurationView.setOwnerShipTypeId(Long.valueOf(ownerShipMenu.getValue().toString()));
		//financialConfigurationView.setActionNo(Long.valueOf(actionNo.getValue().toString()));
		//financialConfigurationView.setDescription(description.getValue().toString());
		financialConfigurationView.setTrxTypeName(trxTypeMenu.getValue().toString().trim());
		financialConfigurationView.setTrxTypeNameDebit(trxTypeMenuDebit.getValue().toString().trim());
		
		financialConfigurationView.setUpdatedBy(getLoggedInUser());
		financialConfigurationView.setUpdatedOn(new Date());
		financialConfigurationView.setPaymentMethodId(new Long(paymentMethod.getValue().toString()));
		financialConfigurationView.setMemoLineName(memoLine.getValue().toString());
		financialConfigurationView.setXxAmafReceiptTypeId(receiptType.getValue().toString());
		financialConfigurationView.setActivityName(activityName.getValue().toString());
	}
	private boolean isValidated()
	{
	
		boolean isValidated = true;
//		try
//		{
//			Long.valueOf(actionNo.getValue().toString());
//			
//		}
//		catch(Exception e)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_ERROR_ACTION_NO_IS_INCORRECT));
//			isValidated= false;
//		}
//		if(ownerShipMenu.getValue()==null||ownerShipMenu.getValue().toString().trim().equals("-1"))
//		{
//			errorMessages.add(MessageConstants.FinancialConfiguration.MSG_ERROR_OWNERSHIP_TYPE_IS_MANDATORY);
//			isValidated= false;
//		}
		if(paymentMethod.getValue()== null || paymentMethod.getValue().toString().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_ERROR_PAYMENT_METHOD_REQ ));
			isValidated= false;
		}
		if(bankRemitAccount.getValue()== null || bankRemitAccount.getValue().toString().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_ERROR_BANK_ACC_NAME_REQ));
			isValidated= false;
		}	
		if(trxTypeMenu.getValue()==null || trxTypeMenu.getValue().toString().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.FinancialConfiguration.MSG_ERROR_TRX_TYPE_REQ ));
			isValidated= false;
		}
		
		return isValidated;
	
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentMethodList()
	{
		
		Map <Long,PaymentMethodView> map = new HashMap<Long, PaymentMethodView>();
		List<SelectItem> itemList = new ArrayList<SelectItem>(0);
		if(!viewMap.containsKey("paymentMethodList"))
		{
			// List<PaymentMethodView> voList  = new ArrayList<PaymentMethodView>(0);
		    // if(viewMap.containsKey(key))
			List<PaymentMethodView> voList = new UtilityServiceAgent().getPaymentMethods();
			for (PaymentMethodView vo: voList) 
			{
				SelectItem item = new SelectItem(vo.getPaymentMethodId().toString(),vo.getDescription());
				itemList.add(item);
				map.put(vo.getPaymentMethodId(), vo);
			}
			viewMap.put("paymentMethodList", itemList);
			viewMap.put("paymentMethodMap", map);
		}
		else
			itemList = (ArrayList<SelectItem>)viewMap.get("paymentMethodList");
		return itemList;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getComboBankRemitAccViewFromPaymentMethod()
	{
		String methodName="getComboBankRemitAccViewFromPaymentMethod|";
		logger.logInfo(methodName+"Start");
		List<SelectItem> itemList = new ArrayList<SelectItem>(0);
		Map <Long,PaymentMethodView> map = (HashMap<Long,PaymentMethodView>)viewMap.get("paymentMethodMap");
		if(paymentMethod.getValue() != null && !paymentMethod.getValue().toString().equals("-1"))
		{
			PaymentMethodView pmView = map.get(new Long(paymentMethod.getValue().toString()));
			if(pmView!=null && pmView.getGrpPaymentMethod()!=null)
			{
				for (XxAmafBankRemitAccountView pojo : getBankRemitAccView()) 
				{
					if(pmView.getGrpPaymentMethod().compareTo(pojo.getPaymentMethod())==0)
					{
					  SelectItem item = new SelectItem(pojo.getPaymentMethod()+"_"+pojo.getBankAccountNum(),pojo.getBankName()+" "+pojo.getBankAccountNum());
					  itemList.add(item);
					}
				}
			}
		}
		populateBankRemitAccViewDetails();
		logger.logInfo(methodName+"Finish");
		return itemList;
		
	}
	@SuppressWarnings("unchecked")
	public void comboBankRemitAcc_Changed()
	{
		String methodName="comboBankRemitAcc_Changed|";
		logger.logInfo(methodName+"Start");
		//bankRemitAccount.setValue(null);
		//bankRemitanceAccountName.setValue("");
		//GRPAccountNumber.setValue("");
		//getComboBankRemitAccViewFromPaymentMethod();
		populateBankRemitAccViewDetails();
		logger.logInfo(methodName+"Finish");
		
	}
	@SuppressWarnings("unchecked")
	public void populateBankRemitAccViewDetails()
	{
		String methodName="populateBankRemitAccViewDetails|";
		logger.logInfo(methodName+"Start");
		for (XxAmafBankRemitAccountView pojo : getBankRemitAccView()) 
		{
			if(bankRemitAccount != null && bankRemitAccount.getValue()!=null && !bankRemitAccount.getValue().toString().equals("-1") && 
				bankRemitAccount.getValue().toString().compareTo(pojo.getPaymentMethod()+"_"+pojo.getBankAccountNum())==0)
			{
				bankRemitanceAccountName.setValue(pojo.getBankAccountName());
				GRPAccountNumber.setValue(pojo.getBankAccountNum() );
				break;
				
			}
			else
			{
				bankRemitanceAccountName.setValue("");
				GRPAccountNumber.setValue("");
				
			}
		}
		
		logger.logInfo(methodName+"Finish");
		
	}
	@SuppressWarnings("unchecked")
	private List<XxAmafBankRemitAccountView> getBankRemitAccView()
	{
		String methodName="getBankRemitAccView|";
		logger.logInfo(methodName+"Start");
		List<XxAmafBankRemitAccountView> list = new ArrayList<XxAmafBankRemitAccountView>(0);
		if(!viewMap.containsKey("BankRemitAccView"))
		{
		    list= (ArrayList<XxAmafBankRemitAccountView>)new GRPServiceAgent().getXXAmafBankRemitAccountView(); 
		    viewMap.put("BankRemitAccView",list);
		}
		else
			list = (ArrayList<XxAmafBankRemitAccountView>)viewMap.get("BankRemitAccView");
		logger.logInfo(methodName+"Finish");
		return list;
	}
	

	public HtmlInputText getActionNo() {
		return actionNo;
	}

	public void setActionNo(HtmlInputText actionNo) {
		this.actionNo = actionNo;
	}

	public HtmlInputText getDescription() {
		return description;
	}

	public void setDescription(HtmlInputText description) {
		this.description = description;
	}
	
	
	private String getLoggedInUser() 
	{
		String msg = "getLoggedInUser";
		logger.logInfo(msg+"started");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}
		logger.logInfo(msg+" completed");
		return loggedInUser;
	}

	public HtmlSelectOneMenu getTrxTypeMenu() {
		return trxTypeMenu;
	}

	public void setTrxTypeMenu(HtmlSelectOneMenu trxTypeMenu) {
		this.trxTypeMenu = trxTypeMenu;
	}
	public HtmlSelectOneMenu getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(HtmlSelectOneMenu paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public HtmlSelectOneMenu getActivityName() {
		return activityName;
	}
	public void setActivityName(HtmlSelectOneMenu activityName) {
		this.activityName = activityName;
	}
	public HtmlSelectOneMenu getMemoLine() {
		return memoLine;
	}
	public void setMemoLine(HtmlSelectOneMenu memoLine) {
		this.memoLine = memoLine;
	}
	public HtmlSelectOneMenu getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(HtmlSelectOneMenu receiptType) {
		this.receiptType = receiptType;
	}
	public HtmlSelectOneMenu getBankRemitAccount() {
		return bankRemitAccount;
	}
	public void setBankRemitAccount(HtmlSelectOneMenu bankRemitAccount) {
		this.bankRemitAccount = bankRemitAccount;
	}

	public HtmlSelectOneMenu getTrxTypeMenuDebit() {
		return trxTypeMenuDebit;
	}

	public void setTrxTypeMenuDebit(HtmlSelectOneMenu trxTypeMenuDebit) {
		this.trxTypeMenuDebit = trxTypeMenuDebit;
	}
	public String backToSearch()
	{
		return "Search";
	}

}
