package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import oracle.bpel.services.workflow.repos.CalculatedColumn;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("unchecked")
public class BeneficiaryPopupController extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;
	private static final String VIEW_MODE 			= "viewMode";
	private static final String VIEW_MODE_POPUP 	= "popup";
	private static final String DISBURSEMENT_VIEW 	= "disbursement";
	private static final String BENEFICIARY_LIST	= "beneficiarylist";
	private static final String TO_DELETE_LIST		= "toDeleteList";
	private static final String CMB_BENEFICIARY		= "beneficiary";
	private static final String CMB_DEFAULT			= "-1";
	private static final String PERSON_BALANCE_MAP			= "personBalance";
	private static final String PERSON_BLOCKING_MAP			= "personBlocking";
	private static final String BENEF_NO_COST_CENTER_MAP			= "BENEF_NO_COST_CENTER_MAP";
	
	private HtmlDataTable beneficiariesTable = new HtmlDataTable();
	private HtmlSelectOneMenu cmbBeneficiaryList = new HtmlSelectOneMenu();
	private HtmlInputText accountBalance = new HtmlInputText();
	private HtmlInputText blockingBalance = new HtmlInputText();
	private HtmlInputText txtAmount = new HtmlInputText();
	DecimalFormat oneDForm = new DecimalFormat("#.00");
	private String noofBeneficiaries;
	public HtmlInputText getTxtAmount() {
		return txtAmount;
	}

	public void setTxtAmount(HtmlInputText txtAmount) {
		this.txtAmount = txtAmount;
	}

	public HtmlSelectOneMenu getCmbBeneficiaryList() {
		return cmbBeneficiaryList;
	}

	public void setCmbBeneficiaryList(HtmlSelectOneMenu cmbBeneficiaryList) {
		this.cmbBeneficiaryList = cmbBeneficiaryList;
	}

	@Override
	public void init()
	{	
		super.init();
		if( !isPostBack() ) 
		{
			try 
			
			{
				DisbursementDetailsView disbursementDetails = (DisbursementDetailsView) sessionMap.remove( WebConstants.LOAD_PAYMENT_DETAILS);
				
				Long fileId = (Long) sessionMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_ID);
				viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID, fileId);
				
				HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
				String viewMode = request.getParameter( VIEW_MODE);
				if( null!=viewMode && viewMode.equals( VIEW_MODE_POPUP) ) {
					viewMap.put( VIEW_MODE, VIEW_MODE_POPUP);				
				}
				if(sessionMap.get( WebConstants.PAGE_MODE ) != null)
				{
					viewMap.put( WebConstants.PAGE_MODE , sessionMap.remove( WebConstants.PAGE_MODE ) );
				}
				viewMap.put( DISBURSEMENT_VIEW, disbursementDetails);
				
				List<DisbursementRequestBeneficiaryView> beneficiaryList = disbursementDetails.getRequestBeneficiaries();
				if( null==beneficiaryList ) 
				{
					beneficiaryList = new ArrayList<DisbursementRequestBeneficiaryView>();
					disbursementDetails.setRequestBeneficiaries( beneficiaryList);
				}
				viewMap.put( BENEFICIARY_LIST, beneficiaryList);
				List<DisbursementRequestBeneficiaryView> toDelete = new ArrayList<DisbursementRequestBeneficiaryView>();
				viewMap.put( TO_DELETE_LIST, toDelete);
				loadBeneficiaryList( fileId);
				
			} catch(Exception ex) {
				logger.LogException("[Exception occured in init()]", ex);
				errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
	}
		
	public Double getTotalAmount() {
		DisbursementDetailsView disbursement = this.getDisbursementDetails();
		return disbursement.getAmount();
	}
	
	public String getPaymentType() {
		DisbursementDetailsView disbursement = this.getDisbursementDetails();
		return disbursement.getPaymentTypeEn();
	}
	
	private boolean validateDone()throws Exception
	{
		List<DisbursementRequestBeneficiaryView> beneficiaries = getBeneficiaryList();
		
		double amount = 0;
		double totalAmount = this.getTotalAmount();
		for(DisbursementRequestBeneficiaryView singleView : beneficiaries) 
		{
			if( 
					singleView.getIsDeleted() != null && 
					singleView.getIsDeleted().compareTo(1L)==0 
			  ) 
			{
				continue;
			}
			amount += singleView.getAmount();
			boolean hasValidateCostCenter = validateCostCenter( singleView.getPersonId().toString() );
			boolean hasValidateBalance = validateBalance(
															getDisbursementDetails(), 
															singleView.getAmount(),
															singleView.getPersonId(),
															singleView.getName()
														 );
			if( !hasValidateCostCenter || !hasValidateBalance )
			{
				return false;
			}
		}
		logger.logDebug( "Total Amount to Disburse:%s, Sum of All Amounts:%s",Double.valueOf( oneDForm .format( totalAmount ) ),Double.valueOf( oneDForm.format( amount ) )  );
		if( Double.valueOf( oneDForm .format( totalAmount ) ).compareTo(Double.valueOf( oneDForm.format( amount ) )  )!= 0 )  
		{
			errorMessages.add( 
							  java.text.MessageFormat.format(
									  						 CommonUtil.getBundleMessage(  "normalDisbursement.msg.sumOfAmountNotEqualToAmountToDisburse"),
									  						 amount,totalAmount
									  						)
							  );
			return false;
		}
		return true;
	}
	
	private boolean validateCostCenter(String personId) throws Exception 
	{
		Map<String, String> map  =  (HashMap<String, String>) viewMap.get( BENEF_NO_COST_CENTER_MAP );
		if ( map.containsKey( personId ) )
		{
			String message = map.get( personId );
			errorMessages.add( 
								java.text.MessageFormat.format(
																CommonUtil.getBundleMessage("disburseFinance.msg.associateCostCenter"),
																 (" "+message)
															  )
							    
							 );
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean validateInput() throws Exception 
	{
		boolean bValid = true;
		DisbursementDetailsView  disbursementDetails = (DisbursementDetailsView)viewMap.get( DISBURSEMENT_VIEW );
		if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT))
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_BENEF_REQ));
			bValid = false;
		}
		else if
			( !validateCostCenter(cmbBeneficiaryList.getValue().toString() ) )
		{
			return false;
		}

		List<DisbursementRequestBeneficiaryView> beneficiaries = getBeneficiaryList();
		double updtTotalAmnt = 0;
		
		for(DisbursementRequestBeneficiaryView beneficiary : beneficiaries) 
		{
			Long benId = new Long( cmbBeneficiaryList.getValue().toString());
			updtTotalAmnt += beneficiary.getAmount();
			if( beneficiary.getPersonId().equals( benId)) 
			{				
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_BENEF_ALREADY));
				bValid = false;
			}
		}
		double amount =  0d;
		if ( null==txtAmount.getValue() || 0==txtAmount.getValue().toString().trim().length() ) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_AMNT_REQ));
			bValid = false;
		} 
		else 
		{
			try 
			{
				amount = Double.parseDouble(txtAmount.getValue().toString().trim());
			} 
			catch (NumberFormatException ex) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_AMNT_NUMERIC));
				return false;
			}
			 boolean hasValidateBalance = validateBalance(
															disbursementDetails, 
															amount,
															new Long (cmbBeneficiaryList.getValue().toString()),
															null
														 );
			 if( !hasValidateBalance )
			 {
				 return false;
			 }

		}
		
		Double totalAmount = getTotalAmount();
		updtTotalAmnt += amount;
		if( 
			Double.valueOf( oneDForm .format( totalAmount ) ).compareTo(Double.valueOf( oneDForm.format( updtTotalAmnt ) )  )< 0 		
		  ) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_AMNT_EXCEEDS));
			bValid = false;
		} 
					 
		return bValid;
	}

	/**
	 * @param disbursementDetails
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	private boolean validateBalance(DisbursementDetailsView disbursementDetails, double amount, Long beneficiaryId,String name ) throws Exception 
	{
		if ( disbursementDetails != null && disbursementDetails.getSrcType() != null &&
			   disbursementDetails.getSrcType().longValue() == WebConstants.PaymentSrcType.AID_ID
		   )
		{ 
			return true;
		}
		Long fileId = Long.valueOf( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() );
		Double requestedAmount = DisbursementService.getRequestedAmount(
																			beneficiaryId, 
																			fileId, 
																			disbursementDetails.getReqId(),
																			null
																	    );
		
		
		 int validateBalanceCode = validateBalance(
				 									 getBalanceMap().get( beneficiaryId ) ,
													 getBlockingMap().get( beneficiaryId ),
													 requestedAmount,
													 amount,
													 beneficiaryId,
													 name,
													 fileId
		   										  ) ;

		 if ( validateBalanceCode != 0 )
		 {
				 return false;
		 }
			
		 return true;
	}
	
	public void saveBeneficiary() 
	{
		try
		{
			if(! validateInput() ) 
			{return;}
			Double amount = Double.parseDouble( txtAmount.getValue().toString());
			List<SelectItem> beneficiaryList = this.getBeneficiaries();
			String personName = "";
			for(SelectItem singleItem : beneficiaryList) 
			{
				if( singleItem.getValue().toString().equals( cmbBeneficiaryList.getValue().toString()) ) 
				{
					personName = singleItem.getLabel();
					break;
				}
			}
			Long personId = Long.parseLong( cmbBeneficiaryList.getValue().toString());
			DisbursementRequestBeneficiaryView beneficiary = new DisbursementRequestBeneficiaryView();
			beneficiary.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
			beneficiary.setAmount( amount);
			beneficiary.setDestDetId( getDisbursementDetails().getDisDetId());
			beneficiary.setPersonId( personId);
			beneficiary.setName( personName);
			getBeneficiaryList().add( beneficiary);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_BENEF_ADD));
			//clear fields Start
			txtAmount.setValue("0");
			cmbBeneficiaryList.setValue("-1");
			accountBalance.setValue( "" );
			blockingBalance.setValue("");
			availableBalance.setValue("");
			//clear fields Finish
			
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in saveBeneficiary()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals("-1")) {
			return;
			}
			DisbursementDetailsView disbursementDetails = (DisbursementDetailsView)viewMap.get( DISBURSEMENT_VIEW );	
			Long requestId = -1l;
			if( disbursementDetails.getReqId() != null )
			{
				requestId  = disbursementDetails.getReqId() ;
			}
			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+
								  														Long.valueOf( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() )+"','"+ 
								  														requestId+
								  												 "');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals("-1")) {
			return;
			}
				
			executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+ viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() +"');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void done() 
	{
		try 
		{
			if( !validateDone() ) 
			{ return;}
			DisbursementDetailsView disbursement = this.getDisbursementDetails();
			List<DisbursementRequestBeneficiaryView> allRecords  = new ArrayList<DisbursementRequestBeneficiaryView>();
			allRecords.addAll( getBeneficiaryList());
			allRecords.addAll( getToDeleteList());
			disbursement.setRequestBeneficiaries( allRecords);
			RequestService reqWS = new RequestService();
			DisbursementDetailsView updatedView = reqWS.saveOrUpdate( disbursement);
			sessionMap.put( WebConstants.LOAD_PAYMENT_DETAILS, updatedView);
			String javaScriptText = "window.opener.handlePopupChange();  window.close();";
			executeJavaScript( javaScriptText );
			
		} catch(Exception ex) {
			logger.logInfo("[Exception occured in done()", ex) ;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}

	@SuppressWarnings("unchecked")
	private boolean hasOnDistributeError (  List<DisbursementRequestBeneficiaryView> beneficiaryList  , List<BeneficiariesPickListView> beneficiaresPickList   ) throws Exception
	{
		
		boolean hasError =  false;
		Double sumBenefShares = 0.0d;
		Double totalFileShare = 0.0d;
		String messageTemp = CommonUtil.getBundleMessage("normalDisbursment.msg.shariaShareEmpty");
		for (BeneficiariesPickListView obj : beneficiaresPickList) 
		{
	
			if(!validateCostCenter(obj.getPersonId().toString())) return true;
			if( 
					obj.getSharePercentage()  == null || obj.getSharePercentage()  <= 0 
			  )
		    {
				  String msg= java.text.MessageFormat.format(  
						  										messageTemp ,
						  										obj.getFullName()
						  									);
				  errorMessages.add( msg );
				  hasError = true;
				  break;
			}
			totalFileShare = obj.getTotalFileShares();
			sumBenefShares += obj.getSharePercentage()!= null?obj.getSharePercentage():0.0d;
		}
		if( totalFileShare.compareTo( sumBenefShares ) != 0)
		{
				String msg = java.text.MessageFormat.format( 
															CommonUtil.getBundleMessage("mems.inheritanceFile.msg.sumOfBenefShareNotEqualToFileShare"),
															sumBenefShares,
															totalFileShare
														   );
			  errorMessages.add(  msg  );
			  hasError = true;
		}
		return hasError;
	}
	@SuppressWarnings("unchecked")
	public void onDistribute() 
	{
		try
		{
			InheritedAssetService inheritedAssetWS = new InheritedAssetService();
	    	Long fileId = new Long( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString() );
			List<BeneficiariesPickListView> beneficiaresPickList = inheritedAssetWS.getBeneficiariesView(fileId);
			List<DisbursementRequestBeneficiaryView> beneficiaryList  = getBeneficiaryList();
			
			if (	hasOnDistributeError(beneficiaryList, beneficiaresPickList) )return;
			
			for (DisbursementRequestBeneficiaryView disbursementRequestBeneficiaryView : beneficiaryList) 
			{
			
				disbursementRequestBeneficiaryView.setIsDeleted( WebConstants.IS_DELETED_TRUE);
				getToDeleteList().add( disbursementRequestBeneficiaryView );
			}
			beneficiaryList = new ArrayList<DisbursementRequestBeneficiaryView>();
			divide(beneficiaryList, beneficiaresPickList);
			viewMap.put( BENEFICIARY_LIST, beneficiaryList);
			getBeneficiaryList();
			successMessages.add(CommonUtil.getBundleMessage( "normalDisbursment.msg.distributedShariaSuccess" ) );
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onDistribute()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	@SuppressWarnings ( "unchecked" )
	private void divide( List<DisbursementRequestBeneficiaryView> beneficiaryList  , List<BeneficiariesPickListView> beneficiaresPickList  ) throws Exception
	{
    	DecimalFormat oneDForm = new DecimalFormat("#.00");
    	
		double caluclatedAmount = 0.00d;
		double sumCaluclatedAmount = 0.00d;
		DisbursementDetailsView disbursementDetails =  getDisbursementDetails();
		DisbursementRequestBeneficiaryView lastObj = null; 
		for (BeneficiariesPickListView obj : beneficiaresPickList) 
		{
		
			DisbursementRequestBeneficiaryView beneficiary = new DisbursementRequestBeneficiaryView();
			beneficiary.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);

			beneficiary.setDestDetId( disbursementDetails .getDisDetId());
			beneficiary.setPersonId( obj.getPersonId() );
			beneficiary.setName( obj.getFullName() );
					
			caluclatedAmount = Double.valueOf( oneDForm.format(  obj.getFileShareRatio() * disbursementDetails.getAmount() ) );
			sumCaluclatedAmount += caluclatedAmount;
			beneficiary.setAmount( caluclatedAmount );
			lastObj = beneficiary;
			beneficiaryList.add( beneficiary );
			
			
		}
		if( sumCaluclatedAmount < disbursementDetails.getAmount().doubleValue() )
		{
			
			lastObj.setAmount(  Double.valueOf( oneDForm.format(
																lastObj.getAmount()+( disbursementDetails.getAmount() - sumCaluclatedAmount)
																)
											  )
							 ); 
		}
		else if( sumCaluclatedAmount > disbursementDetails.getAmount().doubleValue() )
		{
			lastObj.setAmount( 
								Double.valueOf( 
												oneDForm.format(  
																 lastObj.getAmount()- (sumCaluclatedAmount  - disbursementDetails.getAmount()) 
															   )
											   )
							);
		}
	}

	
	public void deleteBeneficiary() 
	{
		DisbursementRequestBeneficiaryView selectedRow = (DisbursementRequestBeneficiaryView) beneficiariesTable.getRowData();
		if( null!=selectedRow && null!=selectedRow.getDesReqBeneficiary() ) 
		{
			selectedRow.setIsDeleted( WebConstants.IS_DELETED_TRUE);
			getToDeleteList().add( selectedRow);
		}				
		getBeneficiaryList().remove( selectedRow);
	}
	
	private void loadBeneficiaryList(final Long fileId) throws Exception 
	{
		InheritedAssetService inheritedAssetWS = new InheritedAssetService();
		List<BeneficiariesPickListView> beneficiaresPickList = inheritedAssetWS.getBeneficiariesView(fileId);
		
		
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
		Map<String, String> benefNoCostCenterMap = new HashMap<String, String>();
		
		
		ArrayList<SelectItem> beneficiaries = new ArrayList<SelectItem>();
		
		for(BeneficiariesPickListView singleBeneficiary : beneficiaresPickList) 
		{
			SelectItem selItem = new SelectItem(singleBeneficiary.getPersonId().toString(),
												singleBeneficiary.getFullName() );
			beneficiaries.add(selItem);
			if(	singleBeneficiary.getCostCenter() == null )
			{
				benefNoCostCenterMap.put(singleBeneficiary.getPersonId().toString(),singleBeneficiary.getFullName() ); 		
			}
			else
			{
				Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(singleBeneficiary.getPersonId(),fileId);
	//            Double amount = accountWS.getAmountByPersonId( singleBeneficiary.getPersonId());
				
				personBalanceMap.put( singleBeneficiary.getPersonId(), amount);
				storeBeneficiaryBlockingAmountInMap(singleBeneficiary.getPersonId());
				
				Double blocking = BlockingAmountService.getBlockingAmount(singleBeneficiary.getPersonId(), null, null, null, null );
				personBlockingMap.put( singleBeneficiary.getPersonId(), blocking);
			}
			
		}			
		
		viewMap.put(CMB_BENEFICIARY, beneficiaries);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
		viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
	}

	private void storeBeneficiaryBlockingAmountInMap( Long beneficiaryId ) throws Exception
	  {
		   if(  !getBlockingMap().containsKey(beneficiaryId  ) )
		   {
		    
			Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
			Double amount =  BlockingAmountService.getBlockingAmount(  beneficiaryId , null, null, null, null );
			personBlockingMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
//			handleBeneficiaryChange();				
		   }
		   
	   }
	
	public List<DisbursementRequestBeneficiaryView> getToDeleteList() {
		return (List<DisbursementRequestBeneficiaryView>) viewMap.get( TO_DELETE_LIST);
	}
	public DisbursementDetailsView getDisbursementDetails()
	{
		if( viewMap.containsKey( DISBURSEMENT_VIEW)) {
			return (DisbursementDetailsView) viewMap.get( DISBURSEMENT_VIEW);
		}
		return new DisbursementDetailsView();
	}
	
	public List<DisbursementRequestBeneficiaryView> getBeneficiaryList() {
		if( viewMap.containsKey( BENEFICIARY_LIST)) {
			return (List<DisbursementRequestBeneficiaryView>) viewMap.get( BENEFICIARY_LIST);
		}
		return new ArrayList<DisbursementRequestBeneficiaryView>();
	}
	
	public List<SelectItem> getBeneficiaries() {
		if( viewMap.containsKey( CMB_BENEFICIARY)) {
			return (List<SelectItem>) viewMap.get( CMB_BENEFICIARY);
		}
		return new ArrayList<SelectItem>(0);
	}
	public Boolean getViewMode() {
		if( viewMap.containsKey( VIEW_MODE)) {
			return ((String) viewMap.get( VIEW_MODE)).equals( VIEW_MODE_POPUP);
		}
		return false;		
	}
	public Boolean getIsEditable()
	{
		if( viewMap.get( WebConstants.PAGE_MODE )==null )return true; else return false;
	}
	public HtmlDataTable getBeneficiariesTable() {
		return beneficiariesTable;
	}

	public void setBeneficiariesTable(HtmlDataTable beneficiariesTable) {
		this.beneficiariesTable = beneficiariesTable;
	}
	public void handleBeneficiaryChange() 
	{
		try
		{
		
			if( !cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT) && 
				validateCostCenter(cmbBeneficiaryList.getValue().toString() ) 	
			   ) 
			{
				Long fileId = (Long)viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID );
				DisbursementDetailsView disbDetails  = ( DisbursementDetailsView )viewMap.get( DISBURSEMENT_VIEW );
				Map<Long, Double> balanceMap = getBalanceMap();
				
				Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
				Double amount = 0.00d;
				Double blockingMapAmount = 0.00d;
				if( balanceMap.containsKey( beneficiaryId) ) 
				{
					amount = balanceMap.get( beneficiaryId);
					accountBalance.setValue( amount.toString());
				}
				
				Map<Long, Double> blockingMap = getBlockingMap();
				if( blockingMap.containsKey( beneficiaryId) ) 
				{
					blockingMapAmount = blockingMap.get( beneficiaryId);
					blockingBalance.setValue( blockingMapAmount.toString());
					
				}
				Double requestAmount = DisbursementService.getRequestedAmount(beneficiaryId, fileId, disbDetails.getReqId(),null );
				requestedAmount.setValue( requestAmount );
				
				
				DecimalFormat oneDForm = new DecimalFormat("#.00");
				availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
				
			} 
			else 
			{
				accountBalance.setValue( "");
				blockingBalance.setValue( "");
				availableBalance.setValue("");
				
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in handleBeneficiaryChange()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private Map<Long, Double> getBalanceMap() {
		if( viewMap.containsKey( PERSON_BALANCE_MAP)) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BALANCE_MAP);
		}
		return new HashMap<Long, Double>();
	}

	private Map<Long, Double> getBlockingMap() {
		if( viewMap.containsKey( PERSON_BLOCKING_MAP )) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BLOCKING_MAP  );
		}
		return new HashMap<Long, Double>();
	}
	public HtmlInputText getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(HtmlInputText accountBalance) {
		this.accountBalance = accountBalance;
	}

	public HtmlInputText getBlockingBalance() {
		return blockingBalance;
	}

	public void setBlockingBalance(HtmlInputText blockingBalance) {
		this.blockingBalance = blockingBalance;
	}

	public String getNoofBeneficiaries() {
		DisbursementDetailsView disbursement = this.getDisbursementDetails();
		noofBeneficiaries =  disbursement.getNoOfBeneficiaries()!= null ?disbursement.getNoOfBeneficiaries().toString():"";
		return noofBeneficiaries;
	}

}
