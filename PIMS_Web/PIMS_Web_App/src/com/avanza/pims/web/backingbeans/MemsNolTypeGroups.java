package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.UserGroup;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.MemsNolTypeUsrGrp;
import com.avanza.pims.entity.MemsNolTypes;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.GroupView;
import com.avanza.pims.ws.vo.MemsNolTypeView;

public class MemsNolTypeGroups extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private HtmlDataTable dataTable;
	private HtmlSelectOneMenu selectOneMemsNolType = new HtmlSelectOneMenu();

	private ArrayList<MemsNolTypeView> dataList = new ArrayList();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	@Override
	public void init() {
		super.init();
		try {
			showAll();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void prerender() {
		super.prerender();
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlSelectOneMenu getSelectOneMemsNolType() {
		return selectOneMemsNolType;
	}

	public void setSelectOneMemsNolType(HtmlSelectOneMenu selectOneMemsNolType) {
		this.selectOneMemsNolType = selectOneMemsNolType;
	}

	public ArrayList<MemsNolTypeView> getDataList() {
		if (viewMap.get("MEMS_NOL_TYPE_LIST") != null)
			return (ArrayList<MemsNolTypeView>) viewMap.get("MEMS_NOL_TYPE_LIST");

		return dataList;
	}

	public void setDataList(ArrayList<MemsNolTypeView> dataList) {

		this.dataList = dataList;
		if (this.dataList != null)
			viewMap.put("MEMS_NOL_TYPE_LIST", this.dataList);

	}

	public void buttonSearch() throws PimsBusinessException {

		dataList.clear();
		if (getSelectOneMemsNolType().getValue() != null) {
			Long value = Long.parseLong(getSelectOneMemsNolType().getValue()
					.toString());

			if (value == -1) {
				showAll();

			} else {
				MemsNolTypes memsNolType = EntityManager.getBroker().findById(
						MemsNolTypes.class, value);
				MemsNolTypeView memsNolTypeView = fillMemsNolTypeView(memsNolType);

				// if (memsNolType.getMemsNolTypeUserGroupMappings().size() > 0)
				// {
				dataList.add(memsNolTypeView);
				// }
			}
			if (this.dataList != null)
				this.setDataList(this.dataList);

		}
	}

	public void showAll() throws PimsBusinessException {

		MemsNolTypeView memsNolTypeView;
		List<MemsNolTypes> memsNolTypeList = EntityManager.getBroker().findAll(
				MemsNolTypes.class);
		for (MemsNolTypes memsNolType : memsNolTypeList) {
			memsNolTypeView = fillMemsNolTypeView(memsNolType);
			dataList.add(memsNolTypeView);
		}

		viewMap.remove("MEMS_NOL_TYPE_LIST");
	}

	public MemsNolTypeView fillMemsNolTypeView(MemsNolTypes memsNolType)
			throws PimsBusinessException {
		MemsNolTypeView memsNolTypeView = new MemsNolTypeView();
		memsNolTypeView.setMemsNolTypeNameEn(memsNolType.getNolTypeNameEn());
		memsNolTypeView.setMemsNolTypeNameAr(memsNolType.getNolTypeNameAr());
		memsNolTypeView.setUserGroups(getUserGroupString(memsNolType
				.getMemsNolTypeUsrGrps()));
		memsNolTypeView.setMemsNolTypeUserGroupMappings(memsNolType
				.getMemsNolTypeUsrGrps());

		memsNolTypeView.setMemsNolTypeId(memsNolType.getMemsNolTypeId());
		return memsNolTypeView;

	}

	private String getUserGroupString(Set userGroups)
			throws PimsBusinessException {
		String userGroupString = "";
		MemsNolTypeUsrGrp memsNolTypeUserGroupMapping = null;
		Object[] userGroupsArray = userGroups.toArray();

		HashMap<String, GroupView> groupMap = new UtilityService()
				.getGroupMap();
		GroupView groupView = null;

		if (userGroups.size() > 0) {
			for (int count = 0; count < userGroupsArray.length; count++) {
				memsNolTypeUserGroupMapping = (MemsNolTypeUsrGrp) userGroupsArray[count];

				groupView = groupMap.get(memsNolTypeUserGroupMapping
						.getUserGroupId());
				userGroupString = userGroupString
						+ groupView.getGroupPrimaryName();

				if (count < userGroupsArray.length - 1) {
					userGroupString = userGroupString + " - ";
				}
			}
		}

		return userGroupString;
	}

	public void editLinkClick(javax.faces.event.ActionEvent event) {
		logger.logInfo("selectProcedureTask() started...");

		try {

			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "javascript:showMemsNolTypeGroupsMappingPopup();";
			// MEMS_NOL_TYPE_LIST
			MemsNolTypeView selectedMemsNolTypeView = (MemsNolTypeView) dataTable
					.getRowData();
			List<UserGroup> memsNolTypeGroups = new ArrayList<UserGroup>();

			sessionMap.put("selectedMemsNolTypeView", selectedMemsNolTypeView);
			// Add the Javascript to the rendered page's header for
			// immediate execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("selectProcedureTask() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("selectProcedureTask() crashed ", exception);
		}

	}

}