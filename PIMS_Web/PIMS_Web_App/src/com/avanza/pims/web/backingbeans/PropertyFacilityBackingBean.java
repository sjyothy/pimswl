package com.avanza.pims.web.backingbeans;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.util.Logger;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;


import com.avanza.pims.ws.vo.FacilityView;

import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.PropertyFacilityView;
import com.avanza.pims.ws.vo.PropertyView;

import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.controller.AbstractController;

public class PropertyFacilityBackingBean extends AbstractController {
	
	
	
	private HtmlDataTable dataTable;
	private String facilitySearch = new String();
	private HtmlInputHidden allSelectedFacilities = new HtmlInputHidden();
	private List<FacilityView> facilities = new ArrayList<FacilityView>();
	

	
	private static Logger logger = Logger.getLogger("PropertyFacilityBackingBean");
	
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	
	public PropertyFacilityBackingBean(){
		
		
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub		
		super.init();	
		SearchFacilities();
	}
	
	public String SearchFacilities(){
		
		logger.logInfo("Stepped into the SearchFacilities method");
		facilities = null;
		try {
			
			System.out.println("Steped in SearchFacilities");
			
			
			PropertyView propertyView = new PropertyView();
			propertyView.setPropertyId(5L);
			
			
			facilities = psa.getAllFacilityByNameExcludingProperty(facilitySearch, propertyView);
			
			
		
			logger.logInfo("SearchFacilities method Completed Succesfully");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.logError("SrearchFacility crashed due to:"+e.getStackTrace());
		}
		
		return "";
	}
	

	public String AddFacilities(){
		
		logger.logInfo("Stepped into the AddFacilities method");
		System.out.println("Selected Facilites are : "+allSelectedFacilities.getValue());
		
		String comaSeperatedFacilities = (String) allSelectedFacilities.getValue();
		
if (comaSeperatedFacilities!= null && comaSeperatedFacilities.length()>0)
{


Set <PropertyFacilityView> propertyFacilityViewSet = new HashSet(0);

String[] facilities = comaSeperatedFacilities.split(",");

Calendar c = Calendar.getInstance();

		
for (int i=0;i<facilities.length;i++)
{
		long l = Long.parseLong(facilities[i]);
		
		PropertyFacilityView propertyfacilityView = new PropertyFacilityView();
//		propertyfacilityView.setFacilityId(l);
//		propertyfacilityView.setPropertyId(5L);
//		propertyfacilityView.setAccountNo("abc");
//		propertyfacilityView.setCreatedBy("A F Z A L");
		propertyfacilityView.setCreatedOn(new Date());
		propertyfacilityView.setUpdatedBy("A F Z A L");
		propertyfacilityView.setUpdatedOn(new Date());
		propertyfacilityView.setIsDeleted(new Long(0));
		propertyfacilityView.setRecordStatus(new Long(1));
		
		propertyFacilityViewSet.add(propertyfacilityView);
}

		try {
			System.out.println("Successful Persistence = "+psa.persistPropertyFacilities(propertyFacilityViewSet));
		} catch (com.avanza.pims.business.exceptions.PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
		logger.logInfo("AddFacilities method Completed Succesfully");
		
		
		
		return "success";
	}
		
	public String getFacilitySearch() {
		return facilitySearch;
	}

	public void setFacilitySearch(String facilitySearch) {
		this.facilitySearch = facilitySearch;
	}

	public List getFacilities() {
		return facilities;
	}

	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputHidden getAllSelectedFacilities() {
		return allSelectedFacilities;
	}

	public void setAllSelectedFacilities(HtmlInputHidden allSelectedFacilities) {
		this.allSelectedFacilities = allSelectedFacilities;
	}

	
	
	
	

}
