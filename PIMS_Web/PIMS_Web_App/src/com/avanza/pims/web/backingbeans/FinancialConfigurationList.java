package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.FinanceServiceAgent;

import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;

import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.FinancialConfigurationView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.ui.util.ResourceUtil;

public class FinancialConfigurationList extends AbstractController{
	//Initialization
	//Commons
	FinanceServiceAgent financeServiceAgent = new FinanceServiceAgent();
	FinancialConfigurationView financialConfigurationView = new FinancialConfigurationView();
	SystemParameters parameters = SystemParameters.getInstance();
	private transient Logger logger = Logger.getLogger(FinancialConfigurationList.class);
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map<String,Object> sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil = new CommonUtil();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private HtmlInputHidden parentHidden = new HtmlInputHidden();
	
	//List
	private HtmlDataTable configurationGrid = new HtmlDataTable();
	private List<FinancialConfigurationView> configurationDataList;
	//Search Fields
	private HtmlInputText bankRemitanceAccountName = new HtmlInputText();
	private HtmlInputText GRPAccountNumber = new HtmlInputText();
	//Search Menu
	private HtmlSelectOneMenu paymentTypeMenu = new HtmlSelectOneMenu();
    List<SelectItem> paymentTypeList = new ArrayList<SelectItem>();	
	
	private HtmlSelectOneMenu ownerShipMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu trxTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu paymentMethod = new HtmlSelectOneMenu();
	
	
	
	
	
	
	
	
	//Command Buttons
	private HtmlCommandButton searchButton = new HtmlCommandButton();
	private HtmlCommandButton clearButton = new HtmlCommandButton();
	
	//Life Cycle Implementation
	@SuppressWarnings("unchecked")
	public void init(){
		super.init();
		UtilityServiceAgent usa = new UtilityServiceAgent();
		try {
			logger.logInfo("GRP Posting Init");
			List<PaymentTypeView> listPaymentType = null;
			if(!viewMap.containsKey("LIST_PAYMENT_TYPE")||
					viewMap.get("LIST_PAYMENT_TYPE")==null){
				listPaymentType= usa.getAllPaymentTypes();
				viewMap.put("LIST_PAYMENT_TYPE",listPaymentType);
			}
			if(listPaymentType!=null){
				for(PaymentTypeView paymentTypeView : listPaymentType){
					if(isEnglishLocale())
						paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn()));
					else
						paymentTypeList.add(new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr()));
				}
				viewMap.put("PAYMENT_TYPE_LIST",paymentTypeList);
			}
		}
		catch (PimsBusinessException e) 
		{
			// TODO Auto-generated catch block
			logger.LogException("Financial Configuration INIT CRASHED", e);
		} 
	}
	public void preprocess(){
		super.preprocess();
	}
	public void prerender(){
		super.prerender();
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentMethodList()
	{
		
		
		List<SelectItem> itemList = new ArrayList<SelectItem>(0);
		
		
			// List<PaymentMethodView> voList  = new ArrayList<PaymentMethodView>(0);
		    // if(viewMap.containsKey(key))
			List<PaymentMethodView> voList = new UtilityServiceAgent().getPaymentMethods();
			for (PaymentMethodView vo: voList) 
			{
				SelectItem item = new SelectItem(vo.getPaymentMethodId().toString(),vo.getDescription());
				itemList.add(item);
				
			}
		
		return itemList;
	}
	private void setQueryCriteria()
	{
		financialConfigurationView.setGrpAccountNumber(GRPAccountNumber.getValue().toString());
		financialConfigurationView.setBankRemitanceAccountName(bankRemitanceAccountName.getValue().toString());
		if(ownerShipMenu.getValue()!=null && !ownerShipMenu.getValue().toString().equals("-1"))
			financialConfigurationView.setOwnerShipTypeId(Long.valueOf(ownerShipMenu.getValue().toString()));
		
		if(paymentTypeMenu.getValue()!=null&&  !paymentTypeMenu.getValue().toString().equals("-1"))
			financialConfigurationView.setPaymentTypeId(Long.valueOf(paymentTypeMenu.getValue().toString()));
		if(trxTypeMenu.getValue() !=null && trxTypeMenu.getValue().toString().trim().length()>0 &&
				!trxTypeMenu.getValue().toString().trim().equals("-1"))
			financialConfigurationView.setTrxTypeName(trxTypeMenu.getValue().toString());
		if(paymentMethod.getValue()!=null&&  !paymentMethod.getValue().toString().equals("-1"))
			financialConfigurationView.setPaymentMethodId(Long.valueOf(paymentMethod.getValue().toString()));
		
	}
	
	@SuppressWarnings("unchecked")
	public List<FinancialConfigurationView> getConfigurationDataList() 
	{
		if(viewMap.containsKey(WebConstants.FinancialAccConfiguration.TRANSACTIONS_LIST))
			configurationDataList = (List<FinancialConfigurationView>) viewMap.get(WebConstants.FinancialAccConfiguration.TRANSACTIONS_LIST);

		if(configurationDataList == null)
			configurationDataList = new ArrayList<FinancialConfigurationView>();


		return configurationDataList;
	}
	public void setConfigurationDataList(List<FinancialConfigurationView> configurationDataList) {
		this.configurationDataList = configurationDataList;
	}
	@SuppressWarnings("unchecked")
	public void searchTransactions()
	{
		setQueryCriteria();
		if(configurationDataList==null)
			configurationDataList = new ArrayList<FinancialConfigurationView>();
		try 
		{
			configurationDataList = financeServiceAgent.getFinancialAccConfigurationList(financialConfigurationView);
			viewMap.put("recordSize", configurationDataList.size());
			viewMap.put(WebConstants.FinancialAccConfiguration.TRANSACTIONS_LIST, configurationDataList);
		} catch (PimsBusinessException e) 
		{
			logger.LogException("searchTransactions|Error|", e);
			
		}
		 catch (Exception e) 
			{
				logger.LogException("searchTransactions|Error|", e);
				
			}
	}
	public String btnClear_Click()
	{
		
		return "clear";
		
	}
	public String btnAdd_Click()
	{
		
		return "manage";
		
	}
	@SuppressWarnings("unchecked")
	public String imgEdit_Click()
	{
		String methodName = "imgEdit_Click|";
		logger.logInfo(methodName+"Start");
		sessionMap.put(WebConstants.FinancialAccConfiguration.FINANCIAL_VIEW, configurationGrid.getRowData());
		logger.logInfo(methodName+"Finish");
		
		
		return "manage";
	}
	public void cmdDelete_Click()
	{
		String methodName="cmdDelete_Click|";
		FinancialConfigurationView financialConfiguration = (FinancialConfigurationView)configurationGrid.getRowData();
		financialConfiguration.setUpdatedBy(getLoggedInUserId());
		try 
		{
			logger.logInfo(methodName+"Started");
			financeServiceAgent.deleteFinancialAccConfiguration(financialConfiguration);
			this.getConfigurationDataList().remove(financialConfiguration);
			logger.logInfo(methodName+"Finished");
		} 
		catch (PimsBusinessException e) 
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		
		}
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}
	
	public HtmlInputText getGRPAccountNumber() {
		return GRPAccountNumber;
	}
	public void setGRPAccountNumber(HtmlInputText accountNumber) {
		GRPAccountNumber = accountNumber;
	}
	public HtmlSelectOneMenu getPaymentTypeMenu() {
		return paymentTypeMenu;
	}
	public void setPaymentTypeMenu(HtmlSelectOneMenu paymentTypeMenu) {
		this.paymentTypeMenu = paymentTypeMenu;
	}
	public HtmlSelectOneMenu getOwnerShipMenu() {
		return ownerShipMenu;
	}
	public void setOwnerShipMenu(HtmlSelectOneMenu ownerShipMenu) {
		this.ownerShipMenu = ownerShipMenu;
	}
	public HtmlCommandButton getSearchButton() {
		return searchButton;
	}
	public void setSearchButton(HtmlCommandButton searchButton) {
		this.searchButton = searchButton;
	}
	public HtmlCommandButton getClearButton() {
		return clearButton;
	}
	public void setClearButton(HtmlCommandButton clearButton) {
		this.clearButton = clearButton;
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	


	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPaymentTypeList() {
		if(viewMap.containsKey("PAYMENT_TYPE_LIST")&&
				viewMap.get("PAYMENT_TYPE_LIST")!=null){
			paymentTypeList= ((List<SelectItem>)viewMap.get("PAYMENT_TYPE_LIST"));
		}
		
		return paymentTypeList;
	}
	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public HtmlInputHidden getParentHidden() {
		return parentHidden;
	}
	public void setParentHidden(HtmlInputHidden parentHidden) {
		this.parentHidden = parentHidden;
	}
	public HtmlDataTable getConfigurationGrid() {
		return configurationGrid;
	}
	public void setConfigurationGrid(HtmlDataTable configurationGrid) {
		this.configurationGrid = configurationGrid;
	}
	public HtmlInputText getBankRemitanceAccountName() {
		return bankRemitanceAccountName;
	}
	public void setBankRemitanceAccountName(HtmlInputText bankRemitanceAccountName) {
		this.bankRemitanceAccountName = bankRemitanceAccountName;
	}

public HtmlSelectOneMenu getTrxTypeMenu() {
	return trxTypeMenu;
}
public void setTrxTypeMenu(HtmlSelectOneMenu trxTypeMenu) {
	this.trxTypeMenu = trxTypeMenu;
}
public HtmlSelectOneMenu getPaymentMethod() {
	return paymentMethod;
}
public void setPaymentMethod(HtmlSelectOneMenu paymentMethod) {
	this.paymentMethod = paymentMethod;
}
}
