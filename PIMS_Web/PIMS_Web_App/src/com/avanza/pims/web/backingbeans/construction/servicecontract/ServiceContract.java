package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.plugins.ScopeOfWork;
import com.avanza.pims.web.backingbeans.construction.servicecontract.GeneratePayments;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExtendApplicationFilterView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.ServiceContractView;
import com.avanza.pims.ws.vo.TenderPropertyView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.pims.ws.vo.TenderWorkScopeView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class ServiceContract extends AbstractController {
	
	private transient Logger logger = Logger.getLogger(ServiceContract.class);
	public interface Page_Mode {
		
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String EDIT="PAGE_MODE_EDIT";
		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
		public static final String COMPLETE="PAGE_MODE_COMPLETE";
		public static final String POPUP="PAGE_MODE_POPUP";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String ACTIVE="PAGE_MODE_ACTIVE";
		
	}
    public interface TAB_ID {
		
		public static final String ContractDetails="tabContractDetails";
		public static final String WorkScope="scopeOfWorkTab";
		public static final String Properties="tabProperties";
		public static final String PaymentSchedule="tabPaymentSchedule";
		public static final String Attachment="attachmentTab";
		
		
	}
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	protected  String pageMode="pageMode";
	HttpServletRequest request ;
	
	protected SystemParameters parameters = SystemParameters.getInstance();
	Map viewRootMap ;
	Map sessionMap ;
	FacesContext context ;
	protected List<String> errorMessages = new ArrayList<String>();
	protected List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil ;
	protected DomainDataView ddContractType;
	protected String CONTRACT_TYPE="CONTRACT_TYPE";
	protected String DD_CONTRACT_TYPE="DD_CONTRACT_TYPE";
	protected String DD_CONTRACT_STATUS_LIST ="DD_CONTRACT_STATUS_LIST";
	protected String PROCEDURE_TYPE;
	protected String EXTERNAL_ID;
	protected String TENDER_INFO ="tenderInfo";
	protected String contractCreatedOn;
	protected String contractCreatedBy;
	protected ContractorView contractor= new ContractorView();
	protected String CONTRACTOR_INFO  ="CONTRACTOR_INFO";
	protected String CONTRACT_VIEW ="CONTRACT_VIEW";
	protected String REQUEST_VIEW ="REQUEST_VIEW";
	protected ServiceContractView serviceContractView = new ServiceContractView();
	protected RequestView requestView = new RequestView();
	public String contractId;
	public String pageTitle;
	protected org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
	protected HtmlCommandButton btnApprove = new HtmlCommandButton();
    protected HtmlCommandButton btnReject = new HtmlCommandButton();
    protected HtmlCommandButton btnReview = new HtmlCommandButton();
    protected HtmlCommandButton btnComplete = new HtmlCommandButton();
    protected HtmlCommandButton btnPrint = new HtmlCommandButton();
    protected HtmlCommandButton btnReviewReq = new HtmlCommandButton();
    
    protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    protected String txtRemarks;    
    protected List<TenderWorkScopeView> tenderWorkScopeViewList =new ArrayList<TenderWorkScopeView>(0);
    protected List<TenderPropertyView> tenderPropertyViewList =new ArrayList<TenderPropertyView>(0);
    protected String hdnTenderId;
    protected String hdnContractorId;
	protected String requestId;
	
	public String getHdnTenderId() {
		return hdnTenderId;
	}

	public void setHdnTenderId(String hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public ServiceContract(){
		request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getFacesContext().getExternalContext().getSessionMap();
		context = FacesContext.getCurrentInstance();
		commonUtil = new CommonUtil();
		PROCEDURE_TYPE ="procedureType";
		EXTERNAL_ID ="externalId";
	}
	
	@Override
    public void init() 
    {
    	
    	String methodName="init";
    	try{
	    	logger.logInfo(methodName+"|"+"Start");
	    	
		       if(!isPostBack())
		       {
		    	   viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE,true);
		    	   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ADD);
		    	   logger.logDebug(methodName+"|"+"Is not postback");
		    	   loadAttachmentsAndComments(null);
		    	   		    	   
			    	if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				       getDataFromTaskList();
			    	else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
			    	 {
			    		 requestView=(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
					     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
					         this.contractId=requestView.getContractView().getContractId().toString();
					}
			    	else if (getRequestParam(WebConstants.SERVICE_CONTRACT_ID)!=null)
			    		this.contractId = getRequestParam(WebConstants.SERVICE_CONTRACT_ID).toString();
			    	if((this.contractId!=null && this.contractId.trim().length()>0) )
			    	{
			    		getContractById(this.contractId );
			    		setPageModeBasedOnContractStatus();
			    		
			    	}
		       }	
		       else
		       {
		       
			       if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
			       {
			    	   getPaymentScheduleFromSession();
			    	   
			       }
		       }
	    	logger.logInfo(methodName+"|"+"Finish");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|Error Occured", ex);
    	}
    }

	private void getPaymentScheduleFromSession() {
		String methodName ="getPaymentScheduleFromSession";
		logger.logInfo(methodName+"|Start");
		List<PaymentScheduleView> tempPaySch=new ArrayList<PaymentScheduleView>(0);   
		   viewRootMap.put( WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,	   
				   (ArrayList<PaymentScheduleView>)sessionMap.get(
				                                     WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		   
		   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		   
			logger.logInfo(methodName+"|Finish");
	}
	
	public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			ServiceContractView cView =(ServiceContractView)viewRootMap.get(CONTRACT_VIEW);
			RequestView rv =(RequestView)viewRootMap.get(REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		
		
		
	}
	
	private void setPageModeBasedOnContractStatus()
	{
		DomainDataView ddContractStatus = commonUtil.getDomainDataFromId( getContractStatusList(),
                serviceContractView.getStatus());
		if(ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_DRAFT))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.COMPLETE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_ACTIVE))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ACTIVE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_REVIEW_REQUIRED_LEGAL))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.LEGAL_REVIEW_REQUIRED);
			return;
		}

	}

	protected List<DomainDataView>  getContractStatusList() {
		List<DomainDataView> ddvContractStatusList = null;
		if(!viewRootMap.containsKey(DD_CONTRACT_STATUS_LIST))
		{
		ddvContractStatusList = commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
		viewRootMap.put(DD_CONTRACT_STATUS_LIST, ddvContractStatusList );
		}
		else
			ddvContractStatusList =(ArrayList<DomainDataView>)viewRootMap.get(DD_CONTRACT_STATUS_LIST);
		return ddvContractStatusList ;
	}
	private void getRequestForContract()throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		if(viewRootMap.get(PROCEDURE_TYPE).toString().equals(WebConstants.PROCEDURE_TYPE_PREPARE_MAINTENANCE_CONTRACT))
		      rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_MAINTENANCE_CONTRACT);
		else if(viewRootMap.get(PROCEDURE_TYPE).toString().equals(WebConstants.PROCEDURE_TYPE_PREPARE_INSURANCE_CONTRACT))
			  rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_INSURANCE_CONTRACT);
		else if(viewRootMap.get(PROCEDURE_TYPE).toString().equals(WebConstants.PROCEDURE_TYPE_PREPARE_SECURITY_CONTRACT))
			  rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_SECURITY_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(new Long(this.contractId));
		
		rv.setContractView(cv);
		
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0); 
			viewRootMap.put(REQUEST_VIEW,requestView );
			loadAttachmentsAndComments(requestView.getRequestId());
			 
		}
    }
	protected void manageControlsForTenders()
	{
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		ScopeOfWork sw =(ScopeOfWork)getBean("pages$ScopeOfWork");
		if(this.hdnTenderId!=null && this.hdnTenderId.length()>0 && isPostBack())
		{
		
			
			scpd.getBtnAddProperty().setRendered(false);
			scpd.getBtnAddUnits().setRendered(false);
			scpd.getLnkDelPropUnit().setRendered(false);
			sw.getSaveScopeOfWork().setRendered(false);
			sw.getImgdeleteLink().setRendered(false);
			(scdt.getTxtTotalAmount()).setDisabled(true);
			(scdt.getImgSearchContractor()).setRendered(false);
			if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
			(scdt.getImgRemoveTender()).setRendered(true);
		}
		else
		{
			scpd.getBtnAddProperty().setRendered(true);
			scpd.getBtnAddUnits().setRendered(true);
			scpd.getLnkDelPropUnit().setRendered(true);
			sw.getSaveScopeOfWork().setRendered(true);
			sw.getImgdeleteLink().setRendered(true);
			(scdt.getTxtTotalAmount()).setDisabled(false);
			if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD) || viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
			(scdt.getImgSearchContractor()).setRendered(true);
			
			(scdt.getImgRemoveTender()).setRendered(false);
		}
      
	}
	protected void PageModeAdd()
	{
		
		tbl_Action.setRendered(false);
		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls("");
		
	}
	protected void getTenderInfo()throws PimsBusinessException
    {
    	String methodName="getTenderInfo";
    	logger.logInfo(methodName+"| Start");
    	TenderView tenderView;
    	
    	if(viewRootMap.containsKey(TENDER_INFO))
    	{
    		tenderView=(TenderView)viewRootMap.get(TENDER_INFO);
	    	if(hdnTenderId!=null && hdnTenderId.toString().trim().length()>0 && tenderView.getTenderId()!=null && tenderView.getTenderId().compareTo(new Long(hdnTenderId))!=0)
	    	{
	    		tenderView = getTenderById();
	    		if(tenderView !=null)
	    		{
	    			viewRootMap.put(TENDER_INFO,tenderView);
	    		  //WHEN NEW TENDER IS SELECTED PREVIOUS VALUES if any of scope of work and property tab will be removed
	    			viewRootMap.remove(WebConstants.Tender.SCOPE_OF_WORK_LIST);
	    			viewRootMap.remove(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
	    			
	    		}
	    	}
    	}
    	else if(hdnTenderId!=null && hdnTenderId.trim().length()>0)
    	{
    		tenderView = getTenderById();
    		if(tenderView !=null)
    		{
    			viewRootMap.put(TENDER_INFO,tenderView);
    			//WHEN NEW TENDER IS SELECTED PREVIOUS VALUES if any of scope of work and property tab will be removed
    			viewRootMap.remove(WebConstants.Tender.SCOPE_OF_WORK_LIST);
    			viewRootMap.remove(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
    			
    		}
    			
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }
    protected void getContractPropertyByTenderId() throws PimsBusinessException
    {
    	String methodName ="getContractPropertyByTenderId";
    	logger.logInfo(methodName+"|Start");
    	ServiceContractAgent sca = new ServiceContractAgent();
    	List<TenderPropertyView> tpvList = sca.getTenderPropertyViewByTenderId(new Long(hdnTenderId));
    	viewRootMap.put(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST, tpvList);
    	ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.getTenderPropertyViewList();
		
    	logger.logInfo(methodName+"|Finish");
    	
    }
    protected void getContractWorkScopeByTenderId() throws PimsBusinessException
    {
    	String methodName ="getContractWorkScopeByTenderId";
    	logger.logInfo(methodName+"|Start");
    	ServiceContractAgent sca = new ServiceContractAgent();
    	List<TenderWorkScopeView> tpwsList = sca.getTenderScopeOfWorkByTenderId(new Long(hdnTenderId));
    	viewRootMap.put(WebConstants.Tender.SCOPE_OF_WORK_LIST, tpwsList );
    	
    	logger.logInfo(methodName+"|Finish");
    	
    }
    protected void getContractPropertyByContractId() throws PimsBusinessException
    {
    	String methodName ="getContractPropertyByContractId";
    	logger.logInfo(methodName+"|Start");
    	ServiceContractAgent sca = new ServiceContractAgent();
    	List<TenderPropertyView> tpvList = sca.getTenderPropertyViewByContractId(new Long(contractId));
    	viewRootMap.put(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST, tpvList);
    	logger.logInfo(methodName+"|Finish");
    	
    }
    protected void getContractWorkScopeByContractId() throws PimsBusinessException
    {
    	String methodName ="getContractWorkScopeByContractId";
    	logger.logInfo(methodName+"|Start");
    	ServiceContractAgent sca = new ServiceContractAgent();
    	List<TenderWorkScopeView> tpwsList = sca.getTenderScopeOfWorkByContractId(new Long(contractId));
    	viewRootMap.put(WebConstants.Tender.SCOPE_OF_WORK_LIST, tpwsList );
    	ScopeOfWork sw =(ScopeOfWork)getBean("pages$ScopeOfWork");
		sw.getScopeOfWorkList();    	
		
    	logger.logInfo(methodName+"|Finish");
    	
    }
	protected TenderView getTenderById() throws PimsBusinessException {
		ServiceContractAgent csa=new ServiceContractAgent();
		PersonView pv=new PersonView();
		pv.setPersonId(new Long(hdnTenderId));
		TenderView tenderView=  csa.getTenderViewByTenderId(new Long(hdnTenderId));
		return tenderView;
	}
	protected void setPageTitleBasedOnProcedureType()
	{
		
		if(viewRootMap.get(PROCEDURE_TYPE).toString().trim().equals(WebConstants.PROCEDURE_TYPE_PREPARE_MAINTENANCE_CONTRACT))
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.TITLE_MAINTAINENCE_CONTRACT));
		else if(viewRootMap.get(PROCEDURE_TYPE).toString().trim().equals(WebConstants.PROCEDURE_TYPE_PREPARE_INSURANCE_CONTRACT))
            this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.TITLE_INSURANCE_CONTRACT));
		else if(viewRootMap.get(PROCEDURE_TYPE).toString().trim().equals(WebConstants.PROCEDURE_TYPE_PREPARE_SECURITY_CONTRACT))
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.TITLE_SECURITY_CONTRACT));
	}
	
	protected ContractorView getContractorInfo()throws PimsBusinessException
    {
    	ConstructionServiceAgent csa=new ConstructionServiceAgent();
	    if(viewRootMap.containsKey(CONTRACTOR_INFO))
    	{
    		contractor=(ContractorView)viewRootMap.get(CONTRACTOR_INFO);
	    	if(hdnContractorId!=null && hdnContractorId.trim().length()>0 && 
	    			contractor.getPersonId()!=null && contractor.getPersonId().compareTo(new Long(hdnContractorId))!=0)
	    	{
	    		contractor =  csa.getContractorById(new Long(hdnContractorId), null);
	    	    viewRootMap.put(CONTRACTOR_INFO, contractor );
	    	
	    			
	    	}
	        return contractor;
    	}
    	else if(hdnContractorId!=null && hdnContractorId.trim().length()>0 )
    	{
    		contractor =  csa.getContractorById(new Long(hdnContractorId), null);
    	    viewRootMap.put(CONTRACTOR_INFO, contractor );
    	    return contractor;
    			
    	}
    	else 
    		return null;
	    	    
    }

	
	protected Boolean generateNotification(String eventName)
    {
    	   String methodName ="generateNotification";
    	   Boolean success = false;
            try
            {
                  logger.logInfo(methodName+"|Start");
                  HashMap placeHolderMap = new HashMap();
                  serviceContractView= (ServiceContractView)viewRootMap.get("CONTRACT_VIEW");
                  List<ContactInfo> contractorEmailList    = new ArrayList<ContactInfo>(0);
                  
                  NotificationFactory nsfactory = NotificationFactory.getInstance();
                  NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
                  Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
                  getNotificationPlaceHolder(event);
                  if(viewRootMap.containsKey(CONTRACTOR_INFO))
                  {
                        contractorEmailList = CommonUtil.getEmailContactInfos((PersonView)viewRootMap.get(CONTRACTOR_INFO));
                        if(contractorEmailList.size()>0)
                              notifier.fireEvent(event, contractorEmailList);    
                  }     
                  
                  success  = true;
                  logger.logInfo(methodName+"|Finish");
            }
    	   catch(Exception ex)
    	   {
    	          logger.LogException(methodName+"|Finish", ex);
    	   }
    	   return success;
    }
    
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		
		logger.logDebug(methodName+"|Finish");
		
	}
	
	protected void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present ");
			try
			{
				ServiceContractAgent sca =new ServiceContractAgent();
				serviceContractView= sca.getServiceContractById(new Long(contractId));
				viewRootMap.put(CONTRACT_VIEW ,serviceContractView);
				this.contractId =contractId;
				refreshViews();
				
				
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|"+"Error occured:",ex);
				throw ex;
				
			}
	}

	protected void refreshViews() throws PimsBusinessException {
		getRequestForContract();
		populateContractDetailsTabFromContractView();
		getPaymentScheduleByContractId();
		getContractWorkScopeByContractId();
		getContractPropertyByContractId();
	}
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String methodName="setTaskOutCome";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			String loggedInUser=getLoggedInUser();
			logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId()+"| TaskOutCome..."+taskOutCome+"|loggedInUser..."+loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
    public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
    public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.CONTRACT;
    		logger.logInfo(methodName + "contractId..."+contractId);
    		if(contractId!=null && contractId.trim().length()>0)
    		{
    		  Long contractsId = new Long(contractId);
    		  logger.logInfo(methodName + "notesOwner..."+notesOwner);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, contractsId );
	    	  logger.logInfo(methodName + "completed successfully!!!");
    		}
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	 CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	public void loadAttachmentsAndComments(Long requestId){
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewRootMap.get(PROCEDURE_TYPE).toString());
    		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
        	String externalId = viewRootMap.get(EXTERNAL_ID).toString();
        	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
    		viewMap.put("noteowner", WebConstants.REQUEST);
    		if(requestId!= null){
    	    	String entityId = requestId.toString();
    			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
    			viewMap.put("entityId", entityId);
    		}
    	}	
    public void tabAttachmentsComments_Click()
    {
    	String methodName ="saveCommensAttachment";
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		 if(requestView != null && requestView.getRequestId()!=null)
		 {
			 loadAttachmentsAndComments(requestView.getRequestId());
		 }
    	
    }
	
	protected void getDataFromTaskList()throws Exception
    {
    	    	   String methodName="getDataFromTaskList";
    	    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
    		      viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
    		      if(userTask.getTaskAttributes().get("CONTRACT_ID")!=null)
    			       this.contractId= userTask.getTaskAttributes().get("CONTRACT_ID").toString();
    		       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    }
	
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
		    return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
	protected  String getLoggedInUser() 
	{
	        return CommonUtil.getLoggedInUser();
	}
	public String getContractId() {
		return contractId;
	}

	protected TenderView getTenderView()
	{
		TenderView tv =null;
		if(viewRootMap.containsKey(TENDER_INFO))
			tv = (TenderView )viewRootMap.get(TENDER_INFO);
		return tv;
	}
	@SuppressWarnings("unchecked")
	
	protected void populateContractDetailsTabFromContractView()throws PimsBusinessException
	{
		String methodName ="populateContractDetailsTabFromContractView";
		logger.logInfo(methodName+"|"+"Start..");
		TenderView tv =getTenderView();
		DateFormat dateFormat =new SimpleDateFormat(getDateFormat());
	    //if contract has been saved
		if(contractId != null && contractId.trim().length()>0)
		{		
			   serviceContractView = (ServiceContractView)viewRootMap.get(CONTRACT_VIEW);
			   if(serviceContractView.getTenderView()!=null)
			   {
                viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,serviceContractView.getTenderView().getTenderNumber());
                if(serviceContractView.getTenderView().getTenderId()!=null)
                hdnTenderId = serviceContractView.getTenderView().getTenderId().toString();
			   }
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,serviceContractView.getRentAmount());
				viewRootMap.put(CONTRACTOR_INFO ,serviceContractView.getContractorView());
				contractor= serviceContractView.getContractorView();
				hdnContractorId  = serviceContractView.getContractorView().getPersonId().toString();
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_START_DATE,serviceContractView.getStartDate());
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_END_DATE,serviceContractView.getEndDate());
				contractCreatedOn = dateFormat.format(serviceContractView.getCreatedOn());
				contractCreatedBy = serviceContractView.getCreatedBy();
				if(serviceContractView.getBankGuaranteeValue() !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT,serviceContractView.getBankGuaranteeValue() );
				viewRootMap.put(WebConstants.ContractDetailsTab.BANK_ID,serviceContractView.getBankView().getBankId() );
		 	    viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_NUM,serviceContractView.getContractNumber());
		 	    contractor = getContractorInfo();
			    populateContractorInfoInTab(); 
			    
		 	   
			}
		     
		
		logger.logInfo(methodName+"|"+"Finish..");
	}
	@SuppressWarnings("unchecked")
	protected void populateTenderInfoInTab()
	{
		TenderView tv =getTenderView();
		//if tender is selected and contract has not been saved
		if((contractId == null || contractId.trim().length()<=0) && hdnTenderId != null && hdnTenderId.trim().length()>0 )
		{		
		  viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_ID,tv.getTenderId());
			viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,tv.getTenderNumber());
			if(tv.getRemarks()!=null && tv.getRemarks().length()>0)
			  viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_DESC,tv.getRemarks());
			if(tv.getTenderWonProposalAmount()!=null)
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,tv.getTenderWonProposalAmount());
			hdnContractorId  = tv.getWinnerContractor().getPersonId().toString();
			
		}
		
	}
	@SuppressWarnings("unchecked")
	public void deleteTenderRelatedInfo()
	{
		String methodName="deleteTenderRelatedInfo";
		try
		{
			logger.logInfo(methodName+"|Start");
		hdnTenderId="";
		hdnContractorId="";
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_DESC,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,"");
		viewRootMap.remove(CONTRACTOR_INFO);
		viewRootMap.remove(TENDER_INFO);
		viewRootMap.remove(WebConstants.Tender.SCOPE_OF_WORK_LIST);
		viewRootMap.remove(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_ID,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM,"");
		logger.logInfo(methodName+"|Finish");
		}catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured", e);
		}
		
	}
	@SuppressWarnings("unchecked")
	protected void populateContractorInfoInTab()
	{
		
		contractor = getContractorFromViewRoot();
		if(contractor !=null)
		{
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_ID,contractor.getPersonId());
			if(getIsEnglishLocale() )
			{
			    if(contractor.getContractorNameEn()!=null)		
			      viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,contractor.getContractorNameEn());
			}
			else if(getIsArabicLocale())
			{
				if(contractor.getContractorNameAr()!=null)
				  viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,contractor.getContractorNameAr());
			}
			if(contractor.getContractorNumber()!=null)
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM,contractor.getContractorNumber());
		}
		
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public void tabPaymentTerms_Click()
	{
		String methodName ="tabPaymentTerms_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(this.contractId!=null && this.contractId.trim().length()>0 &&
					!viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
			{
				getPaymentScheduleByContractId();
			}
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	private void getPaymentScheduleByContractId() throws PimsBusinessException {
		PropertyServiceAgent psa= new PropertyServiceAgent();
		List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(new Long(contractId),null);
		viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,paymentScheduleViewList);
	}
	public void tabWorkScope_Click()
	{
		String methodName ="tabWorkScope_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(!viewRootMap.containsKey(WebConstants.Tender.SCOPE_OF_WORK_LIST) ||  
					((ArrayList<TenderWorkScopeView>)viewRootMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST)).size()<=0 		
			)
			{
			  if((contractId == null || contractId.trim().length()<=0) && hdnTenderId != null && hdnTenderId.trim().length()>0 )
	    		  getContractWorkScopeByTenderId();
	    	  else if(contractId != null && contractId.trim().length()>0 )
	    		  getContractWorkScopeByContractId();
			}
    	  
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}
	public ContractorView getContractorFromViewRoot()
	{
		if(viewRootMap.get(CONTRACTOR_INFO)!=null)
			return (ContractorView)viewRootMap.get(CONTRACTOR_INFO);
		else
			return null;
		
	}
	public ServiceContractView getServiceContractFromViewRoot()
	{
		if(viewRootMap.get(CONTRACT_VIEW)!=null)
			return (ServiceContractView)viewRootMap.get(CONTRACT_VIEW);
		else
			return null;
		
	}
	public RequestView getRequestViewFromViewRoot()
	{
		if(viewRootMap.get(REQUEST_VIEW)!=null)
			return (RequestView)viewRootMap.get(REQUEST_VIEW);
		else
			return null;
		
	}
	public void tabPropertyDetails_Click()
	{
		String methodName ="tabPropertyDetails_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(!viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST) ||  
					((ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)).size()<=0 		
			)
			{
			  if((contractId == null || contractId.trim().length()<=0) && hdnTenderId != null && hdnTenderId.trim().length()>0 )
    		    getContractPropertyByTenderId();
    	      else if(contractId != null && contractId.trim().length()>0 )
    		    getContractPropertyByContractId();
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
	}
	public void tabAuditTrail_Click()
	{
		String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	errorMessages = new ArrayList<String>(0);
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(contractId!=null && contractId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
		
	}
	
	public void btnReviewReq_Click()
	{
		String methodName ="btnReviewReq_Click";
		logger.logInfo(methodName+"|"+"Start..");
		successMessages= new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			 
			if(txtRemarks!=null && txtRemarks.trim().length()>0)
			{
				if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();
				setTaskOutCome(TaskOutcome.LEGAL_DEPT_REVIEW);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				saveCommensAttachment(MessageConstants.ContractEvents.REVIEW_REQUIRED_LEGAL);
				txtRemarks ="";
				getContractById(this.contractId);
				viewRootMap.put(Page_Mode.PAGE_MODE , Page_Mode.LEGAL_REVIEW_REQUIRED);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_SENT_FOR_REVIEW));
			}
			else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnApprove_Click()
	{
		String methodName ="btnApprove_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.APPROVE);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_APPROVED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.COMPLETE);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_APPROVED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReject_Click()
	{
		String methodName ="btnReject_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.REJECT);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_REJECTED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REJECED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReview_Click()
	{
		String methodName ="btnReview_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			   if(txtRemarks!=null && txtRemarks.trim().length()>0)
			   {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					 getIncompleteRequestTasks();
				   setTaskOutCome(TaskOutcome.OK);
				   viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				   saveCommensAttachment(MessageConstants.ContractEvents.REVIEWED_LEGAL);
				   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.APPROVAL_REQUIRED);
				   txtRemarks ="";
				   getContractById(this.contractId);
				   successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REVIEWED_SUCCESSFULLY));
			   }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	  
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	
	public void btnComplete_Click()
	{
		String methodName ="btnComplete_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.COMPLETE);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_ACTIVATED);
			    	viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ACTIVE);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_COMPLETED_SUCCESSFULLY));
			    }
			    	
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnPrint_Click()
	{
		String methodName ="btnPrint_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_PRINTED);
			        txtRemarks ="";
			        successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_PRINTED_SUCCESSFULLY));
			    }
				else
				  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	protected boolean getIsContractValueandRentAmountEqual()
    {
    	String methodName="getIsContractValueandRentAmountEqual";
    	logger.logInfo(methodName+"|"+"Start");
    	boolean isEqual=false;
    	Double totalContractValue = new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());
    	if(totalContractValue!=null && totalContractValue>0D)
    	{
    	  Double contractValue=new Double(totalContractValue);	
    		if(contractValue.compareTo(getSumofAllRentAmounts())==0)
    			isEqual=true;
    	}
    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
    	logger.logInfo(methodName+"|"+"Finish");
    	return isEqual;
    }
    protected Double getSumofAllRentAmounts()
    {
    	String methodName="getSumofAllRentAmounts";
    	logger.logInfo(methodName+"|"+"Start");
    	Double rentAmount=new Double(0);
    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
    	
    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				//If payment Schedule is for the rent 
				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&& 
						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0)
				   rentAmount+=paymentScheduleView.getAmount();
			}
		    
        }
    	DecimalFormat df=new DecimalFormat();
    	
    	//rentAmount=commonUtil.getDoubleValue(rentAmount.toString());
    	
    	logger.logInfo(methodName+"|"+"Finish");
    	return Math.ceil(rentAmount);
    	
    }
    
	private void saveCommensAttachment(String eventDesc) throws Exception {
		String methodName ="saveCommensAttachment";
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		 if(requestView != null && requestView.getRequestId()!=null)
		 {
		     saveComments(requestView.getRequestId());
			 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
			 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
			 saveAttachments(requestView.getRequestId().toString());
			 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		
		 }
		saveSystemComments(eventDesc);
	}

	
	public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
		return tabPaymentTerms;
	}

	public void setTabPaymentTerms(
			org.richfaces.component.html.HtmlTab tabPaymentTerms) {
		this.tabPaymentTerms = tabPaymentTerms;
	}
	public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
		return tabAuditTrail;
	}
	
	
	public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
		this.tabAuditTrail = tabAuditTrail;
	}
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;
		
		return isViewModePopup;
	}

	public String getPageTitle() {
		if(viewRootMap.containsKey("pageTitle") && viewRootMap.get("pageTitle")!=null)
			pageTitle = viewRootMap.get("pageTitle").toString();
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
		if(this.pageTitle !=null)
			viewRootMap.put("pageTitle",this.pageTitle );
			
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public DomainDataView getDdContractType() {
		return ddContractType;
	}

	public void setDdContractType(DomainDataView ddContractType) {
		this.ddContractType = ddContractType;
	}

	public String getPageMode() {
		if(viewRootMap.get(Page_Mode.PAGE_MODE)!=null)
			pageMode =viewRootMap.get(Page_Mode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getContractCreatedOn() {
		return contractCreatedOn;
	}

	public void setContractCreatedOn(String contractCreatedOn) {
		this.contractCreatedOn = contractCreatedOn;
	}

	public String getContractCreatedBy() {
		return contractCreatedBy;
	}

	public void setContractCreatedBy(String contractCreatedBy) {
		this.contractCreatedBy = contractCreatedBy;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnReview() {
		return btnReview;
	}

	public void setBtnReview(HtmlCommandButton btnReview) {
		this.btnReview = btnReview;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlCommandButton getBtnReviewReq() {
		return btnReviewReq;
	}

	public void setBtnReviewReq(HtmlCommandButton btnReviewReq) {
		this.btnReviewReq = btnReviewReq;
	}
	
	
	protected void PageModeEdit()
	{
		
		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls("");
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.EDIT);
		tbl_Action.setRendered(false);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		
	}
	protected void PageModeApprovalRequired()
	{
		
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.APPROVAL_REQUIRED);

		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.enableDisableControls(ServiceContractPropertyDetailsTab.Page_Mode.APPROVAL_REQUIRED);
		scpd.getLnkDelPropUnit().setRendered(false);
		SetUpWorkers suw =(SetUpWorkers)getBean("pages$setUpWorkers");
		suw.enableDisableControls(SetUpWorkers.Page_Mode.APPROVAL_REQUIRED);
		

		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		
		
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		btnReviewReq.setRendered(true);
		btnReview.setRendered(false);
		tbl_Action.setRendered(true);
		
	}
	protected void PageModeLegalReviewRequired()
	{

		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.LEGAL_REVIEW_REQUIRED);
		
		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.enableDisableControls(ServiceContractPropertyDetailsTab.Page_Mode.LEGAL_REVIEW_REQUIRED);
		
		SetUpWorkers suw =(SetUpWorkers)getBean("pages$setUpWorkers");
		suw.enableDisableControls(SetUpWorkers.Page_Mode.LEGAL_REVIEW_REQUIRED);
		

		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(true);
		tbl_Action.setRendered(true);
	}
	protected void PageModeComplete()
	{
		
		
		tbl_Action.setRendered(true);
		btnComplete.setRendered(true);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.COMPLETE);
		
		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.enableDisableControls(ServiceContractPropertyDetailsTab.Page_Mode.COMPLETE);
		
		SetUpWorkers suw =(SetUpWorkers)getBean("pages$setUpWorkers");
		suw.enableDisableControls(SetUpWorkers.Page_Mode.COMPLETE);
		
		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		
	}
	protected void PageModeActive()
	{
		
		
		btnComplete.setRendered(false);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.ACTIVE);
		
		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.enableDisableControls(ServiceContractPropertyDetailsTab.Page_Mode.ACTIVE);
		
		SetUpWorkers suw =(SetUpWorkers)getBean("pages$setUpWorkers");
		suw.enableDisableControls(SetUpWorkers.Page_Mode.ACTIVE);
		
		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		
		ScopeOfWork sw =(ScopeOfWork)getBean("pages$ScopeOfWork");
		sw.getWorkTypeMenu().setDisabled(true);
		sw.getScopeWorkDescription().setDisabled(true);
		sw.getSaveScopeOfWork().setRendered(false);
		sw.getImgdeleteLink().setRendered(false);
		
	}
	protected void PageModeView()
	{
		
		ServiceContractDetailsTab scdt= (ServiceContractDetailsTab )getBean("pages$ServiceContractDetailsTab");
		scdt.enableDisableControls(ServiceContractDetailsTab.Page_Mode.VIEW);

		ServiceContractPropertyDetailsTab scpd=(ServiceContractPropertyDetailsTab)getBean("pages$ServiceContractPropertyDetails");
		scpd.enableDisableControls(ServiceContractPropertyDetailsTab.Page_Mode.VIEW);
		
		SetUpWorkers suw =(SetUpWorkers)getBean("pages$setUpWorkers");
		suw.enableDisableControls(SetUpWorkers.Page_Mode.VIEW);
		
		ServiceContractPaymentSchTab paymentSchTab=(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		paymentSchTab.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		
		ScopeOfWork sw =(ScopeOfWork)getBean("pages$ScopeOfWork");
		sw.getWorkTypeMenu().setRendered(false);
		sw.getScopeWorkDescription().setRendered(false);
		sw.getSaveScopeOfWork().setRendered(false);
		sw.getImgdeleteLink().setRendered(false);
		
		
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	
	


	
}
