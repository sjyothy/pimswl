package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.ui.util.ResourceUtil;

public class FileNetDocumentsPopup extends AbstractMemsBean
{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	List<DocumentView> list = new ArrayList<DocumentView>();
	private HtmlDataTable dataTable;
	String externalId;
	String procedureKey;
	String associatedObjectId;
	public FileNetDocumentsPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (PimsBusinessException exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			if(  exception.getExceptionCode().compareTo(ExceptionCodes.FILE_NET_CONFIGURATION_NOT_PRESENT) == 0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("attachment.msg.fileNetConfigurationNotSpecified") );	
			}
			else
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException,Exception
	{
		getQueryString();
		if( viewMap.get( "associatedObjectId" ) != null )
		{
			externalId = viewMap.get( "associatedObjectId" ).toString();
		}
		if( viewMap.get( "externalId" ) != null )
		{
			externalId = viewMap.get( "externalId" ).toString();
		}
		if( viewMap.get( WebConstants.Attachment.PROCEDURE_KEY ) != null )
		{
			procedureKey = viewMap.get( WebConstants.Attachment.PROCEDURE_KEY ).toString();
		}
		if( viewMap.get( "list") == null  )
		{
			list = UtilityService.getArchivedFileNetDocuments(  externalId, associatedObjectId, procedureKey);
			
		}
		else
		{
			list = (ArrayList<DocumentView>)viewMap.get( "list");
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	public String download()
	{
		try 
		{
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			DocumentView downloadFile = (DocumentView) dataTable.getRowData();
			sessionMap.put("fileNetDocumentId", downloadFile.getFileNetDocId());
	     } 
		catch (Exception e) 
	     {			
	    	logger.LogException(" download--- CRASHED --- ", e );
	    	errorMessages = new ArrayList<String>();
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    	 
		}
		return "DownloadFile";
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void getQueryString() throws Exception
	{
		if( request.getParameter("associatedObjectId") != null || sessionMap.get("associatedObjectId") != null)
		{
			if( request.getParameter("associatedObjectId") != null)
			{
				associatedObjectId =  request.getParameter("associatedObjectId").toString();
			}
			
			else if ( sessionMap.get("associatedObjectId") != null  )
			{
				associatedObjectId = sessionMap.remove("associatedObjectId").toString(); 
			}
			viewMap.put("associatedObjectId", associatedObjectId);
		}
		if( request.getParameter("externalId") != null || sessionMap.get("externalId") != null)
		{
			if( request.getParameter("externalId") != null)
			{
				externalId =  request.getParameter("externalId").toString();
			}
			
			else if ( sessionMap.get("externalId") != null  )
			{
				externalId = sessionMap.remove("externalId").toString(); 
			}
			viewMap.put("externalId", externalId);
		}
		if( request.getParameter(WebConstants.Attachment.PROCEDURE_KEY) != null || sessionMap.get(WebConstants.Attachment.PROCEDURE_KEY) != null)
		{
			if( request.getParameter(WebConstants.Attachment.PROCEDURE_KEY) != null)
			{
				procedureKey =  request.getParameter(WebConstants.Attachment.PROCEDURE_KEY).toString();
			}
			
			else if ( sessionMap.get(WebConstants.Attachment.PROCEDURE_KEY) != null  )
			{
				procedureKey = sessionMap.remove(WebConstants.Attachment.PROCEDURE_KEY).toString(); 
			}
			viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, procedureKey);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
		if( externalId != null )
		{
			viewMap.put( "externalId", externalId);
		}
		if( associatedObjectId != null )
		{
			viewMap.put( "associatedObjectId", associatedObjectId);
		}
		if( procedureKey != null )
		{
			viewMap.put( WebConstants.Attachment.PROCEDURE_KEY, procedureKey);
		}
	}

	public List<DocumentView> getList() {
		if( list!= null )
		{
			recordSize = list.size() ;
		}
		else
		{
			recordSize = 0;
		}
		return list;
		
	}

	public void setList(List<DocumentView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getAssociatedObjectId() {
		return associatedObjectId;
	}

	public void setAssociatedObjectId(String associatedObjectId) {
		this.associatedObjectId = associatedObjectId;
	}
    

}
