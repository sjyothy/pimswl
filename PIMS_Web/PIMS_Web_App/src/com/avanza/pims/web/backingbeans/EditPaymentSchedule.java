package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PaymentTypeView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.ui.util.ResourceUtil;




public class EditPaymentSchedule  extends AbstractController
{
     /**
	 * 
	 */
	private static final long serialVersionUID = 6100425815097470767L;


	public interface Keys {
		
		public static final String MUTIPLE_PAYMENT_DUE_ON="MUTIPLE_PAYMENTD_DUE_ON";
		public static final String PROJECT_ID="PROJECT_ID";
					
	}
	private DomainDataView ddExempted = null;
	private DomainDataView ddPending = null;
	public ResourceBundle bundle;
	String dateformat="";
	public String selectedPaymentMode;
	public String selectedPaymentStatus;
	public String selectedPaymentType;
	public String paymentModeRefNum;
	public Date paymentDueOn;
	public String paymentDate;
	public String bankNum;
	public String amount;
	public String totalContractValue;
	public String description;
	public boolean isPaymentReceived;
	public String paymentReferenceNumber;
	private String txtCompletionPercentage;
	private String selectedMileStone;
    private Boolean exempted = false ; 
    private String  reason;
    private boolean isAmountEditable;
	private List<String> errorMessages;
	@SuppressWarnings( "unchecked" )
	List<SelectItem> mileStoneList = new ArrayList<SelectItem>();
	@SuppressWarnings( "unchecked" )
	List<SelectItem> paymentModeList = new ArrayList<SelectItem>();
	@SuppressWarnings( "unchecked" )
	List<SelectItem> paymentStatusList = new ArrayList<SelectItem>();
	@SuppressWarnings( "unchecked" )
	List<SelectItem> paymentTypeList = new ArrayList<SelectItem>();
	
	String PROJECT_MILE_STONE_LIST="PROJECT_MILE_STONE_LIST";
	HtmlSelectOneMenu cmbMileStone = new HtmlSelectOneMenu();
	
	private boolean isArabicLocale = false;
	private static Logger logger = Logger.getLogger( EditPaymentSchedule.class );
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings( "unchecked" )
	Map sessionMap=context.getExternalContext().getSessionMap();;
	@SuppressWarnings( "unchecked" )
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	HttpServletRequest request =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	private boolean systemPayment;
	



	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		try
		{
	    	logger.logInfo( "init|Start");
	    	super.init();
	    	if(!isPostBack())
	    	{
	    		
	    		loadFromSessionMapAndQueryStrings();
	    		loadCombos();
	    	}
	    	logger.logInfo( "init|Finish" );
		}
		catch( Exception e)
		{
			logger.LogException("init|Exception Occured::", e);
			errorMessages  = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		
		}

	 }
	@SuppressWarnings( "unchecked" )
	private void loadFromSessionMapAndQueryStrings() 
	{
		if(sessionMap.get("CONTRACT_END_DATE") != null){
			viewMap.put("CONTRACT_END_DATE", sessionMap.get("CONTRACT_END_DATE"));
			sessionMap.remove("CONTRACT_END_DATE");
		}
		
		if(sessionMap.get("STOP_AMOUNT_EDIT")!=null)
		{
			viewRootMap.put("STOP_AMOUNT_EDIT", (Boolean)sessionMap.get("STOP_AMOUNT_EDIT"));
			sessionMap.remove("STOP_AMOUNT_EDIT");
		}
			if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		{
			viewRootMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
					         sessionMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE ) );
			sessionMap.remove( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE );
		}
		if( sessionMap.containsKey( WebConstants.SAVE_EDITED_PS_DB  ) )
		{
			viewRootMap.put( WebConstants.SAVE_EDITED_PS_DB ,"true") ;
			sessionMap.remove( WebConstants.SAVE_EDITED_PS_DB  ); 
			
		}
		if(sessionMap.containsKey( WebConstants.PAYMENT_SCHDULE_EDIT ) )
		{
		   viewRootMap.put( WebConstants.PAYMENT_SCHDULE_EDIT, sessionMap.get( WebConstants.PAYMENT_SCHDULE_EDIT ) );
		   sessionMap.remove( WebConstants.PAYMENT_SCHDULE_EDIT );
		}
		
		if(sessionMap.containsKey( "viewMode" ))
		{
		   viewRootMap.put("viewMode", sessionMap.get( "viewMode" ));
		   sessionMap.remove( "viewMode" );
		}
		
		if( request.getParameter( Keys.PROJECT_ID )!=null )
		{
			viewRootMap.put( Keys.MUTIPLE_PAYMENT_DUE_ON, true );
			viewRootMap.put( Keys.PROJECT_ID, request.getParameter( Keys.PROJECT_ID ) );
		}
		if(sessionMap.containsKey("FROM_CANCEL_CONTRACT_BEAN"))
		{
			viewRootMap.put("FROM_CANCEL_CONTRACT_BEAN", true);
			sessionMap.remove("FROM_CANCEL_CONTRACT_BEAN");
		}
		if(sessionMap.containsKey("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION"))
		{
			viewRootMap.put("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION", true);
			sessionMap.remove("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION");
		}
	}

	@SuppressWarnings( "unchecked" )
	    public void prerender() 
	    {
			if(!isPostBack())   
	    	   loadData();
	    }


	
	// Constructors

	/** default constructor */
	public EditPaymentSchedule() 
	{
		
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	
		
	}
	private void loadCombos()
	{
	  String methodName="loadCombos";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  loadPaymentMode();
		  //loadPaymentType();
		  loadPaymentStatus();
		  if(viewRootMap.get(Keys.MUTIPLE_PAYMENT_DUE_ON)!=null)
			  loadProjectMileStoneList();
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	}
	
	  logger.logInfo(methodName +"|"+"Finish ");
	}
	@SuppressWarnings( "unchecked" )
	private void loadProjectMileStoneList() 
	{
		String methodName="loadProjectMileStoneList";
		  logger.logInfo(methodName +"|"+"Start ");
		  try
		  {
			  ProjectServiceAgent psa =new ProjectServiceAgent();
			  if(viewRootMap.get(PROJECT_MILE_STONE_LIST)==null)
			  {
			    List<ProjectMileStoneView> pmsList = (ArrayList)psa.
			                              getMileStonesByProjectId(new Long(
			                            		   viewRootMap.get(Keys.PROJECT_ID).toString()));
			    viewRootMap.put(PROJECT_MILE_STONE_LIST, pmsList);
			  }
			  
			  
		  }
		  catch (Exception e)
		  {
			logger.logError(methodName +"|"+"Exception Occured "+e);
		  }
		
		  logger.logInfo(methodName +"|"+"Finish ");
		
	}
	@SuppressWarnings( "unchecked" )
	private void loadPaymentStatus()
	{
	  String methodName="loadPaymentStatus";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  PropertyServiceAgent psa =new PropertyServiceAgent();
		  if(!sessionMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
		  {
		    List<DomainDataView>ddv= (ArrayList)psa.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_STATUS);
		    sessionMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, ddv);
		  }
		  
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}
	@SuppressWarnings( "unchecked" )
	private void loadPaymentMode()
	{
	    String methodName="loadPaymentMode";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  PropertyServiceAgent psa =new PropertyServiceAgent();
		  if(!sessionMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_MODE))
		  {
		    List<DomainDataView>ddv= (ArrayList<DomainDataView>)psa.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_MODE);
		    sessionMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_MODE, ddv);
		  }
		  
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	}
	@SuppressWarnings( "unchecked" )
	private void loadPaymentType()
	{
	  String methodName="loadPaymentType";
	  logger.logInfo(methodName +"|"+"Start ");
	  try
	  {
		  UtilityServiceAgent usa =new UtilityServiceAgent();
		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE))
		  {
		    List<PaymentTypeView>ddv= (ArrayList<PaymentTypeView>)usa.getAllPaymentTypes();
		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE, ddv);
		  }
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}
	private String getLoggedInUser() 
	{
		return CommonUtil.getLoggedInUser();
	}
   
    
	
   @SuppressWarnings( "unchecked" )
   private void loadData()
   {
	   String methodName="loadData";
	   logger.logInfo( methodName +"|"+"Start" );
	   PaymentScheduleView psv = ( PaymentScheduleView )viewRootMap.get( WebConstants.PAYMENT_SCHDULE_EDIT );
	   setDescription ( psv.getDescription() );
	   paymentReferenceNumber = psv.getPaymentNumber();
	   setAmount ( psv.getAmount().toString() );
	   if( psv.getPaymentDueOn()!=null )
		   paymentDueOn = psv.getPaymentDueOn();
	   if( psv.getProjectMileStoneId()!=null )
		   selectedMileStone  = psv.getProjectMileStoneId().toString();
	   if( psv.getCompletionPercentage()!=null )
		   txtCompletionPercentage = psv.getCompletionPercentage().toString();
	   if( psv.getTypeId() != null )
		   selectedPaymentType = psv.getTypeId().toString();
	   if( psv.getPaymentModeId() != null )
		   selectedPaymentMode = psv.getPaymentModeId().toString();
	   if( psv.getIsSystem()!= null && psv.getIsSystem().longValue() == 1L )
		   this.setSystemPayment( true );
	   if( psv.getComments() != null && psv.getComments().trim().length() > 0 )
	   {
		   this.reason = psv.getComments().trim();
	   }
	  List<DomainDataView> ddViewList = new ArrayList<DomainDataView>( 0 );
	   ddViewList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
	   DomainDataView ddStatus = CommonUtil.getDomainDataFromId( ddViewList, psv.getStatusId() );
	  if( ddStatus.getDataValue().compareTo( WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED )==0 )
		  this.setExempted( true );
	   logger.logInfo( methodName +"|"+"Finish" );
   }
	private boolean hasError() throws Exception
	{
	
		String methodName="hasError";
		errorMessages=new ArrayList<String>();
		logger.logInfo(methodName +"|"+"Start");
		try 
		{
				if(viewMap.get("CONTRACT_END_DATE") != null && paymentDueOn != null){
					Date contractEndDate = (Date) viewMap.get("CONTRACT_END_DATE");
					if(contractEndDate.before(paymentDueOn)){
						errorMessages.add(CommonUtil.getBundleMessage("leaseContractErrorMsg.payemtDueDate"));
						return  true;
					}
				}
				
				try
				{
					if( getAmount()!=null && !getAmount().trim().equals( "" ) )
                		new Double( getAmount() );		
					else
					{
						errorMessages.add( getBundle().getString( MessageConstants.PaymentSchedule.MSG_REQ_PAYMENT_AMOUNT ) );
						return  true;
					}
				}
				catch(Exception ex)
				{
					errorMessages.add( getBundle().getString( MessageConstants.PaymentSchedule.MSG_INVALID_RENT_VALUE ) );
					return true;
				}
				logger.logInfo( methodName +"|"+"IsSystem:%s|IsExempted:%s",this.isSystemPayment(), this.getExempted()   );
				if( this.isSystemPayment()  )
				{
					if( this.getExempted() && ( this.reason == null || this.reason.length() <= 0 ) )
					{
						errorMessages.add( getBundle().getString( "paymentSchedule.systemPaymentChangeReasonRequired" ) );
						return  true;
					}
				}
			logger.logInfo( methodName +"|"+"Finsih" );	
		}
		catch ( Exception e ) 
		{
              logger.LogException( methodName +"|"+"Exception Occured::",e );
              throw e;
              
		}
		
		
		return false;
	}
	
	
	public String getErrorMessages()
	{
		String messageList="";
		if ( ( errorMessages == null ) || ( errorMessages.size() == 0 ) ) 
			messageList = "";
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
					messageList += "<LI>"+ message+ " <br></br>" ;

			messageList = messageList + "</UL></B></FONT>\n";
		}
		return ( messageList );
	}
	public ResourceBundle getBundle() 
	{
        if(getIsArabicLocale())
              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");
        else
              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");
        return bundle;
    }

  public void setBundle(ResourceBundle bundle) 
  {
        this.bundle = bundle;
  }

    
	public String btnAdd_Click()
	{
		String methodName="btnAdd_Click|";
		try
		{
		   addNewItemsInSession();
		   
        }
        catch(Exception e)
        {
        	logger.LogException( methodName + "Exception Occured::", e);
        }
    	return "";
	}
	@SuppressWarnings( "unchecked" )
	private void addNewItemsInSession() throws Exception
	{
	   List<PaymentScheduleView> previousPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
	   List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
	   PaymentScheduleView paymentScheduleView=new PaymentScheduleView(); 
	   paymentScheduleView = ( PaymentScheduleView )viewRootMap.get( WebConstants.PAYMENT_SCHDULE_EDIT );
		 if( !hasError() )
		 {
           if( viewRootMap.containsKey( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE ) && 
        	   viewRootMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE ) !=null 
        	 )
           {
            previousPaymentScheduleViewList=( ArrayList<PaymentScheduleView> )viewRootMap.get( 
            		                                                                     WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE 
            		                                                                  ); 
           }
           //Adding new updated List to session
           for ( PaymentScheduleView paymentScheduleView2 : previousPaymentScheduleViewList ) 
           {
			
        	if(!viewRootMap.containsKey("FROM_CANCEL_CONTRACT_BEAN"))
        	{	  
        	   if( paymentScheduleView2.getRowId().compareTo( paymentScheduleView.getRowId() )== 0)
        	   {
        		   paymentScheduleView2.setAmount( new Double ( amount ) );
        		   if( description!=null && description.trim().length() > 0 )
        		     paymentScheduleView2.setDescription( description.trim() );
        		   paymentScheduleView2.setPaymentDueOn( paymentDueOn );
        		   paymentScheduleView2.setUpdatedBy( getLoggedInUser() );
       		       paymentScheduleView2.setUpdatedOn( new Date() );
        		   paymentScheduleView2.setPaymentModeId( new Long ( selectedPaymentMode ) );
        		   List<DomainDataView> ddViewList =(ArrayList<DomainDataView>)sessionMap.get( WebConstants.SESSION_PAYMENT_SCHEDULE_MODE );
        		   for (DomainDataView domainDataView : ddViewList) 
        		   {
					   if(domainDataView.getDomainDataId().compareTo( new Long ( selectedPaymentMode ) )==0)
					   {
							paymentScheduleView2.setPaymentModeEn( domainDataView.getDataDescEn() );
							paymentScheduleView2.setPaymentModeAr( domainDataView.getDataDescAr() );
							break;
					   }
				   }
        		   if( this.isSystemPayment() || this.isNonSystemPaymentExemptionAllowed() )
        		   {
        			   if( this.getExempted()  )
        			   {
	        			   ddViewList = new ArrayList<DomainDataView>( 0 );
	        			   ddViewList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
	        			   DomainDataView ddExempted = CommonUtil.getIdFromType( ddViewList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED );
	        			   paymentScheduleView2.setStatusId( ddExempted.getDomainDataId() );
	        			   paymentScheduleView2.setStatusAr( ddExempted.getDataDescAr() );
	        			   paymentScheduleView2.setStatusEn( ddExempted.getDataDescEn() );
        			   }
        			   paymentScheduleView2.setComments( this.reason.trim() );
        		   }
        		   if( viewRootMap.containsKey( WebConstants.SAVE_EDITED_PS_DB ) && paymentScheduleView2.getPaymentScheduleId()!= null )
        			   new PropertyService().editPaymentSchedule( paymentScheduleView2 );
        		   
        	   }
        	} 
        	
        	if( viewRootMap.containsKey("FROM_CANCEL_CONTRACT_BEAN") )
        	 {
        		
        	   if( paymentScheduleView2.getPaymentScheduleId() == paymentScheduleView.getPaymentScheduleId() )
         	   {
         		   paymentScheduleView2.setAmount( new Double ( amount ) );
         		   if( description!=null && description.trim().length() > 0 )
         		     paymentScheduleView2.setDescription( description.trim() );
         		   paymentScheduleView2.setPaymentDueOn( paymentDueOn );
         		   paymentScheduleView2.setUpdatedBy( getLoggedInUser() );
        		       paymentScheduleView2.setUpdatedOn( new Date() );
         		   paymentScheduleView2.setPaymentModeId( new Long ( selectedPaymentMode ) );
         		   List<DomainDataView> ddViewList =(ArrayList<DomainDataView>)sessionMap.get( WebConstants.SESSION_PAYMENT_SCHEDULE_MODE );
         		   for (DomainDataView domainDataView : ddViewList) 
         		   {
 					   if(domainDataView.getDomainDataId().compareTo( new Long ( selectedPaymentMode ) )==0)
 					   {
 							paymentScheduleView2.setPaymentModeEn( domainDataView.getDataDescEn() );
 							paymentScheduleView2.setPaymentModeAr( domainDataView.getDataDescAr() );
 							break;
 					   }
 				   }
         		   if( this.isSystemPayment() || this.isNonSystemPaymentExemptionAllowed() )
         		   {
         			   ddViewList = new ArrayList<DomainDataView>( 0 );
        			   ddViewList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
         			   if( this.getExempted()  )
         			   {
 	        			   DomainDataView ddExempted = CommonUtil.getIdFromType( ddViewList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED );
 	        			   paymentScheduleView2.setStatusId( ddExempted.getDomainDataId() );
 	        			   paymentScheduleView2.setStatusAr( ddExempted.getDataDescAr() );
 	        			   paymentScheduleView2.setStatusEn( ddExempted.getDataDescEn() );
         			   }
         			   else{
         				  ddExempted = CommonUtil.getIdFromType( ddViewList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED );
         				 ddPending = CommonUtil.getIdFromType( ddViewList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING );
         				  if(paymentScheduleView2.getStatusId().compareTo(ddExempted.getDomainDataId()) == 0){
         					 paymentScheduleView2.setStatusId( ddPending.getDomainDataId() );
   	        			   	 paymentScheduleView2.setStatusAr( ddPending.getDataDescAr() );
   	        			     paymentScheduleView2.setStatusEn( ddPending.getDataDescEn() );
         				  }
         			   }
         			   paymentScheduleView2.setComments( this.reason.trim() );
         		   }
         		  if(     viewRootMap.containsKey( WebConstants.SAVE_EDITED_PS_DB ) && 
         				  paymentScheduleView2.getPaymentScheduleId()!= null )
         		  {
       			   new PropertyService().editPaymentSchedule( paymentScheduleView2 );
         		  }
         		   
         	   }
        	 
        	 }
        		
        	
        	 newPaymentScheduleViewList.add( paymentScheduleView2 );	   
           }
           sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, newPaymentScheduleViewList );
           AddResource addResource = AddResourceFactory.getInstance( getFacesContext() );
           addResource.addInlineScriptAtPosition( getFacesContext(), 
        		                                  AddResource.HEADER_BEGIN, 
        		                                  "window.opener.document.forms[0].submit();window.close();"
        		                                 );
           
		 }
     }
	
	public String getSelectedPaymentMode() {
		return selectedPaymentMode;
	}


	public void setSelectedPaymentMode(String selectedPaymentMode) {
		this.selectedPaymentMode = selectedPaymentMode;
	}


	public String getSelectedPaymentStatus() {
		return selectedPaymentStatus;
	}


	public void setSelectedPaymentStatus(String selectedPaymentStatus) {
		this.selectedPaymentStatus = selectedPaymentStatus;
	}


	public String getSelectedPaymentType() {
		return selectedPaymentType;
	}


	public void setSelectedPaymentType(String selectedPaymentType) {
		this.selectedPaymentType = selectedPaymentType;
	}


	public String getPaymentModeRefNum() {
		return paymentModeRefNum;
	}


	public void setPaymentModeRefNum(String paymentModeRefNum) {
		this.paymentModeRefNum = paymentModeRefNum;
	}


	public Date getPaymentDueOn() {
		return paymentDueOn;
	}


	public void setPaymentDueOn(Date paymentDueOn) {
		this.paymentDueOn = paymentDueOn;
	}


	public String getPaymentDate() {
		return paymentDate;
	}


	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}


	public String getBankNum() {
		return bankNum;
	}


	public void setBankNum(String bankNum) {
		this.bankNum = bankNum;
	}


	public String getAmount() {
		
		if(viewRootMap.get("AMOUNT")!=null)
			amount = viewRootMap.get("AMOUNT").toString();
		
		return amount;
	}


	public void setAmount(String amount) {
		this.amount = amount;
		
		if(this.amount!=null)
			viewRootMap.put("AMOUNT", this.amount);
	}


	public boolean isPaymentReceived() {
		return isPaymentReceived;
	}


	public void setPaymentReceived(boolean isPaymentReceived) {
		this.isPaymentReceived = isPaymentReceived;
	}


	public List<SelectItem> getPaymentModeList() {
	 paymentModeList.clear();
	    if(!sessionMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_MODE))
	       loadPaymentMode();
	       
	       paymentModeList= getCombosData(WebConstants.SESSION_PAYMENT_SCHEDULE_MODE);
		   
	
		
		return paymentModeList;
	}


	public void setPaymentModeList(List<SelectItem> paymentModeList) {
		this.paymentModeList = paymentModeList;
	}


	public List<SelectItem> getPaymentStatusList() 
	{
	
	    paymentStatusList.clear();
	    if(!sessionMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
	       loadPaymentStatus();
	       
	       paymentStatusList= getCombosData(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
		   
	
		return paymentStatusList;
	}


	public void setPaymentStatusList(List<SelectItem> paymentStatusList) {
		this.paymentStatusList = paymentStatusList;
	}
	@SuppressWarnings( "unchecked" )
    private List<SelectItem> getCombosData(String sessionId)
    { 
          List<SelectItem> itemList=new ArrayList<SelectItem>();
             List<DomainDataView> domainDataViewList=(ArrayList<DomainDataView>)sessionMap.get(sessionId);
	       for(int i=0;i<domainDataViewList.size();i++)
	       {  
	         DomainDataView domainDataView=(DomainDataView)domainDataViewList.get(i);
	         SelectItem item =null; 
	         if(getIsEnglishLocale())
	          item=new SelectItem(domainDataView.getDomainDataId().toString(), domainDataView.getDataDescEn());  
	         else
	          item=new SelectItem(domainDataView.getDomainDataId().toString(), domainDataView.getDataDescAr());
	        
	           itemList.add(item);
	       } 
	   return itemList;
    
    }
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getPaymentTypeList() 
	{
	
	    paymentTypeList.clear();
	    if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE) || viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE)==null)
	       loadPaymentType();
	    List<PaymentTypeView> ptvList = (ArrayList<PaymentTypeView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE);
	    List<SelectItem> typeList = new ArrayList<SelectItem>(0);
	    for (PaymentTypeView paymentTypeView : ptvList) {
	    	SelectItem item =null; 
	         if(getIsEnglishLocale())
	          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn());
	         else 
		          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr());
		    typeList.add(item);   
	    }
	       paymentTypeList= typeList;
		   return paymentTypeList;
	}


	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}


	public String getPaymentReferenceNumber() {
		return paymentReferenceNumber;
	}


	public void setPaymentReferenceNumber(String paymentReferenceNumber) {
		this.paymentReferenceNumber = paymentReferenceNumber;
	}
	public String getLocale(){
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode();
		}

	public String getDateformat() {
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		dateformat= localeInfo.getDateFormat();
		return dateformat;
	}


	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}


	public String getTotalContractValue() {
		return totalContractValue;
	}


	public void setTotalContractValue(String totalContractValue) {
		this.totalContractValue = totalContractValue;
	}


	public String getDescription() {
		
		if(viewRootMap.get("DESCRIPTION")!=null)
			description = viewRootMap.get("DESCRIPTION").toString(); 
			
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
		
		if( this.description !=null )
			viewRootMap.put("DESCRIPTION", this.description);
	}


	public String getTxtCompletionPercentage() {
		return txtCompletionPercentage;
	}


	public void setTxtCompletionPercentage(String txtCompletionPercentage) {
		this.txtCompletionPercentage = txtCompletionPercentage;
	}


	public String getSelectedMileStone() {
		return selectedMileStone;
	}


	public void setSelectedMileStone(String selectedMileStone) {
		this.selectedMileStone = selectedMileStone;
	}

	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getMileStoneList() {
		SelectItem item ;
		mileStoneList.clear();
		List<ProjectMileStoneView>pmsList = (ArrayList<ProjectMileStoneView>)viewRootMap.get(PROJECT_MILE_STONE_LIST);
		for (ProjectMileStoneView projectMileStoneView : pmsList) {
	          item =    new SelectItem(projectMileStoneView.getProjectMileStoneId().toString(),
	        		                   projectMileStoneView.getMileStoneName());
	          item.setDescription("Completion %age"+projectMileStoneView.getCompletionPercentage());
	          mileStoneList.add(item);
		}
		return mileStoneList;
	}


	public void setMileStoneList(List<SelectItem> mileStoneList) {
		this.mileStoneList = mileStoneList;
	}


	public boolean getMultiplePaymentDueOn()
	{
	    boolean multiplePaymentDueOn=true;
		if(viewRootMap.get(Keys.MUTIPLE_PAYMENT_DUE_ON)!=null )
			multiplePaymentDueOn=true;
		else
			multiplePaymentDueOn=false;
		
		return multiplePaymentDueOn;
		
	}


	public boolean isReadonlyMode()
	{
		Boolean isReadonly = (Boolean) viewRootMap.get("viewMode");
		return isReadonly==null?false:isReadonly;
	}
	
	public boolean isCancelContractBean()
	{
		if(viewRootMap.containsKey("FROM_CANCEL_CONTRACT_BEAN"))
			return false;
		else
		   return true;	
	}
	@SuppressWarnings( "unchecked" )
	public boolean isSystemPayment() {
		if( viewRootMap.get(  "systemPayment" ) != null )
			systemPayment = (Boolean) viewRootMap.get(  "systemPayment" );
		return systemPayment;
		
	}

    @SuppressWarnings( "unchecked" )
	public void setSystemPayment(boolean systemPayment) {
		this.systemPayment = systemPayment;
		viewRootMap.put(  "systemPayment", this.systemPayment  );
	}





	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public Boolean getExempted() {
		return exempted;
	}


	public void setExempted(Boolean exempted) {
		this.exempted = exempted;
	}


	public boolean isAmountEditable()
	{
		if(viewRootMap.get("STOP_AMOUNT_EDIT")!=null && ((Boolean)viewRootMap.get("STOP_AMOUNT_EDIT")==true))
			isAmountEditable=false;
		else
			isAmountEditable=true;
		return isAmountEditable;
	}
	public boolean isNonSystemPaymentExemptionAllowed()
	{
		if(viewRootMap.get("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION")!=null && ( (Boolean)viewRootMap.get("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION")==true))
			return true;

		return false;
	}
	

	public void setAmountEditable(boolean isAmountEditable) {
		this.isAmountEditable = isAmountEditable;
	}	
	

}