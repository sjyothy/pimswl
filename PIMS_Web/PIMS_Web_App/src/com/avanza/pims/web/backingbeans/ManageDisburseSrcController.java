package com.avanza.pims.web.backingbeans;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import com.avanza.core.util.Logger;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisburseSrcService;
import com.avanza.pims.ws.vo.mems.DisbursementSourceView;
import com.avanza.ui.util.ResourceUtil;


/** The class acts as a backing bean for ManageDisbursementSrc.jsp view.
 * @author Farhan Muhammad Amin
 *
 */
@SuppressWarnings("unchecked")
public class ManageDisburseSrcController extends AbstractMemsBean {
	
	private static final long serialVersionUID = 1L;
	
	private transient Logger logger = Logger.getLogger(ManageDisburseSrcController.class);
	
	protected HtmlInputText sourceNameEn = new HtmlInputText();
	protected HtmlInputText sourceNameAr = new HtmlInputText();
	protected HtmlInputText grpNum = new HtmlInputText();
	protected HtmlInputText acctsPayCombCode = new HtmlInputText();
	protected HtmlInputTextarea description = new HtmlInputTextarea();
	
	private static final String EDIT_MODE 	= "editMode";
	private static final String ADD_MODE  	= "addMode";
	private static final String PAGE_MODE 	= "pageMode";
	private static final String EDIT_ENTITY = "entity";
	
	
	/** The init function is called by the JSF framework each time the page is accessed. 
	 * The ManageDisbursementSrc.jsp can have two different views depending upon the context
	 * in which it is being called/invoked. The views are
  	 * <p> 1. Add view, User can add new disbursement sources into the system.<p>
  	 * <p> 2. Edit view - User can update the edit able information about the disbursement source.</p>
  	 *  When init functions finds a DisbursementView object in the request attributes the page caches
  	 *  the object in its view root and displays the page in the edit mode.</br>
  	 *  When init functions cannot finds DisbursementView object in the request attributes the page
  	 *  is opened in the add mode.
  	 *  
  	 *  The init functions also stores appropriate flags in the view root to indicate the page mode.
	 * 
	 * @see com.avanza.pims.web.mems.AbstractMemsBean#init()
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		
		if( !isPostBack() ) {
			DisbursementSourceView srcView = null;
			if(getSessionMap().get(EDIT_ENTITY) != null)
			{
				srcView = (DisbursementSourceView) getSessionMap().remove(EDIT_ENTITY);
			}
			if ( null != srcView ) {
				
				viewMap.put(PAGE_MODE, EDIT_MODE);		
				viewMap.put(EDIT_ENTITY, srcView);
				
				this.sourceNameAr.setValue(srcView.getSrcNameAr());
				this.sourceNameEn.setValue(srcView.getSrcNameEn());
				this.description.setValue(srcView.getDescription());
				this.grpNum.setValue(srcView.getGrpNum());
				this.acctsPayCombCode.setValue( srcView.getAcctsPayCombCode() );
			}
			else {
				viewMap.put(PAGE_MODE, ADD_MODE);
			}								
		}
	}
	
	/** The primary responsibility of the function is to delegate the addition
	 *  of new disbursement source to the DisburseSrcService passing it the user provided input wrapped
	 *  into the  DisbursementSourceView object.
	 *  
	 *  <p> The function is called from <strong>manage</strong> when all the validation test are
	 *  passed successfully </p>
	 *  
	 *  @author Farhan Muhammad Amin
	 */
	private void add() {
		
		try {							
			DisbursementSourceView view = getUserInput();			
			DisburseSrcService service = new DisburseSrcService();
			service.addDisbursementSource(view);
			
			successMessages.add(CommonUtil.getBundleMessage(
							MessageConstants.CommonMessage.RECORD_SAVED));
			
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[add CRASHED]",ex);
		}
	}
	
	/** The primary responsibility of the function is to delegate the updation of the disbursement 
	 * sources to the DisburseSrcService passing it the updated value provided by the user wrapped 
	 * in the DisbursementSourceView object.
	 * 
	 * 	<p> The function is called from <strong>manage</strong> when all the validation test are
	 *  passed successfully </p>
	 * 
	 */
	private void update() {
		logger.logInfo("[update() .. STARTS]");
		
		try {			
			DisbursementSourceView view		 = (DisbursementSourceView) viewMap.get(EDIT_ENTITY);
			DisbursementSourceView userInput = getUserInput();
			
			userInput.setSrcId(view.getSrcId());			
			DisburseSrcService service = new DisburseSrcService();
			service.updateDisbursementSource(userInput);
			
			successMessages.add(ResourceUtil.getInstance().getProperty(
								MessageConstants.CommonMessage.RECORD_UPDATED));
		
		} catch(Exception ex) {
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException( "[update CRASHED]",ex);
	        ex.printStackTrace();										
		}
					
		logger.logInfo("[update() .. ENDS]");	
	}
	
	/** The function is an action associated with the "Add" or "Update" button. The flow of the
	 * function can be stated as.
	 * 	<p>1. The function first validates the input provided by the user. This phase doesn't 
	 * make any discrimination whether the page is in add or edit mode. </p>
	 *  <p>2. The function queries the view root for the current page mode <p>
	 *  <p>3. If the page is in add mode it calls <strong>add</strong> </p>
	 *  <p>4. If the page is in edit mode it calls <strong>update<strong> </p>
	 *   
	 *  @author Farhan Muhammad Amin
	 * 
	 */
	public void manage() {
				
		logger.logInfo("[addDisbursementSource() .. STARTS]");
		
		if( validate() ) {			
			if( getIsAddMode() ) {
				add();
			} else if ( getIsEditMode()) {
				update();
			} else {
				errorMessages.add(ResourceUtil.getInstance().getProperty(
								MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}					
		logger.logInfo("[addDisbursementSource() .. ENDS]");
	}
	
	public String onBack() {
		return "back";
	}
	
	/** The function performs the validations on the input values provided by the user.
	 * If any validation fails the function adds appropriate error messages.
	 *  
	 * @return if all the inputs are validated the function returns true else false.
	 * 
	 * @author Farhan Muhammad Amin 
	 */
	
	private boolean validate() {
		
		logger.logInfo("[validate() .. STARTS]");
		boolean bValid = true;
		
		if ( null == sourceNameEn.getValue() || 0 == sourceNameEn.getValue().toString().trim().length() ) {
			bValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(					
					MessageConstants.DisbursementSource.MSG_SRC_EN_REQUIRED));
		}
		
		if ( null == sourceNameAr.getValue() || 0 == sourceNameAr.getValue().toString().trim().length() ) {
			bValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(
					MessageConstants.DisbursementSource.MSG_SRC_AR_REQUIRED));
		}
		
		if ( null == grpNum.getValue() || 0 == grpNum.getValue().toString().trim().length() ) {
			bValid = false;
			errorMessages.add(CommonUtil.getBundleMessage(
					MessageConstants.DisbursementSource.MSG_SRC_GRP_REQUIRED));
		}
											
		logger.logInfo("[validate() .. ENDS]");
		return bValid;
	}
	
	/** The function reads the input values from the controls.
	 * @return user input values wrapped into the DisbursementSourceView object
	 * @author Farhan Muhammad Amin
	 */
	
	private DisbursementSourceView getUserInput() {
		DisbursementSourceView view = new DisbursementSourceView();			
		view.setGrpNum(this.grpNum.getValue().toString());
		view.setSrcNameEn(this.sourceNameEn.getValue().toString());
		view.setSrcNameAr(this.sourceNameAr.getValue().toString());
		view.setDescription(this.description.getValue().toString());
		view.setAcctsPayCombCode( this.acctsPayCombCode.getValue().toString());
		return view;
	}
		
	public HtmlInputText getSourceNameEn() {
		return sourceNameEn;
	}
	public void setSourceNameEn(HtmlInputText sourceNameEn) {
		this.sourceNameEn = sourceNameEn;
	}
	
	public HtmlInputText getSourceNameAr() {
		return sourceNameAr;
	}
	
	public void setSourceNameAr(HtmlInputText sourceNameAr) {
		this.sourceNameAr = sourceNameAr;
	}
	
	public HtmlInputText getGrpNum() {
		return grpNum;
	}
	
	public void setGrpNum(HtmlInputText grpNum) {
		this.grpNum = grpNum;
	}
	
	public HtmlInputTextarea getDescription() {
		return description;
	}
	
	public void setDescription(HtmlInputTextarea description) {
		this.description = description;
	}	
	
	/** The function queries whether the page is in edit mode 
	 * @return true if the page is in edit mode else false.
	 */
	public boolean getIsEditMode() {
		String pageMode = (String) viewMap.get(PAGE_MODE);
		return pageMode.equalsIgnoreCase(EDIT_MODE);
	}
	
	
	/** The function queries the view root to know whether the page is in add mode or not
	 * @return true if the page is in add mode else false.
	 */
	public boolean getIsAddMode() {
		String pageMode = (String) viewMap.get(PAGE_MODE);
		return pageMode.equalsIgnoreCase(ADD_MODE);
	}

	public HtmlInputText getAcctsPayCombCode() {
		return acctsPayCombCode;
	}

	public void setAcctsPayCombCode(HtmlInputText acctsPayCombCode) {
		this.acctsPayCombCode = acctsPayCombCode;
	}
	
}
