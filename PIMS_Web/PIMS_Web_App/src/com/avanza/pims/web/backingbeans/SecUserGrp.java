package com.avanza.pims.web.backingbeans;

import java.io.Serializable;

import com.avanza.core.security.UserGroup;

public class SecUserGrp implements Serializable
{
	private String userGroupId;
	private UserGroup userGroup;
	private boolean isAssignedToUser ;
	
	public String getUserGroupId() {
		return userGroupId;
	}
	public void setUserGroupId(String userGroupId) {
		this.userGroupId = userGroupId;
	}
	public UserGroup getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
	public boolean getIsAssignedToUser() {
		return isAssignedToUser;
	}
	public void setIsAssignedToUser(boolean isAssignedToUser) {
		this.isAssignedToUser = isAssignedToUser;
	}
	
}
