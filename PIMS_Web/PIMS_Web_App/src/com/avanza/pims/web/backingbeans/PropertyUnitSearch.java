package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.*;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;

public class PropertyUnitSearch extends AbstractController
{
	private transient Logger logger = Logger.getLogger(PropertyUnitSearch.class);
	
	private HtmlDataTable dataTable;
	private String floorNumber;
	private String unitNumber;
	private String propertyNumber;
	private String buildingName;
	private String selectManyUnits;
    private Boolean isArabicLocale;	
    private Boolean isEnglishLocale;
	private String pageMode;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String unitId;
	private String createdBy;
	private String createdOn;
	
	private HtmlSelectOneMenu unitTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitUsageSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitInvestmentSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitSideSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu inquiryMethodSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu prioritySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitStatusSelectMenu= new HtmlSelectOneMenu(); 
	
	String selectOneUnitType;
	String selectOneUnitUsage;
	String selectOneUnitSide;
	String selectOneUnitStatus;
	String selectOneState;
	
	List<SelectItem> unitType= new ArrayList<SelectItem>();
	List<SelectItem> unitUsage= new ArrayList<SelectItem>();
	List<SelectItem> unitInvestment= new ArrayList<SelectItem>();
	List<SelectItem> unitSide= new ArrayList<SelectItem>();
	List<SelectItem> unitStatus= new ArrayList<SelectItem>();
	
	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	
	HashMap unitSearchMap = new HashMap ();
	
	UnitView unitView = new UnitView();
	String floorNumber1 = null;
	PropertyView propertyView = new PropertyView();
	ContactInfoView contactInfoView  = new ContactInfoView();

	List<SelectItem> city= new ArrayList();
	private List<SelectItem> state = new ArrayList<SelectItem>();
	List<SelectItem> country= new ArrayList();
	
	private List<String> errorMessages;
	private UnitView dataItem = new UnitView();
	private List<UnitView> dataList = new ArrayList<UnitView>();
	
	FacesContext context = FacesContext.getCurrentInstance();
    Map sessionMap;
	
     @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
		     if(!isPostBack())
		     {
		    	 loadCombos();
		     }
		 }
    	 catch(Exception exception){
	 		logger.LogException("loadStates() crashed ",exception);
		 }
	 }
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 	}
     
     private void loadStates(){
     try {
			logger.logInfo("loadStates() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			if(sessionMap.containsKey("states")){
				state = (ArrayList<SelectItem>)sessionMap.get("states");
			}
			else{
				state = new ArrayList<SelectItem>();
				PropertyServiceAgent psa = new PropertyServiceAgent();
				String regionName = "";
				List<RegionView> statesList = psa.getRegionViewsByRegionType(WebConstants.REGION_TYPE_STATE);
				for(RegionView stateView:statesList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
					else 
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
					state.add(item);
				}
					sessionMap.put("states",state);
			}
				
//			loadCities(vce);
			logger.logInfo("loadStates() completed successfully!!!");
		}catch(Exception exception){
 		logger.LogException("loadStates() crashed ",exception);
		}
 	}
     
     private void loadCombos() throws PimsBusinessException
     {
    	 loadStates();
     }
	 
	public List<UnitView> getUnitDataList()
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<UnitView>)viewMap.get("unitList");
		if(dataList==null)
			dataList = new ArrayList<UnitView>();
		return dataList;
	}

	public String doSearch(){
		try {
		    if(selectOneUnitType!=null && !selectOneUnitType.equals("")&& !selectOneUnitType.equals("-1") ){
		    	unitView.setUnitTypeId(new Long(selectOneUnitType));
		    }
		    if(selectOneUnitUsage !=null && !selectOneUnitUsage.equals("")&& !selectOneUnitUsage.equals("-1") ){
		    	unitView.setUsageTypeId(new Long(selectOneUnitUsage));
		    }
		   if(selectOneUnitSide!=null && !selectOneUnitSide.equals("")&& !selectOneUnitSide.equals("-1") ){
			   unitView.setUnitSideTypeId(new Long(selectOneUnitSide));
		    }
		   if(selectOneUnitStatus!=null && !selectOneUnitStatus.equals("")&& !selectOneUnitStatus.equals("-1") ){
			   unitView.setStatusId(new Long(selectOneUnitStatus));
		    }
		   if(selectOneState!=null && !selectOneState.equals("")&& !selectOneState.equals("-1") ){
			   contactInfoView.setStateId(new Long(selectOneState));
		    }
		   if (unitNumber !=null && !unitNumber.equals("")){
			   unitView.setUnitNumber(unitNumber);
		   }
		   if (buildingName !=null && !buildingName.equals("")){
			   propertyView.setCommercialName(buildingName);
		   }
		   if (propertyNumber !=null && !propertyNumber.equals("")){
			   propertyView.setPropertyNumber(propertyNumber);
		   }
		   if (floorNumber !=null && !floorNumber.equals("")){
			   floorNumber1 = floorNumber;
		   }
  	       dataList.clear();
		   loadDataList();
		}
	    catch (Exception e){
		    System.out.println("Exception doSearch"+e);       
	    }
	    return "search";
	}

	
	public List<UnitView> loadDataList() 
	{
		 String methodName="loadDataList";
		 List<UnitView> list = new ArrayList();
		    unitSearchMap.put("unit",unitView);
		    unitSearchMap.put("floor",floorNumber1);
		    unitSearchMap.put("property",propertyView);
		    unitSearchMap.put("contactInfo",contactInfoView);
		
		try
		{
			PropertyServiceAgent myPort = new PropertyServiceAgent();
	        // System.out.println("calling " + myPort.getEndpoint());
	        // list =  myPort.getPropertyInquiryUnit(inquiryView);
			list =  myPort.getPropertyUnit(unitSearchMap);
			if(list.isEmpty() || list==null)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));	
			}
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("unitList", list);
			if(list!=null)
				recordSize = list.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
	    }
		catch (Exception ex) 
		{
	        ex.printStackTrace();
	    }
		return list;
		
	}
	
	///////////screen mode methods///////////
	
	@SuppressWarnings("unchecked")
	public String addUnit(){
		logger.logInfo("addUnit() started...");
		try {
			 Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			 sessionMap.remove(WebConstants.UNIT_VIEW);
			 
			logger.logInfo("addUnit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("addUnit() crashed ", exception);
			errorMessages.clear();
			//TODO error msg to user
		}
		return "unitDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String editUnit(){
		logger.logInfo("editUnit() started...");
		try {
			 Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			 UnitView unitView=(UnitView)dataTable.getRowData();
			 
			 sessionMap.put(WebConstants.UNIT_VIEW,unitView);
			 
			logger.logInfo("editUnit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editUnit() crashed ", exception);
			errorMessages.clear();
			//TODO error msg to user
		}
		return "unitDetails";
	}
	
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the floorNumber
	 */
	public String getFloorNumber() {
		return floorNumber;
	}

	/**
	 * @param floorNumber the floorNumber to set
	 */
	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}

	/**
	 * @return the unitNumber
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * @param unitNumber the unitNumber to set
	 */
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	/**
	 * @return the propertyNumber
	 */
	public String getPropertyNumber() {
		return propertyNumber;
	}

	/**
	 * @param propertyNumber the propertyNumber to set
	 */
	public void setPropertyNumber(String propertyNumber) {
		this.propertyNumber = propertyNumber;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName() {
		return buildingName;
	}

	/**
	 * @param buildingName the buildingName to set
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	/**
	 * @return the selectManyUnits
	 */
	public String getSelectManyUnits() {
		return selectManyUnits;
	}

	/**
	 * @param selectManyUnits the selectManyUnits to set
	 */
	public void setSelectManyUnits(String selectManyUnits) {
		this.selectManyUnits = selectManyUnits;
	}

	/**
	 * @return the pageMode
	 */
	public String getPageMode() {
		return pageMode;
	}

	/**
	 * @param pageMode the pageMode to set
	 */
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	/**
	 * @return the unitId
	 */
	public String getUnitId() {
		return unitId;
	}

	/**
	 * @param unitId the unitId to set
	 */
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the unitTypeSelectMenu
	 */
	public HtmlSelectOneMenu getUnitTypeSelectMenu() {
		return unitTypeSelectMenu;
	}

	/**
	 * @param unitTypeSelectMenu the unitTypeSelectMenu to set
	 */
	public void setUnitTypeSelectMenu(HtmlSelectOneMenu unitTypeSelectMenu) {
		this.unitTypeSelectMenu = unitTypeSelectMenu;
	}

	/**
	 * @return the unitUsageSelectMenu
	 */
	public HtmlSelectOneMenu getUnitUsageSelectMenu() {
		return unitUsageSelectMenu;
	}

	/**
	 * @param unitUsageSelectMenu the unitUsageSelectMenu to set
	 */
	public void setUnitUsageSelectMenu(HtmlSelectOneMenu unitUsageSelectMenu) {
		this.unitUsageSelectMenu = unitUsageSelectMenu;
	}

	/**
	 * @return the unitInvestmentSelectMenu
	 */
	public HtmlSelectOneMenu getUnitInvestmentSelectMenu() {
		return unitInvestmentSelectMenu;
	}

	/**
	 * @param unitInvestmentSelectMenu the unitInvestmentSelectMenu to set
	 */
	public void setUnitInvestmentSelectMenu(
			HtmlSelectOneMenu unitInvestmentSelectMenu) {
		this.unitInvestmentSelectMenu = unitInvestmentSelectMenu;
	}

	/**
	 * @return the unitSideSelectMenu
	 */
	public HtmlSelectOneMenu getUnitSideSelectMenu() {
		return unitSideSelectMenu;
	}

	/**
	 * @param unitSideSelectMenu the unitSideSelectMenu to set
	 */
	public void setUnitSideSelectMenu(HtmlSelectOneMenu unitSideSelectMenu) {
		this.unitSideSelectMenu = unitSideSelectMenu;
	}

	/**
	 * @return the inquiryMethodSelectMenu
	 */
	public HtmlSelectOneMenu getInquiryMethodSelectMenu() {
		return inquiryMethodSelectMenu;
	}

	/**
	 * @param inquiryMethodSelectMenu the inquiryMethodSelectMenu to set
	 */
	public void setInquiryMethodSelectMenu(HtmlSelectOneMenu inquiryMethodSelectMenu) {
		this.inquiryMethodSelectMenu = inquiryMethodSelectMenu;
	}

	/**
	 * @return the prioritySelectMenu
	 */
	public HtmlSelectOneMenu getPrioritySelectMenu() {
		return prioritySelectMenu;
	}

	/**
	 * @param prioritySelectMenu the prioritySelectMenu to set
	 */
	public void setPrioritySelectMenu(HtmlSelectOneMenu prioritySelectMenu) {
		this.prioritySelectMenu = prioritySelectMenu;
	}

	/**
	 * @return the unitStatusSelectMenu
	 */
	public HtmlSelectOneMenu getUnitStatusSelectMenu() {
		return unitStatusSelectMenu;
	}

	/**
	 * @param unitStatusSelectMenu the unitStatusSelectMenu to set
	 */
	public void setUnitStatusSelectMenu(HtmlSelectOneMenu unitStatusSelectMenu) {
		this.unitStatusSelectMenu = unitStatusSelectMenu;
	}

	/**
	 * @return the selectOneUnitType
	 */
	public String getSelectOneUnitType() {
		return selectOneUnitType;
	}

	/**
	 * @param selectOneUnitType the selectOneUnitType to set
	 */
	public void setSelectOneUnitType(String selectOneUnitType) {
		this.selectOneUnitType = selectOneUnitType;
	}

	/**
	 * @return the selectOneUnitUsage
	 */
	public String getSelectOneUnitUsage() {
		return selectOneUnitUsage;
	}

	/**
	 * @param selectOneUnitUsage the selectOneUnitUsage to set
	 */
	public void setSelectOneUnitUsage(String selectOneUnitUsage) {
		this.selectOneUnitUsage = selectOneUnitUsage;
	}

	/**
	 * @return the selectOneUnitSide
	 */
	public String getSelectOneUnitSide() {
		return selectOneUnitSide;
	}

	/**
	 * @param selectOneUnitSide the selectOneUnitSide to set
	 */
	public void setSelectOneUnitSide(String selectOneUnitSide) {
		this.selectOneUnitSide = selectOneUnitSide;
	}

	/**
	 * @return the selectOneUnitStatus
	 */
	public String getSelectOneUnitStatus() {
		return selectOneUnitStatus;
	}

	/**
	 * @param selectOneUnitStatus the selectOneUnitStatus to set
	 */
	public void setSelectOneUnitStatus(String selectOneUnitStatus) {
		this.selectOneUnitStatus = selectOneUnitStatus;
	}

	/**
	 * @return the selectOneState
	 */
	public String getSelectOneState() {
		return selectOneState;
	}

	/**
	 * @param selectOneState the selectOneState to set
	 */
	public void setSelectOneState(String selectOneState) {
		this.selectOneState = selectOneState;
	}

	/**
	 * @return the unitType
	 */
	public List<SelectItem> getUnitType() {
		return unitType;
	}

	/**
	 * @param unitType the unitType to set
	 */
	public void setUnitType(List<SelectItem> unitType) {
		this.unitType = unitType;
	}

	/**
	 * @return the unitUsage
	 */
	public List<SelectItem> getUnitUsage() {
		return unitUsage;
	}

	/**
	 * @param unitUsage the unitUsage to set
	 */
	public void setUnitUsage(List<SelectItem> unitUsage) {
		this.unitUsage = unitUsage;
	}

	/**
	 * @return the unitInvestment
	 */
	public List<SelectItem> getUnitInvestment() {
		return unitInvestment;
	}

	/**
	 * @param unitInvestment the unitInvestment to set
	 */
	public void setUnitInvestment(List<SelectItem> unitInvestment) {
		this.unitInvestment = unitInvestment;
	}

	/**
	 * @return the unitSide
	 */
	public List<SelectItem> getUnitSide() {
		return unitSide;
	}

	/**
	 * @param unitSide the unitSide to set
	 */
	public void setUnitSide(List<SelectItem> unitSide) {
		this.unitSide = unitSide;
	}

	/**
	 * @return the unitStatus
	 */
	public List<SelectItem> getUnitStatus() {
		return unitStatus;
	}

	/**
	 * @param unitStatus the unitStatus to set
	 */
	public void setUnitStatus(List<SelectItem> unitStatus) {
		this.unitStatus = unitStatus;
	}

	/**
	 * @return the citySelectMenu
	 */
	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}

	/**
	 * @param citySelectMenu the citySelectMenu to set
	 */
	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}

	/**
	 * @return the stateSelectMenu
	 */
	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}

	/**
	 * @param stateSelectMenu the stateSelectMenu to set
	 */
	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}

	/**
	 * @return the countrySelectMenu
	 */
	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}

	/**
	 * @param countrySelectMenu the countrySelectMenu to set
	 */
	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}

	/**
	 * @return the unitSearchMap
	 */
	public HashMap getUnitSearchMap() {
		return unitSearchMap;
	}

	/**
	 * @param unitSearchMap the unitSearchMap to set
	 */
	public void setUnitSearchMap(HashMap unitSearchMap) {
		this.unitSearchMap = unitSearchMap;
	}

	/**
	 * @return the unitView
	 */
	public UnitView getUnitView() {
		return unitView;
	}

	/**
	 * @param unitView the unitView to set
	 */
	public void setUnitView(UnitView unitView) {
		this.unitView = unitView;
	}

	/**
	 * @return the floorNumber1
	 */
	public String getFloorNumber1() {
		return floorNumber1;
	}

	/**
	 * @param floorNumber1 the floorNumber1 to set
	 */
	public void setFloorNumber1(String floorNumber1) {
		this.floorNumber1 = floorNumber1;
	}

	/**
	 * @return the propertyView
	 */
	public PropertyView getPropertyView() {
		return propertyView;
	}

	/**
	 * @param propertyView the propertyView to set
	 */
	public void setPropertyView(PropertyView propertyView) {
		this.propertyView = propertyView;
	}

	/**
	 * @return the contactInfoView
	 */
	public ContactInfoView getContactInfoView() {
		return contactInfoView;
	}

	/**
	 * @param contactInfoView the contactInfoView to set
	 */
	public void setContactInfoView(ContactInfoView contactInfoView) {
		this.contactInfoView = contactInfoView;
	}

	/**
	 * @return the city
	 */
	public List<SelectItem> getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(List<SelectItem> city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public List<SelectItem> getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}

	/**
	 * @return the dataItem
	 */
	public UnitView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(UnitView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<UnitView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<UnitView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the context
	 */
	public FacesContext getContext() {
		return context;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(FacesContext context) {
		this.context = context;
	}

	/**
	 * @return the sessionMap
	 */
	public Map getSessionMap() {
		return sessionMap;
	}

	/**
	 * @param sessionMap the sessionMap to set
	 */
	public void setSessionMap(Map sessionMap) {
		this.sessionMap = sessionMap;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(List<SelectItem> state) {
		this.state = state;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public List<SelectItem> getState() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		state = (List<SelectItem>) sessionMap.get("states");
		if(state==null)
			state = new ArrayList<SelectItem>();
		return state;
	}

	public Boolean getIsArabicLocale()
	{
		return CommonUtil.getIsArabicLocale();
	}
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}

}
