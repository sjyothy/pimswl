package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;
import com.collaxa.cube.engine.deployment.Resource;

public class UnitDetails extends AbstractController {

	public class Messages {
		public static final String REQUIRED_NO_OF_UNITS = "generateUnits.messages.NoOfUnitsRequired";
		public static final String REQUIRED_UNIT_NUM_FROM = "generateUnits.messages.UnitNumFromRequired";
		public static final String REQUIRED_PROPERTY_FLOOR = "generateUnits.messages.floorRequired";	
		public static final String REQUIRED_UNIT_NUMBER_PREFIX = "generateUnits.messages.UnitNumberPrefixRequired";
		public static final String NO_OF_UNITS_POSITIVE_WHOLE_NUM = "generateUnits.messages.NoOfUnitsPositiveWholeNum";
		public static final String UNIT_NUM_FROM_POSITIVE_WHOLE_NUM = "generateUnits.messages.UnitNumFromPositiveWholeNum";
		public static final String UNITS_GENERATED_SUCCESSFULLY = "generateUnits.messages.UnitsGeneratedSuccess";
		public static final String UNITS_GENERATED_GENERAL_ERROR = "generateUnits.messages.generalError";
		public static final String UNITS_GENERATED_GENERAL_ALREADY_EXISTS = "generateUnits.messages.unitsAlreadyExists";
		
		public static final String PROVIDE_UNIT_NUMBER = "unitDetails.msgs.provideUnitNo";
		public static final String PROVIDE_UNIT_AREA = "unitDetails.msgs.provideUnitArea";
		public static final String PROVIDE_CORRECT_UNIT_AREA = "unitDetails.msgs.provideCorrectUnitArea";
		public static final String PROVIDE_RENT_VALUE = "unitDetails.msgs.provideRentVal";
		public static final String PROVIDE_CORRECT_RENT_VALUE = "unitDetails.msgs.provideCorrectRentVal";
		public static final String PROVIDE_OPENING_PRICE = "unitDetails.msgs.provideOpeningPrice";
		public static final String PROVIDE_CORRECT_OPENING_PRICE = "unitDetails.msgs.provideCorrectOpeningPrice";
		public static final String PROVIDE_GRACE_PERIOD = "unitDetails.msgs.provideGracePeriod";
		public static final String PROVIDE_CORRECT_GRACE_PERIOD = "unitDetails.msgs.provideCorrectGracePeriod";
		public static final String PROVIDE_NO_OF_BEDS = "unitDetails.msgs.provideNoOfBeds";
		public static final String PROVIDE_CORRECT_NO_OF_BEDS = "unitDetails.msgs.provideCorrectNoOfBeds";
		public static final String PROVIDE_NO_OF_BATHS = "unitDetails.msgs.provideNoOfBaths";
		public static final String PROVIDE_CORRECT_NO_OF_BATHS = "unitDetails.msgs.provideCorrectNoOfBaths";
		public static final String PROVIDE_NO_OF_LIVING_ROOMS = "unitDetails.msgs.provideNoOfLivingRooms";
		public static final String PROVIDE_CORRECT_NO_OF_LIVING_ROOMS = "unitDetails.msgs.provideCorrectNoOfLivingRooms";
		public static final String UNIT_ADDED = "unitDetails.msgs.unitAdded";
		public static final String UNIT_UPDATED = "unitDetails.msgs.unitUpdated";
		public static final String PROPERTY_NOT_SELECTED = "unitDetails.msgs.propNotSelected";
		
	}

	// Fields

	private transient Logger logger = Logger.getLogger(UnitDetails.class);
		
	private HtmlInputHidden hdnTextPropertyId = new HtmlInputHidden();
	private HtmlInputHidden hdnTextFloorId = new HtmlInputHidden();
	private HtmlInputHidden hdnTextUnitId = new HtmlInputHidden();
		
	private String floorId;
	private String floorType;
	private String rentProcessId;
	private HtmlCommandButton commandButtonSearch;	
	private List<String> messages;
	private List<String> errorMessages;
	private List<String> error_Messages;
	private List<String> error_Messages_ub;
	private List<SelectItem> propertyFloors;
	private UnitView unitView;
	private PropertyView property;
	private String unitArea;
	private String rentValue;
	private String requiredPrice;
	private String gracePeriod;
	private String noOfBed;
	private String noOfBath;
	private String noOfLiving;
	private static String ADD_MODE = "ADD_MODE";
	private Boolean isAddMode;
	
	
	// Constructor
	/** default constructor */
	public UnitDetails() {
	}

	// Methods

	public String getRentProcessTypeId() {
		
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getRentProcessId() != null)
		{
			val = unitView.getRentProcessId().toString();
		}
		else{
			val = "0";
		}
		return val;
	}	
	

	public void setRentProcessTypeId(String rentProcessTypeId) {
		
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setRentProcessId(Convert.toLong(rentProcessTypeId));
		}
	}

	public String getGracePeriodTypeId() {
		
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getGracePeriodTypeId() != null)
		{
			val = unitView.getGracePeriodTypeId().toString();
		}
		else{
			val = "0";
		}
		return val;
	}

	public void setGracePeriodTypeId(String gracePeriodTypeId) {
		
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setGracePeriodTypeId(Convert.toLong(gracePeriodTypeId));
		}
	}	
	
	public String getUnitSideTypeId() {
		
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getUnitSideTypeId() != null)
		{
			val = unitView.getUnitSideTypeId().toString();
		}
		else{
			return "0";
		}
		return val;
	}

	public void setUnitSideTypeId(String unitSideTypeId) {
		
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setUnitSideTypeId(Convert.toLong(unitSideTypeId));
		}
	}
	
	public String getInvestmentTypeId() {
		
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getInvestmentTypeId() != null)
		{
			val = unitView.getInvestmentTypeId().toString();
		}
		else{
			val = "0";
		}
		return val;
	}

	public void setInvestmentTypeId(String investmentTypeId) {
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setInvestmentTypeId(Convert.toLong(investmentTypeId));
		}
	}

	public String getUnitTypeId() {
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getUnitTypeId() != null)
		{
			val = unitView.getUnitTypeId().toString();
		}
		else{
			val = "0";
		}
		return val;
	}

	public void setUnitTypeId(String unitTypeId) {
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setUnitTypeId(Convert.toLong(unitTypeId));
		}
	}
	
	public String getUnitUsageTypeId() {
		
		UnitView unitView = getUnitView();
		String val = "";
		
		if(unitView.getUsageTypeId() != null)
		{
			val = unitView.getUsageTypeId().toString();
		}
		else{
			val = "0";
		}
		return val;
	}
	
	public String getRentProcessId(){
		UnitView unitView = getUnitView();
//		String val = "";
		
		if(unitView.getRentProcessId() != null)
		{
			rentProcessId = unitView.getRentProcessId().toString();
		}
		else{
			rentProcessId = "0";
		}
//		return val;
		return rentProcessId;
	}
	
	public void setRentProcessId(String rentProcessId) {
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setRentProcessId(Convert.toLong(rentProcessId));
		}		
	}
	
	public void setUnitUsageTypeId(String unitUsageTypeId) {
		UnitView unitView = getUnitView();
		if(unitView != null)
		{
			unitView.setUsageTypeId(Convert.toLong(unitUsageTypeId));
		}
	}
	
	public UnitView getUnitView() {
		
		UnitView unitView;
		
		//Map sessionMap =  getFacesContext().getExternalContext().getSessionMap();
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		
		if(viewRootMap.containsKey(WebConstants.UnitDetails.UnitDetails_UnitView))
		{
			unitView = (UnitView) viewRootMap.get(WebConstants.UnitDetails.UnitDetails_UnitView);
		}
		else
		{
			unitView = new UnitView();
			viewRootMap.put(WebConstants.UnitDetails.UnitDetails_UnitView,unitView);
		}	
		
		return unitView;
	}

	public void setUnitView(UnitView unitView) {
		this.unitView = unitView;
	}
	
	public List<SelectItem> getPropertyFloors() {
		
		logger.logInfo("in getPropertyFloors");
		if(propertyFloors == null)
			propertyFloors = new ArrayList<SelectItem>();
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object viewObjPropertyView = viewRootMap.get(WebConstants.UnitDetails.PROPERTY_VIEW);
		
		if (viewObjPropertyView != null){
			PropertyView propertyView = (PropertyView)viewObjPropertyView;
			Long propertyId = Convert.toLong(propertyView.getPropertyId());
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			try {
				logger.logInfo("trying to get property floors");
				ArrayList<FloorView> floors = null;
				
				Object viewObjFloorViewList = viewRootMap.get(WebConstants.UnitDetails.FLOOR_VIEW_LIST);
				if(viewObjFloorViewList != null){
					floors = (ArrayList<FloorView>)viewObjFloorViewList;
				}
				else{
					floors = psAgent.getPropertyFloors(propertyId);
					viewRootMap.put(WebConstants.UnitDetails.FLOOR_VIEW_LIST, floors);
				}
				logger.logInfo("property floors got");
				
				if (floors != null) {
					propertyFloors = new ArrayList<SelectItem>();
					for (FloorView fView : floors) {
						SelectItem item = new SelectItem(fView.getFloorId()
								.toString(), fView.getFloorNumber());
						propertyFloors.add(item);
					}
					setFloorTypeIdForSelectedFloor();
					//floorValueChange(new ValueChangeEvent(new HtmlSelectOneMenu(), "", ((FloorView)property.getFloorView().iterator().next()).getFloorId()));
				}				
			} catch (PimsBusinessException e) {
				logger.logError("getPropertyFloors crashed",e);				
			}
		}
		logger.logInfo("getPropertyFloors completed successfully");
		return propertyFloors;
	}

	private void setFloorTypeIdForSelectedFloor() throws PimsBusinessException{
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object mode = viewRootMap.get(ADD_MODE);
		
		if((boolean)mode.equals(true)){
			floorValueChange(new ValueChangeEvent(new HtmlSelectOneMenu(), "", ((FloorView)property.getFloorView().iterator().next()).getFloorId()));
		}
		else{
			UnitView uv = getUnitView();
			floorValueChange(new ValueChangeEvent(new HtmlSelectOneMenu(), "", uv.getFloorView().getFloorId()));
		}
	}

	private boolean isAddMode() {
		return false;
	}

	public void setPropertyFloors(List<SelectItem> propertyFloors) {
		this.propertyFloors = propertyFloors;
	}
	
	private String getLoggedInUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public boolean isValidPositiveLong(String val) {
		boolean isValidPositiveLong = true;

		try {
			Long lVal = Long.parseLong(val);
			if (lVal < 0)
				isValidPositiveLong = false;
		} catch (Exception exp) {
			isValidPositiveLong = false;
		}

		return isValidPositiveLong;
	}

	public boolean ValidateScreen() {
		
		String method = "ValidateScreen";
		logger.logInfo(method + " started...");
		boolean isValid = true;	
		UnitView uv = getUnitView();
		NumberFormat nf = new DecimalFormat(getNumberFormat());
		
		//VALIDATING PROPERTY SELECTED OR NOT
		boolean isValueBad = false;
		if(property == null || (property != null && property.getPropertyId() == null)){
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROPERTY_NOT_SELECTED));
			isValueBad = true;
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING UNIT NUMBER///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(uv.getUnitNumber() == null || uv.getUnitNumber().length() == 0){
			//writeErrorMessage("Plz provide Unit Number");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_UNIT_NUMBER));
			isValueBad = true;
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING UNIT AREA ///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getUnitArea() == null || (getUnitArea() != null && getUnitArea().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide Unit Area");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_UNIT_AREA));
		}
		else{
			try{
				Double.parseDouble(getUnitArea());
				//nf.parse(getUnitArea());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for Unit Area");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_UNIT_AREA));
			}
			catch (IllegalArgumentException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for Unit Area");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_UNIT_AREA));
			}
//			catch (ParseException e) {
//				isValueBad = true;
//				writeMessage("Plz provide correct value for Unit Area");
//			}
			
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING RENT VALUE///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getRentValue() == null || (getRentValue() != null && getRentValue().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide Rent Value");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_RENT_VALUE));
		}
		else{
			try{
				Double.parseDouble(getRentValue());
				//nf.parse(getRentValue());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct Rent Value");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_RENT_VALUE));
			}
			catch (IllegalArgumentException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct Rent Value");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_RENT_VALUE));
			}
//			catch (ParseException e) {
//				isValueBad = true;
//				writeMessage("Plz provide correct Rent Value");
//			}
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING OPENING PRICE///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getRequiredPrice() == null || (getRequiredPrice() != null && getRequiredPrice().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide Opening Price");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_OPENING_PRICE));
		}
		else{
			try{
				Double.parseDouble(getRequiredPrice());
				//nf.parse(getRequiredPrice());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for Opening Price");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_OPENING_PRICE));
			}
			catch (IllegalArgumentException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for Opening Price");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_OPENING_PRICE));
			}
//			catch (ParseException e) {
//				isValueBad = true;
//				writeMessage("Plz provide correct value for Opening Price");
//			}
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING GRACE PERIOD///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getGracePeriod() == null || (getGracePeriod() != null && getGracePeriod().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide Grace Period");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_GRACE_PERIOD));
		}
		else{
			try{
				Integer.parseInt(getGracePeriod());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for Grace Period");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_GRACE_PERIOD));
			}			
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING NO OF BED///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getNoOfBed() == null || (getNoOfBed() != null && getNoOfBed().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide No of Beds");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_NO_OF_BEDS));
		}
		else{
			try{
				Integer.parseInt(getNoOfBed());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for No of Beds");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_NO_OF_BEDS));
			}			
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING NO OF BATHROOMS///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getNoOfBath() == null || (getNoOfBath() != null && getNoOfBath().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide No of Bathrooms");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_NO_OF_BATHS));
		}
		else{
			try{
				Integer.parseInt(getNoOfBath());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for No of Bathrooms");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_NO_OF_BATHS));
			}			
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		//VALIDATING NO OF LIVING ROOMS///////////////////////////////////////////////////////////////
		isValueBad = false;
		if(getNoOfLiving() == null || (getNoOfLiving() != null && getNoOfLiving().length() == 0)){
			isValueBad = true;
			//writeErrorMessage("Plz provide No of Living Rooms");
			writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_NO_OF_LIVING_ROOMS));
		}
		else{
			try{
				Integer.parseInt(getNoOfLiving());
			}
			catch (NumberFormatException e) {
				isValueBad = true;
				//writeErrorMessage("Plz provide correct value for No of Living Rooms");
				writeErrorMessage(ResourceUtil.getInstance().getProperty(Messages.PROVIDE_CORRECT_NO_OF_LIVING_ROOMS));
			}			
		}
		if(isValueBad){			
			isValid = false;			
		}
		
		return isValid;
	}
	
	private void writeMessage(String message){
		if(messages == null)
			messages = new ArrayList<String>();
		messages.add(message);
	}
	
	private void writeErrorMessage(String message){
		if(errorMessages == null)
			errorMessages = new ArrayList<String>();
		errorMessages.add(message);
	}
	
	public String cancel(){
		return "cancel";
	}

	public String persistUnit() {
		
		String method = "persistUnit";
		logger.logInfo(method + " started...");
		try {			
			UnitView unitView = getUnitView();
			if(ValidateScreen())
			{
				PropertyServiceAgent psAgent = new PropertyServiceAgent();
				
				Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();				
				Object mode = viewRootMap.get(ADD_MODE);
				 
				if((boolean)mode.equals(true)){				
					ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();
					propertyUnitViews.add(getUnitViewToAdd(unitView));
					propertyUnitViews = psAgent.generateUnits(getPropertyId(), Long.parseLong(floorId),
							propertyUnitViews);
					logger.logDebug("Unit count returned:%s",propertyUnitViews.size());
					if(propertyUnitViews.size() == 0){					
						//writeMessage("Unit has been added successfully");					
						writeMessage(ResourceUtil.getInstance().getProperty(Messages.UNIT_ADDED));
					}
					else{
						writeMessage(ResourceUtil.getInstance().getProperty(Messages.UNITS_GENERATED_GENERAL_ALREADY_EXISTS));
					}
				}
				else{
					Long unitId = unitView.getUnitId();
					UnitView uV = getUnitViewToAdd(unitView);
					uV.setUnitId(unitId);
					psAgent.updateUnit(uV);
					//writeMessage("Unit has been updated successfully");
					writeMessage(ResourceUtil.getInstance().getProperty(Messages.UNIT_UPDATED));
				}				
			}			
			logger.logInfo(method+"completed");
		} catch (Exception ex) {
			logger.logError("Crashed while persisting Unit:", ex);
			ex.printStackTrace();
			error_Messages_ub.add(ResourceUtil.getInstance().getProperty(
					GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ERROR));
		}
		return "";
	}

	private Long getPropertyId() throws Exception{
		logger.logInfo("getPropertyId started...");
		try{
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object viewObj = viewRootMap.get(WebConstants.UnitDetails.PROPERTY_VIEW);
			PropertyView propertyView = null;
			if(viewObj != null){
				propertyView = (PropertyView)viewObj;
			}
			if(propertyView != null)
				return propertyView.getPropertyId();
			else
				throw new Exception();
		}
		catch (Exception e) {
			logger.LogException("getPropertyInfo crashed", e);
			throw new Exception();
		}		
	}

	private UnitView getUnitViewToAdd(UnitView unitView) throws Exception{
		UnitView localunitView = new UnitView();
		//////////////////////////////////
		try{
			NumberFormat nf = new DecimalFormat(getNumberFormat());
			localunitView.setCreatedBy(getLoggedInUser());
			localunitView.setCreatedOn(new Date());
			localunitView.setUpdatedBy(getLoggedInUser());
			localunitView.setUpdatedOn(new Date());
			localunitView.setIsDeleted(new Long(0));
			localunitView.setRecordStatus(new Long(1));
			localunitView.setUnitSideTypeId(unitView.getUnitSideTypeId());
			localunitView.setUnitNumber(unitView.getUnitNumber());
			localunitView.setInvestmentTypeId(unitView.getInvestmentTypeId());
			localunitView.setUnitTypeId(unitView.getUnitTypeId());
			localunitView.setUsageTypeId(unitView.getUsageTypeId());
			localunitView.setFloorId(Long.parseLong(floorId));
			localunitView.setRequiredPrice(Double.parseDouble(getRequiredPrice()));
			localunitView.setRentValue(Double.parseDouble(getRentValue()));
			localunitView.setUnitArea(Double.parseDouble(getUnitArea()));
			localunitView.setNoOfBed(Long.parseLong(getNoOfBed()));
			localunitView.setNoOfBath(Long.parseLong(getNoOfBath()));
			localunitView.setNoOfLiving(Long.parseLong(getNoOfLiving()));
			localunitView.setAccountNumber(unitView.getAccountNumber());
			localunitView.setGracePeriod(Long.parseLong(getGracePeriod()));
			localunitView.setGracePeriodTypeId(unitView.getGracePeriodTypeId());
			localunitView.setRentProcessId(unitView.getRentProcessId());
		}
//		catch (ParseException pE) {
//			logger.logError("getUnitViewToAdd crashed with Parse Exception|%s", pE);
//		}
		catch (Exception e) {
			logger.logError("getUnitViewToAdd crashed with Exception|%s", e);
		}
		/////////////////////////////////
		return localunitView;
	}

	private void closeWindow() {
		final String viewId = "/GenerateUnits.jsp";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		// Your Java script code here
		String javaScriptText = "javascript:closeWindow();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

	}
	
	private void setInfo() {
		String methodName = "setInfo";
		logger.logInfo(methodName + " started...");
		try {
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			
			Object objUnitViewInSession = getFacesContext().getExternalContext().getSessionMap()
					.get(WebConstants.UNIT_VIEW);

			if (objUnitViewInSession != null) {	
				UnitView unitView = (UnitView) objUnitViewInSession;
				hdnTextUnitId.setValue(unitView.getUnitId());
				viewRootMap.put(WebConstants.UnitDetails.UnitDetails_UnitView, unitView);
				viewRootMap.put(ADD_MODE, false);
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.UNIT_VIEW);
			}
			else{
				viewRootMap.put(ADD_MODE, true);
			}
			logger.logInfo(methodName + " comleted successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed...", exception);
		}
	}
	
	private void loadPropertyInfo() {
		String methodName = "loadPropertyInfo";
		logger.logInfo(methodName + " started...");
		try {
			PropertyView propertyView = null;
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			
			Object mode = viewRootMap.get(ADD_MODE);
			
			if((boolean)mode.equals(false)){
				isAddMode = false;
				UnitView uv = getUnitView();
				propertyView = new PropertyView();
				propertyView.setPropertyId(uv.getPropertyId());
				propertyView.setPropertyNumber(uv.getPropertyNumber());
				propertyView.setCategoryId(uv.getPropertyCategoryId());
				propertyView.setCategoryTypeAr(uv.getPropertyCategoryAr());
				propertyView.setCategoryTypeEn(uv.getPropertyCategoryEn());
				propertyView.setCommercialName(uv.getPropertyCommercialName());
				propertyView.setEndowedName(uv.getPropertyEndowedName());
				propertyView.setPropertyTypeAr(uv.getPropertyTypeAr());
				propertyView.setPropertyTypeEn(uv.getPropertyTypeEn());
//				setUnitArea(getFormattedDoubleString(uv.getUnitArea(), getNumberFormat()));
//				setRentValue(getFormattedDoubleString(uv.getRentValue(), getNumberFormat()));
//				setRequiredPrice(getFormattedDoubleString(uv.getRequiredPrice(), getNumberFormat()));
				if(uv.getUnitArea() != null)
					setUnitArea(uv.getUnitArea().toString());
				else
					setUnitArea("0");
				if(uv.getRentValue() != null)
					setRentValue(uv.getRentValue().toString());
				else
					setRentValue("0");
				if(uv.getRequiredPrice() != null)
					setRequiredPrice(uv.getRequiredPrice().toString());
				else
					setRequiredPrice("0");
				if(uv.getGracePeriod() != null)
					setGracePeriod(uv.getGracePeriod().toString());
				else
					setGracePeriod("0");
				if(uv.getNoOfBed() != null)
					setNoOfBed(uv.getNoOfBed().toString());
				else
					setNoOfBed("0");
				if(uv.getNoOfBath() != null)
					setNoOfBath(uv.getNoOfBath().toString());
				else
					setNoOfBath("0");
				if(uv.getNoOfLiving() != null)
					setNoOfLiving(uv.getNoOfLiving().toString());
				else
					setNoOfLiving("0");
				
				viewRootMap.put(WebConstants.UnitDetails.PROPERTY_VIEW, propertyView);
			}
			else{
				isAddMode = true;
				Object sessionObj = ApplicationContext.getContext().get(WebContext.class).getAttribute(WebConstants.UnitDetails.PROPERTY_VIEW);
				if(sessionObj != null){
					propertyView = (PropertyView)sessionObj;
					viewRootMap.put(WebConstants.UnitDetails.PROPERTY_VIEW, propertyView);
					ApplicationContext.getContext().get(WebContext.class).removeAttribute(WebConstants.UnitDetails.PROPERTY_VIEW);
					viewRootMap.remove(WebConstants.UnitDetails.FLOOR_VIEW_LIST);
				}
				Object viewObj = viewRootMap.get(WebConstants.UnitDetails.PROPERTY_VIEW);
				if(viewObj != null){
					propertyView = (PropertyView)viewObj;
				}				
			}
			if(propertyView != null){
				property = propertyView;
			}
			logger.logInfo(methodName + " comleted successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed...", exception);
		}
	}

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if (!isPostBack())
			setInfo();
		loadPropertyInfo();
	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();		
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	// Getters and Setters

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}
	
	public String NavigateToAddUnit() {

//		UnitView unitView = new UnitView();
//		
//		unitView.setNoOfBath(new Long(2));
//		unitView.setNoOfBed(new Long(4));
//		unitView.setNoOfLiving(new Long(3));
//		unitView.setInvestmentTypeId(new Long(8001));
//		unitView.setUsageTypeId(new Long(6101));
//		unitView.setUnitTypeId(new Long(22002));
//		unitView.setUnitArea(new Double(5.99));
//		unitView.setUnitNumber("Un-123");
//		
//		Object objUnitView = getFacesContext().getExternalContext().getSessionMap()
//		.put(WebConstants.UnitDetails.UnitDetails_UnitView,unitView);
		
		return "UnitDetails";
		
	}
	
	public void showPropertyList(ActionEvent event) {

		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		try {
			PropertyView property = psAgent.getPropertyByID(new Long(4));
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		final String viewId = "/UnitDetails.jsf";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = "javascript:showPropertyList();";
		// Add the Javascript to the rendered page's header for immediate execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,	AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	public void floorValueChange (ValueChangeEvent vce){
		String floorId = vce.getNewValue().toString();
		ArrayList<FloorView> floors = null;
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object viewObjFloorViewList = viewRootMap.get(WebConstants.UnitDetails.FLOOR_VIEW_LIST);
		if(viewObjFloorViewList != null){
			floors = (ArrayList<FloorView>)viewObjFloorViewList;
			floorType = searchFloorType(floors, floorId);
		}
	}

	private String searchFloorType(ArrayList<FloorView> floors, String floorId) {
		String floorType = "";
		for (FloorView fView : floors) {
			if(fView.getFloorId().toString().trim().equals(floorId.trim())){
				if(getIsEnglishLocale())
					floorType = fView.getFloorTypeEn();
				else
					floorType = fView.getFloorTypeAr();
			}	
		}
		return floorType;
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	
	public boolean getIsEnglishLocale()
	{
		boolean isEnglishLocale = false; 
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getMessages() {
		String messageList;
		if ((messages == null) || (messages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>\n";
			for (String message : messages) {
				messageList = messageList + message + "<BR> \n";
			}
			messageList = messageList + "</UL>\n";
		}
		return (messageList);
	}
	
	public String getErrorMsgs() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);
	}

	public String getErrorMessages() {

		String messageList = "";

		if ((error_Messages_ub == null) || (error_Messages_ub.size() == 0)) {
			messageList = "";
		} else {
			messageList = messageList + "<FONT COLOR=RED><B><UL>\n";
			for (String message : error_Messages_ub) {
				messageList = messageList + "" + message + "<BR>\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}

		if ((error_Messages == null) || (error_Messages.size() == 0)) {

		} else {
			messageList = messageList + "<FONT COLOR=RED><B><UL>\n";
			for (String message : error_Messages) {
				messageList = messageList + "<LI>" + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return (messageList);
	}	

	public HtmlInputHidden getHdnTextPropertyId() {
		return hdnTextPropertyId;
	}

	public void setHdnTextPropertyId(HtmlInputHidden hdnTextPropertyId) {
		this.hdnTextPropertyId = hdnTextPropertyId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public PropertyView getProperty() {
		return property;
	}

	public void setProperty(PropertyView property) {
		this.property = property;
	}

	public HtmlInputHidden getHdnTextFloorId() {
		return hdnTextFloorId;
	}

	public void setHdnTextFloorId(HtmlInputHidden hdnTextFloorId) {
		this.hdnTextFloorId = hdnTextFloorId;
	}

	public HtmlInputHidden getHdnTextUnitId() {
		return hdnTextUnitId;
	}

	public void setHdnTextUnitId(HtmlInputHidden hdnTextUnitId) {
		this.hdnTextUnitId = hdnTextUnitId;
	}

	public String getFloorType() {
		return floorType;
	}

	public void setFloorType(String floorType) {
		this.floorType = floorType;
	}

	public String getUnitArea() {
		return unitArea;
	}

	public void setUnitArea(String unitArea) {
		this.unitArea = unitArea;
	}

	public String getRentValue() {
		return rentValue;
	}

	public void setRentValue(String rentValue) {
		this.rentValue = rentValue;
	}

	public String getRequiredPrice() {
		return requiredPrice;
	}

	public void setRequiredPrice(String requiredPrice) {
		this.requiredPrice = requiredPrice;
	}

	public String getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(String gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public String getNoOfBed() {
		return noOfBed;
	}

	public void setNoOfBed(String noOfBed) {
		this.noOfBed = noOfBed;
	}

	public String getNoOfBath() {
		return noOfBath;
	}

	public void setNoOfBath(String noOfBath) {
		this.noOfBath = noOfBath;
	}

	public String getNoOfLiving() {
		return noOfLiving;
	}

	public void setNoOfLiving(String noOfLiving) {
		this.noOfLiving = noOfLiving;
	}

	public Boolean getIsAddMode() {
		return isAddMode;
	}

	public void setIsAddMode(Boolean isAddMode) {
		this.isAddMode = isAddMode;
	}
}
