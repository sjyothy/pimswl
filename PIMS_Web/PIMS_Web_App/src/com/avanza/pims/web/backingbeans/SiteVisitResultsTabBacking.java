package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.SiteVisitResultsView;


public class SiteVisitResultsTabBacking extends AbstractController {

	private static Logger logger = Logger.getLogger(SiteVisitResultsTabBacking.class);
	
	private HtmlDataTable dataTable = new HtmlDataTable();
	private List<SiteVisitResultsView> siteVisitResultList = new ArrayList<SiteVisitResultsView>();
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	public SiteVisitResultsTabBacking (){
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
   	 	if(!isPostBack()){
   	 		
   	 	}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("siteVisitResultsRecordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public List<SiteVisitResultsView> getSiteVisitResultList() {
		siteVisitResultList = (List<SiteVisitResultsView>)viewMap.get(WebConstants.SiteVisit.VISIT_RESULT_LIST);
		if(siteVisitResultList==null)
			siteVisitResultList = new ArrayList<SiteVisitResultsView>();
		return siteVisitResultList;
	}

	public void setSiteVisitResultList(
			List<SiteVisitResultsView> siteVisitResultList) {
		this.siteVisitResultList = siteVisitResultList;
	}

	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}    

	public String getDateFormat()
	{
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		return dateFormat;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

}
