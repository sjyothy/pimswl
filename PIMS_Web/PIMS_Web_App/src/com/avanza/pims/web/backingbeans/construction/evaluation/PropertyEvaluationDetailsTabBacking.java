package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.bpel.services.identity.xpath.GetDefaultRealmNameFunction;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.EvaluationByUnitTypeView;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.crystaldecisions.reports.reportdefinition.ev;


@SuppressWarnings("serial")
public class PropertyEvaluationDetailsTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(PropertyEvaluationDetailsTabBacking.class);
	private boolean showEvaluationBy=false;
	private EvaluationRequestDetailsView requestDetailsTabView = new EvaluationRequestDetailsView();
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private HtmlSelectOneMenu evaluationCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu evaluationByCombo = new HtmlSelectOneMenu();
	PropertyServiceAgent psa=new PropertyServiceAgent();
	private HtmlDataTable evaluationUnitDT=new  HtmlDataTable();
	List<EvaluationByUnitTypeView> evaluationUnitTypeList=new ArrayList<EvaluationByUnitTypeView>(0);
	private List<DomainDataView> unitTypeList =new ArrayList<DomainDataView>(0);
	private List<ReceivePropertyUnitView> unitList =new ArrayList<ReceivePropertyUnitView>(0);
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private boolean popUp=false;
	private final String EVALUATION_UNIT_TYPE_LIST = "EVALUATION_UNIT_TYPE_LIST";
	private final String UNIT_TYPE_ITEMS_LIST = "UNIT_TYPE_ITEMS_LIST";
	private final String ADDED_UNIT = "ADDED_UNIT";
	private final String UNIT_TYPE_MAP = "UNIT_TYPE_MAP";
	private final Object UNIT_SIDE_MAP = "UNIT_SIDE_MAP";
	private final Object UNIT_TYPE_RECORD_MAP = "UNIT_TYPE_RECORD_MAP";
	private List<DomainDataView> unitSideList =  new ArrayList<DomainDataView>();
	
	private HtmlSelectOneMenu cboUnitSide = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cboUnitType = new HtmlSelectOneMenu();
	private HtmlInputText txtRent = new HtmlInputText();
	private HtmlInputText txtAreaFrom = new HtmlInputText();
	private HtmlInputText txtAreaTo = new HtmlInputText();
	
	private String areaFrom;
	private String areaTo;
	private String unitSideId;
	private String unitTypeId;
	private String unitTypeRent;
	private List<String> errorMessages = new ArrayList<String>();
	private HashMap<Long, String> unitTypeMap = new HashMap<Long, String>();
	private HashMap<Long, String> unitSideMap = new HashMap<Long, String>();
	private HashMap<Long, List<EvaluationByUnitTypeView>> unitTypeRecordMap = new HashMap<Long, List<EvaluationByUnitTypeView>>();
	
	 @SuppressWarnings("unchecked")
	@Override 
	 public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				unitTypeList=CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_TYPE);
				unitSideList = CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_SIDE_TYPE);
					viewMap.put("UNIT_TYPE_LIST",unitTypeList);
					
					if(unitSideList != null && unitSideList.size() > 0)
						viewMap.put("UNIT_SIDE_LIST",unitSideList);
					
					if(CommonUtil.getIsEnglishLocale())
					{
						if(unitTypeList != null && unitTypeList.size() > 0)
						{
							for(DomainDataView unitTypeDD : unitTypeList)
							unitTypeMap.put(unitTypeDD.getDomainDataId(),unitTypeDD.getDataDescEn());
						}
						if(unitSideList != null && unitSideList.size() > 0)
						{
							for(DomainDataView unitSideDD : unitSideList)
								unitSideMap.put(unitSideDD.getDomainDataId(),unitSideDD.getDataDescEn());
						}
					}
					else
					{
						
						if(unitTypeList != null && unitTypeList.size() > 0)
						{
							for(DomainDataView unitTypeDD : unitTypeList)
								unitTypeMap.put(unitTypeDD.getDomainDataId(),unitTypeDD.getDataDescAr());
						}
						if(unitSideList != null && unitSideList.size() > 0)
						{
							for(DomainDataView unitSideDD : unitSideList)
								unitSideMap.put(unitSideDD.getDomainDataId(),unitSideDD.getDataDescAr());
						}
						
					}
					viewMap.put(UNIT_TYPE_MAP, unitTypeMap);
					viewMap.put(UNIT_SIDE_MAP, unitSideMap);
			}
			if(viewMap.get("requestId")!=null)
				if(viewMap.get("evaluationPurposeId")!=null)
					evaluationCombo.setValue(viewMap.get("evaluationPurposeId").toString());
			if(sessionMap.containsKey("IS_POPUP"))
			{
				popUp=(Boolean) sessionMap.get("IS_POPUP");
				sessionMap.remove("IS_POPUP");
			}
			else if(viewMap.containsKey("IS_POPUP"))
				popUp=(Boolean) viewMap.get("IS_POPUP");
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 	}

	public Boolean getReadonlyMode(){
		Boolean evaluationRequestDetailsReadonlyMode = (Boolean)viewMap.get("evaluationRequestDetailsReadonlyMode");
		if(evaluationRequestDetailsReadonlyMode==null)
			evaluationRequestDetailsReadonlyMode = false;
		return evaluationRequestDetailsReadonlyMode;
	}

	public String getPropertyStatusId() {
		String temp = (String)viewMap.get("propertyStatusId");
		return temp;
	}
	public void setPropertyStatusId(String propertyStatusId) {
		if(propertyStatusId!=null)
			viewMap.put("propertyStatusId" , propertyStatusId);
	}
	public String getPropertyStatus() {
		String temp = (String)viewMap.get("propertyStatus");
		return temp;
	}
	public void setPropertyStatus(String propertyStatus) {
		if(propertyStatus!=null)
			viewMap.put("propertyStatus" , propertyStatus);
	}
	public String getPropertyName() {
		String temp = (String)viewMap.get("propertyName");
		return temp;
	}
	public void setPropertyName(String propertyName) {
		if(propertyName!=null)
			viewMap.put("propertyName" , propertyName);
	}
	public String getEmirate() {
		String temp = (String)viewMap.get("emirate");
		return temp;
	}
	public void setEmirate(String emirate) {
		if(emirate!=null)
			viewMap.put("emirate" , emirate);
	}
	public String getLocationArea() {
		String temp = (String)viewMap.get("locationArea");
		return temp;
	}
	public void setLocationArea(String locationArea) {
		if(locationArea!=null)
			viewMap.put("locationArea" , locationArea);
	}
	public String getLandNumber() {
		String temp = (String)viewMap.get("landNumber");
		return temp;
	}
	public void setLandNumber(String landNumber) {
		if(landNumber!=null)
			viewMap.put("landNumber" , landNumber);
	}
	public String getLandArea() {
		String temp = (String)viewMap.get("landArea");
		return temp;
	}
	public void setLandArea(String landArea) {
		if(landArea!=null)
			viewMap.put("landArea" , landArea);
	}
	public String getAddress() {
		String temp = (String)viewMap.get("address");
		return temp;
	}
	public void setAddress(String address) {
		if(address!=null)
			viewMap.put("address" , address);
	}
	public String getPropertyType() {
		String temp = (String)viewMap.get("propertyType");
		return temp;
	}
	public void setPropertyType(String propertyType) {
		if(propertyType!=null)
			viewMap.put("propertyType" , propertyType);
	}
	public String getFloorCount() {
		String temp = (String)viewMap.get("floorCount");
		return temp;
	}
	public void setFloorCount(String floorCount) {
		if(floorCount!=null)
			viewMap.put("floorCount" , floorCount);
	}
	public String getConstructionDate() {
		String temp = (String)viewMap.get("constructionDate");
		return temp;
	}
	public void setConstructionDate(String constructionDate) {
		if(constructionDate!=null)
			viewMap.put("constructionDate" , constructionDate);
	}
	public Long getPropertyId() {
		Long temp = (Long)viewMap.get("propertyId");
		return temp;
	}
	public void setPropertyId(Long propertyId) {
		if(propertyId!=null)
			viewMap.put("propertyId" , propertyId);
	}
	
	@SuppressWarnings("unchecked")
	public void clearProperty(ActionEvent event){
		logger.logInfo("clearProperty() started...");
		try{
			EvaluationRequestDetailsView requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
			requestDetailsTabView.setPropertyId(0L);
			requestDetailsTabView.setPropertyName("");
			requestDetailsTabView.setPropertyStatus("");
			requestDetailsTabView.setPropertyStatusId("");
			requestDetailsTabView.setPropertyType("");
			requestDetailsTabView.setAddress("");
			requestDetailsTabView.setConstructionDate("");
			requestDetailsTabView.setEmirate("");
			requestDetailsTabView.setFloorCount("");
			requestDetailsTabView.setLandArea("");
			requestDetailsTabView.setLandNumber("");
			requestDetailsTabView.setLocationArea("");

			viewMap.put("clearProperty", true);
			viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
			populateFromView();
		}
    	catch(Exception exception){
    		logger.LogException("clearProperty() crashed ",exception);
    	}
	}
	
	private void populateFromView(){
		requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
		if(requestDetailsTabView!=null){
			setPropertyId(requestDetailsTabView.getPropertyId());
			setPropertyType(requestDetailsTabView.getPropertyType());
			setPropertyStatus(requestDetailsTabView.getPropertyStatus());
			setPropertyName(requestDetailsTabView.getPropertyName());
			setEmirate(requestDetailsTabView.getEmirate());
			setLocationArea(requestDetailsTabView.getLocationArea());
			setLandNumber(requestDetailsTabView.getLandNumber());
			setLandArea(requestDetailsTabView.getLandArea());
			setAddress(requestDetailsTabView.getAddress());
			setPropertyType(requestDetailsTabView.getPropertyType());
			setFloorCount(requestDetailsTabView.getFloorCount());
			setConstructionDate(requestDetailsTabView.getConstructionDate());
		}
	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}

	public EvaluationRequestDetailsView getRequestDetailsTabView() {
		return requestDetailsTabView;
	}

	public void setRequestDetailsTabView(
			EvaluationRequestDetailsView requestDetailsTabView) {
		this.requestDetailsTabView = requestDetailsTabView;
	}

	public HtmlSelectOneMenu getEvaluationCombo() {
		return evaluationCombo;
	}

	public void setEvaluationCombo(HtmlSelectOneMenu evaluationCombo)
	{
		this.evaluationCombo = evaluationCombo;
	}

	@SuppressWarnings("unchecked")
	public void openNewAmountPopup()
	{
		EvaluationByUnitTypeView ut=new EvaluationByUnitTypeView();
		List<EvaluationByUnitTypeView> euList=new ArrayList<EvaluationByUnitTypeView>(0);
		if(viewMap.containsKey(EVALUATION_UNIT_TYPE_LIST) &&  viewMap.get(EVALUATION_UNIT_TYPE_LIST)!=null)
			euList=(List<EvaluationByUnitTypeView>) viewMap.get(EVALUATION_UNIT_TYPE_LIST);
		ut=(EvaluationByUnitTypeView) evaluationUnitDT.getRowData();
		sessionMap.put("SELECTED_UNIT_TYPE",ut);
		viewMap.put("SELECTED_INDEX",euList.indexOf(ut) );
		executeJavascript("openNewAmountPopupJS();");
	}
	public void executeJavascript(String javascript) {
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);		
		} 
		catch (Exception exception) 
		{			
			logger.LogException(" executing JavaScript CRASHED --- ", exception);
		}
	}

	public HtmlDataTable getEvaluationUnitDT() {
		return evaluationUnitDT;
	}

	public void setEvaluationUnitDT(HtmlDataTable evaluationUnitDT) {
		this.evaluationUnitDT = evaluationUnitDT;
	}

	public boolean isShowEvaluationBy() 
	{
		showEvaluationBy = false;
		if(viewMap.get("SHOW_EVALUATION_BY") != null)
		{
			showEvaluationBy = (Boolean) viewMap.get("SHOW_EVALUATION_BY");
		}
		return showEvaluationBy;
	}

	public void setShowEvaluationBy(boolean showEvaluationBy) {
		this.showEvaluationBy = showEvaluationBy;
	}
	public Boolean getEvaluationReadonlyMode(){
		Boolean evaluationReadonlyMode = (Boolean)viewMap.get("evaluationReadonlyMode");
		return (evaluationReadonlyMode==null)?false:evaluationReadonlyMode;
	}
	public boolean getEvaluationByUnitType()
	{
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<EvaluationByUnitTypeView> getEvaluationUnitTypeList() 
	{
		if(viewMap.get(EVALUATION_UNIT_TYPE_LIST)!=null)
			{
				evaluationUnitTypeList=(List<EvaluationByUnitTypeView>) viewMap.get(EVALUATION_UNIT_TYPE_LIST);
				if(evaluationUnitTypeList!=null)
					viewMap.put("RECORD_SIZE", evaluationUnitTypeList.size());
			}
		return evaluationUnitTypeList;
	}

	@SuppressWarnings("unchecked")
	public void setEvaluationById(String id)
	{
		if(id!=null)
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_BY_ID,id);
	}
	
	public String getEvaluationById()
	{
		String id="";
		if(viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null)
			id=viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString();
		return id;
	}	
	@SuppressWarnings("unchecked")
	public boolean isShowUnitTypeDetails() 
	{
		boolean show=false;
			if(isShowEvaluationBy() && (getEvaluationById().equalsIgnoreCase("2")))
				{
					show=true;
					viewMap.put("HIDE_PROPERTY_RENT_VALUE", true);
				}
			else
				{
					show=false;
					viewMap.put("HIDE_PROPERTY_RENT_VALUE", false);
				}
		return show;	
	}

	public HtmlSelectOneMenu getEvaluationByCombo() {
		return evaluationByCombo;
	}

	public void setEvaluationByCombo(HtmlSelectOneMenu evaluationByCombo) {
		this.evaluationByCombo = evaluationByCombo;
	}
	@SuppressWarnings("unchecked")
	public void evaluationByNewValue(ValueChangeEvent e)
	{
		viewMap.put("NEW_PROPERTY_VALUE",0D);
		sessionMap.put("CLEAR_FIELD", true);
		if(e.getNewValue()!=null && (e.getNewValue().toString().compareTo("2")==0))
		{
		viewMap.put("HIDE_PROPERTY_RENT_VALUE", true);
		setEvaluationById(e.getNewValue().toString());
		try 
		{
			if(getPropertyId()!=null)
			{
				List<SelectItem> items = new ArrayList<SelectItem>();
				if(viewMap.containsKey(UNIT_TYPE_ITEMS_LIST))
					viewMap.remove(UNIT_TYPE_ITEMS_LIST);
				
				unitList=psa.getPropertyUnitList(getPropertyId());
				HashSet<Long> addedUnitTypeIds = new HashSet<Long>();
				if(viewMap.get(UNIT_TYPE_ITEMS_LIST) != null)
				{
					items = (List<SelectItem>) viewMap.get(UNIT_TYPE_ITEMS_LIST);
				}
				else
				{
					for(ReceivePropertyUnitView ut:unitList)
					{
						if(! addedUnitTypeIds.contains(ut.getUnitTypeId()) )
						{
							SelectItem item = new SelectItem(ut.getUnitTypeId().toString(),CommonUtil.getIsEnglishLocale()?ut.getUnitTypeEn():ut.getUnitTypeAr());
							items.add(item);
							addedUnitTypeIds.add(ut.getUnitTypeId());
						}
						else
							continue;
					}
					viewMap.put(UNIT_TYPE_ITEMS_LIST, items);
						
					}
				}
		} 
		catch (PimsBusinessException ex) 
		{	
			logger.LogException("evaluationByNewValue Crashed", ex);
		}
		}
	}

	public List<DomainDataView> getUnitTypeList() {
		return unitTypeList;
	}

	public void setUnitTypeList(List<DomainDataView> unitTypeList) {
		this.unitTypeList = unitTypeList;
	}

	public List<ReceivePropertyUnitView> getUnitList() {
		return unitList;
	}

	public void setUnitList(List<ReceivePropertyUnitView> unitList) {
		this.unitList = unitList;
	}

	public void setEvaluationUnitTypeList(
			List<EvaluationByUnitTypeView> evaluationUnitTypeList) {
		this.evaluationUnitTypeList = evaluationUnitTypeList;
	}

	public boolean isPopUp() 
	{
		if(viewMap.containsKey("IS_POPUP"))
			popUp=(Boolean) viewMap.get("IS_POPUP");
		return popUp;
	}

	public void setPopUp(boolean popUp) {
		this.popUp = popUp;
	}

	public Integer getPaginatorMaxPages() 
	{
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() 
	{
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() 
	{
		if(viewMap.containsKey("RECORD_SIZE"))
			recordSize=(Integer) viewMap.get("RECORD_SIZE");
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getPropertyUnitTypeList()
	{
		List<SelectItem> items = new ArrayList<SelectItem>();
		if(viewMap.get(UNIT_TYPE_ITEMS_LIST) != null)
		{
			items = (List<SelectItem>) viewMap.get(UNIT_TYPE_ITEMS_LIST);
		}
		return items;
	}
	@SuppressWarnings("unchecked")
	public void addUnitRentToGrid()
	{
		if(isUnitFieldsValidated() )
		{
			List<EvaluationByUnitTypeView> evaUnitTypeList = new ArrayList<EvaluationByUnitTypeView>();
			evaUnitTypeList = getEvaluationUnitTypeList();
//			
			if(!isConflictingWithExistingRecords(getAddedEvaluationUnitType()))
			{
				evaUnitTypeList.add(getAddedEvaluationUnitType());
				if(evaUnitTypeList != null && evaUnitTypeList.size() > 0)
				{
					viewMap.put(EVALUATION_UNIT_TYPE_LIST, evaUnitTypeList);
					clearUnitTypeFields();
				}
			}
			else
				{
					if(errorMessages != null && errorMessages.size() > 0 )
						errorMessages.clear();
					errorMessages.add("propertyEvaluation.errMsg.criteriaConflicting");
					viewMap.put("ERROR_MESSAGES", errorMessages);
				}
			
		}
	}
	@SuppressWarnings("unchecked")
	private boolean isConflictingWithExistingRecords(EvaluationByUnitTypeView unitToBAdded) 
	{
		boolean conflicting = false;
		if(viewMap.get(UNIT_TYPE_RECORD_MAP) != null)
		{
			unitTypeRecordMap = (HashMap<Long, List<EvaluationByUnitTypeView>>) viewMap.get(UNIT_TYPE_RECORD_MAP);
			
			//first check unit_Type_Id
			if(unitTypeRecordMap.containsKey(unitToBAdded.getUnitTypeId()))
			{
				List<EvaluationByUnitTypeView> evaUnitList = new ArrayList<EvaluationByUnitTypeView>();
				//extracted list of added records for the selected unit type
				evaUnitList =  unitTypeRecordMap.get(unitToBAdded.getUnitTypeId());
				
				if(evaUnitList != null && evaUnitList.size() > 0)
				{
					for(EvaluationByUnitTypeView recordedUnit : evaUnitList)
					{
					   //**********************1*********************
						//if recorded unit has all fields null
//						if( (recordedUnit.getUnitSideId() == null && 
//							 StringHelper.isEmpty(recordedUnit.getAreaFrom()) && 
//							 StringHelper.isEmpty(recordedUnit.getAreaTo() ))) 
//							{
//								conflicting = true;
//								return conflicting;
//							}
						
						// NEW UNIT SIDE && OLD UNIT SIDE = NULL
						if(unitToBAdded.getUnitSideId() == null && recordedUnit.getUnitSideId() == null)
						{
							if(!isCorrectRange(unitToBAdded, recordedUnit))
								{
									conflicting = true;
									break;
								}
						}
						
						// NEW UNIT SIDE && OLD UNIT SIDE = NOT NULL 
						else
						if(unitToBAdded.getUnitSideId() != null && recordedUnit.getUnitSideId() != null)
						{
							//NEW UNIT SIDE = OLD UNIT SIDE
							if(unitToBAdded.getUnitSideId().compareTo(recordedUnit.getUnitSideId()) == 0)
							{
								if(!isCorrectRange(unitToBAdded, recordedUnit))
									{
										conflicting = true;
										break;
									}
							}
							//NEW UNIT SIDE != OLD UNIT SIDE
							else
							{
								conflicting = false;
							}
						}	
						
						// NEW UNIT SIDE NOT NULL && OLD UNIT SIDE = NULL 
						else
						if(unitToBAdded.getUnitSideId() != null  && recordedUnit.getUnitSideId() == null)
						{
							if(!isCorrectRange(unitToBAdded, recordedUnit))
								{
									conflicting = true;
									break;
								}
						}
						
						// NEW UNIT SIDE = NULL && OLD UNIT SIDE NOT NULL 
						else
						if(unitToBAdded.getUnitSideId() == null  && recordedUnit.getUnitSideId() != null)
						{
							if(!isCorrectRange(unitToBAdded, recordedUnit))
								{
									conflicting = true;
									break;
								}
						}
						
					}
				//if not conflicting then add it to Map	
				if(!conflicting)
				{
					evaUnitList.add(unitToBAdded) ;
					unitTypeRecordMap.put(unitToBAdded.getUnitTypeId(), evaUnitList);
					viewMap.put(UNIT_TYPE_RECORD_MAP, unitTypeRecordMap);
				}
					
				}
				
			}
			else
			{
				conflicting = false;
				List<EvaluationByUnitTypeView> unitList = new ArrayList<EvaluationByUnitTypeView>();
				unitList.add(unitToBAdded);
				unitTypeRecordMap.put(unitToBAdded.getUnitTypeId(), unitList);
				viewMap.put(UNIT_TYPE_RECORD_MAP,unitTypeRecordMap);
			}	
		}
		else
		{
			List<EvaluationByUnitTypeView> unitList = new ArrayList<EvaluationByUnitTypeView>();
			unitList.add(unitToBAdded);
			unitTypeRecordMap.put(unitToBAdded.getUnitTypeId(), unitList);
			viewMap.put(UNIT_TYPE_RECORD_MAP,unitTypeRecordMap);
		}
		return conflicting;
	}

	private EvaluationByUnitTypeView getAddedEvaluationUnitType() 
	{
		EvaluationByUnitTypeView evaUnit = new EvaluationByUnitTypeView();
		if(viewMap.get(ADDED_UNIT) != null)
			evaUnit = (EvaluationByUnitTypeView) viewMap.get(ADDED_UNIT);
		return evaUnit;
	}

	@SuppressWarnings("unchecked")
	//This method validate the unit type rent fields and also creating the populated class to be added in grid.
	public boolean isUnitFieldsValidated()
	{
		boolean validated = true;
		boolean hasToAreaFormatError = false;
		boolean hasFromAreaFormatError = false;
		EvaluationByUnitTypeView addedUnit = new EvaluationByUnitTypeView();
		
		if(errorMessages != null && errorMessages.size() > 0 )
			errorMessages.clear();
		
		if(getUnitTypeId() != null && getUnitTypeId().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.selectUnitType"));
			validated = false;
		}
		else if(getUnitTypeId() != null )
			{
				addedUnit.setUnitTypeId(Long.parseLong(getUnitTypeId()));
				addedUnit.setUnitType(getUnitTypeDescriptionById(addedUnit.getUnitTypeId()));
			}
			
		
		if(getAreaFrom() != null && getAreaFrom().trim().length() > 0)
		{
			try
			{
				if(Double.parseDouble(getAreaFrom().trim()) <= 0D)
					{
						errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.areaCantBeLEZero"));
						validated = false;
						hasFromAreaFormatError = true;
					}
				else
					addedUnit.setAreaFrom(getAreaFrom().trim());
			}
			catch (NumberFormatException e) 
			{
				hasFromAreaFormatError = true;
				errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.invalidFromArea"));
				validated = false;
			}
		}
		
		if(getAreaTo() != null && getAreaTo().trim().length() > 0)
		{
			try
			{
				if(Double.parseDouble(getAreaTo().trim()) <= 0D)
					{
						errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.areaToCantBeLEZero"));
						validated = false;
						hasToAreaFormatError = true;
					}
				else
					addedUnit.setAreaTo(getAreaTo().trim());
			}
			catch (NumberFormatException e) 
			{
				hasToAreaFormatError = true;
				errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.invalidToArea"));
				validated = false;
			}
		}
		
		if(getUnitTypeRent() != null && getUnitTypeRent().trim().length() > 0)
		{
			try
			{
				if(Double.parseDouble(getUnitTypeRent().trim()) <= 0D)
					{
						errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.rentCntBeLEZero"));
						validated = false;
					}
				else if(getUnitTypeRent() != null )
					addedUnit.setNewRentValue(Double.parseDouble(getUnitTypeRent()));
			}
			catch (NumberFormatException e) 
			{
				errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.InvalidRent"));
				validated = false;
			}
		}
		
		if(getUnitSideId() != null && getUnitSideId().compareTo("-1")!=0)
		{
			addedUnit.setUnitSideId(Long.parseLong(getUnitSideId()));
			addedUnit.setUnitSide(getUnitSideDescriptionById(addedUnit.getUnitSideId()));
		}
		
		if( !hasToAreaFormatError && !hasFromAreaFormatError && validated && !StringHelper.isEmpty(getAreaFrom()) && !StringHelper.isEmpty(getAreaTo()))
		{
			double areaFromDbl = Double.parseDouble(getAreaFrom());
			double areaToDbl = Double.parseDouble(getAreaTo());
			if(areaFromDbl > areaToDbl)
			{
				errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.fromCantBeLTTo"));
				validated = false;
			}
		}
		if(validated)
			viewMap.put(ADDED_UNIT,addedUnit);
		else
			viewMap.put("ERROR_MESSAGES", errorMessages);
		return validated;
		
	}

	public String getAreaFrom() 
	{
		if(viewMap.get("AREA_FROM") != null)
			this.areaFrom = viewMap.get("AREA_FROM").toString(); 
		return areaFrom;
	}

	@SuppressWarnings("unchecked")
	public void setAreaFrom(String areaFrom) 
	{
		if(areaFrom != null && areaFrom.trim().length() > 0)
			viewMap.put("AREA_FROM", areaFrom);
		this.areaFrom = areaFrom;
	}

	public String getAreaTo() 
	{
		if(viewMap.get("AREA_TO") != null)
			this.areaTo = viewMap.get("AREA_TO").toString(); 
		return areaTo;
	}

	@SuppressWarnings("unchecked")
	public void setAreaTo(String areaTo) 
	{
		if(areaTo != null && areaTo.trim().length() > 0)
			viewMap.put("AREA_TO", areaTo);
		this.areaTo = areaTo;
	}

	public String getUnitSideId() 
	{
		if(viewMap.get("UNIT_SIDE_ID") != null)
			this.unitSideId = viewMap.get("UNIT_SIDE_ID").toString(); 
		return unitSideId;
	}

	@SuppressWarnings("unchecked")
	public void setUnitSideId(String unitSideId)
	{
		if(unitSideId != null && unitSideId.trim().length() > 0)
			viewMap.put("UNIT_SIDE_ID", unitSideId);
		this.unitSideId = unitSideId;
	}

	public String getUnitTypeId() 
	{
		if(viewMap.get("UNIT_TYPE_ID") != null)
			this.unitTypeId = viewMap.get("UNIT_TYPE_ID").toString(); 
		return unitTypeId;
	}

	@SuppressWarnings("unchecked")
	public void setUnitTypeId(String unitTypeId) 
	{
		if(unitTypeId != null && unitTypeId.trim().length() > 0)
			viewMap.put("UNIT_TYPE_ID", unitTypeId);
		this.unitTypeId = unitTypeId;
	}

	public String getUnitTypeRent() 
	{
		if(viewMap.get("UNIT_TYPE_RENT") != null)
			this.unitTypeRent = viewMap.get("UNIT_TYPE_RENT").toString(); 
		else
			this.unitTypeRent = "0";
		return unitTypeRent;
	}

	@SuppressWarnings("unchecked")
	public void setUnitTypeRent(String unitTypeRent) 
	{
		if(unitTypeRent != null && unitTypeRent.trim().length() > 0)
			viewMap.put("UNIT_TYPE_RENT", unitTypeRent);
		this.unitTypeRent = unitTypeRent;
	}
	@SuppressWarnings("unchecked")
	private String getUnitTypeDescriptionById(long id)
	{
		String description="";
		if(viewMap.get(UNIT_TYPE_MAP) != null)
		{
			unitTypeMap = (HashMap<Long, String>) viewMap.get(UNIT_TYPE_MAP);
			if(unitTypeMap.containsKey(id))
			description = unitTypeMap.get(id).toString();
		}
		return description;
	}
	@SuppressWarnings("unchecked")
	private String getUnitSideDescriptionById(long id)
	{
		String description="";
		if(viewMap.get(UNIT_SIDE_MAP) != null)
		{
			unitSideMap = (HashMap<Long, String>) viewMap.get(UNIT_SIDE_MAP);
			if(unitSideMap.containsKey(id))
			description = unitSideMap.get(id).toString();
		}
		return description;
	}
	@SuppressWarnings("unchecked")
	public void deleteUnitType()
	{
		evaluationUnitTypeList.remove(evaluationUnitTypeList.indexOf(evaluationUnitDT.getRowData()));
		viewMap.put(EVALUATION_UNIT_TYPE_LIST, evaluationUnitTypeList);
		
		if(viewMap.get(UNIT_TYPE_RECORD_MAP) != null)
		{
			//clear Map then fill it again with current list
			viewMap.remove(UNIT_TYPE_RECORD_MAP);
			if(evaluationUnitTypeList != null && evaluationUnitTypeList.size() > 0)
			{
				unitTypeRecordMap = new HashMap<Long, List<EvaluationByUnitTypeView>>();
				for(EvaluationByUnitTypeView evaUnit : evaluationUnitTypeList)
				{
					List<EvaluationByUnitTypeView> evaList = new ArrayList<EvaluationByUnitTypeView>();
					if(unitTypeRecordMap.get(evaUnit.getUnitTypeId()) != null)
					{
						evaList = unitTypeRecordMap.get(evaUnit.getUnitTypeId());
						evaList.add(evaUnit);
					}
					else
						evaList.add(evaUnit);
					
					unitTypeRecordMap.put(evaUnit.getUnitTypeId(), evaList);
				}
				viewMap.put(UNIT_TYPE_RECORD_MAP, unitTypeRecordMap);
			}
		}
			
	}
	@SuppressWarnings("unchecked")
	public void clearUnitTypeFields()
	{
		cboUnitSide.setValue("-1");
		cboUnitType.setValue("-1");
		txtAreaFrom.setValue("");
		txtAreaTo.setValue("");
		txtRent.setValue("0");
		viewMap.remove("UNIT_SIDE_ID");
		viewMap.remove("UNIT_TYPE_ID");
		viewMap.remove("AREA_FROM");
		viewMap.remove("AREA_TO");
		viewMap.put("UNIT_TYPE_RENT","0");
	}

	public HtmlSelectOneMenu getCboUnitSide() {
		return cboUnitSide;
	}

	public void setCboUnitSide(HtmlSelectOneMenu cboUnitSide) {
		this.cboUnitSide = cboUnitSide;
	}

	public HtmlSelectOneMenu getCboUnitType() {
		return cboUnitType;
	}

	public void setCboUnitType(HtmlSelectOneMenu cboUnitType) {
		this.cboUnitType = cboUnitType;
	}

	public HtmlInputText getTxtRent() {
		return txtRent;
	}

	public void setTxtRent(HtmlInputText txtRent) {
		this.txtRent = txtRent;
	}

	public HtmlInputText getTxtAreaFrom() {
		return txtAreaFrom;
	}

	public void setTxtAreaFrom(HtmlInputText txtAreaFrom) {
		this.txtAreaFrom = txtAreaFrom;
	}

	public HtmlInputText getTxtAreaTo() {
		return txtAreaTo;
	}

	public void setTxtAreaTo(HtmlInputText txtAreaTo) {
		this.txtAreaTo = txtAreaTo;
	}
	private boolean isInRange(Double checkNumber, Double from, Double to)
	{
		if(checkNumber >= from && checkNumber <= to)
			return true;
		else
			return false;	
	}
	private boolean isCorrectRange(EvaluationByUnitTypeView newUnit, EvaluationByUnitTypeView oldUnit)
	{
		boolean isCorrect = true;
		
		//NEW = NULL && OLD = NULL
		if(StringHelper.isEmpty(newUnit.getAreaFrom()) &&
		   StringHelper.isEmpty(newUnit.getAreaTo()) &&
		   StringHelper.isEmpty(oldUnit.getAreaFrom()) &&
		   StringHelper.isEmpty(oldUnit.getAreaTo()))
		{
			isCorrect = false;
			
		}
		
		//NEW = NOT NULL && OLD = NOT NULL
		else
		if(StringHelper.isNotEmpty(newUnit.getAreaFrom()) &&
		   StringHelper.isNotEmpty(newUnit.getAreaTo()) &&
		   StringHelper.isNotEmpty(oldUnit.getAreaFrom()) &&
		   StringHelper.isNotEmpty(oldUnit.getAreaTo()))
		{
			Double newAreaFrom 	= new Double(newUnit.getAreaFrom());
			Double newAreaTo 	= new Double(newUnit.getAreaTo());
			Double oldAreaFrom 	= new Double(oldUnit.getAreaFrom());
			Double oldAreaTo 	= new Double(oldUnit.getAreaTo());
			
			if(	isInRange(newAreaFrom, oldAreaFrom, oldAreaTo) || 
				isInRange(newAreaTo, oldAreaFrom, oldAreaTo) || 
				isInRange(oldAreaFrom, newAreaFrom, newAreaTo)||
				isInRange(oldAreaTo, newAreaFrom, newAreaTo))
			isCorrect = false;
			
		}
		
		//NEW = NOT NULL
		else
		if(StringHelper.isNotEmpty(newUnit.getAreaFrom()) &&
		   StringHelper.isNotEmpty(newUnit.getAreaTo()) )
		{
			Double newFrom = new Double(newUnit.getAreaFrom());
			Double newTo = new Double(newUnit.getAreaTo());
			Double oldFrom = null;
			Double oldTo = null;
			
			//OLD FROM = NOT NULL && OLD TO = NULL
			if(StringHelper.isNotEmpty(oldUnit.getAreaFrom()) &&
				StringHelper.isEmpty(oldUnit.getAreaTo()))
			{
				oldFrom = new Double(oldUnit.getAreaFrom());
				if(isInRange(oldFrom, newFrom, newTo))
					isCorrect = false;
					
			}
			
			//OLD FROM = NULL && OLD TO = NOT NULL
			else
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) &&
			   StringHelper.isNotEmpty(oldUnit.getAreaTo()) )
			{
				oldTo = new Double(oldUnit.getAreaTo());
				if(isInRange(oldTo, newFrom, newTo))
					isCorrect = false;
				
			}	
			
			//OLD FROM = NULL && OLD TO =NULL
			else
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) &&
			   StringHelper.isEmpty(oldUnit.getAreaTo()) )
			{
				isCorrect = false;
				
			}
			
		}	
		
		//OLD = NOT NULL
		else
		if(StringHelper.isNotEmpty(oldUnit.getAreaFrom()) &&
		   StringHelper.isNotEmpty(oldUnit.getAreaTo()) )
		{
			Double oldFrom = new Double(oldUnit.getAreaFrom());
			Double oldTo = new Double(oldUnit.getAreaTo());
			Double newFrom = null;
			Double newTo = null;
			
			//NEW FROM = NOT NULL && NEW TO = NULL
			if(StringHelper.isNotEmpty(newUnit.getAreaFrom()) &&
				StringHelper.isEmpty(newUnit.getAreaTo()))
			{
				newFrom = new Double(newUnit.getAreaFrom());
				if(isInRange(newFrom, oldFrom, oldTo))
					isCorrect = false;
					
			}
			
			//NEW FROM = NULL && NEW TO = NOT NULL
			else
			if(StringHelper.isEmpty(newUnit.getAreaFrom()) &&
			   StringHelper.isNotEmpty(newUnit.getAreaTo()) )
			{
				newTo = new Double(newUnit.getAreaTo());
				if(isInRange(newTo, oldFrom, oldTo))
					isCorrect = false;
				
			}	
			
			//NEW FROM = NULL && NEW TO =NULL
			else
			if(StringHelper.isEmpty(newUnit.getAreaFrom()) &&
			   StringHelper.isEmpty(newUnit.getAreaTo()) )
			{
				isCorrect = false;
				
			}
			
		}
		
		//NEW FROM= NOT NULL && NEW TO = NULL
		else
		if(StringHelper.isNotEmpty(newUnit.getAreaFrom()) &&
		   StringHelper.isEmpty(newUnit.getAreaTo()) )
		{
			Double newFrom = new Double(newUnit.getAreaFrom());
			
			//OLD FROM = NULL && OLD TO = NULL
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isEmpty(oldUnit.getAreaTo()))
			{
				isCorrect = false;
				
			}
			
			//OLD FROM = NOT NULL && OLD TO = NULL
			else
			if(StringHelper.isNotEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isEmpty(oldUnit.getAreaTo()))
			{
				isCorrect = false;
				
			}
			
			//OLD FROM =  NULL && OLD TO = NOT NULL
			else
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isNotEmpty(oldUnit.getAreaTo()))
			{
				Double oldTo = new Double(oldUnit.getAreaTo());
				if(oldTo >= newFrom)
					isCorrect = false;
				
			}	
		}
		
		//NEW FROM= NULL && NEW TO = NOT NULL
		else
		if(StringHelper.isEmpty(newUnit.getAreaFrom()) &&
		   StringHelper.isNotEmpty(newUnit.getAreaTo()) )
		{
			Double newTo= new Double(newUnit.getAreaTo());
			
			//OLD FROM = NULL && OLD TO = NULL
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isEmpty(oldUnit.getAreaTo()))
			{
				isCorrect = false;
				
			}
			
			//OLD FROM = NOT NULL && OLD TO = NULL
			else
			if(StringHelper.isNotEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isEmpty(oldUnit.getAreaTo()))
			{
				Double oldFrom = new Double(oldUnit.getAreaFrom());
				if(oldFrom <= newTo)
					isCorrect = false;
				
			}
			
			//OLD FROM =  NULL && OLD TO = NOT NULL
			else
			if(StringHelper.isEmpty(oldUnit.getAreaFrom()) && 
			   StringHelper.isNotEmpty(oldUnit.getAreaTo()))
			{
				isCorrect = false;
				
			}	
		}
		
		return isCorrect;
	}
}
