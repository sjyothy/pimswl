package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.ui.util.ResourceUtil;
import com.collaxa.mail.internet.ParseException;



public class AuctionUnitPriceEditBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionUnitPriceEditBackingBean.class);
	/**
	 * 
	 */
	
	public AuctionUnitPriceEditBackingBean(){
		
	}
	
	private AuctionUnitView auctionUnitView = new AuctionUnitView();

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	private AuctionUnitView dataItem = new AuctionUnitView();
	private List<AuctionUnitView> dataList = new ArrayList<AuctionUnitView>();

	private HtmlDataTable dataTable = new HtmlDataTable();
	
	private String openingPrice = "";
	private String depositAmount = "";
	
	private String maxAmount = "5000";
	private String minAmount = "0";
	private String maxAmountMessage = "";
	
	HtmlOutputLabel maxDepositLabel = new HtmlOutputLabel();
	HtmlOutputLabel openingPriceLabel = new HtmlOutputLabel();
	HtmlOutputLabel depositAmountLabel = new HtmlOutputLabel();
	HtmlInputText openingPriceField = new HtmlInputText();
	HtmlInputText depositAmountField = new HtmlInputText();
	
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	
	
	@SuppressWarnings("unchecked")
	public void saveAuctionUnitPrices(){
		/*Long auctionUnitId = 0L;
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();*/
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		logger.logInfo("saveAuctionUnitPrices() started...");
		
		try{
			if(validatePrices())
			{
				String pattern = getNumberFormat();
				Number opPriceNum = 0;
				Number depAmtNum = 0;
				Double opPrice = 0D;
				Double depAmt = 0D;

	   		    List<AuctionUnitView> selectedAuctionUnits = (List<AuctionUnitView>)viewMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
	   		    String popupMode = (String)viewMap.get("POPUP_MODE");
	   		    for(AuctionUnitView auctionUnitView: selectedAuctionUnits){
	   		    	
					if(popupMode!=null){
						if(popupMode.equals(WebConstants.OPENINIG_PRICE)){
							opPriceNum = getNumberValue(openingPrice, pattern);
							opPrice = opPriceNum.doubleValue();
							auctionUnitView.setOpeningPrice(opPrice);
						}
						else if(popupMode.equals(WebConstants.DEPOSIT_AMOUNT)){
							depAmtNum = getNumberValue(depositAmount, pattern);
							depAmt = depAmtNum.doubleValue();
							auctionUnitView.setDepositAmount(depAmt);
						}
						else{
							opPriceNum = getNumberValue(openingPrice, pattern);
							opPrice = opPriceNum.doubleValue();
							depAmtNum = getNumberValue(depositAmount, pattern);
							depAmt = depAmtNum.doubleValue();
							auctionUnitView.setOpeningPrice(opPrice);
							auctionUnitView.setDepositAmount(depAmt);
						}
					}
	   		    }
	   		    
	   		    sessionMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedAuctionUnits);
	   		    sessionMap.put("POPUP_MODE",popupMode);
	   		    
	   		    FacesContext facesContext = FacesContext.getCurrentInstance();
	   		
		        String javaScriptText = "javascript:closeWindowSubmit();";
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			
			logger.logInfo("saveAuctionUnitPrices() completed successfully!!!");
		}
    	/*catch(PimsBusinessException exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}*/
    	catch(Exception exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}
    	
	}
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	public String getInvalidMessage(String field){
		logger.logInfo("getInvalidMessage(String) started...");
		StringBuffer message = new StringBuffer("");
		
		try
		{
		message = message.append(getBundleMessage(field)).append(" ").append(getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}
	
	public Number getNumberValue(String doubleStr, String pattern) throws Exception{
		logger.logInfo("getNumberValue started...");
		Number numberVal = null;
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			numberVal = df.parse(doubleStr);
			logger.logInfo("getNumberValue completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("getNumberValue crashed!!!", exception);
			throw exception;
		}
		return numberVal;
	}
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}
	
	public Boolean validatePrices(){
		logger.logInfo("validatePrices() started...");
		Boolean validated = true;
		
    	try{
    		String pattern = getNumberFormat();
    		String popupMode = (String)viewMap.get("POPUP_MODE");
    		Boolean validateOpeningPrice = false;
    		Boolean validateDepositAmount = false;

    		if(popupMode!=null){
    			if(popupMode.equals(WebConstants.OPENINIG_PRICE)){
    				validateOpeningPrice = true;
    			}
    			else if(popupMode.equals(WebConstants.DEPOSIT_AMOUNT)){
    				validateDepositAmount = true;
    			}
    			else{
    				validateOpeningPrice = true;
    				validateDepositAmount = true;
    			}
    		}
    		
    		Number maxAmt = 0;
    		try{
    			maxAmt = getNumberValue(maxAmount,pattern);
    		} catch(Exception e){
    			validated = false;
    		}
    		
    		if(validateOpeningPrice || (validateDepositAmount && validateOpeningPrice)){
	    		Number opPrice = 0;
	    		try{
	    			if(openingPrice.equals(""))
	    			{
	    				errorMessages.clear();
		    			validated = false;
		    			errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.AuctionUnit.OPENING_PRICE));
		    			return validated;
	    			}
	    			else{
		    			opPrice = getNumberValue(openingPrice,pattern);
		    			String opStr = openingPrice.replace(',', '0');
		    			Double temp = new Double(opStr);
	    			}
	    		} catch(Exception e){
	    			errorMessages.clear();
	    			validated = false;
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.OPENING_PRICE));
	    			return validated;
	    		}
    		
	    		if(opPrice.doubleValue()<0D)
	    		{
	    			errorMessages.clear();
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.OPENING_PRICE));
	    			validated = false;
	    			return validated;
	    		}
    		}
    		
    		if(validateDepositAmount || (validateDepositAmount && validateOpeningPrice)){
	    		Number depAmt = 0;
	    		try{
	    			depAmt = getNumberValue(depositAmount,pattern);
	    			String depStr = depositAmount.replace(',', '0');
	    			Double temp = new Double(depStr);
	    		} catch(Exception e){
	    			validated = false;
	    			errorMessages.clear();
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.DEPOSIT_AMOUNT));
	    			return validated;
	    		}
	    		
	    		if(depAmt.doubleValue()<0D)
	    		{
	    			errorMessages.clear();
	    			errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.AuctionUnit.DEPOSIT_AMOUNT));
	    			validated = false;
	    			return validated;
	    		}
	    		else if(depAmt.doubleValue()>maxAmt.doubleValue())
	    		{
	    			errorMessages.clear();
	    			errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.Validation.MAX_DEPOSIT));
	    			validated = false;
	    			return validated;
	    		}
    		}
    		
	        logger.logInfo("validatePrices() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("validatePrices() crashed ", exception);
		}
		return validated;
	}
	
    public void cancel(ActionEvent event) {
    	logger.logInfo("cancel() started...");
        FacesContext facesContext = FacesContext.getCurrentInstance();

        String javaScriptText = "window.close();";
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        logger.logInfo("cancel() completed successfully!!!");
    }

    public void done(ActionEvent event) {
    	logger.logInfo("done() started...");
    	try{
    		
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	
	        String javaScriptText = "window.closeWindowSubmit();";
	        
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("done() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("done() crashed ", exception);
		}
    }
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
    
 
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				if(sessionMap.containsKey("POPUP_MODE"))
				{
					String popupMode = (String)sessionMap.get("POPUP_MODE");
					if(popupMode!=null){
						viewMap.put("POPUP_MODE",popupMode);
					}
					sessionMap.remove("POPUP_MODE");
				}
				if(sessionMap.containsKey(WebConstants.CHOSEN_AUCTION_UNITS))
				{
					List<AuctionUnitView> selectedAuctionUnits = (List<AuctionUnitView>)sessionMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
					viewMap.put(WebConstants.CHOSEN_AUCTION_UNITS, selectedAuctionUnits);
					sessionMap.remove(WebConstants.CHOSEN_AUCTION_UNITS);
				}
			}
			if(sessionMap.containsKey(WebConstants.MAX_DEPOSIT_AMOUNT)){
				maxAmount = (String)sessionMap.get(WebConstants.MAX_DEPOSIT_AMOUNT);
				maxAmountMessage = getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.MAX_DEPOSIT_AMOUNT)+ " : " + maxAmount;
			}
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		
		applyMode();
	}

	private void applyMode(){
		String popupMode = (String)viewMap.get("POPUP_MODE");
		if(popupMode!=null){
			if(popupMode.equals(WebConstants.OPENINIG_PRICE)){
				openingPriceLabel.setRendered(true);
				openingPriceField.setRendered(true);
				depositAmountLabel.setRendered(false);
				depositAmountField.setRendered(false);
				maxDepositLabel.setRendered(false);
			}
			else if(popupMode.equals(WebConstants.DEPOSIT_AMOUNT)){
				openingPriceLabel.setRendered(false);
				openingPriceField.setRendered(false);
				depositAmountLabel.setRendered(true);
				depositAmountField.setRendered(true);
				maxDepositLabel.setRendered(true);
			}
			else{
				openingPriceLabel.setRendered(true);
				depositAmountLabel.setRendered(true);
				openingPriceField.setRendered(true);
				depositAmountField.setRendered(true);
				maxDepositLabel.setRendered(true);
			}
		}
		
	}
	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		UIViewRoot view = (UIViewRoot) session.getAttribute("view");		
		isEnglishLocale =  view.getLocale().toString().equals("en");
		return isEnglishLocale;
	}


	/**
	 * @return the auctionUnitView
	 */
	public AuctionUnitView getAuctionUnitView() {
		return auctionUnitView;
	}


	/**
	 * @param auctionUnitView the auctionUnitView to set
	 */
	public void setAuctionUnitView(AuctionUnitView auctionUnitView) {
		this.auctionUnitView = auctionUnitView;
	}



	/**
	 * @return the openingPrice
	 */
	public String getOpeningPrice() {
		return openingPrice;
	}


	/**
	 * @param openingPrice the openingPrice to set
	 */
	public void setOpeningPrice(String openingPrice) {
		this.openingPrice = openingPrice;
	}


	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}


	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}


	/**
	 * @return the maxAmount
	 */
	public String getMaxAmount() {
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		if(sessionMap.containsKey(WebConstants.MAX_DEPOSIT_AMOUNT))
			maxAmount = (String)sessionMap.get(WebConstants.MAX_DEPOSIT_AMOUNT);
		return maxAmount;
	}


	/**
	 * @param maxAmount the maxAmount to set
	 */
	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}


	/**
	 * @return the minAmount
	 */
	public String getMinAmount() {
		return minAmount;
	}


	/**
	 * @param minAmount the minAmount to set
	 */
	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}


	/**
	 * @return the maxAmountMessage
	 */
	public String getMaxAmountMessage() {
		return maxAmountMessage;
	}


	/**
	 * @param maxAmountMessage the maxAmountMessage to set
	 */
	public void setMaxAmountMessage(String maxAmountMessage) {
		this.maxAmountMessage = maxAmountMessage;
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}


	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}


	public AuctionUnitView getDataItem() {
		return dataItem;
	}


	public void setDataItem(AuctionUnitView dataItem) {
		this.dataItem = dataItem;
	}


	public List<AuctionUnitView> getDataList() {
		dataList = (List<AuctionUnitView>)viewMap.get(WebConstants.CHOSEN_AUCTION_UNITS);
		return dataList;
	}


	public void setDataList(List<AuctionUnitView> dataList) {
		if(dataList!=null)
			viewMap.put(WebConstants.CHOSEN_AUCTION_UNITS,dataList);
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}


	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public HtmlOutputLabel getOpeningPriceLabel() {
		return openingPriceLabel;
	}


	public void setOpeningPriceLabel(HtmlOutputLabel openingPriceLabel) {
		this.openingPriceLabel = openingPriceLabel;
	}


	public HtmlOutputLabel getDepositAmountLabel() {
		return depositAmountLabel;
	}


	public void setDepositAmountLabel(HtmlOutputLabel depositAmountLabel) {
		this.depositAmountLabel = depositAmountLabel;
	}


	public HtmlInputText getOpeningPriceField() {
		return openingPriceField;
	}


	public void setOpeningPriceField(HtmlInputText openingPriceField) {
		this.openingPriceField = openingPriceField;
	}


	public HtmlInputText getDepositAmountField() {
		return depositAmountField;
	}


	public void setDepositAmountField(HtmlInputText depositAmountField) {
		this.depositAmountField = depositAmountField;
	}


	public HtmlOutputLabel getMaxDepositLabel() {
		return maxDepositLabel;
	}


	public void setMaxDepositLabel(HtmlOutputLabel maxDepositLabel) {
		this.maxDepositLabel = maxDepositLabel;
	}


}