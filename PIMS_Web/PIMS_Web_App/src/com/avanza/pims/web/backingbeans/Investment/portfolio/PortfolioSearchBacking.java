package com.avanza.pims.web.backingbeans.Investment.portfolio;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PortfolioServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PortfolioView;

public class PortfolioSearchBacking extends AbstractController 
{	
	private static final long serialVersionUID = -5739423213522221787L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private boolean disable=false;
	
	private PortfolioServiceAgent portfolioServiceAgent;
	
	private String pageMode;
	private String portfolioStatus;
	private PortfolioView portfolioView;
	private List<PortfolioView> portfolioViewList;
	private List<Long> alreadySelectedPortfolioList;
	private DomainDataView domainDataView=new DomainDataView();
	UtilityServiceAgent usa=new UtilityServiceAgent();
	
	private HtmlDataTable dataTable;
	
	public PortfolioSearchBacking() 
	{
		logger = Logger.getLogger( PortfolioSearchBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		portfolioServiceAgent = new PortfolioServiceAgent();
		pageMode = WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SEARCH;
		portfolioView = new PortfolioView();
		portfolioViewList = new ArrayList<PortfolioView>(0);
		alreadySelectedPortfolioList = new ArrayList<Long>(0);
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			domainDataView=usa.getDomainDataByValue(WebConstants.Portfolio.PORTFOLIO_STATUS_NEW);
			if(portfolioStatus!=null)
			{
				portfolioView.setPortfolioId(domainDataView.getDomainDataId());
				disable=true;
			}
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY );
		}
		if ( sessionMap.get( WebConstants.Portfolio.PORTFOLIO_STATUS ) != null )
		{
			portfolioStatus = (String) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_STATUS );
			portfolioView.setStatusId(domainDataView.getDomainDataId().toString());
			disable=true;
			sessionMap.remove( WebConstants.Portfolio.PORTFOLIO_STATUS );			
		}
		else if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_STATUS ) != null )
		{
			portfolioStatus = (String) viewMap.get( WebConstants.Portfolio.PORTFOLIO_STATUS );
		}
		
		// PORTFOLIO VIEW
		if ( viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW ) != null )
		{
			portfolioView = (PortfolioView) viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW );
		}
		
		
		// PORTFOLIO VIEW LIST
		if( viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST ) != null )
		{
			portfolioViewList = (List<PortfolioView>) viewMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST );
		}
		
		
		// ALREADY SELECTED PORTFOLIO LIST
		if ( sessionMap.get( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST ) != null )
		{
			alreadySelectedPortfolioList = (List<Long>) sessionMap.get( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST );
			sessionMap.remove( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST );
		}
		else if ( viewMap.get( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST ) != null )
		{
			alreadySelectedPortfolioList = (List<Long>) viewMap.get( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST );
		}
		
				
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY, pageMode );
		}
		// Portfoliol status		
		if ( portfolioStatus != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_STATUS, portfolioStatus );
		}
		
		
		// PORTFOLIO VIEW
		if ( portfolioView != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW, portfolioView );
		}
		
		
		// PORTFOLIO VIEW LIST
		if ( portfolioViewList != null )
		{
			viewMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST, portfolioViewList );
		}
		
	
		// ALREADY SELECTED PORTFOLIO LIST
		if ( alreadySelectedPortfolioList != null )
		{
			viewMap.put( WebConstants.Portfolio.ALREADY_SELECTED_PORTFOLIO_VIEW_LIST, alreadySelectedPortfolioList );
		}
	}
	
	public Boolean getIsSearchMode()
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SEARCH ) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Boolean getIsMultiSelectPopupMode()
	{
		Boolean isMultiSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP ) )
			isMultiSelectPopupMode = true;
		
		return isMultiSelectPopupMode;
	}
	
	public Boolean getIsSingleSelectPopupMode() 
	{
		Boolean isSingleSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP ) )
			isSingleSelectPopupMode = true;
		
		return isSingleSelectPopupMode;
	}
	
	public String onSearch() 
	{
		String METHOD_NAME = "onSearch()";		
		String path = null;
		
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			if(portfolioStatus!=null)
			{
				portfolioView.setStatusId(domainDataView.getDomainDataId().toString());
				disable=true;
			}
			portfolioView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			portfolioView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );			
			portfolioViewList = portfolioServiceAgent.getPortfolioList( portfolioView, alreadySelectedPortfolioList );
			
			updateValuesToMaps();
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onAdd() 
	{
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY , WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_ADD );
		return "portfolioManage";
	}
	
	public String onEdit() 
	{
		PortfolioView selectedPortfolio = (PortfolioView) dataTable.getRowData();
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW , selectedPortfolio );
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY , WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_EDIT );
		return "portfolioManage";
	}
	
	public String onView() 
	{
		PortfolioView selectedPortfolio = (PortfolioView) dataTable.getRowData();
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW , selectedPortfolio );
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_KEY , WebConstants.Portfolio.PORTFOLIO_MANAGE_PAGE_MODE_VIEW );
		return "portfolioManage";
	}
	
	public String onClose()
	{
		PortfolioView selectedPortfolio = (PortfolioView) dataTable.getRowData();
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW , selectedPortfolio );
		return "portfolioManage";
	}
	
	public String onDelete()
	{		
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		Boolean isDeleted = false;
		
		PortfolioView selectedPortfolio = (PortfolioView) dataTable.getRowData();
		selectedPortfolio.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedPortfolio.setIsDeleted( 1L );
		
		try
		{
			isDeleted = portfolioServiceAgent.updatePortfolio( selectedPortfolio );
			
			if ( isDeleted )
			{
				portfolioViewList.remove( selectedPortfolio );
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_DELETE_SUCCESS ) );
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
		}
		catch (Exception exception) 
		{	
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.PORTFOLIO_DELETE_ERROR ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;
	}
	
	public String onSingleSelect()
	{
		PortfolioView selectedPortfolio = (PortfolioView) dataTable.getRowData();
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW , selectedPortfolio );
		executeJavascript("closeAndPassSelected();");
		return null;
	}
	
	public String onMultiSelect()
	{
		List<PortfolioView> selectedPortfolioList = new ArrayList<PortfolioView>(0);
		
		if ( portfolioViewList != null )
		{			
			for ( PortfolioView portfolioView : portfolioViewList )
			{
				if ( portfolioView.getIsSelected()!=null &&  portfolioView.getIsSelected() )
				{
					selectedPortfolioList.add( portfolioView );
				}
			}			
		}
		
		if ( selectedPortfolioList.size() == 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Portfolio.NO_PORTFOLIO_SELECTED_ERROR ) );
		}
		else
		{
			sessionMap.put( WebConstants.Portfolio.PORTFOLIO_VIEW_LIST , selectedPortfolioList );
			executeJavascript("closeAndPassSelected();");
		}
		
		return null;
	}
	
	public Integer getRecordSize() 
	{
		return portfolioViewList.size();
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
	}

	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{	
		super.prerender();
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public PortfolioServiceAgent getPortfolioServiceAgent() {
		return portfolioServiceAgent;
	}

	public void setPortfolioServiceAgent(PortfolioServiceAgent portfolioServiceAgent) {
		this.portfolioServiceAgent = portfolioServiceAgent;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public PortfolioView getPortfolioView() {
		return portfolioView;
	}

	public void setPortfolioView(PortfolioView portfolioView) {
		this.portfolioView = portfolioView;
	}

	public List<PortfolioView> getPortfolioViewList() {
		return portfolioViewList;
	}

	public void setPortfolioViewList(List<PortfolioView> portfolioViewList) {
		this.portfolioViewList = portfolioViewList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public List<Long> getAlreadySelectedPortfolioList() {
		return alreadySelectedPortfolioList;
	}

	public void setAlreadySelectedPortfolioList(
			List<Long> alreadySelectedPortfolioList) {
		this.alreadySelectedPortfolioList = alreadySelectedPortfolioList;
	}

	public String getPortfolioStatus() {
		return portfolioStatus;
	}

	public void setPortfolioStatus(String portfolioStatus) {
		this.portfolioStatus = portfolioStatus;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}
}
