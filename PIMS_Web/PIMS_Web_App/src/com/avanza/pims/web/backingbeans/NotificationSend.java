package com.avanza.pims.web.backingbeans;

import java.util.Date;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelInfo;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.PriorityType;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.PaymentManager;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.NotificationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.notificationservice.notification.NotificationState;

public class NotificationSend extends AbstractController {
	private transient Logger logger = Logger.getLogger(NotificationSend.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

	private ServletContext context = (ServletContext) FacesContext
			.getCurrentInstance().getExternalContext().getContext();

	private Map<String, Long> auctionStatusMap = new HashMap<String, Long>();
	private Map<String, String> notificationStatusMap = new HashMap<String, String>();

	public NotificationSend() {
		System.out.println("NotificationSend Constructor");

	}

	private String message;

	private AuctionView auctionView = new AuctionView();

	private NotificationView dataItem = new NotificationView();
	private List<NotificationView> dataList = new ArrayList<NotificationView>();

	private NotificationView notificationView = new NotificationView();

	private List<SelectItem> auctionStatuses = new ArrayList<SelectItem>();
	private List<SelectItem> notificationStatuses = new ArrayList<SelectItem>();

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String hhStart = "";
	private String mmStart = "";
	private String hhEnd = "";
	private String mmEnd = "";
	Map sessionMap;
	private List<String> errorMessages;
	private String infoMessage = "";

	private HtmlDataTable dataTable;

	private HtmlInputHidden addCount = new HtmlInputHidden();

	private String sortField = null;
	private Boolean sortAscending = true;

	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;

	// Search Criteria
	private String srchProcedureType = null;
	private String srchChannelType = null;
	private String srchRequestNumber = null;
	private String srchStatus = null;
	private String srchContractNumber = null;
	private String srchSubject = null;
	private String srchPersonName = null;

	private Date srchSendDate = null;
	private Date srchNotificationDate = null;

	public Map<String, String> getNotificationStatusMap() {
		return notificationStatusMap;
	}

	public void setNotificationStatusMap(
			Map<String, String> notificationStatusMap) {
		this.notificationStatusMap = notificationStatusMap;
	}

	public NotificationView getNotificationView() {
		return notificationView;
	}

	public void setNotificationView(NotificationView notificationView) {
		this.notificationView = notificationView;
	}

	public String getSrchProcedureType() {
		return srchProcedureType;
	}

	public void setSrchProcedureType(String srchProcedureType) {
		this.srchProcedureType = srchProcedureType;
	}

	public String getSrchChannelType() {
		return srchChannelType;
	}

	public void setSrchChannelType(String srchChannelType) {
		this.srchChannelType = srchChannelType;
	}

	public String getSrchRequestNumber() {
		return srchRequestNumber;
	}

	public void setSrchRequestNumber(String srchRequestNumber) {
		this.srchRequestNumber = srchRequestNumber;
	}

	public String getSrchStatus() {
		return srchStatus;
	}

	public void setSrchStatus(String srchStatus) {
		this.srchStatus = srchStatus;
	}

	public String getSrchContractNumber() {
		return srchContractNumber;
	}

	public void setSrchContractNumber(String srchContractNumber) {
		this.srchContractNumber = srchContractNumber;
	}

	public String getSrchSubject() {
		return srchSubject;
	}

	public void setSrchSubject(String srchSubject) {
		this.srchSubject = srchSubject;
	}

	public String getSrchPersonName() {
		return srchPersonName;
	}

	public void setSrchPersonName(String srchPersonName) {
		this.srchPersonName = srchPersonName;
	}

	public Date getSrchSendDate() {
		return srchSendDate;
	}

	public void setSrchSendDate(Date srchSendDate) {
		this.srchSendDate = srchSendDate;
	}

	public Date getSrchNotificationDate() {
		return srchNotificationDate;
	}

	public void setSrchNotificationDate(Date srchNotificationDate) {
		this.srchNotificationDate = srchNotificationDate;
	}

	/*
	 * Methods
	 */
	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	public void clearSessionMap() {
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try {
			//			sessionMap.remove("auctionList");
			/*sessionMap.remove(WebConstants.FROM_SEARCH);
			sessionMap.remove(WebConstants.AUCTION_STATUS_MAP);
			sessionMap.remove(WebConstants.ROW_ID);
			sessionMap.remove(WebConstants.FIRST_TIME);
			sessionMap.remove(WebConstants.VIEW_MODE);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.SELECT_VENUE_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
			sessionMap.remove(WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ID);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.FULL_VIEW_MODE);*/
			logger.logInfo("clearSessionMap() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}

	public void  sendSelectedNotification()
    {
		
    	String methodName ="sendSelectedNotification";
    	
    	PropertyServiceAgent psa = new PropertyServiceAgent();
    	
    	logger.logInfo(methodName+"|"+"Start...");
    	
    	List<NotificationView> notificationViewList=new ArrayList<NotificationView>(5);
    	
    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
		.getAttributes();
    	
    	if( viewMap.containsKey("dataListForNotification") ){
    		
    		notificationViewList=(List<NotificationView>) viewMap.get("dataListForNotification"); 
    		int count = 0;
    	for (NotificationView notficationViewObj : notificationViewList) 
    	{
    		Notification notificationToBeSent = null;
    	if(notficationViewObj.getSelected() != null && !notficationViewObj.equals(""))
    	 {
    		if(notficationViewObj.getSelected())
    		{
    			
    		 	// Get Protocol Adapter
    			
    			ChannelCatalog catalog = ChannelCatalog.getInstance();
    			ProtocolAdapter protocolAdapter = catalog.getAdapter(notficationViewObj.getChannelId());
    			
    			// Transform to Notification Object
    			
    			if(notficationViewObj.getRecipientToList()== null || notficationViewObj.getRecipientToList().equals("") ||  notficationViewObj.getRecipientToList().equals("null")){
    				
    			}
    			else if(notficationViewObj.getRecipientToList() != null  || !notficationViewObj.getRecipientToList().equals("") || !notficationViewObj.getRecipientToList().equals("null")){
    				 notificationToBeSent = transformToNotification( notficationViewObj );
    			}
    			
    			// Call Notify of Adapter
    			if( notificationToBeSent != null ){
    				protocolAdapter.notify(notificationToBeSent);
    				count ++;
    			}
    			
    			
    			
             // Some Message handling
//    		 if(psa.addChequeStatus(paySchId, statusId,chequeNo)){
//    		        	errorMessages = new ArrayList<String>();
//    					errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeBounced"));
//           		 }
//    		 else{
    			   	errorMessages = new ArrayList<String>();
					errorMessages.add(count + " message(s) processed successfully.");
//    		 }
    		 
    		 logger.logInfo("Notification Processed :" + notficationViewObj.getNotificationId());
    		
    		}
    	  }
		}
    }	                                 
    	logger.logInfo(methodName+"|"+"End...");
    	viewMap.clear();
    	
    	//loadDataList();
       //return "btnBounce_Click";
   }
	
	public Notification transformToNotification(NotificationView notificationView){
		
		Notification notification = new Notification();
		
		notification.setNotificationId( notificationView.getNotificationId() );
		notification.setServerInstanceId(notificationView.getServerInstanceId() );
		notification.setBody( notificationView.getBody() );
		notification.setChannelId( notificationView.getChannelId() );
		notification.setContent(null );
		notification.setCreatedBy( notificationView.getCreatedBy() );
		notification.setCreatedOn( notificationView.getCreatedOn() );
		notification.setDeleted(new Boolean(false) );
		notification.setDocumentList( null );
		
		notification.setExternalId1( notificationView.getExternalId1() );
		notification.setExternalId2( notificationView.getExternalId2() );
		notification.setExternalId3( notificationView.getExternalId3() );
		notification.setLocale( notificationView.getLocale() );
		
		

				
		notification.setNsFrom( notificationView.getNsFrom() );
		
		//  Lowest(0), Low(1), Medium(2), High(3), Highest(4);
		if( notificationView.getPriority() != null ){
			if( notificationView.getPriority().equals("Lowest") ){
				
				notification.setPriority( PriorityType.Lowest ) ;
			}
			else if( notificationView.getPriority().equals("Low") ){
				
				notification.setPriority( PriorityType.Low ) ;
			}
			else if( notificationView.getPriority().equals("Medium") ){
						
						notification.setPriority( PriorityType.Medium ) ;
					}
			else if( notificationView.getPriority().equals("High") ){
				
				notification.setPriority( PriorityType.High ) ;
			}
			else if( notificationView.getPriority().equals("Highest") ){
				
				notification.setPriority( PriorityType.Highest ) ;
			}
			else{
				
				notification.setPriority( PriorityType.Medium ) ;
			}
		}else{
			notification.setPriority( PriorityType.Medium ) ;
		}
		
		notification.setProtocolFields( null );
		notification.setRecipientBccList( notificationView.getRecipientBccList() );
		notification.setRecipientCcList( notificationView.getRecipientCcList() );
		notification.setRecipientToList( notificationView.getRecipientToList() );
		notification.setRecipientToList( notificationView.getRecipientToList() );
		
		notification.setRecordStatus( notificationView.getRecordStatus() );
		notification.setRetryCount(  notificationView.getRetryCount() );
		
		notification.setSentTime( notificationView.getSentTime() );
		
		if(notificationView.getState() != null ){
		if( notificationView.getState().equals("None") ){
			
			notification.setNotificationState( NotificationState.None ) ;
		}
		else if( notificationView.getState().equals("New") ){
			
			notification.setNotificationState( NotificationState.New ) ;
		}
		else if( notificationView.getState().equals("Wait") ){
					
					notification.setNotificationState( NotificationState.Wait ) ;
				}
		else if( notificationView.getState().equals("Sent") ){
			
			notification.setNotificationState( NotificationState.Sent ) ;
		}
		else if( notificationView.getState().equals("Failed") ){
			
			notification.setNotificationState( NotificationState.Failed ) ;
		}
		else if( notificationView.getState().equals("Expired") ){
			
			notification.setNotificationState( NotificationState.Expired ) ;
		}
		else{
			
			notification.setNotificationState( NotificationState.None ) ;
		}
		}
		else{
			notification.setNotificationState( NotificationState.None ) ;
		}
		notification.setStatusText(  notificationView.getStatusText() );
		notification.setServerInstanceId( notificationView.getServerInstanceId() );
		notification.setSubject( notificationView.getSubject() );
		if( notificationView.getTimeOut() != null){
			notification.setTimeOut( notificationView.getTimeOut() );
		}
		else{
			notification.setTimeOut(60 );
		}
		notification.setUpdatedBy( notificationView.getUpdatedBy() );
		notification.setUpdatedOn( notificationView.getUpdatedOn() );
		
		if( notificationView.getTimeOut() != null){
			notification.setWaitTime( notificationView.getWaitTime() );
		}
		else{
			notification.setWaitTime( 60 );
		}
		notification.setUpdation( notificationView.getUpdatedBy() );

		return notification;
	}

	
	@SuppressWarnings("unchecked")
	public void openDetailsPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openDetailsPopup() started...");
		try
		{
	        final String viewId = "/NotificationSend.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	        String javaScriptText = "javascript:showDetailPopup();";
	
	        // Add the Java script to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openDetailsPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openDetailsPopup() crashed ", exception);
		}
    }
	
	public String gotoGenerateNotification()
	{
		String methodName="gotoGenerateNotification";
		logger.logInfo(methodName+"|"+"Start..");

		
		
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "generateNotification";
	}
	
	
	
	
	public String openDetailsPopupForView()
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");

		
		openDetailsPopup("VIEW");
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	
	public String openDetailsPopupForEdit()
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");

		
		openDetailsPopup("EDIT");
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	public String openDetailsPopup(String screenMode)
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");

		
		        FacesContext facesContext = FacesContext.getCurrentInstance();
		        NotificationView notificationViewRow =(NotificationView) dataTable.getRowData();
		        
//		        String extrajavaScriptText ="var screen_width = screen.width;"+
//                " var screen_height = screen.height;"+
//                " window.open('receivePayment.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=no');";

		        String extrajavaScriptText = " var screen_width = screen.width; " +
		        " var screen_height = screen.height; " +
		        " var popup_width = screen_width-590; " +
		        " var popup_height = screen_height-450; " +
		        " var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20; " +
	              
		        " var popup = window.open('NotificationDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes'); " +
		        " popup.focus(); " ;
		        
		        sessionMap.put( WebConstants.NOTIFICATION_VIEW_OBJECT , notificationViewRow);
		        sessionMap.put( WebConstants.NOTIFICATION_SCREEN_MODE , screenMode);
		        
		        openPopUp("NotificationDetails.jsf", extrajavaScriptText);
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/NotificationSend.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public Boolean getIsEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}

	@SuppressWarnings("unchecked")
	public List<NotificationView> getAuctionDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		
		return dataList;
	}

	@SuppressWarnings("unchecked")
	private void loadAuctionList() {

		logger.logInfo("loadAuctionList() started...");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		notificationStatusMap = (Map<String, String>) sessionMap
				.get(WebConstants.NOTIFICATION_STATUS_MAP);

		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

		try {
			dataList = new ArrayList<NotificationView>();

			String dateFormat = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			List<NotificationView> auctionList = null;
			auctionList = dataList;
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put("dataListForNotification", auctionList);
			recordSize = auctionList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
 
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);

			Long statusId = null;
			if (!auctionList.isEmpty()) {
				for (NotificationView temp : auctionList) {
					

					if (statusId.compareTo(auctionStatusMap
							.get(WebConstants.Statuses.AUCTION_READY_STATUS)) == 0) {
					
					}
					dataList.add(temp);
				}
			} else {
				errorMessages = new ArrayList<String>();
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadAuctionList() completed successfully!!!");
		}

		catch (Exception exception) {
			logger.LogException("loadAuctionList() crashed ", exception);
		}
	}

	/*
	 *  Validation Methods
	 */

	public String getInvalidMessage(String field) {

		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try {
			message = message
					.append(getBundleMessage(field))
					.append(" ")
					.append(
							getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
			logger
					.logInfo("getInvalidMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger
					.LogException("getInvalidMessage(String) crashed ",
							exception);
		}
		return message.toString();
	}

	public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		if (((hhStart.trim().length() > 0) && (mmStart.trim().length() == 0))
				|| ((mmStart.trim().length() > 0) && (hhStart.trim().length() == 0))) {
			errorMessages
					.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.START_TIME));
			validated = false;
		}
		if (((hhEnd.trim().length() > 0) && (mmEnd.trim().length() == 0))
				|| ((mmEnd.trim().length() > 0) && (hhEnd.trim().length() == 0))) {
			errorMessages
					.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.END_TIME));
			validated = false;
		}

		if (auctionView.getAuctionDateVal() == null) {
			hhStart = hhStart.trim();
			mmStart = mmStart.trim();
			hhEnd = hhEnd.trim();
			mmEnd = mmEnd.trim();
			String tempStartTime = hhStart + mmStart;
			String tempEndTime = hhEnd + mmEnd;
			if ((!tempStartTime.equals("")) || (!tempEndTime.equals(""))) {
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.SHOULD_HAVE_AUCTION_DATE));
				validated = false;
			}
		}

		if (validated) {
			auctionView.setAuctionStartTime(hhStart + ":" + mmStart);
			auctionView.setAuctionEndTime(hhEnd + ":" + mmEnd);
		}
		return validated;
	}

	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);

	}

	/*
	 *  JSF Lifecycle methods 
	 */

	@Override
	public void init() {
			
		super.init();
		
		sessionMap =FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) {
				clearSessionMap();
				loadStatusMap();
			}

			if (dataList != null && dataList.size() > 0) {

				List<NotificationView> auctionList = null;
				auctionList = dataList;
				Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
						.getAttributes();
				viewMap.put("dataListForNotification", auctionList);
				recordSize = auctionList.size();
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize / paginatorRows;

				if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewMap.put("paginatorMaxPages", paginatorMaxPages);
			}
			logger.logInfo("init() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}

	}
	
	


	@Override
	public void preprocess() {
		
		super.preprocess();
		logger.logInfo("NotificationSend preprocess");
	}

	@Override
	public void prerender() {
	
		super.prerender();
		logger.logInfo("NotificationSend prerender");
	}

	private void loadStatusMap() {

		//		None, New, Sent, Failed, Expired, Wait

		notificationStatusMap.put("None", "None");
		notificationStatusMap.put("New", "New");
		notificationStatusMap.put("Sent", "Sent");

		notificationStatusMap.put("Failed", "Failed");
		notificationStatusMap.put("Expired", "Expired");
		notificationStatusMap.put("Wait", "Wait");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		sessionMap.put(WebConstants.NOTIFICATION_STATUS_MAP,
				notificationStatusMap);
	}

	/*
	 * Button Click Action Handlers 
	 */

	public String auctionDetailsEditMode() {
		logger.logInfo("auctionDetailsEditMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.EDIT_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			setRequestParam(WebConstants.BACK_SCREEN,
					WebConstants.BACK_SCREEN_AUCTION_SEARCH);
			//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsEditMode() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("auctionDetailsEditMode() crashed ", exception);
		}
		return "auctionDetailsEditMode";
	}

	public String notificationDetailsEditMode() {
		logger.logInfo("notificationDetailsEditMode() started...");
		try {
			clearSessionMap();

			NotificationView notifcationView = (NotificationView) dataTable
					.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.NOTIFICATION_ID, notifcationView
					.getNotificationId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.EDIT_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			setRequestParam(WebConstants.BACK_SCREEN,
					WebConstants.BACK_SCREEN_AUCTION_SEARCH);

			logger
					.logInfo("notificationDetailsEditMode() completed successfully!!!");
		}

		catch (Exception exception) {
			logger.LogException("auctionDetailsEditMode() crashed ", exception);
		}
		return "notificationDetailsEditMode";
	}

	public String notificationDetailsViewMode() {
		logger.logInfo("notificationDetailsViewMode() started...");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		notificationStatusMap = (Map<String, String>) sessionMap
				.get(WebConstants.NOTIFICATION_STATUS_MAP);

		try {
			clearSessionMap();

			NotificationView notificationView = (NotificationView) dataTable
					.getRowData();

			sessionMap.put(WebConstants.NOTIFICATION_ID, notificationView
					.getNotificationId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);

			//if((statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0) || (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_DATE_VENUE_DECIDED_STATUS))==0) || (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_READY_STATUS))==0))
			//{
			//	sessionMap.put(WebConstants.SELECT_VENUE_MODE,WebConstants.DATE_VENUE_SELECTED);
			//}

			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);

			//    	 	if(statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0){
			//    	 		return "auctionCompletedViewMode";
			//    	 	}

			logger
					.logInfo("notificationDetailsViewMode() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}

		return "notificationDetailsViewMode";
	}

	public String auctionDetailsViewMode() {
		logger.logInfo("auctionDetailsViewMode() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		auctionStatusMap = (Map<String, Long>) sessionMap
				.get(WebConstants.AUCTION_STATUS_MAP);
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Long statusId = Long.parseLong(aucView.getAuctionStatusId());
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			if ((statusId.compareTo(auctionStatusMap
					.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS)) == 0)
					|| (statusId
							.compareTo(auctionStatusMap
									.get(WebConstants.Statuses.AUCTION_DATE_VENUE_DECIDED_STATUS)) == 0)
					|| (statusId.compareTo(auctionStatusMap
							.get(WebConstants.Statuses.AUCTION_READY_STATUS)) == 0)) {
				sessionMap.put(WebConstants.SELECT_VENUE_MODE,
						WebConstants.DATE_VENUE_SELECTED);
			}
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);

			if (statusId.compareTo(auctionStatusMap
					.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS)) == 0) {
				return "auctionCompletedViewMode";
			}
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
		return "auctionDetailsViewMode";
	}

	public String auctionDetailsFullViewMode() {
		logger.logInfo("auctionDetailsViewMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.FULL_VIEW_MODE,
					WebConstants.FULL_VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
		return "auctionDetailsViewMode";
	}

	public String auctionDetailsConductMode() {
		logger.logInfo("auctionDetailsConductMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.CONDUCT_AUCTION_MODE,
					WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsConductMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsConductMode() crashed ",
					exception);
		}
		return "auctionDetailsConductMode";
	}

	public String auctionSelectDateVenueMode() {
		logger.logInfo("auctionSelectDateVenueMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.SELECT_VENUE_MODE,
					WebConstants.SELECT_VENUE_MODE);
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionSelectDateVenueMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionSelectDateVenueMode() crashed ",
					exception);
		}
		return "auctionSelectDateVenueMode";
	}

	public String searchAuctions() {
		logger.logInfo("searchAuctions() started...");
		try {
			if (validateFields()) {
				//auctionView.setAuctionStartTime(hhStart+":"+mmStart);
				loadAuctionList();
			}

			logger.logInfo("searchAuctions() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}
		return "searchAuctions";
	}

	public void searchNotificationsTest() {
		logger.logInfo("searchNotificationsTest() started...");

		List notificationViewList = new ArrayList();

		try {

			HashMap hmCriteria = bulidSearchCriteria();

			PaymentManager pm = new PaymentManager();

			List resultList = pm.getAllNotifications(hmCriteria);

			for (Object object : resultList) {

				Object[] objArray = (Object[]) object;

				NotificationView notifView = processResultRow(objArray);

				notificationViewList.add(notifView);

			}

			dataList = notificationViewList;

			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("dataListForNotification", dataList);
			String dateFormat = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			List<NotificationView> auctionList = null; //propertyServiceAgent.getAuctions(auctionView);
			auctionList = dataList;
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put("dataListForNotification", auctionList);
			recordSize = auctionList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			//			if((recordSize%paginatorRows)>0)
			//				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);

			logger
					.logInfo("searchNotificationsTest() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}

		//return "";
	}

	private HashMap bulidSearchCriteria() {

		HashMap hmSearchCriteria = new HashMap();

		if (srchProcedureType != null && !srchProcedureType.equals("")) {

			hmSearchCriteria.put("srchProcedureType", srchProcedureType);
		}

		if (srchChannelType != null && !srchChannelType.equals("")
				&& !srchChannelType.equals("0")) {

			hmSearchCriteria.put("srchChannelType", srchChannelType);
		}

		if (srchRequestNumber != null && !srchRequestNumber.equals("")) {

			hmSearchCriteria.put("srchRequestNumber", srchRequestNumber);
		}

		if (srchStatus != null && !srchStatus.equals("")
				&& !srchStatus.equals("0")) {

			hmSearchCriteria.put("srchStatus", srchStatus);
		}

		if (srchContractNumber != null && !srchContractNumber.equals("")) {

			hmSearchCriteria.put("srchContractNumber", srchContractNumber);
		}

		if (srchSubject != null && !srchSubject.equals("")) {

			hmSearchCriteria.put("srchSubject", srchSubject);
		}

		if (srchPersonName != null && !srchPersonName.equals("")) {

			hmSearchCriteria.put("srchPersonName", srchPersonName);
		}

		if (srchSendDate != null && !srchSendDate.equals("")) {

			hmSearchCriteria.put("srchSendDate", srchSendDate);
		}

		if (srchNotificationDate != null && !srchNotificationDate.equals("")) {

			hmSearchCriteria.put("srchNotificationDate", srchNotificationDate);
		}

		return hmSearchCriteria;
	}

	private NotificationView processResultRow(Object[] resultRow) {

		NotificationView nView = new NotificationView();

		nView.setNotificationId(Long.valueOf(((BigDecimal)resultRow[0]).longValue()));

		nView.setServerInstanceId(resultRow[1].toString());
		nView.setChannelId(resultRow[2].toString());
		nView.setRetryCount(Integer.valueOf(resultRow[3].toString()));

		if (resultRow[4] != null)
			nView.setRecipientToList(resultRow[4].toString());

		if (resultRow[5] != null)
			nView.setRecipientCcList(resultRow[5].toString());

		if (resultRow[6] != null)
			nView.setRecipientBccList(resultRow[6].toString());
		nView.setNsFrom(resultRow[7].toString());

		nView.setSubject(resultRow[8].toString());
		nView.setProtocolFields(null); //resultRow[9].toString() );
		nView.setState(resultRow[10].toString());
		nView.setContent(null);

		nView.setStatusText(resultRow[12].toString());
		nView.setLocale(resultRow[13].toString());
		nView.setBody(resultRow[14].toString());
		nView.setDocumentList(null);

		nView.setSentTime(resultRow[16].toString());
		// DeadNotification 17
		if (resultRow[18] != null)
			nView.setExternalId1(resultRow[18].toString());

		if (resultRow[19] != null)
			nView.setExternalId2(resultRow[19].toString());

		if (resultRow[20] != null)
			nView.setExternalId3(resultRow[20].toString());

		nView.setCreatedOn((Date) resultRow[21]);
		nView.setCreatedBy(resultRow[22].toString());

		nView.setUpdatedOn((Date) resultRow[23]);
		nView.setUpdatedBy(resultRow[24].toString());

		if (resultRow[25] != null)
			nView.setContractNo(resultRow[25].toString());

		if (resultRow[26] != null)
			nView.setRequestNo(resultRow[26].toString());

		if (resultRow[27] != null)
			nView.setPersonName(resultRow[27].toString());

		if (resultRow[28] != null)
			nView.setProcedureType(resultRow[28].toString());

		return nView;
	}

	public String searchNotifications() {
		logger.logInfo("searchNotifications() started...");
		try {
			if (validateFields()) {
				//auctionView.setAuctionStartTime(hhStart+":"+mmStart);
				loadAuctionList();
			}

			logger.logInfo("searchNotifications() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}
		return "searchAuctions";
	}

	public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "window.close();";

			clearSessionMap();

			// Add the Javascript to the rendered page's header for immediate execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("cancel() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
	}

	/*
	 * Setters / Getters
	 */

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
		//		AuctionVenueView temp = new AuctionVenueView();
		//		temp.setVenueName(auctionView.getVenueName());
		//		auctionView.setAuctionVenue(temp);
		return auctionView;
	}

	/**
	 * @param auctionView the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
		//		AuctionVenueView temp = new AuctionVenueView();
		//		temp.setVenueName(auctionView.getVenueName());
		//		auctionView.setAuctionVenue(temp);
		this.auctionView = auctionView;
	}

	/**
	 * @return the dataItem
	 */
	public NotificationView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(NotificationView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<NotificationView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		//		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		dataList = (List<NotificationView>) viewMap
				.get("dataListForNotification");
		if (dataList == null)
			dataList = new ArrayList<NotificationView>();
		//return dataList;
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<NotificationView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}


	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the auctionStatuses
	 */
	public List<SelectItem> getAuctionStatuses() {
		return auctionStatuses;
	}

	/**
	 * @param auctionStatuses the auctionStatuses to set
	 */
	public void setAuctionStatuses(List<SelectItem> auctionStatuses) {
		this.auctionStatuses = auctionStatuses;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(
			PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(ServletContext context) {
		this.context = context;
	}

	/**
	 * @return the auctionStatusMap
	 */
	public Map<String, Long> getAuctionStatusMap() {
		return auctionStatusMap;
	}

	/**
	 * @param auctionStatusMap the auctionStatusMap to set
	 */
	public void setAuctionStatusMap(Map<String, Long> auctionStatusMap) {
		this.auctionStatusMap = auctionStatusMap;
	}

	/**
	 * @return the hhStart
	 */
	public String getHhStart() {
		return hhStart;
	}

	/**
	 * @param hhStart the hhStart to set
	 */
	public void setHhStart(String hhStart) {
		this.hhStart = hhStart;
	}

	/**
	 * @return the mmStart
	 */
	public String getMmStart() {
		return mmStart;
	}

	/**
	 * @param mmStart the mmStart to set
	 */
	public void setMmStart(String mmStart) {
		this.mmStart = mmStart;
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	/**
	 * @return the hhEnd
	 */
	public String getHhEnd() {
		return hhEnd;
	}

	/**
	 * @param hhEnd the hhEnd to set
	 */
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}

	/**
	 * @return the mmEnd
	 */
	public String getMmEnd() {
		return mmEnd;
	}

	/**
	 * @param mmEnd the mmEnd to set
	 */
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		//		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		////		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		//		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

}
