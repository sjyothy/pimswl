package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.AssetView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.SubSectorView;

public class AssetManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	private String pageMode;
	private String viewMode;
	private String screenName;
	private Date tansactionDate;
	private final String noteOwner = WebConstants.Attachment.EXTERNAL_ID_ASSET;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_ASSET;
	private final String procedureTypeKey = WebConstants.PROCEDURE_ASSET;
	
	private AssetView assetView;
	private AssetServiceAgent assetServiceAgent;
	private List<SelectItem> assetSubClassList = new ArrayList<SelectItem>();
	private List<SelectItem> assetSubSectorList = new ArrayList<SelectItem>();
	
		
	public AssetManageBacking() 
	{
		
		logger = Logger.getLogger(AssetManageBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		commonUtil = new CommonUtil();
		pageMode = WebConstants.ASSET.ASSET_MANAGE_PAGE_MODE_ADD;
		viewMode = new String();
		screenName = new String();
		assetView = new AssetView();
		assetServiceAgent = new AssetServiceAgent();
		
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		
		try	
		{	
			if(!isPostBack())
			{
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				
			}	
			
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			
			updateValuesFromMaps();
			showHideAttachmentsAndCommentsBtn();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE          
		if ( sessionMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_KEY) != null )
		{
		 pageMode = (String) sessionMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_KEY);
		 sessionMap.remove( WebConstants.ASSET.ASSET_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_KEY ) != null )
		{
		 pageMode = (String) viewMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_KEY);
		}
		                     
		if ( sessionMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) sessionMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY);
		 sessionMap.remove( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY );			
		}
		else if ( viewMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY ) != null )
		{
		 viewMode = (String) viewMap.get( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY );
		}
		
		// ASSET VIEW
		if ( sessionMap.get( WebConstants.ASSET.ASSET_VIEW) != null )
		{
		 assetView = (AssetView) sessionMap.get( WebConstants.ASSET.ASSET_VIEW);
		 sessionMap.remove( WebConstants.ASSET.ASSET_VIEW);
		 
		 onPopulateAssetSubClass();
		 onPopulateAssetSubSector();
		 
		 CommonUtil.loadAttachmentsAndComments(assetView.getAssetId().toString());
		}
		else if ( viewMap.get( WebConstants.ASSET.ASSET_VIEW )  != null )
		{
		 assetView = (AssetView) viewMap.get( WebConstants.ASSET.ASSET_VIEW ) ;
		 
		 
		}
			
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{                 
			viewMap.put( WebConstants.ASSET.ASSET_PAGE_MODE_KEY, pageMode );
		}
		if ( viewMode != null )
		{                
			viewMap.put( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW_KEY, viewMode );
		}
		
		
		// ASSET VIEW
		if ( assetView != null )
		{
			viewMap.put( WebConstants.ASSET.ASSET_VIEW , assetView);
		}
		

	
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "searchAsset";
		return path;
	}
	public String onReset() 
	{
		String path = null;		
		path = "AssetManage";
		try {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
		response.sendRedirect("assetManage.jsf");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{ if(checkMissingField())
		  {
			    
				isSuccess = saveAssetView(assetView);
				
				
			    CommonUtil.loadAttachmentsAndComments(assetView.getAssetId()+ "");
				CommonUtil.saveAttachments(assetView.getAssetId());
				CommonUtil.saveComments(assetView.getAssetId(),noteOwner);
				
				
				if(viewMap.containsKey("DUPLICATE_ASSET_NUMBER"))
				{
					updateValuesToMaps();
					errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.duplicateAssetNumber") );
					viewMap.remove("DUPLICATE_ASSET_NUMBER");
				}
				
				
				if(viewMap.containsKey("UPDATE_DONE") && isSuccess)
	            { 
					pageMode = WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE;
	            	updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("assetManage.msg.recordUpdate") );
					viewMap.remove("UPDATE_DONE");
	            }
	            else if (isSuccess)
				{              
	            	pageMode = WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE; 
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("assetManage.msg.recordSave") );
				}
		 }    
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.error") );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveAssetView( AssetView assetView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		Boolean duplicate  = false;
		
		if ( assetView.getAssetId() == null )
		{
//			if(assetServiceAgent.checkDuplicateAssetNumber(assetView, false))
//			{
//		      viewMap.put("DUPLICATE_ASSET_NUMBER", true);
//		      return isSuccess;
//			}
		
			
			assetView.setCreatedBy( CommonUtil.getLoggedInUser() );
			assetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			
		    isSuccess = assetServiceAgent.addAsset(assetView);
		}
		else
		{
//			if(assetServiceAgent.checkDuplicateAssetNumber(assetView, true))
//			{
//		      viewMap.put("DUPLICATE_ASSET_NUMBER", true);
//		      return isSuccess;
//			}
			
			
			assetView.setUpdatedBy( CommonUtil.getLoggedInUser() );			
			isSuccess = assetServiceAgent.updateAsset(assetView);
			viewMap.put("UPDATE_DONE", true);
		}
		
		return isSuccess;
	}
	
	

	public boolean checkMissingField()
	{
		boolean check = true;
		
//		if(assetView.getAssetNumber() == null || assetView.getAssetNumber().equals(""))
//		{
//			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetNumber") );
//			check = false;
//		}
		if(assetView.getAssetStatus()==null || assetView.getAssetStatus().equals("")|| assetView.getAssetStatus().equals("-1"))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetStatus") );
			check = false;
		}
		if(assetView.getCurrencyId() ==null || assetView.getCurrencyId().equals("")|| assetView.getCurrencyId().equals("-1"))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetCurrency") );
			check = false;
		}
		if(assetView.getAssetClassId() == null || assetView.getAssetClassId().equals("")||assetView.getAssetClassId().equals("-1"))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetClass") );
			check = false;
		}
		if(assetView.getAssetSymbol() == null || assetView.getAssetSymbol().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetSymbol") );
			check = false;
		}
		if(assetView.getAssetNameEn() == null || assetView.getAssetNameEn().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("assetManage.msg.assetName") );
			check = false;
		}
		
		
	
		return check;
	}
	public void onPopulateAssetSubClass() 
	{	
				
		boolean isLocaleEnglish = getIsEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		AssetClassView assetClassView = new AssetClassView();
		assetClassView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
		assetClassView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );		
		List<AssetClassView> assetClassViewList = new ArrayList<AssetClassView>(0); 
		
		if(assetView.getAssetClassId()!=null && !assetView.getAssetClassId().equals(-1))
		{	
			viewMap.remove("ASSET_SUB_CLASS_NEW");
			assetSubClassList.clear();
			try 
			{
				assetClassViewList = assetServiceAgent.getAssetSubClasses(Long.parseLong(assetView.getAssetClassId()));
			}
			catch (Exception e) 
			{
				logger.LogException("getCurrencyList()", e);
			}
			
			if ( assetClassViewList != null )		
				for ( AssetClassView aCV : assetClassViewList )			
					assetSubClassList.add( new SelectItem(aCV.getAssetClassId().toString(), isLocaleEnglish ? aCV.getAssetClassNameEn() : aCV.getAssetClassNameAr() ) );
			
			Collections.sort(assetSubClassList, ListComparator.LIST_COMPARE);
			viewMap.put("ASSET_SUB_CLASS_NEW", assetSubClassList);
		}	
		
		
	}

	
	public void onPopulateAssetSubSector() 
	{	
				
		boolean isLocaleEnglish = getIsEnglishLocale();
		AssetServiceAgent assetServiceAgent = new AssetServiceAgent();
		List<SubSectorView> subSectorViewList = new ArrayList<SubSectorView>(0); 
		
	  
		if(assetView.getAssetSectorId()!=null && !assetView.getAssetSectorId().equals(-1))
		{	
			viewMap.remove("ASSET_SUB_SECTOR_NEW");
			assetSubSectorList.clear();
			try 
			{
				subSectorViewList = assetServiceAgent.getSubSectorList(Long.parseLong(assetView.getAssetSectorId()));
			}
			catch (Exception e) 
			{
				logger.LogException("getCurrencyList()", e);
			}
			
			if ( subSectorViewList != null )		
				for ( SubSectorView sSV : subSectorViewList )			
					assetSubSectorList.add( new SelectItem(sSV.getSubSectorId().toString(), isLocaleEnglish ? sSV.getSectorNameEn() : sSV.getSectorNameAr() ) );
			
			Collections.sort(assetSubSectorList, ListComparator.LIST_COMPARE);
			
			viewMap.put("ASSET_SUB_SECTOR_NEW", assetSubSectorList);
		}	
	  
	}
   
	

	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
                                          		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW) )
			isViewMode = true;
		
		return isViewMode;
	}

	public String getIsViewReadOnly() 
	{
		String readOnly = "";
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsEditReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsStatusReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW)
			 || pageMode.equalsIgnoreCase(WebConstants.ASSET.ASSET_MANAGE_PAGE_MODE_ADD))
			readOnly = "READONLY";
		else if(pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE))
			readOnly = "";
	
			
		return readOnly;
	}
	
	public void showHideAttachmentsAndCommentsBtn(){
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_MANAGE_PAGE_MODE_ADD ) )
		    canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_POPUP_VIEW_ONLY) )
		    canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_UPDATE) )
	    	canAddAttachmentsAndComments(true);
	    if ( viewMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_VIEW) )
	    	canAddAttachmentsAndComments(false);
	}
	
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.ASSET.ASSET_PAGE_MODE_POPUP_VIEW_ONLY) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(noteOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	private void canAddAttachmentsAndComments(boolean canAdd)
	{
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}


	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}


	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	
	// Shiraz Code Start Here 
	
	public void onAddMember(){
	
	final String viewId = "/studyManage.jsf";

	FacesContext facesContext = FacesContext.getCurrentInstance();

	// This is the proper way to get the view's url
	ViewHandler viewHandler = facesContext.getApplication()
			.getViewHandler();
	String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	String javaScriptText = "javascript:SearchMemberPopup();";

	// Add the Javascript to the rendered page's header for immediate
	// execution
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,
			AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}


	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}


	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Date getTansactionDate() {
		return tansactionDate;
	}

	public void setTansactionDate(Date tansactionDate) {
		this.tansactionDate = tansactionDate;
	}

	public List<SelectItem> getAssetSubClassList() {
		if(viewMap.containsKey("ASSET_SUB_CLASS_NEW"))
			assetSubClassList = (List<SelectItem>)viewMap.get("ASSET_SUB_CLASS_NEW");
		return assetSubClassList;
	}

	public void setAssetSubClassList(List<SelectItem> assetSubClassList) {
		this.assetSubClassList = assetSubClassList;
	}

	public List<SelectItem> getAssetSubSectorList() {
		if(viewMap.containsKey("ASSET_SUB_SECTOR_NEW"))
			assetSubSectorList = (List<SelectItem>)viewMap.get("ASSET_SUB_SECTOR_NEW");
		return assetSubSectorList;
	}

	public void setAssetSubSectorList(List<SelectItem> assetSubSectorList) {
		this.assetSubSectorList = assetSubSectorList;
	}

	public AssetView getAssetView() {
		return assetView;
	}

	public void setAssetView(AssetView assetView) {
		this.assetView = assetView;
	}

	public AssetServiceAgent getAssetServiceAgent() {
		return assetServiceAgent;
	}

	public void setAssetServiceAgent(AssetServiceAgent assetServiceAgent) {
		this.assetServiceAgent = assetServiceAgent;
	}

	

}

