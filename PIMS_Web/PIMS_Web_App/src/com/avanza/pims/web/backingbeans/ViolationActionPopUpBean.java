package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationActionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationView;

public class ViolationActionPopUpBean extends AbstractController {

	public String selectedType;
	public String selectedStatus;
	public String selectedCategory;
	public Date violationDate;
	public String txtunitRefNum;
	public String txtcontractNum;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(ViolationActionPopUpBean.class);
	private List<InspectionViolationActionView> dataList;
	InspectionView inspectionView = new InspectionView();
	private HtmlDataTable tbl_violationSearch;
	private InspectionViolationView dataItem = new InspectionViolationView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;

	// Constructors

	/** default constructor */
	public ViolationActionPopUpBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		String method="getIsEnglishLocale";
		logger.logInfo(method+"|"+"Start...");
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		logger.logInfo(method+"|"+"Finish...");
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public InspectionViolationView getDataItem() {
		return dataItem;
	}

	public void setDataItem(InspectionViolationView dataItem) {
		this.dataItem = dataItem;
	}




	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) {
				if(sessionMap.containsKey("violation"))
				{
					ViolationView violationView=(ViolationView)sessionMap.get("violation");
					sessionMap.remove("violation");
					List<InspectionViolationActionView> violationList=new ArrayList<InspectionViolationActionView>();
					Iterator iter= violationView.getInspectionViolationActionsView().iterator();
					while(iter.hasNext())
					{
						InspectionViolationActionView inspectionViolationActionView=(InspectionViolationActionView)iter.next(); 
					violationList.add(inspectionViolationActionView);
					}
					sessionMap.put("SESSION_VIOLATION_ACTION_POPUP_LIST",violationView);
					dataList=violationList;
				}
			}
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		System.out.println("prerender");
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	
	public String getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}


	public String getTxtcontractNum() {
		return txtcontractNum;
	}

	public void setTxtcontractNum(String txtcontractNum) {
		this.txtcontractNum = txtcontractNum;
	}

	public List<InspectionViolationActionView> getDataList() {
		//dataList =null;
		if (dataList == null) 
		{
			if(sessionMap.containsKey("SESSION_VIOLATION_ACTION_POPUP_LIST"));
			dataList=(ArrayList)sessionMap.get("SESSION_VIOLATION_ACTION_POPUP_LIST");
		}
		return dataList;

	}

	public void setDataList(List<InspectionViolationActionView> dataList) {
		this.dataList = dataList;
	}

	public String getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public HtmlDataTable getTbl_violationSearch() {
		return tbl_violationSearch;
	}

	public void setTbl_violationSearch(HtmlDataTable tbl_violationSearch) {
		this.tbl_violationSearch = tbl_violationSearch;
	}

	public Date getViolationDate() {
		return violationDate;
	}

	public void setViolationDate(Date violationDate) {
		this.violationDate = violationDate;
	}

}