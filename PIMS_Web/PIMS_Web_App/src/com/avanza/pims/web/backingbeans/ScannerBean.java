package com.avanza.pims.web.backingbeans;

import java.io.File;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DefaultFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang.ArrayUtils;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.myfaces.webapp.filter.MultipartRequestWrapper;


import com.avanza.core.util.Logger;
import com.avanza.pims.web.controller.AbstractController;

public class ScannerBean extends AbstractController {
    private transient Logger logger = Logger.getLogger(ScannerBean.class);

    public void init() {
        super.init();
        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

    }

    public void preprocess() {
        super.preprocess();
        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

    }

    public void prerender() {

        super.prerender();
        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
        logger.logInfo("In prerender ");
        logger.logInfo(" if the request has multipart content:%s ", FileUpload.isMultipartContent(request));
        FileUpload l_usdUpLoad = new FileUpload();
        l_usdUpLoad.setFileItemFactory(new DefaultFileItemFactory());
        Collection l_usdList;

        l_usdList = parseRequest(request);

        logger.logInfo(" checking if any files are availble with the request ");
        if (l_usdList != null) {
            logger.logInfo(" YES - files are there in the request ");
            Iterator l_usdIterator = l_usdList.iterator();
            logger.logInfo(" iterating in files ");
            while (l_usdIterator.hasNext()) {
                logger.logInfo(" iteration " + l_usdIterator.toString());
                org.apache.commons.fileupload.FileItem l_usdFile =
                    (org.apache.commons.fileupload.FileItem) l_usdIterator.next();
                logger.logInfo(" setting the scanned file in session ");
                getFacesContext().getExternalContext()
                                 .getSessionMap()
                                 .put("SCANNED_IMAGE", l_usdFile);

            }
            getFacesContext().getExternalContext()
                             .getSessionMap()
                             .put("isProcDoc", true);
        } else {
            logger.logInfo(" l_usdList is null ");
        }
    }

    private Collection<FileItem> parseRequest(HttpServletRequest requestObj) {
        logger.logInfo(" In parseRequest ");
        Collection<FileItem> files = new ArrayList<FileItem>();
        // if the request is a multi-part
        logger.logInfo(" go to check if request is MultipartRequestWrapper ");
        if (requestObj instanceof MultipartRequestWrapper) {
            // cast to multi-part request
            MultipartRequestWrapper request = (MultipartRequestWrapper) requestObj;
            // get file parameter names
            logger.logInfo(" trying to get file tems ");
            Map<String, FileItem> f = request.getFileItems();
            // add the File to our list
            files = f.values();
            logger.logInfo(" file.isEmpty: " + files.isEmpty());

        }
        return files.isEmpty() ? null : files;
    }


}
