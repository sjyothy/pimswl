package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.soap.rpc.RPCConstants;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.ProjectFollowUpActivityView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.ui.util.ResourceUtil;


public class ProjectFollowupDetailsTab extends AbstractController {

	private static Logger logger = Logger.getLogger(ProjectFollowupDetailsTab.class);
	private HtmlDataTable dataTable = new HtmlDataTable();
	private List<ProjectFollowUpActivityView> projectFollowupList = new ArrayList<ProjectFollowUpActivityView>();
	
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	private ProjectServiceAgent projectServiceAgent = new ProjectServiceAgent();
	private Integer paginatorMaxPages=0;
	private Integer paginatorRows=0;
	
	public ProjectFollowupDetailsTab ()
	{
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
		try
		{
   	 	if(!isPostBack()){
   	 		
   	 	}
   	 	if(viewMap.containsKey(WebConstants.ProjectFollowUp.PROJECT_ID))
   	 	{
   	 		ProjectServiceAgent psa=new ProjectServiceAgent(); 
   	 		Long projectId=Long.parseLong(viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID).toString());
   	 		projectFollowupList=psa.getAllProjectFollowupActivitiesByProjectId(projectId);
   	 		viewMap.put("PROJECT_FOLLWOUP_ACTIVITY_LIST", projectFollowupList);
   	 		viewMap.put("followUpRecordSize", projectFollowupList.size());
   	 	}
		}
		catch(Exception e)
		{
			logger.LogException("init() Crashed due to :", e);
		}
		
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	
	
	
	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("followUpRecordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}

	public List<ProjectFollowUpActivityView> getProjectFollowupList() 
	{
		if(viewMap.containsKey("PROJECT_FOLLWOUP_ACTIVITY_LIST"))
			projectFollowupList=(List<ProjectFollowUpActivityView>) viewMap.get("PROJECT_FOLLWOUP_ACTIVITY_LIST");
		return projectFollowupList;
	}

	public void setProjectFollowupList(
			List<ProjectFollowUpActivityView> projectFollowupList) {
		this.projectFollowupList = projectFollowupList;
	}
	public Integer getPaginatorMaxPages() 
	{
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public Integer getPaginatorRows() 
	{
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
}
