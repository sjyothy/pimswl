package com.avanza.pims.web.backingbeans;

import java.io.Serializable;
import java.util.ArrayList;

public class TaskView implements Serializable {
	private String taskId;
	private String workFlowName;
	private String taskName;
	private String assignedDate;
	private String expiryDate;
	private String subject;
	private String taskType;
	private ArrayList<String> content;
	private String url;
	
	public TaskView()
	{
		setTaskType("");
	}
	
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setWorkFlowName(String workFlowName) {
		this.workFlowName = workFlowName;
	}
	public String getWorkFlowName() {
		return workFlowName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}
	public String getAssignedDate() {
		return assignedDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setContent(ArrayList<String> content) {
		this.content = content;
	}
	public ArrayList<String> getContent() {
		return content;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl() {
		return url;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskType() {
		return taskType;
	}

}
