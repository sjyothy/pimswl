package com.avanza.pims.web.backingbeans.Investment.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.OrderView;

public class OrderSearchBacking extends AbstractController 
{	
	private static final long serialVersionUID = -380353931340430945L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private OrderServiceAgent orderServiceAgent;
	
	private String pageMode;
	private OrderView orderView;
	private List<OrderView> orderViewList;
	
	private HtmlDataTable dataTable;
	
	public OrderSearchBacking() 
	{
		logger = Logger.getLogger( OrderSearchBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		orderServiceAgent = new OrderServiceAgent();
		
		pageMode = WebConstants.Order.ORDER_SEARCH_PAGE_MODE_SEARCH;
		orderView = new OrderView();
		orderViewList = new ArrayList<OrderView>(0);
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY );
		}
		
		
		// ORDER VIEW
		if ( viewMap.get( WebConstants.Order.ORDER_VIEW ) != null )
		{
			orderView = (OrderView) viewMap.get( WebConstants.Order.ORDER_VIEW );
		}
		
		
		// ORDER VIEW LIST
		if( viewMap.get( WebConstants.Order.ORDER_VIEW_LIST ) != null )
		{
			orderViewList = (List<OrderView>) viewMap.get( WebConstants.Order.ORDER_VIEW_LIST );
		}
		
		updateValuesToMaps();
	}
	
	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_KEY, pageMode );
		}
		
		
		// ORDER VIEW
		if ( orderView != null )
		{
			viewMap.put( WebConstants.Order.ORDER_VIEW, orderView );
		}
		
		
		// ORDER VIEW LIST
		if ( orderViewList != null )
		{
			viewMap.put( WebConstants.Order.ORDER_VIEW_LIST, orderViewList );
		}
	}
	
	public Boolean getIsSearchMode()
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_SEARCH ) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Boolean getIsSingleSelectPopupMode() 
	{
		Boolean isSingleSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP ) )
			isSingleSelectPopupMode = true;
		
		return isSingleSelectPopupMode;
	}
	
	public Boolean getIsMultiSelectPopupMode()
	{
		Boolean isMultiSelectPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_SEARCH_PAGE_MODE_MULTI_SELECT_POPUP ) )
			isMultiSelectPopupMode = true;
		
		return isMultiSelectPopupMode;
	}
	
	public String onSearch() 
	{
		String METHOD_NAME = "onSearch()";		
		String path = null;
		
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			orderView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			orderView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );			
			orderViewList = orderServiceAgent.getOrderList( orderView );			
			
			updateValuesToMaps();
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onAddSellingOrder() 
	{
		OrderView newOrderView = new OrderView();
		
		DomainDataView ddvOrderTypeSelling = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_SELLING);
		newOrderView.setOrderTypeId( String.valueOf( ddvOrderTypeSelling.getDomainDataId() ) );
		newOrderView.setOrderTypeEn( ddvOrderTypeSelling.getDataDescEn() );
		newOrderView.setOrderTypeAr( ddvOrderTypeSelling.getDataDescAr() );
		
		DomainDataView ddvOrderStatusNew = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_STATUS), WebConstants.Order.ORDER_STATUS_NEW );			
		newOrderView.setOrderStatusId( String.valueOf( ddvOrderStatusNew.getDomainDataId() ) );
		newOrderView.setOrderStatusEn( ddvOrderStatusNew.getDataDescEn() );
		newOrderView.setOrderStatusAr( ddvOrderStatusNew.getDataDescAr() );
		
		newOrderView.setTransactionDate( new Date() );
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, newOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW);
		
		return "orderManage";
	}
	
	public String onAddBuyingOrder() 
	{
		OrderView newOrderView = new OrderView();
		
		DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);
		newOrderView.setOrderTypeId( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) );
		newOrderView.setOrderTypeEn( ddvOrderTypeBuying.getDataDescEn() );
		newOrderView.setOrderTypeAr( ddvOrderTypeBuying.getDataDescAr() );		
		
		DomainDataView ddvOrderStatusNew = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_STATUS), WebConstants.Order.ORDER_STATUS_NEW );			
		newOrderView.setOrderStatusId( String.valueOf( ddvOrderStatusNew.getDomainDataId() ) );
		newOrderView.setOrderStatusEn( ddvOrderStatusNew.getDataDescEn() );
		newOrderView.setOrderStatusAr( ddvOrderStatusNew.getDataDescAr() );
		
		newOrderView.setTransactionDate( new Date() );
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, newOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW);
		
		return "orderManage";
	}
	
	public String onEdit() 
	{
		OrderView selectedOrderView = (OrderView) dataTable.getRowData();
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, selectedOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT);
		
		return "orderManage";
	}
	
	public String onView() 
	{
		OrderView selectedOrderView = (OrderView) dataTable.getRowData();
		
		sessionMap.put( WebConstants.Order.ORDER_VIEW, selectedOrderView );
		sessionMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY , WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW);
		
		return "orderManage";		
	}
	
	public String onDelete() 
	{
		String METHOD_NAME = "onDelete()";		
		Boolean isDeleted = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			OrderView selectedOrderView = (OrderView) dataTable.getRowData();
			selectedOrderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			selectedOrderView.setIsDeleted( 1L );
			
			isDeleted = orderServiceAgent.updateOrder( selectedOrderView );
			
			if ( isDeleted )
			{
				orderViewList.remove( selectedOrderView );
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_DELETE_SUCCESS ) );
				updateValuesToMaps();
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
		}
		catch (Exception exception) 
		{	
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_DELETE_ERROR ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;		
	}
	
	public String onSingleSelect()
	{
		return null;
	}
	
	public String onMultiSelect()
	{
		return null;
	}
	
	public Integer getRecordSize() {
		return orderViewList.size();
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public OrderServiceAgent getOrderServiceAgent() {
		return orderServiceAgent;
	}

	public void setOrderServiceAgent(OrderServiceAgent orderServiceAgent) {
		this.orderServiceAgent = orderServiceAgent;
	}

	public OrderView getOrderView() {
		return orderView;
	}

	public void setOrderView(OrderView orderView) {
		this.orderView = orderView;
	}

	public List<OrderView> getOrderViewList() {
		return orderViewList;
	}

	public void setOrderViewList(List<OrderView> orderViewList) {
		this.orderViewList = orderViewList;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
}
