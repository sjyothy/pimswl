package com.avanza.pims.web.backingbeans;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.criteria.ChequeDetailsListReportCriteria;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PaymentTypeView;

public class ChequeDetailsListBackingBean extends AbstractController
{
	private static final long serialVersionUID = -5874063374554422002L;
	private Logger logger;
	private ChequeDetailsListReportCriteria chequeDetailsListReportCriteria;
//	private List<SelectItem> paymentTypeList = new ArrayList();
//	private FacesContext context = FacesContext.getCurrentInstance();
//	private Map viewMap=context.getViewRoot().getAttributes();
//	private boolean isArabicLocale;
//	private boolean isEnglishLocale;
	public ChequeDetailsListBackingBean()
	{	logger = Logger.getLogger(ChequeDetailsListBackingBean.class);
	if(CommonUtil.getIsEnglishLocale())
		chequeDetailsListReportCriteria=new ChequeDetailsListReportCriteria(ReportConstant.Report.CHEQUE_DETAILS_LIST_EN, ReportConstant.Processor.CHEQUE_DETAILS_LIST, CommonUtil.getLoggedInUser());
	else    
	chequeDetailsListReportCriteria=new ChequeDetailsListReportCriteria(ReportConstant.Report.CHEQUE_DETAILS_LIST_AR, ReportConstant.Processor.CHEQUE_DETAILS_LIST, CommonUtil.getLoggedInUser());
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, chequeDetailsListReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public ChequeDetailsListReportCriteria getChequeDetailsListReportCriteria() {
		return chequeDetailsListReportCriteria;
	}
	public void setChequeDetailsListReportCriteria(
			ChequeDetailsListReportCriteria chequeDetailsListReportCriteria) {
		this.chequeDetailsListReportCriteria = chequeDetailsListReportCriteria;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
///////////Inserted into ApplicationBean////////////////////	
//	public List<SelectItem> getPaymentTypeList() 
//	{
//	
//	    paymentTypeList.clear();
//	    if(!viewMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE) || viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE)==null)
//	       loadPaymentType();
//	    List<PaymentTypeView> ptvList = (ArrayList<PaymentTypeView>)viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE);
//	    List<SelectItem> typeList = new ArrayList<SelectItem>(0);
//	    for (PaymentTypeView paymentTypeView : ptvList) {
//	    	SelectItem item =null; 
//	         if(getIsEnglishLocale())
//	          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionEn());
//	         else 
//		          item=new SelectItem(paymentTypeView.getPaymentTypeId().toString(),paymentTypeView.getDescriptionAr());
//		    typeList.add(item);   
//	    }
//	       paymentTypeList= typeList;
//		   return paymentTypeList;
//	}
//	
//	public void setPaymentTypeList(List<SelectItem> paymentTypeList) {
//		this.paymentTypeList = paymentTypeList;
//	}
//	private void loadPaymentType()
//	{
//	  String methodName="loadPaymentType";
//	  logger.logInfo(methodName +"|"+"Start ");
//	  try
//	  {
//		  UtilityServiceAgent usa =new UtilityServiceAgent();
//		  if(!viewMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE))
//		  {
//		    List<PaymentTypeView>ddv= (ArrayList<PaymentTypeView>)usa.getAllPaymentTypes();
//		    viewMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE, ddv);
//		  }
//		  
//	  }
//	  catch (Exception e)
//	  {
//		logger.logError(methodName +"|"+"Exception Occured "+e);
//	  }
//	
//	  logger.logInfo(methodName +"|"+"Finish ");
//	
//	
//	
//	}
//	public boolean getIsEnglishLocale() {
//		WebContext webContext = ApplicationContext.getContext().get(
//				WebContext.class);
//		LocaleInfo localeInfo = webContext
//				.getAttribute(CoreConstants.CurrentLocale);
//		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
//		return isEnglishLocale;
//	}
//	public boolean getIsArabicLocale() {
//		isArabicLocale = !getIsEnglishLocale();
//		return isArabicLocale;
//	}
//	
 
}
