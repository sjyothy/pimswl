package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import com.avanza.core.document.control.AttachmentBean;
import com.avanza.core.util.Logger;

public class TestController {

	private static Logger logger = Logger.getLogger(TestController.class);

	private List<AttachmentBean> tablelist = new ArrayList<AttachmentBean>();
	
	public List<AttachmentBean> getTablelist(){
		tablelist = new ArrayList<AttachmentBean>();
		tablelist.add(new AttachmentBean(1,  "one", "t1", "url"));
		tablelist.add(new AttachmentBean(2,  "two", "t2", "url"));
		tablelist.add(new AttachmentBean(3,  "three", "t3", "url"));
		tablelist.add(new AttachmentBean(4,  "four", "t4", "url"));
		tablelist.add(new AttachmentBean(5,  "five", "t5", "url"));
		tablelist.add(new AttachmentBean(6,  "six", "t6", "url"));
		tablelist.add(new AttachmentBean(7,  "seven", "t7", "url"));
		return tablelist;
	}
}
