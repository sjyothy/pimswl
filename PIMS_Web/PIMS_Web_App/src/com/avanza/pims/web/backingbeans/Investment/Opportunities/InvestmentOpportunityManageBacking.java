package com.avanza.pims.web.backingbeans.Investment.Opportunities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.PIMSInvestmentOpportunitiesBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.InvestmentOpportunityServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.WebConstants.InvestmentOpportunity;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ConstructionContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InvestmentOpportunityView;
import com.avanza.pims.ws.vo.OpportunityEvaluationView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.ui.util.ResourceUtil;
import com.collaxa.cube.ant.taskdefs.customize.xml.Remove;

public class InvestmentOpportunityManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 1243101210652238837L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private SystemParameters parameters;	
	private InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent;
	
	private String pageMode;
	private InvestmentOpportunityView investmentOpportunityView;
	private List<OpportunityEvaluationView> opportunityEvaluationViewList;
	private UserTask userTask;
	String notesOwner = WebConstants.InvestmentOpportunity.PROCEDURE_TYPE_INVESTMENT_OPPORTUNITY;	
	private HtmlDataTable dataTable;
	private String PROCEDURE_TYPE="procedureType";
	private String EXTERNAL_ID ="externalId";
	private String txtRemarks="";
	private String portfolioNumber="";
	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	@SuppressWarnings("unchecked")
	public InvestmentOpportunityManageBacking() 
	{
		logger = Logger.getLogger( this.getClass() );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		parameters = SystemParameters.getInstance(); 
		
		investmentOpportunityServiceAgent = new InvestmentOpportunityServiceAgent();
		
		pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_ADD;
		investmentOpportunityView = new InvestmentOpportunityView();
		opportunityEvaluationViewList = new ArrayList<OpportunityEvaluationView>(0);
		userTask = new UserTask();
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");			
			super.init();			
			if(!isPostBack())
			{
				
//					loadAttachmentsAndComments(null);
					viewMap.put(PROCEDURE_TYPE,WebConstants.InvestmentOpportunity.PROCEDURE_TYPE_INVESTMENT_OPPORTUNITY);
		    	   viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_INVESTMENT_OPPORTUNITY);
		    	   loadAttachmentsAndComments(null);
		    	   if(sessionMap.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW)!=null)
		    	   	{
		    		   investmentOpportunityView=(InvestmentOpportunityView) sessionMap.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW);
		    		   
		    		   if ( investmentOpportunityView.getPortfolioView() != null )
		    			   viewMap.put(WebConstants.Portfolio.PORTFOLIO_VIEW , investmentOpportunityView.getPortfolioView());		    		   
		    		   
		    		   sessionMap.remove(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW);
		    	   	}
		    	   DomainDataView ddvOpportunityStatusNew = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS), WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_STATUS_NEW);
				   investmentOpportunityView.setOpportunityStatusId( String.valueOf( ddvOpportunityStatusNew.getDomainDataId() ) );			
				   investmentOpportunityView.setOpportunityDate( new Date() );
				   loadAttachmentsAndComments(investmentOpportunityView.getOpportunityId());
			}
			
			if(sessionMap.containsKey(WebConstants.Portfolio.PORTFOLIO_VIEW ))
			{
//				loadAttachmentsAndComments(null);
				PortfolioView portfolioView = (PortfolioView) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW  );
				viewMap.put(WebConstants.Portfolio.PORTFOLIO_VIEW , portfolioView);
				sessionMap.remove(WebConstants.Portfolio.PORTFOLIO_VIEW);
				loadAttachmentsAndComments(investmentOpportunityView.getOpportunityId());
			}
					
			updateValuesFromMaps();			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}		
	}
    
	
	
	@Override
	public void prerender()
	{
		canAddAttachmentsComments(true);
		if(pageMode.equals(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_VIEW))
			canAddAttachmentsComments(false);
			
		
	}
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception 
	{		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY );
		}
		else if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY );
		}
		
		
		// INVESTMENT OPPORTUNITY VIEW
		if ( sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW ) != null )
		{
			investmentOpportunityView = (InvestmentOpportunityView) sessionMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );			
			sessionMap.remove( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );
			
			if ( investmentOpportunityView.getPortfolioView() != null )
				viewMap.put(WebConstants.Portfolio.PORTFOLIO_VIEW , investmentOpportunityView.getPortfolioView());
			
			investmentOpportunityView = investmentOpportunityServiceAgent.getInvestmentOpportunityView( investmentOpportunityView.getOpportunityId() );
		}
		else if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW ) != null )
		{
			investmentOpportunityView = (InvestmentOpportunityView) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW );
		}
		

		// OPPORTUNITY EVALUATION VIEW LIST
		if ( viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION_VIEW_LIST ) != null )
		{
			opportunityEvaluationViewList = (List<OpportunityEvaluationView>) viewMap.get( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION_VIEW_LIST );
		}
		else if ( investmentOpportunityView != null && investmentOpportunityView.getOpportunityId() != null )
		{
			OpportunityEvaluationView opportunityEvaluationView = new OpportunityEvaluationView();
			opportunityEvaluationView.setInvestmentOpportunityView( investmentOpportunityView );
			opportunityEvaluationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			opportunityEvaluationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			opportunityEvaluationViewList = investmentOpportunityServiceAgent.getOpportunityEvaluationViewList( opportunityEvaluationView );
		}
		
		// USER TASK
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			
			if ( userTask != null )
			{
				investmentOpportunityView = investmentOpportunityServiceAgent.getInvestmentOpportunityView( Long.parseLong( userTask.getTaskAttributes().get( InvestmentOpportunity.TASK_ATTRIBUTE_INVESTMENT_OPPORTUNITY_ID ) ) );
				OpportunityEvaluationView opportunityEvaluationView = new OpportunityEvaluationView();
				opportunityEvaluationView.setInvestmentOpportunityView( investmentOpportunityView );
				opportunityEvaluationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				opportunityEvaluationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				if ( investmentOpportunityView.getPortfolioView() != null )
					viewMap.put(WebConstants.Portfolio.PORTFOLIO_VIEW , investmentOpportunityView.getPortfolioView());
				
				opportunityEvaluationViewList = investmentOpportunityServiceAgent.getOpportunityEvaluationViewList( opportunityEvaluationView );
				
				if ( userTask.getTaskAttributes().get( WebConstants.InvestmentOpportunity.TASK_ATTRIBUTE_TASK_TYPE ) != null && 
						! userTask.getTaskAttributes().get( WebConstants.InvestmentOpportunity.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase("") )
				{
					if ( userTask.getTaskAttributes().get( WebConstants.InvestmentOpportunity.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.InvestmentOpportunity.TASK_TYPE_EVALUATE_INVESMENT_OPPORTUNITY ) )				
						pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EVALUATION;
					else if ( userTask.getTaskAttributes().get( WebConstants.InvestmentOpportunity.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.InvestmentOpportunity.TASK_TYPE_APPROVE_INVESMENT_OPPORTUNITY ) )
						pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_APPROVAL;							
				}	
			}
		}
		else if ( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		}
		loadAttachmentsAndComments(investmentOpportunityView.getOpportunityId());
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		
		// INVESTMENT OPPORTUNITY VIEW
		if ( investmentOpportunityView != null )
		{
			 viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_VIEW, investmentOpportunityView );
		}
		
		// OPPORTUNITY EVALUATION VIEW LIST
		if ( opportunityEvaluationViewList != null )
		{
			viewMap.put( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION_VIEW_LIST, opportunityEvaluationViewList );
		}
		
		// USER TASK
		if ( userTask != null )
		{
			viewMap.put( WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask );
		}
		
	}
	private void canAddAttachmentsComments(boolean canAdd)
	{
		viewMap.put("canAddAttachment", canAdd);
		viewMap.put("canAddNote", canAdd);

	}
//	public void tabAuditTrail_Click()
//	{
//		String methodName="tabAuditTrail_Click";
//    	logger.logInfo(methodName+"|"+"Start..");
//    	errorMessages = new ArrayList<String>(0);
//    	try	
//    	{
//    	  RequestHistoryController rhc=new RequestHistoryController();
//    	  if(investmentOpportunityView!=null && investmentOpportunityView.getOpportunityId()!= null)
//    	    rhc.getAllRequestTasksForRequest(notesOwner,investmentOpportunityView.getOpportunityId().toString());
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+"Error Occured..",ex);
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    	}
//    	logger.logInfo(methodName+"|"+"Finish..");
//    
//		
//	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long associatedObjectId){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = viewMap.get(EXTERNAL_ID).toString();
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", notesOwner);
		if(associatedObjectId!= null)
		{
	    	String entityId = associatedObjectId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
			
		}
	}	
	private void saveCommensAttachment(String eventDesc) throws Exception 
	{
		String methodName ="saveCommensAttachment";
		 
		 if(investmentOpportunityView != null && investmentOpportunityView.getOpportunityId()!=null)
		 {
		     saveComments(investmentOpportunityView.getOpportunityId());
			 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
			 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
			 saveAttachments(investmentOpportunityView.getOpportunityId().toString());
			 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		
		 }
		saveSystemComments(eventDesc);
	}
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try
    	{
	    	logger.logInfo(methodName + "started...");
    		logger.logInfo(methodName + "OpportunityId..."+ investmentOpportunityView.getOpportunityId());
    		NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, investmentOpportunityView.getOpportunityId());
    		logger.logInfo(methodName + "completed successfully!!!");
    		
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try
    	{
	    	logger.logInfo(methodName + "started...");
	    	if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	  txtRemarks="";
	    	}
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.saveAttachments(Long.parseLong(referenceId));
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validate() )
			{
				investmentOpportunityView.setPortfolioView(getPortfolioView());
				isSuccess = saveInvestmentOpportunityView( investmentOpportunityView );				
				
				if (isSuccess)
				{
					pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EDIT;
					updateValuesToMaps();
					
					if(pageMode == WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_ADD )
						successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_SAVE_SUCCESS_ADD ) );
					else
						successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_SAVE_SUCCESS_EDIT ) );
					logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
				}
				
			}
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	public String onSaveEvaluation()
	{
		String METHOD_NAME = "onSaveEvaluation()";
		String path = null;
		boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validateForEvaluation() )
			{
				isSuccess = saveOpportunityEvaluationViewList( opportunityEvaluationViewList );				
				
				if (isSuccess)
				{
					pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EVALUATION;
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_SAVE_SUCCESS ) );
					logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
				}
				
			}
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}

	public String onSendForApproval()
	{
		String METHOD_NAME = "onSendForApproval()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validateForEvaluation() )
			{
				onSaveEvaluation();
				String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				approveRejectOkTask(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
				afterBPELActions();
				saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_SENT_FOR_APPROVAL);
				successMessages.clear();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.APPROVAL_REQUEST_SENT_SUCCESS ) );			
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}	
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	private void approveRejectOkTask(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception 
	{
		String METHOD_NAME = "ApproveRejectTask()";		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");				
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
	}
	
	private void afterBPELActions() throws PimsBusinessException
	{
		investmentOpportunityView = investmentOpportunityServiceAgent.getInvestmentOpportunityView( investmentOpportunityView.getOpportunityId() );
		pageMode = WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_VIEW;
		updateValuesToMaps();		
	}
	
	public String onSendForEvaluation() 
	{
		String METHOD_NAME = "onSend()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			if ( validate() )
			{
				onSave();
			PIMSInvestmentOpportunitiesBPELPortClient portClient = new PIMSInvestmentOpportunitiesBPELPortClient();
			portClient.setEndpoint( parameters.getParameter( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_BPEL_END_POINT ) );
			portClient.initiate(investmentOpportunityView.getOpportunityId(), CommonUtil.getLoggedInUser(), null, null);
			
			investmentOpportunityServiceAgent.setInvestmentOpportunityAsUnderEvaluation(investmentOpportunityView.getOpportunityId(), CommonUtil.getLoggedInUser());
			
			afterBPELActions();
			saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_SENT_FOR_EVALUATION);
			successMessages.clear();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.EVALUATION_REQUEST_SENT_SUCCESS ) );
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
			
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onCancel() 
	{
		return "investmentOpportunitySearch";
	}
	
	private Boolean saveInvestmentOpportunityView( InvestmentOpportunityView investmentOpportunityView ) throws PimsBusinessException ,Exception
	{
		boolean isSuccess = false;
		
		if ( investmentOpportunityView.getOpportunityId() == null )
		{
			investmentOpportunityView.setCreatedBy( CommonUtil.getLoggedInUser() );
			investmentOpportunityView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = investmentOpportunityServiceAgent.addInvestmentOpportunity( investmentOpportunityView );
//			saveSystemComments(MessageConstants.InvestmentOpportunity.OPP_SAVED);
			saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_SAVED);
		}
		else
		{
			investmentOpportunityView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = investmentOpportunityServiceAgent.updateInvestmentOpportunity( investmentOpportunityView );
//			saveSystemComments(MessageConstants.InvestmentOpportunity.OPP_UPDATED);
			saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_UPDATED);
		}
		
		return isSuccess;
	}
	
	private boolean saveOpportunityEvaluationViewList(List<OpportunityEvaluationView> opportunityEvaluationViewList) throws PimsBusinessException,Exception 
	{	
		boolean isSuccess = false;
		
		if ( opportunityEvaluationViewList != null && opportunityEvaluationViewList.size() > 0  )
		{
			for ( OpportunityEvaluationView opportunityEvaluationView : opportunityEvaluationViewList )
			{
				if ( opportunityEvaluationView.getEvaluationId() == null )
				{
					// Add Opportunity Evaluation
					opportunityEvaluationView.setCreatedBy( CommonUtil.getLoggedInUser() );
					opportunityEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					opportunityEvaluationView.setInvestmentOpportunityView( investmentOpportunityView );
					isSuccess = investmentOpportunityServiceAgent.addOpportunityEvaluation( opportunityEvaluationView );
					saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_EVALUATION_SAVED);
				}
				else if ( opportunityEvaluationView.getIsDeleted().equals( WebConstants.IS_DELETED_TRUE  ) )
				{
					// Update Opportunity Evaluation
					opportunityEvaluationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
					isSuccess = investmentOpportunityServiceAgent.updateOpportunityEvaluation( opportunityEvaluationView );
					saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_EVALUATION_UPDATED);
				}
				else
				{
					// No Change
					isSuccess = true;
				}
			}
		}
		
		return isSuccess;
	}
	
	public Integer getEvaluationRecordSize() 
	{
		int size = 0;
		
		for ( OpportunityEvaluationView opportunityEvaluationView :  opportunityEvaluationViewList )
		{
			if ( opportunityEvaluationView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		}
		return size;
	} 
	
	public String onAddEvaluation()
	{
		executeJavascript("openEvaluationManagePopup();");
		return null;
	}
	
	public String onReceiveEvaluation()
	{
		if ( sessionMap.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION) != null )
		{
			OpportunityEvaluationView receivedEvaluation = (OpportunityEvaluationView) sessionMap.get(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION);
			sessionMap.remove(WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_EVALUATION);
			opportunityEvaluationViewList.add( receivedEvaluation );
			updateValuesToMaps();
		}
		
		return null;
	}
	
	public String onDeleteEvaluation()
	{
		OpportunityEvaluationView selectedOpportunityEvaluationView = (OpportunityEvaluationView) dataTable.getRowData();
		
		if ( selectedOpportunityEvaluationView.getEvaluationId() == null )
			opportunityEvaluationViewList.remove( selectedOpportunityEvaluationView );
		else
			selectedOpportunityEvaluationView.setIsDeleted( WebConstants.IS_DELETED_TRUE );
		
		updateValuesToMaps();
		
		return null;
	}

	private boolean validate() 
	{
		boolean isValid = true;
		
		if (investmentOpportunityView.getOpportunityTitle() ==null || investmentOpportunityView.getOpportunityTitle().trim().equals("")) 
		{
			 errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_TITLE_IS_REQUIRED));
			 isValid = false;
		}
		if (investmentOpportunityView.getOpportunityOwner() ==null || investmentOpportunityView.getOpportunityOwner().trim().equals("")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_OWNER_IS_REQUIRED));
			 isValid = false;
		}
		if (investmentOpportunityView.getOpportunityTypeId() ==null || investmentOpportunityView.getOpportunityTypeId().trim().equals("")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_TYPE_IS_REQUIRED));
			 isValid = false;
		}
		if (investmentOpportunityView.getOpportunitySubjectId() ==null || investmentOpportunityView.getOpportunitySubjectId().trim().equals("")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_SUBJECT_IS_REQUIRED));
			 isValid = false;
		}
		if (investmentOpportunityView.getOpportunityDate() ==null) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_OPPORTUNITY_DATE_IS_REQUIRED));
			 isValid = false;
		}
		if (investmentOpportunityView.getOpportunityDescription() ==null || investmentOpportunityView.getOpportunityDescription().trim().equals("")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_DESCRIPTION_IS_REQUIRED));
			 isValid = false;
		}
		
		
		/* Commented on requirement of CICL
		if (getPortfolioNumber() ==null ||getPortfolioNumber().trim().equals("")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_PORTFOLIO_IS_REQUIRED));
			 isValid = false;
		}
		*/
		
		
		return isValid;
	}
	
	private boolean validateForEvaluation() 
	{
		boolean isValid = true;
		
		if ( opportunityEvaluationViewList == null || opportunityEvaluationViewList.size() == 0 || checkDeletedEvaluations(opportunityEvaluationViewList)   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.OPPORTUNITY_EVALUATION_IS_REQUIRED));
			 isValid = false;
		}		
		
		return isValid;
	}
	
	private boolean checkDeletedEvaluations(List<OpportunityEvaluationView> opportunityEvaluationViewList)
	{
		boolean allDeleted = true;
		
		for(OpportunityEvaluationView opportunityEvaluationView : opportunityEvaluationViewList)
		{
			if(opportunityEvaluationView.getIsDeleted().compareTo(WebConstants.DEFAULT_IS_DELETED) == 0)
				allDeleted = false;
		}
		return allDeleted;
	}
	
	public Boolean getIsAddMode()
	{
		Boolean isAddMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_ADD  ) )
			isAddMode = true;
		
		return isAddMode;
	}

	public Boolean getIsEditMode() 
	{
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EDIT ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode()
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_VIEW ) )
			isViewMode = true;
		
		return isViewMode;
	}
	
	public Boolean getIsEvaluationMode()
	{
		Boolean isEvaluationMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_EVALUATION ) )
			isEvaluationMode = true;
		
		return isEvaluationMode;
	}
	
	public Boolean getIsApprovalMode()
	{
		Boolean isApprovalMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.InvestmentOpportunity.INVESTMENT_OPPORTUNITY_MANAGE_PAGE_MODE_APPROVAL ) )
			isApprovalMode = true;
		
		return isApprovalMode;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public String onApprove()
	{
		String METHOD_NAME = "onApprove()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectOkTask(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
			
			investmentOpportunityServiceAgent.setInvestmentOpportunityAsApproved(investmentOpportunityView.getOpportunityId(), CommonUtil.getLoggedInUser());
			
			afterBPELActions();
			saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_APPROVED);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.APPROVAL_REQUEST_APPROVE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onReject()
	{
		String METHOD_NAME = "onReject()";
		String path = null;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectOkTask(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
			
			investmentOpportunityServiceAgent.setInvestmentOpportunityAsRejected(investmentOpportunityView.getOpportunityId(), CommonUtil.getLoggedInUser());
			
			afterBPELActions();
			saveCommensAttachment(MessageConstants.InvestmentOpportunity.OPP_REJECTED);
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.InvestmentOpportunity.APPROVAL_REQUEST_REJECT_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	public String onSearchPortfolio()
	{
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY, WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP );
		sessionMap.put(WebConstants.Portfolio.PORTFOLIO_STATUS,WebConstants.Portfolio.PORTFOLIO_STATUS_NEW);
		executeJavascript("openPortfolioSearchPopup();");
		return null;
	}
	public String onReceiveSelectedPortfolio()
	{
		updateValuesToMaps();
		return null;
	}
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public Integer getPaginatorRows() 
	{		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() 
	{
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() 
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public InvestmentOpportunityView getInvestmentOpportunityView() {
		return investmentOpportunityView;
	}

	public void setInvestmentOpportunityView(
			InvestmentOpportunityView investmentOpportunityView) {
		this.investmentOpportunityView = investmentOpportunityView;
	}

	public InvestmentOpportunityServiceAgent getInvestmentOpportunityServiceAgent() {
		return investmentOpportunityServiceAgent;
	}

	public void setInvestmentOpportunityServiceAgent(
			InvestmentOpportunityServiceAgent investmentOpportunityServiceAgent) {
		this.investmentOpportunityServiceAgent = investmentOpportunityServiceAgent;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public SystemParameters getParameters() {
		return parameters;
	}

	public void setParameters(SystemParameters parameters) {
		this.parameters = parameters;
	}

	public List<OpportunityEvaluationView> getOpportunityEvaluationViewList() {
		return opportunityEvaluationViewList;
	}

	public void setOpportunityEvaluationViewList(
			List<OpportunityEvaluationView> opportunityEvaluationViewList) {
		this.opportunityEvaluationViewList = opportunityEvaluationViewList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public PortfolioView getPortfolioView() 
	{
		PortfolioView portfolioView =null;
		if(viewMap.get(WebConstants.Portfolio.PORTFOLIO_VIEW)!=null)
			portfolioView=(PortfolioView) viewMap.get(WebConstants.Portfolio.PORTFOLIO_VIEW);
		return portfolioView;
	}
	public String getPortfolioNumber() 
	{
		String portfolioNumber ="";
		PortfolioView portfolioView = getPortfolioView();
		if(portfolioView != null)
		{
			portfolioNumber = portfolioView.getPortfolioNumber();
		}
		return portfolioNumber;
	}

	public void setPortfolioNumber(String portfolioNumber) {
		this.portfolioNumber = portfolioNumber;
	}
	
	public void tabActivityLog_Click()
    {
    	String methodName="tabActivityLog_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(investmentOpportunityView.getOpportunityId().toString()!=null && investmentOpportunityView.getOpportunityId().toString().trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(notesOwner,investmentOpportunityView.getOpportunityId().toString());
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }
}
