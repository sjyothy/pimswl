package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import pimsrenewcontractproxy.proxy.PIMSRenewContractBPELPortClient;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractDisclosureReportCriteria;
import com.avanza.pims.report.criteria.PrintLeaseContractReportCriteria;
import com.avanza.pims.report.criteria.RequestDetailsReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.EjariIntegration;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.EjariResponse;
import com.avanza.pims.ws.vo.PaidFacilitiesView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;

public class RenewContractBackingBean extends AbstractController {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5295461664678854738L;
	private int index;
	private boolean currentFine;
	private boolean currentViolation;
	private String PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID = "paymentScheduleOwnerShipTypeId";
	private String UNIT_VIEW = "unitView";
	private String TAB_PAYMENT = "PaymentTab";
	private String TAB_UNIT = "tabUnit";
	private String TAB_CONTRACT_DETAILS = "contractDetailsTab";
	private String OLD_CONTRACT_VIEW="OLD_CONTRACT_VIEW";
	private List<PaymentScheduleView> fine = new ArrayList();
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	private List<ViolationView> violations = new ArrayList();
	private Long contractId;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private Double oldRentValue;
	private Double oldUnitRentValue;
	private Double oldRentValueSaved;
	private Double suggestedRentAmount; 
	private String newRentAmount;
	private RequestDetailsReportCriteria requestDetailsReportCriteria;
	private String dateFormatForDataTable;
	private String contractNumber;
	private String msgs;
	CommonUtil commonUtil = new CommonUtil();
	private ContractView contractView;
	CommonUtil commUtil = new CommonUtil();
	private String tenantName;
	private String contractType;
	private String contractStatus;
	private String contractStartDateString;
	private String contractEndDateString;
	private Date contractStartDate;
	private Date contractEndDate;
	private UnitView unitView;
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	private String paidFacilitiesAll="";
	
	UserTask userTask;
	
	public String contractAction;
	
	public String oldContractId;
	private String ownerShipTypeIdHid;
	private ResourceBundle bundle;
	HtmlTabPanel tabPanel = new HtmlTabPanel();
	HtmlTab tabUnit = new HtmlTab();
	public boolean showOldContractOutputText;
	
	public boolean finesOrViolationsExisits;
	
	private HtmlDataTable dataTableFine;
	private HtmlDataTable dataTablePaymentSchedule;
	private HtmlDataTable dataTableViolation;
	private HtmlInputText rentValueTxt =  new HtmlInputText();
	private HtmlInputText cashValueTxt =  new HtmlInputText();
	private HtmlInputText numberofChqTxt =  new HtmlInputText();
	private HtmlInputText txtUnitRentValue =  new HtmlInputText();
	
	private HtmlSelectOneMenu contractActionMenu = new HtmlSelectOneMenu();
	org.richfaces.component.html.HtmlTab  violationsTab = new  org.richfaces.component.html.HtmlTab();
	private HtmlCommandButton approveButton = new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	private HtmlCommandButton okButton = new HtmlCommandButton();
	private HtmlCommandButton sendForApprovalButton = new HtmlCommandButton();
	//private HtmlCommandButton reqPayFeesButton = new HtmlCommandButton();
	private HtmlCommandButton reqCompleteButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton= new HtmlCommandButton();
	//private HtmlCommandButton btnCollectPayments = new HtmlCommandButton();
	private HtmlCommandButton btnGeneratePayments = new HtmlCommandButton();
	private HtmlCommandButton btnRegisterInEjari = new HtmlCommandButton();
	private HtmlCommandButton btnAddPayments = new HtmlCommandButton();
	private HtmlCommandButton btnPrint=new HtmlCommandButton();
	private HtmlCommandButton btnPrintDisclosure=new HtmlCommandButton();
	private HtmlCommandButton btnCancelRenewContractButton = new HtmlCommandButton();
	
	private HtmlSelectBooleanCheckbox chkPaySch = new HtmlSelectBooleanCheckbox();
	public boolean disableChkBoxes;
	public String unitRentAmount;
	//public boolean fromTaskListCollectPayment;
	
	private HtmlDataTable paidFacilitiesDataTable;
	private List<PaidFacilitiesView> paidFacilitiesDataList = new ArrayList<PaidFacilitiesView>();
	private PaidFacilitiesView paidFacilitiesDataItem = new PaidFacilitiesView();
	private List<PaymentScheduleView> psDataList=new ArrayList<PaymentScheduleView>(0);
	private HtmlDataTable tbl_PaymentSchedule;
	
	//public boolean fromReqList = false;
	
	public Integer numberOfChq;
	
	private List<String> errorMessages = new ArrayList();
	
	private String datesAll;
	private String chqsAll;
	private String cash;
	private DomainDataView ddvContractTypeCommercial=new DomainDataView();
	
	Map sessionMap;
	
	NotesController notesControl = new NotesController();
	
	FacesContext context;
	HttpSession session;
	
	//private String fromRenwList;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private Long tenantId;
	private Long requestId;
	
	private double totalRealizedRentAmount;
	private double totalRentAmount;
	private double totalCashAmount;
	private double totalChqAmount;
	private int installments;
	private double totalDepositAmount;
	private double totalFees;
	private double totalFines;
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;

	private List<String> selectedPayments;
	public String LIST_REQUEST_STATUS="listRequestStatus";
	public String HAS_EDIT_RENT_AMOUNT_PERMISSION="HAS_EDIT_RENT_AMOUNT_PERMISSION";
	public String HAS_EDIT_START_DATE_PERMISSION="HAS_EDIT_START_DATE_PERMISSION";
	boolean unitRentAmountEditable= false;
	boolean startDateEditPermission= false;
	
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

	
	private static Logger logger = Logger.getLogger(RenewContractBackingBean.class);
	
	public interface Page_Mode {
		
		public static final String PAGE_MODE         = "PAGE_MODE";
		public static final String ADD               = "ADD";
		public static final String EDIT              = "EDIT";
		public static final String APPROVE_REJECT    = "APPROVE_REJECT";
		public static final String COLLECT_PAYMENT   = "COLLECT_PAYMENT";
		public static final String COMPLETE          = "COMPLETE";
		public static final String VIEW              = "VIEW";
		public static final String ACTIVE            = "ACTIVE";
		
	}
	public RenewContractBackingBean()
	{
		context = FacesContext.getCurrentInstance();
		session =(HttpSession) context.getExternalContext().getSession(true);
		sessionMap = context.getExternalContext().getSessionMap();
	}
	public String getDatesAll() {
		return datesAll;
	}

	public void setDatesAll(String datesAll) {
		this.datesAll = datesAll;
	}

	public String getChqsAll() {
		return chqsAll;
	}

	public void setChqsAll(String chqsAll) {
		this.chqsAll = chqsAll;
	}

	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        
		logger.logInfo(methodName+"|"+"Start..");
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-10)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(context);
        addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScriptText);
        

		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	

	/**
	 * Iterates through the Payment Receipt View's PaymentReceiptDetailView 
	 * to add the amounts and return the amount total 
	 * @param paymentReceiptView
	 * @return
	 */
	private Double getPaymentAmountTotal(PaymentReceiptView paymentReceiptView) {
		Double totalAmount = new Double(0);
        try{
			Iterator paymentReceiptIterator = paymentReceiptView.getPaymentReceiptDetails().iterator();
			while(paymentReceiptIterator.hasNext()){
				PaymentReceiptDetailView paymentReceiptDetailView = (PaymentReceiptDetailView)paymentReceiptIterator.next();
				totalAmount += paymentReceiptDetailView.getAmount();
			}
        } catch (Exception exception) {
			logger.LogException("getPaymentAmountTotal| crashed ...", exception);
		}   
		return totalAmount;
	}
	

	 @SuppressWarnings( "unchecked" )
	 private void CollectPayment()
	 {
			try
			{
		    	 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			     PropertyServiceAgent psa=new PropertyServiceAgent();
			     contractView  = (ContractView)viewMap.get("contractView");
			     prv.setPaidById(contractView.getTenantView().getPersonId());
			     prv = psa.collectPayments(prv);
			     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			     //Refresh Payment Schedule list here
			     contractId = (Long) sessionMap.get("contractId");
			     getContractView();
			     getContractPaymentSchedules();
			     getPaymentSchedules();
			     sessionMap.remove("contractId");
                 if(viewMap.get(Page_Mode.PAGE_MODE).equals(Page_Mode.COLLECT_PAYMENT) )
                 {
                    if(isAllPaymentsCollected())
                    {
                       collectReqFees();
                       //if(isAllPaymentsCollected())
			           viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.COMPLETE);
			           
			           // Change Request automatically complete the request after collect payment
			           completeReq();
			           
                    }
                    else
                    	this.enableCollectPayment();
			     }
                 
    		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.RENEW_CONTRACT);
			     getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
			}
			catch(Exception ex)
			{
				logger.logError( "CollectPayment|Exception Occured..."+ex);
			}
	    }
	public void btnPrintPaymentSchedule_Click()
	{
	    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.RENEW_CONTRACT);
	}
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()   {
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
    }
	
	@SuppressWarnings("unchecked")
	public String openReceivePaymentsPopUp()
	{
		try
		{
			List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
			for (PaymentScheduleView paymentScheduleView : this.getPaymentSchedules())
			{
				if ( paymentScheduleView.getSelected()!= null && paymentScheduleView.getSelected() )
				{
					paymentsForCollection.add( paymentScheduleView );
				}
			}
			if( paymentsForCollection.size() > 0 )
			{
			 sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
			 sessionMap.put("contractId", contractId);
	         PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	         sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	         openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");
			}
			else
			{
				errorMessages = new ArrayList<String>( 0 );
				errorMessages.add(ResourceUtil.getInstance().getProperty( "renewContract.msg.noPaymentSelected" ));
			}
		}
		catch( Exception e )
		{
			errorMessages = new ArrayList<String>( 0 );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
            logger.LogException("openReceivePaymentsPopUp|Error Occured", e);
		}
		return "";
	}
	
	public String showPop()
	{
		
		return "";
	}
	
	public boolean getIsArabicLocale()
	{
		
		
		isArabicLocale = !getIsEnglishLocale();
		
		
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings("unchecked")
	public void btnEditPaymentSchedule_Click()
    {
    	String methodName = "btnEditPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) )
    		sessionMap.put(
    				        WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
    				        viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)
    				       );
    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
    	if( ispageModeCollectPayments() || ispageModeComplete() )
        	sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
    	if( psv.getIsSystem() != null && psv.getIsSystem().longValue() == 1l )
    	{
	    	sessionMap.put("STOP_AMOUNT_EDIT",true);
    	}
        openPopUp("", "javascript:openPopupEditPaymentSchedule()");
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	public void btnAddPayments_Click()
    {
    	final String methodName = "btnAddPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> PS=new ArrayList<PaymentScheduleView>();
    	
    	try
    	{
    			if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) )
    		    {
    		     PS = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,PS);
    		    }
    			
    			 String extrajavaScriptText ="var screen_width = 1024;"+
                 "var screen_height = 450;"+
                 "window.open('PaymentSchedule.jsf?totalContractValue="+newRentAmount+
                 "&ownerShipTypeId="+viewMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+
    			 "','_blank','width='+(screen_width-300)+',height='+(screen_height-50)+',left=200,top=250,scrollbars=yes,status=yes');";
    			 openPopUp("PaymentSchedule.jsf", extrajavaScriptText);

    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
	@SuppressWarnings( "unchecked" )
	public String generatePayments()
	{
		
		String methodName="generatePayments";
		logger.logInfo(methodName+"|"+"Start..");
		List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try 
		{
			if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		    {
			 removeUnPaidRentFromPaymentScheduleSession();
		     alreadyPresentPS = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,alreadyPresentPS);
		     viewMap.put("payments", alreadyPresentPS);
		    }
			
				sessionMap.put("contractId", contractId);
		        String extrajavaScriptText ="var screen_width = 1024;"+
                "var screen_height = 450;"+
                "window.open('GeneratePayments.jsf?ownerShipTypeId="+
                viewMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString()+"','_blank'," +
                		"'width='+(screen_width-200)+',height='+(screen_height)+',left=100,top=150,scrollbars=yes,status=yes');";
		        sessionMap.put(WebConstants.GEN_PAY_CONTRACT_START_DATE,df.format(this.getContractStartDate()));
 				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_END_DATE,df.format(this.getContractEndDate()));
		        sessionMap.put(WebConstants.DEPOSIT_AMOUNT,cash);
				sessionMap.put(WebConstants.TOTAL_CONTRACT_VALUE,newRentAmount);
				
				sessionMap.put(WebConstants.GEN_PAY_UPDATE_PAYMENT, true);
				
		        openPopUp("GeneratePayments.jsf", extrajavaScriptText);
    	   
		logger.logInfo(methodName+"|"+"Finish..");
			
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|"+"Error Occured..",e);
		}
    	
		
		return "";
	}
	@SuppressWarnings( "unchecked" )
	private void removeUnPaidRentFromPaymentScheduleSession()
    {
    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
    	List<PaymentScheduleView> psvList=(ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
    	for (PaymentScheduleView psv : psvList) 
    	{
		   //If payment is for Rent
    		if(psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 	)
			{
    			
    			//if Rent is paid
				if(!psv.getIsReceived().equals("N") ) {
					newPaymentScheduleViewList.add(psv);
				}
				else if (psv.getPaymentScheduleId()!=null){ // that are not paid but are in DB
					psv.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					psv.setRecordStatus(WebConstants.DELETED_RECORD_STATUS);
					newPaymentScheduleViewList.add(psv);
				}
			}
    		//If payment other than rent
			else
        	  	newPaymentScheduleViewList.add(psv);
        
    	}
    	viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
    
    }
	public String cancel()
	{
		sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		viewMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		String s="";
		
//		try {
//			
////			if (fromRenwList != null && fromRenwList.equals("true"))
////				s="cancelToList";
////			else if (fromReqList == true)
////				s="cancelToReqSearch";
////			else
////				s="cancelToTaskList";
////		
////		
////			notesControl.saveNotes(WebConstants.CONTRACT, new Long(contractId));
////			saveAttachments(contractId);
//			
//		} catch (PimsBusinessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		return s;
	}
	
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	
	private boolean isCorrectAmount()
	{
		boolean correct = false;
		boolean hasManintance = false;
		
		double sum = 0;
		
		try {
		
			if (new Double(newRentAmount) > 0)
			{
				Long modeId;
				
					modeId = WebConstants.PAYMENT_TYPE_DEPOSIT_ID;
				
				
				List<PaymentScheduleView> pvList = (List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				
				if (pvList != null)
				{
				
					for (int i=0; i<pvList.size();i++)
					{
						if ( pvList.get(i).getIsDeleted().longValue()!=1l &&
							 pvList.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue())
							sum = sum + pvList.get(i).getAmount();
						else
							hasManintance = true;
					}
				}
				if (sum == new Double(newRentAmount))
				{
					//if (hasManintance)
						correct = true;
					
						
				} 
				
				if (sum != new Double(newRentAmount))
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_CONTRACT_VALUE));
				
//				if (!hasManintance)
//					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_MNT_NOT_FOUND));
				
			
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return correct;
	}


	
	@SuppressWarnings("unchecked")
	public void getPaymentSchedulesFromPage(ContractView thisContractView)
	{
		
		try {
		

			//For New 
			Set<PaymentScheduleView> pset = new HashSet<PaymentScheduleView> (0);
			
			List<PaymentScheduleView> pvList = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for (int i=0;i<pvList.size();i++)
			{
			
				if ( pvList.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue() ||
					 pvList.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue())
				pvList.get(i).setRequestId(requestId);
				
				pset.add(pvList.get(i));
			}
			
			thisContractView.setPaymentScheduleView(pset);
		

		
		} catch (Exception e) {
			logger.LogException("Error occured", e );
		}
		
	}
	@SuppressWarnings("unchecked")
	private void addFeeToPaymentScheduleSession() throws PimsBusinessException,Exception
	{
		List<PaymentScheduleView> NewpaymentScheduleViewList=new ArrayList();
//		if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//		{
//			List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewtMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			paymentScheduleViewList = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_FEES_ID);
//			NewpaymentScheduleViewList.addAll(paymentScheduleViewList);
//		}
		DomainDataView ddvCash = new UtilityServiceAgent().getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
		List<PaymentScheduleView>  feesList =getPaymentScheduleFromPaymentConfiguration() ;
		
			for (PaymentScheduleView paymentScheduleView : feesList) 
			{
				paymentScheduleView.setPaymentModeId(ddvCash.getDomainDataId());
				paymentScheduleView.setPaymentModeEn(ddvCash.getDataDescEn());
				paymentScheduleView.setPaymentModeAr(ddvCash.getDataDescAr());
				paymentScheduleView.setShowEdit(true);
				NewpaymentScheduleViewList.add(paymentScheduleView);
			}
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, NewpaymentScheduleViewList);
		
	}
	private List<PaymentScheduleView> getPaymentScheduleFromPaymentConfiguration() throws PimsBusinessException,Exception
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		Long ownerShipTypeId = new Long(viewMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString());
		HashMap feesConditionsMap  = new HashMap();
		DomainDataView ddv = new DomainDataView();
		if(this.contractView.getContractTypeId().compareTo(WebConstants.RESIDENTIAL_LEASE_CONTRACT_ID)==0)
		{
			ddv =CommonUtil.getIdFromType(
					CommonUtil.getDomainDataListForDomainType(WebConstants.CONDITION),
					"CONTRACT_TYPE_RESIDENTIAL_FEES"
					  );
			
		}
		else if(this.contractView.getContractTypeId().compareTo(WebConstants.COMMERCIAL_LEASE_CONTRACT_ID)==0)
		{
			ddv =CommonUtil.getIdFromType(
					CommonUtil.getDomainDataListForDomainType(WebConstants.CONDITION),
					"CONTRACT_TYPE_COMMERCIAL_FEES"
					  );
			
		}
		feesConditionsMap.put(ddv.getDomainDataId(), ddv.getDomainDataId());
		if(newRentAmount!=null && !newRentAmount.equals("0"))
		{
		 return psa.getPaymentScheduleFromPaymentConfiguration(
				 												WebConstants.PROCEDURE_TYPE_RENEW , 
				 												null,//WebConstants.PAYMENT_TYPE_FEES_ID, 
				 												feesConditionsMap, 
				 												Double.valueOf( newRentAmount), 
				 												getIsEnglishLocale(),
				 												ownerShipTypeId
				 												);
		}
		else
		 return null;

	}
public void getPaymentSchedulesWithFeesFromPage(ContractView thisContractView){
		
		try {
		

			//For New 
			Set<PaymentScheduleView> pset = new HashSet(0);
			
			List<PaymentScheduleView> pvList = (List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			if (pvList != null)
			{
			
				for (int i=0;i<pvList.size();i++)
				{
					pset.add(pvList.get(i));
				}
			}  //If the List is null..So The payments are not generated hence one needs to generate the payments for deposit amount 
			
			
			pvList = (ArrayList<PaymentScheduleView>)context.getViewRoot().getAttributes().get("RENEW_FEES");
			
			if (pvList != null)
			{
				for (int i=0;i<pvList.size();i++)
				{
					pset.add(pvList.get(i));
				}
			}
			thisContractView.setPaymentScheduleView(pset);
		

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	@SuppressWarnings( "unchecked" )
	public void getContractUnitFromPage(ContractView thiscontractView){
		
		try {
		
			
			//For New 
			String[] s = paidFacilitiesAll.split(",");
			
			Set paidFacilitiesSet = new HashSet(0);
			
			for (int i=0;i<s.length;i++)
			{
				if (s[i] != null && s[i].length()>0)
				{
					PaidFacilitiesView psv = getPaidFacilityById(s[i]);
					psv.setPaidFacilitiesId(null);
					psv.setDateFrom(this.getContractStartDate());
					psv.setDateTo(this.getContractEndDate());
					paidFacilitiesSet.add(psv);
				}
			}
			
			
			if (thiscontractView.getContractUnitView() != null && thiscontractView.getContractUnitView().size()>0)
			{
				
				Iterator it = thiscontractView.getContractUnitView().iterator(); 
				ContractUnitView conUnitView = (ContractUnitView) it.next();
				conUnitView.setRentValue(new Double(this.getUnitRentAmount()));
				if(this.getSuggestedRentAmount() != null)
					conUnitView.setSuggestedRentAmount( new Double( this.getSuggestedRentAmount() )  );
				conUnitView.setUnitView((UnitView)viewMap.get(UNIT_VIEW));
				if (conUnitView.getContractUnitId() != null )
				{
					conUnitView.setPaidFacilitieses(paidFacilitiesSet);
					
					Set<ContractUnitView> cUSet = new HashSet(0);
					
					cUSet.add(conUnitView);
					thiscontractView.setContractUnitView(cUSet);
				} else
				{
					Set<ContractUnitView> cUSet = new HashSet(0);
					
					ContractUnitView cU = new ContractUnitView();
					cU.setPaidFacilitieses(paidFacilitiesSet);
					
					cUSet.add(cU);
					
					thiscontractView.setContractUnitView(cUSet);
				}
			} else
			{
				Set<ContractUnitView> cUSet = new HashSet(0);
				
				ContractUnitView cU = new ContractUnitView();
				cU.setRentValue( new Double( this.getUnitRentAmount() ) );
				cU.setSuggestedRentAmount( new Double( this.getSuggestedRentAmount() )  );
				cU.setUnitView((UnitView)viewMap.get(UNIT_VIEW));
				cU.setPaidFacilitieses(paidFacilitiesSet);
				
				cUSet.add(cU);
				
				thiscontractView.setContractUnitView(cUSet);
			}
			
		
		

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
	public PaidFacilitiesView getPaidFacilityById(String id)
	{
		PaidFacilitiesView pfv = new PaidFacilitiesView();
		getPaidFacilitiesDataList();
		for (int i=0; i<paidFacilitiesDataList.size();i++)
		{
			if (paidFacilitiesDataList.get(i).getFacilityId().toString().equals(id))
			{				
				paidFacilitiesDataList.get(i).setSelected(true);
				pfv = paidFacilitiesDataList.get(i);
				break;
			}
		}
		
		
		return pfv;
	}
	
	public String getDateFormat()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return localeInfo.getDateFormat();
		
	}
	public void populateViolations()
	{
		String methodName = "populateViolations";
		try
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			HashMap<String,Object> searchCriteria = new HashMap<String, Object>();
			List<DomainDataView> ddl= CommonUtil.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
			DomainDataView ddViolationClosed = CommonUtil.getIdFromType(ddl, WebConstants.VIOLATION_STATUS_CLOSED);
			DomainDataView ddViolationRemoved    = CommonUtil.getIdFromType(ddl, WebConstants.VIOLATION_STATUS_REMOVED);
            List<Long> statusIdIn = new ArrayList<Long>(0);
            statusIdIn.add(ddViolationClosed.getDomainDataId());
            statusIdIn.add(ddViolationRemoved .getDomainDataId());
            //Searching for violations against a Tenant
            searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.VIOLATION_STATUS_NOT_IN, statusIdIn);
			searchCriteria.put(WebConstants.VIOLATION_SEARCH_CRITERIA.TENANT_ID, this.getTenantId());
			this.setViolations(psa.getAllInspectionViolation(searchCriteria ,"dd/MM/yyyy"));
		 
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured", ex);
		}
	}
	private boolean hasSaveErrors()throws Exception
	{
		boolean hasErrors= false;
		populateViolations();
		contractView  = (ContractView)viewMap.get("contractView");
		
		if(!viewMap.containsKey(WebConstants.LOCAL_PERSON_ID) || viewMap.get(WebConstants.LOCAL_PERSON_ID)==null)
	    {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_APPLICANT_NOT_FOUND));
			hasErrors = true;
	    }
		
		if( hasContractDateErrors() )
		  	  hasErrors = true;
		
		if (CommonUtil.isOldPendingPaymentsExists( new Long(this.getOldContractId()), getOldContractDetails().getRentAmount() ))
		{
			//Please Pay Old Payments
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAY_OLD_PAYENTS));
			hasErrors = true;
		}
		if ( isAnyPaymentDueOnOutSideContractDuration() )
		{
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(("renewContract.MSG.paymentDueOnOutsideContractDuration")));
    		hasErrors = true;
		}
		if(!isCorrectAmount())
		   hasErrors =true;
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
    		hasErrors = true;
    	}
		if(contractView.getTenantView().getIsBlacklisted().compareTo(1L)==0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("tenant.blackList.NoTransaction"));
			hasErrors=true;
		}
		else if(this.getViolations()!=null && this.getViolations().size()>0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("tenant.openViolation.noTransaction"));
			hasErrors=true;
		}
		return hasErrors;
	}
	public boolean isAnyPaymentDueOnOutSideContractDuration() throws Exception
	{
		DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
		
		for (PaymentScheduleView paymentScheduleView : this.getPaymentSchedules()) 
			if ( paymentScheduleView.getIsDeleted() != null && 
				 paymentScheduleView.getStatusId().compareTo( getPaymentScheduleExemptedStausId() )!=0 && 
				 (
				  df.parse( df.format( paymentScheduleView.getPaymentDueOn() ) ).compareTo( df.parse( df.format( this.getContractStartDate() )))<0 ||
				  df.parse( df.format( paymentScheduleView.getPaymentDueOn() ) ).compareTo( df.parse( df.format( this.getContractEndDate() )))>0 )
			    )
				return true;
		
		
		return false;
		
		
		
	}
	
	public String cancelRenewContract() {
		
		try {
			
			getContractView();
		
			if (psa.cancelRenewContract(contractView)){
				getMsg(CommonUtil.getParamBundleMessage("renewContract.cancelled",contractView.getContractNumber()));
				logger.logDebug(CommonUtil.getParamBundleMessage("renewContract.cancelled",contractView.getContractNumber()));
				
				viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
				
				Long requestId=contractView.getRequestsView().iterator().next().getRequestId(); 
				
				NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_CANCELED,requestId);
				
				saveComments(requestId);
				saveAttachments(requestId);
				
				List<DomainDataView>ddvList = (ArrayList<DomainDataView>) viewMap.get(LIST_REQUEST_STATUS);
				
				DomainDataView ddv = CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
				RequestView requestView = contractView.getRequestsView().iterator().next();
				
				if(ddv.getDomainDataId().compareTo(requestView.getStatusId())!=0) {
					String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
					BPMWorklistClient bpmWorkListClient;
					bpmWorkListClient = new BPMWorklistClient(contextPath);
					UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
					bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.CANCEL);	
				}
				
				viewMap.put(Page_Mode.PAGE_MODE, "");
				refreshAllValues();
				getRequestDetails();
				populateApplicationDetailsTab();
				tabPanel.setSelectedTab("ApplicationDetailsTab");
				
				
			}
		}	
	    catch (PimsBusinessException pbe)  {
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    	logger.LogException("cancelRenewContract|"+" Error Occured...",pbe);
	    }
	    catch (Exception e) {
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("cancelRenewContract|"+" Error Occured...",e);
		}
		
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String saveDraft()
	{
		String methodName ="saveDraft|";
		errorMessages=new ArrayList<String>(0);
		try 
		{
			logger.logInfo(methodName +"Start");
			if( !hasSaveErrors() )
			{
			   DomainDataView ddContractCategory = CommonUtil.getIdFromType(
			    		                              CommonUtil.getDomainDataListForDomainType(WebConstants.ContractCategory.CONTRACT_CATEGORY), 
			    		                                                                        WebConstants.ContractCategory.LEASE_CONTRACT
			    		                                                       );
				if ( viewMap.get( Page_Mode.PAGE_MODE ).toString().compareTo( Page_Mode.ADD ) !=0 )
				{
					ContractView contractViewDraft = createContractViewForUpdate( ddContractCategory );
				 	psa.updateRenewContract( contractViewDraft );
					getMsg( ResourceUtil.getInstance().getProperty( MessageConstants.RenewContract.MSG_REQUEST_UPDATED ) );
					
					tabPanel.setSelectedTab("ApplicationDetailsTab");
					
				}
				else
				{
			        ContractView  contractView  = persistContract( ddContractCategory );
					requestId = contractView.getRequestsView().iterator().next().getRequestId();
					getMsg( ResourceUtil.getInstance().getProperty( MessageConstants.RenewContract.MSG_CONTRACT_SAVED)+
							getRequestNumberByRequestId( requestId ) );
					refreshAllValues();
					getRequestDetails();
					NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_CREATED, requestId);
					viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.EDIT);
					populateApplicationDetailsTab();
					tabPanel.setSelectedTab("ApplicationDetailsTab");
				}
				//If the Contract is Just Created
				if ( viewMap.get( Page_Mode.PAGE_MODE ).toString().compareTo( Page_Mode.ADD ) ==0  || 
						viewMap.get( Page_Mode.PAGE_MODE ).toString().compareTo( Page_Mode.EDIT ) ==0 
				   )
				{
				 saveComments(requestId);
				 saveAttachments(requestId);
				 viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				}
			}
			logger.logInfo(methodName +"Finish");
		}
        catch (PimsBusinessException e) 
        {
	      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	      logger.LogException("saveDraft|"+" Error Occured...",e);
		}
        catch (Exception e) 
        {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("saveDraft|"+" Error Occured...",e);
		}
		return "";
	}
	@SuppressWarnings("unchecked")
    public void sendForApproval(){
		
		String methodName="sendForApproval";
		errorMessages = new ArrayList<String>(0);
		try 
		{
			logger.logInfo(methodName+"|Start");
//			      if (!hasErrors())
			      {
			    	RequestServiceAgent rsa = new RequestServiceAgent();
					SystemParameters parameters = SystemParameters.getInstance();
		            String endPoint= parameters.getParameter(WebConstants.RENEW_CONTRACT_ENDPOINT);
					PIMSRenewContractBPELPortClient proxy = new PIMSRenewContractBPELPortClient();
					proxy.setEndpoint(endPoint);
					proxy.initiate(contractId,requestId,getLoggedInUser(), null, null);
					
					rsa.setRequestAsApprovalRequired(requestId,getLoggedInUser());
					notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Renew_Request_Received);
					getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_TASK_SENT));
					NotesController.saveNotes(WebConstants.CONTRACT, new Long(contractId));
					saveAttachments(contractId);
					NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, requestId);
					viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVE_REJECT);
					CommonUtil.printReport(requestId);
		     	 }
		     	 
		     	 	logger.logInfo(methodName+"|Finish");
			
		}catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName+"| Error Occured...",e);
		}
		
		
	}

	@SuppressWarnings("unchecked")
	public void onRenewInEjari()
	{	
		errorMessages = new ArrayList<String>(0);
		try
		{
			RequestView requestView = (RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
			ContractView contractView  = (ContractView)viewMap.get("contractView");
			Contract oldContract = EntityManager.getBroker().findById(Contract.class, contractView.getOldContractId());
			//If Old Contract Already Registered In Ejari then renew it else create new contract in ejari
			if(oldContract.getEjariNumber() != null && oldContract.getEjariNumber().trim().length()>0)
			{
				EjariResponse response= EjariIntegration.renewContract(
						   												contractView.getContractId(),
						   												oldContract,
						   												getLoggedInUser()
												   					 );
				if(response.getStatus().equals("0"))
				{
					errorMessages.add( response.getMsg() );
				}
				else
				{
				   btnRegisterInEjari.setRendered(false);
				   NotesController.saveSystemNotesForRequest(
					   										  WebConstants.PROCEDURE_TYPE_RENEW,
					   										  "contract.event.ContractRenewedInEjari", 
					   										  requestView.getRequestId()
						   									);
				}
			}
			else
			{
				EjariResponse response= EjariIntegration.registerContract(
																			contractView.getContractId(),
																			getLoggedInUser()
													   					 );
				if(response.getStatus().equals("0"))
				{
					errorMessages.add( response.getMsg() );
				}
				else
				{
				  btnRegisterInEjari.setRendered(false);
				   NotesController.saveSystemNotesForRequest(
						   										WebConstants.PROCEDURE_TYPE_RENEW,
						   										"contract.event.ContractRegisteredInEjari", 
						   										requestView.getRequestId()
						   									);
				}
				
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("onRenewInEjari| Exception Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
    @SuppressWarnings("unchecked")
    public void appoveTask(){
		
		String methodName ="appoveTask";
		errorMessages = new ArrayList<String>(0);
		
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try 
		{
			contractView  = (ContractView)viewMap.get("contractView");
			if(!hasSaveErrors() )
			{
			    logger.logInfo(methodName+"|Start");
			    
				saveDraft();
				if(!viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.APPROVE);
				new PropertyServiceAgent().ApproveRenewContract(contractView.getContractId(), requestId, getLoggedInUser() );
				notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Renew_Request_Approved);
				getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_TASK_APPROVED));
				sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				saveComments( requestId );
				saveAttachments( requestId );
				NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_APPROVED, requestId);
				viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.COLLECT_PAYMENT);
			}
			logger.logInfo(methodName+"|Finish");

		} catch (IOException e) {
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (PIMSWorkListException e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (NumberFormatException e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (PimsBusinessException e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (Exception e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);		}
		
		//return "cancelToReqSearch";
	}
    @SuppressWarnings("unchecked")
	public void rejectTask(){
		
		String methodName ="rejectTask";
		errorMessages = new ArrayList<String>(0);
		
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			if(!viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				  getIncompleteRequestTasks();
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.REJECT);
			contractView  = (ContractView)viewMap.get("contractView");
			new PropertyServiceAgent().RejectRenewContract(contractView.getContractId(), requestId, getLoggedInUser() );
			new PropertyService().setPaymentScheduleCanceled(requestId);
			getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_TASK_REJECTED));
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			saveComments(requestId);
			saveAttachments(requestId);
			
			NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_REJECTED, requestId);
			viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
			logger.logInfo(methodName+"|Finish");

		}  catch (IOException e) {
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (PIMSWorkListException e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (NumberFormatException e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (PimsBusinessException e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);
		} catch (Exception e) {
                    errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
                      logger.LogException(methodName+"|Error Occured", e);		}
				
		//return "cancelToReqSearch";
	}
	
	public void okTask(){
		
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			UserTask userTask = (UserTask) session.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.OK);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PIMSWorkListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//return "cancelToReqSearch";
	}
	@SuppressWarnings("unchecked")
	private ContractView createContractViewForUpdate( DomainDataView ddContractCategory ) 
	{
		ContractView contractViewDraft = (ContractView)viewMap.get("contractView");
		contractViewDraft.setContractCategory(ddContractCategory.getDomainDataId() );
		contractViewDraft.setPaymentScheduleView(null);
		getPaymentSchedulesFromPage(contractViewDraft);
		getContractUnitFromPage(contractViewDraft);
		contractViewDraft.setStartDate(this.getContractStartDate() );
		contractViewDraft.setEndDate(this.getContractEndDate());
		rentValueTxt.getValue();
		contractViewDraft.setRentAmount(new Double(newRentAmount));
		contractViewDraft.setUpdatedBy(getLoggedInUser());
		contractViewDraft.setUpdatedOn(new Date());
		
		return contractViewDraft;
	}
	
	@SuppressWarnings("unchecked")
	private ContractView  persistContract(DomainDataView ddContractCategory)
	throws PimsBusinessException {
		
		ContractView contractView = new ContractView();
		RequestView req = new RequestView();
		PersonView applicant = new PersonView();
		PersonView tenant = new PersonView();
		DomainDataView ddv = CommonUtil.getIdFromType(
				                                       CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
				                                                                                 WebConstants.REQUEST_STATUS_NEW
	                                                 );
		DomainDataView ddvRenewDraft = CommonUtil.getIdFromType( 
				                                               CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS), 
	                                                                                                     WebConstants.CONTRACT_STATUS_RENEW_DRAFT
	                                                           );
		getPaymentSchedulesWithFeesFromPage(contractView);
		getContractUnitFromPage(contractView);
		contractView.setStatus(ddvRenewDraft.getDomainDataId());
		contractView.setContractCategory(ddContractCategory.getDomainDataId() );
		contractView.setRentAmount(new Double(newRentAmount));
		contractView.setStartDate(this.getContractStartDate());
		contractView.setContractDate(new Date());
		contractView.setEndDate(this.getContractEndDate());
		contractView.setCreatedBy(getLoggedInUser());
		contractView.setCreatedOn(new Date());
		contractView.setUpdatedBy(getLoggedInUser());
		contractView.setUpdatedOn(new Date());
		contractView.setRecordStatus(1L);
		contractView.setIsDeleted(0L);
		//contractView.setOldContractId(contractId);
		//this.setOldContractId(OlcontractId.toString());
		createRequestView(req, applicant, tenant, ddv);
		contractView.getRequestsView().add(req);		
		
		contractView = psa.saveRenewContract(new Long(getOldContractId()), contractView,false);
		contractId = contractView.getContractId();
		return contractView ;
    }
	private void createRequestView( RequestView req, PersonView applicant,
			PersonView tenant, DomainDataView ddv) 
	{
		applicant.setPersonId(Long.parseLong((String)viewMap.get(WebConstants.LOCAL_PERSON_ID)));
		tenant.setPersonId(tenantId);
		String desc = (String) viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
		req.setApplicantView(applicant);
		req.setTenantsView(tenant);
		req.setDescription(desc);
		req.setStatusId(ddv.getDomainDataId());
		req.setRequestTypeId(WebConstants.REQUEST_TYPE_RENEW_CONTRACT);
		req.setCreatedBy(getLoggedInUser());
		req.setCreatedOn(new Date());
		req.setUpdatedBy(getLoggedInUser());
		req.setUpdatedOn(new Date());
		req.setRecordStatus(1L);
		req.setIsDeleted(0L);
		req.setRequestDate(new Date());
		req.setUnitVacated(0l);
	}
    @SuppressWarnings("unchecked")	
	public void getContractView() throws PimsBusinessException 
	{
		contractView = new ContractView();
		contractView.setContractId(contractId);
		HashMap contractHashMap = new HashMap();
		contractHashMap.put("contractView", contractView);
		contractHashMap.put("dateFormat", getDateFormat());
		contractView = psa.getAllContracts(contractHashMap).get(0);
		viewMap.put("contractView",contractView ); 
		putViewValuesInControl();
		
		
	}
	@SuppressWarnings("unchecked")
	private void putViewValuesInControl() throws PimsBusinessException 
	{
		
		if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
		{
		  Iterator<ContractUnitView> iter=contractView.getContractUnitView().iterator();
		  while(iter.hasNext())
		  {
			  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
			  viewMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,contractUnitView.getUnitView().getPropertyCategoryId());
			  viewMap.put(UNIT_VIEW,contractUnitView.getUnitView());
			  this.setUnitRentAmount( contractUnitView.getRentValue().toString() );
			  if( viewMap.get(Page_Mode.PAGE_MODE)!=null && !viewMap.get(Page_Mode.PAGE_MODE).toString().equals( Page_Mode.ADD )  )
			      this.setSuggestedRentAmount( contractUnitView.getSuggestedRentAmount() );
		      break;
		  }		    	
		}
		//For setting the values for Contract Details////
		contractNumber = contractView.getContractNumber();
		contractStartDateString =contractView.getStartDateString();
		contractEndDateString =contractView.getEndDateString();
		this.setContractEndDate(contractView.getEndDate());
		this.setContractStartDate(contractView.getStartDate());
		if (contractView.getTenantView() != null)
		{
			if (contractView.getTenantView().getPersonFullName() != null && contractView.getTenantView().getPersonFullName().length()>0)
				tenantName = contractView.getTenantView().getPersonFullName();
			else
				tenantName =contractView.getTenantView().getCompanyName();
			
			tenantId = contractView.getTenantView().getPersonId();
		}
		if (getIsArabicLocale())
		{
			contractType = contractView.getContractTypeAr();
			contractStatus = contractView.getStatusAr();
		}
		else
		{
			contractType   = contractView.getContractTypeEn();
			contractStatus = contractView.getStatusEn();
		}
		//Add all the selected Paid Facilities' Amounts with Suggested Amount Here
		tabPaidFacility_Click();
		List<PaymentScheduleView> pv = psa.getContractPaymentSchedule(contractView.getContractId(),null);
		if (pv != null && pv.size()>0)
		{
			Iterator iterator = pv.iterator();
			Long modeIdMaintainence = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_TYPE, WebConstants.PAYMENT_SCHEDULE_TYPE_MAINTAINENCE).getDomainDataId();
			Long modeIdFee = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_TYPE, WebConstants.PAYMENT_SCHEDULE_TYPE_LATE_RENEW_FINE).getDomainDataId();
			psDataList.clear();
			while (iterator.hasNext())
			{
				
				PaymentScheduleView pmv = (PaymentScheduleView) iterator.next();
				if (statusChq(pmv,modeIdMaintainence))
				{
					cash = pmv.getAmount().toString();
					viewMap.put("MAINTAINENCE_PAYMENT",pmv);
				}
				if (statusChq(pmv,modeIdFee))
					psDataList.add(pmv);
			}
			viewMap.put("RENEW_FEES",psDataList);
		}
	}
	@SuppressWarnings( "unchecked" )
	public ContractView getContractViewForDraft()
	{
		
		ContractView contractViewDraft = new ContractView();
		contractViewDraft.setContractId(contractId);
		HashMap contractHashMap = new HashMap();
		contractHashMap.put("contractView", contractViewDraft);
		contractHashMap.put("dateFormat", getDateFormat());
		try 
		{
			contractViewDraft = psa.getAllContracts(contractHashMap).get(0);
		} 
		catch (PimsBusinessException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contractViewDraft;
		
	}
	@SuppressWarnings( "unchecked" )
	public boolean statusChq(PaymentScheduleView paymentScheduleView,Long modeId)
	{
		boolean chqStatus = false;
		try 
		{
			if (paymentScheduleView.getTypeId()!=null && paymentScheduleView.getTypeId().longValue() == modeId.longValue())
				chqStatus = true;
			
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return chqStatus;
		
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		String methodName ="init";
		logger.logInfo(methodName+"|Start");
		try {
				super.init();
				sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
				putHiddenValuesInViews();
				//For receiving Payments
				if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
					if(isPaymentCollected)
				    {
							CollectPayment();
							sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				    }
				}
				//IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
		 	   //is copied to viewRoot of this page
		        if(
		     		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
		        )
		        {
		     	   viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		     	   
		        }    	
				if (!isPostBack())
				{
					
					
					String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR);
					if(daysInYear!=null)
				         viewMap.put(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR,daysInYear );
					
					viewMap.put( HAS_EDIT_RENT_AMOUNT_PERMISSION, hasEditUniRentAmountPermission() );
					viewMap.put( HAS_EDIT_START_DATE_PERMISSION, hasEditStartDatePermission() );
					//It means page is in add mode
					viewMap.put(LIST_REQUEST_STATUS,CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS));
					loadAttachmentsAndComments(null);
					clearSessions();
					dateFormatForDataTable = getDateFormat();
					currentFine = true;
					String fromRenwList = (String) getRequestParam("fromRenewList");
					if (FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
						 fromRequestScreen();
					else if (fromRenwList != null && fromRenwList.equals("true"))
					{
						viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.ADD);
						fromSearchContract();
					}
					else
						fromTaskList();
					if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
						populateApplicationDetailsTab();
					if( viewMap.get( "contractView" ) != null )
					{
			          getPageModeFromRequestStatus();
			          if (viewMap.get(Page_Mode.PAGE_MODE).toString().compareTo(Page_Mode.ADD) ==0)
			           setContractDates();
					}
					if (viewMap.get(Page_Mode.PAGE_MODE).toString().compareTo(Page_Mode.ADD) ==0)
					{
						contractNumber="";
						contractStatus="";
						//For page in add mode current contract will be served as old contract for 
						//new application
						this.setOldContractId(contractView.getContractId().toString());
						

					}
					else if (contractView.getOldContractId() != null)
						this.setOldContractId(contractView.getOldContractId().toString());
					//TODO:Fee
					if (viewMap.get(Page_Mode.PAGE_MODE).toString().compareTo(Page_Mode.ADD)==0)
					{
							calculateSuggestedRentAmount();
//							getSystemPayments();
							addFeeToPaymentScheduleSession();
					}

					tabPaidFacility_Click();
					finesOrViolationsExisits = false;
					if (violations.size()>0 || fine.size()>0)
						finesOrViolationsExisits = true;
					if (!viewMap.containsKey("cashId") && !viewMap.containsKey("chqId"))
					{
						    UtilityServiceAgent usa = new UtilityServiceAgent();
							DomainDataView ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
							viewMap.put("cashId", ddv.getDomainDataId());
							ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
							viewMap.put("chqId", ddv.getDomainDataId());
					}
//					
					if (requestId != null)
					{
							viewMap.put("noteowner",WebConstants.REQUEST);
							viewMap.put("entityId", requestId);
							loadAttachmentsAndComments(requestId);
							viewMap.put("canAddAttachment",true);
					}
					
				}
				
				calculateSummaray();
				
		} catch (Exception e) {
			logger.LogException(methodName+"|Error Occured",e);
		}
	
	}
	@SuppressWarnings( "unchecked" )
	private Boolean hasEditUniRentAmountPermission()
	{
//		&& 
//        ( !pages$RenewContract.pageModeAdd || 
//          !pages$RenewContract.pageModeEdit || 
//          !pages$RenewContract.pageModeApproveReject  
//        ) 
		
		boolean hasPermission = false;
		User user = SecurityManager.getUser(getLoggedInUserId());
		hasPermission = user.hasPermission("Pims.Contract.Renew.EditUnitRentAmount");
		return hasPermission;
	}
	@SuppressWarnings( "unchecked" )
	private Boolean hasEditStartDatePermission()
	{
		boolean hasPermission = false;
		User user = SecurityManager.getUser( getLoggedInUserId() );
		hasPermission = user.hasPermission("Pims.Contract.Renew.EditStartDate");
		return hasPermission;
	}
	@SuppressWarnings("unchecked")
	private ContractView getOldContractDetails()throws PimsBusinessException
	{
        if(!viewMap.containsKey(OLD_CONTRACT_VIEW))
        {
        	ContractView oldContractView= new ContractView();
	        oldContractView.setContractId(new Long(this.getOldContractId()));
			HashMap contractHashMap = new HashMap();
			contractHashMap.put("contractView", oldContractView);
			contractHashMap.put("dateFormat", getDateFormat());
		    oldContractView = psa.getAllContracts(contractHashMap).get(0);
			viewMap.put(OLD_CONTRACT_VIEW,oldContractView);
			this.getOldRentValue();
			return oldContractView;
        }
        else
        	return ( ContractView )viewMap.get(OLD_CONTRACT_VIEW);
	}
	@SuppressWarnings( "unchecked" )
	private void getContractUnitView()
	{
		
		  ContractView contractView= (ContractView)viewMap.get("contractView");
		  Iterator<ContractUnitView> iter = contractView.getContractUnitView().iterator();
		  while(iter.hasNext())
		  {
			  ContractUnitView contractUnitView =iter.next();
			  viewMap.put("unitView",contractUnitView.getUnitView());
		      break;
		  }	
		
	}
    
    @SuppressWarnings("unchecked")
	private void fromTaskList()throws PimsBusinessException,Exception 
	{
	
		    userTask = (UserTask) session.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put( WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
			session.removeAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			contractId =Long.parseLong(userTask.getTaskAttributes().get("CONTRACT_ID"));
			requestId =Long.parseLong(userTask.getTaskAttributes().get("REQUEST_ID"));
			
			getRequestDetails();
			getPageModeFromRequestStatus();
			getContractView();
			
			
			newRentAmount = contractView.getRentAmount().toString();
			if (contractView.getOldContractId() != null)
				this.setOldContractId(contractView.getOldContractId().toString());
			getOldContractRentValue(contractView.getOldContractId());
			getContractView();
		    if (contractView != null && contractView.getContractNumber() != null)
			   contractNumber = contractView.getContractNumber();
		    RequestView request = getRequestDetails();
		    viewMap.put(WebConstants.REQUEST_VIEW, request );
		    getContractPaymentSchedules();
		    
	}

    @SuppressWarnings( "unchecked" )
	private void fromSearchContract() throws PimsBusinessException 
	{
		
		contractId = (Long) getRequestParam("contractId");
		oldRentValueSaved = (Double) getRequestParam("oldRentValue");
		getContractView();
    	//setSuggestedRentAmount( (Double) getRequestParam("suggestedRentAmount") );
	    newRentAmount = String.valueOf( getSuggestedRentAmount() );
		oldRentValue = oldRentValueSaved;
		if (contractView != null && contractView.getContractNumber() != null)
			contractNumber = contractView.getContractNumber();
	}
    @SuppressWarnings("unchecked")
	private void fromRequestScreen()throws PimsBusinessException
	{
		
		RequestView request = (RequestView) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
		viewMap.put( WebConstants.REQUEST_VIEW, request );
		getPageModeFromRequestStatus();
		contractId = request.getContractView().getContractId();
		requestId = request.getRequestId();
		getContractView();
		getContractPaymentSchedules();
		contractNumber = contractView.getContractNumber();
		newRentAmount = String.valueOf(contractView.getRentAmount());
		
		if (isRequestStatusEqualToArgument(WebConstants.REQUEST_STATUS_NEW))
		{
			oldRentValueSaved = getOldRentValue();
			newRentAmount = String.valueOf(contractView.getRentAmount());
			oldRentValue = oldRentValueSaved;
		}
	}
    @SuppressWarnings( "unchecked" )
	public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			RequestView rv =(RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}
	}
	
	private void clearSessions() {
		//Removing any Session's Value for Payment Schedules
		if (sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	}
	@SuppressWarnings( "unchecked" )
	private void putHiddenValuesInViews() 
	{
		if(hdnPersonId!=null && !hdnPersonId.equals("")){
                viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
                viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
        }
        if(hdnPersonName!=null && !hdnPersonName.equals(""))
                viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
        if(hdnCellNo!=null && !hdnCellNo.equals(""))
                viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
        if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
                DomainDataView ddv = new DomainDataView();
                if(Long.parseLong(hdnIsCompany)==1L){
                        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
                                        WebConstants.TENANT_TYPE_COMPANY);}
                else{
                        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
                                        WebConstants.TENANT_TYPE_INDIVIDUAL);}
               viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
            }
	}
	
	@SuppressWarnings( "unchecked" )	
	 private List<DomainDataView> getDomainDataListForDomainType(String domainType)

	    {

	      List<DomainDataView> ddvList=new ArrayList<DomainDataView>();

	      List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);

	            Iterator<DomainTypeView> itr = list.iterator();

	            //Iterates for all the domain Types

	            while( itr.hasNext() ) 

	            {

	                  DomainTypeView dtv = itr.next();

	                  if( dtv.getTypeName().compareTo(domainType)==0 ) 

	                  {

	                        Set<DomainDataView> dd = dtv.getDomainDatas();

	                        //Iterates over all the domain datas

	                        for (DomainDataView ddv : dd) 

	                        {

	                          ddvList.add(ddv);     

	                        }

	                        break;

	                  }

	            }

	      

	      return ddvList;

	    }


		@Override
		@SuppressWarnings( "unchecked" )
		protected void afterUpdateModelValues() {
			// TODO Auto-generated method stub
			super.afterUpdateModelValues();
			errorMessages = new ArrayList<String>(0);
			checkUnitRentAmount();
		}
		private void checkUnitRentAmount() {
			try
			{
				
				if(this.getUnitRentAmount() != null && this.getUnitRentAmount().toString().trim().length()>0)
				{
					Double rentAmount = new Double (this.getUnitRentAmount() )+getSumOfAllPaidFacilities();
			 	    newRentAmount = String.valueOf(rentAmount );
			
				}
				else
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_REQUIRED_UNIT_RENT));
					tabPanel.setSelectedTab(TAB_UNIT);
				}
				
				
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_INVALID_UNIT_RENT));
				tabPanel.setSelectedTab(TAB_UNIT);
			}
		}
	@Override
	@SuppressWarnings( "unchecked" )
	public void prerender() {
        String methodName ="prerender";
        logger.logInfo(methodName+"|Start");
        try 
        {
            super.prerender();
	    	putHiddenValuesInViews();
    		enableDisableControls();
			getOldContractDetails();
			logger.logInfo(methodName+"|Finish");
		} 
        catch (Exception e)
        {
    		logger.LogException(methodName+"|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void enableDisableControls()
	{
		String methodName="enableDisableControls";
		String pageMode = viewMap.get(Page_Mode.PAGE_MODE).toString();
		logger.logInfo(methodName+"|PageMode :::"+pageMode );
		disableAll();
		viewMap.put("applicationDetailsReadonlyMode",true);
		violationsTab.setRendered(false);
		if( ispageModeAdd()  )
		{
			viewMap.put("applicationDetailsReadonlyMode",false);
			PageModeAdd();
			if(this.getViolations() != null && this.getViolations().size()>0)
			  violationsTab.setRendered(true);
		}
		else if(  ispageModeEdit() )
			PageModeEdit();
		else if(  ispageModeApproveReject() )
			PageModeApproveReject();
		else if( ispageModeCollectPayments() )
			PageModeCollectPayment();
		else if( ispageModeComplete() )
			PageModeComplete();
		else if( ispageModeView() )
			PageModeView();
	}
	@SuppressWarnings( "unchecked" )
	private void disableAll() {
		btnAddPayments.setRendered(false);
		btnGeneratePayments.setRendered(false);
		//reqPayFeesButton.setRendered(false);
		btnCollectPayment.setRendered( false );
		chkPaySch.setRendered(false);
		approveButton.setRendered(false);
		rejectButton.setRendered(false);
		sendForApprovalButton.setRendered(false); 
		reqCompleteButton.setRendered(false);
		btnRegisterInEjari.setRendered(false);
		saveButton.setRendered(false);
		btnPrint.setRendered(false);
		btnPrintDisclosure.setRendered(false);
		btnCancelRenewContractButton.setRendered(false);
	}
	@SuppressWarnings( "unchecked" )
	private void PageModeAdd()
	{
		btnAddPayments.setRendered(true);
		btnGeneratePayments.setRendered(true);
		saveButton.setRendered(true);
		viewMap.put("canAddAttachment", true); 
	    viewMap.put("canAddNote", true);
	    btnPrint.setRendered(false);
	    btnPrintDisclosure.setRendered(false);
	    btnCancelRenewContractButton.setRendered(false);
	}
	@SuppressWarnings( "unchecked" )
	private void PageModeEdit()
	{
		btnAddPayments.setRendered(true);
		btnGeneratePayments.setRendered(true);
		sendForApprovalButton.setRendered(true); 
		saveButton.setRendered(true);
		viewMap.put("canAddAttachment", true); 
	    viewMap.put("canAddNote", true);
	    btnPrint.setRendered(true);
	    btnPrintDisclosure.setRendered(false);
		btnCancelRenewContractButton.setRendered(true);
	}
	@SuppressWarnings( "unchecked" )
	private void PageModeApproveReject()
	{
		if(!isPostBack())
		  tabPanel.setSelectedTab(TAB_CONTRACT_DETAILS);
		approveButton.setRendered(true);
		rejectButton.setRendered(true);
		viewMap.put("canAddAttachment", true); 
	    viewMap.put("canAddNote", true);
	    btnPrint.setRendered(true);
	    btnPrintDisclosure.setRendered(false);
	    
	    btnAddPayments.setRendered(true);
		btnGeneratePayments.setRendered(true);
		btnCancelRenewContractButton.setRendered(false);
		
		
	}
	@SuppressWarnings( "unchecked" )
	private void PageModeCollectPayment()
	{
		if(!isPostBack())
		    tabPanel.setSelectedTab(TAB_PAYMENT);
		//reqPayFeesButton.setRendered(true);
		if( newRentAmount != null)
	    {
	    	 double contractValue = new Double( newRentAmount );
	    	 if( getTotalRentAmount() != contractValue )
	    	 {
	    		errorMessages  = new ArrayList<String>(0);
	           	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
	           	tabPanel.setSelectedTab(TAB_PAYMENT );
	           	btnCollectPayment.setRendered(false);
	           	chkPaySch.setRendered(false);
	    	 }
		     else
		     {
		      btnCollectPayment.setRendered(true);
		      chkPaySch.setRendered(true);
		     }
	    }
		viewMap.put("canAddAttachment", true); 
	    viewMap.put("canAddNote", true);
	    btnPrint.setRendered(false);
	    btnPrintDisclosure.setRendered(false);
		btnCancelRenewContractButton.setRendered(true);
	}
	@SuppressWarnings( "unchecked" )
	private void PageModeComplete()
	{
		reqCompleteButton.setRendered(true);
		btnRegisterInEjari.setRendered(true);
		viewMap.put("canAddAttachment", true); 
	    viewMap.put("canAddNote", true);
	    btnPrint.setRendered(false);
	    btnPrintDisclosure.setRendered(false);
		btnCancelRenewContractButton.setRendered(false);
	}
	private void PageModeView()
	{
		  btnPrint.setRendered(true);
		  btnPrintDisclosure.setRendered(true);
		  if( viewMap.get("contractView") != null )
		  {
			  contractView  = (ContractView)viewMap.get("contractView");
			  if(
					contractView.getEjariNumber() != null && 
					contractView.getEjariNumber().trim().length()>0 
				)
			  {
				btnRegisterInEjari.setRendered(true);	
			  }
		  }
	}
	@SuppressWarnings( "unchecked" )
	private boolean isRequestStatusEqualToArgument(String argRequestStatus)
	{
		List<DomainDataView>ddvList = (ArrayList<DomainDataView>) viewMap.get(LIST_REQUEST_STATUS);
		RequestView requestView = (RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
		DomainDataView domainDataView = CommonUtil.getIdFromType(ddvList, argRequestStatus);
		if(requestView.getStatusId().compareTo(domainDataView.getDomainDataId())==0)
              return true;
		
		return false;

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()
	{
		if(viewMap.containsKey(WebConstants.REQUEST_VIEW) && viewMap.get(WebConstants.REQUEST_VIEW)!=null)
		{
			List<DomainDataView>ddvList = (ArrayList<DomainDataView>) viewMap.get(LIST_REQUEST_STATUS);
			RequestView requestView = (RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
			for (DomainDataView domainDataView : ddvList) {
				if(requestView.getStatusId().compareTo(domainDataView.getDomainDataId())==0)
				{
					if(domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NEW)==0 )
					     viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.EDIT);
					else if(domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED)==0 )
					     viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.APPROVE_REJECT);
					else if(domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_APPROVED)==0 )
						 viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.COLLECT_PAYMENT);
					else if(domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_REJECTED)==0 )
						 viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
					else if(domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_APPROVED_COLLECTED)==0 )
						 viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.COMPLETE);
					else if (domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_COMPLETE)==0) {
						 viewMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
					}
					else if (domainDataView.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CANCELED)==0){
						 viewMap.put(Page_Mode.PAGE_MODE,"");
					}
					
					break;
				}
				
			}
		}
		else
			viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.ADD);
		
	}
	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
	public LocaleInfo getLocaleInfo()
	{
	    	
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	
	     return localeInfo;
		
	}
	@SuppressWarnings( "unchecked" )
	private void setContractDates()
    {
    	
    	     DateFormat df= new SimpleDateFormat(getDateFormat());
    		 ContractView contractView =(ContractView)viewMap.get("contractView");
    	     Calendar calStart=new GregorianCalendar();
    	     logger.logInfo("oldcontractEndDate"+contractView.getEndDate());
    	     calStart.setTime(contractView.getEndDate());
    	     calStart.add(Calendar.DATE, 1);
    	     this.setContractStartDate(calStart.getTime());
    	     logger.logInfo("contractStartDate"+this.getContractStartDate());
    	     Calendar calEnd =(Calendar)calStart.clone();
    	     calEnd.add(Calendar.YEAR, 1);
    	     calEnd.add( Calendar.DATE, -1);
		     this.setContractEndDate(calEnd.getTime());
		     
		     logger.logInfo("contractEndDate"+this.getContractEndDate());
    	
    }
	@SuppressWarnings( "unchecked" )
	private void getRequestIdByContractId()
	{
		RequestView request;
		
		try {
			
			
			request = psa.getRequestByContractId(contractId);
			viewMap.put(WebConstants.REQUEST_VIEW,request);
			if (request != null)
				requestId = request.getRequestId();
			
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	@SuppressWarnings( "unchecked" )
	private RequestView getRequestDetails() throws PimsBusinessException,Exception
	{
		RequestView request = null;
		if (requestId != null)
		{
	    	request = psa.getRequestById(requestId);
	        viewMap.put(WebConstants.REQUEST_VIEW, request );
	    } 		
		return request;
	}
	@SuppressWarnings( "unchecked" )
	private void populateApplicationDetailsTab() 
	{
		
		RequestView request =(RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
		if (request.getRequestNumber() !=null)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER,request.getRequestNumber());
		if (request.getRequestDate() !=null)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,request.getRequestDate());
		if (request.getDescription() !=null)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,request.getDescription());
		if (isArabicLocale)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,request.getStatusAr());
		else
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,request.getStatusEn());
		//For Applicant
		if(request.getApplicantView().getPersonId()!=null && !request.getApplicantView().getPersonId().equals(""))
		    viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,request.getApplicantView().getPersonId());
	    if(request.getApplicantView().getFirstName()!=null && !request.getApplicantView().getFirstName().equals(""))
		    viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME, request.getApplicantView().getPersonFullName());
	    if(request.getApplicantView().getPersonTypeId()!=null && !request.getApplicantView().getPersonTypeId().equals(""))
	        viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, getIsEnglishLocale()?request.getApplicantView().getPersonTypeEn():request.getApplicantView().getPersonTypeAr());
	    if(request.getApplicantView().getCellNumber()!=null && !request.getApplicantView().getCellNumber().equals(""))
	        viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,request.getApplicantView().getCellNumber());
	    if(request.getApplicantView().getIsCompany()!=null && !request.getApplicantView().getIsCompany().equals(""))
	    {
	        DomainDataView ddv = new DomainDataView();
	        if( request.getApplicantView().getIsCompany() == 1L )
	         ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE), WebConstants.TENANT_TYPE_COMPANY);
	        else
	         ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE), WebConstants.TENANT_TYPE_INDIVIDUAL);
	         viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
        }
	    if(request.getApplicantView().getPersonId()!=null)
	    {
	    	hdnPersonId = request.getApplicantView().getPersonId().toString();
	        viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
	    }
	}
	
	private String getRequestNumberByRequestId(Long reqId)
	{
		RequestView request;
		String reqNumber="";
		try {
			
			
			request = psa.getRequestById(reqId);
			
			if (request != null)
				reqNumber = request.getRequestNumber();
			
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reqNumber;
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsRequestStatusAproved(RequestView request)
	{
		Long statusID;
		boolean approved = false;
		
		try {
			
			//Checking for status "Approved"
			statusID = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVED).getDomainDataId();
			if (statusID != null && request.getStatusId() != null && request.getStatusId().longValue() == statusID.longValue())
				approved = true;
			
			//Checking for status "Accepted Approved"
			statusID = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_ACCEPTED_APPROVED).getDomainDataId();
			if (statusID != null && request.getStatusId() != null && request.getStatusId().longValue() == statusID.longValue())
				approved = true;
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return approved;
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsRequestStatusCollected(RequestView request)
	{
		Long statusID;
		boolean collected = false;
		
		try {
			
			//Checking for status "Approved_collected"
			statusID = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVED_COLLECTED).getDomainDataId();
			if (statusID != null && request.getStatusId() != null && request.getStatusId().longValue() == statusID.longValue())
				collected = true;
			
			//Checking for status "Accepted_collected"
			statusID = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_ACCEPTED_COLLECTED).getDomainDataId();
			if (statusID != null && request.getStatusId() != null && request.getStatusId().longValue() == statusID.longValue())
				collected = true;
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return collected;
	}
	
	public boolean getIsRequestStatusComplete(RequestView request)
	{
		Long statusID;
		boolean complete = false;
		
		try {
			
			//Checking for status "COMPLETE"
			statusID = psa.getDomainDataByValue(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId();
			if (statusID != null && request.getStatusId() != null && request.getStatusId().longValue() == statusID.longValue())
				complete = true;
			
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return complete;
	}
	@SuppressWarnings( "unchecked" )
	private void refreshAllValues()throws PimsBusinessException
	{

		dateFormatForDataTable = getDateFormat();
		context = FacesContext.getCurrentInstance();
		session =(HttpSession) context.getExternalContext().getSession(true);
		currentFine = true;
		if (viewMap.get(Page_Mode.PAGE_MODE).equals(Page_Mode.ADD))
		{
			getContractView();
			if (contractView != null && contractView.getContractNumber() != null)
				contractNumber = contractView.getContractNumber();
		}
		else 
		{
				RequestView request = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
				contractId = request.getContractView().getContractId();
				requestId = request.getRequestId();
				getContractView();
				getContractPaymentSchedules();
				if (contractView != null && contractView.getContractNumber() != null)
					contractNumber = contractView.getContractNumber();
				if(contractView.getRentAmount()!=null) 
				    newRentAmount = String.valueOf(contractView.getRentAmount());
		}
		if (contractView.getOldContractId() != null)
			this.setOldContractId(contractView.getOldContractId().toString());

		finesOrViolationsExisits = false;
		if (violations.size()>0 || fine.size()>0)
			finesOrViolationsExisits = true;
		//For view Notes
		if (showOldContractOutputText)
		{
			viewMap.put("noteowner", "contractId");
			viewMap.put("entityId", oldContractId);
			loadAttachmentsAndComments(contractId);
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void getContractPaymentSchedules()throws PimsBusinessException
	{
		paymentSchedules = psa.getContractPaymentSchedule(contractId,null);
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, paymentSchedules);
		viewMap.put("payments", paymentSchedules);
	}
	public boolean isContractStartDateGreaterThanMaxDaysAllowedAfterExpiryDate()throws Exception
	{
		Calendar start = new GregorianCalendar();
		start.setTime( this.getContractStartDate() );
		Calendar end = new GregorianCalendar();
		end.setTime( getOldContractDetails().getEndDate() );
		
		if ( DateUtil.getDaysBetween( start,end )> getMaxDaysAllowedAfterExpiryOfOldContract() + 1 )
			return true;
		return false;
		
	}
	public boolean isContractStartDateBeforeOldContractExpiry()throws Exception
	{
		if ( this.getContractStartDate().compareTo( getOldContractDetails().getEndDate() ) <= 0 )
			return true;
		return false;	
		
		
	}
	public boolean hasContractDateErrors()throws Exception
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if(this.getContractStartDate ()== null || this.getContractEndDate() == null)
    	{
    	      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
    	      return true;
    	}
		if ( isContractStartDateBeforeOldContractExpiry() )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "renewContract.MSG.newStartDateLessThanOldExpiryDate" ) );
			return true;
		}
		if (df.parse(df.format(this.getContractStartDate())).compareTo(df.parse(df.format(this.getContractEndDate() )))>=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
			return true;
		}
		else if( isContractStartDateGreaterThanMaxDaysAllowedAfterExpiryDate()  )
		{
			errorMessages.add( java.text.MessageFormat.format(
					                   ResourceUtil.getInstance().getProperty( "renewContract.MSG.newStartDateExceedMaxDaysAfterOldExpiryDate" ),
					                   getMaxDaysAllowedAfterExpiryOfOldContract()
			                            )
			                  );
			return true;
		}
		
		return false;
		
	}
	@SuppressWarnings( "unchecked" )
    public void onContractDurationChanged()
    {
	  String methodName = "onContractDurationChanged |";
      try
      {
    	  
    	  errorMessages = new ArrayList<String>( 0 );
		  if( !hasContractDateErrors() && !ispageModeApproveReject() )
		  {
	   	    getUnitAmuntForSelectedDuration();
	   	    newRentAmount = String.valueOf( this.getUnitRentAmount() + getSumOfAllPaidFacilities()  );
		  }
      }
      catch (Exception e)
      {
    	  logger.LogException( methodName +"Error Occured:", e );
      }
    }
	@SuppressWarnings( "unchecked" )
	private Double getUnitAmuntForSelectedDuration()throws Exception
	{
	  String methodName="getUnitAmuntForSelectedDuration";
	  //Double unitAmount;
	  BigDecimal unitAmount;
	  int dateDiffDays=1;
//	  Double unitDailyRent=new Double(0);
	//  Double daysInYear= new Double(viewMap.get(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR).toString());
	  BigDecimal unitDailyRent=new BigDecimal(0.0);
	  BigDecimal daysInYear= new BigDecimal(viewMap.get(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR).toString());
	  
	  logger.logInfo(methodName+"|Start|Calculating unit's daily rent amount by formula " +
		  		" Unit Offered Rent Amount/days in year from system config:"+ getSuggestedRentAmount()+"/" +daysInYear );
	  //unitDailyRent=new Double( getSuggestedRentAmount() ) / daysInYear;
	  unitDailyRent=new BigDecimal( getSuggestedRentAmount() ).divide( new BigDecimal( daysInYear.doubleValue() ),20,RoundingMode.HALF_UP);
	  logger.logInfo(methodName+"|Value of unit's daily rent amount by formula:%s|Contract Start Date:%s" +
	  		"|Contract End Date:%s ",unitDailyRent,getContractStartDate(),getContractEndDate());
	  dateDiffDays= CommonUtil.getDaysDifference(getContractStartDate(),getContractEndDate());
	  logger.logInfo(methodName+"|Diff in days:%s",dateDiffDays);
	  //unitAmount = unitDailyRent * dateDiffDays;
	  unitAmount = unitDailyRent.multiply(  new BigDecimal(  dateDiffDays ) );
	  logger.logInfo(methodName+"|unitAmount:%s",unitAmount);
	  //unitAmount = Math.ceil( unitAmount );
	   
	  setUnitRentAmount(  new Double( Math.ceil( unitAmount.doubleValue() ) ).toString() );
	  logger.logInfo(methodName+"|Fiinish|Amount after ceiling:%s",new Double( Math.ceil( unitAmount.doubleValue() ) ).toString());
	  return new Double( Math.ceil( unitAmount.doubleValue() ) );
	}
	@SuppressWarnings( "unchecked" )
	private void calculateSuggestedRentAmount() throws Exception 
	{
		UnitView unitView = ( UnitView )viewMap.get( UNIT_VIEW );
		setSuggestedRentAmount( Math.ceil( psa.calculateSuggestedRentAmount( contractView ) ) );
		unitView.setRentValue(  getSuggestedRentAmount() );
		this.setUnitRentAmount( Double.toString( getSuggestedRentAmount() ) );
		viewMap.put( UNIT_VIEW, unitView );
		//Original
		//setSuggestedRentAmount( getSuggestedRentAmount() + getSumOfAllPaidFacilities() );
		//newRentAmount = String.valueOf( getSuggestedRentAmount() );
		//Changed
		getUnitAmuntForSelectedDuration();
		 
		newRentAmount =  String.valueOf( new Double ( this.getUnitRentAmount() ) + new Double ( getSumOfAllPaidFacilities() ) );
		
		
	}
	@SuppressWarnings( "unchecked" )
	private void getOldContractRentValue(Long oldContractId)
	{
		contractView = new ContractView();
		contractView.setContractId(oldContractId);
		HashMap contractHashMap = new HashMap();
		contractHashMap.put("contractView", contractView);
		contractHashMap.put("dateFormat", getDateFormat());
		try 
		{
			ContractView oldContractView = psa.getAllContracts(contractHashMap).get(0);
			oldContractId = oldContractView.getContractId();
			oldRentValue = oldContractView.getRentAmount();
		} 
		catch (PimsBusinessException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
		
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
//	@SuppressWarnings("unchecked")

	
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long contractIdForAttachment){
		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_RENEW);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_RENEW;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		if(contractIdForAttachment !=null)
		{
		String entityId = contractIdForAttachment.toString();
		
		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
		viewMap.put("entityId", entityId);
		}
	}
	

	
	@SuppressWarnings( "unchecked" )
	public void tabPaidFacility_Click()
	{
		String methodName="tabPaidFacility_Click";
		logger.logInfo(methodName+"|"+"Start...");
		Map viewRootMap = viewMap;
		List<PaidFacilitiesView> facilityViewList=new ArrayList<PaidFacilitiesView>(0);
		try
		{
					if(!viewRootMap.containsKey(WebConstants.PAID_FACILITIES))
					{
						facilityViewList=	 getAllPaidFacilities();
						for (PaidFacilitiesView paidFacilitiesView : facilityViewList) {
							if(paidFacilitiesView.getSelected())
							{
								addToSelectedFacilities(paidFacilitiesView);
							paidFacilitiesAll = paidFacilitiesAll+","+paidFacilitiesView.getFacilityId();
							}
						}
						 
		                 viewRootMap.put(WebConstants.PAID_FACILITIES, facilityViewList);
					}
					
				    //For Paging Labels/////
			        Map viewMap = getFacesContext().getExternalContext().getSessionMap();
			        recordSize = facilityViewList.size();
					viewMap.put("recordSize", recordSize);
					paginatorRows = getPaginatorRows();
					paginatorMaxPages = recordSize/paginatorRows;
					if((recordSize%paginatorRows)>0)
						paginatorMaxPages++;
					if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
						paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
					viewMap.put("paginatorMaxPages", paginatorMaxPages);
			        ////////////////////////
		}
		catch (Exception ex)
		{
			logger.LogException(methodName+"|"+"Exception Occured...",ex);
		}
		logger.logInfo(methodName+"|"+"Finish...");
		
	}
//	public void chkPaidFacilities_Changed()

    private void removeFromSelectedFacilities(PaidFacilitiesView pfv)
    {
    	   Map viewRootMap = viewMap;
    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
    	List<PaidFacilitiesView> newFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
        for (PaidFacilitiesView paidFacilitiesView : paidFacilitiesViewList) 
        {
        	if(pfv.getFacilityId().compareTo(paidFacilitiesView.getFacilityId())!=0)
		       newFacilitiesViewList.add(paidFacilitiesView);
        }
      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,newFacilitiesViewList);
    
    }
	 private void addToSelectedFacilities(PaidFacilitiesView pfv)
	    {
	    
		 Map viewRootMap = viewMap;
		 
		 List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
	      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) && viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null)
	    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
	       
	      	paidFacilitiesViewList.add(pfv);
	      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,paidFacilitiesViewList);
	    
	    }
	 @SuppressWarnings( "unchecked" )
	private  List<PaidFacilitiesView> getAllPaidFacilities() throws PimsBusinessException
    {
    	String methodName="getAllPaidFacilities";
		logger.logInfo(methodName+"|"+"Start...");
        PropertyServiceAgent psa=new PropertyServiceAgent();
        Long conId=contractId!=null && contractId>0? new Long(contractId): null;
        ContractView contractView = (ContractView )viewMap.get("contractView");
        Iterator contractUnitIterator =(contractView.getContractUnitView()).iterator();
        UnitView uv= ((ContractUnitView)contractUnitIterator.next()).getUnitView();
        List<PaidFacilitiesView> facilityViewList =psa.getPropertyFacilitiesForContractByPropertyId(uv.getPropertyId(),conId);
        logger.logInfo(methodName+"|"+"Finish...");
    	return facilityViewList;
    }
	 @SuppressWarnings( "unchecked" )
	public String calculateRentValueWithPaidFacilities() 
	{
		try {
		//For New 
		String[] s = paidFacilitiesAll.split(",");
		
		Set paidFacilitiesSet = new HashSet(0);
		
		for (int i=0;i<s.length;i++)
		{
			if (s[i] != null && s[i].length()>0)
			{
				PaidFacilitiesView psv = getPaidFacilityById(s[i]);
				paidFacilitiesSet.add(psv);
			}
		}
		getContractView();
		contractView.setRentAmount(new Double(newRentAmount));
		newRentAmount =psa.calculateNewRentAmountAfterAddingPaidFacilities(paidFacilitiesSet, contractView).toString();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	public boolean isCurrentViolation() {
		return currentViolation;
	}

	public void setCurrentViolation(boolean currentViolation) {
		this.currentViolation = currentViolation;
	}

	public boolean isCurrentFine() {
		return currentFine;
	}

	public void setCurrentFine(boolean currentFine) {
		this.currentFine = currentFine;
	}

	@SuppressWarnings( "unchecked" )
	public List<ViolationView> getViolations() {
		if(viewMap.containsKey("violations"))
			violations = (ArrayList<ViolationView>)viewMap.get("violations");
		return violations;
	}
	@SuppressWarnings( "unchecked" )
	public void setViolations(List<ViolationView> violations) {
		this.violations = violations;
		if(this.violations!= null && this.violations.size()>0)
			viewMap.put("violations", this.violations);
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public HtmlDataTable getDataTableFine() {
		return dataTableFine;
	}

	public void setDataTableFine(HtmlDataTable dataTableFine) {
		this.dataTableFine = dataTableFine;
	}

	public HtmlDataTable getDataTableViolation() {
		return dataTableViolation;
	}

	public void setDataTableViolation(HtmlDataTable dataTableViolation) {
		this.dataTableViolation = dataTableViolation;
	}

	
	public Double getOldRentValue() {
		if(viewMap.containsKey(OLD_CONTRACT_VIEW))
		{
			ContractView contractView =(ContractView)viewMap.get(OLD_CONTRACT_VIEW);
		    oldRentValue= contractView.getRentAmount();
		}
		return oldRentValue;
	}

	public void setOldRentValue(Double oldRentValue) {
		this.oldRentValue = oldRentValue;
	}
	@SuppressWarnings( "unchecked" )
	public Double getSuggestedRentAmount() {
		if( viewMap.get("suggestedRentAmount") != null )
			suggestedRentAmount = Double.parseDouble( viewMap.get("suggestedRentAmount").toString() );
		return suggestedRentAmount;
		
	}
   @SuppressWarnings( "unchecked" )
	public void setSuggestedRentAmount(Double suggestedRentAmount) {
		this.suggestedRentAmount = suggestedRentAmount;
		if(this.suggestedRentAmount!=null)
		viewMap.put("suggestedRentAmount" , this.suggestedRentAmount );
	}

	public HtmlInputText getRentValueTxt() {
		return rentValueTxt;
	}

	public void setRentValueTxt(HtmlInputText rentValueTxt) {
		this.rentValueTxt = rentValueTxt;
	}

	public String getNewRentAmount() {
		
		return newRentAmount;
	}
	

	public void setNewRentAmount(String newRentAmount) {
		this.newRentAmount = newRentAmount;
	}

	public Double getOldRentValueSaved() {
		return oldRentValueSaved;
	}

	public void setOldRentValueSaved(Double oldRentValueSaved) {
		this.oldRentValueSaved = oldRentValueSaved;
	}

	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}

	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}

	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}

	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}

	public HtmlCommandButton getOkButton() {
		return okButton;
	}

	public void setOkButton(HtmlCommandButton okButton) {
		this.okButton = okButton;
	}

	public HtmlCommandButton getSendForApprovalButton() {
		return sendForApprovalButton;
	}

	public void setSendForApprovalButton(HtmlCommandButton sendForApprovalButton) {
		this.sendForApprovalButton = sendForApprovalButton;
	}



	public String getCash() {
		return cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	
	public List<PaymentScheduleView> getFine() {
		return fine;
	}

	public void setFine(List<PaymentScheduleView> fine) {
		this.fine = fine;
	}

	public boolean isShowOldContractOutputText() {
		return showOldContractOutputText;
	}

	public void setShowOldContractOutputText(boolean showOldContractOutputText) {
		this.showOldContractOutputText = showOldContractOutputText;
	}

	public String getErrorMessages() {
		
		String methodName="getErrorMessages";
    	logger.logInfo(methodName+"|"+" Start");
		return	CommonUtil.getErrorMessages(errorMessages);
	}
	
	public void getMsg(String msg) {
		
		String methodName="getMsg";
    	logger.logInfo(methodName+"|"+" Start");
		
    	msgs="";
		msgs += "<LI>"+ msg+ " <br></br>" ;
    		logger.logInfo(methodName+"|"+" Finish");
		
		
	}
	@SuppressWarnings( "unchecked" )
	public String deletePayment()
	{
		try
		{
			int rowID = dataTablePaymentSchedule.getRowIndex();
			paymentSchedules = getPaymentSchedules();
			if( viewMap.get( Page_Mode.PAGE_MODE ).toString().compareTo( Page_Mode.ADD ) ==0  )
			{
				paymentSchedules.remove(rowID);
			}
			else
			{
			 paymentSchedules.get(rowID).setIsDeleted(1L);
			 paymentSchedules.get(rowID).setRecordStatus(0L);
			}
			viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
			calculateSummaray();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getContractAction() {
		return contractAction;
	}

	public void setContractAction(String contractAction) {
		this.contractAction = contractAction;
	}

	public String getOldContractId() {
		if(viewMap.containsKey("oldContractId"))
			oldContractId = viewMap.get("oldContractId").toString(); 
		return oldContractId;
	}
	@SuppressWarnings( "unchecked" )
	public void setOldContractId(String oldContractId) {
		this.oldContractId = oldContractId;
		if(this.oldContractId!=null)
			viewMap.put("oldContractId",this.oldContractId);
	}

	
	public HtmlInputText getCashValueTxt() {
		return cashValueTxt;
	}

	public void setCashValueTxt(HtmlInputText cashValueTxt) {
		this.cashValueTxt = cashValueTxt;
	}

	public HtmlSelectOneMenu getContractActionMenu() {
		return contractActionMenu;
	}

	public void setContractActionMenu(HtmlSelectOneMenu contractActionMenu) {
		this.contractActionMenu = contractActionMenu;
	}

	public boolean isFinesOrViolationsExisits() {
		return finesOrViolationsExisits;
	}

	public void setFinesOrViolationsExisits(boolean finesOrViolationsExisits) {
		this.finesOrViolationsExisits = finesOrViolationsExisits;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getMsgs() {
		return msgs;
	}

	public void setMsgs(String msgs) {
		this.msgs = msgs;
	}

	public Integer getNumberOfChq() {
		return numberOfChq;
	}

	public void setNumberOfChq(Integer numberOfChq) {
		this.numberOfChq = numberOfChq;
	}

	public HtmlInputText getNumberofChqTxt() {
		return numberofChqTxt;
	}

	public void setNumberofChqTxt(HtmlInputText numberofChqTxt) {
		this.numberofChqTxt = numberofChqTxt;
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentSchedules() {
		
		
	if (viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
		paymentSchedules = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		
		return paymentSchedules;
	}

	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}

	public String getDateFormatForDataTable() {
		return dateFormatForDataTable;
	}

	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractStartDateString() {
		return contractStartDateString;
	}

	public void setContractStartDateString(String contractStartDateString) {
		this.contractStartDateString = contractStartDateString;
	}

	public String getContractEndDateString() {
		return contractEndDateString;
	}

	public void setContractEndDateString(String contractEndDateString) {
		this.contractEndDateString = contractEndDateString;
	}

	public HtmlDataTable getPaidFacilitiesDataTable() {
		return paidFacilitiesDataTable;
	}

	public void setPaidFacilitiesDataTable(HtmlDataTable paidFacilitiesDataTable) {
		this.paidFacilitiesDataTable = paidFacilitiesDataTable;
	}
	@SuppressWarnings( "unchecked" )
	public List<PaidFacilitiesView> getPaidFacilitiesDataList() {
		
		Map viewRootMap = viewMap;
		
		if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES))
		paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);

		chkSelectedFacilities(paidFacilitiesDataList);
		
		if(paidFacilitiesDataList!=null)
		viewRootMap.put(WebConstants.PAID_FACILITIES,paidFacilitiesDataList);
		
		
		return paidFacilitiesDataList;
	}
	@SuppressWarnings( "unchecked" )
	private void chkSelectedFacilities(List<PaidFacilitiesView> psvList)
	{
		if (paidFacilitiesAll != null && paidFacilitiesAll.length()>0 )
		{
			Map selectedFacilitiesMap = new HashMap();
			 
			String[] s = paidFacilitiesAll.split(",");
			
			for (int i=0;i<s.length;i++)
			{
				if (s[i] != null && s[i].length()>0)
				{
					selectedFacilitiesMap.put(s[i], s[i]);
				}
			}
			
			for (int i=0;i<psvList.size();i++)
			{
				if (selectedFacilitiesMap.containsKey(psvList.get(i).getFacilityId().toString()))
					psvList.get(i).setSelected(true);
				else 
					psvList.get(i).setSelected(false);
			}
		}
		
	}
	@SuppressWarnings( "unchecked" )
	private double getSumOfAllPaidFacilities()
	{
		double sum = 0;
		List<PaidFacilitiesView> psvList = new ArrayList<PaidFacilitiesView>(0);
		if(viewMap.containsKey(WebConstants.PAID_FACILITIES))
			psvList = (ArrayList<PaidFacilitiesView>)viewMap.get(WebConstants.PAID_FACILITIES);
		if (paidFacilitiesAll != null && paidFacilitiesAll.length()>0 )
		{
			Map selectedFacilitiesMap = new HashMap();
			 
			String[] s = paidFacilitiesAll.split(",");
			
			for (int i=0;i<s.length;i++)
			{
				if (s[i] != null && s[i].length()>0)
				{
					selectedFacilitiesMap.put(s[i], s[i]);
				}
			}
			
			for (int i=0;i<psvList.size();i++)
			{
				if (selectedFacilitiesMap.containsKey(psvList.get(i).getFacilityId().toString()))
					sum = sum+psvList.get(i).getRent();
				
			}
		}
		
		return sum;
	}

	public void setPaidFacilitiesDataList(
			List<PaidFacilitiesView> paidFacilitiesDataList) {
		this.paidFacilitiesDataList = paidFacilitiesDataList;
	}

	public PaidFacilitiesView getPaidFacilitiesDataItem() {
		return paidFacilitiesDataItem;
	}

	public void setPaidFacilitiesDataItem(PaidFacilitiesView paidFacilitiesDataItem) {
		this.paidFacilitiesDataItem = paidFacilitiesDataItem;
	}

	public String getPaidFacilitiesAll() {
		return paidFacilitiesAll;
	}

	public void setPaidFacilitiesAll(String paidFacilitiesAll) {
		this.paidFacilitiesAll = paidFacilitiesAll;
	}

	public Integer getPaginatorMaxPages() {
		
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
		
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	@SuppressWarnings( "unchecked" )
	public Integer getRecordSize() {
		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public boolean isDisableChkBoxes() {
		
		if(ispageModeAdd() || ispageModeEdit())
		return false;
		 
		return true;
	}

	public void setDisableChkBoxes(boolean disableChkBoxes) {
		this.disableChkBoxes = disableChkBoxes;
	}

	@SuppressWarnings( "unchecked" )
	public String btnTenant_Click()
	{
		
		String methodName="btnTenant_Click";
		logger.logInfo(methodName+"|"+"Start..");
		context.getExternalContext().getRequestMap().put("tenantId", tenantId.toString());
		context.getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		
		logger.logInfo(methodName+"|"+"Finish..");
		return "TENANT_VIEW";
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String btnContract_Click()
	{
		
		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		context.getExternalContext().getSessionMap().put("contractId", this.getOldContractId());
		context.getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		
		String javaScriptText="var screen_width = 1024;"+
        "var screen_height = 450;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=10,scrollbars=no,status=yes');";
       openPopUp("LeaseContract.jsf",javaScriptText);
		
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	public Long getRentIdFromDomainData()
    {
//		Map viewRootMap = viewMap;
//	  CommonUtil commonUtil=new CommonUtil();
//    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE))
//    		loadPaymentType();
//    		
//    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE);
//    	DomainDataView ddv=commonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_TYPE_RENTED);
//    	return ddv.getDomainDataId();
		
		return WebConstants.PAYMENT_TYPE_RENT_ID;
    }
	
	private void loadPaymentType()
	{
	  String methodName="loadPaymentType";
	  logger.logInfo(methodName +"|"+"Start ");
	  Map viewRootMap = viewMap;
	  try
	  {
		  PropertyServiceAgent psa =new PropertyServiceAgent();
		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE))
		  {
		    List<DomainDataView>ddv= (ArrayList)psa.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_TYPE);
		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_TYPE, ddv);
		  }
		  
		  
	  }
	  catch (Exception e)
	  {
		logger.logError(methodName +"|"+"Exception Occured "+e);
	  }
	
	  logger.logInfo(methodName +"|"+"Finish ");
	
	
	
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPsDataList() {
		
		if(context.getViewRoot().getAttributes().containsKey("RENEW_FEES"))
			psDataList=(ArrayList<PaymentScheduleView>)context.getViewRoot().getAttributes().get("RENEW_FEES");
	
		return psDataList;
	}

	public void setPsDataList(List<PaymentScheduleView> psDataList) {
		this.psDataList = psDataList;
	}

	public HtmlDataTable getTbl_PaymentSchedule() {
		return tbl_PaymentSchedule;
	}

	public void setTbl_PaymentSchedule(HtmlDataTable tbl_PaymentSchedule) {
		this.tbl_PaymentSchedule = tbl_PaymentSchedule;
	}
	@SuppressWarnings( "unchecked" )	
	public void getSystemPayments()throws PimsBusinessException,Exception
	{
			Double dep = null;
			if ( cash != null && cash.trim().length() > 0 )
				dep = new Double( cash ); 
			Long ownerShipTypeId = new Long( viewMap.get( PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID ).toString() );
			paymentSchedules =  psa.getDefaultPaymentsForRenewContract( 
																		contractId, 
																		new Double(newRentAmount), 
																		dep,
					                                                    getIsEnglishLocale(), 
					                                                    ownerShipTypeId  
					                                                  );
			viewMap.put("payments", paymentSchedules);
			viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
	}
	@SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleExemptedStausId()
    {
		return getPaymentScheduleIdFromPaymentType( WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
    }
	@SuppressWarnings( "unchecked" )
    public Long getPaymentSchedulePendingStausId()
    {
		return getPaymentScheduleIdFromPaymentType( WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING );
    }
	@SuppressWarnings( "unchecked" )
    public Long getPaymentScheduleDraftStausId()
    {
    	
    	return getPaymentScheduleIdFromPaymentType( WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT );
    }
	@SuppressWarnings( "unchecked" )
	private Long getPaymentScheduleIdFromPaymentType( String paymentType )
	{
		
		DomainDataView ddv = new DomainDataView();
    	if(!viewMap.containsKey( paymentType ) )
    	{	
	    	if(! viewMap.containsKey( WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS ) ||
	    		 viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS)==null 
	    	  )
	    	{
	    	  List<DomainDataView> paymentStatusList = CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS );
	    	  viewMap.put( WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, paymentStatusList );
	    	}
	    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewMap.get( WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS );
	    	ddv= CommonUtil.getIdFromType( ddvList, paymentType );
	    	viewMap.put( paymentType, ddv );
    	}
    	else
    	  ddv = ( DomainDataView )viewMap.get( paymentType );
    	return ddv.getDomainDataId();
		
	}
	@SuppressWarnings( "unchecked" )
	public String collectReqFees()throws PimsBusinessException,Exception
	{
		RequestServiceAgent rsa = new RequestServiceAgent();
		RequestView req = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
		requestId = req.getRequestId();
		rsa.setRequestAsApprovedCollected(req.getRequestId(), getLoggedInUser());
		getRequestDetails();
		refreshAllValues();
		return "";
	}
	@SuppressWarnings( "unchecked" )
	private boolean isAllFeesCollected() throws PimsBusinessException
	{
		boolean allfeesCollected = true;
		
		getPaymentSchedules();
		
		Long modeIdFee = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_STATUS, WebConstants.PAYMENT_SCHEDULE_COLLECTED).getDomainDataId();
		
		
		for (int i=0; i<paymentSchedules.size(); i++)
		{
			//Only Rent Amount must be calculated [Commented to chk all the payments]
//			if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue() || paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue())
//			{
				if (paymentSchedules.get(i).getStatusId().longValue() != modeIdFee)
					allfeesCollected = false;
//			}
		}
		
		return allfeesCollected;
	}
	@SuppressWarnings( "unchecked" )
	public void completeReq()
	{
		errorMessages = new ArrayList<String>(0);
		try 
		{
		    ContractView cv = (ContractView)viewMap.get("contractView");
		    RequestView rv = (RequestView)viewMap.get(WebConstants.REQUEST_VIEW);
			PropertyService ps = new PropertyService();
			cv.setUpdatedBy( getLoggedInUserId() );
            ps.completeRenewRequest(cv, rv, CommonUtil.getLoggedInUser());			
			completeTask();
			refreshAllValues();
			getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_REQ_COMPLETED));
			viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
		} 
		catch (Exception e) 
		{
	          errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	          logger.LogException("completeReq|Error Occured", e);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	private boolean getIsOldContractTerminated()throws PimsBusinessException
	{
		ContractView cv = (ContractView)viewMap.get("contractView");
		return psa.getIsOldContractTerminated(cv.getContractId());
		
	}
	@SuppressWarnings( "unchecked" )
	private boolean isAllPaymentsCollected()
	{
		long draftId = getPaymentScheduleDraftStausId();
		long pendingId = getPaymentSchedulePendingStausId();
		
		List<PaymentScheduleView> psvList = getPaymentSchedules();
		for (PaymentScheduleView paymentScheduleView : psvList) 
			if( paymentScheduleView.getStatusId().longValue() == pendingId || paymentScheduleView.getStatusId().longValue() == draftId )
				return false;
		
	   return true;
	}

	
//	public HtmlCommandButton getReqPayFeesButton() {
//		return reqPayFeesButton;
//	}
//
//	public void setReqPayFeesButton(HtmlCommandButton reqPayFeesButton) {
//		this.reqPayFeesButton = reqPayFeesButton;
//	}

	public HtmlCommandButton getReqCompleteButton() {
		return reqCompleteButton;
	}

	public void setReqCompleteButton(HtmlCommandButton reqCompleteButton) {
		this.reqCompleteButton = reqCompleteButton;
	}

	public double getTotalCashAmount() {
		return totalCashAmount;
	}

	public void setTotalCashAmount(double totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}

	public double getTotalChqAmount() {
		return totalChqAmount;
	}

	public void setTotalChqAmount(double totalChqAmount) {
		this.totalChqAmount = totalChqAmount;
	}
	@SuppressWarnings( "unchecked" )
	public void calculateSummaray()
	{
		
		
		paymentSchedules = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	    if( paymentSchedules !=null && paymentSchedules.size() > 0 )
	    {
		Long cashId = (Long) viewMap.get("cashId");
		Long chqId = (Long) viewMap.get("chqId");
		List<DomainDataView> paymentStatusList;
		if(! viewMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS) || viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS)==null )
		{
			paymentStatusList	= CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);
			viewMap.put( WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, paymentStatusList );
		}
		else
		  paymentStatusList = (ArrayList<DomainDataView> )viewMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
		DomainDataView ddvDraftStatus     = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
		DomainDataView ddvPendingStatus   = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		DomainDataView ddvCollectedStatus = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_COLLECTED);
		DomainDataView ddvRealizedStatus  = CommonUtil.getIdFromType(paymentStatusList, WebConstants.REALIZED);
		DomainDataView ddvExemptedStatus  = CommonUtil.getIdFromType(paymentStatusList, WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
		totalCashAmount = 0;
		totalChqAmount = 0;
		totalRentAmount = 0;
		totalDepositAmount = 0;
 		totalFines = 0;
		totalFees = 0;
		installments = 0;
		totalRealizedRentAmount = 0;
		
		for (PaymentScheduleView paymentSchedule : paymentSchedules) 
		{
			//Checking Type
			if (paymentSchedule.getIsDeleted().compareTo(new Long(0))==0 && !CommonUtil.isPaymentBouncedReplacedOrExempted(paymentSchedule.getStatusId()))
			{
				if (paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue() &&
					  ( 
					   paymentSchedule.getStatusId().compareTo(ddvDraftStatus.getDomainDataId())==0 ||
					   paymentSchedule.getStatusId().compareTo(ddvPendingStatus.getDomainDataId())==0 ||
					   paymentSchedule.getStatusId().compareTo(ddvCollectedStatus.getDomainDataId())==0 ||
					   paymentSchedule.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0 
					  )
				    )
				{
					installments++;
					this.setTotalRentAmount( this.getTotalRentAmount()  + paymentSchedule.getAmount() );
					
					if (paymentSchedule.getStatusId().compareTo(ddvRealizedStatus.getDomainDataId())==0) { // realized rent amount
						totalRealizedRentAmount+=paymentSchedule.getAmount();
					}
					
				}
				else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue() &&
						  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0 
				         )
					this.setTotalDepositAmount(this.getTotalDepositAmount() + paymentSchedule.getAmount());
				else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue() &&
						  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0 
						 )
					this.setTotalFees( this.getTotalFees() + paymentSchedule.getAmount() );
				
				else if ( paymentSchedule.getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue() &&
						  paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) != 0 
						 )
					this.setTotalFines( this.getTotalFines()  + paymentSchedule.getAmount());
				//Checking Mode
				if ( paymentSchedule.getPaymentModeId() != null &&
					 paymentSchedule.getStatusId().compareTo( ddvExemptedStatus.getDomainDataId() ) !=0 	
				   )
				{
						if (paymentSchedule.getPaymentModeId().longValue() == cashId.longValue())
							this.setTotalCashAmount(this.getTotalCashAmount()  +  paymentSchedule.getAmount());
						else if (paymentSchedule.getPaymentModeId().longValue() == chqId.longValue())
							this.setTotalChqAmount( this.getTotalChqAmount() + paymentSchedule.getAmount());
				}
			}
		  }
	    }
	}

	public double getTotalRentAmount() {
		return totalRentAmount;
	}

	public void setTotalRentAmount(double totalRentAmount) {
		this.totalRentAmount = totalRentAmount;
	}

	public int getInstallments() {
		return installments;
	}

	public void setInstallments(int installments) {
		this.installments = installments;
	}

	public double getTotalDepositAmount() {
		return totalDepositAmount;
	}

	public void setTotalDepositAmount(double totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}

	public double getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(double totalFees) {
		this.totalFees = totalFees;
	}

	public double getTotalFines() {
		return totalFines;
	}

	public void setTotalFines(double totalFines) {
		this.totalFines = totalFines;
	}

	public List<String> getSelectedPayments() {
		return selectedPayments;
	}

	public void setSelectedPayments(List<String> selectedPayments) {
		this.selectedPayments = selectedPayments;
	}
	
	public TimeZone getTimeZone()

	{
	       return TimeZone.getDefault();
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	
	
	public String cancelPaymentTask()
	{
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		
		try 
		{
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.CANCEL);
			
			NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_CANCELED, requestId);
			
			saveComments(requestId);
			saveAttachments(requestId);
			
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PIMSWorkListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "cancelToReqSearch";
	}
	@SuppressWarnings("unchecked")
	public void completeTask() throws Exception
	{
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		
			
				if(!viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				bpmWorkListClient.completeTask(userTask, getLoggedInUser(), TaskOutcome.COMPLETE);
				notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_Renew_Request_Completed);
				refreshAllValues();
				getMsg(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
				NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
				
				saveComments(requestId);
				saveAttachments(requestId);
				viewMap.put(Page_Mode.PAGE_MODE, Page_Mode.COMPLETE);
				
	}
	@SuppressWarnings( "unchecked" )
	public void saveComments(Long reqIdForComments) throws Exception
    {
    	try{
	    	
    		String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, reqIdForComments);

    	}
    	catch (Exception exception) {
			logger.LogException("saveComments|crashed ", exception);
			throw exception;
		}
    }
	@SuppressWarnings( "unchecked" )
	public void tabRequestHistory_Click()
    {
    	String methodName="tabRequestHistory_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try	
    	{
    		RequestHistoryController rhc=new RequestHistoryController();
    	  if(requestId!=null)
    	  {
    		
    	    rhc.getAllRequestTasksForRequest(WebConstants.PROCEDURE_TYPE_RENEW,requestId.toString());
    	  }
    	  else
    		  rhc.getAllRequestTasksForRequest(WebConstants.PROCEDURE_TYPE_RENEW,"");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }
	@SuppressWarnings( "unchecked" )
	public String getOwnerShipTypeIdHid() {
		if(context.getViewRoot().getAttributes().containsKey(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID))
			ownerShipTypeIdHid = context.getViewRoot().getAttributes().get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString();
		return ownerShipTypeIdHid;
	}

	public void setOwnerShipTypeIdHid(String ownerShipTypeIdHid) {
		this.ownerShipTypeIdHid = ownerShipTypeIdHid;
	}
	@SuppressWarnings( "unchecked" )
	public Date getContractStartDate() {
		if(viewMap.containsKey("contractStartDate"))
			contractStartDate=(Date)viewMap.get("contractStartDate");

		return contractStartDate;
	}
	@SuppressWarnings( "unchecked" )
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
		if(this.contractStartDate !=null)
			viewMap.put("contractStartDate",contractStartDate);
	}
	@SuppressWarnings( "unchecked" )
	public Date getContractEndDate() {
		if(viewMap.containsKey("contractEndDate"))
			contractEndDate =(Date)viewMap.get("contractEndDate");
		return contractEndDate;
	}
	@SuppressWarnings( "unchecked" )
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
		if(this.contractEndDate!=null)
			viewMap.put("contractEndDate",contractEndDate);
	}
	
	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	public UnitView getUnitView() {
		if(viewMap.containsKey(UNIT_VIEW))
			unitView = (UnitView)viewMap.get(UNIT_VIEW);
		return unitView;
	}
	public void setUnitView(UnitView unitView) {
		this.unitView = unitView;
	}
	public String getUnitRentAmount() {
		if(viewMap.containsKey("unitRentAmount") && viewMap.get("unitRentAmount").toString().trim().length()>0)
			unitRentAmount =viewMap.get("unitRentAmount").toString();
		return unitRentAmount;
	}
	public void setUnitRentAmount(String unitRentAmount) {
		this.unitRentAmount = unitRentAmount;
		if(this.unitRentAmount !=null)
			viewMap.put("unitRentAmount",this.unitRentAmount);
		else
			viewMap.put("unitRentAmount","");
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public HtmlTab getTabUnit() {
		return tabUnit;
	}
	public void setTabUnit(HtmlTab tabUnit) {
		this.tabUnit = tabUnit;
	}
//	public HtmlCommandButton getBtnCollectPayments() {
//		return btnCollectPayments;
//	}
//	public void setBtnCollectPayments(HtmlCommandButton btnCollectPayments) {
//		this.btnCollectPayments = btnCollectPayments;
//	}
	public HtmlCommandButton getBtnGeneratePayments() {
		return btnGeneratePayments;
	}
	public void setBtnGeneratePayments(HtmlCommandButton btnGeneratePayments) {
		this.btnGeneratePayments = btnGeneratePayments;
	}
	public HtmlCommandButton getBtnAddPayments() {
		return btnAddPayments;
	}
	public void setBtnAddPayments(HtmlCommandButton btnAddPayments) {
		this.btnAddPayments = btnAddPayments;
	}
	public HtmlSelectBooleanCheckbox getChkPaySch() {
		return chkPaySch;
	}
	public void setChkPaySch(HtmlSelectBooleanCheckbox chkPaySch) {
		this.chkPaySch = chkPaySch;
	}
	
	public boolean ispageModeApproveReject()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVE_REJECT))
			return true;
		
		return false;
	}
	public boolean ispageModeActive()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ACTIVE))
			return true;
		
		return false;
	}
	public boolean ispageModeView()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
			return true;
		
		return false;
	}
	public boolean ispageModeComplete()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
			return true;
		
		return false;
	}
	public boolean ispageModeEdit()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
			return true;
		
		return false;
	}
	public boolean ispageModeAdd()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
			return true;
		
		return false;
	}
	public boolean ispageModeCollectPayments()
	{
		if(viewMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COLLECT_PAYMENT))
			return true;
		
		return false;
	}
	public org.richfaces.component.html.HtmlTab getViolationsTab() {
		return violationsTab;
	}
	public void setViolationsTab(org.richfaces.component.html.HtmlTab violationsTab) {
		this.violationsTab = violationsTab;
	}
	public Double getOldUnitRentValue() {

		if(!viewMap.containsKey("oldUnitRentValue"))
		{
			 ContractView contractView = (ContractView)viewMap.get(OLD_CONTRACT_VIEW);
			 Iterator<ContractUnitView> iter=contractView.getContractUnitView().iterator();
			 while(iter.hasNext())
			 {
				  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
				  oldUnitRentValue = contractUnitView.getRentValue();
				  this.setOldUnitRentValue(oldUnitRentValue);
				  break;
			 }		    	
			 
			 
		}
		oldUnitRentValue = new Double(viewMap.get("oldUnitRentValue").toString());
		return oldUnitRentValue;
	}
	public void setOldUnitRentValue(Double oldUnitRentValue) {
		
		this.oldUnitRentValue = oldUnitRentValue;
		viewMap.put("oldUnitRentValue",this.oldUnitRentValue);
	}
	public HtmlInputText getTxtUnitRentValue() {
		return txtUnitRentValue;
	}
	public void setTxtUnitRentValue(HtmlInputText txtUnitRentValue) 
	{
		this.txtUnitRentValue = txtUnitRentValue;
	}
	public boolean isUnitRentAmountEditable() 
	{
		if(viewMap.containsKey(HAS_EDIT_RENT_AMOUNT_PERMISSION))
			unitRentAmountEditable = (Boolean)viewMap.get(HAS_EDIT_RENT_AMOUNT_PERMISSION);
		return unitRentAmountEditable;
	}
	public boolean isStartDateEditPermission() 
	{
		
		if( viewMap.containsKey(HAS_EDIT_START_DATE_PERMISSION) )
			startDateEditPermission = (Boolean)viewMap.get(HAS_EDIT_START_DATE_PERMISSION);
		return startDateEditPermission;
	}
	
	public void setUnitRentAmountEditable(boolean unitRentAmountEditable) {
		this.unitRentAmountEditable = unitRentAmountEditable;
	}
	
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		RequestView reqView = null;
		ContractView contView = null;
		PersonView applicant = null;
		PersonView tenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		Long reqId = new Long(this.getRequestId());
		
		try
		{
		
		reqView = psAgent.getRequestById(reqId);	
		if(reqView != null)
		{
			contView = reqView.getContractView();
			if(contView != null)
			{
				tenant = contView.getTenantView();
				applicant = reqView.getApplicantView();
				
				ContractView oldContract = new ContractView();
				
				if(viewMap.containsKey(OLD_CONTRACT_VIEW) && viewMap.get(OLD_CONTRACT_VIEW)!= null)
					oldContract = (ContractView)viewMap.get(OLD_CONTRACT_VIEW);
				UnitView unit = this.getUnitView();
							
				eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
				eventAttributesValueMap.put("OLD_CONTRACT_NUMBER", oldContract.getContractNumber());
				eventAttributesValueMap.put("NEW_CONTRACT_NO", this.getContractNumber());
				eventAttributesValueMap.put("CONTRACT_START_DATE", getStringFromDate(this.getContractStartDate()));
				eventAttributesValueMap.put("CONTRACT_END_DATE", getStringFromDate(this.getContractEndDate()));				
				eventAttributesValueMap.put("PROPERTY_NAME", unit.getPropertyCommercialName());
				eventAttributesValueMap.put("UNIT_NO", unit.getUnitNumber());
				if(this.getRentValueTxt().getValue() != null)
				{
					eventAttributesValueMap.put("TOTAL_CONTRACT_VALUE", this.getRentValueTxt().getValue().toString());
				}
				else if(contractView != null && contractView.getRentAmount() != null )
				{
					eventAttributesValueMap.put("TOTAL_CONTRACT_VALUE", contractView.getRentAmount().toString() );
					
				}
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(tenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}

				
			}
		}
		}
		catch (Exception e)
		{
			logger.logInfo("requestStatusNotification crashed...");
			e.printStackTrace();
		
		}
	}
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	public String btnPrint_Click() 
	{	
		
		UtilityServiceAgent usa = new UtilityServiceAgent(); 
		try 
		{
		    PrintLeaseContractReportCriteria reportCriteria = 
			new PrintLeaseContractReportCriteria(ReportConstant.Report.PRINT_LEASE_CONTRACT, ReportConstant.Processor.PRINT_LEASE_CONTRACT);
			ContractView contractView=(ContractView)viewMap.get("contractView");
			if ( contractView.getStatus() != null )
			 reportCriteria.setStatusId( contractView.getStatus().toString() );
			reportCriteria.setContractId(contractView.getContractId().toString());
		    HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.ContractEvents.CONTRACT_PRINTED, requestId);
		   	usa.printContract(new Long(this.getContractId()),getLoggedInUser());
		} 
		catch(Exception ex)
		{
			errorMessages =  new ArrayList<String>(0);
			logger.LogException("btnPrint_Click|Error Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return null;
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}
	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	public String btnPrintDisclosure_Click() {	
		
		String methodName="btnPrint_Click";
		logger.logInfo(methodName+"|Started...");
		UtilityServiceAgent usa = new UtilityServiceAgent(); 
		try {
				ContractDisclosureReportCriteria reportCriteria ;
				ContractView contractView=(ContractView)viewMap.get("contractView");
				if(isCommercialContract(contractView))
					reportCriteria=new ContractDisclosureReportCriteria(ReportConstant.Report.COMMERCIAL_DISCLOSURE, ReportConstant.Processor.CONTRACT_DISCLOSURE_PROCESSOR);
				else
					reportCriteria=new ContractDisclosureReportCriteria(ReportConstant.Report.RESIDENTIAL_DISCLOSURE, ReportConstant.Processor.CONTRACT_DISCLOSURE_PROCESSOR);
				
				reportCriteria.setContractId(contractView.getContractId().toString());
		    	
				HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
				NotesController.saveSystemNotesForRequest(WebConstants.PROCEDURE_TYPE_RENEW,MessageConstants.ContractEvents.DISCLOSURE_PRINTED, requestId);
			   	usa.printContract(new Long(this.getContractId()),getLoggedInUser());
			   	logger.logInfo(methodName+"|Finish...");    
		} 
		catch(Exception ex)
		{
			errorMessages =  new ArrayList<String>(0);
			logger.LogException(methodName+"|Error Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return null;
	}
	@SuppressWarnings( "unchecked" )
	private Integer getMaxDaysAllowedAfterExpiryOfOldContract()
	{
		String methodName = "getMaxDaysAllowedAfterExpiryOfOldContract|";
		String value ="0";
		Integer retValue = 0;
		try
		{
		 if( !viewMap.containsKey( WebConstants.MAX_DAYS_ALLOWED_AFTER_EXPIRY_FOR_RENEWAL_START_DATE ) ||  
			 viewMap.get( WebConstants.MAX_DAYS_ALLOWED_AFTER_EXPIRY_FOR_RENEWAL_START_DATE )==null 	 
		   )
		 {
		  value = new UtilityServiceAgent().getValueFromSystemConfigration( WebConstants.MAX_DAYS_ALLOWED_AFTER_EXPIRY_FOR_RENEWAL_START_DATE );
		  if( value == null)
			  value = "0";
		  viewMap.put( WebConstants.MAX_DAYS_ALLOWED_AFTER_EXPIRY_FOR_RENEWAL_START_DATE ,value);
		 }
		 else
			value = viewMap.get( WebConstants.MAX_DAYS_ALLOWED_AFTER_EXPIRY_FOR_RENEWAL_START_DATE ).toString();
		 logger.logInfo( methodName +"Finish|maxDaysAllowed:%s",value );
		 retValue = Integer.parseInt( value );
		}
		catch( Exception e )
		{
			logger.LogException(methodName +"Error Occured", e );

		}
		return retValue;
	}
	public DomainDataView getDdvContractTypeCommercial() {
		return ddvContractTypeCommercial;
	}
	public void setDdvContractTypeCommercial(
			DomainDataView ddvContractTypeCommercial) {
		this.ddvContractTypeCommercial = ddvContractTypeCommercial;
	}
	public boolean isCommercialContract(ContractView contract)
	{
		ddvContractTypeCommercial=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE), WebConstants.COMMERCIAL_LEASE_CONTRACT);
		if(contract.getContractTypeId().compareTo(ddvContractTypeCommercial.getDomainDataId())==0)
			return true;
		else
			return false;
	}
	public HtmlCommandButton getBtnPrintDisclosure() {
		return btnPrintDisclosure;
	}
	public void setBtnPrintDisclosure(HtmlCommandButton btnPrintDisclosure) {
		this.btnPrintDisclosure = btnPrintDisclosure;
	}
	public HtmlCommandButton getBtnCancelRenewContractButton() {
		return btnCancelRenewContractButton;
	}
	public void setBtnCancelRenewContractButton(
			HtmlCommandButton btnCancelRenewContractButton) {
		this.btnCancelRenewContractButton = btnCancelRenewContractButton;
	}
	public double getTotalRealizedRentAmount() {
		return totalRealizedRentAmount;
	}
	public void setTotalRealizedRentAmount(double totalRealizedRentAmount) {
		this.totalRealizedRentAmount = totalRealizedRentAmount;
	}
	public HtmlCommandButton getBtnRegisterInEjari() {
		return btnRegisterInEjari;
	}
	public void setBtnRegisterInEjari(HtmlCommandButton btnRegisterInEjari) {
		this.btnRegisterInEjari = btnRegisterInEjari;
	}

	
}
