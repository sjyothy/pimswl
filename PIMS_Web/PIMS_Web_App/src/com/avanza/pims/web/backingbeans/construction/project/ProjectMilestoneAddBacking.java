package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.ui.util.ResourceUtil;


public class ProjectMilestoneAddBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ProjectMilestoneAddBacking.class);
	/**
	 * 
	 */
	
	public ProjectMilestoneAddBacking(){
		
	}
	
	private List<String> errorMessages = new ArrayList<String>();
	
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private final String PAGE_MODE = WebConstants.ProjectMilestone.AddPopup.PAGE_MODE;
	private final String PAGE_MODE_ADD = WebConstants.ProjectMilestone.AddPopup.PAGE_MODE_ADD;
	private final String PAGE_MODE_EDIT = WebConstants.ProjectMilestone.AddPopup.PAGE_MODE_EDIT;
	private String pageMode="";
	
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = WebConstants.ProjectMilestone.AddPopup.Headings.PAGE_MODE_ADD;
	
	private String heading="";
	
	ProjectMileStoneView projectMileStoneView = new ProjectMileStoneView();
	
	private ProjectServiceAgent projectServiceAgent = new ProjectServiceAgent();
	
	@SuppressWarnings("unchecked")
	public void saveProjectMilestone(ActionEvent event) {
		logger.logInfo("saveProjectMilestone() started...");
		Boolean success = false;
		try{
			if(validated())
			{
				updateMilestoneView();
				if(getIsEditMode()){
					if(getProjectId()!=null){
						success = projectServiceAgent.saveProjectMilestone(projectMileStoneView, CommonUtil.getArgMap());
						if(success){
							sessionMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW, projectMileStoneView);
							sessionMap.put("milestoneEditSuccess", true);
						}
					}
					else{
						sessionMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW, projectMileStoneView);
						sessionMap.put("milestoneEditSuccess", false);
					}
				}
				else{
					List<ProjectMileStoneView> milestoneList = getProjectMileStoneViewList();
					milestoneList.add(projectMileStoneView);
					sessionMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, milestoneList);
					sessionMap.put("milestoneAddSuccess", true);
				}
				
	   		    FacesContext facesContext = FacesContext.getCurrentInstance();
	   		
		        String javaScriptText = "javascript:closeWindowSubmit();";
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			
			logger.logInfo("saveProjectMilestone() completed successfully!!!");
		}
    	catch(PimsBusinessException exception){
    		logger.LogException("saveAuctionUnitPrices() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("saveProjectMilestone() crashed ",exception);
    	}
	}
	
	public void updateMilestoneView(){
		logger.logInfo("updateMilestoneView() started...");
		
		if(getIsEditMode())
			projectMileStoneView = (ProjectMileStoneView) viewMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
		
		projectMileStoneView.setProjectId(getProjectId());
		projectMileStoneView.setMileStoneNumber(getMilestoneNumber());
		projectMileStoneView.setMileStoneName(getMilestoneName());
		projectMileStoneView.setCompletionDate(getCompletionDate());
		projectMileStoneView.setCompletionPercentage(Long.parseLong(getCompletionPercent()));
		projectMileStoneView.setDescription(getDescription());
		
		logger.logInfo("updateMilestoneView() completed...");
	}
	
	public void populateMilestoneView(){
		logger.logInfo("populateMilestoneView() started...");
		
		projectMileStoneView = (ProjectMileStoneView) viewMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
		if(projectMileStoneView !=null)
		{
			setProjectId(projectMileStoneView.getProjectId());
			setMilestoneNumber(projectMileStoneView.getMileStoneNumber());
			setMilestoneName(projectMileStoneView.getMileStoneName());
			setCompletionDate(projectMileStoneView.getCompletionDate());
			setCompletionPercent(projectMileStoneView.getCompletionPercentage().toString());
			setDescription(projectMileStoneView.getDescription());
		}
		logger.logInfo("populateMilestoneView() completed...");
	}
	
	public Boolean validated(){
		logger.logInfo("validated() started...");
		Boolean validated = true;
		
    	try{
    		errorMessages.clear();
    		
    		if(StringHelper.isEmpty(getMilestoneName()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectMilestone.AddPopup.Fields.MILESTONE_NAME));
	        }
    		
    		Boolean percentValid = true;
    		
    		if(StringHelper.isEmpty(getCompletionPercent()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectMilestone.AddPopup.Fields.COMPLETION_PERCENT));
	        }
    		else
	        {
    			try{
    				Long percent = Long.parseLong(getCompletionPercent());
    				
	    				if(!(percent>0 && percent<=100)){
	    		    		validated = false;
	    		    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_PERCENT_OUT_OF_RANGE));
	    		    		percentValid = false;
	    				}
    				
    				/*else{
    					if(percent>getValidPercentage()){
        		    		validated = false;
        		        	errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.ProjectMilestone.MSG_PERCENT_OUT_OF_LIMIT, getValidPercentage().toString()));
        				}
    				}*/
    			}
    			catch(NumberFormatException nfe){
    				validated = false;
    				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_PERCENT_OUT_OF_RANGE));
    			}
	        }
    		if(getCompletionDate()==null)
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectMilestone.AddPopup.Fields.COMPLETION_DATE));
	        }
    		else{
    			if(percentValid){
	    			List<ProjectMileStoneView> projectMilestones = getProjectMileStoneViewList();
	    			Long percent = Long.parseLong(getCompletionPercent());
		    		for(ProjectMileStoneView projectMileStoneView: projectMilestones){
		    			Boolean milstonePercentValid = projectMileStoneView.getCompletionDate().before(getCompletionDate()) && projectMileStoneView.getCompletionPercentage()<percent;
		    			milstonePercentValid |= projectMileStoneView.getCompletionDate().after(getCompletionDate()) && projectMileStoneView.getCompletionPercentage()>percent; 
		    			if(!milstonePercentValid){
		    				validated = false;
		    				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_MILESTONE_DATE_PERCENT_INVALID));
		    				break;
		    			}
		    		}
    			}
    		}
    		if(StringHelper.isEmpty(getDescription()))
	        {
	    		validated = false;
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectMilestone.AddPopup.Fields.DESCRIPTION));
	        }	
    		logger.logInfo("validated() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("validated() crashed ", exception);
		}
		return validated;
	}
	
    public void done(ActionEvent event) {
    	logger.logInfo("done() started...");
    	try{
    		
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	
	        String javaScriptText = "window.closeWindowSubmit();";
	        
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("done() completed successfully!!!");
	    }
		catch (Exception exception) {
			logger.LogException("done() crashed ", exception);
		}
    }
    
    public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
    	return new CommonUtil().getNumberFormat();
    }
    
 
	@Override
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
			if(!isPostBack())
			{
				setMode();
				setValues();
			}
			pageMode = (String) viewMap.get(PAGE_MODE);
			
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
		
	}

	@SuppressWarnings("unchecked")
	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			///
//			sessionMap.put(PAGE_MODE,PAGE_MODE_CITY);
//			sessionMap.remove(POPUP_MODE);
//			sessionMap.remove(PAGE_MODE);
//			sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_ID, 693L);
//	   		sessionMap.put(WebConstants.RegionDetails.EVALUATION_REQUEST_MODE, WebConstants.RegionDetails.EVALUATION_REQUEST_MODE_EVALUATE);
//	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_EVALUATION_REQUEST_SEARCH);
			///
			
			// if popup mode i.e. add country, city or area
			if(sessionMap.get(PAGE_MODE)!=null)
				viewMap.put(PAGE_MODE, sessionMap.get(PAGE_MODE));
			
			sessionMap.remove(PAGE_MODE);
			
			// if edit mode
			if(sessionMap.containsKey(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW)){
				ProjectMileStoneView projectMileStoneView = (ProjectMileStoneView) sessionMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
				viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW, projectMileStoneView);
				sessionMap.remove(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW);
			}
			
			// if add mode
			if(sessionMap.containsKey(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST)){
				List<ProjectMileStoneView> milestoneList = (List<ProjectMileStoneView>) sessionMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
				viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, milestoneList);
				sessionMap.remove(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
			}
			if(sessionMap.containsKey("projectId")){
				viewMap.put("projectId", sessionMap.get("projectId"));
				sessionMap.remove("projectId");
			}
			//
			
			/*if(sessionMap.containsKey("validPercentage")){
				Long validPercentage = (Long) sessionMap.get("validPercentage");
				viewMap.put("validPercentage", validPercentage);
				sessionMap.remove("validPercentage");
			}*/
			
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setValues() {
		logger.logInfo("setValues() started...");
		try{
			pageMode = (String) viewMap.get(PAGE_MODE);
			if(pageMode==null)
				pageMode = PAGE_MODE_ADD; 
			
			viewMap.put("asterisk", "*");
			
			
			if(pageMode.equals(PAGE_MODE_ADD)){
				viewMap.put(HEADING, WebConstants.ProjectMilestone.AddPopup.Headings.PAGE_MODE_ADD);
			}	
			else 
				viewMap.put(HEADING, WebConstants.ProjectMilestone.AddPopup.Headings.PAGE_MODE_EDIT);
			
			populateMilestoneView();
			
			viewMap.put(PAGE_MODE, pageMode);	
			logger.logInfo("setValues() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("setValues() crashed ", exception);
		}
	}
	
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		applyMode();
	}

	private void applyMode(){
		
	}
	
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
 		this.errorMessages = errorMessages;
	}
	
	public boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}

	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public List<ProjectMileStoneView> getProjectMileStoneViewList() {
		List<ProjectMileStoneView> milestoneList = (List<ProjectMileStoneView>) viewMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
		if(milestoneList==null)
			milestoneList = new ArrayList<ProjectMileStoneView>();
		return milestoneList;
	}
	
	public Boolean getIsAddMode() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_ADD))
			return true;
		return false;
	}
	
	public Boolean getIsEditMode() {
		if (pageMode.equalsIgnoreCase(PAGE_MODE_EDIT))
			return true;
		return false;
	}
	

	public String getMilestoneNumber() {
		String temp = (String)viewMap.get("milestoneNumber");
		return temp==null?"":temp;
	}

	public void setMilestoneNumber(String milestoneNumber) {
		if(milestoneNumber!=null)
			viewMap.put("milestoneNumber" , milestoneNumber);
	}
	
	public Long getProjectId() {
		Long temp = (Long)viewMap.get("projectId");
		return temp;
	}

	public void setProjectId(Long projectId) {
		if(projectId!=null)
			viewMap.put("projectId" , projectId);
	}
	
	public String getMilestoneName() {
		String temp = (String)viewMap.get("milestoneName");
		return temp==null?"":temp;
	}

	public void setMilestoneName(String milestoneName) {
		if(milestoneName!=null)
			viewMap.put("milestoneName" , milestoneName);
	}
	
	public String getCompletionPercent() {
		String temp = (String)viewMap.get("completionPercent");
		return temp==null?"":temp;
	}

	public void setCompletionPercent(String completionPercent) {
		if(completionPercent!=null)
			viewMap.put("completionPercent" , completionPercent);
	}
	
	public Date getCompletionDate() {
		Date temp = (Date)viewMap.get("completionDate");
		return temp;
	}

	public void setCompletionDate(Date completionDate) {
		if(completionDate!=null)
			viewMap.put("completionDate" , completionDate);
	}
	
	public String getDescription() {
		String temp = (String)viewMap.get("description");
		return temp==null?"":temp;
	}

	public void setDescription(String description) {
		if(description!=null)
			viewMap.put("description" , description);
	}
	
	public Long getValidPercentage() {
		Long temp = (Long)viewMap.get("validPercentage");
		return temp==null?100L:temp;
	}

	public String getLocale(){
		return new CommonUtil().getLocale();
	}
}