package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.MemsNolTypes;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.web.workflow.WFTaskAction;
import com.avanza.pims.web.workflow.WorkFlowVO;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.pims.ws.vo.mems.MemsNOLTypeUserGrpView;
import com.avanza.pims.ws.vo.mems.MemsNOLTypeView;
import com.avanza.ui.util.ResourceUtil;


/** The class acts as a backing bean of IssueMemsNOL.jsp.
 * @author Farhan Muhammad Amin
 */

@SuppressWarnings("unchecked")
public class IssueMemsNOLController extends AbstractMemsBean {

	private static final long serialVersionUID 	= 1L;
	private static final Long INVALID_STATUS 	= -1L;
	
	private static final String ID_APP_TAB			= "applicationTab";
	private static final String ID_NOL_TAB			= "NOLTab";	
	private static final String PROCEDURE_TYPE 		= "procedureType";
	private static final String EXTERNAL_ID 		= "externalId";
	private static final String SELECTED_TAB		= "SelectedTab";
	private static final String NOL_REASON			= "NolReason";
	private static final String NOL_SELECTED_TYPE 	= "SelectedNOLType";
	private static final String NOL_GROUP_MAP		= "NolGroupMap";
	private static final String ALL_USER_GROUPS		= "AllUserGroups";
	private static final String USER_IN_GROUP		= "UserInGroup";
	private static final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnRequestId;
	
	protected HtmlTabPanel richPanel = new HtmlTabPanel();
	protected HtmlSelectOneMenu cmbNolTypes = new HtmlSelectOneMenu();
	protected HtmlSelectOneMenu cmbUserGroups = new HtmlSelectOneMenu();
	protected HtmlInputTextarea reasonNOL = new HtmlInputTextarea();
	protected HtmlInputText nolTypeGroups = new HtmlInputText();
	protected HtmlInputText reviewTxt = new HtmlInputText();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlFileType = new HtmlInputText();
	private HtmlInputText htmlFileStatus  = new HtmlInputText();
	private HtmlInputText htmlFileDate = new HtmlInputText();
	
		


	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlFileType() {
		return htmlFileType;
	}

	public void setHtmlFileType(HtmlInputText htmlFileType) {
		this.htmlFileType = htmlFileType;
	}

	public HtmlInputText getHtmlFileStatus() {
		return htmlFileStatus;
	}

	public void setHtmlFileStatus(HtmlInputText htmlFileStatus) {
		this.htmlFileStatus = htmlFileStatus;
	}

	public HtmlInputText getHtmlFileDate() {
		return htmlFileDate;
	}

	public void setHtmlFileDate(HtmlInputText htmlFileDate) {
		this.htmlFileDate = htmlFileDate;
	}

	public HtmlInputText getReviewTxt() {
		return reviewTxt;
	}

	public void setReviewTxt(HtmlInputText reviewTxt) {
		this.reviewTxt = reviewTxt;
	}

	/** 
	 * 	The function is called called by the framework when the user hits the URL either directly
	 * 	or through page navigation rules. The page can be accessed through three possible scenarios.
	 * 
	 * 	</br> 1. The page is accessed from requestSearch.jsp page in that case the request has 
	 * 	RequestView object with the key WebConstants.REQUEST_VIEW.
	 * 
	 * 	</br> 2. The page is accessed from InheritedFileSearch.jsp in that case the request has 
	 * 	InheritedFileView object with the key.
	 * 
	 * 	</br> 3. The page is accessed from the user task page.
	 * 
	 * 	<p>Depending on the attributes in the request map appropriate initialization function is called
	 * 
	 * 	</br>1. {@link IssueMemsNOLController#initFromInheritedFileSearch()} 
	 * 	</br>2. {@link IssueMemsNOLController#initFromRequestSearch()} 
	 * 	</br>3. {@link IssueMemsNOLController#initFromUserTasks()}
	 * 
	 * 	<p> To make things uniform the every init function creates/or gets {@link RequestView} and puts it
	 * in the view root </p>
	 * 
	 * @author Farhan Muhammad Amin
	 */
	@Override
	public void init() {
		logger.logInfo("[init() .. Started]");
		super.init();					
		try {							
    		viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_TYPE_MEMS_NOL);
	    	viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_MEMS_NOL);
	    	
	    	if( !isPostBack() ) {		
	    		
	    		if( !viewMap.containsKey(WebConstants.MemsNOLTypes.NOL_LIST )) {
					viewMap.put(WebConstants.MemsNOLTypes.NOL_LIST, doGetMemsNolTypes());
				}
	    		
				RequestView reqView = (RequestView ) getRequestMap().get(WebConstants.REQUEST_VIEW);
				String inheritanceFileId = (String) getRequestMap().get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID); 
								
				
				if( null!=reqView ) {
					initFromRequestSearch(reqView);
				} 
				
				if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null ){
					
					if( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) instanceof UserTask) {
						
						initFromSoaUserTasks( );
					}
					else if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) instanceof TaskListVO) {
						initFromWorkFlowUserTasks();
					}
				} else if ( null!=inheritanceFileId ) {
					initFromInheritedFileSearch(inheritanceFileId);
				} else {
					throw new PimsBusinessException(999, "Page cannot be accessed directly");
				}												 				
				loadUserGroupsInViewRoot();				
				setUserInGroup();
			}	
		}
		catch(Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));						
		}
		logger.logInfo("[init() .. Ends]");
	}
	
	/** 
	 * 	The function is called from the {@link IssueMemsNOLController#init()} when the user is forwarded
	 * 	from InheritedFileSearch screen. The function creates the {@link RequestView} and sets the status to 
	 * 	INVALID_STATUS which indicates the request is <strong>NEW and not serialized to database</strong> as 
	 * 	yet. The function performs other initialization such as loading attachment and comments. 
	 * 	
	 * 	@author Farhan Muhammad Amin
	 * 	
	 * 	@see IssueMemsNOLController#handleAttachmentCommentTabStatus(RequestView)
	 * 	@see IssueMemsNOLController#loadAttachmentsAndComments(Long)
	 * 	@see IssueMemsNOLController#setRequestView(RequestView)
	 * 
	 */
	
	public void initFromInheritedFileSearch(final String inheritanceFileId) throws Exception
	{
		RequestView reqView = new RequestView();
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setRecordStatus(1L);
		reqView.setIsDeleted(0L);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.NOL_REQUEST_ID);
		reqView.setStatusId(INVALID_STATUS);		
		
		Long fileId = Long.parseLong(inheritanceFileId);
		InheritanceFileView fileView = new InheritanceFileView();
		fileView.setInheritanceFileId(fileId);
		
		reqView.setInheritanceFileId( fileId);
		reqView.setInheritanceFileView(fileView);		
		setRequestView(reqView);	
		loadInhFileParamTextBoxes(fileId);
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
	}
	
	private void loadInhFileParamTextBoxes(Long fileId)throws Exception 
	{
		
		try 
		{
			if (fileId != null) 
			{
				InheritanceFileView inhFile = new UtilityService().getInheritanceFileById(fileId);
				htmlFileNumber.setValue(inhFile.getFileNumber());
				DomainDataView domainData = new UtilityService().getDomainDataByDomainDataId(inhFile.getStatusId());
				DomainDataView domainDataFileType = new UtilityService().getDomainDataByDomainDataId( new Long (inhFile.getFileTypeId()));
//				PropertyService service = new PropertyService();
//				PersonView personView=service.getPersonById(new Long(inhFile.getFilePersonId()));
				
//				@SuppressWarnings("unused")
//				String completeName = personView.getPersonFullName();
				txtFileOwner.setValue( inhFile.getFilePerson() );
				if (CommonUtil.getIsEnglishLocale()) {
					htmlFileStatus.setValue(domainData.getDataDescEn());
					htmlFileType.setValue(domainDataFileType.getDataDescEn());
					htmlFileDate.setValue(inhFile.getCreatedOn());
					
				} else {
					htmlFileStatus.setValue(domainData.getDataDescAr());
					htmlFileType.setValue(domainDataFileType.getDataDescAr());
					htmlFileDate.setValue(inhFile.getCreatedOn());
				}

			}
		} catch (Exception ex) {
			logger.LogException("loadInhFileParamTextBoxes|Error Occured:", ex);
			throw ex;
		}
		
	}

	/** 
	 * 	The function is called when the user is forwarded by requestSearch.jsp screen. The calling function
	 * 	retrieves the {@link RequestView} from the request attribute map and pass it on The main flow of the 
	 * 	function is
	 * 
	 * 	<p> 1. The function updates the view root with the request view object </p>
	 * 	<p> 2. The function refreshes the UI. The {@link IssueMemsNOLController#refreshTabs(RequestView)} is responsible
	 * 		to synchronize the user interface with the model.
	 * 	<p> 3. The function loads the attachments and comments associated with the current request </p>
	 * 
	 * 	@param reqView The request view virtual object representing the attributes of the underlying request
	 * 	@author Farhan Muhammad Amin
	 * 	
	 * 	@see IssueMemsNOLController#refreshTabs(RequestView)
	 * 	@see IssueMemsNOLController#setRequestView(RequestView)
	 * 	@see IssueMemsNOLController#loadAttachmentsAndComments(Long)
	 */
	
	public void initFromRequestSearch(RequestView reqView)throws Exception {
		setRequestView(reqView);
		refreshTabs(reqView);
		loadAttachmentsAndComments(reqView.getRequestId());		
		loadInhFileParamTextBoxes(reqView.getInheritanceFileId()!=null?reqView.getInheritanceFileId():null);
	}
	
	@SuppressWarnings( "unchecked" )
	protected void initFromWorkFlowUserTasks()throws Exception
    {
		TaskListVO task = ( TaskListVO )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, task);
		if( task.getRequestId() == null ){ return; }
	  	long  id = new Long  ( task.getRequestId() );
	  	RequestService reqSrv = new RequestService();
		RequestView requestView= reqSrv.getRequestById(id);					
		initFromRequestSearch(requestView);
		loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
    }
	@SuppressWarnings("unchecked")
	public void initFromSoaUserTasks() throws PimsBusinessException,Exception
	{
		UserTask userTask = (UserTask)sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		viewMap.put(TASK_LIST_USER_TASK, userTask);
		Map taskAttributes =  userTask.getTaskAttributes();				
		
		if( null!=taskAttributes.get(WebConstants.UserTasks.REQUEST_ID) )
		{			
			Long reqId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			RequestService reqSrv = new RequestService();
			RequestView requestView= reqSrv.getRequestById(reqId);					
			initFromRequestSearch(requestView);
			loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
		}
	}
	
	
	
	/** 
	 * 	The function retrieves the all possible Mems NOL types from the view root. The view root
	 * 	is populated with the list in the {@link IssueMemsNOLController#init()}
	 * 
	 * 	@return the list of {@link SelectItem} from the view root. If the function cannot find the 
	 * 			list in the view root it returns an empty list.
	 * 
	 * 	@author Farhan Muhammad Amin
	 * 	@see IssueMemsNOLController#init()
	 */
	public List<SelectItem> getMemsNolTypes() {		
		if(viewMap.containsKey((WebConstants.MemsNOLTypes.NOL_LIST))) {
			return (List<SelectItem>) viewMap.get(WebConstants.MemsNOLTypes.NOL_LIST);
		}
		return new ArrayList<SelectItem>();
	}
	
	
	/** 
	 * 	The function is responsible to load the attachments and comments for the request and to
	 * 	communicate with the Attachment and Comments tabs Controller by putting the appropriate objects in the 
	 * 	view root with the KEYS that the corresponding TAB expects.
	 * 
	 * 	@param requestId The id of the request for which to load the attachment and comments
	 * 	@author Farhan Muhammad Amin
	 * 
	 */
	
	public void loadAttachmentsAndComments(Long requestId)throws Exception{		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = viewMap.get(EXTERNAL_ID).toString();
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if( null!=requestId ) {
	    	String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	/** 
	 * 	The function is called first time when the component tree is constituted. The flow of the function 
	 * 	is
	 * 	
	 * 	<p>1. The function calls UtilityServiceAgent API to get the list of the MemsNOLTypeView </p>
	 * 	<p>2. The function constructs a list of SelectItems as per the current user locale </p>
	 * 	<p>3. The function returns the list of SelectItem </p>
	 * 	
	 * 	@return The list of SelectItem 
	 * 	@author Farhan Muhammad Amin
	 */
	
	private List<SelectItem> doGetMemsNolTypes() {				
		logger.logInfo("[getMemsNolTypes Starts]");
		List<SelectItem> nolTypeItems = new ArrayList<SelectItem>();
		
		try {					
			UtilityServiceAgent usa = new UtilityServiceAgent();
			List <MemsNOLTypeView> nolTypeViewList = usa.getMemsNOLTypes(1l);
							
			for(MemsNOLTypeView singleView : nolTypeViewList) {				
				if(CommonUtil.getIsEnglishLocale()) {
					nolTypeItems.add(new SelectItem(singleView.getMemsNolTypeId().toString(), singleView.getNolTypeNameEn()));									
				} else {
					nolTypeItems.add(new SelectItem(singleView.getMemsNolTypeId().toString(), singleView.getNolTypeNameAr()));
				}
			}			
			
			Map<String, Set<MemsNOLTypeUserGrpView>> nolGrpMap = new HashMap<String, Set<MemsNOLTypeUserGrpView>>();
			
			for(MemsNOLTypeView singleView : nolTypeViewList) {
				nolGrpMap.put(singleView.getMemsNolTypeId().toString(), singleView.getUserGroups());							
			}			
			viewMap.put(NOL_GROUP_MAP, nolGrpMap);
			
		} catch(Exception ex) {
			logger.LogException("[Exception occurted in getMemsNolTypes", ex);
		}
		
		Collections.sort(nolTypeItems, ListComparator.LIST_COMPARE);
		logger.logInfo("[getMemsNolTypes Ends]");
		return nolTypeItems;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void prerender() {
		super.prerender();
		
		readParams();

		String selectTab = (String) viewMap.get(SELECTED_TAB);
				
		if( StringHelper.isNotEmpty( selectTab)) {
			richPanel.setSelectedTab(selectTab);
			viewMap.remove(SELECTED_TAB);
		}				
	}
	
	/** 
	 * 	The function is called in the prerender phase. The responsibility of the function is to read the 
	 * 	request query parameters and communicate it to ApplicationTab so that the TAB can update it self.
	 * 	The communication is done by putting the appropriate values with the appropriate KEYS in the view 
	 * 	root.
	 * 	
	 * @author Farhan Muhammad Amin
	 */
	
	private void readParams() {
		logger.logInfo("[readParams .. Starts]");
		if( hdnPersonId!=null && !hdnPersonId.equals("")) {
			
			RequestView reqView = getRequestView();
			PersonView applicant = reqView.getApplicantView();
			
			if( null!=applicant) {
				applicant.setPersonId(Long.parseLong(hdnPersonId));
			} else {
				applicant = new PersonView();
				applicant.setPersonId(Long.parseLong(hdnPersonId));
				reqView.setApplicantView(applicant); 
				
			}					
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}
		
		if( hdnPersonName!=null && !hdnPersonName.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
		}
		
		if( null!=hdnCellNo && !hdnCellNo.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, hdnCellNo);
		}
		
		if( null!=hdnPersonType && !hdnPersonType.equals("")) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, hdnPersonType);
		}
		
		logger.logInfo("[readParams() .. Ends]");
	}
	
	private boolean validateSendForReview() {
		logger.logInfo("[validateSendForReview .. Started]");		
		if( null==cmbUserGroups.getValue() || cmbUserGroups.getValue().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.ERR_GRP_REQUIRED));
			return false;
		}
		logger.logInfo("[validateSendForReview .. Ends]");
		return true;
	}
	
	/** 
	 * 	The function is called when the status of the request is REVIEW REQUIRED and the user clicks 
	 * 	the done button. The responsibility of the function is to verify the user input.
	 * 	
	 * 	@return <strong>true</strong> if the user has entered some text else the function populates the
	 * 			error messages array with appropriate message and return <strong>false</strong>
	 * 	
	 * 	@author Farhan Muhammad amin
	 */
	
	private boolean validateReview() {
		logger.logInfo("[validateReview() .. Starts]");		
		if( null==reviewTxt.getValue() || 0==reviewTxt.getValue().toString().trim().length()) {
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Notes.MSG_NO_COMMENTS));			
			return false;
		}				
		logger.logInfo("[validateReview() .. Ends]");
		return true;
	}
	
	
	/**  
	 * 	The function validates the input provided by user. The function validates the following 
	 *  input components and add approriate error messages where ever applicable
	 *   
	 *   <p> 1. The function checks if the user has selected a valid applicant</p>
	 *   
	 *   <p> 2. The function checks if the user has selected a valid NO OBJECTION LETTER from the drop 
	 *   	 down list.</p>
	 *   
	 *   <p> 3. The function then validates that the user must provide the reason for NO OBJETION LETTER</p>
	 *   
	 *   <p> The function is called from the {@link IssueMemsNOLController#save()}
	 * 	
	 * 	@return <b>true</b> if input passes all the validations stated above.<b>false</b>, otherwise. The 
	 * 			function returns immediately upon encountering an invalid input from the user. 
	 * 	
	 * 	@author Farhan Muhammad Amin
	 * 	@see IssueMemsNOLController#save()
	 */
	
	public boolean validateInput() {
		logger.logInfo("[validateInput() .. Starts]");
		
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			viewMap.put(SELECTED_TAB, ID_APP_TAB);
			return false;
		}
		
		if( null==cmbNolTypes.getValue() || cmbNolTypes.getValue().toString().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.NOL_NolType));
			viewMap.remove(SELECTED_TAB);
			viewMap.put(SELECTED_TAB, ID_NOL_TAB);
			return false;
		}
		
		if( null==reasonNOL.getValue() || 0==reasonNOL.getValue().toString().trim().length()) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.NOL_NolReason));
			viewMap.remove(SELECTED_TAB);
			viewMap.put(SELECTED_TAB, ID_NOL_TAB);
			return false;
		}
		
		if( null==nolTypeGroups || 0==nolTypeGroups.getValue().toString().trim().length() ) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNOL.ERR_NO_GRP_ASSOCIATED));
			viewMap.remove(SELECTED_TAB);
			viewMap.put(SELECTED_TAB, ID_NOL_TAB);
			return false;
		}
				 
											
		logger.logInfo("[validateInput() .. Ends]");
		return true;
	}
	
	
	/**
	 * 	The function is registered as an action on the "Save" button. The save button is only rendered
	 * 	when the status of the {@link RequestView} is either INVALID_STATUS or WebConstants.REQUEST_STATUS_NEW. 
	 * 	<p> 1. The function performs validation on the user input by calling {@link IssueMemsNOLController#save()}
	 * 			</p>
	 * 	<p> 2. If the function in steps <strong>1</strong> returns true {@link IssueMemsNOLController#doSave()} is 
	 * 		called.
	 * 
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void save() {
		logger.logInfo("[save() .. Starts]");
		if(validateInput()) {
			doSave();			
		}
		logger.logInfo("[save() .. Ends]");
	}
	
	/**
	 * 	The function is called from the {@link IssueMemsNOLController#save()} when the user input is validated
	 * 	successfully. The flow of the function can be described as
	 * 	<p> 1. The function gets the RequestView from the view root <p>
	 * 	<p> 2. The function queries the status of the request. </p>
	 * 	<p> 3. If the status is INVALID_STATUS the function adds a new request into the database. <p>
	 * 	<p> 4. Else the function updates the request.
	 * 
	 * 	@author Farhan Muhammad Amin
	 * 
	 * 	@see IssueMemsNOLController#doSave()
	 */
	
	private Boolean doSave() {
		RequestView reqView = getRequestView();		
		try {
			if( reqView.getStatusId().equals(INVALID_STATUS) ) {
				add(reqView);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_SAVE_SUCCESS));
			} else {
				update(reqView);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_UPDATE_SUCCESS));
			}
			return true;
		} catch( Exception ex ) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in addRequest()]", ex);
	        ex.printStackTrace();										
		}
		return false;
	}
	
	private void update(RequestView reqView) throws Exception {
		
		if( null==reqView ) {
			throw new IllegalArgumentException();
		}		
		
		reqView.setUpdatedOn(new Date());
		
		String desc = (String)viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
		reqView.setDescription(desc);
		
		reqView.setMemsNolReason(reasonNOL.getValue().toString());
		reqView.setMemsNolType(Long.parseLong(cmbNolTypes.getValue().toString()));
							
		RequestServiceAgent reqAgent = new RequestServiceAgent();
		Long reqId = reqAgent.addRequest(reqView);
		RequestView updatedRequestView = reqAgent.getRequestById(reqId);
		setRequestView(updatedRequestView);
		
		viewMap.put(SELECTED_TAB, ID_APP_TAB);		
		
		saveAttachmentsAndComments(updatedRequestView.getRequestId());	
	}
	
	private void add(RequestView reqView) throws Exception {
		
		if( null==reqView ) {
			throw new IllegalArgumentException();
		}
		
		reqView.setCreatedOn(new Date());
		reqView.setUpdatedOn(new Date());
		reqView.setRequestDate(new Date());					
		
		String desc = (String)viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
		reqView.setDescription(desc);
		
		reqView.setMemsNolReason(reasonNOL.getValue().toString());
		reqView.setMemsNolType(Long.parseLong(cmbNolTypes.getValue().toString()));
				
		List<DomainDataView> reqStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView dataView = CommonUtil.getIdFromType(reqStatusList, WebConstants.REQUEST_STATUS_NEW);
				
		reqView.setStatusId(dataView.getDomainDataId());
				
		RequestServiceAgent reqAgent = new RequestServiceAgent();
		Long reqId = reqAgent.addRequest(reqView);
		RequestView updatedRequestView = reqAgent.getRequestById(reqId);
		
		setRequestView(updatedRequestView);
		
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, updatedRequestView.getRequestNumber());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, updatedRequestView.getRequestDate());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, updatedRequestView.getStatusEn());
		viewMap.put(SELECTED_TAB, ID_APP_TAB);		
		
		saveAttachmentsAndComments(updatedRequestView.getRequestId());	
		CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_CREATED, updatedRequestView.getRequestId());
	}
	
	public void showInheritanceFilePopup() {
		logger.logInfo("[showInheritanceFilePopup() .. Starts]");
		try	 {			
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, getRequestView().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			String javaScriptText ="var screen_width = 900;"+
            	"var screen_height =600;"+
            	"window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    sendToParent( javaScriptText);		    		    
		}
		catch (Exception ex)  {
			logger.LogException("[Exception occured in showInheritanceFilePopup()]", ex);
			ex.printStackTrace();
		}
		logger.logInfo("[showInheritanceFilePopup() .. Ends]");
	}
	
	private String sendToParent(String javaScript) {
		final String viewId = "/SearchAssets.jsp";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	
	public void getRequestHistory() {		
		logger.logInfo("[getRequestHistory() .. Starts]");    			
		try	{
    		Long reqId = getRequestView().getRequestId();
    		if ( null!=reqId && !reqId.equals(INVALID_STATUS) ) {
    			RequestHistoryController rhc=new RequestHistoryController();
    			rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, reqId.toString());
    		}
    	} catch(Exception ex) {
    		logger.logInfo("[Exception occured in getRequestHistory()]", ex);
    		ex.printStackTrace();
    	}    	
		logger.logInfo("[getRequestHistory() .. Ends]");	
	}
	
	
	/**
	 * 	The function returns the database ID of the request status given the request status string.
	 * 
	 * @param status The request status 
	 * @return The database id of the request status
	 * @author Farhan Muhammad Amin
	 */
	
	private Long getIdFromStatus(String status) {
		List<DomainDataView> reqStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView dataView = CommonUtil.getIdFromType(reqStatusList, status);
		return dataView.getDomainDataId();
	}
	
	private void  getNotificationPlaceHolder(Event placeHolderMap,RequestView reqView) throws Exception
	{
		placeHolderMap.setValue("APPLICANT_NAME",viewMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME).toString());
		placeHolderMap.setValue("REQUEST_NUMBER",reqView.getRequestNumber());
		placeHolderMap.setValue("DATE",new Date());
		MemsNolTypes nolType = EntityManager.getBroker().findById(MemsNolTypes.class, reqView.getMemsNolType());
		placeHolderMap.setValue("NOL_TYPE", nolType.getNolTypeNameAr()+" / " +nolType.getNolTypeNameEn() );
		InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, reqView.getInheritanceFileId());
		placeHolderMap.setValue("FILE_NUM", file.getFileNumber());
		
	}
	private List<ContactInfo> getEmailContactInfos(PersonView personView) throws Exception
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 && !previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUserId(), personView.getCellNumber(), null, ciView.getEmail()));
	        }
		}
		if( ( emailList== null || emailList.size() <= 0) && personView.getCellNumber()!= null)
		{
			emailList.add(new ContactInfo(getLoggedInUserId(), personView.getCellNumber(), null, null));
		}
		return emailList;
	}

	public void generateNotification(String eventType,RequestView reqView) throws Exception
	{
        List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
        personsEmailList = getEmailContactInfos(reqView.getApplicantView());
        if(personsEmailList.size()>0)
        {
	        NotificationFactory nsfactory = NotificationFactory.getInstance();
	        NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
	        Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
			getNotificationPlaceHolder(event,reqView);
	        notifier.fireEvent(event, personsEmailList);
        }
		          
	}
	
	private void refreshTabs(RequestView reqView)throws Exception {
		
		if( null==reqView && null==reqView.getApplicantView()) {
			throw new IllegalArgumentException();		
		}
		
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, reqView.getRequestDate());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, reqView.getRequestNumber());
		
		PersonView applicant = reqView.getApplicantView();
		viewMap.put(WebConstants.LOCAL_PERSON_ID, applicant.getPersonId());
		
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,applicant.getPersonId());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME, applicant.getPersonFullName());
		if( null != applicant.getCellNumber() ){
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, applicant.getCellNumber());
		}
		viewMap.put(NOL_REASON, reqView.getMemsNolReason());
		viewMap.put(NOL_SELECTED_TYPE, reqView.getMemsNolType().toString());
		
		String desc = reqView.getDescription();
		
		if( null!=desc && 0!=desc.trim().length()) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, reqView.getDescription());
		}
		
		if( CommonUtil.getIsEnglishLocale()) {			
			//viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, applicant.getPersonTypeEn());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusEn());
		} 
		else if( CommonUtil.getIsArabicLocale() ) {
			//viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, applicant.getPersonTypeAr());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusAr());
		}	
		
		if( !getIsNew() ) 
		{ 
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");			
		}
		if( getIsReviewRequired() && reqView.getMemsNolReviewGrpId()   != null )
		{
			cmbUserGroups.setValue( reqView.getMemsNolReviewGrpId()  );
		}
		handleAttachmentCommentTabStatus(reqView);		
		reasonNOL.setValue(reqView.getMemsNolReason());
		cmbNolTypes.setValue(reqView.getMemsNolType().toString());
		nolTypeGroups.setValue(getUserGroupString(reqView.getMemsNolType().toString()));
	}
	
	/**
	 * 	The function is registered as an action to the "reject" button. The button is rendered
	 * 	when the request is in <strong>APPROVAL_REQUIRED</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>2. Marks an entry in for the activity logs for the action <p>
	 * 	<p>3. The function then updates the status in the database to <strong>REJECTED</strong>and updates the 
	 * 		  view root and refreshes the UI which synchronize the user interface with the model. </p>
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void reject() {
		logger.logInfo("[reject() .. Starts]");
		
		try {
			saveAttachmentsAndComments(getRequestView().getRequestId());
			RequestView reqView = updateRequestStatus(getRequestView(), WebConstants.REQUEST_STATUS_REJECTED);
			CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_REJECTED, getRequestView().getRequestId());
			setRequestView(reqView);
			refreshTabs(reqView);
			viewMap.put(SELECTED_TAB, ID_APP_TAB);
			
			completeTask(TaskOutcome.REJECT,
                                     WFTaskAction.REJECT.toString(),
                                     null );
			generateNotification("Event.MinorNOL.Reject", reqView);
			setIsView( true );
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_REJECT_SUCCESS));
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reject()]", ex);
		}			
		logger.logInfo("[reject() .. Ends]");
	}
	
	/**
	 * 	The function is registered as an action to the "Send For Review" button. The button is rendered
	 * 	when the request is in <strong>NEW</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>2. Marks an entry in for the activity logs for the action <p>
	 * 	<p>3. The function then updates the status in the database to <strong>REVIEW REQUIRED/EVALUATION REQUIRED</strong>, 
	 * 		  updates the view root and refreshes the UI which synchronize the user interface with the 
	 * 		  model. </p>
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void sendForReview() 
	{
		try 
		{
			if( validateSendForReview() ) 
			{
				RequestView reqView = getRequestView();
				reqView.setStatusId(getIdFromStatus(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED));
				reqView.setMemsNolReviewGrpId(cmbUserGroups.getValue().toString());
				update(reqView);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.MemsNOL.MSG_REQ_SENT_FOR_REVIEW, getRequestView().getRequestId());
				refreshTabs(getRequestView());
				setUserInGroup();
				completeTask(TaskOutcome.OK,							 		
                                             WFTaskAction.REVIEW.toString(),
                                             cmbUserGroups.getValue().toString()
                                             );
				viewMap.put(SELECTED_TAB, ID_APP_TAB);		
				setIsView(true);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_SENT_FOR_REVIEW_SUCCESS));
			}
		} 
		catch(Exception ex) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in sendForReview()]", ex);
		}			
	}
	
	/**
	 * 	The function is registered as an action to the "approve" button. The button is rendered
	 * 	when the request is in <strong>APPROVAL_REQUIRED</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>2. Marks an entry in for the activity logs for the action <p>
	 * 	<p>3. The function then updates the status in the database to <strong>APPROVED</strong> 
	 * 		  and updates the view root and refreshes the UI which synchronize the user interface with the 
	 * 		  model. </p>
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void approve() {
		logger.logInfo("[approve() .. Starts]");		
		try {
			saveAttachmentsAndComments(getRequestView().getRequestId());
			RequestView reqView = updateRequestStatus(getRequestView(), WebConstants.REQUEST_STATUS_APPROVED);
			CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_APPROVED, getRequestView().getRequestId());
			setRequestView(reqView);
			refreshTabs(reqView);
			viewMap.put(SELECTED_TAB, ID_APP_TAB);
			completeTask(TaskOutcome.APPROVE,WFTaskAction.APPROVED.toString(),null);
			generateNotification("Event.MinorNOL.Approve", reqView);
			setIsView(true);
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_APPROVE_SUCCESS));
			
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in approve()]", ex);
		}			
		logger.logInfo("[approve() .. Ends]");
	}
	
	/**
	 * 	The function is registered as an action to the "SubmitQuery" button. The button is rendered
	 * 	when the request is in <strong>NEW</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>2. Marks an entry in for the activity logs for the action <p>
	 * 	<p>3. The function then updates the status in the database to <strong>APPROVAL REQUIRED</strong> 
	 * 	and updates the view root and refreshes the UI which synchronize the user interface with the model. </p>
	 * 
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void submitQuery() {
		try {
			if( validateInput() && doSave() ) {							
				saveAttachmentsAndComments(getRequestView().getRequestId());
				RequestView reqView = updateRequestStatus(getRequestView(), WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, getRequestView().getRequestId());
				setRequestView(reqView);
//				invokeBpel(reqView.getRequestId());
                invokeWorkFlow(reqView.getRequestId());
				setIsView(true);
				refreshTabs(reqView);
				generateNotification("Event.MinorNOL.Submit", reqView);
				viewMap.put(SELECTED_TAB, ID_APP_TAB);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_SUBMIT_QUERY_SUCCESS));
				CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
			}
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in submitQuery()]", ex);
		}			
	}
	
	/**
	 * 	The function is registered as an action to the "Done" button. The Done button is rendered
	 * 	when the request is in <strong>REVIEW REQUIRED</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Validates the text in the review text area. </p>
	 * 	<p>2. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>3. Marks an entry in for the activity logs for the action <p>
	 * 	<p>4. The function then updates the status in the database to <strong>REVIEWED/EVALUATED</strongand 
	 * 		  updates the view root and refreshes the UI which synchronize the user interface with the model. </p>
	 * 
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void done() {		
		logger.logInfo("[done() .. Starts]");						
		try {					
			if( validateReview() ) {
				RequestView currentReq = getRequestView();			
				saveAttachmentsAndComments(currentReq.getRequestId());
				CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.MemsNOL.MSG_REQ_REVIWED, currentReq.getRequestId());
				CommonUtil.saveRemarksAsComments(currentReq.getRequestId(), reviewTxt.getValue().toString(), WebConstants.REQUEST);				
				RequestView reqView = updateRequestStatus(getRequestView(), WebConstants.REQUEST_STATUS_REVIEW_DONE);			
				setRequestView(reqView);
				refreshTabs(reqView);				
				completeTask(
                                                TaskOutcome.OK,						 		
                                                WFTaskAction.POSTBACKRESUBMIT.toString(),
                                                null
                                             );
				setIsView(true);
				viewMap.put(SELECTED_TAB, ID_APP_TAB);
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_REVIWED_SUCCESS));
			}		
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in done()]", ex);
	        ex.printStackTrace();								
		}						
		logger.logInfo("[done() .. Ends]");
	}
	
	/**
	 * 	The function is registered as an action to the "Complete" button. The button is rendered
	 * 	when the request is in <strong>APPROVED</strong> state and the logged in user has appropriate
	 * 	rights to carry out the operation. The flow of the function is as follows
	 * 
	 * 	<p>1. Gets a reference to the request view from the view root saves attachments and comments <p>
	 * 	<p>2. Marks an entry in for the activity logs for the action <p>
	 * 	<p>3. The function then updates the status in the database to <strong>Complete</strong> and updates 
	 * 		  the view root and refreshes the UI which synchronize the user interface with the model. </p>
	 * 
	 * 	@author Farhan Muhammad Amin
	 */
	
	public void complete() {
		logger.logInfo("[complete() .. Starts]");		
		try {
			saveAttachmentsAndComments(getRequestView().getRequestId());
			RequestView reqView = updateRequestStatus(getRequestView(), WebConstants.REQUEST_STATUS_COMPLETE);
			CommonUtil.saveSystemComments(WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED, getRequestView().getRequestId());
			setRequestView(reqView);
			refreshTabs(reqView);
			completeTask(TaskOutcome.OK,						 		
                                     WFTaskAction.OK.toString(),
                                    null);
			generateNotification("Event.MinorNOL.Complete", reqView);
			viewMap.put(SELECTED_TAB, ID_APP_TAB);
			setIsView(true);
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNOL.MSG_NOL_COMPLETE_SUCCESS));
		} catch(Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in complete()]", ex);
		}				
		logger.logInfo("[complete() .. Ends]");	
	}
	
	public void print() {
	
		try
    	{
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void handleNolTypeChange(ValueChangeEvent evt) {		
		String newVal = evt.getNewValue().toString();
		nolTypeGroups.setValue(getUserGroupString(newVal));
	}
	
	/** 
	 * 	The function updates the status of the request in the database.
	 * @param oldView Reference to the request view which is to be updated in the database.
	 * @param status The new status of the request in the database
	 * 
	 * @return upon success the function returns a reference to the updated request view.
	 * 	
	 * @throws PimsBusinessException 
	 */
	
	private RequestView updateRequestStatus(RequestView oldView, String status) throws PimsBusinessException {					
		if( null==oldView || null==status || 0==status.trim().length()) {
			throw new IllegalArgumentException();
		}			
		Long reqId = oldView.getRequestId();
		RequestServiceAgent reqSrvAgent = new RequestServiceAgent();
		reqSrvAgent.updateRequestStatus( reqId, status);
		return reqSrvAgent.getRequestById(reqId);
	}
	
	
	private void saveAttachmentsAndComments(Long reqId) throws PimsBusinessException,Exception {
		loadAttachmentsAndComments(reqId);
		CommonUtil.saveAttachments(reqId);
		String noteOwner = (String)viewMap.get("noteowner");
		CommonUtil.saveComments(reqId,noteOwner);
	}
	
	private void loadUserGroupsInViewRoot() throws Exception {
		if( !viewMap.containsKey(ALL_USER_GROUPS) ) {
			List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
			List<SelectItem> listUserGrps = new ArrayList<SelectItem>(allUserGrps.size());
			for(UserGroup singleGrp : allUserGrps) {
				listUserGrps.add(new SelectItem(singleGrp.getUserGroupId(), isEnglishLocale()? singleGrp.getPrimaryName():singleGrp.getSecondaryName() ));
			}
			Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
			viewMap.put(ALL_USER_GROUPS, listUserGrps);
		}
	}
	
	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}
	
	public HtmlTabPanel getRichPanel() {
		return richPanel;
	}

	public void setRichPanel(HtmlTabPanel richPanel) {
		this.richPanel = richPanel;
	}
	
	public HtmlSelectOneMenu getCmbNolTypes() {
		return cmbNolTypes;
	}

	public void setCmbNolTypes(HtmlSelectOneMenu cmbNolTypes) {
		this.cmbNolTypes = cmbNolTypes;
	}
	
	public HtmlInputTextarea getReasonNOL() {
		return reasonNOL;
	}

	public void setReasonNOL(HtmlInputTextarea reasonNOL) {
		this.reasonNOL = reasonNOL;
	}						
	
	public void setRequestView(RequestView reqView) {
		@SuppressWarnings("unused")
		RequestView oldView = (RequestView) viewMap.remove(WebConstants.REQUEST_VIEW);
		oldView = null;
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
	}
	
	public RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}
	public void setIsView( boolean arg)
	{
		
		viewMap.put("IsNOLView", arg);
	}
	public Boolean getIsView( )
	{
		if( viewMap.get( "IsNOLView" )!= null )
			return (Boolean)viewMap.get("IsNOLView" );
		else 
			return false;
		
	}
	public Boolean getIsTaskPresent()
	{
		return viewMap.get( TASK_LIST_USER_TASK )!= null ||
				viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null ;
	}
	public Boolean getIsNew() {
		RequestView reqView = getRequestView();
		Long reqId = reqView.getStatusId();
		return ( reqId.equals(INVALID_STATUS)) ||
								(reqId.equals(getIdFromStatus(WebConstants.REQUEST_STATUS_NEW)));   
	}
	
	public Boolean getIsReviewRequired() {
		
		RequestView requestView = getRequestView();
		if( requestView != null )
		{
			return getRequestView().getStatusId().equals(getIdFromStatus(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED));
		}
		return false;
	}
	
	public Boolean getIsReviewed() {
						
		RequestView requestView = getRequestView();
		if( requestView != null )
		{
			return getRequestView().getStatusId().equals(getIdFromStatus(WebConstants.REQUEST_STATUS_REVIEW_DONE));
		}
		return false;
	}
	
	public Boolean getIsApprovalRequired() {
		
		RequestView requestView = getRequestView();
		if( requestView != null )
		{
			return getRequestView().getStatusId().equals(getIdFromStatus(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED));
		}
		return false;
	}
	
	public Boolean getIsApproved() {

		RequestView requestView = getRequestView();
		if( requestView != null )
		{
			return getRequestView().getStatusId().equals(getIdFromStatus(WebConstants.REQUEST_STATUS_APPROVED));
		}
		return false;

	}
	
	public Boolean getIsComplete() {
		RequestView requestView = getRequestView();
		if( requestView != null )
		{
			return getRequestView().getStatusId().equals(getIdFromStatus(WebConstants.REQUEST_STATUS_COMPLETE));
		}
		return false;
	}
	
	public Boolean getIsNOLSelected() {
		return richPanel.getSelectedTab().equals(ID_NOL_TAB);
	}
	
	public Boolean getIsUserInGroup() {
		String userInGrp = ((String)viewMap.get(USER_IN_GROUP));
		if( null==userInGrp ) {
			return false;
		}
		return userInGrp.equals("TRUE");
	}
	
	public void setUserInGroup() {
		if(getIsReviewRequired()) {					
			User loggedInUser = getLoggedInUserObj();
			if( loggedInUser.hasUserGroup(getRequestView().getMemsNolReviewGrpId()) ) {
				viewMap.put(USER_IN_GROUP, "TRUE");
			}									
		}		
	}
	
	public Map<String, Set<MemsNOLTypeUserGrpView>> getNolGrpMap() {
		if(viewMap.containsKey(NOL_GROUP_MAP)) {
			return (HashMap<String, Set<MemsNOLTypeUserGrpView>>) viewMap.get(NOL_GROUP_MAP);
		}
		return new HashMap<String, Set<MemsNOLTypeUserGrpView>>();
	}
	
	public List<SelectItem> getAllUserGroups() {
		if( viewMap.containsKey(ALL_USER_GROUPS)) {
			return (List<SelectItem>) viewMap.get(ALL_USER_GROUPS);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	
	//TODO: remove me
	public List<SelectItem> getUserGroups() {
		String memNolTypeId = cmbNolTypes.getValue().toString();
		return getUserGroups(memNolTypeId);
	}
	
	private List<SelectItem> getUserGroups(String memNolTypeId) {
		Map<String, Set<MemsNOLTypeUserGrpView>> userGrpMap = getNolGrpMap();
		Set<MemsNOLTypeUserGrpView> grpSet = userGrpMap.get(memNolTypeId);		
		List<SelectItem> selectList = new ArrayList<SelectItem>();
		for( MemsNOLTypeUserGrpView singleGrp : grpSet ) {
			selectList.add(new SelectItem(singleGrp.getMemsNolTypeUsrGrpId().toString(), singleGrp.getUserGroupId()));					
		}		
		return selectList;
	}
	
	private String getUserGroupString(String memNolTypeId) {
		StringBuilder builder = new StringBuilder();
		Map<String, Set<MemsNOLTypeUserGrpView>> userGrpMap = getNolGrpMap();
		Set<MemsNOLTypeUserGrpView> grpSet = userGrpMap.get(memNolTypeId);
		if( null!=grpSet) {
			for( MemsNOLTypeUserGrpView singleGrp : grpSet ) {
				builder.append(singleGrp.getUserGroupId()+",");					
			}
		}
		if( 0<builder.length() ) {
			builder.deleteCharAt(builder.length()-1);
		}
		return builder.toString();
	}

	public HtmlInputText getNolTypeGroups() {		
		return nolTypeGroups;
	}
	
	public void setNolTypeGroups(HtmlInputText nolTypeGroups) {
		this.nolTypeGroups = nolTypeGroups;
	}
	
	public void handleAttachmentCommentTabStatus(RequestView view)throws Exception {
		if( view.getStatusId().equals(WebConstants.REQUEST_STATUS_COMPLETE)) {
			viewMap.put("canAddAttachment", false);
			viewMap.put("canAddNote", false);
		} else {
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
		}
	}
	
	public Long getInheritanceFileId() {
		return getRequestView().getInheritanceFileView().getInheritanceFileId();
	}

	public HtmlSelectOneMenu getCmbUserGroups() {
		return cmbUserGroups;
	}

	public void setCmbUserGroups(HtmlSelectOneMenu cmbUserGroups) {
		this.cmbUserGroups = cmbUserGroups;
	}	
	@SuppressWarnings("unchecked")
	private void invokeWorkFlow(Long requestId) throws Exception
	{
		WFClient client = new WFClient();
		WorkFlowVO workFlowVO = new WorkFlowVO();
		workFlowVO.setRequestId(requestId);
		workFlowVO.setUserId(getLoggedInUserId());
		workFlowVO.setProcedureTypeId(WebConstants.PROCEDURE_TYPE_MEMS_NOL);
		workFlowVO.setAssignedGroups(nolTypeGroups.getValue().toString());
		client.initiateProcess(workFlowVO);
                
        }
	@SuppressWarnings("unchecked")
	private Boolean invokeBpel(Long requestId) {
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try {
			memsnolbpelproxyii.proxy.MEMSNOLBPELPortClient bpelNOLClient = new memsnolbpelproxyii.proxy.MEMSNOLBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.MemsNOLTypes.MEMSNOLBPEL_ENDPOINT);
                        logger.logDebug("invokeBpel()|endPoint :%s",endPoint );
			String userId = CommonUtil.getLoggedInUser();
			String nolType = cmbNolTypes.getValue().toString();
			bpelNOLClient.setEndpoint(endPoint);
			bpelNOLClient.initiate(requestId, userId, new Long(nolType),WebConstants.PROCEDURE_TYPE_MEMS_NOL, null, null);			
			logger.logInfo("invokeBpel() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
			exception.printStackTrace();
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
			exception.printStackTrace();
		}
		finally{
			if(!success)
			{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
		return success;
	}
        
	@SuppressWarnings("unchecked")
	private Boolean completeTask(
                                     TaskOutcome taskOutcome,
                                     String taskAction,
                                     String assignedGroups
                                    ) throws Exception 
	{

            Boolean success = true;
            
            if(viewMap.get(TASK_LIST_USER_TASK) !=null ){
                    logger.logInfo("Completing Oracle SOA Suite Task");
                    
                    UserTask userTask = null;
                    if(viewMap.containsKey(TASK_LIST_USER_TASK)) {
                        userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
                    }
                    else {
                        userTask = getIncompleteRequestTasks();
                    }
                    if(userTask!=null){
                        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;               
                        BPMWorklistClient client = new BPMWorklistClient(contextPath);
                        client.completeTask(userTask, CommonUtil.getLoggedInUser(), taskOutcome);
                    }
            }
            else if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null ){
                logger.logInfo("Completing Pims WorkFlow Task");
                TaskListVO  userTask = (    TaskListVO )viewMap.get(  WebConstants.TASK_LIST_SELECTED_USER_TASK  ); 
                
                if(taskAction != null && taskAction.trim().length()>0)
                {
                        WorkFlowVO workFlowVO = new WorkFlowVO();
                        workFlowVO.setTaskAction(taskAction);
                        if(assignedGroups !=null && assignedGroups.length()>0)
                        {
                                workFlowVO.setAssignedGroups(assignedGroups);
                        }
                        CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
                }
            }
            else{
                    return false;
            }
            return success;
	}
	
	public UserTask getIncompleteRequestTasks() {
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try {
	   		Long requestId = getRequestView().getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();	   		
	   		if( null!=taskId ) {	   		
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);				 
				taskType = userTask.getTaskType();
				logger.logInfo("Task Type is:" + taskType);
				return userTask;
	   		} else {	   		
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		} catch(PIMSWorkListException ex) {				
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {			
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC) {			
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else {			
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		} catch (Exception exception) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		return null;
	}				
}
