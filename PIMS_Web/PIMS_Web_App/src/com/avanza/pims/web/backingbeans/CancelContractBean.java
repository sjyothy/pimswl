package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.CoreException;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.core.web.context.impl.ViewContextImpl;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.EvacuationServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.proxy.pimsterminatecontractbpelproxy.PIMSTerminateContractBPELPortClient;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ClearenceLetterCriteria;
import com.avanza.pims.report.criteria.SettlementReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.EjariIntegration;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.EjariResponse;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.PaymentBean;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.RequiredDocumentsView;
import com.avanza.pims.ws.vo.SettlementBean;
import com.avanza.pims.ws.vo.ViolationBean;
import com.avanza.pims.ws.vo.ViolationView;
import com.avanza.ui.util.ResourceUtil;

public class CancelContractBean extends AbstractController
{

	private static final long serialVersionUID = 5070806278933027841L;
	private static final Logger logger = Logger.getLogger(CancelContractBean.class);
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private boolean exemptFine;
	private static final String MODE_FROM_REUQEST_SEARCH_REQ_STATUS_REJECED = "MODE_FROM_REUQEST_SEARCH_REQ_STATUS_REJECED";
	private static final String MODE_EVC_NEW = "EVACUATION_NEW";
	private static final String MODE_EVC_REQ = "EVACUATION_REQ";
	private static final String MODE_NOL_REQ = "NOL_REQ";
	private static final String MODE_INSPECTION_REQ = "INSPECTION_REQ";
	private static final String MODE_SETTLE_PAYMENT_REQ = "SETTLE_PAYMENT_REQ";
	private static final String MODE_FOLLOW_UP_REQ = "FOLLOW_UP_REQ";
	private static final String MODE_AFTER_SETTLEMENT = "MODE_AFTER_SETTLEMENT";
	private static final String MODE_PAYMENT_COLLECT_REQ = "PAYMENT_COLLECT_REQ";
	private static final String MODE_COMPLETED_REQ = "MODE_COMPLETED_REQ";
	private static final String MODE_APPROVAL_REQ = "MODE_APPROVAL_REQ";
	private static final String MODE_READONLY = "READONLY_REQ";
	private static final String MODE_DEFAULT = MODE_EVC_NEW;
	private static final String MODE_INITIATE = MODE_EVC_REQ;
	private static final String MODE_POPUP = "MODE_POPUP";
	private static final String EXEMPTED_PAYMENT_MAP = "EXEMPTED_PAYMENT_LIST";
	
	private static final String BEAN_CONTRACT = "contractBean";
	private static final String BEAN_UNIT = "unitsBean";
	private static final String BEAN_VIOLATION = "violationBean";
	private static final String BEAN_VIOLATION_LIST = "violationListBean";
	private static final String VIOLATION_TYPE_LIST = "violationTypeList";
	private static final String BEAN_PAYMENTS = "paymentsBean";
	private static final String BEAN_SETTLEMENT = "settlementBean";
	
	private final String TAB_APPLICATION = "applicationTab";
	private final String TAB_CONTRACT_DETAILS = "contractDetailsTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private final String TAB_PAYMENTS = "paymentsTab";
	private final String TAB_REQUEST_HISTORY = "requestHistoryTab";
	private final String TAB_INSPECTION = "inspectionTab";
	private final String TAB_FOLLOW_UP = "cancelContractFollowUpTab";
	
	private final String BACK_PAGE = "BACK_PAGE";
	private final String BACK_PAGE_TASK_LIST = "TASK_LIST";
	private final String BACK_PAGE_CONTRACT_SEARCH = "CONTRACT_SEARCH";
	private final String BACK_PAGE_REQUEST_SEARCH = "REQUEST_SEARCH";
	private static final String EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION = "EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION";
	
	private List<String> errorMessages=new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();

	//	private String pageMode = MODE_EVC_REQ;
	private String pageMode = MODE_DEFAULT;
	private String SelectedTab;
	
	private transient EvacuationServiceAgent evacuationService = new EvacuationServiceAgent();
	private transient PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private transient CommonUtil commonUtil = new CommonUtil();
	private ContractView contractView;
	
	private String hdnContractId;
	private String hdnContractNumber;
	private String hdnTenantId;
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	
	List<SelectItem> violationTypes = new ArrayList<SelectItem>(0);
	private HtmlCommandButton printCL = new HtmlCommandButton();
	private HtmlDataTable dataTablePaymentSchedule;
	private HtmlCommandButton submitBtn = new HtmlCommandButton();
	private HtmlCommandButton terminateInEjariBtn = new HtmlCommandButton();
	
	
		
	/*private String contractNoText = "";
	private String tenantNameText = "";
	private String contractTypeText = "";
	private String contractStartDateText = "";
	private String contractEndDateText = "";
	private String tenantNumberType = "";
	private String contractStatusText = "";
	private String totalContractValText = "";
	private String feeText = "";
	private String txtUnitType = "";
	private String txtpropertyName = "";
	private String txtunitRefNum = "";
	private String txtUnitRentValue = "";
	private String txtpropertyType = "";*/
	
//	private HtmlSelectOneMenu nolTypeSelectOneMenu = new HtmlSelectOneMenu();
//	private HtmlInputTextarea reasontForNol = new HtmlInputTextarea();
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private SettlementReportCriteria reportCriteria;
	private String showSettlementButton;
	
   public String getShowSettlementButton(){
		
		if( viewMap.get(  "ShowSettlementButton" ) != null && viewMap.get(  "ShowSettlementButton" ).toString().equals("1") )
			return "1";
		
		return "0";
	}
   @SuppressWarnings ( "unchecked" )
   public void  setShowSettlementButton(Boolean showSettlementButton){
		
	   
	   if ( showSettlementButton != null && showSettlementButton)
	   {
		   this.showSettlementButton = "1";
		   viewMap.put(  "ShowSettlementButton" ,"1");
	   }
		
	}
	public boolean isFollowUpDone(){
		
		if(pageMode==null)
			return false;
		
		
		if (pageMode.equalsIgnoreCase(MODE_FOLLOW_UP_REQ) )
			return true;
		return false;
	}
	public boolean isShowInspection(){
		if(pageMode==null)
			return false;
		else if (pageMode.equalsIgnoreCase(MODE_NOL_REQ) || pageMode.equalsIgnoreCase(MODE_EVC_REQ) )
			return false;
		
		else if (pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ) || pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ) || 
				pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ) || pageMode.equalsIgnoreCase(MODE_COMPLETED_REQ) ||
				pageMode.equalsIgnoreCase(MODE_READONLY) 
				|| pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ))
			return true;
		return false;
	}
	
	public boolean isShowSettlementDetails(){
		if(pageMode==null)
			return false;
		return true;
	}
	
	public boolean isShowPayments(){
		if(pageMode==null) {
			return false;
		}	
		else if (pageMode.equalsIgnoreCase(MODE_NOL_REQ) || pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ) || 
				 pageMode.equalsIgnoreCase(MODE_EVC_REQ) || pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ) ) {
			return false;
		}	
		// CHANGED BY Majid
		//else if (pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ)|| pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT) || pageMode.equalsIgnoreCase(MODE_COMPLETED_REQ) || pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ))
		else if (pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ)|| pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT) || 
				 pageMode.equalsIgnoreCase(MODE_COMPLETED_REQ) || pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ) || 
				 (isFromRequestSearch() && MODE_EVC_NEW.equalsIgnoreCase(pageMode)) ) {
			return true;
		}	
		return false;
	}
	public boolean isShowFollowUpTab()
	{
		if ( pageMode == null )
		{
			return false;
		}
		if ( pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ) || pageMode.equalsIgnoreCase(MODE_COMPLETED_REQ) || 
		     pageMode.equalsIgnoreCase(MODE_FOLLOW_UP_REQ) || pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ) ||
		     pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT)     
		)
			return true;
		return false;
	}
	
	public boolean isShowSettlement(){
		return false;
	}
	
	public boolean isShowUnit(){
		return true;
	}
	
	public boolean isShowRequiredDocument(){
		if(pageMode==null)
			return true;
		else if (pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ) ||pageMode.equalsIgnoreCase(MODE_EVC_REQ) || pageMode.equalsIgnoreCase(MODE_NOL_REQ) || pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ))
			return false;
		
		return true;
	}
	public boolean isShowRejectButton(){
		if(pageMode==null)
			return false;
		else if  ( pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ) )
			return true;
		
		return false;
	}
	public boolean isShowSendForInspectionButton(){
		if(pageMode==null)
			return false;
		else if  ( pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ) )
			return true;
		
		return false;
	}
	
	public boolean isShowRequestHistory(){
		return true;
	}
	
	public boolean isShowDocuments(){
		return true;
	}
	
	public boolean isShowNotes(){
		return true;
	}
	
	public boolean isShowNOL(){
		if(pageMode==null)
			return false;
		if (pageMode.equalsIgnoreCase(MODE_NOL_REQ))
			return true;
		return false;
	}
	public boolean isShowApproveButton()
	{
		if(pageMode==null)
			return false;
		else if ( pageMode.equalsIgnoreCase( MODE_APPROVAL_REQ ) )
			return true;
		
		return false;
	} 
	public boolean isShowSubmit()
	{
		if(pageMode==null)
			return false;
		else if (   
				    pageMode.equalsIgnoreCase(MODE_NOL_REQ )|| 
				    pageMode.equalsIgnoreCase(MODE_EVC_REQ) || 
					pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ) || 
					pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ)
				)
			return true;
		
		return false;
	}
	
	public boolean isShowSave()
	{
		
		boolean showSave = false;
		
		if(pageMode==null) 
		{
			showSave = false;
		}	
		
		if ( 
			  MODE_EVC_NEW.equalsIgnoreCase(pageMode) || 
		   	  MODE_EVC_REQ.equalsIgnoreCase(pageMode) 
			)
		{
			showSave = true;
		}
		
		if (isFromRequestSearch() && MODE_EVC_NEW.equalsIgnoreCase(pageMode)){
			showSave = false;
		}
		
		if (isFromTaskList() && MODE_EVC_NEW.equalsIgnoreCase(pageMode) ){
			showSave = false;
		}
		
		
		
		return showSave;
	}
	
	public boolean isShowComplete(){
		if(pageMode==null)
			return false;
		if (pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ) || pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT))
			return true;
		return false;
	}
	public boolean isShowSendForFollowUp(){
		if(pageMode==null)
			return false;
		if ( pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT))
			return true;
		return false;
	}
	public boolean isShowSendForCollectPayment(){
		if(pageMode==null)
			return false;
		if ( pageMode.equalsIgnoreCase(MODE_AFTER_SETTLEMENT))
			return true;
		return false;
	}
	public boolean isShowCancelRequest(){
		if(pageMode==null)
			return false;
		if(viewMap.containsKey("paymentDoneSuccessfully") && new Boolean(viewMap.get("paymentDoneSuccessfully").toString()))
			return false;
		if (pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ))
			return true;
		
		return false;
	}

	public boolean isShowClose(){
		return true;
	}
	
	public boolean isShowPayment(){
		if(pageMode==null)
			return false;
		if(viewMap.containsKey("paymentDoneSuccessfully") && new Boolean(viewMap.get("paymentDoneSuccessfully").toString()))
			return false;
		if (pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ)|| pageMode.equalsIgnoreCase(MODE_COMPLETED_REQ))
			return true;
		
		return false;
	}
	
	public boolean isReadOnly(){
		if(pageMode==null)
			return false;
		if (pageMode.equalsIgnoreCase(MODE_READONLY))
			return true;
		return false;
	}
	
	public boolean isFromRequestSearch(){
		return viewMap.containsKey( WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH);
	}
	
	public boolean isFromTaskList(){
//			UserTask userTask = (UserTask) webContext.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		UserTask userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
		if (userTask != null)
			return true;
		return false;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		this.pageMode = viewContext.getAttribute("pageMode");
		if(hdnPersonId!=null && !hdnPersonId.equals(""))
		{
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}
		if(hdnPersonName!=null && !hdnPersonName.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
		if(hdnCellNo!=null && !hdnCellNo.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
		if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
		{
			DomainDataView ddv = new DomainDataView();
			if(Long.parseLong(hdnIsCompany)==1L)
			  ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
			else
			  ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);
		    
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
		
		}
		String selectedTab = (String)viewMap.get("selectedTab");
		if(selectedTab!=null)
		{
			tabPanel.setSelectedTab(selectedTab);
			viewMap.remove("selectedTab");
		}
				
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{

		viewMap.put("FROM_CANCEL_CONTRACT", true);
		logger.logDebug("init|Start");
		super.init();
		
    	initContext();
    	
    	HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		errorMessages=new ArrayList<String>(0);
		successMessages=new ArrayList<String>(0);
		
		if(CommonUtil.getIsEnglishLocale())
		{
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_EN,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		}
		else
		{
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_AR,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		}
		
		if (viewContext != null && viewContext.getAttribute("pageMode") != null)
		{
			pageMode = viewContext.getAttribute("pageMode");
		}
		try
		{
			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
			fromEditAddPaymentSchedule();	
			if (!isPostBack()) 
			{
				//for showing print btn on UI
				printCL.setRendered(false);
				loadAttachmentsAndComments();
				getTenantTypeDomainData();
			    if( getRequestParam(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH)!=null &&
			    	getRequestParam(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH ).toString().length()>0 &&		
			    	getRequestParam(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH ).toString().equals( "true" )
			      )
			    	viewMap.put(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH, "true");
				 
				//Removes the task from session and adds it to viewMap
				if(getExternalContext().getSessionMap().containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				{
					 UserTask userTask = (UserTask) webContext.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
					 viewMap.put(WebConstants.CANCEL_CONTRACT_TASK_LIST, userTask);
				}
				else if(request.getParameter("pageMode")!=null)
					viewMap.put(MODE_POPUP,true);
				
				contractView = getContract();
				viewMap.put("CONRTRACT_VIEW_FOR_COMPLETE_TASK", contractView);
				if (contractView != null) 
				{
					viewContext.setAttribute("contractView", contractView);
					viewMap.put("FOR_FIRST_TIME", true);
					if( setPageMode() )
						return;
					setSelectedTab();
					this.createBeanFromContract(contractView);
					this.populateInspectionTab();
					this.populatePaymentsTab(contractView.getContractId());
					viewContext.setAttribute(BEAN_SETTLEMENT, new ArrayList<SettlementBean>());
					
					populateContract();
				}
			}
			
			// Added for receive payments popup logic - START
			if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
			{
				List<PaymentScheduleView> temp = (List<PaymentScheduleView>)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				viewMap.put(WebConstants.SESSION_REGISTER_AUCTION_SELECTED_PAYMENT_SCHEDULE,temp);
			}
			
			else if(sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null)
			{
				Boolean success = (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
				sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
				if(success)
				{
	 		       collectPayments();
			 	}
			}
			
			if(sessionMap.containsKey("PAYMENT_SCHDULE_SPLIT_DONE"))
			{
					Long reqId = (Long) sessionMap.remove("PAYMENT_SCHDULE_SPLIT_DONE");
					contractView = getContractView();
					populatePaymentsTab(contractView.getContractId());
					//List<PaymentScheduleView> list = new PropertyServiceAgent().getContractPaymentSchedule( null, reqId );
					//getPaymentBeanFromPaymentScheduleView(list);	
			}
			logger.logDebug("init|Finish");
		} 
		catch (Exception ex) 
		{
			logger.LogException("init|Exception Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private Boolean collectPayments()
	{
    	logger.logInfo("collectPayments started...");
    	Boolean success = true;
		successMessages.clear();
		errorMessages.clear();

		try{
			 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     
		     contractView = viewContext.getAttribute("contractView");
		     prv.setPaidById(contractView.getTenantView().getPersonId());
		     prv = psa.collectPayments(prv);
		     
		     viewMap.put("paymentDoneSuccessfully", true);
		     successMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
		     
		     populatePaymentsTab(contractView.getContractId());
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.CANCEL_CONTRACT);
		     logger.logInfo("collectPayments completed successfully...");
		}
		catch(Exception ex)
		{
			success = false;
			logger.LogException("collectPayments crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED_FAILED));
		}
		return success;
	}
	@SuppressWarnings( "unchecked" )
	private void getTenantTypeDomainData(){
		if(!viewMap.containsKey(WebConstants.SESSION_CONTRACT_TENANT_TYPE))
		{		   
		   List<DomainDataView> ddl=CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
		   viewMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE, ddl);
		   DomainDataView ddvCompany= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_COMPANY);
		   viewMap.put(WebConstants.TENANT_TYPE_COMPANY,ddvCompany);
		   DomainDataView ddvIndividual= CommonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_INDIVIDUAL);
		   viewMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL,ddvIndividual);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, Constant.PROCEDURE_TYPE_EVACUATION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(String associatedObjectId){
		if(StringHelper.isNotEmpty(associatedObjectId)){
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, associatedObjectId);
			viewMap.put("entityId", associatedObjectId);
			viewMap.put("noteowner", WebConstants.REQUEST);
			
		}
	}
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null)
		    	success = CommonUtil.updateDocuments();
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
    	}
    	catch (Throwable throwable) {
			logger.LogException("saveComments| crashed ", throwable);
		}
    	return success;
    }
	
	private String getTenantType(Boolean isCompany)
    {
       logger.logDebug("getTenantType started...");
     
       if(isCompany!=null){
    	   DomainDataView ddv = null;
    	   if(isCompany){
    		   ddv = (DomainDataView )viewMap.get(WebConstants.TENANT_TYPE_COMPANY);	   
    	   }
    	   else{
    		   ddv = (DomainDataView )viewMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
    	   }
    	   if(ddv!=null)
    		   return getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr();
       }
       
       logger.logDebug("getTenantType completed successfully...");
  
       return "";
    }
	@SuppressWarnings("unchecked")
	private ContractView getContract() {
		
		ContractView contractView = (ContractView) getRequestParam(WebConstants.Contract.CONTRACT_VIEW);
		RequestView requestView = webContext.getAttribute(WebConstants.REQUEST_VIEW);
		try {
			LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
			UserTask userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			if (contractView == null && userTask != null) 
			{
				viewMap.put(BACK_PAGE, BACK_PAGE_TASK_LIST);
				Long contractId = Long.parseLong(userTask.getTaskAttributes().get("CONTRACT_ID"));
				Long requestId = Long.parseLong(userTask.getTaskAttributes().get("REQUEST_ID"));
				HashMap contractHashMap = new HashMap();
				ContractView contract = new ContractView();
				contract.setContractId(contractId);
				contractHashMap.put("contractView", contract);
				contractHashMap.put("dateFormat", locale.getShortDateFormat());
				List<ContractView> list = propertyServiceAgent.getAllContracts(contractHashMap);
				contractView = list.get(0);
			} 
			 else if (contractView != null)
			 {
			 	viewMap.put(BACK_PAGE, BACK_PAGE_CONTRACT_SEARCH);
				HashMap contractHashMap = new HashMap();
				ContractView contract = new ContractView();
				contract.setContractId(contractView.getContractId());
				contractHashMap.put("contractView", contract);
				contractHashMap.put("dateFormat", locale.getShortDateFormat());
				List<ContractView> list = propertyServiceAgent.getAllContracts(contractHashMap);
				
				contractView = list.get(0);
			}
			else if (requestView != null) 
			{
				if(requestView.getIsExempted() !=null )
					viewMap.put("isExempted", requestView.getIsExempted());
				viewMap.put(BACK_PAGE, BACK_PAGE_REQUEST_SEARCH);
				HashMap contractHashMap = new HashMap();
				ContractView contract = new ContractView();
				contract.setContractId(requestView.getContractView().getContractId());
				contractHashMap.put("contractView", contract);
				contractHashMap.put("dateFormat", locale.getShortDateFormat());
				List<ContractView> list = propertyServiceAgent.getAllContracts(contractHashMap);
				webContext.removeAttribute(WebConstants.REQUEST_VIEW);
				List<DomainDataView> ddview = CommonUtil.getDomainDataListForDomainType(Constant.REQUEST_STATUS_CANCELCNTRACT);
				//Code to check if request has rejected status and coming from request search then pageMode is emptied
				if( viewMap.get(WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH)!=null &&
					(
					 requestView.getStatusId().equals(CommonUtil.getIdFromType(ddview, WebConstants.REQUEST_STATUS_REJECTED).getDomainDataId())||
					 requestView.getStatusId().equals(CommonUtil.getIdFromType(ddview, WebConstants.REQUEST_STATUS_CANCELED).getDomainDataId()) ||
					 requestView.getStatusId().equals(CommonUtil.getIdFromType(ddview, WebConstants.REQUEST_STATUS_ACCEPTED_REJECTED).getDomainDataId()) 
				     )
				   )
				{
					  pageMode = MODE_FROM_REUQEST_SEARCH_REQ_STATUS_REJECED;
				      viewMap.put( "rejectedRequestFromSearch", requestView );
				}
				
				contractView = list.get(0);
			}
		} catch (Throwable t) {
			logger.LogException("Exception while getting the Contract from the BPEL Client.", t);
		}		
		return contractView;
	}

	private void checkPaymentReceipt() {
		PaymentReceiptView paymentReceiptView = webContext.removeAttribute(WebConstants.PAYMENT_RECEIPT_VIEW);
		if (paymentReceiptView != null)
			viewContext.setAttribute(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	}
	@SuppressWarnings("unchecked")
	private void populatePaymentsTab(Long contractId) {

		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		boolean paymentDoneSuccessfully = true;
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		List<PaymentScheduleView> payments = null;
		DomainDataView ddvPending = CommonUtil.getIdFromType(
				        CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_PENDING);
		try 
		{
			payments = evacuationService.getPaymentSchedules(new Long(contractId));
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("Error:CancelContract while populating the paymentsTab", e);
		}
		
		if (payments != null) 
		{
			Map<Long,PaymentBean> exemptedMap  = null;
			if(viewMap.get(EXEMPTED_PAYMENT_MAP) != null )
			{
				exemptedMap = (HashMap<Long,PaymentBean>)viewMap.get( EXEMPTED_PAYMENT_MAP ); 
			}
			for (PaymentScheduleView view : payments) 
			{
				PaymentBean pb = new PaymentBean();
				//If some payments status has been edited to exempted but not yet save then we will use that 
				//exempted one and not take the row from db
				if( exemptedMap != null && exemptedMap.containsKey( view.getPaymentScheduleId() ))
				{
				 	
					pb = exemptedMap.get( view.getPaymentScheduleId() );
				} 	
				else
				{
					pb.setAmount(String.valueOf(view.getAmount()));
					if (view.getPaymentDueOn() != null)
					{
						pb.setDueDate(view.getPaymentDueOn().toString());
					}
					pb.setPaymentMethod(english?view.getPaymentModeEn():view.getPaymentModeAr());
					if (view.getPaymentDate() != null)
					{
						pb.setPaymentDate(view.getPaymentDate().toString());
					}
					pb.setStatus(english?view.getStatusEn():view.getStatusAr());
					pb.setPaymentType(english?view.getTypeEn():view.getTypeAr());
					pb.setPaymentScheduleView(view);
					
					if(view.getStatusId().compareTo(ddvPending.getDomainDataId())==0)
					{
						paymentDoneSuccessfully = false;
					}
				}
				
				paymentBeans.add(pb);
			}
		}
		viewMap.put("paymentDoneSuccessfully", paymentDoneSuccessfully);
		viewContext.setAttribute(BEAN_PAYMENTS, paymentBeans);
	}
   @SuppressWarnings( "unchecked" )
   private boolean createReceiptPayment( List<PaymentBean> paymentBeans ) 
   {
		//Now creating the receipt payment view object
		//List<DomainDataView> ddview = CommonUtil.getDomainDataListForDomainType(Constant.PAYMENT_SCHEDULE_TYPE);
		List<PaymentScheduleView> psViewList = new ArrayList<PaymentScheduleView>( 0 );
		PaymentReceiptView paymentReceiptView = viewContext.getAttribute( WebConstants.PAYMENT_RECEIPT_VIEW );
		DomainDataView ddvPending = CommonUtil.getIdFromType(
				                                              CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS ), 
				                                              WebConstants.PAYMENT_SCHEDULE_PENDING
				                                             );
		if ( paymentReceiptView == null )
			paymentReceiptView = new PaymentReceiptView();
		for ( PaymentBean pb : paymentBeans ) 
		 if ( pb.getPaymentScheduleView().getStatusId().compareTo( ddvPending.getDomainDataId() ) == 0 
			  && pb.getSelected() )  {
			paymentReceiptView.setAmount( pb.getPaymentScheduleView().getAmount() );
			paymentReceiptView.setPaymentScheduleView( pb.getPaymentScheduleView() );
			psViewList.add( pb.getPaymentScheduleView() );
		 }

		viewContext.setAttribute( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, psViewList );
		//webContext.setAttribute(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
		return true;
	}

	private void populateInspectionTab()
	{

		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		viewContext.setAttribute(BEAN_VIOLATION, new ViolationBean());
		viewContext.setAttribute(BEAN_VIOLATION_LIST, new ArrayList<ViolationBean>());
		if (isShowInspection()) 
		{
			RequestView requestView = viewContext.getAttribute("requestView");
			if (requestView != null )
			{
				InspectionView inspectionView = null;
				try {
					inspectionView = evacuationService.getInspectionFor(requestView.getRequestId());
					if(inspectionView!=null)
					{
						ContractView cv = getContractView();
						ContractUnitView unitView = cv.getContractUnitView().iterator().next();
						List<ViolationView> violations = evacuationService.getInspectionViolationFor(inspectionView.getInspectionId(), unitView.getUnitView().getUnitId(), unitView.getContractUnitId());
						List<ViolationBean> violationBeans = new ArrayList<ViolationBean>();
						for (ViolationView v : violations){
							ViolationBean bean = new ViolationBean(String.valueOf(violationBeans.size()),
									v.getViolationDate(), v.getCategoryId().toString(), english?v.getTypeEn():v.getTypeAr(), v.getTypeId().toString(), english?v.getCategoryEn():v.getCategoryAr(), String.valueOf(v.getDamageAmount()), v.getDescription(), english?v.getStatusEn():v.getStatusAr());
							violationBeans.add(bean);
						}
						viewContext.setAttribute(BEAN_VIOLATION_LIST, violationBeans);
					}
					//new ArrayList is used here, because BEAN_VIOLATION_LIST key expects some value.
					else
						viewContext.setAttribute(BEAN_VIOLATION_LIST, new ArrayList<ViolationBean>());
					
				} catch (PimsBusinessException e) {
					logger.LogException("Error while loading the inspection data from CancelContract.populateInspectionTab() method.", e);
				}
				if (inspectionView != null)
					viewContext.setAttribute("inspectionView", inspectionView);
			}
		} 
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getViolationTypes()
	{
		
		if(viewMap.containsKey(VIOLATION_TYPE_LIST))
			violationTypes = (ArrayList<SelectItem>)viewMap.get(VIOLATION_TYPE_LIST);
	 
		return violationTypes;
	
	}
	@SuppressWarnings("unchecked")
	public void loadViolationTypesForCategory()
	{
		String methodName="loadViolationTypesForCategory|";
		try 
		{
			logger.logInfo(methodName+"Start");
			List<SelectItem> itemList = new ArrayList<SelectItem>(0); 
			if(this.getViolationBean().getCategory()!=null && this.getViolationBean().getCategory().length()>0)
			{
			  ApplicationBean bean = (ApplicationBean)getBean("pages$ApplicationBean");
			  
			  itemList = bean.getViolationTypeListByCategory(new Long(this.getViolationBean().getCategory()));
			
			
			}
				
			viewMap.put(VIOLATION_TYPE_LIST,itemList);
			logger.logInfo(methodName+"Finish");
			
		}
		catch (Exception e) 
		{
			logger.LogException(methodName+"Error Occured", e);
		}
		
		
		
		
	}
	@SuppressWarnings("unchecked")
	private boolean setPageMode() throws PimsBusinessException, IOException, PIMSWorkListException,Exception {
		//Getting the Request from the Database to determine the mode of the page.
		
		terminateInEjariBtn.setRendered(false);
		if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		
		if ( pageMode.equalsIgnoreCase(MODE_EVC_NEW) || pageMode.equalsIgnoreCase(MODE_FROM_REUQEST_SEARCH_REQ_STATUS_REJECED)) 
		{
			
			
			RequestView requestView = null;
			
//			List<DomainDataView> ddview = CommonUtil.getDomainDataListForDomainType(Constant.REQUEST_STATUS_CANCELCNTRACT);
			if ( pageMode.equalsIgnoreCase(MODE_FROM_REUQEST_SEARCH_REQ_STATUS_REJECED))
			  requestView = ( RequestView ) viewMap.get( "rejectedRequestFromSearch");
			else if( pageMode.equalsIgnoreCase(MODE_EVC_NEW)  )
			{
				try 
				{
					requestView = this.evacuationService.getEvacuationRequest(this.contractView.getContractId(), Constant.CANCEL_CONTRACT);
					if(requestView  != null && requestView.getIsExempted() != null)
						viewMap.put("isExempted", requestView.getIsExempted());
					
				}
				catch (PimsBusinessException e) 
				{
					if (this.contractView != null) 
					{
						logger.LogException("Exception while calling the EvacuationService.getEvacuationRequest(%s,%s)", e, this.contractView.getContractId(), Constant.CANCEL_CONTRACT);
						throw new CoreException("Exception while calling the EvacuationService.getEvacuationRequest(%s,%s)", e, this.contractView.getContractId(), Constant.CANCEL_CONTRACT);
					}
					logger.LogException("Exception while calling the EvacuationService.getEvacuationRequest(%s,%s)", e, "null", Constant.CANCEL_CONTRACT);
					throw new CoreException("Exception while calling the EvacuationService.getEvacuationRequest(%s,%s)", e, "null", Constant.CANCEL_CONTRACT);
				}
			}
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			submitBtn.setValue(CommonUtil.getBundleMessage("cancelContract.submit"));
			
			if (requestView == null || requestView.getRequestId() == null)
			{
				this.pageMode = MODE_EVC_NEW;
				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				viewMap.put("applicationDetailsReadonlyMode", false);
			}
			else if (requestView.getStatusId().compareTo(Constant.REQUEST_STATUS_NEW_ID) == 0)
			{
				this.pageMode = MODE_EVC_REQ;
				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				viewMap.put("applicationDetailsReadonlyMode", false);
			}
			else if (requestView.getStatusId().compareTo( Constant.REQUEST_STATUS_APPROVAL_REQUIRED_ID)== 0)
			{
				// Below line is commented to by pass the NOL printing step
				//this.pageMode = MODE_NOL_REQ;
				this.pageMode = MODE_APPROVAL_REQ;
				submitBtn.setValue(CommonUtil.getBundleMessage("commons.approve"));
				if(
						contractView.getEjariNumber() != null && 
						contractView.getEjariNumber().trim().length()>0 &&
						(
							contractView.getTerminatedInEjari()== null || 
							contractView.getTerminatedInEjari().equals("0")
						)
						
				  )
				{
					terminateInEjariBtn.setRendered(true);	
				}
//				getUserTask(requestView.getRequestId());
//				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			}
			else if (requestView.getStatusId().compareTo(Constant.REQUEST_STATUS_SITE_VISIT_REQUIRED_ID)==0)
			{
				// Below line is commented to by pass the NOL printing step
				//this.pageMode = MODE_NOL_REQ;
				this.pageMode = MODE_INSPECTION_REQ;
//				getUserTask(requestView.getRequestId());
//				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			}
			else if (requestView.getStatusId().compareTo(Constant.CANCELCNTRACT_NOL_ISSUED_ID) == 0 )
			{
				viewMap.put("selectedTab", TAB_INSPECTION);
				this.pageMode = MODE_INSPECTION_REQ;
//				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//				getUserTask(requestView.getRequestId());
			}
			
			else if (requestView.getStatusId().compareTo ( Constant.REQUEST_STATUS_APPROVED_ID)==0) 
			{
					if(!viewMap.containsKey(MODE_POPUP))
					{
						this.pageMode = MODE_SETTLE_PAYMENT_REQ;
						viewContext.setAttribute("pageMode", pageMode);
						viewContext.setAttribute("requestView", requestView);
						viewContext.setAttribute("contractView", contractView);
						webContext.setAttribute("pageMode", pageMode);
						webContext.setAttribute("requestView", requestView);// put to session
						webContext.setAttribute("contractView", contractView);// put to session
						sessionMap.put("FROM_CANCEL_CONTRACT_BEAN", true);
						HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//						getUserTask(requestView.getRequestId());
//						if(getExternalContext().getSessionMap().containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//						{
//							 UserTask userTask = (UserTask) webContext.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//							 viewMap.put(WebConstants.CANCEL_CONTRACT_TASK_LIST, userTask);
//						}
						try 
						{
							response.sendRedirect("settlement.jsf");
						} 
						catch (Exception e) 
						{
							logger.LogException("Exception when forwarding the request to settlement.jsp", e);
							return true;
						}
					}
					else
					{
						this.pageMode = MODE_READONLY;
						getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);
					}
			}
			else if (
						requestView.getStatusId().compareTo(Constant.CANCELCNTRACT_SETTLED_ID) == 0 ||
						requestView.getStatusId().compareTo(Constant.REQUEST_STATUS_ACCEPTED_REVIEWED_ID) == 0
			        ) 
			{
				UserTask userTask = null;
				this.setShowSettlementButton( true  );
				if(
						contractView.getEjariNumber() != null && 
						contractView.getEjariNumber().trim().length()>0 &&
						(
							contractView.getTerminatedInEjari()== null || 
							contractView.getTerminatedInEjari().equals("0")
						 )
						
				  )
				{
					terminateInEjariBtn.setRendered(true);	
				}
				if( viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST) !=null )
					userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
				
				if(  userTask !=null && userTask.getTaskType() != null )
				{
					
					
					
					if(userTask.getTaskType().equals( WebConstants.CancelContract_TaskTypes.TerminateContractCollectRequestPayment ) )
					{
						this.pageMode = MODE_PAYMENT_COLLECT_REQ;
						Long specifiedLetterCount = new UtilityServiceAgent().getSpecifiedLetterCountForContract(WebConstants.LetterTypes.CLEARENCE_LETTER, this.getContractView().getContractId());
						if(specifiedLetterCount !=null && specifiedLetterCount.compareTo(0L)==0)
						printCL.setRendered(true);
						else
							printCL.setRendered(false);
						viewMap.put("selectedTab", TAB_PAYMENTS);
					}
					else if (userTask.getTaskType().equals( WebConstants.CancelContract_TaskTypes.TerminateContractForHOSAfterSettlement ) )
					{
						this.pageMode = MODE_AFTER_SETTLEMENT;
						Long specifiedLetterCount = new UtilityServiceAgent().getSpecifiedLetterCountForContract(WebConstants.LetterTypes.CLEARENCE_LETTER, this.getContractView().getContractId());
						if(specifiedLetterCount !=null && specifiedLetterCount.compareTo(0L)==0)
						printCL.setRendered(true);
						else
							printCL.setRendered(false);
						viewMap.put("selectedTab", TAB_PAYMENTS);
					}
				}
			}
			else if (requestView.getStatusId().compareTo( Constant.REQUEST_STATUS_ON_FOLLOW_UP_ID)==0)
			{
				viewMap.put("selectedTab", TAB_FOLLOW_UP);
				this.pageMode = MODE_FOLLOW_UP_REQ;
			}
			else if(requestView.getStatusId().compareTo(WebConstants.REQUEST_STATUS_COMPLETE_ID)==0)
			{
				this.pageMode = MODE_COMPLETED_REQ;
				viewMap.put("canAddAttachment",false);
				Long specifiedLetterCount = new UtilityServiceAgent().getSpecifiedLetterCountForContract(WebConstants.LetterTypes.CLEARENCE_LETTER, this.getContractView().getContractId());
				if(specifiedLetterCount !=null && specifiedLetterCount.compareTo(0L)==0)
				    printCL.setRendered(true);
				else
					printCL.setRendered(false);
				
				viewMap.put("canAddNote", false);
			}
			else if(
						requestView.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_ID)==0 ||
						requestView.getStatusId().compareTo( WebConstants.REQUEST_STATUS_CANCELED_ID)==0  ||
						requestView.getStatusId().compareTo( WebConstants.REQUEST_STATUS_ACCEPTED_REJECTED_ID)==0  
			        )
			{
				
				//This means not coming from request search and therefore should be opened in mode so that new request can be created
			    // else if coming from request search should show that request's history/details
				if( !viewMap.containsKey( WebConstants.CANCEL_CONTRACT_FROM_REQUEST_SEARCH ) )
				{
				  requestView = new RequestView();
				  viewMap.put("applicationDetailsReadonlyMode", false);
				}
				else
					this.pageMode = MODE_READONLY;
			}
			else 
			{
				this.pageMode = MODE_READONLY;
				Long specifiedLetterCount = new UtilityServiceAgent().getSpecifiedLetterCountForContract(WebConstants.LetterTypes.CLEARENCE_LETTER, this.getContractView().getContractId());
				if(specifiedLetterCount !=null && specifiedLetterCount.compareTo(0L)==0)
				printCL.setRendered(true);
				else
					printCL.setRendered(false);
				
				getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
			}
			viewContext.setAttribute("pageMode", pageMode);
			
			if(requestView!=null && requestView.getRequestId()!=null)
			{
				viewContext.setAttribute("requestView", requestView);
				Long requestId = requestView.getRequestId();
				populateApplicationDataTab(requestId.toString());
				loadAttachmentsAndComments(requestId.toString());
			}
		}
		return false;
	}

	private void initContext() {
		webContext = ApplicationContext.getContext().get(WebContext.class);
    	requestContext = ApplicationContext.getContext().get(RequestContext.class);
    	viewContext = ApplicationContext.getContext().get(ViewContext.class);

    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	webContext = new WebContextImpl(req.getSession());
    	requestContext = new RequestContextImpl(req);
    	if (getFacesContext().getViewRoot() != null) {
    		viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
    		ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
    	}
    	ApplicationContext.getContext().add(WebContext.class.getName(), webContext);
    	ApplicationContext.getContext().add(RequestContext.class.getName(), requestContext);
		if (viewContext != null && viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
	}
	
	@Override
	public void preprocess(){
		super.preprocess();
		
    	initContext();
    	
    	this.checkPaymentReceipt();
	}
	@SuppressWarnings("unchecked")
	public void onSplitPayment()
	{	
		try
		{
			logger.logInfo("onSplitPayment| Start...");
			PaymentBean row = (PaymentBean)dataTablePaymentSchedule.getRowData();		
			sessionMap.put("PAYMENT_SCHDULE_SPLIT",row.getPaymentScheduleView());
			AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
			addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, "javaScript:openSplitingPopup();");
			logger.logInfo("onSplitPayment| Finish...");
		}
		catch (Exception ex) 
		{
			logger.LogException("onSplitPayment| Exception Occured::", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void onPayment(ActionEvent event){
		logger.logDebug("CancelContractBean.onPayment() Event called");
    	
		successMessages.clear();
		errorMessages.clear();
		
		try{
			boolean paymentsSelected = false;
			
			List<PaymentBean> payments = getPayments();
			
			for (PaymentBean paymentBean :payments) {
				if (paymentBean.getSelected()!=null && paymentBean.getSelected()) {
					paymentsSelected=true;
					break;
				}
			}
			
			if (!paymentsSelected) { // no payment selected
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.SELECT_AT_LEAST_ONE_PAYMENT));
			}
			else {
				viewMap.put("selectedTab", TAB_PAYMENTS);
				if ( this.createReceiptPayment( this.getPayments() ) ) 
				{
					//PaymentReceiptView paymentReceiptView = webContext.getAttribute(WebConstants.PAYMENT_RECEIPT_VIEW);
					//PaymentScheduleView paymentScheduleView = paymentReceiptView.getPaymentScheduleView();
					//if(paymentScheduleView!=null)

					List<PaymentScheduleView> paymentsForCollection = (ArrayList<PaymentScheduleView>)viewContext.getAttribute(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
					//paymentsForCollection.add(paymentScheduleView);
					sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);

					PaymentReceiptView paymentReceiptView2 =new PaymentReceiptView();
					sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView2);

					FacesContext facesContext = FacesContext.getCurrentInstance();
					ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
					String javaScriptText = "javascript:receivePayments();";

					// Add the Javascript to the rendered page's header for immediate execution
					AddResource addResource = AddResourceFactory.getInstance(facesContext);
					addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

				}
			
		        logger.logInfo("openReceivePaymentsPopUp completed successfully...");
			}
		}
		catch(Exception ex)
		{
			logger.LogException("openReceivePaymentsPopUp crashed...", ex);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	
	public void onViewSettlementDetails(ActionEvent event) {
		// TODO open settlement.jsp page
		
		calculateSettlement();
	}
	public String  onCalculateEvacFine()
	{
		calculateSettlement();
		
		return "";
	}
	
	private void completeTask(TaskOutcome taskOutcome, String successMessageKey){
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			String loginId = CommonUtil.getLoggedInUser();
			UserTask userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			bpmWorkListClient.completeTask(userTask, loginId, taskOutcome);
			this.successMessages.add(ResourceUtil.getInstance().getProperty(successMessageKey));
		} catch (Throwable t) {
			logger.LogException("Exception while completing task from BPEL.", t);
		}
	}
	@SuppressWarnings("unchecked")
	public String onCancelRequest(){
		if(viewMap.containsKey("paymentDoneSuccessfully")){
			completeTask(TaskOutcome.CANCEL, MessageConstants.CommonsMessages.REQUEST_CANCEL_SUCCESS);
			RequestView requestView = viewContext.getAttribute("requestView");
			Long requestId = requestView.getRequestId();
			try {
				if(requestId!=null){
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_CANCELED);
					viewContext.setAttribute("pageMode", MODE_READONLY);
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments(requestId);
					saveComments(requestId);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
				}
			} catch (Exception e) {
				logger.LogException("Exception in saving system comments...", e);
			}
		}
		else
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.PAY_FIRST));
		return "REQ_CANCEL";
	}
	@SuppressWarnings("unchecked")
    public void	onSendForCollectPayment()
	{
		    String methodName="onSendForCollectPayment|";
		    logger.logInfo(methodName+"Start");
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = new ContractView();
			RequestService rsa = new RequestService();
			if(viewMap.containsKey("CONRTRACT_VIEW_FOR_COMPLETE_TASK"))
			  contractView  =(ContractView)viewMap.get("CONRTRACT_VIEW_FOR_COMPLETE_TASK");                                   
			Long requestId = requestView.getRequestId();
			try 
			{
			    rsa.setRequestAsAcceptedSettled(requestId, getLoggedInUser());
			    completeTask(TaskOutcome.OK, MessageConstants.CancelContract.SENT_FOR_COLLECT_PAYMENT);
			    if(requestId!=null)
				{
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_SENT_FOR_COLLECT_PAYMENT);
					viewContext.setAttribute("pageMode", MODE_READONLY);
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments(requestId);
					saveComments(requestId);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
				}
				logger.logInfo(methodName+"Finish");
			}
			catch (Exception e) 
			{
				logger.LogException(methodName+"Exception in saving system comments...", e);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
	@SuppressWarnings("unchecked")
    public void	onFollowUpDone()
	{
		    String methodName="onFollowUpDone|";
		    logger.logInfo(methodName+"Start");
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = new ContractView();
			RequestService  rs = new RequestService();
			if(viewMap.containsKey("CONRTRACT_VIEW_FOR_COMPLETE_TASK"))
			  contractView  =(ContractView)viewMap.get("CONRTRACT_VIEW_FOR_COMPLETE_TASK");                                   
			Long requestId = requestView.getRequestId();
			try 
			{
			    rs.setRequestAsAcceptedReviewed(requestId, getLoggedInUser());
			    completeTask(TaskOutcome.CLOSE, MessageConstants.CancelContract.LEGAL_DEPT_REVIEW_DONE);
			    if(requestId!=null)
				{
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_LEGAL_REVIEW_DONE);
					viewContext.setAttribute("pageMode", MODE_READONLY);
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments(requestId);
					saveComments(requestId);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
				}
				logger.logInfo(methodName+"Finish");
			}
			catch (Exception e) 
			{
				logger.LogException(methodName+"Exception in saving system comments...", e);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
	@SuppressWarnings("unchecked")
    public void	onSendForFollowUp()
	{
		    String methodName="onSendForFollowUp|";
		    logger.logInfo(methodName+"Start");
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = new ContractView();
			RequestServiceAgent rsa = new RequestServiceAgent();
			if(viewMap.containsKey("CONRTRACT_VIEW_FOR_COMPLETE_TASK"))
			  contractView  =(ContractView)viewMap.get("CONRTRACT_VIEW_FOR_COMPLETE_TASK");                                   
			Long requestId = requestView.getRequestId();
			try 
			{
			    rsa.setRequestAsOnFollowUp(requestId, getLoggedInUser());
			    completeTask(TaskOutcome.LEGAL_DEPT_REVIEW, MessageConstants.CancelContract.SENT_FOR_FOLLOW_UP);
			    if(requestId!=null)
				{
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_SENT_FOR_FOLLOWUP);
					viewContext.setAttribute("pageMode", MODE_READONLY);
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments(requestId);
					saveComments(requestId);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
				}
				logger.logInfo(methodName+"Finish");
			}
			catch (Exception e) 
			{
				logger.LogException(methodName+"Exception in saving system comments...", e);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
	@SuppressWarnings("unchecked")	
	public String onCompleteRequest()
	{
		logger.logInfo("onCompleteRequest|Start");
		
		boolean hasNoPendingPayments = true; //+ve thinking
		
		DomainDataView ddvPending = CommonUtil.getIdFromType(
                CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS ), 
                WebConstants.PAYMENT_SCHEDULE_PENDING
               );
		DomainDataView ddvExempted = CommonUtil.getIdFromType(
                CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS ), 
                WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED
               );
		
		List<PaymentBean> payments = getPayments();
		List<PaymentScheduleView> exemptedPayments = new ArrayList<PaymentScheduleView>();
		for (PaymentBean paymentBean : payments) 
		{
			if (paymentBean.getPaymentScheduleView().getStatusId().compareTo( ddvPending.getDomainDataId() ) == 0 ) 
			{
				hasNoPendingPayments = false;
				break;
			}
			if (paymentBean.getPaymentScheduleView().getStatusId().compareTo( ddvExempted.getDomainDataId() ) == 0 ) 
			{
				exemptedPayments.add(paymentBean.getPaymentScheduleView());
			}
		}
		
		if(viewMap.containsKey("paymentDoneSuccessfully") && hasNoPendingPayments)
		{
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = new ContractView();
			PropertyServiceAgent psa = new PropertyServiceAgent();
			if(viewMap.containsKey("CONRTRACT_VIEW_FOR_COMPLETE_TASK"))
			  contractView  =(ContractView)viewMap.get("CONRTRACT_VIEW_FOR_COMPLETE_TASK");                                   
			Long requestId = requestView.getRequestId();
			try 
			{

			    psa.completeTerminateContract(requestId,contractView,getLoggedInUser(),exemptedPayments);
			    completeTask(TaskOutcome.COMPLETE, MessageConstants.CommonsMessages.REQUEST_COMPLETE_SUCCESS);
			    if(requestId!=null)
				{
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_COMPLETED);
					viewContext.setAttribute("pageMode", MODE_READONLY);
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments(requestId);
					saveComments(requestId);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
					notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_TerminateContract_Request_Completed);
				}
				logger.logInfo("onCompleteRequest|Finish");
			}
			catch (Exception e) 
			{
				logger.LogException("onCompleteRequest|Exception in saving system comments...", e);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				return "";
			}
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.PAY_FIRST));
		}
		return "COMPLETE_REQ";
	}
  public void onPrintCL()
  {
	  String methodName="onPrintCL";
	  logger.logInfo(methodName+"|Start..");
	  errorMessages = new ArrayList<String>(0);
      try
      {
		ClearenceLetterCriteria clearenceLetterCriteria=new ClearenceLetterCriteria(ReportConstant.Report.CLEARANCE_LETTER_REPORT, ReportConstant.Processor.CLEARANCE_LETTER_REPORT,CommonUtil.getLoggedInUser());
  		clearenceLetterCriteria.setRequestId(viewMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString());
  		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
  		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, clearenceLetterCriteria);
  		String javaScript = "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "')";
  		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript);
		saveSystemComments(new Long(viewMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString()),
				           MessageConstants.RequestEvents.REQUEST_CL_ISSUED);
		new UtilityServiceAgent().printLetterforContract(new Long(this.getContractView().getContractId()),getLoggedInUser(),WebConstants.LetterTypes.CLEARENCE_LETTER);
		logger.logInfo(methodName+"|Finish..");
		printCL.setRendered(false);
      }
      catch (Exception e) 
      {
		logger.LogException(methodName+"|Exception...", e);
		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	   }
	  
  }
	
	@SuppressWarnings("unchecked")
	public String onSubmitRequest()
	{

		try
		{
			Boolean inValid = false;
			WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
			User user = webContext.getAttribute(CoreConstants.CurrentUser);
			String loginId = (user == null) ? User.UNKNOWN_USER : user.getLoginId();
			ApplicationContext applicationContext = ApplicationContext.getContext();
			
			Long requestNo = 0L;
			
			if (viewContext.getAttribute("pageMode") != null)
				pageMode = viewContext.getAttribute("pageMode");
			
			RequestView requestView = viewContext.getAttribute("requestView");
			if (requestView != null)
				requestNo = requestView.getRequestId();
			
			if (pageMode.equalsIgnoreCase(MODE_EVC_REQ)) 
			{
				//requestView = submitEvacReq(loginId);
				if (requestView != null && validateForSubmitRequest())
				{
					
					requestNo = requestView.getRequestId();
					viewContext.setAttribute("requestView", requestView);
					requestNo = updateEvacReq(loginId, Constant.REQUEST_STATUS_APPROVAL_REQUIRED, requestView.getRequestId());
					SystemParameters parameters = SystemParameters.getInstance();
	                String endPoint= parameters.getParameter(WebConstants.CANCEL_CONTRACT_ENDPOINT);
					PIMSTerminateContractBPELPortClient client = new PIMSTerminateContractBPELPortClient();
					client.setEndpoint(endPoint);
					client.initiate(requestView.getRequestId(), getContractView().getContractId(), loginId, null, null);
					notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_TerminateContract_Request_Received);
					viewMap.put("selectedTab", TAB_APPLICATION);
					Long requestId = requestView.getRequestId();
					saveSystemComments(requestId, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);
					this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestforward"));
					CommonUtil.printReport(requestId);
				}
				else
					inValid = true;
					
	//			completeTask(loginId);
			} 
			else if (pageMode.equalsIgnoreCase(MODE_NOL_REQ)) 
			{
				
				if(validateForPrintNOLRequest())
				{
			//	requestNo = updateEvacReq(loginId, Constant.CANCELCNTRACT_NOL_ISSUED, requestView.getRequestId());
					if(completeTask(loginId))
						if(requestNo!=null)
							saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_NOL_PRINTED);
				}
				else
					inValid = true;
			}
			
//			else if (pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ))
//			{
//			  if(validateApprovedRequest())
//			  {
//				  try
//				  {
//					 applicationContext.getTxnContext().beginTransaction();
//					 ContractView contractView = getContractView();
//					 evacuationService.updateSettlementDate(this.getEvacDate(), contractView.getContractId(), CommonUtil.getLoggedInUser(),getRequestId());
//					 new RequestServiceAgent().updateRequestEvacuation(this.getTxttotalEvacFines(), this.getExempted(), CommonUtil.getLoggedInUser(), getRequestId());
//					 new PropertyServiceAgent().changeUnitStatusToVacantForTerminateContract(requestNo, contractView.getContractId(), CommonUtil.getLoggedInUser());
//					 if(completeTask(CommonUtil.getLoggedInUser(),TaskOutcome.APPROVE) && requestNo!=null)
//					 {
//						   saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_APPROVED);
//						   requestNo = updateEvacReq(loginId, Constant.REQUEST_STATUS_APPROVED, requestView.getRequestId());
//					 }
//					 applicationContext.getTxnContext().commit();
//				  
//				  }
//				  catch( Exception e )
//				  {
//					  applicationContext.getTxnContext().rollback();
//					  logger.LogException("onSubmitRequest() crashed ", e);
//						errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//					  
//				  }
//				  finally
//				  {
//					  applicationContext.getTxnContext().release();
//				  }
//			  
//			  
//			  
//			  
//			  
//			  
//			  
//			  }
//			  else
//				  inValid = true;
//			}
			else if (pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ))
			{
				
				onSaveInspection();
				if(completeTask(CommonUtil.getLoggedInUser()) && requestNo!=null)
				{					
					 	saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_SITE_VISIT_DONE);
					    requestNo = updateEvacReq(loginId, Constant.REQUEST_STATUS_APPROVAL_REQUIRED, requestView.getRequestId());
				}
			}
	
			
			
			if (!inValid && requestNo != null && requestNo > 0)
			{
				viewContext.setAttribute("pageMode", MODE_READONLY);
				populateApplicationDataTab(requestNo.toString());
				loadAttachmentsAndComments(requestNo.toString());
				saveAttachments(requestNo);
				saveComments(requestNo);
				viewMap.put("applicationDetailsReadonlyMode", true);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				if(tabPanel.getSelectedTab().equals(TAB_REQUEST_HISTORY))
					tabRequestHistory_Click();
				
			}
		}
		catch (Exception exception) 
		{
			logger.LogException("onSubmitRequest() crashed ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "SUBMIT_REQ";
	}
	
	@SuppressWarnings("unchecked")
	public void onTerminateInEjari()
	{	
		errorMessages = new ArrayList<String>(0);
		try
		{
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = getContractView();
			if(contractView.getEjariNumber() != null && contractView.getEjariNumber().trim().length()>0)
			{
				EjariResponse response= EjariIntegration.terminateContractInEjariResponse(
												   												contractView.getContractId(),
												   												requestView.getRequestNumber(),
												   												getLoggedInUser()
												   										 );
				if(response.getStatus().equals("0"))
				{
					errorMessages.add( response.getMsg() );
				}
				else
				{
				   saveSystemComments(requestView.getRequestId(), "request.event.contractTerminatedInEjari");
				}
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("onTerminateInEjari| Exception Occured::", ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public String onApproveRequest()
	{
		  Long requestNo = 0L; 
		 if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
		
		 RequestView requestView = viewContext.getAttribute("requestView");
		 try
		 {
			if (requestView != null)
				requestNo = requestView.getRequestId();
			
			  if(!validateApprovedRequest())
			  {
				  return "";
			  }
			  
			 doApporve(requestNo, requestView);
			 if (requestNo != null && requestNo > 0)
				{
					viewContext.setAttribute("pageMode", MODE_READONLY);
					populateApplicationDataTab(requestNo.toString());
					loadAttachmentsAndComments(requestNo.toString());
					saveAttachments(requestNo);
					saveComments(requestNo);
					viewMap.put("applicationDetailsReadonlyMode", true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote", false);
					if(tabPanel.getSelectedTab().equals(TAB_REQUEST_HISTORY))
						tabRequestHistory_Click();
					
				}
		 }
		 catch(Exception e)
		 {
		   logger.LogException("onApproveRequest() crashed ", e);
		   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		  return "";
	}
	@SuppressWarnings( "unchecked" )
	private void doApporve(Long requestNo, RequestView requestView)throws Exception 
	{
		
		 WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
		 User user = webContext.getAttribute(CoreConstants.CurrentUser);
		 String loginId = (user == null) ? User.UNKNOWN_USER : user.getLoginId();
		 ApplicationContext applicationContext = ApplicationContext.getContext();
		
		try
		 {		  
			 applicationContext.getTxnContext().beginTransaction();
			 ContractView contractView = getContractView();
			 evacuationService.updateSettlementDate(this.getEvacDate(), contractView.getContractId(), CommonUtil.getLoggedInUser(),getRequestId());
			 new RequestServiceAgent().updateRequestEvacuation(this.getTxttotalEvacFines(), this.getExempted(), CommonUtil.getLoggedInUser(), getRequestId());
			 new PropertyServiceAgent().changeUnitStatusToVacantForTerminateContract(requestNo, contractView.getContractId(), CommonUtil.getLoggedInUser());
			 if(completeTask(CommonUtil.getLoggedInUser(),TaskOutcome.APPROVE) && requestNo!=null)
			 {
				   
				   requestNo = updateEvacReq(loginId, Constant.REQUEST_STATUS_APPROVED, requestView.getRequestId());
				   saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_APPROVED);
			 }
			 applicationContext.getTxnContext().commit();
			  
		  }
		  catch( Exception e )
		  {
			  applicationContext.getTxnContext().rollback();
			  throw e;
			  
		  }
		  finally
		  {
			  applicationContext.getTxnContext().release();
		  }
	}
	
	public String onRejectRequest()
	{
	
		Long requestNo = 0L;
		
		if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
	try{	
		
		RequestView requestView = viewContext.getAttribute("requestView");
		requestNo = updateEvacReq(CommonUtil.getLoggedInUser(), Constant.REQUEST_STATUS_REJECTED, requestView.getRequestId());
		if (requestView != null)
			requestNo = requestView.getRequestId();
		if (pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ))
		{
			 if(completeTask(CommonUtil.getLoggedInUser(),TaskOutcome.REJECT))
				if(requestNo!=null)
					saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_REJECTED);
		}
		
		if ( requestNo != null && requestNo > 0)
		{
			viewContext.setAttribute("pageMode", MODE_READONLY);
			populateApplicationDataTab(requestNo.toString());
			loadAttachmentsAndComments(requestNo.toString());
			saveAttachments(requestNo);
			saveComments(requestNo);
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("canAddAttachment",false);
			viewMap.put("canAddNote", false);
			if(tabPanel.getSelectedTab().equals(TAB_REQUEST_HISTORY))
				tabRequestHistory_Click();
			
		}
		
	 }catch (Exception exception) 
	  {
		logger.LogException("onRejectRequest() crashed ", exception);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }
		
		return "";
	}
	
	public String onSendForInspection()
	{
	
		Long requestNo = 0L;
		
		if (viewContext.getAttribute("pageMode") != null)
			pageMode = viewContext.getAttribute("pageMode");
	try{	
		
		RequestView requestView = viewContext.getAttribute("requestView");
		
	   if (requestView != null)
			requestNo = requestView.getRequestId();
	   if (pageMode.equalsIgnoreCase(MODE_APPROVAL_REQ) && requestNo!=0L)
		{
		     if(completeTask(CommonUtil.getLoggedInUser(),TaskOutcome.SEND_FOR_SITE_VISIT))
		     {
			 saveSystemComments(requestNo, MessageConstants.RequestEvents.REQUEST_ON_SITE_VISIT);
			 requestNo = updateEvacReq(CommonUtil.getLoggedInUser(), Constant.REQUEST_STATUS_ON_SITE_VISIT, requestNo);
		     }
		}
		
		if ( requestNo != null && requestNo > 0)
		{
			viewContext.setAttribute("pageMode", MODE_READONLY);
			populateApplicationDataTab(requestNo.toString());
			loadAttachmentsAndComments(requestNo.toString());
			saveAttachments(requestNo);
			saveComments(requestNo);
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("canAddAttachment",false);
			viewMap.put("canAddNote", false);
			if(tabPanel.getSelectedTab().equals(TAB_REQUEST_HISTORY))
				tabRequestHistory_Click();
			
		}
		
	 }catch (Exception exception) 
	  {
		logger.LogException("onSendForInspection() crashed ", exception);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }
		
		return "";
	}
	
	
	public String onSaveRequest()
	{
		String methodName = "onSaveRequest|";
		try
	    {
			logger.logInfo( methodName +"Start" );
			RequestView requestView = new RequestView();
			Long requestNo = 0L;
			WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
			User user = webContext.getAttribute(CoreConstants.CurrentUser);
			String loginId = (user == null) ? User.UNKNOWN_USER : user.getLoginId();
	    	if ( validateForSubmitRequest() )
			{
				if ( !viewMap.containsKey("REQUEST_VIEW") && pageMode.equalsIgnoreCase( MODE_EVC_NEW ) ) 
				{
					requestView = submitEvacReq(loginId);
					viewContext.setAttribute("requestView",requestView);
					this.successMessages.add(java.text.MessageFormat.format(
							               ResourceUtil.getInstance().getProperty("cancelContract.message.requestCreated"),requestView.getRequestNumber()));
					saveSystemComments(requestView.getRequestId()  , MessageConstants.RequestEvents.REQUEST_CREATED);
				}
				else if(viewMap.containsKey("REQUEST_VIEW"))
				{
					requestView = (RequestView)viewMap.get("REQUEST_VIEW");
					requestNo = updateRequest(loginId, Constant.REQUEST_STATUS_NEW, requestView.getRequestId());
					ContractView contractView = getContractView();
					evacuationService.updateSettlementDate(this.getEvacDate(), contractView.getContractId(), CommonUtil.getLoggedInUser(),requestView.getRequestId());
					this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestUpdated"));
					saveSystemComments(requestView.getRequestId()  , MessageConstants.RequestEvents.REQUEST_UPDATED);
					viewContext.setAttribute("pageMode", MODE_INITIATE);
			     }
 				if(requestView!=null && requestView.getRequestId()!=null)
				{
					Long requestId = requestView.getRequestId();
					populateApplicationDataTab(requestId.toString());
					loadAttachmentsAndComments(requestId.toString());
					saveAttachments( requestId );
					saveComments( requestId );
					
				}

			}
			logger.logInfo( methodName +"Finish" );
	    }
	    catch ( Exception e  )
	    {
	    	errorMessages =  new ArrayList<String>(0);
			logger.LogException(methodName+"|Error Occured...",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    	
	    }
		 
		return "SAVE_REQ";
	}

	private boolean completeTask(String loginId){
		boolean success = false;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			UserTask userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			bpmWorkListClient.completeTask(userTask, loginId, TaskOutcome.OK);
			this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestforward"));
			success = true;
		} catch (Throwable t) {
			logger.LogException("Exception while completing task from BPEL.", t);
		}
		return success;
	}
	private boolean completeTask(String loginId,TaskOutcome taskOutcome){
		boolean success = false;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient;
		try {
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
			//UserTask userTask = (UserTask) session.getAttribute(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			UserTask userTask = (UserTask)viewMap.get(WebConstants.CANCEL_CONTRACT_TASK_LIST);
			bpmWorkListClient.completeTask(userTask, loginId, taskOutcome);
			this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestforward"));
			success = true;
		} catch (Throwable t) {
			logger.LogException("Exception while completing task from BPEL.", t);
		}
		return success;
	}
	@SuppressWarnings("unchecked")
	private RequestView submitEvacReq(String loginId) {
		RequestView request = null;
		try {
				this.contractView = getContractView();
				String requestDescription = "";
				HashMap<String, Object> argMap = new HashMap<String, Object>(0);
				
				if(viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
				{
					requestDescription = (String)viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
					argMap.put("requestDescription", requestDescription);
				}
				if(viewMap.containsKey(WebConstants.LOCAL_PERSON_ID))
				{
					String applicantId = (String)viewMap.get(WebConstants.LOCAL_PERSON_ID);
					argMap.put("applicantId", applicantId);
				}
				if(getExempted()!=null)
					argMap.put("exempted", getExempted());
				request = evacuationService.addEvacuationRequest(loginId, contractView.getContractId(), getEvacDate(), argMap);
				
				if(request!=null && request.getRequestId()!=null)
				{
					Long requestId = request.getRequestId();
					populateApplicationDataTab(requestId.toString());
					loadAttachmentsAndComments(requestId.toString());
				}
				
				this.pageMode = MODE_EVC_REQ;
				//this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestCreated",(Object) request.getRequestNumber()));
			
		} catch (Throwable e) 
		{
			if (getContractView().getContractId() != null) 
			{
				logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(%s,%s)", e, loginId, getContractView().getContractId());
				throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(%s,%s)", e, loginId, getContractView().getContractId());
			}
			logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(%s,%s)", e, loginId, "null");
			throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(%s,%s)", e, loginId, "null");
		}
		viewContext.setAttribute("pageMode", MODE_INITIATE);
		return request;
	}
	@SuppressWarnings("unchecked")
	private boolean validateForSubmitRequest() throws Exception
	{
		if(viewMap.get(WebConstants.LOCAL_PERSON_ID)==null)
		{
			this.errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.MSG_APPLICANT_NOT_SELECTED));
			viewMap.put("selectedTab", TAB_APPLICATION);
			return false;
		}
		if (  this.getEvacDate()!= null )
		{
				
			DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
			Date startDate = df.parse ( df.format( getContractView().getStartDate() ) );
			Date evacDate = df.parse ( df.format( getEvacDate()  ) );
			if (  startDate.compareTo( evacDate )> 0 ) 
			{
				this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.evacmessage"));
				viewMap.put("selectedTab", TAB_CONTRACT_DETAILS);
				return false;
			}
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	private boolean validateApprovedRequest() throws Exception 
	{
		
		//if (this.getEvacDate() == null || getContractView().getStartDate().after(getEvacDate()) || getContractView().getEndDate().before(getEvacDate())) 
		if (this.getEvacDate() == null ) 
		{
			this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.evacDate"));
			viewMap.put("selectedTab", TAB_CONTRACT_DETAILS);
			return false;
		
		
		}
		else if (  this.getEvacDate()!= null )
		{
				
			DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
			Date startDate = df.parse ( df.format( getContractView().getStartDate() ) );
			Date evacDate = df.parse ( df.format( getEvacDate()  ) );
			if (  startDate.compareTo( evacDate ) >0 ) 
			{
				this.errorMessages.add(ResourceUtil.getInstance().getProperty("settlement.message.evacmessage"));
				viewMap.put("selectedTab", TAB_CONTRACT_DETAILS);
				return false;
			}
		}
		return true;
	}

	private boolean validateForPrintNOLRequest() {
		if(viewMap.get("nolPrinted")==null){
			this.errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CancelContract.NOL_NOT_PRINTED));
			return false;
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	private Long updateEvacReq(String loginId, String reqStatus, Long requestId) 
	{
		Long requestNo = 0L;
		try {
			if (getContractView().getContractId() != null) 
			{
				requestNo = evacuationService.updateEvacuationRequest(loginId, reqStatus, null, requestId);
				this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestupdated",(Object) requestNo));
			}
			else 
			{
				logger.logError("Error occoured the ContractView is null while adding Evacuation Request.");
			}
		} 
		catch (PimsBusinessException e) 
		{
			if (getContractView().getContractId() != null) 
			{
				logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, getContractView().getContractId(), reqStatus);
				throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, getContractView().getContractId(), reqStatus);
			}
			logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
			throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
		}
		viewContext.setAttribute("pageMode", MODE_DEFAULT);
		return requestNo;
	}
	
	@SuppressWarnings("unchecked")
	private Long updateRequest(String loginId, String reqStatus, Long requestId) 
	{
		Long requestNo = 0L;
		try {
			if (getContractView().getContractId() != null) {
				requestNo = evacuationService.updateEvacuationRequest(loginId, reqStatus, null, requestId);
				this.successMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.message.requestCreated",(Object) requestNo));
			} else {
				logger.logError("Error occoured the ContractView is null while adding Evacuation Request.");
			}
		} catch (PimsBusinessException e) {
			if (getContractView().getContractId() != null) {
				logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, getContractView().getContractId(), reqStatus);
				throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, getContractView().getContractId(), reqStatus);
			}
			logger.LogException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
			throw new CoreException("Exception while calling the EvacuationService.addEvacuationRequest(loginId:%s,contractId:%s,reqStatus:%s)", e, loginId, "null", reqStatus);
		}
		viewContext.setAttribute("pageMode", MODE_DEFAULT);
		return requestNo;
	}

	@SuppressWarnings("unchecked")
	public String onClose()
	{
		
		if(viewMap.get(BACK_PAGE)==null || viewMap.get(BACK_PAGE).toString().equals(BACK_PAGE_CONTRACT_SEARCH))
			sessionMap.put("preserveSearchCriteria", true);
		
		return viewMap.get(BACK_PAGE)==null?BACK_PAGE_CONTRACT_SEARCH:viewMap.get(BACK_PAGE).toString();
	}

	@SuppressWarnings("unchecked")
	public String onNOL()
	{
		viewMap.put("nolPrinted", true);
		successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CancelContract.NOL_PRINTED));
		return "NOL";
	}
	
	public void onSaveInspection()
	{
		logger.logDebug("CancelContractBean|Inspection|onSaveViolation called");
		List<ViolationBean> violationList = viewContext.getAttribute(BEAN_VIOLATION_LIST);
		if (pageMode.equalsIgnoreCase(MODE_READONLY)) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("cancelcontract.error.readonlymode"));
			return;
		}
		List<ViolationView> violations = new ArrayList<ViolationView>();
		for (ViolationBean v : violationList) 
		{
			ViolationView vbean = new ViolationView();
			vbean.setViolationDate(v.getDate());
			vbean.setCategoryId(Long.parseLong(v.getCategory()));
			vbean.setTypeId(Long.parseLong(v.getType()));
			vbean.setDamageAmount(Double.parseDouble(v.getDamageAmount()));
			vbean.setDescription(v.getDescription());
			violations.add(vbean);
		}
		WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = (user == null) ? User.UNKNOWN_USER : user.getLoginId();
		try
		{
			RequestView requestView = viewContext.getAttribute("requestView");
			InspectionView inspectionView = viewContext.getAttribute("inspectionView");
			inspectionView = evacuationService.saveViolationList(inspectionView, violations, getContractView(), requestView.getRequestId(), loginId);
			viewContext.setAttribute("inspectionView", inspectionView);
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("Error:CancelContractBean.onSaveInspection()", e);
		}
	}
	
	public void onAddInspection(ActionEvent event){
		logger.logDebug("CancelContractBean|Inspection|onAddViolation called");
		List<ViolationBean> violationList = viewContext.getAttribute(BEAN_VIOLATION_LIST);
		if (violationList == null)
			violationList = new ArrayList<ViolationBean>();
		ViolationBean bean = getViolationBean();
		if (pageMode.equalsIgnoreCase(MODE_READONLY)) {
			errorMessages.add(ResourceUtil.getInstance().getProperty("cancelcontract.error.readonlymode"));
			return;
		}
		if(!validateViolationBean(bean))
			return;
		if (bean != null) 
		{
			if (!violationList.isEmpty())
				bean.setId(String.valueOf(Integer.parseInt(String.valueOf(violationList.get(violationList.size() - 1).getId())) + 1));
			else
				bean.setId("0");
			List<SelectItem> typeList = (List<SelectItem>) viewMap.get(VIOLATION_TYPE_LIST);
			for (SelectItem item:typeList){
				if (item.getValue().toString().equalsIgnoreCase(bean.getType())){
					bean.setTypeDesc(item.getLabel());
					break;
				}
			}
			typeList = (List<SelectItem>) getValue("#{pages$ApplicationBean.violationCategoryList}");
			for (SelectItem item:typeList){
				if (item.getValue().toString().equalsIgnoreCase(bean.getCategory())){
					bean.setCategoryDesc(item.getLabel());
					break;
				}
				List<DomainDataView> ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
				DomainDataView ddv= CommonUtil.getIdFromType(ddvList, WebConstants.VIOLATION_STATUS_REPORTED);
				bean.setStatus(getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
                
				
			}
			
//			bean.setDescription(CommonUtil.getBundleMessage(WebConstants.CancelContract.PAYMENT_SCHEDULE_DESC));
			violationList.add(bean);
			viewContext.setAttribute(BEAN_VIOLATION_LIST, violationList);
			viewContext.setAttribute(BEAN_VIOLATION, new ViolationBean());
		    viewMap.put(VIOLATION_TYPE_LIST, new ArrayList<SelectItem>(0));	
		}
		
	}
	
	private boolean validateViolationBean(ViolationBean bean) {
		if (StringHelper.isEmpty(bean.getCategory()) || StringHelper.isEmpty(bean.getType()) || StringHelper.isEmpty(bean.getDamageAmount()) || StringHelper.isEmpty(bean.getDateString())) {
			this.errorMessages.add(ResourceUtil.getInstance().getProperty("cancelContract.inspection.error"));
			return false;
		}
		return true;
	}

	public void onDeleteViolation(ActionEvent event) {
		logger.logDebug("CancelContractBean|Inspection|onDeleteViolation called");
		String id = (String) ((UIParameter) event.getComponent().getChildren().get(0)).getValue();
		List<ViolationBean> list = getViolationListBean();
		ViolationBean beanRemove = null;
		for (ViolationBean bean : list) {
			if (bean.getId().equalsIgnoreCase(id)) {
				beanRemove = bean;
				break;
			}
		}
		if (beanRemove != null) {
			list.remove(beanRemove);
			viewContext.setAttribute(BEAN_VIOLATION_LIST, list);
		}
	}
	
	public void onEditViolation(ActionEvent event) {
		logger.logDebug("CancelContractBean|Inspection|onEditViolation called");
		
		viewMap.put("editViolationRequest", true);
		
		
		String id = (String) ((UIParameter) event.getComponent().getChildren().get(0)).getValue();
		List<ViolationBean> list = getViolationListBean();
		
		loadViolationTypesForCategory();
		for (ViolationBean bean  :list) {
			if (bean!=null && bean.getId()!=null && bean.getId().equalsIgnoreCase(id)) {
				
				List<SelectItem> typeList = (List<SelectItem>) viewMap.get(VIOLATION_TYPE_LIST);
				for (SelectItem item:typeList){
					if (item.getValue().toString().equalsIgnoreCase(bean.getType())){
						bean.setTypeDesc(item.getLabel());
						break;
					}
				}
				typeList = (List<SelectItem>) getValue("#{pages$ApplicationBean.violationCategoryList}");
				for (SelectItem item:typeList){
					if (item.getValue().toString().equalsIgnoreCase(bean.getCategory())){
						bean.setCategoryDesc(item.getLabel());
						break;
					}
					 
					
				}
				
				List<DomainDataView> ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
				DomainDataView ddv= CommonUtil.getIdFromType(ddvList, WebConstants.VIOLATION_STATUS_REPORTED);
				bean.setStatus(getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
	           
				
				viewContext.setAttribute(BEAN_VIOLATION,bean);
				
				break;
			}
		}
		loadViolationTypesForCategory();
		
		
	}
	
	
	public boolean isEditViolationRequest() {
		boolean isEditViolationRequest = false;
		
		if (viewMap.containsKey("editViolationRequest")) {
			isEditViolationRequest = true;
		}
		
		return isEditViolationRequest;
	}
	
	public void onUpdateInspection(ActionEvent event){
		logger.logDebug("CancelContractBean|Inspection|onUpdateViolation called");
		String id = (String) ((UIParameter) event.getComponent().getChildren().get(0)).getValue();
		List<ViolationBean> list = getViolationListBean();
		
		ViolationBean violationBean = getViolationBean();
		
		int editIndex=-1;
		for (int index=0; list.size()>0; index++) {
			ViolationBean bean = list.get(index); 
			if (bean!=null && bean.getId()!=null && bean.getId().equalsIgnoreCase(id)) {
				editIndex = index;
				break;
			}
		}
		if (editIndex!=-1) {
			list.remove(editIndex);
			
			List<SelectItem> typeList = (List<SelectItem>) viewMap.get(VIOLATION_TYPE_LIST);
			for (SelectItem item:typeList){
				if (item.getValue().toString().equalsIgnoreCase(violationBean.getType())){
					violationBean.setTypeDesc(item.getLabel());
					break;
				}
			}
			typeList = (List<SelectItem>) getValue("#{pages$ApplicationBean.violationCategoryList}");
			for (SelectItem item:typeList){
				if (item.getValue().toString().equalsIgnoreCase(violationBean.getCategory())){
					violationBean.setCategoryDesc(item.getLabel());
					break;
				}
				
				
				
			}
			
			List<DomainDataView> ddvList= CommonUtil.getDomainDataListForDomainType(WebConstants.VIOLATION_STATUS);
			DomainDataView ddv= CommonUtil.getIdFromType(ddvList, WebConstants.VIOLATION_STATUS_REPORTED);
			violationBean.setStatus(getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
            
			
			list.add(violationBean);
			viewContext.setAttribute(BEAN_VIOLATION_LIST, list);
			viewMap.remove("editViolationRequest");
			viewContext.setAttribute(BEAN_VIOLATION, new ViolationBean());
		}
	}
	

	public void tabRequestHistory_Click() {
		try {
			logger.logInfo("tabRequestHistory_Click started...");
			
			RequestView requestView = viewContext.getAttribute("requestView");
			RequestHistoryController rhc = new RequestHistoryController();
			if(requestView != null && requestView.getRequestId() != null)
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, requestView.getRequestId().toString());
			
			logger.logInfo("tabRequestHistory_Click completed successfully...");
		}
		catch(Exception ex) {
			logger.LogException("tabRequestHistory_Click crashed...",ex);
		}
    }
	
	public Boolean saveSystemComments(Long requestId, String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	
	    	if(requestId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(notesOwner, sysNote, requestId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	
/*	private List<UnitTabBean> createUnitTabBean(ContractView contract) {

		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");

		List<UnitTabBean> retVal = new ArrayList<UnitTabBean>();
		
		for (ContractUnitView unitView : contract.getContractUnitView()) {
			setUnitId(unitView.getUnitView().getUnitId());
			setTxtunitRefNum(String.valueOf(unitView.getUnitView().getUnitNumber()));
			setTxtpropertyName(unitView.getUnitView().getPropertyCommercialName());
			setTxtUnitRentValue(String.valueOf(unitView.getUnitView().getRentValue()));
			setUnitUsage(english?unitView.getUnitView().getUsageTypeEn():unitView.getUnitView().getUsageTypeAr());
		}
		
		return retVal;
	}*/
	
	private void createBeanFromContract(ContractView contractView) {
		
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		boolean english = locale.getLanguageCode().equalsIgnoreCase("en");
		setContractView(contractView);
		setContractNoText(contractView.getContractNumber());
		setContractStartDateText(contractView.getStartDateString());
		setContractTypeText(english?contractView.getContractTypeEn():contractView.getContractTypeAr());
		setContractStatusText(english?contractView.getStatusEn():contractView.getStatusAr());
		setContractEndDateText(contractView.getEndDateString());
		setTotalContractValText(String.valueOf(contractView.getRentAmount()));
		setEvacDate(contractView.getSettlementDate());

		if(contractView.getTenantView() != null) {
			setHdnTenantId(contractView.getTenantView().getPersonId()!=null?contractView.getTenantView().getPersonId().toString():"");
		    String name = contractView.getTenantView().getFirstName()+contractView.getTenantView().getMiddleName()+contractView.getTenantView().getLastName();	
		    setTenantNameText(name.equals("")||name==null?contractView.getTenantView().getCompanyName():name);
		}
		if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0) { 
			List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
			contractUnitViewList.addAll(contractView.getContractUnitView());
			ContractUnitView contractUnitView = contractUnitViewList.get(0);

			setTxtUnitType(english ? contractUnitView.getUnitView().getUnitTypeEn():contractUnitView.getUnitView().getUnitTypeAr());
			setTxtpropertyName(contractUnitView.getUnitView().getPropertyCommercialName());
			setTxtunitRefNum(contractUnitView.getUnitView().getUnitNumber());
			setTxtUnitRentValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
			viewMap.put("propertyCategoryId", contractUnitView.getUnitView().getPropertyCategoryId());
		}	
		calculateSettlement();
	}

	public String getErrorMessages() {
		StringBuilder sb = new StringBuilder();
		for (String msg: this.errorMessages)
			sb.append(msg).append("\n");
		
		return sb.toString();
	}

	public String getSuccessMessages() {
		
		StringBuilder sb = new StringBuilder();
		for (String msg: this.successMessages)
			sb.append(msg).append("\n");
		
		return sb.toString();
	}

	public ContractView getContractView() {
		contractView = viewContext.getAttribute("contractView");
		if(contractView==null)
			contractView = new ContractView();
		return contractView;
	}

	@SuppressWarnings("unchecked")
	public void setSelectedTab(){
		
		if (pageMode.equalsIgnoreCase(MODE_EVC_REQ) || pageMode.equalsIgnoreCase(MODE_NOL_REQ))
			SelectedTab = "unitTab";
		else if (pageMode.equalsIgnoreCase(MODE_INSPECTION_REQ))
			SelectedTab = "inspectionTab";
		else if (pageMode.equalsIgnoreCase(MODE_SETTLE_PAYMENT_REQ) || pageMode.equalsIgnoreCase(MODE_PAYMENT_COLLECT_REQ))
			SelectedTab = "paymentsTab";
		else SelectedTab = TAB_APPLICATION;
		viewMap.put("selectedTab",SelectedTab);
	}
	public void tabFollowUp_Click()
	{
		String methodName ="tabFollowUp_Click|";
		try	
		{
			RequestView requestView = viewContext.getAttribute("requestView");
			CancelContractFollowUpTab obj = (CancelContractFollowUpTab)getBean("pages$cancelContractFollowUpTab");
			if(obj.getList() == null || obj.getList().size()==0 )
			  obj.getAllFollowUpsForRequest( requestView.getRequestId());
		}
		catch(Exception ex){
			logger.LogException(methodName + "Error Occured::",ex);
		}
		
	}
	public void tabRequiredDocuments_Click() {
		String methodName ="tabRequiredDocuments_Click";
		logger.logDebug(methodName + "|" + "Start");
		
		try	{
			UtilityServiceAgent usa=new UtilityServiceAgent();
			List<DomainDataView> ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.PROCEDURE_TYPE);
			DomainDataView ddv=CommonUtil.getIdFromType(ddvList, Constant.PROCEDURE_TYPE_EVACUATION);		
			List<RequiredDocumentsView> requiredDocumentsViewList= usa.getRequiredDocumentsByProcedureType(ddv.getDomainDataId());
			viewContext.setAttribute("SESSION_CHNG_TENANT_REQ_DOC", requiredDocumentsViewList);
		}
		catch(Exception ex){
			logger.LogException(methodName + "|" + "Error Occured::",ex);
		}
	}
	
	public String getSelectedTab() {
		return SelectedTab;
	}
	
	public void setSelectedTab(String selectedTab){
		this.SelectedTab = selectedTab;
	}
	
	public ViolationBean getViolationBean(){
		return viewContext.getAttribute(BEAN_VIOLATION);
	}
	
	public List<ViolationBean> getViolationListBean(){
		return viewContext.getAttribute(BEAN_VIOLATION_LIST);
	}
	
	public List<PaymentBean> getPayments(){
		return viewContext.getAttribute(BEAN_PAYMENTS);
	}
	
	public List<PaymentBean> getSettlement(){
		return viewContext.getAttribute(BEAN_SETTLEMENT);
	}
		
	private void checkViewRoot(){
		if (getFacesContext().getViewRoot() != null && viewContext == null) {
			viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
			ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
		}
	}

	public boolean getIsArabicLocale() {
		return !getIsEnglishLocale();
	}
	public boolean getIsPagePopUp() {
		
		if(viewMap.containsKey(MODE_POPUP))
		  return true;
		else
			return false;
	}   
	public boolean getIsEnglishLocale() {
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	public String getPageMode() {
		return pageMode;
	}
	
	public String btnTenant_Click(){
		String methodName = "btnTenant_Click";
		logger.logInfo(methodName + "|Start");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		logger.logDebug(methodName + "|Person Id:: " + getContractView().getTenantView().getPersonId());
		setRequestParam(WebConstants.PERSON_ID, getContractView().getTenantView().getPersonId());
		setRequestParam("viewMode", "popup");
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+getContractView().getTenantView().getPersonId()+");";

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(methodName + "|Finish");
		return "";

	}
    @SuppressWarnings("unchecked")
	public String btnContract_Click()
	{
		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		getFacesContext().getExternalContext().getSessionMap().put("contractId", getContractView().getContractId());
		getFacesContext().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		String javaScriptText="var screen_width = 1024;"+
        "var screen_height = 450;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=120,top=150,scrollbars=no,status=yes');";
		
		AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
		addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);        
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	private void setTenantFields(ContractView contractView) {
		String  METHOD_NAME = "setTenantFields|";		
        logger.logInfo(METHOD_NAME+" started...");
        PersonView tenantView = contractView.getTenantView();
		if(tenantView!=null){
			if(tenantView.getIsCompany()!=null){
				setTenantType(getTenantType(tenantView.getIsCompany().compareTo(new Long(1))==0));	
			}
			else
				setTenantType("");
			
			StringBuilder nameBuilder = new StringBuilder("");
			nameBuilder = nameBuilder.append(tenantView.getFirstName()!= null?tenantView.getFirstName():"");
			nameBuilder = nameBuilder.append(tenantView.getMiddleName()!= null?" "+tenantView.getMiddleName():"");
			nameBuilder = nameBuilder.append(tenantView.getLastName()!= null?" "+tenantView.getLastName():"");
			
			setTenantNameText(nameBuilder.toString());
		}
        logger.logInfo(METHOD_NAME+" ended...");
	}
	
	public String populateContract(){
		String  METHOD_NAME = "populateContract|";		
        logger.logInfo(METHOD_NAME+"started...");
        

        try{
				SystemParameters parameters = SystemParameters.getInstance();			
				String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
				try{				
					contractView = propertyServiceAgent.getContract(contractView.getContractNumber(),dateFormat);
					
				}
				catch (Exception e) {
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_NO_CONTRACT_FOUND));
					throw e;
				}
				if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
				{
					getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					//getFacesContext().getExternalContext().getSessionMap().put(WebConstants.LOCAL_PERSON_ID, contractView.getTenantView().getPersonId());
					
					//Setting the Contract/Tenant Info
					setTenantFields(contractView);
					setContractTypeText(contractView.getContractTypeEn());
					setContractStartDateText(CommonUtil.getStringFromDate(contractView.getStartDate()));
					setContractEndDateText(CommonUtil.getStringFromDate(contractView.getEndDate()));
					setContractNoText(contractView.getContractNumber());
					 // commented because their is a change in the tenant type 
					//tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
					if(getIsEnglishLocale())
						setContractStatusText(contractView.getStatusEn());
					else
						setContractStatusText(contractView.getStatusAr());
					setTotalContractValText(contractView.getRentAmount().toString());
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						setTxtUnitType(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
						setTxtpropertyName(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    setTxtpropertyType(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    setTxtunitRefNum(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getRentValue());
					    setTxtUnitRentValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					
					}

				}

			
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {			
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		return "";
	}
	@SuppressWarnings("unchecked")
	public void populateApplicationDataTab(String requestId)
	{
		
		RequestView rV= new RequestView();
		List<RequestView> rVList = new ArrayList<RequestView>();
		try 
		{
			rV.setRequestId(new Long(requestId));
			PropertyServiceAgent psa = new PropertyServiceAgent();
		    rVList = psa.getAllRequests(rV, null, null, null);
			rV = rVList.get(0);
			viewMap.put("REQUEST_VIEW", rV);
			if(rV.getRequestNumber()!=null && !rV.getRequestNumber().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, rV.getRequestNumber());
			if(rV.getStatusId().toString()!=null && !rV.getStatusId().toString().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?rV.getStatusEn():rV.getStatusAr());
			if(rV.getRequestDate()!=null)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, rV.getRequestDate());
			if(rV.getDescription()!=null && !rV.getDescription().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, rV.getDescription());
			if(rV.getRequestId()!=null && !rV.getRequestId().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,rV.getRequestId());
		
			if(rV.getApplicantView()!=null && !rV.getApplicantView().equals("")){
				
				viewMap.put("applicationDetailsReadonlyMode", true);
				viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
				
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals(""))
				{
			       viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,rV.getApplicantView().getPersonId());
			       hdnPersonId =   rV.getApplicantView().getPersonId().toString();
			       viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				}
	
			if(rV.getApplicantView().getFirstName()!=null && !rV.getApplicantView().getFirstName().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
						(rV.getApplicantView().getFirstName()!=null && rV.getApplicantView().getFirstName().length()>0? rV.getApplicantView().getFirstName():"")+" "+
	                    (rV.getApplicantView().getMiddleName()!=null && rV.getApplicantView().getMiddleName().length()>0?rV.getApplicantView().getMiddleName():"")+" "+
			            (rV.getApplicantView().getLastName()!=null && rV.getApplicantView().getLastName().length()>0?rV.getApplicantView().getLastName():""));
			
			if(rV.getApplicantView().getPersonFullName()!=null && !rV.getApplicantView().getPersonFullName().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,rV.getApplicantView().getPersonFullName());
			
			if(rV.getApplicantView().getPersonTypeId()!=null && !rV.getApplicantView().getPersonTypeId().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
						getIsEnglishLocale()?rV.getApplicantView().getPersonTypeEn():rV.getApplicantView().getPersonTypeAr());
			if(rV.getApplicantView().getCellNumber()!=null && !rV.getApplicantView().getCellNumber().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,rV.getApplicantView().getCellNumber());
			
			if(rV.getApplicantView().getIsCompany()!=null && !rV.getApplicantView().getIsCompany().equals("")){
				DomainDataView ddv = new DomainDataView();
				if(rV.getApplicantView().getIsCompany()==1L){
				        ddv= CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);}
				else{
				        ddv= CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);}
				
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
		
		}

		}

		/*public static final String APPLICATION_APPLICANT_TYPE = "APPLICATION_APPLICANT_TYPE";
		public static final String APPLICATION_APPLICANT_EMAIL = "APPLICATION_APPLICANT_EMAIL";*/
		
		
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logInfo(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	logger.logInfo(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return hMap;
		
	}

	 private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	    {
	    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
	    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
			Iterator<DomainTypeView> itr = list.iterator();
			//Iterates for all the domain Types
			while( itr.hasNext() ) 
			{
				DomainTypeView dtv = itr.next();
				if( dtv.getTypeName().compareTo(domainType)==0 ) 
				{
					Set<DomainDataView> dd = dtv.getDomainDatas();
					//Iterates over all the domain datas
					for (DomainDataView ddv : dd) 
					{
					  ddvList.add(ddv);	
					}
					break;
				}
			}
	    	
	    	return ddvList;
	    }
	 
	 public String getNumberFormat(){
			return commonUtil.getNumberFormat();
	    }

	public ServletContext getServletcontext() {
		return servletcontext;
	}

	public void setServletcontext(ServletContext servletcontext) {
		this.servletcontext = servletcontext;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public String getHdnContractId() {
		return hdnContractId;
	}

	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}

	public String getHdnContractNumber() {
		return hdnContractNumber;
	}

	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}

	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}


	public String getContractNoText() {
		String temp = (String)viewMap.get("contractNoText");
		return temp;
	}

	public void setContractNoText(String contractNoText) {
		if(contractNoText!=null)
			viewMap.put("contractNoText" , contractNoText);
	}

	public String getTenantNameText() {
		String temp = (String)viewMap.get("tenantNameText");
		return temp;
	}

	public void setTenantNameText(String tenantNameText) {
		if(tenantNameText!=null)
			viewMap.put("tenantNameText" , tenantNameText);
	}

	public String getContractTypeText() {
		String temp = (String)viewMap.get("contractTypeText");
		return temp;
	}

	public void setContractTypeText(String contractTypeText) {
		if(contractTypeText!=null)
			viewMap.put("contractTypeText" , contractTypeText);
	}

	public String getContractStartDateText() {
		String temp = (String)viewMap.get("contractStartDateText");
		return temp;
	}

	public void setContractStartDateText(String contractStartDateText) {
		if(contractStartDateText!=null)
			viewMap.put("contractStartDateText" , contractStartDateText);
	}

	public String getContractEndDateText() {
		String temp = (String)viewMap.get("contractEndDateText");
		return temp;
	}

	public void setContractEndDateText(String contractEndDateText) {
		if(contractEndDateText!=null)
			viewMap.put("contractEndDateText" , contractEndDateText);
	}

	public String getTenantType() {
		String temp = (String)viewMap.get("tenantType");
		return temp;
	}

	public void setTenantType(String tenantType) {
		if(tenantType!=null)
			viewMap.put("tenantType" , tenantType);
	}

	public String getContractStatusText() {
		String temp = (String)viewMap.get("contractStatusText");
		return temp;
	}

	public void setContractStatusText(String contractStatusText) {
		if(contractStatusText!=null)
			viewMap.put("contractStatusText" , contractStatusText);
	}

	public String getTotalContractValText() {
		String temp = (String)viewMap.get("totalContractValText");
		return temp;
	}

	public void setTotalContractValText(String totalContractValText) {
		if(totalContractValText!=null)
			viewMap.put("totalContractValText" , totalContractValText);
	}

	public String getFeeText() {
		String temp = (String)viewMap.get("feeText");
		return temp;
	}

	public void setFeeText(String feeText) {
		if(feeText!=null)
			viewMap.put("feeText" , feeText);
	}

	public String getTxtUnitType() {
		String temp = (String)viewMap.get("txtUnitType");
		return temp;
	}

	public void setTxtUnitType(String txtUnitType) {
		if(txtUnitType!=null)
			viewMap.put("txtUnitType" , txtUnitType);
	}

	public String getTxtpropertyName() {
		String temp = (String)viewMap.get("txtpropertyName");
		return temp;
	}

	public void setTxtpropertyName(String txtpropertyName) {
		if(txtpropertyName!=null)
			viewMap.put("txtpropertyName" , txtpropertyName);
	}

	public String getTxtunitRefNum() {
		String temp = (String)viewMap.get("txtunitRefNum");
		return temp;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		if(txtunitRefNum!=null)
			viewMap.put("txtunitRefNum" , txtunitRefNum);
	}

	public String getTxtUnitRentValue() {
		String temp = (String)viewMap.get("txtUnitRentValue");
		return temp;
	}

	public void setTxtUnitRentValue(String txtUnitRentValue) {
		if(txtUnitRentValue!=null)
			viewMap.put("txtUnitRentValue" , txtUnitRentValue);
	}

	public String getTxtpropertyType() {
		String temp = (String)viewMap.get("txtpropertyType");
		return temp;
	}

	public void setTxtpropertyType(String txtpropertyType) {
		if(txtpropertyType!=null)
			viewMap.put("txtpropertyType" , txtpropertyType);
	}
	public Double getTxttotalEvacFines() {
		Double temp = (Double)viewMap.get("txttotalEvacFines");
		return temp;
	}

	public void setTxttotalEvacFines(Double txttotalEvacFines) {
		if(txttotalEvacFines!=null)
			viewMap.put("txttotalEvacFines" , txttotalEvacFines);
	}
	
	public Long getExempted() 
	{
		Long temp = 0L;
		if( viewMap.get("isExempted")!=null )
		temp = (Long)viewMap.get("isExempted");
		
		return temp;
	}

	public void setExempted(Long isExempted) 
	{
		if(isExempted !=null)
			viewMap.put("isExempted" , isExempted);
	}


	public Date getEvacDate() {
		Date temp=null;
		if(viewMap.get("evacDate")!=null)
		  temp = (Date)viewMap.get("evacDate");
		return temp;
	}

	public void setEvacDate(Date evacDate) {
		if(evacDate!=null)
			viewMap.put("evacDate" , evacDate);
		else
			viewMap.remove("evacDate");	
	}
	
	public Long getUnitId() {
		Long temp = (Long)viewMap.get("unitId");
		return temp;
	}

	public void setUnitId(Long unitId) {
		if(unitId!=null)
			viewMap.put("unitId" , unitId);
	}

	public String getUnitUsage() {
		String temp = (String)viewMap.get("unitUsage");
		return temp;
	}

	public void setUnitUsage(String unitUsage) {
		if(unitUsage!=null)
			viewMap.put("unitUsage" , unitUsage);
	}
	
	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}	
    
    public void getUserTask(Long requestId) throws PimsBusinessException, IOException, PIMSWorkListException{
	    
    	
    	String taskType = "";
		//Long requestStatusId = reqView.getStatusId();
		RequestTasksView reqTaskView = new RequestServiceAgent().getIncompleteRequestTask(requestId);
		String taskId = reqTaskView.getTaskId();
		String user = getLoggedInUser();
		
		if(taskId!=null)
		{
   		BPMWorklistClient bpmWorkListClient = null;
        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		bpmWorkListClient = new BPMWorklistClient(contextPath);
		
		UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
		taskType = userTask.getTaskType();
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
		logger.logInfo("Task Type is: " + taskType);
		 
		viewMap.put(WebConstants.CANCEL_CONTRACT_TASK_LIST, userTask);
	
		}
    }

	public HtmlCommandButton getPrintCL() {
		return printCL;
	}

	public void setPrintCL(HtmlCommandButton printCL) {
		this.printCL = printCL;
	}
	
	
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		ContractView contView = null;
		PersonView applicant = null;
		PersonView tenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		
		RequestView requestView = viewContext.getAttribute("requestView");
		
		try
		{
		
		if(requestView != null)
		{
			contView = requestView.getContractView();
			if(contView != null)
			{
				
				applicant = requestView.getApplicantView();
				tenant = contView.getTenantView();
				
				String pattern = getDateFormat();
				DateFormat formatter = new SimpleDateFormat(pattern);
				String evacDate = formatter.format(this.getEvacDate());
				
				eventAttributesValueMap.put("REQUEST_NUMBER", requestView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", this.getContractNoText());
				eventAttributesValueMap.put("CONTRACT_TYPE", this.getContractTypeText());
				eventAttributesValueMap.put("CONTRACT_START_DATE", this.getContractStartDateText());
				eventAttributesValueMap.put("CONTRACT_END_DATE", this.getContractEndDateText());
				eventAttributesValueMap.put("EVACUATION_DATE", evacDate);
				eventAttributesValueMap.put("PROPERTY_NAME", this.getTxtpropertyName());
				eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(tenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				

				
			}
		}
		}
		catch (Exception e)
		{
			logger.logInfo("requestStatusNotification crashed...");
			e.printStackTrace();
		
		}
	}
		
	public void onEvacFinesExempted(ActionEvent event)
	{
      this.setExempted(1L);		
	}
	public void onEvacFinesApplied(ActionEvent event)
	{
      this.setExempted(0L);		
	}
	
	private void calculateSettlement() 
	{

		ContractView contractForm = getContractView();
		DateFormat df = new SimpleDateFormat(WebConstants.DATE_FORMAT);
		
		try 
		{
		  if(this.getEvacDate()!=null )
		  {
		    RequestView requestView =  new RequestServiceAgent().getRequestById(getRequestId());  
			ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
			Date startDate = df.parse( df.format(  contractForm.getStartDate() ) );
			Date endDate = this.getEvacDate();
			Date expDate = df.parse( df.format(contractForm.getEndDate()) );
			String daysInYear =	new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR);
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(startDate);
			calendar.setTime(endDate);
			calendar.setTime(expDate);
			int months =0;
			int days = 0;
	
			// Calculate Actual Rent Period( Months/Days) Start
			Calendar calendarCounter = GregorianCalendar.getInstance();
			calendarCounter.setTime( startDate );
			Calendar calendarSettle = GregorianCalendar.getInstance();
			calendarSettle.setTime( endDate );
			Integer[] months_days= new Integer[]{0,0};
			months_days = CommonUtil.calculateMonthsDaysBetween( startDate, endDate );
			months = months_days[0];
			days = months_days[1] ;
		     // Calculate Actual Rent Period( Months/Days) end
		    contractForm.setMonths( months );
			contractForm.setDays( days );
			 
			Double totalEvacFines = 0.0D;
			Date formattedStartDate = df.parse(df.format(startDate));
			Date formattedEvacDate = df.parse(df.format(endDate));
			Date formattedExpiryDate = df.parse(df.format(expDate));
			Integer[] contractDuration= new Integer[]{0,0};
			contractDuration = CommonUtil.calculateMonthsDaysBetween( formattedStartDate, formattedExpiryDate );
			Integer daysInContractDuration = ( (30 * contractDuration[0])+ contractDuration[1]);
			daysInYear = daysInContractDuration.toString();
			// if evac date is before expiry date then calculate fine
			if( formattedEvacDate.compareTo( formattedExpiryDate )< 0 )
			totalEvacFines += calculateFineForEvacDateBeforeExpiryDate( contractForm, appBean, daysInYear );

			// if request date is not before specified months than calculate fine
			else
			totalEvacFines += calculateFineForRequestNotBeforeSpecifedMonths( contractForm, appBean, expDate, daysInYear );
			
			
             if(requestView.getEvacuationFine()!=null && requestView.getIsExempted()!=null && viewMap.containsKey("FOR_FIRST_TIME") ) 
             {	 
            	 viewMap.remove("FOR_FIRST_TIME"); 
				this.setTxttotalEvacFines( requestView.getEvacuationFine() );
				
				if(requestView.getIsExempted().compareTo(1L)==0)
				   this.setExempted(1L);				
				else			
				   this.setExempted(0L);
             }	
             else
             {
			  this.setTxttotalEvacFines( (double) ( ( (long) (totalEvacFines * 100 ) ) / 100.00 ) );
             }
		  }	
		  else
			  this.setTxttotalEvacFines( 0.0D ); 

		} catch (Throwable t)
		{
			
			logger.LogException("Exception when calculating the Settlement Values for contractId:%s", t, contractForm.getContractId());
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private Double calculateFineForEvacDateBeforeExpiryDate(ContractView contractForm, ApplicationBean appBean, String daysInYear) throws ParseException
	{
		Double fine = 0.0D;
		Double paymentConfigurationAmount= 0.0D;

		
			DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
			DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
			if (contractForm.getContractTypeId().equals(comercialType.getDomainDataId()) )
			{
				PaymentConfigurationView commercial = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_COMMERCIAL);
				if (commercial!=null )
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
					if(commercial.getIsFixed().compareTo(0L)==0) 
					  paymentConfigurationAmount = commercial.getAmount();
				    else
					  return commercial.getAmount();
				}
			}
			else if (contractForm.getContractTypeId().equals(residentalType.getDomainDataId()) )
			{
				PaymentConfigurationView residental = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_RESIDENTIAL);
				if(residental!=null)
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
					if(residental.getIsFixed().compareTo(0L)==0) 
					   paymentConfigurationAmount = residental.getAmount();
				    else
				       return residental.getAmount();
						
				}
			}
			fine = Math.ceil ((30.0/new Double(daysInYear)) * paymentConfigurationAmount * contractForm.getRentAmount());
	    return fine;
	}
    @SuppressWarnings("unchecked")
	private Double calculateFineForRequestNotBeforeSpecifedMonths(ContractView contractForm, ApplicationBean appBean, Date expDate,String daysInYear) throws ParseException,Exception 
	{
		int months;
		Calendar calendarCounter;
		Integer[] months_days;
		Double totalEvacFines  = 0.0;
		Double totalFineMonths = 0.0D;
		LocaleInfo locale = webContext.getAttribute( CoreConstants.CurrentLocale );
		RequestView requestView = viewContext.getAttribute("requestView");
		if(requestView != null && requestView.getRequestId()!= null  )
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date formattedRequestDate = df.parse( df.format( requestView.getRequestDate() ) );
			Date formattedExpiryDate  = df.parse( df.format( expDate ) );
			//if request date is less then  contract expiry date then check if this request date is before specified months
			//other wise apply fines
		     if( formattedRequestDate.compareTo( formattedExpiryDate ) < 0 )
		     {
		    	calendarCounter = GregorianCalendar.getInstance();
				calendarCounter.setTime(formattedRequestDate);
				Calendar calendarExpiry = GregorianCalendar.getInstance();
				calendarExpiry.setTime(expDate);
				months_days= new Integer[]{0,0};
				months_days = CommonUtil.calculateMonthsDaysBetween(formattedRequestDate, formattedExpiryDate );
				months = months_days[0];
			    if(months>=2)
			    {
			    	totalEvacFines=0.0;
			    	return totalEvacFines;
			    }
		     }
			 
			 {
					DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
					DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
					if (contractForm.getContractTypeId().equals(comercialType.getDomainDataId()) )
					{
						
						PaymentConfigurationView commercial = getPaymentConfig(Constant.ANNULMENT_FINE_COMMERCIAL);
						if (commercial!=null)
						{
						 viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
						 totalFineMonths = commercial.getAmount();
						}
					}
					else if (contractForm.getContractTypeId().equals(residentalType.getDomainDataId()) )
					{
						PaymentConfigurationView residental = getPaymentConfig(Constant.ANNULMENT_FINE_RESIDENTIAL);
						if(residental!=null)
						{
						 viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
						 totalFineMonths = residental.getAmount();
						}
					}
					totalEvacFines = Math.ceil ((30.0/new Double(daysInYear)) * totalFineMonths * contractForm.getRentAmount());
					

			    	
			}
		  }
			
		
		
		
		return totalEvacFines;
	}
    
	private PaymentConfigurationView getPaymentConfig(String conditionKey){
		logger.logInfo("getPaymentConfig() started...");
		PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
		
		try
		{
			UtilityServiceAgent usa = new UtilityServiceAgent();
			//paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_FINE_ID);
			paymentConfigurationView.setConditionKey(conditionKey);
			paymentConfigurationView.setProcedureTypeKey(Constant.PROCEDURE_TYPE_EVACUATION);
			List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
			if(!paymentConfigList.isEmpty())
				paymentConfigurationView = paymentConfigList.get(0);
			else
				paymentConfigurationView = null;
				
			logger.logInfo("getPaymentConfig() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getPaymentConfig() crashed ", exception);
		}
		
		return paymentConfigurationView; 
	}
	
	public Long getRequestId(){
		if (viewContext!= null && viewContext.getAttribute("requestView") != null)
			return ((RequestView) viewContext.getAttribute("requestView")).getRequestId();
		else
			return 0L;
	}
	
	public void setRequestId(Long requestId)
	{
		
		
	}

	public void printReport()
	{
		String methodName = "printReport|";
		logger.logDebug( methodName + "Start");
		putScreenValuesInCriteriaObject();
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		logger.logDebug( methodName + "Finish");

		
	}
	
	private void putScreenValuesInCriteriaObject() 
	{
		String methodName = "putScreenValuesInCriteriaObject|";
		logger.logDebug( methodName + "Start" );
		reportCriteria.setRequestId( getRequestId() );
		logger.logDebug( methodName + "Finish" );
	}
	
	private void openPopup(String javaScriptText) 
	{
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
    public void onAddPaymentsSchedule()
    {
    	logger.logInfo("onAddPayments| Start...");
    	String psStatusInReq="";
    	try
    	{
			if(viewContext.getAttribute(BEAN_PAYMENTS)!=null)
			{
				getPaymentScheduleViewFromPaymentBean(((List<PaymentBean>) viewContext.getAttribute(BEAN_PAYMENTS)));
			}	
			setRequestParam(WebConstants.PAYMENT_STATUS_IN_REQUEST,WebConstants.PAYMENT_SCHEDULE_PENDING);
			psStatusInReq = WebConstants.PAYMENT_SCHEDULE_PENDING;
	        openPopUp("", "javaScript:openAddPaymentSchedulePopup('','"+psStatusInReq+"','"+viewMap.get("propertyCategoryId").toString() +"');");
    		logger.logInfo("onAddPayments|Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("onAddPayments|Error Occured::",ex);	
			errorMessages  = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    }

	@SuppressWarnings( "unchecked" )
	public void btnEditPaymentSchedule_Click()
	{
		
		try
		{
			logger.logInfo("btnEditPaymentSchedule_Click| Start...");
			PaymentScheduleView psv = (PaymentScheduleView)(((PaymentBean)dataTablePaymentSchedule.getRowData()).getPaymentScheduleView());
			if(viewContext.getAttribute(BEAN_PAYMENTS)!=null)
			{
				getPaymentScheduleViewFromPaymentBean(((List<PaymentBean>) viewContext.getAttribute(BEAN_PAYMENTS)));
			}	
				
			sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,true);
			sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
			sessionMap.put("FROM_CANCEL_CONTRACT_BEAN",true);
			//sessionMap.put("viewMode",true);
			//sessionMap.put("SAVE_EDITED_PS_DB",true);
			sessionMap.put("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION",true);
			openPopUp("", "javascript:openPopupEditPaymentSchedule();");
			logger.logInfo("btnEditPaymentSchedule_Click| Finish...");
		}
		catch( Exception e)
		{
			logger.LogException("btnEditPaymentSchedule_Click|Exception Occured::", e);
			errorMessages  = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		
		}
	}
	
	@SuppressWarnings( "unchecked" )
	
	private void openPopUp(String URLtoOpen,String extraJavaScript)throws Exception
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText=extraJavaScript;
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
	
	}
	
	@SuppressWarnings("unchecked")
	private void fromEditAddPaymentSchedule() throws Exception
	{
		if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
     		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
        )
        {
			getPaymentBeanFromPaymentScheduleView (
					                               (List<PaymentScheduleView>) 
					                                  sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)
					                              );
     	   
     	  
        }
	}
	@SuppressWarnings("unchecked")
	private void getPaymentScheduleViewFromPaymentBean(List<PaymentBean> paymentBeanList) 
	{
		LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
		List<PaymentScheduleView> paymentScList = new ArrayList<PaymentScheduleView>();
		List<PaymentScheduleView> payments = null;
		for (PaymentBean bean : paymentBeanList) 
		{
			paymentScList.add(bean.getPaymentScheduleView()) ;
		}
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScList);
	}
	@SuppressWarnings("unchecked")
	private void getPaymentBeanFromPaymentScheduleView(List<PaymentScheduleView> paymentSList )throws Exception 
	{
	
		boolean paymentDoneSuccessfully = true;
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		DomainDataView ddvPending = CommonUtil.getIdFromType(
				        CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_PENDING);
		
		if (paymentSList != null) 
		{
			Map<Long,PaymentBean> exemptedBeanMap = new HashMap<Long,PaymentBean>();
			RequestView requestView = viewContext.getAttribute("requestView");
			ContractView contractView = getContract();
			for (PaymentScheduleView view : paymentSList) 
			{
				PaymentBean pb = new PaymentBean();
				pb.setAmount(String.valueOf(view.getAmount()));
				if (view.getPaymentDueOn() != null)
				{
					pb.setDueDate(view.getPaymentDueOn().toString());
				}
				pb.setPaymentMethod(CommonUtil.getIsEnglishLocale()?view.getPaymentModeEn():view.getPaymentModeAr());
				if (view.getPaymentDate() != null)
				{
					pb.setPaymentDate(view.getPaymentDate().toString());
				}
				pb.setStatus(CommonUtil.getIsEnglishLocale()?view.getStatusEn():view.getStatusAr());
				pb.setPaymentType(CommonUtil.getIsEnglishLocale()?view.getTypeEn():view.getTypeAr());
				pb.setPaymentScheduleView(view);
				if(view.getStatusId().compareTo(ddvPending.getDomainDataId())==0)
				{
					paymentDoneSuccessfully = false;
				}
				if( view.getPaymentScheduleId() == null && view.getIsDeleted() != null &&
					view.getIsDeleted().longValue()==WebConstants.DEFAULT_IS_DELETED	
				)
				{
					List<PaymentScheduleView> paymentToAddList  = new ArrayList<PaymentScheduleView>(1);
					view.setContractId( contractView.getContractId() );
					view.setRequestId(  requestView.getRequestId() );
					paymentToAddList.add( view );
					paymentToAddList =new UtilityServiceAgent().persistPaymentSchedule(paymentToAddList);
					view = paymentToAddList.iterator().next();
					pb.setPaymentScheduleView(view);
				}
				paymentBeans.add(pb);
   			    // If some payment is exempted on edit and after that split is done on another payment
				// then split picks record from database the exempted payment's status will change to PENDING
				// again.
				if( view.getStatusId().longValue() == 36019l )
				{
					exemptedBeanMap.put( pb.getPaymentScheduleView().getPaymentScheduleId(),pb);
				}
			}
			if(exemptedBeanMap != null && exemptedBeanMap.size() > 0)
			{
				viewMap.put( EXEMPTED_PAYMENT_MAP,exemptedBeanMap);
			} 
		}
		viewMap.put("paymentDoneSuccessfully", paymentDoneSuccessfully);
				
		viewMap.put(BEAN_PAYMENTS, paymentBeans);
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	public HtmlCommandButton getSubmitBtn() {
		return submitBtn;
	}

	public void setSubmitBtn(HtmlCommandButton submitBtn) {
		this.submitBtn = submitBtn;
	}
	@SuppressWarnings("unchecked")
	public void onCheckBoxClick()
	{
		if(exemptFine)
			viewMap.put("isExempted",1L);
		else
			viewMap.put("isExempted",0L);
	}

	public boolean isExemptFine() 
	{
		if(getExempted() !=null)
		{
			if(getExempted().compareTo(1L)==0)
				exemptFine=true;
			else
				exemptFine=false;
		}
		else
			exemptFine=false;
		return exemptFine;
	}

	public void setExemptFine(boolean exemptFine) {
		this.exemptFine = exemptFine;
	}
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()
    {
    	try
    	{
	    	PaymentScheduleView psv = ((PaymentBean)dataTablePaymentSchedule.getRowData()).getPaymentScheduleView();
	    	CommonUtil.viewPaymentDetails(psv);
    	}
    	catch(Exception e)
    	{
    		logger.LogException("btnViewPaymentDetails_Click|Error", e);
    	}
    }
	public HtmlCommandButton getTerminateInEjariBtn() {
		return terminateInEjariBtn;
	}
	public void setTerminateInEjariBtn(HtmlCommandButton terminateInEjariBtn) {
		this.terminateInEjariBtn = terminateInEjariBtn;
	}

}
    
