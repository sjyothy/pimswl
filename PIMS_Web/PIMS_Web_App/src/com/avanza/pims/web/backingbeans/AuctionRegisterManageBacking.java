package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionRequestRegView;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;

public class AuctionRegisterManageBacking extends AbstractController
{
	private static final long serialVersionUID = 3940323405218088841L;
	
	private Logger logger;
	@SuppressWarnings("unchecked")
	private Map viewMap;
	@SuppressWarnings("unchecked")
	private Map requestMap;
	@SuppressWarnings("unchecked")
	private Map sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private String pageMode;
	private String locale;
	
	private PersonView bidderView;
	private AuctionView auctionView;
	private List<AuctionRequestRegView> auctionRequestRegViewList;
	private String totalDepositAmount;
	private String totalSelectedUnits;
	private List<PaymentScheduleView> paymentScheduleViewList; 
	private RequestView requestView;
	private PaymentReceiptView paymentReceiptView;
	private boolean isRegistered;		
	private PropertyServiceAgent propertyServiceAgent;
	
	private HtmlDataTable unitDataTable;
	private HtmlDataTable paymentScheduleDataTable;
	
	public AuctionRegisterManageBacking() 
	{
		logger = Logger.getLogger( this.getClass() );
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		pageMode = WebConstants.AuctionRegister.PAGE_MODE_NEW;
		locale = CommonUtil.getLocale();
		
		bidderView = new PersonView();
		auctionView = new AuctionView();
		auctionRequestRegViewList = new ArrayList<AuctionRequestRegView>(0);
		totalDepositAmount = "0.00";
		totalSelectedUnits = "0";
		paymentScheduleViewList = new ArrayList<PaymentScheduleView>(0);
		requestView = new RequestView();
		paymentReceiptView = new PaymentReceiptView();
		
		propertyServiceAgent = new PropertyServiceAgent();
		
		unitDataTable = new HtmlDataTable();
		paymentScheduleDataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			loadAttachmentsAndComments(null);
			updateValuesFromMaps();
			canAddAttachmentsAndComents();
		}
		catch (Exception exception) 
		{
			logger.LogException( " init--- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception 
	{
		// PAGE MODE
		if ( viewMap.get( WebConstants.AuctionRegister.PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.AuctionRegister.PAGE_MODE_KEY );
		}
		
		// BIDDER VIEW
		if ( viewMap.get( WebConstants.AuctionRegister.BIDDER_VIEW ) != null )
		{
			bidderView = (PersonView) viewMap.get( WebConstants.AuctionRegister.BIDDER_VIEW );
		}
		
		// AUCTION VIEW
		if ( viewMap.get( WebConstants.AuctionRegister.AUCTION_VIEW ) != null )
		{
			auctionView = (AuctionView) viewMap.get( WebConstants.AuctionRegister.AUCTION_VIEW );
		}
		
		// AUCTION REQUEST REG VIEW LIST
		if ( viewMap.get( WebConstants.AuctionRegister.AUCTION_REQUEST_REG_VIEW_LIST ) != null )
		{
			auctionRequestRegViewList = (List<AuctionRequestRegView>) viewMap.get( WebConstants.AuctionRegister.AUCTION_REQUEST_REG_VIEW_LIST );
		}
		
		// TOTAL DEPOSIT AMOUNT
		if ( viewMap.get( WebConstants.AuctionRegister.TOTAL_DEPOSIT_AMOUNT ) != null )
		{
			 totalDepositAmount = (String) viewMap.get( WebConstants.AuctionRegister.TOTAL_DEPOSIT_AMOUNT );
		}
		
		// TOTAL SELECTED UNITS
		if ( viewMap.get( WebConstants.AuctionRegister.TOTAL_SELECTED_UNITS ) != null )
		{
			totalSelectedUnits = (String) viewMap.get( WebConstants.AuctionRegister.TOTAL_SELECTED_UNITS );
		}
		
		// PAYMENT SCHEDULE VIEW
		if ( sessionMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE ) != null )
		{
			paymentScheduleViewList = (List<PaymentScheduleView>) sessionMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE );			
			sessionMap.remove( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE );
		}
		else if ( viewMap.get( WebConstants.AuctionRegister.PAYMENT_SCHEDULE_VIEW ) != null )
		{
			paymentScheduleViewList = (List<PaymentScheduleView>) viewMap.get( WebConstants.AuctionRegister.PAYMENT_SCHEDULE_VIEW );
		}
		else
		{
			loadProcedurePayments();
		}
		
		// REQUEST VIEW
		if ( sessionMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) sessionMap.get( WebConstants.REQUEST_VIEW );
			sessionMap.remove( WebConstants.REQUEST_VIEW );
			
			requestView = propertyServiceAgent.getAuctionRequestById( requestView.getRequestId() );
			bidderView = propertyServiceAgent.getPersonInformation( requestView.getBidderView().getPersonId() );			
			auctionView = requestView.getAuctionView();			
			paymentScheduleViewList = propertyServiceAgent.getPaymentScheduleByRequestID( requestView.getRequestId() );
			pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECT_PAYMENT;			
			DomainDataView ddvStatusComplete = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), WebConstants.REQUEST_STATUS_COMPLETE );			
			if ( requestView.getStatusId().equals( ddvStatusComplete.getDomainDataId() ) )
				pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECTED;			
			loadAuctionRequestRegViewList();
		}	
		else if ( viewMap.get( WebConstants.AuctionRegister.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.AuctionRegister.REQUEST_VIEW );
		}
		
		// PAYMENT RECEIPT VIEW
		if( sessionMap.get( WebConstants.PAYMENT_COLLECTED_SUCESSFULLY )!= null && 
				(Boolean) sessionMap.get( WebConstants.PAYMENT_COLLECTED_SUCESSFULLY ) &&
				sessionMap.get( WebConstants.PAYMENT_RECEIPT_VIEW ) != null )
		{
			paymentReceiptView = (PaymentReceiptView) sessionMap.get( WebConstants.PAYMENT_RECEIPT_VIEW );
			sessionMap.remove( WebConstants.PAYMENT_COLLECTED_SUCESSFULLY );			
			sessionMap.remove( WebConstants.PAYMENT_RECEIPT_VIEW );
			
			collectPayments();
		}
		else if ( viewMap.get( WebConstants.AuctionRegister.PAYMENT_RECEIPT_VIEW ) != null )
		{
			paymentReceiptView = (PaymentReceiptView) viewMap.get( WebConstants.AuctionRegister.PAYMENT_RECEIPT_VIEW );
		}
		
		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.AuctionRegister.PAGE_MODE_KEY, pageMode );
		}
		
		// BIDDER VIEW
		if ( bidderView != null )
		{
			viewMap.put( WebConstants.AuctionRegister.BIDDER_VIEW, bidderView );
		}
		
		// AUCTION VIEW
		if ( auctionView != null )
		{
			viewMap.put( WebConstants.AuctionRegister.AUCTION_VIEW, auctionView );
		}
		
		// AUCTION REQUEST REG VIEW LIST
		if ( auctionRequestRegViewList != null )
		{
			viewMap.put( WebConstants.AuctionRegister.AUCTION_REQUEST_REG_VIEW_LIST, auctionRequestRegViewList );
		}
		
		// TOTAL DEPOSIT AMOUNT
		if ( totalDepositAmount != null )
		{
			viewMap.put( WebConstants.AuctionRegister.TOTAL_DEPOSIT_AMOUNT, totalDepositAmount );
		}
		
		// TOTAL SELECTED UNITS
		if ( totalSelectedUnits != null )
		{
			viewMap.put( WebConstants.AuctionRegister.TOTAL_SELECTED_UNITS, totalSelectedUnits );
		}
		
		// PAYMENT SCHEDULE VIEW
		if ( paymentScheduleViewList != null )
		{
			viewMap.put( WebConstants.AuctionRegister.PAYMENT_SCHEDULE_VIEW , paymentScheduleViewList );
		}
		
		// REQUEST VIEW
		if ( requestView != null )
		{
			viewMap.put( WebConstants.AuctionRegister.REQUEST_VIEW , requestView );
			loadAttachmentsAndComments(requestView.getRequestId());
		}
		
		// PAYMENT RECEIPT VIEW
		if ( paymentReceiptView != null )
		{
			viewMap.put( WebConstants.AuctionRegister.PAYMENT_RECEIPT_VIEW, paymentReceiptView );
		}
	}
	
	private Boolean collectPayments()
	{
		final String METHOD_NAME = "collectPayments()";
		Boolean isSuccess = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			paymentReceiptView.setPaidById(bidderView.getPersonId() );
			paymentReceiptView = propertyServiceAgent.collectPayments( paymentReceiptView );
			pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECTED;
			
			CommonUtil.updateChequeDocuments( paymentReceiptView.getPaymentReceiptId().toString() );
			CommonUtil.printPaymentReceipt( "", "", paymentReceiptView.getPaymentReceiptId().toString(), getFacesContext(), MessageConstants.PaymentReceiptReportProcedureName.AUCTION_REQ_REG );			
			
			DomainDataView ddvStatusCollected = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType( WebConstants.PAYMENT_SCHEDULE_STATUS ), WebConstants.PAYMENT_SCHEDULE_COLLECTED );
			DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	                WebConstants.REALIZED);
			DomainDataView ddvCheques = CommonUtil.getIdFromType(
                    CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
                    WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE
                    );
			DomainDataView ddvCash = CommonUtil.getIdFromType(
                    CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
                    WebConstants.PAYMENT_SCHEDULE_MODE_CASH
                    );
			DomainDataView ddvBankTransfer = CommonUtil.getIdFromType(
                    CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
                    WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER 
                    );
			for ( PaymentScheduleView paymentScheduleView : paymentScheduleViewList )
			{
				DomainDataView dd = new DomainDataView();
				if( paymentScheduleView.getPaymentModeId().longValue() == ddvCash.getDomainDataId().longValue() || 
					paymentScheduleView.getPaymentModeId().longValue() == ddvBankTransfer.getDomainDataId().longValue()
				  )
					dd = ddvRealized;
				else if ( paymentScheduleView.getPaymentModeId().longValue() == ddvCheques.getDomainDataId().longValue() )
					dd = ddvStatusCollected ;
					paymentScheduleView.setStatusId( dd.getDomainDataId() );
				paymentScheduleView.setStatusEn( dd.getDataDescEn() );
				paymentScheduleView.setStatusAr( dd.getDataDescAr() );
				paymentScheduleView.setIsReceived("Y");
			}
			
			DomainDataView ddvStatusPresent = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType( WebConstants.ConductAuction.BIDDER_AUCTION_STATUS ), WebConstants.ConductAuction.PRESENT );
			BidderAuctionRegView bidderAucRegView = new BidderAuctionRegView();
		    bidderAucRegView.setBidderId( bidderView.getPersonId() );
		    bidderAucRegView.setBidderView( bidderView );
		    bidderAucRegView.setAuctionId( auctionView.getAuctionId() );
		    bidderAucRegView.setAuctionView( auctionView );
		    bidderAucRegView.setAnyWin("N");
		    bidderAucRegView.setAttended("Y");
		    bidderAucRegView.setBalanceDepositAmount(0.00);
		    bidderAucRegView.setConfiscatedAmount(new Double(0));
		    bidderAucRegView.setHasRefunded("N");
		    bidderAucRegView.setTotalDepositAmount( Double.parseDouble( totalDepositAmount ) );
		    bidderAucRegView.setCreatedBy( CommonUtil.getLoggedInUser() );
		    bidderAucRegView.setCreatedOn( new Date() );
		    bidderAucRegView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		    bidderAucRegView.setUpdatedOn( new Date() );
		    bidderAucRegView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
		    bidderAucRegView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
		    bidderAucRegView.setStatusId( ddvStatusPresent.getDomainDataId() );
		    
		    // requestView.setRequestDetailView( null );
		    DomainDataView ddvStatusComplete = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), WebConstants.REQUEST_STATUS_COMPLETE );
		    requestView.setStatusId( ddvStatusComplete.getDomainDataId() );
		    requestView.setStatusEn( ddvStatusComplete.getDataDescEn() );
		    requestView.setStatusAr( ddvStatusComplete.getDataDescAr() );
		    isSuccess = propertyServiceAgent.updateAuctionRequest( requestView, bidderAucRegView, paymentScheduleViewList );
		    
			updateValuesToMaps();
			
			if(requestView.getRequestId() != null)
			{
				String noteOwner = (String)viewMap.get("noteowner");
				CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
			}
			CommonUtil.saveSystemComments(	 WebConstants.REQUEST, 
											 MessageConstants.RequestEvents.REQUEST_COLLECT, 
											 requestView.getRequestId()
										  );
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch ( Exception e )
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return isSuccess;
	}
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{
			updateValuesFromMaps();
			if(requestView== null || requestView.getRequestId() ==null) return;
         
			RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	
	public String onRegister()
	{
		final String METHOD_NAME = "onRegister()";
		Boolean isSuccess = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( isValid() )
			{
				isSuccess = registerAuctionRequest();
				
				if ( isSuccess )
				{
//					viewMap.put("REGISTERED",true);
					pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECT_PAYMENT;
					updateValuesToMaps();
					String msg = java.text.MessageFormat.format(
																	CommonUtil.getBundleMessage(  MessageConstants.AuctionRegistration.MSG_AUCTION_REGISTER ), 
																	requestView.getRequestNumber() 
											 					);
					successMessages.add(  msg );
					CommonUtil.saveSystemComments(	 WebConstants.REQUEST, 
													 MessageConstants.RequestEvents.REQUEST_SAVED, 
													 requestView.getRequestId()
												 );
					CommonUtil.printReport(requestView.getRequestId());
				}			
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch ( Exception e )
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return null;
	}
	
	private boolean isValid() throws Exception 
	{
		boolean isValid = true;
		DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
		
		Date todaysDate = df.parse(df.format(new Date()));
		
		int selectedUnregisteredAuctionUnitsCount = 0;
		
		if ( bidderView == null || bidderView.getPersonId() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_SELECT_BIDDER ) );
		}
//		else if (!CommonUtil.isPersonContainCountry(bidderView))
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.NationalyNotAdded"));
//    		isValid = false ;
//    	}
		
		if ( auctionView == null || auctionView.getAuctionId() == null )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_SELECT_AUCTION ) );
		}
		
		if ( auctionView.getRequestStartDate() != null && todaysDate.before( auctionView.getRequestStartDate() ) )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_REGISTRATION_NOT_YET_STARTED ) );
		}
		
		if ( auctionView.getRequestEndDate() != null && todaysDate.after( auctionView.getRequestEndDate() ) )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_DEADLINE_CROSSED ) );
		}
		
		if ( auctionRequestRegViewList == null || auctionRequestRegViewList.size() <= 0 )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_NO_UNITS_TO_REGISTER ) );
		}
		
		for ( AuctionRequestRegView auctionRequestRegView : auctionRequestRegViewList )
		{
			if ( auctionRequestRegView.getRegistered().equals( false ) && auctionRequestRegView.getIsSelected().equals( true ) )
			{
				selectedUnregisteredAuctionUnitsCount++;
			}
		}
		if ( selectedUnregisteredAuctionUnitsCount <= 0 )
		{
			isValid = false;
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_SELECT_UNIT_TO_PAY ) );
		}
		
		return isValid;
	}

	private Boolean registerAuctionRequest() throws Exception 
	{
		Boolean isSuccess = false;
		
		if ( requestView.getRequestId() == null )
		{
			// Add Request
			requestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			requestView.setCreatedOn( new Date() );
			requestView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			requestView.setUpdatedOn( new Date() );
			requestView.setRequestDate( new Date() );
			requestView.setDepositAmount( Double.parseDouble( totalDepositAmount ) );
			requestView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			requestView.setDescription( CommonUtil.getBundleMessage( WebConstants.AuctionRequest.REQUEST_DESC ) );			
			requestView.setAuctionId( auctionView.getAuctionId() );
			requestView.setAuctionView( auctionView );
			requestView.setPaymentScheduleView( paymentScheduleViewList );			
			requestView.setBidderView( bidderView );			
			requestView.setTenantsView( null );
			requestView.setContactorView( null );
			requestView.setRequestDetailView( getRequestDetailViewSetByAuctionRequestRegViewList( auctionRequestRegViewList, false, true ) );			
			requestView.setRequestId( propertyServiceAgent.createAuctionRequest( requestView ) );
			
			if ( requestView.getRequestId() != null )
			{
				isSuccess = true;
			}
		}		
		else
		{
			// Update Request
			requestView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			requestView.setUpdatedOn( new Date() );
			requestView.setDepositAmount( Double.parseDouble( totalDepositAmount ) );
			requestView.setAuctionId( auctionView.getAuctionId() );
			requestView.setAuctionView( auctionView );			
			requestView.setBidderView( bidderView );			
			requestView.setTenantsView( null );
			requestView.setContactorView( null );
		}
		
		if(requestView.getRequestId() != null)
		{
			loadAttachmentsAndComments(requestView.getRequestId());
			CommonUtil.saveAttachments(requestView.getRequestId());
			String noteOwner = (String)viewMap.get("noteowner");
			CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
		}
		
		return isSuccess;
	}
	
	@SuppressWarnings("unchecked")
	public String onCollectPayment()
	{
		
		try
		{
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentScheduleViewList );			
	        sessionMap.put( WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView );
	        executeJavascript("openPopupReceivePayment();");
	        btnCollectPayment.setDisabled(true);
		}
		catch ( Exception e )
		{
			logger.LogException( " onCollectPayment--- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return null;
	}

	public String onCancel()
	{
		return null;
	}
	
	private Set<RequestDetailView> getRequestDetailViewSetByAuctionRequestRegViewList( List<AuctionRequestRegView> auctionRequestRegViewList, Boolean registered, Boolean selected )
	{
		Set<RequestDetailView> requestDetailViewSet = new HashSet<RequestDetailView>(0);
		
		for ( AuctionRequestRegView auctionRequestRegView : auctionRequestRegViewList )
		{
			if ( auctionRequestRegView.getRegistered().equals( registered ) && auctionRequestRegView.getIsSelected().equals( selected ) )				
			{
				RequestDetailView requestDetailView = new RequestDetailView();
				
				requestDetailView.setCreatedBy( CommonUtil.getLoggedInUser() );
				requestDetailView.setCreatedOn( new Date() );
				requestDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				requestDetailView.setUpdatedOn( new Date() );
				requestDetailView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				requestDetailView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				requestDetailView.setDepositAmount( auctionRequestRegView.getDepositAmount() );
				requestDetailView.setWonAuction( false );
				requestDetailView.setHasRefunded( false );
				requestDetailView.setMoveToContract( 0L );
				requestDetailView.setConfiscatedAmount( 0.0D );
				
				UnitView unitView = new UnitView();
				unitView.setUnitId( auctionRequestRegView.getUnitId() );
				requestDetailView.setUnitView( unitView );
				
				AuctionUnitView auctionUnitView = new AuctionUnitView();
				auctionUnitView.setAuctionUnitId( auctionRequestRegView.getAuctionUnitId() );
				requestDetailView.setAuctionUnitView( auctionUnitView );
				
				requestDetailViewSet.add( requestDetailView );
			}
		}
		
		return requestDetailViewSet;
	}
	
	public String onOpenPopupBidderSearch()
	{
		executeJavascript("openPopupBidderSearch();");
		return null;
	}
	
	public String onReceiveBidderSelection()
	{
		String METHOD_NAME = "onReceiveBidderSelection()";
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( bidderView.getPersonId() != null )
			{
				bidderView = propertyServiceAgent.getPersonInformation( bidderView.getPersonId() );
				loadAuctionRequestRegViewList();
				updateValuesToMaps();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch(Exception ex)
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return null;
	}
	
	public String onOpenPopupAuctionSearch()
	{
		executeJavascript("openPopupAuctionSearch();");
		return null;
	}
	
	public String onReceiveAuctionSelection()throws Exception
	{
		String METHOD_NAME = "onReceiveAuctionSelection()";
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( auctionView.getAuctionId() != null )
			{
				auctionView = propertyServiceAgent.getAuctionById( auctionView.getAuctionId() );
				loadAuctionRequestRegViewList();
				
				DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
				
				Date todaysDate = df.parse(df.format(new Date()));
				
				
				if ( todaysDate.before( auctionView.getRequestStartDate() ) )
				{
					// pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECTED;
					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_REGISTRATION_NOT_YET_STARTED ) );
				}
				
				if ( todaysDate.after( auctionView.getRequestEndDate() ) )
				{
					// pageMode = WebConstants.AuctionRegister.PAGE_MODE_COLLECTED;
					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.AuctionRegistration.MSG_DEADLINE_CROSSED ) );
				}
				
				updateValuesToMaps();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch(Exception ex)
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return null;
	}
	
	private void loadAuctionRequestRegViewList() throws PimsBusinessException
	{
		String METHOD_NAME = "loadAuctionRequestRegViewList()";
		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		if ( bidderView != null && bidderView.getPersonId() != null && auctionView != null && auctionView.getAuctionId() != null )
		{
			
			if( requestView ==null || requestView.getRequestId() == null ) 
				paymentScheduleViewList = new ArrayList<PaymentScheduleView>(0);
			// All Auction Request Reg View
			auctionRequestRegViewList = propertyServiceAgent.getAllAuctionUnits(auctionView);
			
			// Unregistered Auction Request Reg View 
			List<AuctionRequestRegView> unregisteredAuctionRequestRegViewList = propertyServiceAgent.getUnRegisteredUnitsOfBidderForAuction(auctionView, bidderView);
			Map<Long, AuctionRequestRegView> unregisteredAuctionRequestRegViewMap = new HashMap<Long, AuctionRequestRegView>( unregisteredAuctionRequestRegViewList.size() );			
			for ( AuctionRequestRegView auctionRequestRegView : unregisteredAuctionRequestRegViewList ) 
			{
				unregisteredAuctionRequestRegViewMap.put(auctionRequestRegView.getAuctionUnitId(), auctionRequestRegView);
			}
			
			// Marking Auction Request Reg View As Registered / Unregistered 
			// Calculating Total Deposit Amount and Total Selected Units
			double dblTotalDepositAmount = 0.00d;
			int intTotalSelectedUnits = 0;			
			for ( AuctionRequestRegView auctionRequestRegView : auctionRequestRegViewList )
			{
				if ( unregisteredAuctionRequestRegViewMap.containsKey( auctionRequestRegView.getAuctionUnitId() ) )
				{
					auctionRequestRegView.setIsSelected(false);
					auctionRequestRegView.setRegistered(false);
				}
				else
				{
					auctionRequestRegView.setIsSelected(true);
					auctionRequestRegView.setRegistered(true);
					
					dblTotalDepositAmount += auctionRequestRegView.getDepositAmount().doubleValue();
					intTotalSelectedUnits ++;
				}
			}			
			totalDepositAmount = String.valueOf( dblTotalDepositAmount );
			totalSelectedUnits = String.valueOf( intTotalSelectedUnits );
			
			// Updating the changed values in maps
			updateValuesToMaps();
		}
		
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
	}
	
	public String onUnitSelectionChanged()
	{
		String METHOD_NAME = "onUnitSelectionChanged()";
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
			AuctionRequestRegView selectedAuctionRequestRegView = (AuctionRequestRegView) unitDataTable.getRowData();
			
			if ( selectedAuctionRequestRegView.getIsSelected() )
			{
				totalDepositAmount = String.valueOf( Double.valueOf( totalDepositAmount ) + selectedAuctionRequestRegView.getDepositAmount() );
				totalSelectedUnits = String.valueOf( Integer.valueOf( totalSelectedUnits ) + 1 );
				
				paymentScheduleViewList.add( propertyServiceAgent.getPaymentScheduleViewByAuctionRequestRegView( selectedAuctionRequestRegView, CommonUtil.getLoggedInUser() ) );
			}
			else
			{
				totalDepositAmount = String.valueOf( Double.valueOf( totalDepositAmount ) - selectedAuctionRequestRegView.getDepositAmount() );
				totalSelectedUnits = String.valueOf( Integer.valueOf( totalSelectedUnits ) - 1 );
				
				for ( PaymentScheduleView paymentScheduleView : paymentScheduleViewList )
				{
					if ( paymentScheduleView.getAuctionUnitId() != null && paymentScheduleView.getAuctionUnitId().equals( selectedAuctionRequestRegView.getAuctionUnitId() ) )
					{
						paymentScheduleViewList.remove( paymentScheduleView );
						break;
					}
				}
			}
			
			updateValuesToMaps();
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		return null;
	}
	
	private void loadProcedurePayments() throws Exception
	{
		PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
		//paymentConfigurationView.setPaymentTypeId( WebConstants.PAYMENT_TYPE_FEES_ID );
		paymentConfigurationView.setProcedureTypeKey( WebConstants.PROCEDURE_TYPE_REGISTER_AUCTION );
		paymentConfigurationView.setIsFixed( 1L );
		paymentConfigurationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
		paymentConfigurationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
		paymentConfigurationView.setCreatedBy( CommonUtil.getLoggedInUser() );
		paymentConfigurationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		paymentConfigurationView.setCreatedOn( new Date() );
		paymentConfigurationView.setUpdatedOn( new Date() );
		
		paymentScheduleViewList = propertyServiceAgent.getPaymentScheduleViewListByPaymentConfigurationView( paymentConfigurationView );
		
		updateValuesToMaps(); 
	}
	
	@SuppressWarnings("unchecked")
	public String onOpenPopupEditPaymentSchedule()
	{
		PaymentScheduleView selectedPaymentScheduleView = (PaymentScheduleView) paymentScheduleDataTable.getRowData();
		sessionMap.put( WebConstants.PAYMENT_SCHDULE_EDIT, selectedPaymentScheduleView );
		sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, paymentScheduleViewList );
		sessionMap.put("STOP_AMOUNT_EDIT",true);
		
		executeJavascript("openPopupEditPaymentSchedule();");
		return null;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsCollectedMode()
	{
		Boolean isCollectedMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.AuctionRegister.PAGE_MODE_COLLECTED ) )
			isCollectedMode = true;
		
		return isCollectedMode;
	}
	
	public Boolean getIsNewMode()
	{
		Boolean isNewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.AuctionRegister.PAGE_MODE_NEW ) )
			isNewMode = true;
		
		return isNewMode;
	}
	
	public Boolean getIsCollectPaymentMode()
	{
		Boolean isCollectPaymentMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.AuctionRegister.PAGE_MODE_COLLECT_PAYMENT ) )
			isCollectPaymentMode = true;
		
		return isCollectPaymentMode;
	}
	
	public String getBidderEmail()
	{
		String bidderEmail = null;
		
		if ( bidderView != null && bidderView.getContactInfoViewSet() != null && bidderView.getContactInfoViewSet().size() > 0 )
		{
			Set<ContactInfoView> contactInfoViewSet = bidderView.getContactInfoViewSet();
			
			for ( ContactInfoView contactInfoView : contactInfoViewSet )
			{
				if ( contactInfoView != null && contactInfoView.getEmail() != null && ! contactInfoView.getEmail().equalsIgnoreCase("") )
				{
					bidderEmail = contactInfoView.getEmail();
					break;
				}
			}
		}
		
		return bidderEmail;
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public String getNumberFormat() {
    	return CommonUtil.getDoubleFormat();
    }
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@SuppressWarnings("unchecked")
	public Map getViewMap() {
		return viewMap;
	}

	@SuppressWarnings("unchecked")
	public void setViewMap(Map viewMap) {
		this.viewMap = viewMap;
	}

	@SuppressWarnings("unchecked")
	public Map getRequestMap() {
		return requestMap;
	}

	@SuppressWarnings("unchecked")
	public void setRequestMap(Map requestMap) {
		this.requestMap = requestMap;
	}

	@SuppressWarnings("unchecked")
	public Map getSessionMap() {
		return sessionMap;
	}

	@SuppressWarnings("unchecked")
	public void setSessionMap(Map sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public PersonView getBidderView() {
		return bidderView;
	}

	public void setBidderView(PersonView bidderView) {
		this.bidderView = bidderView;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public AuctionView getAuctionView() {
		return auctionView;
	}

	public void setAuctionView(AuctionView auctionView) {
		this.auctionView = auctionView;
	}

	public List<AuctionRequestRegView> getAuctionRequestRegViewList() {
		return auctionRequestRegViewList;
	}

	public void setAuctionRequestRegViewList(
			List<AuctionRequestRegView> auctionRequestRegViewList) {
		this.auctionRequestRegViewList = auctionRequestRegViewList;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public HtmlDataTable getUnitDataTable() {
		return unitDataTable;
	}

	public void setUnitDataTable(HtmlDataTable unitDataTable) {
		this.unitDataTable = unitDataTable;
	}

	public String getTotalDepositAmount() {
		return totalDepositAmount;
	}

	public void setTotalDepositAmount(String totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}

	public String getTotalSelectedUnits() {
		return totalSelectedUnits;
	}

	public void setTotalSelectedUnits(String totalSelectedUnits) {
		this.totalSelectedUnits = totalSelectedUnits;
	}

	public List<PaymentScheduleView> getPaymentScheduleViewList() {
		return paymentScheduleViewList;
	}

	public void setPaymentScheduleViewList(
			List<PaymentScheduleView> paymentScheduleViewList) {
		this.paymentScheduleViewList = paymentScheduleViewList;
	}

	public HtmlDataTable getPaymentScheduleDataTable() {
		return paymentScheduleDataTable;
	}

	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
		this.paymentScheduleDataTable = paymentScheduleDataTable;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public PaymentReceiptView getPaymentReceiptView() {
		return paymentReceiptView;
	}

	public void setPaymentReceiptView(PaymentReceiptView paymentReceiptView) {
		this.paymentReceiptView = paymentReceiptView;
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_REGISTER_AUCTION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_AUCTION;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if(requestId!= null){
			
			String entityId = requestId.toString();
	    	
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	@SuppressWarnings("unchecked")
	public void canAddAttachmentsAndComents()
	{
//		if(getIsNewMode())
//		{
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote",true);
		//}
//		else if(getIsCollectPaymentMode() || getIsCollectedMode())
//		{
//			viewMap.put("canAddAttachment",false);
//			viewMap.put("canAddNote",false);
//		}
		
	}
	@SuppressWarnings( "unchecked" )
	private DomainDataView getRequestStatusComplete()
	{
		
		if( viewMap.containsKey( "REQUEST_STATUS_COMPLETE" ) && viewMap.get( "REQUEST_STATUS_COMPLETE" ) != null )
		{
		 List<DomainDataView> list = CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS );
		 DomainDataView ddv = CommonUtil.getIdFromType( list, WebConstants.REQUEST_STATUS_COMPLETE );
		 viewMap.put( "REQUEST_STATUS_COMPLETE", ddv );
		}
		return (DomainDataView) viewMap.get( "REQUEST_STATUS_COMPLETE" );
	}
	public void btnPrintPaymentSchedule_Click()
    {
    	String methodName = "btnPrintPaymentSchedule_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.AUCTION_REQ_REG);
    	logger.logInfo(methodName+"|"+" Finish...");
    }

	public boolean isRegistered() 
	{
		boolean registered = false;
		
		if ( viewMap.containsKey("REGISTERED"))
			registered=(Boolean) viewMap.get("REGISTERED");
		return registered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
	@Override
	public void prerender()
	{
		super.prerender();
		if(getIsCollectPaymentMode())
			btnCollectPayment.setRendered(true);
		else
			btnCollectPayment.setRendered(false);
	}
	
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()
    {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }
    

    
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		final String viewId = "/auctionRegisterManage.jsp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+1024+',height='+450+',left=0,top=40,scrollbars=yes,status=yes');";
      if (extraJavaScript!=null&& extraJavaScript.trim().length()>0)      
    		javaScriptText=extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
}
