package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.bpel.proxy.pimstenderinquirybpelproxy.PIMSTenderInquiryBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.ui.util.ResourceUtil;


public class TenderInquiryAdd extends AbstractController {

	/**
	 * 
	 */
	

	private final Logger logger = Logger.getLogger(TenderInquiryAdd.class);

	public interface Keys {
		public static final String TENDER_INQUIRY_LIST = "TENDER_INQUIRY_LIST";
	}


	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>() ;
	private final String notesOwner = WebConstants.TENDER_INQUIRY_REQUESTS;
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	ServletContext servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();
	
	//field used in the tender Inquiry Add jsp
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private String  applicationNumber;
	private String applicationStatus;
	private Date applicationDate;
	private String applicationSourceId;
	private String contractorName;
	private String contractorNo;
	private String tenderNumber;
	private String tenderDes;
	private String reasons;
	private String licencseNumber;

	//hdn variable for contractor and tender  
	private String hdnContractorId;
	private String hdnContractorNumber;
	private String hdnContractorNameEn;
	private String hdnContractorNameAr;
	private String hdnTenderId;
	private String hdnTenderNumber;
	private String hdnTenderDescription;
	private String hdnRequestId;
	private String hdnLicencseNumber;
	
	private String  VIEW_MODE="pageMode";
	private String   MODE_POPUP = "MODE_POPUP";
	
	private TenderInquiryView tenderInquiryView = new TenderInquiryView();
	private ContractorView contractorView = new ContractorView();
	private TenderView tenderView = new TenderView();
	private CommonUtil commUtil = new CommonUtil();
	private HashMap map = new HashMap ();
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_TENDER_INQUIRY_REQUESTS;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_TENDER_INQUIRY;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_TENDER_INQUIRY_REQUESTS;
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		
		HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
		try {
			if (!isPostBack()) {
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				
				
				if(sessionMap.containsKey("FROM_CONSTRUCTION_TENDER"))
				{
				  viewMap.put("FROM_CONSTRUCTION_TENDER", true);
				  sessionMap.remove("FROM_CONSTRUCTION_TENDER");
				}
				
             if(!sessionMap.containsKey("FROM_INQUIRY_GRID")){
				
			
			// for setting default date on the date field 	
				applicationDate = new Date();
			// for enabling button on the Attachment and Comments tab 	
				canAddAttachmentsAndComments(true);
				DomainDataView ddv = commUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
						WebConstants.REQUEST_STATUS_OPEN);
				
				if(getIsEnglishLocale())
			    	setApplicationStatus(ddv.getDataDescEn());
			    else
			    	setApplicationStatus(ddv.getDataDescAr());
				
				 
				viewMap.put("TENDER_INQUIRY_STATUS", ddv.getDomainDataId());
				
				 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_POPUP)) 
		    		 { 	      	    		
	      	    		viewMap.put(VIEW_MODE, MODE_POPUP);
	      	    	}
		    	 }	
				
				
					 }
                }
             if(sessionMap.containsKey("FROM_INQUIRY_GRID")){
            	 
            	 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_POPUP)) 
		    		 { 	      	    		
	      	    		viewMap.put(VIEW_MODE, MODE_POPUP);
	      	    	}
		    	 }
            	 viewMap.put("IN_READ_ONLY_MODE", true);
            	 viewMap.put("IN_VIEW_MODE", true);
            	 
            		
				TenderInquiryView tIV =  (TenderInquiryView)sessionMap.get("FROM_INQUIRY_GRID");
				sessionMap.remove("FROM_INQUIRY_GRID");
				this.hdnRequestId = tIV.getRequestId().toString();
				populateRequestFields(tIV.getRequestId().toString());
				CommonUtil.loadAttachmentsAndComments(hdnRequestId); 
            	 
             
				 
			}

			logger.logInfo("init method completed successfully!");
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}
	
	public void prerender(){
		super.prerender();
		
		
		if(hdnContractorNumber!=null && !hdnContractorNumber.equals("")){
		 contractorNo = hdnContractorNumber;	
		}
		if(getIsEnglishLocale()&& hdnContractorNameEn!=null && !hdnContractorNameEn.equals("")){
		contractorName = hdnContractorNameEn;	
		}
		if(!getIsEnglishLocale()&& hdnContractorNameAr!=null && !hdnContractorNameAr.equals("")){
		contractorName = hdnContractorNameAr;	
	    }
		if(hdnTenderNumber!=null && !hdnTenderNumber.equals("")){
		tenderNumber = hdnTenderNumber;
		}
		if(hdnTenderDescription!=null && !hdnTenderDescription.equals("")){
		tenderDes = hdnTenderDescription; 	
		}
		if(hdnLicencseNumber!=null && !hdnLicencseNumber.equals("")){
		licencseNumber = hdnLicencseNumber;
		}
			
		
		

	}		

	// Accessors starts from here

	public String getInfoMessages() {
	  return CommonUtil.getErrorMessages(infoMessages);
	}
	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}
   	



	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
    
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}
  
	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		  backScreen = "cancelAdd";
		return backScreen;
    }
	
	public String saveInquiry() {
		logger.logInfo("cancel() started...");
		infoMessages  = new ArrayList<String>();
		Boolean flage = false;
		
		errorMessages.clear();
		
		RequestServiceAgent rsa = new RequestServiceAgent();
		
		try {

		//tenderInquiryView.setRequestNumber(applicationNumber);
		//tenderInquiryView.setRecordStatus(Long.parseLong(applicationStatus));
		tenderInquiryView.setRequestDate(applicationDate);
		if(getApplicationSourceId()!=null && !getApplicationSourceId().equals("")&& !getApplicationSourceId().equals("-1"))
			tenderInquiryView.setRequestSource(Long.parseLong(getApplicationSourceId()));
		else{
			flage = true;
			errorMessages.add(getBundleMessage("tenderInquiryAdd.msg.applicationSource"));
		}
		
		if(hdnContractorId!=null && !hdnContractorId.equals("")){
			tenderInquiryView.setContractorNameEn(hdnContractorNameEn);
			tenderInquiryView.setContractorNameAr(hdnContractorNameAr);
			tenderInquiryView.setContractorNumber(hdnContractorNumber);
			tenderInquiryView.setContractorId(Long.parseLong(hdnContractorId));
		}else{
			flage = true;
			errorMessages.add(getBundleMessage("tenderInquiryAdd.msg.contractor"));
		}

		if(hdnTenderId!=null && !hdnTenderId.equals("")){
			tenderInquiryView.setTenderId(Long.parseLong(hdnTenderId));
			tenderInquiryView.setTenderNumber(hdnTenderNumber);
			tenderInquiryView.setTenderDescription(hdnTenderDescription);
		}else{
			flage = true;
			errorMessages.add(getBundleMessage("tenderInquiryAdd.msg.tender"));
		}
		tenderInquiryView.setRequestDescription(reasons);
		tenderInquiryView.setUpdatedBy(getLoggedInUser());
		tenderInquiryView.setUpdatedOn(new Date());
		tenderInquiryView.setCreatedBy(getLoggedInUser());
		tenderInquiryView.setCreatedOn(new Date());
		tenderInquiryView.setRecordStatus(1L);
		tenderInquiryView.setIsDeleted(0L);
		tenderInquiryView.setStatusId((Long)viewMap.get("TENDER_INQUIRY_STATUS"));
		
		if(!flage){
			
		if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))	
		map.put("FROM_CONSTRUCTION_TENDER", true);
		
    	tenderInquiryView = rsa.saveTenderInquiryRequest(tenderInquiryView,map);
    	infoMessages.add(getBundleMessage("tenderInquiryAdd.msg.save"));
    	infoMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.REQUEST_NUMBER) + " " + tenderInquiryView.getRequestNumber());
    	// after adding request successfully save show request number and status 
    	applicationNumber = tenderInquiryView.getRequestNumber();
    	
    	if(getIsEnglishLocale())
    	applicationStatus = tenderInquiryView.getStatusEn();
    	else
    	applicationStatus = tenderInquiryView.getStatusEn();
    	
    	
    	
    	CommonUtil.loadAttachmentsAndComments(tenderInquiryView.getRequestId().toString());
		Boolean attachSuccess = CommonUtil.saveAttachments(tenderInquiryView.getRequestId());
		Boolean commentSuccess = CommonUtil.saveComments(tenderInquiryView.getRequestId(), noteOwner);
    	
    	PIMSTenderInquiryBPELPortClient client = new PIMSTenderInquiryBPELPortClient();
		
		SystemParameters parameters = SystemParameters.getInstance();			
		String endPoint = parameters.getParameter(WebConstants.TENDER_INQUIRY_BPEL_END_POINT);
		System.out.println("End Point::::::::::::"+endPoint); 
		client.setEndpoint(endPoint);
		client.initiate(tenderInquiryView.getRequestId(),getLoggedInUser(), null,null);
		
		}else{
         return "SaveNotDone";
		}
    	

		
		} catch (PimsBusinessException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
    }
	
	public String onReset() 
	{
		String path = null;		
		try {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
		response.sendRedirect("tenderInquiryAdd.jsf");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getApplicationNumber() {
	    if(viewMap.containsKey("APPLICATION_NUMBER"))
	    	applicationNumber=(String)viewMap.get("APPLICATION_NUMBER");
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
		
		if(this.applicationNumber!=null)
			viewMap.put("APPLICATION_NUMBER", this.applicationNumber);
		
	}

	public String getApplicationStatus() {
		if(viewMap.containsKey("APPLICATION_STATUS"))
			applicationStatus = (String)viewMap.get("APPLICATION_STATUS");
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
		
		if(this.applicationStatus!=null)
			viewMap.put("APPLICATION_STATUS",this.applicationStatus);
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationSourceId() {
		if(viewMap.containsKey("APPLICATION_SOURCE"))
			applicationSourceId  = (String)viewMap.get("APPLICATION_SOURCE");
		return applicationSourceId;
	}

	public void setApplicationSourceId(String applicationSourceId) {
		this.applicationSourceId = applicationSourceId;
		
		if(this.applicationSourceId!=null)
			viewMap.put("APPLICATION_SOURCE", this.applicationSourceId);
	}

	public String getContractorName() {
		return contractorName;
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}

	public String getContractorNo() {
		return contractorNo;
	}

	public void setContractorNo(String contractorNo) {
		this.contractorNo = contractorNo;
	}

	public String getTenderNumber() {
		return tenderNumber;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public String getTenderDes() {
		return tenderDes;
	}

	public void setTenderDes(String tenderDes) {
		this.tenderDes = tenderDes;
	}

	public String getReasons() {
		if(viewMap.containsKey("REASONS"))
			reasons = (String)viewMap.get("REASONS");
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
		
		if(this.reasons!=null)
			viewMap.put("REASONS", this.reasons);
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public String getHdnContractorNumber() {
		return hdnContractorNumber;
	}

	public void setHdnContractorNumber(String hdnContractorNumber) {
		this.hdnContractorNumber = hdnContractorNumber;
	}

	public String getHdnContractorNameEn() {
		return hdnContractorNameEn;
	}

	public void setHdnContractorNameEn(String hdnContractorNameEn) {
		this.hdnContractorNameEn = hdnContractorNameEn;
	}

	public String getHdnContractorNameAr() {
		return hdnContractorNameAr;
	}

	public void setHdnContractorNameAr(String hdnContractorNameAr) {
		this.hdnContractorNameAr = hdnContractorNameAr;
	}

	public String getHdnTenderId() {
		return hdnTenderId;
	}

	public void setHdnTenderId(String hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	public String getHdnTenderNumber() {
		return hdnTenderNumber;
	}

	public void setHdnTenderNumber(String hdnTenderNumber) {
		this.hdnTenderNumber = hdnTenderNumber;
	}

	public String getHdnTenderDescription() {
		return hdnTenderDescription;
	}

	public void setHdnTenderDescription(String hdnTenderDescription) {
		this.hdnTenderDescription = hdnTenderDescription;
	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	

	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	
	
	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}
	
	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}
	
	public boolean getIsViewModePopUp()
	{
		boolean returnVal = false;
		
		if(viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE)!=null)
		{
			if(viewMap.get(VIEW_MODE).equals(MODE_POPUP)) 
		    { 	      	    		
			 returnVal =  true;
	    	}
		}
		 
		else 
		{
		 returnVal =  false;
		}
		
		return returnVal;
		
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	
	public void populateRequestFields(String requestId){
		
		RequestServiceAgent rsa = new RequestServiceAgent();
		
		try {
			tenderInquiryView = rsa.getRequestByIdForTenderInquiry(Long.parseLong(requestId));
             
			
			setApplicationNumber(tenderInquiryView.getRequestNumber());
			if(getIsEnglishLocale())
			setApplicationStatus(tenderInquiryView.getStatusEn());
		    else
		    setApplicationStatus(tenderInquiryView.getStatusAr());
			
			setApplicationDate(tenderInquiryView.getRequestDate());
			
			setApplicationSourceId(tenderInquiryView.getRequestSource().toString());
			
			
			if(getIsEnglishLocale()){
			setContractorName(tenderInquiryView.getContractorNameEn());
			viewMap.put("PERSON_NAME", tenderInquiryView.getContractorNameEn());
			}
			else{
			setContractorName(tenderInquiryView.getContractorNameAr());
			viewMap.put("PERSON_NAME", tenderInquiryView.getContractorNameAr());
			}
			
			viewMap.put("TENDER_NUMBER", tenderInquiryView.getTenderNumber());
			
			setContractorNo(tenderInquiryView.getContractorNumber());
			setTenderNumber(tenderInquiryView.getTenderNumber());
			setTenderDes(tenderInquiryView.getTenderDescription());
			setReasons(tenderInquiryView.getRequestDescription());

			//hdn variable for contractor and tender  
			this.hdnContractorId= tenderInquiryView.getContractorId().toString();
			viewMap.put("PERSON_ID", tenderInquiryView.getContractorId());
			
			this.hdnContractorNumber= tenderInquiryView.getContractorNumber();
			this.hdnContractorNameEn= tenderInquiryView.getContractorNameEn();
			this.hdnContractorNameAr= tenderInquiryView.getContractorNameAr();
			this.hdnTenderId= tenderInquiryView.getTenderId().toString();
			this.hdnTenderNumber= tenderInquiryView.getTenderNumber();
			this.hdnTenderDescription= tenderInquiryView.getTenderDescription();
			this.hdnRequestId= tenderInquiryView.getRequestId().toString();
			
			viewMap.put("EMAIL_LIST", tenderInquiryView.getEmailList());
		
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PimsBusinessException e) {
			e.printStackTrace();
		}
		
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}

	public Boolean getInViewMode(){
		
		if(viewMap.containsKey("IN_VIEW_MODE"))
			return true;
		else
			return false;
		
	}
	
	public String getInReadOnlyMode(){
		
		if(viewMap.containsKey("IN_READ_ONLY_MODE"))
			return "READONLY";
		else
			return "";
	}
	
	public String onSearchTender() 
	{
		String path = null;
		if(viewMap.containsKey("FROM_CONSTRUCTION_TENDER"))
		openPopup("showSearchConstructionTenderPopUp();");
		else
		openPopup("showSearchTenderPopUp();");
		
		
		
		
		return path;
	}
	
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public String getHdnLicencseNumber() {
		return hdnLicencseNumber;
	}

	public void setHdnLicencseNumber(String hdnLicencseNumber) {
		this.hdnLicencseNumber = hdnLicencseNumber;
	}

	public String getLicencseNumber() {
		return licencseNumber;
	}

	public void setLicencseNumber(String licencseNumber) {
		this.licencseNumber = licencseNumber;
	}
	
}