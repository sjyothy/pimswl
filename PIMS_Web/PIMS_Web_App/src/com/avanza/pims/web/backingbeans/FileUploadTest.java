package com.avanza.pims.web.backingbeans;
import java.util.Date;
import java.util.Map;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.HtmlHiddenRenderer;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.dao.DocumentManager;
import com.avanza.pims.entity.Document;
import com.avanza.pims.entity.DocumentContent;
import com.avanza.pims.web.controller.AbstractController;
import com.evermind.server.ejb.interceptor.joinpoint.SessionLifecycle.SetSessionContextJoinPointImpl;


public class FileUploadTest extends AbstractController {

	private static Logger logger = Logger.getLogger( FileUploadTest.class );
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	
	private HtmlInputHidden docId = new HtmlInputHidden();
	
	public FileUploadTest (){		
	}	
	public void init() 
    {   	
   	 super.init();
   	 	        
	}
	public String UploadFile()
	{		
		
		Long documentId = Upload();
		docId.setValue(documentId);
		//outcome = Download(documentId);		
		return "";
	}
	public String Download()
	{
		DocumentManager documentManager = new DocumentManager();
		try {
				
				Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
				Long documentId = new Long(56);
				if(docId.getValue() != null)
					documentId = Long.parseLong(docId.getValue().toString());
				sessionMap.put("documentId", documentId.toString());
							
		     } catch (Exception e) {			
			e.printStackTrace();
		}
		return "DownloadFile";
	}
	public Long Upload()
	{
		Long documentId = null;
		try
		{
		if(fileUploadCtrl.getUploadedFile() != null)
		{			
			UploadedFile file =  fileUploadCtrl.getUploadedFile();
			Document document = new Document();
			
			String updatedBy = "admin";
			String createdBy = "admin";
			Date updatedOn = new Date();
			Date createdOn = new Date();
			String associatedObjectId = "123";
			String documentFileName = getFileNameForPath(file.getName());
			String documentPath = file.getName();
			String documentTitle = "Test File Upload";
			String documentType = file.getContentType();
			String externalId = "";
			String repositoryId = "";
			Long sizeInBytes = file.getSize();
			
			document.setUpdatedBy(updatedBy);
			document.setUpdatedOn(updatedOn);
			document.setCreatedOn(createdOn);
			document.setCreatedBy(createdBy);
			document.setAssociatedObjectId(associatedObjectId);
			document.setDocumentFileName(documentFileName);
			document.setDocumentPath(documentPath);
			document.setDocumentTitle(documentTitle);
//			document.setDocumentType(documentType);
			document.setExternalId(externalId);
			document.setRepositoryId(repositoryId);
			document.setSizeInBytes(sizeInBytes);
			document.setIsDeleted(new Long(0));
			document.setRecordStatus(new Long(0));
			
			DocumentManager documentManager = new DocumentManager();
			
			//documentId = (documentManager.addAttachment(document, file.getInputStream())).getDocumentId();
			
		 }
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
		}
		
		return documentId;			
	}
	private String getFileNameForPath (String path) {
        
        if ( path == null || path.equalsIgnoreCase("") ) {
            return path;
         }        
        return path.substring( path.lastIndexOf("\\") + 1, path.length() );
    }
	
public String getLocale(){
	WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getLanguageCode();
}

public String getDateFormat(){
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getDateFormat();
}
public HtmlInputFileUpload getFileUploadCtrl() {
	return fileUploadCtrl;
}
public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
	this.fileUploadCtrl = fileUploadCtrl;
}
public HtmlInputHidden getDocId() {
	return docId;
}
public void setDocId(HtmlInputHidden docId) {
	this.docId = docId;
}
}
