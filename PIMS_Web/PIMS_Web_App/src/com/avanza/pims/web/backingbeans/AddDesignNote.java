package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.construction.project.ProjectService;
import com.avanza.pims.ws.vo.DesignNoteView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.ui.util.ResourceUtil;


public class AddDesignNote extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AddDesignNote.class);
	private final String DESIGN_NOTE_VIEW ="DESIGN_NOTE_VIEW";
	private final String DEFAULT_COMBO_VALUE ="-1";
	
		
	private final String HEADING = "HEADING";
	private final String DEFAULT_HEADING = "designNote.header";
	
	private String heading="";

	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	

	// Methods
	public String getDesignNoteType() {
		String designNoteType = DEFAULT_COMBO_VALUE;
		DesignNoteView designNoteView = getDesignNoteView();
		if(designNoteView != null && designNoteView.getNoteTypeId() != null)
			designNoteType = designNoteView.getNoteTypeId().toString();
		return designNoteType;
	}

	public void setDesignNoteType(String designNoteType) {
		if(!designNoteType.equals(""))
		{
			DesignNoteView designNoteView = getDesignNoteView();
			if(designNoteView != null )
			{
				designNoteView.setNoteTypeId(Long.parseLong(designNoteType));
			}
		}
	}

	public String getDesignNoteStatus() {
		String designNoteStatus = DEFAULT_COMBO_VALUE;
		DesignNoteView designNoteView = getDesignNoteView();
		if(designNoteView != null && designNoteView.getStatusId() != null)
			designNoteStatus = designNoteView.getStatusId().toString();
		return designNoteStatus;
	}

	public void setDesignNoteStatus(String designNoteStatus) {
		if(!designNoteStatus.equals(""))
		{
			DesignNoteView designNoteView = getDesignNoteView();
			if(designNoteView != null )
			{
				designNoteView.setStatusId(Long.parseLong(designNoteStatus));
			}
		}
	}

	public String getDesignNoteOwner() {
		String designNoteOwner = DEFAULT_COMBO_VALUE;
		DesignNoteView designNoteView = getDesignNoteView();
		if(designNoteView != null && designNoteView.getNoteOwnerId() != null)
			designNoteOwner = designNoteView.getNoteOwnerId().toString();
		return designNoteOwner;
	}

	public void setDesignNoteOwner(String designNoteOwner) {
		if(!designNoteOwner.equals(""))
		{
			DesignNoteView designNoteView = getDesignNoteView();
			if(designNoteView != null )
			{
				designNoteView.setNoteOwnerId(Long.parseLong(designNoteOwner));
			}
		}
	}
	public DesignNoteView getDesignNoteView() {
		
		DesignNoteView designNoteView = null;
		
		if(viewMap.containsKey(DESIGN_NOTE_VIEW))
		{
			 Object obj = viewMap.get(DESIGN_NOTE_VIEW);
			 
			 if(obj != null)
				 designNoteView = (DesignNoteView)obj;
		}
		if(designNoteView == null)
		{
			designNoteView = new DesignNoteView();
			setDesignNoteView(designNoteView);
		}
		
		return designNoteView;
	}

	public void setDesignNoteView(DesignNoteView designNoteView) {
		viewMap.put(DESIGN_NOTE_VIEW, designNoteView);
	}
	
	
	public ProjectView getProjectView() {
		
		ProjectView projectView = null;
		
		if(viewMap.containsKey(WebConstants.PROJECT_VIEW))
		{
			 Object obj = viewMap.get(WebConstants.PROJECT_VIEW);
			 
			 if(obj != null)
				 projectView = (ProjectView)obj;
		}		
		
		return projectView;
	}
	public void init() {
		logger.logInfo("init() started...");
		super.init();
		if(!isPostBack())
		{
			ProjectView pv = (ProjectView) sessionMap.get(WebConstants.PROJECT_VIEW);
			if(pv!=null)
				viewMap.put(WebConstants.PROJECT_VIEW, pv);
			sessionMap.remove(WebConstants.PROJECT_VIEW);
			
			if(sessionMap.containsKey(WebConstants.DESIGN_NOTE_VIEW))
			{
				DesignNoteView designNoteView = (DesignNoteView) sessionMap.get(WebConstants.DESIGN_NOTE_VIEW);
				setDesignNoteView(designNoteView);
			}
		}

		try
		{
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}
	}

	
	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		//applyMode();
	}

	@SuppressWarnings("unchecked")
	public Boolean validateForSave() throws Exception{
	    logger.logInfo("validateForSave() started...");
	    Boolean validated = true;
	    try
		{	  	
			DesignNoteView designNoteView = getDesignNoteView();
			if(designNoteView != null)
			{
				Object noteDate = designNoteView.getNoteDate();
				Object status = designNoteView.getStatusId();
				Object noteType = designNoteView.getNoteTypeId();
				Object noteOwner = designNoteView.getNoteOwnerId();
				Object note = designNoteView.getNote();
				Object closingDate = designNoteView.getClosingDate();
				Object closingNote = designNoteView.getClosingNote();
				
				if(noteDate==null || noteDate.toString().equals(""))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_DATE));
					validated = false;
				}
				if(status==null || status.toString().equals("") || status.toString().equals(DEFAULT_COMBO_VALUE))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_STATUS));
					validated = false;
				}
				if(noteType==null || noteType.toString().equals("") || noteType.toString().equals(DEFAULT_COMBO_VALUE))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_TYPE));
					validated = false;
				}
				if(noteOwner==null || noteOwner.toString().equals("") || noteOwner.toString().equals(DEFAULT_COMBO_VALUE) )
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_OWNER));
					validated = false;
				}
				if(note==null || note.toString().equals(""))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE));
					validated = false;
				}
				if(closingDate==null || closingDate.toString().equals(""))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_CLOSING_DATE));
					validated = false;
				}
				if(closingNote==null || closingNote.toString().equals(""))
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.ProjectFollowUp.RequiredFields.DESIGN_NOTE_CLOSING_NOTE));
					validated = false;
				}
			}

			logger.logInfo("validateForSave() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForSave() crashed ", exception);
			throw exception;
		}
		return validated;
	}
    
	public void persistProjectDesignNote(DesignNoteView designNoteView) throws Exception
	{
		logger.logInfo("persistDesignNote() started...");
    	try{
    		
    		String loggedInUser = getLoggedInUserId();
			ProjectService ps = new ProjectService();    			
			ProjectView pv = getProjectView();
			designNoteView.setProjectId(pv.getProjectId());
			
			designNoteView.setUpdatedBy(loggedInUser);
			designNoteView.setUpdatedOn(new Date());
			if(designNoteView.getDesignNoteId() == null)
			{
				designNoteView.setCreatedBy(loggedInUser);
    			designNoteView.setCreatedOn(new Date());    			
			}
			ps.persistDesignNote(designNoteView);
    			
    		logger.logInfo("persistDesignNote() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("persistDesignNote() crashed ",exception);
    		throw exception;
    	}
    	catch(Exception exception){
    		logger.LogException("persistDesignNote() crashed ",exception);
    		throw exception;
    	}
	}
	public String persistDesignNote()
    {
    	logger.logInfo("persistDesignNote() started...");
    	try{
    		DesignNoteView designNoteView = getDesignNoteView();
    		if(validateForSave())
    		{
    			designNoteView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
    			designNoteView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
    			persistProjectDesignNote(designNoteView);   			
    			sessionMap.put("DESIGN_NOTE_ADDED", true);
    			RegisterJavaScript("javascript:closeWindowSubmit();");
    	   		//errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.RegionDetails.REGION_SAVE_FAILURE));
    		}
    			
    		logger.logInfo("persistDesignNote() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("persistDesignNote() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("persistDesignNote() crashed ",exception);
    	}
        return "persistDesignNote";
    }
	
	private void RegisterJavaScript(String javaScriptText )
	{
		final String viewId = "/addDesignNote.jsp"; 
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);  
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
    /*
	 * Setters / Getters
	 */

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the heading
	 */
	public String getHeading() {
		heading = (String) viewMap.get(HEADING);
		if(StringHelper.isEmpty(heading))
			heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
		else
			heading = CommonUtil.getBundleMessage(heading);
		return heading;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}


	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	

	
}

