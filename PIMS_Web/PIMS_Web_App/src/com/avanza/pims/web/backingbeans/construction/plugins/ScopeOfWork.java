package com.avanza.pims.web.backingbeans.construction.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.TenderWorkScopeView;
import com.avanza.pims.ws.vo.WorkTypeView;

public class ScopeOfWork extends AbstractController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7389005902359716562L;
	
	// Commons
	private Map<String, Object> viewMap = getFacesContext().getViewRoot().getAttributes();	
	private ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private ApplicationBean applicationBean = new ApplicationBean();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	// Scope of Work Tab
	
	// For Work Type Drop Down
	private HtmlSelectOneMenu workTypeMenu = new HtmlSelectOneMenu();
	private List<SelectItem> workTypeItems = new ArrayList<SelectItem>();
	private List<WorkTypeView> workTypeViewList = new ArrayList<WorkTypeView>();	
	
	// For Tender Work Scope Description
	private HtmlInputTextarea scopeWorkDescription = new HtmlInputTextarea();
	
	// For Tender Work Scope Data Grid
	private HtmlDataTable scopeOfWorkDataTable = new HtmlDataTable();
	private List<TenderWorkScopeView> scopeOfWorkList= new ArrayList<TenderWorkScopeView>();
	private HtmlGraphicImage imgdeleteLink = new HtmlGraphicImage();
	// For Saving Tender Work Scope
	private TenderWorkScopeView tenderWorkScopeView = new TenderWorkScopeView(); 
	private HtmlCommandButton saveScopeOfWork = new HtmlCommandButton();
	
	public ScopeOfWork() {		
		loadScreenData();
		if(viewMap.containsKey("canAddWorkScope")&&
				Boolean.valueOf(viewMap.get("canAddWorkScope").toString())){
			saveScopeOfWork.setRendered(true);
			
		}
		else{
			saveScopeOfWork.setRendered(false);
		}
	}
	
	public void init(){
		super.init();		
	}
	
	public void preprocess(){
		super.preprocess();		
	}
	
	public void prerender(){
		super.prerender();		
	}
	
	private void loadScreenData() 
	{
		try 
		{
			updateListsFromViewRoot();			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public String saveScopeOfWork_action() {		
		if(applyScreenData(tenderWorkScopeView))
		{
		scopeOfWorkList.add(tenderWorkScopeView);
		workTypeViewList.remove( tenderWorkScopeView.getWorkTypeView() );
		workTypeItems = applicationBean.getWorkTypeItems(workTypeViewList);
		updateListsToViewRoot();
		}
		return null;		
	}
	
	public String deleteLink_action() 
	{		
		TenderWorkScopeView selectedTenderWorkScopeView = (TenderWorkScopeView) scopeOfWorkDataTable.getRowData();
		
		if(!(selectedTenderWorkScopeView.getTenderWorkScopeId()!=null))
		scopeOfWorkList.remove( selectedTenderWorkScopeView );
		else
		selectedTenderWorkScopeView.setIsDeleted(1L);
		
		workTypeViewList.add( selectedTenderWorkScopeView.getWorkTypeView() );
		workTypeItems = applicationBean.getWorkTypeItems(workTypeViewList);
		updateListsToViewRoot();
		return null;
	}
	
	private Boolean applyScreenData(TenderWorkScopeView tenderWorkScopeView)
	{
		Boolean canAdd= true;
		if ( workTypeMenu.getValue() != null && ! workTypeMenu.getValue().equals("-1") )
		{
			tenderWorkScopeView.setWorkTypeView( findWorkTypeView( workTypeViewList, Long.parseLong( (String) workTypeMenu.getValue() ) ) );
		}else
			return false;
		
		if ( scopeWorkDescription.getValue() != null && ! scopeWorkDescription.getValue().equals("") )
		{
			tenderWorkScopeView.setDescription( (String) scopeWorkDescription.getValue() );	
		}else
			return false;
		
		tenderWorkScopeView.setCreatedBy(CommonUtil.getLoggedInUser());
		tenderWorkScopeView.setCreatedOn(new Date());
		tenderWorkScopeView.setUpdatedBy(CommonUtil.getLoggedInUser());
		tenderWorkScopeView.setUpdatedOn(new Date());
		tenderWorkScopeView.setIsDeleted(new Long(0));
		tenderWorkScopeView.setRecordStatus(new Long(1));
		
		return true;
	}
	
	private WorkTypeView findWorkTypeView( List<WorkTypeView> workTypeViewList, Long workTypeId )
	{
		for ( WorkTypeView workTypeView : workTypeViewList )
			if ( workTypeView.getWorkTypeId().equals(workTypeId) )
				return workTypeView;
		
		return null;
	}
	
	 
	private void updateListsFromViewRoot() throws Exception {
		
		if( viewMap.get(WebConstants.Tender.WORK_TYPE_VIEW_LIST) != null )
			workTypeViewList = (List<WorkTypeView>) viewMap.get(WebConstants.Tender.WORK_TYPE_VIEW_LIST);
		else		
			workTypeViewList = constructionServiceAgent.getAllWorkTypes();
		
		if( viewMap.get(WebConstants.Tender.WORK_TYPE_ITEMS) != null )
			workTypeItems = (List<SelectItem>) viewMap.get(WebConstants.Tender.WORK_TYPE_ITEMS);
		else		
			workTypeItems = applicationBean.getWorkTypeItems(workTypeViewList);
		
		if( viewMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST) != null )
			scopeOfWorkList = (List<TenderWorkScopeView>) viewMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST);
		else
			scopeOfWorkList = new ArrayList<TenderWorkScopeView>();
		
		updateListsToViewRoot();
	}
	
	private void updateListsToViewRoot() 
	{
		viewMap.put(WebConstants.Tender.WORK_TYPE_VIEW_LIST, workTypeViewList);
		
		viewMap.put(WebConstants.Tender.WORK_TYPE_ITEMS, workTypeItems);
		viewMap.put(WebConstants.Tender.SCOPE_OF_WORK_LIST, scopeOfWorkList);
		viewMap.put("recordSize",scopeOfWorkList.size());
	}
	
	public HtmlDataTable getScopeOfWorkDataTable() {
		return scopeOfWorkDataTable;
	}
	public void setScopeOfWorkDataTable(HtmlDataTable scopeOfWorkDataTable) {
		this.scopeOfWorkDataTable = scopeOfWorkDataTable;
	}
	public HtmlInputTextarea getScopeWorkDescription() {
		return scopeWorkDescription;
	}
	public void setScopeWorkDescription(HtmlInputTextarea scopeWorkDescription) {
		this.scopeWorkDescription = scopeWorkDescription;
	}
	public List<SelectItem> getWorkTypeItems() {
		return workTypeItems;
	}
	public void setWorkTypeItems(List<SelectItem> workTypeItems) {
		this.workTypeItems = workTypeItems;
	}
	public HtmlSelectOneMenu getWorkTypeMenu() {
		return workTypeMenu;
	}
	public void setWorkTypeMenu(HtmlSelectOneMenu workTypeMenu) {
		this.workTypeMenu = workTypeMenu;
	}
	public HtmlCommandButton getSaveScopeOfWork() {
		return saveScopeOfWork;
	}
	public void setSaveScopeOfWork(HtmlCommandButton saveScopeOfWork) {
		this.saveScopeOfWork = saveScopeOfWork;
	}
	public List<TenderWorkScopeView> getScopeOfWorkList() {
		
		if( viewMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST) != null )
			scopeOfWorkList = (List<TenderWorkScopeView>) viewMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST);
		
		return scopeOfWorkList;
	}
	public void setScopeOfWorkList(List<TenderWorkScopeView> scopeOfWorkList) {
		this.scopeOfWorkList = scopeOfWorkList;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public List<WorkTypeView> getWorkTypeViewList() {
		return workTypeViewList;
	}

	public void setWorkTypeViewList(List<WorkTypeView> workTypeViewList) {
		this.workTypeViewList = workTypeViewList;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public ConstructionServiceAgent getConstructionServiceAgent() {
		return constructionServiceAgent;
	}

	public void setConstructionServiceAgent(
			ConstructionServiceAgent constructionServiceAgent) {
		this.constructionServiceAgent = constructionServiceAgent;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public TenderWorkScopeView getTenderWorkScopeView() {
		return tenderWorkScopeView;
	}

	public void setTenderWorkScopeView(TenderWorkScopeView tenderWorkScopeView) {
		this.tenderWorkScopeView = tenderWorkScopeView;
	}
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public HtmlGraphicImage getImgdeleteLink() {
		return imgdeleteLink;
	}

	public void setImgdeleteLink(HtmlGraphicImage imgdeleteLink) {
		this.imgdeleteLink = imgdeleteLink;
	}
	
	public Boolean getCanDeleteTenderScopeOfWork()
	{
        if (viewMap.get("CAN_DELETE_TENDER_SCOPE_OF_WORK")!=null)
        	return false;
        else
        	return true;
        	
	}
	
	public Boolean getCanAddTenderScopeOfWork()
	{
        if (viewMap.get("CAN_ADD_TENDER_SCOPE_OF_WORK")!=null)
        	return false;
        else
        	return true;
        	
	}
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
}
