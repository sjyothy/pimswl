package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.context.ViewResources;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;

public class inspectionSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	public String selectedType;
	public String selectedStatus;
	public Date fromDate;
	public Date toDate;
	public String txtpropertyRefNum;
	public String txtunitRefNum;
	public String txtinspectionNum;
	public String txtcontractNum;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(inspectionSearchBean.class);
	private List<InspectionView> dataList;
	InspectionView inspectionView = new InspectionView();
	private HtmlDataTable tbl_InspectionSearch;
	private InspectionView dataItem = new InspectionView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	
	private HtmlInputText htmlInspectionNumber = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneInspectionStatus = new HtmlSelectOneMenu();
	private HtmlCalendar htmlCalendarInspectionFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarInspectionTo = new HtmlCalendar();
	private HtmlSelectOneMenu htmlSelectOneInspectionType = new HtmlSelectOneMenu();
	private HtmlInputText htmlInspectorName = new HtmlInputText();
	private HtmlInputText htmlPropertyName = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOnePropertyType = new HtmlSelectOneMenu();
	private HtmlInputText htmlContractNumber = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneContractType = new HtmlSelectOneMenu();
	private HtmlInputText htmlTenantName = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOneTenantType = new HtmlSelectOneMenu();
	private HtmlInputText htmlUnitNumber = new HtmlInputText();
	
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	// Constructors

	/** default constructor */
	public inspectionSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		String method="getIsEnglishLocale";
		logger.logInfo(method+"|"+"Start...");
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		logger.logInfo(method+"|"+"Finish...");
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public HtmlDataTable getTbl_InspectionSearch() {
		return tbl_InspectionSearch;
	}

	public void setTbl_InspectionSearch(HtmlDataTable tbl_InspectionSearch) {
		this.tbl_InspectionSearch = tbl_InspectionSearch;
	}

	public InspectionView getDataItem() {
		return dataItem;
	}

	public void setDataItem(InspectionView dataItem) {
		this.dataItem = dataItem;
	}

	private boolean putControlValuesInView() throws Exception {

		String methodName = "putControlValuesInView";
		logger.logInfo(methodName + "|" + "Start");
		boolean isError=true;
		
		InspectionUnitView inspectionUnitView = new InspectionUnitView();
		PropertyView propertyView = new PropertyView();
		UnitView unitView = new UnitView();
		ContractUnitView contractUnitView = new ContractUnitView();
		ContractView contractView = new ContractView();
		
		try {
			inspectionView = new InspectionView();

			DateFormat DF=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			inspectionView.setInspectionFrom(fromDate);
			inspectionView.setInspectionTo(toDate);
			logger.logInfo(methodName + "|" + "Is Deleted:0");
			// The value is set to 0 so that only those items are selected that
			// are not logically deleted
			inspectionView.setIsDeleted(new Long(0));
			logger.logInfo(methodName + "|" + "Inspection Number:"+txtinspectionNum);
		   //inspectionView.setInspectionDate(new Date(fromDate));
			if (txtinspectionNum!= null && txtinspectionNum.length()>0)
				inspectionView.setInspectionNumber(txtinspectionNum);
			
			logger.logInfo(methodName + "|" + "selectedStatus:"+selectedStatus);
			if (selectedStatus != null && !selectedStatus.equals("-1"))
				inspectionView.setStatus(new Long(selectedStatus));
			
			logger.logInfo(methodName + "|" + "selectedType:" + selectedType);
			if (selectedType != null && !selectedType.equals("-1"))
				inspectionView.setType(new Long(selectedType));
			
			logger.logInfo(methodName + "|" + "txtunitRefNum:" + txtunitRefNum);
			if (txtunitRefNum != null && txtunitRefNum.trim().length() > 0) 
			{
				unitView.setUnitNumber(txtunitRefNum.trim());
				inspectionUnitView.setUnitView(unitView);
			}
			
			logger.logInfo(methodName + "|" + "txtpropertyRefNum:" + txtpropertyRefNum);
			if (txtpropertyRefNum!= null && txtpropertyRefNum.trim().length() > 0) 
			{

				//UnitNumber(txtunitRefNum.trim());
				inspectionUnitView.setUnitView(unitView);
			}
			
			logger.logInfo(methodName + "|" + "txtcontractNum:"
					+ txtcontractNum);
			if (txtcontractNum != null && txtcontractNum.trim().length() > 0) 
			{
				inspectionUnitView.setContractNumber(txtcontractNum);
			}

			inspectionView.getInspectionUnitsView().add(inspectionUnitView);
		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
			throw e;

		}

		logger.logInfo(methodName + "|" + "Finsih");
		return isError;
	}

	public String cmdDetails_Click()
	{
		String methodName = "cmdDetails_Click";
        String outCome="";		
        logger.logInfo(methodName+"started...");
        try{

        	InspectionView inspectionView=(InspectionView)tbl_InspectionSearch.getRowData();
        	setRequestParam("InspectionView", inspectionView);
        	outCome="inspectionDetails";
			logger.logInfo(methodName+"completed successfully...");
		}
		catch (Exception e) {
			logger.LogException(methodName+ "crashed...", e);			
		}
		return outCome;
	}
	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		try {

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "inspectionDetails";
	}
	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();
		
		String emptyValue = "-1";		
		Object inspectionNumber = htmlInspectionNumber.getValue();
		Object inspectionStatus = htmlSelectOneInspectionStatus.getValue();
		Object inspectionDateFrom = htmlCalendarInspectionFrom.getValue();
		Object inspectionDateTo = htmlCalendarInspectionTo.getValue();
		Object inspectionType = htmlSelectOneInspectionType.getValue();
		Object inspectorName = htmlInspectorName.getValue();
		Object propertyName = htmlPropertyName.getValue();
		Object propertyType = htmlSelectOnePropertyType.getValue();
		Object contractNumber = htmlContractNumber.getValue();
		Object contractType = htmlSelectOneContractType.getValue();
		Object tenantName = htmlTenantName.getValue();
		Object tenantType = htmlSelectOneTenantType.getValue();
		Object unitNumber = htmlUnitNumber.getValue();
		
		if(inspectionNumber != null && !inspectionNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTION_NUMBER, inspectionNumber.toString());
		}		
		if(inspectionStatus != null && !inspectionStatus.toString().trim().equals("") && !inspectionStatus.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTION_STATUS, inspectionStatus.toString());
		}
		if(inspectionDateFrom != null && !inspectionDateFrom.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTION_DATE_FROM, inspectionDateFrom);
		}		
		if(inspectionDateTo != null && !inspectionDateTo.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTION_DATE_TO,inspectionDateTo);
		}
		if(inspectionType != null && !inspectionType.toString().trim().equals("") && !inspectionType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTION_TYPE, inspectionType.toString());
		}
		if(inspectorName != null && !inspectorName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.INSPECTOR_NAME, inspectorName.toString());
		}		
		if(propertyName != null && !propertyName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.PROPERTY_NAME, propertyName.toString());
		}		
		if(propertyType != null && !propertyType.toString().trim().equals("") && !propertyType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.PROPERTY_TYPE, propertyType.toString());
		}
		if(contractNumber != null && !contractNumber.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.CONTRACT_NUMBER, contractNumber.toString());
		}		
		if(contractType != null && !contractType.toString().trim().equals("") && !contractType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.CONTRACT_TYPE, contractType.toString());
		}
		if(tenantName != null && !tenantName.toString().trim().equals(""))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.TENANT_NAME, tenantName.toString());
		}		
		if(tenantType != null && !tenantType.toString().trim().equals("") && !tenantType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.TENANT_TYPE, tenantType.toString());
		}
		if(unitNumber != null && !unitNumber.toString().trim().equals("") && !unitNumber.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.INSPECTION_SEARCH_CRITERIA.UNIT_NUMBER, unitNumber.toString());
		}
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
		Object inspectionNumber = htmlInspectionNumber.getValue();
		Object inspectionStatus = htmlSelectOneInspectionStatus.getValue();
		Object inspectionDateFrom = htmlCalendarInspectionFrom.getValue();
		Object inspectionDateTo = htmlCalendarInspectionTo.getValue();
		Object inspectionType = htmlSelectOneInspectionType.getValue();
		Object inspectorName = htmlInspectorName.getValue();
		Object propertyName = htmlPropertyName.getValue();
		Object propertyType = htmlSelectOnePropertyType.getValue();
		Object contractNumber = htmlContractNumber.getValue();
		Object contractType = htmlSelectOneContractType.getValue();
		Object tenantName = htmlTenantName.getValue();
		Object tenantType = htmlSelectOneTenantType.getValue();
		Object unitNumber = htmlUnitNumber.getValue();
		
		if(StringHelper.isNotEmpty(inspectionNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(inspectorName.toString()))
			return true;
		if(StringHelper.isNotEmpty(propertyName.toString()))
			return true;
		if(StringHelper.isNotEmpty(contractNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(tenantName.toString()))
			return true;
		if(StringHelper.isNotEmpty(unitNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(inspectionStatus.toString()) && !inspectionStatus.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(inspectionType.toString()) && !inspectionType.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(propertyType.toString()) && !propertyType.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(contractType.toString()) && !contractType.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(tenantType.toString()) && !tenantType.toString().equals("-1"))
			return true;
		if(inspectionDateFrom!=null)
			return true;
		if(inspectionDateTo!=null)
			return true;
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		try 
		{
			PropertyServiceAgent psa = new PropertyServiceAgent();
			HashMap<String,Object> searchCriteria = getSearchCriteria();
			if(searchCriteria.size() > 0 )
			{					
					List<InspectionView> inspectionViewList = psa.getInspectionsByCriteria(searchCriteria);
					dataList=inspectionViewList;
					if(dataList!=null)
						recordSize = dataList.size();
					viewRootMap.put("recordSize", recordSize);
					paginatorRows = getPaginatorRows();
					paginatorMaxPages = recordSize/paginatorRows;
					if((recordSize%paginatorRows)>0)
						paginatorMaxPages++;
					if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
						paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
					viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
					
					context.getViewRoot().getAttributes().put("VIEW_INSPECTION_LIST", dataList);
		
					if(inspectionViewList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
				else{
					errorMessages.clear();
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
				}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::", e);
		}
		return "";
	}

	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}

	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		String methodName="prerender";
		try
		{
		
			
			logger.logInfo(methodName+"|Start..");
			super.prerender();
			enableDisableControls();
			
			logger.logInfo(methodName+"|Finish..");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|Error Occured..",ex);
		}
	}
    private void enableDisableControls()
    {
    	String methodName="enableDisableControls";
    	logger.logInfo(methodName+"|Start..");
    
    	
    	logger.logInfo(methodName+"|Finish..");
    }
	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public String getTxtpropertyRefNum() {
		return txtpropertyRefNum;
	}

	public void setTxtpropertyRefNum(String txtpropertyRefNum) {
		this.txtpropertyRefNum = txtpropertyRefNum;
	}

	public String getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(String txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}

	public String getTxtinspectionNum() {
		return txtinspectionNum;
	}

	public void setTxtinspectionNum(String txtinspectionNum) {
		this.txtinspectionNum = txtinspectionNum;
	}

	public String getTxtcontractNum() {
		return txtcontractNum;
	}

	public void setTxtcontractNum(String txtcontractNum) {
		this.txtcontractNum = txtcontractNum;
	}

	public List<InspectionView> getDataList() {
		if (context.getViewRoot().getAttributes().containsKey("VIEW_INSPECTION_LIST")) 
		{
             dataList=(ArrayList)context.getViewRoot().getAttributes().get("VIEW_INSPECTION_LIST");
		}
		return dataList;

	}

	public void setDataList(List<InspectionView> dataList) {
		this.dataList = dataList;
	}

	

	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
    public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
    public LocaleInfo getLocaleInfo()
	{
		 String method="getLocaleInfo";
         logger.logDebug(method+"|"+"Start...");
	    	
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	     logger.logDebug(method+"|"+"Finish...");
	     return localeInfo;
		
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	 
	
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public HtmlInputText getHtmlInspectionNumber() {
		return htmlInspectionNumber;
	}

	public void setHtmlInspectionNumber(HtmlInputText htmlInspectionNumber) {
		this.htmlInspectionNumber = htmlInspectionNumber;
	}

	public HtmlSelectOneMenu getHtmlSelectOneInspectionStatus() {
		return htmlSelectOneInspectionStatus;
	}

	public void setHtmlSelectOneInspectionStatus(
			HtmlSelectOneMenu htmlSelectOneInspectionStatus) {
		this.htmlSelectOneInspectionStatus = htmlSelectOneInspectionStatus;
	}

	public HtmlCalendar getHtmlCalendarInspectionFrom() {
		return htmlCalendarInspectionFrom;
	}

	public void setHtmlCalendarInspectionFrom(
			HtmlCalendar htmlCalendarInspectionFrom) {
		this.htmlCalendarInspectionFrom = htmlCalendarInspectionFrom;
	}

	public HtmlCalendar getHtmlCalendarInspectionTo() {
		return htmlCalendarInspectionTo;
	}

	public void setHtmlCalendarInspectionTo(HtmlCalendar htmlCalendarInspectionTo) {
		this.htmlCalendarInspectionTo = htmlCalendarInspectionTo;
	}

	public HtmlSelectOneMenu getHtmlSelectOneInspectionType() {
		return htmlSelectOneInspectionType;
	}

	public void setHtmlSelectOneInspectionType(
			HtmlSelectOneMenu htmlSelectOneInspectionType) {
		this.htmlSelectOneInspectionType = htmlSelectOneInspectionType;
	}

	public HtmlInputText getHtmlInspectorName() {
		return htmlInspectorName;
	}

	public void setHtmlInspectorName(HtmlInputText htmlInspectorName) {
		this.htmlInspectorName = htmlInspectorName;
	}

	public HtmlInputText getHtmlPropertyName() {
		return htmlPropertyName;
	}

	public void setHtmlPropertyName(HtmlInputText htmlPropertyName) {
		this.htmlPropertyName = htmlPropertyName;
	}

	public HtmlSelectOneMenu getHtmlSelectOnePropertyType() {
		return htmlSelectOnePropertyType;
	}

	public void setHtmlSelectOnePropertyType(
			HtmlSelectOneMenu htmlSelectOnePropertyType) {
		this.htmlSelectOnePropertyType = htmlSelectOnePropertyType;
	}

	public HtmlInputText getHtmlContractNumber() {
		return htmlContractNumber;
	}

	public void setHtmlContractNumber(HtmlInputText htmlContractNumber) {
		this.htmlContractNumber = htmlContractNumber;
	}

	public HtmlSelectOneMenu getHtmlSelectOneContractType() {
		return htmlSelectOneContractType;
	}

	public void setHtmlSelectOneContractType(
			HtmlSelectOneMenu htmlSelectOneContractType) {
		this.htmlSelectOneContractType = htmlSelectOneContractType;
	}

	public HtmlInputText getHtmlTenantName() {
		return htmlTenantName;
	}

	public void setHtmlTenantName(HtmlInputText htmlTenantName) {
		this.htmlTenantName = htmlTenantName;
	}

	public HtmlSelectOneMenu getHtmlSelectOneTenantType() {
		return htmlSelectOneTenantType;
	}

	public void setHtmlSelectOneTenantType(HtmlSelectOneMenu htmlSelectOneTenantType) {
		this.htmlSelectOneTenantType = htmlSelectOneTenantType;
	}

	public HtmlInputText getHtmlUnitNumber() {
		return htmlUnitNumber;
	}

	public void setHtmlUnitNumber(HtmlInputText htmlUnitNumber) {
		this.htmlUnitNumber = htmlUnitNumber;
	}
	
	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}
	
}