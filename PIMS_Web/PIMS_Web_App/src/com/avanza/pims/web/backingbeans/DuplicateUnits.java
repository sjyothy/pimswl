package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.ReceivePropertyFloorView;
import com.avanza.pims.ws.vo.ReceivePropertyUnitView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;
@SuppressWarnings( "unchecked" )
public class DuplicateUnits extends AbstractController {
	private HtmlInputText unitPrefix = new HtmlInputText();
	private HtmlInputText startNumber = new HtmlInputText();
	private HtmlInputText endNumber = new HtmlInputText();
	private HtmlSelectOneRadio generationFloor = new HtmlSelectOneRadio();
	private HtmlSelectBooleanCheckbox includeFloorNumberVar = new HtmlSelectBooleanCheckbox();
	private Boolean includeFloorNumber = new Boolean(false);
	private HtmlInputText unitSeperator = new HtmlInputText();
	private HtmlInputHidden propertyId= new HtmlInputHidden();
	private List<SelectItem> floorItems = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu floorMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu floorMenuTo = new HtmlSelectOneMenu();
	private List<String> messages = new ArrayList<String>();
	private HtmlCommandButton generateDuplicateUnits = new HtmlCommandButton();
	private HtmlCommandButton cancel = new HtmlCommandButton();
	private HtmlInputText csvUnitNumbers = new HtmlInputText();
	private static Logger logger = Logger.getLogger(DuplicateUnits.class);
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	ReceivePropertyUnitView  unitViewFromParent = new ReceivePropertyUnitView ();
	public HtmlInputText getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(HtmlInputText startNumber) {
		this.startNumber = startNumber;
	}
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if ( getFacesContext().getExternalContext().getSessionMap().containsKey( "UNIT_VIEW_AS_PARAM" ) )
		{
		 this.setUnitViewFromParent(  (ReceivePropertyUnitView)getFacesContext().getExternalContext().getSessionMap().get("UNIT_VIEW_AS_PARAM") );
		 getFacesContext().getExternalContext().getSessionMap().remove("UNIT_VIEW_AS_PARAM");
		}
	}
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub

		super.preprocess();


		if(propertyId.getValue()!=null)

			loadFloors(Long.valueOf(propertyId.getValue().toString()));


	}
	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		if(getFacesContext().getExternalContext().getSessionMap().containsKey("PROPERTY_ID_DUPLICATE_UNITS")){
			propertyId.setValue(getFacesContext().getExternalContext().getSessionMap().get("PROPERTY_ID_DUPLICATE_UNITS").toString());
			viewMap.put("PROPERTY_ID_DUPLICATE_UNITS",propertyId.getValue());
			getFacesContext().getExternalContext().getSessionMap().remove("PROPERTY_ID_DUPLICATE_UNITS");
		}
		if(propertyId.getValue()!=null)
			loadFloors(Long.valueOf(propertyId.getValue().toString()));


	}
	public HtmlCommandButton getCancel() {
		return cancel;
	}
	public void setCancel(HtmlCommandButton cancel) {
		this.cancel = cancel;
	} 
	private void loadFloors(Long propertyId)  {

		String methodName="loadFloors"; 
		logger.logInfo(methodName+"|"+"Start");
		try {
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			List <ReceivePropertyFloorView> floorViewList = propertyServiceAgent.getPropertyFloorList(propertyId);
			floorItems=new ArrayList<SelectItem>();

			for(int i=0;i<floorViewList.size();i++)
			{
				ReceivePropertyFloorView rcvPropertyFloorVO=(ReceivePropertyFloorView)floorViewList.get(i);

				if(rcvPropertyFloorVO.getFloorNo()!=null)
					floorItems.add(new SelectItem(rcvPropertyFloorVO.getFloorId().toString()+"%%"+rcvPropertyFloorVO.getFloorNo().toString(), rcvPropertyFloorVO.getFloorName()));			  
			}
			PropertyView propertyView = propertyServiceAgent.getPropertyByID(propertyId);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Property.PROPERTY_VIEW,propertyView);



			logger.logInfo(methodName+"|"+"Finish");
		}catch (Exception e){
			logger.LogException(methodName+"|Error Occured ",e);

		}
	}

	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public String cancelButton()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "javascript:window.opener.document.forms[0].submit();window.close();";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		
		return "";
	}
	public void duplicateUnit(){
		String method = "duplicateUnit";
		logger.logInfo(method + " started...");

		try
		{
			if(validateUnitScreen())			
			{
				Boolean isValid = true;

				if(!viewMap.containsKey("MISSING_FLOOR")||!(Boolean.valueOf(viewMap.get("MISSING_FLOOR").toString()))){

					ReceivePropertyUnitView unitView = this.getUnitViewFromParent();
					
					String loggedInUser = getLoggedInUser();
					Integer unitNumberFrom = null;
					Integer unitNumberTo  = null;
					String[] csvUnitNumArr = null;
					if( startNumber.getValue()!= null && startNumber.getValue().toString().trim().length() > 0)
					{
					   unitNumberFrom = Convert.toInteger(startNumber.getValue().toString().trim() );
					}
					if( endNumber.getValue()!= null && endNumber.getValue().toString().trim().length() > 0)
					{
					  unitNumberTo = Convert.toInteger(endNumber.getValue().toString());
					}
					if( csvUnitNumbers.getValue() != null && csvUnitNumbers.getValue().toString().trim().length() > 0)
					{
						csvUnitNumArr = csvUnitNumbers.getValue().toString().trim().split( "," ); 
					}
					String floorNumber = unitView.getFloorNumber();

					Long unitTypeId = unitView.getUnitTypeId();			
					Long unitSideId = unitView.getUnitSideTypeId();
					Long usageTypeId= unitView.getUsageTypeId();
					Long floorId = unitView.getFloorId();

					Long investmentTypeId= unitView.getInvestmentTypeId();
					PropertyServiceAgent psAgent = new PropertyServiceAgent();
					//For Other Floor
					if(generationFloor.getValue()!=null&&generationFloor.getValue().equals("2"))
					{
						ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();
						ArrayList<UnitView> allreadyExistingPropertyUnitViews = new ArrayList<UnitView>();
						ArrayList<FloorView> floorViewList = new ArrayList<FloorView>();
						
						if( floorMenu.getValue()!=null &&  !floorMenu.getValue().toString().split("%%")[0].equals("-1")&&
								floorMenuTo.getValue()!=null&&!floorMenuTo.getValue().toString().split("%%")[0].equals("-1"))
						{
							
							Long floorIdFrom = Long.parseLong(floorMenu.getValue().toString().split("%%")[0]);
							Long floorIdTo = Long.parseLong(floorMenuTo.getValue().toString().split("%%")[0]);

							floorViewList = (ArrayList<FloorView>) psAgent.getFloorsInBetween(floorIdFrom, floorIdTo);
						 for(FloorView floorView: floorViewList)
						 {
						   propertyUnitViews = new ArrayList<UnitView>();							
						   floorId = floorView.getFloorId();
						   floorNumber = floorView.getFloorNumber();
						   if( unitNumberFrom != null && unitNumberTo != null)
						   {
								for(int index = unitNumberFrom; index<=(unitNumberTo);index++)
								{
									createUnitViewObj(unitView, 
											floorNumber, unitTypeId,
											unitSideId, usageTypeId, floorId,
											investmentTypeId,
											propertyUnitViews, index);
								}

						   }
						   else if( csvUnitNumArr != null  && csvUnitNumArr.length > 0)
						   {
							   for (String no: csvUnitNumArr) 
							   {
								   int index = Integer.parseInt(no);
									createUnitViewObj(unitView, 
											floorNumber, unitTypeId,
											unitSideId, usageTypeId, floorId,
											investmentTypeId,
											propertyUnitViews, index);
								
							   }
							   
						   }
								propertyUnitViews = psAgent.generateUnits(Long.valueOf(propertyId.getValue().toString()),floorId,propertyUnitViews);
								
								allreadyExistingPropertyUnitViews.addAll(propertyUnitViews);
							}
						 
						 
							 if(allreadyExistingPropertyUnitViews.size()  >0) 
							 {
								 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ALREADY_EXISTS));                
								 for(UnitView unitView2: allreadyExistingPropertyUnitViews) {
									 {
										 messages.add(unitView2.getUnitNumber()+" ; "+unitView2.getFloorView().getFloorNumber());
										 isValid=false;
									 }
								 }
							 }

							getFacesContext().getExternalContext().getSessionMap().put("UNIT_DATA_LIST_FROM_POPUP",propertyUnitViews);
						}
					}
					else
					{
						ArrayList<UnitView> propertyUnitViews = new ArrayList<UnitView>();
						if( unitNumberFrom != null && unitNumberTo != null)
						{
							for(int index = unitNumberFrom; index<=(unitNumberTo );index++)
							{
								createUnitViewObj(unitView, 
										floorNumber, unitTypeId,
										unitSideId, usageTypeId, floorId,
										investmentTypeId,
										propertyUnitViews, index);
							}
						}
						else if( csvUnitNumArr != null  && csvUnitNumArr.length > 0)
						{
							   for (String no: csvUnitNumArr) 
							   {
								   int index = Integer.parseInt(no);
									createUnitViewObj(unitView, 
											floorNumber, unitTypeId,
											unitSideId, usageTypeId, floorId,
											investmentTypeId,
											propertyUnitViews, index);
							   }
							   
						}
						propertyUnitViews = psAgent.generateUnits(Long.valueOf(propertyId.getValue().toString()),floorId,propertyUnitViews);
						 if(propertyUnitViews.size()  >0) 
						 {
							 messages.add(ResourceUtil.getInstance().getProperty(GenerateUnits.Messages.UNITS_GENERATED_GENERAL_ALREADY_EXISTS));                
							 for(UnitView unitView2: propertyUnitViews) 
							 {
									 messages.add(unitView2.getUnitNumber()+" ; "+unitView2.getFloorView().getFloorNumber());
									 isValid=false;
							 }
						 }
						getFacesContext().getExternalContext().getSessionMap().put("UNIT_DATA_LIST_FROM_POPUP",propertyUnitViews);					
					}
					if(isValid)
					{
						
					FacesContext facesContext = FacesContext.getCurrentInstance();
					String javaScriptText = "javascript:window.opener.document.forms[0].submit();window.close();";
					AddResource addResource = AddResourceFactory.getInstance(facesContext);
					addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
}
					
				}else
				{
					messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.FLOORS_MISSING_IN_DEFINITE_RANGE));
				}

			}
		}
		catch(Exception ex)
		{
			logger.logError("Crashed while generating floors:",ex);
			ex.printStackTrace();
			messages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	private void createUnitViewObj(ReceivePropertyUnitView unitView,
			 String floorNumber, Long unitTypeId,
			Long unitSideId, Long usageTypeId, Long floorId,
			Long investmentTypeId, ArrayList<UnitView> propertyUnitViews,
			int index) throws Exception 
	{
		String loggedInUser = CommonUtil.getLoggedInUser();
		UnitView propertyUnitView = new UnitView();
		propertyUnitView.setCreatedBy(loggedInUser);
		propertyUnitView.setCreatedOn(new Date());
		propertyUnitView.setIsDeleted(new Long(0));
		propertyUnitView.setRecordStatus(new Long(1));
		propertyUnitView.setPropertyId(Long.valueOf(propertyId.getValue().toString()));
		propertyUnitView.setFloorId(floorId);
		propertyUnitView.setUnitTypeId(unitTypeId);
		propertyUnitView.setUnitSideTypeId(unitSideId);
		propertyUnitView.setUsageTypeId(usageTypeId);
		propertyUnitView.setInvestmentTypeId(investmentTypeId);
		propertyUnitView.setUnitDesc(unitView.getUnitDesc());
		propertyUnitView.setUnitArea(unitView.getUnitArea());
		propertyUnitView.setNoOfBath(unitView.getNoOfBath());
		propertyUnitView.setNoOfBed(unitView.getNoOfBed());
		propertyUnitView.setNoOfLiving(unitView.getNoOfLiving());
		propertyUnitView.setRentValue(unitView.getRentValue());
		propertyUnitView.setRequiredPrice(unitView.getRentValue());
		propertyUnitView.setUpdatedBy(loggedInUser);
		propertyUnitView.setUpdatedOn(new Date());
		propertyUnitView.setStatusId(unitView.getStatusId());
		propertyUnitView.setUnitPrefix(unitView.getUnitPrefix());
		if(includeFloorNumber!=null&&includeFloorNumber)
		{
			propertyUnitView.setIncludeFloorNumber(1L);
			String unitNumber =floorNumber+
					            ( unitView.getUnitPrefix()!=null ?unitView.getUnitPrefix().trim():"")+
					            index;
			propertyUnitView.setUnitNumber(unitNumber);
			propertyUnitView.setUnitName( unitNumber	);

		}
		else
		{
			propertyUnitView.setIncludeFloorNumber(0L);
			String unitNumber = ( unitView.getUnitPrefix()!=null ?unitView.getUnitPrefix().trim():"" ) +index;
			propertyUnitView.setUnitNumber( unitNumber );
			propertyUnitView.setUnitName(unitNumber);
		}
		propertyUnitView.setUnitNo(Double.valueOf(index));
		propertyUnitViews.add(propertyUnitView);
	}
	public HtmlInputText getEndNumber() {
		return endNumber;
	}
	public void setEndNumber(HtmlInputText endNumber) {
		this.endNumber = endNumber;
	}
	public List<SelectItem> getFloorItems() {
		return floorItems;
	}
	public void setFloorItems(List<SelectItem> floorItems) {
		this.floorItems = floorItems;
	}
	public HtmlSelectOneMenu getFloorMenu() {
		return floorMenu;
	}
	public void setFloorMenu(HtmlSelectOneMenu floorMenu) {
		this.floorMenu = floorMenu;
	}
	public Boolean getIncludeFloorNumber() {
		return includeFloorNumber;
	}
	public void setIncludeFloorNumber(Boolean includeFloorNumber) {
		this.includeFloorNumber = includeFloorNumber;
	}
	public HtmlSelectBooleanCheckbox getIncludeFloorNumberVar() {
		return includeFloorNumberVar;
	}
	public void setIncludeFloorNumberVar(
			HtmlSelectBooleanCheckbox includeFloorNumberVar) {
		this.includeFloorNumberVar = includeFloorNumberVar;
	}
	public HtmlInputText getUnitPrefix() {
		return unitPrefix;
	}
	public void setUnitPrefix(HtmlInputText unitPrefix) {
		this.unitPrefix = unitPrefix;
	}
	public HtmlInputText getUnitSeperator() {
		return unitSeperator;
	}
	public void setUnitSeperator(HtmlInputText unitSeperator) {
		this.unitSeperator = unitSeperator;
	}
	public HtmlCommandButton getGenerateDuplicateUnits() {
		return generateDuplicateUnits;
	}
	public void setGenerateDuplicateUnits(HtmlCommandButton generateDuplicateUnits) {
		this.generateDuplicateUnits = generateDuplicateUnits;
	}
	public HtmlSelectOneRadio getGenerationFloor() {
		return generationFloor;
	}
	public void setGenerationFloor(HtmlSelectOneRadio generationFloor) {
		this.generationFloor = generationFloor;
	}
	public HtmlInputHidden getPropertyId() {
		if(			viewMap.containsKey("PROPERTY_ID_DUPLICATE_UNITS")&&
				viewMap.get("PROPERTY_ID_DUPLICATE_UNITS")!=null){
			propertyId.setValue(viewMap.get("PROPERTY_ID_DUPLICATE_UNITS").toString());
		}
		return propertyId;
	}
	public void setPropertyId(HtmlInputHidden propertyId) {
		this.propertyId = propertyId;
	}
	public HtmlSelectOneMenu getFloorMenuTo() {
		return floorMenuTo;
	}
	public void setFloorMenuTo(HtmlSelectOneMenu floorMenuTo) {
		this.floorMenuTo = floorMenuTo;
	}

	public String getMessages() {
		return CommonUtil.getErrorMessages(messages);
	}


	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	private boolean validateUnitScreen() {

		String method = "ValidateUnitScreen";
		logger.logInfo(method + " started...");
		boolean isValid = true;	

		NumberFormat nf = new DecimalFormat(getNumberFormat());
		if(generationFloor.getValue()==null)
		{
			messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_DIRECTION_REQUIRED));
			isValid = false;
		}
		else if(generationFloor.getValue()!=null&&generationFloor.getValue().toString().equals("2")&&
				((floorMenu.getValue()==null||floorMenu.getValue().toString().equals("-1"))||
						(floorMenuTo.getValue()==null||floorMenuTo.getValue().toString().equals("-1") )))
		{
			messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_FLOOR_RANGE_REQUIRED));
			isValid = false;
		}
		if(  isValid && (csvUnitNumbers.getValue() == null || csvUnitNumbers.getValue().toString().trim().length()<=0 ))
		{
				if( ( startNumber.getValue()== null || endNumber.getValue()== null ) )
				{
					messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_RANGE_REQUIRED));
					isValid = false;
				}
				else
				{
					try
					{
						if( startNumber.getValue()!=null && startNumber.getValue().toString().trim().length() > 0 )
						{
							if(Integer.parseInt(startNumber.getValue().toString())<=0) throw new NumberFormatException();
						}
						else
						{
							messages.add(CommonUtil.getBundleMessage("receiveProperty.msg.unitStartRangeRequired"));
							isValid = false;
						}
					}
					catch (NumberFormatException e) 
					{
						isValid = false;
						messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_UNIT_START_NUMBER));
					}
					try
					{
						if( endNumber.getValue() != null && endNumber.getValue().toString().trim().length() > 0 )
						{
							if(Integer.parseInt(endNumber.getValue().toString())<=0) throw new NumberFormatException();
						}
						else
						{
							messages.add(CommonUtil.getBundleMessage("receiveProperty.msg.unitEndRangeRequired"));
							isValid = false;
						}
					}
					catch (NumberFormatException e) 
					{
						isValid = false;
						messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_UNIT_END_NUMBER));
					}
					if( isValid && startNumber.getValue()!=null && endNumber.getValue()!= null && 
							 Integer.valueOf( startNumber.getValue().toString() ) > Integer.valueOf(endNumber.getValue().toString()) )
							{
								messages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_UNIT_RANGE_INVLAID));
								isValid = false;
							}
				}
		}
		else
		{
			String csvUnitNumArr[] = csvUnitNumbers.getValue().toString().trim().split( "," );
			try
			{
				for (String unitNo : csvUnitNumArr) 
				{
					int number = Integer.parseInt(unitNo);
				} 

			}
			catch (NumberFormatException e) 
			{
				isValid = false;
				messages.add(CommonUtil.getBundleMessage("receiveProperty.msg.invlaidSpecificUnitNum"));
			}

		}
		return isValid;
	}
	private String getNumberFormat(){
		String methodName ="getNumberFormat";
		logger.logInfo(methodName+ " started");
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		logger.logInfo(methodName+" compelted");
		return localeInfo.getNumberFormat();
	}
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
				
	}
	public boolean getIsArabicLocale()
	{
		return  !getIsEnglishLocale();
	}
	public HtmlInputText getCsvUnitNumbers() {
		return csvUnitNumbers;
	}
	public void setCsvUnitNumbers(HtmlInputText csvUnitNumbers) {
		this.csvUnitNumbers = csvUnitNumbers;
	}
	public ReceivePropertyUnitView getUnitViewFromParent() {
		if( viewMap.get("UNIT_VIEW_AS_PARAM") != null )
		{
			unitViewFromParent = (ReceivePropertyUnitView)viewMap.get("UNIT_VIEW_AS_PARAM");
		}
		return unitViewFromParent;
	}
	public void setUnitViewFromParent(ReceivePropertyUnitView unitViewFromParent) {
		this.unitViewFromParent = unitViewFromParent;
		if( this.unitViewFromParent != null )
		{
			viewMap.put("UNIT_VIEW_AS_PARAM", this.unitViewFromParent ); 
		}
	}
}
