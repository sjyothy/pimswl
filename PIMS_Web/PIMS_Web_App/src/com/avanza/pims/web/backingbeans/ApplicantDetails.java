package com.avanza.pims.web.backingbeans;

import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;



public class ApplicantDetails extends AbstractController
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(ApplicantDetails.class);
	private String applicantType;
	private boolean isArabicLocale;	
    private boolean isEnglishLocale;
	private String pageMode;
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap=context.getViewRoot().getAttributes();
    
    
    private String txtOrganization;
    private String stateId;
    private String txtRequester;
    private String txtJobTitle;

    public void setApplicantDetails(String txtOrganization, String txtEmiratesId, String txtRequester, String txtJobTitle) {
        this.setTxtOrganization(txtOrganization);
        this.setStateId(txtEmiratesId);
        this.setTxtRequester(txtRequester);
        this.setTxtJobTitle(txtJobTitle);
    }


    @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
			sessionMap=context.getExternalContext().getSessionMap();
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
     
        
	private List<String> errorMessages;
		public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}


		@SuppressWarnings("unchecked")
		public String getApplicantType() {
			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE))
				applicantType=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE).toString();
			return applicantType;
		}
		
		@SuppressWarnings("unchecked")
		public void setApplicantType(String applicantType) {
			this.applicantType = applicantType;
			if(this.applicantType!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,this.applicantType);
			
		}

		@SuppressWarnings("unchecked")
		public Boolean getApplicationDetailsReadonlyMode(){
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Boolean applicationDetailsReadonlyMode = (Boolean)viewMap.get("applicationDetailsReadonlyMode");
			if(applicationDetailsReadonlyMode==null)
				applicationDetailsReadonlyMode = false;
			return applicationDetailsReadonlyMode;
		}
	
		@SuppressWarnings("unchecked")
		public String getApplicationDescriptionReadonlyMode(){
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			String applicationDetailsReadonlyMode = "APPLICANT_DETAILS_INPUT "+(String)viewMap.get("applicationDescriptionReadonlyMode");
			if(applicationDetailsReadonlyMode==null)
				applicationDetailsReadonlyMode = "APPLICANT_DETAILS_INPUT";
			return applicationDetailsReadonlyMode;
		}

	

    public void setTxtOrganization(String txtOrganization) {
    	
    	this.txtOrganization = txtOrganization;
    	
		if(this.txtOrganization != null)
			viewRootMap.put(WebConstants.ApplicantDetails.ORGANIZATION, this.txtOrganization);
    	
        
    }

    public String getTxtOrganization() {
    	
    	if(viewRootMap.containsKey(WebConstants.ApplicantDetails.ORGANIZATION)  )
    		txtOrganization = viewRootMap.get(WebConstants.ApplicantDetails.ORGANIZATION).toString();

        return txtOrganization;
    }

    public void setStateId(String stateId) {
    	
    	
        this.stateId = stateId;
        
		if(this.stateId != null)
			viewRootMap.put(WebConstants.ApplicantDetails.EMIRATES, this.stateId);
        
    }

    public String getStateId() {
        
    	if(viewRootMap.containsKey(WebConstants.ApplicantDetails.EMIRATES)  )
    		stateId = (String)viewRootMap.get(WebConstants.ApplicantDetails.EMIRATES);

    	return stateId;
    }

    public void setTxtRequester(String txtRequester) {
    	
    	
        this.txtRequester = txtRequester;
        
        if(this.txtRequester != null)
			viewRootMap.put(WebConstants.ApplicantDetails.REQUESTER, this.txtRequester);
        
    }

    public String getTxtRequester() {

    	if(viewRootMap.containsKey(WebConstants.ApplicantDetails.REQUESTER)  )
    		txtRequester = viewRootMap.get(WebConstants.ApplicantDetails.REQUESTER).toString();

        return txtRequester;
    }

    public void setTxtJobTitle(String txtJobTitle) {
        
    	this.txtJobTitle = txtJobTitle;
    	if(this.txtJobTitle != null)
			viewRootMap.put(WebConstants.ApplicantDetails.JOB_TITLE, this.txtJobTitle);
        
        
    }

    public String getTxtJobTitle() {
    	
    	if(viewRootMap.containsKey(WebConstants.ApplicantDetails.JOB_TITLE)  )
    		txtJobTitle = viewRootMap.get(WebConstants.ApplicantDetails.JOB_TITLE).toString();

    	
        return txtJobTitle;
    }
}
