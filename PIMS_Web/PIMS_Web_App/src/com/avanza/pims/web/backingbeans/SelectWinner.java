package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.NotificationType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.PropertyManager;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.Region;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AuctionReplacedUnitView;
import com.avanza.pims.ws.vo.AuctionRequestRegView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderAttendanceView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class SelectWinner extends AbstractController {

	// Fields

	private transient Logger logger = Logger.getLogger(SelectWinner.class);

	private HtmlInputText inputTextBidderName;
	private HtmlInputText inputTextSocialSecNo;

	private HtmlInputText inputTextCellNumber;

	private HtmlInputText inputTextEmirates;
	
	private HtmlCommandButton commandButtonSearch;

	private HtmlDataTable dataTable;

	private PersonView dataItem = new PersonView();

	private List<PersonView> dataList = new ArrayList<PersonView>();

	private List<SelectItem> biddersType = new ArrayList<SelectItem>();

	private List<SelectItem> nationality = new ArrayList<SelectItem>();

	private List<SelectItem> stateList = new ArrayList<SelectItem>();

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;

	private String bidPrice;

	private String bidderTypeSelected;

	private String nationalitySelected;

	private Double hdnBidPrice;

	private Long hdnAuctionId;

	private Long hdnAuctionUnitId;

	private String hdnBidderId;

	private List<String> messages;
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	private String stateId;

	private static final String SELECT_WINNER_LIST = "SELECT_WINNER_LIST";

	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	private Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer unitPaginatorMaxPages = 0;
	private Integer unitPaginatorRows = 0;
	private Integer unitRecordSize = 0;
	private List<UnitView> replaceableUnitList = new ArrayList<UnitView>();
	private HtmlDataTable unitDataTable = new  HtmlDataTable();
	private  Boolean showUnitSection ;
	
	private HtmlInputText txtSelectedName = new HtmlInputText();
	private HtmlInputText txtSelectedSocialSecNo = new HtmlInputText();
	private HtmlInputText txtSelectedNationality = new HtmlInputText();
	private HtmlInputText txtSelectedEmirates = new HtmlInputText();
	private HtmlInputText txtSelectedCellNumber = new HtmlInputText();
	private HtmlInputText txtBidPrice = new HtmlInputText();
	private HtmlSelectBooleanCheckbox chkUnit = new HtmlSelectBooleanCheckbox();
	private HtmlSelectBooleanCheckbox chkBidder = new HtmlSelectBooleanCheckbox();
	private HtmlTab replaceUnitsTab = new HtmlTab();
	private HtmlTab bidderTab = new HtmlTab();
	private HtmlTabPanel detailTab = new HtmlTabPanel();
	
	// Constructor
	/** default constructor */
	public SelectWinner() {
	}

	// Methods

	private void loadDataList() {
		String methodName = "loadDataList";
		logger.logInfo(methodName + " started...");

		try {
			PersonView bidderView = new PersonView();

			if (inputTextBidderName != null
					&& inputTextBidderName.getValue() != null
					&& !inputTextBidderName.getValue().toString().equals("")) {
				String personName = inputTextBidderName.getValue().toString()
						.trim();
				String[] nameSplit = personName.split(" ");

				bidderView.setFirstName(nameSplit[0]);

				if (nameSplit.length == 2)
					bidderView.setLastName(nameSplit[1]);
				if (nameSplit.length >= 3) {
					bidderView.setMiddleName(nameSplit[1]);
					bidderView.setLastName(nameSplit[2]);
				}
			}

			if (inputTextSocialSecNo != null
					&& inputTextSocialSecNo.getValue() != null
					&& !inputTextSocialSecNo.getValue().toString().equals(""))
				bidderView.setResidenseVisaNumber(inputTextSocialSecNo.getValue()
						.toString());

			if (nationalitySelected != null
					&& !(nationalitySelected.trim().toString().equals("")
					|| nationalitySelected.trim().toString().equals("-1"))) {
				Long regionId = new Long(nationalitySelected.trim()
						.toString());
				bidderView.setNationalityId(regionId);
				UtilityManager utilityManager = new UtilityManager();
				Region region = utilityManager.getRegionByRegionId(regionId);
				bidderView.setNationalityEn(region.getRegionName());
				bidderView.setNationalityAr(region.getRegionName());
			}

			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			// List<BidderView> bidders = pSAgent.getBidderLike(bidderView);
			String bidderType = "REG";
//			if (bidderTypeSelected != null && bidderTypeSelected.equals("0"))
//				bidderType = "ALL";
//			if (bidderTypeSelected != null && bidderTypeSelected.equals("1"))
//				bidderType = "REG";

			// Emirates criteria has to be injected here

			if (inputTextCellNumber != null
					&& inputTextCellNumber.getValue() != null
					&& !inputTextCellNumber.getValue().toString().equals(""))
				bidderView.setCellNumber(inputTextCellNumber.getValue()
						.toString());
			//Old code to fetch only registered bidder
			List<PersonView> unitBidders = pSAgent.getAuctionUnitBidders(
					hdnAuctionId, hdnAuctionUnitId, bidderType, bidderView);
			HashMap<Long, PersonView>  allBidders = new HashMap<Long, PersonView>();
			for(PersonView person :unitBidders)
				{	
					PersonView regBidder = person;
					regBidder.setIsRegistered(true);
					allBidders.put(person.getPersonId(), person);
				}
			List<PersonView> bidders = new ArrayList<PersonView>();
			
			//code to fetch unregistered User
			if((inputTextBidderName.getValue() == null || inputTextBidderName.getValue().toString().trim().length()==0) && 
			   (inputTextSocialSecNo.getValue() ==null || inputTextSocialSecNo.getValue().toString().trim().length()==0) && 
			    nationalitySelected.equals("-1") && stateId.equals("-1") && 
			   (inputTextCellNumber.getValue() == null || inputTextCellNumber.getValue().toString().trim().length()==0) )
			{
				Long id = (Long) viewMap.get("AUCTION_ID");
				bidders = new PropertyService().getBidderByAuctionId(id);
				HashMap<Long, Double> idDepositPair= new HashMap<Long, Double>();
				
				if(viewMap.get("BIDDER_ID_DEPOSIT_PAIR") != null )
					idDepositPair =(HashMap<Long, Double>) viewMap.get("BIDDER_ID_DEPOSIT_PAIR");
				
				for(PersonView person : bidders)
				{
					if(allBidders.containsKey(person.getPersonId()))
						continue;
					else
					{
						PersonView unRegBidder = person;
						if(idDepositPair.containsKey(unRegBidder.getPersonId()))
							unRegBidder.setTotalDepositAmount(idDepositPair.get(unRegBidder.getPersonId()));
						unRegBidder.setIsRegistered(false);
						allBidders.put(unRegBidder.getPersonId(),unRegBidder);
					}
				}
			}
			Collection collection  = allBidders.values();
			Iterator iter =collection.iterator();
			bidders.clear();
			while(iter.hasNext())
				bidders.add((PersonView) iter.next());
			bidders = filteredBidders(bidders);
			/*for (PersonView personView : bidders)
				if (personView.getNationalityId() != null) {
					Long regionId = personView.getNationalityId();
					UtilityManager utilityManager = new UtilityManager();
					Region region = utilityManager.getRegionByRegionId(regionId);
					personView.setNationalityEn(region.getRegionName());
					personView.setNationalityAr(region.getRegionName());
				}*/

			// //////////////////////////////////////////////////////////
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			// Map viewMap =
			// FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			// viewRootMap.put("biddersList", bidders);
			recordSize = bidders.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
			// /////////////////////////////////////////////////////////

			dataList = bidders;
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			logger.LogException(methodName + " crashed ...", exception);
			exception.printStackTrace();
		}
		logger.logInfo(methodName + " ended...");
	}
	
	private List<PersonView> filteredBidders(List<PersonView> allBidders){
		List<PersonView> filteredBidders = new ArrayList<PersonView>();
		List<BidderAttendanceView> absentBidders = (List<BidderAttendanceView>) viewMap.get("absentBidders");
		if(absentBidders!=null && allBidders!=null){
			for(PersonView bidder: allBidders){
				for(BidderAttendanceView absentBidder: absentBidders){
					if(bidder.getPersonId().compareTo(absentBidder.getBidderId()) == 0){
						filteredBidders.add(bidder);
					}
				}
			}
			allBidders.removeAll(filteredBidders);
		}
		
		return allBidders;
	}

	public String search() {
		String methodName = "search";
		logger.logInfo(methodName + " started...");
		// if (dataList != null) {
		loadDataList();
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		viewRootMap.put(SELECT_WINNER_LIST, dataList);

		// }
		logger.logInfo(methodName + " ended...");
		return "";
	}

	public String saveWinner() {
		String methodName = "saveWinner";
		logger.logInfo(methodName + " started...");
		try {
			if(viewMap.get("SELECTED_BIDDER_ID")!=null)
			{
				hdnBidderId = viewMap.get("SELECTED_BIDDER_ID").toString();
				hdnAuctionId = (Long) viewMap.get("AUCTION_ID");
				hdnAuctionUnitId = (Long) viewMap.get("AUCTION_UNIT_ID");
				
				if(viewMap.get("UNREGISTERED_BIDDER") != null &&  ((PersonView)viewMap.get("UNREGISTERED_BIDDER")).getPersonId() != null)
				{
					if (validateSave())
					{
						AuctionReplacedUnitView aucRplcdUnit = new AuctionReplacedUnitView();
						populateAuctionReplacedUnit(aucRplcdUnit);
						new PropertyService().addAuctionReplacedUnit(aucRplcdUnit);
						
						AuctionWinnersView auctionWinnersView = new AuctionWinnersView();

						auctionWinnersView.setBidPrice((Double) txtBidPrice.getValue());
						auctionWinnersView.setAuctionWin(true);
						auctionWinnersView.setBidderId(Long.parseLong(hdnBidderId));
						auctionWinnersView.setAuctionId(hdnAuctionId);
						auctionWinnersView.setAuctionUnitId(hdnAuctionUnitId);
						auctionWinnersView.setOldAuctionUnitId(aucRplcdUnit.getOldAuctionUnitId());
						PropertyServiceAgent pSAgent = new PropertyServiceAgent();
						pSAgent.updateAuctionWinner(auctionWinnersView);
						
						PersonView winnerDetail = pSAgent.getPersonInformation(Long.parseLong(hdnBidderId));
						
						
						String sysMsgEn = CommonUtil.getParamBundleMessage("conductionAuciton.editAuctionBidderReplaced","en",
								winnerDetail.getPersonFullName(),getUnitNumberToBeReplaced(),getNewUnitNumber(),auctionWinnersView.getBidPrice());
						String sysMsgAr = CommonUtil.getParamBundleMessage("conductionAuciton.editAuctionBidderReplaced","ar",
								winnerDetail.getPersonFullName(),getUnitNumberToBeReplaced(),getNewUnitNumber(),auctionWinnersView.getBidPrice());
						NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, sysMsgEn, sysMsgAr,auctionWinnersView.getAuctionId());

						
						sendNotifications();
						closeWindow();
//						successMessages.clear();
//						successMessages.add("You have replaced Unit# : "+getUnitNumberToBeReplaced()+" with Unit# : "+getNewUnitNumber());
												
					}
				}
				else
				{
					if (validateSave()) {
						
						AuctionWinnersView auctionWinnersView = new AuctionWinnersView();

						auctionWinnersView.setBidPrice((Double) txtBidPrice.getValue());
						auctionWinnersView.setAuctionWin(true);
						auctionWinnersView.setBidderId(Long.parseLong(hdnBidderId));
						auctionWinnersView.setAuctionId(hdnAuctionId);
						auctionWinnersView.setAuctionUnitId(hdnAuctionUnitId);

						PropertyServiceAgent pSAgent = new PropertyServiceAgent();
						pSAgent.updateAuctionWinner(auctionWinnersView);
						
						PersonView winnerDetail = pSAgent.getPersonInformation(Long.parseLong(hdnBidderId));
						
						String sysMsgEn = CommonUtil.getParamBundleMessage("conductionAuciton.editAuctionBidderWon","en",
								winnerDetail.getPersonFullName(),getNewUnitNumber(),auctionWinnersView.getBidPrice());
						String sysMsgAr = CommonUtil.getParamBundleMessage("conductionAuciton.editAuctionBidderWon","ar",
								winnerDetail.getPersonFullName(),getNewUnitNumber(),auctionWinnersView.getBidPrice());
						NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, sysMsgEn, sysMsgAr,auctionWinnersView.getAuctionId());

						
						
						sendNotifications();
						closeWindow();
					} else {
						search();
				
					}
				}
				
			}
			else
			{
				errorMessages.clear();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ConductAuction.MSG_BIDDER_NOT_SELECTED));
			}
			logger.logInfo(methodName + " Finish...");	
		} catch (PimsBusinessException e) {
			logger.LogException( methodName + " crashed...",e);
			
		}
		
		return "";
	}

	
	
	
	
	private void populateAuctionReplacedUnit(AuctionReplacedUnitView aucRplcdUnit) 
	{
		aucRplcdUnit.setAuctionReplacedUnitId(null);
		aucRplcdUnit.setAuctionId(Long.parseLong(viewMap.get("AUCTION_ID").toString()));
		aucRplcdUnit.setNewAuctionUnitId(Long.parseLong(viewMap.get("AUCTION_UNIT_ID").toString()));
		aucRplcdUnit.setOldAuctionUnitId(Long.parseLong(viewMap.get("OLD_AUCTION_UNIT_ID").toString()));
		aucRplcdUnit.setNewUnitId(Long.parseLong(viewMap.get("NEW_UNIT_ID").toString()));
		aucRplcdUnit.setOldUnitId(Long.parseLong(viewMap.get("OLD_UNIT_ID").toString()));
		aucRplcdUnit.setBidderId(Long.parseLong(viewMap.get("SELECTED_BIDDER_ID").toString()));
		aucRplcdUnit.setOldUnitNumber(viewMap.get("OLD_UNIT_NUMBER").toString());
		aucRplcdUnit.setNewUnitNumber(viewMap.get("NEW_UNIT_NUMBER").toString());
		aucRplcdUnit.setIsDeleted(0L);
	}

	private void sendNotifications()
	{
		try
		{
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			AuctionView auctionView = new AuctionView();
			AuctionRequestRegView auctionRegView = new AuctionRequestRegView();
			if(sessionMap.containsKey(WebConstants.ConductAuction.AUCTION_WINNER) &&
					sessionMap.get(WebConstants.ConductAuction.AUCTION_WINNER)!= null)
			{
				auctionRegView = (AuctionRequestRegView)sessionMap.get(WebConstants.ConductAuction.AUCTION_WINNER);
				if(auctionRegView != null)
				{
					
					auctionView = psAgent.getAuctionById(auctionRegView.getAuctionId());
				}
				
				if(auctionRegView != null && auctionView != null)
				{
					notifyWinner(auctionRegView, auctionView);
					notifyOtherBidders(auctionRegView, auctionView);
				}
			}
		}
		catch (PimsBusinessException e) {
			logger.logInfo("send notifications crashed...");
			e.printStackTrace();
		}
	}
	
	
	private void notifyWinner(AuctionRequestRegView auctionRegView,AuctionView auctionView)
	{
		try
		{
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			PersonView winnerDetail = psAgent.getPersonInformation(Long.parseLong(hdnBidderId));
			List<ContactInfo> contactInfoList = getContactInfoList(winnerDetail);
			
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			eventAttributesValueMap.put("AUCTION_NO", auctionView.getAuctionNumber());
			eventAttributesValueMap.put("AUCTION_VENUE", auctionView.getVenueName());
			eventAttributesValueMap.put("PROPERTY_NO",auctionRegView.getPropertyRefNumber());
			eventAttributesValueMap.put("PROPERTY_NAME", auctionRegView.getCommercialName());
			eventAttributesValueMap.put("UNIT_NO", auctionRegView.getUnitNumber());
			eventAttributesValueMap.put("WINNER_NAME", winnerDetail.getPersonFullName());
			eventAttributesValueMap.put("BID_PRICE", hdnBidPrice.toString());
			
			generateNotification(WebConstants.Notification_MetaEvents.Event_Auction_Winner_Selected, contactInfoList, eventAttributesValueMap,null);
			
		}
		catch (PimsBusinessException e) {
			logger.logInfo("send notification crashed...");
			e.printStackTrace();
		}
		
	}
	
	private void notifyOtherBidders(AuctionRequestRegView auctionRegView,AuctionView auctionView)
	{
		try
		{
			PersonView bidderView = new PersonView();
			String bidderType = "REG";
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			List<PersonView> bidders = psAgent.getAuctionUnitBidders(
					hdnAuctionId, hdnAuctionUnitId, bidderType, bidderView);

			bidders = filteredBidders(bidders);
			Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
			
			List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>(); 
			 
			for(PersonView pView : bidders)
			{
				if(pView.getPersonId().compareTo(new Long(hdnBidderId)) == 0)
					continue;
				contactInfoList = getContactInfoList(pView);
				eventAttributesValueMap.put("AUCTION_NO", auctionView.getAuctionNumber());
				eventAttributesValueMap.put("AUCTION_VENUE", auctionView.getVenueName());
				eventAttributesValueMap.put("PROPERTY_NO",auctionRegView.getPropertyRefNumber());
				eventAttributesValueMap.put("PROPERTY_NAME", auctionRegView.getCommercialName());
				eventAttributesValueMap.put("UNIT_NO", auctionRegView.getUnitNumber());
				eventAttributesValueMap.put("WINNER_NAME", pView.getPersonFullName());
							
				generateNotification(WebConstants.Notification_MetaEvents.Event_Auction_Lost, contactInfoList, eventAttributesValueMap,null);
			}
			
			
			
			
		}
		catch (PimsBusinessException e) {
			logger.logInfo("send notification crashed...");
			e.printStackTrace();
		}
		
	}
	
	
	public boolean generateNotification(String eventName, List<ContactInfo> recipientList, Map<String, Object> eventAttributesValueMap, NotificationType notificationType)
	{
		final String METHOD_NAME = "generateNotification()";
		boolean success = false;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- Successfully Started --- ");
			if ( recipientList != null && recipientList.size() > 0 )
			{
				NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
				Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
				if ( eventAttributesValueMap != null && eventAttributesValueMap.size() > 0 )
					event.setValue( eventAttributesValueMap );
				if ( notificationType != null )
					notificationProvider.fireEvent(event, recipientList, notificationType);
				else
					notificationProvider.fireEvent(event, recipientList);
				
			}
			success = true;
			logger.logInfo(METHOD_NAME + " --- Successfully Completed --- ");
		}
		catch (Exception exception)
		{
			logger.LogException(METHOD_NAME + " --- Exception Occured --- ", exception);
		}
		
		return success;
	}
	
	private void closeWindow() {
		// final String viewId = "/SelectWinner.jsp";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		// ViewHandler viewHandler =
		// facesContext.getApplication().getViewHandler();
		// String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		// Your Java script code here
		String javaScriptText = "javascript:closeWindow();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

	}

	public void loadState() {
		String methodName = "loadState";
		logger.logInfo(methodName + "|" + "Start");
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			String selectedCountryName = "";
			for (int i = 0; i < this.getNationality().size(); i++)
				if (new Long(nationalitySelected) == (new Long(this
						.getNationality().get(i).getValue().toString()))
						.longValue()) {
					selectedCountryName = this.getNationality().get(i)
							.getLabel();
				}

			List<RegionView> regionViewList = psa
					.getCountryStates(selectedCountryName,getIsArabicLocale());

			for (int i = 0; i < regionViewList.size(); i++) {
				RegionView rV = (RegionView) regionViewList.get(i);
				SelectItem item;
				if (getIsEnglishLocale()) {
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionEn());
				} else
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionAr());
				this.getStateList().add(item);
			}
			FacesContext.getCurrentInstance().getViewRoot().getAttributes()
					.put("stateList", stateList);
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {

			logger.LogException(methodName + "|Error Occured ", e);

		}
	}

	private Boolean validateSave() 
	{
		Boolean returnVal = true;
		if (hdnBidderId == null || hdnBidderId.equals("")) {
			writeMessage(ResourceUtil.getInstance().getProperty(
					MessageConstants.ConductAuction.MSG_BIDDER_NOT_SELECTED));
			returnVal = false;
		}
		if (hdnBidPrice.compareTo(new Double(0)) <= 0) {
			writeMessage(ResourceUtil.getInstance().getProperty(
					MessageConstants.ConductAuction.MSG_INVALID_BID_PRICE));
			returnVal = false;
		}
		hdnBidPrice.compareTo(new Double(0));
		//check if the selected user has replaced this unit.
		Long unitId = (Long) viewMap.get("NEW_UNIT_ID");
		if(new PropertyService().isUnitReplaced(hdnAuctionId, Long.parseLong(hdnBidderId), unitId))
			{
				errorMessages.add(CommonUtil.getBundleMessage("auction.selectwinner.errorMsg.unableToWin"));
				returnVal = false;
			}
		return returnVal;
	}

	@SuppressWarnings("unchecked")
	private void setInfo() {
		String methodName = "setInfo";
		logger.logInfo(methodName + " started...");
		try {
			List<PersonView>  biddersViewList = new ArrayList<PersonView>();
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getCurrentInstance().getExternalContext().getRequest();
			if (request.getParameter("aucId") != null)
				{
					hdnAuctionId = Long.parseLong(request.getParameter("aucId").toString());
					viewMap.put("AUCTION_ID", hdnAuctionId);
				}
			if(hdnAuctionId !=null )
				biddersViewList = new PropertyService().getBidderByAuctionId(hdnAuctionId);
			
			if (request.getParameter("aucUnitId") != null)
				{
					hdnAuctionUnitId = Long.parseLong(request.getParameter("aucUnitId").toString());
					viewMap.put("AUCTION_UNIT_ID", hdnAuctionUnitId);
				}
			if (sessionMap.get("NEW_UNIT_ID") != null)
				{
					viewMap.put("NEW_UNIT_ID",Long.parseLong(sessionMap.get("NEW_UNIT_ID") .toString()));
					sessionMap.remove("NEW_UNIT_ID"); 
				}
			
			if (sessionMap.get("NEW_UNIT_NUMBER")  != null)
				{
					viewMap.put("NEW_UNIT_NUMBER",sessionMap.get("NEW_UNIT_NUMBER").toString());
					sessionMap.remove("NEW_UNIT_NUMBER");
				}
			if(sessionMap.get("BIDDER_ID_DEPOSIT_PAIR") != null)
				{
					viewMap.put("BIDDER_ID_DEPOSIT_PAIR",sessionMap.get("BIDDER_ID_DEPOSIT_PAIR"));
					sessionMap.remove("BIDDER_ID_DEPOSIT_PAIR");
				}
			
			if (request.getParameter("bidPrice") != null) {
				String bidPrice = request.getParameter("bidPrice").toString();

				try {
					hdnBidPrice = Double.parseDouble(bidPrice);
				} catch (Exception e) {
					hdnBidPrice = 0.0d;
				}
			}

			List<BidderAttendanceView> absentBidders = (List<BidderAttendanceView>) sessionMap.get("absentBidders");
			if(absentBidders!=null && !absentBidders.isEmpty()){
				viewMap.put("absentBidders", absentBidders);
				sessionMap.remove("absentBidders");
			}
			
			logger.logInfo(methodName + " comleted successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed...", exception);
		}
	}

	private void writeMessage(String message) {
		if (errorMessages == null)
			errorMessages = new ArrayList<String>();
		errorMessages.add(message);
	}

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		if (!isPostBack())
			setInfo();

		loadCountry();
	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	// Getters and Setters
	public List<PersonView> getBidderDataList() {
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		Object list = viewRootMap.get(SELECT_WINNER_LIST);
		if (list != null)
			dataList = (List<PersonView>) list;
		// else{
		// loadDataList();
		// viewRootMap.put(SELECT_WINNER_LIST, dataList);
		// }
		return dataList;
	}

	public HtmlInputText getInputTextSocialSecNo() {
		return inputTextSocialSecNo;
	}

	public void setInputTextSocialSecNo(HtmlInputText inputSocialSecNo) {
		this.inputTextSocialSecNo = inputSocialSecNo;
	}

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public PersonView getDataItem() {
		return dataItem;
	}

	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}

	public List<PersonView> getDataList() {
		return dataList;
	}

	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @param isEnglishLocale
	 *            the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale
	 *            the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public List<SelectItem> getBiddersType() {
		biddersType = new ArrayList<SelectItem>();
		// if(getIsEnglishLocale()){
		// biddersType.add(new SelectItem("0", "All"));
		// biddersType.add(new SelectItem("1", "Registered"));
		biddersType
				.add(new SelectItem(
						"0",
						ResourceUtil
								.getInstance()
								.getProperty(
										WebConstants.ConductAuction.BIDDERS_REGISTERED_IN_AUCTION)));
		biddersType
				.add(new SelectItem(
						"1",
						ResourceUtil
								.getInstance()
								.getProperty(
										WebConstants.ConductAuction.BIDDERS_REGISTERED_IN_UNIT)));
		// }
		// else{
		// biddersType.add(new SelectItem("0", "Haza All"));
		// biddersType.add(new SelectItem("1", "Haza Registered"));
		// }
		return biddersType;
	}

	public void setBiddersType(List<SelectItem> biddersType) {
		this.biddersType = biddersType;
	}

	public String getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(String bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getBidderTypeSelected() {
		return bidderTypeSelected;
	}

	public void setBidderTypeSelected(String bidderTypeSelected) {
		this.bidderTypeSelected = bidderTypeSelected;
	}

	public Double getHdnBidPrice() {
		return hdnBidPrice;
	}

	public void setHdnBidPrice(Double hdnBidPrice) {
		this.hdnBidPrice = hdnBidPrice;
	}

	public Long getHdnAuctionId() {
		return hdnAuctionId;
	}

	public void setHdnAuctionId(Long hdnAuctionId) {
		this.hdnAuctionId = hdnAuctionId;
	}

	public Long getHdnAuctionUnitId() {
		return hdnAuctionUnitId;
	}

	public void setHdnAuctionUnitId(Long hdnAuctionUnitId) {
		this.hdnAuctionUnitId = hdnAuctionUnitId;
	}

	public String getHdnBidderId() {
		return hdnBidderId;
	}

	public void setHdnBidderId(String hdnBidderId) {
		this.hdnBidderId = hdnBidderId;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getMessages() {
		/*String messageList;
		if ((messages == null) || (messages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<B><UL>\n";
			for (String message : messages) {
				messageList = messageList + message + "<BR>";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return (messageList);*/
		return CommonUtil.getErrorMessages(messages);
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages
	 *            the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows
	 *            the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	public void loadCountry() {

		String methodName = "loadCountry";
		logger.logInfo(methodName + "|" + "Start");
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<RegionView> regionViewList = psa.getCountry();

			for (int i = 0; i < regionViewList.size(); i++) {
				RegionView rV = (RegionView) regionViewList.get(i);
				SelectItem item;
				if (getIsEnglishLocale()) {
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionEn());
				} else
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionAr());
				// if(i==0)
				// nationalitySelected = rV.getRegionId().toString();
				this.getNationality().add(item);

			}

			FacesContext.getCurrentInstance().getViewRoot().getAttributes()
					.put("countryList", nationality);
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);

		}
	}

	/**
	 * @param recordSize
	 *            the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public HtmlInputText getInputTextBidderName() {
		return inputTextBidderName;
	}

	public void setInputTextBidderName(HtmlInputText inputTextBidderName) {
		this.inputTextBidderName = inputTextBidderName;
	}

	public String getNationalitySelected() {
		return nationalitySelected;
	}

	public void setNationalitySelected(String nationalitySelected) {
		this.nationalitySelected = nationalitySelected;
	}

	public List<SelectItem> getNationality() {
		return nationality;
	}

	public void setNationality(List<SelectItem> nationality) {
		this.nationality = nationality;
	}

	public HtmlInputText getInputTextCellNumber() {
		return inputTextCellNumber;
	}

	public void setInputTextCellNumber(HtmlInputText inputTextCellNumber) {
		this.inputTextCellNumber = inputTextCellNumber;
	}

	public HtmlInputText getInputTextEmirates() {
		return inputTextEmirates;
	}

	public void setInputTextEmirates(HtmlInputText inputTextEmirates) {
		this.inputTextEmirates = inputTextEmirates;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public List<SelectItem> getStateList() {
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes()
				.containsKey("stateList"))
			stateList = (ArrayList<SelectItem>) FacesContext
					.getCurrentInstance().getViewRoot().getAttributes().get(
							"stateList");
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public List<UnitView> getReplaceableUnitList()
	{
		if(viewMap.containsKey("REPLACEABLE_UNITS") && viewMap.get("REPLACEABLE_UNITS") !=null)
			replaceableUnitList = (List<UnitView>) viewMap.get("REPLACEABLE_UNITS") ;
		return replaceableUnitList;
	}

	public void setReplaceableUnitList(List<UnitView> replaceableUnitList) {
		this.replaceableUnitList = replaceableUnitList;
	}

	public HtmlDataTable getUnitDataTable() {
		return unitDataTable;
	}

	public void setUnitDataTable(HtmlDataTable unitDataTable) {
		this.unitDataTable = unitDataTable;
	}

	public Integer getUnitPaginatorMaxPages() {
		return unitPaginatorMaxPages;
	}

	public void setUnitPaginatorMaxPages(Integer unitPaginatorMaxPages) {
		this.unitPaginatorMaxPages = unitPaginatorMaxPages;
	}

	public Integer getUnitPaginatorRows() {
		return unitPaginatorRows;
	}

	public void setUnitPaginatorRows(Integer unitPaginatorRows) {
		this.unitPaginatorRows = unitPaginatorRows;
	}

	public Integer getUnitRecordSize()
	{
		unitRecordSize = (Integer) viewMap.get("unitRecordSize");
		if (unitRecordSize == null)
			unitRecordSize = 0;
		return unitRecordSize;
	}

	public void setUnitRecordSize(Integer unitRecordSize) {
		this.unitRecordSize = unitRecordSize;
	}
	@SuppressWarnings("unchecked")
	public void unRegisteredBidderSelected()
	{
		try {
		PersonView unRegBidder = (PersonView) dataTable.getRowData();
		viewMap.put("UNREGISTERED_BIDDER", unRegBidder);
		Long auctionId = (Long) viewMap.get("AUCTION_ID");
		Long auctionUnitId = (Long) viewMap.get("AUCTION_UNIT_ID");
		List<UnitView> unitList = new PropertyService().getSameCostUnitsByBidderId(unRegBidder.getPersonId(), auctionId, auctionUnitId);
		if(unitList !=null && unitList.size()>0)
			{
				viewMap.put("SHOW_UNIT_SECTION", true);
				viewMap.put("REPLACEABLE_UNITS", unitList);
				unitRecordSize = unitList.size();
				viewMap.put("unitRecordSize", unitRecordSize);
				getReplaceableUnitList();
				detailTab.setSelectedTab(replaceUnitsTab.getId());
				replaceUnitsTab.setRendered(true);
			}
		else
			{
			    replaceUnitsTab.setRendered(false);
				viewMap.put("SHOW_UNIT_SECTION", false);
				viewMap.remove("SELECTED_BIDDER_ID");
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage("auction.selectwinner.errMsg.unableToRegistered"));
			}
		txtSelectedName.setValue(null);
		txtSelectedSocialSecNo.setValue(null);
		txtSelectedNationality.setValue(null);
		txtSelectedEmirates.setValue(null);
		txtSelectedCellNumber.setValue(null);
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("findReplaceableUnits crashed |",e);
		}
		
	}

	public Boolean getShowUnitSection() 
	{
		showUnitSection = false;
		if(viewMap.containsKey("SHOW_UNIT_SECTION"))
			showUnitSection= (Boolean) viewMap.get("SHOW_UNIT_SECTION");

		return showUnitSection;
	}

	public void setShowUnitSection(Boolean showUnitSection) {
		this.showUnitSection = showUnitSection;
	}

	public HtmlInputText getTxtSelectedName() {
		return txtSelectedName;
	}

	public void setTxtSelectedName(HtmlInputText txtSelectedName) {
		this.txtSelectedName = txtSelectedName;
	}

	public HtmlInputText getTxtSelectedSocialSecNo() {
		return txtSelectedSocialSecNo;
	}

	public void setTxtSelectedSocialSecNo(HtmlInputText txtSelectedSocialSecNo) {
		this.txtSelectedSocialSecNo = txtSelectedSocialSecNo;
	}

	public HtmlInputText getTxtSelectedNationality() {
		return txtSelectedNationality;
	}

	public void setTxtSelectedNationality(HtmlInputText txtSelectedNationality) {
		this.txtSelectedNationality = txtSelectedNationality;
	}

	public HtmlInputText getTxtSelectedEmirates() {
		return txtSelectedEmirates;
	}

	public void setTxtSelectedEmirates(HtmlInputText txtSelectedEmirates) {
		this.txtSelectedEmirates = txtSelectedEmirates;
	}

	public HtmlInputText getTxtSelectedCellNumber() {
		return txtSelectedCellNumber;
	}

	public void setTxtSelectedCellNumber(HtmlInputText txtSelectedCellNumber) {
		this.txtSelectedCellNumber = txtSelectedCellNumber;
	}

	public HtmlInputText getTxtBidPrice() {
		return txtBidPrice;
	}

	public void setTxtBidPrice(HtmlInputText txtBidPrice) {
		this.txtBidPrice = txtBidPrice;
	}

	public HtmlSelectBooleanCheckbox getChkUnit() {
		return chkUnit;
	}

	public void setChkUnit(HtmlSelectBooleanCheckbox chkUnit) {
		this.chkUnit = chkUnit;
	}

	public HtmlTab getReplaceUnitsTab() {
		return replaceUnitsTab;
	}

	public void setReplaceUnitsTab(HtmlTab replaceUnitsTab) {
		this.replaceUnitsTab = replaceUnitsTab;
	}

	public HtmlTab getBidderTab() {
		return bidderTab;
	}

	public void setBidderTab(HtmlTab bidderTab) {
		this.bidderTab = bidderTab;
	}

	public HtmlTabPanel getDetailTab() {
		return detailTab;
	}

	public void setDetailTab(HtmlTabPanel detailTab) {
		this.detailTab = detailTab;
	}
	@SuppressWarnings("unchecked")
	public void registeredBidderSelected()
	{
		detailTab.setSelectedTab(bidderTab.getId());
		replaceUnitsTab.setRendered(false);
		PersonView regBidder = (PersonView) dataTable.getRowData();
		
		viewMap.put("SELECTED_BIDDER_ID", regBidder.getPersonId());
		String name;
		//name = (regBidder.getIsCompany()==0)?regBidder.getFirstName()+" "+ regBidder.getMiddleName() +" " +regBidder.getLastName():regBidder.getCompanyName();
		name = regBidder.getPersonFullName();
		txtSelectedName.setValue(name);
		txtSelectedSocialSecNo.setValue(regBidder.getSocialSecNumber());
		txtSelectedNationality.setValue(getIsEnglishLocale()?regBidder.getNationalityEn():regBidder.getNationalityAr());
		txtSelectedEmirates.setValue("emirates");
		txtSelectedCellNumber.setValue(regBidder.getCellNumber());
		replaceableUnitList.clear();
		viewMap.put("REPLACEABLE_UNITS", replaceableUnitList);
		viewMap.put("UNREGISTERED_BIDDER",new PersonView());
		viewMap.put("SHOW_OLD_NEW_UNIT", false);
	}

	public HtmlSelectBooleanCheckbox getChkBidder() {
		return chkBidder;
	}

	public void setChkBidder(HtmlSelectBooleanCheckbox chkBidder) {
		this.chkBidder = chkBidder;
	}
	@SuppressWarnings("unchecked")
	public void replaceUnit()
	{
		UnitView unitToBeReplaced = new UnitView();
		PersonView unRgstrdBidder = new PersonView();
		PropertyService ps = new PropertyService();
		if(viewMap.get("UNREGISTERED_BIDDER") !=null && ((PersonView)viewMap.get("UNREGISTERED_BIDDER")).getPersonId() != null)
		{
			unRgstrdBidder= (PersonView) viewMap.get("UNREGISTERED_BIDDER");
			unitToBeReplaced = (UnitView) unitDataTable.getRowData();
			hdnAuctionId = (Long) viewMap.get("AUCTION_ID");
			Long unitIdToWin = (Long) viewMap.get("NEW_UNIT_ID");
			if(ps.isUnitReplaced(hdnAuctionId, unRgstrdBidder.getPersonId(), unitToBeReplaced.getUnitId()) )
			{	
				errorMessages.add(CommonUtil.getBundleMessage("auction.selectwinner.errorMsg.alreadyReplaced"));
				return;
			}
//			Remove the comments, if you want to replace only those unit which are from same property.
//			if(!ps.isFromSameProperty(unitIdToWin, unitToBeReplaced.getUnitId()))
//			{
//				errorMessages.add(CommonUtil.getBundleMessage("auction.selectwinner.errorMsg.differentProperty"));
//				return;
//			}
			Long oldAuctionUnitId = ps.getAuctionUnitIdByAuctionAndUnitId(unitToBeReplaced.getUnitId(), hdnAuctionId);
			
			viewMap.put("OLD_AUCTION_UNIT_ID", oldAuctionUnitId);
			viewMap.put("UNIT_TO_BE_REPLACED", unitToBeReplaced);
			viewMap.put("SELECTED_BIDDER_ID", unRgstrdBidder.getPersonId());
			viewMap.put("OLD_UNIT_NUMBER", unitToBeReplaced.getUnitNumber());
			viewMap.put("OLD_UNIT_ID", unitToBeReplaced.getUnitId());
			viewMap.put("SHOW_OLD_NEW_UNIT", true);
			
			
			String name;
			name = (unRgstrdBidder.getIsCompany()==0)?unRgstrdBidder.getFirstName()+" "+ unRgstrdBidder.getMiddleName() +" " +unRgstrdBidder.getLastName():unRgstrdBidder.getCompanyName();
			txtSelectedName.setValue(name);
			txtSelectedSocialSecNo.setValue(unRgstrdBidder.getSocialSecNumber());
			txtSelectedNationality.setValue(getIsEnglishLocale()?unRgstrdBidder.getNationalityEn():unRgstrdBidder.getNationalityAr());
			txtSelectedEmirates.setValue("emirates");
			txtSelectedCellNumber.setValue(unRgstrdBidder.getCellNumber());
			detailTab.setSelectedTab(bidderTab.getId());
		}
		
	}
	@SuppressWarnings( "unchecked" )	
    public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	@SuppressWarnings( "unchecked" )
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
	public String getNewUnitNumber()
	{
		String unitNumber=null;
		if(viewMap.get("NEW_UNIT_NUMBER") !=null)
			unitNumber =viewMap.get("NEW_UNIT_NUMBER").toString();
		return unitNumber;
	}
	public String getUnitNumberToBeReplaced()
	{
		String unitNumber=null;
		if(viewMap.get("OLD_UNIT_NUMBER") !=null)
			unitNumber =viewMap.get("OLD_UNIT_NUMBER").toString();
		return unitNumber;
	}
	public String getWarningMessage()
	{
		String msg="";
		msg+=CommonUtil.getBundleMessage("auction.selectwinner.warngMsg.replacingUnitPart1")+ " "+
			getUnitNumberToBeReplaced()+" "+CommonUtil.getBundleMessage("auction.selectwinner.warngMsg.replacingUnitPart2")+" "+getNewUnitNumber();
		return msg;
	}
	public boolean getShowOldNewUnit()
	{
		boolean isShow = false;
		if(viewMap.get("SHOW_OLD_NEW_UNIT") != null && (Boolean)viewMap.get("SHOW_OLD_NEW_UNIT"))
			isShow = true;
		return isShow;
	}
}
