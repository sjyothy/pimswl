package com.avanza.pims.web.backingbeans;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationState;
import com.avanza.notificationservice.notification.PriorityType;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.PaymentManager;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.GenerateNotificationView;
import com.avanza.pims.ws.vo.NotificationView;
import com.avanza.ui.util.ResourceUtil;

public class GenerateNotification extends AbstractController {
	private transient Logger logger = Logger.getLogger(NotificationSend.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

	private ServletContext context = (ServletContext) FacesContext
			.getCurrentInstance().getExternalContext().getContext();

	private Map<String, Long> auctionStatusMap = new HashMap<String, Long>();
	private Map<String, String> notificationStatusMap = new HashMap<String, String>();

	public GenerateNotification() {
		
		System.out.println("NotificationSend Constructor");

	}

	private String message;

	private AuctionView auctionView = new AuctionView();

	private GenerateNotificationView dataItem = new GenerateNotificationView();
	private List<GenerateNotificationView> dataList = new ArrayList<GenerateNotificationView>();

	private NotificationView notificationView = new NotificationView();
	
	private List<SelectItem> personType = new ArrayList<SelectItem>();

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String hhStart = "";
	private String mmStart = "";
	private String hhEnd = "";
	private String mmEnd = "";
	
	Map sessionMap;
	
	private List<String> errorMessages;
	private String infoMessage = "";

	private HtmlDataTable dataTable;

	private HtmlInputHidden addCount = new HtmlInputHidden();

	private String sortField = null;
	private Boolean sortAscending = true;

	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;

	private Date srchSendDate = null;
	private Date srchNotificationDate = null;
	
	// Search Criteria 
	// Date
	private Date srchExpiryDateFrom;
	private Date srchExpiryDateTo;
	
	// String
	private String srchRequestNumber = null;
	private String srchContractNumber = null;
	private String srchPersonName = null;
	private String srchProcedureType = null;
	private String srchPersonType = null;
	private String srchEmail = null;
	private String srchCellNumber = null;

	
	public Map<String, String> getNotificationStatusMap() {
		return notificationStatusMap;
	}

	public void setNotificationStatusMap(
			Map<String, String> notificationStatusMap) {
		this.notificationStatusMap = notificationStatusMap;
	}

	public NotificationView getNotificationView() {
		return notificationView;
	}

	public void setNotificationView(NotificationView notificationView) {
		this.notificationView = notificationView;
	}

	public String getSrchProcedureType() {
		return srchProcedureType;
	}

	public void setSrchProcedureType(String srchProcedureType) {
		this.srchProcedureType = srchProcedureType;
	}

	public String getSrchRequestNumber() {
		return srchRequestNumber;
	}

	public void setSrchRequestNumber(String srchRequestNumber) {
		this.srchRequestNumber = srchRequestNumber;
	}

	public String getSrchContractNumber() {
		return srchContractNumber;
	}

	public void setSrchContractNumber(String srchContractNumber) {
		this.srchContractNumber = srchContractNumber;
	}

	public String getSrchPersonName() {
		return srchPersonName;
	}

	public void setSrchPersonName(String srchPersonName) {
		this.srchPersonName = srchPersonName;
	}

	public Date getSrchSendDate() {
		return srchSendDate;
	}

	public void setSrchSendDate(Date srchSendDate) {
		this.srchSendDate = srchSendDate;
	}

	public Date getSrchNotificationDate() {
		return srchNotificationDate;
	}

	public void setSrchNotificationDate(Date srchNotificationDate) {
		this.srchNotificationDate = srchNotificationDate;
	}

	/*
	 * Methods
	 */
	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	public void clearSessionMap() {
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try {
			//			sessionMap.remove("auctionList");
			/*sessionMap.remove(WebConstants.FROM_SEARCH);
			sessionMap.remove(WebConstants.AUCTION_STATUS_MAP);
			sessionMap.remove(WebConstants.ROW_ID);
			sessionMap.remove(WebConstants.FIRST_TIME);
			sessionMap.remove(WebConstants.VIEW_MODE);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.SELECT_VENUE_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
			sessionMap.remove(WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ID);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.FULL_VIEW_MODE);*/
			logger.logInfo("clearSessionMap() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}

	public void  generateSelectedNotification()
    {
		
    	String methodName ="generateSelectedNotification";
    	
    	logger.logInfo(methodName+"|"+"Start...");
    	
    	List<GenerateNotificationView> notificationViewList=new ArrayList<GenerateNotificationView>(5);
    	
    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
		.getAttributes();
    	
    	if( viewMap.containsKey("dataListForGeneration") ){
    		
    		notificationViewList=(List<GenerateNotificationView>) viewMap.get("dataListForGeneration"); 
    		int count = 0;
    	for (GenerateNotificationView notficationViewObj : notificationViewList) 
    	{
    		Notification notificationToBeSent = null;
    		if(notficationViewObj.getSelected() != null && !notficationViewObj.equals(""))
    		{
    			if(notficationViewObj.getSelected())
    			{
    			
	    		 	// Get Protocol Adapter
	    			
	    			ChannelCatalog catalog = ChannelCatalog.getInstance();
	    			ProtocolAdapter protocolAdapter = catalog.getAdapter(notficationViewObj.getChannelId());
	    			
	    			// Transform to Notification Object
	    			
	    			if(notficationViewObj.getRecipientToList()== null || notficationViewObj.getRecipientToList().equals("") ||  notficationViewObj.getRecipientToList().equals("null")){
	    				
	    			}
	    			else if(notficationViewObj.getRecipientToList() != null  || !notficationViewObj.getRecipientToList().equals("") || !notficationViewObj.getRecipientToList().equals("null")){
	    				 notificationToBeSent = transformToNotification( notficationViewObj );
	    			}
	    			
	    			// Call Notify of Adapter
	    			if( notificationToBeSent != null ){
	    				protocolAdapter.notify(notificationToBeSent);
	    				count ++;
	    			}
    			
    			
    			
             // Some Message handling
//    		 if(psa.addChequeStatus(paySchId, statusId,chequeNo)){
//    		        	errorMessages = new ArrayList<String>();
//    					errorMessages.add(getBundleMessage("bouncedCheque.msg.chequeBounced"));
//           		 }
//    		 else{
    			   	errorMessages = new ArrayList<String>();
					errorMessages.add(count + " message(s) processed successfully.");
//    		 }
    		 
    		 logger.logInfo("Notification Processed :" + notficationViewObj.getNotificationId());
    		
    		}
    	  }
		}
    }	                                 
    	logger.logInfo(methodName+"|"+"End...");
    	viewMap.clear();
    	
    	//loadDataList();
       //return "btnBounce_Click";
   }
	
	public Notification transformToNotification(GenerateNotificationView notificationView){
		
		Notification notification = new Notification();
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		
		notification.setNotificationId( notificationView.getNotificationId() );
		notification.setServerInstanceId(notificationView.getServerInstanceId() );
		notification.setBody( notificationView.getBody() );
		notification.setChannelId( notificationView.getChannelId() );
		notification.setContent(null );
		notification.setCreatedBy( notificationView.getCreatedBy() );
		notification.setCreatedOn( notificationView.getCreatedOn() );
		notification.setDeleted(new Boolean(false) );
		notification.setDocumentList( null );
		
		notification.setExternalId1( notificationView.getExternalId1() );
		notification.setExternalId2( notificationView.getExternalId2() );
		notification.setExternalId3( notificationView.getExternalId3() );
		notification.setLocale( notificationView.getLocale() );
		
		

				
		notification.setNsFrom( notificationView.getNsFrom() );
		
		//  Lowest(0), Low(1), Medium(2), High(3), Highest(4);
		if( notificationView.getPriority() != null ){
			if( notificationView.getPriority().equals("Lowest") ){
				
				notification.setPriority( PriorityType.Lowest ) ;
			}
			else if( notificationView.getPriority().equals("Low") ){
				
				notification.setPriority( PriorityType.Low ) ;
			}
			else if( notificationView.getPriority().equals("Medium") ){
						
						notification.setPriority( PriorityType.Medium ) ;
					}
			else if( notificationView.getPriority().equals("High") ){
				
				notification.setPriority( PriorityType.High ) ;
			}
			else if( notificationView.getPriority().equals("Highest") ){
				
				notification.setPriority( PriorityType.Highest ) ;
			}
			else{
				
				notification.setPriority( PriorityType.Medium ) ;
			}
		}else{
			notification.setPriority( PriorityType.Medium ) ;
		}
		
		notification.setProtocolFields( null );
		notification.setRecipientBccList( notificationView.getRecipientBccList() );
		notification.setRecipientCcList( notificationView.getRecipientCcList() );
		notification.setRecipientToList( notificationView.getRecipientToList() );
		notification.setRecipientToList( notificationView.getRecipientToList() );
		
		notification.setRecordStatus( notificationView.getRecordStatus() );
		notification.setRetryCount(  notificationView.getRetryCount() );
		
		notification.setSentTime( dateFormat.format(new Date()) );
		
		if(notificationView.getState() != null ){
		if( notificationView.getState().equals("None") ){
			
			notification.setNotificationState( NotificationState.None ) ;
		}
		else if( notificationView.getState().equals("New") ){
			
			notification.setNotificationState( NotificationState.New ) ;
		}
		else if( notificationView.getState().equals("Wait") ){
					
					notification.setNotificationState( NotificationState.Wait ) ;
				}
		else if( notificationView.getState().equals("Sent") ){
			
			notification.setNotificationState( NotificationState.Sent ) ;
		}
		else if( notificationView.getState().equals("Failed") ){
			
			notification.setNotificationState( NotificationState.Failed ) ;
		}
		else if( notificationView.getState().equals("Expired") ){
			
			notification.setNotificationState( NotificationState.Expired ) ;
		}
		else{
			
			notification.setNotificationState( NotificationState.None ) ;
		}
		}
		else{
			notification.setNotificationState( NotificationState.None ) ;
		}
		notification.setStatusText(  notificationView.getStatusText() );
		notification.setServerInstanceId( notificationView.getServerInstanceId() );
		notification.setSubject( notificationView.getSubject() );
		if( notificationView.getTimeOut() != null){
			notification.setTimeOut( notificationView.getTimeOut() );
		}
		else{
			notification.setTimeOut(60 );
		}
		notification.setUpdatedBy( notificationView.getUpdatedBy() );
		notification.setUpdatedOn( notificationView.getUpdatedOn() );
		
		if( notificationView.getTimeOut() != null){
			notification.setWaitTime( notificationView.getWaitTime() );
		}
		else{
			notification.setWaitTime( 60 );
		}
		notification.setUpdation( notificationView.getUpdatedBy() );

		return notification;
	}

	
	
	
	@SuppressWarnings("unchecked")
	public void openDetailsPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openDetailsPopup() started...");
		try
		{
	        final String viewId = "/NotificationSend.jsp"; 
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	        String javaScriptText = "javascript:showDetailPopup();";
	
	        // Add the Java script to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openDetailsPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openDetailsPopup() crashed ", exception);
		}
    }
	
	
	public String openDetailsPopupForView()
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");


		
		openDetailsPopup("VIEW");
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	
	public String openDetailsPopupForGenerate()
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
		.getAttributes();
		//viewMap.put("dataListForGeneration", auctionList);
		List<GenerateNotificationView> lstObjs = (List) viewMap.get("dataListForGeneration");
		List<GenerateNotificationView> selectedList = new ArrayList();
		
		if(lstObjs != null){
			
			for(int i =0; i < lstObjs.size(); i++ ){
				
				if( lstObjs.get(i).getSelected()!= null && lstObjs.get(i).getSelected() ){
					
					selectedList.add( lstObjs.get(i) );
				}
			}
		}
		if(selectedList != null && selectedList.size() > 0  )
		{
			sessionMap.put("dataListForGeneration", selectedList);
		
			openDetailsPopup("ADD");
		}
		else{
			
			errorMessages = new ArrayList<String>();
			errorMessages.add(getBundleMessage("notificationGenerate.noneSelected")); //modified by muhammad.akif
					
			
		}
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	public String openDetailsPopup(String screenMode)
	{
		String methodName="openDetailsPopup";
		logger.logInfo(methodName+"|"+"Start..");

		
		        FacesContext facesContext = FacesContext.getCurrentInstance();
		        if(!screenMode.equals("ADD") ){
		        	NotificationView notificationViewRow =(NotificationView) dataTable.getRowData();
		        	
		        	 sessionMap.put( WebConstants.NOTIFICATION_VIEW_OBJECT , notificationViewRow);
		        }
		        
//		        String extrajavaScriptText ="var screen_width = screen.width;"+
//                " var screen_height = screen.height;"+
//                " window.open('receivePayment.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=no');";

		        String extrajavaScriptText = " var screen_width = screen.width; " +
		        " var screen_height = screen.height; " +
		        " var popup_width = screen_width-790; " +
		        " var popup_height = screen_height-570; " +//modified by muhammad.akif
		        " var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20; " +
	              
		        " var popup = window.open('NotificationDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes'); " +
		        " popup.focus(); " ;
		        
		       
		        sessionMap.put( WebConstants.NOTIFICATION_SCREEN_MODE , screenMode);
		        
		        openPopUp("NotificationDetails.jsf", extrajavaScriptText);
    	   
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/NotificationSend.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public Boolean getIsEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}

	@SuppressWarnings("unchecked")
	public List<GenerateNotificationView> getAuctionDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		
		return dataList;
	}

	@SuppressWarnings("unchecked")
	private void loadAuctionList() {

		logger.logInfo("loadAuctionList() started...");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		notificationStatusMap = (Map<String, String>) sessionMap
				.get(WebConstants.NOTIFICATION_STATUS_MAP);

		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();

		try {
			dataList = new ArrayList<GenerateNotificationView>();

			String dateFormat = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			List<GenerateNotificationView> auctionList = null;
			auctionList = dataList;
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put("dataListForGeneration", auctionList);
			recordSize = auctionList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
 
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);

			Long statusId = null;
			if (!auctionList.isEmpty()) {
				for (GenerateNotificationView temp : auctionList) {
					

					if (statusId.compareTo(auctionStatusMap
							.get(WebConstants.Statuses.AUCTION_READY_STATUS)) == 0) {
					
					}
					dataList.add(temp);
				}
			} else {
				errorMessages = new ArrayList<String>();
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadAuctionList() completed successfully!!!");
		}

		catch (Exception exception) {
			logger.LogException("loadAuctionList() crashed ", exception);
		}
	}

	/*
	 *  Validation Methods
	 */

	public String getInvalidMessage(String field) {

		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try {
			message = message
					.append(getBundleMessage(field))
					.append(" ")
					.append(
							getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
			logger
					.logInfo("getInvalidMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger
					.LogException("getInvalidMessage(String) crashed ",
							exception);
		}
		return message.toString();
	}

	public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		if (((hhStart.trim().length() > 0) && (mmStart.trim().length() == 0))
				|| ((mmStart.trim().length() > 0) && (hhStart.trim().length() == 0))) {
			errorMessages
					.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.START_TIME));
			validated = false;
		}
		if (((hhEnd.trim().length() > 0) && (mmEnd.trim().length() == 0))
				|| ((mmEnd.trim().length() > 0) && (hhEnd.trim().length() == 0))) {
			errorMessages
					.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.END_TIME));
			validated = false;
		}

		if (auctionView.getAuctionDateVal() == null) {
			hhStart = hhStart.trim();
			mmStart = mmStart.trim();
			hhEnd = hhEnd.trim();
			mmEnd = mmEnd.trim();
			String tempStartTime = hhStart + mmStart;
			String tempEndTime = hhEnd + mmEnd;
			if ((!tempStartTime.equals("")) || (!tempEndTime.equals(""))) {
				errorMessages
						.add(getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.SHOULD_HAVE_AUCTION_DATE));
				validated = false;
			}
		}

		if (validated) {
			auctionView.setAuctionStartTime(hhStart + ":" + mmStart);
			auctionView.setAuctionEndTime(hhEnd + ":" + mmEnd);
		}
		return validated;
	}

	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message; //+ "\n";
			}
			messageList = messageList + "</UL>";//</B></FONT>\n";
		}
		return (messageList);

	}

	/*
	 *  JSF Lifecycle methods 
	 */

	@Override
	public void init() {
			
		super.init();
		
		sessionMap =FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) {
				clearSessionMap();
				loadStatusMap();
			}

			if (dataList != null && dataList.size() > 0) {

				List<GenerateNotificationView> auctionList = null;
				auctionList = dataList;
				Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
						.getAttributes();
				viewMap.put("dataListForGeneration", auctionList);
				recordSize = auctionList.size();
				viewMap.put("recordSize", recordSize);
				paginatorRows = getPaginatorRows();
				paginatorMaxPages = recordSize / paginatorRows;

				if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
					paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
				viewMap.put("paginatorMaxPages", paginatorMaxPages);
			}
			logger.logInfo("init() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}

	}
	
	


	@Override
	public void preprocess() {
		
		super.preprocess();
		logger.logInfo("NotificationSend preprocess");
	}

	@Override
	public void prerender() {
	
		super.prerender();
		logger.logInfo("NotificationSend prerender");
	}

	private void loadStatusMap() {

		//		None, New, Sent, Failed, Expired, Wait

		notificationStatusMap.put("None", "None");
		notificationStatusMap.put("New", "New");
		notificationStatusMap.put("Sent", "Sent");

		notificationStatusMap.put("Failed", "Failed");
		notificationStatusMap.put("Expired", "Expired");
		notificationStatusMap.put("Wait", "Wait");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		sessionMap.put(WebConstants.NOTIFICATION_STATUS_MAP,
				notificationStatusMap);
	}

	/*
	 * Button Click Action Handlers 
	 */

	public String auctionDetailsEditMode() {
		logger.logInfo("auctionDetailsEditMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.EDIT_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			setRequestParam(WebConstants.BACK_SCREEN,
					WebConstants.BACK_SCREEN_AUCTION_SEARCH);
			//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsEditMode() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("auctionDetailsEditMode() crashed ", exception);
		}
		return "auctionDetailsEditMode";
	}

	public String notificationDetailsEditMode() {
		logger.logInfo("notificationDetailsEditMode() started...");
		try {
			clearSessionMap();

			NotificationView notifcationView = (NotificationView) dataTable
					.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.NOTIFICATION_ID, notifcationView
					.getNotificationId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.EDIT_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			setRequestParam(WebConstants.BACK_SCREEN,
					WebConstants.BACK_SCREEN_AUCTION_SEARCH);

			logger
					.logInfo("notificationDetailsEditMode() completed successfully!!!");
		}

		catch (Exception exception) {
			logger.LogException("auctionDetailsEditMode() crashed ", exception);
		}
		return "notificationDetailsEditMode";
	}

	public String notificationDetailsViewMode() {
		logger.logInfo("notificationDetailsViewMode() started...");

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		notificationStatusMap = (Map<String, String>) sessionMap
				.get(WebConstants.NOTIFICATION_STATUS_MAP);

		try {
			clearSessionMap();

			NotificationView notificationView = (NotificationView) dataTable
					.getRowData();

			sessionMap.put(WebConstants.NOTIFICATION_ID, notificationView
					.getNotificationId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);

			//if((statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0) || (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_DATE_VENUE_DECIDED_STATUS))==0) || (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_READY_STATUS))==0))
			//{
			//	sessionMap.put(WebConstants.SELECT_VENUE_MODE,WebConstants.DATE_VENUE_SELECTED);
			//}

			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);

			//    	 	if(statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0){
			//    	 		return "auctionCompletedViewMode";
			//    	 	}

			logger
					.logInfo("notificationDetailsViewMode() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}

		return "notificationDetailsViewMode";
	}

	public String auctionDetailsViewMode() {
		logger.logInfo("auctionDetailsViewMode() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
		auctionStatusMap = (Map<String, Long>) sessionMap
				.get(WebConstants.AUCTION_STATUS_MAP);
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Long statusId = Long.parseLong(aucView.getAuctionStatusId());
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			if ((statusId.compareTo(auctionStatusMap
					.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS)) == 0)
					|| (statusId
							.compareTo(auctionStatusMap
									.get(WebConstants.Statuses.AUCTION_DATE_VENUE_DECIDED_STATUS)) == 0)
					|| (statusId.compareTo(auctionStatusMap
							.get(WebConstants.Statuses.AUCTION_READY_STATUS)) == 0)) {
				sessionMap.put(WebConstants.SELECT_VENUE_MODE,
						WebConstants.DATE_VENUE_SELECTED);
			}
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);

			if (statusId.compareTo(auctionStatusMap
					.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS)) == 0) {
				return "auctionCompletedViewMode";
			}
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
		return "auctionDetailsViewMode";
	}

	public String auctionDetailsFullViewMode() {
		logger.logInfo("auctionDetailsViewMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.FULL_VIEW_MODE,
					WebConstants.FULL_VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
		return "auctionDetailsViewMode";
	}

	public String auctionDetailsConductMode() {
		logger.logInfo("auctionDetailsConductMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.CONDUCT_AUCTION_MODE,
					WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsConductMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionDetailsConductMode() crashed ",
					exception);
		}
		return "auctionDetailsConductMode";
	}

	public String auctionSelectDateVenueMode() {
		logger.logInfo("auctionSelectDateVenueMode() started...");
		try {
			clearSessionMap();
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			sessionMap.put(WebConstants.SELECT_VENUE_MODE,
					WebConstants.SELECT_VENUE_MODE);
			sessionMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			sessionMap.put(WebConstants.FROM_SEARCH, WebConstants.FROM_SEARCH);
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionSelectDateVenueMode() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("auctionSelectDateVenueMode() crashed ",
					exception);
		}
		return "auctionSelectDateVenueMode";
	}

	public String searchAuctions() {
		logger.logInfo("searchAuctions() started...");
		try {
			if (validateFields()) {
				//auctionView.setAuctionStartTime(hhStart+":"+mmStart);
				loadAuctionList();
			}

			logger.logInfo("searchAuctions() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}
		return "searchAuctions";
	}

	public void searchToGenerate() {
		logger.logInfo("searchNotificationsTest() started...");

		List notificationViewList = new ArrayList();

		try {

			HashMap hmCriteria = bulidSearchCriteria();

			PaymentManager pm = new PaymentManager();

			List resultList = pm.searchForNotifications(hmCriteria);

			for (Object object : resultList) {

				Object[] objArray = (Object[]) object;

				GenerateNotificationView generateNotificationView = processResultRow(objArray);

				notificationViewList.add(generateNotificationView);

			}

			dataList = notificationViewList;

			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("dataListForGeneration", dataList);
			String dateFormat = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			List<GenerateNotificationView> auctionList = null; //propertyServiceAgent.getAuctions(auctionView);
			auctionList = dataList;
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			viewMap.put("dataListForGeneration", auctionList);
			recordSize = auctionList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			//			if((recordSize%paginatorRows)>0)
			//				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);

			logger
					.logInfo("searchNotificationsTest() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}

		//return "";
	}

	private HashMap bulidSearchCriteria() {

		HashMap hmSearchCriteria = new HashMap();
		
		if (srchExpiryDateFrom != null && !srchExpiryDateFrom.equals("")) {

			hmSearchCriteria.put("srchExpiryDateFrom", srchExpiryDateFrom);
		}
		
		if (srchExpiryDateTo != null && !srchExpiryDateTo.equals("")) {

			hmSearchCriteria.put("srchExpiryDateTo", srchExpiryDateTo);
		}

		if (srchContractNumber != null && !srchContractNumber.equals("")) {

			hmSearchCriteria.put("srchContractNumber", srchContractNumber);
		}
		
		
		if (srchPersonName != null && !srchPersonName.equals("")) {

			hmSearchCriteria.put("srchPersonName", srchPersonName);
		}
		
		if (srchEmail != null && !srchEmail.equals("")) {

			hmSearchCriteria.put("srchEmail", srchEmail);
		}
		
		if (srchCellNumber != null && !srchCellNumber.equals("")) {

			hmSearchCriteria.put("srchCellNumber", srchCellNumber);
		}
		
		if (srchRequestNumber != null && !srchRequestNumber.equals("")) {

			hmSearchCriteria.put("srchRequestNumber", srchRequestNumber);
		}
		
		if (srchProcedureType != null && !srchProcedureType.equals("")) {

			hmSearchCriteria.put("srchProcedureType", srchProcedureType);
		}

		if (srchPersonType != null && !srchPersonType.equals("")
				&& !srchPersonType.equals("0")) {

			hmSearchCriteria.put("srchPersonType", srchPersonType);
		}
		else{
			
			hmSearchCriteria.put("srchPersonType", "30008");
		}

		return hmSearchCriteria;
	}

	private GenerateNotificationView processResultRow(Object[] resultRow) {

		GenerateNotificationView gnView = new GenerateNotificationView();
		
//		
//		p.PERSON_ID, p.CELL_NUMBER ,"
//		+ " NVL(p.FIRST_NAME,'')  || ' ' || NVL( p.MIDDLE_NAME , '' )|| CASE WHEN p.MIDDLE_NAME IS NULL THEN '' "
//		+ " WHEN RTRIM(p.MIDDLE_NAME) = ''  THEN '' ELSE ' ' END || NVL(  p.LAST_NAME, '' ) as PERSON_NAME , "
//		+ " c.CONTRACT_NUMBER , r.REQUEST_NUMBER, ci.EMAIL ,  "
//		+ " CONTACT_INFO_ID, c.CONTRACT_ID, r.REQUEST_ID "
//		
		

		gnView.setExternalId1(Long.valueOf(((BigDecimal)resultRow[0]).longValue()));

		if (resultRow[1] != null)
			gnView.setMobileNumber( resultRow[1].toString() );
		
		gnView.setPersonName( resultRow[2].toString() );
		
		if (resultRow[3] != null)
			gnView.setContractNo(resultRow[3].toString());

		if (resultRow[4] != null)
			gnView.setRequestNo(resultRow[4].toString());

		if (resultRow[5] != null)
			gnView.setEmailAddress( resultRow[5].toString());

		if (resultRow[7] != null)
			gnView.setExternalId2( resultRow[7].toString() );
		
		if (resultRow[8] != null)
			gnView.setExternalId3( resultRow[8].toString() );

//		gnView.setSubject(resultRow[8].toString());
//		gnView.setProtocolFields(null); //resultRow[9].toString() );
//		gnView.setState(resultRow[10].toString());
//		gnView.setContent(null);
//
//		gnView.setStatusText(resultRow[12].toString());
//		gnView.setLocale(resultRow[13].toString());
//		gnView.setBody(resultRow[14].toString());
//		gnView.setDocumentList(null);
//
//		gnView.setSentTime(resultRow[16].toString());
//		// DeadNotification 17
//		if (resultRow[18] != null)
//			gnView.setExternalId1(resultRow[18].toString());
//
//		if (resultRow[19] != null)
//			gnView.setExternalId2(resultRow[19].toString());
//
//		if (resultRow[20] != null)
//			gnView.setExternalId3(resultRow[20].toString());
//
//		gnView.setCreatedOn((Date) resultRow[21]);
//		gnView.setCreatedBy(resultRow[22].toString());
//
//		gnView.setUpdatedOn((Date) resultRow[23]);
//		gnView.setUpdatedBy(resultRow[24].toString());
//
//		if (resultRow[25] != null)
//			gnView.setContractNo(resultRow[25].toString());
//
//		if (resultRow[26] != null)
//			gnView.setRequestNo(resultRow[26].toString());
//
//		if (resultRow[27] != null)
//			gnView.setPersonName(resultRow[27].toString());
//
//		if (resultRow[28] != null)
//			gnView.setProcedureType(resultRow[28].toString());

		return gnView;
	}

	public String searchNotifications() {
		logger.logInfo("searchNotifications() started...");
		try {
			if (validateFields()) {
				//auctionView.setAuctionStartTime(hhStart+":"+mmStart);
				loadAuctionList();
			}

			logger.logInfo("searchNotifications() completed successfully!!!");
		}
		//    	catch(PimsBusinessException exception){
		//    		logger.LogException("searchAuctions() crashed ",exception);
		//    	}
		catch (Exception exception) {
			logger.LogException("searchAuctions() crashed ", exception);
		}
		return "searchAuctions";
	}

	public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "window.close();";

			clearSessionMap();

			// Add the Javascript to the rendered page's header for immediate execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("cancel() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
	}

	/*
	 * Setters / Getters
	 */

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
		//		AuctionVenueView temp = new AuctionVenueView();
		//		temp.setVenueName(auctionView.getVenueName());
		//		auctionView.setAuctionVenue(temp);
		return auctionView;
	}

	/**
	 * @param auctionView the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
		//		AuctionVenueView temp = new AuctionVenueView();
		//		temp.setVenueName(auctionView.getVenueName());
		//		auctionView.setAuctionVenue(temp);
		this.auctionView = auctionView;
	}

	/**
	 * @return the dataItem
	 */
	public GenerateNotificationView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(GenerateNotificationView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<GenerateNotificationView> getDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		//		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		dataList = (List<GenerateNotificationView>) viewMap
				.get("dataListForGeneration");
		if (dataList == null)
			dataList = new ArrayList<GenerateNotificationView>();
		//return dataList;
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<GenerateNotificationView> dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}



	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the propertyServiceAgent
	 */
	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	/**
	 * @param propertyServiceAgent the propertyServiceAgent to set
	 */
	public void setPropertyServiceAgent(
			PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(ServletContext context) {
		this.context = context;
	}

	/**
	 * @return the auctionStatusMap
	 */
	public Map<String, Long> getAuctionStatusMap() {
		return auctionStatusMap;
	}

	/**
	 * @param auctionStatusMap the auctionStatusMap to set
	 */
	public void setAuctionStatusMap(Map<String, Long> auctionStatusMap) {
		this.auctionStatusMap = auctionStatusMap;
	}

	/**
	 * @return the hhStart
	 */
	public String getHhStart() {
		return hhStart;
	}

	/**
	 * @param hhStart the hhStart to set
	 */
	public void setHhStart(String hhStart) {
		this.hhStart = hhStart;
	}

	/**
	 * @return the mmStart
	 */
	public String getMmStart() {
		return mmStart;
	}

	/**
	 * @param mmStart the mmStart to set
	 */
	public void setMmStart(String mmStart) {
		this.mmStart = mmStart;
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	/**
	 * @return the hhEnd
	 */
	public String getHhEnd() {
		return hhEnd;
	}

	/**
	 * @param hhEnd the hhEnd to set
	 */
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}

	/**
	 * @return the mmEnd
	 */
	public String getMmEnd() {
		return mmEnd;
	}

	/**
	 * @param mmEnd the mmEnd to set
	 */
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		//		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		////		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		//		paginatorMaxPages = (Integer) viewMap.get("paginatorMaxPages");
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public List<SelectItem> getPersonType() {
		return personType;
	}

	public void setPersonType(List<SelectItem> personType) {
		this.personType = personType;
	}

	public Date getSrchExpiryDateFrom() {
		return srchExpiryDateFrom;
	}

	public void setSrchExpiryDateFrom(Date srchExpiryDateFrom) {
		this.srchExpiryDateFrom = srchExpiryDateFrom;
	}

	public Date getSrchExpiryDateTo() {
		return srchExpiryDateTo;
	}

	public void setSrchExpiryDateTo(Date srchExpiryDateTo) {
		this.srchExpiryDateTo = srchExpiryDateTo;
	}

	public String getSrchPersonType() {
		return srchPersonType;
	}

	public void setSrchPersonType(String srchPersonType) {
		this.srchPersonType = srchPersonType;
	}

	public String getSrchEmail() {
		return srchEmail;
	}

	public void setSrchEmail(String srchEmail) {
		this.srchEmail = srchEmail;
	}

	public String getSrchCellNumber() {
		return srchCellNumber;
	}

	public void setSrchCellNumber(String srchCellNumber) {
		this.srchCellNumber = srchCellNumber;
	}

}
