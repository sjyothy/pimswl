package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlInputText;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.pimsevaluaterequest.PIMSPropertyEvaluationRequestBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.PropertyEvaluationCriteria;
import com.avanza.pims.report.criteria.UnitEvaluationCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.construction.ConstructionService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.EvaluationByUnitTypeView;
import com.avanza.pims.ws.vo.EvaluationRecommendationView;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;
import com.avanza.pims.ws.vo.EvaluationView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.pims.ws.vo.PortfolioEvaluationView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;

public class EvaluationApplicationDetailsBacking extends AbstractController
{
	private static final long serialVersionUID = 1L;
	private transient Logger logger;
	@SuppressWarnings("unchecked")
	Map viewMap;
	@SuppressWarnings("unchecked")
	Map sessionMap;
	private Date evaluationDate;
	private String evaluationRecomm;
	private final String noteOwner ;
	private final String externalId;
	private final String procedureTypeKey ;
	private List<DomainDataView> unitTypeList;
	private List<DomainDataView> unitSideList;
	private List<DomainDataView> requestStatusList;
	private DomainDataView requestStatusInspector;
	private DomainDataView requestStatusHOS;
	private DomainDataView requestStatusExec;
	private DomainDataView requestStatusComplete;
	private String pageMode;
	private HtmlTabPanel tabPanel;
	private final String PAGE_MODE;
	private final String PAGE_MODE_NEW ;
	private final String PAGE_MODE_STEP_DONE;
	private final String PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED;
	private final String PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED;
	private final String PAGE_MODE_HOS_RECOMMENDATION_REQUIRED;
	private final String PAGE_MODE_REQUEST_COMPLETED;
	private final String PAGE_MODE_VIEW_ONLY;
	private final String PAGE_MODE_EDIT;
	private final String REQUEST_STATUS; 
	private final String REQUEST_STATUS_NEW;
	private final String REQUEST_STATUS_INSPECTOR_REC_REQ;
	private final String REQUEST_STATUS_HOS_REC_REQ;
	private final String REQUEST_STATUS_EXEC_REC_REQ;
	private final String REQUEST_STATUS_COMPLETE;
	private final String TASK_LIST_USER_TASK;
	private final String REQUEST_VIEW;
	private final String DD_REQUEST_STATUS_COMPLETED;
	@SuppressWarnings("unused")
	private final String DD_REQUEST_STATUS_NEW;
	private final String DD_REQUEST_STATUS_INSPECTOR_REC_REQ;
	private final String DD_REQUEST_STATUS_HOS_REC_REQ;
	private final String DD_REQUEST_STATUS_EXEC_REC_REQ;
	private final String DD_LIST_UNIT_SIDE;
	private final String DD_LIST_UNIT_TYPE;
	private final String EVALUATION_UNIT_TYPE_LIST;
	private final String INSPECTOR_DONE;
	private final String HOS_DONE;
	private final String EXEC_DONE;
	@SuppressWarnings("unused")
	private final String DD_LIST_REQUEST_STATUS;
	private final String HEADING;
	private RequestView requestView;
	private boolean isInspectorRecomReq;
	private boolean isHOSRecomReq;
	private boolean isExecRecomReq;
	private boolean isCompleted;
	private final String DEFAULT_HEADING;
	private String heading;
	private List<String> errorMessages;
	private String infoMessage ;
	String hiddenPropertyId;
	String hiddenLoadProperty ;
	String hiddenUnitId;
	String hiddenLoadUnit;
	private EvaluationApplicationFilterView searchFilterView ;
	private UploadedFile selectedFile;
	private HtmlInputFileUpload fileUploadCtrl= new HtmlInputFileUpload();
	private EvaluationView evaluationView;
	private String newPropertyValue;
	private String evaluationEngr;
	private List<EvaluationByUnitTypeView> evaluationUnitList;
	private List<RequestDetailView> requestDetailViewList;
	//private String hosRecommendations;
	//private String execRecommendations;
	//private String rentCommRecommendations;
	HtmlInputText rentValueTxt;
	private boolean isShowProperty;
	private boolean isPopUp;
	private boolean isPageModeNew;
	private boolean isPageModeStepDone;
	private boolean isPageModeView;
	private boolean isPageModeInspector;
	private boolean isPageModeHOS;
	private boolean isPageModeExec;
	private boolean isPageModeCompleted;
	private boolean isStatusNew;
	private boolean isStatusInspector;
	private boolean isStatusHOS;
	private boolean isStatusExec;
	private boolean isStatusCompleted;
	private boolean isHidePropertyRent;
	private boolean isInspectorDone;
	private boolean isHOSDone;
	private boolean isExecDone;
	private boolean isRequestCreated;
	
	private RequestServiceAgent requestServiceAgent;
	
	
	private List<EvaluationRecommendationView> evaluationRecommendationList;
	private EvaluationRecommendationView evaluationRecommendationView;
	private HtmlDataTable evaluationRecommendationDataTable; 
	
	
	public EvaluationApplicationDetailsBacking()
	{
		logger 						= Logger.getLogger(EvaluationApplicationDetailsBacking.class);
		viewMap 					= FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap 					= getFacesContext().getExternalContext().getSessionMap();
		noteOwner 					= WebConstants.EvaluationApplication.EVALUATION_REQUEST;
		externalId 					= WebConstants.Attachment.EXTERNAL_ID_EVALUATION_REQUEST;
		procedureTypeKey 			= WebConstants.PROCEDURE_TYPE_EVALUATION_REQUEST;
		unitTypeList				= new ArrayList<DomainDataView>(0);
		unitSideList				= new ArrayList<DomainDataView>(0);
		requestStatusList			= new ArrayList<DomainDataView>(0);
		requestStatusInspector 		= new DomainDataView();
		requestStatusHOS			= new DomainDataView();
		requestStatusExec			= new DomainDataView();
		requestStatusComplete		= new DomainDataView();
		requestView					= new RequestView();
		pageMode					= "default";
		heading						="";
		PAGE_MODE 									="PAGE_MODE";
		PAGE_MODE_NEW 								="PAGE_MODE_NEW";
		PAGE_MODE_STEP_DONE							="PAGE_MODE_STEP_DONE";
		PAGE_MODE_EDIT 								="PAGE_MODE_EDIT";
		PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED ="PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED";
		PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED ="PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED";
		PAGE_MODE_HOS_RECOMMENDATION_REQUIRED 		="PAGE_MODE_HOS_RECOMMENDATION_REQUIRED";
		PAGE_MODE_REQUEST_COMPLETED 				="PAGE_MODE_REQUEST_COMPLETED";
		PAGE_MODE_VIEW_ONLY 						="PAGE_MODE_VIEW_ONLY";
		TASK_LIST_USER_TASK 						="TASK_LIST_USER_TASK";
		REQUEST_VIEW 								="REQUEST_VIEW";
		DD_REQUEST_STATUS_COMPLETED					="DD_REQUEST_STATUS_COMPLETED";
		DD_REQUEST_STATUS_NEW						="DD_REQUEST_STATUS_NEW";
		DD_REQUEST_STATUS_INSPECTOR_REC_REQ			="DD_REQUEST_STATUS_INSPECTOR_REC_REQ";		
		DD_REQUEST_STATUS_HOS_REC_REQ				="DD_REQUEST_STATUS_HOS_REC_REQ";
		DD_REQUEST_STATUS_EXEC_REC_REQ				="DD_REQUEST_STATUS_EXEC_REC_REQ";
		DD_LIST_UNIT_SIDE							="DD_LIST_UNIT_SIDE";
		DD_LIST_UNIT_TYPE							="DD_LIST_UNIT_TYPE";
		DD_LIST_REQUEST_STATUS						="DD_LIST_REQUEST_STATUS";
		INSPECTOR_DONE								= "INSPECTOR_DONE";
		HOS_DONE									= "HOS_DONE";
		EXEC_DONE									= "EXEC_DONE";
		HEADING 									= "HEADING";
		EVALUATION_UNIT_TYPE_LIST 					= "EVALUATION_UNIT_TYPE_LIST";
		REQUEST_STATUS 								= "REQUEST_STATUS";
		REQUEST_STATUS_NEW							= "REQUEST_STATUS_NEW";
		REQUEST_STATUS_INSPECTOR_REC_REQ			= "REQUEST_STATUS_INSPECTOR_REC_REQ";
		REQUEST_STATUS_HOS_REC_REQ					= "REQUEST_STATUS_HOS_REC_REQ";
		REQUEST_STATUS_EXEC_REC_REQ					= "REQUEST_STATUS_EXEC_REC_REQ";
		REQUEST_STATUS_COMPLETE						= "REQUEST_STATUS_COMPLETE";
		tabPanel 									= new HtmlTabPanel();
		DEFAULT_HEADING 							= WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_NEW;
		errorMessages								= new ArrayList<String>();
		infoMessage 								= "";
		hiddenPropertyId							= "";
		hiddenLoadProperty							= "";
		hiddenUnitId								= "";
		hiddenLoadUnit								= "";
		//hosRecommendations							= "";
		//execRecommendations							= "";
		//rentCommRecommendations						= "";
		searchFilterView 							= new EvaluationApplicationFilterView();
		evaluationView 								= new EvaluationView();
		evaluationRecomm							= "";
		evaluationEngr								="";
		evaluationUnitList 							= new ArrayList<EvaluationByUnitTypeView>();
		requestDetailViewList 						= new ArrayList<RequestDetailView>();
		rentValueTxt								=new HtmlInputText();
		
		evaluationRecommendationList = new ArrayList<EvaluationRecommendationView>(0);
		requestServiceAgent = new RequestServiceAgent();
		evaluationRecommendationView = new EvaluationRecommendationView();
		evaluationRecommendationDataTable = new HtmlDataTable();
	}
	@SuppressWarnings("unchecked")
	public void init() 
	{
		String methodName = "init ";
		logger.logInfo(methodName +"started.");
		if(!isPostBack())
		{
			logger.logInfo("isPostBack started.");
			try
			{
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				putValuesInMaps();
				// from evaluation application search screen
				if(sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE)!=null)
				{
					String requestMode = sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE).toString();
					setPageModeByRequestMode(requestMode);
					sessionMap.remove(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE);
					if(sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID) != null)
					{
						viewMap.put("evaluationRequestDetailsReadonlyMode", true);
						Long requestId = Long.parseLong(sessionMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID).toString());
						if(requestId!=null)
							CommonUtil.loadAttachmentsAndComments(requestId.toString());
						viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
						sessionMap.remove(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
						requestView = new RequestService().getRequestById(requestId);
						viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_VIEW, requestView);
						if(requestView != null && requestView.getRequestId() != null)
							setRequestStatusByStautsId(requestView.getStatusId());
						setPageModeByRequestMode(requestMode);
						EvaluationRequestDetailsView requestDetailsTabView = new RequestService().getEvaluationRequestById(requestId, getArgMap());	
						viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
						populateRequestDetailsTab();
						
						
						
						
					}
				}
				// from Task List
				else if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null)
				{
					viewMap.put("evaluationRequestDetailsReadonlyMode", true);
					checkTask();
				}	
				if(sessionMap.containsKey("IS_POPUP"))
				{
					isPopUp=(Boolean)sessionMap.get("IS_POPUP");
					sessionMap.remove("IS_POPUP");
					viewMap.put("IS_POPUP", isPopUp);
				}
				updateValuesFromMap();
				logger.logInfo("!isPostBack completed successfully");
			}
			catch (PimsBusinessException e) 
			{
				logger.LogException("init crashed ", e);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void setRequestStatusByStautsId(Long statusId) throws PimsBusinessException
	{
		String requestStaus=REQUEST_STATUS_NEW;
			if(statusId != null )
			{	
				Long requestId=(Long) viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				if(statusId.compareTo(getRequestStatusInspector().getDomainDataId()) == 0)
				{
					requestStaus = REQUEST_STATUS_INSPECTOR_REC_REQ;
					pageMode = PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED;
					viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.INSPECTOR_RECOMMENDATION);
				}
				
				else if(statusId.compareTo(getRequestStatusHOS().getDomainDataId()) == 0)
					{
						requestStaus = REQUEST_STATUS_HOS_REC_REQ;
						loadEvaluationResults();
						pageMode = PAGE_MODE_HOS_RECOMMENDATION_REQUIRED;
						viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.HOS_RECOMMENDATION);
						
					}
				
				else if(statusId.compareTo(getRequestStatusExec().getDomainDataId()) == 0)
					{
						requestStaus = REQUEST_STATUS_EXEC_REC_REQ;
						loadEvaluationResults();
						viewMap.put("evaluationReadonlyMode",true);
						pageMode = PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED;
						viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EXECUTE_RECOMMENDATION);
						String hosRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, requestId);
						String rentCommitteeRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, requestId);
						viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, hosRecommendations);
						viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, rentCommitteeRecommendations);
					}
				
				else if(statusId.compareTo(getRequestStatusComplete().getDomainDataId()) == 0)
					{
						requestStaus = REQUEST_STATUS_COMPLETE;
						loadEvaluationResults();
						pageMode = PAGE_MODE_REQUEST_COMPLETED;
						viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_VIEW);
						String execRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION, requestId);
						String hosRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, requestId);
						String rentCommitteeRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, requestId);
						viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION, execRecommendations);
						viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, hosRecommendations);
						viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, rentCommitteeRecommendations);
					}
			}
			viewMap.put(REQUEST_STATUS, requestStaus);
			if(pageMode != null)
				viewMap.put(PAGE_MODE, pageMode);
			
		
	}
	@SuppressWarnings("unchecked")
	public void checkTask()
	{
		String methodName = "init ";
		logger.logInfo(methodName +"started.");
		UserTask userTask = null;
		Long requestId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)!=null)
				{
					requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
					if(requestId!=null)
						CommonUtil.loadAttachmentsAndComments(requestId.toString());
					requestView = new RequestService().getRequestById(requestId);
					EvaluationRequestDetailsView requestDetailsTabView = new RequestService().getEvaluationRequestById(requestId, getArgMap());	
					viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
					populateRequestDetailsTab();
					viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
					
					if(requestView != null && requestView.getRequestId() != null)
					{	
						viewMap.put(REQUEST_VIEW, requestView);
						if(requestView.getPropertyId() != null)
							viewMap.put("EVALUATION_ON", "PROPERTY");
						else if(requestView.getUnitId() != null)
							viewMap.put("EVALUATION_ON", "UNIT");
						setPageModeByRequestStatus(requestView.getStatusId(), requestView.getRequestId());
						setRequestStatusByStautsId(requestView.getStatusId());
							
					}
				}
			}
			logger.logInfo(methodName +" completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(methodName +" crashed ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	private void setPageModeByRequestMode(String requestMode) throws PimsBusinessException
	{
		String methodName = "init ";
		logger.logInfo(methodName +"started.");
		
		if(requestMode.equalsIgnoreCase(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_NEW))
			{
				pageMode = PAGE_MODE_NEW;
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_NEW);
				canAddAttachmentsAndComments(true);
			}
		
		else
		if(requestMode.equalsIgnoreCase(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_DRAFT))
			{
				pageMode = PAGE_MODE_EDIT;
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_DRAFT);
				canAddAttachmentsAndComments(true);
			}
		
		else
		if(requestMode.equalsIgnoreCase(WebConstants.EvaluationApplication.EVALUATION_REQUEST_MODE_VIEW))
			{
				pageMode = PAGE_MODE_VIEW_ONLY;
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_VIEW);
				canAddAttachmentsAndComments(false);
				viewMap.put("evaluationReadonlyMode",true);
			}
		
		if(pageMode != null)
			viewMap.put(PAGE_MODE, pageMode);
		
		logger.logInfo(methodName +"completed successfully.");
	}
	
	@SuppressWarnings("unchecked")
	private void setPageModeByRequestStatus(Long statusId, Long requestId)  throws PimsBusinessException
	{
		String methodName = "init ";
		logger.logInfo(methodName +"started.");
		
		if(statusId.compareTo(getRequestStatusInspector().getDomainDataId()) == 0)
			{
				pageMode = PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED;
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.INSPECTOR_RECOMMENDATION);
				canAddAttachmentsAndComments(true);
				
				//viewMap.put("evaluationReadonlyMode",true);
				viewMap.put("evaluationReadonlyMode",false); // as per new requirement
				if(viewMap.get("EVALUATION_ON") != null) // Added as per new requirement
				{
					String evaluationOn= viewMap.get("EVALUATION_ON").toString();
					if(evaluationOn.equalsIgnoreCase("PROPERTY"))
						viewMap.put("SHOW_EVALUATION_BY", true);
				}
				
			}
		
		if(statusId.compareTo(getRequestStatusHOS().getDomainDataId()) == 0)
			{
				pageMode = PAGE_MODE_HOS_RECOMMENDATION_REQUIRED;
				loadEvaluationResults();
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.HOS_RECOMMENDATION);
				//viewMap.put("evaluationReadonlyMode",false); // as per new requirement
				viewMap.put("evaluationReadonlyMode",true);
				canAddAttachmentsAndComments(true);
				if(viewMap.get("EVALUATION_ON") != null)
				{
					String evaluationOn= viewMap.get("EVALUATION_ON").toString();
					if(evaluationOn.equalsIgnoreCase("PROPERTY"))
						viewMap.put("SHOW_EVALUATION_BY", true);
				}
				
			}
		
		if(statusId.compareTo(getRequestStatusExec().getDomainDataId()) == 0)
			{
				pageMode = PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED;
				loadEvaluationResults();
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EXECUTE_RECOMMENDATION);
				viewMap.put("evaluationReadonlyMode",true);
				canAddAttachmentsAndComments(true);
				String hosRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, requestId);
				String rentCommitteeRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, requestId);
				viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, hosRecommendations);
				viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, rentCommitteeRecommendations);
			}
		
		if(statusId.compareTo(getRequestStatusComplete().getDomainDataId()) == 0)
			{
				pageMode = PAGE_MODE_REQUEST_COMPLETED;
				viewMap.put("evaluationReadonlyMode",true);
				viewMap.put(HEADING, WebConstants.EvaluationApplication.Headings.EVALUATION_REQUEST_MODE_VIEW);
				String execRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION, requestId);
				String hosRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, requestId);
				String rentCommitteeRecommendations = getUserComments(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, requestId);
				viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION, execRecommendations);
				viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION, hosRecommendations);
				viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION, rentCommitteeRecommendations);
			}
		
		if(pageMode != null )
			viewMap.put(PAGE_MODE, pageMode);
		
		logger.logInfo(methodName +"completed successfully.");
	}
	@SuppressWarnings("unchecked")
	private void putValuesInMaps() 
	{
		String methodName = "putDataInMaps ";
		logger.logInfo(methodName +"started.");
		
		unitTypeList=CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_TYPE);
		unitSideList  = CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_SIDE_TYPE);
		requestStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		
		requestStatusInspector= CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_INSPECTOR_RECOMMENDATION_REQUIRED);
		requestStatusHOS = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_HOS_RECOMMENDATION_REQUIRED);
		requestStatusExec = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_EXECUTIVE_RECOMMENDATION_REQUIRED);
		requestStatusComplete = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_COMPLETE);
		
		if(requestStatusComplete != null)
			viewMap.put(DD_REQUEST_STATUS_COMPLETED, requestStatusComplete);
		
		if(requestStatusInspector != null)
			viewMap.put(DD_REQUEST_STATUS_INSPECTOR_REC_REQ, requestStatusInspector);
		
		if(requestStatusHOS != null)
			viewMap.put(DD_REQUEST_STATUS_HOS_REC_REQ, requestStatusHOS);
		
		if(requestStatusExec != null)
			viewMap.put(DD_REQUEST_STATUS_EXEC_REC_REQ, requestStatusExec);
		
		if(unitTypeList!=null && unitTypeList.size()>0)
			viewMap.put(DD_LIST_UNIT_TYPE, unitTypeList);
		
		if(unitSideList!=null && unitSideList.size()>0)
			viewMap.put(DD_LIST_UNIT_SIDE, unitSideList);

		if(requestStatusList!=null && requestStatusList.size()>0)
			viewMap.put(DD_LIST_REQUEST_STATUS, requestStatusList);
		
		
		logger.logInfo(methodName +"completed successfully.");
	}
	
	@SuppressWarnings("unchecked")
	public String saveAndSendForInspector()
	{	
		String methodName = "saveAndSendForInspector ";
		logger.logInfo(methodName +"started.");
		////////////
		logger.logInfo("saveAndSendForInspector() started...");
		String success = "false";
		Boolean crashed = false;
		Boolean firstTime = false;
		try {

			if(isValidatedForNewRequest())
			{
				EvaluationRequestDetailsView evaluationRequestDetailsView = new EvaluationRequestDetailsView();
				Long requestId = (Long)viewMap.get("requestId");
				//if request is available/edit mode 
				if(requestId!=null)
				{
					evaluationRequestDetailsView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
					evaluationRequestDetailsView.setRequestId(requestId);
				}
					 
				 String evaluationPurposeId = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID);
				 evaluationRequestDetailsView.setEvaluationPurposeId(evaluationPurposeId);
				 //This propertyId is uses when evaluation has done on property(by Wholeproperty/UnitType)
				 if(viewMap.get("propertyId")!=null)
				 {
					 String propertyId = viewMap.get("propertyId").toString();
					 evaluationRequestDetailsView.setPropertyId(Long.valueOf(propertyId));
					 viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON,CommonUtil.getBundleMessage("property"));
					 
				 }
				 //If evaluation on unit
				 else if(viewMap.get("UNIT_ID") != null)
				 {
					 String unitId = viewMap.get("UNIT_ID").toString();
					 evaluationRequestDetailsView.setUnitId(Long.valueOf(unitId));
					 viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON,CommonUtil.getBundleMessage("cancelContract.tab.unit"));
				 }
				 
				 String requestDescription = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_DESCRIPTION);
				 evaluationRequestDetailsView.setRequestDescription(requestDescription);
				 
				 String applicationStatusId = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_STATUS_ID);
				 evaluationRequestDetailsView.setApplicationStatusId(applicationStatusId);
				 
				 if(evaluationRequestDetailsView.getRequestId()==null)
					 firstTime = true;
 
				 evaluationRequestDetailsView = new RequestService().saveEvaluationRequest(evaluationRequestDetailsView, getArgMap());
				 
				 viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, evaluationRequestDetailsView);
				 populateRequestDetailsTab();
				 
				 if(evaluationRequestDetailsView.getRequestId()!=null){
					requestId = evaluationRequestDetailsView.getRequestId();
					viewMap.put(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID, requestId);
					
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					
					Boolean attachSuccess = CommonUtil.saveAttachments(requestId);
					Boolean commentSuccess = CommonUtil.saveComments(requestId, noteOwner);
					
					if(attachSuccess && commentSuccess)
						success = "true";
					
					errorMessages.clear();
					
					if(firstTime){
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_CREATED);
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_INSPECTOR_RECOMM);
						infoMessage=CommonUtil.getParamBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_CREATE_SUCCESS, evaluationRequestDetailsView.getApplicationNumber());
					}
					else{
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SAVED);
						infoMessage=CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SAVE_SUCCESS);							
					}
					viewMap.put("evaluationRequestDetailsReadonlyMode", true);
					viewMap.put("asterisk", "");
					canAddAttachmentsAndComments(false);

					if(firstTime)
						{
							new RequestService().setRequestAsInspectorRecommRequired(requestId, getLoggedInUserId());
							invokeBpel(requestId);
						}
			   }
				viewMap.put(PAGE_MODE_STEP_DONE, "REQUEST_CREATED"); 
			}
			else
				success = "validation";
			logger.logInfo("saveAndSendForInspector() completed successfully!!!");
		}
		 catch(PimsBusinessException exception){
			 crashed = true;
			 logger.LogException("saveAndSendForInspector() crashed ",exception);
		 }
		catch (Exception exception) {
			crashed = true;
			logger.LogException("saveAndSendForInspector() crashed ", exception);
		}
		finally{
			if(crashed)
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SAVE_FAILURE));
		}
		return success;
	}
	
	@SuppressWarnings("unchecked")
	public String sendForHOS()
	{	
		logger.logInfo("saveEvaluation() started...");
		Long requestId=0L;
		Boolean success = false;
		try {
				if(validatedForEvaluation())
				{
					if(viewMap.containsKey(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID))			
						requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
					
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					CommonUtil.saveAttachments(requestId);
					CommonUtil.saveComments(requestId, noteOwner);
					
					evaluationView.setRequestId(requestId);
					evaluationView.setEvaluationResultsId(null);
					evaluationView.setRecommendation(getEvaluationRecomm());
					evaluationView.setEngineerName(getEvaluationEngr());
					new ConstructionService().saveEvaluationAndRequestDetails(evaluationView, getArgMap(),requestDetailViewList,requestId,false);

					StringBuilder documentTitle = new StringBuilder(evaluationView.getEngineerName());
					documentTitle = documentTitle.append(" ").append(CommonUtil.getStringFromDate(evaluationView.getEvaluationDate())).append(" ").append(viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_NUMBER).toString());
					DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), documentTitle.toString(), null);
					Boolean attachReportSuccess = documentView!=null; 
					
					
					
					
					// as per new requirement evaluation is not saved by HOS but inspector
					evaluationView = new ConstructionService().getEvaluationResults(requestId);
					
					if ( (viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
						 viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")!=0 )
						 || viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID) == null)
					{
						evaluationView.setPropertyValue(Double.parseDouble(getNewPropertyValue()));
						evaluationView.setPropertyValueStr(getNewPropertyValue().toString());
					}
					evaluationView.setEvaluationDate(getEvaluationDate());
					
					viewMap.put("NEW_PROPERTY_VALUE",getNewPropertyValue());
					
					if (viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
						viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")==0) 
						{
							requestDetailViewList = getPopulatedRequestDetailSet();
							new ConstructionService().saveEvaluationAndRequestDetails(evaluationView, getArgMap(),requestDetailViewList,requestId,true);
						}
					else {
						new ConstructionService().saveEvaluationAndRequestDetails(evaluationView, getArgMap(),requestDetailViewList,requestId,false);
					}	
					
					
					if(completeTask(TaskOutcome.OK))
					{
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_EVALUATION);
						new RequestService().setRequestAsHOSRecommRequired(requestId, getLoggedInUserId());
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SENT_TO_HOS);
						canAddAttachmentsAndComments(false);
						
					}
					viewMap.put(PAGE_MODE_STEP_DONE,INSPECTOR_DONE );
					if(!attachReportSuccess)
						errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_UPLOAD_FAILURE));
				}
			logger.logInfo("saveEvaluation() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("saveEvaluation() crashed ", exception);
			errorMessages.clear();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_EVALUATE_FAILURE));
		}
		return "saveEvaluation";	
	}
	
	@SuppressWarnings("unchecked")
	public void sendForExecutives()
	{	
		logger.logInfo("sendForExecutives started.");
		try{
	    	Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			//if(validatedForEvaluation() && isRecommendationValidated(getHosRecommendations()) && isRecommendationValidated(getRentCommRecommendations()))
	    	if(validatedForEvaluation() && validateRecommendations())
			{
				if(requestId!=null)
		    	{

					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					CommonUtil.saveAttachments(requestId);
					CommonUtil.saveComments(requestId, noteOwner);
					
					
					// as per new requirement evaluation is not saved by HOS but inspector
					/*
					
					evaluationView = new ConstructionService().getEvaluationResults(requestId);
					
					if ( (viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
						 viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")!=0 )
						 || viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID) == null)
					{
						evaluationView.setPropertyValue(Double.parseDouble(getNewPropertyValue()));
						evaluationView.setPropertyValueStr(getNewPropertyValue().toString());
					}
					evaluationView.setEvaluationDate(getEvaluationDate());
					
					viewMap.put("NEW_PROPERTY_VALUE",getNewPropertyValue());
					
					if (viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
						viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")==0) 
						{
							requestDetailViewList = getPopulatedRequestDetailSet();
							new ConstructionService().saveEvaluationAndRequestDetails(evaluationView, getArgMap(),requestDetailViewList,requestId,true);
						}
					else
						new ConstructionService().saveEvaluationAndRequestDetails(evaluationView, getArgMap(),requestDetailViewList,requestId,false);
					*/
					
		    		if(completeTask(TaskOutcome.OK))
		    		{
					    
		    			//-- CHANGE -- HOS-RENTCOMMITTE RECOMMENDATION ARE LIKE THIS WAY--
		    			//CommonUtil.saveRemarksAsComments(requestId, getHosRecommendations(), WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION) ;
					    //CommonUtil.saveRemarksAsComments(requestId, getRentCommRecommendations(), WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION) ;
		    			
		    			requestServiceAgent.addEvaluationRecomendationList(evaluationRecommendationList);
					    
					    saveSystemComments(MessageConstants.RequestEvents.EVALUATION_HOS_RECOMMENDATION_DONE);
					    new RequestService().setRequestAsExecutiveRecommRequired(requestId, getLoggedInUserId());
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage("propertyEvaluation.infoMessage.sentToExec");
						viewMap.put("evaluationReadonlyMode", true);
						viewMap.put("canAddMember", false);
						canAddAttachmentsAndComments(false);
					}
		    		viewMap.put(PAGE_MODE_STEP_DONE,HOS_DONE );
		    	}
			}
			logger.logInfo("sendForExecutives completed successfully!!!");
    	}
    	catch (Exception exception) 
    	{
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    		logger.LogException("sendForExecutives crashed ", exception);
		}
    	
    	
	}
	
	@SuppressWarnings("unchecked")
	public void saveExecutiveRecommendations()
	{	
		String methodName = "saveExecutiveRecommendations ";
		logger.logInfo(methodName +"started.");
		try{
	    	Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			//if(isRecommendationValidated(getExecRecommendations()))
			//{
				if(requestId!=null)
		    	{
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					CommonUtil.saveAttachments(requestId);
					CommonUtil.saveComments(requestId, noteOwner);
					
		    		if(completeTask(TaskOutcome.COMPLETE))
		    		{
					    // NO EXECUTIVE RECOMMENDATIONS
		    			//CommonUtil.saveRemarksAsComments(requestId, getExecRecommendations(), WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION) ;
		    			
		    			
					    saveSystemComments(MessageConstants.RequestEvents.EVALUATION_PROP_MGT_EXEC_RECOMMENDATION_DONE);
//					    This code will update the unit rent in DB automatically on completion of request
//					    new PropertyService().updatePropertyUnitRentByRequestId(requestId,getLoggedInUserId());
					    new RequestService().setRequestAsCompleted(requestId, getLoggedInUserId());
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage("propertyEvaluation.infoMessage.evaluationRecommCompletedByPropMgtExec");
						canAddAttachmentsAndComments(false);
						viewMap.put(PAGE_MODE_STEP_DONE,EXEC_DONE );
					}
		    	}
			//}
	    	
	    	logger.logInfo("saveExecutiveRecommendations completed successfully!!!");
    	}
    	catch (Exception exception) 
    	{
			logger.LogException("saveExecutiveRecommendations crashed ", exception);
		}
	}
	
	public void completeRequest()
	{	
		String methodName = "completeRequest ";
		logger.logInfo(methodName +"started.");
			try{
		    	Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				if(requestId!=null)
		    	{	
		    		if(completeTask(TaskOutcome.COMPLETE))
		    		{
					    
		    			// No recommendations for executive
		    			//CommonUtil.saveRemarksAsComments(requestId, getExecRecommendations(), WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION) ;
					    
					    saveSystemComments(MessageConstants.RequestEvents.EVALUATION_HOS_RECOMMENDATION_DONE);
					    //Mohammad told to not change the rent in DB, amaf will update manually
					    //  new PropertyService().updatePropertyUnitRentByRequestId(requestId,getLoggedInUserId());
					    new RequestService().setRequestAsCompleted(requestId, getLoggedInUserId());
						errorMessages.clear();
						infoMessage = CommonUtil.getBundleMessage("propertyEvaluation.infoMessage.evaluationRecommCompletedByHOS");
						viewMap.put("canAddMember", false);
						canAddAttachmentsAndComments(false);
					}
		    	}
		    	
		    	logger.logInfo("completeRequest completed successfully!!!");
	    	}
	    	catch (Exception exception) 
	    	{
				logger.LogException("completeRequest crashed ", exception);
			}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void prerender()
	{
		String methodName = "prerender ";
		//TODO:
		logger.logInfo(methodName +"started.");
		super.prerender();
		try
		{
			if(viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID) != null && !viewMap.containsKey(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW))
			{
				Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
				EvaluationRequestDetailsView requestDetailsTabView = new RequestService().getEvaluationRequestById(requestId, getArgMap());	
				viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
			}
			if(hiddenUnitId != null && StringHelper.isNotEmpty(hiddenUnitId))
				populateUnitDetails(Long.parseLong(hiddenUnitId));
			else
				loadProperty();
			if(viewMap.get("ERROR_MESSAGES") != null)
			{
				List<String> errList = new ArrayList<String>();
				errList = (List<String>) viewMap.get("ERROR_MESSAGES") ;
				if(errorMessages != null && errorMessages.size() > 0)
					errorMessages.clear();
				if(errList != null && errList.size() > 0)
				{
					for(String err : errList)
					{
						errorMessages.add(err);	
					}
					viewMap.remove("ERROR_MESSAGES");
				}
			}
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("prerender crashed", e);
		}
		
		logger.logInfo(methodName +"completed successfully.");
	}
	@SuppressWarnings("unchecked")
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	@SuppressWarnings("unchecked")
	public List<DomainDataView> getUnitTypeList() 
	{
		if(viewMap.get(DD_LIST_UNIT_TYPE) != null)
			unitTypeList = (List<DomainDataView>) viewMap.get(DD_LIST_UNIT_TYPE);
		return unitTypeList;
	}
	public void setUnitTypeList(List<DomainDataView> unitTypeList) {
		this.unitTypeList = unitTypeList;
	}
	@SuppressWarnings("unchecked")
	public List<DomainDataView> getUnitSideList() 
	{
		if(viewMap.get(DD_LIST_UNIT_SIDE) != null)
			unitSideList = (List<DomainDataView>) viewMap.get(DD_LIST_UNIT_SIDE);
		return unitSideList;
	}
	public void setUnitSideList(List<DomainDataView> unitSideList) {
		this.unitSideList = unitSideList;
	}
	public List<DomainDataView> getRequestStatusList() {
		return requestStatusList;
	}
	public void setRequestStatusList(List<DomainDataView> requestStatusList) {
		this.requestStatusList = requestStatusList;
	}
	public DomainDataView getRequestStatusInspector() 
	{
		if(viewMap.get(DD_REQUEST_STATUS_INSPECTOR_REC_REQ) != null)
			requestStatusInspector = (DomainDataView) viewMap.get(DD_REQUEST_STATUS_INSPECTOR_REC_REQ);
		return requestStatusInspector;
	}
	public void setRequestStatusInspector(DomainDataView requestStatusInspector) {
		
		this.requestStatusInspector = requestStatusInspector;
	}
	public DomainDataView getRequestStatusHOS() 
	{
		if(viewMap.get(DD_REQUEST_STATUS_HOS_REC_REQ) != null)
			requestStatusHOS = (DomainDataView) viewMap.get(DD_REQUEST_STATUS_HOS_REC_REQ);
		return requestStatusHOS;
	}
	public void setRequestStatusHOS(DomainDataView requestStatusHOS) {
		this.requestStatusHOS = requestStatusHOS;
	}
	public DomainDataView getRequestStatusExec()
	{
		if(viewMap.get(DD_REQUEST_STATUS_EXEC_REC_REQ) != null)
			requestStatusExec = (DomainDataView) viewMap.get(DD_REQUEST_STATUS_EXEC_REC_REQ);
		return requestStatusExec;
	}
	public void setRequestStatusExec(DomainDataView requestStatusExec) {
		this.requestStatusExec = requestStatusExec;
	}
	public DomainDataView getRequestStatusComplete() 
	{
		if(viewMap.get(DD_REQUEST_STATUS_COMPLETED) != null)
			requestStatusComplete = (DomainDataView) viewMap.get(DD_REQUEST_STATUS_COMPLETED);
		return requestStatusComplete;
	}
	public void setRequestStatusComplete(DomainDataView requestStatusComplete) {
		this.requestStatusComplete = requestStatusComplete;
	}
	public String getPageMode() {
		return pageMode;
	}
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}
	public RequestView getRequestView() {
		return requestView;
	}
	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}
	public String getProcedureTypeKey() {
		return procedureTypeKey;
	}
	public String getREQUEST_VIEW() {
		return REQUEST_VIEW;
	}
	public boolean isInspectorRecomReq() 
	{
		isInspectorRecomReq = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED));
			isInspectorRecomReq = true;
		}
		return isInspectorRecomReq;
	}
	public void setInspectorRecomReq(boolean isInspectorRecomReq) {
		this.isInspectorRecomReq = isInspectorRecomReq;
	}
	public boolean isHOSRecomReq() 
	{
		isHOSRecomReq = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_HOS_RECOMMENDATION_REQUIRED));
			isHOSRecomReq = true;
		}
		return isHOSRecomReq;
	}
	public void setHOSRecomReq(boolean isHOSRecomReq) {
		this.isHOSRecomReq = isHOSRecomReq;
	}
	public boolean isExecRecomReq() 
	{
		isExecRecomReq = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED));
			isExecRecomReq = true;
		}
		return isExecRecomReq;
	}
	public void setExecRecomReq(boolean isExecRecomReq) {
		this.isExecRecomReq = isExecRecomReq;
	}
	public boolean isCompleted() 
	{
		isCompleted = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_REQUEST_COMPLETED));
			isCompleted = true;
		}
		return isCompleted;
	}
	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getHeading()
	{
		if(viewMap.get(HEADING) != null)
		{
			heading = viewMap.get(HEADING).toString();
			if(StringHelper.isEmpty(heading))
				heading = CommonUtil.getBundleMessage(DEFAULT_HEADING);
			else
				heading = CommonUtil.getBundleMessage(heading);
		}
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	@SuppressWarnings("unchecked")
	public Boolean isValidatedForNewRequest() {
	    logger.logInfo("isValidatedForNewRequest() started...");
	    Boolean validated = true;
	    try
		{
	    	String evaluationPurposeId = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID);
	    	if(evaluationPurposeId.equals("0"))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.EvaluationApplication.Tab.EVALUATION_PURPOSE));
				return validated;
	        }
	    	String requestDescription = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_DESCRIPTION);
	    	if(StringHelper.isEmpty(requestDescription))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage("property.evaluationRequestDesc"));
				return validated;
	        }
	    	//when evaluation on Property
	    	if(viewMap.containsKey(WebConstants.PropertyEvaluation.EVALUATION_ON_ID))
	    	{	
	    		if(viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_ON_ID).toString().compareTo("1")==0)
			    {
			    	
			    	if(StringHelper.isEmpty(hiddenPropertyId))
			        {
			    		validated = false;
			        	errorMessages.clear();
			        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_SELECT_PROPERTY));
						return validated;
			        }
			    	else
			    		viewMap.put("propertyId", hiddenPropertyId);
		
			    	if(!AttachmentBean.mandatoryDocsValidated())
			    	{
			    		errorMessages.clear();
			    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
						validated = false;
						return validated;
			    	}
		
					logger.logInfo("validatedForNewRequest() completed successfully!!!");
			    }
			    else //when evaluation on Unit
			    {
			    	viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON,CommonUtil.getBundleMessage("cancelContract.tab.unit"));
			    	if(!viewMap.containsKey("UNIT_ID")|| viewMap.get("UNIT_ID")==null)
		    		{
			    		errorMessages.clear();
			    		errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.plsSelectUnit"));
			    		validated = false;
						return validated;
		    		}
			    		
			    }
	    	}
		}
		catch (Exception exception) {
			logger.LogException("validatedForNewRequest() crashed ", exception);
		}
		return validated;
	}
	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages
	 *            the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getHiddenPropertyId() {
		return hiddenPropertyId;
	}
	public void setHiddenPropertyId(String hiddenPropertyId) {
		this.hiddenPropertyId = hiddenPropertyId;
	}
	public String getHiddenLoadProperty() {
		return hiddenLoadProperty;
	}
	public void setHiddenLoadProperty(String hiddenLoadProperty) {
		this.hiddenLoadProperty = hiddenLoadProperty;
	}
	public String getHiddenUnitId() {
		return hiddenUnitId;
	}
	public void setHiddenUnitId(String hiddenUnitId) {
		this.hiddenUnitId = hiddenUnitId;
	}
	public String getHiddenLoadUnit() {
		return hiddenLoadUnit;
	}
	public void setHiddenLoadUnit(String hiddenLoadUnit) {
		this.hiddenLoadUnit = hiddenLoadUnit;
	}
	@SuppressWarnings("unchecked")
	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
		return argMap;
	}
	@SuppressWarnings("unchecked")
	private void populateRequestDetailsTab() throws PimsBusinessException
	{
		EvaluationRequestDetailsView requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
		
		 if(requestDetailsTabView!=null)
		{
			if(requestDetailsTabView.getApplicationDate()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_DATE, requestDetailsTabView.getApplicationDate());
			
			if(requestDetailsTabView.getApplicationNumber()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_NUMBER, requestDetailsTabView.getApplicationNumber());
			
			if(requestDetailsTabView.getEvaluationPurpose()!=null)
			{
				viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE, requestDetailsTabView.getEvaluationPurpose());
			    viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID,requestDetailsTabView.getEvaluationPurposeId());
			}
			
			if(requestDetailsTabView.getEvaluationPurposeId()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID, requestDetailsTabView.getEvaluationPurposeId());
			
			if(requestDetailsTabView.getApplicationStatus()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_STATUS, requestDetailsTabView.getApplicationStatus());
			
			if(requestDetailsTabView.getApplicationStatusId()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_STATUS_ID, requestDetailsTabView.getApplicationStatusId());
			
			if(requestDetailsTabView.getRequestDescription()!=null)
				viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_DESCRIPTION, requestDetailsTabView.getRequestDescription());
			
			//if evaluation on property
			if(requestDetailsTabView.getPropertyId()!=null)
			{
				//propertyDetails
				if(requestDetailsTabView.getPropertyType()!=null)
					viewMap.put("propertyType", requestDetailsTabView.getPropertyType());
				
				if(requestDetailsTabView.getPropertyName()!=null)
				{
					viewMap.put("propertyName", requestDetailsTabView.getPropertyName());
					searchFilterView.setPropertyName(requestDetailsTabView.getPropertyName());
				}
				
				if(requestDetailsTabView.getPropertyStatus()!=null)
					viewMap.put("propertyStatus", requestDetailsTabView.getPropertyStatus());
				
				if(requestDetailsTabView.getEmirate()!=null)
					viewMap.put("emirate", requestDetailsTabView.getEmirate());
				
				if(requestDetailsTabView.getLocationArea()!=null)
					viewMap.put("locationArea", requestDetailsTabView.getLocationArea());
				
				if(requestDetailsTabView.getLandNumber()!=null)
					viewMap.put("landNumber", requestDetailsTabView.getLandNumber());
				
				if(requestDetailsTabView.getRequestId()!=null)
					viewMap.put("requestId", requestDetailsTabView.getRequestId());
				
				if(requestDetailsTabView.getLandArea()!=null)
					viewMap.put("landArea", requestDetailsTabView.getLandArea());
				
				if(requestDetailsTabView.getAddress()!=null)
					viewMap.put("address", requestDetailsTabView.getAddress());
				
				if(requestDetailsTabView.getFloorCount()!=null)
					viewMap.put("floorCount", requestDetailsTabView.getFloorCount());
				
				if(requestDetailsTabView.getConstructionDate()!=null)
					viewMap.put("constructionDate", requestDetailsTabView.getConstructionDate());
					viewMap.put("propertyId", requestDetailsTabView.getPropertyId());
					
//				if last argument is null then it will search all evaluation requests for the selected property
				List<EvaluationApplicationFilterView> requestViewList = new RequestService().getEvaluationApplicationRequestsWithUnit(searchFilterView, getArgMap(),null);
				if(requestViewList!=null && requestViewList.size()>0)
					viewMap.put("REQUEST_VIEW_LIST",requestViewList);
				
			    hiddenPropertyId = requestDetailsTabView.getPropertyId().toString();
			    viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON,CommonUtil.getBundleMessage("property"));
				viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON_ID,"1");
			}  
			
			//if evaluation by Unit
			else if(requestDetailsTabView.getUnitView()!=null && requestDetailsTabView.getUnitView().getUnitId() != null)
			{
						List<EvaluationApplicationFilterView> requestViewList = new RequestService().getEvaluationApplicationRequestsWithUnit(searchFilterView, getArgMap(),requestDetailsTabView.getUnitView().getUnitId());
						if(requestViewList!=null && requestViewList.size()>0)
							viewMap.put("REQUEST_VIEW_LIST",requestViewList);
						populateUnitDetails(requestDetailsTabView.getUnitView().getUnitId());
			}
		}
	}
	public Boolean saveSystemComments(String sysNote) throws Exception
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveSystemComments started...");
	    	Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
	    	if(requestId!=null)
	    	{	
	    		NotesController.saveSystemNotesForRequest(noteOwner, sysNote, requestId);
	    		success = true;
	    	}
	    	
	    	logger.logInfo("saveSystemComments completed successfully!!!");
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments crashed ", exception);
			throw exception;
		}
    	
    	return success;
    }
	public Boolean invokeBpel(Long requestId) {
		logger.logInfo("invokeBpel() started...");
		Boolean success = true;
		try {
			PIMSPropertyEvaluationRequestBPELPortClient bpelPortClient= new PIMSPropertyEvaluationRequestBPELPortClient();
			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters.getParameter(WebConstants.EvaluationApplication.EVALUATION_REQUEST_BPEL_END_POINT);
			String userId = CommonUtil.getLoggedInUser();
			bpelPortClient.setEndpoint(endPoint);
			bpelPortClient.initiate(requestId, userId, null, null);

			logger.logInfo("invokeBpel() completed successfully!!!");
		}
		catch(PIMSWorkListException exception)
		{
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);    
		}
		catch (Exception exception) {
			success = false;
			logger.LogException("invokeBpel() crashed ", exception);
		}
		finally{
			if(!success)
			{
				errorMessages.clear();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SUBMIT_FAILURE));
			}
		}
		return success;
	}
	@SuppressWarnings("unchecked")
	private void populateUnitDetails(long id) throws PimsBusinessException
	{
			UnitView unitView = new UnitView();
		
			if(viewMap.get("UNIT_VIEW") != null && !hiddenLoadUnit.equals("true"))
				unitView = (UnitView) viewMap.get("UNIT_VIEW");
			else
			{
				unitView = new PropertyService().getPropertyUnitViewByID(id);
				viewMap.put("UNIT_VIEW",unitView);
			}
				
			if(unitView.getUnitId()!=null)
				viewMap.put("UNIT_ID",unitView.getUnitId());
			
			if(unitView.getUsageTypeId()!=null)
				viewMap.put("USAGE_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getUsageTypeEn():unitView.getUsageTypeAr());
			
			if(unitView.getStatusId()!=null)
				viewMap.put("UNIT_STATUS",CommonUtil.getIsEnglishLocale()?unitView.getStatusEn():unitView.getStatusAr());
			
			if(unitView.getUnitNumber()!=null)
				viewMap.put("UNIT_NUMBER",unitView.getUnitNumber());
			
			if(unitView.getUsageTypeId()!=null)
				viewMap.put("UNIT_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getUnitTypeEn():unitView.getUnitTypeAr());
			
			if(unitView.getInvestmentTypeId()!=null)
				viewMap.put("INVESTMENT_TYPE",CommonUtil.getIsEnglishLocale()?unitView.getInvestmentTypeEn():unitView.getInvestmentTypeAr());
			
			if(unitView.getUnitSideTypeId()!=null)
				viewMap.put("UNIT_SIDE",CommonUtil.getIsEnglishLocale()?unitView.getUnitSideTypeEn():unitView.getUnitSideTypeAr());
			
			if(unitView.getStateEn()!=null || unitView.getStateAr()!=null)
				viewMap.put("UNIT_STATE",CommonUtil.getIsEnglishLocale()?unitView.getStateEn():unitView.getStateAr());
			
			if(unitView.getPropertyCommercialName()!=null)
				viewMap.put("PROPERTY_NAME",unitView.getPropertyCommercialName());
			
			if(unitView.getPropertyNumber()!=null)
				viewMap.put("PROPERTY_NUMBER",unitView.getPropertyNumber());
			
			if(unitView.getPropertyEndowedName()!=null)
				viewMap.put("ENDOWED_NAME",unitView.getPropertyEndowedName());
			
			if(unitView.getFloorNumber()!=null)
				viewMap.put("FLOOR_NUMBER",unitView.getFloorNumber());
			
			if(unitView.getNoOfBed()!=null)
				viewMap.put("NO_OF_BEDS",unitView.getNoOfBed());
			
			if(unitView.getDewaNumber()!=null)
				viewMap.put("DEWA_ACC_NO",unitView.getDewaNumber());
			
			if(unitView.getUnitDesc()!=null)
				viewMap.put("UNIT_DESC",unitView.getUnitDesc());
			
			if(unitView.getRentValue()!=null)
				viewMap.put("UNIT_RENT_AMOUNT",unitView.getRentValue());
			
			List<EvaluationApplicationFilterView> requestViewList = new RequestService().getEvaluationApplicationRequestsWithUnit(searchFilterView, getArgMap(),unitView.getUnitId());
			if(requestViewList!=null && requestViewList.size()>0)
				viewMap.put("REQUEST_VIEW_LIST",requestViewList);
			
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON,CommonUtil.getBundleMessage("cancelContract.tab.unit"));
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON_ID,"2");
			
			if(viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID) != null)
				viewMap.remove(WebConstants.PropertyEvaluation.EVALUATION_BY_ID);
			hiddenLoadUnit = "false";
	}
	public Boolean validatedForEvaluation() {
	    logger.logInfo("validatedForEvaluation() started...");
	    Boolean validated = true;
	    try
		{
	    	if(!isPageModeHOS() && getEvaluationDate()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.EVALUATION_DATE));
				return validated;
	        }
	    	
	    	if(!isPageModeHOS() && StringHelper.isEmpty(getEvaluationEngr()))
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.EVALUATION_ENGINEER));
				return validated;
	        }

	    	//if evaluation by unit type is not there
	    	if (!isPageModeHOS() && ((viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
					viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")!=0) ||
					viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID) == null))
	    	{
	    		if(getNewPropertyValue()==null || getNewPropertyValue().trim().length() <= 0)
		        {
		    		validated = false;
		        	errorMessages.clear();
		        	errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.Evaluation.PROPERTY_VALUE));
					return validated;
		        }
		    	else
		    	{
		    		try 
		    		{
						if(Double.parseDouble(getNewPropertyValue())<= 0.0D)
						{
							validated = false;
				        	errorMessages.clear();
				        	errorMessages.add(CommonUtil.getBundleMessage("unitDetails.msgs.provideCorrectRentVal"));
							return validated;
						}
					}
		    		catch (Exception e) 
					{
		    			validated = false;
			        	errorMessages.clear();
			        	errorMessages.add(CommonUtil.getBundleMessage("unitDetails.msgs.provideCorrectRentVal"));
						return validated;
					}
		    	}
	    	}
	    	if (!isPageModeHOS() && viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID)!=null && 
					viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString().compareTo("2")==0 )
	    	{
	    		
	    		if(viewMap.get("EVALUATION_UNIT_TYPE_LIST") != null)
	    		{
	    			List<EvaluationByUnitTypeView> evaUnits = new ArrayList<EvaluationByUnitTypeView>();
	    			evaUnits = (List<EvaluationByUnitTypeView>) viewMap.get("EVALUATION_UNIT_TYPE_LIST");
	    			if(evaUnits == null || evaUnits.size() <= 0)
	    			{
	    				validated = false;
	    	        	errorMessages.clear();
	    	        	errorMessages.add(CommonUtil.getBundleMessage("propertyEvaluation.errMsg.unitListRequired"));
	    				return validated;
	    			}
	    		}
	    		else
	    		{
	    			validated = false;
    	        	errorMessages.clear();
    	        	errorMessages.add("Units Required");
    				return validated;
	    		}
	    	}
	    	
	    	if(!isPageModeHOS() && getEvaluationRecomm() == null || getEvaluationRecomm().trim().length()<= 0)
	    	{
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getFieldRequiredMessage("evaluation.recommendation"));
				return validated;
	    	}
	    	if(!isPageModeHOS() && getEvaluationRecomm()!=null && getEvaluationRecomm().length()>2000 ){
	    		validated = false;
	    		errorMessages.clear();
	    		errorMessages.add( CommonUtil.getParamBundleMessage("commons.lengthTooLong", CommonUtil.getParamBundleMessage("evaluationApplication.label.inspectorRecomm"), "2000" ) );
				return validated;
	    	}
	    	
	    	
//	    	if(StringHelper.isNotEmpty(evaluationView.getPropertyValueStr()))
//	        {
//	    		try{
//		    		String doubleStr = evaluationView.getPropertyValueStr();
//		    		for(int index=0; index<doubleStr.length(); index++){
//		    			char digit = doubleStr.charAt(index);
//		    			if(!((digit>='0' && digit<='9') || (digit==',') || (digit=='.')))
//		    				throw new NumberFormatException();
//		    		}
//		    		Double propertyValue = CommonUtil.getDoubleValue(doubleStr);
//		    		evaluationView.setPropertyValue(propertyValue);
//	    		}
//	    		catch(NumberFormatException nfe){
//		    		validated = false;
//		        	errorMessages.clear();
//		        	errorMessages.add(CommonUtil.getInvalidMessage(WebConstants.Evaluation.PROPERTY_VALUE));
//					return validated;
//	    		}
//	        }
	    	
	    	if(!isPageModeHOS() && fileUploadCtrl.getUploadedFile()==null)
	        {
	    		validated = false;
	        	errorMessages.clear();
	        	errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_NO_FILE_SELECTED));
				return validated;
	        }
	    	
			logger.logInfo("validatedForEvaluation() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validatedForEvaluation() crashed ", exception);
		}
		return validated;
	}
	public String getNewPropertyValue()
	{
		if(viewMap.containsKey("NEW_PROPERTY_VALUE"))
			newPropertyValue= viewMap.get("NEW_PROPERTY_VALUE").toString();
		return newPropertyValue;
	}

	@SuppressWarnings("unchecked")
	public void setNewPropertyValue(String newPropertyValue) 
	{
		if(newPropertyValue != null && newPropertyValue.trim().length() > 0)
			viewMap.put("NEW_PROPERTY_VALUE", newPropertyValue);
		this.newPropertyValue = newPropertyValue;
	}
	public String getEvaluationRecomm()
	{
		if(evaluationView.getRecommendation()!=null && evaluationView.getRecommendation().length()>0)
			evaluationRecomm=evaluationView.getRecommendation();
		else if(viewMap.containsKey("EVALUATION_RECOMMENDATION"))
			evaluationRecomm=viewMap.get("EVALUATION_RECOMMENDATION").toString();
		return evaluationRecomm;
	}
	public String getEvaluationEngr()
	{
		if(evaluationView != null && evaluationView.getEngineerName()!=null && evaluationView.getEngineerName().length()>0)
			evaluationEngr=evaluationView.getEngineerName();
		else if(viewMap.containsKey("ENGR_NAME"))
			evaluationEngr=viewMap.get("ENGR_NAME").toString();
		return evaluationEngr;
	}
	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception 
	{
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = CommonUtil.getLoggedInUser();
		        if(viewMap.containsKey(TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, taskOutcome);
//					saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
				}
			logger.logInfo("completeTask() completed successfully!!!");
		return success;
	}
	private List<RequestDetailView> getPopulatedRequestDetailSet() 
	{
		List<RequestDetailView> rdl=new ArrayList<RequestDetailView>(0);
		evaluationUnitList=getEvaluationUnitList();
		
		for(EvaluationByUnitTypeView euv:evaluationUnitList)
		{
			RequestDetailView rdv=new RequestDetailView();
			rdv.setRecordStatus(1L);
			rdv.setCreatedBy(CommonUtil.getLoggedInUser());
			rdv.setUpdatedBy(CommonUtil.getLoggedInUser());
			rdv.setCreatedOn(new Date());
			rdv.setUpdatedOn(new Date());
			rdv.setIsDeleted(0L);
			rdv.setUnitTypeAmount(euv.getNewRentValue());
			
			if(euv.getUnitTypeId() != null )
			{
				rdv.setUnitTypeId(euv.getUnitTypeId());
				
				if(euv.getNewRentValue() != null)
					rdv.setUnitTypeAmount(euv.getNewRentValue());
				
				if(euv.getUnitSide() != null)
					rdv.setUnitSideId(euv.getUnitSideId());
				
				
				if(euv.getAreaFrom() != null && StringHelper.isNotEmpty(euv.getAreaFrom()) && euv.getAreaFrom().trim().length() > 0)
					rdv.setAreaFrom(Double.parseDouble(euv.getAreaFrom()));
				
				if(euv.getAreaTo() != null && StringHelper.isNotEmpty(euv.getAreaTo()) && euv.getAreaTo().trim().length() > 0)
					rdv.setAreaTo(Double.parseDouble(euv.getAreaTo()));
				
//				rdv.setUnitTypeRentDetailCSV(getUnitTypeDetailsCSV(euv));
			}
			rdl.add(rdv);
		}
		
		return rdl;
	}
	public Date getEvaluationDate() 
	{
		if(evaluationView != null && evaluationView.getEvaluationDate()!=null)
			evaluationDate=evaluationView.getEvaluationDate();
		else if(viewMap.containsKey("EVALUATION_DATE"))
			evaluationDate=(Date) viewMap.get("EVALUATION_DATE");
		return evaluationDate;
	}
	private String getUnitTypeDetailsCSV(EvaluationByUnitTypeView euv) 
	{
		String csvDetail ="";
		//csvDetail = unitTypeId,unitTypeRent,unitSideId,AreaFrom,AreaTo
//		1
		if(euv.getUnitTypeId()!= null)
			csvDetail += euv.getUnitTypeId() + ",";
//		2
		if(euv.getNewRentValue() != null)
			csvDetail += euv.getNewRentValue().toString() + ",";
//		3
		if(euv.getUnitSideId() != null)
			csvDetail += euv.getUnitSideId() + ",";
		else
			csvDetail += "-1,";
//		4
		if(euv.getAreaFrom() != null && euv.getAreaFrom().trim().length() > 0)
			csvDetail += euv.getAreaFrom() + ",";
		else
			csvDetail += "-1,";
//		5
		if(euv.getAreaTo() != null && euv.getAreaTo().trim().length() > 0)
			csvDetail += euv.getAreaTo();
		else
			csvDetail += "-1";
		
		return csvDetail;
	}
	@SuppressWarnings("unchecked")
	public List<EvaluationByUnitTypeView> getEvaluationUnitList()
	{
		if(viewMap.containsKey(EVALUATION_UNIT_TYPE_LIST) && viewMap.get(EVALUATION_UNIT_TYPE_LIST)!=null)
			evaluationUnitList=(List<EvaluationByUnitTypeView>) viewMap.get(EVALUATION_UNIT_TYPE_LIST);
		return evaluationUnitList;
	}
/*	@SuppressWarnings("unchecked")
	public String getRentCommRecommendations() 
	{
		if(viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION) != null)
			rentCommRecommendations = viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION).toString();
		return rentCommRecommendations;
	}

	public void setRentCommRecommendations(String rentCommRecommendations) 
	{
		if(rentCommRecommendations != null && StringHelper.isNotEmpty(rentCommRecommendations))
			viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_RENT_COMMITTE_RECOMMENDATION,rentCommRecommendations);
		this.rentCommRecommendations = rentCommRecommendations;
	}*/

/*	
	@SuppressWarnings("unchecked")
	public String getHosRecommendations() 
	{
		if(viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION) != null)
			hosRecommendations = viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION).toString();
		return hosRecommendations;
	}
	
*/
	
/*
	@SuppressWarnings("unchecked")
	public void setHosRecommendations(String hosRecommendations) 
	{
		if(hosRecommendations != null && StringHelper.isNotEmpty(hosRecommendations))
			viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_HOS_EVALUATION_RECOMMENDATION,hosRecommendations);
		this.hosRecommendations = hosRecommendations;
	}
	
*/
/*	public String getExecRecommendations() 
	{
		if(viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION) != null)
			execRecommendations = viewMap.get(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION).toString();
		return execRecommendations;
	}
*/	
	private boolean isRecommendationValidated(String recommendation) 
	{
		boolean validated = false;
		if(recommendation == null || recommendation.trim().length() <= 0)
		{
			validated = false;
			errorMessages.add(CommonUtil.getBundleMessage("payments.errMsg.provideMandatoryField"));
		}
		else
			if(recommendation != null && recommendation.trim().length() <= 500)
				validated = true;
		else
		{
			validated = false;
			errorMessages.add(CommonUtil.getBundleMessage("evaluationApplication.errMsg.recommendationLengthTooLong"));
		}
		return validated;
	}	
	@SuppressWarnings("unchecked")
	private void loadEvaluationResults() throws PimsBusinessException
	{
		logger.logInfo("loadEvaluationResults() started...");
	
		Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
		if(requestId!=null){
			if(viewMap.get("REQUEST_VIEW") != null)
				requestView = (RequestView) viewMap.get("REQUEST_VIEW");
			
			if(requestView != null && requestView.getPropertyId() != null && 
			   (requestView.getStatusId().compareTo(getRequestStatusHOS().getDomainDataId()) == 0 ||
				requestView.getStatusId().compareTo(getRequestStatusExec().getDomainDataId()) == 0 ||
			    requestView.getStatusId().compareTo(getRequestStatusComplete().getDomainDataId()) == 0))
			{
				viewMap.put("SHOW_EVALUATION_BY", true);
			}
			else
			{
				viewMap.put("SHOW_EVALUATION_BY", false);
			}
			evaluationView = new ConstructionService().getEvaluationResults(requestId);
			if((requestView.getPropertyId() != null ||requestView.getUnitId() != null)&& evaluationView.getPropertyValue()!=null)
				viewMap.put("NEW_PROPERTY_VALUE", evaluationView.getPropertyValue());
			if(requestView.getPropertyId() != null && 
					(requestView.getStatusId().compareTo(getRequestStatusExec().getDomainDataId()) == 0 ||
					 requestView.getStatusId().compareTo(getRequestStatusHOS().getDomainDataId()) == 0 ||
					 requestView.getStatusId().compareTo(getRequestStatusComplete().getDomainDataId()) == 0 )&& 
					evaluationView.getPropertyValue() == null)
				{
					
					viewMap.put("HIDE_PROPERTY_RENT_VALUE",true);
					viewMap.put("SHOW_EVALUATION_BY",true);
					viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_BY_ID,"2");
					loadEvaluationUnitsDetails(requestView.getRequestId());
				}
			if(evaluationView.getEvaluationDate()!=null)
				viewMap.put("EVALUATION_DATE", evaluationView.getEvaluationDate());
			if(evaluationView.getRecommendation()!=null)
				viewMap.put("EVALUATION_RECOMMENDATION", evaluationView.getRecommendation());
			if(evaluationView.getEngineerName()!=null)
				viewMap.put("ENGR_NAME", evaluationView.getEngineerName());
		
			if(viewMap.containsKey("EVALUATION_DATE"))
				evaluationView.setEvaluationDate((Date) viewMap.get("EVALUATION_DATE"));
			
			if(viewMap.containsKey("NEW_PROPERTY_VALUE"))
				evaluationView.setPropertyValueStr(viewMap.get("NEW_PROPERTY_VALUE").toString());
			else
				evaluationView.setPropertyValueStr(CommonUtil.getFormattedDoubleString(evaluationView.getPropertyValue(),CommonUtil.getDoubleFormat()));
			
			viewMap.put("EVALUATION_VIEW", evaluationView);
			
		}
		viewMap.put(WebConstants.EvaluationApplication.EVALUATION_RESULT, evaluationView);
	
		logger.logInfo("loadEvaluationResults() completed successfully!!!");
	}
	@SuppressWarnings("unchecked")
	private void loadEvaluationUnitsDetails(long requestId) 
	{
		List<RequestDetailView> requestDtlList = new ArrayList<RequestDetailView>();
		
		try 
		{
			requestDtlList = new RequestService().getRequestDetailByRequestId(requestId);
			List<EvaluationByUnitTypeView> evaUnitList = new ArrayList<EvaluationByUnitTypeView>();
			if(requestDtlList != null && requestDtlList.size() > 0)
			{
				for(RequestDetailView rqstDtl : requestDtlList)
				{
					evaUnitList.add(getEvaluationUnitDetailFromRqstDetails(rqstDtl));
				}
				viewMap.put(EVALUATION_UNIT_TYPE_LIST, evaUnitList);
			}
		} 
		catch (PimsBusinessException e) 
		{
			logger.LogException("loadEvaluationUnitsDetails crashed | ",e);
			e.printStackTrace();
		}
	}
	private EvaluationByUnitTypeView getEvaluationUnitDetailFromRqstDetails(RequestDetailView rdv) 
	{
		EvaluationByUnitTypeView evaUnit = new EvaluationByUnitTypeView();
		
		if(rdv.getUnitTypeId() != null)
			{
				evaUnit.setUnitTypeId(rdv.getUnitTypeId());
				evaUnit.setUnitType(getUnitTypeDescriptionById(rdv.getUnitTypeId()));
			}
		
		if(rdv.getUnitTypeAmount() != null)
			evaUnit.setNewRentValue(rdv.getUnitTypeAmount());
			
		if(rdv.getUnitSideId() != null)
			{
				evaUnit.setUnitSideId(rdv.getUnitSideId());
				evaUnit.setUnitSide(getUnitSideDescriptionById(rdv.getUnitSideId()));
			}
			
		if(rdv.getAreaFrom() != null )
			evaUnit.setAreaFrom(rdv.getAreaFrom().toString());
			
		if(rdv.getAreaTo() != null )
			evaUnit.setAreaTo(rdv.getAreaTo().toString());
			
		return evaUnit;
	}
	private String getUnitTypeDescriptionById(long id)
	{
		String description="";
		if(viewMap.get(DD_LIST_UNIT_TYPE) != null)
		{
			unitTypeList = (List<DomainDataView>) viewMap.get(DD_LIST_UNIT_TYPE);
			for(DomainDataView dd : unitTypeList)
			{
				if(dd.getDomainDataId().compareTo(id) == 0)
				{
					description = CommonUtil.getIsEnglishLocale()?dd.getDataDescEn():dd.getDataDescAr() ;
					break;
				}
			}
			
		}
		return description;
	}
	@SuppressWarnings("unchecked")
	private String getUnitSideDescriptionById(long id)
	{
		String description="";
		if(viewMap.get(DD_LIST_UNIT_SIDE) != null)
		{
			unitSideList = (List<DomainDataView>) viewMap.get(DD_LIST_UNIT_SIDE);
			for(DomainDataView dd : unitSideList)
			{
				if(dd.getDomainDataId().compareTo(id) == 0)
				{
					description = CommonUtil.getIsEnglishLocale()?dd.getDataDescEn():dd.getDataDescAr() ;
					break;
				}
			}
		}
		return description;
	}
	public String getUserComments(String owner, Long entityId)
	{
		List<NotesVO> notes = new ArrayList<NotesVO>();
		String comments = "";
		try 
		{
			notes = new NotesServiceAgent().getComments(owner, entityId);
		}
		catch (Exception e) 
		{
			logger.LogException("getUserComments() crashed | " ,e);
		}
		if(notes != null && notes.size() > 0)
			comments = notes.get(0).getNote();
		
		return comments;
	}
	public HtmlInputText getRentValueTxt() {
		return rentValueTxt;
	}
	public void setRentValueTxt(HtmlInputText rentValueTxt) {
		this.rentValueTxt = rentValueTxt;
	}
	public boolean isShowProperty()
	{
		isShowProperty = true;
		if(viewMap.containsKey(WebConstants.PropertyEvaluation.EVALUATION_ON_ID))
		{
			//if evaluation on property
			if(viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_ON_ID).toString().compareTo("1")==0)
				{
					if(viewMap.get("UNIT_ID") != null)
						viewMap.remove("UNIT_ID");
					isShowProperty=true;
				}
			else //if evaluation on unit
				{
				if(viewMap.get("propertyId") != null)
					viewMap.remove("propertyId");
				isShowProperty=false;
				}
		}
		return isShowProperty;
	}
	public void setShowProperty(boolean isShowProperty) {
		this.isShowProperty = isShowProperty;
	}
	public boolean isPopUp() 
	{
		if(viewMap.containsKey("IS_POPUP"))
			isPopUp=(Boolean)viewMap.get("IS_POPUP");
		return isPopUp;
	}
	public void setPopUp(boolean isPopUp) {
		this.isPopUp = isPopUp;
	}
	public void requestHistoryTabClick()
	{
		logger.logInfo("requestHistoryTabClick started...");
		try	
		{
			RequestHistoryController rhc=new RequestHistoryController();
			Long requestId = (Long)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_ID);
			if(requestId!=null && requestId!=0L)
				rhc.getAllRequestTasksForRequest(noteOwner, requestId.toString());
			
			logger.logInfo("requestHistoryTabClick completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("requestHistoryTabClick crashed...",ex);
		}
	}
	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}
	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getNumberFormat(){
		return new CommonUtil().getNumberFormat();
    }
	@SuppressWarnings("static-access")
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public UploadedFile getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(UploadedFile selectedFile) {
		this.selectedFile = selectedFile;
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public boolean isPageModeView()
	{
		isPageModeView = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_VIEW_ONLY))
				isPageModeView = true;
		}
		return isPageModeView;
	}
	public void setPageModeView(boolean isPageModeView) {
		this.isPageModeView = isPageModeView;
	}
	public boolean isPageModeInspector() 
	{
		isPageModeInspector = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_INSPECTOR_RECOMMENDATION_REQUIRED))
				isPageModeInspector = true;
		}
		return isPageModeInspector;
	}
	public void setPageModeInspector(boolean isPageModeInspector) {
		this.isPageModeInspector = isPageModeInspector;
	}
	public boolean isPageModeHOS() 
	{
		isPageModeHOS = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_HOS_RECOMMENDATION_REQUIRED))
				isPageModeHOS = true;
		}
		return isPageModeHOS;
	}
	public void setPageModeHOS(boolean isPageModeHOS) {
		this.isPageModeHOS = isPageModeHOS;
	}
	public boolean isPageModeExec() 
	{
		isPageModeExec = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_EXECUTIVE_RECOMMENDATION_REQUIRED))
				isPageModeExec = true;
		}
		return isPageModeExec;
	}
	public void setPageModeExec(boolean isPageModeExec) {
		this.isPageModeExec = isPageModeExec;
	}
	public boolean isPageModeCompleted()
	{
		isPageModeCompleted = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_REQUEST_COMPLETED))
				isPageModeCompleted = true;
		}
		return isPageModeCompleted;
	}
	public void setPageModeCompleted(boolean isPageModeCompleted) {
		this.isPageModeCompleted = isPageModeCompleted;
	}
	public boolean isStatusNew() 
	{
		isStatusNew = false;
		if(viewMap.get(REQUEST_STATUS) != null)
		{
			String requestStatus = viewMap.get(REQUEST_STATUS).toString();
			if(requestStatus.equalsIgnoreCase(REQUEST_STATUS_NEW))
				isStatusNew = true;
		}
		return isStatusNew;
	}
	public void setStatusNew(boolean isRequestStatusNew) {
		this.isStatusNew = isRequestStatusNew;
	}
	public boolean isStatusInspector() 
	{
		isStatusInspector = false;
		if(viewMap.get(REQUEST_STATUS) != null)
		{
			String requestStatus = viewMap.get(REQUEST_STATUS).toString();
			if(requestStatus.equalsIgnoreCase(REQUEST_STATUS_INSPECTOR_REC_REQ))
				isStatusInspector = true;
		}
		return isStatusInspector;
	}
	public void setStatusInspector(boolean isRequestStatusInspector) {
		this.isStatusInspector = isRequestStatusInspector;
	}
	public boolean isStatusHOS()
	{
		isStatusHOS = false;
		if(viewMap.get(REQUEST_STATUS) != null)
		{
			String requestStatus = viewMap.get(REQUEST_STATUS).toString();
			if(requestStatus.equalsIgnoreCase(REQUEST_STATUS_HOS_REC_REQ))
				isStatusHOS = true;
		}
		return isStatusHOS;
	}
	public void setStatusHOS(boolean isRequestStatusHOS) {
		this.isStatusHOS = isRequestStatusHOS;
	}
	public boolean isStatusExec() 
	{
		isStatusExec = false;
		if(viewMap.get(REQUEST_STATUS) != null)
		{
			String requestStatus = viewMap.get(REQUEST_STATUS).toString();
			if(requestStatus.equalsIgnoreCase(REQUEST_STATUS_EXEC_REC_REQ))
				isStatusExec = true;
		}
		return isStatusExec;
	}
	public void setStatusExec(boolean isRequestStatusExec) {
		this.isStatusExec = isRequestStatusExec;
	}
	public boolean isStatusCompleted()
	{
		isStatusCompleted = false;
		if(viewMap.get(REQUEST_STATUS) != null)
		{
			String requestStatus = viewMap.get(REQUEST_STATUS).toString();
			if(requestStatus.equalsIgnoreCase(REQUEST_STATUS_COMPLETE))
				isStatusCompleted = true;
		}
		return isStatusCompleted;
	}
	public void setStatusCompleted(boolean isRequestStatusCompleted) {
		this.isStatusCompleted = isRequestStatusCompleted;
	}
	@SuppressWarnings("unchecked")
	private void loadProperty(){
		try{
			logger.logInfo("loadProperty started...");
			Boolean propertyLoaded = (Boolean) viewMap.get("propertyLoaded");
			propertyLoaded = (propertyLoaded==null)?false:propertyLoaded;
			Boolean loadProperty = hiddenLoadProperty.equals("true");
			if(StringHelper.isNotEmpty(hiddenPropertyId) && (!propertyLoaded || loadProperty)){
				viewMap.put("propertyId", hiddenPropertyId);
				EvaluationRequestDetailsView propertyView = new RequestService().getEvaluationApplicationPropertyById(Long.parseLong(hiddenPropertyId), getArgMap());
				if(propertyView.getPropertyId()!=null)
					viewMap.put("propertyId", propertyView.getPropertyId());
				if(propertyView.getApplicationStatus()!=null)
					viewMap.put("applicationStatus", propertyView.getApplicationStatus());
				if(propertyView.getPropertyType()!=null)
					viewMap.put("propertyType", propertyView.getPropertyType());
				if(propertyView.getPropertyStatus()!=null)
					viewMap.put("propertyStatus", propertyView.getPropertyStatus());
				if(propertyView.getPropertyName()!=null)
					{
						viewMap.put("propertyName", propertyView.getPropertyName());
						searchFilterView.setPropertyName(propertyView.getPropertyName());
					}
				if(propertyView.getEmirate()!=null)
					viewMap.put("emirate", propertyView.getEmirate());
				if(propertyView.getLocationArea()!=null)
					viewMap.put("locationArea", propertyView.getLocationArea());
				if(propertyView.getLandNumber()!=null)
					{
						viewMap.put("landNumber", propertyView.getLandNumber());
						searchFilterView.setLandNumber(propertyView.getLandNumber());
					}
				if(propertyView.getRequestId()!=null)
					viewMap.put("requestId", propertyView.getRequestId());
				if(propertyView.getLandArea()!=null)
					viewMap.put("landArea", propertyView.getLandArea());
				if(propertyView.getAddress()!=null)
					viewMap.put("address", propertyView.getAddress());
				if(propertyView.getFloorCount()!=null)
					viewMap.put("floorCount", propertyView.getFloorCount());
				if(propertyView.getConstructionDate()!=null)
					viewMap.put("constructionDate", propertyView.getConstructionDate());
				if(viewMap.containsKey(WebConstants.PropertyEvaluation.EVALUATION_BY_ID))
				{
					String id;
					id=viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_BY_ID).toString();
					if(id.compareTo("2")==0)
						viewMap.put("SHOW_UNIT_TYPE_DETAILS", true);
					else
						viewMap.put("SHOW_UNIT_TYPE_DETAILS", false);
				}
				searchFilterView.setPropertyId(propertyView.getPropertyId().toString());
				//if last argument is null then it will search the evluation request on property else unit
				List<EvaluationApplicationFilterView> requestViewList = new RequestService().getEvaluationApplicationRequestsWithUnit(searchFilterView, getArgMap(),null);
				if(requestViewList!=null && requestViewList.size()>0)
					viewMap.put("REQUEST_VIEW_LIST",requestViewList);
				
				viewMap.put("propertyLoaded", true);
				hiddenLoadProperty = "false";
			}
			logger.logInfo("loadProperty completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("loadProperty crashed...", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public boolean isPageModeNew() 
	{
		isPageModeNew = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_NEW))
				isPageModeNew = true;
		}
		return isPageModeNew;
	}
	public void setPageModeNew(boolean isPageModeNew) {
		this.isPageModeNew = isPageModeNew;
	}
	@SuppressWarnings("unchecked")
	public void setEvaluationEngr(String evaluationEngr) 
	{
		if(evaluationEngr != null && StringHelper.isNotEmpty(evaluationEngr))
			viewMap.put("ENGR_NAME", evaluationEngr);
		this.evaluationEngr = evaluationEngr;
	}
	public EvaluationApplicationFilterView getSearchFilterView() {
		return searchFilterView;
	}
	public void setSearchFilterView(EvaluationApplicationFilterView searchFilterView) {
		this.searchFilterView = searchFilterView;
	}
	public EvaluationView getEvaluationView() {
		return evaluationView;
	}
	public void setEvaluationView(EvaluationView evaluationView) {
		this.evaluationView = evaluationView;
	}
	public List<RequestDetailView> getRequestDetailViewList() {
		return requestDetailViewList;
	}
	public void setRequestDetailViewList(
			List<RequestDetailView> requestDetailViewList) {
		this.requestDetailViewList = requestDetailViewList;
	}
	@SuppressWarnings("unchecked")
	public void setEvaluationDate(Date evaluationDate) 
	{
		if(evaluationDate != null)
		viewMap.put("EVALUATION_DATE", evaluationDate);
		this.evaluationDate = evaluationDate;
	}
	@SuppressWarnings("unchecked")
	public void setEvaluationRecomm(String evaluationRecomm) 
	{
		if(evaluationRecomm != null && StringHelper.isNotEmpty(evaluationRecomm))
			viewMap.put("EVALUATION_RECOMMENDATION",evaluationRecomm);
		this.evaluationRecomm = evaluationRecomm;
	}
	public void setEvaluationUnitList(
			List<EvaluationByUnitTypeView> evaluationUnitList) {
		this.evaluationUnitList = evaluationUnitList;
	}

/*	
	@SuppressWarnings("unchecked")
	public void setExecRecommendations(String execRecommendations) 
	{
		if(execRecommendations != null && StringHelper.isNotEmpty(execRecommendations))
			viewMap.put(WebConstants.EvaluationApplication.NOTES_OWNER_EXECUTIVE_RECOMMENDATION,execRecommendations);
		this.execRecommendations = execRecommendations;
	}
	
*/	
	public boolean isPageModeStepDone() 
	{
		isPageModeStepDone = false;
		if(viewMap.get(PAGE_MODE) != null)
		{
			pageMode = viewMap.get(PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(PAGE_MODE_STEP_DONE))
				isPageModeStepDone = true;
		}
		return isPageModeStepDone;
	}
	public void setPageModeStepDone(boolean isPageModeStepDone) 
	{
		
		this.isPageModeStepDone = isPageModeStepDone;
	}
	public boolean isHidePropertyRent() 
	{
		isHidePropertyRent = false;
		if(viewMap.get("HIDE_PROPERTY_RENT_VALUE") != null)
		{
			isHidePropertyRent = (Boolean)viewMap.get("HIDE_PROPERTY_RENT_VALUE"); 
		}
		return isHidePropertyRent;
	}
	public void setHidePropertyRent(boolean isHidePropertyRent) {
		this.isHidePropertyRent = isHidePropertyRent;
	}
	public boolean isInspectorDone() 
	{
		isInspectorDone = false;
		if(viewMap.get(PAGE_MODE_STEP_DONE) != null )
		{
			String step = viewMap.get(PAGE_MODE_STEP_DONE).toString();
			if(step.equalsIgnoreCase(INSPECTOR_DONE))
				isInspectorDone = true;
		}
		return isInspectorDone;
	}
	public void setInspectorDone(boolean isInspectorDone) {
		this.isInspectorDone = isInspectorDone;
	}
	public boolean isHOSDone() 
	{
		isHOSDone = false;
		if(viewMap.get(PAGE_MODE_STEP_DONE) != null )
		{
			String step = viewMap.get(PAGE_MODE_STEP_DONE).toString();
			if(step.equalsIgnoreCase(HOS_DONE))
				isHOSDone = true;
		}
		return isHOSDone;
	}
	public void setHOSDone(boolean isHOSDone) {
		this.isHOSDone = isHOSDone;
	}
	public boolean isExecDone() 
	{
		isExecDone = false;
		if(viewMap.get(PAGE_MODE_STEP_DONE) != null )
		{
			String step = viewMap.get(PAGE_MODE_STEP_DONE).toString();
			if(step.equalsIgnoreCase(EXEC_DONE))
				isExecDone = true;
		}
		return isExecDone;
	}
	public void setExecDone(boolean isExecDone) {
		this.isExecDone = isExecDone;
	}
	public boolean isRequestCreated() 
	{
		isRequestCreated = false;
		if(viewMap.get(PAGE_MODE_STEP_DONE) != null )
		{
			String step = viewMap.get(PAGE_MODE_STEP_DONE).toString();
			if(step.equalsIgnoreCase("REQUEST_CREATED"))
				isRequestCreated = true;
		}
		return isRequestCreated;
	}
	public void setRequestCreated(boolean isRequestCreated) {
		this.isRequestCreated = isRequestCreated;
	}
	
	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(StringHelper.isEmpty(backScreen))
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return "evaluationApplicationSearch";
    }
	
	public boolean isPropertyUnitEvaluationReportViewable(){
		
		boolean isPropertyUnitEvaluationReportViewable = false;
		
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			getRequestDetailViewList();
			DomainDataView ddCompletedRequestStaus = new DomainDataView();
			if(viewMap.get(DD_REQUEST_STATUS_COMPLETED) != null)
			{	ddCompletedRequestStaus = (DomainDataView) viewMap.get(DD_REQUEST_STATUS_COMPLETED);
				if (viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_VIEW) != null) 
				{	
					requestView=(RequestView)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_VIEW);	
					if (requestView != null && (requestView.getStatusId().equals(ddCompletedRequestStaus.getDomainDataId()))) 
						isPropertyUnitEvaluationReportViewable=true;
				}
			}
			if(isExecDone())
				isPropertyUnitEvaluationReportViewable=true;
		
		} catch (Exception e) {
			logger.LogException("Error isPropertyUnitEvaluationReportViewable()", e);
		}
		
		return isPropertyUnitEvaluationReportViewable;
	}
	
	@SuppressWarnings( "unchecked" )
	public String viewPropertyUnitEvaluationReport() 
	{
		
		try {
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	
			if(viewMap.get(REQUEST_VIEW) != null)
				requestView = (RequestView) viewMap.get(REQUEST_VIEW);
			else
				if(viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_VIEW) != null)
				requestView=(RequestView)viewMap.get(WebConstants.EvaluationApplication.EVALUATION_REQUEST_VIEW);	
			
			if (requestView != null && requestView.getPropertyId()!=null) 
			{
				
				PropertyEvaluationCriteria propertyReportCriteria = null;
				propertyReportCriteria = new PropertyEvaluationCriteria(ReportConstant.Report.PROPERTY_EVALUATION_REPORT_AR, ReportConstant.Processor.PROPERTY_EVALUATION_REPORT, CommonUtil.getLoggedInUser());
				propertyReportCriteria.setRequestId(requestView.getRequestId());
				
				ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
				EvaluationView evaluationView = constructionServiceAgent.getEvaluationResults(requestView.getRequestId());
				
				if (evaluationView.getPropertyValue()!=null) { // whole property
					propertyReportCriteria.setWholeProperty(true);
				} else { // by unitType
					propertyReportCriteria.setWholeProperty(false);
				}
				request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, propertyReportCriteria);
			}
			else if (requestView != null && requestView.getUnitId()!=null) {
				UnitEvaluationCriteria unitReportCriteria = null;
				unitReportCriteria = new UnitEvaluationCriteria(ReportConstant.Report.UNIT_EVALUATION_REPORT_AR, ReportConstant.Processor.UNIT_EVALUATION_REPORT, CommonUtil.getLoggedInUser());
				unitReportCriteria.setRequestId(requestView.getRequestId());
				request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, unitReportCriteria);
			}
			
			
			
		} catch (Exception e) {
			logger.LogException("Error viewPropertyUnitEvaluationReport()", e);
		}
		
    	
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	private boolean validateEvaluationRecommendation(){
		boolean valid = true;
		
		if (evaluationRecommendationView.getStrTypeId()==null || "".equalsIgnoreCase(evaluationRecommendationView.getStrTypeId())) {
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.Evaluation.Type") ) );
		}
		else {
			try {
				evaluationRecommendationView.setTypeId(Long.parseLong(evaluationRecommendationView.getStrTypeId()));
			} catch (Exception e) {
				valid = false;
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.invalidValue", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.Evaluation.Type") ) );
				logger.LogException(CommonUtil.getParamBundleMessage("commons.invalidValue", CommonUtil.getParamBundleMessage("portfolio.manage.tab.portfolio.Evaluation.Type") ), e);
			}
		}
		
		if (evaluationRecommendationView.getText()==null || "".equalsIgnoreCase(evaluationRecommendationView.getText())) {
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("evaluation.recommendation") ) );
			valid = false;
		} else if (evaluationRecommendationView.getText().length()>2000){
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.lengthTooLong", CommonUtil.getParamBundleMessage("evaluation.recommendation"), "2000" ) );
			valid = false;
		}
				
		return valid;
		
	}
	
	private boolean validateRecommendations() {
		boolean valid = true;
		
		
		// no recommendations at all
		if (evaluationRecommendationList==null || evaluationRecommendationList.size()==0) {
			valid = false;
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("evaluation.recommendation") ) );			
		}
		else {
			boolean rentCommitteRecommendationSupplied = false;
			boolean hosRecommendationSupplied = false;
			
			Long rentCommitteRecommendTypeId = CommonUtil.getDomainDataId(WebConstants.EvaluationApplication.EVALUATION_RECOMMENDATION_TYPE, WebConstants.EvaluationApplication.RENT_COMMITTE_RECOMMENDATION);
			Long hostCommitteRecommendTypeId = CommonUtil.getDomainDataId(WebConstants.EvaluationApplication.EVALUATION_RECOMMENDATION_TYPE, WebConstants.EvaluationApplication.HOS_RECOMMENDATION);
			
			for (EvaluationRecommendationView recommendation : evaluationRecommendationList) {
				if (rentCommitteRecommendTypeId.equals(recommendation.getTypeId())) {
					rentCommitteRecommendationSupplied = true;
				}
				
				if (hostCommitteRecommendTypeId.equals(recommendation.getTypeId())){
					hosRecommendationSupplied = true;
				}
				
				if (rentCommitteRecommendationSupplied && hosRecommendationSupplied) {
					break;
				}
			}
		
			if (!rentCommitteRecommendationSupplied) {
				valid = false;
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("propertyEvaluation.rentCommitteeRecommendation") ) );
			}
			if (!hosRecommendationSupplied) {
				valid = false;
				errorMessages.add( CommonUtil.getParamBundleMessage("commons.required", CommonUtil.getParamBundleMessage("propertyEvaluation.hosRecommendation") ) );
			}

			
		}
		
		
		return valid;
	}
	
	@SuppressWarnings("unchecked")
	public void addEvaluationRecommendation(ActionEvent event){
		if (validateEvaluationRecommendation()) {
			
			Long typeId = evaluationRecommendationView.getTypeId();

			try {
				updateValuesFromMap();
				Map<Long, DomainData> domainDataMap = (Map<Long, DomainData>) new UtilityManager().getDomainDataMap();

				evaluationRecommendationView.setTypeEn(domainDataMap.get(typeId).getDataDescEn());
				evaluationRecommendationView.setTypeAr(domainDataMap.get(typeId).getDataDescAr());

				evaluationRecommendationView.setCreatedBy( CommonUtil.getLoggedInUser() );
				evaluationRecommendationView.setCreatedOn( new Date() );
				evaluationRecommendationView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				evaluationRecommendationView.setUpdatedOn( new Date() );
				evaluationRecommendationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				evaluationRecommendationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				if(viewMap.get(WebConstants.REQUEST_VIEW) != null) {
					RequestView requestView = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
					if (requestView!=null && requestView.getRequestId()!=null) { //It will work for existing requests
						evaluationRecommendationView.setRequestView(requestView);
					}
				}	
				
			} catch (PimsBusinessException e) {
				errorMessages.add( CommonUtil.getBundleMessage("commons.ErrorMessage"));
				logger.LogException("", e);
			}

			evaluationRecommendationList.add(evaluationRecommendationView);

			updateValuesToMap();
		}
	}
	
	public void deleteEvaluationRecommendation(ActionEvent event){
		try {
			
			updateValuesFromMap();

			EvaluationRecommendationView evaluationRecommendationView = (EvaluationRecommendationView) evaluationRecommendationDataTable.getRowData();

			if ( evaluationRecommendationView.getEvaluationRecommendationId() == null ) {
				evaluationRecommendationList.remove(evaluationRecommendationView);
			}	
			else {
				evaluationRecommendationView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
			}
			
			updateValuesToMap();

		} catch (PimsBusinessException e) {
			errorMessages.add( CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("", e);
		}
		
		
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMap() {
		if (evaluationRecommendationList!=null) {
			viewMap.put(WebConstants.EvaluationApplication.RECOMMENDATION_LIST, evaluationRecommendationList);
			viewMap.put("recordSize",evaluationRecommendationList.size());
			
		}
		
		if(requestView != null) {
			viewMap.put(WebConstants.REQUEST_VIEW,requestView);
		}	
		
		
	
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMap() throws PimsBusinessException {
		if(viewMap.get(WebConstants.REQUEST_VIEW) != null) {
			requestView = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
		}	
		
		
		if (viewMap.get(WebConstants.EvaluationApplication.RECOMMENDATION_LIST)!=null)  {
			evaluationRecommendationList = (List<EvaluationRecommendationView>) viewMap.get(WebConstants.EvaluationApplication.RECOMMENDATION_LIST);
		} else if (requestView!=null && requestView.getRequestId()!=null) {
			EvaluationRecommendationView evaluationRecommendationView = new EvaluationRecommendationView();
			evaluationRecommendationView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			evaluationRecommendationView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			evaluationRecommendationView.setRequestView(requestView);
			evaluationRecommendationList = requestServiceAgent.getEvaluationRecomendationList(evaluationRecommendationView);
		}
		updateValuesToMap();
	}
	public EvaluationRecommendationView getEvaluationRecommendationView() {
		return evaluationRecommendationView;
	}
	public void setEvaluationRecommendationView(
			EvaluationRecommendationView evaluationRecommendationView) {
		this.evaluationRecommendationView = evaluationRecommendationView;
	}
	public HtmlDataTable getEvaluationRecommendationDataTable() {
		return evaluationRecommendationDataTable;
	}
	public void setEvaluationRecommendationDataTable(
			HtmlDataTable evaluationRecommendationDataTable) {
		this.evaluationRecommendationDataTable = evaluationRecommendationDataTable;
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getEvaluationDateFormat() {
		String dateFormat = "dd/MM/yyyy hh:mm:ss aaa";
		return dateFormat;
	}
	
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();		
	}
	
	public List<EvaluationRecommendationView> getEvaluationRecommendationList() {
		if (viewMap.get(WebConstants.EvaluationApplication.RECOMMENDATION_LIST)!=null)  {
			evaluationRecommendationList = (List<EvaluationRecommendationView>) viewMap.get(WebConstants.EvaluationApplication.RECOMMENDATION_LIST);
		}
		return evaluationRecommendationList;
	}
	
	public void setEvaluationRecommendationList(List<EvaluationRecommendationView> evaluationRecommendationList) {
		this.evaluationRecommendationList = evaluationRecommendationList;
	}
	
	public Integer getRecordSize() {
		Integer recordSize = 0;
		 
		if(viewMap.get("recordSize")!=null) {
			recordSize = (Integer)viewMap.get("recordSize");
		}	
		return recordSize;
	}

	
	
}