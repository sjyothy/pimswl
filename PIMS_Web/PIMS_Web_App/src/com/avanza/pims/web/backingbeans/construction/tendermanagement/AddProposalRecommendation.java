package com.avanza.pims.web.backingbeans.construction.tendermanagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.document.control.AttachmentController;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.TenderProposalView;

public class AddProposalRecommendation extends AbstractController
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 928325391349984380L;
	
	private static final Logger logger = Logger.getLogger(EditProposalDetails.class);
	
	private List<String> errorMessages;
	private List<String> infoMessages;
	private TenderProposalView tenderProposalView;
	private ConstructionServiceAgent constructionServiceAgent;
	private Map<String, Object> sessionMap;
	private Map<String, Object> viewMap;
	private Map<String, String> tenderProposalPeriodTypeMap;
	
	public AddProposalRecommendation() 
	{
		logger.logInfo("AddProposalRecommendation() started...");
		
		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();
		tenderProposalView = new TenderProposalView();
		constructionServiceAgent = new ConstructionServiceAgent();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		viewMap = getFacesContext().getViewRoot().getAttributes();
		tenderProposalPeriodTypeMap = new HashMap<String, String>();
		loadScreenData();
		
		logger.logInfo("AddProposalRecommendation() finished...");
	}
	
	private void loadScreenData() 
	{
		logger.logInfo("loadScreenData() started...");
		
		if ( sessionMap.get( WebConstants.Tender.PROPOSAL_VIEW ) != null )
		{
			viewMap.put(WebConstants.Tender.PROPOSAL_VIEW, sessionMap.get( WebConstants.Tender.PROPOSAL_VIEW ));
			sessionMap.remove(WebConstants.Tender.PROPOSAL_VIEW );
			viewMap.put(WebConstants.Tender.PROPOSAL_PERIOD_TYPE_MAP, getProposalPeriodTypeMap());
		}
		
		updateFromViewRoot();
		
		logger.logInfo("loadScreenData() finished...");
	}
	
	private void updateFromViewRoot() 
	{
		logger.logInfo("updateFromViewRoot() started...");
		
		if ( viewMap.get( WebConstants.Tender.PROPOSAL_VIEW ) != null )		
			tenderProposalView = (TenderProposalView) viewMap.get( WebConstants.Tender.PROPOSAL_VIEW );
		
		if( viewMap.get( WebConstants.Tender.PROPOSAL_PERIOD_TYPE_MAP ) != null )
			tenderProposalPeriodTypeMap = (Map<String, String>) viewMap.get( WebConstants.Tender.PROPOSAL_PERIOD_TYPE_MAP );
		
		logger.logInfo("updateFromViewRoot() finished...");
	}
	
	public String btnSave_action() 
	{
		logger.logInfo("btnSave_action() started...");
		
		try 
		{
			tenderProposalView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			tenderProposalView.setRecommendationUpdatedBy( CommonUtil.getLoggedInUser() );
			tenderProposalView.setRecommendationUser( CommonUtil.getLoggedInUser() );
			constructionServiceAgent.updateTenderProposal(tenderProposalView);
			String javaScriptText = "javascript:closeWindowSubmit();";
			closeWindowAndSubmit(javaScriptText);
			sessionMap.put("reloadOpener", true);
			logger.logInfo("btnSave_action() finished...");
		}
		catch (Exception e)
		{
			logger.LogException("btnSave_action() exception ", e);
		}	
		
		return null;
	}
	
	private void closeWindowAndSubmit(String javaScriptText) 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	private Map<String,String> getProposalPeriodTypeMap() 
	{
		logger.logInfo("getProposalPeriodTypeMap() started...");
		
		Map<String, String> proposalPeriodTypeMap = new HashMap<String, String>();
		
		CommonUtil commonUtil = new CommonUtil();		
		List<DomainDataView> tenderProposalPeriodTypeList = commonUtil.getDomainDataListForDomainType( WebConstants.Tender.DOMAIN_TYPE_TENDER_PROPOSAL_PERIOD_TYPE );
		for (DomainDataView tenderProposalPeriodType : tenderProposalPeriodTypeList)
			proposalPeriodTypeMap.put( getIsEnglishLocale() ? tenderProposalPeriodType.getDataDescEn() : tenderProposalPeriodType.getDataDescAr(), 
								String.valueOf( tenderProposalPeriodType.getDomainDataId() ) );
		
		logger.logInfo("getProposalPeriodTypeMap() finished...");
		
		return proposalPeriodTypeMap;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public TenderProposalView getTenderProposalView() {
		return tenderProposalView;
	}

	public void setTenderProposalView(TenderProposalView tenderProposalView) {
		this.tenderProposalView = tenderProposalView;
	}

	public ConstructionServiceAgent getConstructionServiceAgent() {
		return constructionServiceAgent;
	}

	public void setConstructionServiceAgent(
			ConstructionServiceAgent constructionServiceAgent) {
		this.constructionServiceAgent = constructionServiceAgent;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public Map<String, String> getTenderProposalPeriodTypeMap() {
		return tenderProposalPeriodTypeMap;
	}

	public void setTenderProposalPeriodTypeMap(
			Map<String, String> tenderProposalPeriodTypeMap) {
		this.tenderProposalPeriodTypeMap = tenderProposalPeriodTypeMap;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}
	private Boolean saveAttachments(String requestId)
	{
		Boolean success = false;
		String methodName="saveAtttachments";
		logger.logInfo("|"+"Start..");

		try
		{
			String repositoryId = WebConstants.ATTACHMENT_REPOSITORY_ID;
			logger.logInfo(methodName+"|"+"repositoryId.."+repositoryId);
			String fileSystemRepository = WebConstants.FILE_SYSTEM_REPOSITORY_TRANSFER_CONTRACT;
			logger.logInfo(methodName+"|"+"fileSystemRepository.."+fileSystemRepository);
			String user = CommonUtil.getLoggedInUser();
			logger.logInfo(methodName+"|"+"user.."+user);
			String entityId = requestId;
			logger.logInfo(methodName+"|"+"entityId.."+entityId);
			logger.logInfo(methodName+"|"+"Saving attachment..START");
			AttachmentController.saveAttachments(repositoryId, fileSystemRepository, CommonUtil.getLoggedInUser(), entityId);
			logger.logInfo(methodName+"|"+"Saving attachment..complete");
			success = true;
		}
		catch(Exception ex)
		{
			logger.LogException("|"+"crashed..",ex);
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.General.ATTACHMENT_FAILURE));
		}
		logger.logInfo("|"+"Finish..");

		return success;
	}
	public String showAttachmentLink_action() {
		
		String javaScriptText = "javascript:showAttachmentsPopup();";
		openPopup(javaScriptText);
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	

}
