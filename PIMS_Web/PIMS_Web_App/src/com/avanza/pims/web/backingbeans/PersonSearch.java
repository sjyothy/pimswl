package com.avanza.pims.web.backingbeans;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
public class PersonSearch extends AbstractController
{
	
	private HtmlDataTable dataTable = new HtmlDataTable();
	private HtmlDataTable propspectivePersonDataTable;
	private String firstName  ;
	private String lastName ;
	private Long personTypeId;
	private Long nationalityId;
	private String passportNumber;
	private String residenseVisaNumber;
	private String personalSecCardNo;
	private String drivingLicenseNumber;
	private String socialSecNumber;
	private String cellNumber;
	private String designation;
	private String gender;
	private boolean showProspectiveGrid;
	//private Calendar occupiedTillDate = new Calendar();
	private PersonView personView = new PersonView();
	String controlName;
	String hdndisplaycontrolName;
	String controlForId;
	String requestForPersonType;
	String displayStylePersonTypeCombo;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	Map sessionMap= FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    List<SelectItem> personTypeList =new ArrayList<SelectItem>();
	public String selectManyPersonType;
	String sessionName; 
	//We have to look for the list population for facilities
	private List<String> errorMessages;
    private boolean isSelectOnePerson;
    private boolean isSelectManyPerson;
	private PersonView dataItem = new PersonView();
	private PersonView prospectiveDataItem = new PersonView();	
	private static Logger logger = Logger.getLogger( ContractPersonList.class );
	private List<PersonView> dataList = new ArrayList<PersonView>();
	private List<PersonView> prospectivePersonDataList = new ArrayList<PersonView>();
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> personSubTypeList = new ArrayList<SelectItem>();
	private ResourceBundle bundle;
	private String selectedCountryId;
	private String stateId;
	private String personSubTypeId;
	//	public Unit dataItem = new Unit();
	
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	
	public ResourceBundle getBundle() {
			
			String methodName="getBundle";
	    	logger.logInfo(methodName+"|"+" Start");
	
	        if(getIsArabicLocale())
	
	              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");
	
	        else
	
	              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");
	
	        logger.logInfo(methodName+"|"+" Finish");
	        
	        return bundle;
	
	  }
	
	private boolean hasErrors()
	{
		
		String methodName="hasErrors";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean isError=false;
		
		errorMessages=new ArrayList<String>();
    	errorMessages.clear();

		if (firstName == null)
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}

		if (lastName == null)
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}
		
		
		
		if (personTypeId==null || personTypeId.equals("0"))
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}
		
		
		if (gender==null || gender.equals("0"))
		{
			errorMessages.add(getBundle().getString(MessageConstants.ViolationDetails.MSG_SELECT_CATEGORY));
			isError = true;
		}
		
		
		
    	logger.logInfo(methodName+"|"+" Finish");
		
		
		return isError;
	}
	
	public String edit(){
		
		
		PersonView personV = (PersonView) propspectivePersonDataTable.getRowData();
		
		 setRequestParam("persontype", requestForPersonType);
		 setRequestParam("edit", "true");
		 setRequestParam("personId", personV.getPersonId());
		 
		return "add";
	}
	
	
	public String add(){
		
//		try {
//		
//			if (!hasErrors())
//			{
//			
//				PersonView personToAdd = new PersonView();
//				
//				personToAdd.setFirstName(firstName);
//				personToAdd.setLastName(lastName);
//				personToAdd.setPersonTypeId(personTypeId);
//				personToAdd.setDesignation(designation);
//				personToAdd.setNationalityId(nationalityId);
//				personToAdd.setPassportNumber(passportNumber);
//				personToAdd.setResidenseVisaNumber(residenseVisaNumber);
//				personToAdd.setPersonalSecCardNo(personalSecCardNo);
//				personToAdd.setDrivingLicenseNumber(drivingLicenseNumber);
//				personToAdd.setSocialSecNumber(socialSecNumber);
//				personToAdd.setCellNumber(cellNumber);
//				personToAdd.setGender(gender);
//				
//				personToAdd.setUpdatedBy(getLoggedInUser());
//				personToAdd.setUpdatedOn(new Date());
//				personToAdd.setCreatedBy(getLoggedInUser());
//				personToAdd.setCreatedOn(new Date());
//				personToAdd.setRecordStatus(1L);
//				personToAdd.setIsDeleted(0L);
//				
//				PropertyServiceAgent psa = new PropertyServiceAgent();
//			
//				psa.addPerson(personToAdd);
//			
//			}
//			
//		} catch (PimsBusinessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		 HttpServletRequest request =
//			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		   request.setParameter("persontype",requestForPersonType);
		   
		   setRequestParam("persontype", requestForPersonType);
		
		return "add";
	}

	public String getPersonSubTypeId() {
		return personSubTypeId;
	}

	public void setPersonSubTypeId(String personSubTypeId) {
		this.personSubTypeId = personSubTypeId;
	}

	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public String getSelectManyPersonType()
    {
		return selectManyPersonType;
	}

	public void setSelectManyPersonType(String selectManyPersonType) {
		this.selectManyPersonType = selectManyPersonType;
	}

	@Override 
	 public void init() 
     {
		String methodName="init";
    	logger.logInfo(methodName+"|"+"Start");
    	 super.init();
    			   
    			   HttpServletRequest request =
        			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    			   if(request.getParameter("persontype")!=null)
    			   {
        			this.requestForPersonType=request.getParameter("persontype").toString();
        			
        			PropertyServiceAgent psa = new PropertyServiceAgent();
        			
        			DomainDataView ddv = null;
        			try {
        				
	        			if (requestForPersonType.equals("OWNER"))
	        			{
	        				
								ddv = psa.getDomainDataByValue(WebConstants.PERSON_TYPE,WebConstants.PERSON_TYPE_OWNER );
							
	        			}
	        			else if (requestForPersonType.equals("REPRESENTATIVE")) 
	        			{
	        				ddv = psa.getDomainDataByValue(WebConstants.PERSON_TYPE,WebConstants.PERSON_TYPE_REPRESENTATIVE );
	        			}
	    			   
	        			if (ddv != null && ddv.getDomainDataId() != null)
	        			{
	        				personTypeId = ddv.getDomainDataId();
	        			}
        			
        			} catch (PimsBusinessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			   }
    			   loadCountry();
    			   
    			   if (!isPostBack())
    				 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
    			   
    			   loadPersonSubType();
    			   
	 }
	  public void preprocess() 
	    {
		 super.preprocess();
		 requestForPersonType=requestForPersonType;
	//	 getSessionName();
	    }
	  public void prerender() 
	    {
		 super.prerender();
//		 getSessionName();
	//	 getshowProspectiveGrid();
	    }

	  public void loadPersonSubType()  
	  {
		  ApplicationBean appBean = new ApplicationBean();
		  this.setPersonSubTypeList(appBean.getPerosnSubTypeList(this.getRequestForPersonType()+"_TYPE"));
	  }
	  
	  public void loadCountry()  {
			
			
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
			      this.getCountryList().add(item);
			  }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("country", countryList);
			}catch (Exception e){
				logger.logDebug("Exception Occured - In loadCountry() of Person search");
				
			}
		}
	  
		public void loadState(ValueChangeEvent vce)  {
			
			
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			
			String selectedCountryName = "";
			for (int i=0;i<this.getCountryList().size();i++)
			{
				if (new Long(vce.getNewValue().toString())==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
				{
					selectedCountryName = 	this.getCountryList().get(i).getLabel();
				}
			}	
			
			List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (getIsEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      this.getStateList().add(item);
			  }
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
			}catch (Exception e){
				
				logger.logDebug("Error Occuired - In loadCountry() of Person search List");
				
			}
		}
//	 private void loadPersonTypeCombo() 
//	 {
//		 String methodName="loadPersonTypeCombo";
//		 logger.logInfo(methodName+"|"+"Start");
//		 PropertyServiceAgent psa=new PropertyServiceAgent();
//		 try
//		 {
//			 if(!sessionMap.containsKey(WebConstants.PERSON_TYPE) )
//			 {
//				 List<DomainDataView> domainDataList=psa.getDomainDataByDomainTypeName(WebConstants.PERSON_TYPE);
//				 sessionMap.put(WebConstants.PERSON_TYPE, domainDataList);
//				 
//			 }
//			 logger.logInfo(methodName+"|"+"Finish");
//		 }
//		 catch(Exception ex)
//		 {
//			 logger.logError(methodName+"|"+"Exception ::"+ex);
//			 
//		 }
//	 }
	 
//	public String btnAdd_Click()
//	{
//		String methodName="btnAdd_Click";
//		logger.logInfo(methodName+"|"+"Start");
//		
//		logger.logInfo(methodName+"|"+"Finish");
//		return "addPerson";
//		
//	}
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
//	public List<PersonView> getPropertyInquiryDataList()
//	{
//		String methodName="getPropertyInquiryDataList";
//		
//		logger.logInfo(methodName+"|"+"Start");
//		
//		if (dataList == null || dataList.size()==0)
//		{
//			dataList= loadDataList(); 
//		}
//		logger.logInfo(methodName+"|"+"Finish");
//		return dataList;
//	}
	public List<PersonView> getProspectivePersonDataList()
	{
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("persons") != null)
			prospectivePersonDataList = (List<PersonView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("persons");
		
		return prospectivePersonDataList;
	}
	
	public void setProspectivePersonDataList(List <PersonView> pview)
	{
		this.prospectivePersonDataList = pview;
	}

//	public String btnAddPersonToProspectiveSession_Click()
//	{
//		String methodName="btnAddPersonToProspectiveSession_Click";
//		logger.logInfo(methodName+"|"+"Start");
//		try 
//		{
//			requestForPersonType=requestForPersonType;
//			 if(requestForPersonType.equals(WebConstants.OCCUPIER))
//	 			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER;
//			 else if(requestForPersonType.equals(WebConstants.PARTNER))
//	     			 sessionName=WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER;
//		addNewItemsInSession();
//		logger.logInfo(methodName+"|"+"Finish");
//		}
//		catch (Exception ex)
//		{
//			logger.logError(methodName+"|"+"Error"+ex);
//			
//		}
//		showProspectiveGrid=true;
//		return "";
//	}
	public boolean getshowProspectiveGrid()
	{
		
        String methodName="getshowProspectiveGrid";
		logger.logInfo(methodName+"|"+"Start");
		logger.logInfo(methodName+"|"+"showProspectiveGrid:"+showProspectiveGrid);
		showProspectiveGrid=false;
		logger.logInfo(methodName+"|"+"sessionName:"+sessionName);
		if(sessionName!=null)
		{
		if(sessionMap.containsKey(sessionName) )
			showProspectiveGrid=true;
		}
		
		logger.logInfo(methodName+"|"+"showProspectiveGrid:"+showProspectiveGrid);
		logger.logInfo(methodName+"|"+"Finish");
		return showProspectiveGrid;
		
	}
//	private void addNewItemsInSession() throws Exception
//	{
//	    String methodName="addNewItemsInSession";
//		List<PersonView> previouspersonViewList =new ArrayList<PersonView>();
//		List<PersonView> newpersonViewList =new ArrayList<PersonView>();
//		logger.logInfo(methodName+"|"+"Start");
//		try
//		{
//			PersonView editpersonView=(PersonView)dataTable.getRowData();
//			if(sessionMap.containsKey(sessionName))
//			{
//			 	previouspersonViewList= (ArrayList)sessionMap.get(sessionName);
//			 	for(int i=0;i<previouspersonViewList.size();i++)
//		        {
//		             PersonView previousPreviousView=(PersonView) previouspersonViewList.get(i);
//		              //logger.logInfo(methodName+"|"+"previousPaidFacilityView.getFacilityId():::"+previousPaidFacilityView.getFacilityId());
//					  //logger.logInfo(methodName+"|"+"Selected paidFacilitiesView.getFacilityId():::"+paidFacilitiesView.getFacilityId());
//					  //If previous List has same item with id  as new View 
//		              //then discard it else add that item to new List.    
//					 if(!previousPreviousView.getPersonId().toString().equals(editpersonView.getPersonId().toString()))
//		             {
//		            	 newpersonViewList.add(previousPreviousView);
//		              
//		             }
//		         }
//			}
//			
//			newpersonViewList.add(editpersonView);
//			
//			sessionMap.put(sessionName, newpersonViewList);
//					
//			logger.logInfo(methodName+"|"+"Finish");
//		}
//        catch(Exception e)
//        {
//             logger.logError(methodName +"|"+"Exception Occured::"+e);
//             throw e;
//        }
//
//	
//
//
//
//     }
//	public String doBid() {
////		errorMessages = new ArrayList<String>();
///*		if (getCustomerId().equals("")) {
//		 errorMessages.add("local message");
//		}
//		if (errorMessages.size() > 0) {
//			return(null);
//			} else {
//			return("success");
//			}
//			
//*/ 
//		return "";
//		}
	
	public String doSearch (){

        

        //InquiryManager inquiryManager = new InquiryManager(); 

        String methodName="doSearch";
		logger.logInfo(methodName+"|"+"Start");


                    try {
                    	
                    
                            if (firstName !=null && !firstName.equals("")){
                            
                            	personView.setFirstName(firstName.trim());                           	
                             	
                            }
                            if (lastName !=null && !lastName.equals("")){
                            	
                            	personView.setLastName(lastName.trim());
                            }
                    
	                        if(personalSecCardNo!=null && !personalSecCardNo.equals(""))
	                        {
	                        	personView.setPersonalSecCardNo(personalSecCardNo);
	                        	
	                        }
	                        
	                        if(passportNumber!=null && !passportNumber.equals(""))
	                        {
	                        	personView.setPassportNumber(passportNumber);
	                        	
	                        }
	                        if(residenseVisaNumber!=null && !residenseVisaNumber.equals(""))
	                        {
	                        	personView.setResidenseVisaNumber(residenseVisaNumber);
	                        	
	                        }
	                        if(drivingLicenseNumber!=null && !drivingLicenseNumber.equals(""))
	                        {
	                        	personView.setDrivingLicenseNumber(drivingLicenseNumber);
	                        	
	                        }
	                        if(socialSecNumber!=null && !socialSecNumber.equals(""))
	                        {
	                        	personView.setSocialSecNumber(socialSecNumber);
	                        	
	                        }
		                    dataList.clear();
		                  
		                    loadDataList();

		            		logger.logInfo(methodName+"|"+"Finsih");

	                    }

                    catch (Exception e){


                		logger.logError(methodName+"|"+"Start");
       

                    }

                return "";

     }
	
	public String delete()
	{
		 try {
			 
			PersonView selectedPerson = (PersonView) propspectivePersonDataTable.getRowData();
				
			Long personId = selectedPerson.getPersonId();
			
			PropertyServiceAgent psa = new PropertyServiceAgent();
			 
			psa.deletePerson(personId);
			
			loadDataList();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	public String enableOrDisable()
	{
		 try {
			 
			PersonView selectedPerson = (PersonView) propspectivePersonDataTable.getRowData();
				
			Long personId = selectedPerson.getPersonId();
			
			PropertyServiceAgent psa = new PropertyServiceAgent();
			 
			
			if (selectedPerson.getRecordStatus().longValue() == 1L)
				psa.disablePerson(personId);
			else if (selectedPerson.getRecordStatus().longValue() == 0L)
				psa.enablePerson(personId);
			
		
			loadDataList();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public void loadDataList() 
	{
		 String methodName="loadDataList";
		 List<PersonView> list = new ArrayList();

			logger.logInfo(methodName+"|"+"Start");

		try
		{
			   
 			   PropertyServiceAgent myPort = new PropertyServiceAgent();
 			   CommonUtil commonUtil = new CommonUtil();
 			   List<DomainDataView> domainDataList = commonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE);
	        	   //List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
	   			   for(int i=0;i<domainDataList.size();i++)
	   			   {
	   			     DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
	   				 if(domainDataView.getDataValue().equals( this.requestForPersonType)   )
	   				 {
	   					personView.setPersonTypeId(domainDataView.getDomainDataId());
	   					break;
	   				 }
	   			   }
		            
	   		  if (!firstName.equals(""))
	   			personView.setFirstName(firstName);
	   		  
	   		  if (!lastName.equals(""))
	   		  personView.setLastName(lastName);
	   			
	   		  if (!socialSecNumber.equals(""))
	   		  personView.setSocialSecNumber(socialSecNumber);
	   			
	   		  if (!passportNumber.equals(""))
	   		  personView.setPassportNumber(passportNumber);
	   			
	   			if (personSubTypeId != null && personSubTypeId.length()>0 && !stateId.equals("0"))
	   				personView.setPersonSubTypeId(new Long (personSubTypeId));
	   			
	   		  if (!residenseVisaNumber.equals(""))
	   			personView.setResidenseVisaNumber(residenseVisaNumber);
	   			
	   			ContactInfoView con = new ContactInfoView();
	   			
	   			if (selectedCountryId != null && selectedCountryId.length()>0 && !selectedCountryId.equals("0"))
	   				con.setCountryId(new Long(selectedCountryId));
	   			
	   			if (stateId != null && stateId.length()>0 && !stateId.equals("0"))
	   				con.setStateId(new Long(stateId));
	   			
	   			
//	   			if (con.getCountryId()!=null || con.getStateId()!=null)
//	   			personView.setContactInfoView(con);
	   			
	   			this.setProspectivePersonDataList(myPort.getPersonInformation(personView));
	           

	   			   FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("persons", prospectivePersonDataList);
	   			   
	   			   
	   		logger.logInfo(methodName+"|"+"Finish");

	    }
		catch (Exception ex) 
		{

			logger.logError(methodName+"|"+"loadDataList");

	    }
	
		
	}
//	public String getErrorMessages() {
//		String messageList;
//		if ((errorMessages == null) ||
//		(errorMessages.size() == 0)) {
//		messageList = "";
//		} else {
//		messageList = "<FONT COLOR=RED><B><UL>\n";
//		for(String message: errorMessages) {
//		messageList = messageList + "<LI>" + message + "\n";
//		}
//		messageList = messageList + "</UL></B></FONT>\n";
//		}
//		return(messageList);
//		}
//
//    public boolean getisSelectOnePerson()
//    {
//    	String methodName="getisSelectOnePerson";
//
//		logger.logInfo(methodName+"|"+"Start");
//
//    	if(requestForPersonType!=null && !requestForPersonType.equals(""))
//    	{
//    		if(requestForPersonType.equals(WebConstants.SPONSOR)||requestForPersonType.equals(WebConstants.MANAGER))
//    			isSelectOnePerson=true;
//    	}
//
//		logger.logInfo(methodName+"|"+"Finish");
//
//    	return isSelectOnePerson;
//    }
//    public boolean getisSelectManyPerson()
//    {
//    	String methodName="isSelectManyPerson";
//
//		logger.logInfo(methodName+"|"+"Start");
//
//    	if(requestForPersonType!=null && !requestForPersonType.equals(""))
//    	{
//    		if(requestForPersonType.equals(WebConstants.OCCUPIER)||requestForPersonType.equals(WebConstants.PARTNER))
//    			isSelectManyPerson=true;
//    		else
//    			isSelectManyPerson=false;
//    		
//    	}
//    	else
//		isSelectManyPerson=true;
//    	
//		logger.logInfo(methodName+"|"+"Finish");
//
//    	return isSelectManyPerson;
//    }

	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Long getPersonTypeId() {
		return personTypeId;
	}



	public void setPersonTypeId(Long personTypeId) {
		this.personTypeId = personTypeId;
	}



	public Long getNationalityId() {
		return nationalityId;
	}



	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}



	public String getPassportNumber() {
		return passportNumber;
	}



	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}



	public String getResidenseVisaNumber() {
		return residenseVisaNumber;
	}



	public void setResidenseVisaNumber(String residenseVisaNumber) {
		this.residenseVisaNumber = residenseVisaNumber;
	}



	public String getPersonalSecCardNo() {
		return personalSecCardNo;
	}



	public void setPersonalSecCardNo(String personalSecCardNo) {
		this.personalSecCardNo = personalSecCardNo;
	}



	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}



	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}



	public String getSocialSecNumber() {
		return socialSecNumber;
	}



	public void setSocialSecNumber(String socialSecNumber) {
		this.socialSecNumber = socialSecNumber;
	}



	public PersonView getPersonView() {
		return personView;
	}



	public void setPersonView(PersonView personView) {
		this.personView = personView;
	}



	public PersonView getDataItem() {
		return dataItem;
	}



	public void setDataItem(PersonView dataItem) {
		this.dataItem = dataItem;
	}



	public List<PersonView> getDataList() {
		return dataList;
	}



	public void setDataList(List<PersonView> dataList) {
		this.dataList = dataList;
	}



	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getErrorMessages() 
	{
		String methodName="getErrorMessages";
    	logger.logInfo(methodName+"|"+" Start");
		
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
				{
					messageList += "<LI>"+ message+ " <br></br>" ;
			    }
			messageList = messageList + "</UL></B></FONT>\n";
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return	messageList;
		
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getControlForId() {
		return controlForId;
	}
	public void setControlForId(String controlForId) {
		this.controlForId = controlForId;
	}
	public String getRequestForPersonType() {
		return requestForPersonType;
	}
	public void setRequestForPersonType(String requestForPersonType) {
		this.requestForPersonType = requestForPersonType;
	}
	public String getDisplayStylePersonTypeCombo() {
		if(requestForPersonType!=null && !requestForPersonType.equals(""))
			displayStylePersonTypeCombo="display:inline";
		else
			displayStylePersonTypeCombo="display:none";
		return displayStylePersonTypeCombo;
	}
	public void setDisplayStylePersonTypeCombo(String displayStylePersonTypeCombo) {
		this.displayStylePersonTypeCombo = displayStylePersonTypeCombo;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
//	
//	public List<SelectItem> getPersonTypeList() 
//	{
//        String methodName="getPersonTypeList";
//		logger.logInfo(methodName+"|"+"Start");
//
//		personTypeList.clear();
//		if(!sessionMap.containsKey(WebConstants.PERSON_TYPE))
//			loadPersonTypeCombo();
//		else
//		{
//			List<DomainDataView> domainDataList=(ArrayList)sessionMap.get(WebConstants.PERSON_TYPE);
//			for(int i=0;i<domainDataList.size();i++)
//			{
//				SelectItem item =null;
//				DomainDataView domainDataView=(DomainDataView)domainDataList.get(i);
//				System.out.println(domainDataView.getDataValue());
//				if(domainDataView.getDataValue().equals( requestForPersonType)   )
//				{
//					if(getIsEnglishLocale())  
//					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescEn() );
//					else  
//					  item = new SelectItem(domainDataView.getDomainDataId().toString(),domainDataView.getDataDescAr() );
//					  personTypeList.add(item);
//				}
//				
//				
//			}
//		}
//
//		logger.logInfo(methodName+"|"+"Finish");
//
//		return personTypeList;
//	}

	public HtmlDataTable getPropspectivePersonDataTable() {
		return propspectivePersonDataTable;
	}

	public void setPropspectivePersonDataTable(
			HtmlDataTable propspectivePersonDataTable) {
		this.propspectivePersonDataTable = propspectivePersonDataTable;
	}

	public PersonView getProspectiveDataItem() {
		return prospectiveDataItem;
	}

	public void setProspectiveDataItem(PersonView prospectiveDataItem) {
		this.prospectiveDataItem = prospectiveDataItem;
	}

	public String getHdndisplaycontrolName() {
		return hdndisplaycontrolName;
	}

	public void setHdndisplaycontrolName(String hdndisplaycontrolName) {
		this.hdndisplaycontrolName = hdndisplaycontrolName;
	}

	public String getSelectedCountryId() {
		return selectedCountryId;
	}

	public void setSelectedCountryId(String selectedCountryId) {
		this.selectedCountryId = selectedCountryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public List<SelectItem> getPersonSubTypeList() {
		return personSubTypeList;
	}

	public void setPersonSubTypeList(List<SelectItem> personSubTypeList) {
		this.personSubTypeList = personSubTypeList;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	
	
	


	

	
	
}
