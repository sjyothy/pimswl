package com.avanza.pims.web.backingbeans;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.TenantViolationsReportCriteria;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class TenantViolationsReportBean extends AbstractController{

		private TenantViolationsReportCriteria reportCriteria; 
		private HtmlCommandButton btnPrint = new HtmlCommandButton();
		private String dateFormat;
		
		public TenantViolationsReportBean(){
			if(CommonUtil.getIsEnglishLocale())
				reportCriteria = new TenantViolationsReportCriteria(ReportConstant.Report.TENANT_VIOLATION_EN, ReportConstant.Processor.TENANT_VIOLATION, CommonUtil.getLoggedInUser());
			else
				reportCriteria = new TenantViolationsReportCriteria(ReportConstant.Report.TENANT_VIOLATION_AR, ReportConstant.Processor.TENANT_VIOLATION, CommonUtil.getLoggedInUser());
		}

		public TenantViolationsReportCriteria getReportCriteria() {
			return reportCriteria;
		}
		public void setReportCriteria(TenantViolationsReportCriteria reportCriteria) {
			this.reportCriteria = reportCriteria;
		}
		public HtmlCommandButton getBtnPrint() {
			return btnPrint;
		}
		public void setBtnPrint(HtmlCommandButton btnPrint) {
			this.btnPrint = btnPrint;
		}
		 private static Logger logger = Logger.getLogger("TenantViolationsReportBean");
		 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		 
		 HttpServletRequest requestParam =
			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

		public String btnPrint_Click() {	
			
			    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
					openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
					return "";
		}
		private void openPopup(String javaScriptText) {
			logger.logInfo("openPopup() started...");
			try {
				FacesContext facesContext = FacesContext.getCurrentInstance();			
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
				logger.logInfo("openPopup() completed successfully!!!");
			} catch (Exception exception) {
				logger.LogException("openPopup() crashed ", exception);
			}
		}
	    public String getDateFormat(){
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    	
	    	dateFormat= localeInfo.getDateFormat();
	    	
			return dateFormat;
		}
		
	}
	

