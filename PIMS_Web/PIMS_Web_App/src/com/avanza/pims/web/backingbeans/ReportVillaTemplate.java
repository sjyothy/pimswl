package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.exporter.ListBasedExporterMeta;
import com.avanza.pims.exporter.reportdetail.ReportDetail;
import com.avanza.pims.exporter.reportdetail.VillaTemplateReportDetailAR;
import com.avanza.pims.exporter.reportdetail.VillaTemplateReportDetailEN;
import com.avanza.pims.exporter.reportdetail.criteria.VillaTemplateReportDetailCriteria;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.RegionView;

/**
 * 
 * Report Villa Template - XLS report 
 * @author Majid Hameed
 *
 */
public class ReportVillaTemplate extends AbstractController {

	private VillaTemplateReportDetailCriteria criteria;
	private Logger logger;

	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;

	private ListBasedExporterMeta exporterMeta = new ListBasedExporterMeta();

	private List<String> errorMessages;

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<SelectItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	public List<SelectItem> getCityList() {
		return cityList;

	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;

	}

	public ReportVillaTemplate() {
		logger = Logger.getLogger(ReportVillaTemplate.class);

		ReportDetail reportDetail = null;

		criteria = new VillaTemplateReportDetailCriteria();

		if (CommonUtil.getIsEnglishLocale()) {
			reportDetail = new VillaTemplateReportDetailEN();
		} else {
			reportDetail = new VillaTemplateReportDetailAR();
		}

		exporterMeta.setExportFileName("reportVillaTemplate");
		exporterMeta.setReportDetail(reportDetail);
		exporterMeta.setReportCriteria(criteria);
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public String cmdView_Click() {

		HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
		request.getSession().setAttribute(ReportConstant.Keys.REPORT_EXPORTER_META, exporterMeta);
		openPopup("openLoadReportPopup('" + ReportConstant.EXPORT_LIST_BASED_EXCEL_REPORT + "');");

		return null;
	}

	private void openPopup( String javaScriptText ) 
	{
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
		} 
		catch (Exception exception) 
		{
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		//loadState();
		loadCity();

		if (getSessionMap().get(WebConstants.ERROR) != null) {
			
			Integer errorCode =(Integer)getSessionMap().get(WebConstants.ERROR);
			
			if (ExceptionCodes.REPORT_NO_RECORDS_FOUND.equals(errorCode)) {
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Report.ERROR_NO_RECORD_FOUND));
			}
			
			getSessionMap().put(WebConstants.ERROR, null);
		}

		if (!isPostBack()) {
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);

		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}

	public void loadState() {

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			String selectedCountryName = "UAE";

			List<RegionView> regionViewList = psa.getCountryStates(
					selectedCountryName, getIsArabicLocale());

			for (int i = 0; i < regionViewList.size(); i++) {
				RegionView rV = (RegionView) regionViewList.get(i);
				SelectItem item;
				if (getIsEnglishLocale()) {
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				} else {
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
				}
				this.getStateList().add(item);
			}
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		} catch (Exception e) {
			logger.LogException("loadState() - crashed", e);

		}
	}

	@SuppressWarnings("unchecked")
	public void loadCity() {
		String methodName = "loadCity";
		logger.logInfo(methodName + "|" + "Start");
		PropertyServiceAgent psa = new PropertyServiceAgent();

		Long stateId = 20L; // for Dubai only

		try {

			Set<RegionView> regionViewList = null;

			regionViewList = psa.getCity(stateId);

			Iterator itrator = regionViewList.iterator();

			for (int i = 0; i < regionViewList.size(); i++) {
				RegionView rV = (RegionView) itrator.next();
				SelectItem item;
				if (getIsEnglishLocale()) {
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				} else {
					item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
				}
				this.getCityList().add(item);
			}

			Collections.sort(cityList, ListComparator.LIST_COMPARE);
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("city", cityList);
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);

		}

	}

	public VillaTemplateReportDetailCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(VillaTemplateReportDetailCriteria criteria) {
		this.criteria = criteria;
	}

}
