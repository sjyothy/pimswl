package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.Logger;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.IReportCriteria;
import com.avanza.pims.report.manager.DefaultReportManager;
import com.avanza.pims.report.manager.IReportManager;
import com.avanza.pims.report.util.JRCHelperSample;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.crystaldecisions.reports.sdk.ReportClientDocument;

public class PrintReportManageBacking extends AbstractController 
{	
	private static final long serialVersionUID = 8893070478149599937L;
	
	private Logger logger;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private String printer;
	private String noOfCopies;
	
	public PrintReportManageBacking() 
	{
		logger = Logger.getLogger( this.getClass() );
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		printer = "";
		noOfCopies = "1";
	}
	
	public String onPrint()
	{
		try
		{	
			logger.logInfo(" --- Started --- ");
			HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			IReportCriteria reportCriteria = (IReportCriteria) httpServletRequest.getSession().getAttribute( ReportConstant.Keys.REPORT_CRITERIA_KEY );
			IReportManager reportManager = new DefaultReportManager( reportCriteria );
			reportManager.getReportSource();
			ReportClientDocument rptClientDocument = (ReportClientDocument) reportManager.getReportClientDocument();			
			JRCHelperSample.printToServer( rptClientDocument, printer, Integer.parseInt(noOfCopies), "PIMS Application | " + reportCriteria.getReportFileName() );
			rptClientDocument.close();
			successMessages.add( CommonUtil.getParamBundleMessage("commons.print.success", printer) );
			logger.logInfo(" --- Successfully Completed --- "); 
		}
		catch ( Exception e )
		{
			logger.LogException(" --- Exception --- ", e);
			errorMessages.add( CommonUtil.getParamBundleMessage("commons.print.error", printer)  );
		}	
		
		return null;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPrinter() {
		return printer;
	}

	public void setPrinter(String printer) {
		this.printer = printer;
	}

	public String getNoOfCopies() {
		return noOfCopies;
	}

	public void setNoOfCopies(String noOfCopies) {
		this.noOfCopies = noOfCopies;
	}
}
