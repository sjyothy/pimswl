package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.soap.rpc.RPCConstants;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.ui.util.ResourceUtil;


public class ProjectMilestonesTabBacking extends AbstractController {

	private static Logger logger = Logger.getLogger(ProjectMilestonesTabBacking.class);
	
	private HtmlDataTable dataTable = new HtmlDataTable();
	private List<ProjectMileStoneView> projectMilestoneList = new ArrayList<ProjectMileStoneView>();
	
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	private ProjectServiceAgent projectServiceAgent = new ProjectServiceAgent();
	
	public ProjectMilestonesTabBacking (){
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
   	 	if(!isPostBack()){
   	 		
   	 	}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}
	
	public void deleteMilestone(ActionEvent evt){
		logger.logInfo("deleteMilestone() started...");
		try {
			ProjectMileStoneView removeditem = (ProjectMileStoneView) dataTable.getRowData();
			Long milestoneId = removeditem.getProjectMileStoneId();
			Boolean success = true;
			if(milestoneId!=null)
				success = projectServiceAgent.deleteProjectMilestone(milestoneId, CommonUtil.getLoggedInUser());
	   		if(success){
	   			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ProjectMilestone.MSG_MILESTONE_DELETE_SUCCESS);
	   			viewMap.put("infoMessage", infoMessage);
	   			int index = dataTable.getRowIndex();
	   			projectMilestoneList = getProjectMilestoneList();
	   			ProjectMileStoneView temp = projectMilestoneList.remove(index);
	   			viewMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, projectMilestoneList);
	   			viewMap.put("recordSize", projectMilestoneList.size());
	   		}
	   		else
	   			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_DELETE_FAILURE));
	   		
			logger.logInfo("deleteMilestone() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("deleteMilestone() crashed ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_DELETE_FAILURE));
		}
		finally{
			viewMap.put("errorMessages", errorMessages);
		}
	}
	
	public void editMilestone(ActionEvent evt){
		logger.logInfo("editMilestone() started...");
		try {
			ProjectMileStoneView editItem = (ProjectMileStoneView) dataTable.getRowData();
			
			FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showMilestonePopup();";
	        int index = dataTable.getRowIndex();
	        viewMap.put("editMilestoneIndex", Integer.valueOf(index));
	        sessionMap.put("projectId", editItem.getProjectId());
	        sessionMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW, editItem);
	        sessionMap.put(WebConstants.ProjectMilestone.AddPopup.PAGE_MODE, WebConstants.ProjectMilestone.AddPopup.PAGE_MODE_EDIT);
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        
			logger.logInfo("editMilestone() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editMilestone() crashed ", exception);
//			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_DOC_DELETE_FAILURE);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void openMilstonePopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openMilstonePopup() started...");
		try
		{
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showMilestonePopup();";
	        
	        /*Long percentage = 0L;
	        List<ProjectMileStoneView> temp = getProjectMilestoneList();
	        for(ProjectMileStoneView projectMileStoneView: temp){
	        	percentage+= projectMileStoneView.getCompletionPercentage();
	        }
	        percentage = 100 - percentage;
	        sessionMap.put("validPercentage", percentage);*/
	        sessionMap.put(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST, getProjectMilestoneList());
	        sessionMap.put(WebConstants.ProjectMilestone.AddPopup.PAGE_MODE, WebConstants.ProjectMilestone.AddPopup.PAGE_MODE_ADD);
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openMilstonePopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openMilstonePopup() crashed ", exception);
		}
    }
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public List<ProjectMileStoneView> getProjectMilestoneList() {
		projectMilestoneList = (List<ProjectMileStoneView>)viewMap.get(WebConstants.ProjectMilestone.PROJECT_MILESTONE_VIEW_LIST);
		if(projectMilestoneList==null)
			projectMilestoneList = new ArrayList<ProjectMileStoneView>();
		viewMap.put("recordSize", projectMilestoneList.size());
		return projectMilestoneList;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	public Boolean getCanAddMilestone(){
		Boolean canAddMilestone = (Boolean)viewMap.get("canAddMilestone");
		if(canAddMilestone==null)
			canAddMilestone = false;
		return canAddMilestone;
	}
	
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}
	
}
