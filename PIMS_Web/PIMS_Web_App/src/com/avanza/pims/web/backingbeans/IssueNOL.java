package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.proxy.pimsnolbpe.PIMSNOLBPELPortClient;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.NOLCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.CLRequest.PageOutcomes;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.CommercialActivityView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractActivityView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FeeConfigurationDetailView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.NOLTypeView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class IssueNOL extends AbstractController {
	ServletContext servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

	transient Logger logger = Logger.getLogger(IssueNOL.class);
	private HtmlInputText contractNoText = new HtmlInputText();
	private HtmlCommandButton contractButton = new HtmlCommandButton();
	private HtmlCommandButton tenantButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	
	private boolean isContractCommercial;
	private HtmlDataTable commercialActivityDataTable;
	org.richfaces.component.html.HtmlTab tabCommercialActivity=new org.richfaces.component.html.HtmlTab();
	private List<CommercialActivityView> commercialActivityList = new ArrayList<CommercialActivityView>();
	private CommercialActivityView commercialActivityItem = new CommercialActivityView();
	private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
	
	private HtmlTabPanel richTabPanel = new HtmlTabPanel();
	boolean isArabicLocale;
	boolean isEnglishLocale;
	private HtmlCommandButton feeButton = new HtmlCommandButton();
	private HtmlCommandButton sendForApprovalButton = new HtmlCommandButton();
	private int counter = 0;
	private int populationContract = 0;
	private int counterDetail = 0;
	private HtmlInputHidden isApprovalMode = new HtmlInputHidden();
	private HtmlInputHidden isTechnicalMode = new HtmlInputHidden();
	private HtmlInputText tenantNameText = new HtmlInputText();
	private HtmlInputText contractTypeText = new HtmlInputText();
	private HtmlInputText contractStartDateText = new HtmlInputText();
	private HtmlInputText contractEndDateText = new HtmlInputText();
	
	private HtmlInputText tenantNumberType = new HtmlInputText();
	private HtmlInputText contractStatusText = new HtmlInputText();
	private HtmlInputText totalContractValText = new HtmlInputText();
	private HtmlInputText txtUnitType = new HtmlInputText();
	private HtmlInputText txtpropertyName = new HtmlInputText();
	private HtmlInputText txtpropertyType = new HtmlInputText();
	private HtmlInputText txtunitRefNum = new HtmlInputText();
	private HtmlInputText txtUnitRentValue = new HtmlInputText();
	private String  selectOneRequestPriority;
	private HtmlSelectOneMenu cmbRequestPriority = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu nolTypeSelectOneMenu = new HtmlSelectOneMenu();
	private HtmlInputText nolTypeSize = new HtmlInputText();
	private String strNOLTypeSize;
	private String nolType;
	private HtmlInputHidden nolTypeHidden = new HtmlInputHidden();

	private HtmlInputTextarea reasontForNol = new HtmlInputTextarea();
	private List<SelectItem> nolTypeItems = new ArrayList<SelectItem> ();
	private List<String> messages = new ArrayList<String> ();	
	private HtmlInputText feeText = new HtmlInputText();
	private HtmlCommandButton populateContract = new HtmlCommandButton();
	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton sendButton = new HtmlCommandButton();
	private HtmlCommandButton sendButtonWithoutTechComments = new HtmlCommandButton();
	private HtmlCommandButton attachmentButton = new HtmlCommandButton();
	private HtmlCommandButton approveButton = new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	private HtmlCommandButton printButton = new HtmlCommandButton();
	private HtmlCommandButton printNol = new HtmlCommandButton();
	
	private HtmlCommandButton completeRequest = new HtmlCommandButton();
	private HtmlCommandButton cancelRequest = new HtmlCommandButton();
	private String requestId;
	
	private Long contractIdForNavigationToLeaseContract;
	
	private List<String> successMessages;
	private List<String> errorMessages;
	
	private String APPLICATION_TAB = "applicationTab";
	private String CONTRACT_TAB = "contractTab";
	private String NOL_TAB = "NOLTab";
	private String PAYMENT_TAB = "PaymentTab";
	private String ATTACHMENT_TAB = "attachmentTab";
	private String COMMERCIAL_ACTIVITY_TAB = "tabCommercialActivity";
	
	private String SelectedTab;
	protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    protected String txtRemarks;
    private Boolean editPaymentEnable;
    NOLCriteria NOLReportCriteria;

	ResourceBundle bundle = 
		ResourceBundle.getBundle(getFacesContext().getApplication()
				.getMessageBundle(), getFacesContext().getViewRoot().getLocale());
	SystemParameters parameters = SystemParameters.getInstance();
	DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));	
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	ContractView contractView = new ContractView();
	FeeConfigurationView feeConfigurationView = new FeeConfigurationView();
	ResourceBundle resourceBundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());
	private String screenName;
	private Boolean renderFee =false;
	private boolean renderSize=false;
	CommonUtil commUtil = new CommonUtil();
	NOLTypeView nolView = new NOLTypeView();	
    //**********************hdn variable from shiraz *******************************
	
	private String hdnContractId;
	private String hdnContractNumber;
	private String hdnTenantId;
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String strNolTypeId;
	private String hdnSendStatus;
	
   //*******************************************************************************
	
	//********** payment Tab Variable********************************
	
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	private HtmlDataTable dataTablePaymentSchedule;
	public boolean statusIsNotSentForApprovalAndNotApproved;
	private String dateFormatForDataTable;
	FacesContext context = FacesContext.getCurrentInstance();
	private Boolean requestStatusForColumn;
	
	private double totalRentAmount;
	private double totalCashAmount;
	private double totalChqAmount;
	private int installments;
	private double totalDepositAmount;
	private double totalFees;
	private double totalFines;
	
	
	//***************************************************************

	private List<ContractActivityView> contractActivityList = new ArrayList<ContractActivityView>();
	private List<CommercialActivityView> commActViewListforRequest = new ArrayList<CommercialActivityView>();
	
	private final String noteOwner = WebConstants.REQUEST;//WebConstants.PROCEDURE_TYPE_NO_OBJECTION_LETTER;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_NOL;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_NO_OBJECTION_LETTER;
	
	
	public String getSelectOneRequestPriority() {
		return selectOneRequestPriority;
	}
	public void setSelectOneRequestPriority(String selectOneRequestPriority) {
		this.selectOneRequestPriority = selectOneRequestPriority;
	}
	public HtmlSelectOneMenu getCmbRequestPriority() {
		return cmbRequestPriority;
	}
	public void setCmbRequestPriority(HtmlSelectOneMenu cmbRequestPriority) {
		this.cmbRequestPriority = cmbRequestPriority;
	}
	@SuppressWarnings("unchecked")
	public void init()
	{
		if(
	     		sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
	      )
	    {
	     	   viewRootMap.put(
	     			   			WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, 
	     			   			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)
	     			   		  );
	     	   setPaymentSchedules(
	     			   					(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)
	     			   			  );
	     	   	List<PaymentScheduleView> paymentSecList = (List<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
	     	    if(
	     	    		paymentSecList != null && 
	     	    		paymentSecList.size() > 0  
	     	      )
	     	    {
	     	    	viewRootMap.put("PAYMENT_SCH_FOR_EDIT", paymentSecList);
	     	    }
	    
	    }
		
		List<RequestKeyView> requestKeys;
		requestStatusForColumn = false;
		
		
 		RequestFieldDetailView requestFieldDetailViewNew = new RequestFieldDetailView();
		
	
		try{
			requestKeys = new RequestServiceAgent().getRequestKeys(WebConstants.NOL.PROCEDURE_TYPE_NO_OBJECTION_LETTER);
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner,getStrNolTypeId());
			
			if(!isPostBack())
			{
				
				viewRootMap.put("canAddAttachment",true);
				viewRootMap.put("canAddNote", true);
				
				tabCommercialActivity.setRendered(false);
				saveButton.setRendered(false);
				cancelRequest.setRendered(false);
				completeRequest.setRendered(false);
				
				printNol.setRendered(true);
				printButton.setRendered(false);
				sendForApprovalButton.setRendered(false);
				rejectButton.setRendered(false);
				approveButton.setRendered(false);
				sendButton.setRendered(false);
				sendButtonWithoutTechComments.setRendered(false);
				populateContract.setRendered(true);
				btnCollectPayment.setRendered(false);
				setEditPaymentEnable(false);
				
				sendForApprovalButton.setValue(resourceBundle.getString("commons.sendForApproval"));
				attachmentButton.setValue(resourceBundle.getString("commons.attachment"));
				cancelButton.setValue(resourceBundle.getString("commons.cancel"));
				sendButton.setValue(resourceBundle.getString("commons.sendForInspection"));
				sendButtonWithoutTechComments.setValue(resourceBundle.getString("commons.sendForApproval"));
				feeButton.setValue(resourceBundle.getString("commons.Pay"));
				approveButton.setValue(resourceBundle.getString("commons.approve"));
				printButton.setValue(resourceBundle.getString("commons.print"));
				rejectButton.setValue(resourceBundle.getString("commons.reject"));
				completeRequest.setValue(ResourceUtil.getInstance().getProperty("common.completeRequest"));
				cancelRequest.setValue(ResourceUtil.getInstance().getProperty("common.cancelRequest"));
				viewRootMap.put("selectedTab",APPLICATION_TAB);
				tbl_Action.setRendered(false);
				if(getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW)!=null)
				{
					viewRootMap.put(WebConstants.Contract.CONTRACT_VIEW, getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW));
					//when comming from contract Search
					setEditPaymentEnable(true);
					sendButton.setRendered(false);
					sendButtonWithoutTechComments.setRendered(false);
					
					saveButton.setRendered(true);
					populateContract.setRendered(false);
					viewRootMap.put("selectedTab",CONTRACT_TAB);
					viewRootMap.put("FROMCONTRACTSEARCH",true);
					contractView = (ContractView)viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW);
					hdnContractId = contractView.getContractId().toString();
					cmbRequestPriority.setValue( String.valueOf( WebConstants.REQUEST_PRIORITY_NORMAL_ID ));
					getContractById(Long.parseLong(hdnContractId));
				    //contractView = propertyServiceAgent.getContract(contractView.getContractNumber(),dateFormat);
					HashMap contractHashMap=new HashMap();
					ContractView cv=new ContractView();
					cv.setContractId(contractView.getContractId());
					contractHashMap.put("contractView",cv);
				    List<ContractView> contractViewList = propertyServiceAgent.getAllContracts(contractHashMap);
				    contractView = contractViewList.get(0);
					//Setting the Contract/Tenant Info
					contractNoText.setValue(contractView.getContractNumber());
					setTenantName(contractView);					
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					    txtUnitRentValue.setValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					}
					if(getIsEnglishLocale())
						contractStatusText.setValue(contractView.getStatusEn());
					else
						contractStatusText.setValue(contractView.getStatusAr());
				    	totalContractValText.setValue(contractView.getRentAmount());
			    }
				// when comming from request search 
				if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().containsKey(WebConstants.REQUEST_VIEW))
				{
					setEditPaymentEnable(true);
					populateContract.setRendered(false);
					sendButtonWithoutTechComments.setRendered(true);
					sendButton.setRendered(false);
					saveButton.setRendered(true);
					nolTypeSelectOneMenu.setDisabled(true);
					cmbRequestPriority.setDisabled(true);
					viewRootMap.put("canAddAttachment",true);
					viewRootMap.put("canAddNote", true);
					//viewRootMap.put("NOLTabReadOnlyMode","READONLY");
					viewRootMap.put("selectedTab",CONTRACT_TAB);
					viewRootMap.put("FROMREQUESTSEARCH",true);
				    RequestView requestView = 	(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
					this.requestId=requestView.getRequestId().toString();
					populateApplicationDataTab(this.requestId);
					List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
					String contractNo = "";
					if(requestList.size()>0)
					{
						requestView = (RequestView)requestList.iterator().next();
						viewRootMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
						contractNo = requestView.getContractView().getContractNumber();
						requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
						//requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
						viewRootMap.put("NOL_TYPE_FOR_EDIT", requestFieldDetailViewNew);
						setStrNolTypeId(requestFieldDetailViewNew.getRequestKeyValue());
						nolView.setNolTypeId(Long.parseLong(requestFieldDetailViewNew.getRequestKeyValue()));
						viewRootMap.put("NOL_VIEW", requestFieldDetailViewNew.getRequestKeyValue());
						if(!(nolView.getNolTypeId()==11L))
			    		tabCommercialActivity.setRendered(false);
			    	    else 
			    		tabCommercialActivity.setRendered(true);
						paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(Long.parseLong(requestId));
						if(paymentSchedules!=null && paymentSchedules.size()>0)
						{
						 viewRootMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
						 setPaymentSchedules(paymentSchedules);
						}
						else
						 viewRootMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
					}

					requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
					//requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
					viewRootMap.put("NOL_REASON_FOR_EDIT", requestFieldDetailViewNew);
					reasontForNol.setValue(requestFieldDetailViewNew.getRequestKeyValue());
					if(requestView.getRequestPriorityId() != null )
					{
						cmbRequestPriority.setValue(requestView.getRequestPriorityId().toString());
					}
					else
					{
						cmbRequestPriority.setValue( String.valueOf( WebConstants.REQUEST_PRIORITY_NORMAL_ID ));
					}
					ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
					contractNoText.setValue(contractNo);
					populateContract();
					contractActivityList = new RequestServiceAgent().getContractActivitiesByRequestId(Long.parseLong(requestId));
					if(contractActivityList!=null && contractActivityList.size()>0)
					{
						setCommercialActivityList(getAllCommercialActivities());
						for(ContractActivityView conActView: contractActivityList)
						{
						 	for(CommercialActivityView commActView:getCommercialActivityList())
						 	{
						 		if(conActView.getCommercialActivityId().compareTo(commActView.getCommercialActivityId())==0)
						 		{
						 			commActView.setIsSelected(true);
						 			commActView.setIsDisabled(true);
						 			commActView.setContractActivityId(conActView.getContractActivityId());
						 			commActView.setCreatedBy(conActView.getCreatedBy());
						 			commActView.setCreatedOn(conActView.getCreatedOn());
						 			commActView.setIsDeleted(conActView.getIsDeleted());
						 			commActView.setRecordStatus(conActView.getRecordStatus());
						 			commActViewListforRequest.add(commActView);
						 		}else
						 		{
						 			commActView.setIsDisabled(true);
						 			commActViewListforRequest.add(commActView);
						 		}
						 		
						 	}
					     }
					
					    setCommercialActivityList(commActViewListforRequest);
					}
				   CommonUtil.loadAttachmentsAndComments(requestId);
					if(requestView.getStatusId()!=null )
					{
						DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
								WebConstants.REQUEST_STATUS_NEW);
						
						if(!requestView.getStatusId().equals(ddv.getDomainDataId()))
						{
							sendButtonWithoutTechComments.setRendered(false);
							sendButton.setRendered(false);
							saveButton.setRendered(false);
							setEditPaymentEnable(false);
							
							viewRootMap.put("canAddAttachment",false);
							viewRootMap.put("canAddNote", false);		
						}
					}
				}

				

			if(getFacesContext().getExternalContext().getRequestMap().get("PAID")==null)
			{
			UserTask task = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			if(task !=null){
			viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK));
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			}
			
			
     		
			if(task!=null && task.getTaskType().equals(WebConstants.NOL.ProvideTechnicalComments))
			{
				
				populateContract.setRendered(false);
				sendForApprovalButton.setRendered(true);
				viewRootMap.put("canAddAttachment",true);
				viewRootMap.put("canAddNote", true);
				viewRootMap.put("NOLTabReadOnlyMode","READONLY");
				viewRootMap.put("selectedTab",CONTRACT_TAB);
				tbl_Action.setRendered(true);
				Map taskAttributes =  task.getTaskAttributes();
				
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				RequestView requestView = new RequestView();
				requestView.setRequestId(requestId);
				
				this.requestId=requestId.toString();
				populateApplicationDataTab(this.requestId);
				
				List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
				String contractNo = "";
                   
				
				if(requestList.size()>0){
					requestView = (RequestView)requestList.iterator().next();
					viewRootMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
					contractNo = requestView.getContractView().getContractNumber();
				
					requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
					viewRootMap.put("NOL_TYPE_FOR_EDIT", requestFieldDetailViewNew);
					
					setStrNolTypeId(requestFieldDetailViewNew.getRequestKeyValue());
					nolView.setNolTypeId(Long.parseLong(requestFieldDetailViewNew.getRequestKeyValue()));
					viewRootMap.put("NOL_VIEW", requestFieldDetailViewNew.getRequestKeyValue());
					
					if(!(nolView.getNolTypeId()==11L))
		    		tabCommercialActivity.setRendered(false);
		    	    else 
		    		tabCommercialActivity.setRendered(true);
		    		
					
					paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
					if(paymentSchedules!=null && paymentSchedules.size()>0)
					{
					 viewRootMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
					 setPaymentSchedules(paymentSchedules);
					}else
					{
					 viewRootMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
					}
		            
		                               
				}

				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
				viewRootMap.put("NOL_REASON_FOR_EDIT", requestFieldDetailViewNew);
				
				reasontForNol.setValue(requestFieldDetailViewNew.getRequestKeyValue());
				
				ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
				contractNoText.setValue(contractNo);
				populateContract();
				
				contractActivityList = new RequestServiceAgent().getContractActivitiesByRequestId(requestId);
				if(contractActivityList!=null && contractActivityList.size()>0)
				{
					setCommercialActivityList(getAllCommercialActivities());
					for(ContractActivityView conActView: contractActivityList)
					{
					 	for(CommercialActivityView commActView:getCommercialActivityList())
					 	{
					 		if(conActView.getCommercialActivityId().compareTo(commActView.getCommercialActivityId())==0)
					 		{
					 			commActView.setIsSelected(true);
					 			commActView.setIsDisabled(true);
					 			commActView.setContractActivityId(conActView.getContractActivityId());
					 			commActView.setCreatedBy(conActView.getCreatedBy());
					 			commActView.setCreatedOn(conActView.getCreatedOn());
					 			commActView.setIsDeleted(conActView.getIsDeleted());
					 			commActView.setRecordStatus(conActView.getRecordStatus());
					 			commActViewListforRequest.add(commActView);
					 		}else
					 		{
					 			commActView.setIsDisabled(true);
					 			commActViewListforRequest.add(commActView);
					 		}
					 		
					 	}
				     }
				
				    setCommercialActivityList(commActViewListforRequest);
				}
				
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				
				
				getFacesContext().getExternalContext().getSessionMap().put("NEW_REQUEST",false);
				isApprovalMode.setValue(false);
				isTechnicalMode.setValue(true);
				
			}
			if(task!=null && task.getTaskType().equals(WebConstants.NOL.ApproveNOLRequest))
			{
				populateContract.setRendered(false);
				approveButton.setRendered(true);
				sendButton.setRendered(true);
				rejectButton.setRendered(true);
				viewRootMap.put("canAddAttachment",true);
				viewRootMap.put("canAddNote", true);
				printNol.setRendered(true);
				viewRootMap.put("NOLTabReadOnlyMode","READONLY");
				viewRootMap.put("selectedTab",APPLICATION_TAB);
				
				Map taskAttributes =  task.getTaskAttributes();
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				RequestView requestView = new RequestView();
				requestView.setRequestId(requestId);
				ContractView contractView = new ContractView();
				
				this.requestId=requestId.toString();
				populateApplicationDataTab(this.requestId);
				
				List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
				requestView = requestList.get(0);
				contractView = requestView.getContractView();
				if(requestView.getRequestPriorityId() != null )
				{
					cmbRequestPriority.setValue(requestView.getRequestPriorityId().toString());
				}
				else
				{
					cmbRequestPriority.setValue( String.valueOf( WebConstants.REQUEST_PRIORITY_NORMAL_ID ));
				}
				ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
				contractNoText.setValue(contractView.getContractNumber());
				
				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
				
				getContractById(contractView.getContractId());
				
				setStrNolTypeId(requestFieldDetailViewNew.getRequestKeyValue());
				nolView.setNolTypeId(Long.parseLong(requestFieldDetailViewNew.getRequestKeyValue()));
				
				if(!(nolView.getNolTypeId()==11L))
		    		tabCommercialActivity.setRendered(false);
		    	    else 
		    		tabCommercialActivity.setRendered(true);
		    		
					
					paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
					if(paymentSchedules!=null && paymentSchedules.size()>0)
					{
					 viewRootMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
					 viewRootMap.put("CANCEL_PAYMENT_SCH", true);
					 setPaymentSchedules(paymentSchedules);
					}else
					{
					 viewRootMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
					}
				
					contractActivityList = new RequestServiceAgent().getContractActivitiesByRequestId(requestId);
					if(contractActivityList!=null && contractActivityList.size()>0)
					{
						setCommercialActivityList(getAllCommercialActivities());
						for(ContractActivityView conActView: contractActivityList)
						{
						 	for(CommercialActivityView commActView:getCommercialActivityList())
						 	{
						 		if(conActView.getCommercialActivityId().compareTo(commActView.getCommercialActivityId())==0)
						 		{
						 			commActView.setIsSelected(true);
						 			commActView.setIsDisabled(true);
						 			commActView.setContractActivityId(conActView.getContractActivityId());
						 			commActView.setCreatedBy(conActView.getCreatedBy());
						 			commActView.setCreatedOn(conActView.getCreatedOn());
						 			commActView.setIsDeleted(conActView.getIsDeleted());
						 			commActView.setRecordStatus(conActView.getRecordStatus());
						 			commActViewListforRequest.add(commActView);
						 		}else
						 		{
						 			commActView.setIsDisabled(true);
						 			commActViewListforRequest.add(commActView);
						 		}
						 		
						 	}
					     }
					
					    setCommercialActivityList(commActViewListforRequest);
					}	
				
				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
			    reasontForNol.setValue(requestFieldDetailViewNew.getRequestKeyValue());
				
	            

	            
				populateContract();
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				isApprovalMode.setValue(true);
				isTechnicalMode.setValue(false);
				getFacesContext().getExternalContext().getSessionMap().put("NEW_REQUEST",false);
			}
			
			if(task!=null && task.getTaskType().equals(WebConstants.NOL.CollectNOLRequestPayment))
			{
				populateContract.setRendered(false);
				completeRequest.setRendered(true);
				cancelRequest.setRendered(true);
				requestStatusForColumn = true;
				cmbRequestPriority.setDisabled(true);
				btnCollectPayment.setRendered(true);
				viewRootMap.put("canAddAttachment",false);
				viewRootMap.put("canAddNote", false);
				viewRootMap.put("NOLTabReadOnlyMode","READONLY");
				printNol.setRendered(true);
				viewRootMap.put("selectedTab",PAYMENT_TAB);
				
				Map taskAttributes =  task.getTaskAttributes();
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				RequestView requestView = new RequestView();
				requestView.setRequestId(requestId);
				
				this.requestId=requestId.toString();
				populateApplicationDataTab(this.requestId);
				
				List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
				requestView = requestList.get(0);
				String contractNo = "";
				contractNo = requestView.getContractView().getContractNumber();
				
				

				if(requestView.getRequestPriorityId() != null )
				{
					cmbRequestPriority.setValue(requestView.getRequestPriorityId().toString());
				}
				else
				{
					cmbRequestPriority.setValue( String.valueOf( WebConstants.REQUEST_PRIORITY_NORMAL_ID ));
				}
				ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
				
				contractNoText.setValue(contractNo);
				hdnContractId = requestView.getContractView().getContractId().toString();
				
				
				viewRootMap.put(WebConstants.REQUEST_VIEW, requestView);
				
				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
				
				setStrNolTypeId(requestFieldDetailViewNew.getRequestKeyValue());
				nolView.setNolTypeId(Long.parseLong(requestFieldDetailViewNew.getRequestKeyValue()));
				
				if(!(nolView.getNolTypeId()==11L))
		    		tabCommercialActivity.setRendered(false);
		    	    else 
		    		tabCommercialActivity.setRendered(true);
				
				contractActivityList = new RequestServiceAgent().getContractActivitiesByRequestId(requestId);
				if(contractActivityList!=null && contractActivityList.size()>0)
				{
					setCommercialActivityList(getAllCommercialActivities());
					for(ContractActivityView conActView: contractActivityList)
					{
					 	for(CommercialActivityView commActView:getCommercialActivityList())
					 	{
					 		if(conActView.getCommercialActivityId().compareTo(commActView.getCommercialActivityId())==0)
					 		{
					 			commActView.setIsSelected(true);
					 			commActView.setIsDisabled(true);
					 			commActView.setContractActivityId(conActView.getContractActivityId());
					 			commActView.setCreatedBy(conActView.getCreatedBy());
					 			commActView.setCreatedOn(conActView.getCreatedOn());
					 			commActView.setIsDeleted(conActView.getIsDeleted());
					 			commActView.setRecordStatus(conActView.getRecordStatus());
					 			commActViewListforRequest.add(commActView);
					 		}else
					 		{
					 			commActView.setIsDisabled(true);
					 			commActViewListforRequest.add(commActView);
					 		}
					 		
					 	}
				     }
				
				    setCommercialActivityList(commActViewListforRequest);
				}	
				
				paymentSchedules = propertyServiceAgent.getContractPaymentSchedule(null,requestView.getRequestId());
				setPaymentSchedules(paymentSchedules);
				
				
				DomainDataView ddv= CommonUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
		     		       WebConstants.PAYMENT_SCHEDULE_COLLECTED);	
				DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
		                WebConstants.REALIZED);

				requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
			    reasontForNol.setValue(requestFieldDetailViewNew.getRequestKeyValue());
	            
				populateContract();
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				
				isApprovalMode.setValue(true);
				isTechnicalMode.setValue(false);
				cancelRequest.setRendered(true);
				completeRequest.setRendered(true);
				approveButton.setRendered(false);
				sendButton.setRendered(false);
				rejectButton.setRendered(false);
				requestStatusForColumn = true;
				btnCollectPayment.setRendered(true);
				
				if(   paymentSchedules.size() > 0 &&
					( ddv.getDomainDataId().equals( paymentSchedules.get( 0 ).getStatusId() ) ||
					  ddvRealized.getDomainDataId().equals( paymentSchedules.get( 0 ).getStatusId() )
					 )
				  )
				  {
					sessionMap.put("PAYMENT_DONE", true);
					btnCollectPayment.setRendered(false);
					completeRequest.setRendered(true);
					cancelRequest.setRendered(false);
					printNol.setRendered(true);
				}
				
				if(!(paymentSchedules.size()>0)){
					sessionMap.put("PAYMENT_DONE", true);
					btnCollectPayment.setRendered(false);
					completeRequest.setRendered(true);
					cancelRequest.setRendered(true);
					printNol.setRendered(true);
				}
				
				
				getFacesContext().getExternalContext().getSessionMap().put("NEW_REQUEST",false);
			}
		}
	}		
	else
		viewRootMap.put("selectedTab","");	
		  
			if(viewRootMap.get(WebConstants.NOL.NOL_TYPE_LIST)!=null)
				nolTypeItems = (List<SelectItem>) viewRootMap.get(WebConstants.NOL.NOL_TYPE_LIST);
			else
				getNolTypes(WebConstants.NO_OBJECTION_LETTER_REQUEST_TYPE);
					
  }
  catch(Exception e)
  {
			logger.LogException( "crashed...", e);

		} 	


	}
	private void emptyFields(){

		if(counter==0){
			tenantNameText.setValue("");
			contractTypeText.setValue("");
			contractStartDateText.setValue("");
			contractEndDateText.setValue("");
			populateContract.setRendered(true);
			sendButton.setRendered(false);
			sendButtonWithoutTechComments.setRendered(false);
		}
		else
			populateContract.setRendered(false);
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+1024+',height='+450+',left=0,top=40,scrollbars=yes,status=yes');";
             if(extraJavaScript !=null && extraJavaScript.trim().length()>0)
             {
            	 javaScriptText="";
            	 javaScriptText=extraJavaScript;
             }
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		return "";
	}

	@SuppressWarnings("unchecked")
	public Map getIdFromType(List<DomainDataView> ddv ,String type)throws Exception
	{
		
		Map hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
		
				hMap.put("returnId",domainDataView.getDomainDataId());
				hMap.put("IdEn",domainDataView.getDataDescEn());
				hMap.put("IdAr",domainDataView.getDataDescAr());
			}

		}
		return hMap;

	}


	//Iterates for all the domain Types	}


	@SuppressWarnings("unchecked")
	private void getNolTypes(String domainType)throws Exception
	{
		
			  if(viewRootMap.containsKey(WebConstants.NOL.NOL_TYPE_LIST)){return;}

			  UtilityServiceAgent usa = new UtilityServiceAgent();
			  List <NOLTypeView> nolTypeViewList = usa.getAllNOLTypes();
			  for(int i=0;i<nolTypeViewList.size();i++)
			  {
				  NOLTypeView rTV=(NOLTypeView)nolTypeViewList.get(i);
				  if(rTV.getNolReport()!= null && rTV.getNolReport().equals("PracticingActivity.rpt") && viewRootMap.containsKey("IS_CONTRACT_COMM"))
				  {	   
				     if (getIsEnglishLocale())
					 {
				    	 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionEn()));			  
					 }
					 else 
					 {
						 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionAr()));
					 }
				  }
				  else if(rTV.getNolReport()== null ||  !rTV.getNolReport().equals("PracticingActivity.rpt") )
				  {
					  
				     if (getIsEnglishLocale())
					 {
				    	 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionEn()));			  
					 }
					 else
					 {
						 nolTypeItems.add(new SelectItem(rTV.getNolTypeId().toString(),rTV.getDescriptionAr()));
					 }
				  }
			  
			  }
			 Collections.sort(nolTypeItems, ListComparator.LIST_COMPARE);
			 viewMap.put(WebConstants.NOL.NOL_TYPE_LIST, nolTypeItems);
	}
	
	@SuppressWarnings("unchecked")
	public void onRequestPriorityChanged(ValueChangeEvent valueChangeEvent)
	{
	     try
	     {
			List<PaymentScheduleView> listPayments = getPaymentSchedules();
			PaymentScheduleView urgentPayment = null;
			for (PaymentScheduleView payment : listPayments) 
			{
				if( payment.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_FEES_URGENT_REQUEST_ID ) == 0 )
				{
					if ( Long.valueOf(cmbRequestPriority.getValue().toString() ).compareTo(WebConstants.REQUEST_PRIORITY_NORMAL_ID  ) == 0 )
					{
						listPayments.remove(payment);
						break;
					}
					urgentPayment = payment;
					
				}
			}
			if( cmbRequestPriority.getValue() != null && 
				Long.valueOf(cmbRequestPriority.getValue().toString() ).compareTo(WebConstants.REQUEST_PRIORITY_URGENT_ID ) == 0 &&	
				urgentPayment == null 
			  )
			{
				urgentPayment = addUrgentFees();
				listPayments.add(urgentPayment);
				setPaymentSchedules(listPayments);
			}
	     }
	     catch (Exception e) 
		  {
		 		errorMessages = new ArrayList<String>(0);
		 		logger.LogException( "onRequestPriorityChanged|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		  }
	}
	@SuppressWarnings("unchecked")
	public void onNolTypeChanged(ValueChangeEvent valueChangeEvent)
	{
     try
     {
			 counter++;
			 String strComboValues = valueChangeEvent.getNewValue().toString();
			 Long nolTypeId =new Long(strComboValues);
			 nolView.setNolTypeId(nolTypeId);
			 nolType = strComboValues;
			 getFacesContext().getExternalContext().getSessionMap().put("NOL_TYPE",nolType);
			
		     if(nolTypeId!=null && !(nolTypeId==-1))
		     {
		    	viewRootMap.put("NOL_VIEW", nolTypeId);
		    	viewRootMap.put(WebConstants.Attachment.PROCEDURE_DOCS, new ArrayList<DocumentView>());
		    	CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner,nolTypeId.toString());
		    	((AttachmentBean)getBean("pages$attachment")).getProcedureDocs();
		    	if(!(nolTypeId==11L))
		    	{
		    		tabCommercialActivity.setRendered(false);
		    	}
		    	else 
		    	{
		    		tabCommercialActivity.setRendered(true);
		    		tabCommercialActivity_Click();
		    	}
		     }
		    ContractView cv =  (ContractView) viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW);
		    		
			if(
				cv!=null && 
				cv.getContractId() != null  
			  )
			{			
					paymentSchedules = propertyServiceAgent.getDefaultPaymentsForIssueNOL(
																							Long.parseLong(cv.getContractId().toString()),
																							nolView, 
																							getIsEnglishLocale()
																						 );
					if( cmbRequestPriority.getValue() != null && 
						Long.valueOf(cmbRequestPriority.getValue().toString() ).compareTo(WebConstants.REQUEST_PRIORITY_URGENT_ID ) == 0	
					   )
					{
						paymentSchedules.add(addUrgentFees());
							
					}
					setPaymentSchedules(paymentSchedules);
			}
	  }
	  catch (Exception e) 
	  {
	 		errorMessages = new ArrayList<String>(0);
	 		logger.LogException( "onSave|Error Occured", e);
	 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }
	 			

	}


	public String cancel(){
		
		if(viewRootMap.containsKey("FROMCONTRACTSEARCH"))
		{
			sessionMap.put("preserveSearchCriteria", true);
			return "contractSearch";
		}
		if(viewRootMap.containsKey("FROMREQUESTSEARCH"))
		{
			return "requestSearch";
		}
		return "";
		
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	
	@SuppressWarnings("unchecked")
	private Long getNOLRequestStatus() throws PimsBusinessException ,Exception
	{
		Long cleranceLetterRequestStatusId = null;
		Map hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),WebConstants.REQUEST_STATUS_NEW);
		if(hMap.containsKey("returnId"))
		{
			cleranceLetterRequestStatusId = Long.parseLong(hMap.get("returnId").toString());
			if(cleranceLetterRequestStatusId.equals(null))
				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		}
		return cleranceLetterRequestStatusId;
	}
	
	@SuppressWarnings("unchecked")
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)throws Exception
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	private Long getRequestType() throws PimsBusinessException {
		Long nolReqTypeId = null;
		String  METHOD_NAME = "getNOlRequestType|";		
		logger.logInfo(METHOD_NAME+"started...");
		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
		RequestTypeView requestTypeView = requestServiceAgent.getRequestType(WebConstants.NOL.REQUEST_TYPE_NOL_ID);


		nolReqTypeId= requestTypeView.getRequestTypeId();
		logger.logInfo(METHOD_NAME+"ended...");
		return nolReqTypeId;
	}
	
	
	public String reject(){
		counter++;
		String  METHOD_NAME = "reject|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		try {
			logger.logInfo( METHOD_NAME +"Start|" );
			UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
			logger.logInfo("Contextpath is:"+contextPath);
			String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);
			client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);

			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			this.requestId = requestId.toString();
		    new PropertyService().setPaymentScheduleCanceled(requestId);
			addPaymentSchedule(requestId);
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId,noteOwner);
			generateNotification(WebConstants.Notification_MetaEvents.Event_NOOBJECTIONLETTER_REQUESTREJECTED);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_REJECT_SUCCESS));
			NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_REJECTED, requestId);
			logger.logInfo(METHOD_NAME+" completed successfully...");
			
			rejectButton.setRendered(false);
			approveButton.setRendered(false);
			sendButton.setRendered(false);

		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException( "Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_REJECT_FAILURE));

		}
		catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_REJECT_FAILURE));
		}
		return "success";

	}
	@SuppressWarnings("unchecked")
	public void onSave()
	{
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		Long requestId = null;

		
	try
	{	
			populateApplicantDetails();

			if(!validate())     {return;}
			
	    	requestId = persistRequest();
	    	this.requestId = requestId.toString();
			addPaymentSchedule(requestId);
			addContractActivity();
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId,noteOwner);
			if(viewRootMap.containsKey("REQUEST_VIEW_FOR_EDIT"))
				NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_UPDATED, requestId);
			else
			    NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_SAVED, requestId);
			successMessages.add(CommonUtil.getBundleMessage("nolRequest.Save"));
			putRequestForEdit(requestId);
			populateContract.setRendered(false);
			sendButtonWithoutTechComments.setRendered(true);
			sendButton.setRendered(false);
			saveButton.setRendered(true);
			viewRootMap.put("canAddAttachment",true);
			viewRootMap.put("canAddNote", true);
			viewRootMap.put("selectedTab",CONTRACT_TAB);
	}
	catch (Exception e) 
	{
		errorMessages = new ArrayList<String>(0);
		logger.LogException( "onSave|Error Occured", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
			
			
	}
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void populateApplicantDetails()throws Exception
	{
		if(hdnPersonId!=null && !hdnPersonId.equals(""))
		{
		 viewRootMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
		 viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}

		if(hdnPersonName!=null && !hdnPersonName.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
		
		
		if(hdnCellNo!=null && !hdnCellNo.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
		
		if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
		{
			DomainDataView ddv = new DomainDataView();
			if(Long.parseLong(hdnIsCompany)==1L)
			{
			        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
			}
			else
			{
				ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);
			}
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
		
		}
	}
	
	@SuppressWarnings("unchecked")
	public Long persistRequest() throws Exception
	{
			
			RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
			PersonView applicantView  = new PersonView();
			HashSet<RequestFieldDetailView> reqFieldDetailsView = new HashSet<RequestFieldDetailView>();
			RequestFieldDetailView reqFieldFee = new RequestFieldDetailView();
			RequestFieldDetailView reqFieldReason = new RequestFieldDetailView();
			RequestView requestView = new RequestView();
			List<RequestKeyView> requestKeys = requestServiceAgent.getRequestKeys(WebConstants.NOL.PROCEDURE_TYPE_NO_OBJECTION_LETTER);
	        
			if(	viewRootMap.containsKey("REQUEST_VIEW_FOR_EDIT") )
			{
				RequestView reqViewEdit = 	(RequestView)viewRootMap.get("REQUEST_VIEW_FOR_EDIT");
				requestView =  reqViewEdit;
				if(cmbRequestPriority != null && cmbRequestPriority.getValue() != null )
				{
					requestView.setRequestPriorityId( Long.valueOf( cmbRequestPriority.getValue().toString()  ) );
				}
//				requestView.setRequestId(reqViewEdit.getRequestId());
//				requestView.setCreatedBy(reqViewEdit.getCreatedBy());
//				requestView.setCreatedOn(reqViewEdit.getCreatedOn());
				requestView.setUpdatedBy(getLoggedInUser());
				requestView.setUpdatedOn(new Date());
//				requestView.setRecordStatus(reqViewEdit.getRecordStatus());
//				requestView.setIsDeleted(reqViewEdit.getIsDeleted());
//				requestView.setRequestDate(reqViewEdit.getRequestDate());
//				requestView.setStatusId(reqViewEdit.getStatusId());
//				requestView.setRequestTypeId(reqViewEdit.getRequestTypeId());
//				requestView.setContractId(reqViewEdit.getContractId());
//				requestView.setApplicantView(reqViewEdit.getApplicantView());
//				requestView.setDescription(reqViewEdit.getDescription());
			
			}
			else
			{
				requestView.setCreatedBy(getLoggedInUser());
				requestView.setCreatedOn(new Date());
				requestView.setUpdatedBy(getLoggedInUser());
				requestView.setUpdatedOn(new Date());
				requestView.setRecordStatus(new Long(1));
				requestView.setIsDeleted(new Long(0));
				requestView.setRequestDate(new Date());
				requestView.setStatusId(getNOLRequestStatus());
				requestView.setRequestTypeId(getRequestType());
				if(cmbRequestPriority != null && cmbRequestPriority.getValue() != null )
				{
					requestView.setRequestPriorityId( Long.valueOf( cmbRequestPriority.getValue().toString()  ) );
				}
				ContractView cv =  (ContractView) viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW);
				requestView.setContractId(cv.getContractId());
				
				applicantView.setPersonId(Long.parseLong(viewRootMap.get(WebConstants.LOCAL_PERSON_ID).toString()));
				requestView.setApplicantView(applicantView);
					
					if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
					{
					 requestView.setDescription((String)viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION));
					}
			
			}
			if(viewRootMap.containsKey("NOL_TYPE_FOR_EDIT"))
			{	
				RequestFieldDetailView requestFieldDetailView = (RequestFieldDetailView)viewRootMap.get("NOL_TYPE_FOR_EDIT");
				reqFieldFee.setRequestFieldDetailViewId(requestFieldDetailView.getRequestFieldDetailViewId());
				reqFieldFee.setCreatedBy(requestFieldDetailView.getCreatedBy());
				reqFieldFee.setCreatedOn(requestFieldDetailView.getCreatedOn());
				reqFieldFee.setIsDeleted(requestFieldDetailView.getIsDeleted());
				reqFieldFee.setRecordStatus(requestFieldDetailView.getRecordStatus());
				reqFieldFee.setRequestKeyValue(requestFieldDetailView.getRequestKeyValue());
				reqFieldFee.setRequestKeyViewId(requestFieldDetailView.getRequestKeyViewId());
				reqFieldFee.setRequestKeyView(requestFieldDetailView.getRequestKeyView());
				reqFieldFee.setUpdatedBy(getLoggedInUser());
				reqFieldFee.setUpdatedOn(new Date());
				reqFieldDetailsView.add(reqFieldFee);
			}
			else
			{
				
				reqFieldFee.setCreatedBy(getLoggedInUser());
				reqFieldFee.setCreatedOn(new Date());
				reqFieldFee.setIsDeleted(new Long(0));
				reqFieldFee.setRecordStatus(new Long(1));
				reqFieldFee.setRequestKeyValue(viewRootMap.get("NOL_VIEW").toString());
				reqFieldFee.setRequestKeyViewId(getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
				reqFieldFee.setUpdatedBy(getLoggedInUser());
				reqFieldFee.setUpdatedOn(new Date());
				reqFieldDetailsView.add(reqFieldFee);
			
			}
				
			
			if(viewRootMap.containsKey("NOL_REASON_FOR_EDIT"))
			{	
				RequestFieldDetailView requestFieldDetailView = (RequestFieldDetailView)viewRootMap.get("NOL_REASON_FOR_EDIT");
				reqFieldReason.setRequestFieldDetailViewId(requestFieldDetailView.getRequestFieldDetailViewId());
				reqFieldReason.setCreatedBy(requestFieldDetailView.getCreatedBy());
				reqFieldReason.setCreatedOn(requestFieldDetailView.getCreatedOn());
				reqFieldReason.setIsDeleted(requestFieldDetailView.getIsDeleted());
				reqFieldReason.setRecordStatus(requestFieldDetailView.getRecordStatus());
				reqFieldReason.setRequestKeyValue(reasontForNol.getValue().toString().trim());
				reqFieldReason.setRequestKeyViewId(requestFieldDetailView.getRequestKeyViewId());
				reqFieldReason.setRequestKeyView(requestFieldDetailView.getRequestKeyView());
				reqFieldReason.setUpdatedBy(getLoggedInUser());
				reqFieldReason.setUpdatedOn(new Date());
				reqFieldDetailsView.add(reqFieldReason);
			}
			else
			{
				reqFieldReason.setCreatedBy(getLoggedInUser());
				reqFieldReason.setCreatedOn(new Date());
				reqFieldReason.setIsDeleted(new Long(0));
				reqFieldReason.setRecordStatus(new Long(1));
				reqFieldReason.setRequestKeyValue(reasontForNol.getValue().toString());
				reqFieldReason.setRequestKeyViewId(getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
				reqFieldReason.setUpdatedBy(getLoggedInUser());
				reqFieldReason.setUpdatedOn(new Date());
				reqFieldDetailsView.add(reqFieldReason);
				
			}
			requestView.setRequestFieldDetailsView(reqFieldDetailsView);
			Long requestId = requestServiceAgent.addRequest(requestView);
			return requestId;
	}
	
	public void addContractActivity()
	{
		
		try{	
			List<CommercialActivityView> commercialActivityListInSaveMethod=new ArrayList<CommercialActivityView>(0);
			List<ContractActivityView> contractActivityListInSaveMethod=new ArrayList<ContractActivityView>(0);
			commercialActivityListInSaveMethod=getCommercialActivityList();
		 if(commercialActivityListInSaveMethod !=null && commercialActivityListInSaveMethod.size()>0)
		 {	 
			for(CommercialActivityView contActivity:commercialActivityListInSaveMethod)
			{
				if(contActivity.getIsSelected()!=null && contActivity.getIsSelected())
				{
					ContractActivityView contractActivityView=new ContractActivityView();
					
					if(contActivity.getContractActivityId()!=null){
						contractActivityView.setContractActivityId(contActivity.getContractActivityId());
						contractActivityView.setRequestId(Convert.toLong(this.requestId));
						contractActivityView.setIsDeleted(contActivity.getIsDeleted());
						contractActivityView.setRecordStatus(contActivity.getRecordStatus());
						contractActivityView.setCreatedBy(contActivity.getCreatedBy());
						contractActivityView.setUpdatedBy(getLoggedInUser());
						contractActivityView.setCreatedOn(contActivity.getCreatedOn());
						contractActivityView.setUpdatedOn(new Date());
						contractActivityView.setCommercialActivityId(contActivity.getCommercialActivityId());
						contractActivityView.setContractActivityId(null);
						contractActivityView.setContractId(null); 	
					}else{
					
						contractActivityView.setRequestId(Convert.toLong(this.requestId));
						contractActivityView.setIsDeleted(0L);
						contractActivityView.setRecordStatus(1L);
						contractActivityView.setCreatedBy(getLoggedInUser());
						contractActivityView.setUpdatedBy(getLoggedInUser());
						contractActivityView.setCreatedOn(new Date());
						contractActivityView.setUpdatedOn(new Date());
						contractActivityView.setCommercialActivityId(contActivity.getCommercialActivityId());
						contractActivityView.setContractActivityId(null);
						contractActivityView.setContractId(null);
						
					}
					contractActivityListInSaveMethod.add(contractActivityView);
				}
			}
			RequestService rs=new RequestService();
			
			rs.addContractActivitiesForRequest(contractActivityListInSaveMethod,Convert.toLong(this.requestId));
		 }
	 
	}catch (Exception e) {
		// TODO: handle exception
	}
}
	
	public void putRequestForEdit(Long requestId)
	{
		RequestView requestView = new RequestView();
		RequestFieldDetailView requestFieldDetailViewNew =new RequestFieldDetailView();
		
		try{
			
			nolTypeSelectOneMenu.setDisabled(true);
			cmbRequestPriority.setDisabled(true);
		List<RequestKeyView>  requestKeys = new RequestServiceAgent().getRequestKeys(WebConstants.NOL.PROCEDURE_TYPE_NO_OBJECTION_LETTER);
		
		this.requestId = requestId.toString();
		requestView.setRequestId(requestId);
		populateApplicationDataTab(this.requestId);
		
		List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
		String contractNo = "";
           
		
		if(requestList.size()>0)
		{
			requestView = (RequestView)requestList.iterator().next();
			viewRootMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
			contractNo = requestView.getContractView().getContractNumber();
		
			requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_TYPE));
			viewRootMap.put("NOL_TYPE_FOR_EDIT", requestFieldDetailViewNew);
			
			setStrNolTypeId(requestFieldDetailViewNew.getRequestKeyValue());
			nolView.setNolTypeId(Long.parseLong(requestFieldDetailViewNew.getRequestKeyValue()));
			viewRootMap.put("NOL_VIEW", requestFieldDetailViewNew.getRequestKeyValue());
			
			if(!(nolView.getNolTypeId()==11L))
    		tabCommercialActivity.setRendered(false);
    	    else 
    		tabCommercialActivity.setRendered(true);
    		
			
			paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
			if(paymentSchedules!=null && paymentSchedules.size()>0)
			{
			 viewRootMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
			 setPaymentSchedules(paymentSchedules);
			}else
			{
			 viewRootMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
			}
		}

		requestFieldDetailViewNew = new RequestServiceAgent().getRequestFieldDetailByRequestIdAndKeyValue(requestView.getRequestId(),getRequestKeyViewId(requestKeys,WebConstants.NOL.REQUEST_KEY_NOL_REASON));
		viewRootMap.put("NOL_REASON_FOR_EDIT", requestFieldDetailViewNew);
		
		reasontForNol.setValue(requestFieldDetailViewNew.getRequestKeyValue());
		
		ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
		contractNoText.setValue(contractNo);
		populateContract();
		
		contractActivityList = new RequestServiceAgent().getContractActivitiesByRequestId(requestId);
		if(contractActivityList!=null && contractActivityList.size()>0)
		{
			setCommercialActivityList(getAllCommercialActivities());
			for(ContractActivityView conActView: contractActivityList)
			{
			 	for(CommercialActivityView commActView:getCommercialActivityList())
			 	{
			 		if(conActView.getCommercialActivityId().compareTo(commActView.getCommercialActivityId())==0)
			 		{
			 			commActView.setIsSelected(true);
			 			commActView.setIsDisabled(true);
			 			commActView.setContractActivityId(conActView.getContractActivityId());
			 			commActView.setCreatedBy(conActView.getCreatedBy());
			 			commActView.setCreatedOn(conActView.getCreatedOn());
			 			commActView.setIsDeleted(conActView.getIsDeleted());
			 			commActView.setRecordStatus(conActView.getRecordStatus());
			 			commActViewListforRequest.add(commActView);
			 		}else
			 		{
			 			commActView.setIsDisabled(true);
			 			commActViewListforRequest.add(commActView);
			 		}
			 		
			 	}
		     }
		
		    setCommercialActivityList(commActViewListforRequest);
		}
		
		CommonUtil.loadAttachmentsAndComments(requestId.toString());	
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	 @SuppressWarnings("unchecked")
	 public Boolean	validate() throws Exception
	 {
		    // if no applicant is selected than we will not allow to save request
			if(!viewRootMap.containsKey(WebConstants.LOCAL_PERSON_ID))
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
				viewRootMap.put("selectedTab",APPLICATION_TAB);
				return false;
			}
			if(!viewRootMap.containsKey("NOL_VIEW") && !(viewRootMap.get("NOL_VIEW") !=null) )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.NOL_NolType));
				viewRootMap.put("selectedTab",NOL_TAB);
				return false;	
			}
			else if(new Long(viewRootMap.get("NOL_VIEW").toString()).compareTo(11L)==0 && !hasAnyCommercialActivitySelected())
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("nol.commercialActivityRequired"));
				viewRootMap.put("selectedTab",COMMERCIAL_ACTIVITY_TAB);
				
				return false;
				
			}
			if( reasontForNol.getValue() ==null || reasontForNol.getValue().toString().trim().length()<= 0 )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.NOL_NolReason));
				viewRootMap.put("selectedTab",NOL_TAB);
				return false;
			}
			 if(!AttachmentBean.mandatoryDocsValidated())
			 {
			 		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
			 		viewRootMap.put("selectedTab",ATTACHMENT_TAB);  
			 		return false;
			 }
		
			return true;

	 }
	private boolean hasAnyCommercialActivitySelected()
	{
		List<CommercialActivityView> commercialActivityListInSaveMethod=new ArrayList<CommercialActivityView>(0);
		List<ContractActivityView> contractActivityListInSaveMethod=new ArrayList<ContractActivityView>(0);
		commercialActivityListInSaveMethod=getCommercialActivityList();
		if (commercialActivityListInSaveMethod == null )return false;
		
		for(CommercialActivityView contActivity:commercialActivityListInSaveMethod)
		{
			if(contActivity.getIsSelected()!=null && contActivity.getIsSelected())
				return true;
		}
				return false;
	}
	
	@SuppressWarnings("unchecked")
	public String sendForApproval()
	{
		counter++;
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		try 
		{
		  if(validateMandatoryDocuments())
		  {	   
			if(txtRemarks!=null && txtRemarks.trim().length()>0)
		    {
					UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		
					String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
					logger.logInfo("Contextpath is:"+contextPath);
					String loggedInUser = getLoggedInUser();
					BPMWorklistClient client = new BPMWorklistClient(contextPath);			
					
					//NOT NEEDED
					//addPaymentSchedule();
		
					Map taskAttributes =  userTask.getTaskAttributes();
					Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));	
					CommonUtil.saveRemarksAsComments(requestId, txtRemarks, noteOwner) ;
					this.requestId = requestId.toString();
					CommonUtil.loadAttachmentsAndComments(requestId.toString());
					CommonUtil.saveAttachments(requestId);
					CommonUtil.saveComments(requestId,noteOwner);
					txtRemarks ="";
		
					//getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,null);
					RequestView requestView = new RequestView();
					requestView.setRequestId(requestId);
				
		           
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_TECHNICAL_COMMENT_HAS_SUBMITTED_SUCCESSFULLY));
		 
					NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_TECHNICAL_COMMENT_ADDED, requestId);
					viewRootMap.put("canAddAttachment",false);
					viewRootMap.put("canAddNote", false);
					sendForApprovalButton.setRendered(false);
					client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
		    }
			else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
		   } 
		}	  
		catch (Exception e) 
		{
			logger.LogException( "sendForApproval|crashed...", e);
                   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_TECHNICAL_FAILURE));
		}
		return "success";

	}

	private String getRequestNumber(RequestView requestView) {
		String requestNo = "";
		String  METHOD_NAME = "getRequestNumber|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
			
			requestNo = ((RequestView)requestList.iterator().next()).getRequestNumber();
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		return requestNo;
	}
	public String Approved(){
		final String  METHOD_NAME = "Approved|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		counter++;
		try {
			logger.logInfo(METHOD_NAME+"Start|");
			if(validateMandatoryDocuments())
			{	
				UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
				String loggedInUser = getLoggedInUser();
				BPMWorklistClient client = new BPMWorklistClient(contextPath);			
							
				//REMOVED FROM HERE
	
				Map taskAttributes =  userTask.getTaskAttributes();
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				this.requestId = requestId.toString();
				//MOVED TO HERE
				/*if(!addPaymentSchedule(requestId)){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOT_PROCEED_FURTHER));
				return "Approved";
				}*/
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				CommonUtil.saveAttachments(requestId);
				CommonUtil.saveComments(requestId,noteOwner);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_APPROVE_SUCCESS));
				NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_APPROVED, requestId);
				notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_NOL_Approved);
				
				sendButton.setRendered(false);
				sendButtonWithoutTechComments.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
				logger.logInfo(METHOD_NAME+" completed successfully...");
			
		   }
		} 
		catch(PIMSWorkListException ex)
		{
			logger.LogException( "Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_APPROVE_FAILURE));

		}
		catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_APPROVE_FAILURE));
		}
		return "success";

	}
	public String initiateFlowWithoutTechComments()
	{
		return send(false);
	}
	public String initiateFlowWithTechComments()
	{
		String  METHOD_NAME = "initiateFlowWithTechComments|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		counter++;
		try {
			if(validateMandatoryDocuments())
			{	
				UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
				logger.logInfo("Contextpath is:"+contextPath);
				String loggedInUser = getLoggedInUser();
				BPMWorklistClient client = new BPMWorklistClient(contextPath);			
							
				//REMOVED FROM HERE
	
				Map taskAttributes =  userTask.getTaskAttributes();
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				this.requestId = requestId.toString();
			
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				CommonUtil.saveAttachments(requestId);
				CommonUtil.saveComments(requestId,noteOwner);
				
				successMessages.add(ResourceUtil.getInstance().getProperty("commons.sendForInspection"));
				
				NotesController.saveSystemNotesForRequest( WebConstants.ISSUE_NO_OBJECTION_LETTER,
						                                   MessageConstants.RequestEvents.INSPECTION_REQUIRED, requestId);
				
				
				logger.logInfo(METHOD_NAME+" completed successfully...");
				//modeAddRequestDisplay();
				sendButton.setRendered(false);
				sendButtonWithoutTechComments.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				
				client.completeTask(userTask, loggedInUser, TaskOutcome.SEND_FOR_SITE_VISIT);
	//			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_VIEW,null);
			
		   }
		} 
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_APPROVE_FAILURE));
			
			ex.printStackTrace();
//			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_VIEW,null);

		}
		catch (Exception e) 
		{
			logger.LogException(METHOD_NAME + "crashed...", e);
//			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.CONTRACT_VIEW,null);
		}
		
		
		return "";
	}
	public String send(boolean isTechCommentsRequired)
	{
		final String METHOD_NAME = "send";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		counter++;
		try {
			logger.logInfo(METHOD_NAME+" Start|isTechCommentRequired:%s", isTechCommentsRequired);
         if(validateMandatoryDocuments())
         { 
			
			PIMSNOLBPELPortClient pimsNOLBPELPortClient = new PIMSNOLBPELPortClient();

			String endPoint = parameters.getParameter(WebConstants.NOL.NOL_BPEL_END_POINT);
			pimsNOLBPELPortClient.setEndpoint(endPoint);

			pimsNOLBPELPortClient.initiate(Long.parseLong(this.requestId),getLoggedInUser(),isTechCommentsRequired, null,null);
			
			notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_NOL_Received);
			successMessages.add(CommonUtil.getBundleMessage("nolRequest.Send"));
			if( !isTechCommentsRequired )
				NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, Long.parseLong(this.requestId));
	
			sendButton.setRendered(false);
			saveButton.setRendered(false);
			sendButtonWithoutTechComments.setRendered(false);
			populateContract.setRendered(false);
			viewRootMap.put("SEND_BUTTON", true);
			hdnSendStatus = "1";
			cmbRequestPriority.setReadonly( true);
			nolTypeSelectOneMenu.setReadonly(true);
			viewRootMap.put("canAddAttachment",false);
			viewRootMap.put("canAddNote", false);
			setEditPaymentEnable(false);
			viewRootMap.put("NOLTabReadOnlyMode","READONLY");

			CommonUtil.printReport(new Long(requestId));
         }
         logger.logInfo(METHOD_NAME+" Completed");
		} catch (Exception e) {
			logger.LogException( METHOD_NAME+" Error Occured:",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_SENT_FAILURE));

		}
         
		return "send";
	}
	
    private Boolean validateMandatoryDocuments()
    {
    	Boolean validated = true;
    	try
		{
	    	if(!AttachmentBean.mandatoryDocsValidated())
	    	{	    		
	    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
				validated = false;				
				viewRootMap.put("selectedTab",ATTACHMENT_TAB);
	    	}    	
	    	if( reasontForNol.getValue() ==null || reasontForNol.getValue().toString().trim().length()<= 0 )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.NOL_NolReason));
				viewRootMap.put("selectedTab",NOL_TAB);
				return false;
			}
		}
    	catch (Exception exception) {
			logger.LogException("fieldsValidated() crashed ", exception);
			validated = false;		
		}
    	return validated;
    }

	public String printReceipt(){
		//modeAction();
		return "PRINT";
	}
	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
    	
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	@SuppressWarnings("unchecked")
	public String populateContract()
	{
		try{

			if(getFacesContext().getExternalContext().getSessionMap().get("NEW_REQUEST")==null||
					Boolean.valueOf(getFacesContext().getExternalContext().getSessionMap().get("NEW_REQUEST").toString())){
				//modeAddRequest();	

			}
			sessionMap.put("NEW_REQUEST",false);
			contractView = propertyServiceAgent.getContract(contractNoText.getValue().toString(),parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));
			getContractById(contractView.getContractId());
			Map hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
					WebConstants.CONTRACT_STATUS_ACTIVE);
			setTenantName(contractView);

			viewRootMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
			if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0){
				populateFieldsAgainstView();


				viewRootMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.LOCAL_TENANT_ID, contractView.getTenantView().getPersonId());

				tenantButton.setRendered(true);
				contractButton.setRendered(true);

				if(hMap.containsKey("returnId")){
					Long contractExpiredStatusId = Long.parseLong(hMap.get("returnId").toString());
					if(contractExpiredStatusId.equals(contractView.getStatus())){


//						enablePanel();

						if(getFacesContext().getExternalContext().getRequestMap().get("FROM_REQUEST_SEARCH")!=null&&
								(Boolean)getFacesContext().getExternalContext().getRequestMap().get("FROM_REQUEST_SEARCH")){

//							disablePanel();
							//populateContract.setRendered(false);
						}

					}
					else{
						messages.add(bundle.getString(MessageConstants.NOL.MSG_CONTRACT_HAS_EXPIRED)+" "+contractView.getStatusEn()+" "+bundle.getString("commons.status"));
//						disablePanel();
						getFacesContext().getExternalContext().getRequestMap().put("EXPIRED", true);
						contractNoText.setReadonly(false);
						if(getFacesContext().getExternalContext().getRequestMap().get("FROM_REQUEST_SEARCH")!=null&&
								(Boolean)getFacesContext().getExternalContext().getRequestMap().get("FROM_REQUEST_SEARCH")){
							contractNoText.setReadonly(true);
							//populateContract.setRendered(false);
						}



					}
				}

				return "Success";
			}
			else if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()==0)
				return "ZeroRecords";
			else
				return "Failure";
		}
		catch (Exception e) {			
			logger.LogException( "crashed...", e);
			return "Failure";
		}

	}
	private void populateFieldsAgainstView() {
		try{
			populationContract++;
			contractView = (ContractView)viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW);
			if(populationContract==2){
				if(contractView!=null&&(contractView.getTenantView().getFirstName()+" "+contractView.getTenantView().getLastName())!=null&&
						(contractView.getTenantView().getFirstName()+" "+contractView.getTenantView().getLastName()).trim().length()>0){
					contractNoText.setValue(contractView.getContractNumber());
					
					//Added by shiraz for populate required field in the new screen
				
				if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
				{
					viewRootMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					
					//Setting the Contract/Tenant Info
					setTenantName(contractView);
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
					//commented because their is change in tenant type 
					//tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
					if(getIsEnglishLocale())
					contractStatusText.setValue(contractView.getStatusEn());
					else
					contractStatusText.setValue(contractView.getStatusAr());
					totalContractValText.setValue(contractView.getRentAmount());
					
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
					    txtUnitRentValue.setValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					
					}
				}
					viewRootMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					getFacesContext().getExternalContext().getSessionMap().put(WebConstants.Contract.LOCAL_TENANT_ID, contractView.getTenantView().getPersonId());
					tenantButton.setRendered(true);
					contractButton.setRendered(true);
				}
				else{
					emptyFields();

				   }
			}
		  }
		catch(Exception e){

		}

	}
		private void setTenantName(ContractView contractView) {
			String  METHOD_NAME = "setTenantName|";		
	        logger.logInfo(METHOD_NAME+" started...");
			if(contractView.getTenantView() != null){
				hdnTenantId= contractView.getTenantView().getPersonId().toString();
				tenantNameText.setValue(contractView.getTenantView().getPersonFullName());
				if(contractView.getTenantView().getResidenseVisaNumber()!=null &&
						!contractView.getTenantView().getResidenseVisaNumber().equals(""))
				tenantNumberType.setValue(contractView.getTenantView().getResidenseVisaNumber());
				else
				tenantNumberType.setValue(contractView.getTenantView().getSocialSecNumber());	
			}
	        logger.logInfo(METHOD_NAME+" ended...");
		}
		public String getStringFromDate(Date dateVal){
			String pattern = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(pattern);
			String dateStr = "";
			try{
				if(dateVal!=null)
					dateStr = formatter.format(dateVal);
			}
			catch (Exception exception) {
				exception.printStackTrace();
			}
			return dateStr;
		}
		public String getDateFormat(){
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
	    }

	@SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		try
		{
			if(!isPostBack())
			{
				requestStatusForColumn = false;
				if(sessionMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID)!=null)
						sessionMap.remove(WebConstants.Contract.LOCAL_CONTRACT_ID);
			}
			if(viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW)!=null)
			{
				 screenName = resourceBundle.getString("contract.screenName.issueNOL");
				if(getFacesContext().getExternalContext().getRequestMap().get("SIGNBOARD_ENTRY")!=null&&
						Boolean.valueOf(getFacesContext().getExternalContext().getRequestMap().get("SIGNBOARD_ENTRY").toString()))
				if(nolTypeHidden.getValue()!=null && nolTypeHidden.getValue().toString().trim().length()>0)
					nolTypeSize.setValue(nolTypeHidden.getValue());
				populateFieldsAgainstView();
				tenantNumberType.setReadonly(true);
				contractStatusText.setReadonly(true);
				totalContractValText.setReadonly(true);
				txtUnitType.setReadonly(true);
				txtpropertyName.setReadonly(true);
				txtpropertyType.setReadonly(true);
				txtunitRefNum.setReadonly(true);
				txtUnitRentValue.setReadonly(true);
			}
			else
				emptyFields();
			if(getFacesContext().getExternalContext().getSessionMap().get("NOL_TYPE")!=null)
				nolType = getFacesContext().getExternalContext().getSessionMap().get("NOL_TYPE").toString();
	
			if(nolTypeHidden.getValue()!=null&&
					nolTypeHidden.getValue().toString().trim().length()>0){
				nolTypeSize.setValue(nolTypeHidden.getValue());
				if(nolType!=null&&nolType.equals(WebConstants.NOL.NOL_TYPE_SIGNBOARD)){
				//	modeSignBoardEntryDisplay();
				}
				else{
				//	modeSignBoardNormal();
				}
	
				if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.NOL.FEE_CONFIGURATION_DETAIL_VIEW)!=null){
					feeConfigurationView = (FeeConfigurationView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.NOL.FEE_CONFIGURATION_DETAIL_VIEW);
	
	
					FeeConfigurationDetailView feeConfigurationDetailView = new FeeConfigurationDetailView();
					Iterator feeConfigDetailIterator  = feeConfigurationView.getFeeConfigurationDetailsView().iterator();
					while(feeConfigDetailIterator.hasNext()){
						feeConfigurationDetailView = (FeeConfigurationDetailView)feeConfigDetailIterator.next();
						if(feeConfigurationDetailView.getMaxValue()!=null&&
								feeConfigurationDetailView.getMinValue()!=null&&
								feeConfigurationDetailView.getMaxValue()>=Double.valueOf(nolTypeHidden.getValue().toString())
								&&feeConfigurationDetailView.getMinValue()<=Double.valueOf(nolTypeHidden.getValue().toString())){
							
							feeText.setValue(feeConfigurationDetailView.getAmount());
							break;
						}
						else
							feeText.setValue(0);
					}
					if(feeText.getValue()==null)
						feeText.setValue(0);
				}
	
	
			}
			// for time been commented 
			if(nolType!=null&&nolType.equals(WebConstants.NOL.NOL_TYPE_SIGNBOARD)){
			//	modeSignBoardEntryDisplay();
				if(nolTypeSize.getValue()==null||nolTypeSize.getValue().toString().trim().length()==0){
					feeText.setValue(0);
					nolTypeSize.setValue(0);
				}
			}
			if(getFacesContext().getExternalContext().getRequestMap().get("EXPIRED")!=null &&
					Boolean.valueOf(getFacesContext().getExternalContext().getRequestMap().get("EXPIRED").toString()))
				//modeExpired();
	
			if(getFacesContext().getExternalContext().getRequestMap().get("PAID")!=null){
				//modeExpired();
				printButton.setRendered(true);
				cancelButton.setRendered(false);
				populateContract.setRendered(false);
				tenantNumberType.setReadonly(true);
				contractStatusText.setReadonly(true);
				totalContractValText.setReadonly(true);
				txtUnitType.setReadonly(true);
				txtpropertyName.setReadonly(true);
				txtpropertyType.setReadonly(true);
				txtunitRefNum.setReadonly(true);
				txtUnitRentValue.setReadonly(true);
				
			}
			
			
			
			if(hdnContractId!=null && !hdnContractId.equals("")&& hdnContractNumber!=null && !hdnContractNumber.equals(""))
			{
	
				populateContractFromPopUp();
				
				if(!viewRootMap.containsKey("SEND_BUTTON")){
				sendButton.setRendered(false);
				sendButtonWithoutTechComments.setRendered(true);
				}
			  }
		
			
			if(hdnPersonId!=null && !hdnPersonId.equals(""))
			{
				viewRootMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				 viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}
	
				if(hdnPersonName!=null && !hdnPersonName.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
				
				
				if(hdnCellNo!=null && !hdnCellNo.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
				
				if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
					DomainDataView ddv = new DomainDataView();
					if(Long.parseLong(hdnIsCompany)==1L){
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);}
					else{
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);}
					
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}
				
				if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
					sessionMap.put("PAYMENT_DONE", true);
					
					if(isPaymentCollected)
				       {
					    
							CollectPayment();
							
					if(requestId != null){
					
						  RequestServiceAgent rSAgent = new RequestServiceAgent();
						  try {
							rSAgent.setRequestAsApprovedCollected(Long.parseLong(requestId), getLoggedInUser());
						} catch (NumberFormatException e) {
							throw e;
						} catch (PimsBusinessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		                  completeRequest.setRendered(true);
		                  cancelRequest.setRendered(true);
		                  printNol.setRendered(true);
						  
					}
	
	
	
						approveButton.setRendered(false);
						rejectButton.setRendered(false);
						sendButton.setRendered(false);
						sendButtonWithoutTechComments.setRendered(false);
						populateContract.setRendered(false);
						btnCollectPayment.setRendered(false);
						//completeRequest.setRendered(false);
						cancelRequest.setRendered(false);
						printNol.setRendered(true);
						requestStatusForColumn = true;
						//viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
										
										
							
				       }
					
				}
	
	
				if(ApplicationContext.getContext().get(WebContext.class).getAttribute(WebConstants.REQUEST_VIEW)!=null)
					if(counterDetail==2)
						ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,null);
	             
	             
				
			
	            if( viewRootMap.get("selectedTab")!=null && viewRootMap.get("selectedTab").toString().length() > 0 )
	            {
					String selectedTab = (String)viewRootMap.get("selectedTab");
					if(selectedTab!=null)
					{
						richTabPanel.setSelectedTab(selectedTab);
						viewRootMap.remove("selectedTab");
					}
	            }
		}	            
	    catch(Exception e)
	    {
            errorMessages = new ArrayList<String>(0);
	 		logger.LogException( "onSave|Error Occured", e);
	 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    }
	}	
	@SuppressWarnings( "unchecked" )
	public String btnContract_Click()
	{

		contractView = (ContractView)viewRootMap.get(WebConstants.Contract.CONTRACT_VIEW);
		
		
		if(contractView !=null && contractView.getContractId() != null && ! contractView.getContractId().toString().equals("") )
		{
			hdnContractId = contractView.getContractId().toString();
			context.getExternalContext().getSessionMap().put("contractId", hdnContractId);
			context.getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		
		
		String javaScriptText="var screen_width = screen.width;"+
        "var screen_height = screen.height;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";        
       openPopUp("LeaseContract.jsf",javaScriptText);
		}
		return "";
	}
	
	
	public String btnTenant_Click() 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		if(hdnTenantId!=null && !hdnTenantId.equals(""))
		{
		
		setRequestParam(WebConstants.PERSON_ID, hdnTenantId);
		setRequestParam("viewMode", "popup");
		   
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+hdnTenantId+");";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		}
		return "";

	}
	
	public HtmlCommandButton getAttachmentButton() {
		return attachmentButton;
	}
	public void setAttachmentButton(HtmlCommandButton attachmentButton) {
		this.attachmentButton = attachmentButton;
	}
	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	public HtmlInputText getContractEndDateText() {
		return contractEndDateText;
	}
	public void setContractEndDateText(HtmlInputText contractEndDateText) {
		this.contractEndDateText = contractEndDateText;
	}
	public HtmlInputText getContractNoText() {
		return contractNoText;
	}
	public void setContractNoText(HtmlInputText contractNoText) {
		this.contractNoText = contractNoText;
	}
	public HtmlInputText getContractStartDateText() {
		return contractStartDateText;
	}
	public void setContractStartDateText(HtmlInputText contractStartDateText) {
		this.contractStartDateText = contractStartDateText;
	}
	public HtmlInputText getContractTypeText() {
		return contractTypeText;
	}
	public void setContractTypeText(HtmlInputText contractTypeText) {
		this.contractTypeText = contractTypeText;
	}
	public HtmlInputText getFeeText() {
		return feeText;
	}
	public void setFeeText(HtmlInputText feeText) {
		this.feeText = feeText;
	}
	public HtmlSelectOneMenu getNolTypeSelectOneMenu() {
		return nolTypeSelectOneMenu;
	}
	public void setNolTypeSelectOneMenu(HtmlSelectOneMenu nolTypeSelectOneMenu) {
		this.nolTypeSelectOneMenu = nolTypeSelectOneMenu;
	}
	public HtmlCommandButton getPopulateContract() {
		return populateContract;
	}
	public void setPopulateContract(HtmlCommandButton populateContract) {
		this.populateContract = populateContract;
	}
	public HtmlInputTextarea getReasontForNol() {
		return reasontForNol;
	}
	public void setReasontForNol(HtmlInputTextarea reasontForNol) {
		this.reasontForNol = reasontForNol;
	}
	public HtmlCommandButton getSendButton() {
		return sendButton;
	}
	public void setSendButton(HtmlCommandButton sendButton) {
		this.sendButton = sendButton;
	}
	public HtmlInputText getTenantNameText() {
		return tenantNameText;
	}
	public void setTenantNameText(HtmlInputText tenantNameText) {
		this.tenantNameText = tenantNameText;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public boolean isRenderFee() {
		return renderFee;
	}
	public void setRenderFee(boolean renderFee) {
		this.renderFee = renderFee;
	}


	public List<SelectItem> getNolTypeItems() {
		
		if(viewRootMap.containsKey("SELECT_ITEM_NOL_TYPE"))
			nolTypeItems= (List<SelectItem>) viewRootMap.get("SELECT_ITEM_NOL_TYPE");
		return nolTypeItems;

	}

	public void setNolTypeItems(List<SelectItem> nolTypeItems) {
		this.nolTypeItems = nolTypeItems;
		if(this.nolTypeItems!=null)
			viewRootMap.put("SELECT_ITEM_NOL_TYPE",this.nolTypeItems);
	}

	private Long getRequestKeyViewId(List<RequestKeyView> keys, String requestKeyReason) throws PimsBusinessException{
		Long requestKeyViewId = new Long(0);
		for(RequestKeyView req: keys){
			if(req.getKeyName().equals(requestKeyReason)){
				requestKeyViewId = req.getRequestKeyId();
				break;
			}
		}
		if(requestKeyViewId.equals(new Long(0)))
			throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		return requestKeyViewId;
	}


	public HtmlInputText getNolTypeSize() {
		return nolTypeSize;
	}


	public void setNolTypeSize(HtmlInputText nolTypeSize) {
		this.nolTypeSize = nolTypeSize;
	}


	public Boolean getRenderFee() {
		return renderFee;
	}


	public void setRenderFee(Boolean renderFee) {
		this.renderFee = renderFee;
	}



	public String getStrNOLTypeSize() {
		return strNOLTypeSize;
	}


	public void setStrNOLTypeSize(String strNOLTypeSize) {
		this.strNOLTypeSize = strNOLTypeSize;
	}


	public boolean isRenderSize() {
		return renderSize;
	}


	public void setRenderSize(boolean renderSize) {
		this.renderSize = renderSize;
	}


	public HtmlInputHidden getNolTypeHidden() {
		return nolTypeHidden;
	}


	public void setNolTypeHidden(HtmlInputHidden nolTypeHidden) {
		this.nolTypeHidden = nolTypeHidden;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getMessages() {
		String messageList;
		if ((messages == null) ||
				(messages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<B><UL>\n";
			for(String message: messages) {
				messageList = messageList + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}
	
	@SuppressWarnings("unchecked")
	private Boolean addPaymentSchedule(Long requestId)  throws Exception
	{
		ContractView contractView =  new ContractView();
		PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		if(viewRootMap.containsKey("NO_PAYMENT_SCH_FOR_EDIT"))
		   return true;

		if(viewRootMap.containsKey("PAYMENT_SCH_FOR_EDIT"))
		{
			    List<PaymentScheduleView> paymentScheduleViewList = (List<PaymentScheduleView>)viewRootMap.get("PAYMENT_SCH_FOR_EDIT");
			    if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0)
			    for (PaymentScheduleView paymentScheduleViewForEdit : paymentScheduleViewList) 
			    {
			    	paymentScheduleView = paymentScheduleViewForEdit;
				    
					paymentScheduleView.setUpdatedBy(getLoggedInUser());
					paymentScheduleView.setUpdatedOn(new Date());
					paymentScheduleView.setStatusId(paymentScheduleViewForEdit.getStatusId());
					paymentScheduleView.setDescription(paymentScheduleViewForEdit.getDescription());
					if(viewRootMap.containsKey("CANCEL_PAYMENT_SCH"))
					{
						paymentScheduleView.setStatusId( WebConstants.CANCELED_ID );
					}
					//If request is normal and urgent fees is present then delete it. This will happen in case when request was saved with urgent than later
					//changed to normal
					if( 
							cmbRequestPriority.getValue() != null && 
							Long.valueOf( cmbRequestPriority.getValue().toString() ).compareTo(WebConstants.REQUEST_PRIORITY_NORMAL_ID) == 0 &&
							paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_FEES_URGENT_REQUEST_ID ) == 0
					  )
					{
						paymentScheduleView.setIsDeleted(1l);
					}
					
					pSAgent.addPaymentScheduleByPaymentSchedule(paymentScheduleView);
			    }
				
			    
			
		}
		else
		{	
 			  //if payment present than add
			   
			  if ( 
					  getPaymentSchedules() != null && 
					  getPaymentSchedules().size() > 0 
				 )
			  { 	
				  for (PaymentScheduleView feesPs : getPaymentSchedules()) 
				  {
					  	if(feesPs.getAmount()==null)
						{
					  		continue;
						}
					  	paymentScheduleView = feesPs;
					  	paymentScheduleView.setAmount(feesPs.getAmount());
						
					  	contractView =(ContractView)viewRootMap.get("Contract_VIEW_FOR_PAYMENT");
						paymentScheduleView.setContractId(contractView.getContractId());
						paymentScheduleView.setOwnerShipTypeId(
																contractView.getContractUnitView().iterator().next().getUnitView().getPropertyCategoryId()
															  );
						paymentScheduleView.setTypeId( feesPs.getTypeId() );
						paymentScheduleView.setPaymentDueOn(new Date());
						paymentScheduleView.setPaymentModeId(feesPs.getPaymentModeId());
						
						paymentScheduleView.setRequestId(requestId) ;
						paymentScheduleView.setDescription(feesPs.getDescription());
											
			
						paymentScheduleView.setStatusId( WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING_ID  );
						paymentScheduleView.setCreatedBy(getLoggedInUser());
						paymentScheduleView.setCreatedOn(new Date());
						paymentScheduleView.setUpdatedBy(getLoggedInUser());
						paymentScheduleView.setUpdatedOn(new Date());
						paymentScheduleView.setIsDeleted(new Long(0));
						paymentScheduleView.setRecordStatus(new Long(1));
						paymentScheduleView.setIsReceived("N");
						pSAgent.addPaymentScheduleByPaymentSchedule(paymentScheduleView);
				 }
	
			  }
		 }
		return true;				
		
	}
	
	private PaymentScheduleView addUrgentFees( )throws Exception
	{
	  	PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
	  	paymentScheduleView.setAmount(300D);

		paymentScheduleView.setTypeId( WebConstants.PAYMENT_TYPE_FEES_URGENT_REQUEST_ID);
		paymentScheduleView.setStatusId( WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING_ID  );
		paymentScheduleView.setPaymentModeId( WebConstants.PAYMENT_SCHEDULE_MODE_CASH_ID );
		paymentScheduleView.setPaymentDueOn(new Date());
//		paymentScheduleView.setRequestId(requestId) ;
		paymentScheduleView.setDescription( "Urgent Fees");
//		paymentScheduleView.setContractId(contractView.getContractId());
//		paymentScheduleView.setOwnerShipTypeId(contractView.getContractUnitView().iterator().next().getUnitView().getPropertyCategoryId());					

		
		paymentScheduleView.setCreatedBy(getLoggedInUser());
		paymentScheduleView.setCreatedOn(new Date());
		paymentScheduleView.setUpdatedBy(getLoggedInUser());
		paymentScheduleView.setUpdatedOn(new Date());
		paymentScheduleView.setIsDeleted(new Long(0));
		paymentScheduleView.setRecordStatus(new Long(1));
		paymentScheduleView.setIsReceived("N");
//		new PropertyService().addPaymentScheduleByPaymentSchedule( paymentScheduleView );
		return paymentScheduleView;
	}

	public HtmlCommandButton getFeeButton() {
		return feeButton;
	}


	public void setFeeButton(HtmlCommandButton feeButton) {
		this.feeButton = feeButton;
	}

	public HtmlInputHidden getIsApprovalMode() {
		return isApprovalMode;
	}

	public void setIsApprovalMode(HtmlInputHidden isApprovalMode) {
		this.isApprovalMode = isApprovalMode;
	}

	public HtmlInputHidden getIsTechnicalMode() {
		return isTechnicalMode;
	}

	public void setIsTechnicalMode(HtmlInputHidden isTechnicalMode) {
		this.isTechnicalMode = isTechnicalMode;
	}

	public HtmlCommandButton getSendForApprovalButton() {
		return sendForApprovalButton;
	}

	public void setSendForApprovalButton(HtmlCommandButton sendForApprovalButton) {
		this.sendForApprovalButton = sendForApprovalButton;
	}

	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}

	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}

	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}

	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}

	public HtmlTabPanel getRichTabPanel() {
		return richTabPanel;
	}

	public void setRichTabPanel(HtmlTabPanel richTabPanel) {
		this.richTabPanel = richTabPanel;
	}

	public HtmlCommandButton getPrintButton() {
		return printButton;
	}

	public void setPrintButton(HtmlCommandButton printButton) {
		this.printButton = printButton;
	}

	public HtmlCommandButton getContractButton() {
		return contractButton;
	}

	public void setContractButton(HtmlCommandButton contractButton) {
		this.contractButton = contractButton;
	}

	public HtmlCommandButton getTenantButton() {
		return tenantButton;
	}

	public void setTenantButton(HtmlCommandButton tenantButton) {
		this.tenantButton = tenantButton;
	}

	public HtmlInputText getTenantNumberType() {
		return tenantNumberType;
	}

	public void setTenantNumberType(HtmlInputText tenantNumberType) {
		this.tenantNumberType = tenantNumberType;
	}

	public HtmlInputText getContractStatusText() {
		return contractStatusText;
	}

	public void setContractStatusText(HtmlInputText contractStatusText) {
		this.contractStatusText = contractStatusText;
	}

	public HtmlInputText getTxtUnitType() {
		return txtUnitType;
	}

	public void setTxtUnitType(HtmlInputText txtUnitType) {
		this.txtUnitType = txtUnitType;
	}

	public HtmlInputText getTxtpropertyName() {
		return txtpropertyName;
	}

	public void setTxtpropertyName(HtmlInputText txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}

	public HtmlInputText getTxtpropertyType() {
		return txtpropertyType;
	}

	public void setTxtpropertyType(HtmlInputText txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
	}

	public HtmlInputText getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(HtmlInputText txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}

	public HtmlInputText getTxtUnitRentValue() {
		return txtUnitRentValue;
	}

	public void setTxtUnitRentValue(HtmlInputText txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}

	public HtmlInputText getTotalContractValText() {
		return totalContractValText;
	}

	public void setTotalContractValText(HtmlInputText totalContractValText) {
		this.totalContractValText = totalContractValText;
	}

	

	public String getHdnContractId() {
		return hdnContractId;
	}

	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}

	public String getHdnContractNumber() {
		return hdnContractNumber;
	}

	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}

	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}

	public String populateContractFromPopUp(){
		String  METHOD_NAME = "populateContractFromPopUp|";		
        logger.logInfo(METHOD_NAME+"started...");

        try{
			if(hdnContractNumber!=null && !hdnContractNumber.equals(""))
			{
				clearValues();
				SystemParameters parameters = SystemParameters.getInstance();			
				String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
				try{				

					contractView = propertyServiceAgent.getContract(hdnContractNumber,dateFormat);
					viewRootMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
					
					
					contractNoText.setValue(contractView.getContractNumber());
				}
				catch (Exception e) {
					//writeMessage(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_NO_CONTRACT_FOUND));					
					throw e;
				}
				if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
				{
				      viewRootMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
				 	 getFacesContext().getExternalContext().getSessionMap().put(WebConstants.LOCAL_PERSON_ID, contractView.getTenantView().getPersonId());
					
					//Setting the Contract/Tenant Info
					setTenantName(contractView);
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
                    // commented because their is a change in the tenant type 					
					//tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
					if(getIsEnglishLocale())
					contractStatusText.setValue(contractView.getStatusEn());
					else
					contractStatusText.setValue(contractView.getStatusAr());
					totalContractValText.setValue(contractView.getRentAmount());
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
					    txtUnitRentValue.setValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					
					}
		
	
					
					return "Sucess";
				}
				else if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()==0)
					return "ZeroRecords";
				else
					return "Failure";
			}
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {			
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		hdnContractNumber = null;
		return "";
	}
	
private void clearValues() {
		
		setTenantName(contractView);
		contractTypeText.setValue("");
		contractStartDateText.setValue("");
		contractEndDateText.setValue("");
		tenantNumberType.setValue("");		
		contractStatusText.setValue("");		
		totalContractValText.setValue("");
		//feeInputText.setValue("");
		txtpropertyName.setValue("");
		txtpropertyType.setValue("");
		txtunitRefNum.setValue("");
		txtUnitRentValue.setValue("");
		txtUnitType.setValue("");
		
	}

public String getHdnPersonId() {
	return hdnPersonId;
}

public void setHdnPersonId(String hdnPersonId) {
	this.hdnPersonId = hdnPersonId;
}

public String getHdnPersonName() {
	return hdnPersonName;
}

public void setHdnPersonName(String hdnPersonName) {
	this.hdnPersonName = hdnPersonName;
}

public String getHdnPersonType() {
	return hdnPersonType;
}

public void setHdnPersonType(String hdnPersonType) {
	this.hdnPersonType = hdnPersonType;
}

public String getHdnCellNo() {
	return hdnCellNo;
}

public void setHdnCellNo(String hdnCellNo) {
	this.hdnCellNo = hdnCellNo;
}

public String getHdnIsCompany() {
	return hdnIsCompany;
}

public void setHdnIsCompany(String hdnIsCompany) {
	this.hdnIsCompany = hdnIsCompany;
}



		@SuppressWarnings("unchecked")
		public void populateApplicationDataTab(String requestId)throws Exception
		{
			
			RequestView rV= new RequestView();
			List<RequestView> rVList = new ArrayList<RequestView>();
			rV.setRequestId(new Long(requestId));
			PropertyServiceAgent psa = new PropertyServiceAgent();
		    rVList = psa.getAllRequests(rV, null, null, null);
			rV = rVList.get(0);
			if(rV.getRequestNumber()!=null && !rV.getRequestNumber().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, rV.getRequestNumber());
			if(rV.getStatusId().toString()!=null && !rV.getStatusId().toString().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?rV.getStatusEn():rV.getStatusAr());
			if(rV.getRequestDate()!=null)
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, rV.getRequestDate());
			if(rV.getDescription()!=null && !rV.getDescription().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, rV.getDescription());
			if(rV.getRequestId()!=null && !rV.getRequestId().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,rV.getRequestId());
			
			//for read only description 
			viewRootMap.put("applicationDetailsReadonlyMode", true);
			viewRootMap.put("applicationDescriptionReadonlyMode", "READONLY");
			 
			
		
			if(rV.getApplicantView()!=null && !rV.getApplicantView().equals(""))
			{
				viewRootMap.put("APPLICANT_VIEW_FOR_NOTIFICATION", rV.getApplicantView());
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals(""))
				  viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,rV.getApplicantView().getPersonId());
				
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals("")){
					viewRootMap.put(WebConstants.LOCAL_PERSON_ID, rV.getApplicantView().getPersonId());
				}
		
				if(rV.getApplicantView().getFirstName()!=null && !rV.getApplicantView().getFirstName().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,rV.getApplicantView().getPersonFullName() );
				
				if(rV.getApplicantView().getPersonTypeId()!=null && !rV.getApplicantView().getPersonTypeId().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
							getIsEnglishLocale()?rV.getApplicantView().getPersonTypeEn():rV.getApplicantView().getPersonTypeAr());
				if(rV.getApplicantView().getCellNumber()!=null && !rV.getApplicantView().getCellNumber().equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,rV.getApplicantView().getCellNumber());
				
				if(rV.getApplicantView().getIsCompany()!=null && !rV.getApplicantView().getIsCompany().equals(""))
				{
					DomainDataView ddv = new DomainDataView();
					if(rV.getApplicantView().getIsCompany()==1L){
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);}
					else{
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);}
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}
		  }
			
		  
			
		}

public String getRequestId() {
	return requestId;
}

public void setRequestId(String requestId) {
	this.requestId = requestId;
}
public void tabRequestHistory_Click()
 { 
	String methodName="tabRequestHistory_Click";
	logger.logInfo(methodName+"|"+"Start..");
	try	
	{
	  RequestHistoryController rhc=new RequestHistoryController();
	  
	    rhc.getAllRequestTasksForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,requestId);
	}
	catch(Exception ex)
	{
		logger.LogException(methodName+"|"+"Error Occured..",ex);
	}
	logger.logInfo(methodName+"|"+"Finish..");
 }


public List<PaymentScheduleView> getPaymentSchedules() {
	if (viewRootMap.get("PAYMENT_SCHEDULES")!=null)
		paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get("PAYMENT_SCHEDULES");
	return paymentSchedules;
}

public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
	this.paymentSchedules = paymentSchedules;
	if(this.paymentSchedules!=null)
		viewRootMap.put("PAYMENT_SCHEDULES", this.paymentSchedules);
}

public HtmlDataTable getDataTablePaymentSchedule() {
	return dataTablePaymentSchedule;
}

public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
	this.dataTablePaymentSchedule = dataTablePaymentSchedule;
}

public String getDateFormatForDataTable() {

	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	dateFormatForDataTable= localeInfo.getDateFormat();
	return dateFormatForDataTable;
}

public void setDateFormatForDataTable(String dateFormatForDataTable) {
	this.dateFormatForDataTable = dateFormatForDataTable;
}

public double getTotalRentAmount() {
	return totalRentAmount;
}

public void setTotalRentAmount(double totalRentAmount) {
	this.totalRentAmount = totalRentAmount;
}

public double getTotalCashAmount() {
	return totalCashAmount;
}

public void setTotalCashAmount(double totalCashAmount) {
	this.totalCashAmount = totalCashAmount;
}

public double getTotalChqAmount() {
	return totalChqAmount;
}

public void setTotalChqAmount(double totalChqAmount) {
	this.totalChqAmount = totalChqAmount;
}

public double getTotalDepositAmount() {
	return totalDepositAmount;
}

public void setTotalDepositAmount(double totalDepositAmount) {
	this.totalDepositAmount = totalDepositAmount;
}

public double getTotalFees() {
	return totalFees;
}

public void setTotalFees(double totalFees) {
	this.totalFees = totalFees;
}

public double getTotalFines() {
	return totalFines;
}

public void setTotalFines(double totalFines) {
	this.totalFines = totalFines;
}

public int getInstallments() {
	return installments;
}

public void setInstallments(int installments) {
	this.installments = installments;
}

public String deletePayment()
{
	try{
		
	
	int rowID = dataTablePaymentSchedule.getRowIndex();
	
	paymentSchedules = getPaymentSchedules();
	paymentSchedules.remove(rowID);
	sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_NEW,paymentSchedules);
	//calculateSummaray();
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return "";
}

public Boolean getRequestStatusForColumn() {
	return requestStatusForColumn;
}

public void setRequestStatusForColumn(Boolean requestStatusForColumn) {
	this.requestStatusForColumn = requestStatusForColumn;
}


public String getStrNolTypeId() {
	if(viewRootMap.containsKey("NOL_TYPE_SELECTED VALUE"))
		strNolTypeId=viewRootMap.get("NOL_TYPE_SELECTED VALUE").toString();
	return strNolTypeId;
}

public void setStrNolTypeId(String strNolTypeId) {
	this.strNolTypeId = strNolTypeId;
	if(this.strNolTypeId!=null)
		viewRootMap.put("NOL_TYPE_SELECTED VALUE",this.strNolTypeId);

}

public String completed(){
	String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
	String  METHOD_NAME = "completed|";
	errorMessages = new ArrayList<String>(0);
    successMessages = new ArrayList<String>(0);
    logger.logInfo(METHOD_NAME+" started...");
    try {
      
     if(sessionMap.containsKey("PAYMENT_DONE") && sessionMap.get("PAYMENT_DONE")!=null ){
      sessionMap.remove("PAYMENT_DONE");
      //if(true){
		UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		logger.logInfo("Contextpath is:"+contextPath);
		String loggedInUser = getLoggedInUser();
		BPMWorklistClient client = new BPMWorklistClient(contextPath);			
					
		//REMOVED FROM HERE

		Map taskAttributes =  userTask.getTaskAttributes();
		Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
		this.requestId = requestId.toString();
		//MOVED TO HERE
		//addPaymentSchedule(requestId);
		
		
		
		this.requestId  = requestId.toString();
		
		  RequestServiceAgent rSAgent = new RequestServiceAgent();
		  rSAgent.setRequestAsCompleted(requestId, getLoggedInUser());
	    notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_NOL_Completed);
		  CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId,noteOwner);
		client.completeTask(userTask, loggedInUser, TaskOutcome.COMPLETE);
		NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
		

		approveButton.setRendered(false);
		rejectButton.setRendered(false);
		sendButton.setRendered(false);
		populateContract.setRendered(false);
		completeRequest.setRendered(false);
        cancelRequest.setRendered(false);
        btnCollectPayment.setRendered(false);
        cancelButton.setRendered(true);
        viewRootMap.put("canAddAttachment",false);
		viewRootMap.put("canAddNote", false);
		
        //getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,null);
		successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_COMPLETE_SUCCESS));
    	logger.logInfo(METHOD_NAME+" completed successfully...");
      
      }else{
    	  
    	  errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.NOLREQUEST_COLLECT_FEE));
    	  viewRootMap.put("selectedTab",PAYMENT_TAB);
    	  return "";
          }
	} 
	catch(PIMSWorkListException ex)
	{
		logger.logError("Failed to approve due to:",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_COMLETE_FAILURE));
		outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
	}
    catch (Exception e) 
    {
		logger.LogException(METHOD_NAME + "crashed...", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_COMLETE_FAILURE));
	}
    outcome = "";
	return outcome;
  }

public String canceled(){
	String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
	String  METHOD_NAME = "canceled|";
	errorMessages = new ArrayList<String>(0);
    successMessages = new ArrayList<String>(0);
    logger.logInfo(METHOD_NAME+" started...");
    try {
    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		logger.logInfo("Contextpath is:"+contextPath);
		String loggedInUser = getLoggedInUser();
		BPMWorklistClient client = new BPMWorklistClient(contextPath);			
					
		//REMOVED FROM HERE

		Map taskAttributes =  userTask.getTaskAttributes();
		Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
		this.requestId = requestId.toString();
		
		
		
		
		this.requestId  = requestId.toString();
		RequestServiceAgent rSAgent = new RequestServiceAgent();
		  rSAgent.setRequestAsCanceled(requestId, getLoggedInUser());
		  
		  new PropertyService().setPaymentScheduleCanceled(requestId);
		  CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId,noteOwner);
		
		client.completeTask(userTask, loggedInUser, TaskOutcome.CANCEL);
		NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,MessageConstants.RequestEvents.REQUEST_CANCELED, requestId);
		
		
		approveButton.setRendered(false);
		sendButton.setRendered(false);
		rejectButton.setRendered(false);
		populateContract.setRendered(false);
		completeRequest.setRendered(false);
        cancelRequest.setRendered(false);
        btnCollectPayment.setRendered(false);
        cancelButton.setRendered(true);
        //getFacesContext().getExternalContext().getSessionMap().put(WebConstants.TASK_LIST_SELECTED_USER_TASK,null);
		successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_CANCEL_SUCCESS));
    	logger.logInfo(METHOD_NAME+" completed successfully...");
	} 
	catch(PIMSWorkListException ex)
	{
		logger.logError("Failed to approve due to:",ex);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_CANCEL_FAILURE));
		ex.printStackTrace();
		outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
	}
    catch (Exception e) 
    {
		logger.LogException(METHOD_NAME + "crashed...", e);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOL.MSG_REQUEST_CANCEL_FAILURE));
	}
    outcome = "";
	return outcome;
}

public HtmlCommandButton getCompleteRequest() {
	return completeRequest;
}

public void setCompleteRequest(HtmlCommandButton completeRequest) {
	this.completeRequest = completeRequest;
}

public HtmlCommandButton getCancelRequest() {
	return cancelRequest;
}

public void setCancelRequest(HtmlCommandButton cancelRequest) {
	this.cancelRequest = cancelRequest;

}

	private void CollectPayment()
	{
		String methodName="CollectPayment"; 
		logger.logInfo(methodName + "|" + "Start...");
		successMessages = new ArrayList<String>(0);
		
		try
		{
	  	     PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
		     PropertyServiceAgent psa=new PropertyServiceAgent();
		     RequestView rv   = null;
		     if(viewRootMap.containsKey(WebConstants.REQUEST_VIEW))
		     {
		    	 rv  = (RequestView) viewRootMap.get(WebConstants.REQUEST_VIEW);
			     prv.setPaidById(rv.getContractView().getTenantView().getPersonId());
		     }
		     prv = psa.collectPayments(prv);
		     if( rv !=null && rv.getRequestId()!=null )
		     NotesController.saveSystemNotesForRequest(WebConstants.ISSUE_NO_OBJECTION_LETTER,
		    		                MessageConstants.RequestEvents.REQUEST_COLLECT, rv.getRequestId());
		     //Refresh Payment Schedule list here
		     hdnContractId = (String) sessionMap.get("contractId");
		     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
		     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.ISSUE_NOL);
		     sessionMap.remove("contractId");
		     
		     if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_NEW))
		       sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_NEW);
		     getPaymentSchedules();
		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
		     
		     logger.logInfo(methodName + "|" + "Finish...");
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured..."+ex);
			
		}
	}
	public void btnPrintPaymentSchedule_Click()
	{
		
		PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
		CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.ISSUE_NOL);
		
	}
	@SuppressWarnings("unchecked")
	public String openReceivePaymentsPopUp()
	{
		viewMap.put("payments", viewRootMap.get("PAYMENT_SCHEDULES"));
		sessionMap.put("PAYMENT_SCHEDULES",paymentSchedules);
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,getPaymentSchedules());
	    PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	    sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	    openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");		
		return "";
	}
	
	private Long getNOLPaymentScheduleModeId() throws Exception
	{
		Long typeId = new Long(0);
	    Map hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
	 	if(hMap.containsKey("returnId"))
	 	{
	 			typeId = Long.parseLong(hMap.get("returnId").toString());
	 			if(typeId.equals(null))
	 				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
	 	}
		return typeId;
	}
	
	public String getHdnSendStatus() {
		return hdnSendStatus;
	}
	
	public void setHdnSendStatus(String hdnSendStatus) {
		this.hdnSendStatus = hdnSendStatus;
	}
	
	public String getErrorMessages() 
	{
		
	   return commUtil.getErrorMessages(errorMessages);
		
	}
	public String getSuccessMessages() 
	{
	
		return commUtil.getErrorMessages(successMessages);
	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}

	private ContractView getContractById(Long contractId) throws PimsBusinessException
	{
			ContractView contractView = new ContractView();
			ArrayList<ContractView> list;
			contractView.setContractId(contractId);
			
			HashMap contractHashMap=new HashMap();
			contractHashMap.put("contractView",contractView);
			
			SystemParameters parameters = SystemParameters.getInstance();			
			String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
			contractHashMap.put("dateFormat",dateFormat);
			PropertyServiceAgent psa =new PropertyServiceAgent();
			List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
			if(contractViewList.size()>0)
			contractView=(ContractView)contractViewList.get(0);
			viewRootMap.put("Contract_VIEW_FOR_PAYMENT", contractView);
		    DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE), WebConstants.COMMERCIAL_LEASE_CONTRACT);
		    
		    if(ddv.getDomainDataId().equals(contractView.getContractTypeId()))
		    viewRootMap.put("IS_CONTRACT_COMM", true);
			
		    return contractView;
	}

public void generateNotification(String eventType)
{
	   String methodName ="generateNotification";
	      Boolean success = false;
	            try
	            {
	            	logger.logInfo(methodName+"|Start");
	            	
	            	
	            	List<InquiryView> inquiryViewList = new ArrayList<InquiryView>(0);
	            	List<InquiryView> inquiryViewSelectedList = new ArrayList<InquiryView>(0);
	            	List<Long> personIdList= new ArrayList<Long>(0);
	            	List<PersonView> personViewList = new ArrayList<PersonView>();
	            	PersonView personView = new PersonView();
	            	ContractView contractView = new ContractView();
	            	PropertyServiceAgent psa =  new PropertyServiceAgent();
	            	HashMap placeHolderMap = new HashMap();
	                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
	                NotificationFactory nsfactory = NotificationFactory.getInstance();
	                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
	                Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
	        		
	            	if (viewRootMap.containsKey("APPLICANT_VIEW_FOR_NOTIFICATION")
	            		&&viewRootMap.containsKey("Contract_VIEW_FOR_PAYMENT")) {
	            		personView = (PersonView)viewRootMap.get("APPLICANT_VIEW_FOR_NOTIFICATION");
	            		contractView = (ContractView)viewRootMap.get("Contract_VIEW_FOR_PAYMENT");
	            		getNotificationPlaceHolder(event,contractView,personView);
	            		personsEmailList = getEmailContactInfos((PersonView)personView);
                        if(personsEmailList.size()>0)
                              notifier.fireEvent(event, personsEmailList);
	        			
	        		}
	                  
	                  success  = true;
	                  logger.logInfo(methodName+"|Finish");
	            }
	            catch(Exception ex)
	            {
	                  logger.LogException(methodName+"|Finish", ex);
	            }
	          
}
private List<ContactInfo> getEmailContactInfos(PersonView personView)
{
	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
	Iterator iter = personView.getContactInfoViewSet().iterator();
	HashMap previousEmailAddressMap = new HashMap();
	while(iter.hasNext())
	{
		ContactInfoView ciView = (ContactInfoView)iter.next();
		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
        {
			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
			//emailList.add(ciView);
        }
	}
	return emailList;
}
private void  getNotificationPlaceHolder(Event placeHolderMap,ContractView contractView,PersonView perView)
{
	String methodName = "getNotificationPlaceHolder";
	logger.logDebug(methodName+"|Start");
	//placeHolderMap.setValue("CONTRACT", contractView);
	placeHolderMap.setValue("PERSON_NAME",perView.getPersonFullName());
	placeHolderMap.setValue("CONTRACT_NUMBER",contractView.getContractNumber());
	
	logger.logDebug(methodName+"|Finish");
	
}

public HtmlCommandButton getPrintNol() {
	return printNol;
}

public void setPrintNol(HtmlCommandButton printNol) {
	this.printNol = printNol;
}

public String getSelectedTab() {
	return SelectedTab;
}

public void setSelectedTab(String selectedTab) {
	SelectedTab = selectedTab;
}

public HtmlPanelGrid getTbl_Action() {
	return tbl_Action;
}

public void setTbl_Action(HtmlPanelGrid tbl_Action) {
	this.tbl_Action = tbl_Action;
}

public String getTxtRemarks() {
	return txtRemarks;
}

public void setTxtRemarks(String txtRemarks) {
	this.txtRemarks = txtRemarks;
}

public String getNolTabReadOnlyMode(){
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	String applicationDetailsReadonlyMode = (String)viewMap.get("NOLTabReadOnlyMode");
	if(applicationDetailsReadonlyMode==null)
		applicationDetailsReadonlyMode = "";
	return applicationDetailsReadonlyMode;
}

public HtmlCommandButton getSendButtonWithoutTechComments() {
	return sendButtonWithoutTechComments;
}

public void setSendButtonWithoutTechComments(
		HtmlCommandButton sendButtonWithoutTechComments) {
	this.sendButtonWithoutTechComments = sendButtonWithoutTechComments;
}

public String printNolReport()
{

try {
		NOLTypeView nolTypeView=new NOLTypeView();
		String reportName;
		UtilityServiceAgent usa=new UtilityServiceAgent();
		if(viewRootMap.containsKey("NOL_TYPE_SELECTED VALUE"))
		strNolTypeId=viewRootMap.get("NOL_TYPE_SELECTED VALUE").toString();
		nolTypeView=usa.getNOLTypeById(new Long(strNolTypeId));
		reportName=nolTypeView.getNolReport();
		logger.logInfo("Report : "+reportName+ " Started.....");
		NOLReportCriteria=new NOLCriteria(reportName, ReportConstant.Processor.NOL_REPORT,CommonUtil.getLoggedInUser());
		NOLReportCriteria.setRequestId(this.requestId);
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, NOLReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
	} 
	catch (PimsBusinessException e)
	{
		e.printStackTrace();
	}
	return null;
}
private void openPopup(String javaScriptText) {
	logger.logInfo("openPopup() started...");
	try { 
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
		logger.logInfo("openPopup() completed successfully!!!");
	} catch (Exception exception) {
		logger.LogException("openPopup() crashed ", exception);
	}
}
public NOLCriteria getNOLReportCriteria() {
	return NOLReportCriteria;
}

public void setNOLReportCriteria(NOLCriteria reportCriteria) {
	NOLReportCriteria = reportCriteria;
}

public boolean isContractCommercial() {
	return isContractCommercial;
}

public void setContractCommercial(boolean isContractCommercial) {
	this.isContractCommercial = isContractCommercial;
}

public List<CommercialActivityView> getCommercialActivityList()
{
	
	commercialActivityList=null;
 	if(commercialActivityList==null || commercialActivityList.size()<=0)
 	{
 	  if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
 		commercialActivityList=(ArrayList)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
 	 
 	}
   return commercialActivityList;
}

public void setCommercialActivityList(List<CommercialActivityView> commercialActivityList) 
    {
	this.commercialActivityList = commercialActivityList;
	}
		


public HtmlDataTable getCommercialActivityDataTable() {
	return commercialActivityDataTable;
}

public void setCommercialActivityDataTable(
		HtmlDataTable commercialActivityDataTable) {
	this.commercialActivityDataTable = commercialActivityDataTable;
}

public CommercialActivityView getCommercialActivityItem() {
	return commercialActivityItem;
}

public void setCommercialActivityItem(
		CommercialActivityView commercialActivityItem) {
	this.commercialActivityItem = commercialActivityItem;
}

public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkCommercial() {
	return chkCommercial;
}

public void setChkCommercial(
		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial) {
	this.chkCommercial = chkCommercial;
}
public void tabCommercialActivity_Click()
{
	String methodName="tabCommercialActivity_Click";
	logger.logInfo(methodName+"|"+"Start...");
	try
	{
				if(!viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
			   CommercialActivityController.getAllCommercialActivities(null);
	    //sessionMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityList);
	}
	catch (Exception ex)
	{
		logger.LogException(methodName+"|"+"Exception Occured...",ex);
	}
	logger.logInfo(methodName+"|"+"Finish...");
}
private List<CommercialActivityView> getAllCommercialActivities() throws PimsBusinessException
{
	String methodName="getAllCommercialActivities";
	logger.logInfo(methodName+"|"+"Start...");
    PropertyServiceAgent psa=new PropertyServiceAgent();
    List<CommercialActivityView> commercialActivityViewList =psa.getAllCommercialActivities(null);
    viewRootMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityViewList);
	logger.logInfo(methodName+"|"+"Finish...");
	return commercialActivityViewList;
}

public org.richfaces.component.html.HtmlTab getTabCommercialActivity() {
	return tabCommercialActivity;
}

public void setTabCommercialActivity(
		org.richfaces.component.html.HtmlTab tabCommercialActivity) {
	this.tabCommercialActivity = tabCommercialActivity;
}

public HtmlCommandButton getSaveButton() {
	return saveButton;
}

public void setSaveButton(HtmlCommandButton saveButton) {
	this.saveButton = saveButton;
}

public List<ContractActivityView> getContractActivityList() {
	return contractActivityList;
}

public void setContractActivityList(
		List<ContractActivityView> contractActivityList) {
	this.contractActivityList = contractActivityList;
}

public void btnEditPaymentSchedule_Click()
{
	String methodName = "btnEditPaymentSchedule_Click";
	logger.logInfo(methodName+"|"+" Start...");
	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	if(viewRootMap.containsKey("PAYMENT_SCHEDULES"))
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get("PAYMENT_SCHEDULES"));
	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
	openPopUp("", "javascript:openPopupEditPaymentSchedule();");
	logger.logInfo(methodName+"|"+" Finish...");
}



public Boolean getEditPaymentEnable() {
	if(viewRootMap.containsKey("EDIT_ENABLE"))
		editPaymentEnable = (Boolean)viewRootMap.get("EDIT_ENABLE");
	return editPaymentEnable;
}

public void setEditPaymentEnable(Boolean editPaymentEnable) {
	this.editPaymentEnable = editPaymentEnable;
	if(this.editPaymentEnable !=null)
		viewRootMap.put("EDIT_ENABLE",this.editPaymentEnable);
}
	private void notifyRequestStatus(String notificationType)
	{
		Long reqId = new Long(this.getRequestId());
		//reqId = new Long(this.requestId);
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		RequestView requestView = null;
		ContractView contView = null;
		try
		{
			requestView = psAgent.getRequestById(reqId);
			if(requestView != null)
			{
				contView = requestView.getContractView();
				if(contView != null)
				{
					PersonView applicant = requestView.getApplicantView();
					PersonView tenant = null;
					tenant = contView.getTenantView();
					
					if(applicant!=null)
					{
						List<ContactInfo> contactInfoList = getContactInfoList(applicant);
						NOLTypeView nolTypeView=new NOLTypeView();
						UtilityServiceAgent usa=new UtilityServiceAgent();
						if(viewRootMap.containsKey("NOL_TYPE_SELECTED VALUE"))
							strNolTypeId=viewRootMap.get("NOL_TYPE_SELECTED VALUE").toString();
						nolTypeView=usa.getNOLTypeById(new Long(strNolTypeId));
						
						eventAttributesValueMap.put("REQUEST_NUMBER", requestView.getRequestNumber());
						eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
						eventAttributesValueMap.put("PROPERTY_NAME", this.getTxtpropertyName().getValue().toString());
						eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum().getValue().toString());
						eventAttributesValueMap.put("NOL_TYPE", nolTypeView.getDescriptionEn());
						eventAttributesValueMap.put("NOL_REASON", this.getReasontForNol().getValue().toString());
						eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
															
						generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
						
						if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
						{
							contactInfoList = getContactInfoList(tenant);
							eventAttributesValueMap.remove("APPLICANT_NAME");
							eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
							generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
						}
					}
					
					
				}
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("notify request status crashed",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()   {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	
}