package com.avanza.pims.web.backingbeans;


import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.DuePaymentsPerTenantReportCriteria;


import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;

public class DuePaymentsPerTenantReportBackingBean extends AbstractController
{
	private static final long serialVersionUID = 1L;
	private DuePaymentsPerTenantReportCriteria reportCriteria; 
	
	
	public DuePaymentsPerTenantReportBackingBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new DuePaymentsPerTenantReportCriteria(ReportConstant.Report.DUE_PAYMENTS_PER_TENANT_EN, ReportConstant.Processor.DUE_PAYMENTS_PER_TENANT, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new DuePaymentsPerTenantReportCriteria(ReportConstant.Report.DUE_PAYMENTS_PER_TENANT_AR, ReportConstant.Processor.DUE_PAYMENTS_PER_TENANT, CommonUtil.getLoggedInUser());
	}

	public DuePaymentsPerTenantReportCriteria getReportCriteria() {
		return reportCriteria;
	}
	public void setReportCriteria(DuePaymentsPerTenantReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}
		 private static Logger logger = Logger.getLogger("DuePaymentsPerTenantReportBackingBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

		
	public String cmdView_Click() {
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
}
