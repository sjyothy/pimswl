package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.CaringType;
import com.avanza.pims.entity.CaringTypeUserGroupMapping;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ProcedureTaskGroup;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.pims.ws.vo.GroupView;
import com.avanza.pims.ws.vo.ProcedureGroupsView;

public class CaringTypeGroups extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private HtmlDataTable dataTable;
	private HtmlSelectOneMenu selectOneCaringType = new HtmlSelectOneMenu();

	private ArrayList<CaringTypeView> dataList = new ArrayList();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	@Override
	public void init() {
		super.init();
		try {
			showAll();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void prerender() {
		super.prerender();
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlSelectOneMenu getSelectOneCaringType() {
		return selectOneCaringType;
	}

	public void setSelectOneCaringType(HtmlSelectOneMenu selectOneCaringType) {
		this.selectOneCaringType = selectOneCaringType;
	}

	public ArrayList<CaringTypeView> getDataList() {
		if(viewMap.get("CARING_TYPE_LIST") != null)
			return (ArrayList<CaringTypeView> )viewMap.get("CARING_TYPE_LIST") ;
			
		return dataList;
	}

	public void setDataList(ArrayList<CaringTypeView> dataList) {
		
		
		this.dataList = dataList;
		if(this.dataList != null )
			viewMap.put("CARING_TYPE_LIST", this.dataList);
		
	}

	public void buttonSearch() throws PimsBusinessException {

		dataList.clear();
		if (getSelectOneCaringType().getValue() != null) {
			Long value = Long.parseLong(getSelectOneCaringType().getValue()
					.toString());

			if (value == -1) {
				showAll();
				
			} else {
				CaringType caringType = EntityManager.getBroker().findById(
						CaringType.class, value);
				CaringTypeView caringTypeView = fillCaringTypeView(caringType);

				//if (caringType.getCaringTypeUserGroupMappings().size() > 0) {
					dataList.add(caringTypeView);
				//}
			}
			if(this.dataList !=  null)
				this.setDataList(this.dataList);
			
		}
	}

	public void showAll() throws PimsBusinessException {

		CaringTypeView caringTypeView;
		List<CaringType> caringTypeList = EntityManager.getBroker().findAll(
				CaringType.class);
		for (CaringType caringType : caringTypeList) {
			caringTypeView = fillCaringTypeView(caringType);
			dataList.add(caringTypeView);
		}

		viewMap.remove("CARING_TYPE_LIST"); 
	}

	public CaringTypeView fillCaringTypeView(CaringType caringType)
			throws PimsBusinessException {
		CaringTypeView caringTypeView = new CaringTypeView();
		caringTypeView
				.setCaringTypeNameEn(caringType.getCaringTypeNameEn());
		caringTypeView
		.setCaringTypeNameAr(caringType.getCaringTypeNameAr());
		caringTypeView.setUserGroups(getUserGroupString(caringType
				.getCaringTypeUserGroupMappings()));
		caringTypeView.setCaringTypeUserGroupMappings(caringType.getCaringTypeUserGroupMappings());
		
		caringTypeView.setCaringTypeId(caringType.getCaringTypeId());
		return caringTypeView;

	}

	private String getUserGroupString(Set userGroups)
			throws PimsBusinessException {
		String userGroupString = "";
		CaringTypeUserGroupMapping caringTypeUserGroupMapping = null;
		Object[] userGroupsArray = userGroups.toArray();

		HashMap<String, GroupView> groupMap = new UtilityService()
				.getGroupMap();
		GroupView groupView = null;

		if (userGroups.size() > 0) {
			for (int count = 0; count < userGroupsArray.length; count++) {
				caringTypeUserGroupMapping = (CaringTypeUserGroupMapping) userGroupsArray[count];

				groupView = groupMap.get(caringTypeUserGroupMapping
						.getSecUserGroup());
				userGroupString = userGroupString
						+ groupView.getGroupPrimaryName();

				if (count < userGroupsArray.length - 1) {
					userGroupString = userGroupString + " - ";
				}
			}
		}

		return userGroupString;
	}

	public void editLinkClick(javax.faces.event.ActionEvent event) {
		logger.logInfo("selectProcedureTask() started...");

		try {
	
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String javaScriptText = "javascript:showCaringTypeGroupsMappingPopup();";
			//CARING_TYPE_LIST
			CaringTypeView selectedCaringTypeView = (CaringTypeView) dataTable.getRowData();
			List<UserGroup> caringTypeGroups = new ArrayList<UserGroup>();
			
			sessionMap.put("selectedCaringTypeView", selectedCaringTypeView);
			// Add the Javascript to the rendered page's header for
			// immediate execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("selectProcedureTask() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("selectProcedureTask() crashed ", exception);
		}

	}
	
}