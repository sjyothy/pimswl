package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.services.*;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.*;
import com.avanza.pims.ws.vo.*;
import com.avanza.ui.util.ResourceUtil;

/**
 * SecUserGroup generated by MyEclipse Persistence Tools
 */

public class FeeConfigurationList extends AbstractController {

	// Fields
	private static Logger logger = Logger.getLogger(FeeConfigurationList.class);

	private HtmlDataTable dataTable;
	private FeeConfigurationView dataItem = new FeeConfigurationView();
	private ArrayList<FeeConfigurationView> dataList;
	private HtmlInputHidden tenantIdHidden = new HtmlInputHidden();
	private HtmlInputHidden hdnModeSelectOnePopUp = new HtmlInputHidden();
	private HtmlInputHidden hdnModeSetup = new HtmlInputHidden();
	private String hdnMode;

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String selectOneProcedureType;
	private String selectOneFeeType;
	private String accountNo;
	private String percentage;
	private String selectOnebasedOn;
	private String minAmount;
	private String maxAmount;

	// private String mode;
	private String modeSetup = "Mode_Setup";
	private String modeSelectOnePopUp = "Mode_Select_One_Pop_Up";

	private Boolean isModeSetup;
	private Boolean isModeSelectOnePopUp;

	private HtmlInputText txtTenantNumber = new HtmlInputText();
	private HtmlInputText txtTenantName = new HtmlInputText();
	private HtmlInputText txtTenantPassportNumber = new HtmlInputText();
	private HtmlInputText txtTenantVisaNumber = new HtmlInputText();
	private HtmlInputHidden addCount = new HtmlInputHidden();
	boolean isArabicLocale;
	boolean isEnglishLocale;
	TenantView tenantsView = new TenantView();
	FacilityView facilityView = new FacilityView();
	FeeConfigurationView feeConfigurationView = new FeeConfigurationView();
	FacesContext context = getFacesContext();

	private String sortField = null;
	private boolean sortAscending = true;
	
	private List<String> errorMessages;

	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		
		if (!isPostBack()) {

			Map request = context.getExternalContext().getRequestMap();
			if (dataList == null || dataList.size()==0)
    		{
    			 
    		//dataList=loadDataList(); 
    		}
		}
	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	public void preprocess() {

		getIsModeSelectOnePopUp();
		getIsModeSetup();
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	public void prerender() {

		getIsModeSelectOnePopUp();
		getIsModeSetup();
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	public void destroy() {
		;
	}

	// Actions
	// -----------------------------------------------------------------------------------

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters
	// -----------------------------------------------------------------------------------


	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters
	// -----------------------------------------------------------------------------------


	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers
	// -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	/** default constructor */
/*	public FeeConfigurationList() {
		loadDataList();

		addCount.setValue(0);

	}*/

	// Property accessors

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public List<FeeConfigurationView> getGridDataList() {


		 Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			if (dataList == null || dataList.size()==0)
			{
				if(viewMap.containsKey("feeConfigList"))	
					  dataList= (ArrayList)viewMap.get("feeConfigList");
				//else
				//	loadDataList();
			}
		
		return dataList;
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	private void putControlValuesInView() {

		if (selectOneProcedureType != null
				&& !selectOneProcedureType.equals("")
				&& !selectOneProcedureType.equals("-1"))
			feeConfigurationView.setProcedureTypeId(new Long(
					selectOneProcedureType));
		if (selectOneFeeType != null && !selectOneFeeType.equals("")
				&& !selectOneFeeType.equals("-1"))
			feeConfigurationView.setFeeType(new Long(selectOneFeeType));
		if (accountNo != null && !accountNo.equals(""))
			feeConfigurationView.setGrpAccountNo(accountNo);
		if (percentage != null && !percentage.equals(""))
			feeConfigurationView.setPercent(new Double(percentage));
		if (selectOnebasedOn != null && !selectOnebasedOn.equals("")
				&& !selectOnebasedOn.equals("-1"))
			feeConfigurationView.setBasedOnTypeId(new Long(selectOnebasedOn));
		if (minAmount != null && !minAmount.equals(""))
			feeConfigurationView.setMinAmount(new Double(minAmount));
		if (maxAmount != null && !maxAmount.equals(""))
			feeConfigurationView.setMaxAmount(new Double(maxAmount));

	}

	public String btnSearch_Click() {
		String sMethod = "btnSearch_Click";
		String eventOutCome = "failure";

		logger.logInfo("btnSearch_Click Starts:::::::::::::::::");
		
		putControlValuesInView();
		

		loadDataList();
		logger.logInfo("btnSearch_Click Ends:::::::::::::::::");
		return eventOutCome;

	}

	private ArrayList<FeeConfigurationView> loadDataList() {

		String sMethod = "loadDataList";

		logger.logInfo(sMethod+" Starts::::::::::::::");
		dataList = new ArrayList<FeeConfigurationView>();
		List<FeeConfigurationView> list = new ArrayList<FeeConfigurationView>();
		boolean isSucceed = false;
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		try {
			PropertyServiceAgent myPort = new PropertyServiceAgent();
			list = myPort.getFeeConfiguration(feeConfigurationView);
			
			dataList = (ArrayList<FeeConfigurationView>) list;
			
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("feeConfigList", dataList);
			
			logger.logInfo("Record Found In Load Data List : %s",dataList.size());
	        
	        if (dataList.size()==0){
	        	errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
	        	
	        }
			
			recordSize = dataList.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
           
			logger.logInfo(sMethod+" Ends::::::::::::::");
			isSucceed = true;
		} catch (Exception ex) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.logInfo(sMethod+" Crashed::::::::::::::");
			ex.printStackTrace();
		}
	 return dataList;

	}

	private String rectifyValues(String input) {
		String output = input;
		output = output.replace("'", "''");

		return output;
	}

	public void sendTenantInfoToContractInRequest(
			javax.faces.event.ActionEvent event) {
		// Write your server side code here
		final String viewId = "/TenantsList.jsp";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		// Your Java script code here

		// onclick="javascript:sendToParent('#{dataItem.tenantId}','#{dataItem.tenantNumber}','#{dataItem.typeId}');"
		TenantView tenantViewRow = (TenantView) dataTable.getRowData();
		String tenantNames = "";
		if (tenantViewRow.getFirstName() != null)
			tenantNames = tenantViewRow.getFirstName()
					+ tenantViewRow.getMiddleName()
					+ tenantViewRow.getLastName();
		if (tenantNames.trim().length() <= 0)
			tenantNames = tenantViewRow.getCompanyName();
		String javaScriptText = "window.opener.populateTenant(" + "'"
				+ tenantViewRow.getTenantId().toString() + "'," + "'"
				+ tenantViewRow.getTenantNumber().toString() + "'," + "'"
				+ tenantViewRow.getTypeId().toString() + "'," + "'"
				+ tenantNames + "'" + ");"
				+ "window.opener.document.forms[0].submit();"
				+ "window.close();";

		// '"+tenantViewRow.getTenantId().toString()+"'," +
		// "'"+tenantViewRow.getTenantNumber().toString()+"'," +
		// "'"+tenantViewRow.getTypeId().toString()+"'"
		// +");";
		Map sessionMap = facesContext.getExternalContext().getSessionMap();
		sessionMap.put("TENANTINFO", tenantViewRow);
		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);

		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

	}

	// Setters
	// -----------------------------------------------------------------------------------

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	// Public Methods

	// Actions
	// -----------------------------------------------------------------------------------
	public String editDataItem() {

		// dataItem = (SecUserGroupData) dataTable.getRowData();
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap()
				.put("passportNumber", "abc");

		return "edit";
	}

	public String reset() {
		return "success";
	}


	public String saveGroup() {

		// getPojo and populate it with the values in backing bean come from the
		// Form
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.avanza.pims.web.messageresource.Messages");
		String msg = "";

		msg = bundle.getString("commons.save");

		context.addMessage(null, new FacesMessage(msg));

		return "success";

	}

	// Private Methods

	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public HtmlInputHidden getTenantIdHidden() {
		return tenantIdHidden;
	}

	public void setTenantIdHidden(HtmlInputHidden tenantIdHidden) {
		this.tenantIdHidden = tenantIdHidden;
	}

	public HtmlInputText getTxtTenantNumber() {
		return txtTenantNumber;
	}

	public void setTxtTenantNumber(HtmlInputText txtTenantNumber) {
		this.txtTenantNumber = txtTenantNumber;
	}

	public HtmlInputText getTxtTenantName() {
		return txtTenantName;
	}

	public void setTxtTenantName(HtmlInputText txtTenantName) {
		this.txtTenantName = txtTenantName;
	}

	public HtmlInputText getTxtTenantPassportNumber() {
		return txtTenantPassportNumber;
	}

	public void setTxtTenantPassportNumber(HtmlInputText txtTenantPassportNumber) {
		this.txtTenantPassportNumber = txtTenantPassportNumber;
	}

	public HtmlInputText getTxtTenantVisaNumber() {
		return txtTenantVisaNumber;
	}

	public void setTxtTenantVisaNumber(HtmlInputText txtTenantVisaNumber) {
		this.txtTenantVisaNumber = txtTenantVisaNumber;
	}

	public FeeConfigurationView getDataItem() {
		return dataItem;
	}

	public void setDataItem(FeeConfigurationView dataItem) {
		this.dataItem = dataItem;
	}

	public HtmlInputHidden getHdnModeSelectOnePopUp() {
		return hdnModeSelectOnePopUp;
	}

	public void setHdnModeSelectOnePopUp(HtmlInputHidden hdnModeSelectOnePopUp) {
		this.hdnModeSelectOnePopUp = hdnModeSelectOnePopUp;
	}

	public HtmlInputHidden getHdnModeSetup() {
		return hdnModeSetup;
	}

	public void setHdnModeSetup(HtmlInputHidden hdnModeSetup) {
		this.hdnModeSetup = hdnModeSetup;
	}

	public String cmdDelete_Click() {
		// Write your server side code here
		// fromDelete = true;
		FeeConfigurationView feeConfigView = (FeeConfigurationView) dataTable
				.getRowData();
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			feeConfigView.setIsDeleted(1L);
			psa.addFeeConfiguration(feeConfigView);

			loadDataList();

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}
		return "delete";
	}

	public String cmdStatus_Click() {
		// Write your server side code here
		// fromDelete = true;
		final String viewId = "/FacilityList.jsp";
		FacilityView facilityView = (FacilityView) dataTable.getRowData();
		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			if (facilityView.getRecordStatus() == 1) {
				facilityView.setRecordStatus(0L);
			} else {
				facilityView.setRecordStatus(1L);
			}
			psa.addFacility(facilityView);

			loadDataList();

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}

		return "Status";
	}

	public String cmdEdit_Click() {

		FeeConfigurationView feeConfig = (FeeConfigurationView) dataTable
				.getRowData();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			facesContext.getExternalContext().getRequestMap().put(
					"feeConfigurationId", feeConfig.getFeeConfigurationId());
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Exception " + e);
		}

		return "AddFeeConfiguration";

	}

	public void setIsModeSelectOnePopUp(Boolean isModeSelectOnePopUp) {
		this.isModeSelectOnePopUp = isModeSelectOnePopUp;
	}

	public Boolean getIsModeSelectOnePopUp() {
		if (hdnMode != null && hdnMode.equals(modeSelectOnePopUp))
			isModeSelectOnePopUp = true;
		else
			isModeSelectOnePopUp = false;

		return isModeSelectOnePopUp;
		// return true;
	}

	public Boolean getIsModeSetup() {
		if (hdnMode != null) {
			if (hdnMode.equals(modeSetup))
				isModeSetup = true;
			else
				isModeSetup = false;
		} else
			isModeSetup = true;
		return isModeSetup;
	}

	public void setIsModeSetup(Boolean isModeSetup) {
		this.isModeSetup = isModeSetup;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String btnAddFacility_Click() {
		return "AddFacility";

	}

	public String getSelectOneFeeType() {
		return selectOneFeeType;
	}

	public void setSelectOneFeeType(String selectOneFeeType) {
		this.selectOneFeeType = selectOneFeeType;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}

	public String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getSelectOnebasedOn() {
		return selectOnebasedOn;
	}

	public void setSelectOnebasedOn(String selectOnebasedOn) {
		this.selectOnebasedOn = selectOnebasedOn;
	}

	public String getSelectOneProcedureType() {
		return selectOneProcedureType;
	}

	public void setSelectOneProcedureType(String selectOneProcedureType) {
		this.selectOneProcedureType = selectOneProcedureType;
	}

	public String btnAddFee_Click() {
		return "AddFee";

	}

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	
	public String getErrorMessages() {
		StringBuilder sb = new StringBuilder();
		for (String msg: this.errorMessages)
			sb.append(msg).append("\n");
		
		return sb.toString();
	}	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }


}