package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.servicecontract.GeneratePayments.Keys;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab.ViewRootKeys;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;

public class GeneratePayments extends AbstractController {

	public Double amount;
	public Double deposit;
	
	public String numberOfCashPayments;
	public String numberOfChqPayemts;
	public String totalNumberOfPayments;
	private Date contractStartDate;
	private Date contractEndDate;
	public Date startDate;
	public Date today;
	
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	public String dateformat = "";
	private HtmlDataTable dataTable;
	private ResourceBundle bundle;
	Map sessionMap;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private List<String> errorMessages = new ArrayList();
	private static Logger logger = Logger.getLogger(GeneratePayments.class);
	HttpServletRequest request =
			 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	private Map<String,Object> viewMap;
		
	public String getErrorMessages() {
		
		return CommonUtil.getErrorMessages(errorMessages);
		
	}
	public LocaleInfo getLocaleInfo()
	{
	    	
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	
	     return localeInfo;
		
	}
	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}	
	

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		sessionMap =context.getExternalContext().getSessionMap();
		
		viewMap = (Map<String,Object>) FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		getDateformat();
		
		
		if (!isPostBack())
		{
			try {
			
				//Setting the Values from Request
				if(request.getParameter("ownerShipTypeId")!=null)
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.OWNWERSHIP_TYPE_ID, request.getParameter("ownerShipTypeId"));
				
				
				amount = new Double(sessionMap.get(WebConstants.TOTAL_CONTRACT_VALUE).toString());
				
				
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.GEN_PAY_CONTRACT_START_DATE,
						                         sessionMap.get(WebConstants.GEN_PAY_CONTRACT_START_DATE));
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.GEN_PAY_CONTRACT_END_DATE,
						                         sessionMap.get(WebConstants.GEN_PAY_CONTRACT_END_DATE));
				
				
				if (sessionMap.get(WebConstants.GEN_PAY_UPDATE_PAYMENT)!=null) {
					viewMap.put(WebConstants.GEN_PAY_UPDATE_PAYMENT,sessionMap.get(WebConstants.GEN_PAY_UPDATE_PAYMENT));
					//clean up update payment flag
					sessionMap.remove(WebConstants.GEN_PAY_UPDATE_PAYMENT);
				}
				
				sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
				sessionMap.remove(WebConstants.TOTAL_CONTRACT_VALUE);
				
				today = new Date();
				startDate = new Date();
				
				startDate = (new SimpleDateFormat("dd/MM/yyyy")).parse(
				           FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_START_DATE).toString()
				   );
				
				//Getting SystemConfigration Values
				String cash = psa.getSystemConfigurationValueByKey(WebConstants.SYS_CONFIG_KEY_NNUMBER_OF_CASH_PAYMENTS);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.SYS_CONFIG_KEY_NNUMBER_OF_CASH_PAYMENTS,cash);
				String totalPayments = psa.getSystemConfigurationValueByKey(WebConstants.SYS_CONFIG_KEY_NUMBER_OF_PAYMENTS_FOR_RENEW_CONTRACT);
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.SYS_CONFIG_KEY_NUMBER_OF_PAYMENTS_FOR_RENEW_CONTRACT,totalPayments );
				populateChqCashPayments();
				
				List<PaymentScheduleView> paymentSchedulesOld = (List<PaymentScheduleView>) sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				if(paymentSchedulesOld!=null)
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, paymentSchedulesOld);
				
			} catch (PimsBusinessException pbe) {
				// TODO Auto-generated catch block
				pbe.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		getIsEnglishLocale();
	}
	
	
	@Override
	public void prerender() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try
		{
	       this.setContractStartDate(df.parse(
			               FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_START_DATE).toString()
			               ));
	       this.setContractEndDate(df.parse(
	                       FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_END_DATE).toString()
	                       ));
	       
		  
		}
		catch(Exception ex)
		{
			logger.LogException("prerender|Error Occured ..",ex);
		}
	}
    private void populateChqCashPayments() {
		
		String cash = FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.SYS_CONFIG_KEY_NNUMBER_OF_CASH_PAYMENTS).toString() ;
		
		String totalPayments =FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.SYS_CONFIG_KEY_NUMBER_OF_PAYMENTS_FOR_RENEW_CONTRACT).toString() ;
		numberOfCashPayments = cash;
		Integer temp = Integer.parseInt(totalPayments) - Integer.parseInt(cash);
		numberOfChqPayemts = temp.toString();
	}
    public void btnReset_Click()
    {
    	populateChqCashPayments();
    	startDate = new Date();
    	
    	
    	
		try {
			startDate = (new SimpleDateFormat("dd/MM/yyyy")).parse(
			           FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_START_DATE).toString()
			   );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	
    	FacesContext.getCurrentInstance().getViewRoot().getAttributes().remove("payments"); 
    	paymentSchedules = new ArrayList<PaymentScheduleView>();
    	
    	
    }
    public boolean getIsAddToContractBtnShow()
    {
    	boolean IsAddToContractBtnShow=true;
    	if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments") != null && 
    		((ArrayList<PaymentScheduleView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments")).size()>0)
    		IsAddToContractBtnShow=true;
    	else
    		IsAddToContractBtnShow=false;
    	return IsAddToContractBtnShow;
    	
    }
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	
	public boolean getIsArabicLocale()
	{
		
		
		isArabicLocale = !getIsEnglishLocale();
		
		
		
		return isArabicLocale;
	}
	
	public String getDateformat() {
		
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	dateformat= localeInfo.getDateFormat();
		return dateformat;
	}
	@SuppressWarnings("unchecked")
	public String generatePayments()
	{
		try {
			
			
			if (!hasErrors())
			{
							
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				this.setContractStartDate(df.parse(
			               FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_START_DATE).toString()
			               ));
	            this.setContractEndDate(df.parse(
	                       FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.GEN_PAY_CONTRACT_END_DATE).toString()
	                       ));
				String desc = getBundle().getString(MessageConstants.GeneratePayments.RENT_DESCRIPTION);
				Long ownerShipTypeId =new Long(FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.OWNWERSHIP_TYPE_ID).toString());
				paymentSchedules = psa.generatePayments(amount,new Integer(numberOfCashPayments),new Integer(numberOfChqPayemts),startDate,
						                  desc,ownerShipTypeId,contractStartDate,this.getContractEndDate());
				
				
				
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("payments", paymentSchedules);
				
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			logger.LogException("GeneartePatymentSCrashed", e);
			
			e.printStackTrace();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.LogException("GeneartePatymentSCrashed", e);
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			logger.LogException("GeneartePatymentSCrashed", e);
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String sendToContract()
	{
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments") != null)
			paymentSchedules = (List<PaymentScheduleView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments");
		
		List<PaymentScheduleView> paymentSchedulesOld = (List<PaymentScheduleView>) sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		paymentSchedulesOld = (List<PaymentScheduleView>)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		if (paymentSchedulesOld != null)
		{
		
			for (int i=0; i<paymentSchedulesOld.size(); i++)
			{
				if (paymentSchedulesOld.get(i).getTypeId().longValue() != Constant.PAYMENT_TYPE_RENT_ID.longValue()) {
					paymentSchedules.add(paymentSchedulesOld.get(i));
				}
				
				else { // if the payment is rent
					if (viewMap.get(WebConstants.GEN_PAY_UPDATE_PAYMENT)!=null) { // and the flag is update
						PaymentScheduleView paymentSchedule = paymentSchedulesOld.get(i);
						paymentSchedule.setIsDeleted(WebConstants.IS_DELETED_TRUE);
						paymentSchedule.setRecordStatus(WebConstants.DELETED_RECORD_STATUS);
						paymentSchedules.add(paymentSchedule);
					}	
				}
				
				
				
			}
			
			
		}
		

		
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		
		
		
		//Close and Submit ////
		closeAndSubmit();
		///////////////////////
		
		return "";
	}
	
	public String closeAndSubmit()
	{
		String methodName="closeAndSubmit";
        final String viewId = "/GeneratePayments.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =" window.opener.document.forms[0].submit();" +
        					    "window.close();";
        
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	private boolean hasErrors()throws Exception
	{
		String methodName="hasErrors";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean isError=false;
		
		errorMessages=new ArrayList<String>();
    	errorMessages.clear();
    	
    	if (numberOfCashPayments == null || numberOfCashPayments.equals(""))
    	{
	    	errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_CASH));
			isError = true;
    	}
    	else
    	{
    		try{
    			
    			Integer a = new Integer(numberOfCashPayments);
    			
    		}catch (Exception e)
    		{
    			errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_CASH));
    			isError = true;
    		}
    	}
    	
    	if (numberOfChqPayemts == null || numberOfChqPayemts.equals(""))
    	{
	    	errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_CHQ));
			isError = true;
    	}
    	else
    	{
    		try{
    			
    			Integer a = new Integer(numberOfChqPayemts);
    			
    		}catch (Exception e)
    		{
    			errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_CHQ));
    			isError = true;
    		}
    	}
    	
//    	if (!isError )
//    	{
//    		Double total = Double.parseDouble(numberOfChqPayemts) + Double.parseDouble(numberOfCashPayments);
//    		if(total.compareTo(Double.parseDouble(reqInstallments))!=0)
//    		{
//	    	  errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_CASH_CHQ_EQUAL_INSTALLMENTS));
//			  isError = true;
//    		}
//    	}
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	Date contractStartDate   = df.parse( FacesContext.getCurrentInstance().getViewRoot().
                getAttributes().get(WebConstants.GEN_PAY_CONTRACT_START_DATE).
                toString());
		Date contractEndDate = df.parse(  FacesContext.getCurrentInstance().getViewRoot().
					                      getAttributes().get(WebConstants.GEN_PAY_CONTRACT_END_DATE).
					                      toString());
		    	
    	if (startDate == null)
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_DATE));
			isError = true;
    	}
    	else if(startDate.compareTo(contractStartDate)<0 )
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_PAYMENTSTARTDATE_LESSTHAN_CONTRACTSTARTDATE));
			isError = true;
    		
    		
    	}    	
    	else if(startDate.compareTo(contractEndDate)>0)
    	{
    		errorMessages.add(getBundle().getString(MessageConstants.GeneratePayments.MSG_PAYMENTSTARTDATE_GREATERTHAN_CONTRACTENDDATE));
			isError = true;
    		
    		
    	}
    	
    	return isError;
	}
	
	public ResourceBundle getBundle() {
		
		String methodName="getBundle";
    	logger.logInfo(methodName+"|"+" Start");

        if(getIsArabicLocale())

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

        else

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

        logger.logInfo(methodName+"|"+" Finish");
        
        return bundle;

	}
	
	private ArrayList<PaymentScheduleView> dataList ;
	
	public ArrayList<PaymentScheduleView> getDataList() {
		return dataList;
	}
	public void setDataList(ArrayList<PaymentScheduleView> dataList) {
		this.dataList = dataList;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
	public String getNumberOfCashPayments() {
		return numberOfCashPayments;
	}
	public void setNumberOfCashPayments(String numberOfCashPayments) {
		this.numberOfCashPayments = numberOfCashPayments;
	}
	public String getNumberOfChqPayemts() {
		return numberOfChqPayemts;
	}
	public void setNumberOfChqPayemts(String numberOfChqPayemts) {
		this.numberOfChqPayemts = numberOfChqPayemts;
	}
	public String getTotalNumberOfPayments() {
		return totalNumberOfPayments;
	}
	public void setTotalNumberOfPayments(String totalNumberOfPayments) {
		this.totalNumberOfPayments = totalNumberOfPayments;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public Date getToday() {
		return today;
	}
	public void setToday(Date today) {
		this.today = today;
	}
	public List<PaymentScheduleView> getPaymentSchedules() {
		
		if (FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments") != null)
			paymentSchedules = (List<PaymentScheduleView>) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get("payments");
		
		return paymentSchedules;
	}
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	public boolean isArabicLocale() {
		return isArabicLocale;
	}
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	public boolean isEnglishLocale() {
		return isEnglishLocale;
	}
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	
	public TimeZone getTimeZone()

	{
	       return TimeZone.getDefault();
	}

	public Date getContractStartDate() {
		
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
		
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	
	
}
