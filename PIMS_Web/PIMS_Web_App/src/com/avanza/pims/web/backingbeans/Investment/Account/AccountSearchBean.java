package com.avanza.pims.web.backingbeans.Investment.Account;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AccountServiceAgent;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.investment.AssetService;
import com.avanza.pims.ws.vo.AccountView;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ViolationView;

public class AccountSearchBean extends AbstractController {

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	private static Logger logger = Logger.getLogger(AccountSearchBean.class);
	private List<AccountView> dataList;
	AccountView accountView = new AccountView();
	AccountServiceAgent accountServiceAgent = new AccountServiceAgent();
	private HtmlDataTable tbl_AccountSearch;
	private AccountView dataItem = new AccountView();
	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap=context.getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	Integer paginatorRows=0;
	Integer recordSize=0;
	
	
	private HtmlSelectOneMenu htmlSelectOneAccountStatus = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu htmlSelectOneAccountType = new HtmlSelectOneMenu();
	
	
	private HtmlInputText htmlAccountNumber = new HtmlInputText();
	private HtmlInputText htmlAccountName = new HtmlInputText();
	private HtmlInputText htmlGRPAccountNumber = new HtmlInputText();
	private HtmlInputText htmlGRPAccountName = new HtmlInputText();
	
	
	


	
	// Constructors

	/** default constructor */
	public AccountSearchBean() {
		logger.logInfo("Constructor|Inside Constructor");
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}
	
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}

	private String getLoggedInUser() {
		context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	

	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/ViolationSearch.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = screen.width;"+
                               "var screen_height = screen.height;"+
                               //"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        {
        	javaScriptText=extraJavaScript;
        }
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}


	


	public String btnAdd_Click() {
		String methodName = "btnAdd_Click";
		try {

		} catch (Exception e) {
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public HashMap<String,Object> getSearchCriteria()
	{
		HashMap<String,Object> searchCriteria = new HashMap<String, Object>();		
		
		String emptyValue = "-1";	
		
		Object accountNumber = htmlAccountNumber.getValue();
		Object accountName = htmlAccountName.getValue();
	    Object GRPAccountNumber = htmlGRPAccountNumber.getValue();
	//	Object GRPAccountName = htmlGRPAccountName.getValue();


		Object accountStatus = htmlSelectOneAccountStatus.getValue();
		Object accountType = htmlSelectOneAccountType.getValue();
		
		
		if(accountNumber != null && !accountNumber.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.ACCOUNT_NUMBER, accountNumber.toString());
		}
		if(accountName != null && !accountName.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.ACCOUNT_NAME, accountName.toString());
		}
    	if(GRPAccountNumber != null && !GRPAccountNumber.toString().trim().equals("") )
		{
			searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.GRP_ACCOUNT_NUMBER, GRPAccountNumber.toString());
		}
	//	if(GRPAccountName != null && !GRPAccountName.toString().trim().equals("") )
	//	{
	//		searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.GRP_ACCOUNT_NAME, GRPAccountName.toString());
	//	}
		
		
		if(accountStatus != null && !accountStatus.toString().trim().equals("") && !accountStatus.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.SELECT_ACCOUNT_STATUS, accountStatus.toString());
		}
		if(accountType != null && !accountType.toString().trim().equals("") && !accountType.toString().trim().equals(emptyValue))
		{
			searchCriteria.put(WebConstants.ACCOUNT_SEARCH_CRITERIA.SELECT_ACCOUNT_TYPE, accountType.toString());
		}	
		
		
		
		return searchCriteria;
	}
	
	public Boolean atleastOneCriterionEntered() {
		
		Object accountNumber = htmlAccountNumber.getValue();
		Object accountName = htmlAccountName.getValue();
	 	Object GRPAccountNumber = htmlGRPAccountNumber.getValue();
	//	Object GRPAccountName = htmlGRPAccountName.getValue();


		Object accountStatus = htmlSelectOneAccountStatus.getValue();
		Object accountType = htmlSelectOneAccountType.getValue();
		
		
		
		if(StringHelper.isNotEmpty(accountNumber.toString()))
			return true;
		if(StringHelper.isNotEmpty(accountName.toString()))
			return true;
		if(StringHelper.isNotEmpty(GRPAccountNumber.toString()))
			return true;
	//	if(StringHelper.isNotEmpty(GRPAccountName.toString()))
	//		return true;
		
		if(StringHelper.isNotEmpty(accountStatus.toString()) && !accountStatus.toString().equals("-1"))
			return true;
		if(StringHelper.isNotEmpty(accountType.toString()) && !accountType.toString().equals("-1"))
			return true;
		
		return false;
	}
	
	public String btnSearch_Click() {
		String methodName = "btnSearch_Click";
		logger.logInfo(methodName + "|" + "Start::");
		try 
		{
		 //if(atleastOneCriterionEntered())
		//	{
				loadDataList();
				if(viewRootMap.get("SESSION_ACCOUNT_SEARCH_LIST")!=null){
					List<AssetClassView> assetClassList = (List<AssetClassView>) viewRootMap.get("SESSION_ACCOUNT_SEARCH_LIST");
					if(assetClassList.isEmpty()){
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
					}
				}
		//	}
		//	else{
		//		errorMessages.clear();
		//		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
		//	}
		} catch (Exception e) {
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		logger.logInfo(methodName + "|" + "Finish::");
		return "";
	}

	
   private void loadDataList() throws Exception
   {
	   String methodName = "btnSearch_Click";
	   logger.logInfo(methodName + "|" + "Start::");
	   Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	   HashMap searchMap= getSearchCriteria();
		
	   if(viewRootMap.containsKey("SESSION_ACCOUNT_SEARCH_LIST"))
		   viewRootMap.remove("SESSION_ACCOUNT_SEARCH_LIST");
	   
        // new function right here   	   
		List<AccountView> accountViewList =accountServiceAgent.getAllAccounts(searchMap);
		viewRootMap.put("SESSION_ACCOUNT_SEARCH_LIST",accountViewList);
		dataList=accountViewList;
	   
		  if(dataList!=null)
				recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewRootMap.put("paginatorMaxPages", paginatorMaxPages);
	        
		
		logger.logInfo(methodName + "|" + "Finish::");
   }
	
	@Override
	public void init() {

		String methodName = "init";
		logger.logInfo(methodName + "|" + "Start");
		super.init();
		logger.logInfo(methodName + "|" + "isPostBack()=" + isPostBack());
		sessionMap = context.getExternalContext().getSessionMap();
		try {
			if (!isPostBack()) 
			{
				
		    }
			
			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.logError(methodName + "|" + "Exception Occured::" + ex);

		}
	}
    
	@Override
	public void preprocess() {
		super.preprocess();
		System.out.println("preprocess");

	}

	@Override
	public void prerender() {
		super.prerender();
		
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		HashMap searchMap;
		String methodName="prerender"; 
		logger.logInfo(methodName + "|" + "Start...");
		try
		{
		  if(sessionMap.containsKey("FROM_STATUS")  && sessionMap.get("FROM_STATUS").toString().equals("1"))
		  {
			  sessionMap.remove("FROM_STATUS");
			  loadDataList();
			    
		  }
		   
		}
		catch(Exception ex)
		{
			logger.logError(methodName + "|" + "Exception Occured::" + ex);
			
		}
		  
		  
		  logger.logInfo(methodName + "|" + "Finish...");
	}

	

	public List<AccountView> getDataList() {
		dataList =null;
		if (dataList == null) 
		{
			if(viewRootMap.containsKey("SESSION_ACCOUNT_SEARCH_LIST"));
			dataList=(ArrayList)viewRootMap.get("SESSION_ACCOUNT_SEARCH_LIST");
		}
		return dataList;

	}

	public void setDataList(List<AccountView> dataList) {
		this.dataList = dataList;
	}

	


	
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public String btnAddAccount_Click(){
	
		return "accountManage";
	}

	
	
	public String cmdView_Click() 
	{
		String path = null;
		accountView = (AccountView)tbl_AccountSearch.getRowData();
		sessionMap.put( WebConstants.ACCOUNT.ACCOUNT_VIEW, accountView);
		sessionMap.put(WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_KEY, WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_VIEW_ACCOUNT);
		sessionMap.put(WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY, WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY);
		openPopup("openAccountManagePopup();");
		
		return path;
	}
	
	
	public String cmdEdit_Click(){
		accountView = (AccountView)tbl_AccountSearch.getRowData();
		sessionMap.put( WebConstants.ACCOUNT.ACCOUNT_VIEW, accountView);   
		sessionMap.put(WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_KEY, WebConstants.ACCOUNT.ACCOUNT_MANAGE_PAGE_MODE_UPDATE_ACCOUNT);
		return "accountManage";
	}

	public String cmdDelete_Click()
	{
	try {
		
		accountView = (AccountView)tbl_AccountSearch.getRowData();
		accountView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
		Integer index =  tbl_AccountSearch.getRowIndex();
		viewRootMap.put("delete", index);
		accountServiceAgent.updateAccount(accountView);
		
		index = (Integer) viewRootMap.get("delete");
		if(viewRootMap.containsKey("SESSION_ACCOUNT_SEARCH_LIST"))
		{
		 List<AccountView> tempList = (List<AccountView>) viewRootMap.get("SESSION_ACCOUNT_SEARCH_LIST");
		 tempList.remove(index.intValue());
         dataList = tempList;
         viewRootMap.put("SESSION_ACCOUNT_SEARCH_LIST",dataList);
        if(dataList!=null)
        {
			recordSize = dataList.size();
			viewRootMap.put("recordSize", recordSize);
         }
         
	     infoMessages.add(CommonUtil.getBundleMessage("accountSearch.msg.deleteAccount"));
		}
		
		} 
		catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "cmdDelete_Click"; 
	}
	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public HtmlSelectOneMenu getHtmlSelectOneAccountStatus() {
		return htmlSelectOneAccountStatus;
	}

	public void setHtmlSelectOneAccountStatus(
			HtmlSelectOneMenu htmlSelectOneAccountStatus) {
		this.htmlSelectOneAccountStatus = htmlSelectOneAccountStatus;
	}

	public HtmlSelectOneMenu getHtmlSelectOneAccountType() {
		return htmlSelectOneAccountType;
	}

	public void setHtmlSelectOneAccountType(
			HtmlSelectOneMenu htmlSelectOneAccountType) {
		this.htmlSelectOneAccountType = htmlSelectOneAccountType;
	}

	public HtmlInputText getHtmlAccountNumber() {
		return htmlAccountNumber;
	}

	public void setHtmlAccountNumber(HtmlInputText htmlAccountNumber) {
		this.htmlAccountNumber = htmlAccountNumber;
	}

	public HtmlInputText getHtmlAccountName() {
		return htmlAccountName;
	}

	public void setHtmlAccountName(HtmlInputText htmlAccountName) {
		this.htmlAccountName = htmlAccountName;
	}

	public HtmlInputText getHtmlGRPAccountNumber() {
		return htmlGRPAccountNumber;
	}

	public void setHtmlGRPAccountNumber(HtmlInputText htmlGRPAccountNumber) {
		this.htmlGRPAccountNumber = htmlGRPAccountNumber;
	}

	public HtmlInputText getHtmlGRPAccountName() {
		return htmlGRPAccountName;
	}

	public void setHtmlGRPAccountName(HtmlInputText htmlGRPAccountName) {
		this.htmlGRPAccountName = htmlGRPAccountName;
	}

	public HtmlDataTable getTbl_AccountSearch() {
		return tbl_AccountSearch;
	}

	public void setTbl_AccountSearch(HtmlDataTable tbl_AccountSearch) {
		this.tbl_AccountSearch = tbl_AccountSearch;
	}

	public AccountView getDataItem() {
		return dataItem;
	}

	public void setDataItem(AccountView dataItem) {
		this.dataItem = dataItem;
	}

}