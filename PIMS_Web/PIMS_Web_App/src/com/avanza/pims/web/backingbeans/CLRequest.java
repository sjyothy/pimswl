package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.proxy.issueclearanceletter.PIMSIssueClearanceLetterBPELPortClient;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ClearenceLetterCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.BidderAttendanceView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class CLRequest extends AbstractController {
	transient Logger logger = Logger.getLogger(CLRequest.class);
	private HtmlInputText contractNoText = new HtmlInputText();
	private HtmlInputText tenantNameText = new HtmlInputText();
	private HtmlInputText contractTypeText = new HtmlInputText();
	private HtmlInputText contractStartDateText = new HtmlInputText();
	private HtmlInputText contractEndDateText = new HtmlInputText();
	private HtmlInputText tenantNumberType = new HtmlInputText();
	private HtmlInputText contractStatusText = new HtmlInputText();
	private HtmlInputText totalContractValText = new HtmlInputText();
	private HtmlSelectOneMenu nolTypeSelectOneMenu = new HtmlSelectOneMenu();
	private HtmlInputTextarea reasontForNol = new HtmlInputTextarea();
	private HtmlInputText feeText = new HtmlInputText();
	private HtmlInputText txtUnitType = new HtmlInputText();
	private HtmlInputText txtpropertyName = new HtmlInputText();
	private HtmlInputText txtunitRefNum = new HtmlInputText();
	private HtmlInputText txtUnitRentValue = new HtmlInputText();
	private HtmlInputText txtpropertyType = new HtmlInputText();
	private HtmlCommandLink populateContract = new HtmlCommandLink();

	private HtmlCommandButton cancelButton = new HtmlCommandButton();
	private HtmlCommandButton sendButton = new HtmlCommandButton();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton rejectButton = new HtmlCommandButton();
	private HtmlCommandButton approveButton = new HtmlCommandButton();
	private HtmlCommandButton payButton = new HtmlCommandButton();
	private HtmlCommandButton reprintButton = new HtmlCommandButton();
	private HtmlCommandButton completeRequest = new HtmlCommandButton();
	private HtmlCommandButton cancelRequest = new HtmlCommandButton();
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	ClearenceLetterCriteria clearenceLetterCriteria;
	ContractView contractView = new ContractView();
	CommonUtil commUtil  = new CommonUtil();
	RequestView requestView = new RequestView();
	private String screenName;
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	String reason;
	Double fee;
	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;	
	private List<String> messages;	
	private HtmlInputText feeInputText = new HtmlInputText();
	private String requestId;
	private String contractId;
	private String statusId;
	private boolean boolReprintBtn = false;
	
	private List<String> successMessages;
	private List<String> errorMessages;
	
	 
	private HtmlCommandButton requestStatus = new HtmlCommandButton();
	//private HtmlColumnTag requestStatusForColumn = new HtmlColumnTag();
	private Boolean requestStatusForColumn;
	
	
	//hdn variable by shiraz **********************
	private String hdnContractId;
	private String hdnContractNumber;
	private String hdnTenantId;
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	//*********************************************
	
	private Long contractIdForNavigationToLeaseContract;
	
	FacesContext context = FacesContext.getCurrentInstance();
	private Long tenantId;
	
	//********** payment Tab Variable********************************
	
	private List<PaymentScheduleView> paymentSchedules = new ArrayList();
	Map sessionMap;
	private HtmlDataTable dataTablePaymentSchedule;
	public boolean statusIsNotSentForApprovalAndNotApproved;
	private String dateFormatForDataTable;
	
	private double totalRentAmount;
	private double totalCashAmount;
	private double totalChqAmount;
	private int installments;
	private double totalDepositAmount;
	private double totalFees;
	private double totalFines;
	private Boolean editPaymentEnable;
	
	
	//***************************************************************
	
	private String APPLICATION_TAB = "applicationTab";
	private String CONTRACT_TAB = "contractDetailTab";
	private String ATTACHMENT_TAB = "attachmentTab";
	private String PAYMENT_TAB = "PaymentTab"; 
	private String SelectedTab;
	private HtmlTabPanel richTabPanel = new HtmlTabPanel();
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	private final String noteOwner = WebConstants.REQUEST;//WebConstants.PROCEDURE_TYPE_CLEARANCE_LETTER;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_CL;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_CLEARANCE_LETTER;
	
	public class PageOutcomes
	{
		public static final String SuccessToApproveClearanceLetterRequest = "SuccessToApproveClearanceLetterRequest";
		public static final String FailureToApproveClearanceLetterRequest = "FailureToApproveClearanceLetterRequest";
		public static final String FailureToRejectClearanceLetterRequest = "FailureToRejectClearanceLetterRequest";
	}
	
	public void init(){
		super.init();
		sessionMap = context.getExternalContext().getSessionMap();
		boolReprintBtn = false;
		CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
		try {
			
			if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
		     		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
		        )
		        {
		     	   viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		     	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		     	   
		     	   setPaymentSchedules((List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		     	   
		     	   List<PaymentScheduleView> paymentSecList = (List<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		     	    if(paymentSecList.get(0).getPaymentScheduleId()!=null)
		     	    	viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSecList);
		     	   
		    
		        }
			
			
		if(!isPostBack()){
			
			viewMap.put("selectedTab",APPLICATION_TAB);
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			
			cancelButton.setValue(ResourceUtil.getInstance().getProperty("commons.cancel"));
			sendButton.setValue(ResourceUtil.getInstance().getProperty("commons.send"));
			approveButton.setValue(ResourceUtil.getInstance().getProperty("commons.approve"));
			rejectButton.setValue(ResourceUtil.getInstance().getProperty("commons.reject"));
			payButton.setValue(ResourceUtil.getInstance().getProperty("auction.pay"));
			reprintButton.setValue(ResourceUtil.getInstance().getProperty("commons.reprintLetter"));
			completeRequest.setValue(ResourceUtil.getInstance().getProperty("common.completeRequest"));
			cancelRequest.setValue(ResourceUtil.getInstance().getProperty("common.cancelRequest"));
			saveButton.setValue(ResourceUtil.getInstance().getProperty("commons.saveButton"));
			
			
			
			btnCollectPayment.setRendered(false);
			requestStatusForColumn = false;
			cancelButton.setRendered(true);
			sendButton.setRendered(false);
			rejectButton.setRendered(false);
			approveButton.setRendered(false);
			reprintButton.setRendered(false);
			completeRequest.setRendered(false);
			cancelRequest.setRendered(false);
			saveButton.setRendered(false);
			populateContract.setRendered(true);
			setEditPaymentEnable(false);
			
			
	if  (getFacesContext().getExternalContext().getSessionMap().get(WebConstants.Contract.LOCAL_CONTRACT_ID) != null){
		   viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID,sessionMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID));
		   sessionMap.remove(WebConstants.Contract.LOCAL_CONTRACT_ID);
		 
			paymentSchedules = propertyServiceAgent.getDefaultPaymentsForClearanceLetter((Long)viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID), getIsEnglishLocale());
			viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
			setPaymentSchedules(paymentSchedules);
	
	      }
		
		
		
		if(hdnPersonId!=null && !hdnPersonId.equals("")){
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}

			if(hdnPersonName!=null && !hdnPersonName.equals(""))
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			
			
			if(hdnCellNo!=null && !hdnCellNo.equals(""))
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
			
			if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
				DomainDataView ddv = new DomainDataView();
				if(Long.parseLong(hdnIsCompany)==1L){
				        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_COMPANY);}
				else{
				        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_INDIVIDUAL);}
				
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
			
			}

			
			
			
			if(getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW)!=null){
				viewMap.put(WebConstants.Contract.CONTRACT_VIEW, getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW));
				
				viewMap.put("selectedTab",CONTRACT_TAB);
				
				saveButton.setRendered(true);
				cancelButton.setRendered(true);
				populateContract.setRendered(false);
				setEditPaymentEnable(true);
				viewMap.put("FromScreen","Contract_Search");
				
				if(viewMap.get(WebConstants.Contract.CONTRACT_VIEW)!=null){
						contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
						viewMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
						viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					}
			        
					if  (viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID) != null){
						paymentSchedules = propertyServiceAgent.getDefaultPaymentsForClearanceLetter((Long)viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID), getIsEnglishLocale());
						viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
						setPaymentSchedules(paymentSchedules);
						
				}
					
					contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
					
					SystemParameters parameters = SystemParameters.getInstance();			
					String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
					
				    
					HashMap contractHashMap=new HashMap();
					ContractView cv=new ContractView();
					cv.setContractId(contractView.getContractId());
					contractHashMap.put("contractView",cv);
					
					
					
				    List<ContractView> contractViewList = propertyServiceAgent.getAllContracts(contractHashMap);
				    contractView = contractViewList.get(0);
				    getContractById(contractView.getContractId());
					
			
					
					//Setting the Contract/Tenant Info
					contractNoText.setValue(contractView.getContractNumber());
					setTenantName(contractView);					
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
					
					
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
					    txtUnitRentValue.setValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					
					}

					if(getIsEnglishLocale())
						contractStatusText.setValue(contractView.getStatusEn());
					else
						contractStatusText.setValue(contractView.getStatusAr());
					totalContractValText.setValue(contractView.getRentAmount());
					
					

						
				}
			
			
			// when comming from request search 
			if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().containsKey(WebConstants.REQUEST_VIEW))
			{
				//setEditPaymentEnable(true);
				populateContract.setRendered(false);
				setEditPaymentEnable(true);
				sendButton.setRendered(true);
				saveButton.setRendered(true);
				
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
				viewMap.put("NOLTabReadOnlyMode","READONLY");
				viewMap.put("selectedTab",CONTRACT_TAB);
				viewMap.put("FromScreen","Request_Search");
				
			 RequestView requestView = 	(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
			 
			
				
				this.requestId=requestView.getRequestId().toString();
				populateApplicationDataTab(this.requestId);
				
				List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
				String contractNo = "";
                   
				
				if(requestList.size()>0)
				{
					requestView = (RequestView)requestList.iterator().next();
					this.requestId = requestView.getRequestId().toString();
					viewMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
					contractNo = requestView.getContractView().getContractNumber();
					viewMap.put("CONTRACT_NUMBER",contractNo);
		    		
					
					paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(Long.parseLong(requestId));
					if(paymentSchedules!=null && paymentSchedules.size()>0)
					{
					 viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
					 setPaymentSchedules(paymentSchedules);
					}else
					{
					 viewMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
					}
				}

				
				
				ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
				contractNoText.setValue(contractNo);
				populateContract();
				

				
			   CommonUtil.loadAttachmentsAndComments(requestId);
				
				if(requestView.getStatusId()!=null )
				{
					DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);
					
					if(!requestView.getStatusId().equals(ddv.getDomainDataId()))
					{
			
						sendButton.setRendered(false);
						saveButton.setRendered(false);
						setEditPaymentEnable(false);
						
						viewMap.put("canAddAttachment",false);
						viewMap.put("canAddNote", false);		
					}
				}
			}
			
	
    if	(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null){		
			
	    UserTask task = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	    viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK));    
        sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);	
        viewMap.put("FromScreen","Task_List");

	        //comming for approval  
	 
			if(task!=null && task.getTaskType().equals("ApproveClearanceRequest")){
				
				viewMap.put("selectedTab",APPLICATION_TAB);
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
			    
				approveButton.setRendered(true);
				rejectButton.setRendered(true);
				cancelButton.setRendered(true);
				populateContract.setRendered(false);
				
			  
			
				Map taskAttributes =  task.getTaskAttributes();
				
				Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				RequestView requestView = new RequestView();
				requestView.setRequestId(requestId);
				
				this.requestId=requestId.toString();
				populateApplicationDataTab(this.requestId);
				
				List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
				String contractNo = "";
                   
				
				if(requestList.size()>0){
					requestView = (RequestView)requestList.iterator().next();
					contractNo = requestView.getContractView().getContractNumber();
					this.contractId = requestView.getContractView().getContractId().toString();
					viewMap.put("CONTRACT_NUMBER",requestView.getContractView().getContractNumber());
					getContractById(Long.parseLong(this.contractId));
					
					paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
					if(paymentSchedules!=null && paymentSchedules.size()>0)
					{
					 viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
					 viewMap.put("CANCEL_PAYMENT_SCH", true);
					 setPaymentSchedules(paymentSchedules);
					}else
					{
					 viewMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
					}
		            
		                               
				}
				
				populateContract();
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
			 }
	    
	    /// collect payment
		if(task!=null && task.getTaskType().equals("CollectCLRequestPayment")){
			
			viewMap.put("canAddAttachment",true);
			viewMap.put("canAddNote", true);
			viewMap.put("selectedTab",PAYMENT_TAB);
			completeRequest.setRendered(true);
			cancelRequest.setRendered(true);
			cancelButton.setRendered(true);
			btnCollectPayment.setRendered(true);
			populateContract.setRendered(false);
			requestStatusForColumn = true;
		  
		
			Map taskAttributes =  task.getTaskAttributes();
			
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			RequestView requestView = new RequestView();
			requestView.setRequestId(requestId);
			
			this.requestId=requestId.toString();
			populateApplicationDataTab(this.requestId);
			
			List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
			String contractNo = "";
               
			
			if(requestList.size()>0)
			{
				requestView = (RequestView)requestList.iterator().next();
				viewMap.put(WebConstants.REQUEST_VIEW,requestView);
				contractNo = requestView.getContractView().getContractNumber();
				this.contractId = requestView.getContractView().getContractId().toString();
				viewMap.put("CONTRACT_NUMBER",requestView.getContractView().getContractNumber());
				getContractById(Long.parseLong(this.contractId));
				
				paymentSchedules = propertyServiceAgent.getContractPaymentSchedule(null,requestView.getRequestId());
				viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
				setPaymentSchedules(paymentSchedules);
	            
	                               
			}
			
		DomainDataView	ddv= CommonUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
	     		       WebConstants.PAYMENT_SCHEDULE_COLLECTED);	
		DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
                WebConstants.REALIZED);
	
		if( paymentSchedules.size()>0 &&
			( ddv.getDomainDataId().equals(  paymentSchedules.get(0).getStatusId()  ) ||
			  ddvRealized.getDomainDataId().equals(  paymentSchedules.get(0).getStatusId()  )
			)
		   )
		   {
				sessionMap.put("PAYMENT_DONE", true);
				btnCollectPayment.setRendered(false);
				completeRequest.setRendered(true);
				cancelRequest.setRendered(false);
			}
			
			if(!(paymentSchedules.size()>0)){
				sessionMap.put("PAYMENT_DONE", true);
				btnCollectPayment.setRendered(false);
				completeRequest.setRendered(true);
				cancelRequest.setRendered(true);
			}
			
			populateContract();
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
		 }
     }
			
		   }
				
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	private void setVariablesFromJS(RequestView requestView) {
		String  METHOD_NAME = "setVariablesFromJS|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
			final String viewId = "/CLRequest.jsp";
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();        
	
	        // This is the proper way to get the view's url
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	
	        //Your Java script code here
	        String javaScriptText = "document.getElementById(\"cLRForm:reqId\").value = "+requestView.getRequestId().toString()+"; ";
	        javaScriptText += "document.getElementById(\"cLRForm:contractId\").value = "+requestView.getContractView().getContractId().toString()+"; ";
	        javaScriptText += "document.getElementById(\"cLRForm:statusId\").value = "+requestView.getStatusId().toString()+"; ";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.BODY_END, javaScriptText);        	
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
		}				
	}

	private ContractView getContractById(Long contractId) throws PimsBusinessException
	{
		String methodName="getContractById";
		ContractView contractView = new ContractView();
		ArrayList<ContractView> list;
		logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
		try
		{
			contractView.setContractId(contractId);
			
			HashMap contractHashMap=new HashMap();
			contractHashMap.put("contractView",contractView);
			
			SystemParameters parameters = SystemParameters.getInstance();			
			String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
			contractHashMap.put("dateFormat",dateFormat);
			PropertyServiceAgent psa =new PropertyServiceAgent();
			logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
			List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
			logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
			if(contractViewList.size()>0)
			contractView=(ContractView)contractViewList.get(0);
			viewMap.put("Contract_VIEW_FOR_PAYMENT", contractView);
			
		}
		catch(Exception ex)
		{
			logger.logError(methodName+"|"+"Error occured:"+ex);
		}
		return contractView;
	}
	
	private void releaseSessionInfo() {
		String  METHOD_NAME = "managePaymentButton|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
        	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
        	sessionMap.remove(WebConstants.Contract.LOCAL_CONTRACT_ID);
        	//sessionMap.remove(WebConstants.LOCAL_PERSON_ID);
        	sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
        	//sessionMap.remove(WebConstants.PAYMENT_RECEIVED);
        	sessionMap.remove(WebConstants.REQUEST_VIEW);
        	//sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
        	
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
		}		
	}

	private void managePaymentButton(RequestView requestView) throws Exception{
		String  METHOD_NAME = "managePaymentButton|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
        	if(requestView.getStatusId().equals(getApprovedStatusId()))
        		
        		payButton.setRendered(true);
        	else
        		payButton.setRendered(false);
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
        	throw e;
		}        		
	}
	
	private void manageApprovalButtons(RequestView requestView) throws Exception{
		String  METHOD_NAME = "manageApprovalButtons|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
        	if(requestView.getStatusId().equals(getApprovalRequiredStatusId())){
        		approveButton.setRendered(true);
        		rejectButton.setRendered(true);
        	}
        	else{
        		approveButton.setRendered(false);
        		rejectButton.setRendered(false);
        	}        		
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
        	throw e;
		}
	}

	private Long getApprovedStatusId() throws Exception {
		Long statusId = new Long(0);
		String  METHOD_NAME = "getApprovedStatusId|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
        	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
       		       WebConstants.REQUEST_STATUS_APPROVED);
      		if(hMap.containsKey("returnId")){
      			statusId = Long.parseLong(hMap.get("returnId").toString());
      			if(statusId.equals(null))
      				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
      		}
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
        	throw e;
		}
        return statusId;
	}
	
	private Long getApprovalRequiredStatusId() throws Exception {
		Long statusId = new Long(0);
		String  METHOD_NAME = "getApprovalRequiredStatusId|";		
        logger.logInfo(METHOD_NAME+" started...");
        try{
        	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
       		       WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
      		if(hMap.containsKey("returnId")){
      			statusId = Long.parseLong(hMap.get("returnId").toString());
      			if(statusId.equals(null))
      				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
      		}
        }
        catch (Exception e) {
        	logger.logInfo(METHOD_NAME+" ended...");
        	throw e;
		}
        return statusId;
	}

	private void setTenantName(ContractView contractView) {
		String  METHOD_NAME = "setTenantName|";		
        logger.logInfo(METHOD_NAME+" started...");
		if(contractView.getTenantView() != null){
			String name = "";
			if(contractView.getTenantView().getFirstName() != null)
				name += contractView.getTenantView().getFirstName() + " ";
			if(contractView.getTenantView().getMiddleName() != null)
				name += contractView.getTenantView().getMiddleName() + " ";
			if(contractView.getTenantView().getLastName() != null)
				name += contractView.getTenantView().getLastName();
			tenantNameText.setValue(name);
			
			if(contractView.getTenantView().getResidenseVisaNumber()!=null &&
					!contractView.getTenantView().getResidenseVisaNumber().equals(""))
			tenantNumberType.setValue(contractView.getTenantView().getResidenseVisaNumber());
			else
			tenantNumberType.setValue(contractView.getTenantView().getSocialSecNumber());	
		}
        logger.logInfo(METHOD_NAME+" ended...");
	}

	public void preprocess(){
		
		super.preprocess();
		
	}
	public String cancel(){
		
		if(viewMap.containsKey("FromScreen"))
		{
			if(viewMap.get("FromScreen").equals("Request_Search"))
			{
				return "requestSearch";
			}
			if(viewMap.get("FromScreen").equals("Contract_Search"))
			{
				sessionMap.put("preserveSearchCriteria", true);
				return "contractSearch";
			}
			if(viewMap.get("FromScreen").equals("Task_List"))
			{
				return "SuccessToApproveClearanceLetterRequest";
			}
		}
		
		return "";
		
		
	}
	@SuppressWarnings("unchecked")
	public String approve()
	{
		String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
		String  METHOD_NAME = "approve|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        logger.logInfo(METHOD_NAME+" started...");
        try 
        {
        	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			NotesController.saveSystemNotesForRequest(WebConstants.CLEARANCE_LETTER,MessageConstants.RequestEvents.REQUEST_APPROVED, requestId);
			
			this.requestId  = requestId.toString();
			
			/*if(!addPaymentSchedule(requestId)){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.NOT_PROCEED_FURTHER));
				return "Approved";
				}*/
			
			RequestServiceAgent rSAgent = new RequestServiceAgent();
			  rSAgent.setRequestAsApproved(requestId, getLoggedInUser());
			
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId, noteOwner);
			
						
			client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
			notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_CLEARANCELETTER_REQUESTAPPROVED);
			viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
			
			approveButton.setRendered(false);
			rejectButton.setRendered(false);
			populateContract.setRendered(false);
			completeRequest.setRendered(false);
			cancelRequest.setRendered(false);
			
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_APPROVE_SUCCESS));
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} 
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to approve due to:",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_APPROVE_FAILURE));
			ex.printStackTrace();
			outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
		}
        catch (Exception e) 
        {
			logger.LogException(METHOD_NAME + "crashed...", e);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_APPROVE_FAILURE));
		}
        outcome = "";
		return outcome;
	}
	
	private Boolean addPaymentSchedule(Long requestId)  throws Exception{
		String  METHOD_NAME = "addPaymentSchedule|";
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Long contractId=0L;
		ContractView contractView = new ContractView();
		PaymentScheduleView paymentScheduleViewForEdit = new PaymentScheduleView();
		PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	
        	if(viewRootMap.containsKey("NO_PAYMENT_SCH_FOR_EDIT"))
     		   return true;

     		if(viewRootMap.containsKey("PAYMENT_SCH_FOR_EDIT"))
     		{
     			    List<PaymentScheduleView> paymentScheduleViewList = (List<PaymentScheduleView>)viewRootMap.get("PAYMENT_SCH_FOR_EDIT");
     			    if(paymentScheduleViewList !=null && paymentScheduleViewList.size()>0)
     			    	paymentScheduleViewForEdit = paymentScheduleViewList.get(0);
     			    
     			    paymentScheduleView.setPaymentScheduleId(paymentScheduleViewForEdit.getPaymentScheduleId());
     			    paymentScheduleView.setPaymentNumber(paymentScheduleViewForEdit.getPaymentNumber());
     			    paymentScheduleView.setContractId(paymentScheduleViewForEdit.getContractId());
     			    paymentScheduleView.setAmount(paymentScheduleViewForEdit.getAmount());
     				paymentScheduleView.setTypeId(paymentScheduleViewForEdit.getTypeId());
     				paymentScheduleView.setPaymentDueOn(paymentScheduleViewForEdit.getPaymentDueOn());
     				paymentScheduleView.setPaymentModeId(paymentScheduleViewForEdit.getPaymentModeId());
     				paymentScheduleView.setCreatedBy(paymentScheduleViewForEdit.getCreatedBy());
     				paymentScheduleView.setCreatedOn(paymentScheduleViewForEdit.getCreatedOn());
     				paymentScheduleView.setUpdatedBy(getLoggedInUser());
     				paymentScheduleView.setUpdatedOn(new Date());
     				paymentScheduleView.setIsDeleted(paymentScheduleViewForEdit.getIsDeleted());
     				paymentScheduleView.setRecordStatus(paymentScheduleViewForEdit.getRecordStatus());
     				paymentScheduleView.setIsReceived(paymentScheduleViewForEdit.getIsReceived());
     				
     				if(viewRootMap.containsKey("CANCEL_PAYMENT_SCH"))
     				paymentScheduleView.setStatusId(getPaymentScheduleCancelStatusId());
     				else
     				paymentScheduleView.setStatusId(paymentScheduleViewForEdit.getStatusId());
     				
     				paymentScheduleView.setRequestId(paymentScheduleViewForEdit.getRequestId()) ;
     				paymentScheduleView.setDescription(paymentScheduleViewForEdit.getDescription());
     				paymentScheduleView.setOwnerShipTypeId(paymentScheduleViewForEdit.getOwnerShipTypeId());
     				
     				pSAgent.addPaymentScheduleByPaymentSchedule(paymentScheduleView);
     				logger.logInfo(METHOD_NAME+" completed successfully...");
     	
     				return true;
     			
     		}else{	
        	
		        if(viewRootMap.containsKey("Contract_VIEW_FOR_PAYMENT"))
		           contractView =(ContractView)viewRootMap.get("Contract_VIEW_FOR_PAYMENT");
		        
		        if(getPaymentSchedules().size()>0)
		        { 	
		        	if(contractView.getContractId()!=null)
		    		paymentScheduleView.setContractId(contractView.getContractId());
		    			
		    			
					if(getPaymentSchedules().get(0).getAmount()!=null)
					paymentScheduleView.setAmount(getPaymentSchedules().get(0).getAmount());
					else
					return false;	
		        	
		        	
		        	paymentScheduleView.setTypeId(getPaymentSchedules().get(0).getTypeId());
		        	paymentScheduleView.setPaymentDueOn(new Date());
		        	paymentScheduleView.setPaymentModeId(getClearanceLetterPaymentScheduleModeId());
		        	paymentScheduleView.setCreatedBy(getLoggedInUser());
		        	paymentScheduleView.setCreatedOn(new Date());
		        	paymentScheduleView.setUpdatedBy(getLoggedInUser());
		        	paymentScheduleView.setUpdatedOn(new Date());
		        	paymentScheduleView.setIsDeleted(new Long(0));
		        	paymentScheduleView.setRecordStatus(new Long(1));
		        	paymentScheduleView.setIsReceived("N");
		        	paymentScheduleView.setStatusId(getPaymentSchedulePendingStatusId());
		        	paymentScheduleView.setRequestId(requestId);
		        	paymentScheduleView.setDescription(getPaymentSchedules().get(0).getDescription());
		        	paymentScheduleView.setOwnerShipTypeId(contractView.getContractUnitView().iterator().next().getUnitView().getPropertyCategoryId());
		        	
		        	pSAgent.addPaymentScheduleByPaymentSchedule(paymentScheduleView);
		        	logger.logInfo(METHOD_NAME+" completed successfully...");
		        	return true;
         }
        else{
        	logger.logInfo(METHOD_NAME+" completed successfully...");
        	return true;
        }
     }	
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
	}
	
	private void updatePaymentSchedule(Long requestId) throws Exception{
		String  METHOD_NAME = "updatePaymentSchedule|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {
				PaymentScheduleView payScheduleView = new PaymentScheduleView();
				payScheduleView.setIsReceived("Y");
				payScheduleView.setStatusId(getPaymentScheduleCollectedStatusId());
				payScheduleView.setUpdatedBy(getLoggedInUser());
				payScheduleView.setUpdatedOn(new Date());
				payScheduleView.setRequestId(requestId);
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
	        	pSAgent.updatePaymentSchedule(payScheduleView);       	
        	
	        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
	}

	private Long getPaymentScheduleCancelStatusId() {
		Long statusId = new Long(0);
		String methodName="getPaymentSchedulePendingStatusId|";
		logger.logDebug(methodName+"started..");
		try{
        	HashMap hMap= getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
        		       WebConstants.CANCELED);
       		if(hMap.containsKey("returnId")){
       			statusId = Long.parseLong(hMap.get("returnId").toString());
       			if(statusId.equals(null))
       				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
       		}
			
			logger.logDebug(methodName+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		return statusId;
	}
	private Long getClearanceLetterPaymentScheduleModeId() throws Exception{
		Long typeId = new Long(0);
		String  METHOD_NAME = "getClearanceLetterPaymentScheduleTypeId|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE),
      		       WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
     		if(hMap.containsKey("returnId")){
     			typeId = Long.parseLong(hMap.get("returnId").toString());
     			if(typeId.equals(null))
     				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
     		}
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
			throw e;
		}
		return typeId;
	}
	public String reject(){
		String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
		String  METHOD_NAME = "reject|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        logger.logInfo(METHOD_NAME+" started...");
        try {

        	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
		
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			NotesController.saveSystemNotesForRequest(WebConstants.CLEARANCE_LETTER,MessageConstants.RequestEvents.REQUEST_REJECTED, requestId);
			this.requestId = requestId.toString();
			new PropertyService().setPaymentScheduleCanceled(requestId);
			addPaymentSchedule(requestId);
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId,noteOwner);
			generateNotification(WebConstants.Notification_MetaEvents.Event_CLEARANCELETTER_REQUESTREJECTED);
			
			client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);
			viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
			
			approveButton.setRendered(false);
			rejectButton.setRendered(false);
			completeRequest.setRendered(false);
			cancelRequest.setRendered(false);

			
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_REJECT_SUCCESS));
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} 
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to reject due to:",ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_REJECT_FAILURE));
			ex.printStackTrace();
			outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
		}
        catch (Exception e){
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_REJECT_FAILURE));
		}
		outcome = "";
		return outcome;
	}
	@SuppressWarnings("unchecked")
	public String send(){
        errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		try 
		{
			if(hdnPersonId!=null && !hdnPersonId.equals(""))
			{
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}

				if(hdnPersonName!=null && !hdnPersonName.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
				
				
				if(hdnCellNo!=null && !hdnCellNo.equals(""))
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
				
				if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
				{
					DomainDataView ddv = new DomainDataView();
					if(Long.parseLong(hdnIsCompany)==1L)
					        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);
					else
						ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);
					
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}
				CommonUtil.loadAttachmentsAndComments(requestId);
				CommonUtil.saveAttachments(Long.parseLong(requestId));
				CommonUtil.saveComments(Long.parseLong(requestId),noteOwner);
				PIMSIssueClearanceLetterBPELPortClient client = new PIMSIssueClearanceLetterBPELPortClient();
				SystemParameters parameters = SystemParameters.getInstance();			
				String endPoint = parameters.getParameter(WebConstants.ClearanceLetter.CLEARANCE_LETTER_BPEL_END_POINT);
				client.setEndpoint(endPoint);
				client.initiate(Long.parseLong(this.requestId),getLoggedInUser(), null,null); 
				NotesController.saveSystemNotesForRequest(WebConstants.CLEARANCE_LETTER,MessageConstants.RequestEvents.REQUEST_CREATED, Long.parseLong(this.requestId));
				notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_CL_Request_Received);
				successMessages.add(CommonUtil.getBundleMessage("cLRequest.Send"));
				sendButton.setRendered(false);
				saveButton.setRendered(false);
				populateContract.setRendered(false);
				viewMap.put("SEND_BUTTON", true);
				setEditPaymentEnable(false);
				CommonUtil.printReport(new Long(requestId));
		
		}
		catch (Exception e) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.ClearanceLetter.MSG_REQUEST_SENT_FAILURE)));
			logger.LogException( "send|crashed...", e);
		}
		return "send";
	}
	

	public String saveRequest(){
		
		
		String  METHOD_NAME = "saveRequest|";		
        logger.logInfo(METHOD_NAME+" started...");
        PersonView applicantView = new PersonView();
        errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        RequestServiceAgent rSAgent = new RequestServiceAgent();
		RequestView requestView = new RequestView();
		Long requestId=0L;
		
		
		try {
			if(hdnPersonId!=null && !hdnPersonId.equals("")){
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}

				if(hdnPersonName!=null && !hdnPersonName.equals(""))
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
				
				
				if(hdnCellNo!=null && !hdnCellNo.equals(""))
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
				
				if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
					DomainDataView ddv = new DomainDataView();
					if(Long.parseLong(hdnIsCompany)==1L){
					        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);}
					else{
						ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);}
					
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
				
				}

			if(validateSend()){
							
				
				
				requestId = addRequest();
				addPaymentSchedule(requestId);
				
				CommonUtil.loadAttachmentsAndComments(requestId.toString());
				CommonUtil.saveAttachments(requestId);
				CommonUtil.saveComments(requestId, noteOwner);
				
				putRequestForEdit(requestId);
				this.requestId = requestId.toString(); 
				
						

				successMessages.add(CommonUtil.getBundleMessage("cLRequest.Save"));

				
				sendButton.setRendered(true);
				populateContract.setRendered(false);
				viewMap.put("SEND_BUTTON", true);
			}
			logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.ClearanceLetter.MSG_REQUEST_SENT_FAILURE)));
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		return "SaveRequest";
		
	}
	
	private Long addRequest(){
		
		PersonView applicantView = new PersonView();
        errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        RequestServiceAgent rSAgent = new RequestServiceAgent();
		RequestView requestView = new RequestView();
		Long requestId = 0L;
		
		try{
		
			if(viewMap.containsKey("REQUEST_VIEW_FOR_EDIT"))
			{
				RequestView reqViewEdit = 	(RequestView)viewMap.get("REQUEST_VIEW_FOR_EDIT");
				requestView.setRequestNumber(reqViewEdit.getRequestNumber());
				requestView.setRequestId(reqViewEdit.getRequestId());
				requestView.setCreatedBy(reqViewEdit.getCreatedBy());
				requestView.setCreatedOn(reqViewEdit.getCreatedOn());
				requestView.setUpdatedBy(getLoggedInUser());
				requestView.setUpdatedOn(new Date());
				requestView.setRecordStatus(reqViewEdit.getRecordStatus());
				requestView.setIsDeleted(reqViewEdit.getIsDeleted());
				requestView.setRequestDate(reqViewEdit.getRequestDate());
				requestView.setStatusId(reqViewEdit.getStatusId());
				requestView.setRequestTypeId(reqViewEdit.getRequestTypeId());
				requestView.setContractId(reqViewEdit.getContractId());
				requestView.setApplicantView(reqViewEdit.getApplicantView());
				requestView.setDescription(reqViewEdit.getDescription());
			
			}else
			{
				requestView.setCreatedBy(getLoggedInUser());
				requestView.setCreatedOn(new Date());
				requestView.setUpdatedBy(getLoggedInUser());
				requestView.setUpdatedOn(new Date());
				requestView.setRecordStatus(new Long(1));
				requestView.setIsDeleted(new Long(0));
				requestView.setRequestDate(new Date());
				requestView.setStatusId(getCleranceLetterRequestStatusNew());
				requestView.setRequestTypeId(getClearanceLetterRequestType());
				
				Long contractId = Long.parseLong(viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).toString());
				
				applicantView.setPersonId(Long.parseLong(viewMap.get(WebConstants.LOCAL_PERSON_ID).toString()));
				requestView.setContractId(contractId);
				requestView.setApplicantView(applicantView);
				 
				
				if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
				{
					requestView.setDescription((String)FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION));
				}
			}	
			
				requestId = rSAgent.addRequest(requestView);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return requestId;
	}
	private boolean validateSend() throws Exception
	{
		String  METHOD_NAME = "validateSend|";
		boolean returnVal = true;
        logger.logInfo(METHOD_NAME+" started...");
        try
        {
        if(!viewMap.containsKey(WebConstants.LOCAL_PERSON_ID))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			viewMap.put("selectedTab",APPLICATION_TAB);
			returnVal = false;
		}
        
        if(paymentSchedules == null && !paymentSchedules.equals("")){
        	
        	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_NO_CONTRACT_SELECTED));
        	returnVal = false;
        }
        if(!AttachmentBean.mandatoryDocsValidated())
		 {
		 		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
		 		viewMap.put("selectedTab",ATTACHMENT_TAB);  
		 		return false;
		 }
          logger.logInfo(METHOD_NAME+" ended...");
		  return returnVal;
        }
        catch (Exception e)
	   	 {
	   		 throw e;
	   	 }
	}
	
	public void putRequestForEdit(Long requestId)
	{
		RequestView requestView = new RequestView();
		
		try{
			
		this.requestId = requestId.toString();
		requestView.setRequestId(requestId);
		populateApplicationDataTab(this.requestId);
		
		List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
		String contractNo = "";
		
		if(requestList.size()>0)
		{
			requestView = (RequestView)requestList.iterator().next();
			
			viewMap.put("REQUEST_VIEW_FOR_EDIT", requestView);
			contractNo = requestView.getContractView().getContractNumber();
			viewMap.put("CONTRACT_NUMBER",contractNo);
			
			paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
			if(paymentSchedules!=null && paymentSchedules.size()>0)
			{
			 viewMap.put("PAYMENT_SCH_FOR_EDIT", paymentSchedules);
			 setPaymentSchedules(paymentSchedules);
			}else
			{
			 viewMap.put("NO_PAYMENT_SCH_FOR_EDIT", true);
			}
		}
		
		ApplicationContext.getContext().get(WebContext.class).setAttribute(WebConstants.REQUEST_VIEW,requestView);
		contractNoText.setValue(contractNo);
		populateContract();
		
		CommonUtil.loadAttachmentsAndComments(requestId.toString());	
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	private void writeMessage(String message){
		if(messages == null)
			messages = new ArrayList<String>();
		messages.add(message);
	}

	private String getRequestNumber(RequestView requestView) {
		String requestNo = "";
		String  METHOD_NAME = "getRequestNumber|";		
        logger.logInfo(METHOD_NAME+" started...");
        try {

        	List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "", null);
			requestNo = ((RequestView)requestList.iterator().next()).getRequestNumber();
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} catch (Exception e) {
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		return requestNo;
	}
	private Long getRequestKeyViewId(List<RequestKeyView> keys, String requestKeyClearanceLetterReason) throws PimsBusinessException{
		Long requestKeyViewId = new Long(0);
		String  METHOD_NAME = "getRequestKeyViewId|";		
        logger.logInfo(METHOD_NAME+"started...");
		for(RequestKeyView req: keys){
			if(req.getKeyName().equals(requestKeyClearanceLetterReason)){
				requestKeyViewId = req.getRequestKeyId();
				break;
			}
		}
		if(requestKeyViewId.equals(new Long(0)))
			throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		logger.logInfo(METHOD_NAME+"ended...");
		return requestKeyViewId;
	}
	private Long getClearanceLetterRequestType() throws PimsBusinessException {
		Long clearanceLetterReqTypeId = null;
		String  METHOD_NAME = "getClearanceLetterRequestType|";		
        logger.logInfo(METHOD_NAME+"started...");
		RequestServiceAgent rSAgent = new RequestServiceAgent();
		RequestTypeView reqTypeView = rSAgent.getClearanceLetterRequestType();
		clearanceLetterReqTypeId = reqTypeView.getRequestTypeId();
		logger.logInfo(METHOD_NAME+"ended...");
		return clearanceLetterReqTypeId;
	}
	private Long getCleranceLetterRequestStatus() throws PimsBusinessException {
		Long cleranceLetterRequestStatusId = null;
		String  METHOD_NAME = "getCleranceLetterRequestStatus|";		
        logger.logInfo(METHOD_NAME+"started...");		
		HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
 		       WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		if(hMap.containsKey("returnId")){
			cleranceLetterRequestStatusId = Long.parseLong(hMap.get("returnId").toString());
			if(cleranceLetterRequestStatusId.equals(null))
				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		}
		logger.logInfo(METHOD_NAME+"ended...");
		return cleranceLetterRequestStatusId;
	}
	
	private Long getCleranceLetterRequestStatusNew() throws PimsBusinessException {
		Long cleranceLetterRequestStatusId = null;
		String  METHOD_NAME = "getCleranceLetterRequestStatus|";		
        logger.logInfo(METHOD_NAME+"started...");		
		HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
 		       WebConstants.REQUEST_STATUS_NEW);
		if(hMap.containsKey("returnId")){
			cleranceLetterRequestStatusId = Long.parseLong(hMap.get("returnId").toString());
			if(cleranceLetterRequestStatusId.equals(null))
				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
		}
		logger.logInfo(METHOD_NAME+"ended...");
		return cleranceLetterRequestStatusId;
	}
	public String attach(){
		return "attach";
	}
	public String populateContract(){
		String  METHOD_NAME = "populateContract|";		
        logger.logInfo(METHOD_NAME+"started...");
        

        try{
			if(true)
			{
				clearValues();
				SystemParameters parameters = SystemParameters.getInstance();			
				String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
				try{				
					if(viewMap.containsKey("CONTRACT_NUMBER"))
					contractView = propertyServiceAgent.getContract((String)viewMap.get("CONTRACT_NUMBER"),dateFormat);
					
					//Code for Navigation to Teneant Details Screen
					if (contractView != null && contractView.getTenantView() != null)
						tenantId = contractView.getTenantView().getPersonId();
					if (contractView != null)
						contractIdForNavigationToLeaseContract = contractView.getContractId();
				}
				catch (Exception e) {
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_NO_CONTRACT_FOUND));
					throw e;
				}
				if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
				{
					viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					getContractById(contractView.getContractId());
					//getFacesContext().getExternalContext().getSessionMap().put(WebConstants.LOCAL_PERSON_ID, contractView.getTenantView().getPersonId());
					
					//Setting the Contract/Tenant Info
					setTenantName(contractView);
					contractNoText.setValue(contractView.getContractNumber());
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
					 // commented because their is a change in the tenant type 
					//tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
					if(getIsEnglishLocale())
					contractStatusText.setValue(contractView.getStatusEn());
					else
					contractStatusText.setValue(contractView.getStatusAr());
					totalContractValText.setValue(contractView.getRentAmount());
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
					    txtUnitRentValue.setValue(contractUnitView.getRentValue()!=null?contractUnitView.getRentValue().toString():"");
					
					}
					//Setting the Contract/Tenant InfoEnds

					if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.TASK_LIST_SELECTED_USER_TASK) == null){
						Boolean showBtns = false;
						HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
				    		       WebConstants.CONTRACT_STATUS_TERMINATED);
						if(hMap.containsKey("returnId")){
							Long contractExpiredStatusId = Long.parseLong(hMap.get("returnId").toString());
							if(contractExpiredStatusId.equals(contractView.getStatus()))
								showBtns = true;
							else
							    successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_CONTRACT_NOT_EXPIRED));
						}
						
					}
				
					
					return "Sucess";
				}
				else if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()==0)
					return "ZeroRecords";
				else
					return "Failure";
			}
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {			
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		return "";
	}
	public String populateContractFromPopUp(){
		String  METHOD_NAME = "populateContractFromPopUp|";		
        logger.logInfo(METHOD_NAME+"started...");
        

        try{
			if(hdnContractNumber!=null && !hdnContractNumber.equals(""))
			{
				clearValues();
				SystemParameters parameters = SystemParameters.getInstance();			
				String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
				try{				
					//contractView = propertyServiceAgent.getContract(contractNoText.getValue().toString(),dateFormat);
					contractView = propertyServiceAgent.getContract(hdnContractNumber,dateFormat);
					
					/*requestView = commUtil.getIncompleteRequestForRequestType(contractView.getContractId(), WebConstants.REQUEST_TYPE_CLEARANCE_LETTER);
					if(requestView!=null){
					//contractView = requestView.getContractView();
					populateApplicationDataTab(requestView.getRequestId().toString());
					this.requestId=requestView.getRequestId().toString(); 
					sendButton.setRendered(false);
					}*/
					
					contractNoText.setValue(contractView.getContractNumber());
					//Code for Navigation to Teneant Details Screen
					if (contractView != null && contractView.getTenantView() != null)
						tenantId = contractView.getTenantView().getPersonId();
					if (contractView != null)
						contractIdForNavigationToLeaseContract = contractView.getContractId();
				}
				catch (Exception e) {
					errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_NO_CONTRACT_FOUND));
					throw e;
				}
				if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()>0)
				{
					viewMap.put(WebConstants.Contract.LOCAL_CONTRACT_ID, contractView.getContractId());
					//getFacesContext().getExternalContext().getSessionMap().put(WebConstants.LOCAL_PERSON_ID, contractView.getTenantView().getPersonId());
					
					//Setting the Contract/Tenant Info
					setTenantName(contractView);
					contractTypeText.setValue(contractView.getContractTypeEn());
					contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
					contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
                    // commented because their is a change in the tenant type 					
					//tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
					if(getIsEnglishLocale())
					contractStatusText.setValue(contractView.getStatusEn());
					else
					contractStatusText.setValue(contractView.getStatusAr());
					totalContractValText.setValue(contractView.getRentAmount());
					//Added by shiraz for populate required field in the new screen
					if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
					{ 
						List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
						contractUnitViewList.addAll(contractView.getContractUnitView());
						ContractUnitView contractUnitView =contractUnitViewList.get(0);
						logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
						txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
						
						logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
					    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
					    
					    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
					    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
					    
					    
					    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
					    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
					
					    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
					    txtUnitRentValue.setValue(contractUnitView.getUnitView().getRentValue()!=null?contractUnitView.getUnitView().getRentValue().toString():"");
					
					}
					//Setting the Contract/Tenant InfoEnds
					//*****************************************************
                    //we only come here when terminated contract is selected that is why i commented this code 
					//*****************************************************
					/*if(FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(WebConstants.TASK_LIST_SELECTED_USER_TASK) == null){
						Boolean showBtns = false;
						HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
				    		       WebConstants.CONTRACT_STATUS_TERMINATED);
						if(hMap.containsKey("returnId")){
							Long contractExpiredStatusId = Long.parseLong(hMap.get("returnId").toString());
							if(contractExpiredStatusId.equals(contractView.getStatus()))
								showBtns = true;
							else
								writeMessage(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_CONTRACT_NOT_EXPIRED));						
						}
						sendButton.setRendered(showBtns);
					}*/
			
					
					return "Sucess";
				}
				else if(contractView.getContractNumber()!=null && contractView.getContractNumber().trim().length()==0)
					return "ZeroRecords";
				else
					return "Failure";
			}
			logger.logInfo(METHOD_NAME+"completed successfully...");
		}
		catch (Exception e) {			
			logger.LogException(METHOD_NAME + "crashed...", e);
		}
		hdnContractNumber = null;
		return "";
	}
	private void clearValues() {
		
		setTenantName(contractView);
		contractTypeText.setValue("");
		contractStartDateText.setValue("");
		contractEndDateText.setValue("");
		tenantNumberType.setValue("");		
		contractStatusText.setValue("");		
		totalContractValText.setValue("");
		feeInputText.setValue("");
		txtpropertyName.setValue("");
		txtpropertyType.setValue("");
		txtunitRefNum.setValue("");
		txtUnitRentValue.setValue("");
		txtUnitType.setValue("");
		
	}



	private boolean searchValid() {
		boolean returnVal = true;
		String contractNo = contractNoText.getValue().toString();
		if(contractNo.length() == 0)
			returnVal = false;			 
		return returnVal;
	}
	
	public void prerender(){
		super.prerender();
		String  METHOD_NAME = "prerender|";		
        logger.logInfo(METHOD_NAME+" started...");
        screenName = ResourceUtil.getInstance().getProperty("contract.screenName.cLRequest");
        
        if(hdnPersonId!=null && !hdnPersonId.equals("")){
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}

			if(hdnPersonName!=null && !hdnPersonName.equals(""))
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			
			
			if(hdnCellNo!=null && !hdnCellNo.equals(""))
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
			
			if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
				DomainDataView ddv = new DomainDataView();
				if(Long.parseLong(hdnIsCompany)==1L){
				        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_COMPANY);}
				else{
				        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_INDIVIDUAL);}
				
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
			
			}
			
			
				
        
		if(!isPostBack()){
			


			if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW) != null){
				RequestView requestView = (RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
				
				
				requestId = requestView.getRequestId().toString();
				contractId = requestView.getContractView().getContractId().toString();
				statusId = requestView.getStatusId().toString();
				
			}
			
			if(getFacesContext().getExternalContext().getRequestMap().get(WebConstants.Contract.CONTRACT_VIEW)!= null){
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.PAYMENT_RECEIPT_VIEW);
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			}
			if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			}
			
			
		
			
		}
		try{
			
			if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
			{
				boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
				sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
				sessionMap.put("PAYMENT_DONE", true);
				
				if(isPaymentCollected)
			       {
				    
						CollectPayment();
						
				if(requestId != null){
				
					  RequestServiceAgent rSAgent = new RequestServiceAgent();
					  rSAgent.setRequestAsApprovedCollected(Long.parseLong(requestId), getLoggedInUser());
                      completeRequest.setRendered(true);
                      cancelRequest.setRendered(false);
					  
				}


				    boolReprintBtn = true;					
					reprintButton.setRendered(true);
					payButton.setRendered(false);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					sendButton.setRendered(false);
					populateContract.setRendered(false);
					btnCollectPayment.setRendered(false);
					requestStatusForColumn = true;
					cancelRequest.setRendered(false);
									
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_LETTER_IS_PRINTED));
						
			       }
				
			}
			
			if(hdnContractId!=null && !hdnContractId.equals("")&& hdnContractNumber!=null && !hdnContractNumber.equals(""))
			{
				populateContractFromPopUp();
				// if send button is not press 
				if(!viewMap.containsKey("SEND_BUTTON"))
				sendButton.setRendered(true);
				
				paymentSchedules = propertyServiceAgent.getDefaultPaymentsForClearanceLetter(Long.parseLong(hdnContractId), getIsEnglishLocale());
				viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
				setPaymentSchedules(paymentSchedules);
				
			  }
			
			String selectedTab = (String)viewMap.get("selectedTab");
			if(selectedTab!=null){
				richTabPanel.setSelectedTab(selectedTab);
				viewMap.remove("selectedTab");
			}
			/*Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			cancelButton.setRendered(true);
			if(boolReprintBtn)
			reprintButton.setRendered(true);
			else
			reprintButton.setRendered(false);	
			

			feeInputText.setReadonly(true);

			contractNoText.setReadonly(true);
			tenantNameText.setReadonly(true);
			contractTypeText.setReadonly(true);
			contractStartDateText.setReadonly(true);
			contractEndDateText.setReadonly(true);		
			tenantNumberType.setReadonly(true);
			contractStatusText.setReadonly(true);
			totalContractValText.setReadonly(true);
			txtUnitType.setReadonly(true);
			txtpropertyName.setReadonly(true);
			txtpropertyType.setReadonly(true);
			txtunitRefNum.setReadonly(true);
			txtUnitRentValue.setReadonly(true);
			
			if (sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
			{
				boolean isPaymentCollected = (Boolean) (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
				sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
				sessionMap.put("PAYMENT_DONE", true);
				
				if(isPaymentCollected)
			       {
				    
						CollectPayment();
						
				if(requestId != null){
				
					  RequestServiceAgent rSAgent = new RequestServiceAgent();
					  rSAgent.setRequestAsApprovedCollected(Long.parseLong(requestId), getLoggedInUser());
                      completeRequest.setRendered(true);
                      cancelRequest.setRendered(true);
					  
				}


				    boolReprintBtn = true;					
					reprintButton.setRendered(true);
					payButton.setRendered(false);
					approveButton.setRendered(false);
					rejectButton.setRendered(false);
					sendButton.setRendered(false);
					populateContract.setRendered(false);
					requestStatus.setRendered(false);
					//completeRequest.setRendered(false);
					//cancelRequest.setRendered(false);
					requestStatusForColumn = true;
					//viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
									
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.RenewContract.MSG_PAYMENT_RECIVED));
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_LETTER_IS_PRINTED));
						
			       }
			}
			
			if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIPT_VIEW)!=null){ //&& ValidatePayment()){
				RequestView sessionRequestView = new RequestView();
				sessionRequestView.setRequestId(Long.parseLong(requestId));
				PaymentReceiptView paymentReceiptView;
				paymentReceiptView =(PaymentReceiptView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIPT_VIEW);
				paymentReceiptView.setRequestView(sessionRequestView);
				
				//propertyServiceAgent.persistPaymentTransaction(paymentReceiptView);
				//updatePaymentSchedule(Long.parseLong(requestId));
				
				populateApplicationDataTab(requestId);
				
				RequestServiceAgent rSAgent = new RequestServiceAgent();
				rSAgent.setCLRequestAsCompleted(sessionRequestView.getRequestId());
				
				getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.PAYMENT_RECEIPT_VIEW);
				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.PAYMENT_RECEIVED, true);
				//Insert code to print the Clearance Letter
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_LETTER_IS_PRINTED));
				reprintButton.setRendered(true);
				payButton.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				sendButton.setRendered(false);
				populateContract.setRendered(false);
				
				loadAttachmentsAndComments(Long.parseLong(requestId));
			}
			else if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIVED) != null
					&& (Boolean)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIVED)){
				reprintButton.setRendered(true);
				payButton.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				sendButton.setRendered(false);
				populateContract.setRendered(false);
				
				loadAttachmentsAndComments(Long.parseLong(requestId));
				
				populateApplicationDataTab(requestId);
			}
			else if(requestId != null && !requestId.equals("")
					&& contractId != null && !contractId.equals("")){
				
				sendButton.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				populateContract.setRendered(false);
				completeRequest.setRendered(false);
				cancelRequest.setRendered(false);
				requestStatus.setRendered(false);
				
				populateApplicationDataTab(requestId);
				
				try{				
			   
					contractView = getContractById(Long.parseLong(contractId));

				
				//Setting the Contract/Tenant Info
				contractNoText.setValue(contractView.getContractNumber());
				setTenantName(contractView);				
				contractTypeText.setValue(contractView.getContractTypeEn());
				contractStartDateText.setValue(getStringFromDate(contractView.getStartDate()));
				contractEndDateText.setValue(getStringFromDate(contractView.getEndDate()));
				 // commented because their is a change in the tenant type 
//				 tenantNumberType.setValue(getIsEnglishLocale()?contractView.getTenantView().getTypeEn():contractView.getTenantView().getTypeAr());
				//Added by shiraz for populate required field in the new screen
				if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
				{ 
					List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
					contractUnitViewList.addAll(contractView.getContractUnitView());
					ContractUnitView contractUnitView =contractUnitViewList.get(0);
					logger.logInfo("|" + "ContractType::"+contractUnitView.getUnitView().getUnitTypeId() );
					txtUnitType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
					
					logger.logInfo("|" + "PropertyCommercialName::"+contractUnitView.getUnitView().getPropertyCommercialName());
				    txtpropertyName.setValue(contractUnitView.getUnitView().getPropertyCommercialName());
				    
				    logger.logInfo("|" + "PropertyType::"+contractUnitView.getUnitView().getPropertyTypeId());
				    txtpropertyType.setValue(getIsEnglishLocale()?contractUnitView.getUnitView().getPropertyTypeEn():contractUnitView.getUnitView().getPropertyTypeAr());
				    
				    
				    logger.logInfo("|" + "UnitRefNumber::"+contractUnitView.getUnitView().getUnitNumber());
				    txtunitRefNum.setValue(contractUnitView.getUnitView().getUnitNumber());
				
				    logger.logInfo("|" + "UnitRent::"+contractUnitView.getUnitView().getRentValue());
				    txtUnitRentValue.setValue(contractUnitView.getUnitView().getRentValue()!=null?contractUnitView.getUnitView().getRentValue().toString():"");
				
				}

				if(getIsEnglishLocale())
					contractStatusText.setValue(contractView.getStatusEn());
				else
					contractStatusText.setValue(contractView.getStatusAr());
				
				totalContractValText.setValue(contractView.getRentAmount());
				//Setting the Contract/Tenant InfoEnds
				contractStatusText.setReadonly(true);
				totalContractValText.setReadonly(true);
				
			  
				
				
				//reprintButton.setRendered(false);
				RequestView requestView = new RequestView();
				requestView.setStatusId(Long.parseLong(statusId));
				managePaymentButton(requestView);
				loadAttachmentsAndComments(Long.parseLong(requestId));
				
				if  (contractId != null && !contractId.equals("")){
				
					if(requestView.getStatusId().equals(getApprovedStatusId())){
					paymentSchedules = propertyServiceAgent.getContractPaymentSchedule(null,Long.parseLong(requestId));
					sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
					if (!viewMap.containsKey("cashId") && !viewMap.containsKey("chqId"))
					{
						UtilityServiceAgent usa = new UtilityServiceAgent();
						try {
							
							DomainDataView ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
							viewMap.put("cashId", ddv.getDomainDataId());
							
							ddv = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
							viewMap.put("chqId", ddv.getDomainDataId());
							
						} catch (PimsBusinessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					calculateSummaray();
					}

				}

			        
				}catch (Exception e) {					
					throw e;
				}
			}
			else{
				payButton.setRendered(false);
				reprintButton.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				contractNoText.setReadonly(false);
				requestStatus.setRendered(false);
				completeRequest.setRendered(false);
				cancelRequest.setRendered(false);
				
				
				UserTask task = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				String taskType = "";
				if(task != null || viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
					
					if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null){
						UserTask userTask = (UserTask) getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
						taskType = userTask.getTaskType();
						viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
						task = userTask;
						getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
					}
					else{
						task = (UserTask)viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
					}
					
					
					
					
					Map taskAttributes =  task.getTaskAttributes();

					Long requestId1 = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
					
					requestId=requestId1.toString();
					RequestView requestView = new RequestView();
					requestView.setRequestId(requestId1);
					List<RequestView> requestList = new PropertyServiceAgent().getAllRequests(requestView, "", "",null);
                    RequestView reqView = (RequestView)requestList.iterator().next();
					String contractNo = "";
					contractNo = reqView.getContractView().getContractNumber();
					
					getContractById(reqView.getContractView().getContractId());
					
					
					contractNoText.setValue(contractNo);
					populateContract();
					sendButton.setRendered(false);
					populateContract.setRendered(false);
					
					loadAttachmentsAndComments(requestId1);
					
					populateApplicationDataTab(requestId.toString());
					
					if(taskType.equals("ApproveClearanceRequest")){
						
						if(!viewMap.containsKey("PAYMENT_SCHEDULES")){
							paymentSchedules = propertyServiceAgent.getDefaultPaymentsForClearanceLetter(reqView.getContractView().getContractId(), getIsEnglishLocale());
							viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
							setPaymentSchedules(paymentSchedules);
						}
					}
					
					
					if(viewRootMap.get(WebConstants.ClearanceLetter.IS_TASK_COMPLETED) != null){
						requestStatus.setRendered(false);
						approveButton.setRendered(false);
						rejectButton.setRendered(false);
						completeRequest.setRendered(false);
						cancelRequest.setRendered(false);
						
					}
					else{
						approveButton.setRendered(true);
						rejectButton.setRendered(true);
					}
					
					
			 DomainDataView ddv= commUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
     		       WebConstants.REQUEST_STATUS_APPROVED);
				DomainDataView ddv1= commUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
		     		       WebConstants.REQUEST_STATUS_APPROVED_COLLECTED);
        
			if ((ddv.getDomainDataId().equals(reqView.getStatusId())
				||ddv1.getDomainDataId().equals(reqView.getStatusId()))
				&&(!(viewRootMap.get(WebConstants.ClearanceLetter.IS_TASK_COMPLETED) != null))){
				requestStatus.setRendered(true);
				requestStatusForColumn = true;
				completeRequest.setRendered(true);
				cancelRequest.setRendered(true);
				
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				
			}
			else{
				requestStatus.setRendered(false);
				requestStatusForColumn = false;
				completeRequest.setRendered(false);
				cancelRequest.setRendered(false);
				
			   }
			ddv= commUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
	     		       WebConstants.REQUEST_STATUS_COMPLETE);
			if((ddv1.getDomainDataId().equals(reqView.getStatusId()))
				||(ddv.getDomainDataId().equals(reqView.getStatusId()))){
				requestStatus.setRendered(false);
				cancelRequest.setRendered(false);
				approveButton.setRendered(false);
				rejectButton.setRendered(false);
				requestStatusForColumn = true;
				
			paymentSchedules = propertyServiceAgent.getContractPaymentSchedule(null,Long.parseLong(requestId));
			viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
			setPaymentSchedules(paymentSchedules);
			
				ddv= commUtil.getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
		     		       WebConstants.PAYMENT_SCHEDULE_COLLECTED);	
				if((ddv.getDomainDataId().equals(paymentSchedules.get(0).getStatusId()))
						&&(!(viewRootMap.get(WebConstants.ClearanceLetter.IS_TASK_COMPLETED) != null))){
					sessionMap.put("PAYMENT_DONE", true);
					requestStatus.setRendered(false);
					completeRequest.setRendered(true);
				}else if((!ddv.getDomainDataId().equals(paymentSchedules.get(0).getStatusId()))
						&&(!(viewRootMap.get(WebConstants.ClearanceLetter.IS_TASK_COMPLETED) != null))){
					sessionMap.remove("PAYMENT_DONE");
					requestStatus.setRendered(true);
					completeRequest.setRendered(true);
					cancelRequest.setRendered(true);	
				}
				
				if (!viewMap.containsKey("cashId") && !viewMap.containsKey("chqId"))
				{
					UtilityServiceAgent usa = new UtilityServiceAgent();
					try {
						
						DomainDataView ddv2 = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
						viewMap.put("cashId", ddv2.getDomainDataId());
						
						ddv2 = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
						viewMap.put("chqId", ddv2.getDomainDataId());
						
					} catch (PimsBusinessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
		}
			
		
			if(reqView.getStatusId().equals(getApprovedStatusId())){
				paymentSchedules = propertyServiceAgent.getContractPaymentSchedule(null,Long.parseLong(requestId));
				viewMap.put("PAYMENT_SCHEDULES",paymentSchedules);
				setPaymentSchedules(paymentSchedules);
				
				
				if (!viewMap.containsKey("cashId") && !viewMap.containsKey("chqId"))
				{
					UtilityServiceAgent usa = new UtilityServiceAgent();
					try {
						
						DomainDataView ddv2 = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
						viewMap.put("cashId", ddv2.getDomainDataId());
						
						ddv2 = usa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
						viewMap.put("chqId", ddv2.getDomainDataId());
						
					} catch (PimsBusinessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//calculateSummaray();
				}
				
				}
				sendButton.setRendered(false);
//when we r comming from contract search 

			}
			logger.logInfo(METHOD_NAME+" completed successfully...");*/
		}
		catch (Exception e) {
			releaseSessionInfo();
			logger.LogException(METHOD_NAME + "crashed...", e);			
		}		
		
		
		
		
		/*if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIPT_VIEW)!=null){
			if(ValidatePayment().equals(true)){
				reprintButton.setRendered(true);
				payButton.setRendered(false);
			}			
		}*/
		/*if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIVED)!=null){
			reprintButton.setRendered(true);
			payButton.setRendered(false);
		}*/
		
	
	}

	private void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			
			ArrayList<ContractView> list;
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
			try
			{
				ContractView contract=new ContractView();
				contract.setContractId(new Long(contractId));
				
				HashMap contractHashMap=new HashMap();
				contractHashMap.put("contractView",contract);
				
				contractHashMap.put("dateFormat",getDateFormat());
				PropertyServiceAgent psa =new PropertyServiceAgent();
				logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
				List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
				
				logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
				if(contractViewList.size()>0)
				contractView=(ContractView)contractViewList.get(0);
				viewMap.put("CONTRACTVIEWUPDATEMODE",contractView);
				//not in amend mode the request should be filled 
				
				
			
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|"+"Error occured:",ex);
				throw ex;
				
			}
	}
	
	private Long getPaymentScheduleCollectedStatusId() {
		Long statusId = new Long(0);
		String methodName="getPaymentScheduleCollectedStatusId|";
		logger.logDebug(methodName+"started..");
		try{
        	HashMap hMap= getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
        		       WebConstants.PAYMENT_SCHEDULE_COLLECTED);
       		if(hMap.containsKey("returnId")){
       			statusId = Long.parseLong(hMap.get("returnId").toString());
       			if(statusId.equals(null))
       				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
       		}
			
			logger.logDebug(methodName+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		return statusId;
	}
	
	private Long getPaymentSchedulePendingStatusId() {
		Long statusId = new Long(0);
		String methodName="getPaymentSchedulePendingStatusId|";
		logger.logDebug(methodName+"started..");
		try{
        	HashMap hMap= getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
        		       WebConstants.PAYMENT_SCHEDULE_PENDING);
       		if(hMap.containsKey("returnId")){
       			statusId = Long.parseLong(hMap.get("returnId").toString());
       			if(statusId.equals(null))
       				throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
       		}
			
			logger.logDebug(methodName+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		return statusId;
	}

/*	public String pay(){
		String methodName="pay|";
		logger.logInfo(methodName+"started..");
		try{
			Map map = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			PaymentReceiptView paymentreceiptView = new PaymentReceiptView();
			paymentreceiptView.setAmount(Double.parseDouble(feeInputText.getValue().toString()));			
			map.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentreceiptView);
			
			logger.logInfo(methodName+"|"+"completed successfully..");
		}
		catch (Exception e) {
			logger.LogException(methodName+"crashed...", e);
		}
		
		
		openPopUp("receivePayment.jsf", "");		
		return "";
	}*/
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		final String viewId = "/CLRequest.jsp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+1024+',height='+450+',left=0,top=40,scrollbars=yes,status=yes');";
      if (extraJavaScript!=null&& extraJavaScript.trim().length()>0)      
    		javaScriptText=extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	public String rePrintReceipt(){
		String methodName="rePrintReceipt";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
		

		//Add printing code here
    		clearenceLetterCriteria=new ClearenceLetterCriteria(ReportConstant.Report.CLEARANCE_LETTER_REPORT, ReportConstant.Processor.CLEARANCE_LETTER_REPORT,CommonUtil.getLoggedInUser());
    		clearenceLetterCriteria.setRequestId(this.requestId);
    		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, clearenceLetterCriteria);
    		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
    	
		
		successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_LETTER_IS_RE_PRINTED));
		
		
		logger.logInfo(methodName+"|"+"Finish..");
       
		return "";
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try { 
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	/**
	 * Checks whether the payment receipt details contained in the session are enough for the request to be saved
	 * @return validation status
	 */
	/*private Boolean ValidatePayment() {
		Boolean isPaymentValid = false;
		String  methodName = "clearAllData";		
        logger.logInfo(methodName+" started...");
        try{
        	Double feeAmount = Double.parseDouble(feeInputText.getValue().toString());
        	Map map = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			Object obj = map.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			int val = 0;
			if(obj != null){
				if( ((PaymentReceiptView)obj).getAmountReturn() == null  || ((PaymentReceiptView)obj).getAmountReturn() < 0){
					isPaymentValid = false;
					return isPaymentValid;
				}
				Double receivedAmount  = getPaymentAmountTotal((PaymentReceiptView)obj) - ((PaymentReceiptView)obj).getAmountReturn(); 
				val = receivedAmount.compareTo(feeAmount);				
			}
			if( (obj != null) &&
				( ((PaymentReceiptView)obj).getPaymentReceiptDetails().size()>0 ) &&
				( val == 0 )
				){
				isPaymentValid = true;
			}
			else{
				isPaymentValid = false;
			}
	        logger.logInfo(methodName+" completed successfully...");
        } catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}       
		return isPaymentValid;
	}*/
	
	/**
	 * Iterates through the Payment Receipt View's PaymentReceiptDetailView 
	 * to add the amounts and return the amount total 
	 * @param paymentReceiptView
	 * @return
	 */
	private Double getPaymentAmountTotal(PaymentReceiptView paymentReceiptView) {
		Double totalAmount = new Double(0);
		String  methodName = "getPaymentAmountTotal";		
        logger.logInfo(methodName+" started...");
        try{
			Iterator paymentReceiptIterator = paymentReceiptView.getPaymentReceiptDetails().iterator();
			while(paymentReceiptIterator.hasNext()){
				PaymentReceiptDetailView paymentReceiptDetailView = (PaymentReceiptDetailView)paymentReceiptIterator.next();
				totalAmount += paymentReceiptDetailView.getAmount();
			}
	        logger.logInfo(methodName+" completed successfully...");
        } catch (Exception exception) {
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}   
		return totalAmount;
	}
	
	public HtmlCommandButton getCancelButton() {
		return cancelButton;
	}
	public void setCancelButton(HtmlCommandButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	public HtmlInputText getContractEndDateText() {
		return contractEndDateText;
	}
	public void setContractEndDateText(HtmlInputText contractEndDateText) {
		this.contractEndDateText = contractEndDateText;
	}
	public HtmlInputText getContractNoText() {
		return contractNoText;
	}
	public void setContractNoText(HtmlInputText contractNoText) {
		this.contractNoText = contractNoText;
	}
	public HtmlInputText getContractStartDateText() {
		return contractStartDateText;
	}
	public void setContractStartDateText(HtmlInputText contractStartDateText) {
		this.contractStartDateText = contractStartDateText;
	}
	public HtmlInputText getContractTypeText() {
		return contractTypeText;
	}
	public void setContractTypeText(HtmlInputText contractTypeText) {
		this.contractTypeText = contractTypeText;
	}
	public HtmlInputText getFeeText() {
		return feeText;
	}
	public void setFeeText(HtmlInputText feeText) {
		this.feeText = feeText;
	}
	public HtmlSelectOneMenu getNolTypeSelectOneMenu() {
		return nolTypeSelectOneMenu;
	}
	public void setNolTypeSelectOneMenu(HtmlSelectOneMenu nolTypeSelectOneMenu) {
		this.nolTypeSelectOneMenu = nolTypeSelectOneMenu;
	}
	public HtmlCommandLink getPopulateContract() {
		return populateContract;
	}
	public void setPopulateContract(HtmlCommandLink populateContract) {
		this.populateContract = populateContract;
	}
	public HtmlInputTextarea getReasontForNol() {
		return reasontForNol;
	}
	public void setReasontForNol(HtmlInputTextarea reasontForNol) {
		this.reasontForNol = reasontForNol;
	}
	public HtmlCommandButton getSendButton() {
		return sendButton;
	}
	public void setSendButton(HtmlCommandButton sendButton) {
		this.sendButton = sendButton;
	}
	public HtmlInputText getTenantNameText() {
		return tenantNameText;
	}
	public void setTenantNameText(HtmlInputText tenantNameText) {
		this.tenantNameText = tenantNameText;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public String getMessages() {
		String messageList;
		if ((messages == null) ||
			(messages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "";
			for(String message: messages) {
				messageList = messageList + message + "<BR>\n";
		}
		messageList = messageList + "<BR>\n";
		}
		return(messageList);
	}
	
	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				logger.logInfo(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
			   	hMap.put("returnId",domainDataView.getDomainDataId());
			   	logger.logInfo(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
			   	hMap.put("IdEn",domainDataView.getDataDescEn());
			   	hMap.put("IdAr",domainDataView.getDataDescAr());
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return hMap;
		
	}
	
    private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
				  ddvList.add(ddv);	
				}
				break;
			}
		}
    	
    	return ddvList;
    }
	
    public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public HtmlCommandButton getRejectButton() {
		return rejectButton;
	}
	public void setRejectButton(HtmlCommandButton rejectButton) {
		this.rejectButton = rejectButton;
	}
	public HtmlCommandButton getApproveButton() {
		return approveButton;
	}
	public void setApproveButton(HtmlCommandButton approveButton) {
		this.approveButton = approveButton;
	}
	public HtmlInputText getFeeInputText() {
		return feeInputText;
	}
	public void setFeeInputText(HtmlInputText feeInputText) {
		this.feeInputText = feeInputText;
	}

	public HtmlCommandButton getPayButton() {
		return payButton;
	}

	public void setPayButton(HtmlCommandButton payButton) {
		this.payButton = payButton;
	}

	public HtmlCommandButton getReprintButton() {
		return reprintButton;
	}

	public void setReprintButton(HtmlCommandButton reprintButton) {
		this.reprintButton = reprintButton;
	}
	
	
	
	
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	
	public String getFormattedDoubleString(Double val, String pattern){
		String doubleStr = "0.00";
		try{
			DecimalFormat df = new DecimalFormat();
			df.applyPattern(pattern);
			doubleStr = df.format(val);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return doubleStr;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public HtmlInputText getTenantNumberType() {
		return tenantNumberType;
	}

	public void setTenantNumberType(HtmlInputText tenantNumberType) {
		this.tenantNumberType = tenantNumberType;
	}

	public HtmlInputText getContractStatusText() {
		return contractStatusText;
	}

	public void setContractStatusText(HtmlInputText contractStatusText) {
		this.contractStatusText = contractStatusText;
	}

	public HtmlInputText getTotalContractValText() {
		return totalContractValText;
	}

	public void setTotalContractValText(HtmlInputText totalContractValText) {
		this.totalContractValText = totalContractValText;
	}
	
/*	public String btnTenant_Click()
	{
		
		String methodName="btnTenant_Click";
		
		String returnString = "";
		
		logger.logInfo(methodName+"|"+"Start..");
		
		if (tenantId != null)
		{
		
		returnString = "TENANT_VIEW";
		context.getExternalContext().getRequestMap().put("tenantId", tenantId.toString());
		context.getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		
		}
		
		logger.logInfo(methodName+"|"+"Finish..");
		
		return returnString;
	}*/
	
	public String btnTenant_Click() {
		String methodName = "btnTenant_Click";
		logger.logInfo(methodName + "|Start");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		BidderAttendanceView dataItem = new BidderAttendanceView();
		
		if(viewMap.get(WebConstants.LOCAL_PERSON_ID)!=null
				&& !viewMap.get(WebConstants.LOCAL_PERSON_ID).equals("")){
		
		logger.logDebug(methodName + "|Person Id::" + dataItem.getBidderId());
		setRequestParam(WebConstants.PERSON_ID, dataItem.getBidderId());
		setRequestParam("viewMode", "popup");
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+viewMap.get(WebConstants.LOCAL_PERSON_ID).toString()+");";

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		}
		logger.logInfo(methodName + "|Finish");
		return "";

	}
	public String btnContract_Click()
	{

		String methodName="btnContract_Click";
		logger.logInfo(methodName+"|"+"Start..");
		if(viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID)!=null 
				&& !viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).equals("")){
		context.getExternalContext().getSessionMap().put("contractId", viewMap.get(WebConstants.Contract.LOCAL_CONTRACT_ID).toString());
		context.getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
		HttpServletRequest request =(HttpServletRequest) context.getExternalContext().getRequest();
		
		String javaScriptText="var screen_width = screen.width;"+
        "var screen_height = screen.height;"+
        
        "window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
                              WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
                              WebConstants.VIEW_MODE+"="+
                              WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
                              "','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";
        //"window.open('LeaseContract.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=10,scrollbars=no,status=yes');";
       openPopUp("LeaseContract.jsf",javaScriptText);
		logger.logInfo(methodName+"|"+"Finish..");
		
		//return "CONTRACT_VIEW";
		}
		return "";
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getContractIdForNavigationToLeaseContract() {
		return contractIdForNavigationToLeaseContract;
	}

	public void setContractIdForNavigationToLeaseContract(
			Long contractIdForNavigationToLeaseContract) {
		this.contractIdForNavigationToLeaseContract = contractIdForNavigationToLeaseContract;
	}

	public HtmlInputText getTxtUnitType() {
		return txtUnitType;
	}

	public void setTxtUnitType(HtmlInputText txtUnitType) {
		this.txtUnitType = txtUnitType;
	}

	public HtmlInputText getTxtpropertyName() {
		return txtpropertyName;
	}

	public void setTxtpropertyName(HtmlInputText txtpropertyName) {
		this.txtpropertyName = txtpropertyName;
	}

	public HtmlInputText getTxtunitRefNum() {
		return txtunitRefNum;
	}

	public void setTxtunitRefNum(HtmlInputText txtunitRefNum) {
		this.txtunitRefNum = txtunitRefNum;
	}

	public HtmlInputText getTxtUnitRentValue() {
		return txtUnitRentValue;
	}

	public void setTxtUnitRentValue(HtmlInputText txtUnitRentValue) {
		this.txtUnitRentValue = txtUnitRentValue;
	}

	public HtmlInputText getTxtpropertyType() {
		return txtpropertyType;
	}

	public void setTxtpropertyType(HtmlInputText txtpropertyType) {
		this.txtpropertyType = txtpropertyType;
	}

	public String getHdnContractId() {
		return hdnContractId;
	}

	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}

	public String getHdnContractNumber() {
		return hdnContractNumber;
	}

	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}

	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}

	public String getClStatus(){
		CommonUtil comUtil = new CommonUtil();
		DomainDataView ddv= comUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),
	 		       WebConstants.CONTRACT_STATUS_TERMINATED
	              );
		
	  return ddv.getDomainDataId().toString();
	}
	public void populateApplicationDataTab(String requestId){
		
		RequestView rV= new RequestView();
		List<RequestView> rVList = new ArrayList<RequestView>();
		try {
		rV.setRequestId(new Long(requestId));
		PropertyServiceAgent psa = new PropertyServiceAgent();
	    rVList = psa.getAllRequests(rV, null, null, null);
		rV = rVList.get(0);
		if(rV.getRequestNumber()!=null && !rV.getRequestNumber().equals(""))
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, rV.getRequestNumber());
		if(rV.getStatusId().toString()!=null && !rV.getStatusId().toString().equals(""))
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?rV.getStatusEn():rV.getStatusAr());
		if(rV.getRequestDate()!=null)
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_DATE, rV.getRequestDate());
		if(rV.getDescription()!=null && !rV.getDescription().equals(""))
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, rV.getDescription());
		if(rV.getRequestId()!=null && !rV.getRequestId().equals("")){
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_ID,rV.getRequestId());
		}
		
		//for read only description 
		viewMap.put("applicationDetailsReadonlyMode", true);
		viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
	
		if(rV.getApplicantView()!=null && !rV.getApplicantView().equals("")){
			viewMap.put("APPLICANT_VIEW_FOR_NOTIFICATION", rV.getApplicantView());
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals(""))
						FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,rV.getApplicantView().getPersonId());
				
				if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals("")){
						viewMap.put(WebConstants.LOCAL_PERSON_ID, rV.getApplicantView().getPersonId());
					}
				
				if(rV.getApplicantView().getFirstName()!=null && !rV.getApplicantView().getFirstName().equals(""))
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
							(rV.getApplicantView().getFirstName()!=null && rV.getApplicantView().getFirstName().length()>0? rV.getApplicantView().getFirstName():"")+" "+
				            (rV.getApplicantView().getMiddleName()!=null && rV.getApplicantView().getMiddleName().length()>0?rV.getApplicantView().getMiddleName():"")+" "+
				            (rV.getApplicantView().getLastName()!=null && rV.getApplicantView().getLastName().length()>0?rV.getApplicantView().getLastName():""));
				
				if(rV.getApplicantView().getPersonTypeId()!=null && !rV.getApplicantView().getPersonTypeId().equals(""))
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
							getIsEnglishLocale()?rV.getApplicantView().getPersonTypeEn():rV.getApplicantView().getPersonTypeAr());
				if(rV.getApplicantView().getCellNumber()!=null && !rV.getApplicantView().getCellNumber().equals(""))
					FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,rV.getApplicantView().getCellNumber());
				
				if(rV.getApplicantView().getIsCompany()!=null && !rV.getApplicantView().getIsCompany().equals("")){
					DomainDataView ddv = new DomainDataView();
					if(rV.getApplicantView().getIsCompany()==1L){
					        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_COMPANY);}
					else{
					        ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
							WebConstants.TENANT_TYPE_INDIVIDUAL);}
					
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
		
		}

		}

		
		
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public List<PaymentScheduleView> getPaymentSchedules() {
		if (viewMap.get("PAYMENT_SCHEDULES")!=null)
			paymentSchedules = (List<PaymentScheduleView>) viewMap.get("PAYMENT_SCHEDULES");
		return paymentSchedules;
	}

	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
		if(this.paymentSchedules!=null)
			viewMap.put("PAYMENT_SCHEDULES", this.paymentSchedules);
	}

	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}

	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}

	public boolean isStatusIsNotSentForApprovalAndNotApproved() {
		return statusIsNotSentForApprovalAndNotApproved;
	}

	public void setStatusIsNotSentForApprovalAndNotApproved(
			boolean statusIsNotSentForApprovalAndNotApproved) {
		this.statusIsNotSentForApprovalAndNotApproved = statusIsNotSentForApprovalAndNotApproved;
	}

	public String getDateFormatForDataTable() {
		return dateFormatForDataTable;
	}

	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}
	
	public String deletePayment()
	{
		try{
			
		
		int rowID = dataTablePaymentSchedule.getRowIndex();
		
		paymentSchedules = getPaymentSchedules();
		paymentSchedules.remove(rowID);
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}

	public double getTotalRentAmount() {
		return totalRentAmount;
	}

	public void setTotalRentAmount(double totalRentAmount) {
		this.totalRentAmount = totalRentAmount;
	}

	public double getTotalCashAmount() {
		return totalCashAmount;
	}

	public void setTotalCashAmount(double totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}

	public double getTotalChqAmount() {
		return totalChqAmount;
	}

	public void setTotalChqAmount(double totalChqAmount) {
		this.totalChqAmount = totalChqAmount;
	}

	public double getTotalDepositAmount() {
		return totalDepositAmount;
	}

	public void setTotalDepositAmount(double totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}

	public double getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(double totalFees) {
		this.totalFees = totalFees;
	}

	public double getTotalFines() {
		return totalFines;
	}

	public void setTotalFines(double totalFines) {
		this.totalFines = totalFines;
	}

	public int getInstallments() {
		return installments;
	}

	public void setInstallments(int installments) {
		this.installments = installments;
	}
	
	
	
	public void tabRequestHistory_Click()
    {
    	String methodName="tabRequestHistory_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  
    	    rhc.getAllRequestTasksForRequest(WebConstants.CLEARANCE_LETTER,requestId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    }

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}
	@SuppressWarnings("unchecked")
	public String openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("payments", paymentSchedules);
		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentSchedules);
		sessionMap.put("contractId", contractId);
        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
        openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");
		logger.logInfo(methodName+"|"+"Finish..");
    	
		return "";
	}
	
	public String showPop()
	{
		
		return "";
	}
	
	 private void CollectPayment()
	    {
	    	String methodName="CollectPayment"; 
			logger.logInfo(methodName + "|" + "Start...");
	        successMessages = new ArrayList<String>(0);
			
	        try
			{
		    	 PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			     PropertyServiceAgent psa=new PropertyServiceAgent();
			     
			     if(viewMap.containsKey(WebConstants.REQUEST_VIEW))
			     {
			    	 RequestView rv  = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
				     prv.setPaidById(rv.getContractView().getTenantView().getPersonId());
			     }
			     
			     prv=  psa.collectPayments(prv);
			     //Refresh Payment Schedule list here
			     contractId = (String) sessionMap.get("contractId");
			     CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			     CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.CLEARENCE_LETTER);
			     //getContractView();
			     //getContractPaymentSchedules();
			     sessionMap.remove("contractId");
			     logger.logInfo(methodName + "|" + "Finish...");
			}
			catch(Exception ex)
			{
				logger.LogException(methodName + "|" + "Exception Occured...",ex);
				
			}
	    }
	 public void btnPrintPaymentSchedule_Click()
	    {
	    	String methodName = "btnPrintPaymentSchedule_Click";
	    	logger.logInfo(methodName+"|"+" Start...");
	    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
	    	CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "", getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.CLEARENCE_LETTER);
	    	logger.logInfo(methodName+"|"+" Finish...");
	    }
	  
	public HtmlCommandButton getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(HtmlCommandButton requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Boolean getRequestStatusForColumn() {
		return requestStatusForColumn;
	}

	public void setRequestStatusForColumn(Boolean requestStatusForColumn) {
		this.requestStatusForColumn = requestStatusForColumn;
	}


	public String completed(){
		String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
		String  METHOD_NAME = "completed|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        logger.logInfo(METHOD_NAME+" started...");
        try {
          
         if(sessionMap.containsKey("PAYMENT_DONE") && sessionMap.get("PAYMENT_DONE")!=null ){
          sessionMap.remove("PAYMENT_DONE");
          //if(true){
        	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			NotesController.saveSystemNotesForRequest(WebConstants.CLEARANCE_LETTER,MessageConstants.RequestEvents.REQUEST_COMPLETED, requestId);
			
			this.requestId  = requestId.toString();
			
			  RequestServiceAgent rSAgent = new RequestServiceAgent();
			  rSAgent.setRequestAsCompleted(requestId, getLoggedInUser());
              
			  CommonUtil.loadAttachmentsAndComments(requestId.toString());
			  CommonUtil.saveAttachments(requestId);
			  CommonUtil.saveComments(requestId, noteOwner);
			
			
			client.completeTask(userTask, loggedInUser, TaskOutcome.COMPLETE);
			notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_CL_Request_Completed);
			viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
			

			approveButton.setRendered(false);
			rejectButton.setRendered(false);
			populateContract.setRendered(false);
			completeRequest.setRendered(false);
            cancelRequest.setRendered(false);
            btnCollectPayment.setRendered(false);
			
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_COMPLETE_SUCCESS));
			
        	logger.logInfo(METHOD_NAME+" completed successfully...");
          
          }else{
        	  
        	  errorMessages.add(ResourceUtil.getInstance().getProperty(WebConstants.CLREQUEST_COLLECT_FEE));
        	  return "";
              }
		} 
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to approve due to:",ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_COMLETE_FAILURE));
			ex.printStackTrace();
			outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
		}
        catch (Exception e) 
        {
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_COMLETE_FAILURE));
		}
        outcome = "";
		return outcome;
	}

	public String canceled(){
		String outcome = PageOutcomes.SuccessToApproveClearanceLetterRequest;
		String  METHOD_NAME = "canceled|";
		errorMessages = new ArrayList<String>(0);
        successMessages = new ArrayList<String>(0);
        logger.logInfo(METHOD_NAME+" started...");
        try {
        	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
        	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is:"+contextPath);
	        String loggedInUser = getLoggedInUser();
			BPMWorklistClient client = new BPMWorklistClient(contextPath);			
			
			
			Map taskAttributes =  userTask.getTaskAttributes();
			Long requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
			
			NotesController.saveSystemNotesForRequest(WebConstants.CLEARANCE_LETTER,MessageConstants.RequestEvents.REQUEST_CANCELED, requestId);
			
			this.requestId  = requestId.toString();
			RequestServiceAgent rSAgent = new RequestServiceAgent();
			  rSAgent.setRequestAsCanceled(requestId, getLoggedInUser());
			CommonUtil.loadAttachmentsAndComments(requestId.toString());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId, noteOwner);
			
			
			client.completeTask(userTask, loggedInUser, TaskOutcome.CANCEL);
			viewRootMap.put(WebConstants.ClearanceLetter.IS_TASK_COMPLETED, true);
			
			approveButton.setRendered(false);
			rejectButton.setRendered(false);
			populateContract.setRendered(false);
			completeRequest.setRendered(false);
            cancelRequest.setRendered(false);
            btnCollectPayment.setRendered(false);
			
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_CANCEL_SUCCESS));
        	logger.logInfo(METHOD_NAME+" completed successfully...");
		} 
		catch(PIMSWorkListException ex)
		{
			logger.logError("Failed to approve due to:",ex);
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_CANCEL_FAILURE));
			ex.printStackTrace();
			outcome = PageOutcomes.FailureToApproveClearanceLetterRequest;
		}
        catch (Exception e) 
        {
			logger.LogException(METHOD_NAME + "crashed...", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ClearanceLetter.MSG_REQUEST_CANCEL_FAILURE));
		}
        outcome = "";
		return outcome;
	}

	public HtmlCommandButton getCompleteRequest() {
		return completeRequest;
	}

	public void setCompleteRequest(HtmlCommandButton completeRequest) {
		this.completeRequest = completeRequest;
	}

	public HtmlCommandButton getCancelRequest() {
		return cancelRequest;
	}

	public void setCancelRequest(HtmlCommandButton cancelRequest) {
		this.cancelRequest = cancelRequest;
	}

	public String getErrorMessages() 
	{
		
	   return commUtil.getErrorMessages(errorMessages);
		
	}
	public String getSuccessMessages() 
	{
	
		return commUtil.getErrorMessages(successMessages);
	}


	private Long getOwnerShipTypeId(){
		 Iterator iter=contractView.getContractUnitView().iterator();
  	  while(iter.hasNext())
  	  {
  		  
  		  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
  		  return contractUnitView.getUnitView().getPropertyCategoryId();
  	  }
  	  return null;
	}
	
	public void generateNotification(String eventType)
	{
		   String methodName ="generateNotification";
		      Boolean success = false;
		            try
		            {
		            	logger.logInfo(methodName+"|Start");
		            	
		            	
		            	List<InquiryView> inquiryViewList = new ArrayList<InquiryView>(0);
		            	List<InquiryView> inquiryViewSelectedList = new ArrayList<InquiryView>(0);
		            	List<Long> personIdList= new ArrayList<Long>(0);
		            	List<PersonView> personViewList = new ArrayList<PersonView>();
		            	PersonView personView = new PersonView();
		            	ContractView contractView = new ContractView();
		            	PropertyServiceAgent psa =  new PropertyServiceAgent();
		            	HashMap placeHolderMap = new HashMap();
		                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
		                NotificationFactory nsfactory = NotificationFactory.getInstance();
		                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
		                Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
		        		
		            	if (viewMap.containsKey("APPLICANT_VIEW_FOR_NOTIFICATION")
		            		&&viewMap.containsKey("Contract_VIEW_FOR_PAYMENT")) {
		            		personView = (PersonView)viewMap.get("APPLICANT_VIEW_FOR_NOTIFICATION");
		            		contractView = (ContractView)viewMap.get("Contract_VIEW_FOR_PAYMENT");
		            		getNotificationPlaceHolder(event,contractView,personView);
		            		personsEmailList = getEmailContactInfos((PersonView)personView);
	                        if(personsEmailList.size()>0)
	                              notifier.fireEvent(event, personsEmailList);
		        			
		        		}
		                  
		                  success  = true;
		                  logger.logInfo(methodName+"|Finish");
		            }
		            catch(Exception ex)
		            {
		                  logger.LogException(methodName+"|Finish", ex);
		            }
		          
	}
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	private void  getNotificationPlaceHolder(Event placeHolderMap,ContractView contractView,PersonView perView)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		placeHolderMap.setValue("PERSON_NAME",perView.getPersonFullName());
		placeHolderMap.setValue("CONTRACT_NUMBER",contractView.getContractNumber());
		
		logger.logDebug(methodName+"|Finish");
		
	}

	public String getSelectedTab() {
		return SelectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		SelectedTab = selectedTab;
	}

	public HtmlTabPanel getRichTabPanel() {
		return richTabPanel;
	}

	public void setRichTabPanel(HtmlTabPanel richTabPanel) {
		this.richTabPanel = richTabPanel;
	}

	public HtmlCommandButton getSaveButton() {
		return saveButton;
	}

	public void setSaveButton(HtmlCommandButton saveButton) {
		this.saveButton = saveButton;
	}
	
	public void btnEditPaymentSchedule_Click()
	{
		String methodName = "btnEditPaymentSchedule_Click";
		logger.logInfo(methodName+"|"+" Start...");
		PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
		if(viewMap.containsKey("PAYMENT_SCHEDULES"))
			sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewMap.get("PAYMENT_SCHEDULES"));
		sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
		openPopUp("", "javascript:openPopupEditPaymentSchedule()");


		logger.logInfo(methodName+"|"+" Finish...");
	}

	public Boolean getEditPaymentEnable() {
		if(viewMap.containsKey("EDIT_ENABLE"))
			editPaymentEnable = (Boolean)viewMap.get("EDIT_ENABLE");
		return editPaymentEnable;
	}

	public void setEditPaymentEnable(Boolean editPaymentEnable) {
		this.editPaymentEnable = editPaymentEnable;
		if(this.editPaymentEnable !=null)
			viewMap.put("EDIT_ENABLE",this.editPaymentEnable);
	}
	
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		RequestView reqView = null;
		ContractView contView = null;
		PersonView applicant = null;
		PersonView tenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		Long reqId = new Long(this.getRequestId());
		
		try
		{
		
		reqView = psAgent.getRequestById(reqId);	
		if(reqView != null)
		{
			contView = reqView.getContractView();
			if(contView != null)
			{
				tenant = contView.getTenantView();
				applicant = reqView.getApplicantView();
				
						
				
				eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
				eventAttributesValueMap.put("CONTRACT_START_DATE", getStringFromDate(contView.getStartDate()));
				eventAttributesValueMap.put("CONTRACT_END_DATE", getStringFromDate(contView.getEndDate()));				
				eventAttributesValueMap.put("PROPERTY_NAME", this.getTxtpropertyName().getValue().toString());
				eventAttributesValueMap.put("UNIT_NO", this.getTxtunitRefNum().getValue().toString());
				eventAttributesValueMap.put("TOTAL_VALUE", this.getTxtUnitRentValue().getValue().toString());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				
				if(notificationType.compareTo(WebConstants.Notification_MetaEvents.Event_CLEARANCELETTER_REQUESTAPPROVED) == 0)
				{
					List<PaymentScheduleView> payList =  getPaymentSchedules();
					Double fees = 0.0;
					for(PaymentScheduleView payView : payList)
					{
						fees += payView.getAmount();
					}
					
					eventAttributesValueMap.put("CL_FEES", fees.toString());
					
				}
				
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(tenant != null && applicant.getPersonId().compareTo(tenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(tenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", tenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}

				
			}
		}
		}
		catch (Exception e)
		{
			logger.LogException("requestStatusNotification crashed...",e);
		
		}
	}

	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()
    {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	
}
