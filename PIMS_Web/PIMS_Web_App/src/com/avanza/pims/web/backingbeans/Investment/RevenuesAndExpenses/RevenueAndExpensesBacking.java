package com.avanza.pims.web.backingbeans.Investment.RevenuesAndExpenses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.business.services.RevenuesAndExpensesServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ExpenseAndRevenueView;
import com.avanza.pims.ws.vo.OrderView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.ui.util.ResourceUtil;

public class RevenueAndExpensesBacking extends AbstractController 
{	
	private static final long serialVersionUID = 9197824186738487716L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private CommonUtil commonUtil;
	private String pageMode;
	private String viewMode;
	private String screenName;
	private Date tansactionDate;
	private final String noteOwner = WebConstants.Attachment.EXTERNAL_ID_REVENUE_AND_EXPENSE;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_REVENUE_AND_EXPENSE;
	private final String procedureTypeKey = WebConstants.PROCEDURE_REVENUE_AND_EXPENSE;
	private List<SelectItem> transtionNoList = new ArrayList<SelectItem>();
	
	private ExpenseAndRevenueView expenseAndRevenueView;
	private RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent;
	
		
	public RevenueAndExpensesBacking() 
	{
		
		logger = Logger.getLogger(RevenueAndExpensesBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);		
		commonUtil = new CommonUtil();
		pageMode = WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_MANAGE_PAGE_MODE_ADD;
		viewMode = new String();
		screenName = new String();
		expenseAndRevenueView = new ExpenseAndRevenueView();
		revenuesAndExpensesServiceAgent = new RevenuesAndExpensesServiceAgent();
		
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		
		try	
		{	
			if(!isPostBack())
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			
			updateValuesFromMaps();
			showHideAttachmentsAndCommentsBtn();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		
		// PAGE MODE          
		if ( sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY) != null )
		{
		 pageMode = (String) sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY);
		 sessionMap.remove( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY ) != null )
		{
		 pageMode = (String) viewMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY);
		}
		                     
		if ( sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY) != null )
		{
		 viewMode = (String) sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY);
		 sessionMap.remove( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY );			
		}
		else if ( viewMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY ) != null )
		{
		 viewMode = (String) viewMap.get( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY );
		}
		
		// REVENUE AND EXPENSE VIEW
		if ( sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW) != null )
		{
		 expenseAndRevenueView = (ExpenseAndRevenueView) sessionMap.get( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW);
		 sessionMap.remove(  WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW);
		 
		 CommonUtil.loadAttachmentsAndComments(expenseAndRevenueView.getExpRevId().toString());
		}
		else if ( viewMap.get( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW )  != null )
		{
		 expenseAndRevenueView = (ExpenseAndRevenueView) viewMap.get( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW ) ;
		}
		
		if(sessionMap.get(  WebConstants.Portfolio.PORTFOLIO_VIEW )!=null)
		{
			expenseAndRevenueView.setPortfolioView((PortfolioView) sessionMap.get(  WebConstants.Portfolio.PORTFOLIO_VIEW ));
			expenseAndRevenueView.setPortfolioId(expenseAndRevenueView.getPortfolioView().getPortfolioId());
			sessionMap.remove(  WebConstants.Portfolio.PORTFOLIO_VIEW );
			onPopulateTranstionNo();
		}
		else if(viewMap.get(  WebConstants.Portfolio.PORTFOLIO_VIEW )!=null)
		{
			expenseAndRevenueView.setPortfolioView((PortfolioView) viewMap.get(  WebConstants.Portfolio.PORTFOLIO_VIEW ));
			expenseAndRevenueView.setPortfolioId(expenseAndRevenueView.getPortfolioView().getPortfolioId());
			onPopulateTranstionNo();
		}
		
		if(sessionMap.containsKey("ADD_REVENUE"))
		{
			viewMap.put("ADD_REVENUE", sessionMap.get("ADD_REVENUE"));
			sessionMap.remove("ADD_REVENUE");
			screenName = ResourceUtil.getInstance().getProperty("revenueAndExpensesManage.heading.addRevenue");	
			DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_REVENUE);
			expenseAndRevenueView.setExpRevTypeId( String.valueOf( ddvTypeRevenue.getDomainDataId() ) );
		}
		else if(viewMap.containsKey("ADD_REVENUE"))
		{
			screenName = ResourceUtil.getInstance().getProperty("revenueAndExpensesManage.heading.addRevenue");	
			DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_REVENUE);
			expenseAndRevenueView.setExpRevTypeId( String.valueOf( ddvTypeRevenue.getDomainDataId() ) );
		}
		if(sessionMap.containsKey("ADD_EXPENSE"))
		{
			viewMap.put("ADD_EXPENSE", sessionMap.get("ADD_EXPENSE"));
			sessionMap.remove("ADD_EXPENSE");
			screenName = ResourceUtil.getInstance().getProperty("revenueAndExpensesManage.heading.addExpense");	
			DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_EXPENSE);
			expenseAndRevenueView.setExpRevTypeId( String.valueOf( ddvTypeRevenue.getDomainDataId() ) );
		}
		else if(viewMap.containsKey("ADD_EXPENSE"))
		{
			screenName = ResourceUtil.getInstance().getProperty("revenueAndExpensesManage.heading.addExpense");	
			DomainDataView ddvTypeRevenue = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.EXPENSE_AND_REVENUE_TYPE), WebConstants.EXPENSE_AND_REVENUE_TYPE_EXPENSE);
			expenseAndRevenueView.setExpRevTypeId( String.valueOf( ddvTypeRevenue.getDomainDataId() ) );
		}
			
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{                 
			viewMap.put( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_KEY, pageMode );
		}
		if ( viewMode != null )
		{                
			viewMap.put( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW_KEY, viewMode );
		}
		
		
		// REVENUE AND EXPENSE
		if ( expenseAndRevenueView != null )
		{
			viewMap.put( WebConstants.REVENUE_AND_EXPENSE.EXPENSE_AND_REVENUE_VIEW, expenseAndRevenueView);
		}
		
		if(expenseAndRevenueView.getPortfolioView()!=null)
		{
			viewMap.put( WebConstants.REVENUE_AND_EXPENSE.PORTFOLIO_VIEW, expenseAndRevenueView.getPortfolioView());
			expenseAndRevenueView.setPortfolioId(expenseAndRevenueView.getPortfolioView().getPortfolioId());
			onPopulateTranstionNo();
		}

	
	}
	
	@Override
	public void preprocess() 
	{		
		super.preprocess();
	}
	
	@Override
	public void prerender() 
	{		
		super.prerender();
	}
	
	public String onCancel() 
	{
		String path = null;		
		path = "searchRevenueAndExpense";
		return path;
	}
	public String onReset() 
	{
		String path = null;		
		path = "revenueAndExpenseManage";
		try {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
		response.sendRedirect("revenueAndExpensesManage.jsf");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String onSave() 
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		try	
		{ if(checkMissingField())
		  {
			    
				isSuccess = saveExpenseAndRevenueView(expenseAndRevenueView);
				
				
			    CommonUtil.loadAttachmentsAndComments(expenseAndRevenueView.getExpRevId()+ "");
				CommonUtil.saveAttachments(expenseAndRevenueView.getExpRevId());
				CommonUtil.saveComments(expenseAndRevenueView.getExpRevId(),noteOwner);
				
				
				
				
				
				if(viewMap.containsKey("UPDATE_DONE") && isSuccess)
	            { 
					pageMode = WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE;
	            	updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.recordUpdate") );
					viewMap.remove("UPDATE_DONE");
	            }
	            else if (isSuccess)
				{              
	            	pageMode = WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE; 
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.recordSave") );
				}
		 }    
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			errorMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.error") );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	
	private Boolean saveExpenseAndRevenueView( ExpenseAndRevenueView expenseAndRevenueView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		Boolean duplicate  = false;
		
		if ( expenseAndRevenueView.getExpRevId() == null )
		{
			
		
			
			expenseAndRevenueView.setCreatedBy( CommonUtil.getLoggedInUser() );
			expenseAndRevenueView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			
		    isSuccess = revenuesAndExpensesServiceAgent.addRevenueAndExpense(expenseAndRevenueView);
		}
		else
		{
			
			
			
			expenseAndRevenueView.setUpdatedBy( CommonUtil.getLoggedInUser() );			
			isSuccess = revenuesAndExpensesServiceAgent.updateRevenueAndExpense(expenseAndRevenueView);
			viewMap.put("UPDATE_DONE", true);
		}
		
		return isSuccess;
	}
	
	

	public boolean checkMissingField()
	{
		boolean check = true;
		
		
		if(expenseAndRevenueView.getStatusId()==null || expenseAndRevenueView.getStatusId().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.status") );
			check = false;
		}
		if(expenseAndRevenueView.getExpRevTypeId()==null || expenseAndRevenueView.getExpRevTypeId().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.type") );
			check = false;
		}
		if(expenseAndRevenueView.getTransectionDate()==null || expenseAndRevenueView.getTransectionDate().equals(""))
		{
			errorMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.trasactionDate") );
			check = false;
		}
		if(expenseAndRevenueView.getStrActualValue()!=null && expenseAndRevenueView.getStrActualValue().trim().length()>0)
		{
			try
			{
				expenseAndRevenueView.setActualValue(Double.parseDouble(expenseAndRevenueView.getStrActualValue()));
			}
			catch(Exception ex)
			{
				logger.logInfo("|"+"Exception while entering Actual amount in view::"+ex);
				errorMessages.add(CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.InvalidactualAmount"));
				check = false;
			}
		}
		else
		{
			errorMessages.add( CommonUtil.getBundleMessage("revenueAndExpensesManage.msg.actualAmount") );
			check = false;
		}
		
	
		return check;
	}
   
	public String onAddPortfolio()
	{
		String METHOD_NAME = "onAddPortfolio()";
		String path = null;
		Boolean isSuccess = false;
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		
		sessionMap.put(  WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY,   WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP  );
		
		openPopup("openPortfolioSearchPopup()");
		
		return path;
	}

	
	private void openPopup(String javaScriptText) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEditMode() 
	{ 
		Boolean isEditMode = false;
                                          		
		if ( pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode() 
	{
		Boolean isViewMode = false;
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW) )
			isViewMode = true;
		
		return isViewMode;
	}

	public String getIsViewReadOnly() 
	{
		String readOnly = "";
		                                 
		if ( viewMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsEditReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW) )
			readOnly = "READONLY";
		
		return readOnly;
	}
	public String getIsStatusReadOnly() 
	{
		String readOnly = "";
		
		if ( viewMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW)
			 || pageMode.equalsIgnoreCase(WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_MANAGE_PAGE_MODE_ADD))
			readOnly = "READONLY";
		else if(pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE))
			readOnly = "";
	
			
		return readOnly;
	}
	
	public void showHideAttachmentsAndCommentsBtn(){
		if ( pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_MANAGE_PAGE_MODE_ADD ) )
		    canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_POPUP_VIEW_ONLY) )
		    canAddAttachmentsAndComments(false);
	    if ( pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_UPDATE) )
	    	canAddAttachmentsAndComments(true);
	    if ( viewMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_VIEW) )
	    	canAddAttachmentsAndComments(false);
	}
	
	
	public Boolean getIsPopupViewOnlyMode() 
	{
		Boolean isPopupViewOnlyMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.REVENUE_AND_EXPENSE.REVENUE_AND_EXPENSE_PAGE_MODE_POPUP_VIEW_ONLY) )
			isPopupViewOnlyMode = true;
		
		return isPopupViewOnlyMode;
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(noteOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	private void canAddAttachmentsAndComments(boolean canAdd)
	{
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public void onPopulateTranstionNo() 
	{	
				
		boolean isLocaleEnglish = getIsEnglishLocale();
		OrderServiceAgent orderServiceAgent = new OrderServiceAgent();
		OrderView orderView = new OrderView();
		orderView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
		orderView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );		
		List<OrderView> orderViewList = new ArrayList<OrderView>(); 
		
		if(expenseAndRevenueView.getPortfolioView()!=null && expenseAndRevenueView.getPortfolioView().getPortfolioId()!=null)
		{	
			viewMap.remove("TRANSTION_NUMBER");
			transtionNoList.clear();
			try 
			{    
				orderView.setPortfolioView(expenseAndRevenueView.getPortfolioView());
				orderViewList = orderServiceAgent.getOrderList(orderView);
			}
			catch (Exception e) 
			{
				logger.LogException("getCurrencyList()", e);
			}
			
			if ( orderViewList != null )		
				for ( OrderView oV : orderViewList )			
					transtionNoList.add( new SelectItem(oV.getOrderId().toString(), oV.getTransactionRefNo() ) );
			
			Collections.sort(transtionNoList, ListComparator.LIST_COMPARE);
			viewMap.put("TRANSTION_NUMBER", transtionNoList);
		}	
		
		
	}
	
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public String getErrorMessages() 
	{		
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() 
	{		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}


	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}


	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}


	
	// Shiraz Code Start Here 
	
	public void onAddMember(){
	
	final String viewId = "/studyManage.jsf";

	FacesContext facesContext = FacesContext.getCurrentInstance();

	// This is the proper way to get the view's url
	ViewHandler viewHandler = facesContext.getApplication()
			.getViewHandler();
	String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	String javaScriptText = "javascript:SearchMemberPopup();";

	// Add the Javascript to the rendered page's header for immediate
	// execution
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,
			AddResource.HEADER_BEGIN, javaScriptText);
	}
	
	private List<ContactInfo> getEmailContactInfos(PersonView personView)
	{
		List<ContactInfo> emailList = new ArrayList<ContactInfo>();
		Iterator iter = personView.getContactInfoViewSet().iterator();
		HashMap previousEmailAddressMap = new HashMap();
		while(iter.hasNext())
		{
			ContactInfoView ciView = (ContactInfoView)iter.next();
			//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
			if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
					!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
	        {
				previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
				emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
				//emailList.add(ciView);
	        }
		}
		return emailList;
	}
	 private String getLoggedInUser() 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext()
					.getSession(true);
			String loggedInUser = "";

			if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
				UserDbImpl user = (UserDbImpl) session
						.getAttribute(WebConstants.USER_IN_SESSION);
				loggedInUser = user.getLoginId();
			}

			return loggedInUser;
		}


	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

	public ExpenseAndRevenueView getExpenseAndRevenueView() {
		return expenseAndRevenueView;
	}

	public void setExpenseAndRevenueView(ExpenseAndRevenueView expenseAndRevenueView) {
		this.expenseAndRevenueView = expenseAndRevenueView;
	}

	public RevenuesAndExpensesServiceAgent getRevenuesAndExpensesServiceAgent() {
		return revenuesAndExpensesServiceAgent;
	}

	public void setRevenuesAndExpensesServiceAgent(
			RevenuesAndExpensesServiceAgent revenuesAndExpensesServiceAgent) {
		this.revenuesAndExpensesServiceAgent = revenuesAndExpensesServiceAgent;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Date getTansactionDate() {
		return tansactionDate;
	}

	public void setTansactionDate(Date tansactionDate) {
		this.tansactionDate = tansactionDate;
	}

	public List<SelectItem> getTranstionNoList() {
		if(viewMap.containsKey("TRANSTION_NUMBER"))
			transtionNoList = (List<SelectItem>)viewMap.get("TRANSTION_NUMBER");
		return transtionNoList;
	}

	public void setTranstionNoList(List<SelectItem> transtionNoList) {
		this.transtionNoList = transtionNoList;
	}



	

}

