package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.AuctionUnit;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.property.PropertyTransformUtil;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;



public class UnitSearch extends AbstractController
{
	private transient Logger logger = Logger.getLogger(UnitSearch.class);
	
	private HtmlDataTable dataTable;
	
	private HtmlInputText htmlFloorNumber = new HtmlInputText();
	private HtmlInputText htmlUnitNumber = new HtmlInputText();
	private HtmlSelectOneMenu unitUsageSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitInvestmentSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitSideSelectMenu= new HtmlSelectOneMenu();
	private HtmlInputText htmlBedRooms = new HtmlInputText();
	private HtmlInputText htmlDewaNumberNumber = new HtmlInputText();
	private HtmlInputText htmlFromRentAmount = new HtmlInputText();
	private HtmlInputText htmlToRentAmount = new HtmlInputText();
	private HtmlInputText htmlPropertyName= new HtmlInputText();
	private HtmlInputText htmlPropertyEndowedName= new HtmlInputText();
	private HtmlInputText htmlPropertyNumber= new HtmlInputText();
	private HtmlInputText htmlContractNumber= new HtmlInputText();
	private HtmlInputText htmlEndowedMinor= new HtmlInputText();
	private HtmlSelectOneMenu unitStatusSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu unitEmirateSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu propertyOwnerShip = new HtmlSelectOneMenu();
	
	private HtmlInputText htmlunitDesc= new HtmlInputText();
	private HtmlInputText htmlunitCostCenter= new HtmlInputText();
	
    private boolean isArabicLocale;	
    private boolean isEnglishLocale;
	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String  VIEW_MODE="pageMode";
	private String  CONTEXT="context";
	private String  MODE_SELECT_ONE_POPUP="MODE_SELECT_ONE_POPUP";
	private String  MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP";
	private String  DEFAULT_SORT_FIELD = "unitNumber";
	//private Calendar occupiedTillDate = new Calendar();
	
	private Date inquiryDate = new Date();
	
	private String unitId;
	private String createdBy;
	private String createdOn;
		
	List<SelectItem> unitType= new ArrayList();
	List<SelectItem> unitUsage= new ArrayList();
	List<SelectItem> unitInvestment= new ArrayList();
	List<SelectItem> unitSide= new ArrayList();
	List<SelectItem> unitStatus= new ArrayList();
	
	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectBooleanCheckbox  checkBox=new HtmlSelectBooleanCheckbox();
	InquiryView inquiryView = new InquiryView();
	
	HashMap unitSearchMap = new HashMap ();
	
	UnitView unitView = new UnitView();
	String floorNumber1 = null;
	PropertyView propertyView = new PropertyView();
	ContactInfoView contactInfoView  = new ContactInfoView();
	
	

	List<SelectItem> city= new ArrayList();
	private List<SelectItem> state = new ArrayList<SelectItem>();
	List<SelectItem> country= new ArrayList();
	
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap;
 
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
    public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
     @Override 
	 public void init() 
     {    	
    	 super.init();
    	 viewRootMap = getFacesContext().getViewRoot().getAttributes();
    	 
    	 try
    	 {
    		 System.out.println("Testing");
             sessionMap=context.getExternalContext().getSessionMap();
       
              Map requestMap= context.getExternalContext().getRequestMap();
              HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();	
      	    
		     if(!isPostBack())
		     {
					setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
			        setSortField(DEFAULT_SORT_FIELD);
			        setSortItemListAscending(true);
		    	 
		    	 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    
	      	    	}
		    		
		    		if (request.getParameter(CONTEXT)!=null )
			    	 {
		    			viewRootMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
			    	 }
		          
		    	 }
		    	 loadCombos();
		    	 LoadUnitStatuses();
//		    	 if(getIsContextAuctionUnits() || getIsContextNewLeaseContract())
//		    	 {
//		    		 String unitStatusVacant = viewRootMap.get(WebConstants.UNIT_VACANT_STATUS).toString();
//		    		 unitStatusSelectMenu.setValue(unitStatusVacant);
//		    		 unitStatusSelectMenu.setReadonly(true);
//		    	 }
		     }
		    		 
    	 }
    	 catch(Exception es)
    	 {
    		 System.out.println(es);
    		 
    	 }
	        
	 }
     
     public List<SelectItem> getUnitStatusList()
     {
    	 List<SelectItem> allowedUnitStatuses = new ArrayList<SelectItem>(); 	 
    	 if(viewRootMap.containsKey("ALLOWED_UNIT_STATUSES"))
    		 allowedUnitStatuses = (List<SelectItem>) viewRootMap.get("ALLOWED_UNIT_STATUSES");
    	 return allowedUnitStatuses;
    		 
     }
     
     private void LoadUnitStatuses()
     {
    	 List<SelectItem> allowedUnitStatuses = new ArrayList<SelectItem>(); 
    	 boolean isEnglish  = getIsEnglishLocale();
    	 UtilityManager um = new UtilityManager();
    	 try {
			
    		 List<DomainData> domainDataList =  um.getDomainDataByTypeName(WebConstants.UNIT_STATUS);
    		 
    		 for(DomainData dd: domainDataList )
    		 {   			 
    			 boolean isAdd = true;
    			 
    			 if(getIsContextAuctionUnits() && ( !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS))  )
    				 isAdd = false;

    			 else if(getIsContextNewLeaseContract() && ( !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS))  )
    				 isAdd = false;    	
    			 
    			 else if(getIsContextInspectionUnits() && !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) || dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) )  )
    				 isAdd = false;
    			 
    			 else if(getIsContextMaintenanceRequest() && !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) )  )
    				 isAdd = false;
    			 
    			 if(isAdd)
    			 {
	    			 SelectItem item = null;
	    			 
	    			 if(isEnglish)
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescEn());
	    			 else
	    				 item = new SelectItem(dd.getDomainDataId().toString(),dd.getDataDescAr());
	    			 
	    			 allowedUnitStatuses.add(item);
    			 }    			 
    			 
    		 }
    		 
		} catch (PimsBusinessException e) {
			
			e.printStackTrace();
		}
    	 
		if(allowedUnitStatuses!= null && allowedUnitStatuses.size()>1)
			 Collections.sort(allowedUnitStatuses,ListComparator.LIST_COMPARE);
		
    	 viewRootMap.put("ALLOWED_UNIT_STATUSES",allowedUnitStatuses);
     }
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 		if(sessionMap.containsKey("UNIT_UPDATED_SUCCESSFULLY")){
			sessionMap.remove("UNIT_UPDATED_SUCCESSFULLY");
//			errorMessages = new ArrayList<String>();
//			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UNITS_UPDATED_SUCCESSFULLY));
			 loadDataList();
		}
 	}
     
     private void loadStates(){
     try {
			logger.logInfo("loadStates() started...");
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			if(sessionMap.containsKey("states")){
				state = (ArrayList<SelectItem>)sessionMap.get("states");
			}
			else{
				state = new ArrayList<SelectItem>();
				PropertyServiceAgent psa = new PropertyServiceAgent();
				String regionName = "";
				List<RegionView> statesList = psa.getRegionViewsByRegionType(WebConstants.REGION_TYPE_STATE);
				for(RegionView stateView:statesList)
				{
					SelectItem item;
					if (getIsEnglishLocale())
					{
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
					else 
						item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
					state.add(item);
				}
					sessionMap.put("states",state);
			}
				
//			loadCities(vce);
			logger.logInfo("loadStates() completed successfully!!!");
		}catch(Exception exception){
 		logger.LogException("loadStates() crashed ",exception);
		}
 	}
     
     private void loadCombos() throws PimsBusinessException
     {
    	 loadStates();
    	 /*if(!sessionMap.containsKey(WebConstants.UNIT_TYPE))
    	   loadUnitTypeList();
    	 if(!sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
      	   loadUnitSideTypeList();
    	 if(!sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
    	   loadUnitUsageList(); 
    	 if(!sessionMap.containsKey(WebConstants.UNIT_STATUS))
      	   loadUnitStatusList(); */
    	 
     }
	 
     private void loadUnitTypeList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_TYPE);
    	 sessionMap.put(WebConstants.UNIT_TYPE, ddl);
    	 
    	 
     }
     private void loadUnitSideTypeList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_SIDE_TYPE);
    	 sessionMap.put(WebConstants.UNIT_SIDE_TYPE, ddl);
    	 
     }
     private void  loadUnitUsageList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);
    	 sessionMap.put(WebConstants.PROPERTY_USAGE, ddl);
    	 
     }  
     
     private void  loadUnitStatusList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.UNIT_STATUS);
    	 sessionMap.put(WebConstants.UNIT_STATUS, ddl);
    	 
     }  
	 
		//We have to look for the list population for facilities
	private List<String> errorMessages;
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private UnitView dataItem = new UnitView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<UnitView> dataList = new ArrayList<UnitView>();
  
	//	public Unit dataItem = new Unit();
	
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
	public List<UnitView> getUnitDataList()
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<UnitView>)viewMap.get("unitList");
		if(dataList==null)
			dataList = new ArrayList<UnitView>();
		return dataList;
	}

	
	
	public String doBid() {
		return "";
		}
	
	
	public List<UnitView> getSelectedUnits(){
		logger.logInfo("getSelectedUnits started...");
		List<UnitView> selectedUnitList = new ArrayList<UnitView>(0);
    	try{
    		dataList = getUnitDataList();
    		if(dataList != null && dataList.size() > 0)
    		{
    			for(UnitView unitView:dataList )
    				if(unitView.getSelected())
    				{
    					selectedUnitList.add(unitView);
    				}    		
    		}
    		 		
    		logger.logInfo("getSelectedUnits completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedAuctionUnits() crashed ",exception);
    	}    	
    	return selectedUnitList;
    }
	
	public void doSearch (){
		try {
  	       
		     loadDataList();
		}
	    catch (Exception e){
		    System.out.println("Exception doSearch"+e);       
	    }
	}
    @SuppressWarnings("unchecked")
	public HashMap<String,Object> getSearchCriteria()
	{
		unitSearchMap = new HashMap<String, Object>();		
		
		String emptyValue = "-1";
		
		Object floorNumber = htmlFloorNumber.getValue();
		Object unitNumber = htmlUnitNumber.getValue();
		Object unitUsage = unitUsageSelectMenu.getValue();
		Object unitType = unitTypeSelectMenu.getValue();
		Object unitInvestment = unitInvestmentSelectMenu.getValue();
		Object unitSide = unitSideSelectMenu.getValue();
		Object bedRooms = htmlBedRooms.getValue();
		Object dewaNumber = htmlDewaNumberNumber.getValue();
		Object fromRentAmount = htmlFromRentAmount.getValue();
		Object toRentAmount = htmlToRentAmount.getValue();
		Object propertyName = htmlPropertyName.getValue();
		Object propertyEndowedName = htmlPropertyEndowedName.getValue();
		Object propertyNumber = htmlPropertyNumber.getValue();
		Object contractNumber = htmlContractNumber.getValue();
		Object propertyEndowedMinorNo = htmlEndowedMinor.getValue();
		Object unitStatus = unitStatusSelectMenu.getValue();
		Object unitEmirate = unitEmirateSelectMenu.getValue();
		Object unitDesc = htmlunitDesc.getValue();
		Object unitCostCenter = htmlunitCostCenter.getValue();
		Object ownerShip = propertyOwnerShip.getValue();
		
		if(ownerShip != null && !ownerShip.toString().trim().equals("") && !ownerShip.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.OWNERSHIPTYPE, ownerShip.toString());
		}
		if(unitCostCenter != null && !unitCostCenter.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.COSTCENTER, unitCostCenter.toString());
		}
		if(floorNumber != null && !floorNumber.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.FLOOR_NUMBER, floorNumber.toString());
		}
		if(unitNumber != null && !unitNumber.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_NUMBER, unitNumber.toString());
		}
		if(unitUsage != null && !unitUsage.toString().trim().equals("") && !unitUsage.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_USAGE, unitUsage.toString());
		}
		if(unitType != null && !unitType.toString().trim().equals("") && !unitType.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_TYPE, unitType.toString());		}
		
		if(unitInvestment != null && !unitInvestment.toString().trim().equals("") && !unitInvestment.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.INVESTMENT_TYPE, unitInvestment.toString());
		}
		if(unitSide != null && !unitSide.toString().trim().equals("") && !unitSide.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_SIDE, unitSide.toString() );
		}
		if(bedRooms != null && !bedRooms.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.BED_ROOMS, bedRooms.toString());
		}
		if(dewaNumber != null && !dewaNumber.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.DEWA_NUMBER, dewaNumber.toString());
		}		
		if(fromRentAmount != null && !fromRentAmount.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.FROM_RENT_AMOUNT, fromRentAmount.toString());
		}
		if(toRentAmount != null && !toRentAmount.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.TO_RENT_AMOUNT, toRentAmount.toString());
		}		
		if(propertyName != null && !propertyName.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.PROPERTY_NAME, propertyName.toString());
		}
		if(propertyNumber != null && !propertyNumber.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.PROPERTY_NUMBER, propertyNumber.toString());
		}
		if(contractNumber != null && !contractNumber.toString().trim().equals("")) {
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.CONTRACT_NUMBER, contractNumber.toString());
		}
		if(propertyEndowedName != null && !propertyEndowedName.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.ENDOWED_NAME, propertyEndowedName.toString());
		}
		if(propertyEndowedMinorNo != null && !propertyEndowedMinorNo.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.ENDOWED_NUMBER, propertyEndowedMinorNo.toString());
		}
		if(unitDesc != null && !unitDesc.toString().trim().equals(""))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_DESC, unitDesc.toString());
		}
		if(unitStatus != null && !unitStatus.toString().trim().equals("")  && !unitStatus.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_STATUS, unitStatus.toString());
		}
		else
		{
			ArrayList<Long> unitStatuses = new ArrayList<Long>();
			List<SelectItem> unitStatusList =  getUnitStatusList();
			for(SelectItem item :unitStatusList)
			{
				Object unitStat = item.getValue();
				
				if(unitStat != null && !unitStat.toString().trim().equals("")  && !unitStat.toString().trim().equals(emptyValue))
					unitStatuses.add(Long.parseLong(unitStat.toString()));					
				
			}
			
			if(unitStatuses.size() >0)
				unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_STATUS_IN,unitStatuses);
		}
		if(unitEmirate != null && !unitEmirate.toString().trim().equals("")  && !unitEmirate.toString().trim().equals(emptyValue))
		{
			unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.EMIRATE, unitEmirate.toString());
		}		
		
		if(getIsContextAuctionUnits())
		{
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALREADY_SELECTED_AUCTION_UNITS))
	        	{
				  List<AuctionUnitView> allSelectedUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALREADY_SELECTED_AUCTION_UNITS);
				  if(allSelectedUnitList != null && allSelectedUnitList.size()>0)
				  {
					  ArrayList<Long> unitIds = new ArrayList<Long>();
					  for(AuctionUnitView auv: allSelectedUnitList)
						  unitIds.add(auv.getUnitId());
						  
					  unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID_NOT_IN,unitIds);
				  }
	        	}
		}
		
		else if(getIsContextPropertyInquiryUnits())
		{
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALREADY_SELECTED_PROPERTY_INQUIRY_UNITS))
	        	{
				  List<UnitView> allSelectedUnitList = (List<UnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALREADY_SELECTED_PROPERTY_INQUIRY_UNITS);
				  if(allSelectedUnitList != null && allSelectedUnitList.size()>0)
				  {
					  ArrayList<Long> unitIds = new ArrayList<Long>();
					  for(UnitView uv: allSelectedUnitList)
						  unitIds.add(uv.getUnitId());
						  
					  unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID_NOT_IN,unitIds);
				  }
	        	}
		}
		else if(getIsContextInspectionUnits())
		{
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALREADY_SELECTED_PROPERTY_UNITS))
	        	{
				  List<InspectionUnitView> allSelectedUnitList = (List<InspectionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALREADY_SELECTED_PROPERTY_UNITS);
				  if(allSelectedUnitList != null && allSelectedUnitList.size()>0)
				  {
					  ArrayList<Long> unitIds = new ArrayList<Long>();
					  for(InspectionUnitView uv: allSelectedUnitList)
						  unitIds.add(uv.getUnitView().getUnitId());
						  
					  unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID_NOT_IN,unitIds);
				  }
	        	}
		}
		else 
		{
			if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALREADY_SELECTED_PROPERTY_UNITS))
	        	{
				  List<UnitView> allSelectedUnitList = (List<UnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALREADY_SELECTED_PROPERTY_UNITS);
				  if(allSelectedUnitList != null && allSelectedUnitList.size()>0)
				  {
					  ArrayList<Long> unitIds = new ArrayList<Long>();
					  for(UnitView uv: allSelectedUnitList)
						  unitIds.add(uv.getUnitId());
						  
					  unitSearchMap.put(WebConstants.UNIT_SEARCH_CRITERIA.UNIT_ID_NOT_IN,unitIds);
				  }
	        	}
		}
		
		return unitSearchMap;
	}
	public void doSearchItemList()
	{
		loadDataList();
	}
	public List<UnitView> loadDataList() 
	{
		 String methodName="loadDataList";
		 if(dataList != null)
			 dataList.clear();
		 
		 List<UnitView> list = new ArrayList();		    
		
		try
		{
			unitSearchMap = getSearchCriteria();
			PropertyService psa = new PropertyService();
			
			
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  psa.searchUnitGetTotalNumberOfRecords(unitSearchMap);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			
			
			list =  psa.getPropertyUnitsByCriteria(unitSearchMap,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
			
			if(list.isEmpty() || list==null)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));	
			}
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("unitList", list);
			recordSize = totalRows;
			viewMap.put("recordSize", totalRows);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
	    }
		catch (Exception ex) 
		{
	        ex.printStackTrace();
	    }
		return list;
		
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	
	public HtmlSelectOneMenu getUnitTypeSelectMenu() {
		return unitTypeSelectMenu;
	}


	public void setUnitTypeSelectMenu(HtmlSelectOneMenu unitTypeSelectMenu) {
		this.unitTypeSelectMenu = unitTypeSelectMenu;
	}

	public HtmlSelectOneMenu getUnitUsageSelectMenu() {
		return unitUsageSelectMenu;
	}



	public void setUnitUsageSelectMenu(HtmlSelectOneMenu unitUsageSelectMenu) {
		this.unitUsageSelectMenu = unitUsageSelectMenu;
	}



	public HtmlSelectOneMenu getUnitInvestmentSelectMenu() {
		return unitInvestmentSelectMenu;
	}



	public void setUnitInvestmentSelectMenu(
			HtmlSelectOneMenu unitInvestmentSelectMenu) {
		this.unitInvestmentSelectMenu = unitInvestmentSelectMenu;
	}



	public HtmlSelectOneMenu getUnitSideSelectMenu() {
		return unitSideSelectMenu;
	}



	public void setUnitSideSelectMenu(HtmlSelectOneMenu unitSideSelectMenu) {
		this.unitSideSelectMenu = unitSideSelectMenu;
	}

	public List<SelectItem> getUnitType() {
		unitType.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitType.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 
		 }
		
		
		return unitType;
	}



	public void setUnitType(List<SelectItem> unitType) {
		this.unitType = unitType;
	}



	public List<SelectItem> getUnitUsage() {
		unitUsage.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.PROPERTY_USAGE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.PROPERTY_USAGE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitUsage.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 
		 }



		return unitUsage;
	}
	


	public void setUnitUsage(List<SelectItem> unitUsage) {
		this.unitUsage = unitUsage;
	}



	public List<SelectItem> getUnitInvestment() {
		return unitInvestment;
	}



	public void setUnitInvestment(List<SelectItem> unitInvestment) {
		this.unitInvestment = unitInvestment;
	}



	public List<SelectItem> getUnitSide() {
		unitSide.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_SIDE_TYPE))
			 loadUnitTypeList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_SIDE_TYPE);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitSide.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 
		 }

		return unitSide;
	}
	
	public String sendToParent(String javaScript)
	{
		String methodName="sendInfoToParent";
        final String viewId = "/UnitSearch.jsf";
		//logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =javaScript;
        
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		//logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	public void sendUnitInfoToParent(javax.faces.event.ActionEvent event)
	{
	        UnitView unitViewRow=(UnitView)dataTable.getRowData();
	        String proceedingJS = "window.opener.document.forms[0].submit();" +
            					  "window.close();";
	        if( viewRootMap.get(CONTEXT ) != null )
	        {
	        	proceedingJS =	  "window.close();";
	        }
	        		
	        String javaScriptText ="window.opener.populateUnit(" +
			                                                     "'"+unitViewRow.getUnitId().toString()+"'," +
			                                                     "'"+unitViewRow.getUnitNumber().toString()+"'," +
			                                                     "'"+unitViewRow.getUsageTypeId().toString()+"',"+
			                                                     "'"+unitViewRow.getRentValue().toString()+"',"+
			                                                     "'"+(getIsEnglishLocale()?unitViewRow.getUsageTypeEn():unitViewRow.getUsageTypeAr())+"',"+
			                                                     "'"+unitViewRow.getPropertyCommercialName()+"',"+
			                                                     "'"+unitViewRow.getUnitAddress()+"',"+
			                                                     "'"+unitViewRow.getStatusId()+"',"+
			                                                     "'"+(getIsEnglishLocale()?unitViewRow.getStatusEn():unitViewRow.getStatusAr())+"'"
	                                                             +");"+
	        		              proceedingJS;  
	        	
	        Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	        sessionMap.put(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT, unitViewRow);
	        sendToParent(javaScriptText);
	        
	 }
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	public void sendManyUnitInfoToParent(javax.faces.event.ActionEvent event)
	{ 
	       
		  final String viewId = "/UnitSearch.jsp"; 
	      List<UnitView> selectedUnits = getSelectedUnits();
	      if(!selectedUnits.isEmpty())
	      {
	    	  	if(getIsContextAuctionUnits())
	    	  	{
	    	  		setResultsForAuctionUnits();
	    	  	}
	    	  	else if(getIsContextInspectionUnits())
	    	  	{
	    	  		setResultsForInspectionUnits();
	    	  	}
	    	  	else
	    	  	{	    	  		 
	    	  		sessionMap.put(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS, selectedUnits);
	    		     
	    	  	}	    	    
		        String javaScriptText = "javascript:closeWindowSubmit();";		
		        sendToParent(javaScriptText);
	      }
	      else
	      {
	        	errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.AuctionUnit.NO_UNIT_SELECTED));
	      }	      
	        
	    }
	
	@SuppressWarnings("unchecked")
	 private void setResultsForAuctionUnits()
	 {
		 List<AuctionUnitView> selectedUnitList = new ArrayList<AuctionUnitView>();
	     List<AuctionUnitView> allSelectedUnitList = new ArrayList<AuctionUnitView>();
	     List<UnitView> selectedUnits = getSelectedUnits();
	     
	     try
	     {
	    	 if(selectedUnits != null && selectedUnits.size()>0)
	    	 {
	    		 ArrayList<AuctionUnit> auctionUnits = new ArrayList<AuctionUnit>(0);
	    		 
	    		 for(UnitView uniView : selectedUnits )
	    		 {
	    			 AuctionUnit auctionUnit = new AuctionUnit();
	    			 Unit unit = new Unit();
	    			 unit.setUnitId(uniView.getUnitId());
	 				 auctionUnit.setUnit(unit);	 				 
	 				 auctionUnit.setDepositAmount(new Double(0));
	 				 auctionUnit.setIsDeleted(Constant.DEFAULT_IS_DELETED);
	 				 auctionUnit.setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
	 				 if(uniView.getAccountNumber()!=null)
	 					auctionUnit.getUnit().setAccountNumber(uniView.getAccountNumber());
	 				 auctionUnits.add(auctionUnit);
	 				 
	    		 }
			     
			     
			      UtilityManager utilManager = new UtilityManager();
				  Map ddMap = utilManager.getDomainDataMap();
				  Map regionMap = utilManager.getRegionMap();
				
				  selectedUnitList = PropertyTransformUtil
							.transformToAuctionUnitViews(auctionUnits, ddMap, regionMap);
				 
				 
				   UtilityServiceAgent usa = new UtilityServiceAgent();
					
					PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
					
					paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_AUCTION_DEPOSIT_ID);
					paymentConfigurationView.setProcedureTypeKey(WebConstants.PROCEDURE_TYPE_PREPARE_AUCTION);
					paymentConfigurationView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
					paymentConfigurationView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);			
					List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
					
					
					Double depositAmount = new Double(0);
					Double minAmount = new Double(0);
					Double maxAmount = Double.MAX_VALUE; 
					String basedOn = "";
					
					if(paymentConfigList != null && paymentConfigList.size() >0)
					{
						paymentConfigurationView = paymentConfigList.get(0);
						if(paymentConfigurationView.getIsFixed().longValue() == 1)
						{
							depositAmount = paymentConfigurationView.getAmount();
						}
						else
						{
							basedOn = paymentConfigurationView.getBasedOnTypeKey();
							minAmount = paymentConfigurationView.getMinAmount();
							maxAmount = paymentConfigurationView.getMaxAmount();
							depositAmount = paymentConfigurationView.getAmount();		
							
						}
					}
					
					for(AuctionUnitView auctionUnitView: selectedUnitList)
					{
						if(!basedOn.toString().equals(""))
						{
							if(basedOn.equals(WebConstants.BASED_ON_ANNUAL_RENT))
								depositAmount = (depositAmount / 100) * auctionUnitView.getRentValue(); 
							else if(basedOn.equals(WebConstants.BASED_ON_OPENING_AMOUNT))
							{
								
								if(auctionUnitView.getOpeningPrice() == null)
									 auctionUnitView.setOpeningPrice(auctionUnitView.getRentValue());
								
								depositAmount = (depositAmount / 100) * auctionUnitView.getOpeningPrice();
							}
							
							if(depositAmount < minAmount)
								depositAmount = minAmount;
							else if(depositAmount > maxAmount)
								depositAmount = maxAmount;
								
						}
						auctionUnitView.setDepositAmount(depositAmount);
					}
			 
	    	 }
	     }
	     catch(Exception exp)
	     {
	    	 exp.printStackTrace();
	     }
			
		 if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.ALL_SELECTED_AUCTION_UNITS))
	        	allSelectedUnitList = (List<AuctionUnitView>)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.ALL_SELECTED_AUCTION_UNITS);
		 
		 allSelectedUnitList.addAll(selectedUnitList);
	     getFacesContext().getExternalContext().getSessionMap().put(WebConstants.ALL_SELECTED_AUCTION_UNITS, allSelectedUnitList);
	 }
	
	@SuppressWarnings("unchecked")
	private void setResultsForInspectionUnits()
	{
		try
		{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List <UnitView> unitList = getSelectedUnits();
		Long[] unitIds = new Long[unitList.size()];
		int index = 0;
		for (UnitView unitView : unitList) 
		{
			unitIds[index++] = unitView.getUnitId();						
		}
		PropertyService ps = new PropertyService();
		List<InspectionUnitView> selectedUnits = ps.getAllUnitsForInspections(unitIds);
		sessionMap.put(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS, selectedUnits);
		}
		catch(Exception ex)
		{
			logger.LogException( "setResultsForInspectionUnits|started...",ex);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public boolean getIsContextPropertyInquiryUnits()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.PROPERTY_INQUIRY_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextAuctionUnits()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.AUCTION_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextNewLeaseContract()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.NEW_LEASE_CONTRACT))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextInspectionUnits()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.INSPECTION_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextMaintenanceRequest()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.MAINTENANCE_REQUEST))
		{
			return  true;
		}
		else
			return false;
		
	}	

	public boolean getIsPageModeSelectOnePopUp()
	{
		isPageModeSelectOnePopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_ONE_POPUP))
		{
			isPageModeSelectOnePopUp= true;
		}
		return isPageModeSelectOnePopUp;
		
	}
	public boolean getIsViewModePopUp()
	{
		boolean returnVal = false;
		
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null)
		{
			if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())		
				returnVal =  true;
		}
		 
		else 
		{
			returnVal =  false;
		}
		
		return returnVal;
		
	}
	public boolean getIsPageModeSelectManyPopUp()
	{
		isPageModeSelectManyPopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_MANY_POPUP))
		{
			isPageModeSelectManyPopUp= true;
		}
		return isPageModeSelectManyPopUp;
	}

	   public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
		public boolean getIsEnglishLocale()
		{
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		}

	public void setUnitSide(List<SelectItem> unitSide) {
		this.unitSide = unitSide;
	}




	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}



	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}



	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}



	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}



	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}



	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}



	public List<SelectItem> getCity() {
		return city;
	}



	public void setCity(List<SelectItem> city) {
		this.city = city;
	}



/*	public List<SelectItem> getState()  {
		state.clear();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		try {
		List<RegionView> regionViewList =  psa.getCountryStates("UAE");
		 
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
		      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
		      state.add(item);
		  }
		}catch (Exception e){}

		return state;
	}
*/
	public List<SelectItem> getState() {
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		state = (List<SelectItem>) sessionMap.get("states");
		if(state==null)
			state = new ArrayList<SelectItem>();
		return state;
	}
	
	public void setState(List<SelectItem> state) {
		this.state = state;
	}
	public List<SelectItem> getCountry() {
		return country;
	}
	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}
    public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public UnitView getDataItem() {
		return dataItem;
	}
	public void setDataItem(UnitView dataItem) {
		this.dataItem = dataItem;
	}
	public List<UnitView> getDataList() {
		return dataList;
	}
	public void setDataList(List<UnitView> dataList) {
		this.dataList = dataList;
	}
/*
	public Date getOccupiedTillDate() {
		return occupiedTillDate;
	}
	public void setOccupiedTillDate(Date occupiedTillDate) {
		this.occupiedTillDate = occupiedTillDate;
	}
	*/
	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;
	private boolean sortAscending = true;
	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");
		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}
		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters -----------------------------------------------------------------------------------
	
	public boolean getSortAscending() {
		return sortAscending;
	}
	// Setters -----------------------------------------------------------------------------------
	
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	public HtmlSelectBooleanCheckbox getCheckBox() {
		return checkBox;
	}
	public void setCheckBox(HtmlSelectBooleanCheckbox checkBox) {
		this.checkBox = checkBox;
	}	

	public HtmlSelectOneMenu getUnitStatusSelectMenu() {
		return unitStatusSelectMenu;
	}

	public void setUnitStatusSelectMenu(HtmlSelectOneMenu unitStatusSelectMenu) {
		this.unitStatusSelectMenu = unitStatusSelectMenu;
	}

	public List<SelectItem> getUnitStatus() {
		unitStatus.clear();
		 
		try
		 {
		 if(sessionMap.containsKey(WebConstants.UNIT_STATUS))
			 loadUnitStatusList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_STATUS);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitStatus.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 
		 }
		
		return unitStatus;
		
		
	}

	public void setUnitStatus(List<SelectItem> unitStatus) {
		this.unitStatus = unitStatus;
	}	

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getMODE_SELECT_ONE_POPUP() {
		return MODE_SELECT_ONE_POPUP;
	}

	public void setMODE_SELECT_ONE_POPUP(String mode_select_one_popup) {
		MODE_SELECT_ONE_POPUP = mode_select_one_popup;
	}

	public String getMODE_SELECT_MANY_POPUP() {
		return MODE_SELECT_MANY_POPUP;
	}

	public void setMODE_SELECT_MANY_POPUP(String mode_select_many_popup) {
		MODE_SELECT_MANY_POPUP = mode_select_many_popup;
	}

	///////////screen mode methods///////////
	
	@SuppressWarnings("unchecked")
	public String addUnit(){
		logger.logInfo("addUnit() started...");
		try {
			 Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			 sessionMap.remove(WebConstants.UNIT_VIEW);
			 
			logger.logInfo("addUnit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("addUnit() crashed ", exception);
			errorMessages.clear();
			//TODO error msg to user
		}
		return "unitDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String editUnit(){
		logger.logInfo("editUnit() started...");
		try {
			 Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			 String msg="editUnit";
			 logger.logInfo(msg+"started");
			 UnitView unitView=(UnitView)dataTable.getRowData();
				sessionMap.put("UNIT_POPUP_VIEW", unitView);


			 final String viewId = "/UnitSearch.jsf"; 

			 FacesContext facesContext = FacesContext.getCurrentInstance();

			 // This is the proper way to get the view's url
			 ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
			 String actionUrl = viewHandler.getActionURL(facesContext, viewId);

			 String javaScriptText = "javascript:GenerateUnitsPopup();";

			 // Add the Javascript to the rendered page's header for immediate execution
			 AddResource addResource = AddResourceFactory.getInstance(facesContext);
			 addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			 logger.logInfo(msg+"completed");
			logger.logInfo("editUnit() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("editUnit() crashed ", exception);
			errorMessages.clear();
			//TODO error msg to user
		}
		return "unitDetails";
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	public HtmlInputText getHtmlFloorNumber() {
		return htmlFloorNumber;
	}

	public void setHtmlFloorNumber(HtmlInputText htmlFloorNumber) {
		this.htmlFloorNumber = htmlFloorNumber;
	}

	public HtmlInputText getHtmlUnitNumber() {
		return htmlUnitNumber;
	}

	public void setHtmlUnitNumber(HtmlInputText htmlUnitNumber) {
		this.htmlUnitNumber = htmlUnitNumber;
	}

	public HtmlInputText getHtmlBedRooms() {
		return htmlBedRooms;
	}

	public void setHtmlBedRooms(HtmlInputText htmlBedRooms) {
		this.htmlBedRooms = htmlBedRooms;
	}

	public HtmlInputText getHtmlDewaNumberNumber() {
		return htmlDewaNumberNumber;
	}

	public void setHtmlDewaNumberNumber(HtmlInputText htmlDewaNumberNumber) {
		this.htmlDewaNumberNumber = htmlDewaNumberNumber;
	}

	public HtmlInputText getHtmlFromRentAmount() {
		return htmlFromRentAmount;
	}

	public void setHtmlFromRentAmount(HtmlInputText htmlFromRentAmount) {
		this.htmlFromRentAmount = htmlFromRentAmount;
	}

	public HtmlInputText getHtmlToRentAmount() {
		return htmlToRentAmount;
	}

	public void setHtmlToRentAmount(HtmlInputText htmlToRentAmount) {
		this.htmlToRentAmount = htmlToRentAmount;
	}

	public HtmlSelectOneMenu getUnitEmirateSelectMenu() {
		return unitEmirateSelectMenu;
	}

	public void setUnitEmirateSelectMenu(HtmlSelectOneMenu unitEmirateSelectMenu) {
		this.unitEmirateSelectMenu = unitEmirateSelectMenu;
	}

	public HtmlInputText getHtmlPropertyName() {
		return htmlPropertyName;
	}

	public void setHtmlPropertyName(HtmlInputText htmlPropertyName) {
		this.htmlPropertyName = htmlPropertyName;
	}
	public HtmlInputText getHtmlPropertyEndowedName() {
		return htmlPropertyEndowedName;
	}
	public void setHtmlPropertyEndowedName(HtmlInputText htmlPropertyEndowedName) {
		this.htmlPropertyEndowedName = htmlPropertyEndowedName;
	}
	public HtmlInputText getHtmlPropertyNumber() {
		return htmlPropertyNumber;
	}
	public void setHtmlPropertyNumber(HtmlInputText htmlPropertyNumber) {
		this.htmlPropertyNumber = htmlPropertyNumber;
	}
	public HtmlInputText getHtmlEndowedMinor() {
		return htmlEndowedMinor;
	}
	public void setHtmlEndowedMinor(HtmlInputText htmlEndowedMinor) {
		this.htmlEndowedMinor = htmlEndowedMinor;
	}
	public HtmlInputText getHtmlunitDesc() {
		return htmlunitDesc;
	}
	public void setHtmlunitDesc(HtmlInputText htmlunitDesc) {
		this.htmlunitDesc = htmlunitDesc;
	}
	public HtmlInputText getHtmlunitCostCenter() {
		return htmlunitCostCenter;
	}
	public void setHtmlunitCostCenter(HtmlInputText htmlunitCostCenter) {
		this.htmlunitCostCenter = htmlunitCostCenter;
	}
	public HtmlSelectOneMenu getPropertyOwnerShip() {
		return propertyOwnerShip;
	}
	public void setPropertyOwnerShip(HtmlSelectOneMenu propertyOwnerShip) {
		this.propertyOwnerShip = propertyOwnerShip;
	}
	public HtmlInputText getHtmlContractNumber() {
		return htmlContractNumber;
	}
	public void setHtmlContractNumber(HtmlInputText htmlContractNumber) {
		this.htmlContractNumber = htmlContractNumber;
	}

}
