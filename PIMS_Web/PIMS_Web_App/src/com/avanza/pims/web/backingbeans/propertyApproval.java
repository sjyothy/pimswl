/*package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;


import org.apache.myfaces.custom.fileupload.UploadedFile;


import com.avanza.pims.web.controller.AbstractController;

public class propertyApproval extends AbstractController{
private HtmlInputText propertyReferenceNumber = new HtmlInputText();
private HtmlInputText financialAccofuntNumber = new HtmlInputText();



private HtmlInputText departmentCode = new HtmlInputText();

private HtmlInputText numberOfUnits = new HtmlInputText();
private HtmlInputText projectNumber = new HtmlInputText();
private HtmlInputText numberOfFloors =new HtmlInputText();
private HtmlInputText landArea =new HtmlInputText();
private HtmlInputText builtInArea =new HtmlInputText();
private HtmlInputText coOrdinates = new HtmlInputText();
private HtmlInputText locationChart =new HtmlInputText();

private HtmlInputText propertyType = new HtmlInputText();
private HtmlInputText propertyCategory = new HtmlInputText();
private HtmlSelectOneMenu propertyUsageList = new HtmlSelectOneMenu();
private HtmlInputText propertyStatus= new HtmlInputText();
private HtmlInputText investmentPurpose = new HtmlInputText();


private List<SelectItem> propertyUsageItems = new ArrayList();

private HtmlInputTextarea inspectionComments =new HtmlInputTextarea();
private HtmlInputTextarea apporvalComments = new HtmlInputTextarea();

private List<Property> dataList = new ArrayList();
private List<Property> unitsDataList = new ArrayList();

private UploadedFile _upFile;
private HtmlDataTable dataTable;
private HtmlDataTable unitsDataTable;

private boolean   _floorTabVisible     = true;
private boolean   _unitsTabVisible     = true;


	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	public String upload() throws IOException
    {
        
        getFacesContext().getExternalContext().getApplicationMap().put("fileupload_bytes", _upFile.getBytes());
        getFacesContext().getExternalContext().getApplicationMap().put("fileupload_type", _upFile.getContentType());
        getFacesContext().getExternalContext().getApplicationMap().put("fileupload_name", _upFile.getName());
        return "ok";
    }

	
	public List<Property> getFloorList() {
		if (dataList == null) {
//			loadDataList(); // Preload by lazy loading.
			
		}
		return dataList;
	}
	
	public List<Property> getUnitsList() {
		if (unitsDataList == null) {
//			loadDataList(); // Preload by lazy loading.
			
		}
		return unitsDataList;
	}

	private void loadUnitsDataList(){
		
	}
	private void loadDataList() {

		Search query = new Search();

		query.clear();

		query.addFrom(PropertyDbImpl.class);

		List<Property> groupList = SecurityManager.findGroup(query);
		for(int i = 0;i<groupList.size();i++){

			dataList.add(groupList.get(i));
		}


	}
	public boolean is_floorTabVisible() {
		return _floorTabVisible;
	}

	public void set_floorTabVisible(boolean tabVisible) {
		_floorTabVisible = tabVisible;
	}

	public boolean is_unitsTabVisible() {
		return _unitsTabVisible;
	}

	public void set_unitsTabVisible(boolean tabVisible) {
		_unitsTabVisible = tabVisible;
	}

	public UploadedFile get_upFile() {
		return _upFile;
	}

	public void set_upFile(UploadedFile file) {
		_upFile = file;
	}

	public HtmlInputTextarea getApporvalComments() {
		return apporvalComments;
	}

	public void setApporvalComments(HtmlInputTextarea apporvalComments) {
		this.apporvalComments = apporvalComments;
	}

	public HtmlInputText getBuiltInArea() {
		return builtInArea;
	}

	public void setBuiltInArea(HtmlInputText builtInArea) {
		this.builtInArea = builtInArea;
	}

	
	public HtmlInputText getCoOrdinates() {
		return coOrdinates;
	}

	public void setCoOrdinates(HtmlInputText coOrdinates) {
		this.coOrdinates = coOrdinates;
	}

	public HtmlInputText getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(HtmlInputText departmentCode) {
		this.departmentCode = departmentCode;
	}



	public HtmlInputText getFinancialAccofuntNumber() {
		return financialAccofuntNumber;
	}

	public void setFinancialAccofuntNumber(HtmlInputText financialAccofuntNumber) {
		this.financialAccofuntNumber = financialAccofuntNumber;
	}

	public HtmlInputTextarea getInspectionComments() {
		return inspectionComments;
	}

	public void setInspectionComments(HtmlInputTextarea inspectionComments) {
		this.inspectionComments = inspectionComments;
	}

	

	

	public HtmlInputText getLandArea() {
		return landArea;
	}

	public void setLandArea(HtmlInputText landArea) {
		this.landArea = landArea;
	}

	

	public HtmlInputText getLocationChart() {
		return locationChart;
	}

	public void setLocationChart(HtmlInputText locationChart) {
		this.locationChart = locationChart;
	}

	

	public HtmlInputText getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(HtmlInputText numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	
	public HtmlInputText getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(HtmlInputText numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	

	public HtmlInputText getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(HtmlInputText projectNumber) {
		this.projectNumber = projectNumber;
	}

	
	public HtmlInputText getPropertyReferenceNumber() {
		return propertyReferenceNumber;
	}

	public void setPropertyReferenceNumber(HtmlInputText propertyReferenceNumber) {
		this.propertyReferenceNumber = propertyReferenceNumber;
	}

	

	public HtmlInputText getInvestmentPurpose() {
		return investmentPurpose;
	}

	public void setInvestmentPurpose(HtmlInputText investmentPurpose) {
		this.investmentPurpose = investmentPurpose;
	}

	public HtmlInputText getPropertyCategory() {
		return propertyCategory;
	}

	public void setPropertyCategory(HtmlInputText propertyCategory) {
		this.propertyCategory = propertyCategory;
	}

	public HtmlInputText getPropertyStatus() {
		return propertyStatus;
	}

	public void setPropertyStatus(HtmlInputText propertyStatus) {
		this.propertyStatus = propertyStatus;
	}

	public HtmlInputText getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(HtmlInputText propertyType) {
		this.propertyType = propertyType;
	}

	public List<Property> getUnitsDataList() {
		return unitsDataList;
	}

	public void setUnitsDataList(List<Property> unitsDataList) {
		this.unitsDataList = unitsDataList;
	}

	public List<SelectItem> getPropertyUsageItems() {
		return propertyUsageItems;
	}

	public void setPropertyUsageItems(List<SelectItem> propertyUsageItems) {
		this.propertyUsageItems = propertyUsageItems;
	}

	public HtmlSelectOneMenu getPropertyUsageList() {
		return propertyUsageList;
	}

	public void setPropertyUsageList(HtmlSelectOneMenu propertyUsageList) {
		this.propertyUsageList = propertyUsageList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlDataTable getUnitsDataTable() {
		return unitsDataTable;
	}

	public void setUnitsDataTable(HtmlDataTable unitsDataTable) {
		this.unitsDataTable = unitsDataTable;
	}
	
	public String approved(){


		return "approved";
		
	}
	
}
*/