package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxCommandButton;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectManyCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;


import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.BlackListView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InspectionViolationActionView;
import com.avanza.pims.ws.vo.InspectionViolationView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ViolationView;

public class ViolationActionBackingBean extends AbstractController {

	private InspectionViolationView inspectionViolationView;
	private List<ViolationView> violationView;
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private ResourceBundle bundle;
	//Variables for fields
	private String unitRefNo;
	private String unitType;
	private String contractRefNo;
	private String propertyName;
	private String description;
	private Long violationId;
	public boolean tenantBlakListed;
	public boolean hasTenant;
	private String unitStatus;
	private String violationDate;
	Map sessionMap =FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
	public HtmlSelectManyCheckbox selectManyActions;
	
	private ViolationView violationViewForActions;
	
	public String reason;
	
	private String tenentName;
	
	private String violationStatus;
	
	private String violationType;
	private String violationCategory;
	
	private double damageAmount = 0;
	
	private Long inspectionId;
	private Long unitId;
	
	private double fine;
	
	private ContractView contractView;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	private List<String> errorMessages;
	
	private List<String> selectedActions;
	
	
	HashMap selectedUsersMap =new HashMap();
	
	List<SelectItem> actionList=new ArrayList();
	
	private List<String> actionListValues;
	
	List<SelectItem> alreadySelectedList = new ArrayList();
	List alreadySelectedListValues = new ArrayList();
	
	HtmlAjaxCommandButton cmdAddTeamMembers=new HtmlAjaxCommandButton();
	HtmlAjaxCommandButton cmdRemoveTeamMembers=new HtmlAjaxCommandButton();
	
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
	.getAttributes();
	
	
	private static Logger logger = Logger.getLogger(ViolationActionBackingBean.class);
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();	
		
		String methodName="init";
    	logger.logInfo(methodName+"|"+" Start");
    	
    	getIsEnglishLocale();
		getIsArabicLocale();
    	
    	FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request=  (HttpServletRequest) context.getExternalContext().getRequest();
		
    	//For blackList (Reson popup)//From query String
		if (request.getParameter("unitId") != null){
			
			this.setUnitId( new Long (request.getParameter("unitId")));
			this.setInspectionId( new Long (request.getParameter("inspectionId")) );
			this.setViolationId( new Long(request.getParameter("violationId"))) ;
			this.setViolationViewForActions( (ViolationView)getRequestParam("violationView")) ;
		}
    	
		if (!isPostBack()){
			
			this.setViolationViewForActions( (ViolationView)  getFacesContext().getExternalContext().getSessionMap().get(
					"INSPECTION_VIOLATION_VIEW_FOR_VA") );
			
			this.setUnitId(  getViolationViewForActions().getUnitId() ) ;
			this.setInspectionId(  getViolationViewForActions().getInspectionId());
			this.setViolationId(getViolationViewForActions().getViolationId());
			violationDate = getViolationViewForActions().getViolationDateString();
			unitType = getViolationViewForActions().getCategoryEn();
			unitRefNo = getViolationViewForActions().getUnitNumber();
			contractRefNo = getViolationViewForActions().getContractNumber();
//			damageAmount = getViolationViewForActions().getDamageAmount();
			
			
			if(isEnglishLocale)
				violationType = getViolationViewForActions().getTypeEn();
			else if (isArabicLocale)
				violationType = getViolationViewForActions().getTypeAr();
			else
				violationType = getViolationViewForActions().getTypeEn();
			
			if(isEnglishLocale)
				violationCategory = getViolationViewForActions().getCategoryEn();
			else if (isArabicLocale)
				violationCategory = getViolationViewForActions().getCategoryAr();
			else
				violationCategory = getViolationViewForActions().getCategoryEn();
			
			if(isEnglishLocale)
				violationStatus = getViolationViewForActions().getStatusEn();
			else if (isArabicLocale)
				violationStatus = getViolationViewForActions().getStatusAr();
			else
				violationStatus = getViolationViewForActions().getStatusEn();
			
			getInspectionViolation();
			loadActions();
			
			getAlreadyAsignedActionsAndFine();
			
			
		}
		else{
			
			getViolationId();
//			getAlreadySelectedList();
			getViolationViewForActions();
			if( getViolationViewForActions()!= null){
				
				this.setUnitId(  getViolationViewForActions().getUnitId() ) ;
				this.setInspectionId(  getViolationViewForActions().getInspectionId());
				this.setViolationId(getViolationViewForActions().getViolationId());
				violationDate = getViolationViewForActions().getViolationDateString();
				unitType = getViolationViewForActions().getCategoryEn();
				unitRefNo = getViolationViewForActions().getUnitNumber();
				contractRefNo = getViolationViewForActions().getContractNumber();
				
				
				if(isEnglishLocale)
					violationType = getViolationViewForActions().getTypeEn();
				else if (isArabicLocale)
					violationType = getViolationViewForActions().getTypeAr();
				else
					violationType = getViolationViewForActions().getTypeEn();
				
				if(isEnglishLocale)
					violationCategory = getViolationViewForActions().getCategoryEn();
				else if (isArabicLocale)
					violationCategory = getViolationViewForActions().getCategoryAr();
				else
					violationCategory = getViolationViewForActions().getCategoryEn();
				
				if(isEnglishLocale)
					violationStatus = getViolationViewForActions().getStatusEn();
				else if (isArabicLocale)
					violationStatus = getViolationViewForActions().getStatusAr();
				else
					violationStatus = getViolationViewForActions().getStatusEn();
				
//				damageAmount = getViolationViewForActions().getDamageAmount();
				
			}
		}
		
		
		
		if (tenentName == null || tenentName == "" || tenentName.length() <= 0)
			hasTenant = false;
		else 
			hasTenant = true;
		
		//getTenantBlackListed();
		logger.logInfo(methodName+"|"+" Finish");
		
	}
	
	public void getAlreadyAsignedActionsAndFine(){
		
		String methodName="getAlreadyAsignedActionsAndFine";
    	logger.logInfo(methodName+"|"+" Start");
		
		PaymentScheduleView alreadyFine = null;
		try {
			
			selectedActions = new ArrayList();
			
			// Diabled due to no fine from now on
			//alreadyFine = psa.getFinePaymentScheduleByViolationId(violationId);
			
			List<InspectionViolationActionView> alreadyActions = psa.getAllViolationActionByViolationId(this.getViolationId());
			SelectItem selectItem = null;
			if (alreadyFine != null)
				fine = alreadyFine.getAmount();
			
			if (alreadyActions!=null && alreadyActions.size() > 0)
				{
					for (int i=0; i<alreadyActions.size(); i++)
					{
						alreadyActions.get(i).getActionId().toString();
						
						selectedActions.add(alreadyActions.get(i).getActionId().toString());
						selectItem = new SelectItem();
						if(isEnglishLocale)
							selectItem.setLabel(alreadyActions.get(i).getActionEn().toString());
						else if(isArabicLocale)
							selectItem.setLabel(alreadyActions.get(i).getActionAr().toString());
						else
							selectItem.setLabel(alreadyActions.get(i).getActionEn().toString());
					
						selectItem.setValue(alreadyActions.get(i).getActionId().toString());
						
						alreadySelectedList.add(selectItem);
//						alreadySelectedListValues.add(alreadyActions.get(i).getActionId().toString());
						
					}
					
					if(alreadySelectedList != null && alreadySelectedList.size() >0 )
					{
						this.setAlreadySelectedList(alreadySelectedList);
					}
				}
			
			logger.logInfo(methodName+"|"+" Finish");
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
		
		
	}
	
	public void getInspectionViolation(){
		
		String methodName="getInspectionViolation";
    	logger.logInfo(methodName+"|"+" Start");
		
		try {
			
			inspectionViolationView = psa.getViolationsByUnitId(getUnitId(),getInspectionId(),getDateFormat());
			getContractView();
//			inspectionViolationView.setContractNumber(contractView.getContractNumber());
			//violationView =  inspectionViolationView.getViolations();
			unitRefNo = inspectionViolationView.getUnitRefNumber();
			 
			
			//PaymentScheduleView damgePaymentSchedule = psa.getDamageAmountPaymentScheduleByViolationID(violationId);
			
//			if (damgePaymentSchedule != null && damgePaymentSchedule.getAmount() != null)
//			{
//				
//				damageAmount = damgePaymentSchedule.getAmount();
//			}
			
			if (contractView != null && contractView.getTenantView() != null)
			{
				tenentName = contractView.getTenantView().getFirstName();
				contractRefNo = contractView.getContractNumber();
			}
			
			//contractRefNo = inspectionViolationView.getContractNumber();
			
			if (isArabicLocale)
			{
				unitType = inspectionViolationView.getUnitTypeAr();
				unitStatus = inspectionViolationView.getUnitStatusAr();
			}
			else
			{
				unitType = inspectionViolationView.getUnitTypeEn();
				unitStatus = inspectionViolationView.getUnitStatusEn();
			}
			
			propertyName = inspectionViolationView.getPropertyName();
			
			logger.logInfo(methodName+"|"+" Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
	}
	
	private void getContractView(){
		
		String methodName="getContractView";
    	logger.logInfo(methodName+"|"+" Start");
		
		try {
			
			contractView = psa.getContractNumberByUnitId(getUnitId(), getInspectionId(), getDateFormat());
			
			logger.logInfo(methodName+"|"+" Finish");
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.logError(methodName+"|"+"Error Occured:"+e);
		}
		
	}
	
	public ResourceBundle getBundle() {
		
		String methodName="getBundle";
    	logger.logInfo(methodName+"|"+" Start");

        if(getIsArabicLocale())

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");

        else

              bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");

        logger.logInfo(methodName+"|"+" Finish");
        
        return bundle;

  }
	
	private void alreadyBlackListedError(){
		
		String methodName="alreadyBlackListedError";
    	logger.logInfo(methodName+"|"+" Start");
    	
		errorMessages = new ArrayList<String>();
    	errorMessages.clear();
		
    	errorMessages.add(getBundle().getString(MessageConstants.ViolationActions.MSG_ALREADY_BLACKLISTED));
    	
    	 logger.logInfo(methodName+"|"+" Finish");
          
	}

	private boolean hasErrors()
	{
		
		String methodName="hasErrors";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean isError=false;
		
		errorMessages = new ArrayList<String>();
    	errorMessages.clear();
    	
	    	if (tenantBlakListed == true){
	    	
	    		alreadyBlackListedError();
	    		
	    	}
	    	
			if(selectedActions==null || selectedActions.size()<=0 )
			{
				errorMessages.add(getBundle().getString(MessageConstants.ViolationActions.MSG_ACTION));
				isError = true;
			}
		
//			if (fine > 0)
//			{
//				try{
//					
//					double d = fine;
//					
//				}
//				catch(Exception ex)
//				{
//					errorMessages.add(getBundle().getString(MessageConstants.ViolationActions.MSG_FINE_FORMAT));
//					isError = true;
//				}
//				
//			if (!hasFineActionSelected())
//			{
//				errorMessages.add(getBundle().getString(MessageConstants.ViolationActions.MSG_FINE_ACTION));
//				isError = true;
//			}
//			
//		}
		
//			if (hasFineActionSelected() && fine <= 0){
//				errorMessages.add(getBundle().getString(MessageConstants.ViolationActions.MSG_ENTER_FINE_AMOUNT));
//				isError = true;
//			}
		
    	logger.logInfo(methodName+"|"+" Finish");
		
		
		return isError;
	}
	
	private boolean hasFineActionSelected(){
		
		String methodName="hasFineActionSelected";
    	logger.logInfo(methodName+"|"+" Start");
		
		boolean fineActionSelected = false;
		
		DomainDataView fineDomainData = null;
		
		try {
			
			fineDomainData = psa.getDomainDataByValue(WebConstants.VIOLATION_ACTION, WebConstants.VIOLATION_ACTION_FINANCIAL_FINE);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (fineDomainData != null)
		{
			for (int i=0; i<selectedActions.size(); i++)
			{
				if (fineDomainData.getDomainDataId().toString().equals(selectedActions.get(i)))
					fineActionSelected = true;
			}
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return fineActionSelected;
	}

	public String save(){
		
		String methodName="save";
    	logger.logInfo(methodName+"|"+" Start");
    	
		String returnString = "";
		
		try {
			
			if (!hasErrors())
			{
			
				List<InspectionViolationActionView> actionList = new ArrayList();
				
				for (int i=0; i<selectedActions.size(); i++)
				{
				
					InspectionViolationActionView inspectionViolationActionView = new InspectionViolationActionView();
					
					inspectionViolationActionView.setActionDate(new Date());
					inspectionViolationActionView.setActionId(Long.parseLong(selectedActions.get(i)));
					inspectionViolationActionView.setCreatedBy(getLoggedInUser());
					inspectionViolationActionView.setCreatedOn(new Date());
					inspectionViolationActionView.setInspectionViolationId(getViolationId());
					inspectionViolationActionView.setIsDeleted(0L);
					inspectionViolationActionView.setRecordStatus(1L);
					inspectionViolationActionView.setUpdatedBy(getLoggedInUser());
					inspectionViolationActionView.setUpdatedOn(new Date());
					
					actionList.add(inspectionViolationActionView);	
					
				}
				
				psa.addViolationAction(actionList);
				
				//if (fine > 0)
				//{
				
				PaymentScheduleView psv = new PaymentScheduleView();
				
				DomainDataView domainDataViewCash;
				DomainDataView domainDataViewFine;
				
					
					domainDataViewCash = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_MODE, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
					domainDataViewFine = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_TYPE, WebConstants.PAYMENT_SCHEDULE_TYPE_FINE);
					DomainDataView domainDataPaymentStatusPending = psa.getDomainDataByValue(WebConstants.PAYMENT_SCHEDULE_STATUS, WebConstants.PAYMENT_SCHEDULE_PENDING);
					
					getInspectionViolation();
					
					psv.setStatusId(domainDataPaymentStatusPending.getDomainDataId());
					psv.setAmount(fine);
			    	psv.setPaymentModeId(domainDataViewCash.getDomainDataId());
			    	psv.setTypeId(domainDataViewFine.getDomainDataId());
			    	
			    	if (contractView != null)
			    		psv.setContractId(contractView.getContractId());
			    	
			    	psv.setViolationId(getViolationId());
			    	
			    	psv.setIsReceived("N");
			    	psv.setUpdatedBy(getLoggedInUser());
			    	psv.setUpdatedOn(new Date());
			    	psv.setCreatedBy(getLoggedInUser());
			    	psv.setCreatedOn(new Date());
			    	psv.setRecordStatus(1L);
			    	psv.setIsDeleted(0L);
			    	
			    	
			    	psa.addPaymentScheduleForViolationFine(psv);
			    	
				//}
				
				
				psa.setViolationAsActionsDefined(getViolationId());
				
				 setRequestParam("unitId",getUnitId());
				 setRequestParam("inspectionId",getInspectionId());
				 setRequestParam("violationId",getViolationId());
				
				returnString = "violationActionsAdded";
			}
		
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(methodName+"|"+"Error Occured:"+e);
			e.printStackTrace();
			
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return returnString;
	}
	
	public String cancel(){
		
		 setRequestParam("unitId",getUnitId());
		 setRequestParam("inspectionId",getInspectionId());
		 setRequestParam("violationId",getViolationId());
		
		return "cancel";
	}
	
	public void getTenantBlackListed(){
		
		
		String methodName="getTenantBlackListed";
    	logger.logInfo(methodName+"|"+" Start");
		
		getContractView();
		
		if (contractView != null && contractView.getTenantView() != null)
		{
		
//			Long tenantId = contractView.getTenantView().getTenantId();
//			
//			try {
//				
//				if (tenantId != null)
//				{
//					if (psa.isTenantBlackListed(tenantId))
//					{
//						tenantBlakListed = true;
//						alreadyBlackListedError();
//					}
//				}
//				
//				
//			} catch (PimsBusinessException e) {
//				// TODO Auto-generated catch block
//				logger.logError(methodName+"|"+"Error Occured:"+e);
//				e.printStackTrace();
//			}
		
		}
		
		logger.logInfo(methodName+"|"+" Finish");
	}
	
	public String blackList(){
		
		String methodName="blackList";
    	logger.logInfo(methodName+"|"+" Start");
		
//		try {
//			
//			getContractView();
//			
//			if (contractView != null)
//			{
				
//					Long tenantId = contractView.getTenantView().getTenantId();
//				
//				if (!psa.isTenantBlackListed(tenantId))
//				{
//					BlackListView blackListView = new BlackListView();
//					
//					blackListView.setTenantId(tenantId);
//					blackListView.setCreatedBy(getLoggedInUser());
//					blackListView.setCreatedOn(new Date());
//					blackListView.setIsDeleted(0L);
//					blackListView.setLastAddedBy(getLoggedInUser());
//					blackListView.setRecordStatus(1L);
//					blackListView.setUpdatedBy(getLoggedInUser());
//					blackListView.setUpdatedOn(new Date());
//					blackListView.setLastReason(reason);
//				
//					psa.addBlackList(blackListView);
//					
//				}
//                				
//				setRequestParam("unitId", unitId);
//				   //sessionMap.put("unitId", unitId);
//				   //FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("unitId", unitId);
//		    	setRequestParam("inspectionId", inspectionId);
//		    	setRequestParam("violationId", violationId);
//		    	setRequestParam("violationDate", violationDate);
//		    	refreshParent();
//			}
//		} catch (PimsBusinessException e) {
//			// TODO Auto-generated catch block
//			logger.logError(methodName+"|"+"Error Occured:"+e);
//			e.printStackTrace();
//		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return "refresh";
	}
	public String refreshParent()
	{
		String methodName="openPopUp";
        final String viewId = "/Reason.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =//"window.opener.document.getElementById("violationActionForm:reasonHidden").value = document.getElementById("reasonForm:reasonTxt").value;"
  	                          "window.opener.document.forms[0].submit(); " +
  	                          " window.close();";
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}



	public String getUnitRefNo() {
		return unitRefNo;
	}

	public void setUnitRefNo(String unitRefNo) {
		this.unitRefNo = unitRefNo;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);

		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);

		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");

		return isEnglishLocale;
	}

	
	
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	
	public String getDateFormat()
	{
		String methodName="getDateFormat";
    	logger.logInfo(methodName+"|"+" Start");
		
		SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.SHORT_DATE_FORMAT);
		
		logger.logInfo(methodName+"|"+" Finish");
		return dateFormat;
	}


	public List<ViolationView> getViolationView() {
		return violationView;
	}

	public void setViolationView(List<ViolationView> violationView) {
		this.violationView = violationView;
	}


	public String getErrorMessages() 
	{
		String methodName="getErrorMessages";
    	logger.logInfo(methodName+"|"+" Start");
		
		String messageList="";
		if ((errorMessages == null) || (errorMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for (String message : errorMessages) 
				{
					messageList += "<LI>"+ message+ " <br></br>" ;
			    }
			messageList = messageList + "</UL></B></FONT>\n";
		}
		
		logger.logInfo(methodName+"|"+" Finish");
		return	messageList;
		
	}

	public String getTenentName() {
		return tenentName;
	}

	public void setTenentName(String tenentName) {
		this.tenentName = tenentName;
	}

	public List<String> getSelectedActions() {
		return selectedActions;
	}

	public void setSelectedActions(List<String> selectedActions) {
		this.selectedActions = selectedActions;
	}

	private void loadActions()
	{
	
		String METHOD_NAME = "loadActions()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		// TODO Auto-generated method stub	
		try{
		
		List<DomainDataView> domainDataListStatus =getActions();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.VIOLATION_ACTION, domainDataListStatus);
		
		
		}catch(Exception e){
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
	}
	
	public List<DomainDataView> getActions() {
		
		String METHOD_NAME = "getActions()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");
		
		List<DomainDataView> domainDataList =null;
		try {
			
			domainDataList = new ArrayList<DomainDataView>();
			domainDataList = psa.getDomainDataByDomainTypeName(WebConstants.VIOLATION_ACTION);
			
		//	domainDataList.get(0);
			
		} catch (PimsBusinessException e) {
			// TODO Auto-generated catch block
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
		}
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		 return domainDataList;
	}
	
	public List<SelectItem> getActionList() {
		
		String METHOD_NAME = "getActionList()";
		logger.logInfo("Stepped into the "+METHOD_NAME+" method");

		
		try {
				actionList.clear();
                List<DomainDataView> actionComboList=(ArrayList<DomainDataView>) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebConstants.VIOLATION_ACTION);
                
                for(int i=0; i< actionComboList.size();i++)
                {
                	DomainDataView ddv=(DomainDataView)actionComboList.get(i);
                	
	                	SelectItem item=null;
	                	if (isEnglishLocale)
	                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
	                	else if (isArabicLocale)
	                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
	                	else
	                		item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());

	                	actionList.add(item);
                	
                }
               
			}catch(Exception e){
				
			logger.logError(METHOD_NAME+" crashed due to:"+e.getStackTrace());
			
			}
			
		logger.logInfo(METHOD_NAME+" method completed Succesfully");
		
		if (viewRootMap.containsKey("actionList")
				&& viewRootMap.get("actionList") != null)
			actionList =  (List<SelectItem>) viewRootMap.get("actionList");
		 
		return actionList;
	}

	public void setActionList(List<SelectItem> actionList) {
		this.actionList = actionList;
		
		if (this.actionList != null )
			viewRootMap.put("actionList", this.actionList);
	}

	public double getFine() {
		return fine;
	}

	public void setFine(double fine) {
		this.fine = fine;
	}

	public double getDamageAmount() {
		return damageAmount;
	}

	public void setDamageAmount(double damageAmount) {
		this.damageAmount = damageAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isTenantBlakListed() {
		return tenantBlakListed;
	}

	public void setTenantBlakListed(boolean tenantBlakListed) {
		this.tenantBlakListed = tenantBlakListed;
	}

	public HtmlSelectManyCheckbox getSelectManyActions() {
		return selectManyActions;
	}

	public void setSelectManyActions(HtmlSelectManyCheckbox selectManyActions) {
		this.selectManyActions = selectManyActions;
	}

	public Long getViolationId() {
		if (viewRootMap.containsKey("violationId")
				&& viewRootMap.get("violationId") != null)
			violationId = (Long) viewRootMap.get("violationId");
		
		return violationId;
	}

	public void setViolationId(Long violationId) {
		this.violationId = violationId;
		
		if (this.violationId != null )
			viewRootMap.put("violationId", this.violationId);
	}

	public Long getInspectionId() {
		
		if (viewRootMap.containsKey("inspectionId")
				&& viewRootMap.get("inspectionId") != null)
			inspectionId = (Long) viewRootMap.get("inspectionId");
		
		return inspectionId;
	}

	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
		
		if (this.inspectionId != null )
			viewRootMap.put("inspectionId", this.inspectionId);
	}

	public Long getUnitId() {
		if (viewRootMap.containsKey("unitId")
				&& viewRootMap.get("unitId") != null)
			unitId = (Long) viewRootMap.get("unitId");
		
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;

		if (this.unitId != null )
			viewRootMap.put("unitId", this.unitId);
	}

	public boolean isHasTenant() {
		return hasTenant;
	}

	public void setHasTenant(boolean hasTenant) {
		this.hasTenant = hasTenant;
	}

	public String getUnitStatus() {
		return unitStatus;
	}

	public void setUnitStatus(String unitStatus) {
		this.unitStatus = unitStatus;
	}

	public String getViolationDate() {
		return violationDate;
	}

	public void setViolationDate(String violationDate) {
		this.violationDate = violationDate;
	}

	public ViolationView getViolationViewForActions() {
		
		if (viewRootMap.containsKey("violationViewForActions")
				&& viewRootMap.get("violationViewForActions") != null)
			violationViewForActions = (ViolationView) viewRootMap.get("violationViewForActions");
		return violationViewForActions;
	}

	public void setViolationViewForActions(ViolationView violationViewForActions) {
		
		this.violationViewForActions = violationViewForActions;
		
		if (this.violationViewForActions != null )
			viewRootMap.put("violationViewForActions", this.violationViewForActions);
	}

	public String getViolationStatus() {
		return violationStatus;
	}

	public void setViolationStatus(String violationStatus) {
		this.violationStatus = violationStatus;
	}

	public String getViolationType() {
		return violationType;
	}

	public void setViolationType(String violationType) {
		this.violationType = violationType;
	}

	public String getViolationCategory() {
		return violationCategory;
	}

	public void setViolationCategory(String violationCategory) {
		this.violationCategory = violationCategory;
	}

	public String btnRemoveItems_Click() 
	{
		String methodName = "btnRemoveItems_Click";
		
		try 
		{
			//HashMap selectedUsersMap=new HashMap();
			
//			if( selectedActions != null ){
//			  for(int i =0;i<selectedActions.size();i++)
//			  {
//				  selectedUsersMap.put(selectedActions.get(i).toString(), selectedActions.get(i).toString());
//		    	  
//			  }
			    removeItemsFromList(null);
//			}
			

		} catch (Exception e) 
		{
			logger.logError(methodName + "|" + "Exception Occured::" + e);
		}
		return "";
	}

	public String btnAddItems_Click() 
	{
		String methodName = "btnAddItems_Click";
		
		try 
		{
				addItemsToList();
			

		} catch (Exception e) 
		{
			logger.LogException(methodName + "|" + "Exception Occured::" , e);
		}
		return "";
	}
	
//	private void removeItemsFromList(HashMap selectedUsersMap)
//	{
//		   if(selectedUsersInspectionList.size()>0 )
//		   {
//			   if( sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
//			       sessionMap.remove("SESSION_USER_INSPECTIONS");
//			   //usersInspectionList.clear();
//		   }
//		   else
//		   {
//			   if(sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
//				      usersInspectionList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_INSPECTIONS");
//		   }
//		   List<SelectItem> tempList=new ArrayList<SelectItem>();
//		   for(int j=0;j<usersInspectionList.size();j++)
//		   {
//				SelectItem useritem=(SelectItem)usersInspectionList.get(j);
//				if(!selectedUsersMap.containsKey(useritem.getValue()))
//				{
//				  tempList.add(useritem);
//				}
//					 
//			}
//			usersInspectionList= tempList;
//			sessionMap.put("SESSION_USER_INSPECTIONS",usersInspectionList);
//
//		
//		
//		
//	}
    
	private void removeItemsFromList(HashMap selectedUsersMap)
	{
		List<SelectItem> removeList = new ArrayList<SelectItem>(5);
		   
		  for(int i =0; i < getAlreadySelectedListValues().size(); i++)
		  {
//			  if( !getActionListValues().contains(getAlreadySelectedListValues().get(i).toString()))
//			  {					  
				   for( int j=0; j< getAlreadySelectedList().size(); j++){
					   
					   if(getAlreadySelectedList().get(j).getValue().equals(getAlreadySelectedListValues().get(i))){
						   //getActionList().add( getAlreadySelectedList().get(j) );
						   
						   // Add to Remove list for Removing from all action list later
						   removeList.add(getAlreadySelectedList().get(j));
					   }
				   }
//			  }
		  }
		  
		  getAlreadySelectedList().removeAll(removeList);
		  
		  getAlreadySelectedListValues().clear();
		
		
	}
	
	private void addItemsToList()
    {
    		HashMap hMapAddedActions = new HashMap();
    
    		List<SelectItem> removeList = new ArrayList<SelectItem>(5);
		   
			  for(int i =0; i < getActionListValues().size(); i++)
			  {
//				  if( !getAlreadySelectedList().contains(getActionListValues().get(i).toString()))
//				  {					  
					   for( int j=0; j< getActionList().size(); j++){
						   
						   if(getActionList().get(j).getValue().equals(getActionListValues().get(i))){
							   
							   if( !getAlreadySelectedList().contains(getActionList().get(j))){
								   
								   getAlreadySelectedList().add( getActionList().get(j) );
							   
								   // 	Add to Remove list for Removing from all action list later
								   removeList.add(actionList.get(j));
							   }
						   }
					   }
//				  }
			  }
			  
			  getActionList().removeAll(removeList);
			  
			  getActionListValues().clear();
			  
    }
    
    
//    private void addItemsToListBackup()
//    {
//    		HashMap hMapAddedActions = new HashMap();
//    		
//		   if(sessionMap.containsKey("SESSION_HASHMAP_INSPECTION_TEAM_USERS"))
//			  hMap=(HashMap)sessionMap.get("SESSION_HASHMAP_INSPECTION_TEAM_USERS");
//		   if(sessionMap.containsKey("SESSION_USER_INSPECTIONS"))
//			      usersInspectionList=(ArrayList<SelectItem>) sessionMap.get("SESSION_USER_INSPECTIONS");
//		   
//			  for(int i =0; i < actionListValues.size(); i++)
//			  {
//				  if( alreadySelectedListValues.contains(actionListValues.get(i).toString()))
//				  //if(hMap.containsKey(selectedInspectionTeamList.get(i).toString()))
//				  {
//					 // User user=(User) hMap.get(selectedInspectionTeamList.get(i).toString());
//					  SelectItem item=null; 
//					   item =new SelectItem(user.getLoginId(),user.getFullName());
//					   
//					   if(!isItemExists(user))
//					   {
//						   alreadySelectedList.add(item);
//						   
//					   }
//
//				  }
//			  }
//			  sessionMap.put("USER_INSPECTIONS",usersInspectionList);
//			  sessionMap.put("SESSION_USER_INSPECTIONS",usersInspectionList);
//    }
	
    private boolean isItemExists(String value)
    {
    	String methodName="isItemExists";
    	
    	boolean isExist=false;
    	logger.logInfo("|"+"Start..");
    	List<SelectItem> itemList= actionList;
    	
    	if(itemList!=null)
    	{
	    	for(int i=0;i<itemList.size();i++)
	    	{
	    		SelectItem item=(SelectItem)itemList.get(i);
	    		if(item.getValue().equals(value))
	    		{
	    			isExist=true;
	    			break;
	    		}
	    	}
    	}
    	
    	logger.logInfo("|"+"Finish..");
    	return isExist;
    }

	public InspectionViolationView getInspectionViolationView() {
		return inspectionViolationView;
	}

	public void setInspectionViolationView(
			InspectionViolationView inspectionViolationView) {
		this.inspectionViolationView = inspectionViolationView;
	}

	public List<String> getActionListValues() {
		if (viewRootMap.containsKey("actionListValues") && viewRootMap.get("actionListValues") != null)
			
			actionListValues = (List<String>) viewRootMap.get("actionListValues");
		
		return actionListValues;
	}

	public void setActionListValues(List<String> actionListValues) {
		this.actionListValues = actionListValues;
		
		if (this.actionListValues != null )
			viewRootMap.put("actionListValues", this.actionListValues);
	}

	public List<SelectItem> getAlreadySelectedList() {
		
		if (viewRootMap.containsKey("alreadySelectedList")
				&& viewRootMap.get("alreadySelectedList") != null)
			
			alreadySelectedList = (List<SelectItem>) viewRootMap.get("alreadySelectedList");

		return alreadySelectedList;
	}

	public void setAlreadySelectedList(List<SelectItem> alreadySelectedList) {
		
		this.alreadySelectedList = alreadySelectedList;
		
		if (this.alreadySelectedList != null )
			viewRootMap.put("alreadySelectedList", this.alreadySelectedList);
	}

//	public ViolationView getViolationViewForActions() {
//		
//		if (viewRootMap.containsKey("violationViewForActions")
//				&& viewRootMap.get("violationViewForActions") != null)
//			violationViewForActions = (ViolationView) viewRootMap.get("violationViewForActions");
//		return violationViewForActions;
//	}
//
//	public void setViolationViewForActions(ViolationView violationViewForActions) {
//		
//		this.violationViewForActions = violationViewForActions;
//		
//		if (this.violationViewForActions != null )
//			viewRootMap.put("violationViewForActions", this.violationViewForActions);
//	}
	
	public List getAlreadySelectedListValues() {
		
		if (viewRootMap.containsKey("alreadySelectedListValues")
				&& viewRootMap.get("alreadySelectedListValues") != null)
			
			alreadySelectedListValues = (List<SelectItem>) viewRootMap.get("alreadySelectedListValues");
		
		return alreadySelectedListValues;
	}

	public void setAlreadySelectedListValues(List alreadySelectedListValues) {
		this.alreadySelectedListValues = alreadySelectedListValues;
		
		if (this.alreadySelectedListValues != null )
			viewRootMap.put("alreadySelectedListValues", this.alreadySelectedListValues);
	}

	public HtmlAjaxCommandButton getCmdAddTeamMembers() {
		return cmdAddTeamMembers;
	}

	public void setCmdAddTeamMembers(HtmlAjaxCommandButton cmdAddTeamMembers) {
		this.cmdAddTeamMembers = cmdAddTeamMembers;
	}

	public HtmlAjaxCommandButton getCmdRemoveTeamMembers() {
		return cmdRemoveTeamMembers;
	}

	public void setCmdRemoveTeamMembers(HtmlAjaxCommandButton cmdRemoveTeamMembers) {
		this.cmdRemoveTeamMembers = cmdRemoveTeamMembers;
	}

	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
	}
	
	
	public Converter getIdListConverter() {
		return new Converter() {
			public Object getAsObject(FacesContext facesContext,
					javax.faces.component.UIComponent component, String value)
					throws ConverterException {
				if (value == null)
					return null;											
 
				for (Iterator i = getAlreadySelectedListValues().iterator(); i.hasNext();) {
					//String item = (String) i.next();
					String val = (String) i.next();
					if (val.equals(value)) {
						return val;
					}
				}
				return null;
			}
 
			public String getAsString(FacesContext facesContext,
					javax.faces.component.UIComponent component, Object object)
					throws ConverterException {
				if (object instanceof SelectItem) {					
					return ((SelectItem) object).getValue().toString();
				} else if (object instanceof Long) {
					return String.valueOf(object);
				} else {
					return object.toString();
				}				
			}
 
		};
	}

}
