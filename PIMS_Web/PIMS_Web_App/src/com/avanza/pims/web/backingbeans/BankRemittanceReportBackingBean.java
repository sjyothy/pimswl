package com.avanza.pims.web.backingbeans;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.BankRemittanceReportCriteria;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;


public class BankRemittanceReportBackingBean extends AbstractController 
{

	 private static Logger logger = Logger.getLogger("BankRemittanceReportBackingBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	BankRemittanceReportCriteria bankRemittanceReportCriteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlSelectOneMenu  cmbReceiptBy = new HtmlSelectOneMenu();
	private List<String> errorMessages = new ArrayList<String>(0); 
	private List<String> successMessages = new ArrayList<String>(0);
	
	public HtmlSelectOneMenu getCmbReceiptBy() {
		return cmbReceiptBy;
	}

	public void setCmbReceiptBy(HtmlSelectOneMenu cmbReceiptBy) {
		this.cmbReceiptBy = cmbReceiptBy;
	}

	public BankRemittanceReportBackingBean()
	{ 
		if(CommonUtil.getIsEnglishLocale())
			bankRemittanceReportCriteria = new BankRemittanceReportCriteria(ReportConstant.Report.BANK_REMMITANCE_REPORT_EN, ReportConstant.Processor.BANK_REMMITANCE_REPORT, CommonUtil.getLoggedInUser());		
		else
			bankRemittanceReportCriteria = new BankRemittanceReportCriteria(ReportConstant.Report.BANK_REMMITANCE_REPORT_AR, ReportConstant.Processor.BANK_REMMITANCE_REPORT, CommonUtil.getLoggedInUser());
	}
	
		public String cmdView_Click() {
			
		
		if(validate())
		{
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, bankRemittanceReportCriteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		return null;
	}
		
		
	private Boolean  validate()
	{
		Boolean flage = true;
		errorMessages = new ArrayList<String>();
		
		if(bankRemittanceReportCriteria.getReceiptDateFrom()== null || bankRemittanceReportCriteria.getReceiptDateFrom().equals("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "BankRemittanceReport.ErrorMsg.ReceiptDateFrom" ) );
			flage = false;
		}
			
		if(bankRemittanceReportCriteria.getReceiptDateTo()== null || bankRemittanceReportCriteria.getReceiptDateTo().equals("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "BankRemittanceReport.ErrorMsg.ReceiptDateTo" ) );
			flage = false;
		}
		if(bankRemittanceReportCriteria.getPaymentMethod() == null || bankRemittanceReportCriteria.getPaymentMethod().equals("-1") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "BankRemittanceReport.ErrorMsg.PaymentMethod" ) );
			flage = false;
		}
		if(bankRemittanceReportCriteria.getRemittanceAccountName() == null || bankRemittanceReportCriteria.getRemittanceAccountName().equals("-1") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "BankRemittanceReport.ErrorMsg.RemittanceAccountName" ) );
			flage = false;
		}
		
		
		return flage;
	}
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public BankRemittanceReportCriteria getBankRemittanceReportCriteria() {
		return bankRemittanceReportCriteria;
	}

	public void setBankRemittanceReportCriteria(
			BankRemittanceReportCriteria bankRemittanceReportCriteria) {
		this.bankRemittanceReportCriteria = bankRemittanceReportCriteria;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

		
	
}
