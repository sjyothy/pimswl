package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.HtmlHiddenRenderer;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.FloorView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.ui.util.ResourceUtil;

public class GenerateFloors extends AbstractController{

	public class Messages{
		public static final String FLOORS_PREFIX_OR_NUMBER_SHOULD_BE_PROVIDED = "generateFloors.messages.prefixOrNoRequired";
		public static final String REQUIRED_NO_OF_FLOORS = "generateFloors.messages.NoOfFloorsRequired";
		public static final String REQUIRED_FLOOR_NUM_FROM = "generateFloors.messages.FloorNumFromRequired";
		public static final String REQUIRED_FLOOR_NAME_PREFIX = "generateFloors.messages.FloorNamePrefixRequired";
		public static final String REQUIRED_FLOOR_NUMBER_PREFIX  = "generateFloors.messages.FloorNumberPrefixRequired";
		public static final String NO_OF_FLOORS_POSITIVE_WHOLE_NUM = "generateFloors.messages.NoOfFloorsPositiveWholeNum";
		public static final String FLOOR_NUM_FROM_POSITIVE_WHOLE_NUM = "generateFloors.messages.FloorNumFromPositiveWholeNum";
		public static final String FLOORS_GENERATED_SUCCESSFULLY = "generateFloors.messages.FloorsGeneratedSuccess";
		public static final String FLOORS_GENERATED_GENERAL_ERROR = "generateFloors.messages.generalError";
		public static final String FLOORS_GENERATED_GENERAL_ALREADY_EXISTS = "generateFloors.messages.floorsAlreadyExists";
		public static final String FLOORS_FROM_SMALLER_FLOOR_TO="generateFloors.messages.floorFromSmallerorEqualThanFloorTo";
	}
	//Fields
	
	 private transient Logger logger = Logger.getLogger(SelectWinner.class);	  
	 
	 private HtmlInputText inputTextPropertyName = new HtmlInputText() ;
	 private HtmlInputText inputTextNumberOfFloors = new HtmlInputText();
	 private HtmlInputText inputTextFloorNamePrefix = new HtmlInputText();
	 private HtmlInputText inputTextFloorNumberPrefix = new HtmlInputText();
	 private HtmlInputText inputTextFloorNumberFrom = new HtmlInputText();
	 private HtmlInputText inputTextFloorNumberTo = new HtmlInputText();	
	 private HtmlInputHidden hdnTextFloorNumberTo = new HtmlInputHidden();
	 private HtmlInputHidden hdnTextPropertyId = new HtmlInputHidden();
	 private HtmlInputHidden hdnTextPropertyName = new HtmlInputHidden();
	 private HtmlSelectOneMenu selectOneMenu = new HtmlSelectOneMenu();
	 private String floorTypeId;
	 private HtmlCommandButton commandButtonSearch;	 
	 private boolean isEnglishLocale = false;
	 private boolean isArabicLocale = false;
	
	 private List<String> messages;
	 private List<String> error_Messages;
	 private List<String> error_Messages_ub;
	 
	//Constructor
	/** default constructor */
	public GenerateFloors(){				
	}
	
	//Methods
	private String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}
	public boolean isValidPositiveLong(String val)
	{
		boolean isValidPositiveLong = true;
		
		try
		{
			Long lVal = Long.parseLong(val);
			if(lVal < 0)
				isValidPositiveLong = false;
		}
		catch(Exception exp)
		{
			isValidPositiveLong = false;
		}
			
		return isValidPositiveLong;
	}
	public boolean ValidateScreen()
	{
		boolean isValid  = true;
		
		try
		{
		Object noOfFloors = inputTextNumberOfFloors.getValue();
		Object floorNumberFrom = inputTextFloorNumberFrom.getValue();
		Object floorNumberTo = hdnTextFloorNumberTo.getValue();
		Object floorNamePrefix = inputTextFloorNamePrefix.getValue().toString();
		Object floorNumberPrefix = inputTextFloorNumberPrefix.getValue().toString();
		
		error_Messages = new ArrayList<String>();
		
		if(noOfFloors == null || noOfFloors.toString().trim().equals("") )
		{
			error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.REQUIRED_NO_OF_FLOORS));
			isValid = false;
		}
		else
		{
			if(!(isValidPositiveLong(noOfFloors.toString())))
			{
				error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.NO_OF_FLOORS_POSITIVE_WHOLE_NUM));
				isValid = false;
			}
		}
		if(floorNumberFrom == null || floorNumberFrom.toString().trim().equals("") )
		{
			error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.REQUIRED_FLOOR_NUM_FROM));
			isValid = false;
		}
		else
		{
			if(!(isValidPositiveLong(floorNumberFrom.toString())))
			{
				error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOOR_NUM_FROM_POSITIVE_WHOLE_NUM));
			}
		}
		
//		if(floorNamePrefix == null || floorNamePrefix.toString().trim().equals("") )
//		{
//			error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.REQUIRED_FLOOR_NAME_PREFIX));
//			isValid = false;
//		}
//		
//		if(floorNumberPrefix == null || floorNumberPrefix.toString().equals("") )
//		{
//			error_Messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.REQUIRED_FLOOR_NUMBER_PREFIX));
//			isValid = false;
//		}	
		}
		catch(Exception ex)
		{
			logger.logError("Crashed while validating:", ex);			
			error_Messages_ub.add(ResourceUtil.getInstance().getProperty(
					GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ERROR));
			
			isValid = false;
		}		
		return isValid;
		
	}
	public String generatePropertyFloors()
	{
		String method = "generatePropertyUnits";
		logger.logInfo(method + " started...");
		
		try
		{
			if(ValidateScreen())
			{
			logger.logInfo("validation has been done");
			String loggedInUser = getLoggedInUser();
			Long propertyId = Convert.toLong(hdnTextPropertyId.getValue());
			int noOfFloors = Convert.toInteger(inputTextNumberOfFloors.getValue());
			int floorNumberFrom = Convert.toInteger(inputTextFloorNumberFrom.getValue());
			int floorNumberTo = floorNumberFrom + noOfFloors - 1;
			String floorNamePrefix = inputTextFloorNamePrefix.getValue().toString();
			String floorNumberPrefix = inputTextFloorNumberPrefix.getValue().toString();
			Long floorTypeId = Convert.toLong(getFloorTypeId());			
			
			PropertyServiceAgent psAgent = new PropertyServiceAgent();
			
			ArrayList<FloorView> propertyFloorViews = new ArrayList<FloorView>();
            for(int index = floorNumberFrom; index <= floorNumberTo; index ++ ) {
                FloorView propertyFloorView = new FloorView();
                propertyFloorView.setCreatedBy(loggedInUser);
                propertyFloorView.setCreatedOn(new Date());
                propertyFloorView.setFloorNumber(floorNumberPrefix+"-"+index);
                propertyFloorView.setFloorName(floorNamePrefix+"-"+index);
                propertyFloorView.setFloorNo(Double.valueOf(index));
                propertyFloorView.setIsDeleted(new Long(0));
                propertyFloorView.setRecordStatus(new Long(1));
                propertyFloorView.setPropertyId(propertyId);
                propertyFloorView.setTypeId(floorTypeId);
                propertyFloorView.setUpdatedBy(loggedInUser);
                propertyFloorView.setUpdatedOn(new Date());
                
                propertyFloorViews.add(propertyFloorView);
            }
            
            propertyFloorViews = psAgent.generateFloors(propertyId,propertyFloorViews);
            messages = new ArrayList<String>();
            error_Messages_ub = new ArrayList<String>();
            
            if(propertyFloorViews.size()  >0) {
            	error_Messages_ub.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ALREADY_EXISTS));                
                for(FloorView floorView: propertyFloorViews) {
                	{
                		error_Messages_ub.add(floorView.getFloorNumber()+" ; "+floorView.getFloorName());
                		System.out.println("Floor Number: "+floorView.getFloorNumber()+" Floor Name: "+floorView.getFloorName() );
                	}
                }
            }
            else {
            	messages.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_SUCCESSFULLY));               
            }
            
            inputTextFloorNumberTo.setValue(floorNumberTo);
		}
			logger.logInfo(method+"completed");
		}
		catch(Exception ex)
		{
			logger.logError("Crashed while generating floors:",ex);
			ex.printStackTrace();
			error_Messages_ub.add(ResourceUtil.getInstance().getProperty(GenerateFloors.Messages.FLOORS_GENERATED_GENERAL_ERROR));
		}
		
		return "";
		
	}
	
	
	private void closeWindow() {
		final String viewId = "/GenerateFloors.jsp";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		// Your Java script code here
		String javaScriptText = "javascript:closeWindow();";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);

	}

	private void setInfo() {
		String methodName = "setInfo";
		logger.logInfo(methodName + " started...");
		try {
			Object obj = getFacesContext().getExternalContext().getSessionMap()
					.get(WebConstants.GenerateFloors.PROPERTY_VIEW);

			if (obj != null) {
				PropertyView property = (PropertyView) obj;
				hdnTextPropertyId.setValue(property.getPropertyId());
				hdnTextPropertyName.setValue(property.getCommercialName());
				inputTextPropertyName.setValue(property.getCommercialName());
			} 
			logger.logInfo(methodName + " comleted successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed...", exception);
		}
	}
	
	/**
	 * <p>
	 * Callback method that is called whenever a page is navigated to, either
	 * directly via a URL, or indirectly via page navigation. Override this
	 * method to acquire resources that will be needed for event handlers and
	 * lifecycle methods, whether or not this page is performing post back
	 * processing.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub		
		super.init();
		if(!isPostBack())
			setInfo();
	}

	/**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void prerender() {
		super.prerender();
	}
	
    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called, regardless of whether
     * or not this was the page that was actually rendered.  Override this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
    public void destroy() {
        super.destroy();
    }

	//Getters and Setters

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	// This is codo for testing my popup
	public void showGenerateFloorsPopup(ActionEvent event){		
		
		
		PropertyView property = new PropertyView();
		property.setPropertyId(new Long(4));
		property.setCommercialName("This is a test property");
		
		getFacesContext().getExternalContext().getSessionMap()
		.put(WebConstants.GenerateFloors.PROPERTY_VIEW,property);
		
	    final String viewId = "/home.jsf"; 
	    
	    FacesContext facesContext = FacesContext.getCurrentInstance();

	    // This is the proper way to get the view's url
	    ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	    String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	    
	    String javaScriptText = "javascript:GenerateFloorsPopup();";
	    
	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	}	
	
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}	
	public String getMessages() {
		String messageList;
		if ((messages == null) ||
			(messages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<B><UL>\n";
			for(String message: messages) {
				messageList = messageList + message + "<BR> \n";
		}
		messageList = messageList + "</UL></B>\n";
		}
		return(messageList);
	}
	public String getErrorMessages() {
				
		
		
		return CommonUtil.getErrorMessages(error_Messages);
	}

	public HtmlInputText getInputTextPropertyName() {
		return inputTextPropertyName;
	}

	public void setInputTextPropertyName(HtmlInputText inputTextPropertyName) {
		this.inputTextPropertyName = inputTextPropertyName;
	}

	public HtmlInputText getInputTextNumberOfFloors() {
		return inputTextNumberOfFloors;
	}

	public void setInputTextNumberOfFloors(HtmlInputText inputTextNumberOfFloors) {
		this.inputTextNumberOfFloors = inputTextNumberOfFloors;
	}

	public HtmlInputText getInputTextFloorNamePrefix() {
		return inputTextFloorNamePrefix;
	}

	public void setInputTextFloorNamePrefix(HtmlInputText inputTextFloorNamePrefix) {
		this.inputTextFloorNamePrefix = inputTextFloorNamePrefix;
	}

	public HtmlInputText getInputTextFloorNumberPrefix() {
		return inputTextFloorNumberPrefix;
	}

	public void setInputTextFloorNumberPrefix(
			HtmlInputText inputTextFloorNumberPrefix) {
		this.inputTextFloorNumberPrefix = inputTextFloorNumberPrefix;
	}

	public HtmlInputText getInputTextFloorNumberFrom() {
		return inputTextFloorNumberFrom;
	}

	public void setInputTextFloorNumberFrom(HtmlInputText inputTextFloorNumberFrom) {
		this.inputTextFloorNumberFrom = inputTextFloorNumberFrom;
	}

	public HtmlInputText getInputTextFloorNumberTo() {
		return inputTextFloorNumberTo;
	}

	public void setInputTextFloorNumberTo(HtmlInputText inputTextFloorNumberTo) {
		this.inputTextFloorNumberTo = inputTextFloorNumberTo;
	}

	public HtmlInputHidden getHdnTextFloorNumberTo() {
		return hdnTextFloorNumberTo;
	}

	public void setHdnTextFloorNumberTo(HtmlInputHidden hdnTextFloorNumberTo) {
		this.hdnTextFloorNumberTo = hdnTextFloorNumberTo;
	}


	public HtmlSelectOneMenu getSelectOneMenu() {
		return selectOneMenu;
	}

	public void setSelectOneMenu(HtmlSelectOneMenu selectOneMenu) {
		this.selectOneMenu = selectOneMenu;
	}

	public String getFloorTypeId() {
		return floorTypeId;
	}

	public void setFloorTypeId(String floorTypeId) {
		this.floorTypeId = floorTypeId;
	}

	public HtmlInputHidden getHdnTextPropertyId() {
		return hdnTextPropertyId;
	}

	public void setHdnTextPropertyId(HtmlInputHidden hdnTextPropertyId) {
		this.hdnTextPropertyId = hdnTextPropertyId;
	}

	public HtmlInputHidden getHdnTextPropertyName() {
		return hdnTextPropertyName;
	}

	public void setHdnTextPropertyName(HtmlInputHidden hdnTextPropertyName) {
		this.hdnTextPropertyName = hdnTextPropertyName;
	}
	 
}

