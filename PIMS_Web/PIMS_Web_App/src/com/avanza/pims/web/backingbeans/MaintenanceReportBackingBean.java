package com.avanza.pims.web.backingbeans;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.MaintenanceReportCriteria;
import com.avanza.pims.web.controller.AbstractController;


public class MaintenanceReportBackingBean extends AbstractController 
{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("MaintenanceReportBackingBean");
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	MaintenanceReportCriteria criteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlSelectOneMenu  cmbReceiptBy = new HtmlSelectOneMenu();
	
	
	public MaintenanceReportBackingBean()
	{ 
		criteria = new MaintenanceReportCriteria("MaintenanceReport.rpt", "com.avanza.pims.report.processor.MaintenanceReportProcessor", getLoggedInUserObj().getFullName());		
	}
	
	
	public String cmdView_Click() 
	{
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, criteria);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		return null;
	}
	
	private void openPopup(String javaScriptText) {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public MaintenanceReportCriteria getCriteria() {
		return criteria;
	}
	public void setCriteria(MaintenanceReportCriteria criteria) 
	{
		this.criteria = criteria;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlSelectOneMenu getCmbReceiptBy() {
		return cmbReceiptBy;
	}

	public void setCmbReceiptBy(HtmlSelectOneMenu cmbReceiptBy) {
		this.cmbReceiptBy = cmbReceiptBy;
	}
	
	
	
}
