package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.richfaces.component.html.HtmlColumn;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.ContractStatsView;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Locations;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.ProjectStatsView;
import com.avanza.pims.entity.RequestStatsView;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskSearchCriteria;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.RegisterExternalUserService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.services.AppointmentService;
import com.avanza.pims.ws.services.LocationsService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AppointmentInputParameter;
import com.avanza.pims.ws.vo.AspectsCompareInputParameters;
import com.avanza.pims.ws.vo.ContractSearchCriteria;
import com.avanza.pims.ws.vo.NolInputParameter;
import com.avanza.pims.ws.vo.NormalDisbursementInputParameter;
import com.avanza.pims.ws.vo.PersistBeneficiaryInputParameters;
import com.avanza.pims.ws.vo.PersistDonationsInputParameters;
import com.avanza.pims.ws.vo.PersistMinorMaintenanceInputParameters;
import com.avanza.pims.ws.vo.PersistPropertyMaintenanceInputParameters;
import com.avanza.pims.ws.vo.PersistRecommendationsInputParameters;
import com.avanza.pims.ws.vo.PersistSocialResearchInputParameters;
import com.avanza.pims.ws.vo.ReplaceChequeInputParameters;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.StatsUndistributedPaymentsView;
import com.avanza.pims.ws.vo.TransferRequestInputParameters;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class StatsScreen extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HtmlDataTable dataTable;
	private HtmlDataTable dataTableUndistributedCheques;
	private HtmlDataTable dataTableUndistributedNonCheques;
	private HtmlDataTable dataTableUndistributedProfits;
	private HtmlColumn htmlColumnsAssignedDate = new HtmlColumn();
	private String sortField = null;
	private List<String> errorMessages;
	private boolean sortAscending = true;
	private HtmlCommandLink btnAction = new HtmlCommandLink();
	private HtmlInputHidden addCount;

	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private UserTask userTask = new UserTask();
	private List<UserTask> userTasks = new ArrayList<UserTask>();

	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private String dateFormat = "";

	private Date assignedDateFrom;
	private Date assignedDateTo;
	private String applicationTitle;
	private String taskTitle;

	FacesContext context = FacesContext.getCurrentInstance();

	Map<String, Object> sessionMap = context.getExternalContext()
			.getSessionMap();
	Map<String, Object> viewRootMap = getFacesContext().getViewRoot()
			.getAttributes();

	private List<ProjectStatsView> projectsStats = new ArrayList<ProjectStatsView>(
			0);
	private List<ContractStatsView> rentContractStats = new ArrayList<ContractStatsView>(
			0);;
	private List<ContractStatsView> otherContractStats = new ArrayList<ContractStatsView>(
			0);;
	private List<RequestStatsView> requestStats = new ArrayList<RequestStatsView>(
			0);;

	private static Logger logger = Logger.getLogger(StatsScreen.class);

	// Actions
	// -----------------------------------------------------------------------------------

	public HtmlDataTable getDataTableUndistributedProfits() {
		return dataTableUndistributedProfits;
	}

	public void setDataTableUndistributedProfits(
			HtmlDataTable dataTableUndistributedProfits) {
		this.dataTableUndistributedProfits = dataTableUndistributedProfits;
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale = localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public List<UserTask> getUserTasks() {
		if (userTasks == null) {
			loadDataList();
		}

		userTasks = (List<UserTask>) sessionMap.get("userTasks");
		return userTasks;
	}

	public String loadDataList() {

		logger.logInfo("Stepped into the loadDataList method");
		BPMWorklistClient bpmWorkListClient = null;
		try {

			String contextPath = ((ServletContext) getFacesContext()
					.getExternalContext().getContext())
					.getRealPath("\\WEB-INF\\config.properties");
			boolean isEnLocal = getIsEnglishLocale();
			logger.logInfo("Contextpath is:" + contextPath);

			bpmWorkListClient = new BPMWorklistClient(contextPath);

			logger.logInfo("Got BPMWorklistClient instance ");

			String loggedInUser = getLoggedInUser();
			TaskSearchCriteria taskSearchCriteria = new TaskSearchCriteria();

			if (assignedDateFrom != null)
				taskSearchCriteria.setAssignedDateFrom(assignedDateFrom);
			if (assignedDateTo != null)
				taskSearchCriteria.setAssignedDateTo(assignedDateTo);

			// User currentUser = SecurityManager.getUser(loggedInUser);
			// List<UserGroup> userGroups = currentUser.getUserGroups();
			//			
			// if(userGroups != null && userGroups.size() > 0)
			// {
			// List<String> userGrps = new ArrayList<String>(0);
			// for(UserGroup ug : userGroups )
			// {
			// userGrps.add(ug.getUserGroupId());
			// }
			// taskSearchCriteria.setAssigneeGroups(userGrps);
			// }

			Set<UserTask> userTaskss = bpmWorkListClient.getFilteredUserTasks(
					loggedInUser, taskSearchCriteria);
			userTasks = new ArrayList<UserTask>();

			HashMap<String, User> userHashMap = new HashMap<String, User>();

			// Filtering on the basis of Application Title and Task Title

			// if( (applicationTitle != null &&
			// !applicationTitle.toString().equals("")) || taskTitle != null &&
			// !taskTitle.toString().equals(""))
			// {
			for (UserTask userTask : userTaskss) {
				boolean addTask = true;
				String taskApplicationTitleEn = userTask.getRequestTitle_en();
				String taskApplicationTitleAr = userTask.getRequestTitle_ar();

				String taskTaskTitleEn = userTask.getTaskTitle_en();
				String taskTaskTitleAr = userTask.getTaskTitle_ar();

				String userId = userTask.getSentByUser();
				User user = null;

				if (userHashMap.containsKey(userId)) {
					user = userHashMap.get(userId);
				}

				else {
					user = SecurityManager.getUser(userId);
					if (user != null) {
						userHashMap.put(userId, user);
					}
				}

				if (isEnLocal) {
					if (applicationTitle != null
							&& !applicationTitle.toString().equals("")) {
						if (taskApplicationTitleEn != null
								&& !taskApplicationTitleEn.toString()
										.equals("")) {
							if (!taskApplicationTitleEn.toLowerCase().contains(
									applicationTitle.trim().toLowerCase()))
								addTask = false;
						} else {
							addTask = false;
						}

					}
					if (taskTitle != null && !taskTitle.toString().equals("")) {
						if (taskTaskTitleEn != null
								&& !taskTaskTitleEn.toString().equals("")) {
							if (!taskTaskTitleEn.toLowerCase().contains(
									taskTitle.trim().toLowerCase()))
								addTask = false;
						} else {
							addTask = false;
						}

					}

					if (user != null)
						userTask.setSentByUser(user.getFullName());
				} else {
					if (applicationTitle != null
							&& !applicationTitle.toString().equals("")) {
						if (taskApplicationTitleAr != null
								&& !taskApplicationTitleAr.toString()
										.equals("")) {
							if (!taskApplicationTitleAr.toLowerCase().contains(
									applicationTitle.trim().toLowerCase()))
								addTask = false;
						} else {
							addTask = false;
						}
					}
					if (taskTitle != null && !taskTitle.toString().equals("")) {
						if (taskTaskTitleAr != null
								&& !taskTaskTitleAr.toString().equals("")) {
							if (!taskTaskTitleAr.toLowerCase().contains(
									taskTitle.trim().toLowerCase()))
								addTask = false;
						} else {
							addTask = false;
						}
					}

					if (user != null)
						userTask.setSentByUser(user.getSecondaryFullName());
				}

				if (addTask)
					userTasks.add(userTask);
			}
			// }
			// else
			// {
			// userTasks.addAll(userTaskss);
			// }

			/*
			 * Iterator taskIterator = userTaskss.iterator();
			 * while(taskIterator.hasNext()) { UserTask task =
			 * (UserTask)taskIterator.next(); userTasks.add(task); //
			 * System.out.println("TaskId:"+task.getTaskId()); //
			 * System.out.println("Task Num:"+task.getTaskNumber()); //
			 * System.out.println("Subject en:"+task.getTaskSubject_en()); //
			 * System.out.println("Assgined Date:"+task.getAssignedDate()); //
			 * System.out.println("Expiry Date:"+task.getExpiryDate()); }
			 */

			Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			Map sessionMap = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			sessionMap.put("userTasks", userTasks);
			recordSize = userTasks.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
		} catch (PIMSWorkListException e) {

			logger
					.LogException(
							"Task List could not be loaded.loadDataList method crashed due to:",
							e);
		} catch (Exception e) {

			logger
					.LogException(
							"Task List could not be loaded.loadDataList method crashed due to:",
							e);
		}

		logger.logInfo("Stepped out of the loadDataList method");

		return "";
	}

	private String getLoggedInUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(userTasks, new ListComparator(sortField,
					sortAscending));

		}
	}

	// Getters
	// -----------------------------------------------------------------------------------

	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters
	// -----------------------------------------------------------------------------------

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers
	// -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	// ** default constructor *//*
	public StatsScreen() {
	}

	// Property accessors

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask dataItem) {
		this.userTask = dataItem;
	}

	// Public Methods

	// Actions
	// -----------------------------------------------------------------------------------

	// Classes which should be implemented in Business Logic or from where we r
	// fetching data

	public String reset() {

		return "success";
	}

	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public int getDataCount() {
		int rows = dataTable.getRows();
		int count = dataTable.getRowCount();
		return (count / rows) + ((count % rows != 0) ? 1 : 0);
	}

	@Override
	public void init() {

		super.init();
		try {
			if (!isPostBack()) {
//				testMethod();
				testMobileWS();
				assignedDateFrom = new Date();
				assignedDateTo = new Date();
				// loadDataList();
				loadRentContractSection();
				loadOtherContractSection();
				loadProjectSection();
				loadRequestSection();

			}
		} catch (Exception e) {
			logger.LogException("init -Error Occured", e);
		}

	}

	private void testMethod() throws Exception {

		
//		NumberGenerationManager mn = new NumberGenerationManager();
		
//    	String requestRefNum =  mn.getSystemNumber(Constant.SystemNumberOwners.REQUEST);
//    	System.out.println(requestRefNum);
		// Session session = (Session)EntityManager.getBroker().getSession();
		// Query query =
		// session.getNamedQuery("com.avanza.pims.entity.Endowment.SP_ENDOWMENT_MASRAF_COUNT");
		// query.setParameter("fromdate", "-1");
		// query.setParameter("todate", "-1");
		// query.setParameter("waqifName", "-1");
		// List list = query.list();
		// if( list != null && list.size() > 0 )
		// {
		// Object[] obj = (Object[])list.get(0);
		//			
		// }

//		AppointmentService service = new AppointmentService();
//		Map<Object, Object> searchCriteriaMap = new HashMap<Object, Object>();
//		searchCriteriaMap.put(Constant.AppointmentCriteria.CREATED_BY,
//				"pims_admin");
//		List<AppointmentDetails> list = service.getAppointmentList(
//				searchCriteriaMap, 10, 1, "appointmentDate", false);
//		list.size();
	}

	/**
	 * 
	 */
	private void testMobileWS() throws Exception {
//		testLeaseContractList();
//		String json = RequestService.getBounceChequeRequestDetails("pims_admin",47048l,"en");
//		String json = RequestService.getReplaceChequeRequestDetails("pims_admin",47048l,"en");
//		System.out.println(json);		
//		testPersistPropertyNolRequest();
//        Calendar paymentStartFrom = GregorianCalendar.getInstance();
//        paymentStartFrom.add(Calendar.DATE, 1);
//        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
////        
//        String json = UtilityService.getDefaultPaymentsJSONForReplaceCheque(29801l,Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE, "133463", true);
////		String json = PropertyService.getPaymentSchedulesList(
////															  "en", "-1", "10", "1", 29801l, "-1", -1l, "-1", -1l, "-1", 
////															  Constant.PAYMENT_SCHEDULE_STATUS_COLLECTED_ID, 
////															  "-1", 
////															  "-1", -1l
////											   				 );
//		System.out.println(json);
//        testPersistReplaceChequeRequest();											   
//		testpersistBounceChequeRequest();									   
//		List<TradeLicenseIssuersVO> tliList = UtilityService.getTradeLicenseIssuers("20","en");
//		System.out.println( tliList);
//		testPersistTransfer();
//		System.out.println( "getSmartAppRoleForLoggedInUser:"+UtilityService.getSmartAppRoleForLoggedInUser("pims_admin"));
//		System.out.println( UtilityService.getNewOnlineRequestCount("pims_admin"));
//		testAppointment();
//		testRequestList();
//		testPersistTransfer();
//		testCreateRenewContractRequest();
//		testPersistPropertyMaintenance();
//		testGetRenewContractDetails();
		
//		testPropertyNolRequest();
//		testPersistSocialResearch();
//		testDonationRequest();
//		testPersistMinorMaintenance();
//		testCompareAspects();
//		testDonationRequest();
//		testLeaseContractList();
	
	}
	private void testRequestList()throws Exception
	{
		RequestService requestService = new RequestService();
        Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
        searchCriteriaMap.put("loginId",new Long( 1409l));
        List<Long> requestTypeIn = new ArrayList<Long>();
        requestTypeIn.add( Constant.MemsRequestType.NOL_REQUEST_ID );
        requestTypeIn.add( Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID );
        requestTypeIn.add( Constant.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST);
        
        requestTypeIn.add( Constant.RENEW_CONTRACT);
        requestTypeIn.add( Constant.REQUEST_TYPE_NOL);
        requestTypeIn.add( Constant.REQUEST_TYPE_MAINTENANCE_REQUEST);
        requestTypeIn.add( Constant.REQUEST_TYPE_TRANSFER_CONTRACT);
        requestTypeIn.add( Constant.REQUEST_TYPE_REPLACE_CHEQUE);
        requestTypeIn.add( Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
        requestTypeIn.add(Constant.CANCEL_CONTRACT);
        searchCriteriaMap.put("requestTypeIn", requestTypeIn);    
        String json = requestService.searchRequestsForMobile(
																		                searchCriteriaMap,
																		                1,
																		                1,
																		                "requestNumber",
																		                true
																		              );

        System.out.println("Json:"+json);
		
	}
	public String testGetRenewContractDetails() {
		String json = "";
		
		try {
			
			   
				PropertyService ps = new PropertyService();
	           json = ps.getDataForRenewContract("pims_admin",11674);
	           System.out.println(json);
		
		} catch (Exception e) {
			logger.LogException("testGetRenewContractDetails|Error occured", e);

		}
		return json;
	}
	
	public String testCreateRenewContractRequest() {
		String json = "";
		StringBuilder errorJson = null;
		try {
			
			   PropertyService service  = new PropertyService();
			   RequestView requestView = service.createRenewContractRequest("pims_admin", 11674l, 1409l, "l");
			   
	           System.out.println(requestView.getRequestId());
		} catch (Exception e) {
			logger.LogException("testCreateRenewContractRequest|Error occured", e);

		}
		return json;
	}

	public String testPersistPropertyNolRequest() {
		String json = "";
		StringBuilder errorJson = null;
		try {
			
			   String filters="{\"createdBy\":\"pims_admin\",\"createdByPersonId\":1409,\"contractId\":\"11674\",\"requestSource\":\"1\"," +
			   		"\"description\":\"testing for  mobile\",\"nolTypeId\":19," +
			   		"\"requestId\":-1}";
			   Gson jsonObj =  new GsonBuilder().create();
	           NolInputParameter obj = jsonObj.fromJson( 
	        		   												filters,  
	                                                                NolInputParameter.class
	                                                               );
	            RequestService requestService  = new RequestService();
	            
	            RequestView requestView = requestService.persistPropertyNolRequest(obj);
	           System.out.println(json);
		} catch (Exception e) {
			logger.LogException("testPropertyNolRequest|Error occured", e);

		}
		return json;
	}

	public String testLeaseContractList() {
		String json = "";
		StringBuilder errorJson = null;
		try {
			
			   String filters="{\"tenantId\":\"34218\",\"recordsPerPage\":\"10\",\"currentPage\":\"1\",\"orderBy\":\"contractId\"}";
	           Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
	           ContractSearchCriteria searchCriteria = jsonObj.fromJson( 
	                                                              filters,  
	                                                              ContractSearchCriteria.class
	                                                            );
	           json = PropertyService.getLeaseContractListJson(searchCriteria);
	           System.out.println(json);
		} catch (Exception e) {
			logger.LogException("testLeaseContractList|Error occured", e);

		}
		return json;
	}

	public String registerUser(String emiratesId, String mobile,
			String emailAddress, String password) {
		String json = "";
		StringBuilder errorJson = null;
		try {
			logger.logDebug(
					"emiratesId:%s|mobile:%s|emailAddress:%s|password:%s",
					emiratesId, mobile, emailAddress, password);
			List<Person> personPresentInMinorFilesList = new ArrayList<Person>();
			String errorInInputDetails = hasRegisterInputDetailsError(
					errorJson, emiratesId, mobile, emailAddress, password,
					personPresentInMinorFilesList);
			if (errorInInputDetails != null) {
				json = errorInInputDetails;
			}
			Person user = RegisterExternalUserService
					.persistExternalUserAsSecUser(emailAddress, emailAddress,
							mobile, password, personPresentInMinorFilesList);
			return "{\"result\": [{" + " \"userName\":\"" + user.getFullName()
					+ "\"" + ",\"loginId\":\"" + user.getLoginId() + "\"" +

					"}]}";
		} catch (Exception e) {
			logger.LogException("registerUser|Error occured", e);

		}
		return json;
	}

	private String hasRegisterInputDetailsError(StringBuilder errorJson,
			String emiratesId, String mobileNumber, String emailAddress,
			String password, List<Person> personPresentInMinorFilesList)
			throws Exception {
		// Emirate Id is not provided
		if (emiratesId == null || emiratesId.trim().length() <= 0) {
			if (errorJson == null)
				errorJson = new StringBuilder("{\"errors\": [");

			errorJson = errorJson.append("}]}");
			return errorJson.toString();
		}
		// Password is not provided
		else if (password == null || password.trim().length() <= 0) {
			if (errorJson == null)
				errorJson = new StringBuilder("{\"errors\": [");

			errorJson = errorJson.append("}]}");
			return errorJson.toString();
		}

		// Mobile is not provided
		else if (mobileNumber == null || mobileNumber.trim().length() <= 0) {
			if (errorJson == null)
				errorJson = new StringBuilder("{\"errors\": [");

			errorJson = errorJson.append("}]}");
			return errorJson.toString();
		}

		if (emiratesId != null || emiratesId.trim().length() > 0) {
			// Checking for Person Present against the Emirates Id -Start
			List<Person> personList = RegisterExternalUserService
					.getPersonsForEmirateId(emiratesId);

			if (personList == null || personList.size() <= 0) {
				if (errorJson == null)
					errorJson = new StringBuilder("{\"errors\": [");

				errorJson = errorJson.append("}]}");
				return errorJson.toString();
			}
			// Checking for Person Present against the Emirates Id -Finish
			else {
				// Checking for Login Id already Generated for personId Start
				String alreadyGeneratedLoginId = RegisterExternalUserService
						.hasLoginIdGeneratedForPersons(personList);
				// Checking for Login Id already Generated for personId Finish

				// Checking for Person present in Minor files Start
				List<Person> tempPersonPresentInMinorFilesList = RegisterExternalUserService
						.isPersonPresentInMinorFiles(personList);
				if (tempPersonPresentInMinorFilesList == null
						|| tempPersonPresentInMinorFilesList.size() <= 0) {
					if (errorJson == null)
						errorJson = new StringBuilder("{\"errors\": [");

					errorJson = errorJson.append("}]}");
					return errorJson.toString();
				} else {
					personPresentInMinorFilesList
							.addAll(tempPersonPresentInMinorFilesList);
				}

				// Checking for Person present in Minor files Finish

			}

		}
		return null;
	}

	private void testDonationRequest() throws Exception 
	{
		String json = "{\"loginId\":\"-1\",\"deviceId\":\"1234567890\",\"donationProgramId\":\"1\",\"phone\":\"\",\"email\":\"\",\"donationAmount\":\"500\",\"name\":\"danish farooq\",\"paymentMethod\":1\" }";
		Gson jsonObj = new GsonBuilder().create();
		PersistDonationsInputParameters input = jsonObj.fromJson(json,PersistDonationsInputParameters.class);
		RequestService rs = new RequestService();
		rs.persistDonationRequestFromMobile( input );
		System.out.println(input.getDonationRequestId() );
	}
	private void testAppointment() throws Exception 
	{
		String json = "{\"createdBy\":\"pims_admin\"," +
				"		\"createdByPersonId\":3870," +
				"\"description\":\"testing for  mobile\"," +
				"\"appointmentTitle\":\"Appointment Test Title Form Webservice\"," +
				"\"appointmentWith\":\"salkhasibi\"," +
				"\"appointmentStartDate\":\"20/10/2015 14:01\"," +
				"\"duration\":\"3\" ,"+
				"\"email\":\"d@d.com\", " +
				"\"contactNumber\":\"00971551364549\"		}";
		Gson jsonObj = new GsonBuilder().create();
		AppointmentInputParameter input = jsonObj.fromJson(json,AppointmentInputParameter.class);
		
		AppointmentService.persistAppointments( input );
		System.out.println(input.getRequestId() );
	}
	
	
	private void testPersistBeneficiaryDetails() throws Exception 
	{
		
		String json = "{\"loginId\":\"pims_admin\",\"inheritanceBeneficiaryId\":\"213\",\"isStudent\":\"1\", 	\"minorStatus\":\"\", 	\"beneRelationId\":\"\", 	\"nurseRelationId\":\"102\", 	\"guardianRelationId\":\"\" 	 }";
		Gson jsonObj = new GsonBuilder().create();
		PersistBeneficiaryInputParameters input = jsonObj.fromJson(json,PersistBeneficiaryInputParameters.class);
		
		InheritanceFileService.persistInheritanceBeneficiaryDetailsFromMobile(input);
		System.out.println(jsonObj.toJson( input));
	}
	
	private void testCompareAspects() throws Exception 
	{
//
//		String json="{  \"fileId\" : 2479,\"loginId\":\"oaswad@amaf.ae\",\"beneficiaryId\" : 27903,\"oldAspectId\" : \"83\"," +
//				"\"socialAspects\" : {\"socialSkillsString\" : \"-1\",\"statusId\" : \"1\",\"socialProblemsDesc\" : \"\"," +
//				"\"socialProblemTypesDesc\" : \"\",\"socialProblemString\" : \"1\",\"socialProblemTypesString\" : \"3\"}}";
		
//		String json ="{  \"fileId\" : \"2479\",\"loginId\":\"oaswad@amaf.ae\",\"beneficiaryId\" : 27903,\"oldAspectId\" : \"2962\", 	\"monthlyExpense\":{ 	                    \"food\":\"1\",      						\"stationary\":\"2\", 						\"telephone\":\"3\", 						\"electricity\":\"4\", 						\"emergency\":\"5\", 						\"servant\":\"6\", 						\"clothes\":\"7\",\"personalExp\":\"8\",\"mobile\":\"9\",\"petrol\":\"10\",\"entertainment\":\"11\",\"total\":\"122\" 	}}";
//		String json="{  \"fileId\" : \"2479\",\"loginId\":\"oaswad@amaf.ae\",\"beneficiaryId\" : \"27903\",\"oldAspectId\" : \"7187\", \"accomodationAspects\":{  						\"accomodationTypeId\":\"50\", 						\"accomodationTypeDesc\":\"\", 							 						\"accomodationOwnershipId\":\"21\", 						\"accomodationOwnershipDesc\":\"\", 							 						\"resTypeId\":\"100\", 						\"resTypeDesc\":\"\", 							 						\"conditionOfHomeId\":\"3\", 						\"conditionOfHomeDesc\":\"\", 							 						\"beneficiaryOfPropertyId\":\"2\", 						\"beneficiaryOfPropertyDesc\":\"\", 						 						\"reasonForRentId\":\"100\", 						\"reasonForRentDesc\":\"asdf\" 					   }}";
//		Gson jsonObj = new GsonBuilder().create();
//		AspectsCompareInputParameters input = jsonObj.fromJson(json,AspectsCompareInputParameters.class);
//		
//		json = SocialResearchService.compareAspects(input);
//		System.out.println(jsonObj.toJson( json ));
		AspectsCompareInputParameters input = new AspectsCompareInputParameters();
		input.setBeneficiaryId(5324l);
		input.setAspectName("accomodation");
		input.setOldAspectId("7305");
		String json = SocialResearchService.compareSavedAspects(input);
		System.out.println( json );
	}
	
	private void testPersistSocialResearch() throws Exception 
	{
//		String resJson = SocialResearchService.getRecommendationsList("pims_admin","65","-1","10","1");
//		String json = "{   \"socialResearchId\":\"-1\",   \"loginId\":\"pims_admin\",   \"fileId\":\"2252\",   \"beneficiaryId\":\"25173\",   " +
//					   	  "\"copyToPersonIds\":[],   	" +
//					    //"\"generalForm\":{\"hobbies\":\"\",\"isStudent\":\"0\",\"orphanCategory\":\"102\"}" +
//					     "\"socialAspects\":{\"socialProblemTypesString\":\"\",\"socialProblemTypesDesc\":\"\",\"socialProblemString\":\"1,3\",\"socialProblemsDesc\":\"\",\"socialSkillsString\":\"1,2\",\"statusId\":\"2\"}"+			
//					   "}";
//		String json="{  \"fileId\" : 2479,\"loginId\" : \"oaswad@amaf.ae\",\"beneficiaryId\" : 27903,\"socialResearchId\" : \"-1\",\"socialAspects\" : {\"socialSkillsString\" : \"-1\",\"statusId\" : \"1\",\"socialProblemsDesc\" : \"\",\"socialProblemTypesDesc\" : \"\",\"socialProblemString\" : \"1\",\"socialProblemTypesString\" : \"3\"},\"copyToPersonIds\" : []}";
//		String json = "{\"beneficiaryId\":\"5324\",\"copyToPersonIds\":[],\"fileId\":\"93\",\"loginId\":\"malijtabi\",\"socialAspects\":{\"socialProblemDesc\":\"\",\"socialProblemTypeDesc\":\"\",\"socialProblems\":[\"1\"],\"socialProblemsTypes\":[\"1\"],\"socialSkills\":[\"1\",\"2\"],\"socialStatusId\":\"1\"},\"socialResearchId\":\"-1\"}";
		String json="{\"beneficiaryId\":\"5324\",\"copyToPersonIds\":[],\"monthlyExpense\":{\"expClothes\":\"0\",\"expElectricity\":\"0\",\"expEmergency\":\"0\",\"expEntertainment\":\"0\",\"expFood\":\"0\",\"expMobile\":\"0\",\"expPersonalExpenses\":\"0\",\"expPetrol\":\"0\",\"expServant\":\"0\",\"expStationary\":\"56\",\"expTelephone\":\"0\"},\"fileId\":\"93\",\"loginId\":\"malijtabi\",\"socialResearchId\":\"7070\"}";
		
		Gson jsonObj = new GsonBuilder().create();
		PersistSocialResearchInputParameters input = jsonObj.fromJson(json,PersistSocialResearchInputParameters.class);
		
		SocialResearchService.persistSocialResearch(input);
		System.out.println(jsonObj.toJson( input));
	}
			
    private void testPersistRecommendation() throws Exception 
	{
//    	String resJson = SocialResearchService.deleteResearchRecommendation(200,"pims_admin");
//		String json = "{ 	\"loginId\":\"pims_admin\", 	\"socialResearchId\":\"\", 	\"inheritanceFileId\":\"2116\", 	\"recommendationId\":\"\", 	\"recommendationNumber\":\"\", 	\"statusId\":\"\", 	\"personId\":\"24347\", 	\"caringTypeId\":\"2\", 	\"comments\":\"testing 123 123\", 	\"rejectionReason\":\"\", 	\"disbursementRequired\":\"\", 	\"disbursementListTypeId\":\"\", 	\"amount\":\"\", 	\"isPeriodic\":\"\", 	\"startDate\":\"\", 	\"endDate\":\"\", 	\"durationValue\":\"\", 	\"durationFreq\":\"\", 	\"period\":\"\", 	\"disburseSrc\":\"\" }";
    	String json = "{\"amount\":\"200\",\"caringTypeId\":\"1\",\"comments\":\"test recommendation\",\"disburseSrc\":\"\",\"disbursementListTypeId\":\"4\",\"disbursementRequired\":\"0\",\"durationFreq\":\"\",\"durationValue\":\"\",\"endDate\":\"\",\"inheritanceFileId\":\"3211\",\"isPeriodic\":\"\",\"loginId\":\"malijtabi\",\"period\":\"92001\",\"personId\":\"25198\",\"recommendationId\":\"10152\",\"recommendationNumber\":\"RECOM-16-0007844\",\"rejectionReason\":\"pp\",\"socialResearchId\":\"-1\",\"startDate\":\"\"}";
		Gson jsonObj = new GsonBuilder().create();
		PersistRecommendationsInputParameters input = jsonObj.fromJson(json,PersistRecommendationsInputParameters.class);
		
		SocialResearchService.persistRecommendation(input);
		System.out.println(jsonObj.toJson( input));
	}
		
    private void testPersistPropertyMaintenance() throws Exception 
	{
		
//		System.out.println(json );
		String json = "{\"requestSource\":\"1\",\"loginId\":\"tamerhal\",\"applicantId\":1409,\"requestId\":-1,\"contractId\":29801,\"unitId\":1706,\"propertyId\":349,\"complaintId\":-1,\"maintenanceWorkTypeId\":\"5\",	" +
				"\"maintenanceDetails\":\"Test maintenance from WS\"}";
		Gson jsonObj = new GsonBuilder().serializeNulls().create();
		RequestService rs = new RequestService();
		PersistPropertyMaintenanceInputParameters input = jsonObj.fromJson(json,PersistPropertyMaintenanceInputParameters.class);
		RequestView rv = rs.persistPropertyMaintenanceRequest(input);
		System.out.println(rv.getRequestNumber());
	}
    private void testpersistBounceChequeRequest() throws Exception
    {
    	
//    	String resposne = RequestService.getTransferContractRequestDetails("pims_admin", 47011l, -1l, "en");
//		System.out.println(json );
		String json = "{\"requestSource\":\"1\",\"createdBy\":\"tamerhal\",\"createdByPersonId\":6538,\"requestId\":-1,\"contractId\":31760," +
					   "\"paymentSchduleIds\":\"144188\","+
						"\"description\":\"Test replace cheque from WS\" "+
						
						"}";
		Gson jsonObj = new GsonBuilder().serializeNulls().create();
		RequestService rs = new RequestService();
		ReplaceChequeInputParameters input = jsonObj.fromJson(json,ReplaceChequeInputParameters.class);
		RequestView rv = rs.persistReplaceChequeRequest(input,Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
		System.out.println(rv.getRequestNumber());
    	
    }
    private void testPersistReplaceChequeRequest() throws Exception
    {
    	
//    	String resposne = RequestService.getTransferContractRequestDetails("pims_admin", 47011l, -1l, "en");
//		System.out.println(json );
		String json = "{\"requestSource\":\"1\",\"createdBy\":\"tamerhal\",\"createdByPersonId\":6538,\"requestId\":-1,\"contractId\":31760," +
					   "\"paymentSchduleIds\":\"144188\","+
						"\"description\":\"Test replace cheque from WS\" "+
						
						"}";
		Gson jsonObj = new GsonBuilder().serializeNulls().create();
		RequestService rs = new RequestService();
		ReplaceChequeInputParameters input = jsonObj.fromJson(json,ReplaceChequeInputParameters.class);
		RequestView rv = rs.persistReplaceChequeRequest(input,Constant.REQUEST_TYPE_REPLACE_CHEQUE);
		System.out.println(rv.getRequestNumber());
    	
    }
    private void testPersistTransfer() throws Exception 
	{
		
    	String resposne = RequestService.getTransferContractRequestDetails("pims_admin", 47011l, -1l, "en");
//		System.out.println(json );
		String json = "{\"requestSource\":\"1\",\"createdBy\":\"tamerhal\",\"createdByPersonId\":1409,\"requestId\":64911,\"contractId\":31762," +
						"\"evacuationDate\":\"19/10/2039\",	" +
						"\"description\":\"Test transfer from WS\", "+
						"\"newTenant\":{ " +
						" \"isCompany\":\"0\",\"emiratesId\":\"EmiID\",\"name\":\"Test transfer\",\"passportNo\":\"BN8968942\",\"dateOfBirth\":\"23/09/1984\"," +
						" \"mobileNo\":\"0551364549\",\"nationalityId\":\"19\"" +
						"}}";
		Gson jsonObj = new GsonBuilder().serializeNulls().create();
		RequestService rs = new RequestService();
		TransferRequestInputParameters input = jsonObj.fromJson(json,TransferRequestInputParameters.class);
		RequestView rv = rs.persistTransferContractRequest(input);
		System.out.println(rv.getRequestNumber());
	}
    
	private void testPersistMinorMaintenance() throws Exception 
	{
		
//		String json=SocialResearchService.getMinorMaintenanceList("malijtabi","2116","10","1","-1","-1","-1","-1");
//		
//		System.out.println(json );
		String json = "{\"loginId\":\"malijtabi\",\"socialResearchId\":\"-1\",\"requestId\":\"-1\",\"inheritanceFileId\":\"2116\",\"assetMemsId\":\"17422\",\"maintenanceTypeId\":\"51\",\"maintenanceWorkTypeId\":\"5\",\"paymentSrc\":\"167001\",\"maintenanceDetails\":\"Test maintenance from WS\"}";
		Gson jsonObj = new GsonBuilder().create();
		PersistMinorMaintenanceInputParameters input = jsonObj.fromJson(json,PersistMinorMaintenanceInputParameters.class);
		
		SocialResearchService.persistMinorMaintenanceRequest(input,183001L);
		System.out.println(jsonObj.toJson( input));
	}
	
	private void testPersistLocation() throws Exception 
	{
		String json = "{\"createdBy\":\"pims_admin\",\"socialResearchId\":\"147\",\"inheritanceFileId\":\"79\",\"copyToPersonIds\":[5177,5167,5165],\"beneficiaryId\":\"5175\",\"title\":\"Test from code\",\"longitude\":\"50.0004\",\"latitude\":\"25.0004\" }";
		Gson jsonObj = new GsonBuilder().create();
		Locations input = jsonObj.fromJson(json,Locations.class);
		
		LocationsService.persistLocation(input);
		System.out.println(jsonObj.toJson( input));
	}
	private void testNormalDisbursementRequest() throws Exception {
		String json = "{\"createdBy\":'pims_admin',\"createdByPersonId\":3870,\"requestSource\":\"1\", \"disbursementSource\":167001,\"reasonSubType\":-1,\"amount\":500,\"reasonSuperType\":0,\"reason\":1,\"description\":\"testing for  mobile\",\"inheritanceBeneficiariesId\":11681,\"inheritanceFileId\":1368 }";
		Gson jsonObj = new GsonBuilder().create();
		NormalDisbursementInputParameter input = jsonObj.fromJson(json,
				NormalDisbursementInputParameter.class);
		//        
		// NormalDisbursementInputParameter input = new
		// NormalDisbursementInputParameter();
		// input.setAmount(900d);
		// input.setCreatedBy("pims_admin");
		// input.setCreatedByPersonId(3870l);
		// input.setDisbursementSource(167001l);
		// input.setDescription("testing mobile disbursement");
		// // input.setInheritanceBeneficiariesId("11681");
		// input.setInheritanceFileId(1368l);
		// input.setReason(1l);
		// input.setRequestSource("1");
		RequestService rs = new RequestService();
		RequestView requestView = rs.persistNormalDisbursementRequest(input);
		System.out.println(requestView.getRequestNumber());
	}

	/**
	 * 
	 */
	private void testGetAssetDetailsByFileId() {
		// Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
		// searchCriteriaMap.put("loginId", "pims_admin");
		// searchCriteriaMap.put("fileId", "76");
		// List<AssetDetailsForMobile> list =
		// InheritanceFileService.getAssetDetailByFileId(
		// searchCriteriaMap,
		// null ,
		// null ,
		// null,
		// false
		// );
	}

	public String makeUpper(String s) {
		return s.toUpperCase();
	}

	public int makeSquare(int num) {
		return num * num;
	}

	@SuppressWarnings("unchecked")
	public List<StatsUndistributedPaymentsView> getUndistributedChequeStats() {
		errorMessages = new ArrayList<String>();
		List<StatsUndistributedPaymentsView> list = new ArrayList<StatsUndistributedPaymentsView>(
				0);
		try {
			DecimalFormat oneDForm = new DecimalFormat("#.00");
			Session session = (Session) EntityManager.getBroker().getSession();
			List<Object> objList = (ArrayList<Object>) session.createSQLQuery(
					"Select * from GetUndistributedChequeSummary ").list();
			int count = 0;
			for (Object object : objList) {
				Object[] row = (Object[]) object;
				StatsUndistributedPaymentsView view = new StatsUndistributedPaymentsView();

				view.setRowNum(++count);
				if (row[0] != null) {
					view.setFileNumber((String) row[0]);
				}
				if (row[1] != null) {
					view.setInheritanceFileId(Long.parseLong("" + row[1]));
				}

				view
						.setAmount(oneDForm.format(Double.parseDouble(""
								+ row[2])));
				view.setTotalPayments(Long.parseLong("" + row[3]));
				list.add(view);
			}
		} catch (Exception e) {
			logger
					.LogException("getUndistributedChequeStats -Error Occured",
							e);
			errorMessages
					.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	public String onProfitClicked() {
		try {
			StatsUndistributedPaymentsView view = (StatsUndistributedPaymentsView) dataTableUndistributedProfits
					.getRowData();
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, view
					.getInheritanceFileId());
			sessionMap.put("DISTRIBUTION_TAB", "DISTRIBUTION_TAB");

		} catch (Exception e) {
			logger.LogException("onProfitClicked -Error Occured", e);
		}
		return "inheritanceFile";

	}

	@SuppressWarnings("unchecked")
	public List<StatsUndistributedPaymentsView> getUndistributedProfitStats() {
		errorMessages = new ArrayList<String>();
		List<StatsUndistributedPaymentsView> list = new ArrayList<StatsUndistributedPaymentsView>(
				0);
		try {
			DecimalFormat oneDForm = new DecimalFormat("#.00");
			Session session = (Session) EntityManager.getBroker().getSession();
			List<Object> objList = (ArrayList<Object>) session.createSQLQuery(
					"Select * from GETUNDISTRIBUTEDPROFITSUMMARY ").list();
			int count = 0;
			for (Object object : objList) {
				// "File_Number", payment_Schedule_id,"file_id", "AMOUNT",
				// "Description"
				Object[] row = (Object[]) object;
				StatsUndistributedPaymentsView view = new StatsUndistributedPaymentsView();

				view.setRowNum(++count);
				if (row[0] != null) {
					view.setFileNumber((String) row[0]);
				}
				if (row[2] != null) {
					view.setInheritanceFileId(Long.parseLong("" + row[2]));
				}

				view
						.setAmount(oneDForm.format(Double.parseDouble(""
								+ row[3])));
				view.setDescription((String) row[4]);
				list.add(view);
			}
		} catch (Exception e) {
			logger
					.LogException("getUndistributedProfitStats -Error Occured",
							e);
			errorMessages
					.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	public String onRequestClicked() {
		errorMessages = new ArrayList<String>();
		try {
			StatsUndistributedPaymentsView view = (StatsUndistributedPaymentsView) dataTableUndistributedNonCheques
					.getRowData();

			RequestService rs = new RequestService();
			RequestView requestView = rs.getRequestById(view.getRequestId());
			FacesContext.getCurrentInstance().getExternalContext()
					.getRequestMap()
					.put(WebConstants.REQUEST_VIEW, requestView);
			if (claimTask(requestView)) {
				return "collectionRequest";
			}
		} catch (Exception e) {
			logger.LogException("onRequestClicked -Error Occured", e);
			errorMessages
					.add(getBundleMessage(WebConstants.PropertyKeys.Request.NO_TASK_FOUND));
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public boolean claimTask(RequestView reqView) throws Exception,
			PIMSWorkListException {
		boolean hasClaimed = true;
		try {

			RequestTasksView reqTaskView = new RequestServiceAgent()
					.getIncompleteRequestTask(reqView.getRequestId());
			String taskId = reqTaskView.getTaskId();
			String user = getLoggedInUser();
			if (taskId != null) {
				BPMWorklistClient bpmWorkListClient = null;
				String contextPath = ((ServletContext) getFacesContext()
						.getExternalContext().getContext())
						.getRealPath("\\WEB-INF\\config.properties");
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForWFAdmin(taskId);
				boolean hasPermission = true;
				try {
					if (userTask != null
							&& userTask.getTaskAttributes() != null
							&& userTask.getTaskAttributes().containsKey(
									"TASK_ASSIGNEE_GROUPS")) {
						hasPermission = false;
						String groups = userTask.getTaskAttributes().get(
								"TASK_ASSIGNEE_GROUPS");
						if (groups != null) {
							String[] taskGroups = groups.split(",");

							if (taskGroups != null && taskGroups.length > 0) {
								List<UserGroup> usereGroups = getLoggedInUserObj()
										.getUserGroups();

								if (usereGroups != null
										&& usereGroups.size() > 0) {
									for (UserGroup ug : usereGroups) {
										for (String taskGrp : taskGroups) {
											if (ug
													.getUserGroupId()
													.trim()
													.toLowerCase()
													.equals(
															taskGrp
																	.trim()
																	.toLowerCase())) {
												hasPermission = true;
												break;
											}
										}
									}
								}
							}
						}
					}
				} catch (Exception exp) {

					logger.LogException(
							"Failed to check task group level security", exp);
					// do nothing
				}
				if (!hasPermission) {
					hasClaimed = false;
					throw new PIMSWorkListException(
							ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK);
				}
				try {

					bpmWorkListClient.accquireTask(userTask, user);
					CommonUtil.updateRequestTaskAcquiredBy(userTask, CommonUtil
							.getLoggedInUser());
					reqView.setTaskAcquiredByNameAr(getLoggedInUserObj()
							.getSecondaryFullName());
					reqView.setTaskAcquiredByNameEn(getLoggedInUserObj()
							.getFullName());
				} catch (PIMSWorkListException ex) {
					hasClaimed = false;
					if (ex.getExceptionCode() == ExceptionCodes.TASK_ALREADY_ACQUIRED) {
						if (!ex.getUserIdAlreadyPerformedAction().toLowerCase()
								.equals(user.toLowerCase()))
							throw ex;
					}
				}

				getFacesContext().getExternalContext().getSessionMap().put(
						WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
			} else {
				hasClaimed = false;
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getParamBundleMessage(
						WebConstants.PropertyKeys.Request.NO_TASK_FOUND,
						reqView.getRequestNumber()));

			}
		} catch (PIMSWorkListException exp) {
			hasClaimed = false;
			errorMessages = new ArrayList<String>();

			if (exp.getExceptionCode() == ExceptionCodes.TASK_ALREADY_ACQUIRED) {
				String msg = java.text.MessageFormat
						.format(
								CommonUtil
										.getParamBundleMessage("TaskList.messages.claimTaskFailureAlreadyClaimed"),
								exp.getUserIdAlreadyPerformedAction());

				errorMessages.add(msg);
			} else if (exp.getExceptionCode() == ExceptionCodes.USER_NOT_AUTHENTIC) {
				logger.LogException("user not authentic", exp);
				errorMessages
						.add(CommonUtil
								.getBundleMessage("TaskList.messages.userNotAuthentic"));
			} else if (exp.getExceptionCode() == ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {

				logger.LogException("user not authorized", exp);
				errorMessages
						.add(CommonUtil
								.getBundleMessage("TaskList.messages.userNotAuthorize"));
			} else if (exp.getExceptionCode() == ExceptionCodes.FAILED_TO_GET_USER_TASKS) {
				logger.LogException("Failed to fetch user task", exp);
				errorMessages.add(CommonUtil
						.getBundleMessage("TaskList.messages.generalError"));
			}
		} catch (Exception exp) {
			hasClaimed = false;
			logger.LogException("Failed to fetch user task", exp);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil
					.getBundleMessage("TaskList.messages.generalError"));
		}

		return hasClaimed;
	}

	@SuppressWarnings("unchecked")
	public List<StatsUndistributedPaymentsView> getUndistributedNonChequeStats() {
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		List<StatsUndistributedPaymentsView> list = new ArrayList<StatsUndistributedPaymentsView>(
				0);
		try {
			Session session = (Session) EntityManager.getBroker().getSession();
			List<Object> objList = (ArrayList<Object>) session.createSQLQuery(
					"Select * from GETUNDISTRIBUTEDNONCHQSMRY").list();
			int count = 0;
			for (Object object : objList) {
				Object[] row = (Object[]) object;
				StatsUndistributedPaymentsView view = new StatsUndistributedPaymentsView();

				view.setRowNum(++count);
				view.setRequestId(Long.parseLong("" + row[0]));
				view.setCreatedOn((String) row[1]);
				view.setRequestNumber((String) row[2]);
				view.setRequestStatusAr((String) row[3]);
				view.setRequestStatusEn((String) row[4]);
				if (row[5] != null) {
					view.setFileNumber((String) row[5]);
				}
				if (row[6] != null) {
					view.setInheritanceFileId(Long.parseLong("" + row[6]));
				}
				view
						.setAmount(oneDForm.format(Double.parseDouble(""
								+ row[7])));
				view.setTotalPayments(Long.parseLong("" + row[8]));
				list.add(view);
			}
		} catch (Exception e) {
			logger.LogException(
					"getUndistributedNonChequeStats -Error Occured", e);
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	public String onFileClicked() {
		try {
			StatsUndistributedPaymentsView view = (StatsUndistributedPaymentsView) dataTableUndistributedCheques
					.getRowData();
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, view
					.getInheritanceFileId());
			sessionMap.put("DISTRIBUTION_TAB", "DISTRIBUTION_TAB");

		} catch (Exception e) {
			logger.LogException("onFileClicked -Error Occured", e);
		}
		return "inheritanceFile";

	}

	private void loadRentContractSection() {
		rentContractStats = new PropertyServiceAgent().getRentContractStats();
		viewRootMap.put(WebConstants.StatsScreen.RENTCONTRACT,
				rentContractStats);
	}

	private void loadOtherContractSection() {
		otherContractStats = new PropertyServiceAgent().getOtherContractStats();
		viewRootMap.put(WebConstants.StatsScreen.CONTRACT, otherContractStats);
	}

	private void loadProjectSection() {
		projectsStats = new ProjectServiceAgent().getProjectStats();
		viewRootMap.put(WebConstants.StatsScreen.PROJECT, projectsStats);
	}

	private void loadRequestSection() {
		requestStats = new RequestServiceAgent().getRequestOriginationStats();
		viewRootMap.put(WebConstants.StatsScreen.REQUEST, requestStats);
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}

	public String actionClick() {
		logger.logInfo("Stepped into the actionClick method");
		userTask = (UserTask) dataTable.getRowData();
		setRequestParam(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);

		String taskType = userTask.getTaskType();
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		getFacesContext().getExternalContext().getSessionMap().put(
				WebConstants.FROM_TASK_LIST, WebConstants.FROM_TASK_LIST);
		// userTask = (UserTask)
		// getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		setRequestParam(WebConstants.BACK_SCREEN,
				WebConstants.BACK_SCREEN_TASK_LIST);
		logger.logInfo("Task Type is:" + taskType);
		return taskType;
	}

	public String getRemaingTask() {

		String outPut = "";
		Integer taskCount = 0;
		// BPMWorklistClient bpmWorkListClient = null;
		// try {
		//			
		// String contextPath = ((ServletContext)
		// getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties")
		// ;
		// boolean isEnLocal = getIsEnglishLocale();
		// logger.logInfo("Contextpath is:"+contextPath);
		//	        
		// bpmWorkListClient = new BPMWorklistClient(contextPath);
		// taskCount= bpmWorkListClient.getBPMTasksCount(getLoggedInUser(),
		// null);
		//		
		// outPut =
		// CommonUtil.getParamBundleMessage("statsScreen.msg.pendingTask",
		// taskCount);
		//		
		// }catch (Exception e) {
		// // TODO: handle exception
		// }

		return outPut;
	}

	public String sendToTaskList() {

		return "StatsScreen_To_TaskList";
	}

	public String getBundleMessage(String key) {
		logger.logInfo("getBundleMessage(String) started...");
		String message = "";
		try {
			message = ResourceUtil.getInstance().getProperty(key);

			logger
					.logInfo("getBundleMessage(String) completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
		return message;
	}

	public void setBtnAction(HtmlCommandLink btnAction) {
		this.btnAction = btnAction;
	}

	public HtmlCommandLink getBtnAction() {
		return btnAction;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

	}

	/**
	 * @param paginatorMaxPages
	 *            the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows
	 *            the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize
	 *            the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	public Date getAssignedDateFrom() {
		return assignedDateFrom;
	}

	public void setAssignedDateFrom(Date assignedDateFrom) {
		this.assignedDateFrom = assignedDateFrom;
	}

	public Date getAssignedDateTo() {
		return assignedDateTo;
	}

	public void setAssignedDateTo(Date assignedDateTo) {
		this.assignedDateTo = assignedDateTo;
	}

	public String getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public HtmlColumn getHtmlColumnsAssignedDate() {
		return htmlColumnsAssignedDate;
	}

	public void setHtmlColumnsAssignedDate(HtmlColumn htmlColumnsAssignedDate) {
		this.htmlColumnsAssignedDate = htmlColumnsAssignedDate;
	}

	public List<ProjectStatsView> getProjectsStats() {
		if (viewRootMap.get(WebConstants.StatsScreen.PROJECT) != null) {
			projectsStats = (List<ProjectStatsView>) viewRootMap
					.get(WebConstants.StatsScreen.PROJECT);
		}
		return projectsStats;
	}

	public void setProjectsStats(List<ProjectStatsView> projectsStats) {
		this.projectsStats = projectsStats;
	}

	public List<ContractStatsView> getRentContractStats() {
		if (viewRootMap.get(WebConstants.StatsScreen.RENTCONTRACT) != null) {
			rentContractStats = (List<ContractStatsView>) viewRootMap
					.get(WebConstants.StatsScreen.RENTCONTRACT);
		}
		return rentContractStats;
	}

	public void setRentContractStats(List<ContractStatsView> rentContractStats) {
		this.rentContractStats = rentContractStats;
	}

	public List<ContractStatsView> getOtherContractStats() {
		if (viewRootMap.get(WebConstants.StatsScreen.CONTRACT) != null) {
			otherContractStats = (List<ContractStatsView>) viewRootMap
					.get(WebConstants.StatsScreen.CONTRACT);
		}

		return otherContractStats;
	}

	public void setOtherContractStats(List<ContractStatsView> otherContractStats) {
		this.otherContractStats = otherContractStats;
	}

	public List<RequestStatsView> getRequestStats() {
		if (viewRootMap.get(WebConstants.StatsScreen.REQUEST) != null) {
			requestStats = (List<RequestStatsView>) viewRootMap
					.get(WebConstants.StatsScreen.REQUEST);
		}
		return requestStats;
	}

	public void setRequestStats(List<RequestStatsView> requestStats) {
		this.requestStats = requestStats;
	}

	public HtmlDataTable getDataTableUndistributedCheques() {
		return dataTableUndistributedCheques;
	}

	public void setDataTableUndistributedCheques(
			HtmlDataTable dataTableUndistributedCheques) {
		this.dataTableUndistributedCheques = dataTableUndistributedCheques;
	}

	public HtmlDataTable getDataTableUndistributedNonCheques() {
		return dataTableUndistributedNonCheques;
	}

	public void setDataTableUndistributedNonCheques(
			HtmlDataTable dataTableUndistributedNonCheques) {
		this.dataTableUndistributedNonCheques = dataTableUndistributedNonCheques;
	}

	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) || (errorMessages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<UL>";
			for (String message : errorMessages) {
				messageList = messageList + "<LI>" + message;
				;
			}
			messageList = messageList + "</UL>";
		}

		return (messageList);

	}

}
