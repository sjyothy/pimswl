/*package com.avanza.pims.web.backingbeans;
import com.avanza.pims.soa.bpm.worklist.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.Permission;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupPermissionBinding;
import com.avanza.core.security.db.GroupPermissionId;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.controller.AbstractController;

import com.avanza.pims.web.util.JSFUtil;
import com.sun.faces.taglib.html_basic.CommandButtonTag;
import com.sun.faces.taglib.html_basic.CommandLinkTag;


public class TaskListBackingBean extends AbstractController{

	// Fields
	private HtmlDataTable dataTable;

	private TaskView dataItem = new TaskView();

	private List<TaskView> dataList = new ArrayList<TaskView>();

	private String sortField = null;

	private boolean sortAscending = true;

	private CommandLinkTag btnAction = new CommandLinkTag();
	private HtmlInputHidden addCount;

	// Actions -----------------------------------------------------------------------------------
	public List<TaskView> getTaskDataList() {
		if (dataList == null) {
			loadDataList(); // Preload by lazy loading.
		}
		return dataList;
	}


	private void loadDataList() {

		com.avanza.pims.soa.bpm.worklist.BPMWorklistClient bpmWorkListClient = null;;
		try {
			
	        String contextPath = this.getFacesContext().getExternalContext().getRequestContextPath();
	        contextPath = new String("c:\\config.properties");
	        System.out.println(contextPath);
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			Set<UserTask> userTaskList = bpmWorkListClient.getTasksForUser("hammad");
			
			Iterator it = userTaskList.iterator();
			while (it.hasNext()) {
				UserTask userTask = (UserTask)it.next();
				TaskView taskView = new TaskView();
				taskView.setTaskId(userTask.getTaskId());
				taskView.setExpiryDate(userTask.getExpiryDate().toString());
				taskView.setSubject(userTask.getTaskSubject_en());
				taskView.setTaskName(userTask.getTaskTitle_en());
				taskView.setWorkFlowName(userTask.getBpelName_en());
				taskView.setUrl("");
				taskView.setTaskType(userTask.getTaskType());
				dataList.add(taskView);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");

		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}

		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}

	// Getters -----------------------------------------------------------------------------------

	public String getSortField() {
		return sortField;
	}

	public boolean getSortAscending() {
		return sortAscending;
	}

	// Setters -----------------------------------------------------------------------------------

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	// Helpers -----------------------------------------------------------------------------------

	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}

	// Constructors

	*//** default constructor *//*
	public TaskListBackingBean() {
		//addCount.setValue(0);
	}


	// Property accessors


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	// Setters -----------------------------------------------------------------------------------

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public TaskView getDataItem() {
		return dataItem;
	}

	public void setDataItem(TaskView dataItem) {
		this.dataItem = dataItem;
	}

	//Public Methods

	// Actions -----------------------------------------------------------------------------------


	//Classes which should be implemented in Business Logic or from where we r fetching data	    

	public String reset() {

		return "success";
	}

	public void pageFirst() {
		dataTable.setFirst(0);
	}

	public void pagePrevious() {
		dataTable.setFirst(dataTable.getFirst() - dataTable.getRows());
	}

	public void pageNext() {
		dataTable.setFirst(dataTable.getFirst() + dataTable.getRows());
	}

	public int getCurrentPage() {
		int rows = dataTable.getRows();
		int first = dataTable.getFirst();
		int count = dataTable.getRowCount();
		return (count / rows) - ((count - first) / rows) + 1;
	}

	public int getTotalPages() {
		int rows = dataTable.getRows();
		int count = dataTable.getRowCount();
		return (count / rows) + ((count % rows != 0) ? 1 : 0);
	}

	public void pageLast() {
		int count = dataTable.getRowCount();
		int rows = dataTable.getRows();
		dataTable.setFirst(count - ((count % rows != 0) ? count % rows : rows));
	}

	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	public int getDataCount() {
		int rows = dataTable.getRows();
		int count = dataTable.getRowCount();
		return (count / rows) + ((count % rows != 0) ? 1 : 0);
	}        


	@Override
	public void init() {
		// TODO Auto-generated method stub
		loadDataList();

		super.init();
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
	}
	public String actionClick(){
		return "taskOne";
	}
	
	
	public void setBtnAction(CommandLinkTag btnAction) {
		this.btnAction = btnAction;
	}


	public CommandLinkTag getBtnAction() {
		return btnAction;
	}

}*/