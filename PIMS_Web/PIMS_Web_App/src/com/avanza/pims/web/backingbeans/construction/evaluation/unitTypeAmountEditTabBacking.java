package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.UnitTypeDescView;


public class unitTypeAmountEditTabBacking extends AbstractController
{
	private static final long serialVersionUID = 4042644104878615960L;
	@SuppressWarnings("unused")
	private transient Logger logger = Logger.getLogger(unitTypeAmountEditTabBacking.class);
	@SuppressWarnings("unchecked")
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	@SuppressWarnings("unused")
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	List<SelectItem> unitTypeItems = new ArrayList<SelectItem>();
	private String areaFrom;
	private String areaTo;
	private String newAmountType;
	private List<DomainDataView> unitTypeList =new ArrayList<DomainDataView>(0);
	private List<UnitTypeDescView> unitTypeDescList = new ArrayList<UnitTypeDescView>();
	private HtmlDataTable unitTypeTable;
	private UnitTypeDescView unitTypeDescView = new UnitTypeDescView();
	private HtmlSelectOneMenu unitType = new HtmlSelectOneMenu();
	private HashMap<String , String> addedUnitMap = new HashMap<String, String>();
	private DomainDataView selectedUnitType = new DomainDataView();
	private boolean isShowApplyButton;
	private HashMap<Double, Double> unitAreaMap = new HashMap<Double, Double>();
	
	@SuppressWarnings("unchecked")
	public void init() 
	{
		if(!isPostBack())
		{
			unitTypeList=CommonUtil.getDomainDataListForDomainType(WebConstants.UNIT_TYPE);
			viewMap.put("UNIT_TYPE_LIST",unitTypeList);
		}
	}
	public Integer getPaginatorMaxPages() 
	{
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages)
	{
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public Integer getPaginatorRows() 
	{
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	public void setPaginatorRows(Integer paginatorRows) 
	{
		this.paginatorRows = paginatorRows;
	}
	public Integer getRecordSize() 
	{
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	public void setRecordSize(Integer recordSize) 
	{
		this.recordSize = recordSize;
	}
	public HtmlSelectOneMenu getUnitType() {
		return unitType;
	}
	public void setUnitType(HtmlSelectOneMenu unitType) {
		this.unitType = unitType;
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getUnitTypeItems()
	{
		List<SelectItem> items = new ArrayList<SelectItem>();
		if(viewMap.get("UNIT_TYPE_VIEW_ITEMS") != null)
			items = (List<SelectItem>) viewMap.get("UNIT_TYPE_VIEW_ITEMS");
		return items;
	}
	public void setUnitTypeItems(List<SelectItem> unitTypeItems) {
		this.unitTypeItems = unitTypeItems;
	}
	public String getAreaFrom() {
		return areaFrom;
	}
	public void setAreaFrom(String areaFrom) {
		this.areaFrom = areaFrom;
	}
	public String getAreaTo() {
		return areaTo;
	}
	public void setAreaTo(String areaTo) {
		this.areaTo = areaTo;
	}
	public String getNewAmountType() {
		return newAmountType;
	}
	public void setNewAmountType(String newAmountType) {
		this.newAmountType = newAmountType;
	}
	@SuppressWarnings({ "unchecked", "unchecked" })
	public List<DomainDataView> getUnitTypeList() {
		return unitTypeList;
	}
	public void setUnitTypeList(List<DomainDataView> unitTypeList) {
		this.unitTypeList = unitTypeList;
	}
	@SuppressWarnings("unchecked")
	public void addUnitTypeToGrid()
	{
		if(viewMap.get("UNIT_TYPE_AMOUNT_LIST") != null)
			unitTypeDescList = (List<UnitTypeDescView>) viewMap.get("UNIT_TYPE_AMOUNT_LIST");
		if(viewMap.get("UNIT_TYPE_LIST") != null)
			unitTypeList =   (List<DomainDataView>) viewMap.get("UNIT_TYPE_LIST");
		DomainDataView unitTypeDDV = new DomainDataView();
		
		if(viewMap.get("CURRENT_GRID_UPDATED_CLEAR_IT") != null && (Boolean)viewMap.get("CURRENT_GRID_UPDATED_CLEAR_IT"))
			{
				viewMap.put("SHOW_CURRENT_GRID", true);
				return;
			}
		if( !hasErrors() )
		{
			for(DomainDataView unitTypeDD : unitTypeList)
			{
				if(CommonUtil.getIsEnglishLocale() && unitTypeDD.getDataDescEn().compareTo(unitType.getValue().toString()) == 0)
				{
					selectedUnitType  =  unitTypeDD;
					unitTypeDDV = unitTypeDD;
					break ;
				}
				else if(CommonUtil.getIsArabicLocale() && unitTypeDD.getDataDescAr().compareTo(unitType.getValue().toString()) == 0)
				{
					selectedUnitType  =  unitTypeDD;
					unitTypeDDV = unitTypeDD;
					break ;
				}
					
			}
			
			unitTypeDescView.setTypeAmount(newAmountType.trim());
			unitTypeDescView.setUnitTypeId(unitTypeDDV.getDomainDataId());
			unitTypeDescView.setDescEn(unitTypeDDV.getDataDescEn());
			unitTypeDescView.setDescAr(unitTypeDDV.getDataDescAr());
			
			if(areaFrom != null && areaFrom.trim().length() > 0)
				unitTypeDescView.setAreaFrom(Double.parseDouble(areaFrom.trim()));
			else
				unitTypeDescView.setAreaFrom(0D);
			
			if(areaTo != null && areaTo.trim().length() > 0)
				unitTypeDescView.setAreaTo(Double.parseDouble(areaTo.trim()));
			else
				unitTypeDescView.setAreaTo(0D);
			
			unitTypeDescList.add(unitTypeDescView);
			viewMap.put("recordSize", unitTypeDescList.size());
			viewMap.put("UNIT_TYPE_AMOUNT_LIST", unitTypeDescList);
			viewMap.put("CURRENT_GRID_UPDATED_CLEAR_IT",false);
			viewMap.put("SHOW_CURRENT_GRID", false);
			
			if(viewMap.get("SUCCESS_MSG_RECORDS_UPDATED") != null)
				viewMap.remove("SUCCESS_MSG_RECORDS_UPDATED");
			setShowApplyButton(true);
			clearFields();
		}
	}
	private void clearFields() 
	{
		unitType.setValue("-1");
		areaFrom ="";
		areaTo = "";
		newAmountType = "";
	}
	@SuppressWarnings("unchecked")
	public List<UnitTypeDescView> getUnitTypeDescList()
	{
		if(viewMap.get("UNIT_TYPE_AMOUNT_LIST") != null )
			unitTypeDescList = (List<UnitTypeDescView>) viewMap.get("UNIT_TYPE_AMOUNT_LIST");
		return unitTypeDescList;
	}
	public void setUnitTypeDescList(List<UnitTypeDescView> unitTypeDescList) {
		this.unitTypeDescList = unitTypeDescList;
	}
	public HtmlDataTable getUnitTypeTable() {
		return unitTypeTable;
	}
	public void setUnitTypeTable(HtmlDataTable unitTypeTable) {
		this.unitTypeTable = unitTypeTable;
	}
	public UnitTypeDescView getUnitTypeDescView() {
		return unitTypeDescView;
	}
	public void setUnitTypeDescView(UnitTypeDescView unitTypeDescView) {
		this.unitTypeDescView = unitTypeDescView;
	}
	@SuppressWarnings("unchecked")
	public boolean hasErrors()
	{
		boolean hasError = false;
		
		if(unitType.getValue() != null && unitType.getValue().toString().compareTo("-1") == 0)
		{
			hasError = true;
			viewMap.put("ERROR_MSG_SELECT_UNIT_TYPE", true);
			return hasError;
		}
		else
			viewMap.put("ERROR_MSG_SELECT_UNIT_TYPE", false);
		
		if(areaFrom != null && areaFrom.trim().length() > 0)
		{
			try
			{
				Double.parseDouble(areaFrom);
				viewMap.put("ERROR_MSG_PROVIDE_VALID_FORM_AREA", false);
			}
			catch (Exception e)
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_VALID_FORM_AREA", true);
				return hasError;
			}
			
			if(Double.parseDouble(areaFrom) <= 0)
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_VALID_FORM_AREA", true);
				return hasError;
			}
			else
				viewMap.put("ERROR_MSG_PROVIDE_VALID_FORM_AREA", false);
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_VALID_FORM_AREA", false);
		
		if(areaTo != null && areaTo.trim().length() > 0)
		{
			try
			{
				Double.parseDouble(areaTo);
				viewMap.put("ERROR_MSG_PROVIDE_VALID_TO_AREA", false);
			}
			catch (Exception e)
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_VALID_TO_AREA", true);
				return hasError;
			}
			
			if(Double.parseDouble(areaTo) <= 0)
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_VALID_TO_AREA", true);
				return hasError;
			}
			else
				viewMap.put("ERROR_MSG_PROVIDE_VALID_TO_AREA", false);
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_VALID_TO_AREA", false);
		
		if(areaFrom != null && areaFrom.trim().length() > 0 && areaTo != null && areaTo.trim().length() > 0 )
		{
			if(Double.parseDouble(areaFrom) > Double.parseDouble(areaTo))
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT", true);
				return hasError;
			}
			else
				viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT", false);
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AREA_LIMIT", false);
		
		if(newAmountType == null)
		{
			hasError = true;
			viewMap.put("ERROR_MSG_PROVIDE_AMOUNT", true);
			return hasError;
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_AMOUNT", false);
		
		if(newAmountType != null && newAmountType.trim().length() <= 0)
		{
			hasError = true;
			viewMap.put("ERROR_MSG_PROVIDE_AMOUNT", true);
			return hasError;
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_AMOUNT", false);
		
		if(newAmountType != null )
		{
			try
			{
			 double amount = Double.parseDouble(newAmountType.trim());
			 if(amount <= 0)
			 {
				 hasError = true;
				 viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AMOUNT", true);
				 return hasError; 
			 }
			 else
				 viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AMOUNT", false);
				 
			}
			catch (NumberFormatException e) 
			{
				hasError = true;
				viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AMOUNT", true);
				return hasError;
			}
		}
		else
			viewMap.put("ERROR_MSG_PROVIDE_CORRECT_AMOUNT", false);
		
		if(viewMap.get("UNIT_TYPE_AMOUNT_LIST") != null)
		{
			unitTypeDescList = (List<UnitTypeDescView>) viewMap.get("UNIT_TYPE_AMOUNT_LIST");
			if(unitTypeDescList != null && unitTypeDescList.size() > 0)
			{
				
				for(UnitTypeDescView unit : unitTypeDescList)
				{
					
					if(unitType.getValue().equals(unit.getDescEn().toString()) || unitType.getValue().equals(unit.getDescAr().toString()))
					{
						double presentAreaFrom = unit.getAreaFrom(); 
						double presentAreaTo = unit.getAreaTo();
						
						//if both area fields present
						if(areaFrom != null && areaFrom.trim().length() > 0 && areaTo!= null && areaTo.trim().length() > 0)
						{
							double newAreaTo = Double.parseDouble(areaTo);
							double newAreaFrom = Double.parseDouble(areaFrom);
							
							//if toArea is not available in grid records
							if(presentAreaTo == 0)
							{
								if(  newAreaFrom >= presentAreaFrom  || newAreaTo >= presentAreaFrom)
								{
									hasError = true;
									viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
									break;
								}
							}
							
							//if fromArea is not available in grid records
							else if(presentAreaFrom == 0)
							{
								if(  newAreaTo <= presentAreaTo || newAreaFrom <= presentAreaTo )
								{
									hasError = true;
									viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
									break;
								}
							}
							
							//if presentAreaFrom and presentAreaTo are not null
							
						if(	isInRange(newAreaFrom, presentAreaFrom, presentAreaTo) || 
							isInRange(newAreaTo, presentAreaFrom, presentAreaTo) || 
							isInRange(presentAreaFrom, newAreaFrom, newAreaTo)||
							isInRange(presentAreaTo, newAreaFrom, newAreaTo))
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							
							else if( presentAreaFrom == presentAreaTo  )
							{
							 if( newAreaFrom >= presentAreaFrom && newAreaTo <= presentAreaTo )
								{
									hasError = true;
									viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
									break;
								}
							}
							else if( presentAreaFrom == presentAreaTo  )
							{
							 if( newAreaFrom >= presentAreaFrom && newAreaTo <= presentAreaTo )
								{
									hasError = true;
									viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
									break;
								}
							}
							
						}
						
						//this is use if one of the area field is null
						else 
						if( ( areaFrom != null && areaFrom.trim().length() > 0 ) || (areaTo != null && areaTo.trim().length() > 0 ))
						{
							double newAreaTo = 0;
							double newAreaFrom = 0;
							
							if( areaFrom.length()>0 )
								newAreaFrom = Double.parseDouble(areaFrom);
							else
								newAreaTo = Double.parseDouble(areaTo);
							
							//presentAreaTo Start if only areaFrom present
							if(presentAreaTo == 0 && areaFrom.length() > 0 && ( newAreaFrom <= presentAreaFrom || newAreaFrom >= presentAreaFrom) ) 
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							//presentAreaTo End
							
							//presentAreaTo Start if only areaTo present
							else 
							if(presentAreaTo == 0 && areaTo.length() > 0 && newAreaTo >= presentAreaFrom  ) 
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							//presentAreaTo End
							
							//presentAreaFrom Start if only areaFrom present
							if(presentAreaFrom == 0 && areaFrom.length() > 0 && newAreaFrom <= presentAreaTo ) 
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							//presentAreaFrom End
							
							//presentAreaFrom Start if only areaTo present
							else 
							if(presentAreaFrom == 0 && areaTo.length() > 0) 
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							//presentAreaFrom End
							
							//if both presentAreaFrom and To are not null
							else if(areaFrom.length() > 0 && newAreaFrom <= presentAreaTo)
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							else if(areaTo.length() > 0 && newAreaTo >= presentAreaFrom)
							{
								hasError = true;
								viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
								break;
							}
							
						}
						else if(presentAreaFrom == 0 && presentAreaTo == 0)
						{
							hasError = true;
							viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
							break;
						}
						else if(presentAreaFrom != 0 && presentAreaTo != 0)
						{
							hasError = true;
							viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
							break;
						}
						else if(presentAreaFrom != 0 )
						{
							hasError = true;
							viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
							break;
						}
						else if(presentAreaTo != 0 )
						{
							hasError = true;
							viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", true);
							break;
						}
						else
							viewMap.put("AREA_OF_THIS_RANGE_ALREADY_EXIST", false);	
					}
				}
				return hasError;
			}
		}
		
		return hasError;
	}
	public HashMap<String, String> getAddedUnitMap() {
		return addedUnitMap;
	}
	public void setAddedUnitMap(HashMap<String, String> addedUnitMap) {
		this.addedUnitMap = addedUnitMap;
	}
	public DomainDataView getSelectedUnitType() {
		return selectedUnitType;
	}
	public void setSelectedUnitType(DomainDataView selectedUnitType) {
		this.selectedUnitType = selectedUnitType;
	}
	@SuppressWarnings({ "unchecked", "unchecked" })
	public void saveAmountWithTypeAndArea()
	{
		try
		{
			//this propertyId is used to modify the unit rent amount
			long propertyId = (Long) viewMap.get("PROPERTY_ID_FOR_FETCHING_UNIT_TYPE");
			int recordsUpdated=0;
			List<UnitTypeDescView> returnedList = new ArrayList<UnitTypeDescView>();
			
			if(viewMap.get("UNIT_TYPE_AMOUNT_LIST") != null )
				unitTypeDescList = (List<UnitTypeDescView>) viewMap.get("UNIT_TYPE_AMOUNT_LIST");
			
			if(unitTypeDescList != null && unitTypeDescList.size() > 0)
				{
					returnedList = new PropertyService().saveAmountWithTypeAndArea(unitTypeDescList,propertyId, getLoggedInUserId());
					viewMap.put("ERROR_MSG_PROVIDE_UNIT_TYPE_AND_AMOUNT", false);
				}
			else
				viewMap.put("ERROR_MSG_PROVIDE_UNIT_TYPE_AND_AMOUNT", true);
			
			if(returnedList != null && returnedList.size() > 0)
			{
				HashMap<Double, Double> areaFromAndToMap = new HashMap<Double, Double>();
				HashSet<Long> unitTypeIdSet = new HashSet<Long>(); 
				for(UnitTypeDescView unit : returnedList)
					{
						if(unitTypeIdSet.contains(unit.getUnitTypeId()) && areaFromAndToMap.containsKey(unit.getAreaFrom()) && areaFromAndToMap.get(unit.getAreaFrom()) != null)
						{
							Double areaTo= areaFromAndToMap.get(unit.getAreaFrom());
							if(unit.getAreaTo().compareTo(areaTo)!= 0)
							{
								if(unit.getNoOfUnits() > 0)
								{
									recordsUpdated += unit.getNoOfUnits();
									areaFromAndToMap.put(unit.getAreaFrom(), unit.getAreaTo());
									saveMessageForSystemNotes(unit, propertyId);
								}
							}
						}
						else
						{
							if(unit.getNoOfUnits() > 0)
							{
								recordsUpdated += unit.getNoOfUnits();
								areaFromAndToMap.put(unit.getAreaFrom(), unit.getAreaTo());
								saveMessageForSystemNotes(unit, propertyId);
								unitTypeIdSet.add(unit.getUnitTypeId());
							}	
						}	
					}
			}
			viewMap.put("SUCCESS_MSG_RECORDS_UPDATED", recordsUpdated);
			viewMap.put("CURRENT_GRID_UPDATED_CLEAR_IT", true);
			setShowApplyButton(false);
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException("saveAmountWithTypeAndArea() crashed", e);
		}
	}
	private void saveMessageForSystemNotes(UnitTypeDescView unit, Long propertyId)
	{
		  Locale localeAr = new Locale( "ar" );
		  Locale localeEn = new Locale( "en" );
		  try{
		  ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
		  ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
		  String sysMsgEn = java.text.MessageFormat.format( bundleEn.getString( "receiveProperty.systemNotes.rentUpdated" ),
				  											unit.getDescEn(),
				  											unit.getAreaFrom(),
				  											unit.getAreaTo(),
				  											unit.getTypeAmount()
				                                            ); 	
		  String sysMsgAr = java.text.MessageFormat.format( bundleAr.getString( "receiveProperty.systemNotes.rentUpdated" ),
														  	unit.getDescAr(),
															unit.getAreaFrom(),
															unit.getAreaTo(),
															unit.getTypeAmount()
							                                );
		  NotesController.saveSystemNotesForRequest( WebConstants.NOTES_OWNER_PROPERTY , sysMsgEn,sysMsgAr, propertyId );
		  }
		  catch (PimsBusinessException  e) 
		  {
			  logger.LogException("saveMessageForSystemNotes crashed", e);
		  }
	}
	public boolean getEnglishLocale() {

		 
		boolean isEnglishLocale = false; 
		 WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		 LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		 isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		 return isEnglishLocale;
	 }
	@SuppressWarnings("unchecked")
	public void deleteFromGrid()
	{
		if(viewMap.get("UNIT_TYPE_AMOUNT_LIST") != null )
			unitTypeDescList = (List<UnitTypeDescView>) viewMap.get("UNIT_TYPE_AMOUNT_LIST");
		
		UnitTypeDescView unitToBeDeleted = (UnitTypeDescView) unitTypeTable.getRowData();
		unitTypeDescList.remove(unitToBeDeleted);
		if(unitTypeDescList == null || unitTypeDescList.size() <= 0)
			{
				viewMap.put("CURRENT_GRID_UPDATED_CLEAR_IT",false);
				viewMap.put("SHOW_CURRENT_GRID", false);
			}
			
		if(unitTypeDescList != null && unitTypeDescList.size()>0)
			viewMap.put("recordSize", unitTypeDescList.size());
		
		viewMap.put("UNIT_TYPE_AMOUNT_LIST",unitTypeDescList);
		if(viewMap.get("SUCCESS_MSG_RECORDS_UPDATED") != null)
			viewMap.remove("SUCCESS_MSG_RECORDS_UPDATED");
	}
	
	@SuppressWarnings("unchecked")
	public void clearGrid()
	{
		if(unitTypeDescList != null && unitTypeDescList.size() > 0)
		{
			unitTypeDescList.clear();
			viewMap.put("UNIT_TYPE_AMOUNT_LIST",unitTypeDescList);
			this.setShowApplyButton(false);
			viewMap.put("recordSize", 0);
			
			viewMap.put("CURRENT_GRID_UPDATED_CLEAR_IT", false);
			viewMap.put("SHOW_CURRENT_GRID", false);
			
			if(viewMap.get("SUCCESS_MSG_RECORDS_UPDATED") != null)
				viewMap.remove("SUCCESS_MSG_RECORDS_UPDATED");
			
		}	
	}
	public boolean isShowApplyButton() 
	{
		if(viewMap.get("SHOW_APPLY_BUTTON") != null)
			isShowApplyButton = (Boolean) viewMap.get("SHOW_APPLY_BUTTON"); 
		return isShowApplyButton;
	}
	
	@SuppressWarnings("unchecked")
	public void setShowApplyButton(boolean isShowApplyButton)
	{
		viewMap.put("SHOW_APPLY_BUTTON",isShowApplyButton);
		this.isShowApplyButton = isShowApplyButton;
	}
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	public HashMap<Double, Double> getUnitAreaMap() {
		return unitAreaMap;
	}
	public void setUnitAreaMap(HashMap<Double, Double> unitAreaMap) {
		this.unitAreaMap = unitAreaMap;
	}
	private boolean isInRange(Double checkNumber, Double from, Double to)
	{
		if(checkNumber >= from && checkNumber <= to)
			return true;
		else
			return false;
	}
}

