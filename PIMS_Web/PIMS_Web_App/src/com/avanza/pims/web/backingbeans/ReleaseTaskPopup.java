package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.SocialResearch;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskSearchCriteria;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialResearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.TaskListContract;
import com.avanza.pims.ws.vo.TaskListResult;
import com.avanza.ui.util.ResourceUtil;

public class ReleaseTaskPopup extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;
	private HtmlDataTable dataTable;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String claimedByUserId;
	
	private List<UserTask> userTasks = new ArrayList<UserTask>();
	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	private Map<Long,String> mapFile = new HashMap<Long, String>();
	private Map<Long,String> mapSocialResearch = new HashMap<Long, String>();
	private Map<Long,String> mapSocialResearchTaskDesc = new HashMap<Long, String>();
	private static Logger logger = Logger.getLogger( ReleaseTaskPopup.class );

	@SuppressWarnings("unchecked")
	public List<UserTask> getUserTasks() 
	{
		if( viewMap.get( "userTasks" ) != null )
		{
		  userTasks = (ArrayList<UserTask>)viewMap.get( "userTasks" );
		}
		return userTasks;
	}		
	
	
	
	@SuppressWarnings("unchecked")
	public String loadClaimedTasks(String claimedByUser)throws Exception 
	{
		
		BPMWorklistClient bpmWorkListClient = null;
		try 
		{
	        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			bpmWorkListClient = new BPMWorklistClient(contextPath);
			
			TaskSearchCriteria taskSearchCriteria = new TaskSearchCriteria();
			taskSearchCriteria.setAcquiredBy(	claimedByUser );
			Set<UserTask> userTaskss = bpmWorkListClient.getFilteredUserTasks(claimedByUser,taskSearchCriteria);
			userTasks = new ArrayList<UserTask>(); 
			HashMap<String, User> userHashMap = new HashMap<String, User>();
			HashMap<String, UserTask> taskHistoryMap = new HashMap<String, UserTask>();
			TaskListResult taskListResult = null;
			InheritanceFileService inheritanceFileService = new InheritanceFileService();
			PropertyService propertyService = new PropertyService();
			   
				for(UserTask userTask: userTaskss)
				{
					boolean addTask = false;
					
					if( !taskHistoryMap.containsKey( userTask.getTaskId() ) )
					{
					
					String userId = userTask.getSentByUser();					
					User user = null;
					
					if(userHashMap.containsKey(userId))
					{
						user = userHashMap.get(userId);
					}
					
					else
					{
						user = SecurityManager.getUser(userId);
						if(user != null)
						{
							userHashMap.put(userId, user);
						}					
					}
					
					Map<String,String> taskAttributes =  userTask.getTaskAttributes();
					TaskListContract taskListContract = null;
					if(taskAttributes.containsKey("CONTRACT_ID"))
					{
						Object contractId = taskAttributes.get("CONTRACT_ID");
						
						if(contractId != null && !contractId.equals(""))
						{
							Long cntId = Long.parseLong(contractId.toString()); 
							taskListContract =  propertyService.getContractByContractId(cntId);
						}
					}
					
					if(taskAttributes.containsKey("REQUEST_ID"))
					{
						Object reqId = taskAttributes.get("REQUEST_ID");
						
						if(reqId != null && !reqId.equals(""))
						{
							Long requestId = Long.parseLong(reqId.toString());
							//System.out.print("Req ID==="+requestId);
							if(taskListResult != null && taskListResult.getRequests() !=null && 
								!taskListResult.getRequests().contains(requestId))
							{
								addTask = false;
							}
							else 
							{
								//Search in contract 
								taskListContract = propertyService.getContractByRequestId(requestId);
								
								if( taskListResult != null && 
									taskListResult.getContracts() !=null && 
								   !taskListResult.getContracts().containsKey(taskListContract.getContractId())
								   )
								
								{
									  addTask = false;
								}
								else
								{
									//if request does not belong to contract than search in inheritance file
									taskListContract =inheritanceFileService.getInheritanceFileByRequestId(requestId);
									if(  taskListResult != null && 
										 taskListResult.getInheritanceFiles()  !=null && 
										!taskListResult.getInheritanceFiles().containsKey(taskListContract.getContractId())
									   )
									{
									  addTask = false;
									}
									else
									{
										//if request id doest not belong to file then search FINALLY in request
										TaskListContract taskListContracts = propertyService.getRequestByRequestId( requestId );
										if(  taskListResult != null && 
											 taskListResult.getRequests()    !=null && 
											 !taskListResult.getRequests().contains(taskListContracts.getContractId())
										  )
										{
											  addTask = false;
										}
										else 
										{
											addTask = true;
										}
										if( taskListContracts != null && 
										   ( taskListContract==null || taskListContract.getContractId() == null ||
											 taskListContract.getContractNumber()  == null || taskListContract.getContractNumber().length()==0  	   
										    ) 
										  )
										{
											taskListContract = taskListContracts;
										}
									}
								}
							}
						}
					}
					if(taskAttributes.containsKey("SOCIAL_RESEARCH_ID"))
					{
						long rsrchId = Long.valueOf( taskAttributes.get("SOCIAL_RESEARCH_ID").toString() );
						SocialResearch rsrch = EntityManager.getBroker().findById(SocialResearch.class, rsrchId);
						if(rsrch != null && rsrch.getInheritanceFile() != null)
						{
							mapFile.put(rsrchId,rsrch.getInheritanceFile().getFileNumber());
							String taskType =    	  taskAttributes.get( "TASK_TYPE" ).toString();
							Long caringTypeId = SocialResearchBean.taskTypeCaringTypeMapping(taskType);
							String desc = UtilityService.getRecommendationDescriptionForTask( rsrch ,caringTypeId );
							
							mapSocialResearchTaskDesc.put(rsrchId, desc);
						}
					}
					if(taskListContract != null)
					{
						 userTask.setContractNumber(taskListContract.getContractNumber());
					 	 userTask.setContractTenantNameEn(taskListContract.getContractTenantName());
						 userTask.setContractTenantNameAr(taskListContract.getContractTenantName());
					}
					
					
					if(addTask)
					{						
					
					
						if(addTask && taskAttributes.containsKey("REQUEST_ID"))
						{
							String requestId=taskAttributes.get("REQUEST_ID");
							String contractId="";
							HashMap< Long, TaskListContract> requestContractMap=new HashMap<Long, TaskListContract>();
							if(requestId != null && !requestId.equals(""))
							{
								if(taskAttributes.containsKey("CONTRACT_ID"))
										contractId = taskAttributes.get("CONTRACT_ID").toString();
								
								TaskListContract tasListContract= new TaskListContract();
								if(!requestContractMap.containsKey(Long.parseLong(requestId)))
									tasListContract =  propertyService.getContractByRequestId(Long.parseLong(requestId));
								else
									tasListContract =requestContractMap.get(Long.parseLong(requestId));
								if(tasListContract!=null && tasListContract.getContractNumber()!=null)
								{
									if(!requestContractMap.containsKey(Long.parseLong(requestId)))
									{
										requestContractMap.put(Long.parseLong(requestId), tasListContract);
									}
									userTask.setContractNumber(tasListContract.getContractNumber());
									userTask.setContractTenantNameEn(tasListContract.getContractTenantName());
									userTask.setContractTenantNameAr(tasListContract.getContractTenantName());
								
									if(!contractId.equals(""))
									{
										
										if(tasListContract.getContractId().compareTo(Long.parseLong(contractId))!=0)
										{
											TaskListContract contract = new TaskListContract();
											contract = propertyService.getContractByContractId(Long.parseLong(contractId));
											//In case of renew the conrtactId will have latest contract and tasListContract.getContractId() will have
											//old contract
											if( tasListContract.getContractId().compareTo(Long.parseLong(contractId)) < 0 )
											{
											userTask.setContractNumber( tasListContract.getContractNumber()+ " \\ " +  contract.getContractNumber());
											userTask.setContractTenantNameEn( tasListContract.getContractTenantName() );
											userTask.setContractTenantNameAr( tasListContract.getContractTenantName() );
											}
											//In case of transfer the conrtactId will have old contract and tasListContract.getContractId() will have
											//latest contract
											else if( tasListContract.getContractId().compareTo(Long.parseLong(contractId)) > 0 )
											{
											userTask.setContractNumber( contract.getContractNumber()+ " \\ " + tasListContract.getContractNumber());
											String concatName = contract.getContractTenantName()+" \\ " +tasListContract.getContractTenantName();
											userTask.setContractTenantNameEn( concatName );
											userTask.setContractTenantNameAr( concatName );
											}
										}	
									}
								}
							}
						}
						if(addTask && taskAttributes.containsKey("FILE_ID"))
						{
							long fileId = Long.valueOf( taskAttributes.get("FILE_ID").toString() );
							if( mapFile.containsKey( fileId ) )
							{
								userTask.setContractNumber(mapFile.get( fileId )); 
							}
							else
							{
								InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, fileId);
								mapFile.put(fileId,file.getFileNumber());
								userTask.setContractNumber(file.getFileNumber());
								file=null;
							}
							
						}
						if(addTask && taskAttributes.containsKey("SOCIAL_RESEARCH_ID"))
						{
							String taskDesciption ="";
							long rsrchId = Long.valueOf( taskAttributes.get("SOCIAL_RESEARCH_ID").toString() );
							if( mapSocialResearch.containsKey( rsrchId ) )
							{
								taskDesciption = mapFile.get( rsrchId );
								if( mapSocialResearchTaskDesc.get(rsrchId) != null )
								{
									taskDesciption +=  " / " + mapSocialResearchTaskDesc.get(rsrchId) ;
								}
								userTask.setContractNumber( taskDesciption );
								
							}
							else
							{
								SocialResearch rsrch = EntityManager.getBroker().findById(SocialResearch.class, rsrchId);
								mapFile.put(rsrchId,rsrch.getInheritanceFile().getFileNumber());
								String taskType =    	  taskAttributes.get( "TASK_TYPE" ).toString();
								Long caringTypeId = SocialResearchBean.taskTypeCaringTypeMapping(taskType);
								String desc = UtilityService.getRecommendationDescriptionForTask( rsrch ,caringTypeId );
								userTask.setContractNumber(rsrch.getInheritanceFile().getFileNumber() +"/"+desc );
								rsrch=null;
							}
							
						}
						if(	taskAttributes.containsKey("ENDOWMENT_FILE_NUMBER") )
						{
							userTask.setContractNumber(taskAttributes.get("ENDOWMENT_FILE_NUMBER").toString() );
						}
						else if(	taskAttributes.containsKey("REQUEST_NUM") && 
								   (userTask.getContractNumber()==null || userTask.getContractNumber().trim().length() <= 0 )
								)
						{
							userTask.setContractNumber(taskAttributes.get("REQUEST_NUM").toString() );
						}
						else if(	taskAttributes.containsKey("ENDOWMENT_PROGRAM_NUM") && 
								   (userTask.getContractNumber()==null || userTask.getContractNumber().trim().length() <= 0 )
								)
						{
							userTask.setContractNumber(taskAttributes.get("ENDOWMENT_PROGRAM_NUM").toString() );
						}
					}
						userTasks.add(userTask);
						taskHistoryMap.put(userTask.getTaskId(),userTask );
				}
				}

			
			viewMap.put("userTasks", userTasks);
			setRecordSize ( userTasks.size());
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
		}
		catch (PIMSWorkListException e) {
			
			logger.LogException("Task List could not be loaded.loadDataList method crashed due to:",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		catch (Exception e) {
			
			logger.LogException("Task List could not be loaded.loadDataList method crashed due to:",e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
		
		return "";
	}
	
	

	public String getLoggedInUser()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		
		return loggedInUser;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public void init() 
	{		
		super.init();
		try
		{
			if(!isPostBack())
			{
				setClaimedByUserId( getLoggedInUserId() );
				onSearch();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
		
	}
	private BPMWorklistClient getBPMClientInstance() throws IOException
	{
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
		return bpmWorkListClient;
	}
	public void onSearch()
	{
		try
		{
		  loadClaimedTasks( getClaimedByUserId() );
		}
		catch(Exception exp)
		{
			logger.LogException("onSearch:",exp);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onReleaseAll()
	{
	  errorMessages.clear();
	  infoMessage = "";
	  try
	  {
		for (UserTask userTask   : getUserTasks()) 
		{
			doReleaseTask(userTask);
		}
		onSearch();
	  }
	  catch(PIMSWorkListException pimsExp)
	  {
		logger.LogException("onReleaseAll crashed due to:",pimsExp);
		if(pimsExp.getExceptionCode() == ExceptionCodes.TASK_IS_NOT_ACQUIRED)
		{
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureNotClaimed"));
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
		}
	  }
	  catch(Exception exp)
	  {
		logger.LogException("onReleaseAll:",exp);
		errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
	  }
	}
	
	@SuppressWarnings("unchecked")
	public void Release() 
	{
		try
		{
			errorMessages.clear();
			UserTask userTask =  (UserTask) dataTable.getRowData();
			doReleaseTask(userTask  );
			onSearch();
		}
		catch(PIMSWorkListException pimsExp)
		{
			logger.LogException("Release method crashed due to:",pimsExp);
			if(pimsExp.getExceptionCode() == ExceptionCodes.TASK_IS_NOT_ACQUIRED)
			{
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureNotClaimed"));
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
			}
		}
		catch(Exception exp)
		{
			logger.LogException("Release method crashed due to:",exp);
			errorMessages.add(CommonUtil.getBundleMessage("TaskList.messages.releaseTaskFailureGeneral"));
		}
		
	}



	/**
	 * @param user
	 * @throws IOException
	 * @throws PIMSWorkListException
	 * @throws PimsBusinessException
	 */
	private void doReleaseTask(UserTask userTask ) throws IOException,PIMSWorkListException, PimsBusinessException 
	{
		String user = getClaimedByUserId();
		BPMWorklistClient bpmWorkListClient = getBPMClientInstance();
		if(bpmWorkListClient.releaseTask(userTask, user))
		{
			userTask.setAccquiredby(null);
			userTask.setIsAccquiredByUser(false);
			CommonUtil.updateRequestTaskAcquiredBy( userTask,null );
		}
		
		infoMessage =  CommonUtil.getBundleMessage( "TaskList.messages.releaseSuccess");
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	@SuppressWarnings("unchecked")
	public String getClaimedByUserId() {
		if( viewMap.get("claimedByUserId") != null )
		{
			claimedByUserId = viewMap.get("claimedByUserId").toString();
		}
		return claimedByUserId;
	}
	
	@SuppressWarnings("unchecked")
	public void setClaimedByUserId(String claimedByUserId) {
		this.claimedByUserId = claimedByUserId;
		if(  this.claimedByUserId != null )
		{
			viewMap.put( "claimedByUserId" ,this.claimedByUserId );
		}
	}
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() 
	{
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
		{
			recordSize = 0;
		}
		return recordSize;
	}
	
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		viewMap.put("recordSize",this.recordSize );
	}

	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

}

