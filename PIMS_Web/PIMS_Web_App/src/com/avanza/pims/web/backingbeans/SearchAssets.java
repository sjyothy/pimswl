package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SearchAssetsService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;

public class SearchAssets extends AbstractController {
	private transient Logger logger = Logger.getLogger(SearchAssets.class);

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable dataTable;

	private HtmlInputText htmlAssetNumber = new HtmlInputText();
	private HtmlInputText htmlAssetNameEn = new HtmlInputText();
	private HtmlInputText htmlAssetDesc = new HtmlInputText();
	private String inheritanceFileId;
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlBeneficiary = new HtmlInputText();
	private HtmlInputText htmlFilePersonName = new HtmlInputText();
	private HtmlInputText htmlAssetNameAr = new HtmlInputText();
	private HtmlSelectOneMenu assetTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlInputText htmlManageName = new HtmlInputText();
	private HtmlSelectBooleanCheckbox htmlIsManagerAmaf = new HtmlSelectBooleanCheckbox();
	private HtmlSelectOneMenu fileStatusSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu isConfirmSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu isFinalSelectMenu = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	private List<SelectItem> fileStatusList = new ArrayList<SelectItem>(0);
	private List<SelectItem> fileTypeList = new ArrayList<SelectItem>(0);
	private HtmlSelectOneMenu managerType = new HtmlSelectOneMenu();
	
	HashMap searchAssetsMap = new HashMap();
	private List<AssetsGridView> dataList = new ArrayList<AssetsGridView>();
	private List<SelectItem> fileStatus = new ArrayList<SelectItem>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
    private String fromPage="";
	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	private String MAINTENANCE_REQUEST= "MAINTENANCE_REQUEST";
	
	
	private String  DEFAULT_SORT_FIELD = "assetNumber";

	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	
	private String  CONTEXT="context";
	private List<String> errorMessages;
	HttpServletRequest request;

	public SearchAssets()
	{
		sessionMap = context.getExternalContext().getSessionMap();
		request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	}
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputText getHtmlAssetNumber() {
		return htmlAssetNumber;
	}

	public void setHtmlAssetNumber(HtmlInputText htmlAssetNumber) {
		this.htmlAssetNumber = htmlAssetNumber;
	}

	public HtmlInputText getHtmlAssetNameNameEn() {
		return htmlAssetNameEn;
	}

	public void setHtmlAssetNameNameEn(HtmlInputText htmlAssetNameNameEn) {
		this.htmlAssetNameEn = htmlAssetNameNameEn;
	}

	public HtmlInputText getHtmlAssetDesc() {
		return htmlAssetDesc;
	}

	public void setHtmlAssetDesc(HtmlInputText htmlAssetDesc) {
		this.htmlAssetDesc = htmlAssetDesc;
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlBeneficiary() {
		return htmlBeneficiary;
	}

	public void setHtmlBeneficiary(HtmlInputText htmlBeneficiary) {
		this.htmlBeneficiary = htmlBeneficiary;
	}

	public HtmlInputText getHtmlFilePersonName() {
		return htmlFilePersonName;
	}

	public void setHtmlFilePersonName(HtmlInputText htmlFilePersonName) {
		this.htmlFilePersonName = htmlFilePersonName;
	}

	public HtmlInputText getHtmlAssetNameAr() {
		return htmlAssetNameAr;
	}

	public void setHtmlAssetNameAr(HtmlInputText htmlAssetNameAr) {
		this.htmlAssetNameAr = htmlAssetNameAr;
	}

	

	public HtmlSelectOneMenu getFileStatusSelectMenu() {
		return fileStatusSelectMenu;
	}

	public void setFileStatusSelectMenu(HtmlSelectOneMenu fileStatusSelectMenu) {
		this.fileStatusSelectMenu = fileStatusSelectMenu;
	}

	public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}
	
	@SuppressWarnings("unchecked")
	public void prerender() 
	{
		if ( getIsContextRevenueFollowup() )
		{
			managerType.setDisabled(true);
			managerType.setValue("0");
		}
	}
	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		try 
		{

			if (!isPostBack()) 
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(true);
				
				 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    	}
		    		if (request.getParameter(CONTEXT)!=null )
			    	 {
		    			viewRootMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
			    	 }
		    	 }
				loadFileStatusList();
				loadFileTypeList();
				enableDisableControls();
				if( request.getParameter(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null )
				{
					htmlFileNumber.setValue( request.getParameter("FILE_NUMBER").toString() );
					htmlFileNumber.setDisabled( true );
					htmlFileNumber.setStyleClass("READONLY");
					setInheritanceFileId(request.getParameter(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString());
				}
				if(sessionMap.get(WebConstants.InheritanceFile.ASSET_IDS_LIST) != null)
				{
					List<Long> assetId = (List<Long>) sessionMap.get(WebConstants.InheritanceFile.ASSET_IDS_LIST);
					sessionMap.remove(WebConstants.InheritanceFile.ASSET_IDS_LIST) ;
					viewRootMap.put(WebConstants.InheritanceFile.ASSET_IDS_LIST, assetId);
				}
				
			}
		}
		catch (Exception es) 
		{
			logger.LogException("init|crashed ", es);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	private void enableDisableControls() 
	{
		
		if(getIsContextMaintenanceRequest())
		{
			assetTypeSelectMenu.setValue(WebConstants.AssetType.LAND_PROPERTIES.toString());
			assetTypeSelectMenu.setDisabled(true);
		}
		
		// TODO Auto-generated method stub
		
	}

	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}

	public void doSearch() {
		try {
			pageFirst();
//			loadDataList();
		} catch (Exception e) {
			System.out.println("Exception doSearch" + e);
		}
	}

	public List<SelectItem> getFileStatus() {
		fileStatus.clear();
		try {

			loadFileStatusList();

			// List<DomainDataView>
			// ddl=(ArrayList)sessionMap.get(WebConstants.UNIT_TYPE);
			// for(int i=0;i<ddl.size();i++)
			// {
			// DomainDataView ddv=(DomainDataView)ddl.get(i);
			// SelectItem item = new
			// SelectItem(ddv.getDomainDataId().toString(),
			// ddv.getDataDescEn());
			// unitType.add(item);
			// }
			//		  
		} catch (Exception ex) {

		}
		//		

		return null;
	}

	private void loadFileStatusList() throws PimsBusinessException {

		List<SelectItem> allowedStatuses = new ArrayList<SelectItem>();

		List<DomainDataView> domainDataList = CommonUtil
				.getDomainDataListForDomainType(WebConstants.InheritanceFileStatus.INH_FILE_STATUS);
		for (DomainDataView dd : domainDataList) {
			boolean isAdd = true;

			if (isAdd) {
				SelectItem item = null;

				if (CommonUtil.getIsEnglishLocale())
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescEn());
				else
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescAr());

				allowedStatuses.add(item);
			}

		}

		this.setFileStatusList(allowedStatuses);

	}

	private void loadFileTypeList() throws PimsBusinessException {

		List<SelectItem> allowedTypes = new ArrayList<SelectItem>();

		List<DomainDataView> domainDataList = CommonUtil
				.getDomainDataListForDomainType(WebConstants.InheritanceFileType.INH_FILE_TYPE);
		for (DomainDataView dd : domainDataList) {
			boolean isAdd = true;

			// if(getIsContextAuctionUnits() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//
			// else if(getIsContextNewLeaseContract() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//				 
			// else if(getIsContextInspectionUnits() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) ||
			// dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;
			//				 
			// else if(getIsContextMaintenanceRequest() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;

			if (isAdd) {
				SelectItem item = null;

				if (CommonUtil.getIsEnglishLocale())
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescEn());
				else
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescAr());

				allowedTypes.add(item);
			}

		}

		this.setFileTypeList(allowedTypes);

	}

	@SuppressWarnings("unchecked")
	private List<AssetsGridView> loadDataList() throws Exception
	{
			if (dataList != null) dataList.clear();
			List<AssetsGridView> list = new ArrayList();
			searchAssetsMap = getSearchCriteria();
			SearchAssetsService searchService = new SearchAssetsService();
			int totalRows = searchService.searchAssetsGetTotalNumberOfRecords(searchAssetsMap);
			setTotalRows(totalRows);
			doPagingComputations();
			list =  searchService.getAssetsByCriteria(searchAssetsMap,getRowsPerPage(), getCurrentPage(), getSortField(),
													  isSortItemListAscending(),CommonUtil.getIsEnglishLocale()
													  );
			//			
			if (list.isEmpty() || list == null) {
				errorMessages = new ArrayList<String>();
				errorMessages
						.add(CommonUtil
								.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			viewMap.put("assetList", list);
			recordSize = totalRows;
			viewMap.put("recordSize", totalRows);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
			
			return list;

	}

	private Boolean getIsContextMaintenanceRequest() {
		
		String contextScreenName="";
		if(viewRootMap.get(CONTEXT)!=null)
		{
			contextScreenName=(String)viewRootMap.get(CONTEXT);
			if(contextScreenName.equals(MAINTENANCE_REQUEST))
				return true;
			else
				return false;			
		}
		return false;
		
	}
	

	private Boolean getIsContextRevenueFollowup() {
		
		if(viewRootMap.get(CONTEXT)!=null)
		{
			String contextScreenName=(String)viewRootMap.get(CONTEXT);
			if(contextScreenName.equals("revenueFollowup"))
				return true;
			else
				return false;			
		}
		return false;
		
	}
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, Object> getSearchCriteria() {
		searchAssetsMap = new HashMap<String, Object>();

		String emptyValue = "-1";

		Object assetNumber = htmlAssetNumber.getValue();
		Object assetNameEn = htmlAssetNameEn.getValue();
		Object assetNameAr = htmlAssetNameAr.getValue();
		Object assetDesc = htmlAssetDesc.getValue();
		Object fileNumber = htmlFileNumber.getValue();
		Object beneficiary = htmlBeneficiary.getValue();
        Object assetType = assetTypeSelectMenu.getValue();

		Object filePersonName = htmlFilePersonName.getValue();
		Object fileStatus = fileStatusSelectMenu.getValue();
		Object fileType = fileTypeSelectMenu.getValue();
		Object isConfirm = isConfirmSelectMenu.getValue();
		Object isFinal = isFinalSelectMenu.getValue();
		Object isManagerAmaf = managerType.getValue();
		Object managerName = htmlManageName.getValue();
		
		List<Long> assetIdToBeExcluded = new ArrayList<Long>();
		
		if(isManagerAmaf != null )
		{
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.IS_MANAGER_AMAF, isManagerAmaf);
		}
		
		if(managerName != null && StringHelper.isNotEmpty(managerName.toString()))
		{
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.MANAGER_NAME, managerName.toString());
		}
		
		if(viewRootMap.get(WebConstants.InheritanceFile.ASSET_IDS_LIST) != null)
		{
			assetIdToBeExcluded = (List<Long>) viewRootMap.get(WebConstants.InheritanceFile.ASSET_IDS_LIST);
			if(assetIdToBeExcluded != null && assetIdToBeExcluded.size() > 0)
				searchAssetsMap.put(WebConstants.InheritanceFile.ASSET_IDS_LIST,assetIdToBeExcluded);
		}
		if (assetNumber != null && !assetNumber.toString().trim().equals("")
				&& !assetNumber.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_NUMBER,
					assetNumber.toString());
		}
		if (assetNameEn != null && !assetNameEn.toString().trim().equals("")
				&& !assetNameEn.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_NAME_EN,
					assetNameEn.toString());
		}
		if (assetNameAr != null && !assetNameAr.toString().trim().equals("")
				&& !assetNameAr.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_NAME_AR,
					assetNameAr.toString());
		}
		if (assetDesc != null && !assetDesc.toString().trim().equals("")
				&& !assetDesc.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_DESC,
					assetDesc.toString());
		}
		if (fileNumber != null && !fileNumber.toString().trim().equals("")
				&& !fileNumber.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.FILE_NUMBER, fileNumber
							.toString());
		}
		if (beneficiary != null && !beneficiary.toString().trim().equals("")
				&& !beneficiary.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.BENEFICIARY,
					beneficiary.toString());
		}
//		if (assetType != null && !assetType.toString().trim().equals("")
//				&& !assetType.toString().trim().equals(emptyValue)) {
//			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_TYPE,
//					assetType.toString());
//		}
		if (filePersonName != null
				&& !filePersonName.toString().trim().equals("")
				&& !filePersonName.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.FILE_PERSON_NAME,
					filePersonName.toString());
			
			
		}
		if (assetType != null && !assetType.toString().trim().equals("")
				&& !assetType.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.ASSET_TYPE, assetType
							.toString());
		}
		if (fileStatus != null && !fileStatus.toString().trim().equals("")
				&& !fileStatus.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(
					WebConstants.SEARCH_ASSETS_CRITERIA.FILE_STATUS, fileStatus
							.toString());
		}
		if (fileType != null && !fileType.toString().trim().equals("")
				&& !fileType.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.FILE_TYPE,
					fileType.toString());
		}
		if (isFinal != null && !isFinal.toString().trim().equals("")
				&& !isFinal.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.IS_FINAL,
					isFinal.toString());
		}
		if (isConfirm != null && !isConfirm.toString().trim().equals("")
				&& !isConfirm.toString().trim().equals(emptyValue)) {
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.IS_CONFIRM,
					isConfirm.toString());
		}
		if( getInheritanceFileId() != null  && getInheritanceFileId().trim().length() > 0 )
		{
			searchAssetsMap.put(WebConstants.SEARCH_ASSETS_CRITERIA.FILE_ID, getInheritanceFileId() );
		}
		

		// TODO Auto-generated method stub
		return searchAssetsMap;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getFileStatusList() {
		if (viewRootMap.get("fileStatusList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileStatusList");
		return new ArrayList<SelectItem>();
	}

	@SuppressWarnings("unchecked")
	public void setFileStatusList(List<SelectItem> fileStatusList) {

		this.fileStatusList = fileStatusList;
		if (this.fileStatusList != null)
			viewRootMap.put("fileStatusList", this.fileStatusList);
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getFileTypeList() {
		if (viewRootMap.get("fileTypeList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileTypeList");
		return new ArrayList<SelectItem>();
	}
	@SuppressWarnings("unchecked")
	public void setFileTypeList(List<SelectItem> fileTypeList) {
		this.fileTypeList = fileTypeList;
		if (fileTypeList != null)
			viewRootMap.put("fileTypeList", this.fileTypeList);
	}
	@SuppressWarnings("unchecked")
	public void doSearchItemList()
	{
		try
		{
			loadDataList();
		}
		catch(Exception e)
		{
			logger.LogException("doSearchItemList", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	@SuppressWarnings("unchecked")
	public List<AssetsGridView> getDataList() {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		dataList = (List<AssetsGridView>) viewMap.get("assetList");
		if (dataList == null)
			dataList = new ArrayList<AssetsGridView>();
		return dataList;
	}

	public void setDataList(List<AssetsGridView> dataList) {
		this.dataList = dataList;
	}
	public List<AssetsGridView> getSelectedAssets(){
		logger.logInfo("getSelectedAssets started...");
		List<AssetsGridView> selectedAssetsList = new ArrayList<AssetsGridView>(0);
    	try{
    		dataList = getDataList();
    		if(dataList != null && dataList.size() > 0)
    		{
    			for(AssetsGridView assetView:dataList )
    			
    				if(assetView.getSelected())
    				{
    					selectedAssetsList.add(assetView);
    				}
    			
    		}
    		 		
    		logger.logInfo("getSelectedAssets completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedAssets() crashed ",exception);
    	}    	
    	return selectedAssetsList;
    }
	@SuppressWarnings("unchecked")
	public void sendManyAssetsInfoToParent(javax.faces.event.ActionEvent event)//many assets selected
	{ 
	       
		  final String viewId = "/SearchAssets.jsp"; 
	      List<AssetsGridView> selectedAssets = getSelectedAssets();
	      if(!selectedAssets.isEmpty())
	      {
//	    	  	if(getIsContextAuctionUnits())
//	    	  	{
//	    	  		setResultsForAuctionUnits();
//	    	  	}
//	    	  	else if(getIsContextInspectionUnits())
//	    	  	{
//	    	  		setResultsForInspectionUnits();
//	    	  	}
//	    	  	else
//	    	  	{	    	  		 
	    	  	sessionMap.put(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_MANY_ASSET, selectedAssets);
	    		     
//	    	  	}	    	    
		        String javaScriptText = "javascript:closeWindowSubmit();";		
		        sendToParent(javaScriptText);
	      }
	      else
	      {
	        	errorMessages = new ArrayList<String>();
				
	      }	      
	        
	    }
	public void sendAssetInfoToParent(javax.faces.event.ActionEvent event) // single asset selected
	{
	        AssetsGridView assetGridViewRow=(AssetsGridView)dataTable.getRowData();
	        String assetDesc = assetGridViewRow.getAssetDesc()!=null ? assetGridViewRow.getAssetDesc().toString():null;
	        
	        String javaScriptText ="window.opener.populateAsset(" +
			                                                     "'"+assetDesc+"'," +
			                                                     "'"+assetGridViewRow.getAssetNameAr()+"'," +
			                                                     "'"+assetGridViewRow.getAssetNameEn()+"',"+
			                                                     "'"+assetGridViewRow.getAssetType().toString()+"',"+
			                                                     "'"+assetGridViewRow.getFileNumber()+"',"+
			                                                     "'"+assetGridViewRow.getPersonName()+"',"+
			                                                     "'"+assetGridViewRow.getAssetId()+"',"+
			                                                     "'"+assetGridViewRow.getStatus()+"'"+
			                                                     ");"+
	        		               "window.close();"; 
	        	
	        Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	        sessionMap.put(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET, assetGridViewRow);
	        sendToParent(javaScriptText);
	        
	 }
	public String edit(){
	AssetsGridView assetGridRow=(AssetsGridView)dataTable.getRowData();
        sessionMap.put("assetGridRow", assetGridRow);
		return "ManageAssets";
	}
	
	public String sendToParent(String javaScript)
	{
		String methodName="sendToParent";
        final String viewId = "/SearchAssets.jsp";
		//logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText =javaScript;
        
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		//logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings("unchecked") 
	public String openCollectionProc()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			AssetsGridView assetGridViewRow=(AssetsGridView)dataTable.getRowData();
			sessionMap.put(WebConstants.ASSET_ID, assetGridViewRow.getAssetId().toString());
			if( assetGridViewRow.getAssetTypeId().longValue() == WebConstants.AssetType.BANK_TRANSFER.longValue() )
			{
				sessionMap.put(WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER, WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER );
			}
			return "collectionProc";
		}
	    catch(Exception e)
	    {
	    	logger.LogException("openCollectionProc|ErrorOccured|", e);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    }
	    return "";
	}

	public HtmlSelectOneMenu getAssetTypeSelectMenu() {
		return assetTypeSelectMenu;
	}

	public void setAssetTypeSelectMenu(HtmlSelectOneMenu assetTypeSelectMenu) {
		this.assetTypeSelectMenu = assetTypeSelectMenu;
	}

	public HtmlSelectOneMenu getIsConfirmSelectMenu() {
		return isConfirmSelectMenu;
	}

	public HtmlSelectOneMenu getIsFinalSelectMenu() {
		return isFinalSelectMenu;
	}

	public void setIsConfirmSelectMenu(HtmlSelectOneMenu isConfirmSelectMenu) {
		this.isConfirmSelectMenu = isConfirmSelectMenu;
	}

	public void setIsFinalSelectMenu(HtmlSelectOneMenu isFinalSelectMenu) {
		this.isFinalSelectMenu = isFinalSelectMenu;
	}

	public HtmlInputText getHtmlManageName() {
		return htmlManageName;
	}

	public void setHtmlManageName(HtmlInputText htmlManageName) {
		this.htmlManageName = htmlManageName;
	}

	public HtmlSelectBooleanCheckbox getHtmlIsManagerAmaf() {
		return htmlIsManagerAmaf;
	}

	public void setHtmlIsManagerAmaf(HtmlSelectBooleanCheckbox htmlIsManagerAmaf) {
		this.htmlIsManagerAmaf = htmlIsManagerAmaf;
	}

	public String getInheritanceFileId() {
		if( viewMap.get("inheritanceFileId") != null )
		{
			inheritanceFileId= viewMap.get("inheritanceFileId").toString(); 
		}
		return inheritanceFileId;
	}

	public void setInheritanceFileId(String inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
		if(this.inheritanceFileId != null )
		{
			viewMap.put("inheritanceFileId",this.inheritanceFileId);
		}
	}
	public HtmlSelectOneMenu getManagerType() {
		return managerType;
	}
	public void setManagerType(HtmlSelectOneMenu managerType) {
		this.managerType = managerType;
	}

	

}
