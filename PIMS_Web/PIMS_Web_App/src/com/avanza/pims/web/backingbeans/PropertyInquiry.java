package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.calendar.HtmlInputCalendar;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.*;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;

import com.avanza.pims.web.controller.*;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.UnitView;



import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.InquiryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;


public class PropertyInquiry extends AbstractController {

	private static Logger logger = Logger.getLogger( PropertyInquiry.class );
	
	private Date inquiryDate;
	private String selectedInquiryMethod;
	private String selectedInquiryPriority;
	private String createdBy;
	private String createdOn;
	private String personId;
	private String passportNo;
	private String designation;
	//private String cellNo;
	
	private String hdnUnitIds;
	private String hdnPersonIds;
    private HtmlInputHidden ctlHdnUnitId=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnPersonId=new HtmlInputHidden();
    
    private HtmlInputHidden ctlHdnUnitAreaMin=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnUnitAreaMax=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnLivingMax=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnBedsMax=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnBathMax=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnRentMin=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnRentMax=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnFloorMin=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnObjSelectUnitType=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnObjSelectUnitUsage=new HtmlInputHidden();
    private HtmlInputHidden ctlHdnObjSelectUnitSide=new HtmlInputHidden();
    org.richfaces.component.html.HtmlCalendar richCalendar=new org.richfaces.component.html.HtmlCalendar(); 
    private final String noteOwner = WebConstants.REQUEST;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_PROPERTY;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_PROPERTY_INQUIRY;
    
    //private HtmlInputHidden inquiryDate=new HtmlInputHidden();
    private HtmlInputHidden ctlInquiryDate=new HtmlInputHidden();
	private HtmlSelectOneMenu inquiryMethodSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu prioritySelectMenu = new HtmlSelectOneMenu();
	private HtmlCommandButton saveButton = new HtmlCommandButton();
	private HtmlCommandButton openUnitListButton = new HtmlCommandButton();
	private HtmlCommandButton clearUnitListButton = new HtmlCommandButton();

	List<SelectItem> inquiryMethod = new ArrayList();
	List<SelectItem> priority = new ArrayList();
	List<SelectItem> groupofUsers = new ArrayList();

	InquiryView inquiryView = new InquiryView();
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	
	//Person tab fields////////////////
	private HtmlInputText txtName;
	private String name;
	private HtmlInputText txtCellNo;
	private String cellNo;
	private HtmlInputText txtEmail;
	private String email;
	private HtmlInputText txtApplicantType;
	private String applicantType;
	
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	
	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;
	
	private String selectOneUnitType;
	private String selectOneUnitUsage;

	private String unitNumber;
	private String rentValueMin;
	private String rentValueMax;
	private String noOfBedsMax;
	private String noOfBedsMin;

	private String countryId;
	private String stateId;
	private String cityId;
	private String selectOnePriority;
	private String selectOneInquiryMethod;
 
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<SelectItem> cityList = new ArrayList<SelectItem>();
	
	private List<UnitView> dataList = new ArrayList<UnitView>();
	private HtmlDataTable dataTable;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
 CommonUtil	commonUtil = new CommonUtil();
	
     
 FacesContext context=FacesContext.getCurrentInstance();
 Map sessionMap= context.getExternalContext().getSessionMap();
 Map viewRootMap=context.getViewRoot().getAttributes();
  
 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
 
 	private String selectOneApplicationStatus;
 	private Map<String,List<SelectItem>> listMap = new HashMap<String,List<SelectItem>>();
 	private List<SelectItem> emptyList = new ArrayList<SelectItem>();
 	private String personNumber;
 	private String passportNumber;
 	private String passportIssueDate;
 	private String passportExpiryDate;
 	private String profession;
 	private String company;
 	private String phoneNumber;
 	private String poBox;
 	
 	public List<SelectItem> getRequestStatusList() {
		Boolean isEnglishLocale = isEnglishLocale();
		
		if(isEnglishLocale) {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_EN);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED_AND_NOTIFIED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CLOSED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NOTIFIED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_EN, cboList);
			}
			return cboList;
		} else {
			List<SelectItem> cboList = listMap.get(WebConstants.REQUEST_STATUS_LIST_AR);
			if(cboList==null) {
				List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
				Iterator<DomainTypeView> itr = list.iterator();
				while( itr.hasNext() ) {
					DomainTypeView dtv = itr.next();
					if( dtv.getTypeName().compareTo(WebConstants.REQUEST_STATUS)==0 ) {
						Set<DomainDataView> dd = dtv.getDomainDatas();
						cboList = new ArrayList<SelectItem>();
						SelectItem item = null;
						for (DomainDataView ddv : dd) {
							if(ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_LOGGED_AND_NOTIFIED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_CLOSED)==0
							 ||ddv.getDataValue().compareTo(WebConstants.REQUEST_STATUS_NOTIFIED)==0){
							item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescAr());
							cboList.add(item);
							}
						}
						break;
					}
				}
				if( cboList==null ) {
					return emptyList;
				}
				Collections.sort(cboList,ListComparator.LIST_COMPARE);
				listMap.put(WebConstants.REQUEST_STATUS_LIST_AR, cboList);
			}
			return cboList;
		}
	}
 	
 	public Boolean isEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	
 @Override
	public void init() {
        
		super.init();
		try {
			
			sessionMap =context.getExternalContext().getSessionMap();
			loadCountry();
			
					
			if(!isPostBack())
			{
				CommonUtil.loadAttachmentsAndComments(null);
				viewRootMap.put(WebConstants.Attachment.EXTERNAL_ID,externalId);				
				getEnableDisableControl();
				errorMessages = new ArrayList<String>();
				successMessages=new ArrayList<String>();
				inquiryDate = new Date(); 
				loadCombos();
				saveButton.setRendered(true);
				openUnitListButton.setRendered(true);
			  viewRootMap.put("canAddAttachment", true); 
		       viewRootMap.put("canAddNote", true);
				
			}

			
			System.out.println("Inside init of PropertyInquiry");

		} catch (Exception es) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
 
 public void prerender(){
		super.prerender();
	
		getEnableDisableControl();
		
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE+"_2") && sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE+"_2")!=null){
		setSelectOneUnitType(sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_TYPE+"_2");
	}
   if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE+"_2")&& sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE+"_2")!=null){
		setSelectOneUnitUsage(sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_USAGE+"_2");
	}	
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER+"_2") && sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER+"_2")!=null){
		setUnitNumber(sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_NUMBER+"_2");
	}	
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN+"_2")&&sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN+"_2")!=null){
		setRentValueMin(sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN+"_2");
	}
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX+"_2") && sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX+"_2")!=null){
		setRentValueMax(sessionMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX+"_2");
	}	
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX+"_2")&& sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX+"_2")!=null){
		setNoOfBedsMax(sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX+"_2");
	}
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN+"_2")&& sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN+"_2")!=null){
		setNoOfBedsMin(sessionMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN+"_2");
	}	

	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID+"_2")&& sessionMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID+"_2")!=null){
		setCountryId(sessionMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.COUNTRY_ID+"_2");
	}	
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID+"_2") && sessionMap.get(WebConstants.INQUIRY_UNITS.STATE_ID+"_2")!=null){
		setStateId(sessionMap.get(WebConstants.INQUIRY_UNITS.STATE_ID+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.STATE_ID+"_2");
	}
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID+"_2") && sessionMap.get(WebConstants.INQUIRY_UNITS.CITY_ID+"_2")!=null){
		setCityId(sessionMap.get(WebConstants.INQUIRY_UNITS.CITY_ID+"_2").toString());
		sessionMap.remove(WebConstants.INQUIRY_UNITS.CITY_ID+"_2");
	}
	
	if(sessionMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)){
		viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST,sessionMap.get(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST));
		getPropertyInquiryDataList();
		sessionMap.remove(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST);
	}
	
	if ( sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS) != null ) 
	{
		List<UnitView> newSelectedUnitViewList = (List<UnitView>) sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
		sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
		List<UnitView> alreadySelectedUnitViewList = getPropertyInquiryDataList();		
		alreadySelectedUnitViewList.addAll( newSelectedUnitViewList );
		viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST, alreadySelectedUnitViewList);
		getPropertyInquiryDataList();		
	}
	
	// to check weather list is get's record or not 
	getEnableDisableControl();
		
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID)){
			countryId = viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString();
			if(countryId!=null && !countryId.equals("")&& !countryId.equals("-1")){
			loadCountry();
			loadState();}
 	        }
    		
    		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID)){
    			
			  stateId=	viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString();
			  if(stateId!=null && !stateId.equals("")&& !stateId.equals("-1")){
			  loadCity();
			  }
    		}    	 
		
		if(hdnPersonId!=null && !hdnPersonId.equals("")){
			viewRootMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}

			if(hdnPersonName!=null && !hdnPersonName.equals(""))
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			
			
			if(hdnCellNo!=null && !hdnCellNo.equals(""))
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);
			
			if(hdnIsCompany!=null && !hdnIsCompany.equals("")){
				DomainDataView ddv = new DomainDataView();
				if(Long.parseLong(hdnIsCompany)==1L){
				        ddv= commonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_COMPANY);}
				else{
				        ddv= commonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_INDIVIDUAL);}
				
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
			
			}
 }
 
     private void loadCombos() throws PimsBusinessException
     {
    	 
    	 if(!sessionMap.containsKey(WebConstants.INQUIRY_METHOD))
    	   loadInquiryMethodList();
    	 if(!sessionMap.containsKey(WebConstants.INQUIRY_PRIORITY))
      	   loadInquiryPriorityList();
    	 
    	 
     }
     private void loadInquiryMethodList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.INQUIRY_METHOD);
    	 sessionMap.put(WebConstants.INQUIRY_METHOD, ddl);
    	 
    	 
     }
     private void loadInquiryPriorityList() throws PimsBusinessException{
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.INQUIRY_PRIORITY);
    	 sessionMap.put(WebConstants.INQUIRY_PRIORITY, ddl);
    	 
     }
     
	

	private List<UnitView> addUsersToView() {
		String methodName = "addUsersToView";
		List<UnitView> unitViewList = new ArrayList<UnitView>();
		List<UnitView> unitViewListSelected = new ArrayList<UnitView>();
		
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)){
			unitViewListSelected = (List<UnitView>) viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST);
			
			for (UnitView uV:unitViewListSelected){
				
				UnitView unitView = new UnitView();
				unitView.setUnitId(uV.getUnitId());
				unitViewList.add(unitView);
			}

		}
		
//		groupofUsers.size();
		//,1,2,3,4
		// UnitView unitView=new UnitView();
		//String delimiters = ",";
	//	String[] userIds = hdnUnitIds.split(delimiters);
/*		for (int i = 0; i < userIds.length; i++) {

			if (userIds[i] != null && !userIds[i].equals("")) {
				UnitView unitView = new UnitView();
				unitView.setUnitId(new Long(userIds[i]));
				unitViewList.add(unitView);

			}

		}*/
		

		return unitViewList;

	}

//	private String getLoggedInUser() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext()
//				.getSession(true);
//		String loggedInUser = "";
//
//		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
//			UserDbImpl user = (UserDbImpl) session
//					.getAttribute(WebConstants.USER_IN_SESSION);
//			loggedInUser = user.getLoginId();
//		}
//
//		return loggedInUser;
//	}

	private boolean putControlValuesInView() {
		String methodName = "putControlValuesInView";

           boolean isSucceed=true;
           boolean enter_field=true;
           String message ;

           

           errorMessages.clear();
           
           
          // selectedInquiryPriority= prioritySelectMenu.getSubmittedValue().toString();
         //  selectedInquiryMethod=inquiryMethodSelectMenu.getSubmittedValue().toString();
		
        /* if(viewRootMap.containsKey("INQUIRY_METHOD")){  
		           if (viewRootMap.get("INQUIRY_METHOD").toString()!=null
		        		&& !viewRootMap.get("INQUIRY_METHOD").toString().equals("")   
		        		&& !viewRootMap.get("INQUIRY_METHOD").toString().equals("-1"))
		        	   
				{
					
					inquiryView.setInquiryReceiveMethodId(new Long(viewRootMap.get("INQUIRY_METHOD").toString()));
		
				}
         }*/
          if(this.getSelectOneInquiryMethod()!=null && !this.getSelectOneInquiryMethod().equals("")
        		  & !this.getSelectOneInquiryMethod().equals("-1")){
        	  inquiryView.setInquiryReceiveMethodId(new Long(this.getSelectOneInquiryMethod()));
          }
		else
		{
			message = ResourceUtil.getInstance().getProperty("inquiry.message.InquiryMethod");
			errorMessages.add(message);
			isSucceed=false;
			
		}
        /* if(viewRootMap.containsKey("INQUIRY_PRI")){  
		         if (viewRootMap.get("INQUIRY_PRI").toString()!=null
			        		&& !viewRootMap.get("INQUIRY_PRI").toString().equals("")   
			        		&& !viewRootMap.get("INQUIRY_PRI").toString().equals("-1"))
			        	   
					{
					inquiryView.setPriorityId(new Long(viewRootMap.get("INQUIRY_PRI").toString()));
		
				    }
         }  */
         if(this.getSelectOnePriority()!=null && !this.getSelectOnePriority().equals("")
       		  & !this.getSelectOnePriority().equals("-1")){
        	 inquiryView.setPriorityId(new Long(this.getSelectOnePriority()));
         }
		else
		{
			message = ResourceUtil.getInstance().getProperty("inquiry.message.InquiryPriority");
			errorMessages.add(message);
			isSucceed=false;
			
		}
	
        List<UnitView> unitViewList = addUsersToView();
		if (unitViewList.size() > 0) 
		{
			inquiryView.setUnitView(unitViewList);
			enter_field=false; 
		}
		/*else
		{
			errorMessages.add("Please select any user");
			isSucceed=false;
		}*/
			
		if(viewRootMap.containsKey(WebConstants.LOCAL_PERSON_ID)){	
			if (viewRootMap.get(WebConstants.LOCAL_PERSON_ID).toString()!=null
				&&!viewRootMap.get(WebConstants.LOCAL_PERSON_ID).toString().equals("")) {
				
				PersonView personView = new PersonView();
				personView.setPersonId(new Long(viewRootMap.get(WebConstants.LOCAL_PERSON_ID).toString()));
				inquiryView.setPersonView(personView);
	
			}
		}	
		else
		{
			message = ResourceUtil.getInstance().getProperty("inquiry.message.PersonRequired");
			errorMessages.add(message);
			isSucceed=false;
			
		}
/*		if (inquiryDate != null && !inquiryDate.equals("")) {
			
			 * Calendar cal=new GregorianCalendar(); cal.
			 
			inquiryView.setInquiriesDate(inquiryDate);

		}
*/		
		if(viewRootMap.containsKey("INQUIRY_DATE")){
			inquiryView.setInquiriesDate((Date)viewRootMap.get("INQUIRY_DATE"));
				}
		else
		{
			message = ResourceUtil.getInstance().getProperty("inquiry.message.InquiryDate");
			errorMessages.add(message);
			isSucceed=false;
			return isSucceed;
		}
		
		
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString()!=null
				&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString().equals("")
				&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString().equals("-1")){
			inquiryView.setUnitType(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString()));
			enter_field=false;
			                 }
				}
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString().equals("")
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString().equals("-1")){
			inquiryView.setUnitUsageTypeId(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString()));
			enter_field=false;
			}
		}	
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString().equals("")){
			inquiryView.setUnitNumber(viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString());
			enter_field=false;
			}
		}	
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString().equals("")){
			inquiryView.setRentValueMin(new Double(viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString()));
			enter_field=false;
			}
		}
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString().equals("")){
			inquiryView.setRentValueMax(new Double(viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString()));
			enter_field=false;
			}
		}	
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString().equals("")){
			inquiryView.setNoOfBedsMax(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString()));
			enter_field=false;
			}
		}
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString().equals("")){
			inquiryView.setNoOfBedsMin(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString()));
			enter_field=false;
			}
		}	

		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString().equals("")
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString().equals("-1")){
			inquiryView.setCountryId(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString()));
			enter_field=false;
			}
		}	
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString().equals("")
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString().equals("-1")){
			inquiryView.setStateId(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString()));
			enter_field=false;
			}
		}
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID)){
			if( viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString()!=null
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString().equals("")
					&& !viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString().equals("-1")){
			inquiryView.setCityId(new Long(viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString()));
			enter_field=false;
			}
		}
		if(enter_field){
			message = ResourceUtil.getInstance().getProperty("inquiry.message.fieldRequired");
			errorMessages.add(message);
			isSucceed=false;
		}
		
		
		inquiryView.setCreatedBy(getLoggedInUser());
		inquiryView.setCreatedOn(new Date());
		inquiryView.setUpdatedBy(getLoggedInUser());
		inquiryView.setUpdatedOn(new Date());
		inquiryView.setIsDeleted(new Long(0));
		inquiryView.setRecordStatus(new Long(1));
	

          return isSucceed;
	}

	public String btnAdd_Click() {

		String methodName = "btnAdd_Click";
		String eventOutCome = "failure";
		String s1;
		String message;
		DateFormat df  =new SimpleDateFormat(getDateFormat());
 
		logger.logInfo(methodName +"Starts:::::::::::");
		
        errorMessages = new ArrayList<String>();
        successMessages=new ArrayList<String>();
		
        try {
        	
        	if(ctlHdnPersonId !=null && ctlHdnPersonId.getSubmittedValue()!=null)
			 hdnPersonIds = ctlHdnPersonId.getSubmittedValue().toString();
        	if(ctlHdnUnitId !=null && ctlHdnUnitId.getSubmittedValue()!=null)
        	   hdnUnitIds = ctlHdnUnitId.getSubmittedValue().toString();
			//inquiryDate =df.parse(richCalendar.getSubmittedValue().toString()); 
			if(putControlValuesInView())
			{
				PropertyServiceAgent myPort = new PropertyServiceAgent();
			  
				logger.logInfo("addInquiry Starts:::::::::::");
				myPort.addInquiry(inquiryView);
				logger.logInfo("addInquiry Ends Successfully:::::::::::");
				CommonUtil.saveAttachments(inquiryView.getInquiryId());
	            CommonUtil.saveComments(inquiryView.getInquiryId(), WebConstants.REQUEST);
	            CommonUtil.loadAttachmentsAndComments(inquiryView.getInquiryId().toString());
				message = ResourceUtil.getInstance().getProperty("inquiry.message.booked");
		    	successMessages.add(message);
		    	successMessages.add( ResourceUtil.getInstance().getProperty("cancelContract.message.requestaccepted", (Object) inquiryView.getRequestNumber()) );
                
//		    	// refrash form 
//	            this.setHdnPersonId("");
//	            viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID);
//	            this.setHdnPersonName("");
//	            viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME);
//	            this.setHdnCellNo("");
//	            viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL);
//	            this.setHdnPersonType("");
//	            this.setHdnIsCompany("");
//	            viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE);
//	            this.setSelectOnePriority("");
//	            this.setSelectOneInquiryMethod("");
//	            this.setSelectOnePriority("");
//	            this.setSelectOneUnitType("");
//	            this.setSelectOneUnitUsage("");
//	            this.setUnitNumber("");
//	            this.setCountryId("");
//	            this.setStateId("");
//	            this.setCityId("");
//	            this.setRentValueMin("");
//	            this.setRentValueMax("");
//	            this.setNoOfBedsMin("");
//	            this.setNoOfBedsMax("");
//	            
//
//	            viewRootMap.clear();
	            
	            setInquiryDate(new Date());
	            this.getInquiryDate();
	            
	            
	            
	            
	            
	            hdnUnitIds = null;
	            ctlHdnUnitId.setValue(null);
	            ctlHdnUnitId.setSubmittedValue(null);
	            
				eventOutCome = "";
			}
			
			logger.logInfo(methodName +"Ends:::::::::::");

		} catch (Exception ex) {
			saveButton.setRendered(true);
			openUnitListButton.setRendered(true);
			logger.logInfo(methodName +"Crashed:::::::::::");
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			ctlHdnUnitId.setValue(null);
            ctlHdnUnitId.setSubmittedValue(null);
			ex.printStackTrace();
		}

		return eventOutCome;

	}


	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}

	public String getSuccessMessages() {
		return commonUtil.getErrorMessages(this.successMessages);
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	
	

	public HtmlSelectOneMenu getInquiryMethodSelectMenu() {
		return inquiryMethodSelectMenu;
	}

	public void setInquiryMethodSelectMenu(
			HtmlSelectOneMenu inquiryMethodSelectMenu) {
		this.inquiryMethodSelectMenu = inquiryMethodSelectMenu;
	}

	public HtmlSelectOneMenu getPrioritySelectMenu() {
		return prioritySelectMenu;
	}

	public void setPrioritySelectMenu(HtmlSelectOneMenu prioritySelectMenu) {
		this.prioritySelectMenu = prioritySelectMenu;
	}

	public List<SelectItem> getInquiryMethod() 
	{
		 inquiryMethod.clear();
		 try
		 {
		 if(sessionMap.containsKey(WebConstants.INQUIRY_METHOD))
			 loadInquiryMethodList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.INQUIRY_METHOD);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
			  inquiryMethod.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		return inquiryMethod;
	}

	public void setInquiryMethod(List<SelectItem> inquiryMethod) {
		this.inquiryMethod = inquiryMethod;
	}

	public List<SelectItem> getPriority() {
		priority.clear();
		/*SelectItem item = new SelectItem("25001", "Low");
		priority.add(item);
		item = new SelectItem("25002", "Medium");
		priority.add(item);
		item = new SelectItem("25003", "High");
		priority.add(item);*/
		
		try
		 {
		 if(sessionMap.containsKey(WebConstants.INQUIRY_PRIORITY))
			 loadInquiryMethodList();
		 
		  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.INQUIRY_PRIORITY);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=(DomainDataView)ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      priority.add(item);
		  }
		  
		 }
		 catch(Exception ex)
		 {
			 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		 }
		return priority;
	}

	public void setPriority(List<SelectItem> priority) {
		this.priority = priority;
	}

	public List<SelectItem> getGroupofUsers() {
		return groupofUsers;
	}

	public void setGroupofUsers(List<SelectItem> groupofUsers) {
		this.groupofUsers = groupofUsers;
	}

	public String getHdnUnitIds() {
		return hdnUnitIds;
	}

	public void setHdnUnitIds(String hdnUnitIds) {
		this.hdnUnitIds = hdnUnitIds;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getHdnPersonIds() {
		return hdnPersonIds;
	}

	public void setHdnPersonIds(String hdnPersonIds) {
		this.hdnPersonIds = hdnPersonIds;
	}

	public Date getInquiryDate() {
		if(viewRootMap.containsKey("INQUIRY_DATE"))
			inquiryDate=(Date) viewRootMap.get("INQUIRY_DATE");
		return inquiryDate;
		
	}

	public void setInquiryDate(Date inquiryDate) {
		this.inquiryDate = inquiryDate;
		if(this.inquiryDate!=null)
			viewRootMap.put("INQUIRY_DATE",this.inquiryDate);
	}

	public HtmlInputHidden getCtlHdnUnitId() {
		return ctlHdnUnitId;
	}

	public void setCtlHdnUnitId(HtmlInputHidden ctlHdnUnitId) {
		this.ctlHdnUnitId = ctlHdnUnitId;
	}

	public HtmlInputHidden getCtlHdnPersonId() {
		return ctlHdnPersonId;
	}

	public void setCtlHdnPersonId(HtmlInputHidden ctlHdnPersonId) {
		this.ctlHdnPersonId = ctlHdnPersonId;
	}

   public HtmlInputHidden getCtlInquiryDate() {
		return ctlInquiryDate;
	}

	public void setCtlInquiryDate(HtmlInputHidden ctlInquiryDate) {
		this.ctlInquiryDate = ctlInquiryDate;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}


	
	public String btnPerson_Click()
	{
		return "InterestedPerson";
		
	}
	public HtmlInputHidden getCtlHdnUnitAreaMin() {
		return ctlHdnUnitAreaMin;
	}
	public void setCtlHdnUnitAreaMin(HtmlInputHidden ctlHdnUnitAreaMin) {
		this.ctlHdnUnitAreaMin = ctlHdnUnitAreaMin;
	}
	public HtmlInputHidden getCtlHdnUnitAreaMax() {
		return ctlHdnUnitAreaMax;
	}
	public void setCtlHdnUnitAreaMax(HtmlInputHidden ctlHdnUnitAreaMax) {
		this.ctlHdnUnitAreaMax = ctlHdnUnitAreaMax;
	}
	public HtmlInputHidden getCtlHdnLivingMax() {
		return ctlHdnLivingMax;
	}
	public void setCtlHdnLivingMax(HtmlInputHidden ctlHdnLivingMax) {
		this.ctlHdnLivingMax = ctlHdnLivingMax;
	}
	public HtmlInputHidden getCtlHdnBedsMax() {
		return ctlHdnBedsMax;
	}
	public void setCtlHdnBedsMax(HtmlInputHidden ctlHdnBedsMax) {
		this.ctlHdnBedsMax = ctlHdnBedsMax;
	}
	public HtmlInputHidden getCtlHdnBathMax() {
		return ctlHdnBathMax;
	}
	public void setCtlHdnBathMax(HtmlInputHidden ctlHdnBathMax) {
		this.ctlHdnBathMax = ctlHdnBathMax;
	}
	public HtmlInputHidden getCtlHdnRentMin() {
		return ctlHdnRentMin;
	}
	public void setCtlHdnRentMin(HtmlInputHidden ctlHdnRentMin) {
		this.ctlHdnRentMin = ctlHdnRentMin;
	}
	public HtmlInputHidden getCtlHdnRentMax() {
		return ctlHdnRentMax;
	}
	public void setCtlHdnRentMax(HtmlInputHidden ctlHdnRentMax) {
		this.ctlHdnRentMax = ctlHdnRentMax;
	}
	public HtmlInputHidden getCtlHdnFloorMin() {
		return ctlHdnFloorMin;
	}
	public void setCtlHdnFloorMin(HtmlInputHidden ctlHdnFloorMin) {
		this.ctlHdnFloorMin = ctlHdnFloorMin;
	}
	public HtmlInputHidden getCtlHdnObjSelectUnitType() {
		return ctlHdnObjSelectUnitType;
	}
	public void setCtlHdnObjSelectUnitType(HtmlInputHidden ctlHdnObjSelectUnitType) {
		this.ctlHdnObjSelectUnitType = ctlHdnObjSelectUnitType;
	}
	public HtmlInputHidden getCtlHdnObjSelectUnitUsage() {
		return ctlHdnObjSelectUnitUsage;
	}
	public void setCtlHdnObjSelectUnitUsage(HtmlInputHidden ctlHdnObjSelectUnitUsage) {
		this.ctlHdnObjSelectUnitUsage = ctlHdnObjSelectUnitUsage;
	}
	public HtmlInputHidden getCtlHdnObjSelectUnitSide() {
		return ctlHdnObjSelectUnitSide;
	}
	public void setCtlHdnObjSelectUnitSide(HtmlInputHidden ctlHdnObjSelectUnitSide) {
		this.ctlHdnObjSelectUnitSide = ctlHdnObjSelectUnitSide;
	}
	public void cancel(ActionEvent event) {
		//logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
	
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	
			}
			catch (Exception exception) {
			//	logger.LogException("cancel() crashed ", exception);
			}
    }

	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	public org.richfaces.component.html.HtmlCalendar getRichCalendar() {
		return richCalendar;
	}
	public void setRichCalendar(
			org.richfaces.component.html.HtmlCalendar richCalendar) {
		this.richCalendar = richCalendar;
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public HtmlInputText getTxtName() {
		return txtName;
	}

	public void setTxtName(HtmlInputText txtName) {
		this.txtName = txtName;
	}

	public String getName() {
		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME))
			name=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME).toString();
		return name;
	}

	public void setName(String name) {
		this.name = name;
		if(this.name!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,this.name);
	}

	
	public HtmlInputText getTxtCellNo() {
		return txtCellNo;
	}

	public void setTxtCellNo(HtmlInputText txtCellNo) {
		this.txtCellNo = txtCellNo;
		
	}

	public String getCellNo() {
		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL))
			cellNo=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL).toString();
			return cellNo;
	}

	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
		if(this.cellNo!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,this.cellNo);
	}

	public HtmlInputText getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(HtmlInputText txtEmail) {
		this.txtEmail = txtEmail;
	}

	public String getEmail() {
		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL))
			email=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL).toString();
		return email;
	}

	public void setEmail(String email) {
		
		this.email = email;
		if(this.email!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL,this.email);
	}
	
	public HtmlInputText getTxtApplicantType() {
		return txtApplicantType;
	}

	public void setTxtApplicantType(HtmlInputText txtApplicantType) {
		this.txtApplicantType = txtApplicantType;
	}


	public String getApplicantType() {
		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE))
			applicantType=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE).toString();
		return applicantType;
	}

	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
		if(this.applicantType!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,this.applicantType);
		
	}
	public String getHdnPersonId() {
		return hdnPersonId;
	}
	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}
	public String getHdnPersonName() {
		return hdnPersonName;
	}
	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}
	public String getHdnPersonType() {
		return hdnPersonType;
	}
	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}
	public String getHdnCellNo() {
		return hdnCellNo;
	}
	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}
	public String getHdnIsCompany() {
		return hdnIsCompany;
	}
	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}
	 private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	    {
	    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
	    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
			Iterator<DomainTypeView> itr = list.iterator();
			//Iterates for all the domain Types
			while( itr.hasNext() ) 
			{
				DomainTypeView dtv = itr.next();
				if( dtv.getTypeName().compareTo(domainType)==0 ) 
				{
					Set<DomainDataView> dd = dtv.getDomainDatas();
					//Iterates over all the domain datas
					for (DomainDataView ddv : dd) 
					{
					  ddvList.add(ddv);	
					}
					break;
				}
			}
	    	
	    	return ddvList;
	    }
    	public void setEnglishLocale(boolean isEnglishLocale) {
			this.isEnglishLocale = isEnglishLocale;
		}

		public void setArabicLocale(boolean isArabicLocale) {
			this.isArabicLocale = isArabicLocale;
		}

		public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
		
		public boolean getIsEnglishLocale()
		{
			WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
			return isEnglishLocale;
		}

		public String getSelectOneUnitType() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE))
				selectOneUnitType=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString();
			return selectOneUnitType;
		}

		public void setSelectOneUnitType(String selectOneUnitType) {
			this.selectOneUnitType = selectOneUnitType;
			if(this.selectOneUnitType !=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_TYPE,this.selectOneUnitType );
		}

		public String getSelectOneUnitUsage() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE))
				selectOneUnitUsage=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString();
			return selectOneUnitUsage;
			
		}

		public void setSelectOneUnitUsage(String selectOneUnitUsage) {
			this.selectOneUnitUsage = selectOneUnitUsage;
			if(this.selectOneUnitUsage!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_USAGE,this.selectOneUnitUsage );
		}

		public String getUnitNumber() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER))
				unitNumber=viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString();
			return unitNumber;
		}

		public void setUnitNumber(String unitNumber) {
			this.unitNumber = unitNumber;
			if(this.unitNumber!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.UNIT_NUMBER,this.unitNumber );
			
		}

		public String getRentValueMin() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN))
				rentValueMin=viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString();
			return rentValueMin;
			
		}

		public void setRentValueMin(String rentValueMin) {
			this.rentValueMin = rentValueMin;
			if(this.rentValueMin!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN,this.rentValueMin );
			
		}

		public String getRentValueMax() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX))
				rentValueMax=viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString();
			return rentValueMax;
		}

		public void setRentValueMax(String rentValueMax) {
			this.rentValueMax = rentValueMax;
			if(this.rentValueMax!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX,this.rentValueMax );
		
		}

		public String getNoOfBedsMax() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX))
				noOfBedsMax=viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString();
			return noOfBedsMax;
		}

		public void setNoOfBedsMax(String noOfBedsMax) {
			
			this.noOfBedsMax = noOfBedsMax;
			if(this.noOfBedsMax!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX,this.noOfBedsMax );
		}

		public String getNoOfBedsMin() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN))
				noOfBedsMin=viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString();
			return noOfBedsMin;
		}

		public void setNoOfBedsMin(String noOfBedsMin) {
			
			this.noOfBedsMin = noOfBedsMin;
			if(this.noOfBedsMin!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN,this.noOfBedsMin );
			
		}

		public String getCountryId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID))
				countryId=viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString();
			return countryId;
		}

		public void setCountryId(String countryId) {
			this.countryId = countryId;
			if(this.countryId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.COUNTRY_ID,this.countryId);
		}

		public String getStateId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID))
				stateId=viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString();
			return stateId;
		}

		public void setStateId(String stateId) {
			this.stateId = stateId;
			if(this.stateId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.STATE_ID,this.stateId);
		}

		public String getCityId() {
			if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID))
				cityId=viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString();
			return cityId;
		}

		public void setCityId(String cityId) {
			this.cityId = cityId;
			if(this.cityId!=null)
				viewRootMap.put(WebConstants.INQUIRY_UNITS.CITY_ID,this.cityId);
		}

		public List<SelectItem> getCountryList() {
			if(viewRootMap.containsKey("countryList")){
				//countryList.clear();
				countryList= (List<SelectItem>) (viewRootMap.get("countryList"));
			}
			return countryList;
			
		}

		public void setCountryList(List<SelectItem> countryList) {
			this.countryList = countryList;
		}

		public List<SelectItem> getStateList() {
			if(viewRootMap.containsKey("stateList")){
				//stateList.clear();
				stateList= (List<SelectItem>) (viewRootMap.get("stateList"));
			}
			return stateList;	
			
			
		}

		public void setStateList(List<SelectItem> stateList) {
			this.stateList = stateList;
			
		}

		public List<SelectItem> getCityList() {
			if(viewRootMap.containsKey("cityList")){
				//cityList.clear();
				cityList= (List<SelectItem>) (viewRootMap.get("cityList"));
				
			}
			return cityList;	
			
		}

		public void setCityList(List<SelectItem> cityList) {
			this.cityList = cityList;
			
		}

		
		 public void loadCountry()  {
				
			  String methodName="loadCountry"; 
				logger.logInfo(methodName+"|"+"Start");
				try {
					if(viewRootMap.containsKey("countryList")){
						viewRootMap.remove("countryList");
						countryList.clear();
					}
				PropertyServiceAgent psa = new PropertyServiceAgent();
				List <RegionView> regionViewList = psa.getCountry();
				
				
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)regionViewList.get(i);
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
					  else 
						  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      
					
				      this.getCountryList().add(item);
				     
				  }
				
				 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("countryList", countryList);
				 logger.logInfo(methodName+"|"+"Finish");
				}catch (Exception e){
					logger.LogException(methodName+"|Error Occured ",e);
					
				}
			}
		  
			public void loadState()  {
				
				String methodName="loadState"; 
				logger.logInfo(methodName+"|"+"Start");
				try {
				PropertyServiceAgent psa = new PropertyServiceAgent();
				
				if(viewRootMap.containsKey("stateList")){
					viewRootMap.remove("stateList");
					stateList.clear();
				}
				
				String selectedCountryName = "";
				for (int i=0;i<this.getCountryList().size();i++)
				{
					if (new Long(countryId)==(new Long(this.getCountryList().get(i).getValue().toString())).longValue())
					{
						selectedCountryName = 	this.getCountryList().get(i).getLabel();
					}
				}	
				
				List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,getIsArabicLocale());
				
				
				for(int i=0;i<regionViewList.size();i++)
				  {
					  RegionView rV=(RegionView)regionViewList.get(i);
					  SelectItem item;
					  if (getIsEnglishLocale())
					  {
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
					  else 
						 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
				      this.getStateList().add(item);
				      
				      
				  }
				
				 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("stateList", stateList);
				 logger.logInfo(methodName+"|"+"Finish");
				}catch (Exception e){
					
					logger.LogException(methodName+"|Error Occured ",e);
					
				}
			}
			
			public void  loadCity(){
				String methodName="loadCity"; 
				logger.logInfo(methodName+"|"+"Start"); 
				PropertyServiceAgent psa = new PropertyServiceAgent();
				
				if(viewRootMap.containsKey("cityList")){
					viewRootMap.remove("cityList");
					cityList.clear();
				}
					
					try 
					{
						
						 Set<RegionView> regionViewList = null;
						 if(stateId!=null && new Long(stateId).compareTo(new Long(-1))!=0)
						 regionViewList =  psa.getCity(new Long(stateId));
					          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
				  	    
						
						Iterator itrator = regionViewList.iterator();

						for(int i=0;i<regionViewList.size();i++)
						  {
							  RegionView rV=(RegionView)itrator.next();
							  SelectItem item;
							  if (getIsEnglishLocale())
							  {
								 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
							  else 
								 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
						      this.getCityList().add(item);
						      
						      
						  }
					
						 FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("cityList", cityList);
						 logger.logInfo(methodName+"|"+"Finish");
					}
					catch(Exception ex)
					{
						logger.LogException(methodName+"|Error Occured ",ex);
						
					}
				   
			   }
			
			public String openUnitListPopUp()
			{
				String methodName="openReceivePaymentsPopUp";
				logger.logInfo(methodName+"|"+"Start..");
				
				
					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_TYPE))
				        sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_TYPE,viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_TYPE).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_USAGE))
						sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_USAGE,viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_USAGE).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_NUMBER))
						sessionMap.put(WebConstants.INQUIRY_UNITS.UNIT_NUMBER,viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_NUMBER).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN))
						sessionMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN,viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MIN).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX))
						sessionMap.put(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX,viewRootMap.get(WebConstants.INQUIRY_UNITS.RENT_VALUE_MAX).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX))
						sessionMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX,viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MAX).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN))
						sessionMap.put(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN,viewRootMap.get(WebConstants.INQUIRY_UNITS.NO_OF_BEDS_MIN).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.COUNTRY_ID))
						sessionMap.put(WebConstants.INQUIRY_UNITS.COUNTRY_ID,viewRootMap.get(WebConstants.INQUIRY_UNITS.COUNTRY_ID).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.STATE_ID))
						sessionMap.put(WebConstants.INQUIRY_UNITS.STATE_ID,viewRootMap.get(WebConstants.INQUIRY_UNITS.STATE_ID).toString());

					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.CITY_ID))
						sessionMap.put(WebConstants.INQUIRY_UNITS.CITY_ID,viewRootMap.get(WebConstants.INQUIRY_UNITS.CITY_ID).toString());
					
					if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)){
						viewRootMap.remove(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST);
						getPropertyInquiryDataList();	
					}
					

		        String extrajavaScriptText ="var screen_height = screen.height;"+
		        "var screen_width = screen.width;"+
                "window.open('UnitList.jsf?','_blank','width='+(screen_width-10)+',height='+(screen_height-150)+',left=0,top=40,scrollbars=yes,status=yes');";

				        
				openPopUp("UnitList.jsf", extrajavaScriptText);
		    	   
				logger.logInfo(methodName+"|"+"Finish..");
		    	
				return "";
			}
			
			public String openPopUp(String URLtoOpen,String extraJavaScript)
			{
				String methodName="openPopUp";
				final String viewId = "/InquiryPropertyList.jsp";
				logger.logInfo(methodName+"|"+"Start..");
				FacesContext facesContext = FacesContext.getCurrentInstance();
				ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
				String actionUrl = viewHandler.getActionURL(facesContext, viewId);
				String javaScriptText ="var screen_width = screen.width;"+
				"var screen_height = screen.height;"+
				"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-100)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=yes,status=yes');";
		      if (extraJavaScript!=null&& extraJavaScript.trim().length()>0)      
		    		javaScriptText=extraJavaScript;

				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
				logger.logInfo(methodName+"|"+"Finish..");
				return "";
			}
			
		public List<UnitView> getPropertyInquiryDataList()
			{
				 Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
					if(viewMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST))	{
						  dataList= (ArrayList)viewMap.get(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST);
					
					recordSize = ((List<UnitView>)viewMap.get(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)).size();
					viewMap.put("recordSize", recordSize);
					paginatorRows = getPaginatorRows();
					}else {
						// when search btn press 
						dataList = new ArrayList<UnitView>();
						recordSize = dataList.size();
						viewMap.put("recordSize", recordSize);
						paginatorRows = getPaginatorRows();
					}
				
				return dataList;
			}
		
		public List<UnitView> getDataList() {
			return dataList;
		}
		public void setDataList(List<UnitView> dataList) {
			this.dataList = dataList;
		}

		public HtmlDataTable getDataTable() {
			return dataTable;
		}

		public void setDataTable(HtmlDataTable dataTable) {
			this.dataTable = dataTable;
		}
		
		public Integer getPaginatorRows() {
			paginatorRows = 10;
			return paginatorRows;
		}

		public void setPaginatorRows(Integer paginatorRows) {
			this.paginatorRows = paginatorRows;
		}

		public Integer getRecordSize() {
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			recordSize = (Integer) viewMap.get("recordSize");
			if(recordSize==null)
				recordSize = 0;
			return recordSize;
		}

		public void setRecordSize(Integer recordSize) {
			this.recordSize = recordSize;
		}
		
		public Integer getPaginatorMaxPages() {

			return WebConstants.SEARCH_RESULTS_MAX_PAGES;

			}

		public void setPaginatorMaxPages(Integer paginatorMaxPages) {
			this.paginatorMaxPages = paginatorMaxPages;
		}

		
		public String getSelectOneInquiryMethod() {
			if(viewRootMap.containsKey("INQUIRY_METHOD"))
				selectOneInquiryMethod=viewRootMap.get("INQUIRY_METHOD").toString();
			return selectOneInquiryMethod;
			
		}

		public void setSelectOneInquiryMethod(String selectOneInquiryMethod) {
			this.selectOneInquiryMethod = selectOneInquiryMethod;
			if(this.selectOneInquiryMethod!=null)
				viewRootMap.put("INQUIRY_METHOD",this.selectOneInquiryMethod);
			
		}

		public String getSelectOnePriority() {
			if(viewRootMap.containsKey("INQUIRY_PRI"))
				selectOnePriority=viewRootMap.get("INQUIRY_PRI").toString();
			return selectOnePriority;
			
		}

		public void setSelectOnePriority(String selectOnePriority) {
			
			this.selectOnePriority = selectOnePriority;
			if(this.selectOnePriority !=null)
				viewRootMap.put("INQUIRY_PRI",this.selectOnePriority );
		}

		public HtmlCommandButton getSaveButton() {
			return saveButton;
		}

		public void setSaveButton(HtmlCommandButton saveButton) {
			this.saveButton = saveButton;
		}

		public HtmlCommandButton getOpenUnitListButton() {
			return openUnitListButton;
		}

		public void setOpenUnitListButton(HtmlCommandButton openUnitListButton) {
			this.openUnitListButton = openUnitListButton;
		}
        
	  public Boolean getEnableDisableControl(){
            
			// if list is populated than disable all controls
		  if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)
				 && ((ArrayList)viewRootMap.get(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)).size()>0){
		  		return true;
		  	}else{
			  return false;  
		  }
		  
		  
	  }

	public HtmlCommandButton getClearUnitListButton() {
		return clearUnitListButton;
	}

	public void setClearUnitListButton(HtmlCommandButton clearUnitListButton) {
		this.clearUnitListButton = clearUnitListButton;
	}
	
	public String clearUnits (){
		
		if(viewRootMap.containsKey(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST)){
			viewRootMap.remove(WebConstants.INQUIRY_UNITS.UNIT_VIEW_LIST);
			getPropertyInquiryDataList();
			getEnableDisableControl();
		}
		
		return "clearUnits";
	}

	public String getSelectOneApplicationStatus() {
		return selectOneApplicationStatus;
	}

	public void setSelectOneApplicationStatus(String selectOneApplicationStatus) {
		this.selectOneApplicationStatus = selectOneApplicationStatus;
	}

	public Map<String, List<SelectItem>> getListMap() {
		return listMap;
	}

	public void setListMap(Map<String, List<SelectItem>> listMap) {
		this.listMap = listMap;
	}

	public List<SelectItem> getEmptyList() {
		return emptyList;
	}

	public void setEmptyList(List<SelectItem> emptyList) {
		this.emptyList = emptyList;
	}
	
	public String loadPerson() {
		
		if ( hdnPersonId != null && !hdnPersonId.equalsIgnoreCase("") ) 
		{
			try 
			{
				PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
				PersonView personView = propertyServiceAgent.getPersonInformation( Long.parseLong( hdnPersonId ) );
				
				personNumber = personView.getPersonalSecCardNo();				
				passportNumber = personView.getPassportNumber();
				company = personView.getCompanyName();
				profession = personView.getDesignation();
				
				if ( personView.getContactInfoViewSet() != null && personView.getContactInfoViewSet().size() > 0 )
				{
					for ( ContactInfoView contantInfoView : personView.getContactInfoViewSet() )
					{
						phoneNumber = contantInfoView.getHomePhone();
						email = contantInfoView.getEmail();
						poBox = contantInfoView.getPostCode();
					}
				}
				
				if ( personView.getPassportIssueDate() != null )
					passportIssueDate = DateUtil.convertToDisplayDate( personView.getPassportIssueDate() );
				
				if ( personView.getPassportExpiryDate() != null )
					passportExpiryDate = DateUtil.convertToDisplayDate( personView.getPassportExpiryDate() ); 
			}
			catch (Exception exception) 
			{
				logger.LogException(" --- loadPerson() --- CRASHED", exception);
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String openUnitSearchPopUp() 
	{
		List<UnitView> alreadySelectedUnitViewList = getPropertyInquiryDataList();
		sessionMap.put(WebConstants.ALREADY_SELECTED_PROPERTY_INQUIRY_UNITS, alreadySelectedUnitViewList);
		
		String javaScriptText = "javascript:openUnitSearchPopup();";
        AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
        addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, javaScriptText);
		
		return null;
	}
	
	public String onUnitDelete() 
	{
		UnitView unitViewToDelete = (UnitView) dataTable.getRowData();
		getPropertyInquiryDataList().remove( unitViewToDelete );
		return null;
	}

	public String getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
}
