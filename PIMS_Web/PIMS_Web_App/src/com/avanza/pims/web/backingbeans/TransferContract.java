package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlOutputLabel;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.custom.column.HtmlSimpleColumn;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTab;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.core.web.context.impl.ViewContextImpl;
import com.avanza.notification.api.ContactInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.EvacuationServiceAgent;
import com.avanza.pims.business.services.GRPCustomerServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.proxy.pimstransfercontractbpelproxy.PIMSTransferContractBPELPortClient;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.SettlementReportCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContractUnitView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PaymentConfigurationView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestKeyView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.RequiredDocumentsView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.ui.util.ResourceUtil;

public class TransferContract extends AbstractController 
{
/**
	 * 
	 */
	private static final long serialVersionUID = 3643403541320602969L;

	CommonUtil commUtil  = new CommonUtil();
	//Transfer Details Tab
	private HtmlInputText newTenantName = new HtmlInputText();
	private HtmlInputText county = new HtmlInputText();
	private HtmlInputText address = new HtmlInputText();
	private HtmlInputText emirate = new HtmlInputText();
	private HtmlInputText cellNo = new HtmlInputText();
	private HtmlInputText email = new HtmlInputText();
	private HtmlInputText oldRentValue =new HtmlInputText();
	private HtmlInputText newRentAmount = new HtmlInputText();
	private Date evacuationDate;
	private Date newStartDate;
	private String readOnlyStyleClass;
	private Date newEndDate ;
	private Boolean renderAddTenantLink = new Boolean(true);
	private Boolean renderViewNewTenantLink = new Boolean(true);
	private Boolean renderAddNewTenantBasicInfoLink = new Boolean(true);
	private Boolean renderPaymentButtons = new Boolean(true);
	@SuppressWarnings( "unchecked" )
	HashMap feesConditionsMap=new HashMap();
	private HtmlInputText comments = new HtmlInputText();
	//Application Detail Tab
	private String hdnContractId;
	private String hdnContractNumber;
	private String hdnTenantId;
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnTDPersonId;
	private String hdnTDPersonName;
	private String hdnTDPersonType;
	private String hdnTDCellNo;
	private String hdnTDIsCompany;
	private Boolean initiationTask;
	private Boolean showHeaderControls;
	private Boolean readonlyHeaderControls;
    private String DDV_COMMERCIAL_CONTRACT="DDV_COMMERCIAL_CONTRACT";
    private boolean  isCollectingNewCntrctPmnt;
	//Action Buttons
	private HtmlCommandButton reviewed = new HtmlCommandButton();
	private HtmlCommandButton approveReview = new HtmlCommandButton();
	private HtmlCommandButton verifySuggestion = new HtmlCommandButton();
	
	private HtmlCommandButton rejectReview = new HtmlCommandButton();
	private HtmlCommandButton collectAppFee = new HtmlCommandButton();
	private HtmlCommandButton cancelAppFee = new HtmlCommandButton();
	private HtmlCommandButton setteled = new HtmlCommandButton();
	private HtmlCommandButton collectOldContractPayment = new HtmlCommandButton();
	private HtmlCommandButton completeOldContractPaymentTask = new HtmlCommandButton();
	private HtmlCommandButton createNewLeaseContract = new HtmlCommandButton();
	private HtmlCommandButton collectNewContractPayment= new HtmlCommandButton();
	private HtmlCommandButton btnSave= new HtmlCommandButton();
	
	//Pending App Tab
	private HtmlDataTable pendingAppTable = new HtmlDataTable();
	private List<RequestView> requestViewList = new ArrayList<RequestView>(); 
	private List<PaymentScheduleView> paymentSchedules = new ArrayList<PaymentScheduleView>();
	private HtmlCommandButton requestStatus = new HtmlCommandButton();
	private double totalRentAmount;
	private double totalCashAmount;
	private double totalChqAmount;
	private int installments;
	private double totalDepositAmount;
	private double totalFees;
	private double totalFines;
	private HtmlDataTable dataTablePaymentSchedule;
	private transient Logger logger = Logger.getLogger(TransferContract.class);
	private HtmlCommandLink populateContract = new HtmlCommandLink();
	ContractView contractView = new ContractView();
	PersonView newTenantView = new PersonView();
	PersonView newTenantViewBasicInfo = new PersonView();
	RequestView requestView = new RequestView();
	ContractView newContractView = new ContractView();
	private Boolean requestStatusForColumn;
	private String dateFormatForDataTable;

	String totalPaymentsMsg = "";
	Integer noOfCashPayments = 0;
	Integer noOfChequePayments = 0;
	Double newRentValue = null;
	String selectedTab; 
	
	private final String noteOwner = WebConstants.REQUEST;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_TRANSFER_CONTRACT;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT;
	

	private static final String EVENT_TRANSFER_CONTRACT_NEW = "Event.TransferContract.NewContractCreation";
	private static final String EVENT_TRANSFER_CONTRACT_REQUEST_REJECTED = "Event.TransferContract.RequestRejected";
	private static final String EVENT_TRANSFER_CONTRACT_REQUEST_APPROVED = "Event.TransferContract.RequestApproved";

	private List<ContractUnitView> unitDataList = new ArrayList<ContractUnitView>();
	private HtmlDataTable unitDataTable = new HtmlDataTable();
	private ContractUnitView unitDataItem = new ContractUnitView();

	private HtmlDataTable tbl_PaymentSchedule;
	private PaymentScheduleView psDataItem=new PaymentScheduleView();
	private List<PaymentScheduleView> psDataList=new ArrayList<PaymentScheduleView>(0);

	private HtmlDataTable tbl_RequiredDocs;
	private RequiredDocumentsView rdDataItem=new RequiredDocumentsView();
	private List<RequiredDocumentsView> rdDataList=new ArrayList<RequiredDocumentsView>(0);

	private HtmlDataTable tblPayments;
	private TransferPaymentBean TransferPaymentBean = new TransferPaymentBean();
	private List<TransferPaymentBean> paymentBeanDataList = new ArrayList<TransferPaymentBean>(0);

	Boolean canIssueContract = false;

	public String pageMode="";
	private final String CONTRACT_MONTHS ="CONTRACT_MONTHS";
	private final String DEFAULT_TAB     ="DEFAULT_TAB";
	private final String PAGE_MODE       ="PAGE_MODE";
	private final String PAGE_MODE_NEW   ="PAGE_MODE_NEW";
	private final String REQUEST_VIEW    = "REQUEST_VIEW";
	// Modes by Wasif
	private final String PAGE_MODE_REVIEW = "PAGE_MODE_REVIEW";
	private final String PAGE_MODE_REVIEW_APPROVAL  = "PAGE_MODE_REVIEW_APPROVAL";
	private final String PAGE_MODE_COLLECT_APP_FEE= "PAGE_MODE_COLLECT_APP_FEE";
	private final String PAGE_MODE_COLLECT_APP_FEE_SETTLEMENT= "PAGE_MODE_SETTLED_APPROVED";
	private final String PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT= "PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT";
	private final String PAGE_MODE_CREATE_NEW_LEASE_CONTRACT= "PAGE_MODE_CREATE_NEW_LEASE_CONTRACT";
	private final String PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT="PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT";
	private final String PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT="PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT";
	private final String PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT = "PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT";
	private final String PAGE_MODE_COMPLETE_REQUEST = "PAGE_MODE_COMPLETE_REQUEST";


	//	Modes end started by Wasif	


	private final String PAGE_MODE_NEW_DONE ="PAGE_MODE_NEW_DONE";
	private final String PAGE_MODE_RENT_APPROVE ="PAGE_MODE_RENT_APPROVE";
	private final String PAGE_MODE_RENT_APPROVE_DONE ="PAGE_MODE_RENT_APPROVE_DONE";
	private final String PAGE_MODE_NEW_CONTRACT ="PAGE_MODE_NEW_CONTRACT";
	private final String PAGE_MODE_NEW_CONTRACT_DONE ="PAGE_MODE_NEW_CONTRACT_DONE";
	private final String PAGE_MODE_ACTION_PERFORMED ="PAGE_MODE_ACTION_PERFORMED";
	private final String PAGE_MODE_PAYMENT_COLLECTED = "PAGE_MODE_PAYMENT_COLLECTED";
	private final String PAGE_MODE_CONTRACT_VERIFIED = "PAGE_MODE_CONTRACT_VERIFIED";
	private final String PAGE_MODE_NEW_CONTRACT_APPROVE ="PAGE_MODE_NEW_CONTRACT_APPROVE";
	private final String PAGE_MODE_NEW_CONTRACT_APPROVE_DONE ="PAGE_MODE_NEW_CONTRACT_APPROVE_DONE";
	private final String PAGE_MODE_TRANSFER_FEE_COLLECTION ="PAGE_MODE_TRANSFER_FEE_COLLECTION";
	private final String PAGE_MODE_TRANSFER_FEE_COLLECTION_DONE ="PAGE_MODE_TRANSFER_FEE_COLLECTION_DONE";
	private final String PAGE_MODE_VIEW ="PAGE_MODE_VIEW";
	private static final String EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION = "EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION";
	
	private final String PAGE_MODE_SETTLED_APPROVED = "PAGE_MODE_SETTLED_APPROVED";
	private final String PAGE_MODE_SETTLED_APPROVED_DONE = "PAGE_MODE_SETTLED_APPROVED_DONE";
	private static final String SCREEN_MODE_POPUP = "SCREEN_MODE_POPUP";
	private static final String BEAN_PAYMENTS = "paymentsBean";


	private Boolean renderSettlementDate;
	private Boolean isDeletable;
	private Boolean disableSettlementDate;

	private HtmlCommandButton btnSubmit = new HtmlCommandButton();
	private HtmlCommandButton btnApprove = new HtmlCommandButton();
	private HtmlCommandButton btnReject = new HtmlCommandButton();
	private HtmlCommandButton btnPay = new HtmlCommandButton();
	private HtmlCommandButton btnComplete = new HtmlCommandButton();
	private HtmlCommandButton btnNewTenant = new HtmlCommandButton();
	private HtmlCommandButton btnSaveNewRent = new HtmlCommandButton();
	private HtmlCommandButton btnIssueContract = new HtmlCommandButton();
	private HtmlCommandButton btnNewContract = new HtmlCommandButton();
	private HtmlCommandButton btnPrintLetter = new HtmlCommandButton();


	private HtmlPanelGrid rentValuePanelGrid = new HtmlPanelGrid();
	private HtmlSimpleColumn unitStatusColumnEn = new HtmlSimpleColumn(); 
	private HtmlSimpleColumn unitStatusColumnAr = new HtmlSimpleColumn();
	private HtmlSimpleColumn unitAddressColumn = new HtmlSimpleColumn();
	private HtmlSimpleColumn unitActionColumn = new HtmlSimpleColumn();
	private HtmlOutputLabel totalPaymentsHtmlLabel = new HtmlOutputLabel();
	private HtmlOutputLabel enterRentHtmlLabel = new HtmlOutputLabel();
	private HtmlOutputLabel clickActionColumnHtmlLabel = new HtmlOutputLabel();
	private HtmlSimpleColumn unitReadOnlyRentColumn = new HtmlSimpleColumn();
	private HtmlSimpleColumn newRentValueColumn = new HtmlSimpleColumn();
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlTab feesTab = new HtmlTab();
	private HtmlTab requestHistoryTab = new HtmlTab();
	private HtmlTab paymentsTab = new HtmlTab();

	private boolean forSaveMethod=false;
	private boolean forSubmitMethod=false;
	private boolean isShowEvacuationClndr=false;

	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();

	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	@SuppressWarnings( "unchecked" )
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	@SuppressWarnings( "unchecked" )
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	
	CommonUtil commonUtil = new CommonUtil();
	SystemParameters parameters = SystemParameters.getInstance();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	public void cmdShowRequestDetails(){

	}
	public Integer getRequestViewRecordSize(){

		return requestViewList.size();
	}

	public String getTcStatus(){
		
		DomainDataView ddv=CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS),WebConstants.CONTRACT_STATUS_TERMINATED
		);

		return "";
	}
	@Override
	@SuppressWarnings("unchecked")
	public void init()
	{
		logger.logInfo("init start...");
		super.init();
		verifySuggestion.setRendered(false);
		reviewed.setRendered(false);
		btnComplete.setRendered(false);
		completeOldContractPaymentTask.setRendered(false);
		errorMessages.clear();
		successMessages.clear();
		HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		try 
		{ 	
			initContext();
			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
			if(!isPostBack())
			{
				if(sessionMap.get(WebConstants.BACK_SCREEN)!=null)
				{
					viewMap.put(WebConstants.BACK_SCREEN, sessionMap.get(WebConstants.BACK_SCREEN));
					sessionMap.remove(WebConstants.BACK_SCREEN);					
				}
				clearSessionMap();
				CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
				if( request.getParameter("pageMode") != null )
					viewMap.put( SCREEN_MODE_POPUP , true );
				setMode();
				setValues();
				populatePaymentTab();
				loadPendingRequestDataList();
				// For fetching the transfer payments
				if(viewMap.get(PAGE_MODE).toString().trim().compareTo(PAGE_MODE_NEW)==0  ||
				   viewMap.get(PAGE_MODE).toString().trim().compareTo(PAGE_MODE_REVIEW_APPROVAL)==0)
				   getPaymentScheduleFromPaymentConfiguration();
			}
			else 
			{
				populateApplicantFromHidden();
				if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) 
						&&  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null
						&& (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY))
				{
					sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
					collectPayment();
				}

				//IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
				else if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
             		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
                )
                {
                	viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
             	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
                }
				//If receiving response from split payment
				fromSplitPaymentScheduleScreen();
		    }
			
			calculateSettlement();
			logger.logInfo("init completed successfully...");
		}
		catch (Exception ex) 
		{
			logger.LogException("init crashed!!!", ex);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void fromSplitPaymentScheduleScreen() throws Exception
	{
		if(sessionMap.remove("PAYMENT_SCHDULE_SPLIT_DONE")!= null )
		{
			pageMode=viewMap.get(PAGE_MODE).toString();
			//Split Functionality Will be available for old Contract Payments Only.
			if(pageMode.equalsIgnoreCase(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT))
			{
				contractView = getContractView();
				populatePaymentScheduleAgainstContract(contractView.getContractId());
			}
			else if (  pageMode.equalsIgnoreCase(PAGE_MODE_CREATE_NEW_LEASE_CONTRACT ) )
			{
				requestView = (RequestView)viewMap.get(REQUEST_VIEW);
				populatePaymentScheduleAgainstRequest(requestView.getRequestId());
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void getPaymentScheduleFromPaymentConfiguration()throws PimsBusinessException, Exception 
	{
		
		String newRentAmount = this.newRentAmount!= null && 
							   this.newRentAmount.getValue()!= null ?this.newRentAmount.getValue().toString():"0.0";
		List<PaymentScheduleView>  feesList =
												UtilityService.getDefaultPaymentsForTransferContract(
																										
																										getIsContractCommercial() ,
																										newRentAmount,
																										getIsEnglishLocale(),
																										this.getContractView().getContractUnit().getUnitView().getPropertyCategoryId()									
							
																									 );
			
		List<PaymentScheduleView> tempFeesList = new ArrayList<PaymentScheduleView>();
		for (PaymentScheduleView view : feesList)
		{
			view.setPaymentModeId(WebConstants.PAYMENT_SCHEDULE_MODE_CASH_ID);
			view.setSelected(true);
			tempFeesList.add(view);
		}
		if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) && 
				viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null&&
				tempFeesList.size()>0)
		{

			List<PaymentScheduleView> existingPaymentList = (ArrayList<PaymentScheduleView>)
			                                                         viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			existingPaymentList.addAll(tempFeesList);

			viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,existingPaymentList);
		}
		else if(tempFeesList.size()>0)
			viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,tempFeesList);
	}
	@SuppressWarnings( "unchecked" )
	private void populateApplicantFromHidden() {
		if(hdnPersonId!=null && !hdnPersonId.equals(""))
		{
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
		}

		if(hdnPersonName!=null && !hdnPersonName.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);


		if(hdnCellNo!=null && !hdnCellNo.equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,hdnCellNo);

		if(hdnIsCompany!=null && !hdnIsCompany.equals(""))
		{
			DomainDataView ddv = new DomainDataView();
			if(Long.parseLong(hdnIsCompany)==1L)
				ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
			else
				ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);

			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());

		}
	}
	@SuppressWarnings( "unchecked" )
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (ArrayList<DomainTypeView> ) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
    @SuppressWarnings("unchecked")
	private void loadPendingRequestDataList()
	{
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		Long contractId =null;
		if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW)){
			contractId = ((ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW)).getContractId();
		}
		if(getFacesContext().getExternalContext().getSessionMap().containsKey(WebConstants.Contract.CONTRACT_ID))
			contractId= (Long)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.Contract.CONTRACT_ID);

		if(viewMap.get("PENDING_REQUEST_DATA_LIST")!=null){
			requestViewList= (ArrayList<RequestView>)viewMap.get("PENDING_REQUEST_DATA_LIST");
		}
		else{
			try {
				if(contractId!=null&&contractId>0){
					requestViewList =propertyServiceAgent.getRequestsForContract(contractId);
					viewMap.put("PENDING_REQUEST_DATA_LIST",requestViewList);
				}
			} catch (PimsBusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}


	}
    @SuppressWarnings( "unchecked" )
	private Boolean generateNotification(String user, ContractView contractViewArg, String eventName) {
		Boolean success = false;
		try{
			logger.logInfo("generateNotification started...");
			logger.logInfo("User : %s | Contract Number : %s ", user, contractViewArg.getContractNumber());
			logger.logInfo("Event Name : %s ", eventName);
			Map<String, Object> dataMap = new HashMap<String, Object>();
			if(contractViewArg!=null){

				PersonView tenantView = contractViewArg.getTenantView();
				if(tenantView!=null)
				{
					dataMap.put("TENANT_NAME", tenantView.getPersonFullName());
					dataMap.put("CONTRACT_NO", contractViewArg.getContractNumber());
					dataMap.put("CONTRACT", contractViewArg);
				}
			}
			success = true;
			logger.logInfo("generateNotification completed successfully...");
		}
		catch(Exception exception){
			logger.LogException("generateNotification crashed...",exception);
		}
		return success;
	}
    @SuppressWarnings("unchecked")
	private void collectPayment()
	{
		logger.logInfo("CollectPayments|Start");
		try
		{
			logger.logInfo("CollectPayments|pageMode::"+viewMap.get(PAGE_MODE).toString());
			String pageMode =viewMap.get(PAGE_MODE).toString();
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			requestView = (RequestView)viewMap.get(REQUEST_VIEW);
			PropertyServiceAgent psa=new PropertyServiceAgent();
			if(pageMode.equals(PAGE_MODE_COLLECT_APP_FEE) || pageMode.equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)) 
			{
				prv.setRequestView(requestView);
			}

			//THE APPLICATION FEE and old contract rent amount SHOULD BE PAID BY OLD TENANT
			if(pageMode.equals(PAGE_MODE_COLLECT_APP_FEE) ||pageMode.equals(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT))
			{
			  prv.setPaidById(this.getContractView().getTenantView().getPersonId());
			}
			else if(pageMode.equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT))
			{
				newContractView = (ContractView)viewMap.get(WebConstants.Contract.NEW_CONTRACT_VIEW);
				prv.setPaidById(newContractView.getTenantView().getPersonId());
			}
			prv = psa.collectPayments(prv);
			saveAttachmentsComments();
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			if(viewMap.containsKey(PAGE_MODE)&&viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT))
			{
			    new GRPCustomerServiceAgent().sendPersonLocationToGRPForContractId(requestView.getRequestId(), newContractView.getContractId(), newContractView.getTenantView().getPersonId(), null);
				ApproveRejectTask(TaskOutcome.OK);
				CommonUtil.saveSystemComments(noteOwner, MessageConstants.TransferLeaseContract.MSG_NEW_PAYMENT_COLLECTED, requestView.getRequestId());
				viewMap.put("NEW_PAYMENT_COLLECTED", true);
				
			}
			else
			{
				ApproveRejectTask(TaskOutcome.COLLECT);
				CommonUtil.saveSystemComments(noteOwner, MessageConstants.TransferLeaseContract.MSG_OLD_PAYMENT_COLLECTED, requestView.getRequestId());
				viewMap.put("OLD_PAYMENT_COLLECTED", true);
			}
			CommonUtil.printPaymentReceipt("", "", prv.getPaymentReceiptId().toString(), getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.TRANSFER_REQUEST);
			if(pageMode.equals(PAGE_MODE_COLLECT_APP_FEE) ||pageMode.equals(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT))
			{
				getReceivedPaymentsAgainstContract(this.getContractView().getContractId());
			}
			else if(pageMode.equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT))
			{
				getReceivedPaymentsAgainstContract(newContractView.getContractId());
			}
			viewMap.put(PAGE_MODE,PAGE_MODE_PAYMENT_COLLECTED);
			successMessages = new ArrayList<String>(0);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
			logger.logInfo("CollectPayments|Finish...");
		}
		catch(Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("CollectPayments|Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	private void addPaymentSchedule(Long requestId)  throws Exception
	{
		List<PaymentScheduleView> listPaymentScheduleView =  this.getPaymentSchedules();
		DomainDataView ddv = CommonUtil.getIdFromType(
													  CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), 
				                                      WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING
				                                     );
		DomainDataView ddvExempted = CommonUtil.getIdFromType(
																CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), 
                												WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED
                											  );
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		Date date = new Date();
		for(PaymentScheduleView paymentScheduleView: listPaymentScheduleView)
		{
			paymentScheduleView.setRequestId(requestId);
			paymentScheduleView.setCreatedBy(CommonUtil.getLoggedInUser());
			paymentScheduleView.setCreatedOn(date);
			paymentScheduleView.setUpdatedBy(CommonUtil.getLoggedInUser());
			paymentScheduleView.setUpdatedOn(date);
			paymentScheduleView.setIsDeleted(new Long(0));
			paymentScheduleView.setRecordStatus(new Long(1));
			paymentScheduleView.setIsReceived("N");
			if( paymentScheduleView.getStatusId().longValue() != ddvExempted.getDomainDataId() )
			{
			 paymentScheduleView.setStatusId(ddv.getDomainDataId());
			 paymentScheduleView.setStatusEn(ddv.getDataDescEn());
			 paymentScheduleView.setStatusAr(ddv.getDataDescAr());
			}
		}
		pSAgent.addDeletePaymentScheduleForTransferContract( requestId, listPaymentScheduleView );
	}

	@SuppressWarnings( "unchecked" )
	private void markAllSystemPaymentScheduleAsDeleted()
    {
		  List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
		  List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
		  
		  if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
		   paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		  
		  for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
		  {
			  if(paymentScheduleView.getIsSystem()==null || paymentScheduleView.getIsSystem().compareTo(0L)==0)
			  {
				  newPaymentScheduleViewList.add(paymentScheduleView);    
			  }
			  
		  }
		  viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList);
    }
	
	public void onLoadUpdatedPayment() 
	{
		try 
		{
			markAllSystemPaymentScheduleAsDeleted();
			getPaymentScheduleFromPaymentConfiguration();
		} 
		catch (Exception e) 
		{
				errorMessages =  new ArrayList<String>(0);
				logger.LogException("onLoadUpdatedPayment |Error Occured...",e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
	
	
	public void btnTabPayment_Click()
	{
		onLoadUpdatedPayment();
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsContractCommercial()
	{
		boolean isContractCommercial=true;
		  DomainDataView ddv=new DomainDataView();
		if(!viewMap.containsKey(DDV_COMMERCIAL_CONTRACT))
		{
		   ddv = CommonUtil.getIdFromType(
				                          CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE),
				                          WebConstants.COMMERCIAL_LEASE_CONTRACT
				                          );
		   viewMap.put(DDV_COMMERCIAL_CONTRACT,ddv);
		}
		else 
		   ddv=(DomainDataView)viewMap.get(DDV_COMMERCIAL_CONTRACT);
		if(this.getContractView().getContractTypeId()!=null 
			&& this.getContractView().getContractTypeId().toString().equals(ddv.getDomainDataId().toString()))
			isContractCommercial=true;
		else
			isContractCommercial=false;
		
		return isContractCommercial;
	}


	
   @SuppressWarnings( "unchecked" )
	public void preprocess() 
	{
		super.preprocess();
		initContext();
		if(viewMap.containsKey(PAGE_MODE)&&viewMap.get(PAGE_MODE).equals(PAGE_MODE_ACTION_PERFORMED)){
			btnApprove.setRendered(false);
			btnReject.setRendered(false);
			renderPaymentButtons=false;
		}
		if(viewMap.containsKey(PAGE_MODE)&&viewMap.get(PAGE_MODE).equals(PAGE_MODE_PAYMENT_COLLECTED)){
			collectAppFee.setRendered(false);
			cancelAppFee.setRendered(false);
		}
		if(viewMap.containsKey(PAGE_MODE)&&viewMap.get(PAGE_MODE).equals(PAGE_MODE_CONTRACT_VERIFIED)){
			verifySuggestion.setRendered(false);
			btnReject.setRendered(false);
		}
		
	}
   @SuppressWarnings( "unchecked" )
	private void initContext() 
	{
		webContext = ApplicationContext.getContext().get(WebContext.class);
		requestContext = ApplicationContext.getContext().get(RequestContext.class);
		viewContext = ApplicationContext.getContext().get(ViewContext.class);

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		webContext = new WebContextImpl(req.getSession());
		requestContext = new RequestContextImpl(req);
		if (getFacesContext().getViewRoot() != null) {
			viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
			ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
		}
		ApplicationContext.getContext().add(WebContext.class.getName(), webContext);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void prerender() {
		String methodName="prerender|"; 
		logger.logInfo(methodName + " started...");
		super.prerender();
		if(sessionMap.containsKey("SETTLE_MESSAGE"))
		{
			successMessages.clear();
			successMessages.add(ResourceUtil.getInstance().getProperty(sessionMap.get("SETTLE_MESSAGE").toString()));
			sessionMap.remove("SETTLE_MESSAGE");
		}

		populateApplicantFromHidden();
		try
		{
			enableDisableButtons();
			if((this.getNewTenantView()==null || this.getNewTenantView().getPersonId()==null) )
			{
				if(this.getNewTenantViewBasicInfo()!=null && ( (this.getNewTenantViewBasicInfo().getFirstName()!=null &&
				   this.getNewTenantViewBasicInfo().getFirstName().trim().length()>0) || 
				   (this.getNewTenantViewBasicInfo().getCompanyName()!=null &&
				   this.getNewTenantViewBasicInfo().getCompanyName().trim().length()>0)) 
				)
				{
					
				  renderAddNewTenantBasicInfoLink=true;
				  renderViewNewTenantLink=false;
				}
				else
				{
					renderAddNewTenantBasicInfoLink=false;
					  renderViewNewTenantLink=false;
				}
			}
			else
			{
				renderAddNewTenantBasicInfoLink = false;
				renderViewNewTenantLink = true;
				
			}
			if(viewMap.containsKey(PAGE_MODE)&&viewMap.get(PAGE_MODE).equals(PAGE_MODE_CONTRACT_VERIFIED))
			{
				verifySuggestion.setRendered(false);
				btnReject.setRendered(false);
			}
			
			if( sessionMap.containsKey( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION ) )
				{
					collectNewContractPayment.setDisabled( true );
					collectOldContractPayment.setDisabled( true );
				}
			
			logger.logInfo(methodName + " completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed..." ,ex);
		}
	}

	@SuppressWarnings("unchecked")
	private void populateApplicationDataTab(RequestView requestView)throws Exception
	{
		String methodName="populateApplicationDataTab|";
        logger.logInfo(methodName+"Start");
		fillRequestViewInApplicationDetailsTab(requestView);
		logger.logInfo(methodName+"Finish");	
	}
	@SuppressWarnings("unchecked")
	private void fillRequestViewInApplicationDetailsTab(RequestView rV)throws Exception
	{
		String methodName="fillRequestViewInApplicationDetailsTab|";
        logger.logInfo(methodName+"Start");
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, rV.getRequestNumber());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?rV.getStatusEn():rV.getStatusAr());
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, rV.getRequestDate());
		if(rV.getDescription()!=null && !rV.getDescription().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, rV.getDescription());
		if(rV.getRequestId()!=null && !rV.getRequestId().equals(""))
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,rV.getRequestId());

		if(rV.getApplicantView()!=null && rV.getApplicantView().getPersonId()!=null)
		{

			if(rV.getApplicantView().getPersonId()!=null && !rV.getApplicantView().getPersonId().equals(""))
			{
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,rV.getApplicantView().getPersonId());
				hdnPersonId = rV.getApplicantView().getPersonId().toString();
			}
			if(rV.getApplicantView().getPersonFullName()!=null && rV.getApplicantView().getPersonFullName().trim().length()>0)
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,rV.getApplicantView().getPersonFullName());
				
			
			if(rV.getApplicantView().getCellNumber()!=null && !rV.getApplicantView().getCellNumber().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,rV.getApplicantView().getCellNumber());

			if(rV.getApplicantView().getIsCompany()!=null)
			{
				DomainDataView ddv = new DomainDataView();
				if(rV.getApplicantView().getIsCompany()==1L)
					ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
				else
					ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);

				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
			}


		}
		logger.logInfo(methodName+"Finish");
	}

	@SuppressWarnings("unchecked")
	public void setMode()
	{
		try 
		{
//			  //Only uncomment to simulate task list functionality-Start
//			  UserTask userTas   = new UserTask();
////			  userTas.setTaskType( "ApproveTransferContract");
//			  userTas.setTaskType( "ApproveOrChangeSuggestedRent");
//			  
//			  Map<String, String> taskAttribute  = new HashMap<String, String>();
//			  taskAttribute.put("REQUEST_ID","30138");
//			  userTas.setTaskAttributes(taskAttribute);
//			  sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTas);
//			  //Only uncomment to simulate task list functionality-Finish

			// from contract list
			if(getRequestParam(WebConstants.Contract.CONTRACT_VIEW)!=null)
			{
			 getDataFromContractView();
			}
			// FROM TASK LIST

			else if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			 getDataFromUserTask();
			else if ((ApplicationContext.getContext().get(WebContext.class).getAttribute(WebConstants.REQUEST_VIEW) != null) && !viewMap.containsKey(WebConstants.TRANSFER_CONTRACT_USER_TASK))
			 getDataFromRequestView();
			if(viewMap.containsKey("TRANSFER_SETTLEMENT_TASK") && 
					(viewMap.get(PAGE_MODE)==null || ! viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT_DONE)))
			{
					UserTask userTask = (UserTask)viewMap.get("TRANSFER_SETTLEMENT_TASK");
					if(userTask!=null)
					{
						viewMap.put(PAGE_MODE,PAGE_MODE_NEW_CONTRACT);
						manageSettlementStatus(userTask);
					}
			}
			

			if( !isPostBack() )
			{
				if(pageMode.equals(PAGE_MODE_RENT_APPROVE))
					viewMap.put(DEFAULT_TAB, WebConstants.TransferContract.UNIT_TAB);
				else if(pageMode.equals(PAGE_MODE_TRANSFER_FEE_COLLECTION))
					viewMap.put(DEFAULT_TAB, WebConstants.TransferContract.FEES_TAB);
				if(getRequestParam(WebConstants.BACK_SCREEN)!=null && getRequestParam(WebConstants.BACK_SCREEN).toString().trim().length()>0)
				{
					viewMap.put(WebConstants.BACK_SCREEN, getRequestParam(WebConstants.BACK_SCREEN));
					setRequestParam(WebConstants.BACK_SCREEN, null);
				}

			}
			else
				viewMap.remove(DEFAULT_TAB);

		}
		catch (Exception ex) 
		{
			logger.LogException("setMode crashed... ", ex);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	private void getDataFromRequestView() throws Exception 
	{
		RequestView reqView = (RequestView)ApplicationContext.getContext().get(WebContext.class).getAttribute(WebConstants.REQUEST_VIEW);
		ApplicationContext.getContext().get(WebContext.class).removeAttribute(WebConstants.REQUEST_VIEW);
		getTransferContractRequest(reqView.getRequestId());
		viewMap.put(PAGE_MODE,pageMode);
	}
	@SuppressWarnings("unchecked")
	private void getDataFromContractView() throws PimsBusinessException,Exception 
	{
		String methodName = "getDataFromContractView|";
		logger.logInfo(methodName+"Start");
		this.setContractView((ContractView)getRequestParam(WebConstants.Contract.CONTRACT_VIEW));
		setRequestParam(WebConstants.Contract.CONTRACT_VIEW, null);
		getContractById();
		viewMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
		RequestServiceAgent reqServiceAgent = new RequestServiceAgent();
		HashMap retMap = reqServiceAgent.isTransferRequestInValid(contractView.getContractId());
		Boolean isTransferReqInvalid = (Boolean)retMap.get("IS_INVALID");
		Long requestId = (Long)retMap.get("REQUEST_ID");
		if(isTransferReqInvalid)
		{
			if(requestId!=null)
				getTransferContractRequest(requestId);
			String requestNumber = requestView.getRequestNumber();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.TransferLeaseContract.MSG_TRANSFER_CONTRACT_REQUEST_ALREADY_EXISTS,requestNumber));
		}
		else
			pageMode = PAGE_MODE_NEW;

		viewMap.put(PAGE_MODE,pageMode);
		sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		logger.logInfo(methodName+"Finish");
	}
	@SuppressWarnings("unchecked")
	private void getContractById() throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		ContractView searchContract = new ContractView();
		searchContract.setContractId(contractView.getContractId());
		HashMap contractHashMap=new HashMap();
		contractHashMap.put("contractView",searchContract);
		contractHashMap.put("dateFormat",CommonUtil.getDateFormat());
		logger.logInfo("setMode | Querying db for contractId : " + contractView.getContractId());
		List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
		logger.logInfo("setMode | Got "+ contractViewList.size() +" row from db ");
		if(contractViewList.size()>0)
			this.setContractView((ContractView)contractViewList.get(0));
	}
	@SuppressWarnings("unchecked")
	private void getDataFromUserTask() throws Exception 
	{
		Long requestId = 0L;
		UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		String taskType = userTask.getTaskType();
		viewMap.put(WebConstants.TASK_TYPE, taskType);
		getPageModeBasedOnTaskType(userTask, taskType);
		if(userTask!=null)
		{
			if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
			{
				requestId = Convert.toLong(userTask.getTaskAttributes().get("REQUEST_ID"));
				viewMap.put("REQUEST_ID", requestId);
				getTransferContractRequest(requestId);
			}



			viewMap.put(WebConstants.TRANSFER_CONTRACT_USER_TASK, userTask);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		}
	}
	@SuppressWarnings("unchecked")
	private void getPageModeBasedOnTaskType(UserTask userTask, String taskType)throws Exception
	{
		

		if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_REVIEW_TRANSFER_CONTRACT))
		{
			
			viewMap.put(PAGE_MODE,PAGE_MODE_REVIEW);
			reviewed.setRendered(true);
		}
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_TRANSFER_CONTRACT))
		{
			if(contractView!=null)
			       newRentValue=contractView.getRentAmount();
			viewMap.put(PAGE_MODE,PAGE_MODE_REVIEW_APPROVAL);
		}
		
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_COLLECT_TRANSFER_CONTRACT_FEES))
			viewMap.put(PAGE_MODE,PAGE_MODE_COLLECT_APP_FEE);
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_SETTLE_TRANSFER_CONTRACT))
		{
			viewMap.put("TRANSFER_SETTLEMENT_TASK", userTask);
			viewMap.put(PAGE_MODE,PAGE_MODE_SETTLED_APPROVED);
		}
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_COLLECT_OLD_CONTRACT_PAYMENTS_TRANSFER_CONTRACT))
		{
			viewMap.put(PAGE_MODE,PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT);
			completeOldContractPaymentTask.setRendered(true);
		}
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_CREATE_NEW_LEASE_CONTRACT))
			viewMap.put(PAGE_MODE,PAGE_MODE_CREATE_NEW_LEASE_CONTRACT);
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_COLLECT_NEW_CONTRACT_PAYMENTS_TRANSFER_CONTRACT))
			viewMap.put(PAGE_MODE,PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT);
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_COLLECT_OLD_CONTRACT_PAYMENTS_TRANSFER_CONTRACT))
			viewMap.put(PAGE_MODE,PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT);
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_OR_CHANGE_SUGGESTED_RENT))
			viewMap.put(PAGE_MODE,PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT);
		else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_VERIFY_SUGGESTED_RENT_AMOUNT))
			viewMap.put(PAGE_MODE,PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT);
	else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_COMPLETE_REQUEST))
			viewMap.put(PAGE_MODE,PAGE_MODE_COMPLETE_REQUEST);
	}
	private void getPageModeBasedOnRequestStatus(RequestView reqView) 
	{
		List<DomainDataView> requestStatusList =CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		DomainDataView ddvNew= CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_NEW);
		DomainDataView ddvAccepted= CommonUtil.getIdFromType(requestStatusList,WebConstants.REQUEST_STATUS_ACCEPTED);
		DomainDataView acceptedApprovedDomainDataView = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_ACCEPTED_APPROVED);
		DomainDataView acceptedRejectedDomainDataView = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_ACCEPTED_REJECTED);
		DomainDataView settledApprovedDomainDataView = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_SETTLED_APPROVED);
		DomainDataView completedDomainDataView = CommonUtil.getIdFromType(requestStatusList, WebConstants.REQUEST_STATUS_COMPLETE);
		if(reqView.getStatusId().compareTo(ddvNew.getDomainDataId())==0  || 
		   reqView.getStatusId().compareTo(ddvAccepted.getDomainDataId())==0)
			   pageMode = PAGE_MODE_NEW;
		else if( reqView.getStatusId().equals(acceptedApprovedDomainDataView.getDomainDataId()) )
			pageMode = PAGE_MODE_TRANSFER_FEE_COLLECTION;
		else if ( reqView.getStatusId().equals(acceptedRejectedDomainDataView.getDomainDataId()) )
			pageMode = PAGE_MODE_TRANSFER_FEE_COLLECTION_DONE;
		else if ( reqView.getStatusId().equals(settledApprovedDomainDataView.getDomainDataId()) )
			pageMode = PAGE_MODE_SETTLED_APPROVED;
		else if ( reqView.getStatusId().equals(completedDomainDataView.getDomainDataId()) )
			pageMode = PAGE_MODE_VIEW;
		else 
			pageMode = PAGE_MODE_VIEW;
		
	}
    @SuppressWarnings("unchecked")
	private void manageSettlementStatus(UserTask userTask) throws Exception 
	{		
		logger.logInfo("manageSettlementStatus|Started...");
		try{
			List<String> comments = userTask.getTaskComments();

			if(comments.size() == 0){
				sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				ApplicationContext.getContext().get(WebContext.class).setAttribute("contractView", contractView);
				ApplicationContext.getContext().get(WebContext.class).setAttribute("requestView", requestView);
				ApplicationContext.getContext().get(WebContext.class).setAttribute("parentPage", "TransferContract");
				HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();				
				response.sendRedirect("settlement.jsf");
			}
			else{
				viewMap.put(WebConstants.TRANSFER_CONTRACT_USER_TASK, userTask);
				ApproveRejectTask(TaskOutcome.OK);
				modeAction();
				sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				
			}
			viewMap.remove("TRANSFER_SETTLEMENT_TASK");
			logger.logInfo("manageSettlementStatus|Completed Successfully...");
		}
		catch (Exception e) {
			logger.LogException("manageSettlementStatus|Crashed...", e);
			
			throw e;
		}
	}

	//Initialize Payment Receipt TAB
    @SuppressWarnings("unchecked")
	private void populatePaymentTab()
    {
		if(viewMap.get(PAGE_MODE)!=null)
		{
			String currentMode = viewMap.get(PAGE_MODE).toString();
			if(currentMode.equals(PAGE_MODE_SETTLED_APPROVED) || currentMode.equals(PAGE_MODE_SETTLED_APPROVED_DONE))
				if(!viewMap.containsKey(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST) && viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW)){
					Long contractId = ((ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW)).getContractId();		
					paymentBeanDataList = populatePaymentsTab(contractId);
					viewMap.put(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST,paymentBeanDataList);
				}
		}

	}
	@SuppressWarnings("unchecked")
	public void populateTenantInfo()
	{
		String methodName="populateTenantInfo|";
		logger.logInfo(methodName+"Start");
		try
		{
			if(sessionMap.containsKey(WebConstants.PERSON_BASIC_INFO)  && sessionMap.get(WebConstants.PERSON_BASIC_INFO)!=null )
			{
				this.setNewTenantView((PersonView)sessionMap.get(WebConstants.PERSON_BASIC_INFO));
				sessionMap.remove(WebConstants.PERSON_BASIC_INFO);
				newTenantName.setValue(this.getNewTenantView().getPersonFullName());
				newTenantView.setCellNumber(this.getNewTenantView().getCellNumber());
				hdnTDPersonId = this.getNewTenantView().getPersonId().toString();
				hdnTDCellNo = (this.getNewTenantView().getCellNumber()!=null)?this.getNewTenantView().getCellNumber():"";
				hdnTDPersonName = this.getNewTenantView().getPersonFullName();
				hdnTDIsCompany = this.getNewTenantView().getIsCompany().toString();
			}
			if(hdnTDPersonId!=null && !hdnTDPersonId.equals(""))
			{
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnTDPersonId);
				viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_ID,hdnTDPersonId);
				newTenantView.setPersonId(new Long(hdnTDPersonId));
				PropertyServiceAgent psa = new PropertyServiceAgent();
				newTenantView = (psa.getPersonInformation(newTenantView)).get(0);
	
			}
	
			if(hdnTDPersonName!=null && !hdnTDPersonName.equals(""))
			{
				viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME,hdnTDPersonName);
				newTenantView.setPersonFullName(hdnTDPersonName);
				newTenantName.setValue(hdnTDPersonName);
			}
	
			if(hdnTDCellNo!=null && !hdnTDCellNo.equals(""))
			{
				viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_CELL,hdnTDCellNo);
				//newTenantView.setCellNumber(hdnTDCellNo);
			}
			if(hdnTDIsCompany!=null && !hdnTDIsCompany.equals(""))
			{
				newTenantView.setIsCompany(Long.parseLong(hdnTDIsCompany));
				DomainDataView ddv = new DomainDataView();
				if(Long.parseLong(hdnTDIsCompany)==1L)
				
					ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_COMPANY);
				else
					ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),WebConstants.TENANT_TYPE_INDIVIDUAL);
	
				viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
	
			}
			this.setNewTenantView(newTenantView);
			logger.logInfo(methodName+"Finish");
		}
		catch(Exception ex)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException(methodName+"|"+" Error Occured...", ex);
			
		}
	}
   @SuppressWarnings("unchecked")
	public void setValues()
   {
		logger.logInfo("setValues start...");

		try
		{
			if(viewMap.containsKey(REQUEST_VIEW))
			{
				requestView = (RequestView) viewMap.get(REQUEST_VIEW);
				CommonUtil.loadAttachmentsAndComments(requestView.getRequestId().toString());
			}
			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW))
			{
				this.setContractView(  (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW));
				Date startDate = contractView.getStartDate();

				Date endDate = contractView.getEndDate();
				if(viewMap.containsKey(PAGE_MODE))
				{ 
					if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT))
					{
						Integer monthsBetweenNewContract = CommonUtil.getMonthsBetween(startDate, endDate);
						viewMap.put(CONTRACT_MONTHS, monthsBetweenNewContract);
					}
					else
						viewMap.remove(CONTRACT_MONTHS);
					
				}
			}
			if(sessionMap.containsKey("TENANTINFO")) // new tenant selected from popup
			{
				newTenantView = (PersonView) sessionMap.get("TENANTINFO");
				sessionMap.remove("TENANTINFO");
				if(newTenantView!=null)
					viewMap.put(WebConstants.Tenant.TENANT_VIEW,newTenantView);
				
			}
			else
				newTenantView = getNewTenantView();

			if(newTenantView==null)
				newTenantView = new PersonView();

			if(contractView!=null)
			{
				contractView.setStartDateString(CommonUtil.getStringFromDate(contractView.getStartDate()));
				contractView.setEndDateString(CommonUtil.getStringFromDate(contractView.getEndDate()));
				contractView.setStatusVal(getIsEnglishLocale()? contractView.getStatusEn():contractView.getStatusAr());
				contractView.setContractTypeVal(getIsEnglishLocale()? contractView.getContractTypeEn():contractView.getContractTypeAr());
				List<ContractUnitView> contractUnitViewList= new ArrayList<ContractUnitView>();
				contractUnitViewList.addAll(contractView.getContractUnitView());
				if(!contractUnitViewList.isEmpty())
				{
					ContractUnitView contractUnit = contractUnitViewList.get(0);
					if(contractUnit!=null)
					{
						UnitView unitView = contractUnit.getUnitView();
						if(unitView!=null){
							unitView.setUnitTypeVal(getIsEnglishLocale()? unitView.getUnitTypeEn():unitView.getUnitTypeAr());
						}
						contractView.setContractUnit(contractUnit);
					}
					viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST, contractUnitViewList);					
				}
			}
			else
				contractView = new ContractView();
			if(contractView.getRentAmount()!=null)
				oldRentValue.setValue(Double.valueOf(contractView.getRentAmount().toString()));
			if(requestView==null)
				requestView = new RequestView();
			logger.logInfo("setValues completed successfully...");
		}
		catch (Exception ex) 
		{
			logger.LogException("setValues crashed... ", ex);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void btnApprove_Click()
	{
		String methodName = "btnApprove_Click";
		errorMessages=new ArrayList<String>(0);
		errorMessages.clear();
		successMessages=new ArrayList<String>(0);
		successMessages.clear();
		Boolean success = false;
		logger.logInfo(methodName+"|"+" Start...");
		try
		{
			requestView = (RequestView) viewMap.get(REQUEST_VIEW);
			if(validateFieldsForRequestApproval())
			{
				initContext();		   
				addRequestFieldDetailForNewContract(requestView.getRequestId());
				
                saveAttachmentsComments();
				String user = CommonUtil.getLoggedInUser();
				if(success)
				{
					success = ApproveRejectTask(TaskOutcome.APPROVE);
					if(success)
					{
						String taskType = (String)viewMap.get(WebConstants.TASK_TYPE);
						if(taskType==null)
							taskType = "";
						if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_TRANSFER_REQUEST))
						{
							//saveNewRent();
							//CODE TO ADD PAYMENT SCHEDULE
							addPaymentSchedule();

							notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_TransferContract_Request_Approved);
							errorMessages.clear();
							successMessages.clear();
							if(!success)
								errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_NOTFICATION_FAILURE));
							successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUCCESS_APPROVED));
							viewMap.put(PAGE_MODE,PAGE_MODE_RENT_APPROVE_DONE);
						}
						else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_NEW_CONTRACT))
						{
							if(viewMap.containsKey(WebConstants.Contract.NEW_CONTRACT_VIEW))
							{
								newContractView = (ContractView)viewMap.get(WebConstants.Contract.NEW_CONTRACT_VIEW);
							}
							if(newContractView.getContractId()!=null)
							{
								PropertyServiceAgent psa = new PropertyServiceAgent();
								Boolean updateSuccess = psa.setContractAsApproved(newContractView.getContractId(),getLoggedInUser());
								if(!updateSuccess)
									logger.logInfo("New Contract Status not updated!!!");
								success = generateNotification(user,newContractView,EVENT_TRANSFER_CONTRACT_NEW);
								errorMessages.clear();
								successMessages.clear();
								if(!success)
									errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_NOTFICATION_FAILURE));
								successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUCCESS_CONTRACT_ISSUE_APPROVED));
								viewMap.put(PAGE_MODE,PAGE_MODE_NEW_CONTRACT_DONE);
							}
						}
					}
				}
				
				logger.logInfo(methodName+"|"+" Finish...");
			}
		}
		catch(PIMSWorkListException ex)
		{
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",ex);
		}
		catch(Exception ex)
		{
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",ex);
		}
		
	}

	@SuppressWarnings("unchecked")
	public void btnReject_Click()
	{
		String methodName = "btnReject_Click";
		Boolean success = false;
		logger.logInfo(methodName+"|"+" Start...");
		try
		{
			initContext();		   
			String user = CommonUtil.getLoggedInUser();
			logger.logDebug(methodName + "|" + "Save Attachments...Start");
			CommonUtil.loadAttachmentsAndComments(requestView.getRequestId().toString());
			Boolean attachSuccess = CommonUtil.saveAttachments(requestView.getRequestId());
			logger.logDebug(methodName + "|" + "Save Attachments...Finish");
	
			logger.logDebug(methodName + "|" + "Save Comments...Start");
	//		Boolean commentSuccess = saveComments(requestView.getRequestId(),MessageConstants.RequestEvents.REQUEST_CANCELED);
			Boolean commentSuccess = CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
			logger.logDebug(methodName + "|" + "Save Comments...Finish");
	
			success = attachSuccess && commentSuccess;
			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW)){
				contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
			}
			if(success){
				success = ApproveRejectTask(TaskOutcome.REJECT);
				new PropertyServiceAgent().setContractSettlementDateToNull(contractView.getContractId());
				if(success)
				{
					String taskType = (String)viewMap.get(WebConstants.TASK_TYPE);
					if(taskType==null)
						taskType = "";
					if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_TRANSFER_REQUEST))
					{
						/*			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW)){
							contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
						}
						 */			success = generateNotification(user,contractView,EVENT_TRANSFER_CONTRACT_REQUEST_REJECTED);
						 errorMessages.clear();
						 successMessages.clear();
						 if(!success)
							 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_NOTFICATION_FAILURE));
						 CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_REJECTED, requestView.getRequestId());
						 successMessages.add(CommonUtil.getBundleMessage(MessageConstants.ChangeTenantName.MSG_SUCCESS_REJECTED));
						 viewMap.put(PAGE_MODE,PAGE_MODE_RENT_APPROVE_DONE);
					}
					else if(taskType.equals(WebConstants.UserTasks.TransferLeaseContract.TASK_TYPE_APPROVE_NEW_CONTRACT))
					{
						logger.logInfo(methodName + "|" + "Invocation of new BPEL on contract rejection...");
						Long requestId = requestView.getRequestId();
						Long requestStatusId = 1L;
						DomainDataView domainDataView = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
								WebConstants.REQUEST_STATUS_APPROVED_COLLECTED);
						if(domainDataView != null)
							requestStatusId = domainDataView.getDomainDataId();
	
						logger.logInfo("Request Id : %s | Request Status Id : %s", requestId, requestStatusId);
	
						RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
						RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
						String taskId = reqTaskView.getTaskId();
	
						logger.logInfo("Task Id : %s", taskId);
						if(taskId!=null)
						{
							BPMWorklistClient bpmWorkListClient = null;
							String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
							bpmWorkListClient = new BPMWorklistClient(contextPath);
							UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
	//						if (userTask != null)
	//						userTask.getTaskComments().add("SETTLED");							
							bpmWorkListClient.addTaskComment(userTask, CommonUtil.getLoggedInUser(), "SETTLED");
	
							successMessages.clear();
							successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUCCESS_CONTRACT_ISSUE_REJECTED));
							viewMap.put(PAGE_MODE,PAGE_MODE_NEW_CONTRACT_APPROVE_DONE);
						}
					}
				}
			}
	
			logger.logInfo(methodName+"|"+" Finish...");
		}
		catch(PIMSWorkListException ex)
		{
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",ex);
		}
		catch(Exception ex)
		{
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",ex);
		}
	
	
	}

   @SuppressWarnings("unchecked")
	public void approveReview()
	{
		String methodName = "approveReview|";
		try 
		{
			errorMessages.clear();
			logger.logInfo(methodName+"Start");

			requestView = (RequestView) viewMap.get(REQUEST_VIEW);
			ContractView contractVU=new ContractView();
			if(viewMap.get(WebConstants.Contract.CONTRACT_VIEW)!=null)
				contractVU=(ContractView) viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
			if( validatePayment( getPaymentSchedules() ) )
			{
				if(this.getEvacuationDate()!=null && requestView.getContractView().getSettlementDate()!=null)
					{
						String formattedDateFromControl;
						String formattedDateFromContract;
						DateFormat formatter= new SimpleDateFormat("MM/dd/yyyy");
						formattedDateFromControl=formatter.format(this.getEvacuationDate());
						formattedDateFromContract=formatter.format(requestView.getContractView().getSettlementDate());
						if(!formattedDateFromControl.equalsIgnoreCase(formattedDateFromContract))
							{
//								if(validate(contractVU))
//								{	
									contractVU.setSettlementDate(this.getEvacuationDate());
									requestView.setContractView(contractVU);
									updateContractSettlementDate(contractVU);
//									saveEvacuationDate(requestView);
//								}
							}
					}
				else if(this.getEvacuationDate()!=null && requestView.getContractView().getSettlementDate()==null)
				{
//					if(validate(contractVU))
//					{
						contractVU.setSettlementDate(this.getEvacuationDate());
						requestView.setContractView(contractVU);
						updateContractSettlementDate(contractVU);
//						saveEvacuationDate(requestView);
//					}
				}
				else
				{
					errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Contract.SETTLEMENT_DATE));
					viewMap.put(DEFAULT_TAB, "contractDetailTab");
					return;
				}
					
//				if(validate(contractVU))
//				{
				Double newRentStored=-1D;
				if(viewMap.get("NEW_RENT_AMOUNT")!=null)
					newRentStored=Double.parseDouble(viewMap.get("NEW_RENT_AMOUNT").toString());
				if(newRentStored.compareTo(Double.parseDouble(newRentAmount.getValue().toString()))!=0)
				{	
					addRequestFieldDetailForNewContract( requestView.getRequestId() );
					addPaymentSchedule( requestView.getRequestId() );
				}
					saveAttachmentsComments();
					 new RequestServiceAgent().updateRequestEvacuation(this.getTxttotalEvacFines(), this.getExempted(), commonUtil.getLoggedInUser(), getRequestId());
					ApproveRejectTask( TaskOutcome.APPROVE );
					CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_APPROVED, requestView.getRequestId());
					successMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.TransferLeaseContract.MSG_SUCCESS_APPROVED ) );
					readOnlyStyleClass="READONLY";
					viewMap.put( PAGE_MODE,PAGE_MODE_ACTION_PERFORMED+PAGE_MODE_REVIEW_APPROVAL );
				
//				}	
			}
			logger.logInfo(methodName+"Finish");
					
		} catch (Exception e) 
		{
			logger.LogException(methodName+"Error Occured", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}


	}
	private void updateContractSettlementDate(ContractView contractToBeUpdated) 
	{
		try {
			if (contractToBeUpdated.getContractId() != null)
				new RequestServiceAgent().updateContractSettlementDate(contractToBeUpdated);
		} catch (PimsBusinessException e) 
		{
			logger.LogException("updateContractSettlementDate() crashed | ", e);
		}
	}
	public void cancelCollection(){
		String methodName = "cancelCollection";
		try 
		{
			logger.logInfo(methodName+"Start");
			saveAttachmentsComments();
			ApproveRejectTask(TaskOutcome.CANCEL);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_SUCCESS_APPROVED));
			logger.logInfo(methodName+"Finish");
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			logger.LogException(methodName+"Error Occured", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		
		}

	}
	@SuppressWarnings("unchecked")
	public void rejectReview(){
		String methodName ="rejectReview";
		try 
		{
			logger.logInfo(methodName+ " started successfully...");
			requestView = (RequestView) viewMap.get(REQUEST_VIEW);
			PropertyService ps =  new PropertyService();
			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW))
			{
				contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
			}
			ps.setContractSettlementDateToNull(contractView.getContractId());
			ps.setPaymentScheduleCanceled(requestView.getRequestId());
			saveAttachmentsComments();
			ApproveRejectTask(TaskOutcome.REJECT);
			verifySuggestion.setRendered(false);
			viewMap.put(PAGE_MODE,PAGE_MODE_ACTION_PERFORMED+PAGE_MODE_REVIEW_APPROVAL);
			readOnlyStyleClass="READONLY";
			logger.logInfo(methodName+ " completed successfully...");
		}
		catch (Exception e) 
		{
			logger.LogException(methodName+"Error Occured", e);
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}

	}
	@SuppressWarnings( "unchecked" )
	public void reviewContract(){
		Boolean success=false;
		String methodName ="reviewContract|";
		try 
		{
			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW)){
				contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
			}

			if(contractView!=null){

				CommonUtil.loadAttachmentsAndComments(requestView.getRequestId().toString());
				Boolean attachSuccess = CommonUtil.saveAttachments(requestView.getRequestId());
				Boolean commentSuccess = CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
				success = attachSuccess && commentSuccess;
			}
			success = ApproveRejectTask(TaskOutcome.OK);
			if(success)
			{
				viewMap.remove(WebConstants.TRANSFER_CONTRACT_USER_TASK);
				errorMessages.clear();
				successMessages.clear(); 
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUCCESS_CONTRACT_ISSUE));
				sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			}
		} 
		catch (PIMSWorkListException e) 
		{
			logger.LogException( methodName+"Error Occured:",e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.LogException( methodName+"Error Occured:",e);
		}

	}
	@SuppressWarnings( "unchecked" )
	public Boolean saveNewRent(){
		String methodName ="saveNewRent";
		logger.logInfo(methodName + "|" + "Start");
		Boolean success = false;
		UnitView unitView = new UnitView();
		try
		{
			unitView = (UnitView)viewMap.get("selectedUnitView");
			newRentValue = (Double)viewMap.get("newRentValue");
			logger.logInfo("unitView -> UnitId : %s | newRentValue : %s",unitView.getUnitId(),newRentValue);
			PropertyServiceAgent psa = new PropertyServiceAgent();
			if(unitView.getUnitId()!=null && newRentValue!=null)
				success = psa.updateUnit2(unitView.getUnitId(),newRentValue);
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " " + "crashed...",ex);
			success = false;
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	public void issueNewContract()
	{
		logger.logInfo("issueNewContract started...");
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		try
		{
			if(validateToIssueContract())
			{
						requestView = (RequestView) viewMap.get(REQUEST_VIEW);
						contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
						newContractView = (ContractView)viewMap.get(WebConstants.Contract.NEW_CONTRACT_VIEW);
						contractView.setUpdatedBy(CommonUtil.getLoggedInUser());
						contractView.setUpdatedOn(new Date());
	                    HashMap argMap = new HashMap(0);
	                    argMap.put("OLD_CONTRACT_VIEW",contractView);
	                    argMap.put("NEW_CONTRACT_VIEW",newContractView);
	                    argMap.put("USER",getLoggedInUser());
	                    propertyServiceAgent.issueNewContractForTransfer(argMap);
						viewMap.put(WebConstants.Contract.NEW_CONTRACT_VIEW,newContractView);
						ApproveRejectTask(TaskOutcome.OK);
						saveAttachmentsComments();
						viewMap.remove(WebConstants.TRANSFER_CONTRACT_USER_TASK);
						successMessages.clear(); 
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.ContractEvents.CONTRACT_CREATED, requestView.getRequestId());
						successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUCCESS_CONTRACT_ISSUE));
						pageMode = PAGE_MODE_NEW_CONTRACT_DONE;
						viewMap.put(PAGE_MODE,pageMode);
						sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				
			}
			logger.logInfo("issueNewContract completed successfully...");	
		}
		catch(Exception ex)
		{
			logger.LogException("issueNewContract crashed...",ex);
			
			errorMessages.clear(); 
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public String btnSaveNewRentClick()
	{
		String methodName ="btnSaveNewRentClick";
		logger.logInfo(methodName + "|" + "Start");
		Boolean success = false;
		try
		{
			viewMap.remove("showEnterRentHtmlLabel");
			ContractUnitView selectedContractUnit = (ContractUnitView)viewMap.get("selectedContractUnit");
			List<ContractUnitView> contractUnitList = new ArrayList<ContractUnitView>();
			contractUnitList.addAll(contractView.getContractUnitView());
			if(!contractUnitList.isEmpty() && contractUnitList!=null){
				contractUnitList.remove(0);
			}
			UnitView unitView = selectedContractUnit.getUnitView();
			unitView.setRentValue(newRentValue);
			viewMap.put("selectedUnitView", unitView);
			viewMap.put("newRentValue", newRentValue);
			selectedContractUnit.setUnitView(unitView);
			contractUnitList.add(0,selectedContractUnit);
			viewMap.put(WebConstants.Contract.CONTRACT_UNIT_LIST,contractUnitList);
			success = true;
			if(success){
				viewMap.put("rentSaved", "rentSaved");
				successMessages.clear();
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.NEW_RENT_SUCCESS));
			}
			else{
				viewMap.remove("rentSaved");
				successMessages.clear();
				errorMessages.clear(); 
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.NEW_RENT_FAILURE));
			}
			logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " " + "crashed...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "saveNewRentVal";
	}
	@SuppressWarnings( "unchecked" )
	public String setSelectedColumn()
	{
		String methodName ="setSelectedColumn";
		logger.logInfo(methodName + "|" + "Start");
		try
		{
			ContractUnitView selectedContractUnit = (ContractUnitView)unitDataTable.getRowData();
			viewMap.put("selectedContractUnitIndex", unitDataTable.getRowIndex());
			newRentValue = selectedContractUnit.getUnitView().getRentValue();
			viewMap.put("newRentValue",newRentValue);
			viewMap.put("selectedContractUnit", selectedContractUnit);
			viewMap.put("showEnterRentHtmlLabel","showEnterRentHtmlLabel");
			logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.LogException("Error Occured::",ex);

		}
		return "saveNewRentVal";
	}
	@SuppressWarnings( "unchecked" )
	public String tabRequiredDocuments_Click()
	{
		String methodName ="tabRequiredDocuments_Click";
		logger.logInfo(methodName + "|" + "Start");
		try
		{
			UtilityServiceAgent usa=new UtilityServiceAgent();
			List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.PROCEDURE_TYPE);
			DomainDataView ddv = CommonUtil.getIdFromType(ddvList, WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT);		
			List<RequiredDocumentsView> requiredDocumentsViewList= usa.getRequiredDocumentsByProcedureType(ddv.getDomainDataId());
			viewMap.put(WebConstants.Contract.TRANSFER_CONTRACT_REQ_DOC,requiredDocumentsViewList);

			logger.logInfo(methodName + "|" + "Finish");
		}
		catch(Exception ex)
		{
			logger.LogException("Error Occured::",ex);

		}
		return "reqDoc";
	}
	@SuppressWarnings( "unchecked" )
	private void enableDisableApprovRejectButtons(boolean rendered)
	{
		btnApprove.setRendered(rendered);
		btnReject.setRendered(rendered);
	}
	@SuppressWarnings("unchecked")
	private void populatePaymentScheduleAgainstRequest(Long requestId)throws Exception
	{
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		paymentSchedules = propertyServiceAgent.getPaymentScheduleByRequestID(requestId);
		if(paymentSchedules !=null && paymentSchedules.size()>0)
			viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
		viewMap.put(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST,paymentBeanDataList);
	}
	@SuppressWarnings("unchecked")
	private void enableDisableButtons()
	{
		logger.logInfo("enableDisableButtons started...");
		btnSave.setRendered(false);
		viewMap.put("applicationDetailsReadonlyMode",true);
		if(viewMap.get(PAGE_MODE)!=null)
		{
			logger.logInfo("Page Mode : %s",viewMap.get(PAGE_MODE).toString());

if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_COMPLETE_REQUEST))
			{
				renderPaymentButtons=false;
				btnIssueContract.setRendered(false);
				paymentsTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				feesTab.setRendered(false);
				newRentValueColumn.setRendered(false);
				unitReadOnlyRentColumn.setRendered(false);
				unitAddressColumn.setRendered(false);
				unitStatusColumnAr.setRendered(false);
				unitStatusColumnEn.setRendered(false);
				unitActionColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				rentValuePanelGrid.setRendered(false);
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(true);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
				viewMap.put( "canAddAttachment" , false );
				viewMap.put( "canAddNote" , false );
				renderAddTenantLink = false;
			}
			//if page is in add Mode than only save button should appear
			if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_REVIEW))
			{
				reviewed.setRendered(true);
				approveReview.setRendered(false);
				requestHistoryTab.setRendered(true);
				rejectReview.setRendered(false);
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = true;
				
				renderPaymentButtons=true;
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_REVIEW_APPROVAL)){
				reviewed.setRendered(false);
				approveReview.setRendered(true);
				rejectReview.setRendered(true);
				collectAppFee.setRendered(false);
				requestHistoryTab.setRendered(true);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = false;
				renderPaymentButtons=true;
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT)){
				reviewed.setRendered(false);
				approveReview.setRendered(true);
				rejectReview.setRendered(true);
				requestHistoryTab.setRendered(true);
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = false;
				renderPaymentButtons=true;
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
				}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(true);
				verifySuggestion.setRendered(true);
				requestHistoryTab.setRendered(true);
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = false;
				renderPaymentButtons=false;
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_COLLECT_APP_FEE)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				requestHistoryTab.setRendered(true);
				collectAppFee.setRendered(true);
				cancelAppFee.setRendered(true);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderPaymentButtons=false;
				renderAddTenantLink = false;
				viewMap.put("styleClass","READONLY");
				
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
				if( !isPostBack() )
				tabPanel.setSelectedTab("PaymentTab");
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_COLLECT_APP_FEE_SETTLEMENT)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(true);
				requestHistoryTab.setRendered(true);
				collectOldContractPayment.setRendered(false);
				renderPaymentButtons=false;
				completeOldContractPaymentTask.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
				if( !isPostBack() )
					tabPanel.setSelectedTab("PaymentTab");

			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				collectAppFee.setRendered(false);
				requestHistoryTab.setRendered(true);
				renderPaymentButtons=false;
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				if(isAnyPendingPayments()){
					collectOldContractPayment.setRendered(true);
					completeOldContractPaymentTask.setRendered(false);
				}
				else{
					collectOldContractPayment.setRendered(false);
					completeOldContractPaymentTask.setRendered(true);
				}



				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = false;
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
				if( !isPostBack() )
					tabPanel.setSelectedTab("PaymentTab");
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_CREATE_NEW_LEASE_CONTRACT)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				renderPaymentButtons=false;
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				createNewLeaseContract.setRendered(true);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = false;
				requestHistoryTab.setRendered(true);
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);

			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)){
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				collectAppFee.setRendered(false);
				renderPaymentButtons=false;
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				requestHistoryTab.setRendered(true);
				collectOldContractPayment.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(true);
				renderAddTenantLink = false;
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
				if( !isPostBack() )
					tabPanel.setSelectedTab("PaymentTab");
			}
			else{
				reviewed.setRendered(false);
				approveReview.setRendered(false);
				rejectReview.setRendered(false);
				
				collectAppFee.setRendered(false);
				cancelAppFee.setRendered(false);
				setteled.setRendered(false);
				collectOldContractPayment.setRendered(false);
				createNewLeaseContract.setRendered(false);
				collectNewContractPayment.setRendered(false);
				renderAddTenantLink = true;
				renderPaymentButtons=true;
				requestHistoryTab.setRendered(true);
				//populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote",true);
			}
			if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_ACTION_PERFORMED+PAGE_MODE_REVIEW_APPROVAL)){
				renderPaymentButtons=false;
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote",false);
			}
			if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_CONTRACT_VERIFIED+PAGE_MODE_ACTION_PERFORMED)){
				renderPaymentButtons=false;
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote",false);
				verifySuggestion.setRendered(false);
			}
			
			if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW))
			{
				viewMap.put( "applicationDetailsReadonlyMode", false );
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				feesTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				newRentValueColumn.setRendered(false);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				unitActionColumn.setRendered(false);
				renderSettlementDate = true;
				disableSettlementDate = false;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnSaveNewRent.setRendered(false);
				renderPaymentButtons=false;
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(true);
				btnSubmit.setRendered(true);
				btnSave.setRendered(true);
				btnIssueContract.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_DONE))
			{
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				feesTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				newRentValueColumn.setRendered(false);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				unitActionColumn.setRendered(false);
				renderSettlementDate = true;
				disableSettlementDate = true;
				renderPaymentButtons=false;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnIssueContract.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			} 
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_RENT_APPROVE))
			{
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				feesTab.setRendered(false);
				renderPaymentButtons=false;
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				unitAddressColumn.setRendered(false);
				unitStatusColumnAr.setRendered(false);
				unitStatusColumnEn.setRendered(false);
				unitActionColumn.setRendered(true);
				disableSettlementDate = true;
				renderSettlementDate = true;
				rentValuePanelGrid.setRendered(true);
				btnSaveNewRent.setRendered(true);
				btnSaveNewRent.setDisabled(true);
				if(viewMap.containsKey("showEnterRentHtmlLabel"))
					btnSaveNewRent.setDisabled(false);
				else{
					btnSaveNewRent.setDisabled(true);
					clickActionColumnHtmlLabel.setRendered(true);
				}
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(true);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				btnIssueContract.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			//if  request Status is APPROVED than ONLY PAY BUTTON  button should appear
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_RENT_APPROVE_DONE))
			{
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				feesTab.setRendered(false);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				renderPaymentButtons=false;
				unitAddressColumn.setRendered(false);
				unitStatusColumnAr.setRendered(false);
				unitStatusColumnEn.setRendered(false);
				unitActionColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = true;
				rentValuePanelGrid.setRendered(false);
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				//---------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!--------//
				btnIssueContract.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT))
			{
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(true);
				feesTab.setRendered(false);
				renderPaymentButtons=false;
				requestHistoryTab.setRendered(true);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				unitActionColumn.setRendered(false);
				renderSettlementDate = false;
				disableSettlementDate = true;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnIssueContract.setRendered(true);
				btnPrintLetter.setRendered(false);
				if(viewMap.containsKey(CONTRACT_MONTHS)){
					Integer mth = (Integer)viewMap.get(CONTRACT_MONTHS);
					String msg = CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_TOTAL_PAYMENTS) + " : " + mth;
					viewMap.put("totalPaymentsMessage",msg);
				}
				totalPaymentsHtmlLabel.setRendered(true);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT_DONE))
			{
				btnNewContract.setRendered(true);
				paymentsTab.setRendered(true);
				feesTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				renderPaymentButtons=false;
				unitActionColumn.setRendered(false);
				renderSettlementDate = false;
				disableSettlementDate = true;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				btnIssueContract.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT_APPROVE))
			{
				btnNewContract.setRendered(true);
				paymentsTab.setRendered(true);
				feesTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				renderPaymentButtons=false;
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				unitActionColumn.setRendered(false);
				renderSettlementDate = false;
				disableSettlementDate = true;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(true);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				btnIssueContract.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW_CONTRACT_APPROVE_DONE))
			{
				btnNewContract.setRendered(true);
				renderPaymentButtons=false;
				paymentsTab.setRendered(true);
				feesTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(true);
				unitAddressColumn.setRendered(true);
				unitActionColumn.setRendered(false);
				renderSettlementDate = false;
				disableSettlementDate = true;
				rentValuePanelGrid.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				btnIssueContract.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_VIEW))
			{
				btnNewContract.setRendered(false);
				renderPaymentButtons=false;
				btnIssueContract.setRendered(false);
				paymentsTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				feesTab.setRendered(false);
				newRentValueColumn.setRendered(false);
				unitReadOnlyRentColumn.setRendered(false);
				unitAddressColumn.setRendered(false);
				unitStatusColumnAr.setRendered(false);
				unitStatusColumnEn.setRendered(false);
				unitActionColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				rentValuePanelGrid.setRendered(false);
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnSubmit.setRendered(false);
				btnSave.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
				viewMap.put( "canAddAttachment" , false );
				viewMap.put( "canAddNote" , false );
				renderAddTenantLink = false;
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_TRANSFER_FEE_COLLECTION)){
				rentValuePanelGrid.setRendered(false);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				renderPaymentButtons=false;
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(true);
				btnComplete.setRendered(false);
				btnSubmit.setRendered(true);
				
				btnIssueContract.setRendered(false);
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				unitActionColumn.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_TRANSFER_FEE_COLLECTION_DONE)){
				rentValuePanelGrid.setRendered(false);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(false);
				renderPaymentButtons=false;
				btnComplete.setRendered(false);
				btnSubmit.setRendered(false);
				btnIssueContract.setRendered(false);
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				unitActionColumn.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}
			else if (viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_SETTLED_APPROVED)){
				rentValuePanelGrid.setRendered(false);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				requestHistoryTab.setRendered(true);
				disableSettlementDate = true;
				renderSettlementDate = false;
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(true);
				btnComplete.setRendered(false);
				renderPaymentButtons=false;
				btnSubmit.setRendered(true);
				btnIssueContract.setRendered(false);
				btnNewContract.setRendered(true);
				paymentsTab.setRendered(true);
				unitActionColumn.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
				feesTab.setRendered(false);
			}
			else if (viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_SETTLED_APPROVED_DONE)){
				rentValuePanelGrid.setRendered(false);
				newRentValueColumn.setRendered(true);
				unitReadOnlyRentColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				requestHistoryTab.setRendered(true);
				btnSaveNewRent.setRendered(false);
				btnNewTenant.setRendered(false);
				renderPaymentButtons=false;
				enableDisableApprovRejectButtons(false);
				btnPay.setRendered(false);
				btnComplete.setRendered(false);
				btnSubmit.setRendered(false);
				btnIssueContract.setRendered(false);
				btnNewContract.setRendered(true);
				paymentsTab.setRendered(true);
				unitActionColumn.setRendered(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(true);
				feesTab.setRendered(false);				
			}
			//if request Status is COMPLETED than NO BUTTON  button should appear
			else
			{
				btnNewContract.setRendered(false);
				paymentsTab.setRendered(false);
				requestHistoryTab.setRendered(true);
				rentValuePanelGrid.setRendered(false);
				newRentValueColumn.setRendered(false);
				unitReadOnlyRentColumn.setRendered(false);
				disableSettlementDate = true;
				renderSettlementDate = false;
				rentValuePanelGrid.setRendered(false);
				btnSaveNewRent.setRendered(false);
				btnPay.setRendered(false);
				//btnComplete.setRendered(false);
				btnNewTenant.setRendered(false);
				btnSubmit.setRendered(false);
				enableDisableApprovRejectButtons(false);
				clickActionColumnHtmlLabel.setRendered(false);
				btnPrintLetter.setRendered(false);
			}

		}
		else
		{
			btnNewContract.setRendered(false);
			paymentsTab.setRendered(false);
			requestHistoryTab.setRendered(true);
			rentValuePanelGrid.setRendered(false);
			
			newRentValueColumn.setRendered(false);
			unitReadOnlyRentColumn.setRendered(false);
			disableSettlementDate = true;
			renderSettlementDate = false;
			rentValuePanelGrid.setRendered(false);
			btnSaveNewRent.setRendered(false);
			btnPay.setRendered(false);
			btnComplete.setRendered(false);
			btnNewTenant.setRendered(false);
			btnSubmit.setRendered(false);
			enableDisableApprovRejectButtons(false);
			clickActionColumnHtmlLabel.setRendered(false);
			btnPrintLetter.setRendered(false);
		}
		if(viewMap.containsKey("showEnterRentHtmlLabel"))
		{
			enterRentHtmlLabel.setRendered(true);
			clickActionColumnHtmlLabel.setRendered(false);
		}
		else
		{
			enterRentHtmlLabel.setRendered(false);
			if(pageMode.equals(PAGE_MODE_RENT_APPROVE))
				clickActionColumnHtmlLabel.setRendered(true);
		}
		if(viewMap.containsKey(DEFAULT_TAB))
		{
			String selectedTab = viewMap.get(DEFAULT_TAB).toString();
			tabPanel.setSelectedTab(selectedTab);
			viewMap.remove(DEFAULT_TAB);
		}
		logger.logInfo("enableDisableButtons completed successfilly...");
	}

	
		private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    @SuppressWarnings("unchecked")
	private void addPaymentSchedule() throws Exception 
	{
		try
		{
			logger.logDebug("addPaymentSchedule started...");
			PaymentScheduleView paymentScheduleView = new PaymentScheduleView();
			paymentScheduleView.setContractId(requestView.getContractView().getContractId());
			paymentScheduleView.setTypeId(getTransferContractPaymentScheduleTypeId());
//			paymentScheduleView.setAmount(getTransferFeeAmount());
			paymentScheduleView.setCreatedBy(CommonUtil.getLoggedInUser());
			paymentScheduleView.setCreatedOn(new Date());
			paymentScheduleView.setUpdatedBy(CommonUtil.getLoggedInUser());
			paymentScheduleView.setUpdatedOn(new Date());
			paymentScheduleView.setIsDeleted(new Long(0));
			paymentScheduleView.setRecordStatus(new Long(1));
			paymentScheduleView.setIsReceived("N");
			paymentScheduleView.setStatusId(getPaymentSchedulePendingStatusId());
			paymentScheduleView.setRequestId(requestView.getRequestId());
			List<ContractUnitView> contractUnits = (List<ContractUnitView>) viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);
			RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
			paymentScheduleView = requestServiceAgent.addTransferFeePaymentSchedule( paymentScheduleView, contractUnits );
			List<PaymentScheduleView> temp = new ArrayList<PaymentScheduleView>();
			temp.add(paymentScheduleView);
			viewMap.put("TRANSFER_FEE_PAYMENT_SCHEDULE",temp);
			logger.logDebug("addPaymentSchedule completed...");
		}
		catch (Exception e) {
			logger.LogException("addPaymentSchedule crashed...", e);
			throw e;
		} 		
	}
    @SuppressWarnings( "unchecked" )
	public void tabRequestHistory_Click()
	{
		String methodName="tabRequestHistory_Click";
		logger.logInfo(methodName+"|"+"Start..");
		try	
		{
			Long requestId=-1L;
			if(viewMap.get(REQUEST_VIEW)!=null)
				requestId=((RequestView)viewMap.get(REQUEST_VIEW)).getRequestId();
				
			RequestHistoryController rhc=new RequestHistoryController();
	    	  if(requestId!=null && (requestId.compareTo(-1L)!=0))
	    	    rhc.getAllRequestTasksForRequest(noteOwner,requestId.toString());
	    	  else
	    		  rhc.getAllRequestTasksForRequest(noteOwner,"");
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured..",ex);
		}
		logger.logInfo(methodName+"|"+"Finish..");
	}

	
	@SuppressWarnings( "unchecked" )
	private Long getTransferContractPaymentScheduleTypeId() throws Exception
	{
		Long typeId = null;
		 DomainDataView domainDataView = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_TYPE),
					                                               WebConstants.PAYMENT_SCHEDULE_TYPE_TRANSFER_CONTRACT_FEE);
			if(domainDataView != null)
			{
				typeId = domainDataView.getDomainDataId();
				if(typeId == null)
				{
					throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");
				}
			}
		return typeId;
	}
	@SuppressWarnings( "unchecked" )
	private Long getPaymentSchedulePendingStatusId()throws Exception {
		Long statusId = null;
			DomainDataView domainDataView= CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
					WebConstants.PAYMENT_SCHEDULE_PENDING);
			if(domainDataView != null){
				statusId = domainDataView.getDomainDataId();
				if(statusId == null){
					logger.logDebug("DomainDataView did not contained DomainDataId");
					throw new PimsBusinessException(new Integer(103), "GENERAL_EXCEPTION");       				
				}
			}
		
		return statusId;
	}

	@SuppressWarnings("unchecked")
	private Boolean ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
	{
		    Boolean success = false;
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			UserTask userTask = (UserTask) viewMap.get(WebConstants.TRANSFER_CONTRACT_USER_TASK);
			String loggedInUser=CommonUtil.getLoggedInUser();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			success = true;

		return success;
	}
	@SuppressWarnings("unchecked")
	private void getTransferContractRequest(Long requestId)throws Exception
	{

		if(requestId!=null && requestId!=0L)
		{
			RequestServiceAgent rsa=new RequestServiceAgent();
			HashMap map=rsa.getTransferContractRequest(requestId);
			requestView=(RequestView)map.get("RequestView");
			contractView=(ContractView)map.get("ContractView");
			
			setExempted(requestView.getIsExempted());
			setTxttotalEvacFines(requestView.getEvacuationFine());
			if(requestView.getRequestId()!=null)
				viewMap.put("REQUEST_ID", requestView.getRequestId());
			if(map.containsKey("NewStartDate") && map.get("NewStartDate")!=null)
				viewMap.put("NEW_START_DATE", convertStringToDate(map.get("NewStartDate").toString()));
			
			if(map.containsKey("NewEndDate") && map.get("NewEndDate")!=null)
				viewMap.put("NEW_END_DATE", convertStringToDate(map.get("NewEndDate").toString()));
			
			if(map.containsKey("EvacuationDate") && map.get("EvacuationDate")!=null)
				this.setEvacuationDate(convertStringToDate(map.get("EvacuationDate").toString()));
			
			if(map.containsKey("NewRentAmount") && map.get("NewRentAmount")!=null)
				{
					newRentAmount.setValue(Double.valueOf(map.get("NewRentAmount").toString()));
					viewMap.put("NEW_RENT_AMOUNT", map.get("NewRentAmount").toString());
				}
			if(map.containsKey("TenantView"))
			{
				newTenantView=(PersonView)map.get("TenantView");
				hdnTDPersonId = newTenantView.getPersonId().toString();
				if(newTenantView!=null) 			
					this.setNewTenantView(newTenantView);
				
				if(newTenantView.getPersonFullName()!=null)
					newTenantName.setValue(newTenantView.getPersonFullName());
				if(newTenantView.getCellNumber()!=null)
					cellNo.setValue(newTenantView.getCellNumber());
			}
			if(map.containsKey("TenantViewBasicInfo"))
			{
				this.setNewTenantViewBasicInfo((PersonView)map.get("TenantViewBasicInfo"));
				//Checking if system tenant is present then no need to show this info
				if(!map.containsKey("TenantView"))
				{
					if(this.getNewTenantViewBasicInfo().getPersonFullName()!=null)
						newTenantName.setValue(this.getNewTenantViewBasicInfo().getPersonFullName());
					if(this.getNewTenantViewBasicInfo().getCellNumber()!=null)
						cellNo.setValue(this.getNewTenantViewBasicInfo().getCellNumber());
					
				}
			}
			newContractView=(ContractView)map.get("NewContractView");
			List<PaymentScheduleView>paymentScheduleList = (ArrayList)map.get("PaymentScheduleList");
			
			if(requestView!=null)
				viewMap.put(REQUEST_VIEW,requestView);
			
			if(contractView!=null)
				viewMap.put(WebConstants.Contract.CONTRACT_VIEW, contractView);
			
			if(newContractView!=null)
				viewMap.put(WebConstants.Contract.NEW_CONTRACT_VIEW, newContractView);
			
			if(paymentScheduleList!=null)
				viewMap.put("TRANSFER_FEE_PAYMENT_SCHEDULE", paymentScheduleList);
			
			
			populateApplicationDataTab(requestView);
			getPageModeBasedOnRequestStatus(requestView);
			if(viewMap.containsKey(PAGE_MODE))
				pageMode=viewMap.get(PAGE_MODE).toString();
			if(!isPostBack())
				{
					
					if(pageMode.equalsIgnoreCase(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT))
						populatePaymentScheduleAgainstContract(contractView.getContractId());
				
					else if(pageMode.equalsIgnoreCase(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT))
						populatePaymentScheduleAgainstContract(newContractView.getContractId());
				
					else
						populatePaymentScheduleAgainstRequest(requestView.getRequestId());
				}
		}
	}
@SuppressWarnings("unchecked")
	private void populatePaymentScheduleAgainstContract(Long contractId)
	{
		List<PaymentScheduleView> paymentList=new ArrayList<PaymentScheduleView>();
		DomainDataView paymentStatusPending=new DomainDataView();
		paymentStatusPending=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		DomainDataView paymentStatusExempted=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		try
		{
		paymentSchedules = propertyServiceAgent.getPaymentScheduleByContractId(contractId);
		if(paymentSchedules !=null && paymentSchedules.size()>0)
		{
			requestView = (RequestView)viewMap.get(REQUEST_VIEW);
			for(PaymentScheduleView psv :paymentSchedules)
			{
				if( psv.getStatusId().compareTo(paymentStatusPending.getDomainDataId())==0 || 
				   ( psv.getStatusId().longValue()  == paymentStatusExempted.getDomainDataId().longValue() &&
					 psv.getRequestId().longValue() == requestView.getRequestId()	   
				    )
				)
				{
					paymentList.add(psv);
				}
			}
		}
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentList);
		viewMap.put(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST,paymentBeanDataList);
		}
		catch (PimsBusinessException e) 
		{
			logger.LogException(" populatePaymentScheduleAgainstContract() crashed ", e);
		}
		
	}
private void getReceivedPaymentsAgainstContract(Long contractId)
{
	List<PaymentScheduleView> paymentList=new ArrayList<PaymentScheduleView>();
	DomainDataView paymentStatusCollected=new DomainDataView();
	paymentStatusCollected=CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_COLLECTED);
	DomainDataView ddvRealized = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
            WebConstants.REALIZED);
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	try{
	paymentSchedules = propertyServiceAgent.getPaymentScheduleByContractId(contractId);
	if(paymentSchedules !=null && paymentSchedules.size()>0)
	{
		for(PaymentScheduleView psv :paymentSchedules)
		{
			if( psv.getStatusId().compareTo(paymentStatusCollected.getDomainDataId())==0 ||
				psv.getStatusId().compareTo(ddvRealized.getDomainDataId())==0	
			   )
				paymentList.add(psv);
		}
	}
		viewMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentList);
	viewMap.put(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST,paymentBeanDataList);
	}
	catch (PimsBusinessException e) 
	{
		logger.LogException(" populatePaymentScheduleAgainstContract() crashed ", e);
	}
	
}

	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
			if(sessionMap.containsKey("TENANTINFO"))
				sessionMap.remove("TENANTINFO");
			if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
				sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
   @SuppressWarnings("unchecked")
	public String btnTenant_Click()
	{
		logger.logInfo("btnTenant_Click start...");
		PersonView tenantView = getContractView().getTenantView();
		Long tenantId = tenantView.getPersonId();
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("tenantId", tenantId);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", 
				                                                          WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TRANSFER_CONTRACT);
		String javaScriptText  = "javascript:showPersonReadOnlyPopup("+tenantId+");";
		openPopUp("",javaScriptText);
		logger.logInfo("btnTenant_Click completed successfully...");
		return "";
	}
   @SuppressWarnings("unchecked")
	public String btnNewTenant_Click()
	{
		logger.logInfo("btnNewTenant_Click start...");
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("tenantId", hdnTDPersonId);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("TENANT_VIEW_MODE", WebConstants.VIEW_MODE_SETUP_IN_VIEW);
		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TRANSFER_CONTRACT);
		logger.logInfo("btnTenant_Click completed successfully...");
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+hdnTDPersonId+");";
		setRequestParam("TENANT_VIEW_MODE",true);
		
		openPopUp("",javaScriptText);
		logger.logInfo("btnContract_Click completed successfully...");
		return "";
	}
   
   @SuppressWarnings("unchecked")
   public void imgAddPerson_Click() 
	{	
		
		String methodName="imgAddPerson_Click";
		logger.logInfo(methodName+"|Started...");
		try 
		{
			    FacesContext facesContext = FacesContext.getCurrentInstance();
				sessionMap.put(WebConstants.PERSON_BASIC_INFO, this.getNewTenantViewBasicInfo());
				String javaScriptText = "javaScript:addBasicInfo();";
   			    AddResource addResource = AddResourceFactory.getInstance(facesContext);
			    addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
			   	logger.logInfo(methodName+"|Finish...");    
		} 
		catch(Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
   		logger.LogException(methodName+"|Error Occured", ex);
   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}


	@SuppressWarnings("unchecked")
	public String btnContract_Click()
	{
		logger.logInfo("btnContract_Click start...");
		contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);		
		Long contractId = contractView.getContractId();
		if(contractId==null)
			contractId = 0L;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractId);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);

		String javaScriptText="var screen_width = screen.width;";
		javaScriptText+="var screen_height = screen.height;";
		javaScriptText+="window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
		WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
		WebConstants.VIEW_MODE+"="+
		WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
		"','_blank','width='+(screen_width- 400)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";
		openPopUp("LeaseContract.jsf",javaScriptText);

		logger.logInfo("btnContract_Click completed successfully...");
		return "";
	}

	@SuppressWarnings("unchecked")
	public void openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		try 
		{
			List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);		
			contractView = this.getContractView();
			DomainDataView ddvRenewDraft =  CommonUtil.getIdFromType(ddvList ,WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
			DomainDataView ddvCollected  =  CommonUtil.getIdFromType(ddvList ,WebConstants.PAYMENT_SCHEDULE_COLLECTED);
			DomainDataView ddvRefunded   =  CommonUtil.getIdFromType(ddvList ,WebConstants.PAYMENT_SCHEDULE_STATUS_REFUNDED);
			DomainDataView ddvPending    =  CommonUtil.getIdFromType(ddvList ,WebConstants.PAYMENT_SCHEDULE_PENDING);
			DomainDataView ddvExempted   =  CommonUtil.getIdFromType(ddvList ,WebConstants.PAYMENT_SCHEDULE_STATUS_EXEMPTED);
			DomainDataView ddvRealized   =  CommonUtil.getIdFromType(ddvList ,  WebConstants.REALIZED);
			if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
					viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null		
			)
			{
				List<PaymentScheduleView> payments = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
				PaymentScheduleView paymentScheduleView;
				Date date=new Date();
				for (int i=0; i<payments.size(); i++)
				{
					long statusId = payments.get( i ).getStatusId().longValue();
					long typeId = payments.get(i).getTypeId().longValue();
					if(     statusId ==  ddvCollected.getDomainDataId().longValue() ||
							statusId ==  ddvRealized.getDomainDataId().longValue()  ||
							statusId ==  ddvRefunded.getDomainDataId().longValue()  || 
							statusId ==  ddvExempted.getDomainDataId().longValue() 
					       )
					{
						continue;
					}
					
					if( viewMap.containsKey( PAGE_MODE ) && viewMap.get( PAGE_MODE ).equals( PAGE_MODE_COLLECT_APP_FEE ) )
					{
						boolean isRentOrDeposit = typeId  == WebConstants.PAYMENT_TYPE_RENT_ID.longValue() ||
						                          typeId == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue();
						boolean isDraftOrPending = statusId   == ddvRenewDraft.getDomainDataId().longValue()||
						                           statusId   == ddvPending.getDomainDataId().longValue();
						if( !isRentOrDeposit && isDraftOrPending )
						  
						{
							paymentScheduleView = payments.get(i);
							paymentScheduleView.setUpdatedBy(getLoggedInUser());
							paymentScheduleView.setUpdatedOn(date);
							paymentsForCollection.add(payments.get(i));
						}
						
					}
					else if( viewMap.containsKey( PAGE_MODE ) && viewMap.get( PAGE_MODE ).equals(  PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT ) )
					{
						if( payments.get(i).getContractId()!=null && 
							payments.get(i).getContractId().compareTo(contractView.getContractId())==0
						   )
						{
							  paymentsForCollection.add( payments.get( i ) );
						}
					}
					else
					{
	
						paymentsForCollection.add( payments.get( i ) );
					}
					
				}
				if(paymentsForCollection.size()>0)
				{
				   sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
				}
			}
			if( sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION ) )
			{
				PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
				sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
				openPopUp("receivePayment.jsf", "javascript:openPopupReceivePayment();");
			}
			else
			{
				ApproveRejectTask(TaskOutcome.COLLECT);
				viewMap.put(PAGE_MODE,PAGE_MODE_PAYMENT_COLLECTED);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_ZERO_PAYMENTS_COLLECTED));	
			}
	
			logger.logInfo(methodName+"|"+"Finish..");
			
			} catch (Exception e) 
			{
				logger.LogException(methodName+"| crashed...", e);
				successMessages.clear();
				errorMessages.clear(); 
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	
	}
	@SuppressWarnings( "unchecked" )
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');";
		if(extraJavaScript!=null && extraJavaScript.length()>0)
			javaScriptText=extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
     @SuppressWarnings("unchecked")
	public String viewNewContract()
	{
		newContractView = (ContractView)viewMap.get(WebConstants.Contract.NEW_CONTRACT_VIEW);
		logger.logInfo("viewNewContract start...");
		Long contractId = newContractView.getContractId();
		if(contractId==null)
			contractId = 0L;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractId);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW,WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);

		String javaScriptText="var screen_width = screen.width;";
		javaScriptText+="var screen_height = screen.height;";
		javaScriptText+="window.open('LeaseContract.jsf?"+WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"="+
		WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW+"&"+
		WebConstants.VIEW_MODE+"="+
		WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP+
		"','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=yes');";
		openPopUp("LeaseContract.jsf",javaScriptText);

		logger.logInfo("viewNewContract completed successfully...");
		return "";
	}

	private Boolean validateFieldsForRequestApproval() throws PimsBusinessException
	{
		String methodName="validateFieldsForRequestApproval";
		Boolean validated = true;
		logger.logInfo(methodName+"|"+"Start..");
		try{
		if(newRentAmount.getValue()==null||newRentAmount.getValue().toString().trim().length()==0
				||Double.parseDouble(newRentAmount.getValue().toString())==0){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_NEW_RENT_AMOUNT));
			validated=false;
		}
		if(newRentAmount.getValue()!=null&&newRentAmount.getValue().toString().trim().length() > 0&&
				Math.round(Double.parseDouble(newRentAmount.getValue().toString()))>99999999&&
				Math.round(totalRentAmount)!=Math.round(Double.valueOf(newRentAmount.getValue().toString()))){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_PAYMENT_RECEIVED));
			validated=false;
		}
		}
		catch (NumberFormatException e) {
			validated = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));

		}
		catch (IllegalArgumentException e) {
			validated = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));

		}
		if(newStartDate==null){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_START_DATE));
			validated=false;
		}
		if(newEndDate==null){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_END_DATE));
			validated=false;
		}

//		if(!viewMap.containsKey("rentSaved"))
//		{
//		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.PropertyKeys.Tenant.NEW_TENANT_NAME);
//		validated=false;
//		return validated;
//		}
 if(this.getEvacuationDate()==null)
		 {
				 validated = false;
				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.SETTLEMENT_DATE_INVALID));
			 //return validated;
		 }

		logger.logInfo(methodName+"|"+"Finish..");
		return validated;
	}


	private Boolean validateToIssueContract() throws PimsBusinessException
	{
		String methodName="validateToIssueContract";
		Boolean validated = true;
		logger.logInfo(methodName+"|"+"Start..");

		//Date startDate = (Date)(viewMap.get("NEW_START_DATE"));
		//Date endDate = (Date)(viewMap.get("NEW_END_DATE"));
		//Integer monthsBetweenNewContract = CommonUtil.getMonthsBetween(startDate, endDate);
		//Integer noOfPayments = noOfCashPayments + noOfChequePayments;

		/*if(noOfPayments!=monthsBetweenNewContract){
			errorMessages.add(CommonUtil.getParamBundleMessage(MessageConstants.TransferLeaseContract.MSG_VALIDATION_FAILED_NO_OF_PAYMENTS,monthsBetweenNewContract));
			validated=false;
			return validated;
		}
*/
		logger.logInfo(methodName+"|"+"Finish..");
		return validated;
	}

	@SuppressWarnings( "unchecked" )
	private boolean validateFields() throws PimsBusinessException
	{
		String methodName="validateFields";
		boolean validated = true;
		logger.logInfo(methodName+"|"+"Start..");
		contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
		PersonView tenantView = contractView.getTenantView(); 
		successMessages.clear();
		errorMessages.clear();		
		newTenantView = getNewTenantView();
		if(tenantView.getPersonId().equals(newTenantView.getPersonId()))
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ChangeTenantName.MSG_SAME_CURRENTANDNEWTENANT));
			validated=false;
			//return validated;
		}
//		else if(!CommonUtil.isPersonContainCountry(this.getNewTenantView()))
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.NationalyNotAdded"));
//    		validated = false;
//    	}
		if(!viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) || viewMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ==null)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			tabPanel.setSelectedTab("applicationTab");
			validated=false;
		}
		

//		Date settlementDate = (Date)this.getEvacuationDate();
//
//		Date startDate =  contractView.getStartDate();
		Date endDate =  contractView.getEndDate();		

		/*		if(newRentAmount.getValue()==null){
			errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Contract.RENT_VALUE));
			validated=false;

			//return validated;
		}
		 */
		//if(!viewMap.containsKey(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME))
		if(this.getNewTenantView()==null || this.getNewTenantView().getPersonId()==null)
		{
			 
			if(this.getNewTenantViewBasicInfo()!=null && this.getNewTenantViewBasicInfo().getPersonFullName()!=null)
				 errorMessages.add(CommonUtil.getFieldRequiredMessage("transferContract.msg.personNotAdded"));
			else
			     errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Tenant.NEW_TENANT_NAME));
			 
			validated=false;
			 //return validated;
		 }	
//	Code is commented after Danish's mail to remove this validation on 19th May 2010.	
//		 if(settlementDate==null)
//		 {
//			 errorMessages.add(CommonUtil.getFieldRequiredMessage(WebConstants.PropertyKeys.Contract.SETTLEMENT_DATE));
//			 validated=false;
//			 //return validated;
//		 }
//		 else
//		 {
//			 if(endDate!=null&&startDate!=null)
//			 {
//				 if(settlementDate.after(endDate) || settlementDate.before(startDate))
//				 {
//					 validated = false;
//					 //errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.SETTLEMENT_DATE_INVALID));
//				 }
//				 //return validated;
//			 }
//		 }

		 logger.logInfo(methodName+"|"+"Finish..");
		 return validated;
	}
	private Set<RequestFieldDetailView> setRequestFieldDetailViewForNewContract(RequestFieldDetailView requestFieldDetailView ) throws PimsBusinessException {

		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());

		requestFieldDetailView.setRequestKeyValue(requestView.getRequestId().toString());
		
		RequestKeyView requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_CONTRACT_ID);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		if(requestKeyView!=null)
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());

		Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>();

		if(newRentAmount.getValue()==null)
			newRentAmount.setValue("0");
		requestFieldDetailView.setRequestKeyValue(newRentAmount.getValue().toString());
		requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_RENT_AMOUNT);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		if(requestKeyView!=null)
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		requestFieldDetailViewSet.add(requestFieldDetailView);
		if(newStartDate!=null){
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(newStartDate));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_START_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}

		if(newEndDate!=null){
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(newEndDate));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_END_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		if(this.getEvacuationDate()!=null){
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(this.getEvacuationDate()));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_EVACUATION_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		return requestFieldDetailViewSet;

	}
	private void setRequestFieldDetailBasicFields(RequestFieldDetailView requestFieldDetailView){
		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
	}

	private Set<RequestFieldDetailView> setRequestFieldDetailView( ) throws PimsBusinessException 
	{
		RequestFieldDetailView requestFieldDetailView = new RequestFieldDetailView();
		Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>();

		
		if(hdnTDPersonId==null)
			hdnTDPersonId = "0";
		requestFieldDetailView.setRequestKeyValue(hdnTDPersonId);
		RequestKeyView  requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_TENANT_ID);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		if(requestKeyView!=null)
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		setRequestFieldDetailBasicFields(requestFieldDetailView);
		requestFieldDetailViewSet.add(requestFieldDetailView);

		//OLD CONTRACT ID
		requestFieldDetailView = new RequestFieldDetailView();
		setRequestFieldDetailBasicFields(requestFieldDetailView);
		requestFieldDetailView.setRequestKeyValue(this.getContractView().getContractId().toString());
		requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_OLD_CONTRACT_ID_FOR_TRANSFER);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		if(requestKeyView!=null)
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		requestFieldDetailViewSet.add(requestFieldDetailView);
		
		
		//New Rent AMOUNT
		requestFieldDetailView = new RequestFieldDetailView();
		setRequestFieldDetailBasicFields(requestFieldDetailView);
		if(newRentAmount.getValue()==null)
			newRentAmount.setValue("0");
		requestFieldDetailView.setRequestKeyValue(newRentAmount.getValue().toString());
		requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_RENT_AMOUNT);
		requestFieldDetailView.setRequestKeyView(requestKeyView);
		if(requestKeyView!=null)
			requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
		requestFieldDetailViewSet.add(requestFieldDetailView);
		
		
		if(newStartDate!=null)
		{
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(newStartDate));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_START_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		
		if(this.getEvacuationDate()!=null)
		{
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(this.getEvacuationDate()));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_EVACUATION_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		if(newEndDate!=null)
		{
			requestFieldDetailView = new RequestFieldDetailView();
			setRequestFieldDetailBasicFields(requestFieldDetailView);
			requestFieldDetailView.setRequestKeyValue(CommonUtil.getStringFromDate(newEndDate));
			requestKeyView = commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_TRANSFER_LEASE_CONTRACT,WebConstants.REQUEST_KEY_NEW_END_DATE);
			requestFieldDetailView.setRequestKeyView(requestKeyView);
			if(requestKeyView!=null)
				requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
			requestFieldDetailViewSet.add(requestFieldDetailView);
		}
		return requestFieldDetailViewSet;
	}
	@SuppressWarnings("unchecked")
	private void setRequestView() 
	{

		List<DomainDataView> ddvList;
		DomainDataView ddv;
       if(viewMap.get(REQUEST_VIEW)!=null)
          requestView = (RequestView)viewMap.get(REQUEST_VIEW);
       else
       {
    	   requestView.setContractId(null);
    	   requestView.setCreatedBy(CommonUtil.getLoggedInUser());
   		   requestView.setCreatedOn(new Date());
   		   requestView.setRecordStatus(new Long(1));
		   requestView.setIsDeleted(new Long(0));
		   requestView.setRequestDate(new Date());
		   ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_SOURCE);
		   ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_SOURCE_TENANT);
		   requestView.setRequestSource(ddv.getDomainDataId());

			ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
			ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_ACCEPTED);
			requestView.setStatusId(ddv.getDomainDataId());

			ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_ORIGINATION);
			ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_ORIGINATION_CSC);
			requestView.setRequestOrigination(ddv.getDomainDataId());

			ddvList=CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_PRIORITY);
			ddv=CommonUtil.getIdFromType(ddvList, WebConstants.REQUEST_PRIORITY_NORMAL);
			requestView.setRequestPriorityId(ddv.getDomainDataId());

       }
		PersonView applicantView = new PersonView();
		applicantView.setPersonId(Long.valueOf(hdnPersonId));
		requestView.setApplicantView(applicantView);
		requestView.setUpdatedBy(CommonUtil.getLoggedInUser());
		requestView.setUpdatedOn(new Date());
		contractView.setSettlementDate(this.getEvacuationDate());
		requestView.setContractView(contractView);
	}

	public Boolean addRequestFieldDetailForNewContract(Long requestId){
		Boolean success = false;
//		RequestFieldDetailView requestFieldDetailView = new RequestFieldDetailView();
		try
		{
			logger.logInfo("fillRequestView started...");
			Set<RequestFieldDetailView> requestFieldDetailViewSet = setRequestFieldDetailViewForNewContract(new RequestFieldDetailView());

			RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
			ArrayList<String> requestKeys= new ArrayList<String>(0);
			requestKeys.add(WebConstants.REQUEST_KEY_NEW_CONTRACT_ID);
			requestKeys.add(WebConstants.REQUEST_KEY_NEW_START_DATE);
			requestKeys.add(WebConstants.REQUEST_KEY_NEW_END_DATE);
			requestKeys.add(WebConstants.REQUEST_KEY_NEW_RENT_AMOUNT);
			requestKeys.add(WebConstants.REQUEST_KEY_EVACUATION_DATE);
			requestServiceAgent.addRequestFieldDetailSet(requestId,requestFieldDetailViewSet,requestKeys);
			

			logger.logInfo("fillRequestView completed successfully...");
		}
		catch(PimsBusinessException pimsBusinessException){
			success =false;
			logger.LogException("fillRequestView crashed...", pimsBusinessException);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUBMIT_FAILURE));
		}

		return success;
	}

	public void fillRequestView()throws Exception
	{
			logger.logInfo("fillRequestView started...");
			setRequestView();
			Set<RequestFieldDetailView> requestFieldDetailViewSet = setRequestFieldDetailView();
			requestView.setRequestFieldDetailsView(requestFieldDetailViewSet);
			logger.logInfo("fillRequestView completed successfully...");
	}
	public void reprintReport()
	{
		String methodName = "reprintReport|";
		logger.logDebug( methodName + "Start");
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		SettlementReportCriteria reportCriteria ;
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_EN,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		else
			reportCriteria = new SettlementReportCriteria(ReportConstant.Report.SETTLEMENT_REPORT_AR,ReportConstant.Processor.SETTLEMENT_REPORT ,CommonUtil.getLoggedInUser());
		requestView = ( RequestView )viewMap.get( REQUEST_VIEW );
		reportCriteria.setRequestId( requestView.getRequestId() );
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
    	String javaScriptText = "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');";
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		logger.logDebug( methodName + "Finish");

		
	}
	
	@SuppressWarnings("unchecked")
	public void btnSave_Click()
	{
		String methodName="btnSave_Click|";
		logger.logInfo(methodName+ "start...");
		forSaveMethod=true;
		try
		{
			if(validateFields())
			{
				boolean isNewRequest=false;
			    fillRequestView();
				RequestServiceAgent rsa=new RequestServiceAgent();
				if(requestView.getRequestId()==null)
					isNewRequest =true;
				requestView = rsa.persistTransferContractRequest(requestView);
	            String successMessage=java.text.MessageFormat.format(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_REQ_SAVE),
	            		
	    				requestView.getRequestNumber());
	    	    if(!isNewRequest)
	    	    	{
	    	    		successMessage=ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_REQ_UPDATE);
	    	    		CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_UPDATED, requestView.getRequestId());
	    	    	}
	    	    else
	    	    	CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_SAVED, requestView.getRequestId());
				
	            viewMap.put(REQUEST_VIEW,requestView);
				saveAttachmentsComments();
	            populateApplicationDataTab(requestView);
				//modeAction();
	            	
				
				successMessages.add(successMessage);
				viewMap.put(PAGE_MODE,PAGE_MODE_NEW);
	
			}
			logger.logInfo(methodName +" Finish");
		}
		catch (Exception e) 
		{
			logger.LogException(methodName + "Exception Occured::" , e);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void saveAttachmentsComments()throws PimsBusinessException 
	{
		CommonUtil.loadAttachmentsAndComments(requestView.getRequestId().toString());
		CommonUtil.saveAttachments(requestView.getRequestId());
		CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
		if( comments != null && comments.getValue()!=null && comments.getValue().toString().trim().length() > 0 )
			CommonUtil.saveRemarksAsComments(requestView.getRequestId(), comments.getValue().toString(), noteOwner);
	}
	@SuppressWarnings("unchecked")
	public void btnSubmit_Click()
	{
		try 
		{
			if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_NEW))
			{				
				if(validateFields())
				{
					if(viewMap.get(REQUEST_VIEW)!=null)
					{
				          requestView = (RequestView)viewMap.get(REQUEST_VIEW);
				        
				        String endPoint= parameters.getParameter(WebConstants.TRANSFER_CONTRACT_BPEL_ENDPOINT);
						saveAttachmentsComments();
						PIMSTransferContractBPELPortClient port=new PIMSTransferContractBPELPortClient();
						port.setEndpoint(endPoint);
						port.initiate(contractView.getContractId(),requestView.getRequestId(), CommonUtil.getLoggedInUser(), null, null);
						notifyRequestStatus(WebConstants.Notification_MetaEvents.Event_TransferContract_Request_Received);
						modeAction();
						CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL, requestView.getRequestId());
						successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_REQ_SENT_FOR_APPROVAL));
						viewMap.put(PAGE_MODE,PAGE_MODE_NEW_DONE);//.toString().equals(PAGE_MODE_UPDATE_APPROVE
						CommonUtil.printReport(requestView.getRequestId());
						
						
					}
				}
			}
			
		}
		catch (Exception e) 
		{
			logger.LogException("btnSubmit_Click|Exception Occured::" , e);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_SUBMIT_FAILURE));
		}

		logger.logInfo("btnSubmit_Click completed successfully...");
	}
	private void modeAction()
	{
	
		address.setReadonly(true);
		cellNo.setReadonly(true);
		newRentAmount.setReadonly(true);
		approveReview.setRendered(false);
		rejectReview.setRendered(false);
		comments.setReadonly(true);
		email.setReadonly(true);
		emirate.setReadonly(true);
		county.setReadonly(true);
		renderAddTenantLink =false;
	}

	@SuppressWarnings( "unchecked" )
	public String btnBack_Click()
	{
		String returnScreen = "";
		logger.logInfo("btnBack_Click start...");

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		String backScreen = viewMap.get(WebConstants.BACK_SCREEN)!=null ? viewMap.get(WebConstants.BACK_SCREEN).toString():"";

		if(backScreen.equals(WebConstants.BACK_SCREEN_TASK_LIST))
			returnScreen = "TASK_LIST";
		else if(backScreen.equals(WebConstants.BACK_SCREEN_CONTRACT_SEARCH))
			{
				sessionMap.put("preserveSearchCriteria", true);
				returnScreen = "CONTRACT_SEARCH";
			}
		else if(backScreen.equals(WebConstants.BACK_SCREEN_REQUEST_SEARCH))
			returnScreen = "REQUEST_SEARCH";
		else if(backScreen.equals(WebConstants.BACK_SCREEN_LEASE_CONTRACT))
		{
			if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW))
			{
				ContractView cv=(ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
				sessionMap.put("contractId", cv.getContractId());
				return "LEASE_CONTRACT";
			}

		}   
		logger.logDebug("btnBack_Click |" + "BackScreen = >> " + backScreen);
		logger.logInfo("btnBack_Click completed successfully...");

		return returnScreen;
	}
	@SuppressWarnings( "unchecked" )
	public String btnPay_Click()
	{
		String methodName ="btnPay_Click";
		logger.logInfo(methodName + "|" + "Start..");
		errorMessages=new ArrayList<String>(0);
		errorMessages.clear();
		successMessages=new ArrayList<String>(0);
		successMessages.clear();

		try
		{
//			getFacesContext().getExnalContext().getSessionMap().remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			if(!sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW)){

				if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_SETTLED_APPROVED))
				{
					this.createReceiptPayment(this.getPaymentBeanDataList());
				}
				else if(viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_TRANSFER_FEE_COLLECTION))
				{				
					List<PaymentScheduleView> paymentList=new ArrayList<PaymentScheduleView>();
					if(viewMap.containsKey("TRANSFER_FEE_PAYMENT_SCHEDULE"))
					{
						paymentList.addAll((ArrayList)viewMap.get("TRANSFER_FEE_PAYMENT_SCHEDULE"));
					}

					PaymentScheduleView paymentScheduleViewRow=(PaymentScheduleView)paymentList.get(0);
					PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
					paymentReceiptView.setAmount(getTotalPaymentScheduleAmount());

					paymentReceiptView.setPaymentScheduleView(paymentScheduleViewRow);
					sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
				}
			}
			requestView=(RequestView)viewMap.get(REQUEST_VIEW);

			logger.logDebug(methodName + "|" + "Save Attachments...Start");
			CommonUtil.saveAttachments(requestView.getRequestId());
			logger.logDebug(methodName + "|" + "Save Attachments...Finish");

			logger.logDebug(methodName + "|" + "Save Comments...Start");
			CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
		    CommonUtil.saveComments(requestView.getRequestId(),noteOwner);
			logger.logDebug(methodName + "|" + "Save Comments...Finish");

			openPopUp("receivePayment.jsf");

		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	public String btnPrintLetter_Click(){
		String methodName ="btnPrintLetter_Click";
		logger.logInfo(methodName + "|" + "Start..");
		errorMessages=new ArrayList<String>(0);
		errorMessages.clear();
		successMessages=new ArrayList<String>(0);
		successMessages.clear();

		try
		{
			//
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_TRANSFER_CONTRACT_CLEARANCE_LETTER_PRINTED));
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings( "unchecked" )
	private boolean createReceiptPayment(List<TransferPaymentBean> TransferPaymentBeans) {
		//Now creating the receipt payment view object

		List<DomainDataView> ddview = CommonUtil.getDomainDataListForDomainType(Constant.PAYMENT_SCHEDULE_TYPE);
		DomainDataView refund = CommonUtil.getIdFromType(ddview, Constant.PAYMENT_SCHEDULE_TYPE_EVACUATION_REFUND_AMOUNT);
		DomainDataView due = CommonUtil.getIdFromType(ddview, Constant.PAYMENT_SCHEDULE_TYPE_EVACUATION_DUE_AMOUNT);

		PaymentReceiptView paymentReceiptView = viewContext.getAttribute(WebConstants.PAYMENT_RECEIPT_VIEW);
		if (paymentReceiptView == null)
			paymentReceiptView = new PaymentReceiptView();
		for (TransferPaymentBean pb : TransferPaymentBeans) {
			if (pb.getPaymentScheduleView().getTypeId().equals(due.getDomainDataId()) || pb.getPaymentScheduleView().getTypeId().equals(refund.getDomainDataId())) {
				paymentReceiptView.setAmount(pb.getPaymentScheduleView().getAmount());
				paymentReceiptView.setPaymentScheduleView(pb.getPaymentScheduleView());
			}
		}
		webContext.setAttribute(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
		return true;
	}
	@SuppressWarnings( "unchecked" )
	private Double getTotalPaymentScheduleAmount() throws Exception{
		Double amount=new Double(0);
		List<PaymentScheduleView> paymentList=new ArrayList<PaymentScheduleView>();
		if(viewMap.containsKey("TRANSFER_FEE_PAYMENT_SCHEDULE"))
		{
			paymentList.addAll((ArrayList<PaymentScheduleView>)viewMap.get("TRANSFER_FEE_PAYMENT_SCHEDULE"));
		}
		for (PaymentScheduleView paymentScheduleView : paymentList) 
		{	
			amount+=paymentScheduleView.getAmount();
		}
		return amount;
	}

	@SuppressWarnings( "unchecked" )
	public String openPopUp(String URLtoOpen)
	{
		String methodName="openPopUp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=40,scrollbars=no,status=yes');";
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPaymentSchedules() 
	{
		if (viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null){
			paymentSchedules = (List<PaymentScheduleView>) viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			for(PaymentScheduleView paymentScheduleView:paymentSchedules)
			{
				if(paymentScheduleView.getTypeId()==WebConstants.PAYMENT_TYPE_FEES_ID.longValue())
					paymentScheduleView.setShowSelect(false);
				else
					paymentScheduleView.setShowSelect(true);
			}
		}
		return paymentSchedules;
	}
	@SuppressWarnings( "unchecked" )
	private List<TransferPaymentBean> populatePaymentsTab(Long contractId) 
	{

		boolean english = CommonUtil.getLocale().equalsIgnoreCase("en");
		List<TransferPaymentBean> TransferPaymentBeans = new ArrayList<TransferPaymentBean>();
		List<PaymentScheduleView> payments = null;
		try 
		{
			EvacuationServiceAgent evacuationService = new EvacuationServiceAgent();
			payments = evacuationService.getPaymentSchedules(new Long(contractId));
		} catch (PimsBusinessException e) {
			logger.LogException("Error:CancelContract while populating the paymentsTab", e);
		}

		if (payments != null) 
		{
			for (PaymentScheduleView view : payments) 
			{
				TransferPaymentBean pb = new TransferPaymentBean();
				pb.setAmount(String.valueOf(view.getAmount()));
				if (view.getPaymentDueOn() != null)
					pb.setDueDate(view.getPaymentDueOn().toString());
				pb.setPaymentMethod(english?view.getPaymentModeEn():view.getPaymentModeAr());
				if(pb.getPaymentMethod() == null)
					pb.setPaymentMethod("N/A");
				if (view.getPaymentDate() != null)
					pb.setPaymentDate(view.getPaymentDate().toString());
				pb.setStatus(english?view.getStatusEn():view.getStatusAr());
				pb.setPaymentType(english?view.getTypeEn():view.getTypeAr());
				pb.setPaymentScheduleView(view);
				TransferPaymentBeans.add(pb);
			}
		}
		return TransferPaymentBeans;

	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	@SuppressWarnings( "unchecked" )
	public String getLocale(){

		return CommonUtil.getLocale();
	}

	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}

	/**
	 * @return the contractView
	 */
	public ContractView getContractView() {
		if(viewMap.containsKey(WebConstants.Contract.CONTRACT_VIEW) && viewMap.get(WebConstants.Contract.CONTRACT_VIEW)!=null)
			contractView = (ContractView)viewMap.get(WebConstants.Contract.CONTRACT_VIEW);
		return contractView;
	}

	/**
	 * @param contractView the contractView to set
	 */
	@SuppressWarnings( "unchecked" )
	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
		if(this.contractView!=null)
			viewMap.put(WebConstants.Contract.CONTRACT_VIEW,this.contractView);
	}

	/**
	 * @return the newTenantView
	 */
	public PersonView getNewTenantView() {
		if(viewMap.get(WebConstants.Tenant.TENANT_VIEW)!=null)
			newTenantView = (PersonView) viewMap.get(WebConstants.Tenant.TENANT_VIEW);
		return newTenantView;
	}

	/**
	 * @param newTenantView the newTenantView to set
	 */
	@SuppressWarnings( "unchecked" )
	public void setNewTenantView(PersonView newTenantView) {
		this.newTenantView = newTenantView;
		if(this.newTenantView!=null)
			viewMap.put(WebConstants.Tenant.TENANT_VIEW,this.newTenantView);
	}

	/**
	 * @return the requestView
	 */
	public RequestView getRequestView() {
		return requestView;
	}

	/**
	 * @param requestView the requestView to set
	 */
	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	/**
	 * @return the pageMode
	 */
	public String getPageMode() 
	{
		if(viewMap.get(PAGE_MODE)!=null)
			pageMode=viewMap.get(PAGE_MODE).toString();
		return pageMode;
	}

	/**
	 * @param pageMode the pageMode to set
	 */
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	/**
	 * @return the btnSubmit
	 */
	public HtmlCommandButton getBtnSubmit() {
		return btnSubmit;
	}

	/**
	 * @param btnSubmit the btnSubmit to set
	 */
	public void setBtnSubmit(HtmlCommandButton btnSubmit) {
		this.btnSubmit = btnSubmit;
	}

	/**
	 * @return the btnApprove
	 */
	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	/**
	 * @param btnApprove the btnApprove to set
	 */
	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	/**
	 * @return the btnReject
	 */
	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	/**
	 * @param btnReject the btnReject to set
	 */
	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	/**
	 * @return the btnPay
	 */
	public HtmlCommandButton getBtnPay() {
		return btnPay;
	}

	/**
	 * @param btnPay the btnPay to set
	 */
	public void setBtnPay(HtmlCommandButton btnPay) {
		this.btnPay = btnPay;
	}

	/**
	 * @return the btnComplete
	 */
	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	/**
	 * @param btnComplete the btnComplete to set
	 */
	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	/**
	 * @return the btnNewTenant
	 */
	public HtmlCommandButton getBtnNewTenant() {
		return btnNewTenant;
	}

	/**
	 * @param btnNewTenant the btnNewTenant to set
	 */
	public void setBtnNewTenant(HtmlCommandButton btnNewTenant) {
		this.btnNewTenant = btnNewTenant;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the successMessages
	 */
	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);
	}

	/**
	 * @param successMessages the successMessages to set
	 */
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	/**
	 * @return the isEnglishLocale
	 */
	@SuppressWarnings( "unchecked" )
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	@SuppressWarnings( "unchecked" )
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @return the isArabicLocale
	 */
	@SuppressWarnings( "unchecked" )
	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	@SuppressWarnings( "unchecked" )
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	/**
	 * @return the unitDataList
	 */
	@SuppressWarnings( "unchecked" )
	public List<ContractUnitView> getUnitDataList() {
		unitDataList = (List<ContractUnitView>) viewMap.get(WebConstants.Contract.CONTRACT_UNIT_LIST);

		if(unitDataList == null)
			unitDataList = new ArrayList<ContractUnitView>();

		return unitDataList;
	}



	/**
	 * @return the unitDataTable
	 */
	public HtmlDataTable getUnitDataTable() {
		return unitDataTable;
	}

	/**
	 * @param unitDataTable the unitDataTable to set
	 */
	public void setUnitDataTable(HtmlDataTable unitDataTable) {
		this.unitDataTable = unitDataTable;
	}

	/**
	 * @return the unitDataItem
	 */
	public ContractUnitView getUnitDataItem() {
		return unitDataItem;
	}

	/**
	 * @param unitDataItem the unitDataItem to set
	 */
	public void setUnitDataItem(ContractUnitView unitDataItem) {
		this.unitDataItem = unitDataItem;
	}

	/**
	 * @param unitDataList the unitDataList to set
	 */
	public void setUnitDataList(List<ContractUnitView> unitDataList) {
		this.unitDataList = unitDataList;
	}

	/**
	 * @return the tbl_RequiredDocs
	 */
	public HtmlDataTable getTbl_RequiredDocs() {
		return tbl_RequiredDocs;
	}

	/**
	 * @param tbl_RequiredDocs the tbl_RequiredDocs to set
	 */
	public void setTbl_RequiredDocs(HtmlDataTable tbl_RequiredDocs) {
		this.tbl_RequiredDocs = tbl_RequiredDocs;
	}

	/**
	 * @return the rdDataItem
	 */
	public RequiredDocumentsView getRdDataItem() {
		return rdDataItem;
	}

	/**
	 * @param rdDataItem the rdDataItem to set
	 */
	public void setRdDataItem(RequiredDocumentsView rdDataItem) {
		this.rdDataItem = rdDataItem;
	}

	/**
	 * @return the rdDataList
	 */
	@SuppressWarnings( "unchecked" )
	public List<RequiredDocumentsView> getRdDataList() {
		if(viewMap.containsKey(WebConstants.Contract.TRANSFER_CONTRACT_REQ_DOC))
			rdDataList = (List<RequiredDocumentsView>)viewMap.get(WebConstants.Contract.TRANSFER_CONTRACT_REQ_DOC);

		return rdDataList;
	}

	/**
	 * @param rdDataList the rdDataList to set
	 */
	public void setRdDataList(List<RequiredDocumentsView> rdDataList) {
		this.rdDataList = rdDataList;
	}

	/**
	 * @return the newRentValue
	 */
	public Double getNewRentValue() {
		newRentValue = (Double)viewMap.get("newRentValue");
		return newRentValue;
	}

	/**
	 * @param newRentValue the newRentValue to set
	 */
	public void setNewRentValue(Double newRentValue) {
		this.newRentValue = newRentValue;
	}

	/**
	 * @return the btnSaveNewRent
	 */
	public HtmlCommandButton getBtnSaveNewRent() {
		return btnSaveNewRent;
	}

	/**
	 * @param btnSaveNewRent the btnSaveNewRent to set
	 */
	public void setBtnSaveNewRent(HtmlCommandButton btnSaveNewRent) {
		this.btnSaveNewRent = btnSaveNewRent;
	}

	/**
	 * @return the rentValuePanelGrid
	 */
	public HtmlPanelGrid getRentValuePanelGrid() {
		return rentValuePanelGrid;
	}

	/**
	 * @param rentValuePanelGrid the rentValuePanelGrid to set
	 */
	public void setRentValuePanelGrid(HtmlPanelGrid rentValuePanelGrid) {
		this.rentValuePanelGrid = rentValuePanelGrid;
	}

	/**
	 * @return the renderSettlementDate
	 */
	public Boolean getRenderSettlementDate() {
		return renderSettlementDate;
	}

	/**
	 * @param renderSettlementDate the renderSettlementDate to set
	 */
	public void setRenderSettlementDate(Boolean renderSettlementDate) {
		this.renderSettlementDate = renderSettlementDate;
	}

	/**
	 * @return the disableSettlementDate
	 */
	public Boolean getDisableSettlementDate() {
		return disableSettlementDate;
	}

	/**
	 * @param disableSettlementDate the disableSettlementDate to set
	 */
	public void setDisableSettlementDate(Boolean disableSettlementDate) {
		this.disableSettlementDate = disableSettlementDate;
	}

	/**
	 * @return the selectedTab
	 */
	public String getSelectedTab() {
//		if(selectedTab==null || selectedTab.equals(""))
//		selectedTab = WebConstants.TransferContract.UNIT_TAB;
		return selectedTab;
	}

	/**
	 * @param selectedTab the selectedTab to set
	 */
	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	/**
	 * @return the tbl_PaymentSchedule
	 */
	public HtmlDataTable getTbl_PaymentSchedule() {
		return tbl_PaymentSchedule;
	}

	/**
	 * @param tbl_PaymentSchedule the tbl_PaymentSchedule to set
	 */
	public void setTbl_PaymentSchedule(HtmlDataTable tbl_PaymentSchedule) {
		this.tbl_PaymentSchedule = tbl_PaymentSchedule;
	}

	/**
	 * @return the psDataItem
	 */
	public PaymentScheduleView getPsDataItem() {
		return psDataItem;
	}

	/**
	 * @param psDataItem the psDataItem to set
	 */
	public void setPsDataItem(PaymentScheduleView psDataItem) {
		this.psDataItem = psDataItem;
	}

	/**
	 * @return the psDataList
	 */
	@SuppressWarnings( "unchecked" )
	public List<PaymentScheduleView> getPsDataList() {
		if(viewMap.containsKey("TRANSFER_FEE_PAYMENT_SCHEDULE"))
			psDataList = (List<PaymentScheduleView>)viewMap.get("TRANSFER_FEE_PAYMENT_SCHEDULE");
		return psDataList;
	}

	/**
	 * @param psDataList the psDataList to set
	 */
	public void setPsDataList(List<PaymentScheduleView> psDataList) {
		this.psDataList = psDataList;
	}

	/**
	 * @return the unitStatusColumnEn
	 */
	public HtmlSimpleColumn getUnitStatusColumnEn() {
		return unitStatusColumnEn;
	}

	/**
	 * @param unitStatusColumnEn the unitStatusColumnEn to set
	 */
	public void setUnitStatusColumnEn(HtmlSimpleColumn unitStatusColumnEn) {
		this.unitStatusColumnEn = unitStatusColumnEn;
	}

	/**
	 * @return the unitStatusColumnAr
	 */
	public HtmlSimpleColumn getUnitStatusColumnAr() {
		return unitStatusColumnAr;
	}

	/**
	 * @param unitStatusColumnAr the unitStatusColumnAr to set
	 */
	public void setUnitStatusColumnAr(HtmlSimpleColumn unitStatusColumnAr) {
		this.unitStatusColumnAr = unitStatusColumnAr;
	}

	/**
	 * @return the unitAddressColumn
	 */
	public HtmlSimpleColumn getUnitAddressColumn() {
		return unitAddressColumn;
	}

	/**
	 * @param unitAddressColumn the unitAddressColumn to set
	 */
	public void setUnitAddressColumn(HtmlSimpleColumn unitAddressColumn) {
		this.unitAddressColumn = unitAddressColumn;
	}

	/**
	 * @return the unitActionColumn
	 */
	public HtmlSimpleColumn getUnitActionColumn() {
		return unitActionColumn;
	}

	/**
	 * @param unitActionColumn the unitActionColumn to set
	 */
	public void setUnitActionColumn(HtmlSimpleColumn unitActionColumn) {
		this.unitActionColumn = unitActionColumn;
	}

	/**
	 * @return the enterRentHtmlLabel
	 */
	public HtmlOutputLabel getEnterRentHtmlLabel() {
		return enterRentHtmlLabel;
	}

	/**
	 * @param enterRentHtmlLabel the enterRentHtmlLabel to set
	 */
	public void setEnterRentHtmlLabel(HtmlOutputLabel enterRentHtmlLabel) {
		this.enterRentHtmlLabel = enterRentHtmlLabel;
	}

	/**
	 * @return the unitReadOnlyRentColumn
	 */
	public HtmlSimpleColumn getUnitReadOnlyRentColumn() {
		return unitReadOnlyRentColumn;
	}

	/**
	 * @param unitReadOnlyRentColumn the unitReadOnlyRentColumn to set
	 */
	public void setUnitReadOnlyRentColumn(HtmlSimpleColumn unitReadOnlyRentColumn) {
		this.unitReadOnlyRentColumn = unitReadOnlyRentColumn;
	}

	/**
	 * @return the newRentValueColumn
	 */
	public HtmlSimpleColumn getNewRentValueColumn() {
		return newRentValueColumn;
	}

	/**
	 * @param newRentValueColumn the newRentValueColumn to set
	 */
	public void setNewRentValueColumn(HtmlSimpleColumn newRentValueColumn) {
		this.newRentValueColumn = newRentValueColumn;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	/**
	 * @return the feesTab
	 */
	public HtmlTab getFeesTab() {
		return feesTab;
	}

	/**
	 * @param feesTab the feesTab to set
	 */
	public void setFeesTab(HtmlTab feesTab) {
		this.feesTab = feesTab;
	}

	/**
	 * @return the requestHistoryTab
	 */
	public HtmlTab getRequestHistoryTab() {
		return requestHistoryTab;
	}

	/**
	 * @param requestHistoryTab the requestHistoryTab to set
	 */
	public void setRequestHistoryTab(HtmlTab requestHistoryTab) {
		this.requestHistoryTab = requestHistoryTab;
	}

	/**
	 * @return the paymentsTab
	 */
	public HtmlTab getPaymentsTab() {
		return paymentsTab;
	}

	/**
	 * @param paymentsTab the paymentsTab to set
	 */
	public void setPaymentsTab(HtmlTab paymentsTab) {
		this.paymentsTab = paymentsTab;
	}

	/**
	 * @return the btnIssueContract
	 */
	public HtmlCommandButton getBtnIssueContract() {
		return btnIssueContract;
	}

	/**
	 * @param btnIssueContract the btnIssueContract to set
	 */
	public void setBtnIssueContract(HtmlCommandButton btnIssueContract) {
		this.btnIssueContract = btnIssueContract;
	}

	/**
	 * @return the btnNewContract
	 */
	public HtmlCommandButton getBtnNewContract() {
		return btnNewContract;
	}

	/**
	 * @param btnNewContract the btnNewContract to set
	 */
	public void setBtnNewContract(HtmlCommandButton btnNewContract) {
		this.btnNewContract = btnNewContract;
	}

	public Integer getNoOfCashPayments() {
		return noOfCashPayments;
	}

	public void setNoOfCashPayments(Integer noOfCashPayments) {
		this.noOfCashPayments = noOfCashPayments;
	}

	public Integer getNoOfChequePayments() {
		return noOfChequePayments;
	}

	public void setNoOfChequePayments(Integer noOfChequePayments) {
		this.noOfChequePayments = noOfChequePayments;
	}

	public Boolean getCanIssueContract() {
		if(viewMap.containsKey(PAGE_MODE))
		{
			if(viewMap.get(PAGE_MODE)!=null){
				String currentMode = viewMap.get(PAGE_MODE).toString();
				canIssueContract = currentMode.equals(PAGE_MODE_NEW_CONTRACT)?true:false;
			}
		}
		return canIssueContract;
	}

	public void setCanIssueContract(Boolean canIssueContract) {
		this.canIssueContract = canIssueContract;
	}
	@SuppressWarnings( "unchecked" )
	public List<TransferPaymentBean> getPayments(){
		return (List<TransferPaymentBean>)viewMap.get(BEAN_PAYMENTS);
	}

	public HtmlDataTable getTblPayments() {
		return tblPayments;
	}

	public void setTblPayments(HtmlDataTable tblPayments) {
		this.tblPayments = tblPayments;
	}
	@SuppressWarnings( "unchecked" )
	public List<TransferPaymentBean> getPaymentBeanDataList() {
		if(viewMap.containsKey(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST))
			paymentBeanDataList = (List<TransferPaymentBean>)viewMap.get(WebConstants.Contract.TRANSFER_CONTRACT_PAYMENT_LIST);
		return paymentBeanDataList;
	}

	public void setPaymentBeanDataList(List<TransferPaymentBean> paymentBeanDataList) {
		this.paymentBeanDataList = paymentBeanDataList;
	}


	public TransferPaymentBean getTransferPaymentBean() {
		return TransferPaymentBean;
	}

	public void setTransferPaymentBean(TransferPaymentBean TransferPaymentBean) {
		this.TransferPaymentBean = TransferPaymentBean;
	}

	public HtmlOutputLabel getClickActionColumnHtmlLabel() {
		return clickActionColumnHtmlLabel;
	}

	public void setClickActionColumnHtmlLabel(
			HtmlOutputLabel clickActionColumnHtmlLabel) {
		this.clickActionColumnHtmlLabel = clickActionColumnHtmlLabel;
	}

	public HtmlCommandButton getBtnPrintLetter() {
		return btnPrintLetter;
	}

	public void setBtnPrintLetter(HtmlCommandButton btnPrintLetter) {
		this.btnPrintLetter = btnPrintLetter;
	}

	public HtmlOutputLabel getTotalPaymentsHtmlLabel() {
		return totalPaymentsHtmlLabel;
	}

	public void setTotalPaymentsHtmlLabel(HtmlOutputLabel totalPaymentsHtmlLabel) {
		this.totalPaymentsHtmlLabel = totalPaymentsHtmlLabel;
	}

	public String getTotalPaymentsMsg() {
		totalPaymentsMsg = (String)viewMap.get("totalPaymentsMessage");
		if(totalPaymentsMsg==null)
			totalPaymentsMsg = "";
		return totalPaymentsMsg;
	}

	public void setTotalPaymentsMsg(String totalPaymentsMsg) {
		this.totalPaymentsMsg = totalPaymentsMsg;
	}

	public HtmlCommandLink getPopulateContract() {
		return populateContract;
	}

	public void setPopulateContract(HtmlCommandLink populateContract) {
		this.populateContract = populateContract;
	}
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	@SuppressWarnings( "unchecked" )
	private void calculateSummaray()
	{
		paymentSchedules = (List<PaymentScheduleView>) sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);

		Long cashId = (Long) viewMap.get("cashId");
		Long chqId = (Long) viewMap.get("chqId");

		totalCashAmount = 0;
		totalChqAmount = 0;
		totalRentAmount = 0;
		totalDepositAmount = 0;
		totalFines = 0;
		totalFees = 0;
		installments = 0;

		for (int i=0; i<paymentSchedules.size(); i++)
		{
			//Checking Type
			if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue())
			{
				installments++;
				totalRentAmount = totalRentAmount + paymentSchedules.get(i).getAmount();
			}
			else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue())
			{
				totalDepositAmount = totalDepositAmount + paymentSchedules.get(i).getAmount();
			}
			else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue())
			{
				totalFees = totalFees + paymentSchedules.get(i).getAmount();
			}
			else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue())
			{
				totalFines = totalFines + paymentSchedules.get(i).getAmount();
			}

			//Checking Mode
			if (paymentSchedules.get(i).getPaymentModeId() != null)
			{
				if (paymentSchedules.get(i).getPaymentModeId().longValue() == cashId.longValue())
				{
					totalCashAmount = totalCashAmount +  paymentSchedules.get(i).getAmount();
				}
				else if (paymentSchedules.get(i).getPaymentModeId().longValue() == chqId.longValue())
				{
					totalChqAmount = totalChqAmount + paymentSchedules.get(i).getAmount();
				}
			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public String deletePayment()
	{
		String methodName = "deletePayment|";
		try
		{
            logger.logInfo(methodName +"Start|loggedInUser:%s ", getLoggedInUser());
			int rowID = dataTablePaymentSchedule.getRowIndex();

			paymentSchedules = getPaymentSchedules();
			logger.logInfo(methodName +"Payment:%s ", getLoggedInUser());
			paymentSchedules.remove(rowID);
			sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentSchedules);
			calculateSummaray();
			logger.logInfo(methodName +"Finish|loggedInUser:%s ", getLoggedInUser());
		}catch(Exception e)
		{
			logger.LogException(methodName+ "Error Occured", e);
		}
		return "";
	}
	public Boolean getRequestStatusForColumn() {
		return requestStatusForColumn;
	}

	public void setRequestStatusForColumn(Boolean requestStatusForColumn) {
		this.requestStatusForColumn = requestStatusForColumn;
	}
	
	public String getDateFormatForDataTable() {
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	dateFormatForDataTable = localeInfo.getDateFormat();
		return dateFormatForDataTable;
	}

	public void setDateFormatForDataTable(String dateFormatForDataTable) {
		this.dateFormatForDataTable = dateFormatForDataTable;
	}
	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}
	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}
	public double getTotalCashAmount() {
		return totalCashAmount;
	}
	public void setTotalCashAmount(double totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}
	public double getTotalChqAmount() {
		return totalChqAmount;
	}
	public void setTotalChqAmount(double totalChqAmount) {
		this.totalChqAmount = totalChqAmount;
	}
	public double getTotalDepositAmount() {
		return totalDepositAmount;
	}
	public void setTotalDepositAmount(double totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}
	public double getTotalFees() {
		return totalFees;
	}
	public void setTotalFees(double totalFees) {
		this.totalFees = totalFees;
	}
	public double getTotalFines() {
		return totalFines;
	}
	public void setTotalFines(double totalFines) {
		this.totalFines = totalFines;
	}
	public double getTotalRentAmount() {
		return totalRentAmount;
	}
	public void setTotalRentAmount(double totalRentAmount) {
		this.totalRentAmount = totalRentAmount;
	}
	public HtmlCommandButton getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(HtmlCommandButton requestStatus) {
		this.requestStatus = requestStatus;
	}
	public HtmlInputText getAddress() {
		return address;
	}
	public void setAddress(HtmlInputText address) {
		this.address = address;
	}
	public HtmlInputText getCellNo() {
		if(viewMap.containsKey(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_CELL))
			cellNo.setValue(viewMap.get(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_CELL).toString());
		return cellNo;
	}
	@SuppressWarnings( "unchecked" )
	public void setCellNo(HtmlInputText cellNo) {
		this.cellNo = cellNo;
		if(this.cellNo.getValue()!=null)
			viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_CELL,this.cellNo.getValue().toString());
	}
	public HtmlInputText getComments() {
		return comments;
	}
	public void setComments(HtmlInputText comments) {
		this.comments = comments;
	}
	public HtmlInputText getCounty() {
		return county;
	}
	public void setCounty(HtmlInputText county) {
		this.county = county;
	}
	public HtmlInputText getEmail() {
		return email;
	}
	public void setEmail(HtmlInputText email) {
		this.email = email;
	}
	public HtmlInputText getEmirate() {
		return emirate;
	}
	public void setEmirate(HtmlInputText emirate) {
		this.emirate = emirate;
	}
	public Date getNewEndDate() {
		if(viewMap.containsKey("NEW_END_DATE"))
			newEndDate= (Date)viewMap.get("NEW_END_DATE");
		return newEndDate;
	}
	@SuppressWarnings( "unchecked" )
	public void setNewEndDate(Date newEndDate) {
		this.newEndDate = newEndDate;
		if(this.newEndDate!=null){
			viewMap.put("NEW_END_DATE", newEndDate);
		}
	}
	public Date getEvacuationDate() {
		if(viewMap.containsKey("EVACUATION_DATE"))
			evacuationDate= (Date)(viewMap.get("EVACUATION_DATE"));
		return evacuationDate;
	}
	@SuppressWarnings( "unchecked" )
	public void setEvacuationDate(Date evacuationDate) {
		this.evacuationDate = evacuationDate;
		if(this.evacuationDate!=null){
			viewMap.put("EVACUATION_DATE", evacuationDate);
		}
	}
	public HtmlInputText getNewRentAmount() {
		if(viewMap.containsKey("newRentValue"))
			newRentAmount.setValue(viewMap.get("newRentValue").toString());
		return newRentAmount;
	}
	public void setNewRentAmount(HtmlInputText newRentAmount) {
		this.newRentAmount = newRentAmount;
	}
	@SuppressWarnings( "unchecked" )
	private Date convertStringToDate(String dateStr){

		DateFormat formatter ; 
		Date date =null ; 
		try{     formatter = new SimpleDateFormat(getDateFormat());
		date = formatter.parse(dateStr);    
		System.out.println("Today is " +date );


		date = formatter.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	@SuppressWarnings( "unchecked" )
	public Date getNewStartDate() {
		if(viewMap.containsKey("NEW_START_DATE"))
			newStartDate= (Date)(viewMap.get("NEW_START_DATE"));
		return newStartDate;
	}
	@SuppressWarnings( "unchecked" )
	public void setNewStartDate(Date newStartDate) {
		this.newStartDate = newStartDate;
		if(this.newStartDate!=null){
			viewMap.put("NEW_START_DATE", newStartDate);
		}
	}
	@SuppressWarnings( "unchecked" )
	public HtmlInputText getNewTenantName() {
		if(viewMap.containsKey(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME))
			newTenantName.setValue(viewMap.get(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME).toString());
		return newTenantName;
	}
	@SuppressWarnings( "unchecked" )
	public void setNewTenantName(HtmlInputText newTenantName) {
		this.newTenantName = newTenantName;
		if(this.newTenantName.getValue()!=null)
			viewMap.put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME,this.newTenantName.getValue().toString());

	}
	public HtmlInputText getOldRentValue() {
		return oldRentValue;
	}
	public void setOldRentValue(HtmlInputText oldRentValue) {
		this.oldRentValue = oldRentValue;
	}
	public HtmlDataTable getPendingAppTable() {
		return pendingAppTable;
	}
	public void setPendingAppTable(HtmlDataTable pendingAppTable) {
		this.pendingAppTable = pendingAppTable;
	}
	@SuppressWarnings( "unchecked" )
	public List<RequestView> getRequestViewList() {
		if(viewMap.containsKey("PENDING_REQUEST_DATA_LIST")&&
				viewMap.get("PENDING_REQUEST_DATA_LIST")!=null)
			requestViewList = (ArrayList<RequestView>)viewMap.get("PENDING_REQUEST_DATA_LIST");
		return requestViewList;
	}
	@SuppressWarnings( "unchecked" )
	public void setRequestViewList(List<RequestView> requestViewList) {
		this.requestViewList = requestViewList;
	}
	public String getHdnCellNo() {
		return hdnCellNo;
	}
	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}
	public String getHdnContractId() {
		return hdnContractId;
	}
	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}
	public String getHdnContractNumber() {
		return hdnContractNumber;
	}
	public void setHdnContractNumber(String hdnContractNumber) {
		this.hdnContractNumber = hdnContractNumber;
	}
	public String getHdnIsCompany() {
		return hdnIsCompany;
	}
	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}
	public String getHdnPersonId() {
		return hdnPersonId;
	}
	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}
	public String getHdnPersonName() {
		return hdnPersonName;
	}
	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}
	public String getHdnPersonType() {
		return hdnPersonType;
	}
	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}
	public String getHdnTenantId() {
		return hdnTenantId;
	}
	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}
	public String getHdnTDCellNo() {
		return hdnTDCellNo;
	}
	public void setHdnTDCellNo(String hdnTDCellNo) {
		this.hdnTDCellNo = hdnTDCellNo;
	}
	public String getHdnTDIsCompany() {
		return hdnTDIsCompany;
	}
	public void setHdnTDIsCompany(String hdnTDIsCompany) {
		this.hdnTDIsCompany = hdnTDIsCompany;
	}
	public String getHdnTDPersonId() {
		return hdnTDPersonId;
	}
	public void setHdnTDPersonId(String hdnTDPersonId) {
		this.hdnTDPersonId = hdnTDPersonId;
	}
	public String getHdnTDPersonName() {
		return hdnTDPersonName;
	}
	public void setHdnTDPersonName(String hdnTDPersonName) {
		this.hdnTDPersonName = hdnTDPersonName;
	}
	public String getHdnTDPersonType() {
		return hdnTDPersonType;
	}
	public void setHdnTDPersonType(String hdnTDPersonType) {
		this.hdnTDPersonType = hdnTDPersonType;
	}
	public HtmlCommandButton getApproveReview() {
		return approveReview;
	}
	public void setApproveReview(HtmlCommandButton approveReview) {
		this.approveReview = approveReview;
	}
	public HtmlCommandButton getCancelAppFee() {
		return cancelAppFee;
	}
	public void setCancelAppFee(HtmlCommandButton cancelAppFee) {
		this.cancelAppFee = cancelAppFee;
	}
	public HtmlCommandButton getCollectAppFee() {
		return collectAppFee;
	}
	public void setCollectAppFee(HtmlCommandButton collectAppFee) {
		this.collectAppFee = collectAppFee;
	}
	public HtmlCommandButton getCollectNewContractPayment() {
		return collectNewContractPayment;
	}
	public void setCollectNewContractPayment(
			HtmlCommandButton collectNewContractPayment) {
		this.collectNewContractPayment = collectNewContractPayment;
	}
	public HtmlCommandButton getCollectOldContractPayment() {
		return collectOldContractPayment;
	}
	public void setCollectOldContractPayment(
			HtmlCommandButton collectOldContractPayment) {
		this.collectOldContractPayment = collectOldContractPayment;
	}
	public HtmlCommandButton getCreateNewLeaseContract() {
		return createNewLeaseContract;
	}
	public void setCreateNewLeaseContract(HtmlCommandButton createNewLeaseContract) {
		this.createNewLeaseContract = createNewLeaseContract;
	}
	public HtmlCommandButton getReviewed() {
		return reviewed;
	}
	public void setReviewed(HtmlCommandButton reviewed) {
		this.reviewed = reviewed;
	}
	public HtmlCommandButton getSetteled() {
		return setteled;
	}
	public void setSetteled(HtmlCommandButton setteled) {
		this.setteled = setteled;
	}
	public HtmlCommandButton getRejectReview() {
		return rejectReview;
	}
	public void setRejectReview(HtmlCommandButton rejectReview) {
		this.rejectReview = rejectReview;
	}
	public String getReadOnlyStyleClass() {
		if(viewMap.containsKey(PAGE_MODE)&&
				(viewMap.get(PAGE_MODE).equals(PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT)))
				return "READONLY";
		if(viewMap.containsKey("styleClass")){
			return viewMap.get("styleClass").toString();
		}
		return readOnlyStyleClass;
	}
	public void setReadOnlyStyleClass(String readOnlyStyleClass) {
		this.readOnlyStyleClass = readOnlyStyleClass;
	}
	public Boolean getRenderAddTenantLink() {
		return renderAddTenantLink;
	}
	public void setRenderAddTenantLink(Boolean renderAddTenantLink) {
		this.renderAddTenantLink = renderAddTenantLink;
	}
	@SuppressWarnings( "unchecked" )
		public Boolean getInitiationTask() {
		if(viewMap.containsKey(PAGE_MODE)&&
				viewMap.get(PAGE_MODE).equals(PAGE_MODE_NEW))
			return false;
		else 
			return true;
	}
		@SuppressWarnings( "unchecked" )
	public void setInitiationTask(Boolean initiationTask) {
		this.initiationTask = initiationTask;
	}
		@SuppressWarnings( "unchecked" )
	public Boolean getShowHeaderControls() {
		if(viewMap.containsKey(PAGE_MODE)&&
				(viewMap.get(PAGE_MODE).equals(PAGE_MODE_REVIEW_APPROVAL)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_ACTION_PERFORMED+PAGE_MODE_REVIEW_APPROVAL)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_CONTRACT_VERIFIED+PAGE_MODE_ACTION_PERFORMED)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT)||viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_APP_FEE)||viewMap.get(PAGE_MODE).equals(PAGE_MODE_CREATE_NEW_LEASE_CONTRACT)
						||viewMap.get(PAGE_MODE).equals(PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT)))
			return true;
		else 
			return false;

	}
	public void setShowHeaderControls(Boolean showHeaderControls) {
		this.showHeaderControls = showHeaderControls;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getReadonlyHeaderControls() {
		if(viewMap.containsKey(PAGE_MODE)&&
				(viewMap.get(PAGE_MODE).equals(PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT)||
						
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_CONTRACT_VERIFIED+PAGE_MODE_ACTION_PERFORMED)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_ACTION_PERFORMED+PAGE_MODE_REVIEW_APPROVAL)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_CREATE_NEW_LEASE_CONTRACT)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_APP_FEE))||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)||
						viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT))
			return true;
		else 
			return false;

	}
	public void setReadonlyHeaderControls(Boolean readonlyHeaderControls) {
		this.readonlyHeaderControls = readonlyHeaderControls;
	}
	
	@SuppressWarnings( "unchecked" )
	public void verifyAndSend(){
		
		String methodName = "verifyAndSend";
		try
		{
			logger.logInfo(methodName+"|"+" Start...");
			requestView = (RequestView) viewMap.get(REQUEST_VIEW);
			
			ContractView contView =null;
			if(PAGE_MODE!=null && viewMap.get(PAGE_MODE).toString().equals(PAGE_MODE_VERIFY_SUGGESTED_RENT_AMOUNT))
			{
				//check if contract is already created
				if(!isNewContractAlreadyCreated(requestView)){
					contView = persistContract();
					PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
					for(PaymentScheduleView paymentScheduleView : getPaymentSchedules())
					{
						paymentScheduleView.setContractId(contView.getContractId());
						propertyServiceAgent.updatePaymentSchedule(paymentScheduleView);	
					}
				}
				saveAttachmentsComments();
				ApproveRejectTask(TaskOutcome.APPROVE);
				viewMap.put(PAGE_MODE, PAGE_MODE_CONTRACT_VERIFIED+PAGE_MODE_ACTION_PERFORMED);
				viewMap.put("styleClass", "READONLY");
				successMessages.clear();
				CommonUtil.saveSystemComments(noteOwner, MessageConstants.TransferLeaseContract.MSG_RENT_VALUE_VERIFIED, requestView.getRequestId());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_TRANSFER_CONTRACT_VERIFIED));
			}
			logger.logInfo(methodName+"|"+" Finish...");
			}
		catch(PIMSWorkListException ex)
		{
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",ex);
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.MSG_FAILURE_APPROVED));
			logger.LogException(methodName+" crashed...",e);
	
		}
	}
	private boolean isNewContractAlreadyCreated(RequestView requestVU) {
		Set<RequestFieldDetailView> requestDetails = new HashSet<RequestFieldDetailView>();
		boolean result = false;
		
		requestDetails = requestVU.getRequestFieldDetailsView();
		if(requestDetails.size() > 0 && requestVU.getContractView() != null ){
			Iterator  iter= requestDetails.iterator();
			while(iter.hasNext()){
				RequestFieldDetailView rfdv= ((RequestFieldDetailView)iter.next());
				if(rfdv.getRequestKeyView().getRequestKeyId().compareTo(25L) == 0){
					if( isNumeric(rfdv.getRequestKeyValue()) && Long.valueOf(rfdv.getRequestKeyValue()).compareTo(requestVU.getContractView().getContractId()) != 0){
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}
	@SuppressWarnings( "unchecked" )
	private boolean isAnyPendingPayments(){
		boolean success =false;
		List<PaymentScheduleView> psList= getPaymentSchedules();
		DomainDataView ddv = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_PENDING);
		for(PaymentScheduleView psv : psList){
			if(psv.getStatusId().compareTo(ddv.getDomainDataId())==0)
				return true;

		}
		return success;	
	}
	@SuppressWarnings( "unchecked" )
	public void completeOldContractPaymentTask()
	{
		String methodName="completeOldContractPaymentTask";
		try
		{
			logger.logInfo(methodName+"|"+"Start..");
			successMessages = new ArrayList<String>(0);
			successMessages.clear();
			errorMessages = new ArrayList<String>(0);
			errorMessages.clear();
			ApproveRejectTask(TaskOutcome.COLLECT);
viewMap.put("OLD_PAYMENT_COLLECTED", true);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_OLD_CONTRACT_PAYMENTS_COLLECTED));
			logger.logInfo(methodName+"|"+"Finish..");
		}
		catch(Exception ex)
		{
			logger.LogException("Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public HtmlCommandButton getCompleteOldContractPaymentTask() {
		return completeOldContractPaymentTask;
	}
	public void setCompleteOldContractPaymentTask(
			HtmlCommandButton completeOldContractPaymentTask) {
		this.completeOldContractPaymentTask = completeOldContractPaymentTask;
	}
	
	@SuppressWarnings( "unchecked" )
	private Boolean validatePaymentButton()
	{
		Boolean validate = true;
		errorMessages.clear();
		
		
		if(this.getNewStartDate()==null)
		{
		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_START_DATE));
		 validate = false;
		}
		if(this.getNewEndDate()==null)
		{
		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_END_DATE));
		 validate = false;
		}
		if(newRentAmount.getValue()==null || !(newRentAmount.getValue().toString().trim().length()>0))
		{
		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_NEW_RENT_AMOUNT));
		 validate = false;
		}
		
		
		return validate;
	}
	@SuppressWarnings( "unchecked" )
	public void btnGenPayments_Click()
    {
    	String methodName = "btnGenPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
    	try
    	{
    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    		if(validatePaymentButton())
    		{
    			 if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)  )
				    alreadyPresentPS = (ArrayList<PaymentScheduleView>)viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
				 
    			 sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,alreadyPresentPS);
				
				 String extrajavaScriptText ="var screen_width = 1024;"+
				 "var screen_height = 450;"+
				 "window.open('GeneratePayments.jsf?ownerShipTypeId="+
				 this.getContractView().getContractUnit().getUnitView().getPropertyCategoryId()+"','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=100,top=150,scrollbars=yes,status=yes');";
				
				//sessionMap.put(WebConstants.DEPOSIT_AMOUNT,cash);
				sessionMap.put(WebConstants.TOTAL_CONTRACT_VALUE,this.getNewRentAmount().getValue().toString());
				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_START_DATE,df.format(this.getNewStartDate()));
				sessionMap.put(WebConstants.GEN_PAY_CONTRACT_END_DATE,df.format(this.getNewEndDate()));
				
				openPopUp("GeneratePayments.jsf", extrajavaScriptText);
				
    		}
    		
    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
	@SuppressWarnings( "unchecked" )
	public void btnAddPayments_Click()
    {
    	String methodName = "btnAddPayments_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
    	try
    	{
    		if( newRentAmount.getValue()!=null || ( newRentAmount.getValue().toString().trim().length()>0 ) )
    		{ 
    		    if( viewMap.containsKey( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)  )
 				    alreadyPresentPS = ( ArrayList<PaymentScheduleView> )viewMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE );
    			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, alreadyPresentPS );
    			String extrajavaScriptText ="var screen_width = 1024;"+
                "var screen_height = 450;"+
                "window.open('PaymentSchedule.jsf?" +
                "totalContractValue="+newRentAmount.getValue().toString()+"&"+
                "ownerShipTypeId="   +this.getContractView().getContractUnit().getUnitView().getPropertyCategoryId()+
                "','_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');";
		        openPopUp( "PaymentSchedule.jsf", extrajavaScriptText );
    		}
    		else
    		{
    			errorMessages.clear();
    			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.TransferLeaseContract.MSG_INCORRECT_NEW_RENT_AMOUNT ) );
    		}
    		logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
    	}
    }
	@SuppressWarnings("unchecked")
	public void onSplitPayment()
	{	
		try
		{
			logger.logInfo("onSplitPayment| Start...");
			PaymentScheduleView row = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();		
			sessionMap.put("PAYMENT_SCHDULE_SPLIT",row );
			AddResource addResource = AddResourceFactory.getInstance(FacesContext.getCurrentInstance());
			addResource.addInlineScriptAtPosition(FacesContext.getCurrentInstance(), AddResource.HEADER_BEGIN, "javaScript:openSplitingPopup();");
			logger.logInfo("onSplitPayment| Finish...");
		}
		catch (Exception ex) 
		{
			logger.LogException("onSplitPayment|Exception Occured::", ex);
			sessionMap.remove( "PAYMENT_SCHDULE_SPLIT" );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
    public void btnEditPaymentSchedule_Click()
    {
    	try
    	{
	    	PaymentScheduleView psv = ( PaymentScheduleView )dataTablePaymentSchedule.getRowData();
	    	if( viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null )
	    	{
	    		sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,
	    				        viewMap.get( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE )
	    				      );
	    	}
	    	sessionMap.put( WebConstants.PAYMENT_SCHDULE_EDIT,psv );
	    	if( viewMap.get(PAGE_MODE).equals( PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT ) )
	    	{
	    		sessionMap.put("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION",true);
	    	    sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
	    	}
	    	else if(viewMap.get(PAGE_MODE).toString().compareToIgnoreCase(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)==0)
	    	{
	    	  sessionMap.put(WebConstants.SAVE_EDITED_PS_DB,"true");
	    	  sessionMap.put("STOP_AMOUNT_EDIT", true);
	    	}
	    	if( psv.getIsSystem() != null && psv.getIsSystem().longValue() == 1l )
	    	{
		    	sessionMap.put("STOP_AMOUNT_EDIT",true);
	    	}
    	    openPopUp("", "javascript:openPopupEditPaymentSchedule();");
	    }
		catch (Exception ex) 
		{
			logger.LogException("btnEditPaymentSchedule_Click|Exception Occured::", ex);
			errorMessages = new ArrayList<String>(0);
			sessionMap.remove("ALLOW_NON_SYSTEM_PAYMENT_EXEMPTION");
			sessionMap.remove( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			sessionMap.remove(WebConstants.SAVE_EDITED_PS_DB);
	    	sessionMap.remove("STOP_AMOUNT_EDIT");
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}    	
    }
	@SuppressWarnings( "unchecked" )	
	public void btnPrintPaymentSchedule_Click()
	{
		String methodName = "btnPrintPaymentSchedule_Click";
		logger.logInfo(methodName+"|"+" Start...");
		PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
		CommonUtil.printPaymentReceipt("", psv.getPaymentScheduleId().toString(), "",    getFacesContext(),MessageConstants.PaymentReceiptReportProcedureName.TRANSFER_REQUEST);
		logger.logInfo(methodName+"|"+" Finish...");
	}
	@SuppressWarnings( "unchecked" )
	private ContractView  persistContract() throws PimsBusinessException,Exception 
	{
		
		String methodName ="persistContract|";
		logger.logInfo(methodName+"Start");
		DomainDataView ddContractCategory = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.ContractCategory.CONTRACT_CATEGORY), WebConstants.ContractCategory.LEASE_CONTRACT);
		ContractView contView = new ContractView();
		requestView = (RequestView) viewMap.get(REQUEST_VIEW);
		contractView = this.getContractView();
		DomainDataView ddvRenewDraft = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS), 
	                                   WebConstants.CONTRACT_STATUS_TRANSFER_DRAFT);
		contView.setStatus(ddvRenewDraft.getDomainDataId());
		contView.setContractCategory(ddContractCategory.getDomainDataId() );
		contView.setRentAmount(Double.valueOf(newRentAmount.getValue().toString()));
		PersonView tenantView = new PersonView();
		tenantView.setPersonId(this.getNewTenantView().getPersonId());
		contView.setTenantView(tenantView);
		contView.setStartDate(this.getNewStartDate());
		contView.setContractDate(new Date());
		contView.setEndDate(this.getNewEndDate());
		contView.setCreatedBy(getLoggedInUser());
		contView.setCreatedOn(new Date());
		contView.setUpdatedBy(getLoggedInUser());
		contView.setUpdatedOn(new Date());
		contView.setRecordStatus(1L);
		contView.setIsDeleted(0L);
		contView.getRequestsView().add(requestView);
	
		PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
		contView = propertyServiceAgent.saveTransferContract(this.getContractView().getContractId(), contView);
		getContractUnitFromPage(contView);
		logger.logInfo(methodName+"Finish");
		return contView ;
	}
	public void getContractUnitFromPage(ContractView thiscontractView) throws Exception
	{
			ContractUnitView conUnitView=null; 
			if (contractView.getContractUnitView() != null && contractView.getContractUnitView().size()>0)
			{
				Iterator<ContractUnitView> it = contractView.getContractUnitView().iterator(); 
				 conUnitView = (ContractUnitView) it.next();
				conUnitView.setRentValue(Double.valueOf(this.getNewRentAmount().getValue().toString()));
				conUnitView.setUnitView(contractView.getContractUnitView().iterator().next().getUnitView());
			} 
			Set<ContractUnitView> cUSet = new HashSet<ContractUnitView>(0);
			cUSet.add(conUnitView);
			thiscontractView.setContractUnitView(cUSet);
  }
	public HtmlCommandButton getVerifySuggestion() {
		return verifySuggestion;
	}
	public void setVerifySuggestion(HtmlCommandButton verifySuggestion) {
		this.verifySuggestion = verifySuggestion;
	}
	@SuppressWarnings( "unchecked" )
	private boolean validatePayment(List<PaymentScheduleView> paymentScheduleList){
		double totalRentAmount = 0;
		boolean result = true;
		errorMessages.clear();
		for(PaymentScheduleView paymentScheduleView : paymentScheduleList)
			if(paymentScheduleView.getTypeId().longValue()==WebConstants.PAYMENT_TYPE_RENT_ID.longValue())
				totalRentAmount= totalRentAmount+paymentScheduleView.getAmount();
		
		try
		{
			
		if(newRentAmount.getValue()==null||newRentAmount.getValue().toString().trim().length()==0
				||Double.parseDouble(newRentAmount.getValue().toString())==0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_NEW_RENT_AMOUNT));
			result=false;
		}
		if(newRentAmount.getValue()!=null&&newRentAmount.getValue().toString().trim().length()>0
				&&Math.round(Double.parseDouble(newRentAmount.getValue().toString()))!=Math.round(totalRentAmount)){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_PAYMENT_RECEIVED));
			result=false;
			tabPanel.setSelectedTab("PaymentTab");
			viewMap.put(DEFAULT_TAB, "PaymentTab");
		}
		if(newRentAmount.getValue()!=null&&newRentAmount.getValue().toString().trim().length() > 0&&
				Math.round(Double.parseDouble(newRentAmount.getValue().toString()))>99999999&&
				totalRentAmount!=Double.valueOf(newRentAmount.getValue().toString())){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_NEW_RENT_AMOUNT));
				result=false;
		}
		}
		catch (NumberFormatException e) {
			result = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
	
		}
		catch (IllegalArgumentException e) {
			result = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ReceiveProperty.MSG_PROPERTY_INVALID_RENT));
	
		}
		if(newStartDate!=null && newEndDate!=null)
		{
			if(this.getEvacuationDate()!=null && newStartDate.before(this.getEvacuationDate())){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_DATE_LT_EVA_DATE));
				result=false;
				viewMap.put(DEFAULT_TAB, "contractDetailTab");
			}
			if(newStartDate.before(getContractView().getStartDate())){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_DATE_LT_START_DATE));
				result=false;
				viewMap.put(DEFAULT_TAB, "contractDetailTab");
			}
			if(newEndDate.before(newStartDate)){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_END_DATE_GT_START_DATE));
				
				result=false;
			}
			
		}
		else
		{
			if(newStartDate==null){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_START_DATE));
				result=false;
			}
			if(newEndDate==null){
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_INCORRECT_END_DATE));
				
				result=false;
			}
			
		}
		
	
		return result;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getIsDeletable() {
		if(viewMap.containsKey(PAGE_MODE)&&
				( viewMap.get(PAGE_MODE).equals(PAGE_MODE_APPROVE_OR_CHANGE_SUGGESTED_RENT)||
				  viewMap.get(PAGE_MODE).equals(PAGE_MODE_REVIEW) || 
				  viewMap.get(PAGE_MODE).equals(PAGE_MODE_REVIEW_APPROVAL) 
				  //||
				  //viewMap.get(PAGE_MODE).equals( PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT ) ||
				  //viewMap.get(PAGE_MODE).equals( PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT )
				)
			)
			return true;
		else 
			return false;
	}
	public void setIsDeletable(Boolean isDeletable) {
		this.isDeletable = isDeletable;
	}
	public Boolean getRenderPaymentButtons() {
		if(viewMap.containsKey(PAGE_MODE)&&
				viewMap.get(PAGE_MODE).equals(PAGE_MODE_COLLECT_APP_FEE))
			return false;
		return renderPaymentButtons;
	}
	public void setRenderPaymentButtons(Boolean renderPaymentButtons) {
		this.renderPaymentButtons = renderPaymentButtons;
	}
	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}
	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}
	
	@SuppressWarnings( "unchecked" )
	private void notifyRequestStatus(String notificationType)
	{
		Map<String, Object> eventAttributesValueMap = new HashMap<String, Object>(0);
		PropertyServiceAgent psAgent = new PropertyServiceAgent();
		
		ContractView contView = null;
		PersonView applicant = null;
		PersonView currentTenant = null;
		PersonView newTenant = null;
		List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();
		
		RequestView reqView = this.getRequestView();
		if(reqView == null || reqView.getRequestId() == null)
			reqView = (RequestView)viewMap.get(REQUEST_VIEW);
		
		
		try
		{
		
		if(reqView != null)
		{
			contView = this.getContractView();
			if(contView != null)
			{
				applicant = reqView.getApplicantView();
				
				if(applicant == null || applicant.getPersonId() == null)
					applicant = psAgent.getPersonInformation(new Long(hdnPersonId));
				
				currentTenant = contView.getTenantView();
				UnitView unit = contView.getContractUnit().getUnitView();
				
				newTenant = this.getNewTenantView();
				if(newTenant == null || newTenant.getPersonId() == null)
					newTenant = psAgent.getPersonInformation(new Long(hdnTDPersonId));
				
				String pattern = getDateFormat();
				DateFormat formatter = new SimpleDateFormat(pattern);
				String evacDate = formatter.format(this.getEvacuationDate());
							
				eventAttributesValueMap.put("REQUEST_NUMBER", reqView.getRequestNumber());
				eventAttributesValueMap.put("CONTRACT_NUMBER", contView.getContractNumber());
				eventAttributesValueMap.put("OLD_TENANT", currentTenant.getPersonFullName());
				eventAttributesValueMap.put("NEW_TENANT", newTenant.getPersonFullName());
				eventAttributesValueMap.put("PROPERTY_NAME", unit.getPropertyCommercialName());
				eventAttributesValueMap.put("EVACUATION_DATE", evacDate);
				eventAttributesValueMap.put("UNIT_NO", unit.getUnitNumber());
				eventAttributesValueMap.put("APPLICANT_NAME", applicant.getPersonFullName());
				
				
				if(applicant != null)
				{
					contactInfoList = getContactInfoList(applicant);
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				
				if(currentTenant != null && applicant.getPersonId().compareTo(currentTenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(currentTenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", currentTenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
				if(newTenant != null && applicant.getPersonId().compareTo(newTenant.getPersonId()) != 0)
				{
					contactInfoList = getContactInfoList(newTenant);
					eventAttributesValueMap.remove("APPLICANT_NAME");
					eventAttributesValueMap.put("APPLICANT_NAME", newTenant.getPersonFullName());
					generateNotification(notificationType,contactInfoList, eventAttributesValueMap,null);
				}
	
				
			}
		}
		}
		catch (Exception e)
		{
			logger.LogException("requestStatusNotification crashed...",e);
			
		
		}
	}
	@SuppressWarnings( "unchecked" )
	public PersonView getNewTenantViewBasicInfo() 
	{
		if(viewMap.get("newTenantViewBasicInfo")!=null)
			newTenantViewBasicInfo = (PersonView)viewMap.get("newTenantViewBasicInfo");
		return newTenantViewBasicInfo;
	}
	@SuppressWarnings( "unchecked" )
	public void setNewTenantViewBasicInfo(PersonView newTenantViewBasicInfo) {
		this.newTenantViewBasicInfo = newTenantViewBasicInfo;
		if(this.newTenantViewBasicInfo!=null)
			viewMap.put("newTenantViewBasicInfo",this.newTenantViewBasicInfo);
	}
	public Boolean getRenderViewNewTenantLink() {
		return renderViewNewTenantLink;
	}
	public void setRenderViewNewTenantLink(Boolean renderViewNewTenantLink) {
		this.renderViewNewTenantLink = renderViewNewTenantLink;
	}
	public Boolean getRenderAddNewTenantBasicInfoLink() {
		return renderAddNewTenantBasicInfoLink;
	}
	public void setRenderAddNewTenantBasicInfoLink(
			Boolean renderAddNewTenantBasicInfoLink) {
		this.renderAddNewTenantBasicInfoLink = renderAddNewTenantBasicInfoLink;
	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsPagePopUp() {
		
		if(viewMap.containsKey(SCREEN_MODE_POPUP))
		  return true;
		else
			return false;
	}
private boolean validate(ContractView contractVU)
	{
		boolean valid=true;
		if(this.getEvacuationDate()!=null)
		{
			 if(contractVU.getEndDate()!=null&&contractVU.getStartDate()!=null)
			 {
				 if(this.getEvacuationDate().after(contractVU.getEndDate()) || this.getEvacuationDate().before(contractVU.getStartDate()))
				 {
					 valid = false;
					 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.TransferLeaseContract.SETTLEMENT_DATE_INVALID));
				 }
				 //return validated;
			 }
		}
		return valid;
	}
	private void saveEvacuationDate(RequestView requestView2) 
	{
		try
		{
			if(validateFields())
			requestView=new RequestServiceAgent().persistTransferContractRequest(requestView2);
		}
		catch (Exception e) 
		{
			logger.LogException(" saveEvacuationDate()| Exception Occured::" , e);
			successMessages.clear();
			errorMessages.clear(); 
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public boolean isForSaveMethod() {
		return forSaveMethod;
	}
	public void setForSaveMethod(boolean forSaveMethod) {
		this.forSaveMethod = forSaveMethod;
	}
	public boolean isForSubmitMethod() {
		return forSubmitMethod;
	}
	public void setForSubmitMethod(boolean forSubmitMethod) {
		this.forSubmitMethod = forSubmitMethod;
	}
	public boolean isShowEvacuationClndr() 
	{
		if(viewMap.containsKey(PAGE_MODE)&&	(viewMap.get(PAGE_MODE).equals(PAGE_MODE_NEW)||viewMap.get(PAGE_MODE).equals(PAGE_MODE_REVIEW_APPROVAL)))
			isShowEvacuationClndr = true;
		else 
			isShowEvacuationClndr = false;
		return isShowEvacuationClndr;

	}
	public void setShowEvacuationClndr(boolean isShowEvacuationClndr) {
		this.isShowEvacuationClndr = isShowEvacuationClndr;
	}
public void completeRequest()
	{
		String methodName="completeRequest";
		try
		{
			logger.logInfo(methodName+"|"+"Start..");
			successMessages = new ArrayList<String>(0);
			errorMessages = new ArrayList<String>(0);
			ApproveRejectTask(TaskOutcome.COMPLETE);
			if(viewMap.containsKey(REQUEST_VIEW))
			{
				requestView = (RequestView) viewMap.get(REQUEST_VIEW);
				CommonUtil.saveSystemComments(noteOwner, MessageConstants.RequestEvents.REQUEST_COMPLETED, requestView.getRequestId());
			}
			btnComplete.setRendered(false);
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.TransferLeaseContract.MSG_TRANSFER_CONTRACT_SETTLED));
			logger.logInfo(methodName+"|"+"Finish..");
		}
		catch(Exception ex)
		{
			logger.LogException("Exception Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public String  onCalculateEvacFine()
	{
		calculateSettlement();
		return "";
	}
	private void calculateSettlement() 
	{

		ContractView contractForm = getContractView();
		DateFormat df = new SimpleDateFormat(WebConstants.DATE_FORMAT);
		
		try 
		{
		  if(this.getEvacuationDate()!=null )
		  {
			ApplicationBean appBean = (ApplicationBean) getValue("#{pages$ApplicationBean}");
			LocaleInfo locale = webContext.getAttribute(CoreConstants.CurrentLocale);
			Date startDate = df.parse( df.format(  contractForm.getStartDate()) );
			Date endDate = this.getEvacuationDate();
			Date expDate = df.parse( df.format(contractForm.getEndDate()) );
			String daysInYear = new UtilityServiceAgent().getValueFromSystemConfigration(WebConstants.SYS_CONFIG_KEY_DAYS_IN_YEAR);
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(startDate);
			calendar.setTime(endDate);
			calendar.setTime(expDate);
			int months =0;
			int days = 0;
	
			// Calculate Actual Rent Period( Months/Days) Start
			Calendar calendarCounter = GregorianCalendar.getInstance();
			calendarCounter.setTime( startDate );
			Calendar calendarSettle = GregorianCalendar.getInstance();
			calendarSettle.setTime( endDate );
			Integer[] months_days= new Integer[]{0,0};
			months_days = calculateMonthsDaysBetween( calendarCounter, calendarSettle );
			months = months_days[0];
			days = months_days[1] ;
		     // Calculate Actual Rent Period( Months/Days) end
		    contractForm.setMonths( months );
			contractForm.setDays( days );
			 
			Double totalEvacFines = 0.0D;
			Date formattedEvacDate = df.parse(df.format(endDate));
			Date formattedExpiryDate = df.parse(df.format(expDate));
		    
			// if evac date is before expiry date then calculate fine
			if( formattedEvacDate.compareTo( formattedExpiryDate )< 0 )
			totalEvacFines += calculateFineForEvacDateBeforeExpiryDate( contractForm, appBean, daysInYear );

			// if request date is not before specified months than calculate fine
			else
			totalEvacFines += calculateFineForRequestNotBeforeSpecifedMonths( contractForm, appBean, expDate, daysInYear );
			
			this.setTxttotalEvacFines( (double) ( ( (long) (totalEvacFines * 100 ) ) / 100.00 ) );
		  }	
		  else
			  this.setTxttotalEvacFines( 0.0D ); 

		} catch (Throwable t)
		{
			
			logger.LogException("Exception when calculating the Settlement Values for contractId:%s", t, contractForm.getContractId());
		}
	}
	private Integer[] calculateMonthsDaysBetween(Calendar calendarFrom,Calendar calendarTo)
	{
		int monthCounter =0;
		Integer[] months_days = new Integer[]{0,0};
		int To_day = calendarTo.get(Calendar.DAY_OF_MONTH);
		int From_day = calendarFrom.get(Calendar.DAY_OF_MONTH);
		int To_month = calendarTo.get(Calendar.MONTH);
		int From_month = calendarFrom.get(Calendar.MONTH);
		int To_year = calendarTo.get(Calendar.YEAR);
		int From_year = calendarFrom.get(Calendar.YEAR);
		if( To_day < From_day )
		{
		  To_day += 30;
		  To_month -= 1;
		}
		
		while( To_year != From_year )
		{
			To_month +=12;
			To_year -=1;
		}
		// 1 is added bcz evacuation date has to be included in the calculation as per Customer's comments
		//tenant is considered as occupying the unit till evacuation date
		months_days[1]= To_day - From_day + 1;
		//if days are greater than 29 then add 1 to To_Month and take mod with 30 to get remaining days
		if( months_days[1] > 29 )
		{
             //This formula will tell exactly how many months will be added for e.g. for 31 days 1 month, for 61 days 2 months,
			// for 91 days 3 months etc.
			To_month += (months_days[1] - ( months_days[1]%30 )) / 30;
			months_days[1] = months_days[1]%30 ;
		}
		months_days[0]=To_month - From_month;
		
		
		return months_days;
	}
	private Double calculateFineForRequestNotBeforeSpecifedMonths(ContractView contractForm, ApplicationBean appBean, Date expDate,String daysInYear) throws ParseException 
	{
		int months;
		Calendar calendarCounter;
		Integer[] months_days;
		Double totalEvacFines  = 0.0;
		Double totalFineMonths = 0.0D;
		
		RequestView requestView = viewContext.getAttribute("requestView");
		if(requestView != null && requestView.getRequestId()!= null  )
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date formattedRequestDate = df.parse( df.format( requestView.getRequestDate() ) );
			Date formattedExpiryDate  = df.parse( df.format( expDate ) );
			//if request date is less then  contract expiry date then check if this request date is before specified months
			//other wise apply fines
		     if( formattedRequestDate.compareTo( formattedExpiryDate ) < 0 )
		     {
		    	calendarCounter = GregorianCalendar.getInstance();
				calendarCounter.setTime(formattedRequestDate);
				Calendar calendarExpiry = GregorianCalendar.getInstance();
				calendarExpiry.setTime(expDate);
				months_days= new Integer[]{0,0};
				months_days = calculateMonthsDaysBetween(calendarCounter, calendarExpiry );
				months = months_days[0];
			    if(months>=2)
			    {
			    	totalEvacFines=0.0;
			    	return totalEvacFines;
			    }
		     }
			 
			 {
					DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
					DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
					if (contractForm.getContractTypeId().equals(comercialType.getDomainDataId()) )
					{
						
						PaymentConfigurationView commercial = getPaymentConfig(Constant.ANNULMENT_FINE_COMMERCIAL);
						viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
						if (commercial!=null) 
							totalFineMonths = commercial.getAmount();
					}
					else if (contractForm.getContractTypeId().equals(residentalType.getDomainDataId()) )
					{
						PaymentConfigurationView residental = getPaymentConfig(Constant.ANNULMENT_FINE_RESIDENTIAL);
						viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
						if(residental!=null)
						totalFineMonths = residental.getAmount();
					}
					totalEvacFines = Math.ceil ((30.0/new Double(daysInYear)) * totalFineMonths * contractForm.getRentAmount());
					

			    	
			}
		  }
			
		
		
		
		return totalEvacFines;
	}
	public Double getTxttotalEvacFines() {
		Double temp = (Double)viewMap.get("txttotalEvacFines");
		return temp;
	}

	public void setTxttotalEvacFines(Double txttotalEvacFines) {
		if(txttotalEvacFines!=null)
			viewMap.put("txttotalEvacFines" , txttotalEvacFines);
	}
	private Double calculateFineForEvacDateBeforeExpiryDate(ContractView contractForm, ApplicationBean appBean, String daysInYear) throws ParseException
	{
		Double fine = 0.0D;
		Double paymentConfigurationAmount= 0.0D;

		
			DomainDataView comercialType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.COMMERCIAL_LEASE_CONTRACT);
			DomainDataView residentalType = appBean.getDomainData(Constant.CONTRACT_TYPE, Constant.RESIDENTIAL_LEASE_CONTRACT);
			if (contractForm.getContractTypeId().equals(comercialType.getDomainDataId()) )
			{
				PaymentConfigurationView commercial = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_COMMERCIAL);
				if (commercial!=null )
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  commercial.getPaymentTypeId() );
					if(commercial.getIsFixed().compareTo(0L)==0) 
					  paymentConfigurationAmount = commercial.getAmount();
				    else
					  return commercial.getAmount();
				}
			}
			else if (contractForm.getContractTypeId().equals(residentalType.getDomainDataId()) )
			{
				PaymentConfigurationView residental = getPaymentConfig(Constant.FINE_FOR_EVAC_DATE_BEFORE_EXPIRY_DATE_RESIDENTIAL);
				if(residental!=null)
				{
					viewMap.put( EVAC_FINE_TYPE_FROM_PAYMENT_CONFIGURATION ,  residental.getPaymentTypeId() );
					if(residental.getIsFixed().compareTo(0L)==0) 
					   paymentConfigurationAmount = residental.getAmount();
				    else
				       return residental.getAmount();
						
				}
			}
			fine = Math.ceil ((30.0/new Double(daysInYear)) * paymentConfigurationAmount * contractForm.getRentAmount());
	    return fine;
	}
	private PaymentConfigurationView getPaymentConfig(String conditionKey){
		logger.logInfo("getPaymentConfig() started...");
		PaymentConfigurationView paymentConfigurationView = new PaymentConfigurationView();
		
		try
		{
			UtilityServiceAgent usa = new UtilityServiceAgent();
			//paymentConfigurationView.setPaymentTypeId(WebConstants.PAYMENT_TYPE_FINE_ID);
			paymentConfigurationView.setConditionKey(conditionKey);
			paymentConfigurationView.setProcedureTypeKey(Constant.PROCEDURE_TYPE_EVACUATION);
			List<PaymentConfigurationView> paymentConfigList = usa.getPrepareAuctionPaymentConfiguration(paymentConfigurationView);
			if(!paymentConfigList.isEmpty())
				paymentConfigurationView = paymentConfigList.get(0);
			else
				paymentConfigurationView = null;
				
			logger.logInfo("getPaymentConfig() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getPaymentConfig() crashed ", exception);
		}
		
		return paymentConfigurationView; 
	}
	public Long getExempted() 
	{
		Long temp = 0L;
		if( viewMap.get("isExempted")!=null )
		temp = (Long)viewMap.get("isExempted");
		
		return temp;
	}

	public void setExempted(Long isExempted) 
	{
		if(isExempted !=null)
			viewMap.put("isExempted" , isExempted);
	}
	public void onEvacFinesExempted(ActionEvent event)
	{
      this.setExempted(1L);		
	}
	public void onEvacFinesApplied(ActionEvent event)
	{
      this.setExempted(0L);		
	}
	public Long getRequestId()
	{
		if(viewMap.get("REQUEST_ID")!=null)
			return (Long) viewMap.get("REQUEST_ID");
		else
			return 0L;
	}
	
	
	public boolean isCollectingNewCntrctPmnt()
	{
		if(viewMap.get(PAGE_MODE)!=null)
		{
			if(viewMap.get(PAGE_MODE).toString().compareToIgnoreCase(PAGE_MODE_COLLECT_NEW_CONTRACT_PAYMENT)==0)
				isCollectingNewCntrctPmnt=true;
			else
				isCollectingNewCntrctPmnt=false;
		}
		else
			isCollectingNewCntrctPmnt=false;
		return isCollectingNewCntrctPmnt;
	}
	public boolean isShowSplitPaymentsIcon()
	{
		if(viewMap.get(PAGE_MODE)!=null && viewMap.get(PAGE_MODE).toString().compareToIgnoreCase(PAGE_MODE_COLLECT_OLD_CONTRACT_PAYMENT)==0)
		{
				return true;
		}
		return false;
	}
	public void setCollectingNewCntrctPmnt(boolean isCollectingNewCntrctPmnt) {
		this.isCollectingNewCntrctPmnt = isCollectingNewCntrctPmnt;
	}
	
	
    @SuppressWarnings( "unchecked" )
    public void btnViewPaymentDetails_Click()
    {
    	String methodName = "btnViewPaymentDetails_Click";
    	logger.logInfo(methodName+"|"+" Start...");
    	PaymentScheduleView psv = (PaymentScheduleView)dataTablePaymentSchedule.getRowData();
    	CommonUtil.viewPaymentDetails(psv);
 
    	logger.logInfo(methodName+"|"+" Finish...");
    }
	
    public boolean isNumeric( String input )   
    {   
       try  
       {   
          Long.parseLong( input );   
          return true;   
       }   
       catch( Exception e)   
       {   
          return false;   
       }   
    }  

	
	
}