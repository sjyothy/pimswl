package com.avanza.pims.web.backingbeans.construction.evaluation;

import java.util.Date;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.EvaluationRequestDetailsView;


public class EvaluationRequestDetailsTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(EvaluationRequestDetailsTabBacking.class);

	private EvaluationRequestDetailsView requestDetailsTabView = new EvaluationRequestDetailsView();
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private HtmlSelectOneMenu evaluationCombo = new HtmlSelectOneMenu();
	
	 @Override 
	 public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
//			populateFromView();
			if(viewMap.get("requestId")!=null)
				if(viewMap.get("evaluationPurposeId")!=null)
					evaluationCombo.setValue(viewMap.get("evaluationPurposeId").toString());
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 	}

	public Boolean getReadonlyMode(){
		Boolean evaluationRequestDetailsReadonlyMode = (Boolean)viewMap.get("evaluationRequestDetailsReadonlyMode");
		if(evaluationRequestDetailsReadonlyMode==null)
			evaluationRequestDetailsReadonlyMode = false;
		return evaluationRequestDetailsReadonlyMode;
	}

	public String getApplicationNumber() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_NUMBER);
		return temp;
	}

	public void setApplicationNumber(String applicationNumber) {
		if(StringHelper.isNotEmpty(applicationNumber))
			viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_NUMBER , applicationNumber);
	}

	public String getApplicationStatus() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_STATUS);
		return temp;
	}

	public void setApplicationStatus(String applicationStatus) {
		if(applicationStatus!=null)
			viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_STATUS , applicationStatus);
	}
	
	public String getApplicationStatusId() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_STATUS_ID);
		return temp;
	}

	public void setApplicationStatusId(String applicationStatusId) {
		if(applicationStatusId!=null)
			viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_STATUS_ID , applicationStatusId);
	}

	public String getApplicationDate() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_DATE);
		return temp;
	}

	public void setApplicationDate(String applicationDate) {
		if(applicationDate!=null)
			viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_DATE , applicationDate);
	}

	public String getEvaluationPurpose() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE);
		return temp;
	}

	public void setEvaluationPurpose(String evaluationPurpose) {
		if(evaluationPurpose!=null)
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE , evaluationPurpose);
	}
	
	public String getEvaluationPurposeId() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID);
		return temp;
	}
	public void setEvaluationPurposeId(String evaluationPurposeId) {
		if(evaluationPurposeId!=null)
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_PURPOSE_ID , evaluationPurposeId);
	}
	public String getRequestDescription() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.APPLICATION_DESCRIPTION);
		return temp;
	}
	public void setRequestDescription(String requestDescription) {
		if(requestDescription!=null)
			viewMap.put(WebConstants.PropertyEvaluation.APPLICATION_DESCRIPTION , requestDescription);
	}

	
	
	
	@SuppressWarnings("unchecked")
//	public void clearProperty(ActionEvent event){
//		logger.logInfo("clearProperty() started...");
//		try{
//			EvaluationRequestDetailsView requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
//			requestDetailsTabView.setPropertyId(0L);
//			requestDetailsTabView.setPropertyName("");
//			requestDetailsTabView.setPropertyStatus("");
//			requestDetailsTabView.setPropertyStatusId("");
//			requestDetailsTabView.setPropertyType("");
//			requestDetailsTabView.setAddress("");
//			requestDetailsTabView.setConstructionDate("");
//			requestDetailsTabView.setEmirate("");
//			requestDetailsTabView.setFloorCount("");
//			requestDetailsTabView.setLandArea("");
//			requestDetailsTabView.setLandNumber("");
//			requestDetailsTabView.setLocationArea("");
//
//			viewMap.put("clearProperty", true);
//			viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, requestDetailsTabView);
//			populateFromView();
//		}
//    	catch(Exception exception){
//    		logger.LogException("clearProperty() crashed ",exception);
//    	}
//	}
	
//	private void populateFromView(){
//		requestDetailsTabView = (EvaluationRequestDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
//		if(requestDetailsTabView!=null){
//			setApplicationNumber(requestDetailsTabView.getApplicationNumber());
//			setApplicationStatus(requestDetailsTabView.getApplicationStatus());
//			setApplicationStatusId(requestDetailsTabView.getApplicationStatusId());
//			setEvaluationPurpose(requestDetailsTabView.getEvaluationPurpose());
//			setEvaluationPurposeId(requestDetailsTabView.getEvaluationPurposeId());
//			setApplicationDate(requestDetailsTabView.getApplicationDate());
//			setRequestDescription(requestDetailsTabView.getRequestDescription());
//		}
//	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}

	public EvaluationRequestDetailsView getRequestDetailsTabView() {
		return requestDetailsTabView;
	}

	public void setRequestDetailsTabView(
			EvaluationRequestDetailsView requestDetailsTabView) {
		this.requestDetailsTabView = requestDetailsTabView;
	}

	public HtmlSelectOneMenu getEvaluationCombo() {
		return evaluationCombo;
	}

	public void setEvaluationCombo(HtmlSelectOneMenu evaluationCombo) {
		this.evaluationCombo = evaluationCombo;
	}
	public String getEvaluationOnId() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_ON_ID);
		return temp;
	}
	@SuppressWarnings("unchecked")
	public void setEvaluationOnId(String evaluationOnId) {
		if(evaluationOnId!=null)
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON_ID , evaluationOnId);
	} 
	public String getEvaluationOn() {
		String temp = (String)viewMap.get(WebConstants.PropertyEvaluation.EVALUATION_ON);
		return temp;
	}
	@SuppressWarnings("unchecked")
	public void setEvaluationOn(String evaluationOn) {
		if(evaluationOn!=null)
			viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON , evaluationOn);
	} 

	@SuppressWarnings("unchecked")
	public void evaluationOnAction(ValueChangeEvent e)
	{
		viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON_ID , e.getNewValue());
		viewMap.put(WebConstants.PropertyEvaluation.EVALUATION_ON_CHANGED, true);
	}
}

