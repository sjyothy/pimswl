package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.BidderAuctionRegView;
import com.avanza.pims.ws.vo.BidderView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class AuctionUnitRefund extends AbstractController{
	private HtmlDataTable auctionUnitTable = new HtmlDataTable();
	private List<AuctionUnitView> auctionUnitList = new ArrayList();
	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private transient Logger logger = Logger.getLogger(AuctionUnitRefund.class);

	private HtmlCommandButton btnPrintLetter = new HtmlCommandButton();
	private HtmlInputText bidderFirstNameText = new HtmlInputText();
	private HtmlInputText bidderLastNameText = new HtmlInputText();
	private HtmlInputText bidderSSNText = new HtmlInputText();
	private HtmlInputText auctionVenueText = new HtmlInputText();
	private HtmlInputText auctionNumberText = new HtmlInputText();
	private HtmlInputText auctionDateText = new HtmlInputText();
	private HtmlInputText auctionTitleText = new HtmlInputText();
	private HtmlInputText bidderPassportNumberText = new HtmlInputText();
	private HtmlSelectBooleanCheckbox payByCash = new HtmlSelectBooleanCheckbox();
	private HtmlCommandButton commandPay = new HtmlCommandButton();
	private List<String> errorMessages = new ArrayList<String>(0);
	private List<String> successMessages = new ArrayList<String>(0);

	private HtmlInputText totalBidderDeposit = new HtmlInputText();
	private HtmlInputText totalRefund = new HtmlInputText();
	BidderAuctionRegView bidderAuctionRegView = new BidderAuctionRegView();
	UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
//	ResourceBundle bundle = 
//	ResourceBundle.getBundle(getFacesContext().getApplication()
//	.getMessageBundle(), getFacesContext().getViewRoot().getLocale());
	public void init(){
		super.init();

		bidderAuctionRegView= (BidderAuctionRegView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_REG_VIEW);

		totalBidderDeposit.setValue(bidderAuctionRegView.getTotalDepositAmount().toString());
		totalRefund.setValue(String.valueOf(bidderAuctionRegView.getTotalDepositAmount()-bidderAuctionRegView.getConfiscatedAmount()));
		bidderFirstNameText.setValue(bidderAuctionRegView.getBidderView().getFirstName());
		bidderLastNameText.setValue(bidderAuctionRegView.getBidderView().getLastName());
		bidderPassportNumberText.setValue(bidderAuctionRegView.getBidderView().getPassportNumber());
		bidderSSNText.setValue(bidderAuctionRegView.getBidderView().getSocialSecNumber());
		auctionVenueText.setValue(bidderAuctionRegView.getAuctionView().getAuctionVenue().getVenueName());
		auctionDateText.setValue(bidderAuctionRegView.getAuctionView().getAuctionDate());
		auctionTitleText.setValue(bidderAuctionRegView.getAuctionView().getAuctionTitle());
		auctionNumberText.setValue(bidderAuctionRegView.getAuctionView().getAuctionNumber());


		loadAuctionUnitList();
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public void preprocess(){


		super.preprocess();
	}
	public void prerender(){
		super.prerender();
		if(bidderAuctionRegView.getHasRefunded().equals("Y")){
			commandPay.setRendered(false);
			btnPrintLetter.setRendered(true);
		}
		else{
			commandPay.setRendered(true);
			btnPrintLetter.setRendered(false);
		}
	}
	
	public String btnRePrint_Click(){
		String methodName ="btnRePrint_Click";
		logger.logInfo(methodName + "|" + "Start..");
		errorMessages=new ArrayList<String>(0);
		errorMessages.clear();
		successMessages=new ArrayList<String>(0);
		successMessages.clear();

		try
		{
			//
			successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.AuctionUnitRefund.SUCCESS_PRINT));
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured::", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	
	public List<AuctionUnitView> getAuctionUnitList() {
		if (auctionUnitList == null) {
			loadAuctionUnitList();
		}
		return auctionUnitList;
	}
	
	public Integer getRecordSize(){
		if(auctionUnitList != null){
			return auctionUnitList.size(); 
		}
		else{
			return 0;
		}
	}
	
	public String openReceivePaymentsPopUp()
	{
		String methodName="openReceivePaymentsPopUp";
		logger.logInfo(methodName+"|"+"Start..");


		openPopUp("receivePayment.jsf", "");

		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
		final String viewId = "/RefundAuctionUnit.jsp";
		logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText ="var screen_width = screen.width;"+
		"var screen_height = screen.height;"+
		"window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";

		javaScriptText=javaScriptText+extraJavaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	private void loadAuctionUnitList(){
		String  methodName = "loadAUctionUnitList";

		try{
			bidderAuctionRegView= (BidderAuctionRegView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_REG_VIEW);

			AuctionView auctionView = new AuctionView();
			auctionView.setAuctionId(bidderAuctionRegView.getAuctionId());
			BidderView bidderView = new BidderView();
			bidderView.setBidderId(bidderAuctionRegView.getBidderId());
			RequestView requestView = new RequestView();
			List<RequestView> listRequestView = new ArrayList<RequestView>();
			//requestView.setBidderView(bidderView);
			requestView.setAuctionView(auctionView);
			requestView.setAuctionId(bidderAuctionRegView.getAuctionId());
			AuctionUnitView auctionUnitView;
			listRequestView = propertyServiceAgent.getAuctionUnits(requestView);
			Iterator iteratorRequest = listRequestView.iterator();
			Iterator iteratorRequestDetail;
			RequestDetailView requestDetailView;
			SystemParameters parameters = SystemParameters.getInstance();


			java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat( parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));



			List<AuctionUnitView> tempAuctionUnitList = new ArrayList();
			while(iteratorRequest.hasNext()){

				requestView = (RequestView)iteratorRequest.next();
				bidderFirstNameText.setValue(requestView.getBidderView().getFirstName());
				bidderLastNameText.setValue(requestView.getBidderView().getLastName());
				bidderPassportNumberText.setValue(requestView.getBidderView().getPassportNumber());
				bidderSSNText.setValue(requestView.getBidderView().getSocialSecNumber());
				java.util.Date date = dateFormat.parse(requestView.getAuctionView().getAuctionDate());
				auctionDateText.setValue(date.toString());
				auctionNumberText.setValue(requestView.getAuctionView().getAuctionNumber());
				auctionTitleText.setValue(requestView.getAuctionView().getAuctionTitle());
				auctionVenueText.setValue(requestView.getAuctionView().getAuctionVenue().getVenueName());
				getFacesContext().getExternalContext().getSessionMap().get(WebConstants.TOTAL_DEPOSIT_AMOUNT);
				getFacesContext().getExternalContext().getSessionMap().get(WebConstants.REFUND_AMOUNT);
				iteratorRequestDetail= requestView.getRequestDetailView().iterator();
				while(iteratorRequestDetail.hasNext()){
					requestDetailView = (RequestDetailView)iteratorRequestDetail.next();
					auctionUnitView = new AuctionUnitView();
					tempAuctionUnitList.add(requestDetailView.getAuctionUnitView());
				}

			}

			Iterator itTempAuctionUnitList = tempAuctionUnitList.iterator();
			while(itTempAuctionUnitList.hasNext()){
				auctionUnitView = (AuctionUnitView)itTempAuctionUnitList.next();
				if(auctionUnitView.getExcluded()){
					auctionUnitView.setExcludedText(ResourceUtil.getInstance().getProperty("commons.yes"));
				}
				else{
					auctionUnitView.setExcludedText(ResourceUtil.getInstance().getProperty("commons.no"));
				}
				auctionUnitList.add(auctionUnitView);

			}

		}
		catch(Exception exception){
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();




		}

	}
	public String pay(){
//		ResourceBundle bundle = 
//		ResourceBundle.getBundle(getFacesContext().getApplication()
//		.getMessageBundle(), getFacesContext().getViewRoot().getLocale());
		Boolean isPopUpClosed = false;
		if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIPT_VIEW)!=null){
			PaymentReceiptView paymentReceiptView = (PaymentReceiptView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.PAYMENT_RECEIPT_VIEW);
			bidderAuctionRegView= (BidderAuctionRegView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_REG_VIEW);
			if(bidderAuctionRegView!=null){
				paymentReceiptView.setBidderAuctionRegView(bidderAuctionRegView);


				try{
//					if(propertyServiceAgent.persistPaymentTransaction(paymentReceiptView))
//						paymentReceiptView.getBidderAuctionRegView().setHasRefunded("Y");
					successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.AuctionUnitRefund.SUCCESS_REFUND));
				}
				catch(Exception exception){
					logger.LogException("Init() crashed ...", exception);
					exception.printStackTrace();




				}
			}
		}
		if(getFacesContext().getExternalContext().getSessionMap().get(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED)!=null)
			isPopUpClosed = (Boolean)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED);
		getFacesContext().getExternalContext().getSessionMap().put(WebConstants.POP_UP_OF_PAYMENT_RECEIPT_CLOSED,null);
		if(!isPopUpClosed){
			try{
				String strUser = getLoggedInUser();
				PaymentReceiptView paymentReceiptView = new PaymentReceiptView();
				Set<PaymentReceiptDetailView> paymentReceiptDetailViewSet = new HashSet();
				PaymentReceiptDetailView paymentReceiptDetailView =new PaymentReceiptDetailView();
				PaymentMethodView paymentMethodView =new PaymentMethodView();
				paymentReceiptDetailView.setAmount(0.00);
				paymentReceiptDetailView.setExchangeRate(1.00);
				paymentReceiptDetailView.setModeOfPaymentEn(WebConstants.PAYMENT_METHOD_CASH_DISPLAY);
				paymentMethodView.setDescription(WebConstants.PAYMENT_METHOD_CASH_DISPLAY);
				paymentReceiptDetailView.setPaymentMethod(paymentMethodView);
				paymentReceiptDetailView.setAmount(Double.valueOf(totalRefund.getValue().toString()));
				paymentReceiptDetailView.setCreatedBy(strUser);
				paymentReceiptDetailView.setCreatedOn(new Date());
				paymentReceiptDetailView.setUpdatedBy(strUser);
				paymentReceiptDetailView.setUpdatedOn(new Date());
				paymentReceiptDetailView.setIsDeleted(new Long(0));
				paymentReceiptDetailView.setRecordStatus(new Long(1));
				paymentReceiptDetailView.setIsReceived("Y");
				paymentReceiptView.setCreatedBy(strUser);
				paymentReceiptView.setCreatedOn(new Date());
				paymentReceiptView.setUpdatedBy(strUser);
				paymentReceiptView.setUpdatedOn(new Date());
				paymentReceiptView.setIsDeleted(new Long(0));
				paymentReceiptView.setRecordStatus(new Long(1));

				paymentReceiptView.setAmount(Double.valueOf(totalRefund.getValue().toString()));
				paymentReceiptView.setAmountReturn(0.00);	

//				paymentReceiptView.setModeId(modeId)

				DomainDataView domainDataView = propertyServiceAgent.getDomainDataByValue(WebConstants.PAYMENT_MODE, WebConstants.PAYMENT_MODE_OUTBOUND);
				paymentReceiptView.setModeId(domainDataView.getDomainDataId());

				paymentReceiptDetailViewSet.add(paymentReceiptDetailView);
				paymentReceiptView.setPaymentReceiptDetails(paymentReceiptDetailViewSet);
				if((Boolean)payByCash.getValue()){
					bidderAuctionRegView = new BidderAuctionRegView();
					bidderAuctionRegView= (BidderAuctionRegView)getFacesContext().getExternalContext().getSessionMap().get(WebConstants.AUCTION_REG_VIEW);
					paymentReceiptView.setBidderAuctionRegView(bidderAuctionRegView);
					paymentReceiptView.getBidderAuctionRegView().setHasRefunded("Y");
//					if(propertyServiceAgent.persistPaymentTransaction(paymentReceiptView))
//						successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.AuctionUnitRefund.SUCCESS_REFUND));
				}
				else{
					getFacesContext().getExternalContext().getSessionMap().put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
					openReceivePaymentsPopUp();
				}

				getFacesContext().getExternalContext().getSessionMap().put(WebConstants.AUCTION_REG_VIEW, bidderAuctionRegView);			

			}
			catch(Exception e){
				e.printStackTrace();
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.AuctionUnitRefund.FAILURE_REFUND));

			}
		}

		return "pay";

	}
	public String back(){
		return "back";
	}
	public String getErrorMessages() {
		String messageList;
		if ((errorMessages == null) ||
				(errorMessages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<FONT COLOR=RED><B><UL>\n";
			for(String message: errorMessages) {
				messageList = messageList + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}
	public String getSuccessMessages() {
		String messageList;
		if ((successMessages == null) ||
				(successMessages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<FONT COLOR=GREEN><B><UL>\n";
			for(String message: successMessages) {
				messageList = messageList + message + "\n";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}



	public HtmlDataTable getAuctionUnitTable() {
		return auctionUnitTable;
	}
	public void setAuctionUnitTable(HtmlDataTable auctionUnitTable) {
		this.auctionUnitTable = auctionUnitTable;
	}
	public HtmlInputText getTotalBidderDeposit() {
		return totalBidderDeposit;
	}
	public void setTotalBidderDeposit(HtmlInputText totalBidderDeposit) {
		this.totalBidderDeposit = totalBidderDeposit;
	}
	public HtmlInputText getTotalRefund() {
		return totalRefund;
	}
	public void setTotalRefund(HtmlInputText totalRefund) {
		this.totalRefund = totalRefund;
	}
	public void setAuctionUnitList(List<AuctionUnitView> auctionUnitList) {
		this.auctionUnitList = auctionUnitList;
	}
	public HtmlInputText getAuctionDateText() {
		return auctionDateText;
	}
	public void setAuctionDateText(HtmlInputText auctionDateText) {
		this.auctionDateText = auctionDateText;
	}
	public HtmlInputText getAuctionNumberText() {
		return auctionNumberText;
	}
	public void setAuctionNumberText(HtmlInputText auctionNumberText) {
		this.auctionNumberText = auctionNumberText;
	}
	public HtmlInputText getAuctionTitleText() {
		return auctionTitleText;
	}
	public void setAuctionTitleText(HtmlInputText auctionTitleText) {
		this.auctionTitleText = auctionTitleText;
	}
	public HtmlInputText getAuctionVenueText() {
		return auctionVenueText;
	}
	public void setAuctionVenueText(HtmlInputText auctionVenueText) {
		this.auctionVenueText = auctionVenueText;
	}
	public BidderAuctionRegView getBidderAuctionRegView() {
		return bidderAuctionRegView;
	}
	public void setBidderAuctionRegView(BidderAuctionRegView bidderAuctionRegView) {
		this.bidderAuctionRegView = bidderAuctionRegView;
	}
	public HtmlInputText getBidderFirstNameText() {
		return bidderFirstNameText;
	}
	public void setBidderFirstNameText(HtmlInputText bidderFirstNameText) {
		this.bidderFirstNameText = bidderFirstNameText;
	}
	public HtmlInputText getBidderLastNameText() {
		return bidderLastNameText;
	}
	public void setBidderLastNameText(HtmlInputText bidderLastNameText) {
		this.bidderLastNameText = bidderLastNameText;
	}
	public HtmlInputText getBidderPassportNumberText() {
		return bidderPassportNumberText;
	}
	public void setBidderPassportNumberText(HtmlInputText bidderPassportNumberText) {
		this.bidderPassportNumberText = bidderPassportNumberText;
	}
	public HtmlInputText getBidderSSNText() {
		return bidderSSNText;
	}
	public void setBidderSSNText(HtmlInputText bidderSSNText) {
		this.bidderSSNText = bidderSSNText;
	}
	public HtmlSelectBooleanCheckbox getPayByCash() {
		return payByCash;
	}
	public void setPayByCash(HtmlSelectBooleanCheckbox payByCash) {
		this.payByCash = payByCash;
	}
	public HtmlCommandButton getCommandPay() {
		return commandPay;
	}
	public void setCommandPay(HtmlCommandButton commandPay) {
		this.commandPay = commandPay;
	}
	public HtmlCommandButton getBtnPrintLetter() {
		return btnPrintLetter;
	}
	public void setBtnPrintLetter(HtmlCommandButton btnPrintLetter) {
		this.btnPrintLetter = btnPrintLetter;
	}




}
