package com.avanza.pims.web.backingbeans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.ws.vo.NotesVO;

public class NotesBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4085949914748648573L;
	
	private Long notesId = 0L;
	private String description = StringHelper.EMPTY;
	private String owner = StringHelper.EMPTY;
	private Long entityId = 0L;
	private Long noteTypeId = 0L;
	private boolean isDeleted = false;
	private String createdBy;
	private Date createdOn;
	private String systemNoteEn;
	private String systemNoteAr;
	private String notesDate;
	private NotesVO note;
	private String createdByFullName;
    private String createdByFullNameAr;
    private String updatedByFullName;
    private String updatedByFullNameAr;


	public NotesBean(){
	}
	public NotesBean(Long notesId, String description, String owner,
			Long entityId, Long noteTypeId, String createdBy, Date createdOn, NotesVO note,
			String systemNoteEn,String systemNoteAr,String dateFormat,String createdByFullName
			,String createdByFullNameAr,String updatedByFullName,String updatedByFullNameAr) 
	{
		super();
		this.notesId = notesId;
		this.description = description;
		this.owner = owner;
		this.entityId = entityId;
		this.noteTypeId = noteTypeId;
		this.note = note;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.systemNoteEn = systemNoteEn;
		this.systemNoteAr = systemNoteAr;
		this.createdByFullName = createdByFullName;
		this.createdByFullNameAr = createdByFullNameAr;
		this.updatedByFullName = updatedByFullName;
		this.updatedByFullNameAr = updatedByFullNameAr;
		DateFormat df = new SimpleDateFormat(dateFormat);
		this.notesDate = df.format(createdOn);
	}
	public NotesBean(Long notesId, String description, String owner,
			Long entityId, Long noteTypeId, String createdBy, Date createdOn, NotesVO note,
			String systemNoteEn,String systemNoteAr,String dateFormat) {
		super();
		this.notesId = notesId;
		this.description = description;
		this.owner = owner;
		this.entityId = entityId;
		this.noteTypeId = noteTypeId;
		this.note = note;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.systemNoteEn = systemNoteEn;
		this.systemNoteAr = systemNoteAr;
		DateFormat df = new SimpleDateFormat(dateFormat);
		this.notesDate = df.format(createdOn);
	}
	public NotesBean(Long notesId, String description, String owner,
			Long entityId, Long noteTypeId, String createdBy, Date createdOn, NotesVO note,
			String systemNoteEn,String systemNoteAr) {
		super();
		this.notesId = notesId;
		this.description = description;
		this.owner = owner;
		this.entityId = entityId;
		this.noteTypeId = noteTypeId;
		this.note = note;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.systemNoteEn = systemNoteEn;
		this.systemNoteAr = systemNoteAr;
		
	}
	public Long getNotesId() {
		return notesId;
	}

	public void setNotesId(Long notesId) {
		this.notesId = notesId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public NotesVO getNote() {
		return note;
	}

	public void setNote(NotesVO note) {
		this.note = note;
	}

	public Long getNoteTypeId() {
		return noteTypeId;
	}

	public void setNoteTypeId(Long noteTypeId) {
		this.noteTypeId = noteTypeId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getSystemNoteEn() {
		return systemNoteEn;
	}

	public void setSystemNoteEn(String systemNoteEn) {
		this.systemNoteEn = systemNoteEn;
	}

	public String getSystemNoteAr() {
		return systemNoteAr;
	}

	public void setSystemNoteAr(String systemNoteAr) {
		this.systemNoteAr = systemNoteAr;
	}

	public String getNotesDate() {
		return notesDate;
	}

	public void setNotesDate(String notesDate) {
		this.notesDate = notesDate;
	}
	
	public String getCreatedByFullName() {
		return createdByFullName;
	}

	public void setCreatedByFullName(String createdByFullName) {
		this.createdByFullName = createdByFullName;
	}

	public String getCreatedByFullNameAr() {
		return createdByFullNameAr;
	}

	public void setCreatedByFullNameAr(String createdByFullNameAr) {
		this.createdByFullNameAr = createdByFullNameAr;
	}

	public String getUpdatedByFullName() {
		return updatedByFullName;
	}

	public void setUpdatedByFullName(String updatedByFullName) {
		this.updatedByFullName = updatedByFullName;
	}

	public String getUpdatedByFullNameAr() {
		return updatedByFullNameAr;
	}

	public void setUpdatedByFullNameAr(String updatedByFullNameAr) {
		this.updatedByFullNameAr = updatedByFullNameAr;
	}
	
}
