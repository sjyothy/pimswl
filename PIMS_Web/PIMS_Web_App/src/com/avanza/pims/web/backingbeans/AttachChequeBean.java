package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.Logger;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;


public class AttachChequeBean extends AbstractController {

	private static Logger logger = Logger.getLogger(AttachChequeBean.class);
	
	private  Map viewMap;
	private  Map sessionMap;
	private HttpServletRequest request; 
	
	private List<String>  errorMessage;
	public AttachChequeBean (){
		
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
    {   	
		super.init();
		
   	 	if(!isPostBack() )
   	 	{
   	 		viewMap  = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
   	 		sessionMap = getFacesContext().getExternalContext().getSessionMap();
   	 	    request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
   	 	    if(request.getParameter(WebConstants.PAYMENT_RECEIPT_DETAIL_ID) != null)
   	 	    {
   	 	    	viewMap.put(WebConstants.PAYMENT_RECEIPT_DETAIL_ID, request.getParameter(WebConstants.PAYMENT_RECEIPT_DETAIL_ID));
   	 	    }
   	 		loadAttachments();
   	 		viewMap.put("canAddAttachment", true);
   	 	}  	 	
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
	}
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessage);
	}
	@SuppressWarnings("unchecked")
	public void loadAttachments()
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_SCANNED_BOUNCE_CHEQUE);
		viewMap.put("noteowner", WebConstants.NOTES_OWNER_B_CHEQUE);
		
		if(viewMap.get(WebConstants.PAYMENT_RECEIPT_DETAIL_ID)!= null){
	    	String entityId = viewMap.get(WebConstants.PAYMENT_RECEIPT_DETAIL_ID).toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
}
