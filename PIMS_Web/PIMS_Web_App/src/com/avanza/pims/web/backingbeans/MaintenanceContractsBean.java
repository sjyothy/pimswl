package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.LeaseContractsSummaryReportCriteria;
import com.avanza.pims.report.criteria.MaintenanceContractsReportCriteria;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.WorkTypeView;

public class MaintenanceContractsBean extends AbstractController{

	private String dateFormat;
	private MaintenanceContractsReportCriteria reportCriteria; 
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private FacesContext context = FacesContext.getCurrentInstance();
	private Map viewMap=context.getViewRoot().getAttributes();
	private List<SelectItem> workTypeItems = new ArrayList<SelectItem>();
	private List<WorkTypeView> workTypeViewList = new ArrayList<WorkTypeView>();
	private ConstructionServiceAgent constructionServiceAgent = new ConstructionServiceAgent();
	private ApplicationBean applicationBean = new ApplicationBean();

	public MaintenanceContractsBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new MaintenanceContractsReportCriteria(ReportConstant.Report.MAINTENANCE_CONTRACTS_EN, ReportConstant.Processor.MAINTENANCE_CONTRACTS, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new MaintenanceContractsReportCriteria(ReportConstant.Report.MAINTENANCE_CONTRACTS_AR, ReportConstant.Processor.MAINTENANCE_CONTRACTS, CommonUtil.getLoggedInUser());
	}

	public MaintenanceContractsReportCriteria getReportCriteria() {
		return reportCriteria;
	}

	public void setReportCriteria(MaintenanceContractsReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}
	
	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}
	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	
	 private static Logger logger = Logger.getLogger("MaintenanceContractsBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();


	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	
    	dateFormat= localeInfo.getDateFormat();
    	
		return dateFormat;
	}


	public String btnPrint_Click() {	
		
		    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
				return "";
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public void init()
	{
		super.init();
		try 
		{
			if( viewMap.get(WebConstants.Tender.WORK_TYPE_VIEW_LIST) != null )
			workTypeViewList = (List<WorkTypeView>) viewMap.get(WebConstants.Tender.WORK_TYPE_VIEW_LIST);
		else
		{
			
				workTypeViewList = constructionServiceAgent.getAllWorkTypes();
			   viewMap.put(WebConstants.Tender.WORK_TYPE_VIEW_LIST,workTypeViewList);
		}
		
		if( viewMap.get(WebConstants.Tender.WORK_TYPE_ITEMS) != null )
			workTypeItems = (List<SelectItem>) viewMap.get(WebConstants.Tender.WORK_TYPE_ITEMS);
		else		
		{
			workTypeItems = applicationBean.getWorkTypeItems(workTypeViewList);
			viewMap.put(WebConstants.Tender.WORK_TYPE_ITEMS,workTypeItems);
		}
	} 
	
		catch(PimsBusinessException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	public List<SelectItem> getWorkTypeItems() {
		return workTypeItems;
	}

	public void setWorkTypeItems(List<SelectItem> workTypeItems) {
		this.workTypeItems = workTypeItems;
	}
}