package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.ws.vo.FeeConfigurationDetailView;
import com.avanza.pims.ws.vo.FeeConfigurationView;
import com.avanza.pims.ws.vo.UnitView;


import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.TenantView;
import com.avanza.pims.ws.vo.FacilityView;
import com.avanza.pims.entity.Property;
import com.avanza.ui.util.ResourceUtil;
import com.evermind.server.ejb.ExtendedContainerManagedObject.Flag;

public class AddFeeConfiguration extends AbstractController {
	
	
	
	
	private String pageMode;
	private String PAGE_MODE_ADD="ADD";
	private String PAGE_MODE_EDIT="EDIT";
	private HtmlDataTable dataTable;
	private FeeConfigurationDetailView dataItem = new FeeConfigurationDetailView();
	private ArrayList<FeeConfigurationDetailView> dataList ;
	private String feeConfigurationId;
	private String selectOneProcedureType;
	private String selectOneFeeType;
	private String accountNo;
	private String percentage;
	private String selectOnebasedOn;
	private String minAmount;
	private String maxAmount;
	

	
	
	
	
	private String selectOneSubFee;
	private String subFeeAmount;
	private String subFeeDescriptionEn;
	private String subFeeDescriptionAr;
	private String subFeeMinValue;
	private String subFeeMaxValue;
	private String pageModeSubType;
	private String PAGE_MODE_ADD_Sub_Type="ADD";
	private String PAGE_MODE_EDIT_Sub_Type="EDIT";
	private String feeSubTypeDetailId; 
	private Boolean subTypeRander;
	private Boolean comboDisable;
	boolean isArabicLocale;
    boolean isEnglishLocale;
    
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	
	
	private HtmlSelectOneMenu feeSubTypeSelectMenu= new HtmlSelectOneMenu();
	List<SelectItem> feeSubType= new ArrayList();
	
	private HtmlInputText ctlRentValue=new HtmlInputText();
	
	private boolean isPageModeAdd;
	
	String selectOneTitle;
	 
	private FacilityView facilityView = new FacilityView();
	private FacilityView facilityViewEdit = new FacilityView();
	
	private FeeConfigurationView feeConfig = new FeeConfigurationView();
	private FeeConfigurationDetailView feeConfigDetailView = new FeeConfigurationDetailView();
	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages = new ArrayList<String>();
///	private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private static Logger logger = Logger.getLogger( AddFeeConfiguration.class );
	FacesContext context=getFacesContext();
	Map  requestForID=context.getExternalContext().getRequestMap();
	Map  request=FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    Map sessionMap;
    
    Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	public AddFeeConfiguration (){
		
	}
	
	public void init() 
    {
   	
   	 super.init();
   	 try
   	 {
   		 System.out.println("Testing");
    sessionMap=context.getExternalContext().getSessionMap();
   
    if(!isPostBack()){
    	
                	
    	
            
    		PropertyServiceAgent psa = new PropertyServiceAgent();
    	   
           if(requestForID.get("feeConfigurationId")!=null && !requestForID.get("feeConfigurationId").toString().trim().equals("")){
        	  feeConfigurationId=requestForID.get("feeConfigurationId").toString().trim();
        	  feeConfig.setFeeConfigurationId(new Long(feeConfigurationId));
        	  
        	  feeConfig =  psa.getFeeConfigById(new Long(feeConfigurationId));
        	  
        	  if(feeConfig.getCreatedBy()!=null){
        	  viewMap.put("CreatedBy", feeConfig.getCreatedBy());}
        	  if(feeConfig.getCreatedOn()!=null){
        	  viewMap.put("CreatedOn", feeConfig.getCreatedOn());}
        	  
        	  
        	  if(feeConfig.getProcedureTypeId()!=null && !feeConfig.getProcedureTypeId().equals("")){
        	  request.put("ProcedureType", feeConfig.getProcedureTypeId());}
        	  
        	  if(feeConfig.getFeeType()!=null && !feeConfig.getFeeType().equals("")){
        	  request.put("FeeType", feeConfig.getFeeType());}
        	  
        	  if(feeConfig.getBasedOnTypeId()!=null && !feeConfig.getBasedOnTypeId().equals("")){
        	  request.put("BasedOnType", feeConfig.getBasedOnTypeId());}
        	  
        	  if (dataList == null || dataList.size()==0)
      		{
        	  loadDataList();
      		} 
    	    pageMode=PAGE_MODE_EDIT;	
    	    subTypeRander = true;
    	    comboDisable = true;
    	    putFacilityViewValueInControl();
    	}
    	else
    	{
    		pageMode=PAGE_MODE_ADD;
    		subTypeRander = false;
    		comboDisable = false;
    	}
    		
     }
   		 
   	 }
   	 catch(Exception es)
   	 {
   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
   		 System.out.println(es);
   		 
   	 }
	        
	 }
	private void loadDataList()
    {
    	
		String sMethod="loadDataList";
		logger.logInfo(sMethod+" Starts:::::::::::::::::::::");
		dataList = new ArrayList<FeeConfigurationDetailView>();
    	List<FeeConfigurationDetailView> list =new ArrayList<FeeConfigurationDetailView>();
    	boolean isSucceed=false;
    	try
    	{
    		PropertyServiceAgent myPort=new PropertyServiceAgent();
	    	list = myPort.getAllFeeSubType(new Long(feeConfigurationId));

	        
	        Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("SESSION_FEE_CONFIG_FEE_SUB_TYPE", list);
			
			 
			
			recordSize = list.size();
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
           
           
	        dataList=(ArrayList<FeeConfigurationDetailView>) list;
	        
	        logger.logInfo("Record Found In Load Data List : %s",dataList.size());
	        
/*	        if (dataList.size()==0){
	        	errorMessages = new ArrayList<String>();
				errorMessages.add(getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
	        }*/
	        
    		
	        isSucceed=true;
	        logger.logInfo(sMethod+" Ends:::::::::::::::::::::");
    	}
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.logInfo(sMethod+" Crashed:::::::::::::::::::::");
    		ex.printStackTrace();
    	}
    	//return isSucceed;
    	
    }
	
	public List<FeeConfigurationDetailView> getGridDataList()
	{

		
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		if (dataList == null || dataList.size()==0)
		{
			if(viewMap.containsKey("SESSION_FEE_CONFIG_FEE_SUB_TYPE"))	
				  dataList= (ArrayList)viewMap.get("SESSION_FEE_CONFIG_FEE_SUB_TYPE");
			else
				loadDataList();
		}
		return dataList;
	}

	public void preprocess(){
	super.preprocess();
	
	
}
	

	
	public DomainDataView  getIdFromType(List<DomainDataView> ddv ,String type)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+type);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		DomainDataView returnDomainDataView =new DomainDataView();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDataValue().equals(type))
			{
				returnDomainDataView=domainDataView;
				return returnDomainDataView;
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return returnDomainDataView;
		
	}
	public DomainDataView  getTypeFromId(List<DomainDataView> ddv ,String id)
	{
		String methodName="getIdFromType";
		logger.logInfo(methodName+"|"+"Start...");
		logger.logInfo(methodName+"|"+"DomainDataType To search..."+id);
		Long returnId =new Long(0);
		HashMap hMap=new HashMap();
		DomainDataView returnDomainDataView =new DomainDataView();
		for(int i=0;i<ddv.size();i++)
		{
			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
			if(domainDataView.getDomainDataId().toString().equals(id))
			{
				returnDomainDataView=domainDataView;
				return returnDomainDataView;
			}
			
		}
		logger.logInfo(methodName+"|"+"Finish...");
		return returnDomainDataView;
		
	}
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
    {
    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
				  ddvList.add(ddv);	
				}
				break;
			}
		}
    	
    	return ddvList;
    }
public void prerender(){
	super.prerender();
	
	
	
	
}

private boolean  putFacilityViewValueInControl() throws ParseException{
		
		String methodName = "putTenantViewValueInControl";
		DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
		
	       boolean isSucceed=true;
	      
	    if(feeConfig.getProcedureTypeId()!=null ){
	       	
	    	selectOneProcedureType=feeConfig.getProcedureTypeId().toString();
	       }

	    if (feeConfig.getFeeType() != null)
	       {
	    	selectOneFeeType = feeConfig.getFeeType().toString();
		   }
	     
	    if (	feeConfig.getGrpAccountNo() != null)
	       {
	    	accountNo = feeConfig.getGrpAccountNo();
		   }

	    if (feeConfig.getPercent() != null)
	       {
	    	percentage = feeConfig.getPercent().toString();
	       	
	 	   }
	    
	    if (feeConfig.getBasedOnTypeId() != null)
	       {
	    	selectOnebasedOn = feeConfig.getBasedOnTypeId().toString();
	       	
	 	   }
	    if (feeConfig.getMinAmount() != null)
	       {
	    	minAmount = feeConfig.getMinAmount().toString();
	       	
	 	   }
	    if (feeConfig.getMaxAmount() != null)
	       {
	    	maxAmount = feeConfig.getMaxAmount().toString();
	       	
	 	   }
	    
	    
	    
	    
	      return isSucceed;
	}
		
	
public String getErrorMessages() {
	StringBuilder sb = new StringBuilder();
	for (String msg: this.errorMessages)
		sb.append(msg).append("\n");
	
	return sb.toString();
}

public String getSuccessMessages() {
	
	StringBuilder sb = new StringBuilder();
	for (String msg: this.successMessages)
		sb.append(msg).append("\n");
	
	return sb.toString();
}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


public String btnAdd_Click() throws Exception {

	String methodName = "btnAdd_Click";
	logger.logInfo(methodName+" Starts::::::::::::::::::::");
	String eventOutCome = "failure";
	String s1;
	String message;
	Long id;
	
	errorMessages = new ArrayList<String>();
	successMessages = new ArrayList<String>();
	try {
 
		if(putControlValuesInView())
		{
		PropertyServiceAgent psa  =new PropertyServiceAgent();
		logger.logInfo("addFeeConfiguration Starts::::::::::::::::");
		   id= psa.addFeeConfiguration(feeConfig);
		logger.logInfo("addFeeConfiguration Ends::::::::::::::::");
			message = ResourceUtil.getInstance().getProperty("common.messages.Save");
	    	successMessages.add(message);
			eventOutCome = "success";
			subTypeRander = true;
			comboDisable  = true;
			if (pageMode.equals("EDIT")){
			if(request.containsKey("ProcedureType")){   
			selectOneProcedureType  = request.get("ProcedureType").toString();}
			if(request.containsKey("FeeType")){   	
			selectOneFeeType = request.get("FeeType").toString();}
			if(request.containsKey("BasedOnType")){   
			selectOnebasedOn= request.get("BasedOnType").toString();}
			                            }
			   if (!pageMode.equals("EDIT")){
				   
				   if(selectOneProcedureType !=null && !selectOneProcedureType.equals("")
					    	 && !selectOneProcedureType.equals("-1")){
						       	
					   request.put("ProcedureType",new Long(selectOneProcedureType)) ;
						       }
				   if (selectOneFeeType != null && ! selectOneFeeType.equals("")
					    	&& ! selectOneFeeType.equals("-1") )
					       {
					   request.put("FeeType",new Long(selectOneFeeType))  ;
						   }
				   if (selectOnebasedOn != null && !selectOnebasedOn.equals("") 
					    	&& !selectOnebasedOn.equals("-1") )
					       {
					   request.put("BasedOnType",new Long(selectOnebasedOn));
					       	
					 	   }
				   feeConfigurationId= id.toString();
			   }
			   
		}
		
		logger.logInfo(methodName+" Ends::::::::::::::::::::");

	} catch (Exception ex) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.logInfo(methodName+" Crashed::::::::::::::::::::");
		ex.printStackTrace();
	}

	return eventOutCome;

}


private boolean putControlValuesInView() throws ParseException {
	
	String methodName = "putControlValuesInView";
	PropertyServiceAgent psa = new PropertyServiceAgent();
	List<FeeConfigurationView> feeConfigViewList = new ArrayList<FeeConfigurationView>();
	Boolean feeTypePresent = true;
	Long proTypeId;
	Long feeTypeId;
	Long BasedOnId;
	Double minAmt =0d;
	Double maxAmt= 0d;
	Double per=0d;
    boolean isSucceed=true;
    String message;
       
    errorMessages.clear();
       
    try { 
    if (pageMode.equals("EDIT")){
    	  feeConfig.setFeeConfigurationId(new Long(feeConfigurationId));
    	  
    	  if (request.containsKey("ProcedureType")){
    	  proTypeId= (Long)request.get("ProcedureType");
    	  feeConfig.setProcedureTypeId(proTypeId);
    	  selectOneProcedureType = request.get("ProcedureType").toString();}

    	  if (request.containsKey("FeeType")){
    	  feeTypeId = (Long)request.get("FeeType");
		  feeConfig.setFeeType(feeTypeId);
		  selectOneFeeType =request.get("FeeType").toString(); }

    	  if (request.containsKey("BasedOnType")){
		  BasedOnId= (Long)request.get("BasedOnType");
    	  feeConfig.setBasedOnTypeId(BasedOnId);
    	  selectOnebasedOn = request.get("BasedOnType").toString();}
       }
      
      if(selectOneProcedureType !=null && !selectOneProcedureType.equals("")
    	 && !selectOneProcedureType.equals("-1") && !pageMode.equals("EDIT")){
	       	
    	  feeConfig.setProcedureTypeId(new Long(selectOneProcedureType)) ;
	       }
      else if (!pageMode.equals("EDIT"))
     	{
    	  message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.procedureType");
    	  errorMessages.add(message);
     		isSucceed=false;
     		
     	}

	    if (selectOneFeeType != null && ! selectOneFeeType.equals("")
	    	&& ! selectOneFeeType.equals("-1")&&!pageMode.equals("EDIT") )
	       {
	    	feeConfig.setFeeType(new Long(selectOneFeeType))  ;
		   }
	    else if (!pageMode.equals("EDIT"))
     	{
     		
     	  message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.messagefeeType");
      	  errorMessages.add(message);
     		isSucceed=false;
     		
     	}
	     
	    if (	accountNo != null && !accountNo.equalsIgnoreCase(""))
	       {
	    	feeConfig.setGrpAccountNo(accountNo);
		   }

	    else
	   	{
	   		
	   		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.accountNo");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	   		
	   	}
	    if (percentage != null && !percentage.equals(""))
	       {
	    	feeConfig.setPercent(new Double(percentage));
	    	per = new Double(percentage);
	 	   }
	    else
	   	{
	   		
	   		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.percentage");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	   		
	   	}
	    if (percentage!=null && per>100d){
	    	
	    	message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.percentageGreater");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	    	
	    }
	    
	    if (selectOnebasedOn != null && !selectOnebasedOn.equals("") 
	    	&& !selectOnebasedOn.equals("-1")&&!pageMode.equals("EDIT") )
	       {
	    	 feeConfig.setBasedOnTypeId(new Long(selectOnebasedOn));
	       	
	 	   }
	    else if (!pageMode.equals("EDIT"))
	   	{
	   		
	   		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.basedOn");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	   		
	   	}
	    if (minAmount != null && !minAmount.equalsIgnoreCase(""))
	       {
	    	feeConfig.setMinAmount(new Double(minAmount));
	    	minAmt = new Double(minAmount);
	       	
	 	   }
	    else
	   	{
	   		
	   		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.minAmount");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	   		
	   	}
	    if (maxAmount != null && !maxAmount.equals(""))
	       {
	    	feeConfig.setMaxAmount(new Double(maxAmount));
	    	maxAmt = new Double(maxAmount);
	       	
	 	   }
	    else
	   	{
	   		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.maxAmunt");
	    	  errorMessages.add(message);
	   		isSucceed=false;
	   		
	   	}
	    if (minAmount!=null && maxAmount!=null && !(minAmt<maxAmt)){
	    	message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.minMaxValue");
	    	  errorMessages.add(message);
	   		isSucceed=false;	
	    }
     
	    if(!(pageMode.equals("EDIT")) && !(per>100d)
	    	&& selectOneFeeType != null && ! selectOneFeeType.equals("")
	    	&& ! selectOneFeeType.equals("-1")&& selectOneProcedureType !=null 
	    	&& !selectOneProcedureType.equals("")&& !selectOneProcedureType.equals("-1")
	    	&& minAmount!=null && maxAmount!=null && (minAmt<maxAmt))
	    {
	    	
	    	try {
				feeConfigViewList  = psa.getFeeConfigByProcedureType(new Long(selectOneProcedureType));
				feeTypePresent= true;
				for (FeeConfigurationView fCV:feeConfigViewList){
				
					if (selectOneFeeType.equals(fCV.getFeeType().toString())){
						feeTypePresent = false;
						break;
					}
				}
				
			} catch (NumberFormatException e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				e.printStackTrace();
			} catch (PimsBusinessException e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				e.printStackTrace();
			}
	    }
      
	    if (!feeTypePresent){
	    	message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.FeePersent");
	    	  errorMessages.add(message);
	   		isSucceed=false;	
	    }
       
      
	    feeConfig.setCreatedBy(getCreatedByUser(getLoggedInUser()));
	    feeConfig.setCreatedOn(getCreatedOnDate());
	    feeConfig.setUpdatedBy(getLoggedInUser());
	    feeConfig.setUpdatedOn(new Date());
	    feeConfig.setIsDeleted(new Long(0));
	    feeConfig.setRecordStatus(new Long(1));
      
      
	    } catch (Exception e) {
			// TODO Auto-generated catch block
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			e.printStackTrace();
		}
	 return isSucceed; 
}

public String btnAddSubType_Click() throws Exception {

	String methodName = "btnAddSubType_Click";
	logger.logInfo(methodName+" Starts:::::::::::::::::::::");
	
	String eventOutCome = "failure";
	String s1;
	String message;
	errorMessages = new ArrayList<String>();
	try {
 
		if(putSubTypeControlValuesInView())
		{
		PropertyServiceAgent psa  =new PropertyServiceAgent();
		if (feeConfigurationId !=null && !feeConfigurationId.equals("")){
		feeConfigDetailView.setFeeConfigurationId(new Long(feeConfigurationId));}
			psa.addFeeConfigurationDetails(feeConfigDetailView);
			message = ResourceUtil.getInstance().getProperty("common.messages.Save");
	    	  errorMessages.add(message);
			eventOutCome = "success";
			feeSubTypeDetailId = null;
			
				 
			if(request.containsKey("ProcedureType")){	 
			selectOneProcedureType  = request.get("ProcedureType").toString();}
			
			if(request.containsKey("FeeType")){	 
			selectOneFeeType = request.get("FeeType").toString();}
			
			if(request.containsKey("BasedOnType")){	 
			selectOnebasedOn= request.get("BasedOnType").toString();}
			
			logger.logInfo(methodName+" Ends:::::::::::::::::::::");
			
			loadDataList();
		}
		else {
			loadDataList();
		}

	} catch (Exception ex) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.logInfo(methodName+" Crashed:::::::::::::::::::::");
		ex.printStackTrace();
	}

	return eventOutCome;

}

private boolean putSubTypeControlValuesInView() throws ParseException {
	
	String methodName = "putSubTypeControlValuesInView";
	PropertyServiceAgent psa = new PropertyServiceAgent();
	List<FeeConfigurationDetailView> feeConfigDetailList = new ArrayList<FeeConfigurationDetailView>();
	Boolean flage = true;
	String message;
	Double minAmt= 0d;
	Double maxAmt= 0d;
	Long feeConfigId = 0L;
	boolean isSucceed=true;
 try {
	if (feeConfigurationId!=null && !feeConfigurationId.equalsIgnoreCase(""))
		feeConfigId = new Long(feeConfigurationId);
	
       
       errorMessages.clear();
       
        
       if (feeSubTypeDetailId !=null && !feeSubTypeDetailId.equals("")){
    	   
    	   feeConfigDetailView.setFeeConfigurationDetailId(new Long(feeSubTypeDetailId));
    	   
       }
   	   
   	
   	
   	
      if(selectOneSubFee !=null && !selectOneSubFee.equals("") && !selectOneSubFee.equals("-1") ){
	       	
    	  feeConfigDetailView.setFeeSubTypeId(new Long(selectOneSubFee)) ;
	       }
      else
     	{
     		message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.messagefeeType");
      	    errorMessages.add(message);
     		isSucceed=false;
     		
     	}

	    if (subFeeAmount != null && ! subFeeAmount.equals(""))
	       {
	    	feeConfigDetailView.setAmount(new Double(subFeeAmount))  ;
		   }
	     
	
	    if (subFeeDescriptionEn != null && !subFeeDescriptionEn.equals(""))
	       {
	    	feeConfigDetailView.setDescriptionEn(subFeeDescriptionEn);
	       	
	 	   }
	    
	    
	 
	    if (subFeeDescriptionAr != null && !subFeeDescriptionAr.equals(""))
	       {
	    	feeConfigDetailView.setDescriptionAr(subFeeDescriptionAr);
	       	
	 	   }
	    if (subFeeMinValue != null && !subFeeMinValue.equals(""))
	       {
	    	feeConfigDetailView.setMinValue(new Double(subFeeMinValue));
	    	minAmt = new Double(subFeeMinValue);
	       	
	 	   }
	  else {
	    	message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.minValue");
	    	  errorMessages.add(message);
     		isSucceed=false;
		   }	
	    if (subFeeMaxValue != null && !subFeeMaxValue.equals(""))
	       {
	    	feeConfigDetailView.setMaxValue(new Double(subFeeMaxValue));
	    	maxAmt = new Double(subFeeMaxValue);
	 	   }
	 else {
	    	message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.maxValue");
	    	  errorMessages.add(message);
     		isSucceed=false;
		  }	
	  
	  if (!(minAmt<maxAmt) && subFeeMinValue != null && subFeeMaxValue != null){
		  message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.minMaxValue");
    	  errorMessages.add(message);
   		  isSucceed=false;
	    }
	    
	    
	    if(selectOneSubFee !=null && !selectOneSubFee.equals("") && !selectOneSubFee.equals("-1")&& subFeeMinValue != null && subFeeMaxValue != null && minAmt<maxAmt ){
	    	
	    	try {
				feeConfigDetailList=psa.getByFeeSubType(new Long(selectOneSubFee));
				
					    if (!(feeConfigDetailList.size()>0)){
					    	flage = true;
					    }
				
				int count=0;
				
				for (FeeConfigurationDetailView fCDV:feeConfigDetailList){
					Long longfeeConfigId= fCDV.getFeeConfigurationId();
					
			  if (longfeeConfigId.equals(feeConfigId)){		
					flage = false;
				if (minAmt<	fCDV.getMinValue()){
								
					            if (maxAmt < fCDV.getMinValue()){
					            	flage=true;
							                                 	}
				                               }
				if (minAmt>	fCDV.getMinValue()){
					
		                        if (minAmt > fCDV.getMaxValue()){
		                        	flage=true;
				                                            	}
	                                          }
				if (!flage){
					 break;
				           }
			     }
				}
				
				
			} catch (NumberFormatException e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				e.printStackTrace();
			} catch (PimsBusinessException e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				e.printStackTrace();
			}
      
		if (!flage && subFeeMinValue!=null && subFeeMaxValue !=null && minAmt<maxAmt  ){
			
			message = ResourceUtil.getInstance().getProperty("feeConfigurationDetail.message.overLappingValue");
	    	  errorMessages.add(message);
     		isSucceed=false;
		  }
			 
	    	
	    }
	    
     
       
      
       
      
	    feeConfigDetailView.setCreatedBy(getCreatedByUserSubType(getLoggedInUser()));
		feeConfigDetailView.setCreatedOn(getCreatedOnDateSubType());
	    feeConfigDetailView.setUpdatedBy(getLoggedInUser());
	    feeConfigDetailView.setUpdatedOn(new Date());
	    feeConfigDetailView.setIsDeleted(new Long(0));
	    feeConfigDetailView.setRecordStatus(new Long(1));
	    
	    } catch (Exception e) {
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
      return isSucceed;
}

public String  cmdEdit_Click()
{
   
    FeeConfigurationDetailView feeConfigDetailView=(FeeConfigurationDetailView)dataTable.getRowData();
    
    if(feeConfigDetailView.getCreatedBy()!=null){
    	viewMap.put("SubTypeCreatedBy", feeConfigDetailView.getCreatedBy());
    }
	if(feeConfigDetailView.getCreatedOn()!=null){
		viewMap.put("SubTypeCreatedOn", feeConfigDetailView.getCreatedOn());	
	    }
    try{
    	if(request.containsKey("ProcedureType")){
    	selectOneProcedureType  = request.get("ProcedureType").toString();}
    	
    	if(request.containsKey("FeeType")){
		selectOneFeeType = request.get("FeeType").toString();}
		
		if(request.containsKey("BasedOnType")){
		selectOnebasedOn= request.get("BasedOnType").toString();} 
    	 
     feeSubTypeDetailId=feeConfigDetailView.getFeeConfigurationDetailId().toString();
    	 
	     if(feeConfigDetailView.getFeeSubTypeId()!=null ){
    	   selectOneSubFee=feeConfigDetailView.getFeeSubTypeId().toString();
	       }
	    if (feeConfigDetailView.getAmount() != null)
	       {
	    	subFeeAmount = feeConfigDetailView.getAmount().toString();
		   }
	     
	    if (feeConfigDetailView.getDescriptionEn() != null)
	       {
	    	subFeeDescriptionEn = feeConfigDetailView.getDescriptionEn();
		   }

	    if (feeConfigDetailView.getDescriptionAr() != null)
	       {
	    	subFeeDescriptionAr = feeConfigDetailView.getDescriptionAr();
	       	
	 	   }
	    
	    if (feeConfigDetailView.getMinValue() != null)
	       {
	    	subFeeMinValue = feeConfigDetailView.getMinValue().toString();
	       	
	 	   }
	    if (feeConfigDetailView.getMaxValue() != null)
	       {
	    	subFeeMaxValue = feeConfigDetailView.getMaxValue().toString();
	       	
	 	   }
	   
    	
    }catch (Exception e) {
    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	System.out.println("Exception "+e);
	}

      return "AddFeeConfiguration";

}

public String cmdDelete_Click()
{
    //Write your server side code here
	//fromDelete = true;
	FeeConfigurationDetailView feeConfigDetailView=(FeeConfigurationDetailView)dataTable.getRowData();
   try
   {
	PropertyServiceAgent psa  =new PropertyServiceAgent();	
	feeConfigDetailView.setIsDeleted(1L);
	psa.addFeeConfigurationDetails(feeConfigDetailView);
	loadDataList();

	if(request.containsKey("ProcedureType")){
	selectOneProcedureType  = request.get("ProcedureType").toString();}
	
	if(request.containsKey("FeeType")){
	selectOneFeeType = request.get("FeeType").toString();}
	
	if(request.containsKey("BasedOnType")){
	selectOnebasedOn= request.get("BasedOnType").toString();} 	
    	
    }catch (Exception e) {
    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	System.out.println("Exception "+e);
	}
    return "delete";
}

public FacilityView getFacilityView() {
	return facilityView;
}

public void setFacilityView(FacilityView facilityView) {
	this.facilityView = facilityView;
}

public FacilityView getFacilityViewEdit() {
	return facilityViewEdit;
}

public void setFacilityViewEdit(FacilityView facilityViewEdit) {
	this.facilityViewEdit = facilityViewEdit;
}

public boolean isPageModeAdd() {
	return isPageModeAdd;
}

public void setPageModeAdd(boolean isPageModeAdd) {
	this.isPageModeAdd = isPageModeAdd;
}

public HtmlInputText getCtlRentValue() {
	return ctlRentValue;
}

public void setCtlRentValue(HtmlInputText ctlRentValue) {
	this.ctlRentValue = ctlRentValue;
}

public String getPageMode() {
	return pageMode;
}

public void setPageMode(String pageMode) {
	this.pageMode = pageMode;
}



public String btnBack_Click(){
	
	return "BackScreen";
	

}

public String getSelectOneProcedureType() {
	return selectOneProcedureType;
}

public void setSelectOneProcedureType(String selectOneProcedureType) {
	this.selectOneProcedureType = selectOneProcedureType;
}

public String getSelectOneFeeType() {
	return selectOneFeeType;
}

public void setSelectOneFeeType(String selectOneFeeType) {
	this.selectOneFeeType = selectOneFeeType;
}

public String getAccountNo() {
	return accountNo;
}

public void setAccountNo(String accountNo) {
	this.accountNo = accountNo;
}

public String getPercentage() {
	return percentage;
}

public void setPercentage(String percentage) {
	this.percentage = percentage;
}

public String getSelectOnebasedOn() {
	return selectOnebasedOn;
}

public void setSelectOnebasedOn(String selectOnebasedOn) {
	this.selectOnebasedOn = selectOnebasedOn;
}

public String getMinAmount() {
	return minAmount;
}

public void setMinAmount(String minAmount) {
	this.minAmount = minAmount;
}

public String getMaxAmount() {
	return maxAmount;
}

public void setMaxAmount(String maxAmount) {
	this.maxAmount = maxAmount;
}

public String getFeeConfigurationId() {
	return feeConfigurationId;
}

public void setFeeConfigurationId(String feeConfigurationId) {
	this.feeConfigurationId = feeConfigurationId;
}

public FeeConfigurationView getFeeConfig() {
	return feeConfig;
}

public void setFeeConfig(FeeConfigurationView feeConfig) {
	this.feeConfig = feeConfig;
}

public FeeConfigurationDetailView getDataItem() {
	return dataItem;
}

public void setDataItem(FeeConfigurationDetailView dataItem) {
	this.dataItem = dataItem;
}

public HtmlSelectOneMenu getFeeSubTypeSelectMenu() {
	return feeSubTypeSelectMenu;
}

public void setFeeSubTypeSelectMenu(HtmlSelectOneMenu feeSubTypeSelectMenu) {
	this.feeSubTypeSelectMenu = feeSubTypeSelectMenu;
}



public String getSelectOneSubFee() {
	return selectOneSubFee;
}

public void setSelectOneSubFee(String selectOneSubFee) {
	this.selectOneSubFee = selectOneSubFee;
}

public String getSubFeeAmount() {
	return subFeeAmount;
}

public void setSubFeeAmount(String subFeeAmount) {
	this.subFeeAmount = subFeeAmount;
}

public String getSubFeeDescriptionEn() {
	return subFeeDescriptionEn;
}

public void setSubFeeDescriptionEn(String subFeeDescriptionEn) {
	this.subFeeDescriptionEn = subFeeDescriptionEn;
}

public String getSubFeeDescriptionAr() {
	return subFeeDescriptionAr;
}

public void setSubFeeDescriptionAr(String subFeeDescriptionAr) {
	this.subFeeDescriptionAr = subFeeDescriptionAr;
}

public String getSubFeeMinValue() {
	return subFeeMinValue;
}

public void setSubFeeMinValue(String subFeeMinValue) {
	this.subFeeMinValue = subFeeMinValue;
}

public String getSubFeeMaxValue() {
	return subFeeMaxValue;
}

public void setSubFeeMaxValue(String subFeeMaxValue) {
	this.subFeeMaxValue = subFeeMaxValue;
}

public boolean getIsArabicLocale()
{
	isArabicLocale = !getIsEnglishLocale();
	return isArabicLocale;
}
public boolean getIsEnglishLocale()
{
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	return isEnglishLocale;
}

public String getPageModeSubType() {
	return pageModeSubType;
}

public void setPageModeSubType(String pageModeSubType) {
	this.pageModeSubType = pageModeSubType;
}

public String getFeeSubTypeDetailId() {
	return feeSubTypeDetailId;
}

public void setFeeSubTypeDetailId(String feeSubTypeDetailId) {
	this.feeSubTypeDetailId = feeSubTypeDetailId;
}

public Boolean getSubTypeRander() {
	return subTypeRander;
}

public void setSubTypeRander(Boolean subTypeRander) {
	this.subTypeRander = subTypeRander;
}

public Boolean getComboDisable() {
	return comboDisable;
}

public void setComboDisable(Boolean comboDisable) {
	this.comboDisable = comboDisable;
}

public Integer getPaginatorMaxPages() {

	return WebConstants.SEARCH_RESULTS_MAX_PAGES;

	}

public void setPaginatorMaxPages(Integer paginatorMaxPages) {
	this.paginatorMaxPages = paginatorMaxPages;
}

/**
 * @return the paginatorRows
 */
public Integer getPaginatorRows() {
	paginatorRows = 10;
	return paginatorRows;
}

public void setPaginatorRows(Integer paginatorRows) {
	this.paginatorRows = paginatorRows;
}

public Integer getRecordSize() {
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	recordSize = (Integer) viewMap.get("recordSize");
	if(recordSize==null)
		recordSize = 0;
	return recordSize;
}

public void setRecordSize(Integer recordSize) {
	this.recordSize = recordSize;
}
private String getLoggedInUser() {
	context = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession) context.getExternalContext()
			.getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
		UserDbImpl user = (UserDbImpl) session
				.getAttribute(WebConstants.USER_IN_SESSION);
		loggedInUser = user.getLoginId();
	}

	return loggedInUser;
}
public Date getCreatedOnDate( )throws Exception
{
	String methodName="getCreatedOnDate";
	SystemParameters parameters = SystemParameters.getInstance();
	
	DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	if(viewMap.containsKey("CreatedOn")
	  && viewMap.get("CreatedOn").toString()!=null 
	  && (viewMap.get("CreatedOn").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdOn.."+viewMap.get("CreatedOn").toString());
		return  df.parse(df.format(viewMap.get("CreatedOn")));
		
	}
	else
	{
		logger.logInfo(methodName+"|"+"createdOn.."+new Date());
		return  new Date();
	}
	 
	
}
public String getCreatedByUser(String loggedInUser)
{
	String methodName="getCreatedByUser";
	
  if(viewMap.containsKey("CreatedBy")
	 && viewMap.get("CreatedBy").toString()!=null 
	 && (viewMap.get("CreatedBy").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"createdBy.."+viewMap.get("CreatedBy").toString());
		return  viewMap.get("CreatedBy").toString();	
	}
  else
  {
		logger.logInfo(methodName+"|"+"createdBy.."+loggedInUser);
		return  loggedInUser;
  }
	 
	
}

public Date getCreatedOnDateSubType( )throws Exception
{
	String methodName="getCreatedOnDateSubType";
	SystemParameters parameters = SystemParameters.getInstance();
	
	DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	if(viewMap.containsKey("SubTypeCreatedOn")
	  && viewMap.get("SubTypeCreatedOn").toString()!=null 
	  && (viewMap.get("SubTypeCreatedOn").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"SubTypeCreatedOn.."+viewMap.get("SubTypeCreatedOn").toString());
		
		return  df.parse(df.format(viewMap.get("SubTypeCreatedOn")));
	}
	else
	{
		logger.logInfo(methodName+"|"+"createdOn.."+new Date());
		return  new Date();
	}
	 
	
}
public String getCreatedByUserSubType(String loggedInUser)
{
	String methodName="getCreatedByUserSubType";
	
  if(viewMap.containsKey("SubTypeCreatedBy")
	 && viewMap.get("SubTypeCreatedBy").toString()!=null 
	 && (viewMap.get("SubTypeCreatedBy").toString()).length()>0)
	{
		logger.logInfo(methodName+"|"+"SubTypeCreatedBy.."+viewMap.get("SubTypeCreatedBy").toString());
		return  viewMap.get("SubTypeCreatedBy").toString();	
	}
  else
  {
		logger.logInfo(methodName+"|"+"createdBy.."+loggedInUser);
		return  loggedInUser;
  }
	 
	
}
  public String getBundleMessage(String key){
	logger.logInfo("getBundleMessage(String) started...");
	String message = "";
	try
	{
		message = ResourceUtil.getInstance().getProperty(key);

	logger.logInfo("getBundleMessage(String) completed successfully!!!");
	}
	catch (Exception exception) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("getBundleMessage(String) crashed ", exception);
	}
	return message;
}

}
