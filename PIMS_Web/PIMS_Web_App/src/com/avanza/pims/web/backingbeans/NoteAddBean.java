package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.ui.util.ResourceUtil;

public class NoteAddBean extends AbstractController {

	private static Logger logger = Logger.getLogger(NoteAddBean.class);

	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private String errorMessage = "";
	
	private static final String NOTES_DATA = "NotesData";
	
	NotesBean notesBean = new NotesBean();
	
	public void init() 
    {   	
		super.init();
   	 	if(!isPostBack()){
   	 		setMode();
   	 		setValues();
   	 	}
   	 	
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	public void setMode(){
		logger.logInfo("setMode start...");

		try {
			String noteOwner = (String)sessionMap.get("noteowner");
			if(noteOwner==null)
				noteOwner = "";
			viewMap.put("noteowner", noteOwner);
			sessionMap.remove("noteowner");
			
			String entityId = (String)sessionMap.get("entityId");
			if(entityId==null)
				entityId = "";
			viewMap.put("entityId", entityId);
			sessionMap.remove("entityId");
			
			logger.logInfo("setMode completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setMode crashed... ", ex);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR);
		}
	}
	
	public void setValues(){
		logger.logInfo("setValues start...");
		try {
			
			logger.logInfo("setValues completed successfully...");
		} catch (Exception ex) {
			logger.LogException("setValues crashed... ", ex);
			errorMessage = CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR);
		}
	}
	
	
	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessage = ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR);        	
		}
	}
	
	public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText = "window.close();";
        
        getFacesContext().getExternalContext().getSessionMap().remove(WebConstants.VACANT_UNIT_LIST);
        
        // Add the Javascript to the rendered page's header for immediate execution
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
        logger.logInfo("cancel() completed successfully!!!");
    }
	
	public void addNote(javax.faces.event.ActionEvent event)
	{
		logger.logInfo("addNote(ActionEvent) started...");
		try
 		{
			if(validateForAdd()){
				sessionMap.put(NOTES_DATA, notesBean);
				
				String noteOwner = (String)viewMap.get("noteowner");
				if(noteOwner==null)
					noteOwner = "";
				sessionMap.put("noteowner", noteOwner);
				
				String entityId = (String)viewMap.get("entityId");
				if(entityId==null)
					entityId = "";
				sessionMap.put("entityId", entityId);
				
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:closeWindowSubmit();";
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
			else{
    			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Notes.MSG_NO_COMMENTS);
			}
			logger.logInfo("addNote(ActionEvent) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("addNote(ActionEvent) crashed ", exception);
		}
	}
	
	private Boolean validateForAdd(){
    	logger.logInfo("validateForAdd() started...");
	    Boolean validated = true;
    	try
 		{
    		if(StringHelper.isEmpty(notesBean.getDescription()))
    		{
    			errorMessage = CommonUtil.getBundleMessage(MessageConstants.Notes.MSG_NO_COMMENTS);
				validated = false;
    		}
			logger.logInfo("validateForAdd() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("validateForAdd() crashed ", exception);
		}
		return validated;
    }

	public String getErrorMessage() {
		List<String> temp = new ArrayList<String>();
		if(!errorMessage.equals(""))
			temp.add(errorMessage);
		return CommonUtil.getErrorMessages(temp);
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public NotesBean getNotesBean() {
		return notesBean;
	}

	public void setNotesBean(NotesBean notesBean) {
		this.notesBean = notesBean;
	}
	
}
