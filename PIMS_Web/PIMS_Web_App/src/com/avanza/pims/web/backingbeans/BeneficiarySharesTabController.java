package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.hibernate.Session;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.entity.InheritedAsset;
import com.avanza.pims.entity.InheritedAssetShares;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.mems.minors.InheritanceFileBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.vo.BeneficiaryDetailsView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.InheritedAssetShareInfoView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;

/** The class acts as a backing bean/controller for the BeneficiarySharesTab.
 * @author Farhan Muhammad Amin
 */
@SuppressWarnings("unchecked")
public class BeneficiarySharesTabController extends AbstractController {
	
	private transient Logger logger = Logger.getLogger(BeneficiarySharesTabController.class);
	
	private static final long serialVersionUID 	= 1L;
	private  long MAX_PCT_WT 	= 0L;
	
	private final String BENENEFICIARY_LIST 			= "BENENEFICIARY_LIST"; 
	private final String ASSET_CLASS_LIST 				= "ASSETCLASS_LIST";
	private final String ASSET_SHARE_INFO 				= "ASSET_SHARE_INFO";
	private final String ASSET_SHARE_INFO_PER_PAGE 		= "ASSET_SHARE_INFO_PER_PAGE";


	Map viewRootMap = getFacesContext().getViewRoot().getAttributes();	
	FacesContext context=FacesContext.getCurrentInstance();  
	protected Map sessionMap   = context.getExternalContext().getSessionMap();;
    private String beneficiary;
    DecimalFormat oneDForm = new DecimalFormat("#.00");    
    HtmlSelectOneMenu cmbBnfList = new HtmlSelectOneMenu();    
    HtmlSelectOneMenu cmbInheritanceAsset = new HtmlSelectOneMenu();
    HtmlInputText pctWeight = new HtmlInputText();
    HtmlInputText txtAssetShare = new HtmlInputText();
    HtmlSelectBooleanCheckbox chkWill = new HtmlSelectBooleanCheckbox();
	private List<String> messages = new ArrayList<String>();
	private List<String> errorMessages=  new ArrayList<String>();
    private String hdnAssetShareMap;
    private String hdnBenefShareMap;
	private List<InheritedAssetShareInfoView> pageView = new ArrayList<InheritedAssetShareInfoView>();
	private List<InheritedAssetShareInfoView> assetShareDataList = null;
       
    protected InheritedAssetService inheritedAssetService = new InheritedAssetService();     
    protected HtmlDataTable inhShareSummaryTable = new HtmlDataTable();
    InheritanceFileBean parentBean;
	boolean isMigratedFile = false ;
	Double sumBenefShares = 0.0D;	
	List<InheritedAssetView> assetsListToDistributeShares = null;
	InheritanceFileView fileView;
	@Override
	@SuppressWarnings("unchecked")
	public void init() {
	
		try {
			super.init();
			
			if ( !isPostBack() ) 
			{							
			    setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     		        		      
		        
		        viewRootMap.put("recordSize",0);
		        
			}
	
			
		}								
		catch(Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}			
		logger.logInfo("[init() .. Ends]");
	}

	public void loadTabDetails() throws Exception 
	{
		Long fileId = null;
		if(viewRootMap.get(WebConstants.InheritanceFile.FILE_ID) != null)
		{	 
			fileId = new Long(viewRootMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
	
			loadAssetList(fileId);				
		}
	}

	
	private void loadAssetList(Long fileId )throws Exception 
	{
		assetShareDataList = inheritedAssetService.getBeneficiarySharesView(fileId);
		
		if (null == assetShareDataList) {
			assetShareDataList = new ArrayList<InheritedAssetShareInfoView>();
		}
		sortAssetShareDataList(assetShareDataList);
		viewRootMap.put(ASSET_SHARE_INFO, assetShareDataList);				

	}
			
	private void sortAssetShareDataList( List<InheritedAssetShareInfoView> sharesTable ) throws Exception
	{
		if( CommonUtil.getIsEnglishLocale() )
		{
		   Collections.sort(sharesTable, new ListComparator("sortFieldEn",true));
		}
		else
		{
			Collections.sort(sharesTable, new ListComparator("sortFieldAr",true));
		}
	}
	@SuppressWarnings("unchecked")
	public void deleteInheritedShare(ActionEvent evt) 
	{		
		InheritedAssetShareInfoView singleRow = (InheritedAssetShareInfoView) inhShareSummaryTable.getRowData();		
		if ( null ==singleRow ) {return;}			
		try 
		{			
			if ( inheritedAssetService.deleteByInheritedAssetShareId(singleRow.getInheritedAssetShareId(), getLoggedInUserId() ) ) 
			{					
				getAssetShareDataList().remove(singleRow) ;
				sortAssetShareDataList(getAssetShareDataList()); 
				pctWeight.setValue(null);
				chkWill.setValue(false);
				cmbInheritanceAsset.setValue(null);
			}			
		} 
		catch( Exception ex) 
		{
			logger.LogException("[Exception occured in deleteInheritedShare(ActionEvent)", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}
	}
	

	@SuppressWarnings("unchecked")
	public void onMessageFromBeneficiarySharePopUp() 
	{
		try 
		{
		  List<InheritedAssetShareInfoView> list = (ArrayList<InheritedAssetShareInfoView>)sessionMap.remove(WebConstants.BeneficiarySharesPopup.INHERITED_ASSET_MEMS_VIEW_LIST);
		  boolean reload= false;
		  for (InheritedAssetShareInfoView shareView : list) 
		  {
			if( shareView.getPctWeight() != null &&  Double.parseDouble( shareView.getPctWeight() ) > 0d )
        	{
        		inheritedAssetService.add(shareView);
        	}
        	else if( shareView.getInheritedAssetShareId() != null )
        	{
        		inheritedAssetService.deleteByInheritedAssetShareId( shareView.getInheritedAssetShareId(),getLoggedInUserId());
        	}
			reload=true;
		  }		
		  if( reload )
		  {
			  long fileId = Long.parseLong( viewRootMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
			  loadAssetList(fileId);
			  
		  }
		  messages.add(CommonUtil.getBundleMessage("beneficiaryShares.SharesDistributed"));
		  viewRootMap.put(WebConstants.InherFileSuccessMsg.ASSET_SHARE_SUCC, messages);
			
		} 
		catch(Exception ex) 
		{
		  logger.LogException("[Error occured onMessageFromBeneficiarySharePopUp", ex);
		  errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		  viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}
	}
	@SuppressWarnings("unchecked")
	private boolean hasDistributeAssetAccordingToShariaSharesError( List<InheritedAssetView> assetsList, List<BeneficiariesPickListView> beneDtl) throws Exception 
	{
		boolean hasError = false;
		boolean isFileTypeUnderguardianLegalAssistanceFounding  = WebConstants.InheritanceFileType.FILES_TYPE_SAME_OWNER_BENEFICIARY.indexOf(  fileView.getFileTypeKey() ) >0;
		  
		for (BeneficiariesPickListView beneficiary : beneDtl) 
		{
			boolean isEmptyShare = beneficiary.getSharePercentage() == null;
			boolean isFilePersonSameAsBeneficiary =  isFileTypeUnderguardianLegalAssistanceFounding && 
			   										 beneficiary.getPersonId().toString().equals( fileView.getFilePersonId() );
			if( 
				( isFilePersonSameAsBeneficiary  || !isFileTypeUnderguardianLegalAssistanceFounding ) && 
				  isEmptyShare
			  )
			{
				String msg=  java.text.MessageFormat.format(
															CommonUtil.getBundleMessage("distributeAsset.msg.benefShareNotSpecified"), 
															beneficiary.getFullName()
														   );
				errorMessages.add( msg );
				hasError = true;
				break;
			}
			else if( isFilePersonSameAsBeneficiary  || !isFileTypeUnderguardianLegalAssistanceFounding ) 
			{
				sumBenefShares +=  beneficiary.getSharePercentage();
			}
		}
		if( hasError )return hasError;
		
		for (InheritedAssetView asset: assetsList ) 
		{
			if( asset.getTotalShareValue() == null  || asset.getTotalShareValue().compareTo( sumBenefShares )!= 0 )
			{
				continue;
			}
			else
			{
				if( assetsListToDistributeShares == null )
				{
				  assetsListToDistributeShares = new ArrayList<InheritedAssetView>();
				}
				assetsListToDistributeShares.add(asset);
			}
		}
		return hasError;
	}
	public boolean getShowDistributeAssetAccordingToSharia()
	{
		if ( assetShareDataList == null || assetShareDataList.size() <= 0 )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public void onDistributeAssetAccordingToShariaShares() 
	{
		try 
		{
			  InheritanceFileBean bean  = (InheritanceFileBean) getBean("pages$inheritanceFile");
			  List<InheritedAssetView> assetsList = bean.loadInheritedAssetTabDetails();

			  RequestView requestView = ( RequestView )viewMap.get(WebConstants.REQUEST_VIEW);
			  fileView = requestView.getInheritanceFileView();
			  sumBenefShares = 0.0D;
			  List<BeneficiariesPickListView> beneDtl = inheritedAssetService.getBeneficiariesView(fileView.getInheritanceFileId() );
			  if( hasDistributeAssetAccordingToShariaSharesError( assetsList, beneDtl ) ) 
			  {
				  viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
				  return;
			  }
			  distributeAssetAccordingToShariaInTransaction( beneDtl );
			  long fileId = Long.parseLong( viewRootMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
			  loadAssetList(fileId);
			  messages.add(CommonUtil.getBundleMessage("beneficiaryShares.SharesDistributed"));
			  viewRootMap.put(WebConstants.InherFileSuccessMsg.ASSET_SHARE_SUCC, messages);
			
		} 
		catch(Exception ex) 
		{
				logger.LogException("Exception occured in 	onDistributeAssetAccordingToShariaShares", ex);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}
	}

	/**
	 * @param assetsList
	 * @param beneDtl
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void distributeAssetAccordingToShariaInTransaction( List<BeneficiariesPickListView>  beneDtl) throws Exception 
	{
	  try
	  {
			
		   boolean isFileTypeUnderguardianLegalAssistanceFounding  = WebConstants.InheritanceFileType.FILES_TYPE_SAME_OWNER_BENEFICIARY.indexOf(  fileView.getFileTypeKey() ) >0;
		   ApplicationContext.getContext().getTxnContext().beginTransaction();
			
		   String delQuery =  " update inherited_asset_shares  set is_deleted=1 ,updated_by= '"+getLoggedInUserId()+"' where  " +
		   					  "	is_deleted<>1 and "+
			   				  " inherited_asset_id in("+
			   				  "							select inherited_asset_id   from inherited_asset where inheritance_file_id ="+fileView.getInheritanceFileId()+ 
			   				  "						  )";
	
		   
			
			Session session = EntityManager.getBroker().getSession();
			session.createSQLQuery(delQuery).executeUpdate();
	
		   for (InheritedAssetView assetView : assetsListToDistributeShares) 
		   {
			   for (BeneficiariesPickListView beneficiary : beneDtl) 
			   {
				   //either beneficiary's share percentage is null or
				   //if underguardian or founding or legal assistance file and file person not same as beneficiary then do not distribute for that person 
				 if( 
					 ( 
						 beneficiary.getSharePercentage() == null || beneficiary.getSharePercentage().compareTo(0d)==0 
						 
				 	  )
					 ||
					  (
							  isFileTypeUnderguardianLegalAssistanceFounding && 
							  !beneficiary.getPersonId().toString().equals( fileView.getFilePersonId() )
					  )
				   )
				 {
					continue;
				 }
				 InheritedAssetShares ias = new InheritedAssetShares();
				 ias.setCreatedBy( getLoggedInUserId() );
				 ias.setUpdatedBy( getLoggedInUserId() );
				 ias.setCreatedOn( new Date() );
				 ias.setUpdatedOn( new Date() );
				 ias.setIsDeleted( 0l );
				 ias.setIsWill( 0l );
				 ias.setRecordStatus(1L);
							 
				 InheritanceBeneficiary ibenef = new InheritanceBeneficiary();
				 ibenef.setInheritanceBeneficiaryId(beneficiary.getInheritedBeneficiaryId() );
				 ias.setInheritanceBeneficiary( ibenef );
	 
				 ias.setSharePercentage(  beneficiary.getSharePercentage() );
							 
				 InheritedAsset asset = new InheritedAsset();
				 asset.setInheritedAssetId( assetView.getInheritedAssetId()  );
				 ias.setInheritedAsset( asset );
							 
				 EntityManager.getBroker().add( ias );

					
							 
			  }	
	          ApplicationContext.getContext().getTxnContext().commit();
			}
	  }
	  catch(Exception e)
	  {
		  ApplicationContext.getContext().getTxnContext().rollback();
		  throw e;
			  
	  }
	  finally
	  {
		  ApplicationContext.getContext().getTxnContext().release();
	  }
	}
	
	@SuppressWarnings("unchecked")
	public void onBeneficiaryShareClicked(){
		

		try {
			InheritedAssetShareInfoView row =  (InheritedAssetShareInfoView)inhShareSummaryTable.getRowData();
			sessionMap.put( WebConstants.BeneficiarySharesPopup.INHERITED_BENEF_ID,row.getInheritedBeneficiaryId());
			prepareValuesForBeneficiarySharesPopup();
			} catch(Exception ex) {
				logger.LogException("[Exception occured in onBeneficiaryShareClicked]", ex);
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
			}
	}
	@SuppressWarnings("unchecked")
	public void onOpenBeneficiarySharesPopup() 
	{
		try 
		{
			prepareValuesForBeneficiarySharesPopup();
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onOpenBeneficiarySharesPopup]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}
								
	
	}

	private void prepareValuesForBeneficiarySharesPopup()throws Exception 
	{
				
			InheritanceFileBean bean  = (InheritanceFileBean) getBean("pages$inheritanceFile");
			List<InheritedAssetView> assetsList = bean.loadInheritedAssetTabDetails();
			Map<Long,Double> assginedSharesMap= new HashMap<Long, Double>();
			for (InheritedAssetShareInfoView obj : getAssetShareDataList()) 
			{
				
				   long key = Long.parseLong(obj.getInheritedAssetId());
					Double assignedShares  =  assginedSharesMap.get( key );
					assignedShares  = assignedShares  != null ? assignedShares:0;
					assginedSharesMap.put(  
											key,
											Double.valueOf( 
																oneDForm.format( 
																					assignedShares + Double.parseDouble(  obj.getPctWeight() ) 
																				)
														  ) 
							             );
			                 
		             
				
				}
				
			sessionMap.put(WebConstants.BeneficiarySharesPopup.ASSIGNED_SHARES_MAP,assginedSharesMap);
			sessionMap.put(WebConstants.BeneficiarySharesPopup.INHERITED_ASSET_MEMS_VIEW_LIST,assetsList);
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, 
					                          viewRootMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
			executeJavascript("javaScript:openBeneficiarySharesPopup();");
		
	}
	
	
	/** The function validates the semantics of each input provided by the user and adds the appropriate
	 * error messages that are to be displayed to the user where applicable. The semantics/bussiness rules
	 * that function validates are as follows,
	 * <p> The function make sure that no beneficiary is added twice for a particular asset. It queries 
	 * 		 the list of table data in the view root for the existence of beneficiary.  </p>
	 * <p> The functions sums the percentage weight allocated to each beneficiary for given asset and make
	 * sure the sums doesn't exceeds 100. <p>	 * 	 *    
	 * 
	 * @return The function returns <b> true </b> if all validations are passed successfully. <b>False</b>,
	 * 	otherwise.
	 * 
	 * @author Farhan Muhammad Amin
	 */
	@SuppressWarnings("unchecked")
	private boolean validateSemantics() {
		logger.logInfo("[validateSemantics() .. Starts]");
		
		boolean bValid = true;		
		ArrayList<InheritedAssetShareInfoView> sharesTableData = 
							(ArrayList<InheritedAssetShareInfoView>) viewRootMap.get(ASSET_SHARE_INFO);
						
		
		if( null != sharesTableData ) {		
			String benefId = cmbBnfList.getValue().toString();
			String assetId = cmbInheritanceAsset.getValue().toString();
			
			Long assetShare = getAssetTotalShare(assetId);
			if(assetShare != null)
			    MAX_PCT_WT = assetShare;
			 
			InheritedAssetShareInfoView info = new InheritedAssetShareInfoView();			
			info.setInheritedBeneficiaryId(benefId);
			info.setInheritedAssetId(assetId);
								
			if ( sharesTableData.contains(info)) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.InheritedAssetShares.MSG_BENEF_ALREADY_EXISTS));
				bValid = false;
			}		
			
			long pctWeightSum = 0;
			
			for(InheritedAssetShareInfoView singleShare : sharesTableData) 
			{
				if( singleShare.getInheritedAssetId().equals(cmbInheritanceAsset.getValue().toString())) 
				{
					pctWeightSum += Double.parseDouble(singleShare.getPctWeight());
				}
			}
			
			pctWeightSum += Double.parseDouble(pctWeight.getValue().toString());
			
			if( pctWeightSum > this.MAX_PCT_WT ) 
			{
				errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.shareSunCdntBeGreater"));
				bValid = false;
			}
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
			}
		}							
		logger.logInfo("[validateSemantics() .. Ends]");	
		return bValid;
	}
	
	private Long getAssetTotalShare(String assetId) {
		if(viewRootMap.get(WebConstants.InheritanceFile.ASSET_SHARE_MAP) != null){
			HashMap<Long, Long> assetshareMap;
			assetshareMap = (HashMap<Long, Long>) viewRootMap.get(WebConstants.InheritanceFile.ASSET_SHARE_MAP);
			if(assetshareMap != null && assetshareMap.size() > 0){
				return assetshareMap.get(Long.valueOf(assetId));
			}
		}
		return null;
	}

	/**  The function validates the input provided by user. The function validates the following 
	 *   input components and add approriate error messages whereever applicable
	 *   <p> The function checks if the user has selected a valid beneficiary from the beneficiary list</p>
	 *   <p> The function checks if the user has selected a valid inherited asset from the asset class list</p>
	 *   <p> The function then validates that the percentage shouldn't be left empty. It should contain 
	 *   	numeric data and it shouldn't be greater than 100 </p>	 *   
	 * @return <b>true</b> if input passes all the validations stated above.<b>false</b>, otherwise.
	 * @author Farhan Muhammad Amin
	 */
	private boolean validateInputs() {			
		logger.logInfo("[validateInputs() .. Starts]");
		
		boolean bValid = true;
		String assetId = cmbInheritanceAsset.getValue().toString();
		
		Long assetShare = getAssetTotalShare(assetId);
		if(assetShare != null)
		MAX_PCT_WT = assetShare;
		
		if( null==cmbBnfList.getValue() || cmbBnfList.getValue().toString().equals("-1") ) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.InheritedAssetShares.MSG_BENF_REQUIRED));
			bValid=  false;
		}
		
		if( null==cmbInheritanceAsset.getValue() ||cmbInheritanceAsset.getValue().toString().equals("-1")) {			
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.InheritedAssetShares.MSG_ASSET_REQUIRED));
			bValid=  false;		
		}
		
		if ( null==pctWeight.getValue() || 0==pctWeight.getValue().toString().trim().length() ) {
			errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.provideShareValue"));
			bValid=  false;
		} 
		else if ( null!=pctWeight.getValue() || 0<pctWeight.getValue().toString().trim().length() ) {
			try {
				if ( MAX_PCT_WT<Double.parseDouble(pctWeight.getValue().toString().trim()) ) {
					errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.shareSunCdntBeGreater"));
					bValid=  false;
				}
			} catch(NumberFormatException ex) {
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.InheritedAssetShares.MSG_INVALID_PCT_WT));
				bValid=  false;
			}
		}		
		if(errorMessages != null && errorMessages.size() > 0)
		{
			viewRootMap.put(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR, errorMessages);
		}
		logger.logInfo("[validateInputs() .. Ends]");
		return bValid;
	}


	@SuppressWarnings("unchecked")
	public List<InheritedAssetShareInfoView> getAssetShareDataList() {					
		if (viewRootMap.containsKey(ASSET_SHARE_INFO)) {
			return (ArrayList<InheritedAssetShareInfoView>) viewRootMap.get(ASSET_SHARE_INFO);			
		}
		return new ArrayList<InheritedAssetShareInfoView>();		
	}

	public void setAssetShareDataList(List<InheritedAssetShareInfoView> assetShareDataList) {
		this.assetShareDataList = assetShareDataList;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getBeneficiaryList() {
		if (viewRootMap.containsKey(BENENEFICIARY_LIST)) { 
			return (ArrayList<SelectItem>)viewRootMap.get(BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>();		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getAssetClassList() {		
		if ( viewRootMap.containsKey(ASSET_CLASS_LIST)) {			
			return (ArrayList<SelectItem>)viewRootMap.get(ASSET_CLASS_LIST);
		}
		return new ArrayList<SelectItem>();
	}
		
	@Override
	public void doSearchItemList() {		
		List<InheritedAssetShareInfoView> shareView = getAssetShareDataList();				
		setTotalRows(shareView.size());		
		doPagingComputations();
		
		if( 0==getTotalRows() ) {
			setCurrentPage(0);
			getPageView().clear();
			return;
		}
		
		int currentPage = getCurrentPage();
		int rowsPerPage = getRowsPerPage();	
		int startIndex 	= (currentPage-1)*rowsPerPage;
		//int endIndex 	= startIndex + rowsPerPage;
		
//		List<InheritedAssetShareInfoView> pageView = getPageView();		
//		pageView.clear();
//		
//		for(int i=startIndex ; (i<endIndex&&i<shareView.size()) ; ++i) {
//			pageView.add(shareView.get(i));
//		}			
	}
	
	public List<InheritedAssetShareInfoView> getPageView() {
		if (viewRootMap.containsKey(ASSET_SHARE_INFO_PER_PAGE)) {
			return (ArrayList<InheritedAssetShareInfoView>) viewRootMap.get(ASSET_SHARE_INFO_PER_PAGE);			
		}
		return new ArrayList<InheritedAssetShareInfoView>();
	}

	public void setPageView(List<InheritedAssetShareInfoView> pageView) {
		this.pageView = pageView;
	}
	
	public HtmlDataTable getInhShareSummaryTable() {
		return inhShareSummaryTable;
	}

	public void setInhShareSummaryTable(HtmlDataTable inhShareSummaryTable) {
		this.inhShareSummaryTable = inhShareSummaryTable;
	}

	public HtmlSelectOneMenu getCmbInheritanceAsset() {
		return cmbInheritanceAsset;
	}

	public void setCmbInheritanceAsset(HtmlSelectOneMenu cmbInheritanceAsset) {
		this.cmbInheritanceAsset = cmbInheritanceAsset;
	}

	public String getBeneficiary() {
		return beneficiary;
	}
	
	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}
	
	public HtmlSelectOneMenu getCmbBnfList() {
		return cmbBnfList;
	}
	
	public void setCmbBnfList(HtmlSelectOneMenu cmbBnfList) {
		this.cmbBnfList = cmbBnfList;
	}
	
	public String getMessages() {
		return CommonUtil.getErrorMessages(messages);
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public HtmlInputText getPctWeight() {
		return pctWeight;
	}
	
	public void setPctWeight(HtmlInputText pctWeight) {
		this.pctWeight = pctWeight;
	}

	public HtmlSelectBooleanCheckbox getChkWill() {
		return chkWill;
	}

	public void setChkWill(HtmlSelectBooleanCheckbox chkWill) {
		this.chkWill = chkWill;
	}
	public void beneficiaryChanged(ValueChangeEvent e)
	{
		if(e.getNewValue() != null && e.getNewValue().toString().compareTo("-1") != 0)
		{
			if(viewRootMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null)
			{
				List<BeneficiaryDetailsView> beneDtl = (List<BeneficiaryDetailsView>) viewRootMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
				if(beneDtl != null && beneDtl.size() > 0)
				{
					for(BeneficiaryDetailsView detail : beneDtl)
					{
						if(detail.getInheritanceBeneficiaryId().compareTo(new Long(e.getNewValue().toString())) == 0)
						{
							//This code is written to remove the '.' if any from the value, coz value is considered as integer
							if(detail.getSharePercentage() != null){
								
								int index = detail.getSharePercentage().indexOf(".");
								if(index != -1){
									String totalShareWitoutPrecision = detail.getSharePercentage().substring(0, index);
									pctWeight.setValue(totalShareWitoutPrecision);
								}
								else
									pctWeight.setValue(detail.getSharePercentage());
							}
							
							break;
						}
					}
				}
			}
			else
				if(viewRootMap.get(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY) != null )
				{
					PersonView beneficiary = (PersonView) viewRootMap.get(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY);
					pctWeight.setValue(beneficiary.getSharePercentage());
				}
		}
	}

	public HtmlInputText getTxtAssetShare() {
		return txtAssetShare;
	}

	public void setTxtAssetShare(HtmlInputText txtAssetShare) {
		this.txtAssetShare = txtAssetShare;
	}
	public void assetChanged(ValueChangeEvent event){
		if(cmbInheritanceAsset.getValue() != null && cmbInheritanceAsset.getValue().toString().compareTo("-1") != 0){
			txtAssetShare.setValue(getAssetTotalShare(cmbInheritanceAsset.getValue().toString()));
		
		}
		else
			txtAssetShare.setValue("0");
	}

	public String getHdnAssetShareMap() {
	    if( viewRootMap.get( "hdnAssetShareMap" ) != null)
	    {
	    	hdnAssetShareMap = viewRootMap.get( "hdnAssetShareMap" ).toString();
	    }
		return hdnAssetShareMap;
	}

	public void setHdnAssetShareMap(String hdnAssetShareMap) {
		this.hdnAssetShareMap = hdnAssetShareMap;
		if( this.hdnAssetShareMap != null )
		{
			 viewRootMap.put(  "hdnAssetShareMap" ,this.hdnAssetShareMap);
		}
	}

	public String getHdnBenefShareMap() {
		if( viewRootMap.get( "hdnBenefShareMap" ) != null)
	    {
			hdnBenefShareMap = viewRootMap.get( "hdnBenefShareMap" ).toString();
	    }
		return hdnBenefShareMap;
	}

	public void setHdnBenefShareMap(String hdnBenefShareMap) {
		this.hdnBenefShareMap = hdnBenefShareMap;
		if( this.hdnBenefShareMap != null )
		{
			 viewRootMap.put(  "hdnBenefShareMap" ,this.hdnBenefShareMap);
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
}
