package com.avanza.pims.web.backingbeans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.DisBenInhBenAssociationView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings("unchecked")
public class FinanceTabController extends AbstractMemsBean {
	
	private static final long serialVersionUID = 1L;
	private HtmlDataTable disbursementTable = new HtmlDataTable();
	private static final String FINANCE_LIST = "FINANCE_LIST";
	DecimalFormat oneDForm = new DecimalFormat("#.00");
    private String totalDisbursementCount;
	@Override
	public void init() 
	{
		super.init();		
	}
	
	public void handleTabChange() throws Exception
	{
		viewMap.remove( FINANCE_LIST);
		
		double sumDisbursements = 0.0d;
		List<DisbursementDetailsView> listFinanceDisbursement = new ArrayList<DisbursementDetailsView>();
		List<DisbursementDetailsView> listDisbursement = getDisbDetails();
		for(DisbursementDetailsView eachDisbursement : listDisbursement)
		{
			List<PaymentDetailsView> listPaymentDetails = eachDisbursement.getPaymentDetails();
			if( null!=listPaymentDetails && 0<listPaymentDetails.size() ) 
			{
				for(PaymentDetailsView eachPayment : listPaymentDetails) 
				{
					DisbursementDetailsView financeDibursement = this.getCopy( eachDisbursement);
					financeDibursement.setRequestBeneficiaries( eachDisbursement.getRequestBeneficiaries());
					List<PaymentDetailsView> eachPaymentList = new ArrayList<PaymentDetailsView>(1);
					eachPaymentList.add( eachPayment);
					financeDibursement.setPaymentDetails( eachPaymentList);
					listFinanceDisbursement.add( financeDibursement);					
				}
			} 
			else 
			{
				DisbursementDetailsView financeDibursement = this.getCopy( eachDisbursement);
				financeDibursement.setRequestBeneficiaries( eachDisbursement.getRequestBeneficiaries());
				listFinanceDisbursement.add( financeDibursement);
			}
			sumDisbursements += eachDisbursement.getAmount();
		}
		setTotalDisbursementCount(oneDForm.format(sumDisbursements));
		viewMap.put( FINANCE_LIST, listFinanceDisbursement);		
		handleCompletion();
	}
	
	private boolean validateDisburse()throws Exception 
	{
		int hasRows = 0 ;
		List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
		boolean paymentDetailsPresent =true;
		String noCostCenterMessage = "";
		String balanceNotEnough = "";
		String balanceMinusBlockNotEnough = "";
		Map<Long,Double> benefSumAmountMap = new HashMap<Long, Double>();
		for(DisbursementDetailsView eachDisbursement : disbursementDetails) 
		{
				if( eachDisbursement.getSelected()==null || eachDisbursement.getSelected().equals(false)) 
				{
					continue;
				}
				hasRows++;
				for(DisbursementRequestBeneficiaryView singleBeneficiary : eachDisbursement.getRequestBeneficiaries()) 
				{
					for (DisBenInhBenAssociationView fileAssociaction  :  singleBeneficiary.getFileAssociations() )
					{
						double eachAmount=singleBeneficiary.getAmount().doubleValue();
						
						if( fileAssociaction.getCostCenter()==null || fileAssociaction.getCostCenter().trim().length()<=0 )
						{
							noCostCenterMessage+= singleBeneficiary.getName()+",";
						}
						else
						{
							Double balance = 0.0d;
							if( getRequestView().getRequestTypeId().compareTo(WebConstants.MemsRequestType.PAY_VENDOR_REQUEST_ID) != 0 )
							{
								
								balance = 	PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(
																											singleBeneficiary.getPersonId(),
																											getRequestView().getInheritanceFileId()
																										  );
							}
							else if( getRequestView().getRequestTypeId().compareTo(WebConstants.MemsRequestType.PAY_VENDOR_REQUEST_ID) != 0 )
							{
									
								 balance = 	PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(
																												singleBeneficiary.getPersonId(),
																												eachDisbursement.getFileId()
																										);
							}
							if( benefSumAmountMap.containsKey( 	singleBeneficiary.getPersonId() ) )
							{
								eachAmount += benefSumAmountMap.get( singleBeneficiary.getPersonId() ).doubleValue();
							}
							if ( eachDisbursement.getSrcType().compareTo(WebConstants.PaymentSrcType.SELF_ACCOUNT_ID) == 0 )
							{
								benefSumAmountMap.put(	singleBeneficiary.getPersonId(), singleBeneficiary.getAmount() );
								if(  balance == null || eachAmount > balance.doubleValue() )
								{
									balanceNotEnough+= singleBeneficiary.getName()+" ("+balance+"),";
								}
								else
								{
									Double blockingAmnt = BlockingAmountService.getBlockingAmount(singleBeneficiary.getPersonId(), null, null, null, null );
									//If its self account than amount is mandatory else its not.
									Long fileId = getRequestView().getInheritanceFileId();
									if( fileId == null)
									{
										fileId = eachDisbursement.getFileId(); 
									}
									Long beneficiaryId = new Long( singleBeneficiary.getPersonId().toString());
									 int validateBalanceCode = validateBalance(
											 									 balance ,
																				 blockingAmnt,
																				 DisbursementService.getRequestedAmount(
																							 								singleBeneficiary.getPersonId() , 
																							 								fileId, 
																							 								getRequestView().getRequestId(),
																							 								null 
																						 								),
																				 singleBeneficiary.getAmount(),
																				 beneficiaryId,
																				 singleBeneficiary.getName(),
																				 fileId
									   										  ) ;
									 if ( validateBalanceCode != 0 )
									 {
										 balanceMinusBlockNotEnough +=		 singleBeneficiary.getName();
									 }
//									}
									
								}
							}
							
						}
					}
					
						
				}
				if( 
					 (null ==balanceMinusBlockNotEnough || balanceMinusBlockNotEnough.trim().length()<=0) &&
					(null==balanceNotEnough || balanceNotEnough.trim().length()<=0) &&  
					(null==noCostCenterMessage || noCostCenterMessage.trim().length()<=0)   &&
					(null==eachDisbursement.getPaymentDetails() || 0==eachDisbursement.getPaymentDetails().size())
				  ) 
				{
					paymentDetailsPresent = false;
					break;
				}

			
		}
		
		if(hasRows<=0)
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_ROW));
			viewMap.put( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS, errorMessages);
			return false;
		}
		if(null!=noCostCenterMessage && noCostCenterMessage.trim().length()>0)
		{
			noCostCenterMessage = noCostCenterMessage.substring(0,noCostCenterMessage.length()-1);
			errorMessages.add(
							  java.text.MessageFormat.format( CommonUtil.getBundleMessage( "disburseFinance.msg.associateCostCenter"),noCostCenterMessage )
							 );
			return false;
		}
		else if(null!=balanceNotEnough && balanceNotEnough.trim().length()>0)
		{
				balanceNotEnough = balanceNotEnough.substring(0,balanceNotEnough.length()-1);
				errorMessages.add(
								  java.text.MessageFormat.format( CommonUtil.getBundleMessage( "disburseFinance.msg.balanceNotEnough"),balanceNotEnough)
								 );
				return false;
		}
		else if ( null != balanceMinusBlockNotEnough && balanceMinusBlockNotEnough.trim().length() > 0 )
		{
			//balanceMinusBlockNotEnough = balanceMinusBlockNotEnough.substring(0,balanceMinusBlockNotEnough.length()-1);
			//errorMessages.add( balanceMinusBlockNotEnough );
			return false;
		}
		else if( !paymentDetailsPresent)
		{
					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_ADD_PAYMENT));
					viewMap.put( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS, errorMessages);
					return false;
		}
		
		return true;
	}
	
	public void disburse() 
	{
		try 
		{
				errorMessages = new ArrayList<String>(0);
    		
				if(! validateDisburse() ) {return;}
				
				List<DisbursementDetailsView> disbDetailList = getDisbursementDetails();
				List<PaymentDetailsView> paymentDetailsViewList = new ArrayList<PaymentDetailsView>();
				List<DisbursementDetailsView> selectedList = new ArrayList<DisbursementDetailsView>();
				RequestView request = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
				for(DisbursementDetailsView singleDisbursement : disbDetailList) 
				{
					if( null!=singleDisbursement.getSelected() && singleDisbursement.getSelected() ) 
					{						
						singleDisbursement.getPaymentDetails().get( 0 ).setStatus( WebConstants.DISBURSED);
						singleDisbursement.setSelected( false);						
						selectedList.add( singleDisbursement  );
						
						paymentDetailsViewList.addAll(  singleDisbursement.getPaymentDetails() );
					}
				}
				
				RequestService reqWS = new RequestService();
				List<Long> disbursementsIdList = reqWS.addOrUpdate( request, paymentDetailsViewList, CommonUtil.getLoggedInUser());
				DisbursementService service = new DisbursementService();
				viewMap.put(WebConstants.FinanceTabPublishKeys.DISBURSEMENTS ,service.getDisbursementDetails(disbursementsIdList) );
				
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsFinanceMsgs.SUCC_DISB));
				viewMap.put( WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS, successMessages);
				handleCompletion();
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in disburse()]", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			if ( errorMessages != null && errorMessages.size()>0 )
			{
				viewMap.put( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS,errorMessages);
			}
			
		}
	}
				
	public void showPaymentDetails() {
		try
		{
				DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();
				sessionMap.remove(WebConstants.LOAD_PAYMENT_DETAILS);
				sessionMap.remove(WebConstants.SHOW_READONLY_POPUP);		
				sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, detailData);
				if( detailData.getIsDisbursed().equals( WebConstants.DISBURSED) ) 
				{
					sessionMap.put(WebConstants.SHOW_READONLY_POPUP, "");
				}
				String personId  = null;
				//if paid to vendor 
				if( detailData.getPaidTo()!= null && detailData.getPaidTo().equals("0") && detailData.getVendorId() != null )
				{
					personId = detailData.getVendorId().toString();
				}
				else
				{
					List<DisbursementRequestBeneficiaryView> reqSet = detailData.getRequestBeneficiaries();
					DisbursementRequestBeneficiaryView next = reqSet.get( 0);
					personId = next.getPersonId().toString();
				}
					
				
				String javaScriptText = " javaScript:openPaymentDetailsPopUp( 'pageMode=MODE_SELECT_MANY_POPUP&"
																				+ WebConstants.PERSON_ID+ "="+ personId
																				+ "&"
																				+ WebConstants.FinanceTabPublishKeys.IS_FINANCE+ "=true');";				
				sendToParent(javaScriptText);		
		}
		catch(Exception e)
		{
			logger.LogException( "[Exception occured in showPaymentDetails]", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event)
    {
    	try
    	{
    		DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
    		if(	
    			gridViewRow.getPersonId() > 0 && 
    			(
    				gridViewRow.getBeneficiaryCount() == 1 || 
					gridViewRow.getIsMultiple() == null ||	
					gridViewRow.getIsMultiple().compareTo( 0L )==0
    			)
    		  )
    		{
    		  sendToParent("javaScript:openBeneficiaryPopup("+ gridViewRow.getPersonId()+");");
    		}
    		else if( gridViewRow.getBeneficiaryCount() > 1 )
    		{
    		  openMultipleBeneficiaryPopup(gridViewRow, true);
    		}
    		else
    		{
    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_BENEFICIARY));
    		}
	   } catch (Exception ex) 
	   {
			logger.LogException("[Exception occured in openManageBeneficiaryPopUp()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	   }
	}
	private void openMultipleBeneficiaryPopup( DisbursementDetailsView disbursement,boolean readOnly )throws Exception
	{
		sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, disbursement);
		sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, disbursement.getFileId());
		if(readOnly)
		{
			sessionMap.put( WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
		}
		sendToParent( "javaScript:openAddMultipleBenefificiaryForDisbursementPopup();");
	}
	@SuppressWarnings("unchecked")
	public void onAddGRPSuppliers() 
	{
		errorMessages = new ArrayList<String>();
		try
		{
			
				List<Long> ids = new ArrayList<Long>();
				for(DisbursementDetailsView view:getDisbursementDetails())
				{
					ids.add(view.getDisDetId());
				}
				sessionMap.put( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS,ids); 
				String javaScriptText = " javaScript:openAddGrpSuppliersPopup( );";				
				sendToParent(javaScriptText);		
		}
		catch(Exception e)
		{
			logger.LogException( "onAddGRPSuppliers", e);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			if ( errorMessages != null && errorMessages.size()>0 )
			{
				viewMap.put( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS,errorMessages);
			}
			
		}
	}	
	
	public void loadPaymentDetails(DisbursementDetailsView view) throws Exception
	{
		PaymentCriteriaView criteriaView = view.getPaymentCriteriaView();
		List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
		for (DisbursementDetailsView dtView : disbursementDetails) 
		{			
			if ( dtView.getId()==view.getId() ) 
			{
				List<PaymentDetailsView> listPaymentDetailsView = dtView.getPaymentDetails();
				if( null==listPaymentDetailsView ) 
				{
					listPaymentDetailsView = new ArrayList<PaymentDetailsView>();
					dtView.setPaymentDetails( listPaymentDetailsView);
				}
				
				PaymentDetailsView paymentView = null;
				if( 0<listPaymentDetailsView.size() ) 
				{
					paymentView = listPaymentDetailsView.get( 0);
				}
				else 
				{
					paymentView = new PaymentDetailsView();
					paymentView.setDisDetId( view.getDisDetId());
					paymentView.setStatus( WebConstants.NOT_DISBURSED);
					paymentView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
				}
				
				paymentView.setAccountNo(criteriaView.getAccountNumber());
				
				if( view.getHasInstallment()!= null && view.getHasInstallment().equals( WebConstants.HAS_INSTALLMENT_DEAFULT)) 
				{
					paymentView.setAmount( dtView.getAmount());
				}
				
				paymentView.setBankId(criteriaView.getBankId());
				if (null != criteriaView.getPaymentMode()) 
				{
					paymentView.setPaymentMethod(new Long(criteriaView.getPaymentMode()));							
				}
				
				if( null!=criteriaView.getPaidTo()) 
				{
					paymentView.setPaidTo( new Long( criteriaView.getPaidTo()));
					paymentView.setOthers( null );
				}				
				else if ( criteriaView.getOther() != null && criteriaView.getOther().trim().length() >0 )
				{
					paymentView.setOthers(criteriaView.getOther().trim() );
					paymentView.setPaidTo( null );
				}
				paymentView.setRefDate(criteriaView.getDate());
				paymentView.setReferenceNo(criteriaView.getReferenceNumber());
				if( 0==listPaymentDetailsView.size() ) 
				{
					listPaymentDetailsView.add( paymentView);
				}
				
				successMessages.add(CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
																 						
									
			}
		}
	}
		
	private String sendToParent(String javaScript) {
		final String viewId = "/SearchAssets.jsp";	
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);	
		return "";
	}
	
	private DisbursementDetailsView getCopy(DisbursementDetailsView singleDetail) {
		DisbursementDetailsView financeDibursement = new DisbursementDetailsView();
		financeDibursement.setFileId( singleDetail.getFileId() );
		financeDibursement.setFileNumber(  singleDetail.getFileNumber() );
		financeDibursement.setId( financeDibursement.hashCode());
		financeDibursement.setRefNum( singleDetail.getRefNum());
		financeDibursement.setCreatedBy(singleDetail.getCreatedBy());
		financeDibursement.setCreatedOn( singleDetail.getCreatedOn());
		financeDibursement.setUpdatedBy( singleDetail.getUpdatedBy());
		financeDibursement.setUpdatedOn( singleDetail.getCreatedOn());
		financeDibursement.setDisbursedBy( singleDetail.getDisbursedBy());
		financeDibursement.setDisDetId( singleDetail.getDisDetId());														
		financeDibursement.setDisburseSrc( singleDetail.getDisburseSrc());
		financeDibursement.setDisburseSrcNameAr( singleDetail.getDisburseSrcNameAr());
		financeDibursement.setDisburseSrcNameEn( singleDetail.getDisburseSrcNameEn());															
		financeDibursement.setStatus( singleDetail.getStatus());
		financeDibursement.setAmount( singleDetail.getAmount());
		financeDibursement.setOriginalAmount( singleDetail.getOriginalAmount());
		financeDibursement.setIsDisbursed( singleDetail.getIsDisbursed());
		financeDibursement.setDescription( singleDetail.getDescription());
		financeDibursement.setSrcType( singleDetail.getSrcType());
		
		financeDibursement.setExternalPartyName( singleDetail.getExternalPartyName());
		financeDibursement.setPaidTo( singleDetail.getPaidTo());
		financeDibursement.setPaymentType( singleDetail.getPaymentType());
		financeDibursement.setIsMultiple( singleDetail.getIsMultiple());										
		financeDibursement.setVendorId( singleDetail.getVendorId());
		financeDibursement.setVendorName( singleDetail.getVendorName());														
		financeDibursement.setSrcTypeEn( singleDetail.getSrcTypeEn());
		financeDibursement.setSrcTypeAr( singleDetail.getSrcTypeAr());
		financeDibursement.setPaymentTypeEn( singleDetail.getPaymentTypeEn());
		financeDibursement.setPaymentTypeAr( singleDetail.getPaymentTypeAr());								
		financeDibursement.setStatusEn( singleDetail.getStatusEn());
		financeDibursement.setStatusAr( singleDetail.getStatusAr());						
		financeDibursement.setReasonId( singleDetail.getReasonId());
		financeDibursement.setItemId( singleDetail.getItemId());
		financeDibursement.setReasonAr( singleDetail.getReasonAr());
		financeDibursement.setReasonEn( singleDetail.getReasonEn());
		financeDibursement.setIsReviewReq( singleDetail.getIsReviewReq());									
		financeDibursement.setItemId( singleDetail.getItemId());
		financeDibursement.setItemAr( singleDetail.getItemAr());
		financeDibursement.setItemEn( singleDetail.getItemEn());										
		financeDibursement.setIsDeleted( singleDetail.getIsDeleted());
		financeDibursement.setRecordStatus( singleDetail.getRecordStatus());
		financeDibursement.setReqId( singleDetail.getReqId());
		financeDibursement.setHasInstallment( singleDetail.getHasInstallment());
		return financeDibursement;
	}
	
	private void handleCompletion() 
	{
		if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.CAN_COMPLETE)) 
		{
			viewMap.remove( WebConstants.FinanceTabPublishKeys.CAN_COMPLETE);
		}
		List<DisbursementDetailsView> list = getDisbursementDetails();
		for(DisbursementDetailsView singleDetail : list) 
		{
			List<PaymentDetailsView> listPaymentDetails = singleDetail.getPaymentDetails();
			if( null==listPaymentDetails ) 
			{
				return;
			}
			else if( null!=listPaymentDetails ) 
			{			
				if( 0==listPaymentDetails.size() ) 
				{
					return;
				}
				for(PaymentDetailsView paymentView : listPaymentDetails) 
				{
					if( paymentView.getStatus().equals( WebConstants.NOT_DISBURSED)) 
					{						
						return;
					}
				}
			}
		}
		viewMap.put(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE, "TRUE");
	}
	
	private RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}
	
	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}


	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}
	
	public Boolean getIsDisbursementRequired() {
		RequestView reqView = getRequestView();
		
		if( reqView.getStatusId().equals( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ_ID ) ) 
		{
			return true;
		}
		return false;		
	}
	
	public Boolean getIsStatusApproved() {
		RequestView reqView = getRequestView();
		Long approvedId = (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVED);
		if( reqView.getStatusId().equals(approvedId)) {
			return true;
		}
		return false;
	}
		
	public List<DisbursementDetailsView> getDisbursementDetails() {
		if( viewMap.containsKey( FINANCE_LIST)) {
			return (List<DisbursementDetailsView>) viewMap.get( FINANCE_LIST);
		}
		return new ArrayList<DisbursementDetailsView>();
	}
	
	public List<DisbursementDetailsView> getDisbDetails() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.DISBURSEMENTS)) {
			return (List<DisbursementDetailsView>) viewMap.get(WebConstants.FinanceTabPublishKeys.DISBURSEMENTS);
		}
		return new ArrayList<DisbursementDetailsView>();
	}


	public Boolean getRenderDisburse() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE)  
				//||
			//viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) == null		
		  )
		{
			return false;
		}
		return true;
	}
	
	public Boolean getRenderPaymentType() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_PAYMENT_TYPE)) {
			return false;
		}
		return true;		
	}
	public Boolean getRenderAmount() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_AMNT)) {
			return false;
		}
		return true;		
	}
	public Boolean getRenderSrcType() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_SRC_TYPE)) {
			return false;
		}
		return true;		
	}
	public Boolean getRenderReason() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_REASON)) {
			return false;
		}
		return true;		
	}
	public Boolean getRenderDescription() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_DESC)) {
			return false;
		}
		return true;
	}
	public Boolean getRenderItems() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_ITEMS)) {
			return false;
		}
		return true;
	}
	public Boolean getRenderPaidTo() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_PAID_TO)) {
			return false;
		}
		return true;
	}
	public Boolean getRenderDisSource() {
		if( viewMap.containsKey(WebConstants.FinanceTabPublishKeys.HIDE_DIS_SRC)) {
			return false;
		}
		return true;
	}		
	@SuppressWarnings("unchecked")
	public String getTotalDisbursementCount() {
		if( viewMap.get("financetotalDisbursementCount") != null )
		{
			 totalDisbursementCount = viewMap.get("financetotalDisbursementCount").toString();
		}
		return totalDisbursementCount;
	}

	@SuppressWarnings("unchecked")
	public void setTotalDisbursementCount(String totalDisbursementCount) {
		this.totalDisbursementCount = totalDisbursementCount;
		viewMap.put("financetotalDisbursementCount",this.totalDisbursementCount);
		
	}

	
}
