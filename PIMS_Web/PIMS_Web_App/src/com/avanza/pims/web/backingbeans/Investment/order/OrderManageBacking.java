package com.avanza.pims.web.backingbeans.Investment.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.OrderServiceAgent;
import com.avanza.pims.proxy.PIMSOrdersBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.OrderView;
import com.avanza.pims.ws.vo.PortfolioView;
import com.avanza.ui.util.ResourceUtil;

public class OrderManageBacking extends AbstractController
{	
	private static final long serialVersionUID = 3295714919720394711L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private OrderServiceAgent orderServiceAgent;
	
	private String pageMode;
	private OrderView orderView;
	
	private SystemParameters systemParameters;
	private UserTask userTask;
	
	private final String noteOwner = WebConstants.PROCEDURE_TYPE_MANAGE_ORDERS;
	private final String externalId = WebConstants.Attachment.EXTERNAL_ID_ORDER_MANAGE;
	private final String procedureTypeKey = WebConstants.PROCEDURE_TYPE_MANAGE_ORDERS;
	
	private HtmlInputText txtPercentage = new HtmlInputText();
	private HtmlInputText txtTotalAmount = new HtmlInputText();
	private HtmlSelectOneMenu selectInvestmentCostBasedOn = new HtmlSelectOneMenu();
	
	public OrderManageBacking() 
	{
		logger = Logger.getLogger( OrderManageBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();		
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		orderServiceAgent = new OrderServiceAgent();
		
		pageMode = WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW;
		orderView = new OrderView();
		
		
		systemParameters = SystemParameters.getInstance();
		userTask = new UserTask();
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			CommonUtil.loadAttachmentsAndComments(procedureTypeKey, externalId, noteOwner);
			
			super.init();
			
			DomainDataView ddvOrderStatusNew = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_STATUS), WebConstants.Order.ORDER_STATUS_NEW );			
			orderView.setOrderStatusId( String.valueOf( ddvOrderStatusNew.getDomainDataId() ) );
			orderView.setOrderStatusEn( ddvOrderStatusNew.getDataDescEn() );
			orderView.setOrderStatusAr( ddvOrderStatusNew.getDataDescAr() );
			
			DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);
			orderView.setOrderTypeId( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) );
			orderView.setOrderTypeEn( ddvOrderTypeBuying.getDataDescEn() );
			orderView.setOrderTypeAr( ddvOrderTypeBuying.getDataDescAr() );
			
			orderView.setTransactionDate( new Date() );
			
			updateValuesFromMaps();
			
			if(!isPostBack())
			{
				onInvestmentCalcMode();
				onInVestmentCostReadOnly();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// USER TASK		
		if ( sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			
			orderView = orderServiceAgent.getOrder( new Long( userTask.getTaskAttributes().get( WebConstants.Order.TASK_ATTRIBUTE_ORDER_ID ) ) );
			
			if ( userTask.getTaskAttributes().get( WebConstants.Order.TASK_ATTRIBUTE_TASK_TYPE ).equalsIgnoreCase( WebConstants.Order.TASK_TYPE_APPROVE_REJECT_ORDER ) )
				pageMode = WebConstants.Order.ORDER_MANAGE_PAGE_MODE_APPROVE_REJECT;
		}
		else if ( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		}
			
		
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}
		else if ( viewMap.get( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY );
			showHideAttachmentAndCommentsBtn();
		}
		
		
		// ORDER VIEW
		if ( sessionMap.get( WebConstants.Order.ORDER_VIEW ) != null )
		{
			orderView = (OrderView) sessionMap.get( WebConstants.Order.ORDER_VIEW );
			sessionMap.remove( WebConstants.Order.ORDER_VIEW );
			
			if(orderView.getOrderId()!=null && orderView.getOrderId().toString().trim().length()>0)
			CommonUtil.loadAttachmentsAndComments(orderView.getOrderId().toString());
		}
		else if ( viewMap.get( WebConstants.Order.ORDER_VIEW ) != null )
		{
			orderView = (OrderView) viewMap.get( WebConstants.Order.ORDER_VIEW );
			
			if(orderView.getOrderId()!=null && orderView.getOrderId().toString().trim().length()>0)
			CommonUtil.loadAttachmentsAndComments(orderView.getOrderId().toString());
		}
		
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// USER TASK
		if ( userTask != null )
		{
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		}
		
		
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_KEY, pageMode );
		}
		
		
		// ORDER VIEW
		if ( orderView != null )
		{
			viewMap.put( WebConstants.Order.ORDER_VIEW, orderView );
		}
	}
	
	public Boolean getIsAddMode()
	{
		Boolean isAddMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW ) )
			isAddMode = true;
		
		return isAddMode;
	}
	
	public Boolean getIsEditMode()
	{
		Boolean isEditMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT ) )
			isEditMode = true;
		
		return isEditMode;
	}
	
	public Boolean getIsViewMode()
	{
		Boolean isViewMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW ) )
			isViewMode = true;
		
		return isViewMode;
	}
	
	public Boolean getIsViewPopupMode()
	{
		Boolean isViewPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW_POPUP ) )
			isViewPopupMode = true;
		
		return isViewPopupMode;
	}
	
	public Boolean getIsAddPopupMode()
	{
		Boolean isNewPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW_POPUP ) )
			isNewPopupMode = true;
		
		return isNewPopupMode;
	}
	
	public Boolean getIsEditPopupMode()
	{
		Boolean isEditPopupMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT_POPUP ) )
			isEditPopupMode = true;
		
		return isEditPopupMode;
	}
	
	public Boolean getIsBuyingOrder()
	{
		Boolean isBuyingOrder = false;
		
		DomainDataView ddvOrderTypeBuying = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_BUYING);
		
		if ( orderView != null && orderView.getOrderTypeId() != null && 
				ddvOrderTypeBuying != null && ddvOrderTypeBuying.getDomainDataId() != null && 
				orderView.getOrderTypeId().equalsIgnoreCase( String.valueOf( ddvOrderTypeBuying.getDomainDataId() ) ) )
			isBuyingOrder = true;
		
		return isBuyingOrder;
	}
	
	public Boolean getIsSellingOrder()
	{
		Boolean isSellingOrder = false;
		
		DomainDataView ddvOrderTypeSelling = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType(WebConstants.Order.ORDER_TYPE), WebConstants.Order.ORDER_TYPE_SELLING);
		
		if ( orderView != null && orderView.getOrderTypeId() != null && 
				ddvOrderTypeSelling != null && ddvOrderTypeSelling.getDomainDataId() != null && 
				orderView.getOrderTypeId().equalsIgnoreCase( String.valueOf( ddvOrderTypeSelling.getDomainDataId() ) ) )
			isSellingOrder = true;		
		
		return isSellingOrder;
	}
	
	public Boolean getIsApproveRejectMode() 
	{	
		Boolean isApproveRejectMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_APPROVE_REJECT ) )
			isApproveRejectMode = true;
		
		return isApproveRejectMode;
	}
	
	public String onSave()
	{	
		String METHOD_NAME = "onSave()";
		String path = null;
		Boolean isSuccess = false;
		
		try	
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			
			  if(validate())	
			  {
						isSuccess = saveOrder( orderView );
						
						if( isSuccess )
						{
							pageMode = WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT;				
							successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_SAVE_SUCCESS ) );
							CommonUtil.loadAttachmentsAndComments(orderView.getOrderId().toString());     
							Boolean attachSuccess = CommonUtil.saveAttachments(orderView.getOrderId());
							Boolean commentSuccess = CommonUtil.saveComments(orderView.getOrderId(), noteOwner);
							updateValuesToMaps();
							logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
						}
			  }					
		}
		 catch (Exception exception) 
			{			
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_SAVE_ERROR ) );
				logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
			}
		
		
		return path;
	}
	
	private Boolean validate()
	
	{
		
		Boolean check = true;
		
		if(orderView.getTransactionDate()==null ||  orderView.getTransactionDate().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.transactionDate"));
		  check = false;
		}
		if(orderView.getTransactionTimeHH()==null ||  orderView.getTransactionTimeHH().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.hours"));
		  check = false;
		}
		if(orderView.getTransactionTimeMM()==null ||  orderView.getTransactionTimeMM().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.min"));
		  check = false;
		}
		if(orderView.getTransactionLogId()==null ||  orderView.getTransactionLogId().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.log"));
		  check = false;
		}
		if(orderView.getInvestmentCostBasedOn()==null ||  orderView.getInvestmentCostBasedOn().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.CostBasedOn"));
		  check = false;
		}
		if(orderView.getCurrentValueNav()==null ||  !(orderView.getCurrentValueNav().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.ValueOfNAV"));
		  check = false;
		}
		  try{
				
				if(orderView.getCurrentValueNav()!=null&& orderView.getCurrentValueNav().toString().trim().length()>0)
				Double.parseDouble(orderView.getCurrentValueNav().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectValueOfNAV"));

				}
		if(orderView.getTotalAmountInvestReturn()==null ||  !(orderView.getTotalAmountInvestReturn().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.amountReturn"));
		  check = false;
		}
		  try{
				
				if(orderView.getTotalAmountInvestReturn()!=null && orderView.getTotalAmountInvestReturn().toString().trim().length()>0)
				Double.parseDouble(orderView.getTotalAmountInvestReturn().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectAmountToReturn"));

				}
		if(orderView.getTotalUnitsBoughtSold()==null || !( orderView.getTotalUnitsBoughtSold().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.unitSold"));
		  check = false;
		}
		
		  try{
				
				if(orderView.getTotalUnitsBoughtSold()!=null && orderView.getTotalUnitsBoughtSold().toString().trim().length()>0)
				Long.parseLong(orderView.getTotalUnitsBoughtSold().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectTotalUnitSold"));

				}
				
		if(orderView.getTotalUnits()==null || !(orderView.getTotalUnits().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.remainingUnits"));
		  check = false;
		}
		  try{
				
				if(orderView.getTotalUnits()!=null && orderView.getTotalUnits().toString().trim().length()>0)
				Long.parseLong(orderView.getTotalUnits().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectTotalRemainingUnit"));

				}
		if(orderView.getTotalAmount()==null   || !(orderView.getTotalAmount().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.remainingAmount"));
		  check = false;
		}
		  try{
				
				if(orderView.getTotalAmount()!=null && orderView.getTotalAmount().toString().trim().length()>0)
				Double.parseDouble(orderView.getTotalAmount().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectTotalRemainingAmount"));

				}
		if(orderView.getBudgetRateReturn()==null || !(orderView.getBudgetRateReturn().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.rateOfreturn"));
		  check = false;
		}
		  try{
				
				if(orderView.getBudgetRateReturn()!=null && orderView.getBudgetRateReturn().toString().trim().length()>0)
				Double.parseDouble(orderView.getBudgetRateReturn().toString());

				}
				catch (NumberFormatException e) {
					check = false;
					errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectBudgetRateOfReturn"));

				}
		if(orderView.getAdditionalExpenses()==null || !(orderView.getAdditionalExpenses().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.additionalExp"));
		  check = false;
		}
		try{
			
			if(orderView.getAdditionalExpenses()!=null && orderView.getAdditionalExpenses().toString().trim().length()>0)
			Double.parseDouble(orderView.getAdditionalExpenses().toString());

			}
			catch (NumberFormatException e) {
				check = false;
				errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectAdditionalExpenses"));

			}
		if(orderView.getDescription()==null &&  orderView.getDescription().equals(""))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.description"));
		  check = false;
		}
	if(getIsSellingOrder())
	{	
		if(orderView.getActualReturn()==null || !(orderView.getActualReturn().toString().trim().length()>0))
		{
		  errorMessages.add(CommonUtil.getBundleMessage("orderManage.ActualReturn"));
		  check = false;
		}
		try{
			
			if(orderView.getActualReturn()!=null && orderView.getActualReturn().toString().trim().length()>0)
			Double.parseDouble(orderView.getActualReturn().toString());

			}
			catch (NumberFormatException e) {
				check = false;
				errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectActualReturn"));

			}
	}
	
	 if(pageMode !=null && !pageMode.equals(WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW_POPUP)
			            && !pageMode.equals(WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW_POPUP))
	 {
		if(orderView.getPortfolioView() ==null || orderView.getPortfolioView().getPortfolioNumber() ==null 
				                               || !(orderView.getPortfolioView().getPortfolioNumber().trim().length()>0))
		{
			  errorMessages.add(CommonUtil.getBundleMessage("orderManage.Propfolio.msg"));
			  check = false;
		}
	 }		
	 
	 	 try{
			
	 		if(orderView.getDaysToCompleteOrder()!=null && orderView.getDaysToCompleteOrder().toString().trim().length()>0)
	 			Long.parseLong(orderView.getDaysToCompleteOrder().toString());

			}
			catch (NumberFormatException e) 
			{
				check = false;
				errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectDaysToFullfilOrder"));
			}
			
		 try{
			if(orderView.getPercentage()!=null && orderView.getPercentage().toString().trim().length()>0)
			Double.parseDouble(orderView.getPercentage().toString());

			}
			catch (NumberFormatException e) {
			check = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectPercentage"));

			}
			
		return check;
		
	}
	
	private Boolean saveOrder(OrderView orderView) throws PimsBusinessException
	{
		Boolean isSuccess = false;
		
		if ( orderView.getOrderId() == null )
		{
			// Add Order
			orderView.setCreatedBy( CommonUtil.getLoggedInUser() );
			orderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = orderServiceAgent.addOrder( orderView );
		}
		else
		{
			// Update Order
			orderView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = orderServiceAgent.updateOrder( orderView );
		}
		
		return isSuccess;
	}

	public String onCancel()
	{
		return "orderSearch";
	}
	
	public String onSend()
	{
		String METHOD_NAME = "onSend()";
		String path = null;
		
		try	
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			PIMSOrdersBPELPortClient portClient = new PIMSOrdersBPELPortClient();
			portClient.setEndpoint( systemParameters.getParameter( WebConstants.Order.MANAGE_ORDER_BPEL_END_POINT ) );
			portClient.initiate( orderView.getOrderId(), CommonUtil.getLoggedInUser(), null, null );
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_REQUEST_SENT_SUCCESS ) );
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskApprove()
	{
		String METHOD_NAME = "onTaskApprove()";
		String path = null;		
		
		try	
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectTask(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_REQUEST_APPROVE_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskReject()
	{
		String METHOD_NAME = "onTaskReject()";
		String path = null;
		
		try	
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");			
			String soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
			approveRejectTask(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
			onAfterBPEL();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.Order.ORDER_REQUEST_REJECT_SUCCESS ) );			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onTaskCancel() 
	{	
		return "taskList";
	}
	
	private void approveRejectTask(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception 
	{
		String METHOD_NAME = "approveRejectTask()";		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");				
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
	}
	
	private void onAfterBPEL() throws PimsBusinessException
	{
		orderView = orderServiceAgent.getOrder( orderView.getOrderId() );
		pageMode = WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW;
		updateValuesToMaps();
	}
	
	public String onPassOrderViewToOpener()
	{
		if(validate())
		{
		sessionMap.put( WebConstants.Order.ORDER_VIEW, orderView );
		executeJavascript("passOrderViewToOpener();");
		}
		return null;		
	}
	
	public String onSearchPortfolio()
	{
		sessionMap.put( WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_KEY, WebConstants.Portfolio.PORTFOLIO_SEARCH_PAGE_MODE_SINGLE_SELECT_POPUP );
		executeJavascript("openPortfolioSearchPopup();");
		return null;
	}
	
	public String onReceiveSelectedPortfolio()
	{
		PortfolioView selectedPortfolio = (PortfolioView) sessionMap.get( WebConstants.Portfolio.PORTFOLIO_VIEW );
		orderView.setPortfolioView( selectedPortfolio );		
		updateValuesToMaps();
		return null;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public void showHideAttachmentAndCommentsBtn()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT ) )
			canAddAttachmentsAndComments(true);
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_APPROVE_REJECT ) )
			canAddAttachmentsAndComments(false);
		if ( pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_VIEW ) )
			canAddAttachmentsAndComments(false);
	}
	
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public String onInvestmentCostBasedOn()
	{
		DomainDataView ddvInvestmentCostBasedOnPercentage = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.INVESTMENT_COST_BASED_ON), WebConstants.INVESTMENT_COST_BASED_ON_PERCENTAGE);
		DomainDataView ddvInvestmentCostBasedOnUnit = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.INVESTMENT_COST_BASED_ON), WebConstants.INVESTMENT_COST_BASED_ON_UNIT);
		
		orderView.setTotalAmount(null);
		orderView.setPercentage(null);
		
		if( orderView.getInvestmentCostBasedOn()==null || orderView.getInvestmentCostBasedOn().equals("") )
		{
			viewMap.put("READ_ONLY_TOTAL_AMOUNT", "READONLY" );
			viewMap.put("READ_ONLY_PERCENTAGE", "READONLY" );
			txtPercentage.setReadonly(true);
			txtTotalAmount.setReadonly(true);
		}
		if( ddvInvestmentCostBasedOnPercentage!=null && ddvInvestmentCostBasedOnPercentage.getDomainDataId().toString().equals(orderView.getInvestmentCostBasedOn()) )
		{
			viewMap.put("READ_ONLY_TOTAL_AMOUNT", "READONLY" );
			viewMap.put("READ_ONLY_PERCENTAGE", "" );
			txtPercentage.setReadonly(false);
			txtTotalAmount.setReadonly(true);
		}
		if( ddvInvestmentCostBasedOnUnit!=null && ddvInvestmentCostBasedOnUnit.getDomainDataId().toString().equals(orderView.getInvestmentCostBasedOn()) )
		{
			viewMap.put("READ_ONLY_TOTAL_AMOUNT", "" );
			viewMap.put("READ_ONLY_PERCENTAGE", "READONLY" );
			txtPercentage.setReadonly(true);
			txtTotalAmount.setReadonly(false);
		}
		
		return "";
	}
	
	public String onInvestmentCalcMode()
	{
		DomainDataView ddvInvestmentCostBasedOnPercentage = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.INVESTMENT_COST_BASED_ON), WebConstants.INVESTMENT_COST_BASED_ON_PERCENTAGE);
		DomainDataView ddvInvestmentCostBasedOnUnit = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.INVESTMENT_COST_BASED_ON), WebConstants.INVESTMENT_COST_BASED_ON_UNIT);
		
	   if ( !pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW ) )
	   {   
			if( ddvInvestmentCostBasedOnPercentage!=null && ddvInvestmentCostBasedOnPercentage.getDomainDataId().toString().equals(orderView.getInvestmentCostBasedOn()) )
			{
				viewMap.put("READ_ONLY_TOTAL_AMOUNT", "READONLY" );
				viewMap.put("READ_ONLY_PERCENTAGE", "" );
				txtPercentage.setReadonly(false);
				txtTotalAmount.setReadonly(true);
			}
			if( ddvInvestmentCostBasedOnUnit!=null && ddvInvestmentCostBasedOnUnit.getDomainDataId().toString().equals(orderView.getInvestmentCostBasedOn()) )
			{
				viewMap.put("READ_ONLY_TOTAL_AMOUNT", "" );
				viewMap.put("READ_ONLY_PERCENTAGE", "READONLY" );
				txtPercentage.setReadonly(true);
				txtTotalAmount.setReadonly(false);
			}
	   }
	   else
	   {
			viewMap.put("READ_ONLY_TOTAL_AMOUNT", "READONLY" );
			viewMap.put("READ_ONLY_PERCENTAGE", "READONLY" );
			txtPercentage.setReadonly(true);
			txtTotalAmount.setReadonly(true);
	   }
		
		return "";
	}
	
	public String onInVestmentCostReadOnly()
	{
		if ( !(pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_NEW )
				|| pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT )
				|| pageMode.equalsIgnoreCase( WebConstants.Order.ORDER_MANAGE_PAGE_MODE_EDIT_POPUP )) )
		{
		selectInvestmentCostBasedOn.setReadonly(true);
		viewMap.put("READ_ONLY_COST_BASED_ON", "READONLY" );
		
		}
		
		return "";
	}
	
	
	public String onCalcTotalAmount()
	{
		Double totalAmount = null ;
		
		if(validateCalField())
		if(( orderView.getPercentage()!=null && !orderView.getPercentage().equals("") ) 
			&& ( orderView.getTotalAmountInvestReturn()!=null && !orderView.getTotalAmountInvestReturn().equals("") )	)
		{
			totalAmount =  ( Double.parseDouble(orderView.getPercentage())/100 );
			totalAmount = ( totalAmount * Double.parseDouble(orderView.getTotalAmountInvestReturn()) );
			
			orderView.setTotalAmount( String.valueOf(totalAmount) );
		}
		return "";
	}
	
	public String getReadOnlyPercentage()
	{
		String readOnlyPercentage = "";
		
		if( viewMap.get("READ_ONLY_PERCENTAGE")!=null && !(viewMap.get("READ_ONLY_PERCENTAGE").toString().equals("")) )
			readOnlyPercentage =  viewMap.get("READ_ONLY_PERCENTAGE").toString();
		
		return readOnlyPercentage;
		
	}
	
	public String getReadOnlyTotalAmount()
	{
		String readOnlyTotalAmount = "";
		
		if( viewMap.get("READ_ONLY_TOTAL_AMOUNT")!=null && !(viewMap.get("READ_ONLY_TOTAL_AMOUNT").toString().equals("")) )
			readOnlyTotalAmount =  viewMap.get("READ_ONLY_TOTAL_AMOUNT").toString(); 
			
		return readOnlyTotalAmount;
	}
	public String getReadOnlyCostBasedOn()
	{
		String readOnlyCostBasedOn = "";
		
		if( viewMap.get("READ_ONLY_COST_BASED_ON")!=null && !(viewMap.get("READ_ONLY_COST_BASED_ON").toString().equals("")) )
			readOnlyCostBasedOn =  viewMap.get("READ_ONLY_COST_BASED_ON").toString();
		
		return readOnlyCostBasedOn;
		
	}
	
	public Boolean validateCalField()
	{
		Boolean check = true;
		
		 try{
			if(orderView.getTotalAmount()!=null && orderView.getTotalAmount().toString().trim().length()>0)
			Double.parseDouble(orderView.getTotalAmount().toString());

			}
			catch (NumberFormatException e) {
				check = false;
				errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectTotalRemainingAmount"));

			}
		 try{
			if(orderView.getPercentage()!=null && orderView.getPercentage().toString().trim().length()>0)
			Double.parseDouble(orderView.getPercentage().toString());

			}
			catch (NumberFormatException e) {
			check = false;
			errorMessages.add(ResourceUtil.getInstance().getProperty("orderManage.CorrectPercentage"));

			}	
	
		return check;
	}
	
	public String getLocale() {
		return CommonUtil.getLocale();
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);		
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {		
		return CommonUtil.getErrorMessages(successMessages);
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public OrderServiceAgent getOrderServiceAgent() {
		return orderServiceAgent;
	}

	public void setOrderServiceAgent(OrderServiceAgent orderServiceAgent) {
		this.orderServiceAgent = orderServiceAgent;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public OrderView getOrderView() {
		return orderView;
	}

	public void setOrderView(OrderView orderView) {
		this.orderView = orderView;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SystemParameters getSystemParameters() {
		return systemParameters;
	}

	public void setSystemParameters(SystemParameters systemParameters) {
		this.systemParameters = systemParameters;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public HtmlInputText getTxtPercentage() {
		return txtPercentage;
	}

	public void setTxtPercentage(HtmlInputText txtPercentage) {
		this.txtPercentage = txtPercentage;
	}

	public HtmlInputText getTxtTotalAmount() {
		return txtTotalAmount;
	}

	public void setTxtTotalAmount(HtmlInputText txtTotalAmount) {
		this.txtTotalAmount = txtTotalAmount;
	}

	public HtmlSelectOneMenu getSelectInvestmentCostBasedOn() {
		return selectInvestmentCostBasedOn;
	}

	public void setSelectInvestmentCostBasedOn(
			HtmlSelectOneMenu selectInvestmentCostBasedOn) {
		this.selectInvestmentCostBasedOn = selectInvestmentCostBasedOn;
	}	
}
