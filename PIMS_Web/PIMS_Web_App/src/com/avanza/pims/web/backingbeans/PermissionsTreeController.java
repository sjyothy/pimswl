package com.avanza.pims.web.backingbeans;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputHidden;

import org.apache.myfaces.custom.tree2.TreeNode;
import org.apache.myfaces.custom.tree2.TreeNodeBase;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.jsf.appbase.dao.security.GroupManager;
import com.avanza.core.security.Permission;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.PermissionType;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupPermissionBinding;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.JSFUtil;
import com.avanza.ui.util.ResourceUtil;




/**
 * @author Nauman Bashir
 * @version 1.0
 */
public class PermissionsTreeController extends AbstractController{
    
    /**
     * 
     */
    
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> infoMessages = new ArrayList<String>();
	private transient Logger logger = Logger.getLogger(PermissionsTreeController.class);
    private static final long serialVersionUID = 1L;
    
    Search search;
    
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false; 
    private List<String> permissionList;
    
    private boolean checkAdd;
    private TreeNode permissionsTree ;    

    private PermissionBinding permissionBinding;

    private GroupManager groupManager = new GroupManager();
    private HtmlCommandButton cmdSavePerm;  
    Map viewRootMap;


    public PermissionsTreeController() {
   
       
    }
    
    public void init()
    {
        // userGroupId = "Admin Group";
    	super.init();
       
    	viewRootMap= getFacesContext().getViewRoot().getAttributes();
    	
        if(!this.isPostBack())
        {
        	String userGroupId = JSFUtil.getRequestParamAsString("View_Permission");       
        	UserGroupDbImpl currentUserGroup = groupManager.findById( UserGroupDbImpl.class, userGroupId.toString() ) ;
        	setCurrentGroup(currentUserGroup);
        }
        
        try 
        {               
                      
           UserGroupDbImpl currentUserGroup = getCurrentGroup();
           PermissionBinding permBind = groupManager.PrepareBindings();
            
            if (currentUserGroup.getPermissions().size() > 0)
                copyListValues(permBind, currentUserGroup.getPermissions());
            
            setPermissionsTree(permBind.getPermissions(), null);
            
        } catch (Exception e) 
        {
        	logger.LogException("Failed to init page", e);
        }
            
    }   
    
    public UserGroupDbImpl getCurrentGroup()
    {
    	UserGroupDbImpl userGroupDbImpl = null;
    	
    	if(viewRootMap.containsKey("CURRENT_USER_GROUP"))
    	{
    		userGroupDbImpl = (UserGroupDbImpl) viewRootMap.get("CURRENT_USER_GROUP");
    	}
    	
    	return userGroupDbImpl;
    }
    public void setCurrentGroup(UserGroupDbImpl userGroupDbImpl)
    {
    	viewRootMap.put("CURRENT_USER_GROUP", userGroupDbImpl );
    }
    
    public void preprocess()
    {        
    }
    
    public String cancel() 
	{
    	return "cancel";
	}
    
    
    public String getErrorMessages() {
        String messageList;
        if ((errorMessages == null) || (errorMessages.size() == 0)) {
            messageList = "";
        }
        else {
            messageList = "<FONT COLOR=RED><B><UL>\n";
            for (String message : errorMessages) {
                messageList = messageList + "<LI>" + message + "\n";
            }
            messageList = messageList + "</UL></B></FONT>\n";
        }
        return (messageList);
    }
    
    private void MakePermissionsList(List list, List<PermissionBinding> list2, Boolean add) 
    {
    	UserGroupDbImpl currentUserGroup = getCurrentGroup();
        Permissions perm;
        
        for (Object obj : list) {
            perm = (Permissions) obj;
            PermissionBinding permissionBinding = perm.permissionBinding;
            if (permissionBinding.getType() == PermissionType.None) {
                if (getCanViewPermission(perm))
                    list2.add(permissionBinding);
                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
                    currentUserGroup.removePermission(permissionBinding);
                
                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
            }
            else if (permissionBinding.getType() == PermissionType.Module) {
                if (getCanViewPermission(perm))
                    list2.add(permissionBinding);
                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
                    currentUserGroup.removePermission(permissionBinding);
                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
            }
           // else if (permissionBinding.getType() == PermissionType.Action) {
           //     if (getCanViewPermission(perm))
           //         list2.add(permissionBinding);
           //     else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
            //        currentUserGroup.removePermission(permissionBinding);
                
           // }
            else if (permissionBinding.getType() == PermissionType.Transaction) {
                if (getCanViewPermission(perm))
                    list2.add(permissionBinding);
                else if (!add && isAddedPermission(permissionBinding.getPermissionId()))
                    currentUserGroup.removePermission(permissionBinding);
                MakePermissionsList(((TreeNode) obj).getChildren(), list2, add);
            }
            
        }
        
    }
    
    private Boolean isAddedPermission(String permissionId) 
    {
    	UserGroupDbImpl currentUserGroup = getCurrentGroup();
        Iterator<GroupPermissionBinding> iterator = currentUserGroup.getGroupPermissionBindings().iterator();
        while (iterator.hasNext()) {
            GroupPermissionBinding groupPermissionBinding =  iterator.next();
            if (groupPermissionBinding.getPermissionId().equals(permissionId)) { return true; }
        }
        return false;
    }
    
    private Boolean getCanViewPermission(Permissions permissions) {
        if (permissions.isCanCreate() || permissions.isCanDelete() || permissions.isCanUpdate() || permissions.isCanView())
            return true;
        else
            return false;
    }
    
  
    private void copyListValues(PermissionBinding permBind, List<PermissionBinding> specificList) {
        specificList.get(0).selectBindings(permBind);
    }
    
  

    public void setCmdSavePerm(HtmlCommandButton commandButton1) {
        this.cmdSavePerm = commandButton1;
    }

    public HtmlCommandButton getCmdSavePerm() {
        return cmdSavePerm;
    }

    public String cmdSavePerm_action() 
    {
    	infoMessages.clear();
    	errorMessages.clear();
    	try
    	{
	    	UserGroupDbImpl currentUserGroup = getCurrentGroup();
	        System.out.println("On Permission");
	        List<PermissionBinding> permissionList = new ArrayList<PermissionBinding>();
	        List list = new ArrayList();
	        list.add(permissionsTree);	        
	        MakePermissionsList(list, permissionList, false);
	        currentUserGroup.addPermission(permissionList);	      
	        groupManager.updatePermissions(currentUserGroup);
	        infoMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.UserGroup.GROUP_PERMISSIONS_SAVED_SUCCESS));
        
    	}
    	catch(Exception exp)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException("Failed to save permission due to : ", exp);
    	}
        return "";
        
    }

    
    
    /*
     * public void setTreeData(List<PermissionBinding> list, DefaultMutableTreeNode treeNodeBase) { for (PermissionBinding root : list) { if
     * (root.getType() == PermissionType.None) { treeData = new DefaultMutableTreeNode(new Permissions(root .getPermissionId(), root,
     * root.getCanCreate(), root .getCanUpdate(), root.getCanDelete(), "foo-folder")); moduleId.add(root.getCanCreate());
     * setTreeData(root.getPermissions(), null); } else if (root.getType() == PermissionType.Module) { DefaultMutableTreeNode personNode = new
     * DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(), root.getCanUpdate(), root .getCanDelete(),
     * "person")); moduleId.add(root.getCanCreate()); setTreeData(root.getPermissions(), personNode); if (treeNodeBase != null)
     * treeNodeBase.insert(personNode); else treeData.insert(personNode); } else if (root.getType() == PermissionType.Transaction) {
     * DefaultMutableTreeNode folderNode = new DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(),
     * root.getCanUpdate(), root .getCanDelete(), "bar-folder")); moduleId.add(root.getCanCreate()); setTreeData(root.getPermissions(), folderNode);
     * if (treeNodeBase != null) treeNodeBase.insert(folderNode); } else if (root.getType() == PermissionType.Action) { DefaultMutableTreeNode
     * documentNode = new DefaultMutableTreeNode( new Permissions(root.getPermissionId(), root, root .getCanCreate(), root.getCanUpdate(), root
     * .getCanDelete(), "document")); moduleId.add(root.getCanCreate()); if (treeNodeBase != null) treeNodeBase.insert(documentNode); } } }
     */

    /**
     * NOTE: This is just to show an alternative way of supplying tree data. You can supply either a TreeModel or TreeNode.
     * 
     * @return TreeModel
     */
    /*
     * public org.apache.myfaces.custom.tree2.TreeModel getExpandedTreeData() { return new TreeModelBase( ((org.apache.myfaces.custom.tree2.TreeNode)
     * getTreeDate1())); }
     */

   /**
    * @author Nauman Bashir
    */
    public class Permissions extends TreeNodeBase implements Serializable {
        
        private String permissionName;
        
        private PermissionBinding permissionBinding;
        
        private boolean canCreate;
        
        private boolean linkCanCreate;
        
        private boolean canUpdate;
        
        private boolean linkCanUpdate;
        
        private boolean canDelete;
        
        private boolean linkCanDelete;
        
        private boolean canView;
        
        public boolean isLinkCanCreate() {
            return permissionBinding.getLinkPermission().getCanCreate();
        }
        
        public boolean isLinkCanDelete() {
            return permissionBinding.getLinkPermission().getCanDelete();
        }
        
        public boolean isLinkCanUpdate() {
            return permissionBinding.getLinkPermission().getCanUpdate();
        }
        
        

        public PermissionBinding getPermissionBinding() {
            return permissionBinding;
        }
        
        public Permissions(String permissionName, PermissionBinding permissionBinding, boolean canCreate, boolean canUpdate, boolean canDelete,
                boolean canView, String node) {
            super(node, permissionName, false);
            this.permissionName = permissionName;
            this.permissionBinding = permissionBinding;
            
            this.canCreate = canCreate;
            this.canUpdate = canUpdate;
            this.canDelete = canDelete;
            this.canView = canView;
            
        }
        
        public String getPermissionName() {
            return permissionName;
        }
        
        public void setPermissionName(String permissionName) {
            this.permissionName = permissionName;
        }
        
        public boolean isCanCreate() {
            return canCreate;
        }
        
        public void setCanCreate(boolean canCreate) {
            permissionBinding.changeCreate(canCreate);
            this.canCreate = canCreate;
        }
        
        public boolean isCanDelete() {
            return canDelete;
        }
        
        public void setCanDelete(boolean canDelete) {
            permissionBinding.changeDelete(canDelete);
            this.canDelete = canDelete;
        }
        
        public boolean isCanUpdate() {
            return canUpdate;
        }
        
        public void setCanUpdate(boolean canUpdate) {
            permissionBinding.changeUpdate(canUpdate);
            this.canUpdate = canUpdate;
        }
        
        public boolean isCanView() {
            return canView;
        }
        
        public void setCanView(boolean canView) {
            this.canView = canView;
        }
        
    }
   
    public org.apache.myfaces.custom.tree2.TreeNode getPermissionsTree() {
        
        return permissionsTree;
    }
    
    public void setPermissionsTree(List<PermissionBinding> list, TreeNodeBase treeNodeBase) {
       
    	UserGroupDbImpl currentUserGroup = getCurrentGroup();
        for (PermissionBinding root : list) {
            if (root.getType() == PermissionType.None) {
                permissionsTree = new Permissions(getLocalWisePermissionName(root.getLinkPermission()), root, root.getCanCreate(), root
                        .getCanUpdate(), root.getCanDelete(), checkPermissionGroupRelation(root, currentUserGroup), "None");
                setPermissionsTree(root.getPermissions(), null);
            }else if (root.getType() == PermissionType.Module) {
                TreeNodeBase personNode = new Permissions(getLocalWisePermissionName(root.getLinkPermission()), root, root.getCanCreate(), root
                        .getCanUpdate(), root.getCanDelete(), checkPermissionGroupRelation(root, currentUserGroup), "Module");
                setPermissionsTree(root.getPermissions(), personNode);
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(personNode);
                else
                    permissionsTree.getChildren().add(personNode);
                
            }else if (root.getType() == PermissionType.Transaction) {
                TreeNodeBase folderNode = new Permissions(getLocalWisePermissionName(root.getLinkPermission()), root, root.getCanCreate(), root
                        .getCanUpdate(), root.getCanDelete(), checkPermissionGroupRelation(root, currentUserGroup), "Transaction");
                setPermissionsTree(root.getPermissions(), folderNode);
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(folderNode);
                
            }else if (root.getType() == PermissionType.Action) {
                TreeNodeBase documentNode = new Permissions(getLocalWisePermissionName(root.getLinkPermission()), root, root.getCanCreate(), root
                        .getCanUpdate(), root.getCanDelete(), checkPermissionGroupRelation(root, currentUserGroup), "Action");
                if (treeNodeBase != null)
                    treeNodeBase.getChildren().add(documentNode);
                
            }
            
        }
        
    }
    
    private Boolean checkPermissionGroupRelation(PermissionBinding permissionBinding, UserGroupDbImpl userGroup) {
        
        if(userGroup==null)
            return  false;
        
        for (GroupPermissionBinding groupPermissionBinding : userGroup.getGroupPermissionBindings()) {
            if (groupPermissionBinding.getPermissionId().equals(permissionBinding.getPermissionId())) { return true; }
        }
        return false;
    }
        
    
	public boolean getIsArabicLocale() {
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
    
    private String getLocalWisePermissionName(Permission permission) {
        
        if (getIsEnglishLocale()) {
        	return permission.getPrimaryName();
        }
        
        else {
            return permission.getSecondaryName();
        }
    }
    
    private String getLocalWiseGroupName(UserGroup userGroup) {
        
        if (getIsEnglishLocale()) {
            return userGroup.getPrimaryName();
        }
        else {
            return userGroup.getSecondaryName();
        }
    }
    
    public boolean isCheckAdd() {
        return checkAdd;
    }
    
    public void setCheckAdd(boolean checkAdd) {
        this.checkAdd = checkAdd;
    }
    
    public boolean getCheckAdd() {
        return this.checkAdd;
    }
    
    public List getPermissionList() {
        return permissionList;
    }
    
    public void setPermissionList(List permissionList) {
        this.permissionList = permissionList;
    }
    
    public Boolean getCanCreate() {
        return this.permissionBinding.getCanCreate();
    }
    
    public Boolean getCanDelete() {
        return this.permissionBinding.getCanDelete();
    }
    
    public Boolean getCanUpdate() {
        return this.permissionBinding.getCanUpdate();
    }

	public List<String> getInfoMessages() {
		return infoMessages;
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
    
   
    
}
