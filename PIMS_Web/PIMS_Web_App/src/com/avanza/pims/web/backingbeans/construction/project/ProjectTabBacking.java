package com.avanza.pims.web.backingbeans.construction.project;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.StudyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ProjectDetailsView;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.pims.ws.vo.RelatedStudiesView;
import com.avanza.pims.ws.vo.StudyDetailView;


public class ProjectTabBacking extends AbstractController
{
	private transient Logger logger = Logger.getLogger(ProjectTabBacking.class);

	private ProjectDetailsView projectDetailsTabView = new ProjectDetailsView();
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	private HtmlSelectOneMenu projectTypeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu projectPurposeCombo = new HtmlSelectOneMenu();
	private boolean showRecievePropertyImage=false;
	private String pageMode="default";
	private List<DomainDataView> propertyTypeList=new ArrayList<DomainDataView>(0);
	private DomainDataView propertyTypeLand=new DomainDataView(); 
	@Override 
	 public void init() {
		logger.logInfo("init() started...");
		super.init();
		try
		{
//			populateFromView();
//			if(viewMap.get("requestId")!=null)
//				if(viewMap.get("evaluationPurposeId")!=null)
//					evaluationCombo.setValue(viewMap.get("evaluationPurposeId").toString());
			logger.logInfo("init() completed successfully...");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed...", exception);
		}
	}
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 	}

	public Boolean getReadonlyMode(){
		Boolean projectDetailsReadonlyMode = (Boolean)viewMap.get("projectDetailsReadonlyMode");
		if(projectDetailsReadonlyMode==null)
			projectDetailsReadonlyMode = false;
		return projectDetailsReadonlyMode;
	}

	public HtmlSelectOneMenu getProjectPurposeCombo() {
		return projectPurposeCombo;
	}

	public void setProjectPurposeCombo(HtmlSelectOneMenu projectPurposeCombo) {
		this.projectPurposeCombo = projectPurposeCombo;
	}

	public String getProjectNumber() {
		String temp = (String)viewMap.get("projectNumber");
		return temp;
	}

	public void setProjectNumber(String projectNumber) {
		if(StringHelper.isNotEmpty(projectNumber))
			viewMap.put("projectNumber" , projectNumber);
	}

	public String getProjectStatus() {
		String temp = (String)viewMap.get("projectStatus");
		return temp;
	}

	public void setProjectStatus(String projectStatus) {
		if(projectStatus!=null)
			viewMap.put("projectStatus" , projectStatus);
	}
	
	public String getProjectStatusId() {
		String temp = (String)viewMap.get("projectStatusId");
		return temp;
	}

	public void setProjectStatusId(String projectStatusId) {
		if(projectStatusId!=null)
			viewMap.put("projectStatusId" , projectStatusId);
	}

	public String getRelatedStudy() {
		String temp = (String)viewMap.get("relatedStudy");
		return temp;
	}

	public void setRelatedStudy(String relatedStudy) {
		if(relatedStudy!=null)
			viewMap.put("relatedStudy" , relatedStudy);
	}
	
	public String getRelatedStudyId() {
		String temp = (String)viewMap.get("relatedStudyId");
		return temp;
	}

	public void setRelatedStudyId(String relatedStudyId) {
		if(relatedStudyId!=null)
			viewMap.put("relatedStudyId" , relatedStudyId);
	}

	public String getStudyDescription() {
		String temp = (String)viewMap.get("studyDescription");
		return temp;
	}

	public void setStudyDescription(String studyDescription) {
		if(studyDescription!=null)
			viewMap.put("studyDescription" , studyDescription);
	}
	
	public String getProjectName() {
		String temp = (String)viewMap.get("projectName");
		return temp;
	}
	public void setProjectName(String projectName) {
		if(projectName!=null)
			viewMap.put("projectName" , projectName);
	}
	public String getProjectTypeId() {
		String temp = (String)viewMap.get("projectTypeId");
		return temp==null?"":temp;
	}
	public void setProjectTypeId(String projectTypeId) {
		if(projectTypeId!=null)
			viewMap.put("projectTypeId" , projectTypeId);
	}
	public String getProjectType() {
		String temp = (String)viewMap.get("projectType");
		return temp;
	}
	public void setProjectType(String projectType) {
		if(projectType!=null)
			viewMap.put("projectType" , projectType);
	}
	public String getPropertyType() {
		String temp = (String)viewMap.get("propertyType");
		return temp;
	}
	public void setPropertyType(String propertyType) {
		if(propertyType!=null)
			viewMap.put("propertyType" , propertyType);
	}
	public String getPropertyTypeId() {
		String temp = (String)viewMap.get("propertyTypeId");
		return temp;
	}
	public void setPropertyTypeId(String propertyTypeId) {
		if(propertyTypeId!=null)
			viewMap.put("propertyTypeId" , propertyTypeId);
	}
	public String getProjectPurpose() {
		String temp = (String)viewMap.get("projectPurpose");
		return temp;
	}
	public void setProjectPurpose(String projectPurpose) {
		if(projectPurpose!=null)
			viewMap.put("projectPurpose" , projectPurpose);
	}
	public String getProjectPurposeId() {
		String temp = (String)viewMap.get("projectPurposeId");
		return temp;
	}
	public void setProjectPurposeId(String projectPurposeId) {
		if(projectPurposeId!=null)
			viewMap.put("projectPurposeId" , projectPurposeId);
	}
	
	public String getProjectSize() {
		String temp = (String)viewMap.get("projectSize");
		return temp;
	}
	public void setProjectSize(String projectSize) {
		if(projectSize!=null)
			viewMap.put("projectSize" , projectSize);
	}
	public String getProjectSizeId() {
		String temp = (String)viewMap.get("projectSizeId");
		return temp;
	}
	public void setProjectSizeId(String projectSizeId) {
		if(projectSizeId!=null)
			viewMap.put("projectSizeId" , projectSizeId);
	}
	
	public String getPropertyOwnership() {
		String temp = (String)viewMap.get("propertyOwnership");
		return temp;
	}
	public void setPropertyOwnership(String propertyOwnership) {
		if(propertyOwnership!=null)
			viewMap.put("propertyOwnership" , propertyOwnership);
	}
	public String getPropertyOwnershipId() {
		String temp = (String)viewMap.get("propertyOwnershipId");
		return temp;
	}
	public void setPropertyOwnershipId(String propertyOwnershipId) {
		if(propertyOwnershipId!=null)
			viewMap.put("propertyOwnershipId" , propertyOwnershipId);
	}
	
	public Date getExpectedStartDate() {
		Date temp = (Date)viewMap.get("expectedStartDate");
		return temp;
	}
	public void setExpectedStartDate(Date expectedStartDate) {
		if(expectedStartDate!=null)
			viewMap.put("expectedStartDate" , expectedStartDate);
	}
	
	public Date getExpectedEndDate() {
		Date temp = (Date)viewMap.get("expectedEndDate");
		return temp;
	}
	public void setExpectedEndDate(Date expectedEndDate) {
		if(expectedEndDate!=null)
			viewMap.put("expectedEndDate" , expectedEndDate);
	}
	
	public String getProjectCategory() {
		String temp = (String)viewMap.get("projectCategory");
		return temp;
	}
	public void setProjectCategory(String projectCategory) {
		if(projectCategory!=null)
			viewMap.put("projectCategory" , projectCategory);
	}
	public String getProjectCategoryId() {
		String temp = (String)viewMap.get("projectCategoryId");
		return temp;
	}
	public void setProjectCategoryId(String projectCategoryId) {
		if(projectCategoryId!=null)
			viewMap.put("projectCategoryId" , projectCategoryId);
	}
	
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	
	public String getProjectEstimatedCost() {
		String temp = (String)viewMap.get("projectEstimatedCost");
		if(temp !=null && StringHelper.isNotEmpty(temp))
		{
			Double projectEstimatedCostVal = CommonUtil.getDoubleValue(temp);
			DecimalFormat decimalFormat = new DecimalFormat(getNumberFormat());
			temp = decimalFormat.format(projectEstimatedCostVal);
		}
		return temp;
	}
	public void setProjectEstimatedCost(String projectEstimatedCost) {
		if(projectEstimatedCost!=null)
			viewMap.put("projectEstimatedCost" , projectEstimatedCost);
	}
	
	public String getLandNumber() {
		String temp = (String)viewMap.get("landNumber");
		return temp;
	}
	public void setLandNumber(String landNumber) {
		if(landNumber!=null)
			viewMap.put("landNumber" , landNumber);
	}
	
	public String getPropertyName() {
		String temp = (String)viewMap.get("propertyName");
		return temp;
	}
	public void setPropertyName(String propertyName) {
		if(propertyName!=null)
			viewMap.put("propertyName" , propertyName);
	}
	
	public String getPropertyId() {
		String temp = (String)viewMap.get("propertyId");
		return temp;
	}
	public void setPropertyId(String propertyId) {
		if(propertyId!=null)
			viewMap.put("propertyId" , propertyId);
	}
	
	public String getProjectId() {
		String temp = (String)viewMap.get("projectId");
		return temp;
	}
	public void setProjectId(String projectId) {
		if(projectId!=null)
			viewMap.put("projectId" , projectId);
	}
	
	public String getProjectDescription() {
		String temp = (String)viewMap.get("projectDescription");
		return temp;
	}
	public void setProjectDescription(String projectDescription) {
		if(projectDescription!=null)
			viewMap.put("projectDescription" , projectDescription);
	}
	
	@SuppressWarnings("unchecked")
	public void openStudySearchPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openStudySearchPopup() started...");
		try
		{
			sessionMap.put(WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY, WebConstants.Study.STUDY_SEARCH_PAGE_MODE_POPUP_SINGLE);			
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:openStudySearchPopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openStudySearchPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openStudySearchPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void openStudyViewPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openStudyViewPopup() started...");
		try
		{
			//RelatedStudiesView selectedRelatedStudiesView = (RelatedStudiesView) viewMap.get(WebConstants.Study.SELECTED_STUDY_DETAIL);
			StudyDetailView studyDetailView = (StudyDetailView) viewMap.get(WebConstants.Study.SELECTED_STUDY_DETAIL);
//			if(selectedRelatedStudiesView!=null)
//				studyDetailView = selectedRelatedStudiesView.getStudyByChildStudyId();
//			else
//				studyDetailView = new StudyServiceAgent().getStudyById(Convert.toLong(getRelatedStudyId())); 
			if(studyDetailView!=null){
				sessionMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailView );
				sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_POPUP_VIEW_ONLY );			
		        FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "javascript:openStudyViewPopup();";
		
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			}
	        logger.logInfo("openStudyViewPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openStudyViewPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void clearProperty(ActionEvent event){
		logger.logInfo("clearProperty() started...");
		try{
			/*ProjectDetailsView projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
			projectDetailsTabView.setPropertyId(0L);
			projectDetailsTabView.setPropertyName("");
			projectDetailsTabView.setPropertyStatus("");
			projectDetailsTabView.setPropertyStatusId("");
			projectDetailsTabView.setPropertyType("");
			projectDetailsTabView.setAddress("");
			projectDetailsTabView.setConstructionDate("");
			projectDetailsTabView.setEmirate("");
			projectDetailsTabView.setFloorCount("");
			projectDetailsTabView.setLandArea("");
			projectDetailsTabView.setLandNumber("");
			projectDetailsTabView.setLocationArea("");*/

			/*viewMap.put("clearProperty", true);
			viewMap.put(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW, projectDetailsTabView);
			populateFromView();*/
		}
    	catch(Exception exception){
    		logger.LogException("clearProperty() crashed ",exception);
    	}
	}
	
	private void populateFromView(){
		/*projectDetailsTabView = (ProjectDetailsView) viewMap.get(WebConstants.RequestDetailsTab.EVALUATION_REQUEST_DETAILS_TAB_VIEW);
		if(projectDetailsTabView!=null){
			setProjectNumber(projectDetailsTabView.getProjectNumber());
			setProjectStatus(projectDetailsTabView.getProjectStatus());
			setProjectStatusId(projectDetailsTabView.getProjectStatusId());
			setEvaluationPurpose(projectDetailsTabView.getEvaluationPurpose());
			setEvaluationPurposeId(projectDetailsTabView.getEvaluationPurposeId());
			setPropertyId(projectDetailsTabView.getPropertyId());
			setPropertyType(projectDetailsTabView.getPropertyType());
			setPropertyStatus(projectDetailsTabView.getPropertyStatus());
			setProjectDate(projectDetailsTabView.getProjectDate());
			setPropertyName(projectDetailsTabView.getPropertyName());
			setEmirate(projectDetailsTabView.getEmirate());
			setLocationArea(projectDetailsTabView.getLocationArea());
			setLandNumber(projectDetailsTabView.getLandNumber());
			setLandArea(projectDetailsTabView.getLandArea());
			setAddress(projectDetailsTabView.getAddress());
			setPropertyType(projectDetailsTabView.getPropertyType());
			setFloorCount(projectDetailsTabView.getFloorCount());
			setConstructionDate(projectDetailsTabView.getConstructionDate());
			setRequestDescription(projectDetailsTabView.getRequestDescription());
		}*/
	}
	
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}

	public ProjectDetailsView getRequestDetailsTabView() {
		return projectDetailsTabView;
	}

	public void setRequestDetailsTabView(
			ProjectDetailsView projectDetailsTabView) {
		this.projectDetailsTabView = projectDetailsTabView;
	}

	 public HtmlSelectOneMenu getProjectTypeCombo() {
		return projectTypeCombo;
	}

	public void setProjectTypeCombo(HtmlSelectOneMenu projectTypeCombo) {
		this.projectTypeCombo = projectTypeCombo;
	}

	public String getReadonlyStyleClass() {
		if(getReadonlyMode())
			return WebConstants.READONLY_STYLE_CLASS;
		return "";
	}
	public boolean isShowRecievePropertyImage()
	{
		
		if(viewMap.get("PAGE_MODE")!=null)
		{
			pageMode=viewMap.get("PAGE_MODE").toString();
			if(pageMode.compareTo("PAGE_MODE_INITIAL_RECEIVING")==0)
				showRecievePropertyImage=true;
			else
				showRecievePropertyImage=false;
		}
		return showRecievePropertyImage;
	}

	public void setShowRecievePropertyImage(boolean showRecievePropertyImage) {
		this.showRecievePropertyImage = showRecievePropertyImage;
	}
	public String openRecieveProperty()
	{
		//To get the DomainData for propertyType "Land"
//				String propertyType="";
			    PropertyView propertyView;
				propertyTypeLand=(DomainDataView) viewMap.get("PROPERTY_TYPE_LAND");
//				propertyType=viewMap.get("propertyTypeId").toString();
				
				
				//code is commented so user can view the property
				//if the property associated with project is Land  
//				 if(propertyType.compareTo(propertyTypeLand.getDomainDataId().toString())==0)
//					{
//					 //get all the properties associated with this project 
//					 if(viewMap.get("ASSOCIATED_PROPERTIES")!=null)
//	    				{
//	    					List<PropertyView> propertiesList=new ArrayList<PropertyView>(0);
//	    					propertiesList=(List<PropertyView>) viewMap.get("ASSOCIATED_PROPERTIES");
//	    					DomainDataView propertyStatusReceived=new DomainDataView();
//	    					if(viewMap.get("RECEIVED_STATUS")!=null)
//	    						propertyStatusReceived=(DomainDataView) viewMap.get("RECEIVED_STATUS");
//	    					for(PropertyView prop:propertiesList)
//	    					{
//	    						if(propertyStatusReceived.getDomainDataId().compareTo(prop.getStatusId())==0)
//	    						   {
//	    							if(viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY")!=null)
//	    							  {
//	    								  sessionMap.put("PROJECT_VIEW_FOR_RECEIVING_PROPERTY", viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY"));
//	    								  return "openReceiveProperty";
//	    							  }
//	    						   }	
//	    					}
//	    				}
//					 //if there is no property received under this project then allow the user to add new property
//					 //send project info to receive property
//					 if(viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY")!=null)
//					  {
//						  sessionMap.put("PROJECT_VIEW_FOR_RECEIVING_PROPERTY", viewMap.get("PROJECT_VIEW_FOR_RECEIVING_PROPERTY"));
//						  return "openReceiveProperty";
//					  }
//					}
//				 //if property is not a building, the open property in editMode or viewMode, based on status
//				  else
//				  {  
					  //open this property in edit mode, whether it is received or not
					  //user will just edit the property and there will be no change in status
					  if(viewMap.get("propertyView")!=null)
					  {
						  propertyView=(PropertyView) viewMap.get("propertyView");
						  if(propertyView.getTypeId().compareTo(propertyTypeLand.getDomainDataId())==0)
							  sessionMap.put("PROPERTY_TYPE_LAND", true);
						  sessionMap.put(WebConstants.Property.EDIT_MODE_FOR_RECEIVE_PROPERTY, propertyView);
						  sessionMap.put("PARENT_SCREEN", "PROJECT");
						  return "openPropertyInEditMode";
					  }
//				  }	
		return "";
	}

	public List<DomainDataView> getPropertyTypeList()
	{
		return propertyTypeList;
	}

	public void setPropertyTypeList(List<DomainDataView> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}

	public DomainDataView getPropertyTypeLand() {
		return propertyTypeLand;
	}

	public void setPropertyTypeLand(DomainDataView propertyTypeLand) {
		this.propertyTypeLand = propertyTypeLand;
	}
}

