package com.avanza.pims.web.backingbeans;

import java.util.List;

import com.avanza.core.util.Logger;
import com.avanza.pims.Utils.AppointmentDetailVO;
import com.avanza.pims.entity.AppointmentDetails;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.services.AppointmentService;



public class AppointmentDetailsBean extends AbstractController
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(AppointmentDetailsBean.class);
	AppointmentDetailVO appointmentDetailVO; 
	
//	FacesContext context=FacesContext.getCurrentInstance();
//    Map sessionMap;
//    Map viewRootMap=context.getViewRoot().getAttributes();
//    
    AppointmentService appointmentService = new AppointmentService();
    
	 @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
     
        
	public String getSelectBlackListKey()
	{
		 return WebConstants.SELECT_BLACK_LIST;
	}
	
	@SuppressWarnings("unchecked")
 	public  void populateAppointmentVO(String requestId){
		
		appointmentDetailVO = new AppointmentDetailVO();
		appointmentDetailVO.setRequestId(requestId);
		
		try{
			List<AppointmentDetails> list = appointmentService.search(appointmentDetailVO, null, null, null, null);
			if(null != list && 0 < list.size() ){
				AppointmentDetails appointmentDetails = list.get(0);
				
				transform(appointmentDetails, appointmentDetailVO);
				setAppointmentDetailVO(appointmentDetailVO);
				
			}
		}catch(Exception ex){
			logger.LogException("Error Occured ", ex);
		}
 	}
	
	public void transform(AppointmentDetails appointmentDetails, AppointmentDetailVO detailVO){
		
		detailVO.setAppointmentDateComplete(appointmentDetails.getAppointmentDateComplete());
		detailVO.setDuration(String.valueOf(appointmentDetails.getDuration()));
		detailVO.setAppointmentRequestedBy(appointmentDetails.getAppointmentRequestedBy().getFullName());
		
	}
 	
	private List<String> errorMessages;
		public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}


		public AppointmentDetailVO getAppointmentDetailVO() {
			return appointmentDetailVO;
		}


		public void setAppointmentDetailVO(AppointmentDetailVO appointmentDetailVO) {
			this.appointmentDetailVO = appointmentDetailVO;
		}


}
