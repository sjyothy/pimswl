package com.avanza.pims.web.backingbeans.construction.project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.soap.rpc.RPCConstants;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.dao.PaymentManager;
import com.avanza.pims.entity.PaymentSchedule;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.construction.project.ProjectService;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.EvaluationApplicationFilterView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.ProjectDetailsView;
import com.avanza.pims.ws.vo.ProjectMileStoneView;
import com.avanza.ui.util.ResourceUtil;


public class ProjectPaymentsTabBacking extends AbstractController {

	private static Logger logger = Logger.getLogger(ProjectPaymentsTabBacking.class);
	
	private HtmlDataTable dataTable = new HtmlDataTable();		
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();	
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	public ProjectPaymentsTabBacking (){
		
	}	
	
	@Override
	public void init() 
    {   	
		super.init();
   	 	if(!isPostBack()){
   	 		
   	 	}
	}

	@Override
	public void preprocess() {
		super.preprocess();
	}

	@Override
	public void prerender() {
		super.prerender();
		applyMode();
	}

	/**
	 * Applies rendering/readonly fields according to the current mode
	 */
	private void applyMode(){
		logger.logInfo("applyMode started...");
		try{
			
			logger.logInfo("applyMode completed successfully...");
		}
		catch(Exception ex)
		{
			logger.LogException("applyMode crashed...", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));        	
		}
	}

	@SuppressWarnings("unchecked")
	public String onAccept()
	{
		logger.logInfo("onAccept() started...");
		try
		{
			if(dataTable.isRowAvailable())
			{
				ProjectService ps = new ProjectService();
				PaymentScheduleView  paymentScheduleView = (PaymentScheduleView) dataTable.getRowData();		
				paymentScheduleView.setUpdatedBy(getLoggedInUserId());
				paymentScheduleView.setUpdatedOn(new Date());
				PaymentScheduleView psv = ps.acceptProjectPayment(paymentScheduleView);
				List<PaymentScheduleView>  paymentScheduleViewList = getProjectPaymentScheduleViewList();
				int index = dataTable.getRowIndex();
				paymentScheduleViewList.remove(index);
				paymentScheduleViewList.add(index, psv);
				setProjectPaymentScheduleViewList(paymentScheduleViewList);
				viewMap.put("infoMessage",CommonUtil.getBundleMessage(MessageConstants.PaymentSchedule.MSG_PAYMENT_ACCEPT_SUCCESS));
//				Long projectId = (Long)viewMap.get(WebConstants.ProjectFollowUp.PROJECT_ID);
//				if(projectId!=null)
//				{					
//					 List<PaymentScheduleView> projectPaymentsList =  ps.getProjectPayments(projectId);
//					 setProjectPaymentScheduleViewList(projectPaymentsList);
//				}
			}
	        logger.logInfo("onAccept() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("onAccept() crashed ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.PaymentSchedule.MSG_PAYMENT_ACCEPT_FAILURE));
		}
		finally{
			viewMap.put("errorMessages", errorMessages);
		}
		return "";
    }
	
	@SuppressWarnings("unchecked")
	public String onView()
	{
		logger.logInfo("onView() started...");
		try
		{
			PaymentScheduleView psv = (PaymentScheduleView)dataTable.getRowData();
	    	if(viewMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
	    			viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
	    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
	    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
	    	sessionMap.put("viewMode",true);
	    	
	    	String extrajavaScriptText ="var screen_width = 1024;"+
	         "var screen_height = 450;"+
	        "window.open('EditPaymentSchedule.jsf','_blank','width='+(screen_width-500)+',height='+(screen_height-200)+',left=300,top=250,scrollbars=yes,status=yes');";
	        openPopUp("EditPaymentSchedule.jsf", extrajavaScriptText);
	        
	        logger.logInfo("onView() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("onView() crashed ", exception);
		}
		return "";
    }
	
	public String openPopUp(String URLtoOpen,String extraJavaScript)
	{
		String methodName="openPopUp";
        final String viewId = "/LeaseContract.jsf";
		logger.logInfo(methodName+"|"+"Start..");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
        String javaScriptText ="var screen_width = 1024;"+
                               "var screen_height = 450;"+
                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
        if(extraJavaScript!=null && extraJavaScript.length()>0)
        javaScriptText=extraJavaScript;
        
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
        
                //AtPosition(facesContext, AddResource., javaScriptText)
		logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}
	
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
		return CommonUtil.getDateFormat();
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
	
	public String showUpdatedPayment()
	{
		List<PaymentScheduleView> paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
		
		if(viewMap.containsKey(WebConstants.ProjectPaymentsTab.PROJECT_PAYMENTS)  )
		{
			paymentScheduleViewList = (List<PaymentScheduleView>)viewMap.get(WebConstants.ProjectPaymentsTab.PROJECT_PAYMENTS);
			
		}
		
		if(viewMap.containsKey("PAYMENT_SCHEDULE_FORM_CONTRACT_DETAIL_TAB")  )
		{
			paymentScheduleViewList.addAll((List<PaymentScheduleView>)viewMap.get("PAYMENT_SCHEDULE_FORM_CONTRACT_DETAIL_TAB")) ;
			viewMap.remove("PAYMENT_SCHEDULE_FORM_CONTRACT_DETAIL_TAB");
			setProjectPaymentScheduleViewList(paymentScheduleViewList);
		}
		
		return null;
	}

	public void setProjectPaymentScheduleViewList(List<PaymentScheduleView> paymentScheduleViewList)
	{
		viewMap.put(WebConstants.ProjectPaymentsTab.PROJECT_PAYMENTS, paymentScheduleViewList);
	}
	
	
	public List<PaymentScheduleView> getProjectPaymentScheduleViewList() 
	{
		List<PaymentScheduleView> paymentScheduleViewList = (List<PaymentScheduleView>)viewMap.get(WebConstants.ProjectPaymentsTab.PROJECT_PAYMENTS);
		if(paymentScheduleViewList==null)
			paymentScheduleViewList = new ArrayList<PaymentScheduleView>();
		viewMap.put("recordSize", paymentScheduleViewList.size());
		return paymentScheduleViewList;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public Boolean getIsArabicLocale() {
		return CommonUtil.getIsArabicLocale();
	}

	public Boolean getCanAddMilestone(){
		Boolean canAddMilestone = (Boolean)viewMap.get("canAddMilestone");
		if(canAddMilestone==null)
			canAddMilestone = false;
		return canAddMilestone;
	}
	
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	
	public TimeZone getTimeZone()
	{
		return TimeZone.getDefault();
	}
	
	public String getNumberFormat(){
		return CommonUtil.getDoubleFormat();
    }
}
