package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.InspectionUnitView;
import com.avanza.pims.ws.vo.InspectionView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.UnitView;

public class InspectionApplicationDetails extends AbstractController 
{
	private List<InspectionUnitView> inspectionUnitsDataList;  
	private List<InspectionUnitView> selectedInspectionUnits; 
	private ServletContext servletcontext;
	private InspectionView inspectionView; 
	private RequestView requestView ; 
	private HtmlDataTable tbl_InspectionUnits ; 
	private Integer recordSize = 0; 
	private Integer paginatorRows = 0;  
	private Integer paginatorMaxPages = 0; 
	private String inspectionReason;
	private String pageMode; 
	private Boolean isViewMode=false; 
	private boolean popUpMode=false;
	private long requestId;
	private List<String> errorMessages ; 
	private PropertyServiceAgent psAgent;
	boolean isArabicLocale;
	boolean isEnglishLocale;
	Map viewMap;
	Map sessionMap;
	Logger logger=Logger.getLogger(InspectionApplicationDetails.class);
	String moduleName="InspectionApplicationBacking";
	
	
	public InspectionApplicationDetails()
	{
		String methodName="InspectionApplicationBacking";
		logger.logInfo(methodName+"|"+" Constructor Start..");
		try
		{
			psAgent = new PropertyServiceAgent();
			viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			errorMessages = new ArrayList(0);
			tbl_InspectionUnits = new HtmlDataTable();
			servletcontext = (ServletContext) getFacesContext().getExternalContext().getContext();
			inspectionView=new InspectionView();
			requestView = new RequestView();
			inspectionUnitsDataList=new ArrayList<InspectionUnitView>(0);
			selectedInspectionUnits=new ArrayList<InspectionUnitView>(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		logger.logInfo(methodName+"|"+"Finish..");
	}
	@SuppressWarnings("unchecked")
	public void init()
	{
		super.init();
		loadAttachmentsAndComments(null);
		
		if(!isPostBack())	
		{
			if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
		
			{	
					this.requestView = (RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
					this.requestId = requestView.getRequestId();
					viewMap.put(WebConstants.REQUEST_ID,this.requestId);
					viewMap.put(WebConstants.REQUEST_VIEW, this.requestView);
					inspectionView=getInspectionFromRequestId(requestId);
					viewMap.put(WebConstants.INSPECTION_VIEW, inspectionView);
					
					loadAttachmentsAndComments(requestId);
					
					pageMode=WebConstants.PAGE_MODE_VIEW;
					setIsViewMode(true);
					viewMap.put("canAddAttachment",false);
					viewMap.put("canAddNote",false);
					
					populateUnitsTab();
					populateApplicationDataTab();
					
			}
		}
	}

	
	private InspectionView getInspectionFromRequestId(Long requestId)
	{
		String methodName="getInspectionFromRequestId";
		logger.logInfo(methodName+"|"+"Start..requestId:"+requestId);
		try {
			inspectionView.setRequestId(requestId);
			
			List<InspectionView> inspectionList = psAgent.getAllInspections(inspectionView);
			if(inspectionList !=null && inspectionList.size()>0) 
			return (InspectionView)inspectionList.get(0);
		
		} 
		catch (Exception e) 
		{
			logger.LogException(methodName+"|"+"Error Occured:",e);
		}
		logger.logInfo(methodName+"|"+"Finish..");
		return null;
	}

	public  void populateUnitsTab()
	{	
		getInspectionReason();
		if(viewMap.containsKey(WebConstants.INSPECTION_VIEW) && viewMap.get(WebConstants.INSPECTION_VIEW)!=null )
		{	
			inspectionView=(InspectionView)viewMap.get(WebConstants.INSPECTION_VIEW);
			inspectionUnitsDataList=(inspectionView.getInspectionUnitsView());
			viewMap.put("UNITS_SELECTED_FROM_REQUEST", inspectionUnitsDataList);
		}
		
		if(viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)!=null)
		{		
				selectedInspectionUnits=(List<InspectionUnitView>)viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
				inspectionUnitsDataList=selectedInspectionUnits;
				if(viewMap.get("UNITS_SELECTED_FROM_REQUEST")!=null)
				{	
					HashMap<Long, InspectionUnitView> hMap=new HashMap<Long,InspectionUnitView>(0);
					inspectionUnitsDataList=(List<InspectionUnitView>)viewMap.get("UNITS_SELECTED_FROM_REQUEST");
					for(InspectionUnitView iuv1:inspectionUnitsDataList)
					{
						hMap.put(iuv1.getUnitView().getUnitId(),iuv1);
					}
					for(InspectionUnitView iuv2:selectedInspectionUnits)
					{
						if(!hMap.containsKey(iuv2.getUnitView().getUnitId()))
							{
								iuv2.setCreatedBy(CommonUtil.getLoggedInUser());
								iuv2.setCreatedOn(new Date());
								iuv2.setUpdatedBy(CommonUtil.getLoggedInUser());
								iuv2.setUpdatedOn(new Date());
								iuv2.setRecordStatus(new Long(1));
								iuv2.setIsDeleted(new Long(0));
								inspectionUnitsDataList.add(iuv2);
							}
					}
				}
			viewMap.put(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS, inspectionUnitsDataList);
		}
	}
	
	
	public void populateApplicationDataTab(){
		
		if(this.requestView != null)
		{
			PersonView applicantView = requestView.getApplicantView();
			if(requestView.getRequestNumber()!=null && !requestView.getRequestNumber().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
			if(requestView.getStatusId().toString()!=null && !requestView.getStatusId().toString().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr());
			if(requestView.getRequestDate()!=null)
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
			if(requestView.getDescription()!=null && !requestView.getDescription().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, requestView.getDescription());
			if(requestView.getRequestId()!=null && !requestView.getRequestId().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,requestView.getRequestId());
			
		//for read only description 
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		 
		

		if(requestView.getApplicantView()!=null && !requestView.getApplicantView().equals(""))
		{
			viewMap.put("APPLICANT_VIEW_FOR_NOTIFICATION", applicantView);
			if(applicantView.getPersonId()!=null && !applicantView.getPersonId().equals(""))
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,applicantView.getPersonId());
			
			if(applicantView.getPersonId()!=null && !applicantView.getPersonId().equals("")){
				viewMap.put(WebConstants.LOCAL_PERSON_ID, applicantView.getPersonId());
			}

		if(applicantView.getFirstName()!=null && !applicantView.getFirstName().equals(""))
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					(applicantView.getFirstName()!=null && applicantView.getFirstName().length()>0? applicantView.getFirstName():"")+" "+
	                (applicantView.getMiddleName()!=null && applicantView.getMiddleName().length()>0?applicantView.getMiddleName():"")+" "+
		            (applicantView.getLastName()!=null && applicantView.getLastName().length()>0?applicantView.getLastName():""));
		
		if(applicantView.getPersonTypeId()!=null && !applicantView.getPersonTypeId().equals(""))
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
					getIsEnglishLocale()?applicantView.getPersonTypeEn():applicantView.getPersonTypeAr());
		if(applicantView.getCellNumber()!=null && !requestView.getApplicantView().getCellNumber().equals(""))
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,requestView.getApplicantView().getCellNumber());
		
		if(applicantView.getIsCompany()!=null && !applicantView.getIsCompany().equals("")){
			DomainDataView ddv = new DomainDataView();
			if(applicantView.getIsCompany()==1L)
			{
			    ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
			    WebConstants.TENANT_TYPE_COMPANY);
			}
			else{
			        ddv= CommonUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
					WebConstants.TENANT_TYPE_INDIVIDUAL);
			    }
			
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());
		
		}
	  }
		
	  } 
		
	}
	
	
	
	
	
	public String getInspectionReason()
	{
		if(viewMap.containsKey(WebConstants.INSPECTION_VIEW) && viewMap.get(WebConstants.INSPECTION_VIEW)!=null )
		{
			inspectionView=(InspectionView)viewMap.get(WebConstants.INSPECTION_VIEW);
			inspectionReason=inspectionView.getRemarks();
		}
		else if(viewMap.get("INSPECTION_REASON")!=null)
			inspectionReason=viewMap.get("INSPECTION_REASON").toString();
		return inspectionReason;
	}
	
	public void setInspectionReason(String inspectionReason)
	{
		if(inspectionReason!=null)
		viewMap.put("INSPECTION_REASON",inspectionReason);
	}
	
	private void unitPaginationItems(Integer totalRecords)
	{
		String methodName="unitPaginationItems";
		logger.logInfo(methodName+"|"+"Start..");
		recordSize = totalRecords;
		viewMap.put("recordSize", recordSize);
		paginatorRows = getPaginatorRows();
		paginatorMaxPages = recordSize / paginatorRows;
		if ((recordSize % paginatorRows) > 0)
			paginatorMaxPages++;
		if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
		logger.logInfo(methodName+"|"+"Finish..");

	}
	
	public List<InspectionUnitView> getInspectionUnitsDataList()
	{
		if(viewMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)&&
				viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS)!=null)
		{
			inspectionUnitsDataList=(List<InspectionUnitView>)viewMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_MANY_UNITS);
		}
		else if(viewMap.containsKey("UNITS_SELECTED_FROM_REQUEST") && viewMap.get("UNITS_SELECTED_FROM_REQUEST")!=null)
		{
			inspectionUnitsDataList=(List<InspectionUnitView>)viewMap.get("UNITS_SELECTED_FROM_REQUEST");
		}
		return inspectionUnitsDataList;
	}

	public void setInspectionUnitsDataList(
			List<InspectionUnitView> inspectionUnitsDataList) {
		this.inspectionUnitsDataList = inspectionUnitsDataList;
	}
	public List<InspectionUnitView> getSelectedInspectionUnits() {
		return selectedInspectionUnits;
	}
	public void setSelectedInspectionUnits(
			List<InspectionUnitView> selectedInspectionUnits) {
		this.selectedInspectionUnits = selectedInspectionUnits;
	}
		
	
	public Boolean getIsViewMode() {
		if(viewMap.get("IS_VIEW_MODE")!=null)
			isViewMode=(Boolean)viewMap.get("IS_VIEW_MODE");
		return isViewMode;
	}


	public void setIsViewMode(Boolean isViewMode) {
		viewMap.put("IS_VIEW_MODE", isViewMode);
		this.isViewMode = isViewMode;
	}

	
	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}


	public InspectionView getInspectionView() {
		return inspectionView;
	}

	public void setInspectionView(InspectionView inspectionView) {
		this.inspectionView = inspectionView;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages) ;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getPageMode()
	{
		if(viewMap.get(WebConstants.PAGE_MODE)!=null)
			pageMode=viewMap.get(WebConstants.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode)
	{
		viewMap.put(WebConstants.PAGE_MODE,pageMode);
	}
	public Integer getRecordSize()
	{	
			if (viewMap.get("recordSize") !=null) 
			{
				recordSize = (Integer) viewMap.get("recordSize");
			}
			return recordSize;
	}
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorRows()
	{
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getPaginatorMaxPages()
	{
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public boolean isPopUpMode()
	{
		if(viewMap.containsKey("POPUP_MODE") && viewMap.get("POPUP_MODE")!=null)
			popUpMode=(Boolean)viewMap.get("POPUP_MODE");
		return popUpMode;
	}

	public void setPopUpMode(boolean popUpMode)
	{
		viewMap.put("POPUP_MODE",popUpMode);
	}
	public boolean getIsArabicLocale()
	{
       String method="getIsArabicLocale";
    	
    	logger.logInfo(method+"|"+"Start...");
    	
		isArabicLocale = !getIsEnglishLocale();
		logger.logInfo(method+"|"+"Finish...");
		return isArabicLocale;
	}
	public boolean getIsEnglishLocale()
	{
        String method="getIsArabicLocale";
    	
    	logger.logInfo(method+"|"+"Start...");
		    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
				LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
				isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		logger.logInfo(method+"|"+"Finish...");
		return isEnglishLocale;
	}
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	
	public void loadAttachmentsAndComments(Long requestId){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_INSPECTION_REQUEST);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_INSPECTION;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if(requestId!= null){
			
			requestId = (Long)viewMap.get(WebConstants.REQUEST_ID);
	    	String entityId = requestId.toString();
	    	
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	public HtmlDataTable getTbl_InspectionUnits() {
		return tbl_InspectionUnits;
	}

	public void setTbl_InspectionUnits(HtmlDataTable tbl_InspectionUnits) {
		this.tbl_InspectionUnits = tbl_InspectionUnits;
	}
	
	public String back()
	{
		return "requestSearch";
	}

}
