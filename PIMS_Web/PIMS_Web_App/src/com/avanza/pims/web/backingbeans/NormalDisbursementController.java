package com.avanza.pims.web.backingbeans;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.TextType;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.memsnormaldisbursementbpel.proxy.proxy.MEMSNormalDisbursementBPELPortClient;
import com.avanza.pims.bpel.proxy.MEMSBlockingBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.DisbursementReason;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ExecFunc;
import com.avanza.pims.entity.Request;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.web.workflow.WFTaskAction;
import com.avanza.pims.web.workflow.WorkFlowVO;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementReasonView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.ItemTypeView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings("unchecked")
public class NormalDisbursementController extends AbstractMemsBean {
	
	private static final long serialVersionUID = 1L;
	private boolean pageModeView =false;
	private String selectOneMainReason;
	private String totalDisbursementCount;
	private static final String DELETED_DISBURSEMENT	= "DELETED";
	private static final String DISBURSEMENTS			= "DISB_DETAILS";
	private static final String BENENEFICIARY_LIST 		= "BENENEFICIARY_LIST";
	private static final String PROCEDURE_TYPE 			= "procedureType";
	private static final String EXTERNAL_ID 			= "externalId";
	private static final String IS_FILE_ASSOCIATED		= "IsFileAssociated";
	private static final String REASON_MAP				= "reasonMap";
	private static final String ITEM_MAP				= "itemMap";
	private static final String ITEM_CMB				= "itemCmb";
	private static final String REASON_CMB				= "reasonCmb";
	private static final String CMB_DEFAULT_VAL			= "-1";
	private static final String CMB_ALL_BENEF 			= "all";
	private static final String IS_UPDATE				= "IsUpdate";
	private static final String lowerBoundDate          = "19/11/2013 23:59:59";
	private static final String IS_REVIEW				= "IsReview";
	private static final String ALL_USER_GROUPS 		= "AllUserGroups";
	private static final String CSS_READONLY			= "READONLY";
	private static final String USER_IN_GROUP 			= "UserInGroup";
	private static final String DISBURSEMENT_SOURCES	= "DisbursementSources";
	private static final String PAYMENT_SCH_UPDATE		= "PaymentUpdate";
	private static final String PERSON_BALANCE_MAP			= "personBalance";
	private static final String PERSON_BLOCKING_MAP			= "personBlocking";
	private static final String BENEF_NO_COST_CENTER_MAP			= "BENEF_NO_COST_CENTER_MAP";
	private static final String TAB_DETAILS				= "idDetails";
	
	private static final String TAB_COMMENTS				= "commentsTab";
	private static final String TAB_APPLICANTS			= "applicationTab";
	private static final String TAB_FINANCE			= "financeTab";
	private static final String TAB_REVIEW			= "reviewTab";
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlDataTable disbursementTable = new HtmlDataTable();
	private HtmlSelectOneMenu cmbBeneficiaryList = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbPaymentSource = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbPaymentTypes = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbReasonTypes = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbMainReason = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbItemTypes = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbUserGroups = new HtmlSelectOneMenu();
	private HtmlInputText txtAmount = new HtmlInputText();
	private HtmlInputText txtDescription = new HtmlInputText();
	private HtmlInputText txtReviewComments = new HtmlInputText();
	private HtmlInputText txtRejectionReason = new HtmlInputText();
	private HtmlSelectOneMenu cmbDisbursementSource = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbPaidTo	= new HtmlSelectOneMenu();
	private HtmlInputText txtBalance = new HtmlInputText();
	private HtmlInputText accountBalance = new HtmlInputText();
	private HtmlInputText noOfBenef= new HtmlInputText();
	private HtmlInputText blockingBalance = new HtmlInputText();
	
	private HtmlInputText txtVendor = new HtmlInputText();
	private HtmlGraphicImage imgVendor = new HtmlGraphicImage();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlInputText htmlFileType = new HtmlInputText();
	private HtmlInputText htmlFileStatus  = new HtmlInputText();
	private HtmlInputText htmlFileDate = new HtmlInputText();
	private java.util.Date lowerBound = null; 
	
	
	private static final Long INVALID_STATUS = -1L;

	public NormalDisbursementController()
	{
		try
		{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		lowerBound = (java.util.Date)df.parse(lowerBoundDate);
		}
		catch(Exception e) {}
	}
	private String hdnPersonId;
	private String hdnPersonName;
	private String hdnPersonType;
	private String hdnCellNo;
	private String hdnIsCompany;
	private String hdnRequestId;
	private String hdnVendorId;
	private String hdnVendorName;
	DomainDataView paymentSourceSelfAccount =null;

	private MemsFollowupView followUp;
	public String getHdnVendorName() {
		return hdnVendorName;
	}

	public void setHdnVendorName(String hdnVendorName) {
		this.hdnVendorName = hdnVendorName;
	}

	public String getHdnVendorId() {
		return hdnVendorId;
	}

	public void setHdnVendorId(String hdnVendorId) {
		this.hdnVendorId = hdnVendorId;
	}

	@Override
	public void init() {
		super.init();
		try {			
			if( !isPostBack() ) {
				
				UtilityService utilityWS = new UtilityService();				
				Map<Long, DisbursementReasonView> reasonMap = utilityWS.getAllDisbursementReasons();
				viewMap.put( REASON_MAP, reasonMap);
//				loadReasonsCombo( reasonMap);
							
		    	viewMap.put( PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_MEMS_NORMAL_PAYMENTS);
		    	viewMap.put( EXTERNAL_ID , WebConstants.Attachment.EXTERNAL_ID_MEMS_NORMAL_PAYMENTS);
	    		
				ApplicationBean application = new ApplicationBean();
	    		
		    	fillStatusInViewMap();
		    	
		    	List<SelectItem> srcList = application.getDomainDataList( WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		    	for(SelectItem singleSrc : srcList) {
		    		if( singleSrc.getLabel().equals( WebConstants.PaymentSrcType.EXTERNAL_PARTY)) {
		    			srcList.remove(singleSrc);
		    			break;
		    		}
		    	}		    
		    	viewMap.put( WebConstants.MemsPaymentCategories.PAYMENT_SOURCE, srcList);
		    	
		    	List<SelectItem> paymentTypeList = application.getDomainDataList( WebConstants.MemsPaymentTypes.DOMAIN_KEY);
		    	
		    	viewMap.put( WebConstants.MemsPaymentTypes.DOMAIN_KEY,paymentTypeList);
		    	
		    	cmbItemTypes.setDisabled( true);
		    	loadUserGroupsInViewRoot();
		    					    		    	
		    	List<DisbursementDetailsView> toDeleteList = new ArrayList<DisbursementDetailsView>();
				viewMap.put( DELETED_DISBURSEMENT, toDeleteList);
				
				RequestView reqView = (RequestView) getRequestMap().get( WebConstants.REQUEST_VIEW);
				InheritanceFileView inheriteanceFile = (InheritanceFileView) getRequestMap().get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				BeneficiaryGridView beneficiary = (BeneficiaryGridView) getRequestMap().get( WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
				followUp   = 			(MemsFollowupView) sessionMap.remove(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW ) ;
				
				
				if( null!=reqView ) {
					initFromRequestSearch( reqView);
				} else if ( null!=inheriteanceFile ){
					initFromInheritanceFileSearch( inheriteanceFile);
				} else if ( null!=beneficiary ) {
					initFromBeneficiarySearch( beneficiary);
				} 
				
				else if( null != followUp )
				{
					initFromFollowup( inheriteanceFile);
				}

				if ( null!=sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
					if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) instanceof UserTask )  {
					initFromSOATask( );
					}
					else if(sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) instanceof TaskListVO)  {
					initFromWorkFlowUserTasks();
					}
				}
				//Must be added before the closing brace of !isPostBack
				setUserInGroup();
			}			
		} catch (Exception ex) {
			logger.LogException("[Exception occured in init()]", ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void fillStatusInViewMap() {
		List<DomainDataView> ddvList = CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS);
		
		viewMap.put( WebConstants.REQUEST_STATUS_NEW, CommonUtil.
							getIdFromType( ddvList, WebConstants.REQUEST_STATUS_NEW).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_APPROVED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_APPROVED).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_REJECTED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_REJECTED).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_COMPLETE, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_COMPLETE).getDomainDataId());
		viewMap.put( WebConstants.REQUEST_STATUS_FINANCE_REJECTED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_FINANCE_REJECTED).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_CARING_REJECTED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_CARING_REJECTED).getDomainDataId());
		

		viewMap.put( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_REVIEW_REQUIRED).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_REVIEW_DONE, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_REVIEW_DONE).getDomainDataId());
		
		viewMap.put( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ, CommonUtil.
				getIdFromType( ddvList, WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ).getDomainDataId());
	}
	
	@Override
	public void prerender() 
	{
		readParams();
		handleControlsStatus();
		if (viewMap.containsKey(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS)) {				
			List<String> sucMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);					
			successMessages.addAll(sucMsgs);
			viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_SUC_MSGS);
		}
		if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS)) {
			List<String> errMsgs = (List<String>) viewMap.get(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS); 
			errorMessages.addAll( errMsgs);
			viewMap.remove(WebConstants.FinanceTabPublishKeys.TAB_ERR_MSGS);
		}
		else if (viewMap.containsKey( WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES ) )
		{
			List<String> errMsgs = (List<String>) viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES ); 
			errorMessages.addAll( errMsgs);
		}
		if (  getIsReviewRequired() && getIsUserInGroup() )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
		}
		if( !isTaskAvailable() && 
				( getIsApprovalRequired() || getIsReviewRequired() || getIsReviewed() || getIsFinanceRejected() || getIsDisbursementRequired() ))
		{
			if( viewMap.remove("fetchTaskNotified") == null  )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
				viewMap.put("fetchTaskNotified", true);
			}
		}
		
	};
	
	public Boolean getIsUserInGroup() {
		String userInGrp = ((String) viewMap.get(USER_IN_GROUP));
		if (null == userInGrp) {
			return false;
		}
		return userInGrp.equals("TRUE");
	}


	
	private void handleControlsStatus() 
	{
		if( getIsCompleted() || getIsReviewRequired() || getIsDisbursementRequired() )
		{
			cmbBeneficiaryList.setReadonly( true);
			cmbItemTypes.setReadonly( true);
			cmbPaymentSource.setReadonly( true);
			cmbPaymentTypes.setReadonly( true);
			cmbReasonTypes.setReadonly( true);
			txtDescription.setReadonly( true);
			txtAmount.setReadonly( true);		
			txtAmount.setStyleClass( CSS_READONLY);
			txtDescription.setStyleClass( CSS_READONLY);
			cmbBeneficiaryList.setStyleClass( CSS_READONLY);
			cmbItemTypes.setStyleClass( CSS_READONLY);
			cmbPaymentSource.setStyleClass( CSS_READONLY);
			cmbPaymentTypes.setStyleClass( CSS_READONLY);
			cmbReasonTypes.setStyleClass( CSS_READONLY);
			txtAmount.setStyleClass( CSS_READONLY);
			txtDescription.setStyleClass( CSS_READONLY);
		}
		
//		if( getIsApprovalRequired() || getIsReviewed() || getIsApprovalRequired() ) {
//			txtAmount.setStyleClass("");
//			txtAmount.setReadonly( false);
//		}
			
	}
	
	public void handlePaymentSchedule() 
	{
			try
			{
				DisbursementDetailsView disbursement = (DisbursementDetailsView) sessionMap.remove( WebConstants.MemsNormalDisbursements.DONE_PAYMENT_SCHEDULE);;
				
				for(DisbursementDetailsView singleView : getDisbursementDetails()) 
				{
					if( singleView.getId()==disbursement.getId()) 
					{
						List<PaymentDetailsView> list = singleView.getPaymentDetails();
						if( null!=list) 
						{
							list.clear();
							list = null;
						}			
						singleView.setPaymentDetails( disbursement.getPaymentDetails());
						singleView.setHasInstallment( WebConstants.HAS_INSTALLMENT_TRUE);
						successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_MSG_SCHEDULE));
					}			
				}
			} 
			catch (Exception ex) 
			{
				logger.LogException("[Exception occured in handlePaymentSchedule()]", ex);
				errorMessages = new ArrayList<String>(0);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
		
	public void handlePaidToChange() 
	{
		try
		{
			if( !cmbPaidTo.getValue().toString().equals( WebConstants.PAID_TO_BENEFICIARY)) 
			{
				imgVendor.setRendered( true);
			} 
			else 
			{
				imgVendor.setRendered( false);
				txtVendor.setValue("");
				DisbursementDetailsView updateMe = this.getIsUpdate();
				if( null!=updateMe ) 
				{
					updateMe.setVendorId(null);
					updateMe.setVendorName("");				
				}
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in handlePaidToChange()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void handlePaymentSourceChange() 
	{
		try
		{
			if( !cmbPaymentSource.getValue().toString().equals( CMB_DEFAULT_VAL))
			{
				if( paymentSourceSelfAccount == null )
				{
					paymentSourceSelfAccount = new UtilityService().getDomainDataByValue(WebConstants.PaymentSrcType.DOMAIN_KEY, WebConstants.PaymentSrcType.SELF_ACCOUNT);
					if( cmbPaymentSource.getValue() != null &&
					   !cmbPaymentSource.getValue().equals(CMB_DEFAULT_VAL) //&&
						//cmbPaymentSource.getValue().equals( paymentSourceSelfAccount.getDomainDataId().toString() )
					  )
					{
						if( getIsApproved() )
						{
					     Long monthlyAlimonyId = CommonUtil.getDomainDataId( WebConstants.MemsPaymentTypes.DOMAIN_KEY, 
					    		 											 WebConstants.MemsPaymentTypes.INCREASING_MONTHLY_ALIMONY
					    		 										   );	
						 renderControlsForApprovedMode( cmbPaymentTypes.getValue().toString(),
								                       monthlyAlimonyId.toString(),cmbPaymentSource.getValue().toString(),
								                       null,null ,null );
						}

					}
				}
				
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in handlePaymentSourceChange()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void handlePaymentTypeChange() 
	{
		try
		{
				if( !cmbPaymentTypes.getValue().toString().equals( CMB_DEFAULT_VAL)) 
				{
					Long paymentTypeId = new Long( cmbPaymentTypes.getValue().toString());
					Long monthlyAlimonyId = CommonUtil.getDomainDataId( WebConstants.MemsPaymentTypes.DOMAIN_KEY, 	
																		WebConstants.MemsPaymentTypes.INCREASING_MONTHLY_ALIMONY);
					if( paymentTypeId.equals( monthlyAlimonyId) ) {
						Long selfAccountId = CommonUtil.getDomainDataId( WebConstants.PaymentSrcType.DOMAIN_KEY, 
																		 WebConstants.PaymentSrcType.SELF_ACCOUNT);
						cmbPaymentSource.setValue( selfAccountId.toString());
						cmbPaymentSource.setReadonly( true);
						cmbPaymentSource.setStyleClass( CSS_READONLY);
					} else {
						cmbPaymentSource.setReadonly( false);
						cmbPaymentSource.setStyleClass( "");
						cmbPaymentSource.setValue( CMB_DEFAULT_VAL);
					}
					
				}
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in handlePaymentTypeChange()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void handleMainReasonChange() 
	{		
		try 
		{
				if( cmbMainReason.getValue().toString().equals( CMB_DEFAULT_VAL)) 
				{
					cmbReasonTypes.setDisabled(true);
					cmbReasonTypes.setValue("-1");
					
					cmbItemTypes.setDisabled(true);
					cmbItemTypes.setValue("-1");
					return;
	
				}
				Long mainReasonId = Long.valueOf( 
													cmbMainReason.getValue().toString()
												);
	            List<DisbursementReason> objList = new UtilityService().getAllDisbursementReasonsList();
	            ArrayList<SelectItem> cmbReasonType = new ArrayList<SelectItem>();
	            for(DisbursementReason obj : objList)
	            {
	                
	                if( 
	                		mainReasonId.compareTo( -1L ) != 0  && obj.getType() != null &&
	                        mainReasonId.compareTo( obj.getType().getDomainDataId() ) == 0 
	                   )
	                {
	                    
	                	if( CommonUtil.getIsEnglishLocale() ) 
	                	{
	        				cmbReasonType.add(new SelectItem( obj.getReasonId().toString(), obj.getReasonEn()));
	        			}
	                	else 
	                	{
	                		cmbReasonType.add(new SelectItem( obj.getReasonId().toString(), obj.getReasonAr()));
	        			}
	                }
	            }
	    		Collections.sort(cmbReasonType, ListComparator.LIST_COMPARE);
	    		viewMap.put(REASON_CMB, cmbReasonType);
				
		} 
		catch(Exception ex) 
		{
			logger.LogException( "[Exception occured in handleMainReasonChange()]", ex);			
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	
	public void handleReasonChange() 
	{		
		try 
		{
				if( cmbReasonTypes.getValue().toString().equals( CMB_DEFAULT_VAL)) 
				{
					cmbItemTypes.setDisabled(true);
					cmbItemTypes.setValue("-1");
	
				}
				Map<Long, ItemTypeView> itemMap = new UtilityService().getItemTypesByReason( cmbReasonTypes.getValue().toString() );
				viewMap.put( ITEM_MAP, itemMap);
				loadItemsCombo( itemMap );
				if( itemMap != null && itemMap.size() > 0) 
				{
					cmbItemTypes.setDisabled(false);
				} 
				else 
				{
					cmbItemTypes.setDisabled(true);
					cmbItemTypes.setValue( CMB_DEFAULT_VAL);
				}
		} 
		catch(Exception ex) 
		{
			logger.LogException( "[Exception occured in handleReasonChange()]", ex);			
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	protected void initFromWorkFlowUserTasks()throws Exception
    {
		TaskListVO task = ( TaskListVO )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, task);
		viewMap.put(WebConstants.TASK_LIST_SELECTED_TASK_TYPE,task.getHumanTaskId());
		if( task.getRequestId() == null ){ return; }
		
	  	long  id = new Long  ( task.getRequestId() );
	  	RequestService reqSrv = new RequestService();
		RequestView requestView= reqSrv.getRequestById(id);					
		initFromRequestSearch(requestView);
		loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
    }
	private void initFromSOATask() throws Exception {
		UserTask userTask = (UserTask) sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK) ;
		viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		Map taskAttributes = userTask.getTaskAttributes();
		if (null != taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)) {
			viewMap.put(WebConstants.TASK_LIST_SELECTED_TASK_TYPE,userTask.getTaskType());
			Long reqId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));					
			RequestService reqSrv = new RequestService();
			RequestView requestView = reqSrv.getRequestById(reqId);
			initFromRequestSearch(requestView);
			loadInhFileParamTextBoxes(requestView.getInheritanceFileId()!=null?requestView.getInheritanceFileId():null);
		}
	}
	
	private void initFromBeneficiarySearch(BeneficiaryGridView beneficiaryView) throws Exception 
	{
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();
		setTotalRows(disbDetails.size());

		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date() );
		reqView.setCreatedOn(new Date() );
		reqView.setRequestDate(new Date() );

		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID);
				
		reqView.setStatusId(INVALID_STATUS);
		reqView.setDisubrsementDetailsView(disbDetails);

		setDisbursementDetails( disbDetails);
		cmbBeneficiaryList.setValue(beneficiaryView.getBeneficiaryPersonId().toString());
		storeBeneficiaryBalanceAmountInMap( beneficiaryView.getBeneficiaryPersonId() );
		storeBeneficiaryBlockingAmountInMap( beneficiaryView.getBeneficiaryPersonId() );
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		loadBeneficiaryList(beneficiaryView);
	}
	private void storeBeneficiaryBalanceAmountInMap( Long beneficiaryId ) throws Exception
	   {
		   if(  !getBalanceMap().containsKey(beneficiaryId  ) )
		   {
//		    PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
			Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
			Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryId ,getRequestView().getInheritanceFileId());
//			Double amount =  PersonalAccountTrxService.getAmount( beneficiaryId  );
			personBalanceMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
			handleBeneficiaryChange();				
		   }
		   
	   }
	
	private void storeBeneficiaryBlockingAmountInMap( Long beneficiaryId ) throws Exception
	   {
		   if(  !getBlockingMap().containsKey(beneficiaryId  ) )
		   {
		    
			Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
			Double amount =  BlockingAmountService.getBlockingAmount(  beneficiaryId , null, null, null, null );
			personBlockingMap.put( beneficiaryId , amount);
		    viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
			handleBeneficiaryChange();				
		   }
		   
	   }
	private void initFromFollowup( InheritanceFileView inheritanceFileView ) throws Exception
	{
		setFollowUp(followUp);
		initFromInheritanceFileSearch(
				                       ( InheritanceFileView )sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) 
				                     );
	}
	private void initFromInheritanceFileSearch(InheritanceFileView inheritanceFileView) throws Exception {		
		RequestView reqView = new RequestView();
		List<DisbursementDetailsView> disbDetails = new ArrayList<DisbursementDetailsView>();										
		setTotalRows(disbDetails.size());		
		
		reqView.setCreatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedBy(CommonUtil.getLoggedInUser());
		reqView.setUpdatedOn(new Date());				
		reqView.setCreatedOn(new Date());
		reqView.setRequestDate(new Date());
		
		reqView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		reqView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		reqView.setRequestTypeId(WebConstants.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID);
		reqView.setStatusId(INVALID_STATUS);		
		reqView.setDisubrsementDetailsView(disbDetails);
				
		setDisbursementDetails(  disbDetails);
		Long fileId = inheritanceFileView.getInheritanceFileId();
		reqView.setInheritanceFileId(fileId);
				
		reqView.setInheritanceFileView(inheritanceFileView);		
		setRequestView(reqView);	
				
		handleAttachmentCommentTabStatus(reqView);
		loadAttachmentsAndComments(null);
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		bean.getAllowedApplicantFromInheritanceFile(fileId);
		loadBeneficiaryList(fileId);
		loadInhFileParamTextBoxes(fileId);
		viewMap.put(IS_FILE_ASSOCIATED, "TRUE");
	}
	private void loadInhFileParamTextBoxes(Long fileId) {
		try {
			if (fileId != null) 
			{
				InheritanceFileView inhFile = new UtilityService().getInheritanceFileById(fileId);
				htmlFileNumber.setValue(inhFile.getFileNumber());
				txtFileOwner.setValue( inhFile.getFilePerson() );
				DomainDataView domainData = new UtilityService().getDomainDataByDomainDataId(inhFile.getStatusId() );
				DomainDataView domainDataFileType = new UtilityService().getDomainDataByDomainDataId(new Long(inhFile.getFileTypeId() ) );
				if (CommonUtil.getIsEnglishLocale()) 
				{
					htmlFileStatus.setValue(domainData.getDataDescEn());
					htmlFileType.setValue(domainDataFileType.getDataDescEn());
					htmlFileDate.setValue(inhFile.getCreatedOn());
				} else {
					htmlFileStatus.setValue(domainData.getDataDescAr());
					htmlFileType.setValue(domainDataFileType.getDataDescAr());
					htmlFileDate.setValue(inhFile.getCreatedOn());
				}
			}
		} catch (Exception ex) {
			logger.LogException("loadInhFileParamTextBoxes|Error Occured:", ex);
			
		}

	}
	
	//FIXME remove warning	
	@SuppressWarnings("unused")
	private void initFromRequestSearch(RequestView reqView) throws Exception {
		setRequestView(reqView);
		Long inhFileId = reqView.getInheritanceFileId();
		refreshTabs(reqView);
		//TODO: if its not from request search populate with the beneficiary combo box 
		if( null!=inhFileId ) 
		{
			loadBeneficiaryList(inhFileId);
			viewMap.put(IS_FILE_ASSOCIATED, "TRUE");
		}
		else 
		{
			//the request is not associated with the inheritance file
			// there should be only one disbursement and one request beneficiary
			List<DisbursementDetailsView> view = getDisbursementDetails(); 
			if( null!=view && 1==view.size()) {
				List<DisbursementRequestBeneficiaryView> reqBenef = view.get(0).getRequestBeneficiaries();
				DisbursementRequestBeneficiaryView next = reqBenef.get( 0);
				BeneficiaryGridView gridView = new BeneficiaryGridView();
				gridView.setPersonName(next.getName());
				gridView.setBeneficiaryPersonId(next.getPersonId());
				loadBeneficiaryList(gridView);							
			}
		}
		loadAttachmentsAndComments(reqView.getRequestId());
		loadInhFileParamTextBoxes(reqView.getInheritanceFileId()!=null?reqView.getInheritanceFileId():null);
	}
	
	private boolean validateBeneficiaryWhenApproved() throws Exception
	{
		
	
		//If its being edited then condition before OR else for adding condition after or
		boolean isPaymentTypeAid = //( disbursement != null && paymentTypeId.equals( disbursement.getSrcType()  ) ) ||
		                             cmbPaymentSource.getValue() == null || cmbPaymentSource.getValue().equals("-1") ||
		                             !cmbPaymentSource.getValue().equals( String.valueOf(  WebConstants.PaymentSrcType.SELF_ACCOUNT_ID ) );
		                           
		if(  isPaymentTypeAid  && cmbDisbursementSource.getValue().toString().equals( CMB_DEFAULT_VAL) ) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_DISB_SRC_REQ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;			
		}
		
		/* when paid to is selected to the vendor and no vendor is selected */
		if( !cmbPaidTo.getValue().toString().equals( WebConstants.PAID_TO_BENEFICIARY) && (txtVendor.getValue() == null || 
				txtVendor.getValue().toString().trim().length() <=0 )
		  ) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_VENDOR_REQ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;
			
		}
		if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_BENEFF_MAND));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;
		}
		else 
		{
			if( !validateCostCenter(cmbBeneficiaryList.getValue().toString() ) )
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * @return
	 */
	private boolean validateCostCenter(String personId) throws Exception 
	{
		Map<String, String> map  =  (HashMap<String, String>) viewMap.get( BENEF_NO_COST_CENTER_MAP );
		if ( map.containsKey( personId ) )
		{
			String message = map.get( personId );
			errorMessages.add( 
								java.text.MessageFormat.format(
																CommonUtil.getBundleMessage("disburseFinance.msg.associateCostCenter"),
																 (" "+message)
															  )
							    
							 );
			return false;
		}
		return true;
	}
	
	/**
	 *  The function also validates and saves the disbursement sources
	 */
	public void saveBeneficiaryWhenApproved() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{			
			if( validateSaveBeneficiary() && validateBeneficiaryWhenApproved() ) 
			{
				DisbursementDetailsView updateMe = this.getIsUpdate();
				addFieldsforApproved(updateMe);										
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_UPDATE_SUCC));
				restoreToDefault();
			}			
		} 
		catch(Exception ex) 
		{
			logger.LogException( "[Exception occured in saveBeneficiaryWhenApproved()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void addFieldsforApproved(DisbursementDetailsView updateMe)throws Exception
	{
//		if( getIsNewStatus()  )
//		{
//			return;
//		}
		populateWithUserInput(updateMe);
		if( cmbPaidTo.getValue() != null )
		{
				updateMe.setPaidTo( cmbPaidTo.getValue().toString());
		}
		if( cmbDisbursementSource.getValue()!= null &&
				updateMe.getSrcType().equals( WebConstants.PaymentSrcType.AID_ID ) ) 
		{
			updateMe.setDisburseSrc( new Long(cmbDisbursementSource.getValue().toString()));
			for(SelectItem disbursementSrc : getDisbursementSources()) 
			{
				Long srcId = new Long( disbursementSrc.getValue().toString());
				if( srcId.compareTo( updateMe.getDisburseSrc())==0 ) 
				{
					
				 updateMe.setDisburseSrcNameEn( disbursementSrc.getLabel());
				 updateMe.setDisburseSrcNameAr( disbursementSrc.getLabel());
				 break;
				}					
			}
		}
	}
		
	public void saveBeneficiary() 
	{
		errorMessages = new ArrayList<String>(0);
		try
		{
		if(txtAmount.getValue() == null || txtAmount.getValue().toString().trim().length() <= 0)
		{
			txtAmount.setValue( "0");
		}
		if( !validateSaveBeneficiary()  ) return;
		else if  (getIsApprovalRequired() && getIsRequestDateAfterLowerBound() && !validateBeneficiaryWhenApproved() )return;
		else if  ( getIsApproved() && !validateBeneficiaryWhenApproved() )return;
//		List<DisbursementDetailsView> disbDetails = getDisbursementDetails();
		DisbursementDetailsView updateMe = this.getIsUpdate();
			
			if( null!=updateMe )
			{
				//populateWithUserInput( updateMe);
				addFieldsforApproved(updateMe);
				
//				disbDetails.remove(updateMe);
				viewMap.remove( IS_UPDATE );
				
			
			}
			else
			{
				
				List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
				DisbursementDetailsView ddView = getDefaultDisbDetailView();
				//populateWithUserInput( ddView);
				addFieldsforApproved(ddView);
				dDetailsView.add(ddView);
				
			}
//			if(getIsApproved() || getIsReviewed() || getIsCaringRejected() || getIsFinanceRejected())
//			{
//				
//				doSaveApplication( getStatusApprovedId() );
//			}
			List<DisbursementDetailsView> list = getDisbursementDetails();
			checkReviewRequired(list );
			setDisbursementDetails(list );
			setTotalRows( list .size());
			restoreToDefault();
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_BENF_UPDATE_SUCC));
		}
		catch(Exception ex)
		{
			logger.LogException( "saveBeneficiary|Crashed|", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public void showInheritanceFilePopup() {
		try	 {			
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, getRequestView().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			String javaScriptText ="var screen_width = 900;"+
            	"var screen_height =600;"+
            	"window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    sendToParent( javaScriptText);		    		    
		}
		catch (Exception ex)  {
			logger.LogException("[Exception occured in showInheritanceFilePopup()]", ex);
			ex.printStackTrace();
		}
	}
	
	public void loadPaymentDetails(ActionEvent evt) 
	{
		try
		{
					DisbursementDetailsView view = (DisbursementDetailsView) sessionMap.get(WebConstants.LOAD_PAYMENT_DETAILS);				
					sessionMap.remove(WebConstants.LOAD_PAYMENT_DETAILS);
					boolean bFinanceTab = false;
					if( sessionMap.containsKey( WebConstants.FinanceTabPublishKeys.IS_FINANCE)) 
					{
						sessionMap.remove( WebConstants.FinanceTabPublishKeys.IS_FINANCE);
						bFinanceTab = true;
					}
					if( bFinanceTab ) 
					{																						
						FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
						if( null!=tabFinanceController ) 
						{
							tabFinanceController.loadPaymentDetails( view);
						}
					} 
					else 
					{
						PaymentCriteriaView criteriaView = view.getPaymentCriteriaView();
						List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
						for (DisbursementDetailsView dtView : disbursementDetails) 
						{
							
							if (areDisbursementDetailSame(view, dtView)) 
							{
								List<PaymentDetailsView> listPaymentDetailsView = dtView.getPaymentDetails();
								if( null==listPaymentDetailsView ) {
									listPaymentDetailsView = new ArrayList<PaymentDetailsView>();
									dtView.setPaymentDetails( listPaymentDetailsView);
								}
								
								PaymentDetailsView paymentView = null;
								if( 0<listPaymentDetailsView.size() ) {
									paymentView = listPaymentDetailsView.get( 0);
								} else {
									paymentView = new PaymentDetailsView();
									paymentView.setMyHashCode(paymentView.getMyHashCode()); 
									paymentView.setStatus( WebConstants.NOT_DISBURSED);
									paymentView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
								}
								
								paymentView.setAccountNo(criteriaView.getAccountNumber());
								paymentView.setAmount(criteriaView.getAmount());
								paymentView.setBankId(criteriaView.getBankId());
								if (null != criteriaView.getPaymentMode()) {
									paymentView.setPaymentMethod(new Long(criteriaView
											.getPaymentMode()));
								}
								if( null!=criteriaView.getPaidTo()) {
									paymentView.setPaidTo( new Long( criteriaView.getPaidTo()));
									paymentView.setOthers( null );
								}
								else if ( criteriaView.getOther() != null && criteriaView.getOther().trim().length() >0 )
								{
									paymentView.setOthers(criteriaView.getOther().trim() );
									paymentView.setPaidTo(  null );
								}
								paymentView.setAmount(dtView.getAmount());
								paymentView.setRefDate(criteriaView.getDate());
								paymentView.setReferenceNo(criteriaView.getReferenceNumber());
								if( 0==listPaymentDetailsView.size() ) {
									listPaymentDetailsView.add( paymentView);
								}
								successMessages.add(CommonUtil						
													.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_PAYMENT_UPDATED));
							}
						}
					}
			} catch (Exception ex) {
				logger.LogException("[loadPaymentDetails |Exception|", ex);
				errorMessages = new ArrayList<String>(0);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
	}
	
	public void handlePopupChange() 
	{
		try
		{
			DisbursementDetailsView disbursements = (DisbursementDetailsView) this.sessionMap.remove( WebConstants.LOAD_PAYMENT_DETAILS);
			List<DisbursementDetailsView> list = getDisbursementDetails();
			for(DisbursementDetailsView singleDisbursement : list) 
			{
				if( areDisbursementDetailSame( disbursements, singleDisbursement ) ) 
				{
					list.remove( singleDisbursement);
					list.add( disbursements);
					singleDisbursement = null;
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_BENEF_UPDATE));
					break;
				}			
			}
		}
		catch (Exception ex) 
		{
			logger.LogException("[handlePopupChange|Exception|", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private boolean areDisbursementDetailSame(DisbursementDetailsView disbursements,DisbursementDetailsView loopedDisbursement) 
	{
		//If looped disbursement doesnt have DisDetId then compare the hashCodes
		return loopedDisbursement.getDisDetId()== null ?loopedDisbursement.getId() == disbursements.getId():
														loopedDisbursement.getDisDetId().equals( disbursements.getDisDetId() ) ;
	}
	
	public Boolean getIsEnglishLocale() {
		return this.isEnglishLocale();
	}
	
	
	private boolean validateAccept() {
		List<DisbursementDetailsView> disbursement = getDisbursementDetails();
		boolean hasAnySelected = false;
		for(DisbursementDetailsView single : disbursement) 
		{
			if( single.getSelected() != null && single.getSelected() ) 
			{
				hasAnySelected = true;
//				if( single.getIsMultiple().equals( WebConstants.IS_MULTIPLE_TRUE) && 0==single.getBeneficiaryCount() )
//				{
//					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_ACCCEPT_ERR));
//					tabPanel.setSelectedTab(TAB_DETAILS);
//					return false;
//				}
			}
		}
		if( !hasAnySelected )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "mems.normaldisb.err.msg.approveRejectDisbursement.selectionRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
             return false;
		}
		return true;
	}
	
	public void acceptDisbursement() 
	{
		try
		{
			if( validateAccept() ) 
			{
				Long acceptId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY,
															WebConstants.DisbursementStatus.ACCEPTED);
				DomainDataView dataView = CommonUtil.getDomainDataView( WebConstants.DisbursementStatus.DOMAIN_KEY, acceptId); 		
				List<DisbursementDetailsView> list = getDisbursementDetails();
				boolean hasCostCenter =true;
				for(DisbursementDetailsView singleDisbursement : list) 
				{
					if( singleDisbursement.getSelected() == null || !singleDisbursement.getSelected() ) continue; 
					if(  singleDisbursement.getRequestBeneficiaries()!=null ) 
					{
						for(DisbursementRequestBeneficiaryView singleBeneficiary : singleDisbursement.getRequestBeneficiaries() ) 
						{
								if ( !validateCostCenter(singleBeneficiary.getPersonId().toString()) )
								{
									hasCostCenter =false;
									break;
								}
						}
					}
					if( !hasCostCenter )
					{
						break;
					}
					else
					{
						singleDisbursement.setStatusEn( dataView.getDataDescEn());
						singleDisbursement.setStatusAr( dataView.getDataDescAr());
						singleDisbursement.setStatus( acceptId);
						singleDisbursement.setSelected( false);
					}
				}
				if( hasCostCenter )
				{
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_MSG_ACCEPT));
				}
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in save()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void rejectDisbursement() {
		Long rejectedId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY, 
													  WebConstants.DisbursementStatus.REJECTED);
		DomainDataView dataView = CommonUtil.getDomainDataView( WebConstants.DisbursementStatus.DOMAIN_KEY, rejectedId);		
		List<DisbursementDetailsView> list = getDisbursementDetails();
		boolean hasAnySelected  = false;
		for(DisbursementDetailsView singleDisbursement : list) 
		{
			if( singleDisbursement.getSelected() != null && singleDisbursement.getSelected() ) 
			{
				hasAnySelected = true;
				singleDisbursement.setStatusAr( dataView.getDataDescAr());
				singleDisbursement.setStatusEn( dataView.getDataDescEn());
				singleDisbursement.setStatus( rejectedId);
				singleDisbursement.setSelected( false);
			}
		}
		if( !hasAnySelected )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "mems.normaldisb.err.msg.approveRejectDisbursement.selectionRequired" ) );
             
		}
		else
		{
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.SUCC_MSG_REJECT));
		}
		
	}
	
	
	public void deleteSelected() 
	{
		try
		{
			List<DisbursementDetailsView> list = getDisbursementDetails();
			for(DisbursementDetailsView singleDisbursement : list) 
			{
				if( 
					singleDisbursement.getSelected() == null || 
					!singleDisbursement.getSelected() 
				  )
				{
					continue;
				}
				
				if( null!=singleDisbursement.getDisDetId() ) 
				{
					singleDisbursement.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					getToDeleteList().add(singleDisbursement);
				} 
			}
			for (DisbursementDetailsView singleDisbursement  : getToDeleteList()) 
			{
				getDisbursementDetails().remove(singleDisbursement);
			}
		
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_DELETE_SUCC));
			setTotalRows( getDisbursementDetails().size() );
			setDisbursementDetails( getDisbursementDetails() );
		} 
		catch(Exception ex) {
			logger.LogException("[Exception occured in deleteSelected()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void deleteBeneficiary() {
		try {
			DisbursementDetailsView detailView = (DisbursementDetailsView) disbursementTable.getRowData();
			if( null!=detailView )
			{
				if( null!=detailView.getDisDetId() ) 
				{
					detailView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
					getToDeleteList().add(detailView);
				} 
				if( getDisbursementDetails().remove(detailView)) 
				{
					successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_BENF_DELETE_SUCC));
					setTotalRows( getDisbursementDetails().size());
				}
				setDisbursementDetails(getDisbursementDetails() );
			}			
		} catch(Exception ex) {
			logger.logInfo("[Exception occured in deleteBeneficiary()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void addPaymentScheduleFromPopup() throws Exception
	{
		try
		{
			logger.logInfo("[addPaymentScheduleFromPopup() .. Starts]");		
			DisbursementDetailsView disbursement = (DisbursementDetailsView) disbursementTable.getRowData();
			if( getRequestView().getInheritanceFileId() != null )
			{
				
				sessionMap.put(  WebConstants.InheritanceFile.INHERITANCE_FILE_ID,getRequestView().getInheritanceFileId().toString() );
			}
			
			if( getIsApproved() ) {
				sessionMap.put( WebConstants.MemsNormalDisbursements.RENDER_PAYMENT_ICON, true);
			} else {
				sessionMap.put( WebConstants.MemsNormalDisbursements.RENDER_PAYMENT_ICON, false);
			}
			
			sessionMap.put( WebConstants.MemsNormalDisbursements.LOAD_PAYMENT_SCHEDULE, disbursement);
			viewMap.put( PAYMENT_SCH_UPDATE, disbursementTable.getRowIndex());		
			String javaScriptText = " var screen_width = 900;"
				+ "var screen_height = 500;"
				+ "var popup_width = screen_width-200;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('addPaymentSchedule.jsf?viewMode=popup"
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
			sendToParent( javaScriptText);
			logger.logInfo("[addPaymentScheduleFromPopup() .. Ends]");
		} catch(Exception ex) {
			logger.logInfo("[Exception occured in addPaymentScheduleFromPopup()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public void addBeneficiaryFromPopup() 
	{
		try
		{
			DisbursementDetailsView disbursement = (DisbursementDetailsView) disbursementTable.getRowData();
			boolean readOnly = false;
			readOnly = getIsCompleted() || getIsDisbursementRequired() ;
            openMultipleBeneficiaryPopup( disbursement,readOnly );	
			
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in addBeneficiaryFromPopup]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private void openMultipleBeneficiaryPopup( DisbursementDetailsView disbursement,boolean readOnly )throws Exception
	{
		sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, disbursement);
		sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, getRequestView().getInheritanceFileId());
		if(readOnly)
		{
			sessionMap.put( WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
		}
		sendToParent( "javaScript:openAddMultipleBenefificiaryForDisbursementPopup();");
	}
	private void sendToParent(String javaScript) 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScript);
	}
	
	public void editBeneficiary() 
	{
		try
		{

			DisbursementDetailsView selectedRow = (DisbursementDetailsView) disbursementTable.getRowData();
			if ( null!=selectedRow )
			{
				populateWithSelectedRow( selectedRow);
				viewMap.put(IS_UPDATE, selectedRow);
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in editBeneficiary()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	
	public void editDetails() {
		try
		{
			DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();
			if( null!=detailData.getPaymentDetails() && 1<detailData.getPaymentDetails().size()) 
			{
				addPaymentScheduleFromPopup();			
			}
			else 
			{
				editPaymentDetails();
			}
		} catch (Exception ex) {
			logger.LogException("[Exception occured in editPaymentDetails()]", ex);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void editPaymentDetails() throws Exception
	{
			DisbursementDetailsView detailData = (DisbursementDetailsView) disbursementTable.getRowData();
			
			String fetchPersonsById = "";
			if( detailData.getRequestBeneficiaries()!= null && detailData.getRequestBeneficiaries().size() > 0  )
			{
			DisbursementRequestBeneficiaryView beneficiary = detailData.getRequestBeneficiaries().get(0);
			fetchPersonsById =  WebConstants.PERSON_ID+ "="+ beneficiary.getPersonId().toString();
			}
			else if( getRequestView().getInheritanceFileId() != null )
			{
				
				fetchPersonsById =  WebConstants.InheritanceFile.INHERITANCE_FILE_ID+ "="+ getRequestView().getInheritanceFileId().toString();
			}
			
			
			sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, detailData);
			if( detailData.getIsMultiple()!= null && detailData.getIsMultiple().longValue() == 1 )
			{
				sessionMap.put( WebConstants.MULTIPLE_BENEFICIARIES, "1" );
				sessionMap.put( WebConstants.DISBURSEMENT_DETAIL_ID, detailData.getDisDetId() );
			}
			String javaScriptText = " javaScript:openPaymentDetailsPopUp( '"+fetchPersonsById+"');";
			sendToParent(javaScriptText);
		
	}
	
	/* validators starts here */
	
	private boolean validateSaveBeneficiary() throws Exception 
	{
		boolean bValid = true;		
//		paymentSourceSelfAccount = new UtilityService().getDomainDataByValue(WebConstants.PaymentSrcType.DOMAIN_KEY, WebConstants.PaymentSrcType.SELF_ACCOUNT);
		
		/* in the conditions only updation is allowed */
//		if ( null==getIsUpdate() && (getIsApprovalRequired() || getIsReviewed() || getIsApproved()) ) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_SEL_BENEF));
//			tabPanel.setSelectedTab(TAB_DETAILS);
//			return false;
//		}
		
		
//		if( null==getIsUpdate() ) 
//		{
//			List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
//			for (DisbursementDetailsView singleDetailView : dDetailsView) 
//			{
//				if (singleDetailView.getPersonId().toString().equals( cmbBeneficiaryList.getValue().toString())) 
//				{		
//					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.
//																	ERR_BENF_ALREADY_EXISTS));
//					bValid = false;
//					tabPanel.setSelectedTab(TAB_DETAILS);
//					break;
//				}
//			}
//		}
		if (
				!cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)&& 
				!cmbBeneficiaryList.getValue().toString().equals( CMB_ALL_BENEF) &&
				!validateCostCenter(cmbBeneficiaryList.getValue().toString()) 
			)
		{
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;
		}
		if( null==cmbPaymentTypes.getValue() || cmbPaymentTypes.getValue().toString().equals(CMB_DEFAULT_VAL)) 
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_PAYMENT_TYPE_REQ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			bValid = false;
		}
		if(txtAmount.getValue() != null && txtAmount.getValue().toString().trim().length() > 0)
		{
			try
			{	// this is just to raise the numeric exception if the second if block was missed
				Double.parseDouble(txtAmount.getValue().toString().trim()); 
				
			}
			catch (NumberFormatException ex) 
			{
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsInvestmentRequestMsgs.ERR_AMNT_NUMERIC));
					tabPanel.setSelectedTab(TAB_DETAILS);
					//return here so it wont crash below while converting the txtAmount to double
					bValid = false;
					return bValid ;
					
			}
		}
		//If its self account than amount is mandatory else its not.
        if(cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID ) ) )
		{ 
			if ( null==txtAmount.getValue() || 0==txtAmount.getValue().toString().trim().length() ) 
			{
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_AMNT_REQ));
				tabPanel.setSelectedTab(TAB_DETAILS);
				bValid = false;
				
			}
			
			if(!cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL) && !cmbBeneficiaryList.getValue().toString().equals( CMB_ALL_BENEF) )
			{
				Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
					
				 int validateBalanceCode = validateBalance(
						 									 getBalanceMap().get( beneficiaryId ) ,
															 getBlockingMap().get( beneficiaryId ),
															 DisbursementService.getRequestedAmount(beneficiaryId, getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
															 Double.parseDouble(txtAmount.getValue().toString().trim()),
															 beneficiaryId,
															 null,
															 getRequestView().getInheritanceFileId()
				   										  ) ;

				 if ( validateBalanceCode != 0 )
				 {
					 tabPanel.setSelectedTab(TAB_DETAILS);
					 return false;
				 }

			}			
			
	    }
			
		
		if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_BENEFF_MAND));
			tabPanel.setSelectedTab(TAB_DETAILS);
			bValid = false;
		}
		else 
		{
			if( !validateCostCenter(cmbBeneficiaryList.getValue().toString() ) )
			{
				bValid =  false;
			}
		}
		if(this.getRequestView().getStatusId().compareTo(-1l)==0 )
		{
			if ( this.getSelectOneMainReason() == null || this.getSelectOneMainReason().equals("-1") ) 
			{
				errorMessages.add( CommonUtil.getBundleMessage( "normaldisbursement.msg.reasonRequired" ));
				tabPanel.setSelectedTab(TAB_DETAILS);
				bValid = false;
			}
			else if(this.getSelectOneMainReason() != null && this.getSelectOneMainReason().equals("217001") )
			{
				errorMessages.add( CommonUtil.getBundleMessage( "normaldisbursement.msg.otherReasonNotAllowed" ));
				tabPanel.setSelectedTab(TAB_DETAILS);
				bValid = false;
			}
		}
		if( null==txtDescription.getValue() || 0==txtDescription.getValue().toString().trim().length() ) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants
										.MemsNormalDisbursementMsgs.ERR_DESC_REQ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			bValid = false;
		}
		
		if ( !bValid ){ return false;  }
		List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
		for (DisbursementDetailsView singleDetailView : dDetailsView) 
		{
			
		    if(  singleDetailView.getSrcType().toString().equals( cmbPaymentSource.getValue().toString() )){ continue;	}
		    DisbursementDetailsView updateView  =getIsUpdate(); 
		    if (updateView != null && singleDetailView.hashCode()==updateView.hashCode()){ continue;	}
		    
			String msgKey = "disbursement.msg.disbursementSelfSourceOnly";
			if ( singleDetailView.getSrcType().longValue() == WebConstants.PaymentSrcType.AID_ID )
			{
				msgKey = "disbursement.msg.disbursementAidSourceOnly";
			}
			errorMessages.add( CommonUtil.getBundleMessage(  msgKey ) );
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;
       }
		
									
		return bValid;
	}
	
	private boolean validateInput() throws Exception {
		boolean bValid = true;
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return false;
		}
		if( getDisbursementDetails() == null || getDisbursementDetails().size() <= 0) 
		{
			errorMessages.add( CommonUtil.getBundleMessage("mems.normaldisb.err.msg.atleastOneDisDetailRequired") );
			tabPanel.setSelectedTab( TAB_DETAILS );
			return false;
		}
		return bValid;
	}
	
	
	/** The function populates the controls with the disbursement 
	 * @param disbursement
	 */
	private void populateWithSelectedRow(DisbursementDetailsView disbursement)throws Exception 
	{
		Long paymentType = new Long( disbursement.getPaymentType().toString());
		Long monthlyAlimonyId = CommonUtil.getDomainDataId( WebConstants.MemsPaymentTypes.DOMAIN_KEY, 
															WebConstants.MemsPaymentTypes.INCREASING_MONTHLY_ALIMONY);
		if( disbursement.getMainReasonId() != null )
		{
			this.setSelectOneMainReason(disbursement.getMainReasonId().toString());
			handleMainReasonChange();
		}
		else
		{
			
		}
		if( paymentType.equals( monthlyAlimonyId)) 
		{
			cmbPaymentSource.setReadonly( true);
			cmbPaymentSource.setStyleClass( CSS_READONLY);
		} else {
			cmbPaymentSource.setReadonly( false);
			cmbPaymentSource.setStyleClass("");
		}
		
		cmbPaymentSource.setValue( disbursement.getSrcType().toString());
		cmbPaymentTypes.setValue( disbursement.getPaymentType().toString());
		if(disbursement.getDisburseSrc() != null && !disbursement.getDisburseSrc().toString().equals("-1"))
		{
			cmbDisbursementSource.setValue( disbursement.getDisburseSrc().toString() );
		}
		
		txtAmount.setValue( disbursement.getAmount().toString());
		txtDescription.setValue( disbursement.getDescription());
		if( null!=disbursement.getReasonId() ) 
		{
			cmbReasonTypes.setValue( disbursement.getReasonId().toString());
			handleReasonChange();
			if( null!=disbursement.getItemId() ) 
			{
				cmbItemTypes.setValue( disbursement.getItemId().toString());
			}
		}
		if( disbursement.getIsMultiple().equals( WebConstants.IS_MULTIPLE_TRUE)) 
		{
			cmbBeneficiaryList.setValue( CMB_ALL_BENEF);
			if( getIsFinanceRejected() || getIsReviewed() )
			{
			cmbBeneficiaryList.setDisabled(true);
			}
			noOfBenef.setReadonly(false);
			noOfBenef.setStyleClass("");
			if( disbursement.getNoOfBeneficiaries() != null )
			{
				noOfBenef.setValue( disbursement.getNoOfBeneficiaries().toString() );
			}
		} 
		else 
		{
			noOfBenef.setReadonly(true);
			noOfBenef.setStyleClass("READONLY");
			cmbBeneficiaryList.setValue( disbursement.getPersonId().toString());
		}		
		
		//cmbBeneficiaryList.setDisabled( true);
		
		Long beneficiaryId = new Long( disbursement.getPersonId().toString());
		Map<Long, Double> balanceMap = getBalanceMap();
		Double amount = 0.00d;
		Double blockingMapAmount = 0.00d;
		if( balanceMap.containsKey( beneficiaryId) ) {
			amount = balanceMap.get( beneficiaryId);
			accountBalance.setValue( amount.toString());
		}
		
		Map<Long, Double> blockingMap = getBlockingMap();
		if( blockingMap.containsKey( beneficiaryId) ) 
		{
			blockingMapAmount = blockingMap.get( beneficiaryId);
			blockingBalance.setValue( blockingMapAmount.toString());
			
		}
		Double requestAmount = DisbursementService.getRequestedAmount(disbursement.getPersonId(), getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null);
		requestedAmount.setValue( requestAmount );
		
		
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
		
		/* this check will only be invoked when the request is approved to handle the status of four 
		 * additional control which are rendered only when the request is approved */
		if( getIsApproved() || getIsFinanceRejected()) {
			renderControlsForApprovedMode( paymentType.toString(),monthlyAlimonyId.toString(),disbursement.getSrcType().toString(),
					                       disbursement.getDisburseSrc(),disbursement.getPaidTo() ,disbursement.getVendorName() );
		}
	}
	
	private void renderControlsForApprovedMode( String paymentType,String monthlyAlimonyId,String paymentSource,Long disbursementSrc,
				                                 String paidTo,String vendorName
				                                )
	{
		if( paymentType.equals( monthlyAlimonyId) ) 
		{
			cmbDisbursementSource.setStyleClass( CSS_READONLY);
			cmbDisbursementSource.setReadonly( true);
			cmbDisbursementSource.setValue( CMB_DEFAULT_VAL);				
			cmbPaidTo.setStyleClass( CSS_READONLY);
			cmbPaidTo.setReadonly( true);					
			cmbPaidTo.setValue( WebConstants.PAID_TO_BENEFICIARY);
			imgVendor.setRendered( false);
		}
		else 
		{
			/* payment type is disbursement disable the disbursement source button if the payment source is
			 * self account 
			 */
			Long selfAccountId = CommonUtil.getDomainDataId( WebConstants.PaymentSrcType.DOMAIN_KEY, 
															 WebConstants.PaymentSrcType.SELF_ACCOUNT);
																 
			if( paymentSource.compareTo( selfAccountId.toString() ) ==0 )
			{
				cmbDisbursementSource.setStyleClass( CSS_READONLY);
				cmbDisbursementSource.setReadonly( true);
				cmbDisbursementSource.setValue( CMB_DEFAULT_VAL);
			} else {
				cmbDisbursementSource.setReadonly( false);
				cmbDisbursementSource.setStyleClass("");					
			}
			if( null!=disbursementSrc ) {
				cmbDisbursementSource.setValue( disbursementSrc.toString() );
			}
			if( null!=paidTo ) {
				cmbPaidTo.setValue( paidTo );
			}
			cmbPaidTo.setStyleClass("");
			cmbPaidTo.setReadonly( false);			
		}
		if( null!=vendorName ) {
			txtVendor.setValue( vendorName );
			imgVendor.setRendered( true);
		} else {
			txtVendor.setValue("");
		}
	}
	private void populateWithUserInput(DisbursementDetailsView ddView) throws Exception 
	{
		double amnt = Double.parseDouble(txtAmount.getValue().toString());
		ddView.setFollowUpView( getFollowUp() );
		ddView.setOriginalAmount(amnt);
		ddView.setDescription(txtDescription.getValue().toString());
		ddView.setPaymentType(new Long(cmbPaymentTypes.getValue().toString()));
		DomainDataView domain = CommonUtil.getDomainDataView(WebConstants.MemsPaymentTypes.DOMAIN_KEY, ddView.getPaymentType());
		ddView.setPaymentTypeEn( domain.getDataDescEn());
		ddView.setPaymentTypeAr( domain.getDataDescAr());
		if( this.getSelectOneMainReason() != null && !this.getSelectOneMainReason().equals("-1") )
		{
			ddView.setMainReasonId( Long.valueOf(this.getSelectOneMainReason()));
		}
		if( getIsApprovalRequired() || getIsReviewed() ) 
		{
			//ddView.setOriginalAmount( amnt);
			ddView.setAmount( amnt);
		}
		else 
		{
			ddView.setAmount( amnt);
		}
		String beneficiaryName = "";
		List<SelectItem> beneficiaryList = this.getBeneficiaryList();
		for(SelectItem singleItem : beneficiaryList) 
		{
			if(singleItem.getValue().toString().equals( cmbBeneficiaryList.getValue().toString() ) ) 
			{
				beneficiaryName = singleItem.getLabel();
				break;
			}
		}
		if( !cmbBeneficiaryList.getValue().toString().equals(CMB_ALL_BENEF) ) 
		{
			ddView.setIsMultiple( 0l );
			ddView.setNoOfBeneficiaries(1l);
			List<DisbursementRequestBeneficiaryView> requestBeneficiaries = ddView.getRequestBeneficiaries();
			if( null==requestBeneficiaries ) 
			{
				requestBeneficiaries = new ArrayList<DisbursementRequestBeneficiaryView>();
				ddView.setRequestBeneficiaries(requestBeneficiaries);
			}
			boolean benefFound=false;
			for(DisbursementRequestBeneficiaryView benef : requestBeneficiaries)
			{
				if( benef.getPersonId() !=null  && benef.getPersonId().toString().equals( cmbBeneficiaryList.getValue().toString() ) )
				{
				  benef.setAmount(new Double(txtAmount.getValue().toString()));
				  benefFound=true;
				  
				}
				else
					benef.setIsDeleted(1l);
			}
			if( !benefFound  )
			{
				DisbursementRequestBeneficiaryView benfView = new DisbursementRequestBeneficiaryView();
				benfView.setName(beneficiaryName);
				benfView.setAmount(new Double(txtAmount.getValue().toString()));
				benfView.setPersonId(Long.parseLong(cmbBeneficiaryList.getValue().toString()));
				benfView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED);
				requestBeneficiaries.add(benfView);
			}
		} 
		else 
		{
			ddView.setIsMultiple( WebConstants.IS_MULTIPLE_TRUE);
			ddView.setBeneficiariesName("");
			if( 
				noOfBenef.getValue() != null && 
				noOfBenef.getValue().toString().trim().equals("")&&
				noOfBenef.getValue().toString().length()>0 )
			{
				ddView.setNoOfBeneficiaries( new Long( noOfBenef.getValue().toString() ) ); 
			}
//			List<DisbursementRequestBeneficiaryView> requestBeneficiaries = ddView.getRequestBeneficiaries();
//			if( null!=requestBeneficiaries ) 
//			{
//				for(DisbursementRequestBeneficiaryView benef : requestBeneficiaries)
//				{
//						benef.setIsDeleted(1l);
//				}
//			}
		}
		if( null!=cmbReasonTypes.getValue() && !cmbReasonTypes.getValue().toString().equals(CMB_DEFAULT_VAL) ) 
		{
			ddView.setReasonId( new Long( cmbReasonTypes.getValue().toString()));
			DisbursementReasonView reason = getReasonMap().get(ddView.getReasonId());
			ddView.setReasonAr( reason.getReasonAr());
			ddView.setReasonEn( reason.getReasonEn());
			ddView.setReasonId( reason.getReasonId());
			ddView.setIsReviewReq( reason.getIsReviewReq());
		}			
		if( null!=cmbItemTypes.getValue() && !cmbItemTypes.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			ddView.setItemId( new Long(cmbItemTypes.getValue().toString()));
			ItemTypeView item = getItemsMap().get( ddView.getItemId());
			if(item != null )
			{
				ddView.setItemEn( item.getTypeNameEn());
				ddView.setItemAr( item.getTypeNameAr());
			}
		}
		if( null!=cmbPaymentSource.getValue() && !cmbPaymentSource.getValue().toString().equals(CMB_DEFAULT_VAL) ) {
			ddView.setSrcType(new Long(cmbPaymentSource.getValue().toString()));
			DomainDataView domainData = CommonUtil.getDomainDataView(
													WebConstants.PaymentSrcType.DOMAIN_KEY,ddView.getSrcType() ); 
			ddView.setSrcTypeEn( domainData.getDataDescEn());
			ddView.setSrcTypeAr( domainData.getDataDescAr());																		
		}
		if(getIsApproved() || getIsReviewed() || getIsCaringRejected() || getIsFinanceRejected())
		{
			
			Long acceptId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY,WebConstants.DisbursementStatus.ACCEPTED);
			DomainDataView dataView = CommonUtil.getDomainDataView( WebConstants.DisbursementStatus.DOMAIN_KEY, acceptId);
			ddView.setStatusEn( dataView.getDataDescEn());
			ddView.setStatusAr( dataView.getDataDescAr());
			ddView.setStatus( acceptId);
		
		}
	}
	
	private DisbursementDetailsView getDefaultDisbDetailView() 
	{
		DisbursementDetailsView ddView = new DisbursementDetailsView();
		ddView.setId(this.hashCode());
		ddView.setCreatedBy(CommonUtil.getLoggedInUser());
		ddView.setCreatedOn(new Date());
		ddView.setUpdatedBy(CommonUtil.getLoggedInUser());
		ddView.setUpdatedOn(new Date());
		ddView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		ddView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		
		ddView.setIsDisbursed(WebConstants.NOT_DISBURSED);
		ddView.setPaidTo( WebConstants.PAID_TO_BENEFICIARY);
		ddView.setHasInstallment( WebConstants.HAS_INSTALLMENT_DEAFULT);
		ddView.setStatus( CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY,
																WebConstants.DisbursementStatus.NEW));
		
		DomainDataView disbursementStatus = CommonUtil.getDomainDataView( WebConstants.DisbursementStatus.DOMAIN_KEY,
																		  ddView.getStatus());			
		ddView.setStatusEn( disbursementStatus.getDataDescEn());
		ddView.setStatusAr( disbursementStatus.getDataDescAr());
							
		ddView.setSrcType( CommonUtil.getDomainDataId( WebConstants.PaymentSrcType.DOMAIN_KEY,
															WebConstants.PaymentSrcType.SELF_ACCOUNT));
									
		DomainDataView domainData = CommonUtil.getDomainDataView(WebConstants.PaymentSrcType.DOMAIN_KEY, 
														ddView.getSrcType() );
		
		ddView.setSrcTypeAr(domainData.getDataDescAr());
		ddView.setSrcTypeEn(domainData.getDataDescEn());
		ddView.setIsMultiple( WebConstants.DEFAULT_IS_MULTIPLE);
		ddView.setId( ddView.hashCode());
		return ddView;
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	public void onOpenEmailPopup() 
	{
		try
		{

			DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
			List<DisbursementDetailsView> ddViewList =new ArrayList<DisbursementDetailsView>();
			ddViewList.add(gridViewRow) ;
			sessionMap.put(WebConstants.REQUEST , getRequestView() );
			sessionMap.put("Disbursements", ddViewList);
			executeJavaScript("javaScript:openPayVendorNotificationPopup();");
			
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenEmailPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenBeneficiaryDisbursementDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			Long requestId = -1l;
			if( getRequestView().getRequestId() != null )
			{
				requestId  = getRequestView().getRequestId();
			}
			executeJavaScript(
								  "javaScript:openBeneficiaryDisbursementDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+
								  														getRequestView().getInheritanceFileId()+"','"+ 
								  														requestId+
								  												 "');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{

			if( null==cmbBeneficiaryList.getValue() || cmbBeneficiaryList.getValue().toString().equals(CMB_DEFAULT_VAL)) {
			return;
			}
				
			executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+cmbBeneficiaryList.getValue() +"','"+getRequestView().getInheritanceFileId()+"');"
								 );
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private boolean validateApprove(RequestView requestView) throws Exception
	{
		Long statusId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY, 
													WebConstants.DisbursementStatus.NEW);
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		boolean isInheritanceRelatedTask=false;
		for(DisbursementDetailsView disbursement : getDisbursementDetails() ) 
		{
			if( 
				!isInheritanceRelatedTask && 
				disbursement.getMainReasonId() != null && 
				disbursement.getMainReasonId()  == 217003l
			  )
			{
				isInheritanceRelatedTask =  true;
				logger.logDebug("isInheritanceRelatedTask|");
			}
			if( disbursement.getStatus().equals( statusId)) 
			{
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_APPROVE));
				tabPanel.setSelectedTab(TAB_DETAILS);
				return false;
			}
			
			if( disbursement.getRequestBeneficiaries() != null )
			{
				double disReqBenefAmount = 0.0d;
				for (DisbursementRequestBeneficiaryView reqBenefView : disbursement.getRequestBeneficiaries() ) 
				{
					if( reqBenefView.getIsDeleted()!= null && reqBenefView.getIsDeleted().compareTo(1l)==0 )continue;
					
					
					if( 
							disbursement.getStatus().compareTo(WebConstants.DisbursementStatus.REJECTED_ID)!= 0 &&
							!validateCostCenter( reqBenefView.getPersonId().toString() ) 
					  )
					{
						tabPanel.setSelectedTab(TAB_DETAILS);
						return false;
					}
					else
					{
						disReqBenefAmount += reqBenefView.getAmount();
					}
					//If its self account than amount is mandatory else its not.
			        if(disbursement.getSrcType().compareTo(   WebConstants.PaymentSrcType.SELF_ACCOUNT_ID  ) == 0 )
					{ 
							Long beneficiaryId = new Long( reqBenefView.getPersonId().toString());
							 int validateBalanceCode = validateBalance(
									 									 getBalanceMap().get( beneficiaryId ) ,
																		 getBlockingMap().get( beneficiaryId ),
																		 DisbursementService.getRequestedAmount(beneficiaryId, getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null),
																		 reqBenefView.getAmount(),
																		 beneficiaryId,
																		 reqBenefView.getName(),
																		 getRequestView().getInheritanceFileId()
							   										  ) ;
							 if ( validateBalanceCode != 0 )
							 {
								 tabPanel.setSelectedTab(TAB_DETAILS);
								 return false;
							 }
					}		
				}
//				logger.logDebug("validating Disbursement Amount:%s| Sum benef Amount:%s|On comparison:%s",disbursement.getAmount(),
//																			 							 Double.valueOf( oneDForm.format( disReqBenefAmount ) ),
//																			 							 disbursement.getAmount().compareTo(  Double.valueOf( oneDForm.format( disReqBenefAmount ) )  
//																					 ) 
//							   ) ;
				if( disbursement.getAmount().compareTo(  Double.valueOf( oneDForm.format( disReqBenefAmount ) ) ) != 0  )
				{
					errorMessages.add( CommonUtil.getBundleMessage( "disburseFinance.msg.sumBenefAmountNotEqualDisburseAmount" ));
					tabPanel.setSelectedTab(TAB_DETAILS);
					return false;
				}
			}
			
		}
		
//		if( isInheritanceRelatedTask && !hasInheritanceHeadReviewed( requestView.getRequestId() ) )
//		{
//			errorMessages.add( CommonUtil.getBundleMessage( "normaldisbursement.msg.inheritanceHeadReviewRequired" ));
//			tabPanel.setSelectedTab(TAB_DETAILS);
//			return false;
//		}
		
		return true;
	}
    
    @SuppressWarnings("unchecked")
    public boolean hasInheritanceHeadReviewed(Long requestId) throws Exception
    {

    		org.hibernate.Session session = (org.hibernate.Session )EntityManager.getBroker().getSession();
            String queryStr= " SELECT  GETREQREVIEWBYINHERHEAD("+requestId+")as RESULT  from dual";
            logger.logDebug("hasInheritanceHeadReviewed|query:%s", queryStr);
            Query query = session.createSQLQuery(queryStr).addScalar("result" , new TextType()).setResultTransformer(Transformers.aliasToBean(ExecFunc.class));
              
            List<ExecFunc> list = (ArrayList<ExecFunc>)query.list();
            if( 
            		list.size() > 0 && 
            		list.get(0) != null && 
            		!list.get(0).getResult().equals( "-1" ) 
               )
            {
                    return true;
            }
            return  false;
                 
    }
	private boolean validateSubmitQuery() throws Exception
	{
		if (0 == getDisbursementDetails().size()) 
		{
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_ONE_BENEF_REQ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return false;
		}		
		return true;
	}
		
	public void onPrint()
    {
    	try
    	{
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	public void submitQuery() 
	{
		try {
			Long statusId = getRequestView().getStatusId();			
			if( validateSubmitQuery() && doSaveApplication( getStatusApprovalRequiredId()) ) 
			{
				if ( statusId.equals( INVALID_STATUS) ) 
				{
					CommonUtil.saveSystemComments( WebConstants.REQUEST, 
												   MessageConstants.RequestEvents.REQUEST_CREATED,
												   getRequestView().getRequestId());													
				}
//				invokeBpel( getRequestView().getRequestId());
				invokeWorkFlow( getRequestView().getRequestId(),WebConstants.PROCEDURE_TYPE_MEMS_NORMAL_PAYMENTS);
				refreshTabs(getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST,
											   MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL,
											   getRequestView().getRequestId());
				CommonUtil.printMemsReport( this.getRequestView().getRequestId() );																	
				successMessages.add( CommonUtil.getBundleMessage( 
											   MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SUBMIT));
				pageModeView=true;
			}
			
		} catch (Exception ex) {
			logger.LogException("[Exception occured in submitQuery()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));											
		}
	}

	
	public void save() {
		try {
			Long statusId = getRequestView().getStatusId();
			if( doSaveApplication( getStatusNewId()) ) {
				if( statusId.equals( INVALID_STATUS)) {
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
												   MessageConstants.RequestEvents.REQUEST_CREATED,
												   getRequestView().getRequestId());
				}
				refreshTabs( getRequestView());
				successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_SAVE));																
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in save()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public boolean validateReview() throws Exception
	{
		if( null==txtReviewComments.getValue() || 0==txtReviewComments.getValue().toString().trim().length()) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_COMMT));
			
			return false;
		}
		if( null==cmbUserGroups.getValue() || cmbUserGroups.getValue().toString().equals( CMB_DEFAULT_VAL)) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return false;
		}
		return true;
	}
	
	public void reviewDone() {
		try {
			doSaveApplication(getStatusReviewedId());
			ReviewRequestView reviewRequestView = (ReviewRequestView) viewMap.get(
																	WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
								
			new UtilityService().persistReviewRequest(reviewRequestView);
			refreshTabs(getRequestView());
			completeTask( TaskOutcome.OK,WFTaskAction.POSTBACKRESUBMIT.toString(),null);
			CommonUtil.saveSystemComments( WebConstants.REQUEST, 
										   MessageConstants.MemsNOL.MSG_REQ_REVIWED, 
										   getRequestView().getRequestId());
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
					  										  MemsCommonRequestMsgs.SUCC_REQ_REVIEW_DONE));
			pageModeView=true;
		} catch (Exception ex) {
			logger.LogException("[Exception occured in reviewDone()]", ex);
			errorMessages.add(CommonUtil
								.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));												
		}
	}
	
	public void review() {
		try {
			if( validateReview() ) {
				RequestView reqView = getRequestView();
				reqView.setMemsNolReviewGrpId( cmbUserGroups.getValue().toString());						
				RequestService reqWS = new RequestService();
				reqWS.saveOrUpdate( getReviewRequestVO());
				if ( doSaveApplication(getStatusReviewRequiredId()) ) {
					refreshTabs( getRequestView());
					completeTask( TaskOutcome.FORWARD,WFTaskAction.REVIEW.toString(),cmbUserGroups.getValue().toString());
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
												   MessageConstants.MemsNOL.MSG_REQ_SENT_FOR_REVIEW,
												   getRequestView().getRequestId());
					setUserInGroup();
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST);
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
																	  MemsCommonRequestMsgs.SUCC_REQ_REVIEW));
					pageModeView=true;
				}
			}
			
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public void followUp() {
		logger.logInfo("[followUp() .. Starts]");
		logger.logInfo("[followUp() .. Ends]");
	}
	
	private boolean validateDoneWhenApproved() throws Exception 
	{

		List<DisbursementDetailsView> list = getDisbursementDetails();
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		for(DisbursementDetailsView eachDisbursement : list) 
		{
			if( WebConstants.PaymentSrcType.AID_ID == eachDisbursement.getSrcType().longValue() &&  
				( null==eachDisbursement.getDisburseSrc() || eachDisbursement.getDisburseSrc().toString().equals("-1") ) ) 
			{
				String msg = java.text.MessageFormat.format(CommonUtil.getBundleMessage( "mems.normaldisb.err.msg.disSercReq"), eachDisbursement.getRefNum());
				errorMessages.add( msg );
				tabPanel.setSelectedTab(TAB_DETAILS);
				return false;			
			}
			if( !eachDisbursement.getPaidTo().toString().equals( WebConstants.PAID_TO_BENEFICIARY) && 
					eachDisbursement.getVendorId() == null 
			   ) 
			{
				String msg = java.text.MessageFormat.format(CommonUtil.getBundleMessage( "mems.normaldisb.err.msg.vendorReqDisbursement"), eachDisbursement.getRefNum());
				errorMessages.add( msg );
				tabPanel.setSelectedTab(TAB_DETAILS);
				return false;
				
			}
			if(null==eachDisbursement.getPaymentDetails() || 0==eachDisbursement.getPaymentDetails().size())
			{
				errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_ADD_PAYMENT));
				tabPanel.setSelectedTab(TAB_DETAILS);
				return false;
			}
			if( eachDisbursement.getRequestBeneficiaries() == null || eachDisbursement.getRequestBeneficiaries().size() <= 0 )
			{
				errorMessages.add( CommonUtil.getBundleMessage("mems.normaldisb.err.msg.distributeInfoReq" ) );
				return false;
			}
			else
			{
					double disReqBenefAmount = 0.0d;				
					for (DisbursementRequestBeneficiaryView reqBenefView : eachDisbursement.getRequestBeneficiaries() ) 
					{
						if( reqBenefView.getIsDeleted()!= null && reqBenefView.getIsDeleted().compareTo(1l)==0 )continue;
						if( eachDisbursement.getStatus().compareTo(WebConstants.DisbursementStatus.REJECTED_ID)!= 0 &&
							! validateCostCenter( reqBenefView.getPersonId().toString() ) 
						   )
						{
							return false;
							
						}
						else
						{
							disReqBenefAmount += reqBenefView.getAmount();
						}
						
					}

					if( eachDisbursement.getAmount().compareTo(  Double.valueOf( oneDForm.format( disReqBenefAmount ) ) ) != 0  )
					//if( eachDisbursement.getAmount().doubleValue()!= disReqBenefAmount )
					{
						errorMessages.add( CommonUtil.getBundleMessage( "disburseFinance.msg.sumBenefAmountNotEqualDisburseAmount" ));
						tabPanel.setSelectedTab(TAB_DETAILS);
						return false;
					}

					
			}
		}		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean hasUnblockError ( List<DisbursementDetailsView> selectedDetails ) throws Exception
	{
		boolean hasError = false;
		if( selectedDetails == null || selectedDetails.size()  <=  0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "disbursement.msg.selectAny" )	);
			hasError = true;
			tabPanel.setSelectedTab(TAB_DETAILS);
		}
		else
		{
			for (DisbursementDetailsView disbursementDetailsView : selectedDetails) 
			{
				Request  request = BlockingAmountService.getActiveUnBlockingRequest( disbursementDetailsView );
				if( request != null )
				{
					String msg = java.text.MessageFormat.format( 
																  CommonUtil.getBundleMessage( "UnBlockingRequest.msg.selectedDisbursementAlreadyBindedWithRequet" ),	
																  disbursementDetailsView.getRefNum(),
																  request.getRequestNumber()
																);
					errorMessages.add( msg );
					hasError = true;
					tabPanel.setSelectedTab(TAB_DETAILS);
					break;
				}
			}
			
		}
		
		return hasError;
	}
	
	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeUnblockBPEL(Request request) throws Exception//, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSBlockingBPEL" );
		 MEMSBlockingBPELPortClient port=new MEMSBlockingBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		Integer.parseInt( request.getRequestId().toString()),
				 		CommonUtil.getLoggedInUser(),
				 		"0", 
				 		null, 
				 		null
				 	   );
//		invokeWorkFlow(request.getRequestId(),WebConstants.BlockingRequest.PROCEDURE_UNBLOCKING_REQUEST);
	}
	
	@SuppressWarnings("unchecked")
	public void onUnblockSelected()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			List<DisbursementDetailsView> selectedDetails = new ArrayList<DisbursementDetailsView>(); 
			for (DisbursementDetailsView dtView : getDisbursementDetails()) 
			{
              if( dtView.getSelected() == null || !dtView.getSelected() ) { continue; }
              selectedDetails.add( dtView );	 
			}
			if( hasUnblockError( selectedDetails  ) ) return;
			
	        ApplicationContext.getContext().getTxnContext().beginTransaction();
	        
	        
			BlockingAmountService blockService = new BlockingAmountService();
			RequestView reqView = getRequestView();
			
			Request unblockingRequest = blockService.populateUnBlockingRequestFromDisbursementView(
																									 reqView, 
																									 selectedDetails,
																									 getLoggedInUserId()
																								  );
			invokeUnblockBPEL( unblockingRequest ); 			
            String msg =  ResourceUtil.getInstance().getProperty( "disbursement.msg.unblockingRequestSaved" );			
            msg =  java.text.MessageFormat.format( 
		            								msg, 
		            								unblockingRequest.getRequestNumber()
		            							  );	
            
			successMessages.add( msg );
	        ApplicationContext.getContext().getTxnContext().commit();
		}
		catch( Exception e)
		{
			logger.LogException( " onUnblockSelected --- EXCEPTION --- ", e );
	        ApplicationContext.getContext().getTxnContext().rollback();
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(	ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );

		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}	
	public void doneWhenApproved() 
	{		
		try 
		{
			    if( !validateDoneWhenApproved() ){	return;	}
				boolean bForwardFinance = false;
				Long disbursementId = CommonUtil.getDomainDataId( WebConstants.MemsPaymentTypes.DOMAIN_KEY,
															   WebConstants.MemsPaymentTypes.DISBURSEMENTS );
				Long monthlyAlimondyId = CommonUtil.getDomainDataId( WebConstants.MemsPaymentTypes.DOMAIN_KEY, 
																	 WebConstants.MemsPaymentTypes.INCREASING_MONTHLY_ALIMONY);
				Long outVendorId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY, 
															   WebConstants.DisbursementStatus.OUTSTAND_VENDOR);
				Long increasedId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY,
															   WebConstants.DisbursementStatus.MONTLY_ALM_INC);
				List<DisbursementDetailsView> listDisbursementDetail = getDisbursementDetails();
				for(DisbursementDetailsView eachDisbursement : listDisbursementDetail) 
				{
					if( eachDisbursement.getPaymentType().equals( disbursementId) && 
								eachDisbursement.getPaidTo().equals( WebConstants.PAID_TO_BENEFICIARY)) {						
						bForwardFinance = true;						
					}
					if( eachDisbursement.getPaymentType().equals( disbursementId) &&
								eachDisbursement.getPaidTo().equals( WebConstants.PAID_TO_EXTERNAL_PARTY) ) {
						eachDisbursement.setStatus( outVendorId);								
					}
					if( eachDisbursement.getPaymentType().equals( monthlyAlimondyId) &&
									eachDisbursement.getPaidTo().equals( WebConstants.PAID_TO_BENEFICIARY) ) {
						eachDisbursement.setStatus( increasedId);
					}																									
				}
				String msg = "";
				if( bForwardFinance ) 
				{
					doSaveApplication( getStatusDisbursementReqId());
//					completeTask( TaskOutcome.OK);
					completeTask( TaskOutcome.OK,WFTaskAction.APPROVED.toString(),null);
					msg = CommonUtil.getBundleMessage("disbursementRequest.msg.sentToFinace");
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
							   MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT,
							   getRequestView().getRequestId());
					
				} 
				else 
				{
					doSaveApplication( getStatusCompleteId());
//					completeTask( TaskOutcome.COMPLETE);
					completeTask( TaskOutcome.COMPLETE,WFTaskAction.OK.toString(),null);
					
					msg = CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_COMPLETE);
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
							   MessageConstants.RequestEvents.REQUEST_COMPLETED,
							   getRequestView().getRequestId());
					
				}
				
								
				
				refreshTabs( getRequestView());
				successMessages.add(msg);
				pageModeView=true;
						
		} catch(Exception ex) {
			
			logger.LogException( "[Exception occured in doneWhenApproved()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private boolean validateRejectCaring()throws Exception
	{
		if ( txtRejectionReason.getValue() == null || 
			 txtRejectionReason.getValue().toString().trim().length() <= 0
		   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("disbursement.msg.rejectionReasonRequried"));
			return false;
		}
		return true;
	}

	public void rejectCaring() 
	{
		try 
		{
			errorMessages = new ArrayList<String>();
			if ( validateRejectCaring() ) 
			{
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				doSaveApplication( getStatusCaringRejectedId() );
//				completeTask( TaskOutcome.REJECT);
				completeTask( TaskOutcome.REJECT,WFTaskAction.POSTBACK.toString(),null);
				refreshTabs( reqView);
				refreshTabs(getRequestView());
				CommonUtil.saveSystemComments( WebConstants.REQUEST,"disbursementRequest.event.caringRejected", getRequestView().getRequestId());
				successMessages.add(CommonUtil.getBundleMessage("disbursementRequest.event.caringRejected" ));
				pageModeView=true;
			}
			
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in rejectCaring()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void reject() 
	{
		try 
		{
			DomainDataView dataView = CommonUtil.getDomainDataView( WebConstants.DisbursementStatus.DOMAIN_KEY, WebConstants.DisbursementStatus.REJECTED_ID );
			for(DisbursementDetailsView singleDisbursement : getDisbursementDetails()) 
			{
					singleDisbursement.setStatusAr( dataView.getDataDescAr());
					singleDisbursement.setStatusEn( dataView.getDataDescEn());
					singleDisbursement.setStatus( dataView.getDomainDataId() );
			} 
			rejectRequest();
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in reject()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
	}
	@SuppressWarnings("unchecked")
	public void onFinanceFromResearch() 
	{
		try 
		{
			RequestView reqView= getRequestView();
			if( areAllRejected() )
			{
				rejectRequest();	
				pageModeView=true;
			} 
			else if( validateApprove(reqView) ) 
			{
				doneWhenApproved();
				
			}
		} catch(Exception ex) {
			
			logger.LogException( "[Exception occured in onFinanceFromResearch()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void approve()
	{
		try 
		{
			RequestView reqView= getRequestView();
			if( areAllRejected() )
			{
				rejectRequest();														
			}
			else if( validateApprove(reqView) && doSaveApplication( getStatusApprovedId())) 
			{				

//				completeTask( TaskOutcome.APPROVE);
				completeTask( TaskOutcome.APPROVE,WFTaskAction.APPROVED.toString(),null);
				CommonUtil.saveSystemComments( WebConstants.REQUEST,
											   MessageConstants.RequestEvents.REQUEST_APPROVED,
											   reqView.getRequestId());
				refreshTabs( reqView);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.	
																  MemsCommonRequestMsgs.SUCC_REQ_APPROVE));
				pageModeView=true;
			}
			
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in approve()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private boolean validateSuspension() throws Exception
	{
		boolean validate = true;
		
		if ( txtRejectionReason.getValue() == null || 
				 txtRejectionReason.getValue().toString().trim().length() <= 0
			   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("request.msg.suspensionReasonRequired"));
			return false;
		}
		return validate;
	}
	public void onSuspended()
	{
		try 
		{
			RequestView reqView= getRequestView();
			
			if( validateSuspension() && doSaveApplication( WebConstants.REQUEST_STATUS_SUSPENDED_ID ) ) 
			{				

				CommonUtil.saveSystemComments( WebConstants.REQUEST,
											   "request.msg.requestSuspended",
											   reqView.getRequestId());
				refreshTabs( reqView);
				successMessages.add( CommonUtil.getBundleMessage( "request.msg.requestSuspended") );
				pageModeView=true;
			}
			
		} 
		
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onSuspended()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onSaveAfterSubmission()
	{
		try 
		{
			RequestView reqView= getRequestView();
			
			if( doSaveApplication( reqView.getStatusId() ) ) 
			{				

				CommonUtil.saveSystemComments( WebConstants.REQUEST,
						   					   MessageConstants.RequestEvents.REQUEST_UPDATED,
											   reqView.getRequestId());
				refreshTabs( reqView);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_UPDATED ) );
				pageModeView=true;
			}
			
		} 
		
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onSaveAfterSubmission()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}



	/**
	 * @throws Exception
	 */
	private void rejectRequest() throws Exception 
	{
		doSaveApplication( getStatusRejectedId());
		RequestView req = getRequestView();
		CommonUtil.saveSystemComments( WebConstants.REQUEST,
									   MessageConstants.RequestEvents.REQUEST_REJECTED,
									   req.getRequestId());
//		completeTask( TaskOutcome.REJECT);
		completeTask( TaskOutcome.REJECT,WFTaskAction.REJECT.toString(),null);
		refreshTabs( req);
		successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
														  MemsCommonRequestMsgs.SUCC_REQ_REJECT));

	}
	
	private boolean areAllRejected() {
		Long rejectedId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY,
													  WebConstants.DisbursementStatus.REJECTED);
		List<DisbursementDetailsView> list = getDisbursementDetails();
		for(DisbursementDetailsView singleRecord : list) {
			if( !singleRecord.getStatus().equals( rejectedId) ) {
				return false;
			}
		}
		return true;				  
	}
	
	private boolean doSaveApplication(final Long statusId) throws Exception 
	{
			if( !validateInput() ){return false;}
			
			RequestView reqView = getRequestView();
			reqView.setRequestDate(new Date());
			reqView.setStatusId(statusId);
			
			int totalSize = getToDeleteList().size() + getDisbursementDetails().size();				
			List<DisbursementDetailsView> totalDisbursement = new ArrayList<DisbursementDetailsView>(totalSize);
			totalDisbursement.addAll(getDisbursementDetails());
			totalDisbursement.addAll(getToDeleteList());
			
			reqView.setDisubrsementDetailsView(totalDisbursement);
			if(txtRejectionReason.getValue() != null && txtRejectionReason.getValue().toString().trim().length() > 0 )
			{
				reqView.setMaintenanceDetails( txtRejectionReason.getValue().toString().trim() );
			}
			
			
			if( statusId.compareTo(WebConstants.REQUEST_STATUS_SUSPENDED_ID ) == 0 )
			{
				 reqView.setOldStatusId(reqView.getStatusId());
			}
			RequestView updatedView = saveOrUpdate(reqView);
			
			
			setRequestView(updatedView);
			List<DisbursementDetailsView> dDetailsView = updatedView.getDisubrsementDetailsView();
			if( null==dDetailsView ) 
			{
				dDetailsView = new ArrayList<DisbursementDetailsView>();
			}
			setDisbursementDetails(dDetailsView);
			setTotalRows(dDetailsView.size());
			saveAttachmentsAndComments(updatedView.getRequestId());
			return true;
		
	}
	
	private RequestView saveOrUpdate(RequestView reqView) throws Exception {
		RequestService reqWS = new RequestService();
		return reqWS.saveOrUpdate(reqView);
	}
	
	private void setDisbursementDetails(List<DisbursementDetailsView> list) {
		double sumDisbursements = 0.0d;
		if ( list != null )
		{
			Collections.sort(list, new ListComparator("refNum",true));
			
			for (DisbursementDetailsView disbursementDetailsView : list) 
			{
				if( disbursementDetailsView.getIsDeleted().compareTo( 1L ) == 0 )
					continue;
				sumDisbursements += disbursementDetailsView.getAmount(); 
			}
			
			viewMap.put(DISBURSEMENTS, list);
			setTotalRows(list.size());
			
		}
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		setTotalDisbursementCount( oneDForm.format( sumDisbursements ) );
	}
	
	public List<DisbursementDetailsView> getToDeleteList() {
		if( viewMap.containsKey(DELETED_DISBURSEMENT) ) {
			return (List<DisbursementDetailsView>) viewMap.get(DELETED_DISBURSEMENT);
		}
		return new ArrayList<DisbursementDetailsView>(0);
	}

	
	private void handleAttachmentCommentTabStatus(RequestView view) {
		if( view.getStatusId().equals(WebConstants.REQUEST_STATUS_COMPLETE)) {
			viewMap.put("canAddAttachment", false);
			viewMap.put("canAddNote", false);
		} else {
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
		}
	}
	
	private void setRequestView(RequestView reqView) {
		@SuppressWarnings("unused")
		RequestView oldView = (RequestView) viewMap.remove(WebConstants.REQUEST_VIEW);
		oldView = null;
		viewMap.put(WebConstants.REQUEST_VIEW, reqView);
	}
	
	private RequestView getRequestView() {
		return (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
	}
	
	private void loadItemsCombo(Map<Long, ItemTypeView> itemMap) 
	{
		ArrayList<SelectItem> cmbItemType = new ArrayList<SelectItem>(itemMap.size());
		Set<Long> keySet = itemMap.keySet();
		for(Long itemKey : keySet) {
			ItemTypeView curItem = itemMap.get( itemKey);
			if( CommonUtil.getIsEnglishLocale() ) {
				cmbItemType.add(new SelectItem( curItem.getTypeId().toString(), curItem.getTypeNameEn()));
			} else {
				cmbItemType.add(new SelectItem( curItem.getTypeId().toString(), curItem.getTypeNameAr()));
			}
		}
		viewMap.put(ITEM_CMB, cmbItemType);
	}
	
	private void setUserInGroup() {
		if (getIsReviewRequired()) {
			User loggedInUser = getLoggedInUserObj();
			if (loggedInUser.hasUserGroup(getRequestView()
					.getMemsNolReviewGrpId())) {
				viewMap.put(USER_IN_GROUP, "TRUE");
			}
		}
	}
	
	private ReviewRequestView getReviewRequestVO() {
		ReviewRequestView reviewReqVO = new ReviewRequestView();
		reviewReqVO.setRequestId(getRequestView().getRequestId());
		reviewReqVO.setCreatedBy(CommonUtil.getLoggedInUser());
		reviewReqVO.setCreatedOn(new Date());
		reviewReqVO.setRfc(txtReviewComments.getValue().toString());
		reviewReqVO.setGroupId(cmbUserGroups.getValue().toString());
		return reviewReqVO;
	}
	
	private void loadReasonsCombo(Map<Long, DisbursementReasonView> reasonMap) {
		ArrayList<SelectItem> cmbReasonType = new ArrayList<SelectItem>(reasonMap.size());
		Set<Long> keySet = reasonMap.keySet();
		for(Long reasonKey : keySet) {
			DisbursementReasonView curReason = reasonMap.get( reasonKey);
			if( CommonUtil.getIsEnglishLocale() ) {
				cmbReasonType.add(new SelectItem( curReason.getReasonId().toString(), curReason.getReasonEn()));
			} else {
				cmbReasonType.add(new SelectItem( curReason.getReasonId().toString(), curReason.getReasonAr()));
			}
		}
		
		Collections.sort(cmbReasonType, ListComparator.LIST_COMPARE);
		viewMap.put(REASON_CMB, cmbReasonType);
	}
	
	private void loadBeneficiaryList(final Long fileId) throws Exception 
	{
		InheritedAssetService inheritedAssetWS = new InheritedAssetService();
		List<BeneficiariesPickListView> beneficiaresPickList = inheritedAssetWS.getBeneficiariesView(fileId);
		
		ArrayList<SelectItem> beneficiaries = new ArrayList<SelectItem>();
//		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
		Map<String, String> benefNoCostCenterMap = new HashMap<String, String>();
		for(BeneficiariesPickListView singleBeneficiary : beneficiaresPickList) 
		{
			SelectItem selItem = new SelectItem(singleBeneficiary.getPersonId().toString(),
																	singleBeneficiary.getFullName());
			beneficiaries.add(selItem);
			if(	singleBeneficiary.getCostCenter() == null )
			{
				benefNoCostCenterMap.put(singleBeneficiary.getPersonId().toString(),singleBeneficiary.getFullName() ); 		
			}
			else
			{
				Double amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(singleBeneficiary.getPersonId(),getRequestView().getInheritanceFileId());
	//			Double amount = PersonalAccountTrxService.getAmount( singleBeneficiary.getPersonId());
				if( amount!=null )
				{
				personBalanceMap.put( singleBeneficiary.getPersonId(), amount);
				
				Double blocking = BlockingAmountService.getBlockingAmount(singleBeneficiary.getPersonId(), null, null, null, null );
				personBlockingMap.put( singleBeneficiary.getPersonId(), blocking);
				
				
				}
			}
		}			
		
		viewMap.put(BENEF_NO_COST_CENTER_MAP, benefNoCostCenterMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiaries);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put( PERSON_BLOCKING_MAP, personBlockingMap);
		
	}
	
	private void saveAttachmentsAndComments(Long reqId) throws PimsBusinessException {	
		loadAttachmentsAndComments(reqId);
		CommonUtil.saveAttachments(reqId);
		String noteOwner = (String) viewMap.get("noteowner");
		CommonUtil.saveComments(reqId, noteOwner);
		if(txtRejectionReason.getValue() != null && txtRejectionReason.getValue().toString().trim().length() > 0)
		{
			CommonUtil.saveRemarksAsComments(reqId, txtRejectionReason.getValue().toString().trim(), noteOwner) ;
		}
	}
	private void loadAttachmentsAndComments(Long requestId){		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewMap.get(PROCEDURE_TYPE).toString());
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = viewMap.get(EXTERNAL_ID).toString();
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);
		
		if( null!=requestId ) {
	    	String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	
	private void refreshTabs(RequestView reqView)throws Exception 
	{
		if (null == reqView && null == reqView.getApplicantView()) 
		{
			throw new IllegalArgumentException();
		}

		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, reqView.getRequestDate());
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, reqView.getRequestNumber());

		PersonView applicant = reqView.getApplicantView();
		viewMap.put(WebConstants.LOCAL_PERSON_ID, applicant.getPersonId());

		viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,applicant.getPersonId());


		if (null != applicant.getPersonFullName()) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,applicant.getPersonFullName());									
		}

		if (null != applicant.getCellNumber()) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,applicant.getCellNumber());									
		}

		String desc = reqView.getDescription();

		if ( null!=desc && 0!=desc.trim().length() ) {
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,
						 reqView.getDescription());									
		}

		if ( CommonUtil.getIsEnglishLocale() ) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusEn());
					
		} else if (CommonUtil.getIsArabicLocale()) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, reqView.getStatusAr());					
		}

		if ( !getIsNewStatus() ) {
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}

		List<DisbursementDetailsView> detailsView = reqView.getDisubrsementDetailsView();
				
		if (null == detailsView) {
			detailsView = new ArrayList<DisbursementDetailsView>();
		}
		setDisbursementDetails( detailsView);
		handleAttachmentCommentTabStatus(reqView);
		
		checkReviewRequired(detailsView);
		if( !viewMap.containsKey( DISBURSEMENT_SOURCES) ) 
		{
			ApplicationBean app = new ApplicationBean();
			List<SelectItem> sources = app.getDisbursementSourceList();
			viewMap.put( DISBURSEMENT_SOURCES, sources);				
		}
		
//		if( getIsRenderCaringApprove() || getIsDisbursementRequired() ) 
		if( getIsDisbursementRequired() )
		{
			
			List<DisbursementDetailsView> accepted = getAcceptedDisbursements(getDisbursementDetails());
			setDisbursementDetails( accepted);
			getRequestView().setDisubrsementDetailsView( accepted);
			setTotalRows( accepted.size());
			populateFinanceData();
		}
		//Show Tab Logic Start
		displayTab(reqView);
		//Finish
	}

	/**
	 * @param detailsView
	 */
	private void checkReviewRequired(List<DisbursementDetailsView> detailsView) 
	{
		if( 
			getRequestView().getStatusId().equals( WebConstants.REQUEST_STATUS_SUSPENDED_ID ) ||
			getRequestView().getStatusId().equals( getStatusApprovalRequiredId()) ||
		    getRequestView().getStatusId().equals( getStatusApprovedId()) ||
			getRequestView().getStatusId().equals( getStatusReviewedId()) || 
			getIsCaringRejected() ||
			getIsFinanceRejected()
		  ) 
		{
			for(DisbursementDetailsView view : detailsView) 
			{
				if(view.getIsReviewReq()!=null && view.getIsReviewReq().equals("1"))
				{
						viewMap.put( IS_REVIEW, "");
						break;
				}
			}
		}
	}
	
	private void displayTab(RequestView reqView) throws Exception
	{
		if( isPostBack() )
			return;
		if(getRequestView().getStatusDataValue().equals(WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ))
		{
			tabPanel.setSelectedTab(TAB_FINANCE);
		}
		else if( reqView.getStatusDataValue().equals(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED) ||
				 reqView.getStatusDataValue().equals(WebConstants.REQUEST_STATUS_REVIEW_DONE)
			   )
		{
			tabPanel.setSelectedTab(TAB_REVIEW);
		}
		else if( getIsFinanceRejected() || getIsCaringRejected() || getIsSuspended())
		{
			tabPanel.setSelectedTab(TAB_DETAILS);
			if( reqView.getMaintenanceDetails() != null && reqView.getMaintenanceDetails().length() >0  )
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(reqView.getMaintenanceDetails().trim() );
			}
		}
	}
	private boolean validateComplete() throws Exception
	{
		if (!viewMap.containsKey(WebConstants.FinanceTabPublishKeys.CAN_COMPLETE)) {
			errorMessages.add(CommonUtil
					.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.ERR_COMPLETE));
			return false;
		}
		return true;
	}
	private boolean validateRejectFinance()throws Exception
	{
		if ( txtRejectionReason.getValue() == null || 
			 txtRejectionReason.getValue().toString().trim().length() <= 0
		   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("disbursement.msg.rejectionReasonRequried"));
			return false;
		}
			FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
			
			List<DisbursementDetailsView> ddView = tabFinanceController.getDisbDetails();
			
			for(DisbursementDetailsView singleView : ddView) 
			{
				if(  singleView.getIsDisbursed().longValue()==1l  || !tabFinanceController.getRenderDisburse()) 
				{
					errorMessages.add(CommonUtil.getBundleMessage("disburseFinance.msg.rejectionError"));
					tabPanel.setSelectedTab( TAB_FINANCE	);
					return false;				
				}
			}
	
		return true;
	}

	public void rejectFinance() 
	{
		try 
		{
			errorMessages = new ArrayList<String>();
			if ( validateRejectFinance() ) 
			{
				RequestService reqWS = new RequestService();
				RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId());
				setRequestView( reqView);
				if(	doSaveApplication( getStatusFinanceRejectedId() )	)
				{
//					completeTask( TaskOutcome.REJECT);
					completeTask( TaskOutcome.REJECT,WFTaskAction.POSTBACK.toString(),null);
					//refreshTabs( reqView);
					refreshTabs(getRequestView());
					CommonUtil.saveSystemComments( WebConstants.REQUEST,"disbursementRequest.event.financeRejected", getRequestView().getRequestId());
					successMessages.add(CommonUtil.getBundleMessage("disbursementRequest.msg.financeRejected" ));
				}
				pageModeView = true;
			}
            
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in rejectFinance()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void complete() 
	{
		try 
		{
			if ( !validateComplete() ) 
			{return;}
			
			RequestService reqWS = new RequestService();
			RequestView reqView = reqWS.getRequestById( getRequestView().getRequestId() );
			setRequestView( reqView );
			doSaveApplication(WebConstants.REQUEST_STATUS_COMPLETE_ID );
			completeTask( TaskOutcome.OK,WFTaskAction.OK.toString(),null);
			refreshTabs( reqView);
			refreshTabs(getRequestView());
			CommonUtil.saveSystemComments( WebConstants.REQUEST, MessageConstants.RequestEvents.REQUEST_COMPLETED, getRequestView().getRequestId());
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_COMPLETE));
		}
		catch (Exception ex) 
		{
			logger.LogException("[Exception occured in complete()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	

	
	public void populateFinanceData() {
		try
		{
				FinanceTabController tabFinanceController = (FinanceTabController) getBean("pages$tabFinance");
				if( null!=tabFinanceController ) 
				{
					tabFinanceController.handleTabChange();
					//For checking
					viewMap.put(WebConstants.FinanceTabPublishKeys.HIDE_PAYMENT_TYPE, "");
				}
		}
		catch (Exception ex) {
				logger.LogException( "[Exception occured in populateFinanceData()]", ex);
		}

	}
	
	public void getRequestHistory() {
		try {
			Long reqId = getRequestView().getRequestId();
			if (null != reqId && !reqId.equals( INVALID_STATUS)) {
				RequestHistoryController rhc = new RequestHistoryController();
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST, reqId.toString());						
			}
		} catch (Exception ex) {
			logger.LogException( "[Exception occured in getRequestHistory()]", ex);
		}
	}
	
	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}
	
	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}
	
	public Map<Long, ItemTypeView> getItemsMap() {
		if( viewMap.containsKey(ITEM_MAP)) {
			return (Map<Long, ItemTypeView>) viewMap.get(ITEM_MAP);
		}
		return new HashMap<Long, ItemTypeView>();
	}
	
	public Map<Long, DisbursementReasonView> getReasonMap() {
		if( viewMap.containsKey(REASON_MAP)) {
			return (Map<Long, DisbursementReasonView>) viewMap.get(REASON_MAP);
		}
		return new HashMap<Long, DisbursementReasonView>();
	}
	
	public List<SelectItem> getItemTypes() {
		if( viewMap.containsKey(ITEM_CMB)) {
			return (List<SelectItem>) viewMap.get(ITEM_CMB);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getReasonTypes() {
		if( viewMap.containsKey(REASON_CMB)) {
			return (List<SelectItem>) viewMap.get(REASON_CMB);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getDisbursementSources() {
		if( viewMap.containsKey( DISBURSEMENT_SOURCES)) {
			return (List<SelectItem>) viewMap.get( DISBURSEMENT_SOURCES);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getPaymentTypes() {
		if( viewMap.containsKey(WebConstants.MemsPaymentTypes.DOMAIN_KEY) ) 
			return (List<SelectItem>) viewMap.get(WebConstants.MemsPaymentTypes.DOMAIN_KEY);
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getPaymentSource() {
		if( viewMap.containsKey(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE) ) {
			return (List<SelectItem>) viewMap.get(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<SelectItem> getBeneficiaryList() {
		if( viewMap.containsKey(BENENEFICIARY_LIST) ) {
			return (List<SelectItem>) viewMap.get(BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	public List<DisbursementDetailsView> getDisbursementDetails() {
		if( viewMap.containsKey(DISBURSEMENTS)) {
			return (List<DisbursementDetailsView>) viewMap.get(DISBURSEMENTS);
		}
		return new ArrayList<DisbursementDetailsView>();
	}
	
	private void loadBeneficiaryList(BeneficiaryGridView beneficiaryGridView) throws Exception 
	{
		ArrayList<SelectItem> beneficiary = new ArrayList<SelectItem>();
		beneficiary.add(new SelectItem(beneficiaryGridView.getBeneficiaryPersonId().toString(), 
														beneficiaryGridView.getPersonName()));
//		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Double amount =0.0d;
		if( beneficiaryGridView.getCostCenter() != null &&  beneficiaryGridView.getCostCenter().trim().length() >0  )
		{
			amount =  PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount(beneficiaryGridView.getBeneficiaryPersonId(),getRequestView().getInheritanceFileId());
	//		Double amount = PersonalAccountTrxService .getAmount( beneficiaryGridView.getBeneficiaryPersonId());
			personBalanceMap.put( beneficiaryGridView.getBeneficiaryPersonId(), amount);
		}
		
		Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
		Double blocking = BlockingAmountService.getBlockingAmount( beneficiaryGridView.getBeneficiaryPersonId(), null, null, null, null );
		personBlockingMap.put( beneficiaryGridView.getBeneficiaryPersonId(), blocking );
		viewMap.put( PERSON_BLOCKING_MAP , personBlockingMap);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		viewMap.put(BENENEFICIARY_LIST, beneficiary);
	}
	
	private void readParams() 
	{
		if( StringHelper.isNotEmpty( hdnVendorId) ) 
		{
			txtVendor.setValue( hdnVendorName);
			DisbursementDetailsView disbursement = this.getIsUpdate();
			if( null!=disbursement ) {
				disbursement.setVendorId( new Long( hdnVendorId));
				disbursement.setVendorName( hdnVendorName);
			}
			hdnVendorId = null;			
		}
		else 
		{
			if( hdnPersonId!=null && !hdnPersonId.equals("")) {						
				RequestView reqView = getRequestView();
				PersonView applicant = reqView.getApplicantView();			
				if( null!=applicant) {
					applicant.setPersonId(Long.parseLong(hdnPersonId));
				} else {
					applicant = new PersonView();
					applicant.setPersonId(Long.parseLong(hdnPersonId));
					reqView.setApplicantView(applicant); 				
				}					
				viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnPersonId);
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId);
			}
			
			if( hdnPersonName!=null && !hdnPersonName.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,hdnPersonName);
			}
			
			if( null!=hdnCellNo && !hdnCellNo.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL, hdnCellNo);
			}
			
			if( null!=hdnPersonType && !hdnPersonType.equals("")) {
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE, hdnPersonType);
			}
		}
	}

	private void restoreToDefault() throws Exception
	{
		cmbPaymentSource.setValue( CMB_DEFAULT_VAL);
		cmbPaymentTypes.setValue( CMB_DEFAULT_VAL);
		cmbReasonTypes.setValue( CMB_DEFAULT_VAL);
		cmbItemTypes.setValue( CMB_DEFAULT_VAL);
		cmbDisbursementSource.setValue(CMB_DEFAULT_VAL);
		if( getIsFileAssociated() ) {
			cmbBeneficiaryList.setValue( CMB_DEFAULT_VAL);
			cmbBeneficiaryList.setDisabled( false);
		}
		
		txtAmount.setValue("");
		txtDescription.setValue("");
		accountBalance.setValue("");
		blockingBalance.setValue("");
		requestedAmount.setValue("");
		availableBalance.setValue(   "" );
		noOfBenef.setValue("");
		noOfBenef.setReadonly(true);
		noOfBenef.setStyleClass( "READONLY" );
		if( getIsNewStatus() ) {
			cmbPaymentSource.setReadonly( false);
			cmbPaymentSource.setStyleClass("");
		}
		txtVendor.setValue("");
		hdnVendorId = "";
		hdnVendorName="";
	}

	
	

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnRequestId() {
		return hdnRequestId;
	}

	public void setHdnRequestId(String hdnRequestId) {
		this.hdnRequestId = hdnRequestId;
	}
	
	public String getFileNumber() {
		return getRequestView().getInheritanceFileView().getFileNumber();
	}
	
	private DisbursementDetailsView getIsUpdate() {
		return (DisbursementDetailsView) viewMap.get(IS_UPDATE);
	}
	
	public Boolean getRenderReview() {
		String taskType=viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE)!= null ?viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE).toString():"";
		if( viewMap.containsKey( IS_REVIEW)) {
			return !pageModeView && 
			getIsTaskPresent() && 
					(
					   getIsApprovalRequired() ||  
					   (
					     getIsReviewed() && 
						 ( taskType.equals("SocialResearcherNormalDisbRequest")  ||
								 taskType.equals("SocialCareNormalDisbRequest") ||
								 taskType.equals("187001")|| 
								 taskType.equals("187002") || 
								 taskType.equals("187005") 
						)
					   ) || 
					   getIsCaringRejected() ||getIsFinanceRejected()||getIsApproved()
						
					);
		}
		return false;
	}
	
	public Boolean getIsReviewRequired() {
		return !pageModeView && this.getRequestView()!= null && 
			   this.getRequestView().getStatusId()!=null && 
		       this.getRequestView().getStatusId().equals( getStatusReviewRequiredId());
	}
	public Boolean getIsRequestDateAfterLowerBound()throws Exception
	{
//		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//		 lowerBound = (java.util.Date)df.parse(lowerBoundDate);
		return getRequestView()==null || getRequestView().getCreatedOn() == null || getRequestView().getCreatedOn().after( lowerBound ) ;
	}
	
	public Boolean getIsRenderResearcherApprove()
	{
		String taskType= viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE)!= null ?viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE).toString():"";
//		String taskType= "SocialResearcherNormalDisbRequest";
		return !pageModeView && getIsTaskPresent() && 
				(
					getIsApprovalRequired() 
					|| 
					getIsSuspended()||
					
				    (
						   getIsReviewed()  && 
						   
						   	 ( taskType.equals( "SocialResearcherNormalDisbRequest" )|| 
						   			taskType.equals("187001")||
									 taskType.equals("187002")||
									 	   
									 taskType.equals("187005")) 
							

				    )
				    || 
				   	getIsCaringRejected()
				   	|| 
				    ( 
						   getIsFinanceRejected( ) && 
						   ( taskType.equals( "SocialResearcherNormalDisbRequest" )||
								   taskType.equals("187001")||
									 taskType.equals("187002")||
									 	   
									 taskType.equals("187005")
						   ) 
				    )
			   );
		
	}
	public Boolean getIsRenderCaringApprove()
	{
		String taskType=  viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE)!= null ?viewMap.get(WebConstants.TASK_LIST_SELECTED_TASK_TYPE).toString():"";
//		String taskType= "SocialCareNormalDisbRequest";
		return !pageModeView && getIsTaskPresent() && 
				(
					getIsApproved()  || 
				   (getIsReviewed()  && 
						   ( taskType.equals( "SocialResearcherNormalDisbRequest" )|| 
									  
									 taskType.equals("187002")) 
				   )|| 
				   ( getIsFinanceRejected( ) &&( taskType.equals( "SocialResearcherNormalDisbRequest" )|| 
							 
							 taskType.equals("187002")  )
							 )
				 );
		
	}
	public Boolean getIsDisbursementRequired() {
		return !pageModeView && this.getRequestView() != null && this.getRequestView().getStatusId().equals( getStatusDisbursementReqId());
	}
	
	public Boolean getIsCompleted() {
		return this.getRequestView() != null &&  this.getRequestView()!= null && this.getRequestView().getStatusDataValue()!=null &&
			   this.getRequestView().getStatusDataValue().equals( WebConstants.REQUEST_STATUS_COMPLETE);				
	}
	public Boolean getIsFinanceRejected() {
		return !pageModeView && this.getRequestView() != null && this.getRequestView()!= null &&this.getRequestView().getStatusDataValue()!=null &&
			   this.getRequestView().getStatusDataValue().equals( WebConstants.REQUEST_STATUS_FINANCE_REJECTED);				
	}
	public Boolean getIsCaringRejected() {
		return !pageModeView && this.getRequestView() != null && this.getRequestView()!= null && this.getRequestView().getStatusDataValue()!=null &&
			   this.getRequestView().getStatusDataValue().equals( WebConstants.REQUEST_STATUS_CARING_REJECTED);				
	}
	public Boolean getIsReviewed() {
		return !pageModeView &&this.getRequestView() != null &&  this.getRequestView().getStatusId().equals( getStatusReviewedId());				
	}
	
	public Boolean getIsApproved() {
		return !pageModeView &&this.getRequestView() != null &&  this.getRequestView().getStatusId().equals( getStatusApprovedId());
	}
	
	
	public Boolean getIsSuspended() {
		return !pageModeView &&this.getRequestView() != null &&  this.getRequestView().getStatusId().equals( WebConstants.REQUEST_STATUS_SUSPENDED_ID );
	}
	

	public Boolean getIsApprovalRequired() {
		Long statusId = this.getRequestView() != null ?this.getRequestView().getStatusId():null;
		return !pageModeView && statusId != null && statusId.equals( getStatusApprovalRequiredId());
	}
	
	public Boolean getIsInvalidStatus() {
		return this.getRequestView() != null && getRequestView().getStatusId().equals( INVALID_STATUS);
	}
	public Boolean getIsNewStatus() {
		Long reqId = this.getRequestView().getStatusId();
		return (reqId.equals(INVALID_STATUS)) || (reqId.equals(getStatusNewId()));
	}
	
	public Boolean getIsFileAssociated() {
		if( viewMap.containsKey(IS_FILE_ASSOCIATED) ) {
			return ((String)viewMap.get(IS_FILE_ASSOCIATED)).equals("TRUE");		
		}		
		return false;
	}
	
	private Long getStatusDisbursementReqId() {
		if( !viewMap.containsKey( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get( WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ);
	}
	
	private Long getStatusNewId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_NEW) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_NEW);
	}
	
	private Long getStatusCompleteId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_COMPLETE)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_COMPLETE);
	}
	private Long getStatusFinanceRejectedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_FINANCE_REJECTED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_FINANCE_REJECTED);
	}
	
	private Long getStatusCaringRejectedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_CARING_REJECTED )) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_CARING_REJECTED );
	}
	private Long getStatusApprovedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVED);
	}
	
	public List<SelectItem> getAllUserGroups() {
		if (viewMap.containsKey(ALL_USER_GROUPS)) {
			return (List<SelectItem>) viewMap.get(ALL_USER_GROUPS);
		}
		return new ArrayList<SelectItem>(0);
	}
	
	private void loadUserGroupsInViewRoot() throws Exception {
		if (!viewMap.containsKey(ALL_USER_GROUPS)) {
			List<UserGroup> allUserGrps = CommonUtil.getAllUserGroups();
			List<SelectItem> listUserGrps = new ArrayList<SelectItem>(allUserGrps.size());
			for (UserGroup singleGrp : allUserGrps) {	
				listUserGrps.add(new SelectItem(
												singleGrp.getUserGroupId(),
												isEnglishLocale()? singleGrp.getPrimaryName(): singleGrp.getSecondaryName()
											    )
								);
			}
			Collections.sort(listUserGrps, ListComparator.LIST_COMPARE);
			viewMap.put(ALL_USER_GROUPS, listUserGrps);
		}
	}
	
	private List<DisbursementDetailsView> getAcceptedDisbursements(List<DisbursementDetailsView> detailList)throws Exception {
		Long acceptedId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY, 
													  WebConstants.DisbursementStatus.ACCEPTED);
		Long disbursedId = CommonUtil.getDomainDataId( WebConstants.DisbursementStatus.DOMAIN_KEY, 
													   WebConstants.DisbursementStatus.DISBURSED);
		
		List<DisbursementDetailsView> acceptedList = new ArrayList<DisbursementDetailsView>();
		for(DisbursementDetailsView detail : detailList) {
			if( detail.getStatus().equals( acceptedId) 
									|| detail.getStatus().equals( disbursedId)) {
				acceptedList.add( detail);
			}
			
		}			
		return acceptedList;
	}
	
	private Long getStatusRejectedId() {	
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REJECTED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REJECTED);
	}
	
	private Long getStatusApprovalRequiredId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);		
	}
		
	private Long getStatusReviewRequiredId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED)) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED);		
	}
	
	private Long getStatusReviewedId() {
		if( !viewMap.containsKey(WebConstants.REQUEST_STATUS_REVIEW_DONE) ) {
			return INVALID_STATUS;
		}
		return (Long) viewMap.get(WebConstants.REQUEST_STATUS_REVIEW_DONE);		
	}

	public HtmlSelectOneMenu getCmbBeneficiaryList() {
		return cmbBeneficiaryList;
	}

	public void setCmbBeneficiaryList(HtmlSelectOneMenu cmbBeneficiaryList) {
		this.cmbBeneficiaryList = cmbBeneficiaryList;
	}

	public HtmlSelectOneMenu getCmbPaymentSource() {
		return cmbPaymentSource;
	}

	public void setCmbPaymentSource(HtmlSelectOneMenu cmbPaymentSource) {
		this.cmbPaymentSource = cmbPaymentSource;
	}

	public HtmlSelectOneMenu getCmbPaymentTypes() {
		return cmbPaymentTypes;
	}

	public void setCmbPaymentTypes(HtmlSelectOneMenu cmbPaymentTypes) {
		this.cmbPaymentTypes = cmbPaymentTypes;
	}

	public HtmlSelectOneMenu getCmbReasonTypes() {
		return cmbReasonTypes;
	}

	public void setCmbReasonTypes(HtmlSelectOneMenu cmbReasonTypes) {
		this.cmbReasonTypes = cmbReasonTypes;
	}

	public HtmlSelectOneMenu getCmbItemTypes() {
		return cmbItemTypes;
	}

	public void setCmbItemTypes(HtmlSelectOneMenu cmbItemTypes) {
		this.cmbItemTypes = cmbItemTypes;
	}

	public HtmlInputText getTxtAmount() {
		return txtAmount;
	}

	public void setTxtAmount(HtmlInputText txtAmount) {
		this.txtAmount = txtAmount;
	}

	public HtmlInputText getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(HtmlInputText txtDescription) {
		this.txtDescription = txtDescription;
	}

	public HtmlSelectOneMenu getCmbUserGroups() {
		return cmbUserGroups;
	}

	public void setCmbUserGroups(HtmlSelectOneMenu cmbUserGroups) {
		this.cmbUserGroups = cmbUserGroups;
	}

	public HtmlInputText getTxtReviewComments() {
		return txtReviewComments;
	}

	public void setTxtReviewComments(HtmlInputText txtReviewComments) {
		this.txtReviewComments = txtReviewComments;
	}

	public HtmlSelectOneMenu getCmbDisbursementSource() {
		return cmbDisbursementSource;
	}

	public void setCmbDisbursementSource(HtmlSelectOneMenu cmbDisbursementSource) {
		this.cmbDisbursementSource = cmbDisbursementSource;
	}

	public HtmlSelectOneMenu getCmbPaidTo() {
		return cmbPaidTo;
	}

	public void setCmbPaidTo(HtmlSelectOneMenu cmbPaidTo) {
		this.cmbPaidTo = cmbPaidTo;
	}

	public HtmlInputText getTxtBalance() {
		return txtBalance;
	}

	public void setTxtBalance(HtmlInputText txtBalance) {
		this.txtBalance = txtBalance;
	}

	public HtmlInputText getTxtVendor() {
		return txtVendor;
	}

	public void setTxtVendor(HtmlInputText txtVendor) {
		this.txtVendor = txtVendor;
	}

	public HtmlGraphicImage getImgVendor() {
		return imgVendor;
	}

	public void setImgVendor(HtmlGraphicImage imgVendor) {
		this.imgVendor = imgVendor;
	}
	
    @SuppressWarnings("unchecked")
    private void invokeWorkFlow(Long requestId,String procedureType) throws Exception
    {
        WFClient client = new WFClient();
        WorkFlowVO workFlowVO = new WorkFlowVO();
        workFlowVO.setRequestId(requestId);
        workFlowVO.setUserId(getLoggedInUserId());
        workFlowVO.setProcedureTypeId(procedureType);
        if(procedureType.equals(WebConstants.PROCEDURE_TYPE_MEMS_NORMAL_PAYMENTS)){
        	setGroupAndTaskNameForInitiateWorkFlow(workFlowVO);
        	
        }
        client.initiateProcess(workFlowVO);
    }
	@SuppressWarnings("unchecked")
	private Boolean invokeBpel(Long requestId) {
		 Boolean success = true;
//		 try {
//			 MEMSNormalDisbursementBPELPortClient bpelClient = new MEMSNormalDisbursementBPELPortClient();			 
//			 SystemParameters parameters = SystemParameters.getInstance();
//			 String endPoint =
//			 parameters.getParameter(WebConstants.MemsBPELEndPoints.MEMS_NORMAL_DISB_END_POINT);
//			 bpelClient.setEndpoint( endPoint);
//			 String userId = CommonUtil.getLoggedInUser();
//			 String groupName =getApprovalGroupName();
//			 bpelClient.initiate( requestId, userId,groupName , null, null);			 
//		 } catch(PIMSWorkListException exception) {		
//			 success = false;
//			 logger.LogException("invokeBpel() crashed ", exception);
//		 } catch (Exception exception) {		 
//			 success = false;
//			 logger.LogException("invokeBpel() crashed ", exception);
//		 } finally {		 
//			 if(!success) {
//				 errorMessages.clear();
//				 errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			 }
//		 }
		return true;
	}
	@SuppressWarnings("unchecked")
	private void setGroupAndTaskNameForInitiateWorkFlow(WorkFlowVO workFlowVO)throws Exception
	{
		String groupName = "Admin Group";
		List<DisbursementDetailsView> dDetailsView = getDisbursementDetails();
		String procedureTaskKey = "SOCIAL_RESEARCHER_NORMAL_DISB_REQUEST_TASK";
		boolean isInheritanceRelatedTask = false;
		for (DisbursementDetailsView disbursementDetailsView : dDetailsView) 
		{
			if( disbursementDetailsView.getMainReasonId() != null && disbursementDetailsView.getMainReasonId()  == 217003l)
			{
				procedureTaskKey = "INHERITANCE_RELATED_DISB_REQUEST_TASK";
				
				isInheritanceRelatedTask =true;
				break;
			}

		}
		if(!isInheritanceRelatedTask )
		{
			for (DisbursementDetailsView disbursementDetailsView : dDetailsView) 
			{
				if( disbursementDetailsView.getSrcType().longValue() == WebConstants.PaymentSrcType.AID_ID )
				{
					procedureTaskKey = "SOCIAL_CARING_NORMAL_DISB_REQUEST_TASK";
					break;
				}
			}
		}
		UtilityService sservice = new UtilityService();
		
		groupName = sservice.getProcedureTaskGroups(
														WebConstants.PROCEDURE_TYPE_MEMS_NORMAL_PAYMENTS, 
														procedureTaskKey
													);
		 
		workFlowVO.setHumanTaskDataValue(procedureTaskKey);
		workFlowVO.setAssignedGroups(groupName);
	}
	@SuppressWarnings("unchecked")
	private Boolean completeTask(TaskOutcome taskOutcome,
								 String taskAction,
								 String assignedGroups) throws Exception 
	{
		
		Boolean success = true;
		String contextPath = ((ServletContext)
		getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");		 
		String loggedInUser = CommonUtil.getLoggedInUser();
		if(viewMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK)) {
			if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK ) instanceof UserTask){
				 logger.logInfo("Completing SOA Task");
				 UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				 BPMWorklistClient client = new BPMWorklistClient(contextPath);
				 if(userTask!=null) {
						 client.completeTask(userTask, loggedInUser, taskOutcome);
				 }
			}  else if(viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null ){
                logger.logInfo("Completing Pims WorkFlow Task");
                TaskListVO  userTask = (    TaskListVO )viewMap.get(  WebConstants.TASK_LIST_SELECTED_USER_TASK  ); 
                
                if(taskAction != null && taskAction.trim().length()>0)
                {
                        WorkFlowVO workFlowVO = new WorkFlowVO();
                        workFlowVO.setTaskAction(taskAction);
                        if(assignedGroups !=null && assignedGroups.length()>0)
                        {
                                workFlowVO.setAssignedGroups(assignedGroups);
                        }
                        CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
                }
            }
		}
		 return success;		
	}
	
	public UserTask getIncompleteRequestTasks() {
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";		 
		try {
	   		Long requestId = getRequestView().getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();	   		
	   		if( null!=taskId ) {	   		
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);				 
				taskType = userTask.getTaskType();
				logger.logInfo("Task Type is:" + taskType);
				return userTask;
	   		} else {	   		
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		} catch(PIMSWorkListException ex) {				
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK) {			
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC) {			
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else {			
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		} catch (Exception exception) {		
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		return null;
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	public HtmlInputText getHtmlFileType() {
		return htmlFileType;
	}

	public void setHtmlFileType(HtmlInputText htmlFileType) {
		this.htmlFileType = htmlFileType;
	}

	public HtmlInputText getHtmlFileStatus() {
		return htmlFileStatus;
	}

	public void setHtmlFileStatus(HtmlInputText htmlFileStatus) {
		this.htmlFileStatus = htmlFileStatus;
	}

	public HtmlInputText getHtmlFileDate() {
		return htmlFileDate;
	}

	public void setHtmlFileDate(HtmlInputText htmlFileDate) {
		this.htmlFileDate = htmlFileDate;
	}				
	public void handleBeneficiaryChange() 
	{
		try
		{
			if( !cmbBeneficiaryList.getValue().toString().equals( CMB_DEFAULT_VAL) &&
				!cmbBeneficiaryList.getValue().toString().equals( CMB_ALL_BENEF) &&  
				validateCostCenter(cmbBeneficiaryList.getValue().toString()) 		
			)
			{
				Map<Long, Double> balanceMap = getBalanceMap();
				Long beneficiaryId = new Long( cmbBeneficiaryList.getValue().toString());
				Double amount =  0.00d;
				if( balanceMap.containsKey( beneficiaryId) ) 
				{
					amount = balanceMap.get( beneficiaryId);
					accountBalance.setValue( amount.toString());
				}
				else
				{
					amount = 0.0d;
					accountBalance.setValue( amount.toString());
				}
				Double blockingMapAmount =0.00d;
				Map<Long, Double> blockingMap = getBlockingMap();
				if( blockingMap.containsKey( beneficiaryId) ) 
				{
					blockingMapAmount = blockingMap.get( beneficiaryId);
					blockingBalance.setValue( blockingMapAmount.toString());
				}
				Double requestAmount = DisbursementService.getRequestedAmount(beneficiaryId, getRequestView().getInheritanceFileId(), getRequestView().getRequestId() ,null);
				requestedAmount.setValue( requestAmount );
				
				
				DecimalFormat oneDForm = new DecimalFormat("#.00");
				availableBalance.setValue(   oneDForm.format( amount- (blockingMapAmount+requestAmount)) );
				
				noOfBenef.setStyleClass("READONLY");
				noOfBenef.setReadonly(true);
			} 
			else 
			{
				accountBalance.setValue( "" );
				blockingBalance.setValue( "" );
				requestedAmount.setValue("");
				availableBalance.setValue("");
				if( cmbBeneficiaryList.getValue().toString().equals( CMB_ALL_BENEF)  )
				{
					noOfBenef.setStyleClass("");
					noOfBenef.setReadonly(false);
					noOfBenef.setValue("");
				}
				else
				{
					noOfBenef.setValue("");
					noOfBenef.setStyleClass("READONLY");
					noOfBenef.setReadonly(true);
				}
//				getErrorMessages();
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in handleBeneficiaryChange()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private Map<Long, Double> getBalanceMap() {
		if( viewMap.containsKey( PERSON_BALANCE_MAP)) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BALANCE_MAP);
		}
		return new HashMap<Long, Double>();
	}

	private Map<Long, Double> getBlockingMap() {
		if( viewMap.containsKey( PERSON_BLOCKING_MAP )) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BLOCKING_MAP  );
		}
		return new HashMap<Long, Double>();
	}
	public HtmlInputText getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(HtmlInputText accountBalance) {
		this.accountBalance = accountBalance;
	}
	   @SuppressWarnings( "unchecked" )
		public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event)
	   {
	    	try
	    	{
	    		DisbursementDetailsView gridViewRow = (DisbursementDetailsView) disbursementTable.getRowData();
	    		if(	gridViewRow.getPersonId()>0 && 
	    			(
	    				gridViewRow.getBeneficiaryCount() == 1 ||
						gridViewRow.getIsMultiple()== null ||	
						gridViewRow.getIsMultiple().compareTo(0L)==0
	    			)
	    		  )
	    		{

	    			sendToParent("javaScript:openBeneficiaryPopup("+ gridViewRow.getPersonId()+");");
	    		}
	    		else if( gridViewRow.getBeneficiaryCount() > 1 )
	    		{
	    		  openMultipleBeneficiaryPopup(gridViewRow, true);
	    		}
	    		else
	    		{
	    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_BENEFICIARY));
	    		}
		   } catch (Exception ex) {
				logger.LogException("[Exception occured in openManageBeneficiaryPopUp()]", ex);
				errorMessages = new ArrayList<String>(0);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
	   
	   
	   public void openAssociateCostCenters() 
	    {
	    	final String methodName = "openAssociateCostCenters";
	    	try
	    	{
	    	
	    	DisbursementDetailsView gridViewRow = ( DisbursementDetailsView ) disbursementTable.getRowData();
			String javaScriptText = "javaScript:openAssociateCostCenters();";
	        sessionMap.put( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW, gridViewRow );
	        if( gridViewRow.getRequestBeneficiaries() != null &&  gridViewRow.getRequestBeneficiaries().size() > 0 )
	        {
	        	
	        	sessionMap.put( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW, gridViewRow.getRequestBeneficiaries().get( 0 ) );
	        	
	        }
			sendToParent( javaScriptText );
	    	}
	    	catch( Exception e )
	    	{
	    		logger.LogException( methodName +"Error Occured:", e );
	    		errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

	    	}
		}
		

	   public void onMessageFromAssociateCostCenter()
		{
			try
			{
				DisbursementDetailsView message = (DisbursementDetailsView) sessionMap.get(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);				
				sessionMap.remove(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW);
				List<DisbursementDetailsView> disbursementDetails = getDisbursementDetails();
				for (DisbursementDetailsView dtView : disbursementDetails) 
				{
					if(dtView.getId() == message.getId() )
					{
					  logger.logInfo("[onMessageFromAssociateCostCenter .. Id matched]");
					  disbursementDetails.remove( dtView  );
					  disbursementDetails.add( message );
					  successMessages.add(CommonUtil.getBundleMessage( "commons.associateCostCenterDoneSuccessfully" ));
					  break;
					}
	                								
					
				}
				setDisbursementDetails( disbursementDetails );
			}
			catch( Exception e)
			{
				logger.LogException( " [onMessageFromAssociateCostCenter --- EXCEPTION --- ", e );
				errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

			}
		}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public HtmlInputText getTxtRejectionReason() {
		return txtRejectionReason;
	}

	public void setTxtRejectionReason(HtmlInputText txtRejectionReason) {
		this.txtRejectionReason = txtRejectionReason;
	}

	@SuppressWarnings( "unchecked" )
	public MemsFollowupView getFollowUp() 
	{
		if( viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW) != null )
		{
			followUp = (MemsFollowupView)viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW);
		}
		return followUp;
	}
	@SuppressWarnings( "unchecked" )
	public void setFollowUp(MemsFollowupView followUp) {
		this.followUp = followUp;
		if( this.followUp != null)
		{
			viewMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,this.followUp);
		}
	}

	public HtmlInputText getBlockingBalance() {
		return blockingBalance;
	}

	public void setBlockingBalance(HtmlInputText blockingBalance) {
		this.blockingBalance = blockingBalance;
	}

	public HtmlInputText getNoOfBenef() {
		return noOfBenef;
	}

	public void setNoOfBenef(HtmlInputText noOfBenef) {
		this.noOfBenef = noOfBenef;
	}

	@SuppressWarnings("unchecked")
	public String getTotalDisbursementCount() {
		if( viewMap.get("totalDisbursementCount") != null )
		{
			 totalDisbursementCount = viewMap.get("totalDisbursementCount").toString();
		}
		return totalDisbursementCount;
	}

	@SuppressWarnings("unchecked")
	public void setTotalDisbursementCount(String totalDisbursementCount) {
		this.totalDisbursementCount = totalDisbursementCount;
		viewMap.put("totalDisbursementCount",this.totalDisbursementCount);
		
	}

	

	@SuppressWarnings("unchecked")
	public String getSelectOneMainReason() {
		if( viewMap.get("selectOneMainReason") != null  )
		{
			selectOneMainReason = viewMap.get("selectOneMainReason").toString(); 
		}
		return selectOneMainReason;
	}

	public void setSelectOneMainReason(String selectOneMainReason) 
	{
		this.selectOneMainReason = selectOneMainReason;
		if( this.selectOneMainReason  != null  )
		{
		  viewMap.put("selectOneMainReason", selectOneMainReason); 
		}
	}

	public HtmlSelectOneMenu getCmbMainReason() {
		return cmbMainReason;
	}

	public void setCmbMainReason(HtmlSelectOneMenu cmbMainReason) {
		this.cmbMainReason = cmbMainReason;
	}


}
