package com.avanza.pims.web.backingbeans.istisnaacontract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.construction.tendermanagement.TenderSearchBacking;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ConstructionContractView;
import com.avanza.pims.ws.vo.ContractAdditionalInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;



public class IstisnaaContractDetailsTab extends AbstractController
{
	private transient Logger logger = Logger.getLogger(IstisnaaContractDetailsTab.class);
     public interface Page_Mode {
		
    	public static final String EDIT="PAGE_MODE_EDIT";
 		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
 		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
 		public static final String COMPLETE="PAGE_MODE_COMPLETE";
 		public static final String POPUP="PAGE_MODE_POPUP";
 		public static final String VIEW="PAGE_MODE_VIEW";
 		public static final String ACTIVE="PAGE_MODE_ACTIVE";
		
	}
    public static final String GRACE_PERIOD_LIST ="GRACE_PERIOD_LIST";
    private String tenderSearchKey_NotBindedWithContract;
 	private String tenderSearchKey_tenderType;
 	private String tenderSearchKey_WinnerContractorPresent;
	private HtmlGraphicImage imgSearchTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgViewTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgRemoveTender=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchContractor=new HtmlGraphicImage();
    
    
    private Date contractStartDate;
    org.richfaces.component.html.HtmlCalendar startDateCalendar = new  org.richfaces.component.html.HtmlCalendar();

    private Date contractDate;
    org.richfaces.component.html.HtmlCalendar contractDateCalendar = new  org.richfaces.component.html.HtmlCalendar();

    private String txtContractHejjriDate;
    
    private Date contractEndDate;
    org.richfaces.component.html.HtmlCalendar endDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
    
    private String txtContractPeriod;
    private String selectOneContractPeriod;
    HtmlSelectOneMenu cmbContractPeriod = new HtmlSelectOneMenu();
    
    private String txtGracePeriod;
    private String selectOneGracePeriod;
    HtmlSelectOneMenu cmbGracePeriod = new HtmlSelectOneMenu();
    
    private String selectOneRentType;
    HtmlSelectOneMenu cmbRentType = new HtmlSelectOneMenu();
    
    HtmlSelectOneMenu cmbEmirateCity = new HtmlSelectOneMenu();
    
    HtmlSelectOneMenu cmbArea = new HtmlSelectOneMenu();
    
	private Map<String, String> countryList;
	private Map<String, String> stateList;
	private Map<String, String> cityList;
	
	
	private String crCountryId;
	private String crStateId;
	private String crCityId;
	
	private String country;
	private String state;
	private String city;
	
	
    
    private String landNumber;
	private HtmlInputText txtLandNumber;
	
	private String landArea;
	private HtmlInputText txtLandArea;
    
    private String contractNo;
	private HtmlInputText txtContractNo;
	
	private String contractStatus;
	private HtmlInputText txtContractStatus;
	
	private String contractType;
	private HtmlInputText txtContractType;
	
	private HtmlInputText txtTenderNum=new HtmlInputText();
	private String tenderNum;
	
	
	
	private String proposalOpenDate;
	
	private HtmlInputText txtContractorNum=new HtmlInputText();
	private String contractorNum;
	
	private HtmlInputText txtContractorName=new HtmlInputText();
	private String contractorName;
	
	private HtmlInputText txtTotalAmount=new HtmlInputText();
	private String totalAmount;
	
	
	private String selectOneIstisnaaMehthod;
    HtmlSelectOneMenu cmbIstisnaaMehthod = new HtmlSelectOneMenu();
    
	
	private HtmlInputText txtAmafShare=new HtmlInputText();
	private String amafShare;
	
	private HtmlInputText txtSecondPartyShare=new HtmlInputText();
	private String secondPartyShare;
	
	private HtmlGraphicImage imgInvestorSearch = new HtmlGraphicImage();
	private HtmlGraphicImage imgViewInvestor = new HtmlGraphicImage();
	
	
	
	
	// loads the country list
	private final void loadCountry() {
		String methodName = "loadCountry()";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List<RegionView> regionViewList = psa.getCountry();

			if (countryList == null)
				countryList = new HashMap<String, String>();

			for (RegionView rV : regionViewList)
				getCountryList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads all the states in the list
	public final void loadState() {
		String methodName = "loadState";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();

			// TODO: FIX THE FOLLOWING LOCs
			// String selectedCountryName = "";
			// for (int i = 0; i < this.getCountryList().size(); i++) {
			// if (new Long(country) == (new Long(this.getCountryList().get(
			// i).getValue().toString())).longValue()) {
			// selectedCountryName = this.getCountryList().get(i)
			// .getLabel();
			// }
			// }

			List<RegionView> regionViewList = psa.getCountryStates("UAE",getIsArabicLocale());// selectedCountryName);

			getStateList().clear();

			for (RegionView rV : regionViewList)
				this.getStateList().put(
						getIsEnglishLocale() ? rV.getDescriptionEn() : rV
								.getDescriptionAr(),
						rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception e) {
			logger.LogException(methodName + "|Error Occured ", e);
		}
	}

	// loads all the cities in the list
	public final void loadCity() {
		String methodName = "loadCity";
		logger.logInfo(methodName + "|" + "Start");

		try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			Set<RegionView> regionViewList = null;

			if (state != null && !state.trim().equals("-1"))
				regionViewList = psa.getCity(new Long(state));
			else if (crStateId != null && !crStateId.trim().equals("-1"))
				regionViewList = psa.getCity(new Long(crStateId));

			getCityList().clear();

			if (regionViewList != null)
				for (RegionView rV : regionViewList)
					getCityList().put(
							getIsEnglishLocale() ? rV.getDescriptionEn() : rV
									.getDescriptionAr(),
							rV.getRegionId().toString());

			logger.logInfo(methodName + "|" + "Finish");
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);
		}

	}
	
	
	
	private String pageMode;
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap=context.getViewRoot().getAttributes();
	 @Override 
	 public void init() 
     {
    	 super.init();
    	 try
    	 {
			if(!isPostBack()){
	 			loadCountry();
	 			viewRootMap.put(GRACE_PERIOD_LIST, CommonUtil.getDomainDataListForDomainType(WebConstants.GRACE_PERIOD_TYPE));
	 		}
			sessionMap=context.getExternalContext().getSessionMap();
			Map requestMap= context.getExternalContext().getRequestMap();
			HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
			this.setContractType(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_TYPE).toString());
			//this.setContractType("BOT");
			//contractType
			//String requestNumber=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_NUMBER).toString();
    	 }
    	 catch(Exception es)
    	 {
    		 logger.LogException("Error Occured ", es);
    	 }
	 }
	public void  calculateShares()
	{
			this.setAmafShare("");
			this.setSecondPartyShare("");
	        this.getIsIstisnaaMethodPercentage();
		
	}
	public boolean getIsIstisnaaMethodPercentage()
	{
		
		if(this.getSelectOneIstisnaaMehthod()==null || this.getSelectOneIstisnaaMehthod().trim().equals("0"))
			return false;
		else
			return true;
		
		
	}
	public void  calculateFirstPartyShares()
	{
		if(this.getSelectOneIstisnaaMehthod()!=null && this.getSelectOneIstisnaaMehthod().compareTo("1")==0)
		{
			try
			{
			  Double secondPercent = new Double(this.getSecondPartyShare());
			  this.setAmafShare(Double.toString(100-secondPercent));
			
			}
			catch(NumberFormatException e)
			{
				return;
			}
		}
		
	}
	public void  calculateSecondPartyShares()
	{
		if(this.getSelectOneIstisnaaMehthod()!=null && this.getSelectOneIstisnaaMehthod().compareTo("1")==0)
		{
			try
			{
			  Double amafPercent = new Double(this.getAmafShare());
			  this.setSecondPartyShare(Double.toString(100-amafPercent));
			
			}
			catch(NumberFormatException e)
			{
				return;
			}
		}
		
	}
    public void calculateContractExpiryDate()
 	{
 		
 		if(this.getContractStartDate() == null )return;
 		if(txtContractPeriod==null || txtContractPeriod.length()<=0 )return;
 		else
 		{
	 		 try {
				new Integer(txtContractPeriod);
			} catch (Exception e) {
				return;
			}
 		}
 		
 			List<DomainDataView> ddvList = (ArrayList<DomainDataView>)viewRootMap.get(GRACE_PERIOD_LIST);
 			for (DomainDataView domainDataView : ddvList) {
				
			if(selectOneContractPeriod.compareTo(domainDataView.getDomainDataId().toString())==0 ){
			       //For Dates
                   Calendar cal = Calendar.getInstance();
                   //Setting The date o parameter
                   cal.setTime(this.getContractStartDate());
		
		           if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.DAYS)==0)
		           {
		           
		           cal.add(Calendar.DATE, new Integer(txtContractPeriod));
		           this.setContractEndDate( cal.getTime());
		            
		           }
		           else if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.MONTHS)==0)
		           { 
		           cal.add(Calendar.MONTH, new Integer(txtContractPeriod));
		           this.setContractEndDate( cal.getTime());
		           } 
		           else if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.YEARS)==0)
		           {
		            cal.add(Calendar.YEAR, new Integer(txtContractPeriod));
		            this.setContractEndDate( cal.getTime());
		           }
			
			}
 			
 		
 	  }
 	}
 	 public void calculateContractStartDate()
 	{
 		
 		if(contractDate == null )return;
 		if(txtGracePeriod==null || txtGracePeriod.length()<=0 )return;
 		else
 		{
	 		 try {
				new Integer(txtGracePeriod);
			} catch (Exception e) {
				return;
			}
 		}
 		
 			List<DomainDataView> ddvList = (ArrayList<DomainDataView>)viewRootMap.get(GRACE_PERIOD_LIST);
 			for (DomainDataView domainDataView : ddvList) {
				
			if(selectOneGracePeriod.compareTo(domainDataView.getDomainDataId().toString())==0 ){
			       //For Dates
                   Calendar cal = Calendar.getInstance();
                   //Setting The date o parameter
                   cal.setTime(contractDate);
		
		           if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.DAYS)==0)
		           {
		           
		           cal.add(Calendar.DATE, new Integer(txtGracePeriod));
		           this.setContractStartDate( cal.getTime());
		            
		           }
		           else if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.MONTHS)==0)
		           { 
		           cal.add(Calendar.MONTH, new Integer(txtGracePeriod));
		           this.setContractStartDate( cal.getTime());
		           } 
		           else if(domainDataView.getDataValue().compareTo(WebConstants.GracePeriodDataValues.YEARS)==0)
		           {
		            cal.add(Calendar.YEAR, new Integer(txtGracePeriod));
		            this.setContractStartDate( cal.getTime());
		           }
		           calculateContractExpiryDate();
			
			}
 			
 		
 	  }
 	}
 	@SuppressWarnings("unchecked")
 	public void populateContractDetails(ConstructionContractView constructionContractView,String CONTRACT_ADDITIONAL_INFO_VIEW) {
		
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,constructionContractView.getRentAmount());
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_START_DATE,constructionContractView.getStartDate());
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_END_DATE,constructionContractView.getEndDate());
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_NUM,constructionContractView.getContractNumber());
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_FREQ,constructionContractView.getContractPeriodFrequency());
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM,constructionContractView.getContractPeriodNum());
		viewRootMap.put(WebConstants.ContractDetailsTab.GRACE_PERIOD_FREQ,constructionContractView.getGracePeriodFrequency());
		viewRootMap.put(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM,constructionContractView.getGracePeriodNum());
		if(constructionContractView.getContractHejreeDate()!=null)
		   viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE,constructionContractView.getContractHejreeDate());
	  
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_DATE,constructionContractView.getContractDate());
	   if(constructionContractView.getContractAdditionalInfos()!= null && constructionContractView.getContractAdditionalInfos().size()>0)
	   {
		  ContractAdditionalInfoView contractAdditionalInfo =  constructionContractView.getContractAdditionalInfos().iterator().next();
		  viewRootMap.put(WebConstants.ContractDetailsTab.ISTISNAA_METHOD,contractAdditionalInfo.getIstisnaaMethod());
		  if(contractAdditionalInfo.getFirstPartyShare()!=null)
		     viewRootMap.put(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE , contractAdditionalInfo.getFirstPartyShare());
		  if(contractAdditionalInfo.getSecondPartyShare() !=null)
		     viewRootMap.put(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE , contractAdditionalInfo.getSecondPartyShare());
		     viewRootMap.put(CONTRACT_ADDITIONAL_INFO_VIEW,contractAdditionalInfo);
	   }
	}
 	@SuppressWarnings("unchecked")
 	public void populateOwner(PersonView owner)
 	{
 		if(owner!=null)
		{
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_ID,owner.getPersonId());
			if(owner.getPersonFullName()!=null)		
			      viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,owner.getPersonFullName());
		}
 		
 	}
	
 	public void enableDisableControls(String pageMode)
 	{
 		 if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACTOR_ID))
  			imgViewInvestor.setRendered(true);
  		else
  			imgViewInvestor.setRendered(false);
  		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.TENDER_ID))
  			imgInvestorSearch.setRendered(false);
 		
 		if(pageMode.equals(Page_Mode.ACTIVE) || pageMode.equals(Page_Mode.COMPLETE) 
 				|| pageMode.equals(Page_Mode.LEGAL_REVIEW_REQUIRED) || pageMode.equals(Page_Mode.VIEW))
 		{
			imgSearchTender.setRendered(false);
			imgSearchContractor.setRendered(false);
			txtTotalAmount.setDisabled(true);
			startDateCalendar.setDisabled(true);
			endDateCalendar.setDisabled(true);
  			imgInvestorSearch.setRendered(false);
		
 		}
 		else if(pageMode.equals(Page_Mode.EDIT) || pageMode.equals(Page_Mode.APPROVAL_REQUIRED))
 		{
			imgSearchTender.setRendered(false);
 		}
 		
 		
 		
 	}
 	
 	public LocaleInfo getLocaleInfo()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	    return localeInfo;
		
	}
 	public String getLocale(){
		LocaleInfo localeInfo = getLocaleInfo();
		return localeInfo.getLanguageCode();
	}
 	public Boolean getIsArabicLocale()
	{
    	
		return CommonUtil.getIsArabicLocale();
	}
 	public Boolean getIsEnglishLocale()
	{
    	
		return CommonUtil.getIsEnglishLocale();
	}
		public String getDateFormat()
		{
	    	
			return CommonUtil.getDateFormat();
		}
		public TimeZone getTimeZone()
		{
			 return CommonUtil.getTimeZone();
			
		}

		public HtmlGraphicImage getImgSearchTender() {
			return imgSearchTender;
		}

		public void setImgSearchTender(HtmlGraphicImage imgSearchTender) {
			this.imgSearchTender = imgSearchTender;
		}

		public HtmlGraphicImage getImgViewTender() {
			return imgViewTender;
		}

		public void setImgViewTender(HtmlGraphicImage imgViewTender) {
			this.imgViewTender = imgViewTender;
		}

		public HtmlGraphicImage getImgRemoveTender() {
			return imgRemoveTender;
		}

		public void setImgRemoveTender(HtmlGraphicImage imgRemoveTender) {
			this.imgRemoveTender = imgRemoveTender;
		}

		public HtmlGraphicImage getImgSearchContractor() {
			return imgSearchContractor;
		}

		public void setImgSearchContractor(HtmlGraphicImage imgSearchContractor) {
			this.imgSearchContractor = imgSearchContractor;
		}

	

		public Date getContractStartDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_START_DATE))
				contractStartDate= (Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
			return contractStartDate;
		}

		public void setContractStartDate(Date contractStartDate) {
			this.contractStartDate = contractStartDate;
			if(this.contractStartDate    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_START_DATE, this.contractStartDate );
		}

		public org.richfaces.component.html.HtmlCalendar getStartDateCalendar() {
			return startDateCalendar;
		}

		public void setStartDateCalendar(
				org.richfaces.component.html.HtmlCalendar startDateCalendar) {
			this.startDateCalendar = startDateCalendar;
		}

		public Date getContractEndDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_END_DATE))
				contractEndDate=(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);

			return contractEndDate;
		}

		public void setContractEndDate(Date contractEndDate) {
			this.contractEndDate = contractEndDate;
			if(this.contractEndDate    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_END_DATE, this.contractEndDate );
		}

		public org.richfaces.component.html.HtmlCalendar getEndDateCalendar() {
			return endDateCalendar;
		}

		public void setEndDateCalendar(
				org.richfaces.component.html.HtmlCalendar endDateCalendar) {
			this.endDateCalendar = endDateCalendar;
		}

		public String getContractNo() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_NUM))
				contractNo=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_NUM).toString();

			return contractNo;
		}

		public void setContractNo(String contractNo) {
			this.contractNo = contractNo;
			if(this.contractNo    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_NUM, this.contractNo );
		}

		public HtmlInputText getTxtContractNo() {
			return txtContractNo;
		}

		public void setTxtContractNo(HtmlInputText txtContractNo) {
			this.txtContractNo = txtContractNo;
		}

		public String getContractStatus() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_STATUS))
				contractStatus=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_STATUS).toString();

			return contractStatus;
		}

		public void setContractStatus(String contractStatus) {
			this.contractStatus = contractStatus;
			if(this.contractStatus    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_STATUS, this.contractStatus );
		}

		public HtmlInputText getTxtContractStatus() {
			return txtContractStatus;
		}

		public void setTxtContractStatus(HtmlInputText txtContractStatus) {
			this.txtContractStatus = txtContractStatus;
		}

		public String getContractType() {
			 if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_TYPE))
				contractType=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_TYPE).toString();

			return contractType;
		}

		public void setContractType(String contractType) {
			this.contractType = contractType;
			if(this.contractType    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE, this.contractType ); 
		}

		public HtmlInputText getTxtContractType() {
			return txtContractType;
		}

		public void setTxtContractType(HtmlInputText txtContractType) {
			this.txtContractType = txtContractType;
		}

	

		public HtmlInputText getTxtContractorNum() {
			return txtContractorNum;
		}

		public void setTxtContractorNum(HtmlInputText txtContractorNum) {
			this.txtContractorNum = txtContractorNum;
		}

		public String getContractorNum() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACTOR_NUM))
				contractorNum=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACTOR_NUM).toString();
			return contractorNum;
		}

		public void setContractorNum(String contractorNum) {
			this.contractorNum = contractorNum;
			if(this.contractorNum   !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM, this.contractorNum);
		}

		public HtmlInputText getTxtContractorName() {
			return txtContractorName;
		}

		public void setTxtContractorName(HtmlInputText txtContractorName) {
			this.txtContractorName = txtContractorName;
		}

		public String getContractorName() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACTOR_NAME))
				contractorName=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACTOR_NAME).toString();
			return contractorName;
		}

		public void setContractorName(String contractorName) {
			this.contractorName = contractorName;
			if(this.contractorName    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME, this.contractorName );
		}

		
		
		public HtmlInputText getTxtTenderNum() {
			return txtTenderNum;
		}

		public void setTxtTenderNum(HtmlInputText txtTenderNum) {
			this.txtTenderNum = txtTenderNum;
		}

		public String getTenderNum() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.TENDER_NUM))
				tenderNum=viewRootMap.get(WebConstants.ContractDetailsTab.TENDER_NUM).toString();
			return tenderNum;
		}

		public void setTenderNum(String tenderNum) {
			this.tenderNum = tenderNum;
			if(this.tenderNum    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM , this.tenderNum );
		}

		public HtmlInputText getTxtTotalAmount() {
			return txtTotalAmount;
		}

		public void setTxtTotalAmount(HtmlInputText txtTotalAmount) {
			this.txtTotalAmount = txtTotalAmount;
		}

		public String getTotalAmount() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT))
				totalAmount=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString();
			
			return totalAmount;
		}

		public void setTotalAmount(String totalAmount) {
			this.totalAmount = totalAmount;
			if(this.totalAmount    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT, this.totalAmount );
		}



		public String getTenderSearchKey_NotBindedWithContract() {
			return TenderSearchBacking.Keys.NOT_BINDED_WITH_CONTRACT;
		}

		

		public String getTenderSearchKey_tenderType() {
			return TenderSearchBacking.Keys.TENDER_TYPE;
		}

		

		public String getTenderSearchKey_WinnerContractorPresent() {
			return TenderSearchBacking.Keys.WINNER_CONTRACTOR_PRESENT;
		}



		public String getLandNumber() {
			return landNumber;
		}



		public void setLandNumber(String landNumber) {
			this.landNumber = landNumber;
		}



		public HtmlInputText getTxtLandNumber() {
			return txtLandNumber;
		}



		public void setTxtLandNumber(HtmlInputText txtLandNumber) {
			this.txtLandNumber = txtLandNumber;
		}



		public String getLandArea() {
			return landArea;
		}



		public void setLandArea(String landArea) {
			this.landArea = landArea;
		}



		public HtmlInputText getTxtLandArea() {
			return txtLandArea;
		}



		public void setTxtLandArea(HtmlInputText txtLandArea) {
			this.txtLandArea = txtLandArea;
		}



		public Map<String, String> getCountryList() {
			return countryList;
		}



		public void setCountryList(Map<String, String> countryList) {
			this.countryList = countryList;
		}



		public Map<String, String> getCityList() {
			return cityList;
		}



		public void setCityList(Map<String, String> cityList) {
			this.cityList = cityList;
		}



		public String getCrCountryId() {
			return crCountryId;
		}



		public void setCrCountryId(String crCountryId) {
			this.crCountryId = crCountryId;
		}



		public String getCrStateId() {
			return crStateId;
		}



		public void setCrStateId(String crStateId) {
			this.crStateId = crStateId;
		}



		public String getCrCityId() {
			return crCityId;
		}



		public void setCrCityId(String crCityId) {
			this.crCityId = crCityId;
		}



		public Map<String, String> getStateList() {
			return stateList;
		}



		public void setStateList(Map<String, String> stateList) {
			this.stateList = stateList;
		}



		public String getCountry() {
			return country;
		}



		public void setCountry(String country) {
			this.country = country;
		}



		public String getState() {
			return state;
		}



		public void setState(String state) {
			this.state = state;
		}



		public String getCity() {
			return city;
		}



		public void setCity(String city) {
			this.city = city;
		}



		public Date getContractDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_DATE))
				contractDate=(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_DATE);
			return contractDate;
		}



		public void setContractDate(Date contractDate) {
			this.contractDate = contractDate;
			if(this.contractDate!=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_DATE, this.contractDate);
		}



		public org.richfaces.component.html.HtmlCalendar getContractDateCalendar() {
			return contractDateCalendar;
		}



		public void setContractDateCalendar(
				org.richfaces.component.html.HtmlCalendar contractDateCalendar) {
			this.contractDateCalendar = contractDateCalendar;
		}



		public String getTxtContractHejjriDate() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE))
				txtContractHejjriDate=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE).toString();
			return txtContractHejjriDate;
			
		}



		public void setTxtContractHejjriDate(String txtContractHejjriDate) {
			this.txtContractHejjriDate = txtContractHejjriDate;
			if(this.txtContractHejjriDate !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_HEJREE_DATE, this.txtContractHejjriDate );
		}



		public String getTxtContractPeriod() {
			
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM))
				txtContractPeriod=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM).toString();
			return txtContractPeriod;
		}



		public void setTxtContractPeriod(String txtContractPeriod) {
			this.txtContractPeriod = txtContractPeriod;
			if(this.txtContractPeriod  !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_NUM, this.txtContractPeriod);
		}



		public String getSelectOneContractPeriod() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_FREQ))
				selectOneContractPeriod=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_FREQ).toString();
			return selectOneContractPeriod;
		}



		public void setSelectOneContractPeriod(String selectOneContractPeriod) {
			this.selectOneContractPeriod = selectOneContractPeriod;
			if(this.selectOneContractPeriod !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_PERIOD_FREQ, this.selectOneContractPeriod );
		}



		public HtmlSelectOneMenu getCmbContractPeriod() {
			return cmbContractPeriod;
		}



		public void setCmbContractPeriod(HtmlSelectOneMenu cmbContractPeriod) {
			this.cmbContractPeriod = cmbContractPeriod;
		}



		public String getTxtGracePeriod() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM))
				txtGracePeriod=viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM).toString();

			return txtGracePeriod;
		}



		public void setTxtGracePeriod(String txtGracePeriod) {
			this.txtGracePeriod = txtGracePeriod;
			if(this.txtGracePeriod !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.GRACE_PERIOD_NUM, this.txtGracePeriod );
		}



		public String getSelectOneGracePeriod() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GRACE_PERIOD_FREQ))
				selectOneGracePeriod=viewRootMap.get(WebConstants.ContractDetailsTab.GRACE_PERIOD_FREQ).toString();

			return selectOneGracePeriod;
		}



		public void setSelectOneGracePeriod(String selectOneGracePeriod) {
			this.selectOneGracePeriod = selectOneGracePeriod;
			if(this.selectOneGracePeriod !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.GRACE_PERIOD_FREQ, this.selectOneGracePeriod );
		}



		public HtmlSelectOneMenu getCmbGracePeriod() {
			return cmbGracePeriod;
		}



		public void setCmbGracePeriod(HtmlSelectOneMenu cmbGracePeriod) {
			this.cmbGracePeriod = cmbGracePeriod;
		}



		public String getSelectOneRentType() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_RENT_TYPE))
				selectOneRentType=viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_RENT_TYPE).toString();
			return selectOneRentType;
		}



		public void setSelectOneRentType(String selectOneRentType) {
			
			this.selectOneRentType = selectOneRentType;
			if(this.selectOneRentType    !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_RENT_TYPE, this.selectOneRentType);
		}



		public HtmlGraphicImage getImgInvestorSearch() {
			return imgInvestorSearch;
		}

		public void setImgInvestorSearch(HtmlGraphicImage imgInvestorSearch) {
			this.imgInvestorSearch = imgInvestorSearch;
		}

		public HtmlGraphicImage getImgViewInvestor() {
			return imgViewInvestor;
		}

		public void setImgViewInvestor(HtmlGraphicImage imgViewInvestor) {
			this.imgViewInvestor = imgViewInvestor;
		}

		public String getSelectOneIstisnaaMehthod() {
			
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.ISTISNAA_METHOD))
				this.selectOneIstisnaaMehthod = viewRootMap.get(WebConstants.ContractDetailsTab.ISTISNAA_METHOD).toString();
			return selectOneIstisnaaMehthod;
		}

		public void setSelectOneIstisnaaMehthod(String selectOneIstisnaaMehthod) {
			this.selectOneIstisnaaMehthod = selectOneIstisnaaMehthod;
			if(this.selectOneIstisnaaMehthod !=null )
				viewRootMap.put(WebConstants.ContractDetailsTab.ISTISNAA_METHOD, this.selectOneIstisnaaMehthod);
		}

		public HtmlSelectOneMenu getCmbIstisnaaMehthod() {
			return cmbIstisnaaMehthod;
		}

		public void setCmbIstisnaaMehthod(HtmlSelectOneMenu cmbIstisnaaMehthod) {
			this.cmbIstisnaaMehthod = cmbIstisnaaMehthod;
		}

		public HtmlInputText getTxtAmafShare() {
			return txtAmafShare;
		}

		public void setTxtAmafShare(HtmlInputText txtAmafShare) {
			this.txtAmafShare = txtAmafShare;
			
		}

		public String getAmafShare() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE))
				this.amafShare  = viewRootMap.get(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE).toString();
			return amafShare;
		}

		public void setAmafShare(String amafShare) {
			this.amafShare = amafShare;
			if(this.amafShare !=null )
				viewRootMap.put(WebConstants.ContractDetailsTab.FIRST_PARTY_SHARE, this.amafShare );
		}

		public HtmlInputText getTxtSecondPartyShare() {
			return txtSecondPartyShare;
		}

		public void setTxtSecondPartyShare(HtmlInputText txtSecondPartyShare) {
			this.txtSecondPartyShare = txtSecondPartyShare;
		}

		public String getSecondPartyShare() {
			if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE))
				this.secondPartyShare  = viewRootMap.get(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE).toString();
			return secondPartyShare;
		}

		public void setSecondPartyShare(String secondPartyShare) {
			this.secondPartyShare = secondPartyShare;
			if(this.secondPartyShare !=null )
				viewRootMap.put(WebConstants.ContractDetailsTab.SECOND_PARTY_SHARE, this.secondPartyShare );
		}




	
}
