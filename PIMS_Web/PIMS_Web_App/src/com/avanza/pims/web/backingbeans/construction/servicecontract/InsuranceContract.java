package com.avanza.pims.web.backingbeans.construction.servicecontract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.services.ServiceContractAgent;
import com.avanza.pims.bpel.pimsinsurancecontractbpel.proxy.*;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.WebConstants.TasksTypes_PrepareInsuranceContract;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.construction.plugins.ScopeOfWork;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContract.Page_Mode;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContract.TAB_ID;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.ServiceContractView;
import com.avanza.pims.ws.vo.TenderPropertyView;
import com.avanza.pims.ws.vo.TenderView;
import com.avanza.pims.ws.vo.TenderWorkScopeView;
import com.avanza.ui.util.ResourceUtil;

public class InsuranceContract extends ServiceContract{

	
	private transient Logger log = Logger.getLogger(InsuranceContract.class);
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnSend_For_Approval= new HtmlCommandButton();
	private String tenderTypeInsurance;
	public HtmlCommandButton getBtnSend_For_Approval() {
		return btnSend_For_Approval;
	}
	public void setBtnSend_For_Approval(HtmlCommandButton btnSend_For_Approval) {
		this.btnSend_For_Approval = btnSend_For_Approval;
	}
	@Override
	public void init()
	{
		viewRootMap.put("canAddAttachment",true);
		viewRootMap.put("canAddNote",true);
		viewRootMap.put("canAddWorkScope",true);
		
		viewRootMap.put(PROCEDURE_TYPE , WebConstants.PROCEDURE_TYPE_PREPARE_INSURANCE_CONTRACT);
		viewRootMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_INSURANCE_CONTRACT);
		super.init();
		if(!isPostBack())
		{
			setContractTypeAsInsurance(); 	
			if(viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
				getPageModeFromTaskList();
		}
	}
	private void getPageModeFromTaskList()
	{
		UserTask userTask = (UserTask)viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
    	{
	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareInsuranceContract.APPROVE_INSURANCE_CONTRACT))
	    	{
	    		pageMode=Page_Mode.APPROVAL_REQUIRED;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
	    	else if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareInsuranceContract.LEGAL_DEPT_REVIEW))
	    	{
	    		pageMode=Page_Mode.LEGAL_REVIEW_REQUIRED;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
	    	else if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareInsuranceContract.PRINT))
	    	{
	    		pageMode=Page_Mode.COMPLETE;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
    	}
	}
	private void setContractTypeAsInsurance() {
		ddContractType = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE) , 
				           WebConstants.INSURANCE_CONTRACT);
         if(ddContractType !=null)
         {
        	 viewRootMap.remove(WebConstants.ContractDetailsTab.CONTRACT_TYPE);
        	 
		   viewRootMap.put(DD_CONTRACT_TYPE, ddContractType );
		   if(getIsEnglishLocale())
		     viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescEn());
		   else if(getIsArabicLocale())
			   viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescAr());
         }
	}
	@Override
	
	public void prerender()
	{
		String methodName ="prerender";
		super.prerender();
		try
		{
		
			
		setPageTitleBasedOnProcedureType();
		if(hdnTenderId!=null)
		{
     	   getTenderInfo();
     	   populateTenderInfoInTab();
		}
		  contractor = getContractorInfo();
	     populateContractorInfoInTab();
		 enableDisableControls();
		 if(viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.ERROR_TOTAL_VALUE_NOT_PRESENT))
		 {
			    errorMessages = new ArrayList<String>(0);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.ERROR_MSG_TOTAL_VALUE_NOT_PRESENT));
				viewRootMap.remove(ServiceContractPaymentSchTab.ViewRootKeys.ERROR_TOTAL_VALUE_NOT_PRESENT);
		 }
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			log.LogException(methodName+"|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	public void enableDisableControls()
	{
		String methodName = "enableDisableControls";
		log.logInfo(methodName+"|Start");
		serviceContractView = getServiceContractFromViewRoot();
		btnSend_For_Approval.setRendered(false);
		ScopeOfWork sw =(ScopeOfWork)getBean("pages$ScopeOfWork");
		sw.getSaveScopeOfWork().setRendered(false);
		sw.getImgdeleteLink().setRendered(false);
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
		{
			PageModeAdd();
			sw.getSaveScopeOfWork().setRendered(true);
			sw.getImgdeleteLink().setRendered(true);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{	
			PageModeEdit();
			btnSend_For_Approval.setRendered(true);
			sw.getSaveScopeOfWork().setRendered(true);
			sw.getImgdeleteLink().setRendered(true);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
		{
			btnSave.setRendered(false);
			PageModeApprovalRequired();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.LEGAL_REVIEW_REQUIRED))
			PageModeLegalReviewRequired();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
			PageModeComplete();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ACTIVE))
		    PageModeActive();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
			PageModeView();
		//If tender is searched
			manageControlsForTenders();
			
		log.logInfo(methodName+"|Finish");
		
	}
	protected void PageModeAdd()
	{
		super.PageModeAdd();
		btnSend_For_Approval.setRendered(false);
		tbl_Action.setRendered(false);
		
	}
	
	private boolean hasError()
	{
	 	boolean hasErrors =false;
	 	errorMessages = new ArrayList<String>(0);
	 	Date contractStartDate =null;
        Date contractEndDate =null;
	 	try
	 	{
		 		if(hasContractDetailsError(hasErrors, contractStartDate,contractEndDate))
		 		{
		 			tabPanel.setSelectedTab(TAB_ID.ContractDetails);
		 			return true;
		 		}    
		 		
		        if(!viewRootMap.containsKey(WebConstants.Tender.SCOPE_OF_WORK_LIST) ||
			        	((ArrayList<TenderWorkScopeView>)viewRootMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST)).size()<=0) 
			 	{
			 	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_SCOPE_OF_WORK));
			 	    	tabPanel.setSelectedTab(TAB_ID.WorkScope);
			 			return true;
			 	}   
			 	if(!viewRootMap.containsKey(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST) ||
			 	    		 ((ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST)).size()<=0)
			 	{
			 	    	 
			 	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_PROPERTY));
			 	    	tabPanel.setSelectedTab(TAB_ID.Properties);
			 			return true;
			 	}
			 	if(!viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST) ||
			 	    		 ((ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)).size()<=0)
			 	{
				    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_PAY_SCH));
				    		tabPanel.setSelectedTab(TAB_ID.PaymentSchedule);
				 			return true;
				}
		        else if(!getIsContractValueandRentAmountEqual())
		 	    {
		 	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE)); 
		 	    	tabPanel.setSelectedTab(TAB_ID.PaymentSchedule);
		 			return true;
		 	    }
		        if(!AttachmentBean.mandatoryDocsValidated())
		    	{
		    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
		    		tabPanel.setSelectedTab(TAB_ID.Attachment);
		 			return true;
		    	}
	 	}
	 	catch(Exception e )
	 	{
	 		log.LogException("hasError|Error Occured", e);
	 	}
   	


	 	
	 	return hasErrors;
		
	}
	private boolean hasContractDetailsError(boolean hasErrors,
			Date contractStartDate, Date contractEndDate) throws ParseException {
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_START_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE)!=null)
		      contractStartDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_END_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE)!=null)
		      contractEndDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);
		
		
		if(hdnContractorId== null || hdnContractorId.trim().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACTOR));
			hasErrors = true;
		}
		if(contractStartDate!=null && !contractStartDate.equals("") 
				 && contractEndDate!=null && !contractEndDate.equals("") )
		{
		    DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
		    String todayDate=dateFormat.format(new Date());

		    if (contractStartDate.compareTo(contractEndDate)>0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_EXP));
				hasErrors=true;
			}
		    else if(viewRootMap.get(Page_Mode.PAGE_MODE).equals(Page_Mode.ADD) && contractStartDate.compareTo(dateFormat.parse(todayDate))==-1)
		    {
			  
			      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_TODAY));
			      hasErrors=true;
		     }
		}
		else
		{
			
			if(contractStartDate ==null)
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_START_DATE));
			if(contractEndDate ==null)
		      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_EXPIRY_DATE));
			      
			hasErrors=true;
		}
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null
			&& viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().trim().length()>0)
		{
			try
			{
				Double chkAmountValidity =new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());
			}
			catch(NumberFormatException nfe)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_CONTRACT_VALUE));
			    hasErrors=true;
			}
		} 
		else 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACT_AMOUNT));
			hasErrors=true;
		}
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT) && viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT)!=null && !StringHelper.isEmpty(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString()))
		{
		    try
			{
				Double chkAmountValidity =new Double(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString());
			}
			catch(NumberFormatException nfe)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_GUARANTEE_PERCENTAGE ));
			    hasErrors=true;
			}
         } 
		//Bank guarantee should be optional in case of services tenders and contracts. (Point in List of issues)
//		else 
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_GUARANTEE_PERCENTAGE));
//			hasErrors=true;
//		}
			
		
		    if(!viewRootMap.containsKey(WebConstants.ContractDetailsTab.BANK_ID)  || viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).toString().trim().length()<=0 ||
		    		viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).toString().equals("-1")
		    )
		    {
		    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_BANK));
		    	hasErrors = true;
			}
		return hasErrors;
	}
	private void getContractDetailsFromContractDetailsTab()throws Exception
	{

		DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
		//Set Contract Number if present
		if(contractId !=null && contractId.trim().length()>0 )
		{
			serviceContractView =(ServiceContractView)viewRootMap.get(CONTRACT_VIEW);
			serviceContractView.setContractId(new Long(contractId));

		}
		//set bank guarantee amount
		serviceContractView.setBankGuaranteeValue(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString()));
		
		//Set bank 
		BankView bankView = new BankView();
		bankView.setBankId(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).toString()));
		serviceContractView.setBankView(bankView);
		//Set Total Contract Value
		serviceContractView.setRentAmount(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString()));
		//Set Contractor
		if(hdnContractorId!=null && hdnContractorId.trim().length()>0)
		{
		ContractorView pv =new ContractorView();
		pv.setPersonId(new Long(hdnContractorId));
	    serviceContractView.setContractorView(pv);
		
		}
	    //Set Tender
	    if(hdnTenderId!=null && hdnTenderId.trim().length()>0)
	    {
	    	TenderView tv = new TenderView();
	    	tv.setTenderId(new Long(hdnTenderId));
	    	serviceContractView.setTenderView(tv);
	    }
	    //Set Contract Start Date
	    serviceContractView.setStartDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE));
	    //Set Contract End Date
	    serviceContractView.setEndDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE));
	    //Set Contract Type
	    ddContractType = (DomainDataView)viewRootMap.get(DD_CONTRACT_TYPE);
	    serviceContractView.setContractTypeId(ddContractType.getDomainDataId());

	    if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
	    {
	    	
	    	DomainDataView ddContractStatus = commonUtil.getIdFromType(getContractStatusList(),
	    			                                                        WebConstants.CONTRACT_STATUS_DRAFT);
	      serviceContractView.setStatus(ddContractStatus.getDomainDataId());
	      
	    }
	    //Set CreatedOn CreatedBy UpdatedOn UpdateBy IsDeleted RecordStatus
	    serviceContractView.setIsDeleted(new Long(0));
	    serviceContractView.setRecordStatus(new Long(1));
	    serviceContractView.setUpdatedBy(getLoggedInUser());
        serviceContractView.setUpdatedOn(new Date());
        
        
        if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
        	serviceContractView.setCreatedOn(new Date());
        	serviceContractView.setCreatedBy(getLoggedInUser());
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
        }
//        else if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) && viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT ) )  
//        {
//        	if(contractCreatedOn!=null && !contractCreatedOn.equals(""))
//        		serviceContractView.setCreatedOn(dateFormat.parse(contractCreatedOn));
//        	if(contractCreatedBy!=null && !contractCreatedBy.equals(""))
//        		serviceContractView.setCreatedBy(contractCreatedBy);
//        
//        }
        
	    
	    
	}
	private void setRequestView()
	{
		Set<RequestView> rvSet = new HashSet<RequestView>(0);
		if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
            requestView.setIsDeleted(new Long(0));
            requestView.setRecordStatus(new Long(1));
            DomainDataView ddRequestStatus = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);
  	        requestView.setStatusId(ddRequestStatus.getDomainDataId());  
            requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_INSURANCE_CONTRACT);
            requestView.setRequestDate(new Date());
        }
		else
		   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		if(hdnContractorId!=null && hdnContractorId.trim().length()>0)
		{
	      PersonView applicantView =new PersonView();
	      applicantView.setPersonId(new Long(hdnContractorId));
	      requestView.setApplicantView(applicantView);
		}
		
		requestView.setUpdatedBy(getLoggedInUser());
        requestView.setUpdatedOn(new Date());
        rvSet.add(requestView);
        serviceContractView.setRequestsView(rvSet);
	}
	private void putControlValuesinViews() throws Exception
	{
		String methodName = "putControlValuesinViews";
		log.logInfo(methodName +"|Start");
         getContractDetailsFromContractDetailsTab();
         //Set Request View 
         setRequestView();
         //Set tender work scope
		tenderWorkScopeViewList = (ArrayList<TenderWorkScopeView>)viewRootMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST);
		Set<TenderWorkScopeView> tenderWorkScopeSet = new HashSet<TenderWorkScopeView>(0);
		tenderWorkScopeSet.addAll(tenderWorkScopeViewList);
		serviceContractView.setTenderWorkScopesView(tenderWorkScopeSet);
		//Set tender property 
		tenderPropertyViewList =(ArrayList<TenderPropertyView>)viewRootMap.get(WebConstants.ServiceContractPropertyDetailsTab.TENDER_PROPERTY_LIST);
		Set<TenderPropertyView> tenderPropertyViewSet = new HashSet<TenderPropertyView>(0);
		tenderPropertyViewSet.addAll(tenderPropertyViewList);
		serviceContractView.setTenderPropertyView(tenderPropertyViewSet);
		List<PaymentScheduleView> paymentScheduleDataList =(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST); 
		Set<PaymentScheduleView> paymentScheduleViewViewSet = new HashSet<PaymentScheduleView>(0);
		paymentScheduleViewViewSet.addAll(paymentScheduleDataList);
		serviceContractView.setPaymentScheduleView(paymentScheduleViewViewSet);
		
		log.logInfo(methodName +"|Finish");
	}
	public void btnSendForApproval_Click()
	{
	
		String methodName = "btnSendForApproval_Click";
		log.logInfo(methodName +"|Start");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
	   try {
		        String endPoint= parameters.getParameter(WebConstants.INSURANCE_CONTRACT_BPEL_ENDPOINT);
	    	    PIMSInsuranceContractBPELPortClient port=new PIMSInsuranceContractBPELPortClient();
		   	    port.setEndpoint(endPoint);
		   	    log.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
		   		if(this.contractId!=null && this.contractId.trim().length()>0)
		   		{
			   	 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			   	 port.initiate(new Long(contractId),requestView.getRequestId(),getLoggedInUser(), null, null);
			   	 if(viewRootMap.containsKey(CONTRACT_VIEW) )
			   	    serviceContractView = (ServiceContractView)viewRootMap.get(CONTRACT_VIEW);
			   	   
			   	 if(requestView != null && requestView.getRequestId()!=null)
				 {
					     saveComments(requestView.getRequestId());
						 log.logInfo(methodName+"|"+" Saving Notes...Finish");
						 log.logInfo(methodName+"|"+" Saving Attachments...Start"); 
						 saveAttachments(requestView.getRequestId().toString());
						 log.logInfo(methodName+"|"+" Saving Attachments...Finish");
				  }
		   		  viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
		   	      saveSystemComments(MessageConstants.ContractEvents.APROVAL_REQUIRED);
	   	          successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
		   		}
			   
			   log.logInfo(methodName+"|"+" Finish...");
	    	}
	    	catch(Exception ex)
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    		log.LogException(methodName+"|"+" Error Occured...",ex);
	    	}
	}
	public void btnSave_Click()
	{
		String methodName = "btnSave_Click";
		log.logInfo(methodName +"|Start");
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		ServiceContractAgent sca = new ServiceContractAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			//If Contract is being created on tender and user has not pressed workscope tab then 
			//to populate workscops
			if( hdnTenderId!=null && hdnTenderId.length()>0 
					&& (!viewRootMap.containsKey(WebConstants.Tender.SCOPE_OF_WORK_LIST) ||
		        	(  (ArrayList<TenderWorkScopeView>) viewRootMap.get(WebConstants.Tender.SCOPE_OF_WORK_LIST)).size()<=0)) 
			{
				tabWorkScope_Click();
			}
			if(!hasError())
			{
				 putControlValuesinViews();
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_CREATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_CREATED_SUCCESSFULLY);
				     tabPanel.setSelectedTab(TAB_ID.ContractDetails);
				 }
				 else 
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_UPDATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY); 
				 }
				 
				 serviceContractView = sca.persistServiceContract(serviceContractView);
				 this.contractId = serviceContractView.getContractId().toString(); 
				 viewRootMap.put(CONTRACT_VIEW,serviceContractView);
				 refreshViews();
				 log.logInfo(methodName +"|Contract Added with id ::"+serviceContractView );
				 log.logInfo(methodName+"|"+" Saving Notes...Start");
				 
				 if(requestView != null && requestView.getRequestId()!=null)
				 {
				     saveComments(requestView.getRequestId());
					 log.logInfo(methodName+"|"+" Saving Notes...Finish");
					 log.logInfo(methodName+"|"+" Saving Attachments...Start"); 
					 saveAttachments(requestView.getRequestId().toString());
					 log.logInfo(methodName+"|"+" Saving Attachments...Finish");
				
					 
				 }
				 
				 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				 saveSystemComments(eventDesc);
				 successMessages.add(successMsg);			
				log.logInfo(methodName +"|Finish");
			}
		}
		catch(Exception e)
		{
			log.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}
	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}
	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}
	
	public String getTenderTypeInsurance()
	{
	  tenderTypeInsurance = WebConstants.Tender.TENDER_TYPE_INSURANCE;
	  return tenderTypeInsurance ;	
	}
	public void setTenderTypeInsurance(String tenderTypeInsurance) {
		this.tenderTypeInsurance = tenderTypeInsurance;
	}	
	
}
