package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.ui.util.ResourceUtil;


public class ReturnCheque extends AbstractController {
	
	transient Logger logger = Logger.getLogger(ReturnCheque.class);
	private List<String> successMessages;
	private boolean isArabicLocale = false;
	private boolean isEnglishLocale = false;
	
	private List<BounceChequeView> bounceChequeList  = new ArrayList<BounceChequeView>();
	private List<BounceChequeView> bounceChequeListComming  = new ArrayList<BounceChequeView>();
	private FollowUpView followUpView = new FollowUpView();
	private PropertyServiceAgent psa = new PropertyServiceAgent();
	private HtmlDataTable dataTableChequeDetail;
	private HtmlCommandButton sendForLegalDepartment = new HtmlCommandButton();
	private HtmlCommandButton saveBinding = new HtmlCommandButton();
	
	
	private String txtRemarks;
	private Boolean flage = false;
	private Date toDate =new Date();
	private DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");
	
	
	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	HttpServletRequest requestParam = (HttpServletRequest) FacesContext.
	getCurrentInstance().getExternalContext().getRequest();
	ServletContext servletcontext = (ServletContext) FacesContext
	.getCurrentInstance().getExternalContext().getContext();
	Map sessionMap;
	FacesContext context = getFacesContext();
	HashMap prdIdMap = new HashMap();
	CommonUtil commUtil = new CommonUtil();
	
public void init(){
		
		super.init();
		sessionMap = context.getExternalContext().getSessionMap();
		
		try {
		
		if(!isPostBack()){
			
			
			DomainDataView ddv = getIdFromType(
					getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
					WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
			flage = false;
			bounceChequeListComming = (List<BounceChequeView>)sessionMap.get("CHEQUE_LIST");
			
			for (BounceChequeView bcv : bounceChequeListComming) {
				if(!prdIdMap.containsKey(bcv.getPaymentReceiptDetailId()))
				{
					prdIdMap.put(bcv.getPaymentReceiptDetailId(),bcv.getPaymentReceiptDetailId());
					bounceChequeList.add(bcv);
					if(bcv.getStatusId().equals(ddv.getDomainDataId()))
						flage = true;
				}
			}
			
			if(flage){
			saveBinding.setRendered(false);
			viewMap.put("SAVE_DONE",true);
			}
			
			if(bounceChequeList!=null && bounceChequeList.size()>0){
			viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL,bounceChequeList);
			getBounceChequeList();
			}
			
		}
		
		} catch (Exception e) {
        logger.LogException("Exception In InIt::::::::", e);
		}
   }

public void preprocess(){
	super.preprocess();
}

public void prerender(){
	super.prerender();
}	

@SuppressWarnings("unchecked")
public void onBounceAndCreateFollowup()
{
	logger.logInfo( "onBounceAndCreateFollowup|Start");
	successMessages= new ArrayList<String>();
	String notesOwner = WebConstants.FOLLOW_UP;
//	DomainDataView ddv = getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
//			                           WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
	Boolean isStatusChange = false;
	Boolean isFollowUpDone = false;
	Boolean isSaveDone = false;
	
	if(viewMap.containsKey("SAVE_DONE")&& (Boolean)viewMap.get("SAVE_DONE"))
		isSaveDone = true;
	try 
	{
		BounceChequeView bCV = (BounceChequeView)dataTableChequeDetail.getRowData();
	    DomainDataView ddOpenStatus  = CommonUtil.getIdFromType(
	    														CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS),
	                                       						WebConstants.FollowUpStatus.OPEN
	                                       					   );
	    DomainDataView ddBouncedType = CommonUtil.getIdFromType(
	    														 CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpType.FOLLOW_UP_TYPE),
	    														 WebConstants.FollowUpType.BOUNCED
	    														);
		followUpView	= fillFollowUpView(bCV,ddOpenStatus  ,ddBouncedType);
		if(txtRemarks!=null && !txtRemarks.equals(""))
		{
		    saveComments(bCV.getPaymentReceiptDetailId(),txtRemarks,notesOwner);
		}
		if(isSaveDone)
		{
		  followUpView =  psa.addFollowUpInTransaction( followUpView, bCV.getPaymentReceiptDetailId(), null,null, bCV.getBouncedReason(),null );
		  isFollowUpDone = true;
		}
		else
		{
		  followUpView = psa.addFollowUpInTransaction(
				  										followUpView, 
				  										bCV.getPaymentReceiptDetailId(), 
				  										WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_ID,
				  										bCV.getChequeNumber(), 
				  										bCV.getBouncedReason(),
				  										null 
				  									 );
		  isStatusChange = true;
		}
		if(followUpView.getCheckAllreadyPresent())
		{
			successMessages.add(java.text.MessageFormat.format(ResourceUtil.getInstance().getProperty("bouncedCheque.msg.followUpPresent"),followUpView.getFollowUpNumber()));
			viewMap.put("SAVE_DONE",true);
		}
		if (!followUpView.getCheckAllreadyPresent() &&  isStatusChange) 
		{
			successMessages.add(getBundleMessage("bouncedCheque.msg.chequeBounced"));
			successMessages.add(getBundleMessage("bouncedCheque.msg.saveFollowUp"));
			sendForLegalDepartment.setRendered(false);
			saveBinding.setRendered(false);
			viewMap.put("SAVE_DONE",true);
		}
		if (!followUpView.getCheckAllreadyPresent() && isFollowUpDone) 
		{
			successMessages.add(getBundleMessage("bouncedCheque.msg.saveFollowUp"));
			saveBinding.setRendered(false);
			sendForLegalDepartment.setRendered(false);
			viewMap.put("SAVE_DONE",true);
		}
		bCV.setStatusId(WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED_ID);
//		bCV.setStatusEn(ddv.getDataDescEn());
//		bCV.setStatusAr(ddv.getDataDescAr());
		
	} 
	catch (Exception e) 
	{
		logger.LogException("onBounceAndCreateFollowup|Error Occured:",e);
	}
}
@SuppressWarnings("unchecked")
public String btn_SendToDepartment()
{
	String methodName = "btn_SendToDepartment|";
	logger.logInfo( methodName + "Start" );
	successMessages= new ArrayList<String>();
	
	List<BounceChequeView> bounceCheList = new ArrayList<BounceChequeView>();
	String notesOwner = WebConstants.FOLLOW_UP;
	DomainDataView ddv = getIdFromType(getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
			                           WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED);
	Boolean isStatusChange = false;
	Boolean isFollowUpDone = false;
	Boolean isSaveDone = false;
	if(viewMap.containsKey("SAVE_DONE")&& (Boolean)viewMap.get("SAVE_DONE"))
	{
		isSaveDone = true;
	}
	try 
	{
	     if(viewMap.containsKey(WebConstants.SESSION_FOR_CHEQUE_DETAIL))
	     {
	       DomainDataView ddOpenStatus  = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS),
                                           WebConstants.FollowUpStatus.OPEN);
           DomainDataView ddBouncedType = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpType.FOLLOW_UP_TYPE),
			                               WebConstants.FollowUpType.BOUNCED);
		   bounceCheList = (ArrayList<BounceChequeView>)viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL);
		   for(BounceChequeView bCV: bounceCheList)
		   {
				if( txtRemarks!=null && !txtRemarks.equals( "" ) )
				{
				  saveComments(bCV.getPaymentReceiptDetailId(),txtRemarks,notesOwner);
				}
				followUpView	= fillFollowUpView(bCV,ddOpenStatus  ,ddBouncedType);
				if( isSaveDone )
				{
				 psa.addFollowUpInTransaction(followUpView, null, null, null, bCV.getBouncedReason(),null  );
				 isFollowUpDone = true;
				}
				else
				{
				 psa.addFollowUpInTransaction( followUpView, bCV.getPaymentReceiptDetailId(), ddv.getDomainDataId(),bCV.getChequeNumber(),
						                       bCV.getBouncedReason() ,null
				                              );
				 isStatusChange = true;
				}
		    }		
			
		 
			if (isStatusChange) 
			{
				successMessages.add(getBundleMessage("bouncedCheque.msg.chequeBounced"));
				successMessages.add(getBundleMessage("bouncedCheque.msg.saveFollowUp"));
				sendForLegalDepartment.setRendered(false);
				saveBinding.setRendered(false);
				viewMap.put("SAVE_DONE",true);
			}
			if (isFollowUpDone) 
			{
				successMessages.add(getBundleMessage("bouncedCheque.msg.saveFollowUp"));
				saveBinding.setRendered(false);
				sendForLegalDepartment.setRendered(false);
				viewMap.put("SAVE_DONE",true);
			}
		
		
	     }
	     logger.logInfo( methodName+"Finish"  );
	     return "btn_SendToDepartment";
	} 
	catch (Exception e) 
	{
		
		logger.LogException( methodName , e );
	}
	return "";
	
	
	
}

public List<BounceChequeView> getBounceChequeList() {
	if (viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL)!=null)
		bounceChequeList = (List<BounceChequeView>) viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL);
	return bounceChequeList;
}

public void setBounceChequeList(List<BounceChequeView> bounceChequeList) {
	this.bounceChequeList = bounceChequeList;
}

public HtmlDataTable getDataTableChequeDetail() {
	return dataTableChequeDetail;
}

public void setDataTableChequeDetail(HtmlDataTable dataTableChequeDetail) {
	this.dataTableChequeDetail = dataTableChequeDetail;
}



public String getSuccessMessages() 
{

	return commUtil.getErrorMessages(successMessages);
}



public HtmlCommandButton getSendForLegalDepartment() {
	return sendForLegalDepartment;
}

public void setSendForLegalDepartment(HtmlCommandButton sendForLegalDepartment) {
	this.sendForLegalDepartment = sendForLegalDepartment;
}

public boolean getIsArabicLocale() {
	isArabicLocale = !getIsEnglishLocale();
	return isArabicLocale;
}

public boolean getIsEnglishLocale() {

	
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	
	return isEnglishLocale;
}

public String getTxtRemarks() {
	return txtRemarks;
}

public void setTxtRemarks(String txtRemarks) {
	this.txtRemarks = txtRemarks;
}

private void saveComments(Long paymentDetailId,String rmk,String nodeOwn) throws Exception
{
	String methodName="saveComments|";
	try{
		logger.logInfo(methodName + "started...");

		
		
		CommonUtil.saveRemarksAsComments(paymentDetailId, rmk, nodeOwn);
	

		logger.logInfo(methodName + "completed successfully!!!");
	}
	catch (Exception exception) {
		logger.LogException(methodName + "crashed ", exception);
		throw exception;
	}
}

		@SuppressWarnings( "unchecked" )
		public String btn_save()
		{
	        String methodName = "btn_save|";
		    logger.logInfo( methodName +"Start|"  );
			successMessages= new ArrayList<String>();
			List<BounceChequeView> bounceCheList = new ArrayList<BounceChequeView>();
			String paymentDetailList = new String();
			paymentDetailList = "";
			int count=0;
			String notesOwner = WebConstants.PAYMENT_RECEIPT_DETAIL;
			DomainDataView ddv = getIdFromType(
												getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS),
												WebConstants.PAYMENT_SCHEDULE_STATUS_BOUNCED
											   );
			Boolean isStatusChange = false;
			try 
			{
				 if(viewMap.containsKey(WebConstants.SESSION_FOR_CHEQUE_DETAIL))
				 {
					bounceCheList = (List<BounceChequeView>)viewMap.get(WebConstants.SESSION_FOR_CHEQUE_DETAIL);
					for(BounceChequeView bCV: bounceCheList)
					{
						if(txtRemarks!=null && !txtRemarks.equals(""))
						{
						 saveComments( bCV.getPaymentReceiptDetailId(), txtRemarks, notesOwner );
						}
						isStatusChange=	psa.markChequeStatus( bCV.getPaymentReceiptDetailId(), ddv.getDomainDataId(), 
								                              bCV.getChequeNumber(), getLoggedInUserId(), bCV.getBouncedReason(),null );
						if( count == 0 )
							paymentDetailList+=bCV.getPaymentReceiptDetailId().toString();
						else
						    paymentDetailList+=","+bCV.getPaymentReceiptDetailId().toString();
							count++;
					}
					if (isStatusChange) 
					{
						successMessages.add( getBundleMessage( "bouncedCheque.msg.chequeBounced" ) );
						saveBinding.setRendered( false );
						viewMap.put( "SAVE_DONE", true );
						refrashChequeDetail( paymentDetailList );
						getBounceChequeList();
			
					} else 
					{
						successMessages.add( getBundleMessage( "bouncedCheque.msg.chequeBounced" ) );
					}
					
					
				  }
				 logger.logInfo( methodName +"Finish"  );
				 return "btn_save";	
			} 
			catch (Exception e) 
			{
				logger.LogException( methodName , e );
	//			errorMessages = new ArrayList<String>(0);
	//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			return "";
		
        }

public List<BounceChequeView> getBounceChequeListComming() {
	return bounceChequeListComming;
}

public void setBounceChequeListComming(
		List<BounceChequeView> bounceChequeListComming) {
	this.bounceChequeListComming = bounceChequeListComming;
}

private List<DomainDataView> getDomainDataListForDomainType(
		String domainType) {
	List<DomainDataView> ddvList = new ArrayList<DomainDataView>();
	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext
			.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	Iterator<DomainTypeView> itr = list.iterator();
	// Iterates for all the domain Types
	while (itr.hasNext()) {
		DomainTypeView dtv = itr.next();
		if (dtv.getTypeName().compareTo(domainType) == 0) {
			Set<DomainDataView> dd = dtv.getDomainDatas();
			// Iterates over all the domain datas
			for (DomainDataView ddv : dd) {
				ddvList.add(ddv);
			}
			break;
		}
	}

	return ddvList;
}
public DomainDataView getIdFromType(List<DomainDataView> ddv, String type) {
	String methodName = "getIdFromType";
	logger.logInfo(methodName + "|" + "Start...");
	logger.logInfo(methodName + "|" + "DomainDataType To search..." + type);
	Long returnId = new Long(0);
	HashMap hMap = new HashMap();
	DomainDataView returnDomainDataView = new DomainDataView();
	for (int i = 0; i < ddv.size(); i++) {
		DomainDataView domainDataView = (DomainDataView) ddv.get(i);
		if (domainDataView.getDataValue().equals(type)) {
			returnDomainDataView = domainDataView;
			return returnDomainDataView;
		}

	}
	logger.logInfo(methodName + "|" + "Finish...");
	return returnDomainDataView;

}
public String getBundleMessage(String key) {
	logger.logInfo("getBundleMessage(String) started...");
	String message = "";
	try {
		message = ResourceUtil.getInstance().getProperty(key);

		logger
				.logInfo("getBundleMessage(String) completed successfully!!!");
	} catch (Exception exception) {
		successMessages.add(ResourceUtil.getInstance().getProperty(
				MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("getBundleMessage(String) crashed ", exception);
	}
	return message;
}

public HtmlCommandButton getSaveBinding() {
	return saveBinding;
}

public void setSaveBinding(HtmlCommandButton saveBinding) {
	this.saveBinding = saveBinding;
}
@SuppressWarnings( "unchecked" )
public void refrashChequeDetail( String PaymentDetailList )
{

	try
	{
			ArrayList<Long> paymentDetailids = new ArrayList<Long>() ;
			
			if(PaymentDetailList.contains(","))
			{
				String[] stringArray= PaymentDetailList.split(",");
				for(int i=0;i<stringArray.length;i++)
				{
					paymentDetailids.add(Long.parseLong(stringArray[i]));
				}
			}
		   else
		   {
				paymentDetailids.add(Long.parseLong(PaymentDetailList));
		   }
		   bounceChequeList = new ArrayList<BounceChequeView>();
		   List<PaymentReceiptDetailView> paymentReceiptDetailViewList = 	psa.getPaymentReceiptDetailList(paymentDetailids);
		   BounceChequeView boucCheViewForList = new BounceChequeView();
		   for(PaymentReceiptDetailView payRecpDetailView:paymentReceiptDetailViewList)
		   {
	           	 boucCheViewForList = new BounceChequeView();
	           	 boucCheViewForList.setBouncedReason( payRecpDetailView.getBouncedReason() );
	           	 boucCheViewForList.setPaymentReceiptDetailId(payRecpDetailView.getPaymentReceiptDetailId());
	           	 boucCheViewForList.setReceiptNumber(payRecpDetailView.getPaymentReceiptNumber());
	           	 boucCheViewForList.setChequeNumber(payRecpDetailView.getMethodRefNo());
	           	 boucCheViewForList.setPaymentDetailAmount(payRecpDetailView.getAmount());
	           	 boucCheViewForList.setAccountNumber(payRecpDetailView.getAccountNo());
	           	 boucCheViewForList.setPaymentMethodDescription(payRecpDetailView.getPaymentMethod().getDescription());
	           	 boucCheViewForList.setStatusEn(payRecpDetailView.getMethodStatusEn());
	       	     boucCheViewForList.setStatusAr(payRecpDetailView.getMethodStatusAr());
	       	     boucCheViewForList.setStatusId(payRecpDetailView.getMethodStatusId());
	       	     boucCheViewForList.setPaymentTypeDescriptionEn(payRecpDetailView.getPaymentMethod().getDescription());
	       	     boucCheViewForList.setPaymentTypeDescriptionAr(payRecpDetailView.getPaymentMethod().getDescription());
	       	     boucCheViewForList.setBankNameEn(payRecpDetailView.getBankEn());
	       	     boucCheViewForList.setBankNameAr(payRecpDetailView.getBankAr());
	       	     boucCheViewForList.setNumber(payRecpDetailView.getContractNumber());
	       	     boucCheViewForList.setPaymentNumber(payRecpDetailView.getPaymentNumber());
	       	     boucCheViewForList.setTenantName(payRecpDetailView.getTenantFullName());
	       	     boucCheViewForList.setContractId( new Long ( payRecpDetailView.getContractIdString() ) );
	           	 bounceChequeList.add(boucCheViewForList);
	       }
		   viewMap.put(WebConstants.SESSION_FOR_CHEQUE_DETAIL, bounceChequeList);
		   getBounceChequeList();
	}
	catch (Exception e) 
	{
		e.printStackTrace();
	}
}



public FollowUpView fillFollowUpView(BounceChequeView bCV,DomainDataView ddOpenStatus,DomainDataView ddBouncedType)
{
	followUpView.setStatusId(ddOpenStatus.getDomainDataId());
	followUpView.setContractId(bCV.getContractId());
	followUpView.setPaymentReceiptDetailId(bCV.getPaymentReceiptDetailId());
	followUpView.setFollowUpTypeId(ddBouncedType.getDomainDataId());
	followUpView.setStartDate(new Date());
	followUpView.setFollowupDate(new Date());
	followUpView.setUpdatedBy(getLoggedInUser());
	followUpView.setUpdatedOn(new Date());
	followUpView.setCreatedBy(getLoggedInUser());
	followUpView.setCreatedOn(new Date());
	followUpView.setRecordStatus(1L);
	followUpView.setIsDeleted(0L);
	return followUpView	;
}

private String getLoggedInUser() {
	
	HttpSession session = (HttpSession) getFacesContext().getExternalContext()
			.getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
		UserDbImpl user = (UserDbImpl) session
				.getAttribute(WebConstants.USER_IN_SESSION);
		loggedInUser = user.getLoginId();
	}

	return loggedInUser;
}

public Date getToDate() {
	return toDate;
}

public void setToDate(Date toDate) {
	this.toDate = toDate;
}
public String getDateFormat(){
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getDateFormat();
}

public TimeZone getTimeZone()
{
       return TimeZone.getDefault();
}
}

