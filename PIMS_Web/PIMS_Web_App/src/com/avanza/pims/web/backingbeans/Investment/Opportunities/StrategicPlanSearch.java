package com.avanza.pims.web.backingbeans.Investment.Opportunities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.investment.InvestmentOpportunityService;
import com.avanza.pims.ws.vo.StrategicPlanView;
import com.avanza.ui.util.ResourceUtil;

public class StrategicPlanSearch extends AbstractController 
{

	protected List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";
	
	public interface Keys {
		public static String PAGE_MODE="PAGE_MODE";
		public static String PAGE_MODE_SELECT_ONE_POPUP  = "MODE_SELECT_ONE_POPUP";
		public static String PAGE_MODE_SELECT_MANY_POPUP  = "MODE_SELECT_MANY_POPUP";
		public static String PAGE_MODE_FULL  = "MODE_FULL";
		public static String STRATEGIC_PLAN_LIST = "STRATEGIC_PLAN_LIST";
		public static String RECORD_SIZE = "RECORD_SIZE";
		public static String PAGINATOR_MAX_PAGES = "PAGINATOR_MAX_PAGES";
		public static String CONTRACT_TYPE = "CONTRACT_TYPE";
		public static String ALLOWED_STATUSES = "ALLOWED_STATUSES";
		public static String ALLOWED_TYPES = "ALLOWED_TYPES";
	}

	private final static Logger logger = Logger.getLogger(StrategicPlanSearch.class);

	@SuppressWarnings("unchecked")
	private final Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();	

	// binded attributes with search criteria components
	
	// attributes used for a data grid
	private Integer recordSize = 0;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;

	// UI component attributes
	private HtmlInputText htmlInputPlanNumber = new HtmlInputText();
	private HtmlSelectOneMenu htmlSelectOnePlanStatus = new HtmlSelectOneMenu();
	private HtmlInputText htmlInputPlanameEn = new HtmlInputText();
	private HtmlInputText htmlInputPlanameAr = new HtmlInputText();
	private HtmlCalendar htmlCalendarCreationDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarCreationDateTo = new HtmlCalendar();
	private List<StrategicPlanView> strategicPlanViewList = new ArrayList<StrategicPlanView>(0);	
	
	private HtmlDataTable dataTable;	
	HttpServletRequest request  =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	@SuppressWarnings("unchecked")
	public void init() {
		logger.logInfo("Stepped into the init method");

		try {
			if (!isPostBack()) 
			{				
				if(request.getParameter(Keys.PAGE_MODE)!=null)
					viewMap.put(Keys.PAGE_MODE, request.getParameter(Keys.PAGE_MODE).toString());
				else
					viewMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_FULL);
				
				loadPreviousSearchCriteria();
			}			
			logger.logInfo("init method completed successfully!");
		} catch (Exception e) {
			logger.logError("init crashed due to:" + e.getStackTrace());
		}
	}
	public void loadPreviousSearchCriteria()
	{
		try
		{
			if(sessionMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA))
			{
				HashMap<String, Object> searchCrtieriaMap = (HashMap<String, Object>) sessionMap.get(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA);
				setSearchCriteria(searchCrtieriaMap);
				if(searchCrtieriaMap != null && searchCrtieriaMap.size() >0)
					btnSearch_Click();
			}
		}
		catch(Exception exp)
		{
			logger.LogException("Failed to load previous search criteria", exp);
		}		
	}
	public void putSearchSearchCriteriaInSession()
	{
		HashMap<String, Object> searchCrtieriaMap = getSearchCriteria();
		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA,searchCrtieriaMap);
	}
	@Override
    public void prerender() 
    {		
    }	
	public boolean getIsPageModeSelectOnePopUp()
	{
		boolean isPageModeSelectOnePopUp= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_SELECT_ONE_POPUP))
				isPageModeSelectOnePopUp= true;
			else
				isPageModeSelectOnePopUp= false;
		}
		else
			isPageModeSelectOnePopUp =false;
		return isPageModeSelectOnePopUp;
	}
	
	public boolean getIsFullMode()
	{
		boolean isFullMode= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_FULL))
				isFullMode= true;
			else
				isFullMode= false;
		}
		else
			isFullMode = false;
		return isFullMode;
	}
	
	public boolean getIsViewModePopUp()
	{
		if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
			return true;
		else
			return false;
		
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public boolean getIsPageModeSelectManyPopUp()
	{
		boolean  isPageModeSelectManyPopUp= true;
		if(viewMap.get(Keys.PAGE_MODE)!=null)
		{
			if(viewMap.get(Keys.PAGE_MODE).toString().equals(Keys.PAGE_MODE_SELECT_MANY_POPUP))
				isPageModeSelectManyPopUp= true;
			else
				isPageModeSelectManyPopUp= false;
		}
		else
			isPageModeSelectManyPopUp =false;
		return isPageModeSelectManyPopUp;
	}


	private HashMap<String, Object> getSearchCriteria()
	{
		HashMap<String, Object> searchCrtieriaMap = new HashMap<String, Object>();
		
		String emptyValue = "-1";		
		Object planNumber  =htmlInputPlanNumber.getValue();
		Object planStauts  =htmlSelectOnePlanStatus.getValue();
		Object planNameEn  =htmlInputPlanameEn.getValue();
		Object planNameAr  =htmlInputPlanameAr.getValue();
		Object creationdatefrom =htmlCalendarCreationDateFrom.getValue();
		Object creationdateto =htmlCalendarCreationDateTo.getValue();
		
		if(creationdatefrom != null && !creationdatefrom.toString().trim().equals(""))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_FROM, creationdatefrom);
		}		
		if(creationdateto != null && !creationdateto.toString().trim().equals(""))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_TO,creationdateto);
		}
		if(planNumber != null && !planNumber.toString().trim().equals(""))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NUMBER, planNumber.toString());
		}		
		if(planNameEn != null && !planNameEn.toString().trim().equals(""))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_EN, planNameEn.toString());
		}		
		if(planNameAr != null && !planNameAr.toString().trim().equals(""))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_AR, planNameAr.toString());
		}		
		if(planStauts != null && !planStauts.toString().trim().equals("") && !planStauts.toString().trim().equals(emptyValue))
		{
			searchCrtieriaMap.put(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_STATUS, planStauts.toString());
		}
		
		return searchCrtieriaMap;
	}
	private void setSearchCriteria(HashMap<String, Object> searchCrtieriaMap)
	{
		
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_FROM) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_FROM) != null)
		{
			Object creationdatefrom = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_FROM);
			
			if(!creationdatefrom.equals(""))
				htmlCalendarCreationDateFrom.setValue(creationdatefrom);
		}	
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_TO) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_TO) != null)
		{
			Object creationdateto = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_CREATE_DATE_TO);
			
			if(!creationdateto.equals(""))
				htmlCalendarCreationDateTo.setValue(creationdateto);
		}	
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_EN) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_EN) != null)
		{
			Object planNameEn = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_EN);
			
			if(!planNameEn.equals(""))
				htmlInputPlanameEn.setValue(planNameEn);
		}	
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_AR) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_AR) != null)
		{
			Object planNameAr = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NAME_AR);
			
			if(!planNameAr.equals(""))
				htmlInputPlanameAr.setValue(planNameAr);
		}
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NUMBER) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NUMBER) != null)
		{
			Object planNumber = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_NUMBER);
			
			if(!planNumber.equals(""))
				htmlInputPlanNumber.setValue(planNumber);
		}
		if(searchCrtieriaMap.containsKey(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_STATUS) &&
				searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_STATUS) != null)
		{
			Object planStauts = searchCrtieriaMap.get(WebConstants.STRATEGIC_PLAN_SEARCH_CRITERIA.PLAN_STATUS);
			
			if(!planStauts.equals(""))
				htmlSelectOnePlanStatus.setValue(planStauts);
		}
		
		
	}
	public void btnSearch_Click()
	{
		errorMessages = new ArrayList<String>(0);
		String methodName = "btnSearch_Click";
		InvestmentOpportunityService ios  = new InvestmentOpportunityService();
		try {
			HashMap searchCriteriaMap = getSearchCriteria();
			logger.logInfo(methodName+"|Start");
			if(searchCriteriaMap.size() > 0)
			{				
				strategicPlanViewList = ios.getStrategicPlansByCriteria(searchCriteriaMap);
				 viewMap.put(Keys.STRATEGIC_PLAN_LIST, strategicPlanViewList );
				if(strategicPlanViewList != null)
				 viewMap.put(Keys.RECORD_SIZE,strategicPlanViewList.size());
				else
				 viewMap.put(Keys.RECORD_SIZE,0);
				
				if(sessionMap.containsKey(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA))
					sessionMap.remove(WebConstants.StrategicPlan.STRATEGIC_PLAN_SEARCH_CRITERIA);
			}
			else 
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			logger.logInfo(methodName+"|Finish");
		} catch (Exception e) {
                logger.LogException(methodName+"|Exception Occured", e);
                errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public String onSingleSelect()
	{
		StrategicPlanView strategicPlanView = (StrategicPlanView) dataTable.getRowData();
		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_VIEW, strategicPlanView);
		executeJavascript("passSelectedStrategicPlanView();");
		return null;
	}
	
	private void executeJavascript(String javascript) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Integer getRecordSize() {
		recordSize = (Integer) viewMap.get(Keys.RECORD_SIZE);
		if (recordSize == null)
			recordSize = 0;

		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}


	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public List<StrategicPlanView> getStrategicPlanViewList() {
		   if(viewMap.get(Keys.STRATEGIC_PLAN_LIST)!=null)
			 strategicPlanViewList = (ArrayList<StrategicPlanView>)viewMap.get(Keys.STRATEGIC_PLAN_LIST);
		return strategicPlanViewList;
	}

	public void setStrategicPlanViewList(List<StrategicPlanView> strategicPlanViewList) {
		this.strategicPlanViewList = strategicPlanViewList;
	}
	public String getInfoMessage() {
		return infoMessage;
	}
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String onEdit(){
		String METHOD_NAME = "onEdit()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			
			if(dataTable.isRowAvailable())
			{
				StrategicPlanView strategicPlanView=(StrategicPlanView)dataTable.getRowData();
		   		Long planId = strategicPlanView.getStrategicPlanId();
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, planId);
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE, WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_DRAFT);
		   		putSearchSearchCriteriaInSession();
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
			}	
	   		
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception e) {
			logger.LogException(METHOD_NAME + " crashed ", e);
		}		
		return "StrategicPlanDetails";
	}

	@SuppressWarnings("unchecked")
	public String onView(){
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			if(dataTable.isRowAvailable())
			{
				StrategicPlanView strategicPlanView=(StrategicPlanView)dataTable.getRowData();
		   		Long planId = strategicPlanView.getStrategicPlanId();
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, planId);
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE, WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_VIEW);
		   		putSearchSearchCriteriaInSession();
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
			}
			
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "StrategicPlanDetails";
	}
	@SuppressWarnings("unchecked")
	public String onClose()
	{
		String METHOD_NAME = "onView()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			if(dataTable.isRowAvailable())
			{				
				StrategicPlanView strategicPlanView=(StrategicPlanView)dataTable.getRowData();
		   		Long planId = strategicPlanView.getStrategicPlanId();
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_ID, planId);
		   		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE, WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_CLOSE);
		   		putSearchSearchCriteriaInSession();
		   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
			}
			
	   		logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed ", exception);
		}	
		return "StrategicPlanDetails";
	}
	
	@SuppressWarnings("unchecked")
	public String onDelete(){
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			if(dataTable.isRowAvailable())
			{
				InvestmentOpportunityService ios = new InvestmentOpportunityService();
				String loggedinUser = CommonUtil.getLoggedInUser();
				StrategicPlanView strategicPlanView =(StrategicPlanView)dataTable.getRowData();
				strategicPlanView.setUpdatedBy(loggedinUser);
				strategicPlanView.setUpdatedOn(new Date());
				strategicPlanView.setIsDeleted(WebConstants.IS_DELETED_TRUE);
				ios.persistStrategicPlan(strategicPlanView);
				
				infoMessage = CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_DELETE_SUCCESS);
	   			int index = dataTable.getRowIndex();
	   			strategicPlanViewList = (List<StrategicPlanView>) viewMap.get(Keys.STRATEGIC_PLAN_LIST);
	   	    	strategicPlanViewList.remove(index);
	   	    	viewMap.put(Keys.STRATEGIC_PLAN_LIST, strategicPlanViewList );
	   	    	viewMap.put(Keys.RECORD_SIZE, strategicPlanViewList.size());
		   			
	   		
			}
			logger.logInfo(METHOD_NAME + " completed successfully!!!");
		}
		catch (Exception exception) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.StrategicPlan.Messages.STRATEGIC_PLAN_DELETE_FAIL));
			logger.LogException(METHOD_NAME + " crashed ", exception);			
		}	
		return "";
	}
	
	public String onAddPlan()
    {
    	logger.logInfo("onAddPlan() started...");
    	try{    	
    		putSearchSearchCriteriaInSession();
    		sessionMap.put(WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE, WebConstants.StrategicPlan.STRATEGIC_PLAN_DETAILS_MODE_NEW);
	   		setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_STRATEGIC_PLAN_SEARCH);
    		logger.logInfo("onAddPlan() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("onAddPlan() crashed ",exception);
    	}
        return "StrategicPlanDetails";
    }
	public HtmlInputText getHtmlInputPlanNumber() {
		return htmlInputPlanNumber;
	}
	public void setHtmlInputPlanNumber(HtmlInputText htmlInputPlanNumber) {
		this.htmlInputPlanNumber = htmlInputPlanNumber;
	}
	public HtmlSelectOneMenu getHtmlSelectOnePlanStatus() {
		return htmlSelectOnePlanStatus;
	}
	public void setHtmlSelectOnePlanStatus(HtmlSelectOneMenu htmlSelectOnePlanStatus) {
		this.htmlSelectOnePlanStatus = htmlSelectOnePlanStatus;
	}
	public HtmlInputText getHtmlInputPlanameEn() {
		return htmlInputPlanameEn;
	}
	public void setHtmlInputPlanameEn(HtmlInputText htmlInputPlanameEn) {
		this.htmlInputPlanameEn = htmlInputPlanameEn;
	}
	public HtmlInputText getHtmlInputPlanameAr() {
		return htmlInputPlanameAr;
	}
	public void setHtmlInputPlanameAr(HtmlInputText htmlInputPlanameAr) {
		this.htmlInputPlanameAr = htmlInputPlanameAr;
	}
	public HtmlCalendar getHtmlCalendarCreationDateFrom() {
		return htmlCalendarCreationDateFrom;
	}
	public void setHtmlCalendarCreationDateFrom(
			HtmlCalendar htmlCalendarCreationDateFrom) {
		this.htmlCalendarCreationDateFrom = htmlCalendarCreationDateFrom;
	}
	public HtmlCalendar getHtmlCalendarCreationDateTo() {
		return htmlCalendarCreationDateTo;
	}
	public void setHtmlCalendarCreationDateTo(
			HtmlCalendar htmlCalendarCreationDateTo) {
		this.htmlCalendarCreationDateTo = htmlCalendarCreationDateTo;
	}
	
}