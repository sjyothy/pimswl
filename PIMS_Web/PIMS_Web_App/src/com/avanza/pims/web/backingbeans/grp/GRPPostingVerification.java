package com.avanza.pims.web.backingbeans.grp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.ExceptionCodes;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.GRPServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.SettlementBean;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.vo.FinancialTransactionView;
import com.avanza.ui.util.ResourceUtil;

public class GRPPostingVerification extends AbstractController
{
	private HtmlDataTable transactionsGrid = new HtmlDataTable();
	private List<FinancialTransactionView> transactionsDataList;
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private Integer pageIndex = 0;
	private String financialId;
	protected List<String> errorMessages = new ArrayList<String>();
	private static final Logger logger = Logger.getLogger(GRPPostingVerification.class);
@SuppressWarnings("unchecked")
	public void init()
    {
		super.init();	
		if(getFacesContext().getExternalContext().getSessionMap().containsKey("LIST_OF_FINANCIAL_TRANSACTION_VIEW")&&
		   getFacesContext().getExternalContext().getSessionMap().get("LIST_OF_FINANCIAL_TRANSACTION_VIEW")!=null)
		{
			transactionsDataList = (ArrayList<FinancialTransactionView>)getFacesContext().getExternalContext().getSessionMap().get("LIST_OF_FINANCIAL_TRANSACTION_VIEW");
			try 
			{
				transactionsDataList = new GRPService().getFinancialAccountNumbers(transactionsDataList);
			}
			catch (PimsBusinessException e) 
		        { 
		            if(e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_CUSTOMER_NUMBER_ERROR))
		            	errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosting.CustomerNotPresent"));
		            else if(e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_FIN_ACC_ERROR))
		            {
		                this.setFinancialId(e.getExceptionMessage());
		            	errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosting.FinAccNotPresent"));
		            
		    			return;
		            }
		            else if ( e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_AR_IVOICE_NOT_PRESENT))
		            		this.errorMessages.add(java.text.MessageFormat.format(
								     ResourceUtil.getInstance().getProperty("settlement.msg.ArInvoiceNotPresent"),e.getExceptionMessage()));
		            logger.LogException("Init"+"|Error Occured::",e);
		        }
			viewMap.put(WebConstants.GRP.TRANSACTIONS_LIST, transactionsDataList);
			getFacesContext().getExternalContext().getSessionMap().remove("LIST_OF_FINANCIAL_TRANSACTION_VIEW");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void  confirmPosting()
	{
		final String methodName="confirmPosting";
		logger.logInfo(methodName +"|Start");
		GRPServiceAgent grpServiceAGent = new GRPServiceAgent();
		try 
		{
			String loggedInUser = getLoggedInUserId();
			grpServiceAGent.persistTransaction(getTransactionsDataList(),loggedInUser);
			getFacesContext().getExternalContext().getSessionMap().put(WebConstants.GRP.IS_POSTED, true);
			String javaScriptText = "javascript:closeWindow('isPosted');";
			openPopup(javaScriptText);
			logger.logInfo(methodName +"|Finish");
		} 
        catch (PimsBusinessException e) 
        { 
            if(e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_CUSTOMER_NUMBER_ERROR))
            	errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosting.CustomerNotPresent"));
            else if(e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_FIN_ACC_ERROR))
            {
                this.setFinancialId(e.getExceptionMessage());
            	errorMessages.add(ResourceUtil.getInstance().getProperty("grpPosting.FinAccNotPresent"));
            
    			return;
            }
            else if ( e.getExceptionCode()!=null && e.getExceptionCode().equals(ExceptionCodes.GRP_AR_IVOICE_NOT_PRESENT))
            		this.errorMessages.add(java.text.MessageFormat.format(
						     ResourceUtil.getInstance().getProperty("settlement.msg.ArInvoiceNotPresent"),e.getExceptionMessage()));
            logger.LogException(methodName+"|Error Occured::",e);
        }

        catch (Exception e) 
        { 
			
			logger.LogException(methodName+"|Error Occured::",e);
        }
		
	}
	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}

	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
	public String getErrorMessages()
	{

    	return CommonUtil.getErrorMessages(errorMessages);
	}
	@SuppressWarnings("unchecked")
	public List<FinancialTransactionView> getTransactionsDataList() {
		if(viewMap.containsKey(WebConstants.GRP.TRANSACTIONS_LIST))
			transactionsDataList = (List<FinancialTransactionView>) viewMap.get(WebConstants.GRP.TRANSACTIONS_LIST);

		if(transactionsDataList == null)
			transactionsDataList = new ArrayList<FinancialTransactionView>();


		return transactionsDataList;
	}
	public void setTransactionsDataList(List<FinancialTransactionView> transactionsDataList) {
		this.transactionsDataList = transactionsDataList;
	}
	public HtmlDataTable getTransactionsGrid() {
		return transactionsGrid;
	}
	public void setTransactionsGrid(HtmlDataTable transactionsGrid) {
		this.transactionsGrid = transactionsGrid;
	}
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}


	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}


	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}


	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}


	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}


	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}


	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		if(pageIndex==null)
			pageIndex = 0;
		return pageIndex;
	}


	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	private void openPopup(String javaScriptText) throws Exception
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
	}

	public String getFinancialId() {
		return financialId;
	}

	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}

}