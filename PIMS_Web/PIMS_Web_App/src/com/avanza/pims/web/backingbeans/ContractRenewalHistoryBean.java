package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.ContractRenewalHistoryReportCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.ui.util.ResourceUtil;

public class ContractRenewalHistoryBean extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 927014354985605290L;
	private ContractRenewalHistoryReportCriteria reportCriteria;
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private List<SelectItem> stateList = new ArrayList<SelectItem>();
	private List<String> errorMessages ;

	 /** FOR TANENT DIALOG **/
	private String hdnTenantId;
	private String TENANT_INFO="TENANTINFO";
	
	@SuppressWarnings( "unchecked" )
	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	
	public String getPersonTenant() {
		
		return WebConstants.PERSON_TYPE_TENANT;
	}
	
	/** FOR TANENT DIALOG END **/
	
	
	public ContractRenewalHistoryBean(){
		if(CommonUtil.getIsEnglishLocale())
			reportCriteria = new ContractRenewalHistoryReportCriteria(ReportConstant.Report.CONTRACT_RENEWAL_HISTORY_EN, ReportConstant.Processor.CONTRACT_RENEWAL_HISTORY, CommonUtil.getLoggedInUser());
		else
			reportCriteria = new ContractRenewalHistoryReportCriteria(ReportConstant.Report.CONTRACT_RENEWAL_HISTORY_AR, ReportConstant.Processor.CONTRACT_RENEWAL_HISTORY, CommonUtil.getLoggedInUser());
	}
	

	 private static Logger logger = Logger.getLogger("ContractRenewalHistoryBean");
	 ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	 
	 
	 HttpServletRequest requestParam =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

	public ContractRenewalHistoryReportCriteria getReportCriteria() {
		return reportCriteria;
	}


	public void setReportCriteria(
			ContractRenewalHistoryReportCriteria reportCriteria) {
		this.reportCriteria = reportCriteria;
	}

	
	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}


	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	
	public List<SelectItem> getStateList() {
		return stateList;
	}


	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}


	public void btnPrint_Click() {	
		if(reportCriteria.getContractNumber().length()>0 || reportCriteria.getTenantName().length()>0)
		{
	    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			
		}
		else
		{
			errorMessages = new ArrayList<String>( 0 );
			errorMessages.add(ResourceUtil.getInstance().getProperty( "msg.rptContractRenewHistory.requiredContractNumber" ));
		}
}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	
	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public void init() {
		super.init();
		
			loadState();
			HttpServletRequest request =(HttpServletRequest)getFacesContext().getExternalContext().getRequest();
			if (!isPostBack())
			{
				FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
			}
		
			
	}
	
	

	
	public void loadState(/*ValueChangeEvent vce*/)  {
				
		
		try {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		
		String selectedCountryName = "UAE";
		List <RegionView> regionViewList = psa.getCountryStates(selectedCountryName,CommonUtil.getIsArabicLocale());
		
		
		for(int i=0;i<regionViewList.size();i++)
		  {
			  RegionView rV=(RegionView)regionViewList.get(i);
			  SelectItem item;
			  if (CommonUtil.getIsEnglishLocale())
			  {
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());
			  
		      this.getStateList().add(item);
		  }
		FacesContext.getCurrentInstance().getViewRoot().getAttributes().put("state", stateList);
		}catch (Exception e){
			System.out.println(e);
			logger.logDebug("In loadCountry() of Property List");
			
		}
	}


	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}


	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	
	/** FOR TANENT DIALOG **/
	public String getHdnTenantId() {
		return hdnTenantId;
	}

	public void setHdnTenantId(String hdnTenantId) {
		this.hdnTenantId = hdnTenantId;
	}
	
	public void retrieveTenant()
	{
		String methodName =  " retrieveTenant";
		logger.logInfo(methodName+ " Started");
		try
		{
		FillTenantInfo();

		if(viewRootMap.containsKey(TENANT_INFO))
	    {
		   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);

//		   String tenant_Type=getTenantType(tenantViewRow);


	       String tenantNames="";
	        if(tenantViewRow.getPersonFullName()!=null)
	        tenantNames=tenantViewRow.getPersonFullName();
	        if(tenantNames.trim().length()<=0)
	        	 tenantNames=tenantViewRow.getCompanyName();
	        ContractListCriteriaBackingBean contractListCriteriaBackingBean = (ContractListCriteriaBackingBean)getBean("pages$contractListCriteria");
	        contractListCriteriaBackingBean.setTenantName(tenantNames);
//	        contractListCriteriaBackingBean.setTenantType(tenant_Type);
	    }

		}
		catch(Exception ex)
		{
			logger.LogException(methodName + " crashed", ex);
		}

	}
	private void FillTenantInfo() throws PimsBusinessException {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		 if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(this.getHdnTenantId()));
	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
	    		if(tenantsList.size()>0)
	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));

	    	}

	}
	/** FOR TANENT DIALOG END **/
	


}
