package com.avanza.pims.web.backingbeans.construction.study;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.StudyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.exporter.ExporterMeta;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.StudyDetailView;

public class StudySearchBacking extends AbstractController 
{	
	private static final long serialVersionUID = 2893262199968091207L;
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	private String  CONTEXT="context";
	private String  PROJECT_DETAILS="ProjectDetails";
	private HtmlSelectOneMenu cmbStudyStatus = new HtmlSelectOneMenu();
	
	private String pageMode;
	private HtmlDataTable dataTable;
	private StudyDetailView studyDetailView;
	private List<StudyDetailView> studyDetailViewList;
	private StudyServiceAgent studyServiceAgent;
	private UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	
	public StudySearchBacking() 
	{
		logger = Logger.getLogger(StudySearchBacking.class);
		viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		studyDetailView = new StudyDetailView();
		studyDetailViewList = new ArrayList<StudyDetailView>(0);
		studyServiceAgent = new StudyServiceAgent();
		pageMode = WebConstants.Study.STUDY_SEARCH_PAGE_MODE_SEARCH;
	}
	
	@Override
	public void init() 
	{		
		String METHOD_NAME = "init()";
		try	
		{	
			if(!isPostBack())
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();	
				if (request.getParameter(CONTEXT)!=null )
		    	 {
					if(request.getParameter(CONTEXT).equals(PROJECT_DETAILS)) 
		    		 { 	   
						DomainDataView domainDataView =  utilityServiceAgent.getDomainDataByValue(WebConstants.Study.STUDY_STATUS, WebConstants.Study.STUDY_STATUS_COMPLETED);
						viewMap.put("DD_ID_STUDY_STATUS_COMPLETED", domainDataView.getDomainDataId().toString());
						viewMap.put(CONTEXT, PROJECT_DETAILS);
	      	    	}		    				          
		    	 }
			 }
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			super.init();
			updateValuesFromMaps();
			DoContextBasedSettings();			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	private void DoContextBasedSettings()
	{
		if(getIsContextProjectDetails())
		{
			cmbStudyStatus.setReadonly(true);
			cmbStudyStatus.setValue(viewMap.get("DD_ID_STUDY_STATUS_COMPLETED"));
		}
	}
	private boolean getIsContextProjectDetails()
	{
		boolean isContextProjectDetails= false;
		if(viewMap.containsKey(CONTEXT) && viewMap.get(CONTEXT)!=null && viewMap.get(CONTEXT).toString().trim().equalsIgnoreCase(PROJECT_DETAILS))
		{
			isContextProjectDetails= true;
		}
		return isContextProjectDetails;
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) sessionMap.get( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY );
		}
		
		// STUDY DETAIL VIEW
		if ( viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW ) != null )
			studyDetailView = (StudyDetailView) viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW );		
		
		// STUDY DETAIL VIEW LIST
		if ( viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW_LIST ) != null )
			studyDetailViewList = (List<StudyDetailView>) viewMap.get( WebConstants.Study.STUDY_DETAIL_VIEW_LIST );		
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_KEY, pageMode );
		}
		
		// STUDY DETAIL VIEW
		if ( studyDetailView != null )
			viewMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailView );

		// STUDY DETAIL VIEW LIST
		if( studyDetailViewList != null )
			viewMap.put( WebConstants.Study.STUDY_DETAIL_VIEW_LIST, studyDetailViewList );
	}

	@Override
	public void preprocess() {		
		super.preprocess();
	}
	
	@Override
	public void prerender() {		
		super.prerender();
	}
	
	public String onAdd() 
	{
		String path = null;		
		path = "studyManage";
		sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_ADD_STUDY );
		return path;
	}
	
	public String onView() 
	{
		String path = null;
		StudyDetailView studyDetailViewToView = (StudyDetailView) dataTable.getRowData();
		sessionMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailViewToView);
		sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_VIEW_STUDY );
		path = "studyManage";
		return path;
	}
	
	public String onEdit() 
	{
		String path = null;
		StudyDetailView studyDetailViewToEdit = (StudyDetailView) dataTable.getRowData();
		sessionMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailViewToEdit);
		sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_UPDATE_STUDY );
		path = "studyManage";
		return path;
	}
	
	public String onAddToInvestmentPlan() 
	{
		String path = null;
		StudyDetailView studyDetailViewToAdd = (StudyDetailView) dataTable.getRowData();
		sessionMap.put( WebConstants.Study.STUDY_DETAIL_VIEW, studyDetailViewToAdd);
		sessionMap.put( WebConstants.Study.STUDY_MANAGE_PAGE_MODE_KEY , WebConstants.Study.STUDY_MANAGE_PAGE_MODE_ADD_TO_INVESTMENT_PLAN );
		path = "studyManage";
		return path;
	}
	
	public String onSearch()
	{
		String METHOD_NAME = "onSearch()";		
		String path = null;
		
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			studyDetailView.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			studyDetailView.setRecordStatus( Constant.DEFAULT_RECORD_STATUS );
			studyDetailViewList = studyServiceAgent.getStudyList(studyDetailView);
			updateValuesToMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onCancel() 
	{
		String METHOD_NAME = "onCancel()";
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		Boolean isCancelled = false;
		String path = null;		
		StudyDetailView studyDetailViewToCancel = (StudyDetailView) dataTable.getRowData();
		studyDetailViewToCancel.setUpdatedOn( new Date() );
		studyDetailViewToCancel.setUpdatedBy( CommonUtil.getLoggedInUser() );
		studyDetailViewToCancel.setRecordStatus( 0L );
		
		try
		{
			isCancelled = studyServiceAgent.updateStudy(studyDetailViewToCancel);
			if ( isCancelled )
			{
				studyDetailViewList.remove( studyDetailViewToCancel );
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_CANCEL_SUCCESS) );
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
			}						
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_CANCEL_ERROR) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onDelete() 
	{
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		Boolean isDeleted = false;
		String path = null;		
		StudyDetailView studyDetailViewToDelete = (StudyDetailView) dataTable.getRowData();
		studyDetailViewToDelete.setUpdatedOn( new Date() );
		studyDetailViewToDelete.setUpdatedBy( CommonUtil.getLoggedInUser() );
		studyDetailViewToDelete.setIsDeleted( 1L );
		
		try
		{
			isDeleted = studyServiceAgent.updateStudy(studyDetailViewToDelete);
			if ( isDeleted  )
			{
				studyDetailViewList.remove( studyDetailViewToDelete );
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_DELETE_SUCCESS) );
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
			}						
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.STUDY_DELETE_ERROR) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onMultiSelect() 
	{
		List<StudyDetailView> selectedStudyDetailView = new ArrayList<StudyDetailView>(0);
		
		if ( studyDetailViewList != null )
		{
			for ( StudyDetailView currentStudyDetailView : studyDetailViewList )
			{
				if ( currentStudyDetailView.getIsSelected()!=null && currentStudyDetailView.getIsSelected() )
				{
					selectedStudyDetailView.add( currentStudyDetailView );
				}
			}
		}
		
		if ( selectedStudyDetailView.size() > 0 )
		{			
			sessionMap.put(WebConstants.Study.SELECTED_STUDY_DETAIL_VIEW_LIST, selectedStudyDetailView);
			executeJavascript("passSelectedStudiesAndClosePopup();");
		}
		else
		{
			errorMessages.add( CommonUtil.getBundleMessage(MessageConstants.Study.NO_STUDY_SELECTED_ERROR) );
		}
		
		return null;
	}
	
	public String onSingleSelect() 
	{
		StudyDetailView selectedStudyDetailView = (StudyDetailView) dataTable.getRowData();
		sessionMap.put(WebConstants.Study.SELECTED_STUDY_DETAIL, selectedStudyDetailView);
		executeJavascript("passSelectedStudiesAndClosePopup();");
		return null;
	}
	
	public Boolean getIsPopupMultiMode()
	{
		Boolean isPopupMultiMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_POPUP_MULTI ) )
			isPopupMultiMode = true;
		
		return isPopupMultiMode;
	}
	
	public Boolean getIsPopupSingleMode()
	{
		Boolean isPopupSingleMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Study.STUDY_SEARCH_PAGE_MODE_POPUP_SINGLE ) )
			isPopupSingleMode = true;
		
		return isPopupSingleMode;
	}
	
	private void executeJavascript(String javascript) {
		String METHOD_NAME = "openPopup()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public Integer getRecordSize() {
		return studyDetailViewList.size();
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public String getLocale() {
		return new CommonUtil().getLocale();
	}
	
	public String getDateFormat() {
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();		
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages(successMessages);		
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public StudyDetailView getStudyDetailView() {
		return studyDetailView;
	}

	public void setStudyDetailView(StudyDetailView studyDetailView) {
		this.studyDetailView = studyDetailView;
	}

	public List<StudyDetailView> getStudyDetailViewList() {
		return studyDetailViewList;
	}

	public void setStudyDetailViewList(List<StudyDetailView> studyDetailViewList) {
		this.studyDetailViewList = studyDetailViewList;
	}

	public StudyServiceAgent getStudyServiceAgent() {
		return studyServiceAgent;
	}

	public void setStudyServiceAgent(StudyServiceAgent studyServiceAgent) {
		this.studyServiceAgent = studyServiceAgent;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}
	
	public String onExcelExport()
	{
		try
		{	
			List<Integer> excludeRowIndex = new ArrayList<Integer>(0);			
			for ( int index = 0; index < studyDetailViewList.size(); index++ )
			{	 
				if ( ! studyDetailViewList.get(index).getIsSelected() )
				{
					excludeRowIndex.add(index);
				}
			}
			
			List<Integer> excludeColumnIndex = new ArrayList<Integer>(0);			
			excludeColumnIndex.add(7);
			
			ExporterMeta exporterMeta = new ExporterMeta();
			exporterMeta.setUiData(dataTable);
			exporterMeta.setExportFileName("exportedStudies");
			exporterMeta.setExcludeColumnIndex(excludeColumnIndex);
			exporterMeta.setExcludeRowIndex(excludeRowIndex);			
			sessionMap.put("exporterMeta", exporterMeta);
			executeJavascript("openExportPopup();");
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public DomainDataView getDomainDataForNewStudy ()
	{
		
		if(!viewMap.containsKey("STUDY_STATUS_NEW"))
		{
		 DomainDataView ddv = new DomainDataView();
		 ddv =  CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.Study.STUDY_STATUS),WebConstants.Study.STUDY_STATUS_NEW) ;
		 viewMap.put("STUDY_STATUS_NEW", ddv);
			return ddv;
		}else
		return (DomainDataView)viewMap.get("STUDY_STATUS_NEW");
	    
	
	}
	
	public boolean getEditingEnable()
	{
		boolean showEditIcons = true;
		StudyDetailView sDV = (StudyDetailView) dataTable.getRowData();
		DomainDataView ddV = getDomainDataForNewStudy();
		
		if(!ddV.getDomainDataId().equals(Long.parseLong( sDV.getStatusId() ) ))
		{
			showEditIcons = false;
		}
		
		
		return showEditIcons;
	}

	public HtmlSelectOneMenu getCmbStudyStatus() {
		return cmbStudyStatus;
	}

	public void setCmbStudyStatus(HtmlSelectOneMenu cmbStudyStatus) {
		this.cmbStudyStatus = cmbStudyStatus;
	}

}

	