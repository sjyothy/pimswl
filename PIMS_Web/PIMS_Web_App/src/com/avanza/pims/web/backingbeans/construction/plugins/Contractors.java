package com.avanza.pims.web.backingbeans.construction.plugins;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.security.db.UserDbImpl;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.PersonView;


public class Contractors extends AbstractController {
	
	private HtmlDataTable contractorGrid = new HtmlDataTable();
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	private List<ContractorView> contractorsList  = new ArrayList<ContractorView>();
	private String hdnTDPersonId;
	private String hdnTDPersonName;
	private String hdnTDPersonType;
	private String hdnTDCellNo;
	private String hdnTDIsCompany;
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	CommonUtil commUtil = new CommonUtil();
	public void init(){
		super.init();
			
		
		if(hdnTDPersonId!=null && !hdnTDPersonId.equals("")){
			viewMap.put(WebConstants.LOCAL_PERSON_ID,hdnTDPersonId);
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_ID,hdnTDPersonId);
		}

		if(hdnTDPersonName!=null && !hdnTDPersonName.equals(""))
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_NAME,hdnTDPersonName);


		if(hdnTDCellNo!=null && !hdnTDCellNo.equals(""))
			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_CELL,hdnTDCellNo);

		if(hdnTDIsCompany!=null && !hdnTDIsCompany.equals("")){
			DomainDataView ddv = new DomainDataView();
			if(Long.parseLong(hdnTDIsCompany)==1L){
				ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_COMPANY);}
			else{
				ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.TENANT_TYPE),
						WebConstants.TENANT_TYPE_INDIVIDUAL);}

			FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.TransferContract.TRANSFER_DETAILS_TENANT_TYPE,getIsEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr());

		}
		
		
	}
	/**
	 * @return the isEnglishLocale
	 */
	public Boolean getIsEnglishLocale() {
		return commUtil.getIsEnglishLocale();
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @return the isArabicLocale
	 */
	public Boolean getIsArabicLocale() {
		return commUtil.getIsArabicLocale();
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	public Contractors(){
		
		if(viewMap.containsKey(WebConstants.Tender.CONTRACTORS_LIST)&&viewMap.get(WebConstants.Tender.CONTRACTORS_LIST)!=null)
			contractorsList = (List<ContractorView>)viewMap.get(WebConstants.Tender.CONTRACTORS_LIST);

		if(viewMap.containsKey(WebConstants.MANY_CONTRACTORS_SELECTED)&&viewMap.get(WebConstants.MANY_CONTRACTORS_SELECTED)!=null)
		{
			List<ContractorView> tempList = (List<ContractorView>)viewMap.get(WebConstants.MANY_CONTRACTORS_SELECTED);
			Integer duplicateCounter = 0;
			if(!tempList.isEmpty())
			{
				for(ContractorView contractorView : tempList)
				{
					boolean isDuplicate = false;
					contractorView.setUpdatedBy(getLoggedInUser());
					contractorView.setCreatedBy(getLoggedInUser());
					contractorView.setTenderContractorIsDeleted(0L);
					
										
					if(!contractorsList.isEmpty())
					{
						for(ContractorView cView : contractorsList)
						{
							if(contractorView.getPersonId().toString().equals(cView.getPersonId().toString()))
								{
									isDuplicate = true;
									duplicateCounter ++;
								}
						}
						
					}
					if(!isDuplicate)
						contractorsList.add(contractorView);
							
				}
				if(duplicateCounter ==  tempList.size())
					viewMap.put("CONTRACTOR_ALREADY_EXIST", true);
				else if(duplicateCounter > 0)
					viewMap.put("MULTIPLE_CONTRACTOR_ALREADY_EXIST", true);
				
				if(duplicateCounter == 0)
				{
					viewMap.remove("CONTRACTOR_ALREADY_EXIST");
					viewMap.remove("MULTIPLE_CONTRACTOR_ALREADY_EXIST");
					
				}
					
				
					
			}
			viewMap.remove(WebConstants.MANY_CONTRACTORS_SELECTED);
			
			
		
		}
		if(viewMap.containsKey("CONTRACTOR_VIEW")&&viewMap.get("CONTRACTOR_VIEW")!=null)
		{
			ContractorView contractorView = (ContractorView)viewMap.get("CONTRACTOR_VIEW");
			boolean isExist = false;
			
			contractorView.setUpdatedBy(getLoggedInUser());
			contractorView.setCreatedBy(getLoggedInUser());
			contractorView.setTenderContractorIsDeleted(0L);
			
			
			List<ContractorView> tempContractorsList = new ArrayList<ContractorView>();
			for(ContractorView tempContractorView:contractorsList){
				if(tempContractorView.getPersonId().toString().equals(contractorView.getPersonId().toString())){
					isExist = true;
					
				}
			}
			if(!isExist)
						contractorsList.add(contractorView);
			else
				viewMap.put("CONTRACTOR_ALREADY_EXIST", true);
				
			
			viewMap.remove("CONTRACTOR_VIEW");
			
		}
		viewMap.put(WebConstants.Tender.CONTRACTORS_LIST, contractorsList);
		viewMap.put("recordSize",contractorsList.size());
	}
	public void preprocess(){
		super.preprocess();
	
	}
	public void prerender(){
		super.prerender();
		Contractors conta = new Contractors();
	}

	public HtmlDataTable getContractorGrid() {
		return contractorGrid;
	}
	public void setContractorGrid(HtmlDataTable contractorGrid) {
		this.contractorGrid = contractorGrid;
	}
	
	private List<DomainDataView> getDomainDataListForDomainType(String domainType)
	{
		List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		//Iterates for all the domain Types
		while( itr.hasNext() ) 
		{
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(domainType)==0 ) 
			{
				Set<DomainDataView> dd = dtv.getDomainDatas();
				//Iterates over all the domain datas
				for (DomainDataView ddv : dd) 
				{
					ddvList.add(ddv);	
				}
				break;
			}
		}

		return ddvList;
	}
	public String getHdnTDCellNo() {
		return hdnTDCellNo;
	}
	public void setHdnTDCellNo(String hdnTDCellNo) {
		this.hdnTDCellNo = hdnTDCellNo;
	}
	public String getHdnTDIsCompany() {
		return hdnTDIsCompany;
	}
	public void setHdnTDIsCompany(String hdnTDIsCompany) {
		this.hdnTDIsCompany = hdnTDIsCompany;
	}
	public String getHdnTDPersonId() {
		return hdnTDPersonId;
	}
	public void setHdnTDPersonId(String hdnTDPersonId) {
		this.hdnTDPersonId = hdnTDPersonId;
	}
	public String getHdnTDPersonName() {
		return hdnTDPersonName;
	}
	public void setHdnTDPersonName(String hdnTDPersonName) {
		this.hdnTDPersonName = hdnTDPersonName;
	}
	public String getHdnTDPersonType() {
		return hdnTDPersonType;
	}
	public void setHdnTDPersonType(String hdnTDPersonType) {
		this.hdnTDPersonType = hdnTDPersonType;
	}
	public List<ContractorView> getContractorsList() {
		if(viewMap.containsKey(WebConstants.Tender.CONTRACTORS_LIST))
			contractorsList = (List<ContractorView>) viewMap.get(WebConstants.Tender.CONTRACTORS_LIST);

		if(contractorsList == null)
			contractorsList = new ArrayList<ContractorView>();

		return contractorsList;
	}
	public void setContractorsList(List<ContractorView> contractorsList) {
		this.contractorsList = contractorsList;
	}
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		/*Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;*/
		return getContractorsList().size();
	}
	private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
		.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
			.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
	public void cmdContractorDelete_Click(){
		ContractorView contractorView = (ContractorView)contractorGrid.getRowData();
		
		if(viewMap.containsKey(WebConstants.Tender.CONTRACTORS_LIST)&&
				viewMap.get(WebConstants.Tender.CONTRACTORS_LIST)!=null){
		contractorsList = (List<ContractorView>)viewMap.get(WebConstants.Tender.CONTRACTORS_LIST);
		List<ContractorView> tempContractorsList = new ArrayList<ContractorView>();
			for(ContractorView tempContractorView:contractorsList){
                if(contractorView.getTenderContractorId()!=null // if tenderContractor already present  
                   && contractorView.getPersonId().compareTo(tempContractorView.getPersonId())==0L)
                {
                	tempContractorView.setTenderContractorIsDeleted(1L);
					tempContractorsList.add(tempContractorView);
				
                }else if(contractorView.getTenderContractorId()!=null // if tenderContractor already present  
                   && contractorView.getPersonId().compareTo(tempContractorView.getPersonId())!=0L)
				{
					tempContractorsList.add(tempContractorView);
				
				} else if(!(contractorView.getTenderContractorId()!=null) // if tenderContractor not present 
						&& tempContractorView.getPersonId()!=contractorView.getPersonId()){
					
					tempContractorsList.add(tempContractorView);
				}
			}
			viewMap.put(WebConstants.Tender.CONTRACTORS_LIST, tempContractorsList);
			viewMap.put("recordSize",tempContractorsList.size());
		}
		
		
		
	}
	
	public Boolean getCanDeleteTenderContractor()
	{
        if (viewMap.containsKey("CAN_DELETE_TENDER_CONTRACTOR"))
        	return false;
        else
        	return true;
        	
	}
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;

		}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
}
