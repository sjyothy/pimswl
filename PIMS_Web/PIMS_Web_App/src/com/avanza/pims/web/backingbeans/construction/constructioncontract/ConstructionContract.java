package com.avanza.pims.web.backingbeans.construction.constructioncontract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.bpel.pimsconstructioncontractbpel.proxy.PIMSConstructionContractBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionContractAgent;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.ProjectServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.business.services.ServiceContractAgent;

import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.WebConstants.TasksTypes_PrepareMaintenanceContract;
import com.avanza.pims.web.backingbeans.botcontract.BOTContract.TAB_ID;
import com.avanza.pims.web.backingbeans.construction.plugins.ProjectDetailsTabBacking;
import com.avanza.pims.web.backingbeans.construction.plugins.ScopeOfWork;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractDetailsTab;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.BankView;

import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DomainDataView;

import com.avanza.pims.ws.vo.ConstructionContractView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.ProjectView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TenderProposalView;


import com.avanza.pims.ws.vo.TenderView;


import com.avanza.ui.util.ResourceUtil;

public class ConstructionContract extends AbstractController {
	
	private transient Logger logger = Logger.getLogger(ConstructionContract.class);
	public interface Page_Mode {
		
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String EDIT="PAGE_MODE_EDIT";
		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
		public static final String LEGAL_REVIEW_REQUIRED="PAGE_MODE_LEGAL_REVIEW_REQUIRED";
		public static final String COMPLETE="PAGE_MODE_COMPLETE";
		public static final String POPUP="PAGE_MODE_POPUP";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String ACTIVE="PAGE_MODE_ACTIVE";
		
	}
    public interface TAB_ID {
		
		public static final String ContractDetails="tabContractDetails";
		public static final String Project="tabProject";
		public static final String PaymentSchedule="tabPaymentSchedule";
		public static final String Attachment="attachmentTab";
		
		
	}
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	protected  String pageMode="pageMode";
	protected  String tenderTypeConstruction; 
	HttpServletRequest request ;
	
	private List<DomainDataView> tenderProposalStatus=new ArrayList<DomainDataView>(0);
	private DomainDataView winnerDD=new DomainDataView();
	private List<TenderProposalView> tenderProposalList=new ArrayList<TenderProposalView>(0);
	
	protected SystemParameters parameters = SystemParameters.getInstance();
	Map viewRootMap ;
	Map sessionMap ;
	FacesContext context ;
	protected List<String> errorMessages;
	protected List<String> successMessages;
	CommonUtil commonUtil ;
	protected DomainDataView ddContractType;
	protected String CONTRACT_TYPE="CONTRACT_TYPE";
	protected String DD_CONTRACT_TYPE="DD_CONTRACT_TYPE";
	protected String DD_CONTRACT_STATUS_LIST ="DD_CONTRACT_STATUS_LIST";
	protected String PROCEDURE_TYPE;
	protected String EXTERNAL_ID;
	protected String TENDER_INFO ="tenderInfo";
	protected String contractCreatedOn;
	protected String contractCreatedBy;
	protected ContractorView contractor= new ContractorView();
	protected String CONTRACTOR_INFO  ="CONTRACTOR_INFO";
	protected String CONTRACT_VIEW ="CONTRACT_VIEW";
	protected String REQUEST_VIEW ="REQUEST_VIEW";
	protected String PROJECT_VIEW ="PROJECT_VIEW";
	private ProjectView projectView;
	protected ConstructionContractView constructionContractView = new ConstructionContractView();
	protected RequestView requestView = new RequestView();
	public String contractId;
	public String pageTitle;
	protected org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabProjectDetails=new org.richfaces.component.html.HtmlTab();
	protected HtmlCommandButton btnApprove = new HtmlCommandButton();
    protected HtmlCommandButton btnReject = new HtmlCommandButton();
    protected HtmlCommandButton btnReview = new HtmlCommandButton();
    protected HtmlCommandButton btnComplete = new HtmlCommandButton();
    protected HtmlCommandButton btnPrint = new HtmlCommandButton();
    protected HtmlCommandButton btnReviewReq = new HtmlCommandButton();
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnSend_For_Approval= new HtmlCommandButton();
    protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
    protected String txtRemarks;    
    protected String hdnTenderId;
    protected String hdnProjectId;
    protected String hdnContractorId;
	protected String requestId;
	private TenderProposalView winnerTenderProposal=new TenderProposalView();
	public String getHdnTenderId() {
		return hdnTenderId;
	}

	public void setHdnTenderId(String hdnTenderId) {
		this.hdnTenderId = hdnTenderId;
	}

	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public ConstructionContract(){
		request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getFacesContext().getExternalContext().getSessionMap();
		context = FacesContext.getCurrentInstance();
		commonUtil = new CommonUtil();
		PROCEDURE_TYPE ="procedureType";
		EXTERNAL_ID ="externalId";
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
	}
	
	@Override
    public void init() 
    {
    	
    	String methodName="init";
    	try{
	    	logger.logInfo(methodName+"|"+"Start");
	    	
		       if(!isPostBack())
		       {
		    	   tenderProposalStatus=CommonUtil.getDomainDataListForDomainType(WebConstants.TENDER_PROPOSAL_STATUS);
	 			   for(DomainDataView ddv:tenderProposalStatus)
	 			   {
	 				   if(ddv.getDataValue().compareTo(WebConstants.TENDER_PROPOSAL_STATUS_WINNER)==0)
	 				   {
	 					   winnerDD=ddv;
	 					   viewRootMap.put("WINNER_DDV", winnerDD);
	 					   break;
	 				   }
	 			   }
		    	   sessionMap.put("fromConstructionContract",WebConstants.FROM_CONSTRUCTION_CONTRACT);
		    	   viewRootMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_TYPE_PREPARE_CONSTRUCTION_CONTRACT);
		    	   viewRootMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_CONSTRUCTION_CONTRACT);
		    	   viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.PAYMENT_DUE_ON_MULTIPLE,true);
		    	   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ADD);
		    	   setContractTypeAsConstruction();
		    	   logger.logDebug(methodName+"|"+"Is not postback");
		    	   loadAttachmentsAndComments(null);
			    	if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				       getDataFromTaskList();
			    	
			    	if(viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK)!=null)
						getPageModeFromTaskList();
			    	
			    	else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
			    	 {
			    		 requestView=(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
					     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
					         this.contractId=requestView.getContractView().getContractId().toString();
					}
			    	else if (getRequestParam(WebConstants.CONSTRUCTION_CONTRACT_ID)!=null)
			    		this.contractId = getRequestParam(WebConstants.CONSTRUCTION_CONTRACT_ID).toString();
			    	if((this.contractId!=null && this.contractId.trim().length()>0) )
			    	{
			    		getContractById(this.contractId );
			    		onEnableDisablePercentage();
			    		
			    	}
		       }	
		       else
		       {
		       
			       if(sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
			       {
			    	   getPaymentScheduleFromSession();
			    	   
			       }
		       }
	    	logger.logInfo(methodName+"|"+"Finish");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|Error Occured", ex);
    	}
    }

	
	public void prerender()
	{
		String methodName ="prerender";
		super.prerender();
		try
		{
		
		setPageTitleBasedOnProcedureType();
		ConstructionContractDetailsTab ccdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		if(hdnTenderId!=null && hdnTenderId.trim().length()>0)
		{
     	   getTenderInfo();
     	   populateTenderInfoInTab();
     	   
     	   //ccdt.getTxtTotalAmount().setReadonly(true);
     	  if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
     	  {
     	    ccdt.getImgRemoveTender().setRendered(true);
     	    ccdt.getImgSearchContractor().setRendered(false);
     	  }
		}
		else
		{
			ccdt.getImgRemoveTender().setRendered(false);
			//ccdt.getTxtTotalAmount().setReadonly(false);
		}
		if(hdnProjectId!=null && hdnProjectId.trim().length()>0)
		{
		     viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.PROJECT_ID,hdnProjectId);
		}
		  contractor = getContractorInfo();
		  projectView = getProjectInfo();
	     populateContractorInfoInTab();
		 enableDisableControls();
		 if(viewRootMap.containsKey(ServiceContractPaymentSchTab.ViewRootKeys.ERROR_TOTAL_VALUE_NOT_PRESENT))
		 {
			    
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.ERROR_MSG_TOTAL_VALUE_NOT_PRESENT)); 
				viewRootMap.remove(ServiceContractPaymentSchTab.ViewRootKeys.ERROR_TOTAL_VALUE_NOT_PRESENT);
		 }
		 
		 
			 
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException(methodName+"|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	public void enableDisableControls()
	{
		String methodName = "enableDisableControls";
		logger.logInfo(methodName+"|Start");
		constructionContractView = getConstructionContractFromViewRoot();
		btnSend_For_Approval.setRendered(false);
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
			PageModeAdd();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{
			PageModeEdit();
			btnSend_For_Approval.setRendered(true);
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
			PageModeApprovalRequired();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.LEGAL_REVIEW_REQUIRED))
			PageModeLegalReviewRequired();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
			PageModeComplete();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ACTIVE))
		    PageModeActive();
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
			PageModeView();
		logger.logInfo(methodName+"|Finish");
		
	}
	
	private void getPageModeFromTaskList()
	{
		UserTask userTask = (UserTask)viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
    	{
	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareMaintenanceContract.APPROVE_MAINTENANCE_CONTRACT))
	    	{
	    		pageMode=Page_Mode.APPROVAL_REQUIRED;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
	    	else if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareMaintenanceContract.LEGAL_DEPT_REVIEW))
	    	{
	    		pageMode=Page_Mode.LEGAL_REVIEW_REQUIRED;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
	    	else if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(TasksTypes_PrepareMaintenanceContract.PRINT))
	    	{
	    		pageMode=Page_Mode.COMPLETE;
		    	viewRootMap.put(Page_Mode.PAGE_MODE, pageMode);
		    	
		
	    	}
    	}
	}
	
	private void setContractTypeAsConstruction() {
		ddContractType = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_TYPE) , WebConstants.CONSTRUCTION_CONTRACT);
         if(ddContractType !=null)
         {
		   viewRootMap.put(DD_CONTRACT_TYPE, ddContractType );
		   if(getIsEnglishLocale())
		     viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescEn());
		   else if(getIsArabicLocale())
			   viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_TYPE,ddContractType.getDataDescAr());
         }
	}
	
	private boolean hasError()
	{
	 	boolean hasErrors =false;
	 	Date contractStartDate =null;
        Date contractEndDate =null;
	 	try
	 	{
	 		
	 		
	 		if(hasContractDetailsError(hasErrors, contractStartDate,contractEndDate))
	 		{
	        	tabPanel.setSelectedTab(TAB_ID.ContractDetails );
	 			return true;
	 		}
	        if(hdnProjectId== null || hdnProjectId.trim().length()<=0)
	        {
	        	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ConstructionContract.MSG_REQ_PROJECT));
	        	tabPanel.setSelectedTab(TAB_ID.Project);
	        	return true;
	    	}  

	        
	        //Validating Payment Schedules Start
	        if(!getIsContractValueandRentAmountEqual())
	        {
	        	errorMessages.add(ResourceUtil.getInstance().getProperty("constructionContract.msg.AddAmount"));
	        	tabPanel.setSelectedTab(TAB_ID.PaymentSchedule);
	        	return true;
	        }
	        //Validating Payment Schedules Finish

	        
	        

	        if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		tabPanel.setSelectedTab(TAB_ID.Attachment);
	    		return true;
	    	}
	        
	        if(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE)!=null
	    			&&!(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString().equals("")))
	    		{
	    			try{
	    			Double.parseDouble(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString());
	    			
	    			}
	    			catch (NumberFormatException e) {
	    				errorMessages.add(ResourceUtil.getInstance().getProperty("contructionContract.msg.validPercentage"));
	    				return true;
	    			}
	    		}
	 	}
	 	catch(Exception e )
	 	{
	 		logger.LogException("hasError|Error Occured", e);
	 	}
   	


	 	
	 	return hasErrors;
		
	}

	private boolean hasContractDetailsError(boolean hasErrors,
			Date contractStartDate, Date contractEndDate) throws ParseException {
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_START_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE)!=null)
		      contractStartDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE);
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_END_DATE) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE)!=null)
		      contractEndDate =(Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE);
		
		
		if(hdnContractorId== null || hdnContractorId.trim().length()<=0)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACTOR));
			hasErrors = true;
		}
		
    	if(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).equals(null)
				||viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).equals("-1")
				||viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).equals(""))
        {
    		errorMessages.add(ResourceUtil.getInstance().getProperty("contructionContract.msg.paymentType"));
    		tabPanel.setSelectedTab(TAB_ID.ContractDetails );
    		hasErrors = true;
        }

        if(viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).equals(null)
				||viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).equals("-1")
				||viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).equals(""))
        {
    		errorMessages.add(ResourceUtil.getInstance().getProperty("serviceContract.message.requiredBank"));
    		hasErrors = true;
        }
		
		if(contractStartDate!=null && !contractStartDate.equals("") 
				 && contractEndDate!=null && !contractEndDate.equals("") )
		{
		    DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
		    String todayDate=dateFormat.format(new Date());

		    if (contractStartDate.compareTo(contractEndDate)>=0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_EXP));
				hasErrors=true;
			}
		    else if(viewRootMap.get(Page_Mode.PAGE_MODE).equals(Page_Mode.ADD) && contractStartDate.compareTo(dateFormat.parse(todayDate))==-1)
		    {
			  
			      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CON_START_SMALL_TODAY));
			      hasErrors=true;
		     }
		}
		else
		{
			
			if(contractStartDate ==null)
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_START_DATE));
			if(contractEndDate ==null)
		      errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_EXPIRY_DATE));
			      
			hasErrors=true;
		}
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT) && viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null
			&& viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().trim().length()>0)
		{
			try
			{
				Double chkAmountValidity =new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());
			}
			catch(NumberFormatException nfe)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_CONTRACT_VALUE));
			    hasErrors=true;
			}
		} 
		else 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_CONTRACT_AMOUNT));
			hasErrors=true;
		}
		if(viewRootMap.containsKey(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT) && viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT)!=null)
		{
		    try
			{
				Double chkAmountValidity =new Double(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString());
			}
			catch(NumberFormatException nfe)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_INVALID_GUARANTEE_PERCENTAGE ));
			    hasErrors=true;
			}
         } 
		else 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_REQ_GUARANTEE_PERCENTAGE));
			hasErrors=true;
		}
		return hasErrors;
	}
	private boolean getIsContractValueandRentAmountEqual()
    {
    	String methodName="getIsContractValueandRentAmountEqual";
    	logger.logInfo(methodName+"|"+"Start");
    	boolean isEqual=false;
    	
    	
    	  Double contractValue=new Double( viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());	
    		if(contractValue.compareTo(getSumofAllRentAmounts())>=0)
    			isEqual=true;
    	
    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
    	logger.logInfo(methodName+"|"+"Finish");
    	return isEqual;
    }
	private Double getSumofAllRentAmounts()
    {
    	String methodName="getSumofAllRentAmounts";
    	logger.logInfo(methodName+"|"+"Start");
    	Double rentAmount=new Double(0);
    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
    	
    	if(viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
        {
		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST);
			for(int i=0;i< paymentScheduleViewList.size();i++)
			{
				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
				//If payment Schedule is for the rent 
				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&& 
						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0)
				   rentAmount+=paymentScheduleView.getAmount();
			}
		    
        }
    	logger.logInfo(methodName+"|"+"Finish");
    	return Math.ceil(rentAmount);
    	
    }
	private void getContractDetailsFromContractDetailsTab()throws Exception
	{

		DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
		//Set Contract Number if present
		if(contractId !=null && contractId.trim().length()>0 )
		{
			constructionContractView =getConstructionContractFromViewRoot();
			constructionContractView.setContractId(new Long(contractId));

		}
		//set bank guarantee amount
		constructionContractView.setBankGuaranteeValue(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString()));
		
		// set Construction Payment Type Id
		constructionContractView.setConstructionPaymentTypeId(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).toString()));
		//if Guaratee expiry date is available
		if( viewRootMap.get("GUARANTEE_EXPIRY_DATE")!=null)
			constructionContractView.setGuaranteeExpiryDate((Date) viewRootMap.get("GUARANTEE_EXPIRY_DATE"));
		// set Project %
		if(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE)!=null &&
				!viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString().equals(""))
		constructionContractView.setProjectPercentage(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString()));
		
		else 
			constructionContractView.setProjectPercentage(null);

		
		
		
		//Set bank 
		BankView bankView = new BankView();
		bankView.setBankId(new Long(viewRootMap.get(WebConstants.ContractDetailsTab.BANK_ID).toString()));
		constructionContractView.setBankView(bankView);
		//Set Total Contract Value
		if(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null &&
				!viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().equals(""))
		constructionContractView.setRentAmount(new Double(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString()));
		//Set Contractor
		if(hdnContractorId!=null && hdnContractorId.trim().length()>0)
		{
		ContractorView pv =new ContractorView();
		pv.setPersonId(new Long(hdnContractorId));
	    constructionContractView.setContractorView(pv);
		
		}
		//Set Project
		if(hdnProjectId!=null && hdnProjectId.trim().length()>0)
		{
		ProjectView pv =new ProjectView();
		pv.setProjectId(new Long(hdnProjectId));
		constructionContractView.setProjectView(pv);
		
		}
	    //Set Tender
	    if(hdnTenderId!=null && hdnTenderId.trim().length()>0)
	    {
	    	TenderView tv = new TenderView();
	    	tv.setTenderId(new Long(hdnTenderId));
	    	constructionContractView.setTenderView(tv);
	    }
	    //Set Contract Start Date
	    constructionContractView.setStartDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_START_DATE));
	    //Set Contract End Date
	    constructionContractView.setEndDate((Date)viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_END_DATE));
	    //Set Contract Type
	    ddContractType = (DomainDataView)viewRootMap.get(DD_CONTRACT_TYPE);
	    constructionContractView.setContractTypeId(ddContractType.getDomainDataId());

	    if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
	    {
	    	
	    	DomainDataView ddContractStatus = commonUtil.getIdFromType(getContractStatusList(),
	    			                                                        WebConstants.CONTRACT_STATUS_DRAFT);
	      constructionContractView.setStatus(ddContractStatus.getDomainDataId());
	      
	    }
	    //Set CreatedOn CreatedBy UpdatedOn UpdateBy IsDeleted RecordStatus
	    constructionContractView.setIsDeleted(new Long(0));
	    constructionContractView.setRecordStatus(new Long(1));
	    constructionContractView.setUpdatedBy(getLoggedInUser());
        constructionContractView.setUpdatedOn(new Date());
        
        
        if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
        	constructionContractView.setCreatedOn(new Date());
        	constructionContractView.setCreatedBy(getLoggedInUser());
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
        }

        
	    
	    
	}
	private void setRequestView()
	{
		Set<RequestView> rvSet = new HashSet<RequestView>(0);
		if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUser());
            requestView.setIsDeleted(new Long(0));
            requestView.setRecordStatus(new Long(1));
            DomainDataView ddRequestStatus = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);
  	        requestView.setStatusId(ddRequestStatus.getDomainDataId());  
            requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_CONSTRUCTION_CONTRACT);
            requestView.setRequestDate(new Date());
        }
		else
		   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		if(hdnContractorId!=null && hdnContractorId.trim().length()>0)
		{
	      PersonView applicantView =new PersonView();
	      applicantView.setPersonId(new Long(hdnContractorId));
	      requestView.setApplicantView(applicantView);
		}
		
		requestView.setUpdatedBy(getLoggedInUser());
        requestView.setUpdatedOn(new Date());
        rvSet.add(requestView);
        constructionContractView.setRequestsView(rvSet);
	}
	private void putControlValuesinViews() throws Exception
	{
		String methodName = "putControlValuesinViews";
		logger.logInfo(methodName +"|Start");
         getContractDetailsFromContractDetailsTab();
         //Set Request View 
         setRequestView();
        
         if(viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)!=null)
         {
		  List<PaymentScheduleView> paymentScheduleDataList =(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST); 
		  Set<PaymentScheduleView> paymentScheduleViewViewSet = new HashSet<PaymentScheduleView>(0);
		  paymentScheduleViewViewSet.addAll(paymentScheduleDataList);
		  constructionContractView.setPaymentScheduleView(paymentScheduleViewViewSet);
         }
		logger.logInfo(methodName +"|Finish");
	}
	public void btnSendForApproval_Click()
	{
	
		String methodName = "btnSendForApproval_Click";
		logger.logInfo(methodName +"|Start");
		
	   try {
		        String endPoint= parameters.getParameter(WebConstants.CONSTRUCTION_CONTRACT_BPEL_ENDPOINT );
	    	    PIMSConstructionContractBPELPortClient port=new PIMSConstructionContractBPELPortClient();
		   	    port.setEndpoint(endPoint);
		   	    logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
		   		if(this.contractId!=null && this.contractId.trim().length()>0)
		   		{
			   	 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			   	 port.initiate(new Long(contractId),requestView.getRequestId(),getLoggedInUser(), null, null);
			   	 if(viewRootMap.containsKey(CONTRACT_VIEW) )
			   	    constructionContractView = getConstructionContractFromViewRoot();
			   	   
			   	 if(requestView != null && requestView.getRequestId()!=null)
				 {
					     saveComments(requestView.getRequestId());
						 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
						 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
						 saveAttachments(requestView.getRequestId().toString());
						 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
				  }
		   		  viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
		   	      saveSystemComments(MessageConstants.ContractEvents.APROVAL_REQUIRED);
	   	          successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
		   		}
			   
			   logger.logInfo(methodName+"|"+" Finish...");
	    	}
	    	catch(Exception ex)
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
	    	}
	}
	public void btnSave_Click()
	{
		String methodName = "btnSave_Click";
		logger.logInfo(methodName +"|Start");
		ConstructionContractAgent sca = new ConstructionContractAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasError())
			{
				 putControlValuesinViews();
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_CREATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_CREATED_SUCCESSFULLY); 
				     tabPanel.setSelectedTab(TAB_ID.ContractDetails);
				 }
				 else 
				 {
				     eventDesc  = MessageConstants.ContractEvents.CONTRACT_UPDATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY); 
				 }
				 
				 constructionContractView = sca.persistConstructionContract(constructionContractView);
				 
				 this.contractId = constructionContractView.getContractId().toString(); 
				 viewRootMap.put(CONTRACT_VIEW,constructionContractView);
				 refresh();
				 logger.logInfo(methodName +"|Contract Added with id ::"+constructionContractView );
				 logger.logInfo(methodName+"|"+" Saving Notes...Start");
				 
				 if(requestView != null && requestView.getRequestId()!=null)
				 {
				     saveComments(requestView.getRequestId());
				     logger.logInfo(methodName+"|"+" Saving Notes...Finish");
				     logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
					 saveAttachments(requestView.getRequestId().toString());
					 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
				
					 
				 }
				 
				 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				 saveSystemComments(eventDesc);
				// getContractById(this.contractId);
				 successMessages.add(successMsg);
				 
				 logger.logInfo(methodName +"|Finish");
			}
		}
		catch(Exception e)
		{
			logger.LogException(methodName+"|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}
	
	
	private void getPaymentScheduleFromSession() {
		String methodName ="getPaymentScheduleFromSession";
		logger.logInfo(methodName+"|Start");
		List<PaymentScheduleView> tempPaySch=new ArrayList<PaymentScheduleView>(0);   
		//List<PaymentScheduleView> oldPsList = (ArrayList<PaymentScheduleView>)viewRootMap.get( WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST)
		   
		viewRootMap.put( WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,	   
				   (ArrayList<PaymentScheduleView>)sessionMap.get(
				                                     WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
		   
		   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
		   
			logger.logInfo(methodName+"|Finish");
	}
	
	public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			ConstructionContractView cView =(ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
			RequestView rv =(RequestView)viewRootMap.get(REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = CommonUtil.getLoggedInUser();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		
		
		
	}
	
	private void setPageModeBasedOnContractStatus()
	{
		DomainDataView ddContractStatus = commonUtil.getDomainDataFromId( getContractStatusList(),
                constructionContractView.getStatus());
		if(ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_DRAFT))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_APPROVED))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.COMPLETE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_ACTIVE))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ACTIVE);
			return;
		}
		else if (ddContractStatus.getDataValue().equals(WebConstants.CONTRACT_STATUS_REVIEW_REQUIRED_LEGAL))
		{
			viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.LEGAL_REVIEW_REQUIRED);
			return;
		}

	}

	protected List<DomainDataView>  getContractStatusList() {
		List<DomainDataView> ddvContractStatusList = null;
		if(!viewRootMap.containsKey(DD_CONTRACT_STATUS_LIST))
		{
		ddvContractStatusList = commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
		viewRootMap.put(DD_CONTRACT_STATUS_LIST, ddvContractStatusList );
		}
		else
			ddvContractStatusList =(ArrayList<DomainDataView>)viewRootMap.get(DD_CONTRACT_STATUS_LIST);
		return ddvContractStatusList ;
	}
	private void getRequestForContract()throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		RequestView rv = new RequestView();
		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_PREPARE_CONSTRUCTION_CONTRACT);
		ContractView cv= new ContractView();
		cv.setContractId(new Long(this.contractId));
		
		rv.setContractView(cv);
		
		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
		if(requestViewList.size()>0)
		{
			requestView =requestViewList.get(0); 
			viewRootMap.put(REQUEST_VIEW,requestView );
			 
		}
    }
	
	@SuppressWarnings("unchecked")
	protected void getTenderInfo()throws PimsBusinessException
    {
    	String methodName="getTenderInfo";
    	logger.logInfo(methodName+"| Start");
    	TenderView tenderView;
    	
    	if(viewRootMap.containsKey(TENDER_INFO))
    	{
    		tenderView=(TenderView)viewRootMap.get(TENDER_INFO);
	    	if(hdnTenderId!=null && hdnTenderId.toString().trim().length()>0 && tenderView.getTenderId()!=null && 
	    			tenderView.getTenderId().compareTo(new Long(hdnTenderId))!=0)
	    	{
	    		tenderView = getTenderById();
	    		if(tenderView !=null)
	    			viewRootMap.put(TENDER_INFO,tenderView);
	    			
	    	}
    	}
    	else if(hdnTenderId!=null && hdnTenderId.trim().length()>0)
    	{
    		tenderView = getTenderById();
    		if(tenderView !=null)
    			viewRootMap.put(TENDER_INFO,tenderView);
    			
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }
    
	protected TenderView getTenderById() throws PimsBusinessException {
		ServiceContractAgent csa=new ServiceContractAgent();
		
		TenderView tenderView=  csa.getTenderViewByTenderId(new Long(hdnTenderId));
		return tenderView;
	}
	protected void setPageTitleBasedOnProcedureType()
	{
		
		if(viewRootMap.get(PROCEDURE_TYPE).toString().trim().equals(WebConstants.PROCEDURE_TYPE_PREPARE_CONSTRUCTION_CONTRACT))
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.ConstructionContract.TITLE_CONSTRUCTION_CONTRACT));
	}
	protected ContractorView getContractorInfo()throws PimsBusinessException
    {
    	ConstructionServiceAgent csa=new ConstructionServiceAgent();
	    if(viewRootMap.containsKey(CONTRACTOR_INFO))
    	{
    		contractor=(ContractorView)viewRootMap.get(CONTRACTOR_INFO);
	    	if(hdnContractorId!=null && hdnContractorId.trim().length()>0 && 
	    			contractor.getPersonId()!=null && contractor.getPersonId().compareTo(new Long(hdnContractorId))!=0)
	    	{
	    		contractor =  csa.getContractorById(new Long(hdnContractorId), null);
	    	    viewRootMap.put(CONTRACTOR_INFO, contractor );
	    	
	    			
	    	}
	        return contractor;
    	}
    	else if(hdnContractorId!=null && hdnContractorId.trim().length()>0 )
    	{
    		contractor =  csa.getContractorById(new Long(hdnContractorId), null);
    	    viewRootMap.put(CONTRACTOR_INFO, contractor );
    	    return contractor;
    			
    	}
    	else 
    		return null;
	    	    
    }
	protected ProjectView getProjectInfo()throws PimsBusinessException
    {
    	ProjectServiceAgent csa=new ProjectServiceAgent();
	    if(viewRootMap.containsKey(PROJECT_VIEW))
    	{
    		projectView=(ProjectView)viewRootMap.get(PROJECT_VIEW);
	    	if(hdnProjectId!=null && hdnProjectId.trim().length()>0 && 
	    			projectView.getProjectId()!=null && projectView.getProjectId().compareTo(new Long(hdnProjectId))!=0)
	    	{
	    		projectView =  csa.getProjectById(new Long(hdnProjectId));
	    		//viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,projectView.getProjectEstimatedCost());
	    	    viewRootMap.put(PROJECT_VIEW, projectView);
	    	    tabProjectDetails_Click();
	    	
	    			
	    	}
	        return projectView;
    	}
    	else if(hdnProjectId!=null && hdnProjectId.trim().length()>0 )
    	{
    		projectView =  csa.getProjectById(new Long(hdnProjectId));
    	    viewRootMap.put(PROJECT_VIEW, projectView);
    	    tabProjectDetails_Click();
    	    return projectView;
    			
    	}
    	else 
    		return null;
	    	    
    }
	
	protected Boolean generateNotification(String eventName)
    {
    	   String methodName ="generateNotification";
    	   Boolean success = false;
            try
            {
                  logger.logInfo(methodName+"|Start");
                  HashMap placeHolderMap = new HashMap();
                  constructionContractView= getConstructionContractFromViewRoot();
                  List<ContactInfo> contractorEmailList    = new ArrayList<ContactInfo>(0);
                  
                  NotificationFactory nsfactory = NotificationFactory.getInstance();
                  NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
                  Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
                  getNotificationPlaceHolder(event);
                  if(viewRootMap.containsKey(CONTRACTOR_INFO))
                  {
                        contractorEmailList = CommonUtil.getEmailContactInfos((PersonView)viewRootMap.get(CONTRACTOR_INFO));
                        if(contractorEmailList.size()>0)
                              notifier.fireEvent(event, contractorEmailList);    
                  }     
                  
                  success  = true;
                  logger.logInfo(methodName+"|Finish");
            }
    	   catch(Exception ex)
    	   {
    	          logger.LogException(methodName+"|Finish", ex);
    	   }
    	   return success;
    }
    
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		//placeHolderMap.setValue("CONTRACT", contractView);
		
		logger.logDebug(methodName+"|Finish");
		
	}
	
	protected void getContractById(String contractId)throws Exception,PimsBusinessException
	{
			String methodName="getContractById";
			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present ");
			try
			{
				ConstructionContractAgent sca =new ConstructionContractAgent ();
				constructionContractView= sca.getConstructionContractById(new Long(contractId));
				if(constructionContractView.getRentAmount()!=null)
					viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,constructionContractView.getRentAmount());
				viewRootMap.put(CONTRACT_VIEW ,constructionContractView);
				if(constructionContractView.getBankGuaranteeValue()!=null)
					viewRootMap.put("GUARANTEE_FIELD_READONLY", true);
				if(!isPostBack())
					setPageModeBasedOnContractStatus();
				this.contractId =contractId;
				refresh();
				guaranteePercentageChange();
				
			}
			catch(Exception ex)
			{
				logger.LogException(methodName+"|"+"Error occured:",ex);
				throw ex;
				
			}
	}

	private void refresh() throws PimsBusinessException 
	{
		getRequestForContract();
		populateContractDetailsTabFromContractView();
		hdnProjectId=constructionContractView.getProjectView().getProjectId().toString();
		getProjectInfo();
		getPaymentScheduleByContractId();
		onCalculatePayment();
		
	}
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String methodName="setTaskOutCome";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);

			String loggedInUser=getLoggedInUser();
			logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId()+"| TaskOutCome..."+taskOutCome+"|loggedInUser..."+loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
    public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		return CommonUtil.getSuccessMessages(successMessages);
	}
    public void saveSystemComments(String sysNoteType) throws Exception
    {
    	String methodName="saveSystemComments|";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	
    		String notesOwner = WebConstants.CONTRACT;
    		logger.logInfo(methodName + "contractId..."+contractId);
    		if(contractId!=null && contractId.trim().length()>0)
    		{
    		  Long contractsId = new Long(contractId);
    		  logger.logInfo(methodName + "notesOwner..."+notesOwner);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, contractsId );
	    	  logger.logInfo(methodName + "completed successfully!!!");
    		}
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	public String onCalculatePayment()
	{
		Double percentage = null;
		Double estimateAmount = null;
		Double contractAmount = null;
		
		
		if(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE)!=null
			&&!(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString().equals(""))
				&& viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST)!=null
				&& !(viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST).toString().equals("")))
		{
			try{
			percentage = Double.parseDouble(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString());
			estimateAmount = Double.parseDouble(viewRootMap.get(WebConstants.ProjectDetailsTab.PROJECT_ESTIMATED_COST).toString());
			}
			catch (NumberFormatException e) {
				
				errorMessages.add(ResourceUtil.getInstance().getProperty("contructionContract.msg.validPercentage"));
				return null;
			}
			
			contractAmount = (percentage*estimateAmount)/100;
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,contractAmount);
			guaranteePercentageChange();
			
		}
		
		
		return null;
	}
	
	public String onConstructionPaymentType()
	{
		
		DomainDataView ddvPaymentTypePercent = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONSTRUCTION_PAYMENT_TYPE), WebConstants.CONSTRUCTION_PAYMENT_TYPE_PERCENTAGE);
		Long PaymentTypeId = null;
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		
		if(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE)!=null
				&&!viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).equals("-1"))
		{
			PaymentTypeId = new Long(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).toString());
			
			if(ddvPaymentTypePercent.getDomainDataId().compareTo(PaymentTypeId)==0)
				    scdt.enableDisableProjectPersentControl("ENABLE");
			else
				    scdt.enableDisableProjectPersentControl("DISABLE");
			
				
			if(viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE)!=null
					&& !viewRootMap.get(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE).toString().equals(""))
			{
				viewRootMap.put(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE,"");
			}
			
			if(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null
					&& !viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().equals(""))
			{
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,"");
			}
			
		
		}
		
				
		return null;
	}
	
	
	public String onEnableDisablePercentage()
	{
		
		DomainDataView ddvPaymentTypePercent = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.CONSTRUCTION_PAYMENT_TYPE), WebConstants.CONSTRUCTION_PAYMENT_TYPE_PERCENTAGE);
		Long PaymentTypeId = null;
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		
		if(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE)!=null
				&&!viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).equals("-1"))
		{
			PaymentTypeId = new Long(viewRootMap.get(WebConstants.ContractDetailsTab.PAYMENT_TYPE).toString());
			
			if(ddvPaymentTypePercent.getDomainDataId().compareTo(PaymentTypeId)==0)
				    scdt.enableDisableProjectPersentControl("ENABLE");
			else
				    scdt.enableDisableProjectPersentControl("DISABLE");
		}
		
				
		return null;
	}
	public void loadAttachmentsAndComments(Long requestId){
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewRootMap.get(PROCEDURE_TYPE).toString());
    		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
        	String externalId = viewRootMap.get(EXTERNAL_ID).toString();
        	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
    		viewMap.put("noteowner", WebConstants.REQUEST);
    		if(requestId!= null){
    	    	String entityId = requestId.toString();
    			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
    			viewMap.put("entityId", entityId);
    		}
    	}	
    public void tabAttachmentsComments_Click()
    {
    	String methodName ="saveCommensAttachment";
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		 if(requestView != null && requestView.getRequestId()!=null)
		 {
			 loadAttachmentsAndComments(requestView.getRequestId());
		 }
    	
    }
	
	protected void getDataFromTaskList()throws Exception
    {
    	    	   String methodName="getDataFromTaskList";
    	    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    	    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
    		      viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
    		      if(userTask.getTaskAttributes().get("CONTRACT_ID")!=null)
    			       this.contractId= userTask.getTaskAttributes().get("CONTRACT_ID").toString();
    		       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    }
	
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
		    return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
	protected  String getLoggedInUser() 
	{
	        return CommonUtil.getLoggedInUser();
	}
	public String getContractId() {
		return contractId;
	}

	protected TenderView getTenderView()
	{
		TenderView tv =null;
		if(viewRootMap.containsKey(TENDER_INFO))
			tv = (TenderView )viewRootMap.get(TENDER_INFO);
		return tv;
	}
	protected void populateContractDetailsTabFromContractView()throws PimsBusinessException
	{
		String methodName ="populateContractDetailsTabFromContractView";
		logger.logInfo(methodName+"|"+"Start..");
		TenderView tv =getTenderView();
		DateFormat dateFormat =new SimpleDateFormat(getDateFormat());
	    //if contract has been saved
		if(contractId != null && contractId.trim().length()>0)
		{		
			   constructionContractView = getConstructionContractFromViewRoot();
			   if(constructionContractView.getTenderView()!=null)
			   {
                viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,constructionContractView.getTenderView().getTenderNumber());
                if(constructionContractView.getTenderView().getTenderId()!=null)
                hdnTenderId = constructionContractView.getTenderView().getTenderId().toString();
			   }
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,constructionContractView.getRentAmount());
				viewRootMap.put(CONTRACTOR_INFO ,constructionContractView.getContractorView());
				contractor= constructionContractView.getContractorView();
				hdnContractorId  = constructionContractView.getContractorView().getPersonId().toString();
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_START_DATE,constructionContractView.getStartDate());
				viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_END_DATE,constructionContractView.getEndDate());
				contractCreatedOn = dateFormat.format(constructionContractView.getCreatedOn());
				contractCreatedBy = constructionContractView.getCreatedBy();
				if(constructionContractView.getBankGuaranteeValue() !=null)
				viewRootMap.put(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT,constructionContractView.getBankGuaranteeValue() );
				viewRootMap.put(WebConstants.ContractDetailsTab.BANK_ID,constructionContractView.getBankView().getBankId() );
		 	    viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_NUM,constructionContractView.getContractNumber());
		 	    
		 	    if(constructionContractView.getProjectPercentage()!=null)
		 	    viewRootMap.put(WebConstants.ContractDetailsTab.PROJECT_PERCENTAGE, constructionContractView.getProjectPercentage());
		 	    
		 	    if(constructionContractView.getGuaranteeExpiryDate()!=null)
		 	    	viewRootMap.put("GUARANTEE_EXPIRY_DATE", constructionContractView.getGuaranteeExpiryDate());
		 	    
		 	    viewRootMap.put(WebConstants.ContractDetailsTab.PAYMENT_TYPE, constructionContractView.getConstructionPaymentTypeId());
		 	   
		 	    contractor = getContractorInfo();
			    populateContractorInfoInTab(); 
			    loadAttachmentsAndComments(constructionContractView.getContractId());
		 	   
			}
		     
		
		logger.logInfo(methodName+"|"+"Finish..");
	}
	@SuppressWarnings("unchecked")
	protected void populateTenderInfoInTab()
	{
		TenderView tv =getTenderView();
		//if tender is selected and contract has not been saved
		if((contractId == null || contractId.trim().length()<=0) && hdnTenderId != null && hdnTenderId.trim().length()>0 )
		{		
		  try
		  {
			viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_ID,tv.getTenderId());
			viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,tv.getTenderNumber());
			if(tv.getRemarks()!=null && tv.getRemarks().trim().length()>0)
			   viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_DESC,tv.getRemarks());
			tenderProposalList=new ConstructionServiceAgent().getTenderProposalList(tv.getTenderId());
			
			if(viewRootMap.containsKey("WINNER_DDV"))
				winnerDD=(DomainDataView) viewRootMap.get("WINNER_DDV");
			for(TenderProposalView tpv:tenderProposalList)
			{
				if(tpv.getStatusId().compareTo(winnerDD.getDomainDataId().toString())==0)
				{
					winnerTenderProposal=tpv;
					viewRootMap.put("WINNER_TENDER_PROPOSAL", winnerTenderProposal);
					winnerTenderProposal=(TenderProposalView) viewRootMap.get("WINNER_TENDER_PROPOSAL");
					if(winnerTenderProposal!=null && winnerTenderProposal.getTenderProposalId()!=null)
					{
						if(winnerTenderProposal.getBankGuaranteePercentage()!=null && winnerTenderProposal.getBankGuaranteePercentage()>0)
							{
								viewRootMap.put(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT, winnerTenderProposal.getBankGuaranteePercentage().toString());
								viewRootMap.put("GUARANTEE_FIELD_READONLY", true);
							}
						if(winnerTenderProposal.getBankView()!=null && winnerTenderProposal.getBankView().getBankId()!=null)
							viewRootMap.put(WebConstants.ContractDetailsTab.BANK_ID,winnerTenderProposal.getBankView().getBankId());
						if(winnerTenderProposal.getGuaranteeExpiryDate()!=null)
							viewRootMap.put("GUARANTEE_EXPIRY_DATE",winnerTenderProposal.getGuaranteeExpiryDate());
						
					}
					break;
				}
			}
			//if(tv.getTenderWonProposalAmount()!=null && tv.getTenderWonProposalAmount().toString().trim().length()>0)
			//viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,tv.getTenderWonProposalAmount());
			hdnContractorId  = tv.getWinnerContractor().getPersonId().toString();
			hdnProjectId=tv.getProjectView().getProjectId().toString();
			
			
		  }
		  catch(Exception e)
		  {
			  logger.LogException("populateTenderInfoInTab() Crashed ", e);
		  }
		}
		
	}
	@SuppressWarnings("unchecked")
	public void imgRemoveTender_Click()
	{
		hdnTenderId="";
		hdnContractorId="";
		hdnProjectId="";
	    viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_ID,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_NUM,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.TENDER_DESC,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_ID,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,"");
		viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM,"");
		viewRootMap.remove(CONTRACTOR_INFO);
		viewRootMap.remove(PROJECT_VIEW);
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD) || viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{
		ConstructionContractDetailsTab ccdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		ccdt.getImgSearchContractor().setRendered(true);
		}
		ProjectDetailsTabBacking pdtb= (ProjectDetailsTabBacking)getBean("pages$projectDetailsTab");
	    pdtb.clearProjectDetails();
		
	}
	protected void populateContractorInfoInTab()
	{
		
		contractor = getContractorFromViewRoot();
		if(contractor !=null)
		{
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_ID,contractor.getPersonId());
			if(getIsEnglishLocale() )
			{
			    if(contractor.getContractorNameEn()!=null)		
			      viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,contractor.getContractorNameEn());
			}
			else if(getIsArabicLocale())
			{
				if(contractor.getContractorNameAr()!=null)
				  viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NAME,contractor.getContractorNameAr());
			}
			if(contractor.getLicenseNumber()!=null)
			viewRootMap.put(WebConstants.ContractDetailsTab.CONTRACTOR_NUM,contractor.getLicenseNumber());
		}
		
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public void tabProjectDetails_Click()
	{
		String methodName ="tabProjectDetails_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
    	    projectView= getProjectInfo();
			if(viewRootMap.get(PROJECT_VIEW)!=null)
				projectView =   (ProjectView)viewRootMap.get(PROJECT_VIEW);
			if(projectView!=null && projectView.getProjectId()!=null)
			{   
				
				ProjectDetailsTabBacking pdtb= (ProjectDetailsTabBacking)getBean("pages$projectDetailsTab");
			    pdtb.populateProjectDetails(projectView,hdnTenderId);
			}
			
				
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void tabPaymentTerms_Click()
	{
		String methodName ="tabPaymentTerms_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			if(this.contractId!=null && this.contractId.trim().length()>0 &&
					!viewRootMap.containsKey(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST))
			{
				getPaymentScheduleByContractId();
			}
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	private void getPaymentScheduleByContractId() throws PimsBusinessException {
		PropertyServiceAgent psa= new PropertyServiceAgent();
		List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(new Long(contractId),null);
		viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,paymentScheduleViewList);
	}
	
	public ContractorView getContractorFromViewRoot()
	{
		if(viewRootMap.get(CONTRACTOR_INFO)!=null)
			return (ContractorView)viewRootMap.get(CONTRACTOR_INFO);
		else
			return null;
		
	}
	public ConstructionContractView getConstructionContractFromViewRoot()
	{
		if(viewRootMap.get(CONTRACT_VIEW)!=null)
			return (ConstructionContractView)viewRootMap.get(CONTRACT_VIEW);
		else
			return null;
		
	}
	public RequestView getRequestViewFromViewRoot()
	{
		if(viewRootMap.get(REQUEST_VIEW)!=null)
			return (RequestView)viewRootMap.get(REQUEST_VIEW);
		else
			return null;
		
	}
	public void tabAuditTrail_Click()
	{
		String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(contractId!=null && contractId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
		
	}
	
	public void btnReviewReq_Click()
	{
		String methodName ="btnReviewReq_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		
		try	
    	{
			 
			if(txtRemarks!=null && txtRemarks.trim().length()>0)
			{
				if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					  getIncompleteRequestTasks();
				setTaskOutCome(TaskOutcome.LEGAL_DEPT_REVIEW);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				saveCommensAttachment(MessageConstants.ContractEvents.REVIEW_REQUIRED_LEGAL);
				txtRemarks ="";
				getContractById(this.contractId);
				viewRootMap.put(Page_Mode.PAGE_MODE , Page_Mode.LEGAL_REVIEW_REQUIRED);
				successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_SENT_FOR_REVIEW));
			}
			else
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnApprove_Click()
	{
		String methodName ="btnApprove_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.APPROVE);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_APPROVED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.COMPLETE);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_APPROVED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReject_Click()
	{
		String methodName ="btnReject_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.REJECT);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_REJECTED);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REJECED_SUCCESSFULLY));
			    }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnReview_Click()
	{
		String methodName ="btnReview_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			
			   if(txtRemarks!=null && txtRemarks.trim().length()>0)
			   {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
					 getIncompleteRequestTasks();
				   setTaskOutCome(TaskOutcome.OK);
				   viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				   saveCommensAttachment(MessageConstants.ContractEvents.REVIEWED_LEGAL);
				   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.APPROVAL_REQUIRED);
				   txtRemarks ="";
				   getContractById(this.contractId);
				   successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_REVIEWED_SUCCESSFULLY));
			   }
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	  
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	
	public void btnComplete_Click()
	{
		String methodName ="btnComplete_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
						  getIncompleteRequestTasks();
					setTaskOutCome(TaskOutcome.COMPLETE);
					viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_ACTIVATED);
			    	viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ACTIVE);
			    	txtRemarks ="";
			    	getContractById(this.contractId);
			    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_COMPLETED_SUCCESSFULLY));
			    }
			    	
				else
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
			
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	public void btnPrint_Click()
	{
		String methodName ="btnPrint_Click";
		logger.logInfo(methodName+"|"+"Start..");
		
		try	
    	{
			
			    if(txtRemarks!=null && txtRemarks.trim().length()>0)
			    {
			    	saveCommensAttachment(MessageConstants.ContractEvents.CONTRACT_PRINTED);
			        txtRemarks ="";
			        successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ServiceContract.MSG_CONTRACT_PRINTED_SUCCESSFULLY));
			    }
				else
				  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_REQUIRED_REMARKS));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}

	private void saveCommensAttachment(String eventDesc) throws Exception {
		String methodName ="saveCommensAttachment";
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		 if(requestView != null && requestView.getRequestId()!=null)
		 {
		     saveComments(requestView.getRequestId());
			 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
			 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
			 saveAttachments(requestView.getRequestId().toString());
			 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
		
		 }
		saveSystemComments(eventDesc);
	}

	
	public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
		return tabPaymentTerms;
	}

	public void setTabPaymentTerms(
			org.richfaces.component.html.HtmlTab tabPaymentTerms) {
		this.tabPaymentTerms = tabPaymentTerms;
	}
	public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
		return tabAuditTrail;
	}
	
	
	public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
		this.tabAuditTrail = tabAuditTrail;
	}
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
			isViewModePopup=false;
		
		return isViewModePopup;
	}

	public String getPageTitle() {
		if(viewRootMap.containsKey("pageTitle") && viewRootMap.get("pageTitle")!=null)
			pageTitle = viewRootMap.get("pageTitle").toString();
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
		if(this.pageTitle !=null)
			viewRootMap.put("pageTitle",this.pageTitle );
			
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public DomainDataView getDdContractType() {
		return ddContractType;
	}

	public void setDdContractType(DomainDataView ddContractType) {
		this.ddContractType = ddContractType;
	}

	public String getPageMode() {
		if(viewRootMap.get(Page_Mode.PAGE_MODE)!=null)
			pageMode =viewRootMap.get(Page_Mode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getContractCreatedOn() {
		return contractCreatedOn;
	}

	public void setContractCreatedOn(String contractCreatedOn) {
		this.contractCreatedOn = contractCreatedOn;
	}

	public String getContractCreatedBy() {
		return contractCreatedBy;
	}

	public void setContractCreatedBy(String contractCreatedBy) {
		this.contractCreatedBy = contractCreatedBy;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnReview() {
		return btnReview;
	}

	public void setBtnReview(HtmlCommandButton btnReview) {
		this.btnReview = btnReview;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}

	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}

	public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlCommandButton getBtnReviewReq() {
		return btnReviewReq;
	}

	public void setBtnReviewReq(HtmlCommandButton btnReviewReq) {
		this.btnReviewReq = btnReviewReq;
	}
	
	protected void PageModeAdd()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
		//ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		//scdt.enableDisableProjectPersentControl("DISABLE");
		btnSend_For_Approval.setRendered(false);
		tbl_Action.setRendered(false);
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);

	}

	protected void PageModeEdit()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.ADD);
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.EDIT);
		tbl_Action.setRendered(false);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);

	}
	protected void PageModeApprovalRequired()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.APPROVAL_REQUIRED);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		btnReviewReq.setRendered(true);
		btnReview.setRendered(false);
		tbl_Action.setRendered(true);
		btnSave.setRendered(false);
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);
		
	}
	protected void PageModeLegalReviewRequired()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.LEGAL_REVIEW_REQUIRED);
		btnComplete.setRendered(false);
		btnPrint.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(true);
		tbl_Action.setRendered(true);
		btnSave.setRendered(false);
		viewRootMap.put("canAddAttachment", true);
		viewRootMap.put("canAddNote", true);
	}
	protected void PageModeComplete()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.COMPLETE);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(true);
		btnSave.setRendered(false);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
	}
	protected void PageModeActive()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.COMPLETE);
		btnComplete.setRendered(false);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(false);
		
	}
	protected void PageModeView()
	{
		ServiceContractPaymentSchTab scpt =(ServiceContractPaymentSchTab)getBean("pages$serviceContractPaySchTab");
		scpt.enableDisableControls(ServiceContractPaymentSchTab.ViewRootKeys.VIEW);	
		ConstructionContractDetailsTab scdt= (ConstructionContractDetailsTab )getBean("pages$constructionContractDetailsTab");
		scdt.enableDisableControls(ConstructionContractDetailsTab.Page_Mode.VIEW);
		btnPrint.setRendered(true);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnReviewReq.setRendered(false);
		btnReview.setRendered(false);
		btnSave.setRendered(false);
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnSend_For_Approval() {
		return btnSend_For_Approval;
	}

	public void setBtnSend_For_Approval(HtmlCommandButton btnSend_For_Approval) {
		this.btnSend_For_Approval = btnSend_For_Approval;
	}



	public org.richfaces.component.html.HtmlTab getTabProjectDetails() {
		return tabProjectDetails;
	}

	public void setTabProjectDetails(
			org.richfaces.component.html.HtmlTab tabProjectDetails) {
		this.tabProjectDetails = tabProjectDetails;
	}

	public String getHdnProjectId() {
		return hdnProjectId;
	}

	public void setHdnProjectId(String hdnProjectId) {
		this.hdnProjectId = hdnProjectId;
	}
	
	public String getTenderTypeConstruction() {
		tenderTypeConstruction = WebConstants.Tender.TENDER_TYPE_CONSTRUCTION;
		return tenderTypeConstruction ;
	}

	public void setTenderTypeConstruction(String tenderTypeConstruction) {
		this.tenderTypeConstruction = tenderTypeConstruction;
	}

	public String getContractorTypeId() {
		
			return WebConstants.ContractorTypes.CONTRACTOR.toString();	
		
		
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public List<DomainDataView> getTenderProposalStatus() {
		return tenderProposalStatus;
	}

	public void setTenderProposalStatus(List<DomainDataView> tenderProposalStatus) {
		this.tenderProposalStatus = tenderProposalStatus;
	}

	public DomainDataView getWinnerDD() {
		return winnerDD;
	}

	public void setWinnerDD(DomainDataView winnerDD) {
		this.winnerDD = winnerDD;
	}

	public List<TenderProposalView> getTenderProposalList() {
		return tenderProposalList;
	}

	public void setTenderProposalList(List<TenderProposalView> tenderProposalList) {
		this.tenderProposalList = tenderProposalList;
	}

	public TenderProposalView getWinnerTenderProposal() {
		return winnerTenderProposal;
	}

	public void setWinnerTenderProposal(TenderProposalView winnerTenderProposal) {
		this.winnerTenderProposal = winnerTenderProposal;
	}
	
	public void guaranteePercentageChange()
	{
		if(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT)!=null && viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString().compareTo("")!=0)
		{
			double amount;
			double totalPerAmount=0;
			double percentage=Double.parseDouble(viewRootMap.get(WebConstants.ContractDetailsTab.GUARANTEE_PERCENT).toString());
			if(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT)!=null)
				{
					if(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString().compareTo("")!=0)
					{
						amount=Double.parseDouble(viewRootMap.get(WebConstants.ContractDetailsTab.CONTRACT_AMOUNT).toString());
						totalPerAmount=amount*percentage/100;
						viewRootMap.put(WebConstants.ContractDetailsTab.BANK_GUARANTEE_AMOUNT, totalPerAmount);
					}
				}
			
			
	
		}
	}

	
}
