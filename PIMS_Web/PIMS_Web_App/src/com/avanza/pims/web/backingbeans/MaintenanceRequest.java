//package com.avanza.pims.web.backingbeans;
//
//import java.text.DateFormat;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.ResourceBundle;
//import java.util.Set;
//import java.util.TimeZone;
//
//import javax.faces.application.ViewHandler;
//import javax.faces.component.UIViewRoot;
//import javax.faces.component.html.HtmlCommandButton;
//import javax.faces.component.html.HtmlGraphicImage;
//import javax.faces.component.html.HtmlInputText;
//import javax.faces.component.html.HtmlInputTextarea;
//import javax.faces.component.html.HtmlOutputLabel;
//import javax.faces.component.html.HtmlSelectOneMenu;
//import javax.faces.context.FacesContext;
//import javax.faces.model.SelectItem;
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//
//import org.apache.myfaces.component.html.ext.HtmlDataTable;
//import org.apache.myfaces.custom.tabbedpane.HtmlPanelTab;
//import org.apache.myfaces.custom.tabbedpane.HtmlPanelTabbedPane;
//import org.apache.myfaces.renderkit.html.util.AddResource;
//import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
//
//import com.avanza.core.constants.CoreConstants;
//import com.avanza.core.data.ApplicationContext;
//import com.avanza.core.security.db.UserDbImpl;
//import com.avanza.core.util.Logger;
//import com.avanza.core.web.WebContext;
//import com.avanza.core.web.config.LocaleInfo;
//import com.avanza.notification.api.ContactInfo;
//import com.avanza.notification.api.NotificationFactory;
//import com.avanza.notification.api.NotificationProvider;
//import com.avanza.notification.api.NotifierType;
//import com.avanza.notificationservice.event.Event;
//import com.avanza.notificationservice.event.EventCatalog;
//import com.avanza.pims.bpel.pimsamendleasecontract.proxy.PIMSAmendLeaseContractBPELPortClient;
//import com.avanza.pims.bpel.pimscontractinitiatebpel.proxy.ContractInitiateBPELPortClient;
//import com.avanza.pims.business.exceptions.PimsBusinessException;
//import com.avanza.pims.business.services.ConstructionServiceAgent;
//import com.avanza.pims.business.services.PropertyServiceAgent;
//import com.avanza.pims.business.services.RequestServiceAgent;
//import com.avanza.pims.constant.Constant;
//import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
//import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
//import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
//import com.avanza.pims.soa.bpm.worklist.UserTask;
//import com.avanza.pims.web.MessageConstants;
//import com.avanza.pims.web.WebConstants;
//import com.avanza.pims.web.controller.AbstractController;
//import com.avanza.pims.web.util.CommonUtil;
//import com.avanza.pims.web.util.SystemParameters;
//import com.avanza.pims.ws.construction.ConstructionService;
//import com.avanza.pims.ws.request.RequestService;
//import com.avanza.pims.ws.vo.AuctionUnitView;
//import com.avanza.pims.ws.vo.CommercialActivityView;
//import com.avanza.pims.ws.vo.ContactInfoView;
//import com.avanza.pims.ws.vo.ContractActivityView;
//import com.avanza.pims.ws.vo.ContractPartnerView;
//import com.avanza.pims.ws.vo.ContractPersonView;
//import com.avanza.pims.ws.vo.ContractUnitView;
//import com.avanza.pims.ws.vo.ContractView;
//import com.avanza.pims.ws.vo.DomainDataView;
//import com.avanza.pims.ws.vo.DomainTypeView;
//import com.avanza.pims.ws.vo.FollowUpView;
//import com.avanza.pims.ws.vo.MaintenanceTypeView;
//import com.avanza.pims.ws.vo.PaidFacilitiesView;
//import com.avanza.pims.ws.vo.PaymentReceiptView;
//import com.avanza.pims.ws.vo.PaymentScheduleView;
//import com.avanza.pims.ws.vo.PersonView;
//import com.avanza.pims.ws.vo.RequestDetailView;
//import com.avanza.pims.ws.vo.RequestFieldDetailView;
//import com.avanza.pims.ws.vo.RequestKeyView;
//import com.avanza.pims.ws.vo.RequestTypeView;
//import com.avanza.pims.ws.vo.RequestView;
//import com.avanza.pims.ws.vo.SiteVisitView;
//import com.avanza.pims.ws.vo.UnitView;
//import com.avanza.pims.ws.vo.WorkTypeView;
//import com.avanza.ui.util.ResourceUtil;
//
//
//public class MaintenanceRequest  extends AbstractController
//{
//
//    String moduleName="MaintenanceRequest";
//    
//    
//    private String requestId;
//    
//    private String AUCTION_UNIT_ID= "auctionUnitId";
//    private String AUCTION_DEPOSIT_AMOUNT   = "auctionDepositAmont";
//    private String AUCTION_UNIT_BIDDER_LIST = "auctionUnitBidderList";
//    private String PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID = "paymentScheduleOwnerShipTypeId";
//    private String PREVIOUS_UNIT_ID = "previousUnitId";
//    private ResourceBundle bundle;
//    
//    private boolean isPageModeAdd;
//    private boolean isPageModeAmendAdd;
//    private boolean isPageModeUpdate;
//    private boolean isPageModeOnSiteVisit;
//    private boolean isPageModeOnFollowUp;
//    private boolean isPageModeCompleted;
//    private boolean isPageModeApprovalRequired;
//    
//    private String DDV_COMMERCIAL_CONTRACT = "DDV_COMMERCIAL_CONTRACT";
//    private String PAGE_MODE_ADD = "ADD";
//    private String PAGE_MODE_UPDATE = "UPDATE";
//    private String PAGE_MODE_APPROVE_REJECT = "APPROVE_REJECT";
//    private String PAGE_MODE_SITE_VISIT = "PAGE_MODE_SITE_VISIT";
//    private String PAGE_MODE_FOLLOW_UP = "PAGE_MODE_FOLLOW_UP";
//    private String PAGE_MODE_COMPLETED = "PAGE_MODE_COMPLETED";
//    private String PAGE_MODE_APPROVAL_REQUIRED = "PAGE_MODE_APPROVAL_REQUIRED";
//    
//    private String REQUEST_HISTORY_FOR_CONTRACT="REQUEST_HISTORY_FOR_CONTRACT";
//    
//    private String TENANT_INFO="TENANTINFO";
//    private String UNIT_DATA_ITEM="UNITDATAITEM";
//    //private String PERSON_SUB_TYPE="PERSON_SUB_TYPE";
//    private HtmlInputText txtRefNumber=new HtmlInputText();
//    private HtmlOutputLabel lblRefNum=new HtmlOutputLabel();
//    private HtmlCommandButton btnOccuipers=new HtmlCommandButton();
//    private HtmlCommandButton btnPartners=new HtmlCommandButton();
//    private HtmlCommandButton btnSaveNewLease=new HtmlCommandButton();
//    private HtmlCommandButton btnSaveAmendLease=new HtmlCommandButton();
//    private HtmlCommandButton btnCompleteAmendLease=new HtmlCommandButton();
//    private HtmlCommandButton btnSubmitAmendLease=new HtmlCommandButton();
//    private HtmlCommandButton btnApprove=new HtmlCommandButton();
//    private HtmlCommandButton btnReject=new HtmlCommandButton();
//    private HtmlCommandButton btnApproveAmendLease=new HtmlCommandButton();
//    private HtmlCommandButton btnRejectAmendLease=new HtmlCommandButton();
//    private HtmlCommandButton btnGenPayments=new HtmlCommandButton();
//    private HtmlCommandButton btnSendNewLeaseForApproval = new HtmlCommandButton();
//    private HtmlCommandButton btnCollectPayment = new HtmlCommandButton();
//    private HtmlCommandButton btnAddUnit = new HtmlCommandButton();
//    private HtmlCommandButton btnAddAuctionUnit = new HtmlCommandButton();
//    private HtmlCommandButton btnAddPayments = new HtmlCommandButton();
//    HtmlSelectOneMenu cmbContractType = new HtmlSelectOneMenu(); 
//    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
//    private org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities=new org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox();
//    org.richfaces.component.html.HtmlTab tabCommercialActivity=new org.richfaces.component.html.HtmlTab();
//
//    org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
//    
//    private org.richfaces.component.html.HtmlPanel taskPanel=new org.richfaces.component.html.HtmlPanel();
//    
//    org.richfaces.component.html.HtmlCalendar issueDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
//    org.richfaces.component.html.HtmlCalendar startDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
//    org.richfaces.component.html.HtmlCalendar endDateCalendar = new  org.richfaces.component.html.HtmlCalendar();
//    HtmlInputText txt_ActualRent = new HtmlInputText();
//    private HtmlGraphicImage  btnPartnerDelete = new HtmlGraphicImage();
//    private HtmlGraphicImage  btnOccupierDelete = new HtmlGraphicImage();
//
//    private HtmlGraphicImage imgSponsor=new HtmlGraphicImage();
//    private HtmlGraphicImage imgTenant=new HtmlGraphicImage();
//    private HtmlGraphicImage imgManager=new HtmlGraphicImage();
//    private HtmlGraphicImage imgDeleteSponsor=new HtmlGraphicImage();
//    private HtmlGraphicImage imgDeleteManager=new HtmlGraphicImage();
//    private String pageMode; 
//    private String refNum;
//    private String datetimerequest;
//    
//    private String selectRequestStatus;
//    private String tenantRefNum;
//    private String unitRefNum;
//    private String unitStatusId;
//    private String unitRentValue;
//    private String txtUnitStatus;
//    private String auctionRefNum;
//    private String hdnAuctionunitId;
//    private String hdnUnitId;
//    private String hdnUnitUsuageTypeId;
//    private String txtUnitUsuageType;
//    private String unitRentAmount;
//    private String hdnTenantId;
//    private String hdntenanttypeId;
//    private String txtTenantName; 
//    private String txtSponsor;
//    private String txtTenantType;
//    private String hdnSponsorId;
//    private String txtManager;
//    private String hdnManagerId;
//    private String personSponsor;
//    private String personManager;
//    private String personOccupier;
//    private String personPartner;
//    private String txtMaintainenceAmount;
//    private Double maintainenceAmount;
//    private Date contractStartDate;
//    private Date contractEndDate;
//    private Date contractIssueDate;
//    private String contractId;
//    private String contractCreatedOn;
//    private String contractCreatedBy;
//    private String propertyCommercialName;
//    private String propertyAddress;
//    private String contractStatus;
//    private Double totalContractValue=new Double(0);
//    public String  txttotalContractValue;
//    public String hdnApplicantId;
//    
//    
//    List<SelectItem> priorityList = new ArrayList();
//	List<SelectItem> statusList = new ArrayList();
//	List<SelectItem> contractTypeList = new ArrayList();
//	private List<String> errorMessages = new ArrayList<String>();
//	private List<String> successMessages=new ArrayList<String>();
//    private HtmlInputText txtRefNum;  
//     boolean isCompanyTenant;
//     boolean isUnitCommercial;
//     boolean isPageUpdateStatusNew;
//     String pageTitle;
//    FacesContext context = null;
//    UIViewRoot view =null; 
//    
//    PersonView tenantsView=new PersonView();
//    RequestDetailView requestDetailView=new RequestDetailView();
//	UnitView unitsView=new UnitView();
//    
//	HtmlPanelTabbedPane panelTabbedPane =new HtmlPanelTabbedPane ();
//	private HtmlPanelTab panelTab=new HtmlPanelTab();
//	Map sessionMap;
//	Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
////	private HtmlDataTable paidFacilitiesDataTable;
////	
////	private List<PaidFacilitiesView> paidFacilitiesDataList = new ArrayList<PaidFacilitiesView>();
////	private PaidFacilitiesView paidFacilitiesDataItem = new PaidFacilitiesView();
//	private boolean isContractStatusApproved;
//	private HtmlDataTable commercialActivityDataTable;
//	private List<CommercialActivityView> commercialActivityList = new ArrayList<CommercialActivityView>();
//	private CommercialActivityView commercialActivityItem = new CommercialActivityView();
//	
//	
//	// Site Visie table 
//	private HtmlDataTable siteVisitDataTable = new HtmlDataTable();
//	private List<SiteVisitView> siteVisitList = new ArrayList<SiteVisitView>();
//	private SiteVisitView siteVisitDataItem = new SiteVisitView();
//	
//	private Integer paginatorMaxPages = 0;
//	private Integer paginatorRows = 0;
//	private Integer recordSize = 0;
//	
//	// Follow Up History
//	private HtmlDataTable followUpDataTable = new HtmlDataTable();
//	private List<FollowUpView> followUpList = new ArrayList<FollowUpView>();
//	private FollowUpView followUpDataItem = new FollowUpView();
//	
//	
//	private HtmlDataTable tenantsDataTable;
////	private List<TenantView> tenantsDataList = new ArrayList<TenantView>();
//	private List<PersonView> tenantsDataList = new ArrayList<PersonView>();
//	private PersonView tenantsDataItem = new PersonView();
//	private HtmlDataTable unitDataTable;
//	private List<UnitView> unitDataList = new ArrayList<UnitView>();
//	private UnitView unitDataItem = new UnitView();
//	
//	private HtmlDataTable paymentScheduleDataTable;
//	private List<PaymentScheduleView> paymentScheduleDataList = new ArrayList<PaymentScheduleView>();
//	private PaymentScheduleView paymentScheduleDataItem = new PaymentScheduleView();
//	private HtmlDataTable propspectiveOccupierDataTable;
//	private List<PersonView> prospectiveOccupierDataList = new ArrayList<PersonView>();
//	private PersonView prospectiveOccupierDataItem = new PersonView();
//	private HtmlDataTable propspectivepartnerDataTable;
//	private List<PersonView> prospectivepartnerDataList  = new ArrayList<PersonView>();
//	private PersonView prospectivepartnerDataItem = new PersonView();
//	boolean isArabicLocale;
//	boolean isEnglishLocale;
//	
//	private String selectOneContractType;
//	ContractView contractView =new ContractView();
//	RequestView requestView=new RequestView();
//	private static Logger logger = Logger.getLogger( RequestDetails.class );
//	private int index;
//	CommonUtil commonUtil=new CommonUtil();
//	HttpServletRequest request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//	String dateFormat="";
//	 HashMap feesConditionsMap=new HashMap();
//	 HashMap depositConditionsMap=new HashMap();
//	 private double totalRentAmount;
//		private double totalCashAmount;
//		private double totalChqAmount;
//		private int installments;
//		private double totalDepositAmount;
//		private double totalFees;
//		private double totalFines;
//	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
//	
//	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
//	
//	// Application Details
//	
//	private String applicationNo;
//	private HtmlInputText txtApplicationNo;
//	private String status;
//	private HtmlInputText txtApplicationStatus;
//	private HtmlInputText txtApplicationDate;
//	private Date appDate;
////	private HtmlInputText txtDescription;
//	
//	private HtmlInputTextarea txtDescription;
//	
//	private String description;
//	private HtmlInputText txtName;
//	private String name;
//	
//	private String selectOneApplicantType;
//	private HtmlInputText txtUnitNo;
//	
//	private HtmlInputText txtPropertyName;
//	
//	private HtmlInputText txtInputTenantName;
//	
//	private HtmlInputText txtApplicantType;
//	private String applicantType;
//	
//	private String maintenanceOn;
//	private String propertyName;
//	private String tenantName;
//	private String ownershipType;
//	List<SelectItem> ownershipTypeSelectList = new ArrayList();
//	
//	private String workType;
//	List<SelectItem> workTypeSelectList = new ArrayList();
//	
//	private String maintenanceType;
//	List<SelectItem> maintenanceTypeSelectList = new ArrayList();
//	
//	private String unitNumber;
//	//private String unitNumber;
//
//	private String svContractId;
//	List<SelectItem> svContractSelectList = new ArrayList();
//
//	
//	private HtmlSelectOneMenu requestStatusCombo = new HtmlSelectOneMenu();
//	private Map<String,Long> requestStatusMap = new HashMap<String,Long>();
//	private String statusId;
//	
//	
//	private String source;
//	private HtmlSelectOneMenu requestSourceCombo = new HtmlSelectOneMenu();
//	
//	private String engineerName;
//	private String actualCost;
//	private String estimatedCost;
//	private String remarksFor;
//	private String contractorName;
//	private String priorityValue;
//	
//	private Date visitDate;
//	
//	
//	public String getActualCost() {
//		if(viewRootMap.containsKey("actualCost") && viewRootMap.get("actualCost")!=null)
//			actualCost = viewRootMap.get("actualCost").toString();
//		
//		return actualCost;
//	}
//
//
//
//	public void setActualCost(String actualCost) {
//		this.actualCost = actualCost;
//	}
//
//
//	public String getEstimatedCost() {
//		if(viewRootMap.containsKey("estimatedCost") && viewRootMap.get("estimatedCost")!=null)
//			estimatedCost= viewRootMap.get("estimatedCost").toString();
//		
//		return estimatedCost;
//	}
//
//
//
//	public void setEstimatedCost(String estimatedCost) {
//		this.estimatedCost = estimatedCost;
//		
//		if(this.estimatedCost !=null)
//			viewRootMap.put("estimatedCost",this.estimatedCost);
//	}
//
//
//
//	public String getRemarksFor() {
//		if(viewRootMap.containsKey("remarksFor") && viewRootMap.get("remarksFor")!=null)
//			remarksFor= viewRootMap.get("remarksFor").toString();
//		
//		return remarksFor;
//	}
//
//
//
//	public void setRemarksFor(String remarksFor) {
//		this.remarksFor = remarksFor;
//	}
//
//
//
//	public String getContractorName() {
//		if(viewRootMap.containsKey("contractorName") && viewRootMap.get("contractorName")!=null)
//			contractorName = viewRootMap.get("contractorName").toString();
//		
//		return contractorName;
//	}
//
//
//
//	public void setContractorName(String contractorName) {
//		this.contractorName = contractorName;
//	}
//
//
//
//	public String getPriorityValue() {
//		if(viewRootMap.containsKey("priorityValue") && viewRootMap.get("priorityValue")!=null)
//			priorityValue = viewRootMap.get("priorityValue").toString();
//		return priorityValue;
//	}
//
//
//
//	public void setPriorityValue(String priorityValue) {
//		this.priorityValue = priorityValue;
//	}
//
//
//
//	public HtmlSelectOneMenu getRequestStatusCombo() {
//		return requestStatusCombo;
//	}
//
//
//
//	public void setRequestStatusCombo(HtmlSelectOneMenu requestStatusCombo) {
//		this.requestStatusCombo = requestStatusCombo;
//	}
//
//
//
//	public Map<String, Long> getRequestStatusMap() {
//		return requestStatusMap;
//	}
//
//
//
//	public void setRequestStatusMap(Map<String, Long> requestStatusMap) {
//		this.requestStatusMap = requestStatusMap;
//	}
//
//
//
//	public String getStatusId() {
//		return statusId;
//	}
//
//
//
//	public void setStatusId(String statusId) {
//		this.statusId = statusId;
//	}
//
//
//
//	public void setErrorMessages(List<String> errorMessages) {
//		this.errorMessages = errorMessages;
//	}
//
//
//
//	public void setSuccessMessages(List<String> successMessages) {
//		this.successMessages = successMessages;
//	}
//
//
//	@SuppressWarnings("unchecked")
//	public void loadRequestSourceMap() {
//		logger.logInfo("loadRequestSourceMap() started...");
//		try{
//			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//			if(sessionMap.containsKey(WebConstants.REQUEST_STATUS_MAP)){
//				requestStatusMap = (HashMap<String,Long>)sessionMap.get(WebConstants.REQUEST_STATUS_MAP);
//			}
//			else
//			{
//				List<DomainDataView> requestStatusList = propertyServiceAgent.getDomainDataByDomainTypeName(WebConstants.REQUEST_STATUS);
//				if(requestStatusList!=null) {
//					for (DomainDataView domainDataView : requestStatusList) {
//						requestStatusMap.put(domainDataView.getDataValue(), domainDataView.getDomainDataId());
//					}
//					sessionMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
//				}
//			}
//			logger.logInfo("loadRequestSourceMap() completed successfully!!!");
//		}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("loadRequestSourceMap() crashed ",exception);
//    	}
//    	catch(Exception exception){
//    		logger.LogException("loadRequestSourceMap() crashed ",exception);
//    	}
//	}
//	
//	@SuppressWarnings("unchecked")
//	public void loadWorkTypes() {
//		logger.logInfo("loadRequestStatusMap() started...");
//		try{
//			
//			ConstructionServiceAgent csa = new ConstructionServiceAgent();
//			
//			
//				
//				List<WorkTypeView> workTypeList = csa.getAllWorkTypes();
//				if(workTypeList!=null) {
//					for (WorkTypeView workTypeView: workTypeList) {
//						SelectItem si = new SelectItem();
//						if(isEnglishLocale)
//							si.setLabel(workTypeView.getDescriptionEn());
//						else if(isArabicLocale)
//							si.setLabel(workTypeView.getDescriptionAr());
//						else
//							si.setLabel(workTypeView.getDescriptionEn());
//						
//						si.setValue(workTypeView.getWorkTypeId().toString());
//						
//						workTypeSelectList.add(si);
//					}
//					sessionMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
//				}
//			
//			logger.logInfo("loadRequestStatusMap() completed successfully!!!");
//		}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//    	catch(Exception exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//	}
//	
//	@SuppressWarnings("unchecked")
//	public void loadMaintenanceTypes() {
//		logger.logInfo("loadMaintenanceTypes() started...");
//		try{
//			
//			ConstructionServiceAgent csa = new ConstructionServiceAgent();
//			
//		
//				
//				List<MaintenanceTypeView> maintenanceTypeList = csa.getAllMaintenanceTypes();
//				if( maintenanceTypeList != null) {
//					
//					for (MaintenanceTypeView maintenanceTypeView: maintenanceTypeList) {
//						SelectItem si = new SelectItem();
//						if(isEnglishLocale)
//							si.setLabel(maintenanceTypeView.getDescriptionEn());
//						else if(isArabicLocale)
//							si.setLabel(maintenanceTypeView.getDescriptionAr());
//						else
//							si.setLabel(maintenanceTypeView.getDescriptionEn());
//						
//						si.setValue(maintenanceTypeView.getMaintenanceTypeId().toString());
//						
//						maintenanceTypeSelectList.add(si);
//					}
//					sessionMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
//				}
//			
//			logger.logInfo("loadRequestStatusMap() completed successfully!!!");
//		}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//    	catch(Exception exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//	}
//	@SuppressWarnings("unchecked")
//	public void loadRequestStatusMap() {
//		logger.logInfo("loadRequestStatusMap() started...");
//		try{
//			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//			if(sessionMap.containsKey(WebConstants.REQUEST_STATUS_MAP)){
//				requestStatusMap = (HashMap<String,Long>)sessionMap.get(WebConstants.REQUEST_STATUS_MAP);
//			}
//			else
//			{
//				List<DomainDataView> requestStatusList = propertyServiceAgent.getDomainDataByDomainTypeName(WebConstants.REQUEST_STATUS);
//				if(requestStatusList!=null) {
//					for (DomainDataView domainDataView : requestStatusList) {
//						requestStatusMap.put(domainDataView.getDataValue(), domainDataView.getDomainDataId());
//					}
//					sessionMap.put(WebConstants.REQUEST_STATUS_MAP,requestStatusMap);
//				}
//			}
//			logger.logInfo("loadRequestStatusMap() completed successfully!!!");
//		}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//    	catch(Exception exception){
//    		logger.LogException("loadRequestStatusMap() crashed ",exception);
//    	}
//	}
//	
////	public String openUnitsPopUp()
////	{
////		String methodName="openUnitsPopUp";
////		logger.logInfo(methodName+"|"+"Start..");
////        //WebConstants.UNIT
//// 
////        FacesContext facesContext = FacesContext.getCurrentInstance();
////        if(sessionMap.containsKey("AUCTIONUNITINFO"))
////        	sessionMap.remove("AUCTIONUNITINFO");
////      
////        auctionRefNum="";
////        hdnAuctionunitId="";
////        String javaScriptText ="var screen_width = screen.width;"+
////        "var screen_height = screen.height;"+
////        "window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-260)+',left=0,top=40,scrollbars=yes,status=yes');";
////
////        openPopUp("UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP", javaScriptText);
////          
////		logger.logInfo(methodName+"|"+"Finish..");
////		return "";
////	}
//
//	
//	// Duplicate Items
////	private boolean isArabicLocale;	
////    private boolean isEnglishLocale;
////	private String pageMode;
//	
//	
// 	public  void populateApplicationDetails(String applicationNo,String status,Date appDate,String description,
// 			String name,String applicantType,String cellNo,String email)
// 	{
// 	  this.setApplicationNo(applicationNo);	
// 	  this.setStatus(status);
// 	  this.setAppDate(appDate);
// 	  this.setDescription(description);
// 	  this.setSelectOneApplicantType(applicantType);
//// 	  this.setCellNo(cellNo);
//// 	  this.setEmail(email);
// 	  	  
// 	}
// 	
////	private List<String> errorMessages;
////		public String getErrorMessages() {
////		return CommonUtil.getErrorMessages(errorMessages);
////	}
//
//		public String getApplicationNo() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_NUMBER)  )
//				applicationNo=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_NUMBER).toString();
//				
//			return applicationNo;
//		}
//
//		public void setApplicationNo(String applicationNo) {
//			this.applicationNo = applicationNo;
//			if(this.applicationNo!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, this.applicationNo);
//		}
//
//		public HtmlInputText getTxtApplicationNo() {
//			return txtApplicationNo;
//		}
//
//		public void setTxtApplicationNo(HtmlInputText txtApplicationNo) {
//			this.txtApplicationNo = txtApplicationNo;
//		}
//
//		public String getStatus() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_STATUS)  )
//				status=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_STATUS).toString();
//			
//			return status;
//		}
//
//		public void setStatus(String status) {
//			this.status = status;
//			if(this.status !=null  )
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,this.status);
//		}
//
//		public HtmlInputText getTxtApplicationStatus() {
//			return txtApplicationStatus;
//		}
//
//		public void setTxtApplicationStatus(HtmlInputText txtApplicationStatus) {
//			this.txtApplicationStatus = txtApplicationStatus;
//		}
//
//		public HtmlInputText getTxtApplicationDate() {
//			return txtApplicationDate;
//		}
//
//		public void setTxtApplicationDate(HtmlInputText txtApplicationDate) {
//			this.txtApplicationDate = txtApplicationDate;
//		}
//
//		public Date getAppDate() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DATE)  )
//				appDate=(Date)viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DATE);
//		
//			return appDate;
//		}
//
//		public void setAppDate(Date appDate) {
//			this.appDate = appDate;
//			if(this.appDate!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,this.appDate);
//		}
//
//		public HtmlInputTextarea getTxtDescription() {
//			return txtDescription;
//		}
//
//		public void setTxtDescription(HtmlInputTextarea txtDescription) {
//			this.txtDescription = txtDescription;
//		}
//
//		public String getDescription() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION))
//				description=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString();
//			return description;
//		}
//
//		public void setDescription(String description) {
//			this.description = description;
//			if(this.description!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,this.description);
//		}
//
//		public HtmlInputText getTxtName() {
//			return txtName;
//		}
//
//		public void setTxtName(HtmlInputText txtName) {
//			this.txtName = txtName;
//		}
//
//		public String getName() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME))
//				name=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME).toString();
//			return name;
//		}
//
//		public void setName(String name) {
//			this.name = name;
//			if(this.name!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,this.name);
//		}
//
//		public String getSelectOneApplicantType() {
//			return selectOneApplicantType;
//		}
//
//		public void setSelectOneApplicantType(String selectOneApplicantType) {
//			this.selectOneApplicantType = selectOneApplicantType;
//		}
//
////		public HtmlInputText getTxtCellNo() {
////			return txtCellNo;
////		}
////
////		public void setTxtCellNo(HtmlInputText txtCellNo) {
////			this.txtCellNo = txtCellNo;
////			
////		}
////
////		public String getCellNo() {
////			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL))
////				cellNo=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL).toString();
////				return cellNo;
////		}
////
////		public void setCellNo(String cellNo) {
////			this.cellNo = cellNo;
////			if(this.cellNo!=null)
////				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,this.cellNo);
////		}
////
////		public HtmlInputText getTxtEmail() {
////			return txtEmail;
////		}
//
////		public void setTxtEmail(HtmlInputText txtEmail) {
////			this.txtEmail = txtEmail;
////		}
////
////		public String getEmail() {
////			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL))
////				email=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL).toString();
////			return email;
////		}
//
////		public void setEmail(String email) {
////			
////			this.email = email;
////			if(this.email!=null)
////				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL,this.email);
////		}
////	
////		public String getDateFormat()
////		{
////	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
////	    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
////	    	 
////			return localeInfo.getDateFormat();
////		}
////		public TimeZone getTimeZone()
////		{
////			 return TimeZone.getDefault();
////			
////		}
//
//		public HtmlInputText getTxtApplicantType() {
//			return txtApplicantType;
//		}
//
//		public void setTxtApplicantType(HtmlInputText txtApplicantType) {
//			this.txtApplicantType = txtApplicantType;
//		}
//
//
//		public String getApplicantType() {
//			if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE))
//				applicantType=viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE).toString();
//			return applicantType;
//		}
//
//		public void setApplicantType(String applicantType) {
//			this.applicantType = applicantType;
//			if(this.applicantType!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,this.applicantType);
//			
//		}
//
//		public Boolean getApplicationDetailsReadonlyMode(){
//			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//			Boolean applicationDetailsReadonlyMode = (Boolean)viewMap.get("applicationDetailsReadonlyMode");
//			if(applicationDetailsReadonlyMode==null)
//				applicationDetailsReadonlyMode = false;
//			return applicationDetailsReadonlyMode;
//		}
//	
//	
//	
//	// Application Details End
//	
//    /** default constructor */
//    public  MaintenanceRequest()
//    {
//    	String methodName="Constructor";
//    	logger.logInfo(methodName+"|"+" Start");
//    	context = FacesContext.getCurrentInstance();
//    	view    = context.getViewRoot();
//	
//    	// String passport =context.getExternalContext().getRequestMap().get("passportNumber").toString();
//    	//   	Load all the combos present in the screen
//    	
//    	logger.logInfo(methodName+"|"+" Finish");
//    }
//   
//
//    /**
//     * <p>Callback method that is called whenever a page is navigated to,
//     * either directly via a URL, or indirectly via page navigation.
//     * Override this method to acquire resources that will be needed
//     * for event handlers and lifecycle methods, whether or not this
//     * page is performing post back processing.</p>
//     *
//     * <p>The default implementation does nothing.</p>
//     */
//    @Override
//    public void init() 
//    {
//    	
//    	String methodName="init";
//    	logger.logInfo(methodName+"|"+"Start");
//    	sessionMap =context.getExternalContext().getSessionMap();
//        
//		
//	    Map  requestMap=context.getExternalContext().getRequestMap();
//		try
//    	{  
//			loadMaintenanceTypes();
//			loadWorkTypes();
//			
// 	        //getCommercialActivityList();
//		       if(!isPostBack())
//		       {
//		    	   logger.logDebug(methodName+"|"+"Is not postback");
//		    	   loadAttachmentsAndComments(null);
//		    	   if(getRequestParam("CONTRACT_VIEW_ROOT")!=null )
//		    	   {
//		    		   viewRootMap=(Map)getRequestParam("CONTRACT_VIEW_ROOT");
//		    		   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//		    		   contractId=contractView.getContractId().toString();
//		    		   this.setPageMode(PAGE_MODE_UPDATE);
////		    		   viewRootMap.put("pageMode", pageMode);
//		    		   
//		    	   }
//		    	   
//		    	   if(request.getParameter(WebConstants.VIEW_MODE)!=null)
//		    		   viewRootMap.put(WebConstants.VIEW_MODE, request.getParameter(WebConstants.VIEW_MODE));
//			    	ArrayList<RequestView> list =new ArrayList<RequestView>();
//			    	
//			    		 //Remove session 
//			    		 logger.logInfo(methodName+"|"+"Remove Sessions..Start");
//			    		 RemoveSessions();
//			    		 logger.logInfo(methodName+"|"+"Remove Sessions..Finish");
//				         //Load Types such as UnitUsuageType,TenantType,ContractType
//				         loadTypes();
//				         //Load Combos data
//				         loadCombos();
//				       
//				         setContractDate();
//				     	 
//				    	 if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null)
//				    	 {
//				    		 this.setPageMode(getRequestParam(WebConstants.LEASE_AMEND_MODE).toString());
////				    	      viewRootMap.put("pageMode", pageMode);
//				    	 }
//				    	 logger.logInfo( methodName + "|" + "Checking if request came from taskList.." );
//				    	
//				    	 //if come from tasklist
//
//				    	 if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//				    	 {
//				    		getDataFromTaskList();
//				    		
//				    	 }
//				     	 
//				     	 //if come from request search or
//				     	 //from contract search for incomplete request of amend
//				    	 else if(FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null)
//				    	 {
//				    		 requestView=(RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
//						     if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
//						        contractId=requestView.getContractView().getContractId().toString();
//						     this.setPageMode(PAGE_MODE_UPDATE);
////				    	      viewRootMap.put("pageMode", pageMode);
//						     //if request was for amend contract
//						     if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null && getRequestParam(WebConstants.LEASE_AMEND_MODE).toString().trim().length()>0) 
//						    	 getAmendDataFromRequestView();
//						     
//				     	     
//				    	 }
//				     	 
//				     	 //if come from contract search for new lease contract OR AMEND FOR ADD
//					     else if(sessionMap.containsKey("contractId"))
//				    	 {
//				    		 contractId= sessionMap.get("contractId").toString(); 
//				    		 sessionMap.remove("contractId");
//				    		 if(getRequestParam(WebConstants.LEASE_AMEND_MODE)==null)
//				    		 CheckIfPageModeView();
//				    			
//				    	 }
//				    	 
//				    	 if(this.getRequestId()!=null && !this.getRequestId().equals("") )
//					     	{
//					     		getRequestById(this.getRequestId());
//				    			putRequestViewValuesInControl(this.getRequestId());
//				    			
//				    			ConstructionService cs= new ConstructionService();
//					    		
//					    		List<ContractView> lstServiceContracts = cs.getContractsByUnit(this.getUnitsView().getUnitId());
//					    		
//					    		if (lstServiceContracts != null) {
//					    			
//									for (int i = 0; i < lstServiceContracts.size(); i++) {
//			
//										ContractView cView = lstServiceContracts.get(i);
//			
//										SelectItem sItem = new SelectItem();
//										sItem.setLabel(cView.getContractNumber());
//										sItem.setValue(cView.getContractId().toString());
//										
//										svContractSelectList.add(sItem);
//			
//									}
//								}
//					     	}
//					     	else
//				    		{
//				    			logger.logInfo(methodName+"|"+"Add Mode");
//				    			//Add Mode
//				    			this.setPageMode(PAGE_MODE_ADD);
////				    			viewRootMap.put("pageMode", PAGE_MODE_ADD);
//				    			this.setStatusId("9001");
//				    		}
//				    	 
////				     	if(contractId!=null && !contractId.equals("") )
////				     	{
////				     		getContractById(contractId);
////			    			putViewValuesInControl(contractId);
////				     	}
////				     	else
////			    		{
////			    			logger.logInfo(methodName+"|"+"Add Mode");
////			    			//Add Mode
////			    			pageMode=PAGE_MODE_ADD;
////			    			viewRootMap.put("pageMode", PAGE_MODE_ADD);
////			    			this.setStatusId("9001");
////			    		}
//				        
//				         
//
//			    		
//		       }
//		       //Is Post Back 
//		       else
//		        { 
//		           
//		    	   logger.logInfo(methodName+"|"+"Is postback");
//		    	   if(viewRootMap.containsKey("pageMode"))
//		    		   this.setPageMode(viewRootMap.get("pageMode").toString());
//		    	   logger.logInfo(methodName+"|"+"PageMode::"+this.getPageMode());
//		    	   
//		    	   ///////////////
//		    	   
//		    	   if(sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT))
//		    		 {
//		    			 UnitView unitView=(UnitView)sessionMap.get(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);
//		    			 sessionMap.put("UNITINFO", unitView);
//		    			 sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT);
//	    				  this.setUnitRefNum(unitView.getUnitNumber());
//	    				  this.setPropertyName(unitView.getPropertyCommercialName());
//	    				  
//	    				 // this.setTenantName(//unitView.get)
//	    				  this.setOwnershipType(unitView.getPropertyCategoryEn());
//		    				  //this.setTenantName(unitView.getPropertyCommercialName());
//		    				  
//		    				  if(unitView.getStatusId()!= null)
//		    			      {
//		    			            //unitView.setStatusId(unitView.getStatusId());
//		    			            this.setUnitStatusId(unitView.getStatusId().toString());
//		    			       }
//		    				  
//		    				  viewRootMap.put(UNIT_DATA_ITEM, unitView);
//		    				  
//		    				  this.setUnitsView(unitView);
//		    				  
//		    				  if(unitView.getRentValue()!= null && (unitRentAmount== null || unitRentAmount.trim().length()==0))
//		    			        	 unitRentAmount = unitView.getRentValue().toString();
//    				 }
//		    			
//		    	   //////////////////
//		    	   
//		    	   if(sessionMap.containsKey(TENANT_INFO))
//		    	    {
//		    		   PersonView tenantViewRow=(PersonView)sessionMap.get(TENANT_INFO);
//		    	       
//		    		   viewRootMap.put(TENANT_INFO,tenantViewRow);
//		    	       txtTenantType=getTenantType(tenantViewRow);
//		    	       sessionMap.remove(TENANT_INFO); 
//		    	    }
//		    	   if(sessionMap.containsKey(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) 
//		    		 &&  sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)!=null
//		    		 && (Boolean)sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY)
//		    	    )
//			 	   {
//	    		       contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	    		       sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY);
//	    		       CollectPayments();
//					   getContractPaymentSchedule(contractView.getContractId().toString());
//			 	   }
//		    	   //IF COMING FROM ADDPAYMENTS,GENERATEPAYMENTS screen then the data list in session
//		    	   //is copied to viewRoot of this page
//                   if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
//                		   sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null	   
//                   )
//                   {
//                	   viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
//                	   sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//                   }    	   
//                	   getPaymentScheduleListFromSession();
//                	   
//                	   
//                	   if(this.getRequestId() != null ){
//                		   
//                		   getRequestById(this.getRequestId());
//                		   putRequestViewValuesInControl(this.getRequestId());
//                		   
//                		   ConstructionService cs= new ConstructionService();
//				    		
//				    		List<ContractView> lstServiceContracts = cs.getContractsByUnit(this.getUnitsView().getUnitId());
//				    		
//				    		if (lstServiceContracts != null) {
//				    			
//								for (int i = 0; i < lstServiceContracts.size(); i++) {
//		
//									ContractView cView = lstServiceContracts.get(i);
//		
//									SelectItem sItem = new SelectItem();
//									sItem.setLabel(cView.getContractNumber());
//									sItem.setValue(cView.getContractId().toString());
//									
//									svContractSelectList.add(sItem);
//		
//								}
//							}
//                	   }
//		    	   
//		    	  
//		    	 
//		    	 
//		     }
//    	}
//    	catch(Exception ex)
//    	{
//    		
//    		logger.LogException(methodName+"|"+"Error Occured:",ex);
//    	}
//       
//       logger.logInfo(methodName+"|"+"Finish");
//    }
//    private void CheckIfPageModeView()
//    {
//    	 if((context.getExternalContext().getRequestMap().containsKey(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
//                 &&	context.getExternalContext().getRequestMap().get(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW).toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
//                 )||
//                 (request.getParameter(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)!=null && 
//              	request.getParameter(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW).toString().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW)
//	    		    )
//                 )
//	    		 {
//    		 		this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW ) ;
//	    		 }
//	    		
//	    		 else
//	    			 this.setPageMode(PAGE_MODE_UPDATE);
//	    		 
////	    			viewRootMap.put("pageMode", pageMode);
//    	
//    }
//    private void getAmendDataFromRequestView()
//    {
//    	String methodName="getDataFromRequestView";
//    	logger.logDebug(methodName+"|"+"Start");
//		
//		
//	   if(getRequestParam(WebConstants.LEASE_AMEND_MODE)!=null)
//		viewRootMap.put("AMEND_CONTRACT_REQUEST_VIEW",requestView);
//		
//	   
//	   populateApplicationDetailsTab(); 
//		DomainDataView ddvComplete= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
//						WebConstants.REQUEST_STATUS_COMPLETE);
//		DomainDataView ddvRejected= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
//						WebConstants.REQUEST_STATUS_REJECTED);
//		DomainDataView ddvCancelled= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
//						WebConstants.REQUEST_STATUS_CANCELED);
//		
//						
//		if(requestView.getStatusId().compareTo(ddvComplete.getDomainDataId())==0 || 
//		   requestView.getStatusId().compareTo(ddvRejected.getDomainDataId())==0 ||
//		   requestView.getStatusId().compareTo(ddvCancelled.getDomainDataId())==0 
//		  )
//			this.setPageMode( WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW );
//		//Either amend add or amend update
//		else if(getRequestParam(WebConstants.LEASE_AMEND_MODE) !=null && getRequestParam(WebConstants.LEASE_AMEND_MODE).toString().trim().length()>0)
//	    	this.setPageMode(getRequestParam(WebConstants.LEASE_AMEND_MODE).toString());
//	        
//		
////		 viewRootMap.put("pageMode", pageMode);
//		 
//		logger.logDebug(methodName+"|"+"Finish");
//    }
//    private void getDataFromTaskList()throws Exception
//    {
//    	   String methodName="getDataFromTaskList";
//    	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//    	   logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
//		   if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
//		       this.setRequestId( userTask.getTaskAttributes().get("REQUEST_ID").toString() );
//		   
//	       viewRootMap.put(WebConstants.CONTRACT_USER_TASK_LIST, userTask);
//	       
//	       if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
//	    	{
//	    	   
//		    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.MaintenanceRequest_TaskTypes.APPROVE_REJECT_SITE_VISIT_REQUEST))
//		    	{
////		    		pageMode=PAGE_MODE_APPROVAL_REQUIRED;
//		    		
//		    		this.setPageMode(PAGE_MODE_APPROVAL_REQUIRED);
////			    	viewRootMap.put("pageMode", pageMode);
//			    	
//			
//		    	}
//		    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.MaintenanceRequest_TaskTypes.SEND_FOR_SITE_VISIT))
//		    	{
//		    		this.setPageMode(PAGE_MODE_SITE_VISIT);
////			    	pageMode=PAGE_MODE_SITE_VISIT;
////			    	viewRootMap.put("pageMode", pageMode);
//			    	
//			    	
//		    	}
//		    	
//		    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.MaintenanceRequest_TaskTypes.FOLLOW_UP_REQUEST))
//		    	{
//		    		
//		    		this.setPageMode(PAGE_MODE_FOLLOW_UP);
////			    	pageMode=PAGE_MODE_FOLLOW_UP;
////			    	viewRootMap.put("pageMode", pageMode);
//		    	}
//	    	}
//	       
////	       CheckIfForAmend();
//	       
//	       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//    }
//    
//    private void setContractDate()
//    {
//    	
//    	     String methodName="setContractDate";
//    	     DateFormat dF= new SimpleDateFormat(getDateFormat());
//    	     this.setDatetimerequest(dF.format(new Date()));
//    	     logger.logInfo(methodName+"|Contract Start Date"+dF.format(new Date()));
//	         this.setContractStartDate(new Date());
//	         Calendar cal=new GregorianCalendar();
//			 cal.set(cal.get(Calendar.YEAR), Calendar.DECEMBER, 31);
//			 logger.logInfo(methodName+"|Contract End Date"+dF.format(cal.getTime()));
//		     this.setContractEndDate(cal.getTime());
//    }
//
//    private void CheckIfForAmend()throws PimsBusinessException
//    {
//    	UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//    	 boolean isTaskForAmend=false;
//    	 	Long reqId=null;
//    	if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
//    		reqId = new Long(userTask.getTaskAttributes().get("REQUEST_ID").toString());
//    	if(userTask.getTaskAttributes().get("TASK_TYPE")!=null)
//    	{
//	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.AmendLeaseContract_TaskTypes.APPROVE_AMEND_LEASE_CONTRACT))
//	    	{
//	    		this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT );
////		    	viewRootMap.put("pageMode", pageMode);
//		    	setRequestParam(WebConstants.LEASE_AMEND_MODE,this.getPageMode());
//		    	isTaskForAmend=true;
//	    	}
//	    	if(userTask.getTaskAttributes().get("TASK_TYPE").toString().equals(WebConstants.AmendLeaseContract_TaskTypes.COMPLETE_AMEND_LEASE_CONTRACT))
//	    	{
//	    		this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL);
////		    	viewRootMap.put("pageMode", pageMode);
//		    	setRequestParam(WebConstants.LEASE_AMEND_MODE,this.getPageMode());
//		    	isTaskForAmend=true;
//	    	}
//	    	if(isTaskForAmend && reqId !=null)
//	    	{
//	    		PropertyServiceAgent psa = new PropertyServiceAgent();
//	    		requestView= psa.getRequestById(reqId);
//	    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
//	    		 if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
//			        contractId=requestView.getContractView().getContractId().toString();
//			    	 getAmendDataFromRequestView();
//	    	}
//    	}
//    	
//    }
//    private void setTenantTypeViewFromPersonSubType()
//    {
//        String method="setTenantTypeViewFromPersonSubType";
//        String tenantType="";
//        logger.logDebug(method+"|"+"Start...");
//        List<DomainDataView> tenantTypeList=new ArrayList<DomainDataView>(0);
//            if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TENANT_TYPE))
// 			{
// 				List<DomainDataView> ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.PERSON_SUB_TYPE);
// 				for(int i=0;i<ddvList.size();i++)
// 				{
// 					DomainDataView ddv=(DomainDataView)ddvList.get(i);
// 					if(ddv.getDataValue().equals(WebConstants.PERSON_SUB_TYPE_COMPANY)||ddv.getDataValue().equals(WebConstants.PERSON_SUB_TYPE_INDIVIDUAL))
// 				     tenantTypeList.add(ddv);
// 				}
// 				viewRootMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE,tenantTypeList);
// 			 }
//             logger.logDebug(method+"|"+"Finish...");
//    }
//    private String getTenantType(PersonView tenantView)
//    {
//    
//       String method="getTenantType";
//       logger.logDebug(method+"|"+"Start...");
//       DomainDataView ddv;
//       if(tenantView!=null && tenantView.getPersonId()!=null && tenantView.getIsCompany()!=null )
//       {
//       if( tenantView.getIsCompany().compareTo(new Long(1))==0)
//       {
//    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
//    		 
//       }
//       else
//       {
//    		 ddv = (DomainDataView )viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
//    		 
//       }
//       if(getIsArabicLocale())
//			return ddv.getDataDescAr();
//	   else if(getIsEnglishLocale())
//			return  ddv.getDataDescEn();
//       }
//            logger.logDebug(method+"|"+"Finish...");
//    return "";
//    
//    
//    
//    }
//    /**
//     * <p>Callback method that is called after the component tree has been
//     * restored, but before any event processing takes place.  This method
//     * will <strong>only</strong> be called on a "post back" request that
//     * is processing a form submit.  Override this method to allocate
//     * resources that will be required in your event handlers.</p>
//     *
//     * <p>The default implementation does nothing.</p>
//     **/
//    @Override
//    public void preprocess() 
//    {
//    	super.preprocess();
//    	if(!isPostBack())
//        {
//     	   
//     	   logger.logInfo("|"+"Is not postback");
//        }
//        contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
//    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
//    
//    }
//  
//    /**
//     * <p>Callback method that is called just before rendering takes place.
//     * This method will <strong>only</strong> be called for the page that
//     * will actually be rendered (and not, for example, on a page that
//     * handled a post back and then navigated to a different page).  Override
//     * this method to allocate resources that will be required for rendering
//     * this page.</p>
//     *
//     * <p>The default implementation does nothing.</p>
//     */
//    @Override
//    public void prerender() 
//    {
//    	
//        String methodName="prerender";
//    	try
//    	{
//		    	FillTenantInfo();
//		    	FillApplicantInfo();
//		    	if(hdnManagerId!=null && hdnManagerId.trim().length()>0)
//		    	{
//		    	  String[] managerInfo =  getPersonNameIdFromHidden(hdnManagerId);
//		    	  this.setTxtManager(managerInfo[1]);
//		    	}
//		    	if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0)
//		    	{
//		    	  String[] sponsorInfo =  getPersonNameIdFromHidden(hdnSponsorId);
//		    	  this.setTxtSponsor(sponsorInfo[1]);
//		    	}
//		    	if(viewRootMap.containsKey(TENANT_INFO))
//			    {
//				   PersonView tenantViewRow=(PersonView)viewRootMap.get(TENANT_INFO);
//			       //tenantRefNum=tenantViewRow.getTenantNumber();
//			       txtTenantType=getTenantType(tenantViewRow);
//			       String tenantNames="";
//			        if(tenantViewRow.getPersonFullName()!=null)
//			        tenantNames=tenantViewRow.getPersonFullName();
//			        if(tenantNames.trim().length()<=0)
//			        	 tenantNames=tenantViewRow.getCompanyName();
//			       txtTenantName=tenantNames;
//			       
//			    }
//		    	if(isPostBack())
//		    	{
//		    		 //tabPaidFacility_Click();
//		    		 //if(pageMode.equals(PAGE_MODE_ADD)|| pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
//		    		 // valueChangeListerner_RentValue();
//		    	}
//		    	calculatePaymentScheduleSummaray();
////		    	getContractStatus();
//		    	EnableDisableButtons();
//		    	setPageTitle();
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+"Error Occured:",ex);		
//    	}
//    }
//    
//    private void clearApplicationDetails() {
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_NUMBER);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_EMAIL);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DATE);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_ID);
//    	viewRootMap.remove(WebConstants.ApplicationDetails.APPLICATION_STATUS);
//    	
//    }
//    private void populateApplicationDetailsTab() {
//		//PopulatingApplicationDetailsTab
//		if(requestView!=null)
//		{
//			if(requestView.getRequestNumber()!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
//			if(requestView.getRequestDate()!=null)
//				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
//			
//			if(requestView.getStatusId()!=null)
//			{
//				if(getIsEnglishLocale())
//				{
//					if(requestView.getStatusEn()!=null)	
//				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusEn());
//				}else if(getIsArabicLocale())
//				{
//					if(requestView.getStatusAr()!=null)
//					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusAr());
//				}
//			}
//			else
//			{
//				List<DomainDataView >ddvList= commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
//				DomainDataView ddv=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
//				if(getIsEnglishLocale())
//				{
//						
//				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescEn());
//				}else if(getIsArabicLocale())
//				{
//					
//					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescAr());
//				}
//			}
//			if(requestView.getApplicantView()!=null)
//				populateApplicantDetails(requestView.getApplicantView());
//			
//			
//		}
//	}
//    private void populateApplicantDetails(PersonView pv)
//    {
//     if(pv.getPersonFullName()!=null && pv.getPersonFullName().trim().length()>0)
//    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
//     else if(pv.getCompanyName()!=null && pv.getCompanyName().trim().length()>0)
//    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
//    	 String personType = getTenantType(pv);
//     if(personType !=null)
//         viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
//     if(pv.getCellNumber()!=null)
//    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
//     if(pv.getPersonId()!=null)
//     {
//    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,pv.getPersonId());
//    	 hdnApplicantId = pv.getPersonId().toString();
//     }
//    	 
//    }
//    private String[] getPersonNameIdFromHidden(String hdn)
//    {
//      return hdn.split(":");
//    }
//    private void FillApplicantInfo()throws PimsBusinessException
//    {
//    	String methodName="FillApplicantInfo";
//    	logger.logInfo(methodName+"| Start");
//    	PersonView applicantView;
//    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW) && 
//    	   viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW)!=null		
//    	   )
//    	{
//    		applicantView=(PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
//	    	if(hdnApplicantId!=null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId))!=0)
//	    	{
//	    		PropertyServiceAgent psa=new PropertyServiceAgent();
//	    		PersonView pv=new PersonView();
//	    		pv.setPersonId(new Long(hdnApplicantId));
//	    		List<PersonView> personsList =  psa.getPersonInformation(pv);
//	    		if(personsList.size()>0)
//	    		{
//	    			pv = personsList.get(0);
//	    			populateApplicantDetails(pv);
//	    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
//	    		}
//	    			
//	    	}
//    	}
//    	else if(hdnApplicantId!=null && hdnApplicantId.trim().length()>0)
//    	{
//    		PropertyServiceAgent psa=new PropertyServiceAgent();
//    		PersonView pv=new PersonView();
//    		pv.setPersonId(new Long(hdnApplicantId));
//    		List<PersonView> personsList=  psa.getPersonInformation(pv);
//    		if(personsList.size()>0)
//    		{
//    			pv = personsList.get(0);
//    			populateApplicantDetails(pv);
//    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
//    		}	
//    	}
//    	
//    	logger.logInfo(methodName+"| Finish");
//    }
//    private void FillTenantInfo()throws PimsBusinessException
//    {
//    	String methodName="FillTenantInfo";
//    	logger.logInfo(methodName+"| Start");
//    	PersonView tenantView;
//    	if(viewRootMap.containsKey(TENANT_INFO))
//    	{
//    		tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
//	    	if(this.getHdnTenantId()!=null && this.getHdnTenantId().toString().trim().length()>0 && tenantView.getPersonId()!=null && tenantView.getPersonId().compareTo(new Long(this.getHdnTenantId()))!=0)
//	    	{
//	    		PropertyServiceAgent psa=new PropertyServiceAgent();
//	    		PersonView pv=new PersonView();
//	    		pv.setPersonId(new Long(this.getHdnTenantId()));
//	    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
//	    		if(tenantsList.size()>0)
//	    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));
//	    			
//	    	}
//    	}
//    	else if(this.getHdnTenantId()!=null && this.getHdnTenantId().trim().length()>0)
//    	{
//    		PropertyServiceAgent psa=new PropertyServiceAgent();
//    		PersonView pv=new PersonView();
//    		pv.setPersonId(new Long(this.getHdnTenantId()));
//    		List<PersonView> tenantsList =  psa.getPersonInformation(pv);
//    		if(tenantsList.size()>0)
//    			viewRootMap.put(TENANT_INFO,tenantsList.get(0));
//    			
//    	}
//    	
//    	logger.logInfo(methodName+"| Finish");
//    }
//	public boolean getIsViewModePopUp()
//	{
//		boolean isViewModePopup=true;
//		if(!viewRootMap.containsKey(WebConstants.VIEW_MODE) || !viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP))
//			isViewModePopup=false;
//		
//		return isViewModePopup;
//	}
//	public boolean getIsContractCommercial()
//	{
//		boolean isContractCommercial=true;
//		  DomainDataView ddv=new DomainDataView();
//		if(!viewRootMap.containsKey(DDV_COMMERCIAL_CONTRACT))
//		{
//		  ArrayList<DomainDataView> ddvList=(ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
//		  ddv=commonUtil.getIdFromType(ddvList, WebConstants.COMMERCIAL_LEASE_CONTRACT);
//		   viewRootMap.put(DDV_COMMERCIAL_CONTRACT,ddv);
//		}
//		else 
//		   ddv=(DomainDataView)viewRootMap.get(DDV_COMMERCIAL_CONTRACT);
//		if(this.getSelectOneContractType()!=null && this.getSelectOneContractType().equals(ddv.getDomainDataId().toString()))
//			isContractCommercial=true;
//		else
//			isContractCommercial=false;
//		
//		return isContractCommercial;
//	}
//	public void selectContractType_Change()
//	{
//	   
//	   if(getIsContractCommercial())
//	    tabCommercialActivity.setRendered(true);
//	   else
//	    tabCommercialActivity.setRendered(false);
//	   
//	   //sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);
//	   
//	}
//   private void setPageTitle()
//   {
//	   if(this.getPageMode().equals(PAGE_MODE_ADD) || this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT)|| 
//			   this.getPageMode().equals(PAGE_MODE_UPDATE)||this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW) ){
//	     
//		   pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_NEW_LEASE_CONTRACT);
////		   pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_NEW_LEASE_CONTRACT);
//	     
//	   }else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND) ||
//			   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)|| 
//			   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT)||
//			   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE) ||
//			   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL)||
//			   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW)
//			   )
//		   pageTitle=getBundle().getString(MessageConstants.ContractAdd.TITLE_AMEND_CONTRACT);
//   }
//   private void EnableDisableButtons()
//   {
//	   String method="EnableDisableButtons";
//	   logger.logDebug(method+"|"+"Start...");
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   if(getIsContrctStatusRejected())
//	   {
//		   this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW );
////		   viewRootMap.put("pageMode",pageMode);
//	   }
//	   if(this.getPageMode().equals(PAGE_MODE_ADD))
//	       PageModeAdd();
//	   
//	   else if(this.getPageMode().equals(PAGE_MODE_UPDATE))
// 	       PageModeUpdate();
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
//	       PageModeAmendAdd();
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE))
//	   {
//		   PageModeAmendUpdate();
//		   tabCommercialActivity_Click(); 
//		   tabPaidFacility_Click();
//	   }
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT))
//		   PageModeAmendApproveReject();
//	   else if(this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT))
//		   PageModeApproveReject();
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW))
//		   PageModeView();
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW))
//		   PageModeAmendView();
//	   else if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_COMPLETECANCEL))
//		   PageModeAmendCompleteCancel();
//	   else if(this.getPageMode().equals(PAGE_MODE_APPROVAL_REQUIRED))
//		   PageModeApprovalRequired();
//	   
//	   logger.logDebug(method+"|"+"Finish...");
//   }
//   private void PageModeAmendView()
//   {
//       txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//	   startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       viewRootMap.put("canAddAttachment", false); 
//       viewRootMap.put("canAddNote", false);
//       btnCollectPayment.setRendered(false);
//       btnAddPayments.setRendered(false);
//       imgTenant.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	   //txtRefNumber.setRendered(true);
//	  // lblRefNum.setRendered(true);
////	   tabContractHistory.setRendered(false);
////	   tabRequestHistory.setRendered(false);
//	   btnGenPayments.setRendered(false);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   chkCommercial.setDisabled(true);
//	   //chkPaidFacilities.setDisabled(true);
//	   
//   }
//   private void PageModeView()
//   {
//	   taskPanel.setRendered(false);
//	   txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//       startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", false); 
//       viewRootMap.put("canAddNote", false);
//       btnCollectPayment.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	   btnAddPayments.setRendered(false);
//	  // txtRefNumber.setRendered(true);
//	   //lblRefNum.setRendered(true);
//	   btnGenPayments.setRendered(false);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   chkCommercial.setDisabled(true);
//	  //chkPaidFacilities.setDisabled(true);
//	   taskPanel.setRendered(false);
//	   
//   }
//   private void PageModeAmendApproveReject()
//   {
//	   txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//	   btnAddPayments.setRendered(false);
//	   startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", true);
//       btnCollectPayment.setRendered(false); 
//       viewRootMap.put("canAddNote", true);
//       btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	  // txtRefNumber.setRendered(false);
//	 //  lblRefNum.setRendered(false);
////	   tabContractHistory.setRendered(false);
////	   tabRequestHistory.setRendered(false);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(true); 
//	   btnRejectAmendLease.setRendered(true);
//	   chkCommercial.setDisabled(true);
//	  // chkPaidFacilities.setDisabled(true);
//	   
//   }
//   private void PageModeAmendCompleteCancel()
//   {
//	   txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//	   btnAddPayments.setRendered(false);
//	   startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", true);
//       btnCollectPayment.setRendered(false); 
//       viewRootMap.put("canAddNote", true);
//       btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	  // txtRefNumber.setRendered(false);
//	 //  lblRefNum.setRendered(false);
////	   tabContractHistory.setRendered(false);
////	   tabRequestHistory.setRendered(false);
//	   btnCompleteAmendLease.setRendered(true);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   chkCommercial.setDisabled(true);
//	  // chkPaidFacilities.setDisabled(true);
//	   tabCommercialActivity_Click();
//	   
//   }
//   private void PageModeApproveReject()
//   {
//	   txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//	   btnAddPayments.setRendered(false);
//	   startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", true); 
//       viewRootMap.put("canAddNote", true);
//       btnCollectPayment.setRendered(false);
//       btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	 //  txtRefNumber.setRendered(true);
//	 //  lblRefNum.setRendered(true);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(true); 
//	   btnReject.setRendered(true);
//	   chkCommercial.setDisabled(true);
//	  // chkPaidFacilities.setDisabled(true);
//	   taskPanel.setRendered(true);
//   }
//   
//   private void PageModeApprovalRequired()
//   {
//	   txt_ActualRent.setReadonly(true);
//       btnPartnerDelete.setRendered(false);
//       btnOccupierDelete.setRendered(false);
//       imgDeleteManager.setRendered(false);
//	   imgDeleteSponsor.setRendered(false);
//	   cmbContractType.setDisabled(true);
//	   btnAddPayments.setRendered(false);
//	   startDateCalendar.setDisabled(true);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", false); 
//       viewRootMap.put("canAddNote", false);
//       btnCollectPayment.setRendered(false);
//       btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(false);
//	 //  txtRefNumber.setRendered(true);
//	 //  lblRefNum.setRendered(true);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   imgManager.setRendered(false);
//	   imgSponsor.setRendered(false);
//	   btnApprove.setRendered(true); 
//	   btnReject.setRendered(true);
//	   chkCommercial.setDisabled(true);
//	  // chkPaidFacilities.setDisabled(true);
//	   taskPanel.setRendered(true);
//   }
//   
//   private void PageModeAmendAdd()
//   {
//	   taskPanel.setRendered(false);
//	   txt_ActualRent.setReadonly(false);
//       btnPartnerDelete.setRendered(true);
//       btnOccupierDelete.setRendered(true);
//       
//	   cmbContractType.setDisabled(true);
//	   btnAddPayments.setRendered(true);
//	   startDateCalendar.setDisabled(false);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//       btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//       imgTenant.setRendered(false);
//       viewRootMap.put("canAddAttachment", true); 
//       viewRootMap.put("canAddNote", true);
//       btnCollectPayment.setRendered(false);
//       btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(true); 
//	   btnPartners.setRendered(true);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(true);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	  // txtRefNumber.setRendered(false);
//	  // lblRefNum.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
////	   tabContractHistory.setRendered(false);
////	   tabRequestHistory.setRendered(false);
////	   tabAuditTrail.setRendered(false);
//	   if(getIsContractCommercial())
//	   {
//		   imgManager.setRendered(true);
//		   imgSponsor.setRendered(true);
//		   
//		   if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0 && !getIsCompanyTenant())
//			   imgDeleteSponsor.setRendered(true);
//		   else
//			   imgDeleteSponsor.setRendered(false);
//		   if(hdnManagerId!=null && hdnManagerId.trim().length()>0 )
//			   imgDeleteManager.setRendered(true);
//		   else
//			   imgDeleteManager.setRendered(false);
//		   tabCommercialActivity.setRendered(true);
////		   tabPartner.setRendered(true);
//		   tabCommercialActivity_Click();
//	   }
//	   else
//	   {
//		   this.setHdnManagerId("");
//		   this.setHdnSponsorId("");
//		   this.txtSponsor="";
//		   this.txtManager="";
//		   imgManager.setRendered(false);
//		   imgSponsor.setRendered(false);
//		   tabCommercialActivity.setRendered(false);
////		   tabPartner.setRendered(false);
//		   sessionMap.remove(WebConstants.PARTNER);
//		   
//		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
//	   }
//	   chkCommercial.setDisabled(false);
//	  // chkPaidFacilities.setDisabled(false);
//   }
//   private void PageModeAmendUpdate()
//   {
//	   taskPanel.setRendered(false);
//	   txt_ActualRent.setReadonly(false);
//       btnPartnerDelete.setRendered(true);
//       btnOccupierDelete.setRendered(true);
//       
//	   cmbContractType.setDisabled(false);    
//	   startDateCalendar.setDisabled(false);
//       endDateCalendar.setDisabled(true);
//       issueDateCalendar.setDisabled(true);
//        btnAddUnit.setRendered(false);
//       btnAddAuctionUnit.setRendered(false);
//        imgTenant.setRendered(false);
//        btnCollectPayment.setRendered(false);
//	    viewRootMap.put("canAddAttachment", true); 
//        viewRootMap.put("canAddNote", true);
//	    btnGenPayments.setRendered(false);
//	   btnSendNewLeaseForApproval.setRendered(false);
//	   btnOccuipers.setRendered(false); 
//	   btnPartners.setRendered(false);
//	   btnSaveNewLease.setRendered(false);
//	   btnSaveAmendLease.setRendered(true);
//	   btnCompleteAmendLease.setRendered(false);
//	//   txtRefNumber.setRendered(false);
//	//   lblRefNum.setRendered(false);
////	   tabContractHistory.setRendered(false);
////	   tabRequestHistory.setRendered(false);
////	   tabAuditTrail.setRendered(true);
//	   btnSubmitAmendLease.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//       btnRejectAmendLease.setRendered(false);
//	   if(getIsContractCommercial())
//	   {
//		   imgManager.setRendered(true);
//		   imgSponsor.setRendered(true);
//		   if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0 && !getIsCompanyTenant())
//			   imgDeleteSponsor.setRendered(true);
//		   else
//			   imgDeleteSponsor.setRendered(false);
//		   if(hdnManagerId!=null && hdnManagerId.trim().length()>0 )
//			   imgDeleteManager.setRendered(true);
//		   else
//			   imgDeleteManager.setRendered(false);
//		   tabCommercialActivity.setRendered(true);
////		   tabPartner.setRendered(true);
//	   }
//	   else
//	   {
//		   this.setHdnManagerId("");
//		   this.setHdnSponsorId("");
//		   this.txtSponsor="";
//		   this.txtManager="";
//		   imgManager.setRendered(false);
//		   imgSponsor.setRendered(false);
//		   tabCommercialActivity.setRendered(false);
////		   tabPartner.setRendered(false);
//		   sessionMap.remove(WebConstants.PARTNER);
//		   
//		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
//	   }
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   RequestView requestView=(RequestView)viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW");
//	   List<DomainDataView> ddvList=commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
//	   DomainDataView ddvApproved=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVED);
//	   DomainDataView ddvApprovalRequired=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
//	   DomainDataView ddvAccepted=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_ACCEPTED);
//	   if(requestView!=null &&  ddvApproved.getDomainDataId().compareTo(requestView.getStatusId())==0)
//	   {
//		   btnSaveAmendLease.setRendered(false);
//		   btnCompleteAmendLease.setRendered(true);
//		   btnAddPayments.setRendered(false);
//	   }
//	   else if(requestView!=null &&  ddvApprovalRequired.getDomainDataId().compareTo(requestView.getStatusId())==0)
//	   { 
//		   btnSubmitAmendLease.setRendered(false);
//	   }
//	   else if(requestView!=null &&  ddvAccepted.getDomainDataId().compareTo(requestView.getStatusId())==0)
//	   {
//		   btnSubmitAmendLease.setRendered(true);
//	   }
//	   chkCommercial.setDisabled(true);
//	  // chkPaidFacilities.setDisabled(true);
//   }
//   private void PageModeUpdate()
//   {
//	   taskPanel.setRendered(false);
//	   txt_ActualRent.setReadonly(false);
//       btnPartnerDelete.setRendered(true);
//       btnOccupierDelete.setRendered(true);
//       
//	   cmbContractType.setDisabled(false);
//	   btnAddPayments.setRendered(true);
//       startDateCalendar.setDisabled(false);
//       endDateCalendar.setDisabled(false);
//       issueDateCalendar.setDisabled(false);
//       btnAddUnit.setRendered(true);
//       btnAddAuctionUnit.setRendered(true);
//       imgTenant.setRendered(true);
//       btnCollectPayment.setRendered(true);
//       viewRootMap.put("canAddAttachment", true); 
//       viewRootMap.put("canAddNote", true);
//       btnGenPayments.setRendered(true);
//	   btnOccuipers.setRendered(true); 
//	   btnPartners.setRendered(true);
//	   btnSaveNewLease.setRendered(true);
//	   btnSaveAmendLease.setRendered(false);
//	   btnCompleteAmendLease.setRendered(true);
//	   btnSubmitAmendLease.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   
//	 //  chkPaidFacilities.setDisabled(true);
//	   if(getIsContractCommercial())
//	   {
//		   chkCommercial.setDisabled(false);
//		   imgManager.setRendered(true);
//		   imgSponsor.setRendered(true);
//		   if(hdnSponsorId!=null && hdnSponsorId.trim().length()>0 && !getIsCompanyTenant())
//			   imgDeleteSponsor.setRendered(true);
//		   else
//			   imgDeleteSponsor.setRendered(false);
//		   if(hdnManagerId!=null && hdnManagerId.trim().length()>0 )
//			   imgDeleteManager.setRendered(true);
//		   else
//			   imgDeleteManager.setRendered(false);
//		   tabCommercialActivity.setRendered(true);
//		   tabCommercialActivity_Click();
////		   tabPartner.setRendered(true);
//	   }
//	   else
//	   {
//		   this.setHdnManagerId("");
//		   this.setHdnSponsorId("");
//		   this.txtSponsor="";
//		   this.txtManager="";
//		   imgManager.setRendered(false);
//		   imgSponsor.setRendered(false);
//		   tabCommercialActivity.setRendered(false);
////		   tabPartner.setRendered(false);
//		   sessionMap.remove(WebConstants.PARTNER);
//		   
//		   viewRootMap.remove("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
//	   
//	   }
//
//	   List<DomainDataView> ddvList=commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
//	    requestView =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
//	   DomainDataView ddvNew=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
//	   //if contract has been saved and contract status is new
//	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
//	   {
//		   btnSendNewLeaseForApproval.setRendered(true);
//	       btnCollectPayment.setRendered(false);
//	   }
////	   else if(getIsContractStatusApprovalRequired())
////	   {
////		   btnSendNewLeaseForApproval.setRendered(false);
////		   btnCollectPayment.setRendered(false);
////	   }
//	   else if(getIsContrctStatusActive())
//		   ActiveContractForUpdateMode();
//	   if(requestView!=null &&  ddvNew.getDomainDataId().compareTo(requestView.getStatusId())!=0)
//	   {  
//    	   btnSendNewLeaseForApproval.setRendered(false);
//	   }
//
//	   
//   }
//   private void ActiveContractForUpdateMode()
//   {
//		   txt_ActualRent.setReadonly(true);
//	       btnPartnerDelete.setRendered(false);
//	       btnOccupierDelete.setRendered(false);
//	       imgDeleteManager.setRendered(false);
//		   imgDeleteSponsor.setRendered(false);
//		   cmbContractType.setDisabled(true);
//	       btnAddPayments.setRendered(false);
//	       startDateCalendar.setDisabled(true);
//	       endDateCalendar.setDisabled(true);
//	       issueDateCalendar.setDisabled(true);
//	       btnAddUnit.setRendered(false);
//	       btnAddAuctionUnit.setRendered(false);
//	       imgTenant.setRendered(false);
//	       
//		   btnSendNewLeaseForApproval.setRendered(false);
//		   btnOccuipers.setRendered(false); 
//		   btnPartners.setRendered(false);
//		   btnSaveNewLease.setRendered(false);
//		   btnSaveAmendLease.setRendered(false);
//		   
//		  // txtRefNumber.setRendered(true);
//		   //lblRefNum.setRendered(true);
//		   btnGenPayments.setRendered(false);
//		   btnCompleteAmendLease.setRendered(false);
//		   btnSubmitAmendLease.setRendered(false);
//		   imgManager.setRendered(false);
//		   imgSponsor.setRendered(false);
//		   btnApprove.setRendered(false); 
//		   btnReject.setRendered(false);
//		   btnApproveAmendLease.setRendered(false); 
//		   btnRejectAmendLease.setRendered(false);
//		   chkCommercial.setDisabled(true);
//		   chkPaidFacilities.setDisabled(true);
//		   
//	  
//	   
//   }
//   private void PageModeAdd()
//   {
//	   taskPanel.setRendered(false);
//	   txt_ActualRent.setReadonly(false);
//       btnPartnerDelete.setRendered(true);
//       btnOccupierDelete.setRendered(true);
//       
//	   cmbContractType.setDisabled(false);
//	   btnAddPayments.setRendered(true);
//       startDateCalendar.setDisabled(false);
//       endDateCalendar.setDisabled(false);
//       issueDateCalendar.setDisabled(false);
//       btnAddUnit.setRendered(true);
//       btnAddAuctionUnit.setRendered(true);
//       imgTenant.setRendered(true);
//       btnCollectPayment.setRendered(false);
//	   btnGenPayments.setRendered(true);
//	   btnOccuipers.setRendered(true); 
//	   btnPartners.setRendered(true);
//	   btnSaveNewLease.setRendered(true);
//	   btnSaveAmendLease.setRendered(false);
//	   btnCompleteAmendLease.setRendered(false);
//	   btnSubmitAmendLease.setRendered(false);
//	   btnApprove.setRendered(false); 
//	   btnReject.setRendered(false);
//	   btnApproveAmendLease.setRendered(false); 
//	   btnRejectAmendLease.setRendered(false);
//	   chkCommercial.setDisabled(false);
//	   viewRootMap.put("canAddAttachment", true); 
//       viewRootMap.put("canAddNote", true);
//    
//	   
//	   //chkPaidFacilities.setDisabled(false);
//	  // txtRefNumber.setRendered(false);
//	  // lblRefNum.setRendered(false);
//	   if(sessionMap.containsKey("UNITINFO") ||sessionMap.containsKey("AUCTIONUNITINFO"))
//	   {
////		   tabFacilities.setRendered(true);
//		   tabPaymentTerms.setRendered(true);
//		   //tabPaidFacility_Click();
//	   }
//	   else{
////		   tabFacilities.setRendered(false);
//		   tabPaymentTerms.setRendered(false);
//	   }
//
//	   //if contract has been saved and contract status is new 
//	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
//		   btnSendNewLeaseForApproval.setRendered(true);
//	   else
//		   btnSendNewLeaseForApproval.setRendered(false);
//	   
//	   //if request has been saved and request status is new 
//	   if(contractId != null && contractId.trim().length()>0 && getIsContractStatusNew())
//		   btnSendNewLeaseForApproval.setRendered(true);
//	   else
//		   btnSendNewLeaseForApproval.setRendered(false);
//   }
//    public boolean getIsCompanyTenant()
//    {
//    	String method="getIsCompanyTenant";
//    	isCompanyTenant=false;
//    	logger.logDebug(method+"|"+"Start...");
//		if(viewRootMap.containsKey(TENANT_INFO))
//	    {
//			PersonView tenantView=(PersonView)viewRootMap.get(TENANT_INFO);
//		    if(tenantView.getIsCompany().compareTo(new Long(1))==0)
//		    	return true;
//		    else
//		    	return false;
//	    }
//		logger.logDebug(method+"|"+"Finish...");
//		return isCompanyTenant;
//    }
//    public void RemoveSessions()
//    {
//    	
//        //Remove session bound with Tabs Start
//    	   
//       	     if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
//	         sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//	         
////	         if(viewRootMap.containsKey(WebConstants.CONTRACT_USER_TASK_LIST));
////	         viewRootMap.remove(WebConstants.CONTRACT_USER_TASK_LIST);
//	         //Remove session bound with Tabs Finish
//	     
//	         if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
//	        	 sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
//	         
//	         if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
//	        	 sessionMap.remove(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
//	         
//	         if(sessionMap.containsKey("UNITINFO"))
//	        	 sessionMap.remove("UNITINFO");
//	         
//	         if(sessionMap.containsKey(TENANT_INFO))
//	        	 sessionMap.remove(TENANT_INFO);
//	         
//	         if(sessionMap.containsKey("AUCTIONUNITINFO"))
//	        	 sessionMap.remove("AUCTIONUNITINFO");
//	         
//	        
//    	
//    }
//    public boolean getIsPageUpdateStatusNew()
//    {
//    	String method="getIsPageUpdateStatusNew";
//    	isPageUpdateStatusNew=false;
//    	logger.logDebug(method+"|"+"Start...");
//    	logger.logDebug(method+"|"+"PAGE MODE..."+this.getPageMode());
//    	logger.logDebug(method+"|"+"Contract View Status..."+contractView.getStatus());
//	    	if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
//	    	{
//	    		  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	        	  logger.logInfo(method+"|"+"Contract Status Approved..."+contractView.getStatus());
//	    	      if(contractView.getStatus()!=null && contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_NEW)) )
//	    	      {
//	    	    		isPageUpdateStatusNew=true;
//	    	    		
//	    	    	}
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageUpdateStatusNew;
//    }
//    public boolean getIsPageModeAdd()
//    {
//    	String method="getIsPageModeAdd";
//    	boolean isPageModeAdd=false;
//    	logger.logDebug(method+"|"+"Start...");
//    	logger.logDebug(method+"|"+"PAGE MODE..."+this.getPageMode());
//	    	if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_ADD) )
//	    	{
//	    		isPageModeAdd=true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeAdd;
//    }
//    
//    public boolean getIsPageModeOnSiteVisit()
//    {
//    	String method="getIsPageModeOnSiteVisit";
//    	boolean isPageModeOnSiteVisit = false;
//    	logger.logDebug(method + "|"+"Start...");
//    	logger.logDebug(method + "|"+"PAGE MODE..." + this.getPageMode());
//	    	if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_SITE_VISIT) )
//	    	{
//	    		isPageModeOnSiteVisit=true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeOnSiteVisit;
//    }
//    
//    public boolean getIsPageModeOnFollowUp()
//    {
//    	String method = "getIsPageModeOnFollowUp";
//    	boolean isPageModeOnFollowUp = false;
//    	logger.logDebug(method + "|" + "Start...");
//    	logger.logDebug(method + "|" + "PAGE MODE..." + this.getPageMode());
//	    	if( this.getPageMode() != null && this.getPageMode().equals( PAGE_MODE_FOLLOW_UP ) )
//	    	{
//	    		isPageModeOnFollowUp=true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeOnFollowUp;
//    }
//    
//    public boolean getIsPageModeApprovalRequired()
//    {
//    	String method = "getIsPageModeApprovalRequired";
//    	boolean isPageModeApprovalRequired = false;
//    	logger.logDebug(method + "|" + "Start...");
//    	logger.logDebug(method + "|" + "PAGE MODE..." + this.getPageMode());
//	    	if( this.getPageMode() != null && this.getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED ) )
//	    	{
//	    		isPageModeApprovalRequired = true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeApprovalRequired;
//    }
//    
//    public boolean getIsPageModeCompleted()
//    {
//    	String method = "getIsPageModeCompleted";
//    	boolean isPageModeCompleted = false;
//    	logger.logDebug(method + "|" + "Start...");
//    	logger.logDebug(method + "|" + "PAGE MODE..." + this.getPageMode());
//	    	if( this.getPageMode() != null && this.getPageMode().equals( PAGE_MODE_COMPLETED ) )
//	    	{
//	    		isPageModeCompleted = true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeCompleted;
//    }
//    
//    public boolean getIsPageModeAmendAdd()
//    {
//    	String method="getIsPageModeAmendAdd";
//    	isPageModeAmendAdd=false;
//    	logger.logDebug(method+"|"+"Start...");
//    	logger.logDebug(method+"|"+"PAGE MODE..."+this.getPageMode());
//	    	if(this.getPageMode()!=null && this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) )
//	    	{
//	    		isPageModeAmendAdd=true;
//	    		
//	    	}
//    	
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isPageModeAmendAdd;
//    }
//    public boolean getIsPageModeUpdate()
//    {
//    	String method="getIsPageModeUpdate";
//    	isPageModeUpdate=false;
//    	logger.logInfo(method+"|"+"Start...");
//    	logger.logInfo(method+"|"+"PAGE MODE..."+this.getPageMode());
//    	//For both update and approve the view should remain same except for APPROVE/REJECT buttons
//	    	if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_UPDATE)  || this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT) )
//	    	{
//	    		isPageModeUpdate=true;
//	    		
//	    	}
//    	
//    	logger.logInfo(method+"|"+"Finish...");
//    	return isPageModeUpdate;
//    }
//    private List<DomainDataView> getDomainDataListForDomainType(String domainType)
//    {
//    	String method="getDomainDataListForDomainType";
//    	logger.logInfo(method+"|"+"Start...");
//    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
//    	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
//		Iterator<DomainTypeView> itr = list.iterator();
//		//Iterates for all the domain Types
//		while( itr.hasNext() ) 
//		{
//			DomainTypeView dtv = itr.next();
//			if( dtv.getTypeName().compareTo(domainType)==0 ) 
//			{
//				Set<DomainDataView> dd = dtv.getDomainDatas();
//				//Iterates over all the domain datas
//				for (DomainDataView ddv : dd) 
//				{
//				  ddvList.add(ddv);	
//				}
//				break;
//			}
//		}
//		logger.logInfo(method+"|"+"Finsih...");
//    	return ddvList;
//    }
//    public boolean getIsContractStatusApproved()
//    {
//    	String method="getIsContractStatusApproved";
//    	
//    	logger.logDebug(method+"|"+"Start...");
//    	isContractStatusApproved=false;
//    	if(this.getPageMode().equals(PAGE_MODE_UPDATE) && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
//    	{
//    	  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	  
//    	  logger.logInfo(method+"|"+"Contract Status Approved..."+contractView.getStatus());
//	    	if( contractView.getStatus()!=null && 
//	    		( 
//	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED))||
//	    				contractView.getStatus().toString().equals(getIdForStatus(WebConstants.CONTRACT_STATUS_RENEW_APPROVED))
//	    		)
//	    	  )
//	    	{
//	    		isContractStatusApproved=true;
//	    		
//	    	}
//    	}
//	    logger.logDebug(method+"|"+"IscontractStatusApproved..."+isContractStatusApproved);
//    	logger.logDebug(method+"|"+"Finish...");
//    	return isContractStatusApproved;
//    }
//    private String getIdForStatus(String statusName)
//    {
//         String method="getIdForStatus";	
//         Long ID=new Long(0);
//         logger.logInfo(method+"|"+"Start...");
//         
//         logger.logInfo(method+"|"+"Status Name..."+statusName);
//    	       List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
//    	       for(int i=0;i<ddvList.size();i++)
//    	       {
//    	    	   
//    	    	   	DomainDataView ddv=(DomainDataView)ddvList.get(i);
//    	    	   if(ddv.getDataValue().equals(statusName))
//    	    	   {
//    	    		   
//    	    		   ID=ddv.getDomainDataId();
//    	    		   logger.logInfo(method+"|"+"Status Matched with id..."+ID);
//    	    	        break;
//    	    	   }
//    	       }
//    	 logger.logInfo(method+"|"+"Finish...");
//    	return ID.toString();
//    	
//    	
//    }
//    public boolean getIsUnitCommercial()
//    {
//    	String method="getIsUnitCommercial";
//    	
//    	logger.logDebug(method+"|"+"Start...");
//    	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
//    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
//		{
//    	  ddvList=(ArrayList<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE);
//		}
//    	
//    	if(sessionMap.containsKey("UNITINFO") )
//	    {
//			isUnitCommercial=false;
//			    UnitView unitView=(UnitView)sessionMap.get("UNITINFO");
//				HashMap hMap= getIdFromType(ddvList,WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL);
//			    logger.logDebug(method+"|"+"The usuageType of unit..."+unitView.getUsageTypeId().toString());
//			    if(hMap.containsKey("returnId"))
//			    {
//					logger.logDebug(method+"|"+"Domain Data id for type.."+WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL+" is.."+hMap.get("returnId").toString());
//				    if(unitView.getUsageTypeId().toString().equals(hMap.get("returnId").toString()))
//				    	isUnitCommercial=true;
//				    else 
//				    {
//				    	isUnitCommercial=false;
//				    	if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
//		  		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);	
//				    }
//			    }
//		}
//		else if (sessionMap.containsKey("AUCTIONUNITINFO"))
//	    {
//			isUnitCommercial=false;
//			AuctionUnitView auctionUnitView=(AuctionUnitView)sessionMap.get("AUCTIONUNITINFO");
//			HashMap hMap= getIdFromType(ddvList,WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL);
//		    logger.logDebug(method+"|"+"The usuageType of unit..."+auctionUnitView.getUnitUsageTypeID().toString());
//		    if(hMap.containsKey("returnId"))
//		    {
//				logger.logDebug(method+"|"+"Domain Data id for type.."+WebConstants.UNIT_USUAGE_TYPE_COMMERCIAL+" is.."+hMap.get("returnId").toString());
//			    if(auctionUnitView.getUnitUsageTypeID().toString().equals(hMap.get("returnId").toString()))
//			    	isUnitCommercial=true;
//			    else 
//			    {
//			    	isUnitCommercial=false;
//			    	if(sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER));
//	  		         sessionMap.remove(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_PARTNER);	
//			    }
//		    }
//		
//				
//				 
//			
//	    }
//		logger.logDebug(method+"|"+"Finish...");
//		return isUnitCommercial;
//    }
//    
//    public String getDateFormat()
//	{
//    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
//    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
//    	dateFormat= localeInfo.getDateFormat();
//		return dateFormat;
//	}
//    public String getLocale(){
//		LocaleInfo localeInfo = getLocaleInfo();
//		return localeInfo.getLanguageCode();
//	}
//    public boolean getIsArabicLocale()
//	{
//       String method="getIsArabicLocale";
//    	
//       logger.logDebug(method+"|"+"Start...");
//    	
//		isArabicLocale = !getIsEnglishLocale();
//		logger.logDebug(method+"|"+"Finish...");
//		return isArabicLocale;
//	}
//	public boolean getIsEnglishLocale()
//	{
//                String method="getIsEnglishLocale";
//                logger.logDebug(method+"|"+"Start...");
//		    
//				LocaleInfo localeInfo = getLocaleInfo();
//				logger.logDebug(method+"|"+"localeInfo..."+localeInfo.getLanguageCode());
//				isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
//				logger.logDebug(method+"|"+"Finish...");
//		return isEnglishLocale;
//	}
//	public LocaleInfo getLocaleInfo()
//	{
//		 String method="getLocaleInfo";
//         logger.logDebug(method+"|"+"Start...");
//	    	
//    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
//		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
//	     logger.logDebug(method+"|"+"Finish...");
//	     return localeInfo;
//		
//	}
//	private void loadTypes() throws PimsBusinessException
//	{
//		   String method="loadTypes";
//		   logger.logInfo(method+"|"+"Start..");
//		   PropertyServiceAgent psa =new PropertyServiceAgent();
//		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE))
//		   {		   
//			  logger.logInfo(method+"|"+"Getting types of unit from application context with type name:"+WebConstants.UNIT_USAGE_TYPE);
//			   //List<DomainDataView> ddl=commonUtil.getDomainDataListForDomainType(WebConstants.PROPERTY_USAGE);
//			   //List<DomainDataView> ddl=psa.getDomainDataByDomainTypeName(WebConstants.PROPERTY_USAGE);
//			   
//			  
//			   viewRootMap.put(WebConstants.SESSION_CONTRACT_UNIT_USUAGE_TYPE, commonUtil.getDomainDataListForDomainType(WebConstants.UNIT_USAGE_TYPE));
//		   }
//		   if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_TENANT_TYPE))
//		   {		   
//			   logger.logInfo(method+"|"+"Getting types of tenant from application type with type type:"+WebConstants.TENANT_TYPE);
//			   
//			   List<DomainDataView> ddl=commonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
//				   //psa.getDomainDataByDomainTypeName(WebConstants.TENANT_TYPE);
//			   viewRootMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE, ddl);
//			   DomainDataView ddvCompany= commonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_COMPANY);
//			   viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY,ddvCompany);
//			   DomainDataView ddvIndividual= commonUtil.getIdFromType(ddl, WebConstants.TENANT_TYPE_INDIVIDUAL);
//			   viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL,ddvIndividual);
//			   
//		   }
//		  
//		   if(!viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
//		   {		   
//			   logger.logInfo(method+"|"+"Getting types of persons from application context with type name:"+WebConstants.PERSON_TYPE);
//			   viewRootMap.put(WebConstants.SESSION_PERSON_TYPE, commonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE));
//			 
//		   }
//		   if(!viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
//		   {		   
//			   logger.logInfo(method+"|"+"Getting types of request from application context with type name:"+WebConstants.REQUEST_STATUS);
//			   viewRootMap.put(WebConstants.SESSION_REQUEST_STATUS, commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS));
//			   
//			   
//		   }
//		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CASH))
//		   {		   
//			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cash");
//			   List<DomainDataView> ddl=commonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
//			   DomainDataView ddvCash= commonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
//			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CASH,ddvCash);
//			   
//		   }
//		   if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE))
//		   {		   
//			   logger.logInfo(method+"|"+"Getting domain data view for payment schedule mode cheque");
//			   List<DomainDataView> ddl=commonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_MODE);
//			   DomainDataView ddvCheque= commonUtil.getIdFromType(ddl, WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
//			   viewRootMap.put(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE,ddvCheque);
//			   
//		   }
//		       loadPaymentStatus();
//		      
//					   logger.logInfo(method+"|"+"Finish..");
//	}
//	private void loadPaymentStatus()
//	{
//	  String methodName="loadPaymentStatus";
//	  logger.logInfo(methodName +"|"+"Start ");
//	  try
//	  {
//		  PropertyServiceAgent psa =new PropertyServiceAgent();
//		  if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
//		  {
//			  List<DomainDataView>ddv=commonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS); 
//		    //List<DomainDataView>ddv= (ArrayList)psa.getDomainDataByDomainTypeName(WebConstants.PAYMENT_SCHEDULE_STATUS);
//		    viewRootMap.put(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS, ddv);
//		  }
//	  }
//	  catch (Exception e)
//	  {
//		logger.logError(methodName +"|"+"Exception Occured "+e);
//	  }
//	
//	  logger.logInfo(methodName +"|"+"Finish ");
//	
//	
//	
//	}
//	
//	private void getRequestById(String requestId)throws Exception,PimsBusinessException
//	{
//			String methodName="getContractById";
//			
//			ArrayList<RequestView> list;
//			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
//			try
//			{
//				RequestView requestView=new RequestView();
//				requestView.setRequestId(new Long(requestId));
//				
////				HashMap contractHashMap=new HashMap();
////				contractHashMap.put("contractView",contract);
//				
////				contractHashMap.put("dateFormat",getDateFormat());
//				PropertyServiceAgent psa =new PropertyServiceAgent();
//				logger.logInfo(methodName+"|"+"Querying db for contractId"+requestId);
//				
//				requestView= psa.getRequestById(new Long(requestId));
//				
//				this.setRequestView(requestView);
//				
////				viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
//				
//				//not in amend mode the request should be filled 
//				
////				if(!getIsPageInAmendMode())
////					getRequestForNewLeaseContract();
//				
//			
//			}
//			catch(Exception ex)
//			{
//				logger.LogException(methodName+"|"+"Error occured:",ex);
//				throw ex;
//				
//			}
//	}
//	
//	private void getContractById(String contractId)throws Exception,PimsBusinessException
//	{
//			String methodName="getContractById";
//			
//			ArrayList<ContractView> list;
//			logger.logInfo(methodName+"|"+"Contract with id :"+contractId+" present so page is in Update Mode");
//			try
//			{
//				ContractView contract=new ContractView();
//				contract.setContractId(new Long(contractId));
//				
//				HashMap contractHashMap=new HashMap();
//				contractHashMap.put("contractView",contract);
//				
//				contractHashMap.put("dateFormat",getDateFormat());
//				PropertyServiceAgent psa =new PropertyServiceAgent();
//				logger.logInfo(methodName+"|"+"Querying db for contractId"+contractId);
//				List<ContractView>contractViewList= psa.getAllContracts(contractHashMap);
//				
//				logger.logInfo(methodName+"|"+"Got "+contractViewList.size()+" row from db ");
//				if(contractViewList.size()>0)
//				contractView=(ContractView)contractViewList.get(0);
//				viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
//				//not in amend mode the request should be filled 
//				if(!getIsPageInAmendMode())
//				getRequestForNewLeaseContract();
//				
//			
//			}
//			catch(Exception ex)
//			{
//				logger.LogException(methodName+"|"+"Error occured:",ex);
//				throw ex;
//				
//			}
//	}
//
//	private boolean getIsPageInAmendMode()
//	{
//		boolean isPageInAmendMode=false;
//		if(viewRootMap.containsKey("pageMode") && viewRootMap.get("pageMode")!=null) 
//			this.setPageMode(viewRootMap.get("pageMode").toString() );
//		if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)||
//				this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)||
//				this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_APPROVE_REJECT)||
//				this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_VIEW)
//		)
//			isPageInAmendMode=true;
//		
//		return isPageInAmendMode;
//	}
//	private void getRequestForNewLeaseContract()
//			throws PimsBusinessException {
////		PropertyServiceAgent psa = new PropertyServiceAgent();
////		RequestView rv = new RequestView();
////		rv.setRequestTypeId(WebConstants.REQUEST_TYPE_LEASE_CONTRACT);
////		ContractView cv= new ContractView();
////		cv.setContractId(contractView.getContractId());
////		cv.setContractNumber(contractView.getContractNumber());
////		rv.setContractView(cv);
//		
////		List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
////		if(requestViewList.size()>0)
////		{
////			requestView =requestViewList.get(0); 
//			viewRootMap.put("REQUESTVIEWUPDATEMODE",requestView );
//			populateApplicationDetailsTab(); 
////		}
//	}
//    private void loadCombos()throws PimsBusinessException
//    {
//    	String methodName="loadCombos";
//    	logger.logInfo(methodName+"|"+" Start");
//    	loadStatus();
//    	logger.logInfo(methodName+"|"+" Finish");
//    	
//    }
//    private void loadStatus() throws PimsBusinessException
//    {
////    	String method="loadStatus";
////    	logger.logInfo(method+"|"+" Start");
////    	PropertyServiceAgent psa=new PropertyServiceAgent();
////    	
////       if(!viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_STATUS))
////   	   {	
////    	   CommonUtil commonUtil=new CommonUtil();
////   		    List<DomainDataView> ddl=commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
////   		   
////   		   //List<DomainDataView> ddl=psa.getDomainDataByDomainTypeName(WebConstants.CONTRACT_STATUS);
////   		   logger.logDebug(method+"|"+"Got "+ddl.size()+" rows for Contract status..");
////   		   logger.logDebug(method+"|"+"Putting contract status list in session with key...."+WebConstants.SESSION_CONTRACT_STATUS);
////   		    viewRootMap.put(WebConstants.SESSION_CONTRACT_STATUS, ddl);
////   	   }
////    	logger.logInfo(method+"|"+" Finish");
//    	
//    }
//    public String btnBack_Click()
//    {
//    	String eventOutcome = "back";
//    	return eventOutcome;
//    }
//    public String btnCancel_Click()
//    {
//    	String eventOutcome = "cancel";
//    	return eventOutcome;
//    }
//    public String btnApprove_Click()
//    {
//    	String methodName = "btnApprove_Click";
//        errorMessages=new ArrayList<String>(0);
//        successMessages=new ArrayList<String>(0);
//        errorMessages.clear();
//        successMessages.clear();
//    	logger.logInfo(methodName+"|"+" Start...");
//    	RequestServiceAgent rsa = new RequestServiceAgent();
//    	try
//    	{
//    	   if(this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT))
//		   {
//    		   generateNotification(WebConstants.Notification_MetaEvents.Event_LeaseContract_Approval); 
//    		   
//    		   ApproveRejectTask(TaskOutcome.APPROVE);
//    		   if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
//			   {
//			     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
//			     if(contractId !=null && contractId.trim().length()>0)
//			     rsa.changeRequestDetailStatusForAuction(rdvList , new Long(contractId),WebConstants.MovedToContract.BINDED_WITH_CONTRACT);
//				}
//    		   if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
//   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//   	   	        getRequestForNewLeaseContract();//updateUnitStatusToRented();
//   	   	        logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode start...");
// 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED);
// 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
// 		      	if(approvedStatus!=null)
// 		      	   contractView.setStatus( new Long(approvedStatus));
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode Finish...");   
// 		      	
// 		      	if(requestView!=null  && requestView.getRequestId()!=null)
// 		      	{
// 		      		logger.logInfo(methodName+"|"+" Saving Notes...Start");
// 		      		saveComments(requestView.getRequestId());
//				    logger.logInfo(methodName+"|"+" Saving Notes...Finish");
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Start");
//				    saveAttachments(requestView.getRequestId().toString() );
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
// 		      	}
// 		      	this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
////			    viewRootMap.put("pageMode",pageMode);
//			    saveSystemComments(MessageConstants.ContractEvents.CONTRACT_APPROVED);
// 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_APPROVED_SUCCESSFULLY));
//		  }
//    	  logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(PIMSWorkListException ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));	
//    		logger.logError(methodName+"|"+" Error Occured..."+ex);
//    	
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.logError(methodName+"|"+" Error Occured..."+ex);
//    		
//    	}
//    	return "";
//    }
//    
////        private void updateUnitStatusToRented()throws PimsBusinessException
////    {
//// 	    String methodName = "updateUnitStatusToRented";
////    	logger.logInfo(methodName+"|"+" Start...");
////    	UnitView unitView=null;
////    	logger.logInfo(methodName+"|"+" if unit info is in session...");
////	    	if(sessionMap.containsKey("UNITINFO"))
////	    	{
////	    		logger.logInfo(methodName+"|"+" Yes unit info is in session...");
////	    		unitView= (UnitView)sessionMap.get("UNITINFO");
////		    	HashMap hMap= getIdFromType( getDomainDataListForDomainType(WebConstants.UNIT_STATUS),
////		    		       WebConstants.UNIT_STATUS_RENTED
////		                 );
////			    if(hMap.containsKey("returnId"));
////			    {
////			    	PropertyServiceAgent psa=new PropertyServiceAgent();
////			    	psa.changeUnitStatus(unitView.getUnitId(), new Long(hMap.get("returnId").toString()));
////			    }
////	    	}
////		    logger.logInfo(methodName+"|"+" Finish...");
////    }
//    
//    public String btnReject_Click()
//    {
//    	String methodName = "btnReject_Click";
//        
//    	logger.logInfo(methodName+"|"+" Start...");
//    	errorMessages=new ArrayList<String>(0);
//        errorMessages.clear();
//        successMessages=new ArrayList<String>(0);
//        successMessages.clear();
//    	RequestServiceAgent rsa = new RequestServiceAgent();
//    	try
//    	{
//    	 if(this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT))
//		 {
//		    	ApproveRejectTask(TaskOutcome.REJECT);
//		    	if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
//				{
//				 List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
//				 
//				 rsa.changeRequestDetailStatusForAuction(rdvList , null,WebConstants.MovedToContract.MOVED_TO_CONTRACT);
//				 //rsa.changeRequestDetailStatusForAuction(rdvList , null,WebConstants.MovedToContract.MOVED_TO_CONTRACT);
//				}
//		    	if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
//	   	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	   	   	    getRequestForNewLeaseContract();
//	   	   	    if(requestView!=null  && requestView.getRequestId()!=null)
//		      	{
//		      		logger.logInfo(methodName+"|"+" Saving Notes...Start");
//		      		saveComments(requestView.getRequestId());
//				    logger.logInfo(methodName+"|"+" Saving Notes...Finish");
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Start");
//				    saveAttachments(requestView.getRequestId().toString() );
//				    logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
//		      	}
//	   	   	this.setPageMode(WebConstants.LEASE_CONTRACT_PAGE_MODE_VIEW);
////			    viewRootMap.put("pageMode",pageMode);
//			    saveSystemComments(MessageConstants.ContractEvents.CONTRACT_REJECTED);
//			    successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_REJECED_SUCCESSFULLY));
//			    logger.logInfo(methodName+"|"+" Finish...");
//		    	
//		 }
//    	}
//    	catch(PIMSWorkListException ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    	
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);	
//    	}
//    	
//    	
//    	return "";
//    }
//
//    private void removeUnPaidRentFromPaymentScheduleSession()
//    {
//    	String methodName="removeUnPaidRentFromPaymentScheduleSession";
//    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
//    	List<PaymentScheduleView> psvList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//    	
//    	for (PaymentScheduleView psv : psvList) 
//    	{
//		   //If payment is for Rent
//    		if(psv.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0 	)
//			{
//    			
//    			//if Rent is paid
//				if(!psv.getIsReceived().equals("N") )
//					newPaymentScheduleViewList.add(psv);
//			}
//    		//If payment other than rent
//			else
//        	  	newPaymentScheduleViewList.add(psv);
//        
//    	}
//    	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
//    
//    }
//    public String openReceivePaymentsPopUp()
//	{
//		String methodName="openReceivePaymentsPopUp";
//		logger.logInfo(methodName+"|"+"Start..");
//		        FacesContext facesContext = FacesContext.getCurrentInstance();
//		        if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
//		    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null		
//		    	 )
//		    	{
//    		        List<PaymentScheduleView> paymentSchedules = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//    		        List<PaymentScheduleView> paymentsForCollection = new ArrayList<PaymentScheduleView>();
//    				for (int i=0; i<paymentSchedules.size(); i++)
//    				{
//    					if (paymentSchedules.get(i).getSelected())
//    						paymentsForCollection.add(paymentSchedules.get(i));
//    				}
//    				sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION,paymentsForCollection);
//		    	}
//		        String extrajavaScriptText ="var screen_width = screen.width;"+
//                "var screen_height = screen.height;"+
//
//                "window.open('receivePayment.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-320)+',left=0,top=10,scrollbars=no,status=no');";
//
//		        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
////		        paymentReceiptView.setAmount(paymentScheduleViewRow.getAmount());
////		        paymentReceiptView.setModeId(paymentScheduleViewRow.getPaymentModeId());
////		        paymentReceiptView.setPaymentScheduleView(paymentScheduleViewRow);
//		        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
//		        openPopUp("receivePayment.jsf", extrajavaScriptText);
//    	   
//		logger.logInfo(methodName+"|"+"Finish..");
//    	
//		return "";
//	}
//    public void btnGenPayments_Click()
//    {
//    	String methodName = "btnGenPayments_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	PropertyServiceAgent psa =new PropertyServiceAgent();
//    	List<PaymentScheduleView> alreadyPresentPS=new ArrayList<PaymentScheduleView>();
//    	try
//    	{
//    		if(txttotalContractValue!=null && txttotalContractValue.length()>0)
//    		{
//
//    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//    		    {
//    				removeUnPaidRentFromPaymentScheduleSession();
//    		     alreadyPresentPS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,alreadyPresentPS);
//    		    }
//    			 String extrajavaScriptText ="var screen_width = screen.width;"+
//                 "var screen_height = screen.height;"+
//                 "window.open('GeneratePayments.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height-300)+',left=100,top=150,scrollbars=yes,status=yes');";
//
// 		        
// 		        //sessionMap.put(WebConstants.DEPOSIT_AMOUNT,cash);
// 				sessionMap.put(WebConstants.TOTAL_CONTRACT_VALUE,txttotalContractValue);
// 				
// 				
// 		        openPopUp("GeneratePayments.jsf", extrajavaScriptText);
//    		}
//    		else
//    			logger.logInfo(methodName+"|"+" Total Contract Value is not present...");
//    		logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
//    	}
//    }
//    public void btnEditPaymentSchedule_Click()
//    {
//    	String methodName = "btnEditPaymentSchedule_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	PaymentScheduleView psv = (PaymentScheduleView)paymentScheduleDataTable.getRowData();
//    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
// 			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null)
//    		sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE));
//    	sessionMap.put(WebConstants.PAYMENT_SCHDULE_EDIT,psv);
//    	
//    	String extrajavaScriptText ="var screen_width = screen.width;"+
//         "var screen_height = screen.height;"+
//        "window.open('EditPaymentSchedule.jsf','_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');";
//        openPopUp("EditPaymentSchedule.jsf", extrajavaScriptText);
//
// 
//    	logger.logInfo(methodName+"|"+" Finish...");
//    }
//    public void btnAddPayments_Click()
//    {
//    	String methodName = "btnAddPayments_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	PropertyServiceAgent psa =new PropertyServiceAgent();
//    	List<PaymentScheduleView> PS=new ArrayList<PaymentScheduleView>();
//    	String psStatusInReq="";
//    	try
//    	{
//    			if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) &&
//    			   viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null		
//    			  )
//    		    {
//    		     PS = (ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//    		     sessionMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,PS);
//    		    }
//    			if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
//    			{
//    			   contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    			   DomainDataView ddv = commonUtil.getDomainDataFromId((List<DomainDataView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS), 
//	                       contractView.getStatus());
//    			   if(ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_NEW && ddv.getDataValue()!=WebConstants.CONTRACT_STATUS_DRAFT )
//    			   {
//    				    setRequestParam(WebConstants.PAYMENT_STATUS_IN_REQUEST,WebConstants.PAYMENT_SCHEDULE_PENDING);
//    				    psStatusInReq = WebConstants.PAYMENT_SCHEDULE_PENDING;
//    			   }
//    			
//    			}
//    			 String extrajavaScriptText ="var screen_width = screen.width;"+
//                 "var screen_height = screen.height;"+
//                 "window.open('PaymentSchedule.jsf?totalContractValue="+txttotalContractValue+"&"+WebConstants.PAYMENT_STATUS_IN_REQUEST+"="+psStatusInReq+
//                 		"','_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');";
// 		        openPopUp("PaymentSchedule.jsf", extrajavaScriptText);
//
//    		logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+" Error Occured::",ex);	
//    	}
//    }
//    public Long getPaymentSchedulePendingStausId()
//    {
//    	DomainDataView ddv = new DomainDataView();
//    	if(!viewRootMap.containsKey(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS))
//    		loadPaymentStatus();
//    	if(!viewRootMap.containsKey(WebConstants.PAYMENT_SCHEDULE_PENDING) )
//    	{
//    	List<DomainDataView>ddvList= (ArrayList<DomainDataView>)viewRootMap.get(WebConstants.SESSION_PAYMENT_SCHEDULE_STATUS);
//    	 ddv=commonUtil.getIdFromType(ddvList, WebConstants.PAYMENT_SCHEDULE_PENDING);
//    	}
//    	else
//    	ddv = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_PENDING) ;
//    	return ddv.getDomainDataId();
//    	
//    }
//   
//    public void btnDelPaymentSchedule_Click()
//    {
//    	String methodName = "btnDelPaymentSchedule_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	try{
//    	PaymentScheduleView psv=(PaymentScheduleView)paymentScheduleDataTable.getRowData();
//    	if(this.getPageMode().equals(PAGE_MODE_ADD))
//    		removeFromPaymentScheduleSession(psv);
//    	else //if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
//    		markPaymentScheduleAsDeleted(psv);
//    	
//    	getPaymentScheduleListFromSession();
//    	//calculatePaymentScheduleSummaray();
//    	logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    		
//    	}
//    }
//    private void markPaymentScheduleAsDeleted(PaymentScheduleView psv)
//    {
//    	String methodName="markPaymentScheduleAsDeleted";
//    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
//    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
//      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
//        {
//            //If payment schedule existed in DB
//        	if(psv.getPaymentScheduleId()!=null )
//        	{
//	        	if(psv.getPaymentScheduleId().compareTo(paymentScheduleView.getPaymentScheduleId())==0 )
//	        	{
//	        		paymentScheduleView.setIsDeleted(new Long(1));
//	        
//	        	}
//	        	newPaymentScheduleViewList.add(paymentScheduleView);
//        	}
//        	//If payment schedule added during amend
//        	else
//        	{
//	        		if(psv.getPaymentDueOn().compareTo(paymentScheduleView.getPaymentDueOn())==0     
//	            		&&	psv.getTypeId().compareTo(paymentScheduleView.getTypeId())==0 
//			   	       )
//	        	      logger.logDebug(methodName+"|"+" ...");
//         	       else
//              		  newPaymentScheduleViewList.add(paymentScheduleView);	
//	        	
//        	        	
//            
//           }
//           }
//        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList);
//    
//    }
//    private void removeFromPaymentScheduleSession(PaymentScheduleView psv)
//    {
//    	String methodName="removeFromPaymentScheduleSession";
//    	List<PaymentScheduleView> paymentScheduleViewList =new ArrayList<PaymentScheduleView>();
//    	List<PaymentScheduleView> newPaymentScheduleViewList =new ArrayList<PaymentScheduleView>();
//      	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//      		paymentScheduleViewList=(ArrayList<PaymentScheduleView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//        for (PaymentScheduleView paymentScheduleView : paymentScheduleViewList) 
//        {
//        	//To uniquely identify the payment in add Mode
//        	//Combination of Payment Type and PaymentDueOn should be unique
//        	if(psv.getPaymentDueOn().compareTo(paymentScheduleView.getPaymentDueOn())==0     
//        		&&	psv.getTypeId().compareTo(paymentScheduleView.getTypeId())==0 
//        			   	)
//        		logger.logDebug(methodName+"|"+" ...");
//        	else
//              		newPaymentScheduleViewList .add(paymentScheduleView);
//        }
//      	viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,newPaymentScheduleViewList );
//    
//    }
//    private void setRequestFieldDetailView(
//			RequestFieldDetailView requestFieldDetailView ) throws PimsBusinessException {
//		
//		
//		requestFieldDetailView.setCreatedBy(requestView.getCreatedBy());
//		requestFieldDetailView.setCreatedOn(requestView.getCreatedOn());
//		requestFieldDetailView.setUpdatedBy(requestView.getUpdatedBy());
//		requestFieldDetailView.setUpdatedOn(requestView.getUpdatedOn());
//		requestFieldDetailView.setRecordStatus(requestView.getRecordStatus());
//		requestFieldDetailView.setIsDeleted(requestView.getIsDeleted());
//		requestFieldDetailView.setRequestKeyValue(contractId);
//		
//		RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);
//		requestFieldDetailView.setRequestKeyView(requestKeyView);
//		requestFieldDetailView.setRequestKeyViewId(requestKeyView.getRequestKeyId());
//	}
//   public Boolean saveComments(Long referenceId)
//    {
//		Boolean success = false;
//    	String methodName="saveComments";
//    	try{
//	    	logger.logInfo(methodName + "started...");
//	    	String notesOwner = WebConstants.REQUEST;
//	    	
//	    	NotesController.saveNotes(notesOwner, referenceId);
//	    	success = true;
//	    	logger.logInfo(methodName + "completed successfully!!!");
//    	}
//    	catch (Throwable throwable) {
//			logger.LogException(methodName + " crashed ", throwable);
//		}
//    	return success;
//    }
//	public Boolean saveAttachments(String referenceId)
//    {
//		Boolean success = false;
//    	try{
//	    	logger.logInfo("saveAtttachments started...");
//	    	if(referenceId!=null){
//		    	success = CommonUtil.updateDocuments();
//	    	}
//	    	logger.logInfo("saveAtttachments completed successfully!!!");
//    	}
//    	catch (Throwable throwable) {
//    		success = false;
//    		logger.LogException("saveAtttachments crashed ", throwable);
//		}
//    	
//    	return success;
//    }
//	public void loadAttachmentsAndComments(Long requestId){
//		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
//		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
//    	String externalId = WebConstants.Attachment.EXTERNAL_ID_NEW_LEASE_CONTRACT;
//    	
//    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
//		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
//		viewMap.put("noteowner", WebConstants.REQUEST);
//		
//		if(requestId!= null){
//			
//	    	String entityId = requestId.toString();
//			
//			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
//			viewMap.put("entityId", entityId);
//		}
//	}
//	public void btnSubmitAmendLease_Click()
//    {
//        String methodName = "btnSubmitAmendLease_Click";
//        SystemParameters parameters = SystemParameters.getInstance();
//	    String endPoint= parameters.getParameter(WebConstants.AMEND_LEASE_CONTRACT_BPEL_ENDPOINT);
//	    logger.logInfo(methodName+"|"+" Start...");
//    	errorMessages=new ArrayList<String>(0);
//    	try
//    	{
//    		PIMSAmendLeaseContractBPELPortClient port=new PIMSAmendLeaseContractBPELPortClient();
//		   	port.setEndpoint(endPoint);
//		   	logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
//		   	RequestView rv = (RequestView)viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW");
//		   	logger.logInfo(methodName+"|"+" RequestId..."+rv.getRequestId());
//		   	logger.logInfo(methodName+"|"+" ContractId..."+contractId);
//		   	logger.logInfo(methodName+"|"+" LoggedInUser..."+getLoggedInUser());
//		   	if(contractId!=null && contractId.length()>0 && rv.getRequestId()!=null)
//		   	{
//		   	       port.initiate(new Long(contractId), rv.getRequestId(),getLoggedInUser(),null, null);
//		   	       rv =new PropertyServiceAgent().getRequestById(rv.getRequestId());
//		   	       viewRootMap.put("AMEND_CONTRACT_REQUEST_VIEW",rv);
//		   	}
//		   	logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(Exception ex)
//     	{
//     		logger.LogException(methodName+"|"+" Error Occured...", ex);
//     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//     		
//     	}
//
//        
//        
//
//    }
//   public void removePaymentScheduleIds()
//   {
//		String methodName = "removePaymentScheduleIds";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	Set<PaymentScheduleView> newPsv=new HashSet<PaymentScheduleView>(0); 
//    	Set<PaymentScheduleView> psv =contractView.getPaymentScheduleView();
//    	Iterator iter=psv.iterator();
//    	while(iter.hasNext())
//    	{
//    		PaymentScheduleView  paymentScheduleView=(PaymentScheduleView)iter.next();
//    		if(paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
//    		{
//    			paymentScheduleView.setPaymentScheduleId(null);
//    			paymentScheduleView.setPaymentNumber(null);
//    			newPsv.add(paymentScheduleView);
//    		}
//    	}
//    	contractView.setPaymentScheduleView(newPsv);
//	   
//    	logger.logInfo(methodName+"|"+" Finish...");
//   }
//	public String btnSaveAmendLease_Click()
//    {
//    	String methodName = "btnSaveAmendLease_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//    	List<PaymentReceiptView> paymentReceiptViewList=new ArrayList<PaymentReceiptView>();
//     	
//     	try
//     	{
// 		    	PropertyServiceAgent myPort = new PropertyServiceAgent();
// 		    	    
// 				if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
// 			    {
// 					if(!putControlValuesInView())
// 			    	{
// 						logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
// 						removePaymentScheduleIds();
// 						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
// 						
// 						Long newContractId=contractView.getContractId();
// 				    	logger.logInfo(methodName+"|"+" Contract added with Id::"+newContractId);
// 		                this.contractId=newContractId.toString();
// 		                requestView.setContractView(contractView);
// 		                List<RequestView> rvList = new PropertyServiceAgent().getAllRequests(requestView, null, null, null);
// 		               if(rvList.size()>0)
// 		            	  viewRootMap.put("AMEND_CONTRACT_REQUEST_VIEW",rvList.get(0));
// 		                this.setPageMode( WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE );
////	         		    viewRootMap.put("pageMode", pageMode);
// 		                getAmendDataFromRequestView();
// 		                String successMessage="";
// 		                successMessage=ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_SAVED);
// 				 	    successMessages.add(successMessage);
// 	         		   
// 	         		   btnSubmitAmendLease.setRendered(true);
// 	         		   logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..COMPLETED "); 
// 	         		    
// 						getContractById(contractId.toString());
// 						logger.logInfo(methodName+"|"+" Reference Number:"+contractView.getContractNumber());
// 						this.setRefNum(contractView.getContractNumber());
//					}
// 			    }
// 				else
// 				{
// 					if(putUpdatedControlValuesInView())
// 					{
// 					    if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
// 						  paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);	
// 					    contractView.setPaymentScheduleView(getPaymentSchedule());
// 					    logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
// 						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
// 						Long contractId=contractView.getContractId();
// 						successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY));
// 				    	logger.logInfo(methodName+"|"+" Contract updated with Id::"+contractId);
// 					}
// 				}
// 				//For time being 
//                PageModeAmendUpdate();
//                btnSaveAmendLease.setRendered(false);
//                
// 				viewRootMap.put("pageMode",this.getPageMode());
// 			logger.logInfo(methodName+"|"+" Finish...");
//     	}
//     	catch(Exception ex)
//     	{
//     		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//     		logger.LogException(methodName+"|"+" Error Occured...", ex);
//     		
//     	}
//     	return "";
//    }
//	
//	  public void btnCompleteTask_Click()
//		{
//			String methodName="btnCompleteTask_Click";
//			logger.logInfo(methodName+"|"+"Start...");
//			errorMessages=new ArrayList<String>(0);
//	        errorMessages.clear();
//	        
//			try
//			{
//				ApproveRejectTask(TaskOutcome.OK);
//
//			    logger.logInfo(methodName+"|"+"Finish...");
//		    }
//			catch(Exception ex)
//	    	{
//				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//	    	}
//		}
//	  
//	  public void btnCompleteRequestTask_Click()
//		{
//			String methodName="btnCompleteRequestTask_Click";
//			logger.logInfo(methodName+"|"+"Start...");
//			errorMessages=new ArrayList<String>(0);
//	        errorMessages.clear();
//	        
//			try
//			{
//				
//				ApproveRejectTask(TaskOutcome.OK);
//
//			    logger.logInfo(methodName+"|"+"Finish...");
//		    }
//			catch(Exception ex)
//	    	{
//				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//	    	}
//		}
//	
//	  public void btnCompleteAmend_Click()
//		{
//			String methodName="btnCompleteAmend_Click";
//			logger.logInfo(methodName+"|"+"Start...");
//			errorMessages=new ArrayList<String>(0);
//	        errorMessages.clear();
//			try
//			{
//				
//				initiateBPELTask();
//				this.getBtnCompleteAmendLease().setRendered(false);
//				
////			    PropertyServiceAgent psa =new PropertyServiceAgent();
////		      	RequestView requestView=(RequestView)viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW");
////		      	Set<RequestFieldDetailView> setRequestFieldDetailsView=requestView.getRequestFieldDetailsView();
////		      	Iterator ITER= setRequestFieldDetailsView.iterator();
////		      	RequestFieldDetailView rfdv=new RequestFieldDetailView ();
////		      	
////		      	while(ITER.hasNext())
////		      	{
////		      		rfdv=(RequestFieldDetailView)ITER.next();
////		      		//rfdv.get
////		      		break;
////		      	}
////
////		      	contractView.setCreatedBy(getLoggedInUser());
////		      	contractView.setCreatedOn(new Date());
////		      	if(!putControlValuesInView())
////		      	{
////		      		contractView=contractView;
////		      		psa.transferContract(requestView.getRequestId(),new Long(rfdv.getRequestKeyValue()),contractView);
////		      	    ApproveRejectTask(TaskOutcome.COMPLETE);
////		      	  btnCompleteAmendLease.setRendered(false);
////			      	viewRootMap.put("pageMode","");
////		      	    successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_COMPLETED));
////		      	
////		      	}
////		      	
//				successMessages.add("Request has been completed and sent for approval.");
//			    logger.logInfo(methodName+"|"+"Finish...");
//		    }
//			catch(Exception ex)
//	    	{
//				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//	    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//	    	}
//		}
//	  
////    public void btnCompleteAmend_Click()
////	{
////		String methodName="btnCompleteAmend_Click";
////		logger.logInfo(methodName+"|"+"Start...");
////		errorMessages=new ArrayList<String>(0);
////        errorMessages.clear();
////		try
////		{
////		    PropertyServiceAgent psa =new PropertyServiceAgent();
////	      	RequestView requestView=(RequestView)viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW");
////	      	Set<RequestFieldDetailView> setRequestFieldDetailsView=requestView.getRequestFieldDetailsView();
////	      	Iterator ITER= setRequestFieldDetailsView.iterator();
////	      	RequestFieldDetailView rfdv=new RequestFieldDetailView ();
////	      	
////	      	while(ITER.hasNext())
////	      	{
////	      		rfdv=(RequestFieldDetailView)ITER.next();
////	      		//rfdv.get
////	      		break;
////	      	}
////
////	      	contractView.setCreatedBy(getLoggedInUser());
////	      	contractView.setCreatedOn(new Date());
////	      	if(!putControlValuesInView())
////	      	{
////	      		contractView=contractView;
////	      		psa.transferContract(requestView.getRequestId(),new Long(rfdv.getRequestKeyValue()),contractView);
////	      	    ApproveRejectTask(TaskOutcome.COMPLETE);
////	      	  btnCompleteAmendLease.setRendered(false);
////		      	viewRootMap.put("pageMode","");
////	      	    successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_COMPLETED));
////	      	
////	      	}
////	      	
////		    logger.logInfo(methodName+"|"+"Finish...");
////	    }
////		catch(Exception ex)
////    	{
////			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
////    		logger.LogException(methodName+"|"+" Error Occured...",ex);
////    	}
////	}    
//
//    public String btnApproveAmend_Click()
//    {
//    	String methodName = "btnApproveAmend_Click";
//        errorMessages=new ArrayList<String>(0);
//        errorMessages.clear();
//    	logger.logInfo(methodName+"|"+" Start...");
//    	RequestServiceAgent rsa=new RequestServiceAgent();
//    	try
//    	{    	   
//	    		if(this.getRequestId() != null && ( this.getSvContractId() != null)){
//	    			rsa.updateRequestContract( Long.valueOf(this.getRequestId()), this.getSvContractId() , getLoggedInUser());
//	    		}
//	    		else if( (this.getRequestView() != null && this.getRequestView().getContractId() != null ) ){
//					
//					rsa.updateRequestContract( Long.valueOf(this.getRequestId()), this.getRequestView().getContractId().toString() , getLoggedInUser());
//					
//				}
//   		        ApproveRejectTask(TaskOutcome.APPROVE);
//   				 
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode start...");
//// 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_APPROVED);
//// 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
//// 		      	if(approvedStatus!=null)
//// 		      	   contractView.setStatus( new Long(approvedStatus));
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be approved for view mode Finish...");   
// 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_APPROVED));
// 		      	btnApproveAmendLease.setRendered(false);
// 		      	btnRejectAmendLease.setRendered(false);
// 		      	
// 		      	viewRootMap.put("pageMode","");
//    	        logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(PIMSWorkListException exc)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",exc);
//    	
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    	}
//    	return "";
//    }
//    
//    public String btnSiteVisit_Click()
//    {
//    	String methodName = "btnRejectAmend_Click";
//        errorMessages=new ArrayList<String>(0);
//        errorMessages.clear();
//    	logger.logInfo(methodName+"|"+" Start...");
//    	RequestServiceAgent rsa=new RequestServiceAgent();
//    	try
//    	{    	   
//    		if(this.getRequestId() != null && ( this.getSvContractId() != null)){
//    			rsa.updateRequestContract( Long.valueOf(this.getRequestId()), this.getSvContractId() , getLoggedInUser());
//    		}
//    		else if( (this.getRequestView() != null && this.getRequestView().getContractId() != null ) ){
//				
//				rsa.updateRequestContract( Long.valueOf(this.getRequestId()), this.getRequestView().getContractId().toString() , getLoggedInUser());
//				
//			}
//   		        ApproveRejectTask(TaskOutcome.SEND_FOR_SITE_VISIT);
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode start...");
//// 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_REJECT);
//// 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
//// 		      	if(approvedStatus!=null)
//// 		      	   contractView.setStatus( new Long(approvedStatus));
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode Finish...");   
// 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_REJECTED));
// 		      	btnApproveAmendLease.setRendered(false);
// 		      	btnRejectAmendLease.setRendered(false);
// 		      	viewRootMap.put("pageMode","");
//    	        logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(PIMSWorkListException exc)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",exc);
//    	
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    	}
//    	return "";
//    }
//    public String btnRejectAmend_Click()
//    {
//    	String methodName = "btnRejectAmend_Click";
//        errorMessages=new ArrayList<String>(0);
//        errorMessages.clear();
//    	logger.logInfo(methodName+"|"+" Start...");
//    	RequestServiceAgent rsa=new RequestServiceAgent();
//    	try
//    	{    	   
//   		        ApproveRejectTask(TaskOutcome.REJECT);
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode start...");
//// 		      	String approvedStatus=getIdForStatus(WebConstants.CONTRACT_STATUS_REJECT);
//// 		      	logger.logDebug(methodName+"|"+" Approved Status...."+approvedStatus);
//// 		      	if(approvedStatus!=null)
//// 		      	   contractView.setStatus( new Long(approvedStatus));
// 		      	logger.logDebug(methodName+"|"+" Contract View Status to be rejected for view mode Finish...");   
// 		      	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_AMEND_CONTRACT_REQ_REJECTED));
// 		      	btnApproveAmendLease.setRendered(false);
// 		      	btnRejectAmendLease.setRendered(false);
// 		      	viewRootMap.put("pageMode","");
//    	        logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(PIMSWorkListException exc)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",exc);
//    	
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    	}
//    	return "";
//    }
//    public String btnSubmit_Click()
//    {
//        String methodName = "btnSubmit_Click";
//        String eventOutcome="failure";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
//        //errorMessages.clear();
//        
//		
//        if(addRequest())
//        {
//           eventOutcome="";
//        }
//        logger.logInfo(methodName+"|"+" eventOutCome For add Event..."+eventOutcome);
//        logger.logInfo(methodName+"|"+" Finish...");
//        
//        
//        return eventOutcome;
//    }
//
//    
//    public String btnSaveSiteVisit_Click()
//    {
//        String methodName = "btnSaveSiteVisti_Click";
//        String eventOutcome="failure";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
//        //errorMessages.clear();
//        
//        RequestServiceAgent rsv = new RequestServiceAgent();
//        SiteVisitView svv = new SiteVisitView();
//        
//        
//        // TODO : Create the Site Visit View Object 
//        svv.setRemarks(this.getRemarksFor());
//        svv.setVisitDate(this.getVisitDate());
//        svv.setEngineerName( this.getEngineerName() );
//        
//        svv.setCreatedBy(getLoggedInUser());
//        svv.setCreatedOn(new Date());
//		
//        svv.setUpdatedBy(getLoggedInUser());
//        svv.setUpdatedOn(new Date());
//        
//        svv.setIsDeleted(new Long(0));
//        svv.setRecordStatus(new Long(1));
//        
//        svv.setRequestId(Long.valueOf( this.getRequestId()) );
//        
//        if(this.getEstimatedCost() != null && !this.getEstimatedCost().equals(""))
//        	svv.setEstimatedCost( Double.valueOf(this.getEstimatedCost()));
//        
//        
//        try {
//			svv = rsv.addSiteVisit(svv);
//			
//			if(svv != null && svv.getSiteVisitId() != null ){
//        		
//        		this.getSiteVisitList().add( svv );
//        	}
//		} catch (PimsBusinessException e) {
//			// TODO Auto-generated catch block
//			logger.LogException(methodName+"|"+" Error Occured...",e);
//			e.printStackTrace();
//		}		
//        
//           eventOutcome="";
//        
//        logger.logInfo(methodName+"|"+" eventOutCome For add Event..."+eventOutcome);
//        logger.logInfo(methodName+"|"+" Finish...");
//        
//        
//        return eventOutcome;
//    }
//    
//    public String btnSaveFollowup_Click()
//    {
//        String methodName = "btnSaveFollowup_Click";
//        String eventOutcome="failure";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//        FacesContext context = FacesContext.getCurrentInstance();
//        UIViewRoot view = context.getViewRoot();
//        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
//        session.setAttribute("view",view);
//        //errorMessages.clear();
//        
//        RequestServiceAgent rsv = new RequestServiceAgent();
//        FollowUpView followUpView = new FollowUpView();
//        
//        followUpView.setRemarks(this.getRemarksFor());
//        followUpView.setCompletionPercent( 0L );
////        followUpView.setContractId( );
//        followUpView.setRequestId( Long.valueOf( this.getRequestId() ) );
//        
//        if( this.getActualCost() != null && !this.getActualCost().equals("") )
//        	followUpView.setActualCost( Double.valueOf(this.getActualCost()) ) ; 
//        
//        followUpView.setFollowupDate(new Date());
//
//        followUpView.setStartDate( new Date()); // Will not be used but setting the value due to not null
//        followUpView.setFollowUpTypeId(  0L ); // Will not be used but setting the value due to not null
//        followUpView.setStatusId(  0L ); // Will not be used but setting the value due to not null
//        
//        try {
//        	followUpView = rsv.addFollowup( followUpView);
//        	if(followUpView != null && followUpView.getFollowUpId() != null ){
//        		
//        		this.getFollowUpList().add( followUpView );
//        	}
//        	
//        	logger.logInfo(methodName+"|"+" Followup objec added" );
//        	
//        	rsv.generateAndAddPayment(Double.valueOf(this.getActualCost()), Long.valueOf(  this.getOwnershipType() ));
//        	
//        	logger.logInfo(methodName+"|"+" Payments Saved ... " );
//        	
//		} catch (PimsBusinessException e) {
//			// TODO Auto-generated catch block
//			logger.LogException(methodName+"|"+" Error Occured...",e);
//			e.printStackTrace();
//		}		
//        
//           eventOutcome="";
//        
//        logger.logInfo(methodName+"|"+" eventOutCome For add Event..."+eventOutcome);
//        logger.logInfo(methodName+"|"+" Finish...");
//
//        return eventOutcome;
//    }
//    
//    public void btnSendNewLeaseForApproval()
//    {
//    	String methodName = "btnSendLeaseForApproval";
//
//    	logger.logInfo(methodName+"|"+" Start...");
//    	SystemParameters parameters = SystemParameters.getInstance();
//    	errorMessages = new ArrayList<String>(0);
//    	try
//    	{
//    		requestView  =(RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
//    	   String endPoint= parameters.getParameter(WebConstants.LEASE_CONTRACT_BPEL_ENDPOINT);
//    	  ContractInitiateBPELPortClient port=new ContractInitiateBPELPortClient();
//	   	    port.setEndpoint(endPoint);
//	   	    logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
//	   		if(contractId!=null && contractId.trim().length()>0)
//	   		{
//	   	      port.initiate(new Long(contractId),requestView.getRequestId(),getLoggedInUser(), null, null);
//	   	      if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE") && viewRootMap.get("CONTRACTVIEWUPDATEMODE")!=null )
//	   	        contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	   	      getRequestForNewLeaseContract();
//	   	      successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
//	   	      btnSendNewLeaseForApproval.setRendered(false);
//	   		}
//		   logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..COMPLETED ");
//		   logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    	}
//		   
//    	
//    	
//    }
//    private boolean addRequest()
//    { 
//    	String methodName = "addRequest";
//    	boolean isSucceed=false;
//    	List<PaymentReceiptView> paymentReceiptViewList=new ArrayList<PaymentReceiptView>();
//    	txttotalContractValue = txttotalContractValue;
//    	totalContractValue = totalContractValue;
//    	logger.logInfo(methodName+"|"+" Start...");
//    	try
//    	{
//	    	
//		    	PropertyServiceAgent myPort = new PropertyServiceAgent();
//		    	RequestServiceAgent  rsa = new RequestServiceAgent();
//		    	
//		    	//generateNotification(WebConstants.Notification_MetaEvents.Event_LeaseContract_Approval);
//		    	
//				if(this.getPageMode().equals(PAGE_MODE_ADD))
//			    {
//					if(!putControlValuesInView())
//			    	{
//						logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
//						
////						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
//						
//						requestView.setTenantsView(null);
//						requestView.setContractView(null);
//						
//						myPort.addContractRequest(requestView);
//						
////						Long contractsId=contractView.getContractId();
////						if(viewRootMap.containsKey(AUCTION_UNIT_BIDDER_LIST) && viewRootMap.get(AUCTION_UNIT_BIDDER_LIST)!=null)
////						{
////						     List<RequestDetailView> rdvList =  (ArrayList<RequestDetailView>)viewRootMap.get(AUCTION_UNIT_BIDDER_LIST);
////						     rsa.changeRequestDetailStatusForAuction(rdvList , contractsId,null);
////						}
////						contractId =contractView.getContractId().toString(); 
//////				    	logger.logInfo(methodName+"|"+" Contract added with Id::"+contractsId);
////				    	viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
//				    	
//				    	
//						this.setPageMode(PAGE_MODE_UPDATE);
////						viewRootMap.put("pageMode",this.getPageMode());
//						
//						if(requestView != null && requestView.getRequestId() != null)
//						{
//							requestView = propertyServiceAgent.getRequestById(requestView.getRequestId());
//							this.setRequestId(requestView.getRequestId().toString());
//							viewRootMap.put("requestId",requestView.getRequestId());
//							getRequestForNewLeaseContract();
//						}
//						
////						logger.logInfo(methodName+"|"+" Reference Number:"+contractView.getContractNumber());
////				    	this.setRefNum(contractView.getContractNumber());
//				    	saveSystemComments(MessageConstants.ContractEvents.CONTRACT_CREATED);
//				    	successMessages.add("Request saved successfully.");
//				    	
//				    	putRequestViewValuesInControl(requestView.getRequestId().toString());
//				    	
////				    	successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_CREATED_SUCCESSFULLY));
//			    	}
//			    }
//				else
//				{
//					if(putUpdatedControlValuesInView())
//					{
//					  if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
//						paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);	
//					    contractView.setPaymentScheduleView(getPaymentSchedule());
//					    logger.logInfo(methodName+"|"+" calling Method From Service Proxy ..START ");
//						contractView=myPort.addContracts(contractView,paymentReceiptViewList);
//						Long contractId=contractView.getContractId();
//						logger.logInfo(methodName+"|"+" Contract updated with Id::"+contractId);
//				    	getContractById(contractView.getContractId().toString());
//						putViewValuesInControl(contractView.getContractId().toString());
//						saveSystemComments(MessageConstants.ContractEvents.CONTRACT_UPDATED);
//						successMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UPDATED_SUCCESSFULLY));
//					}
//				}
//				requestView = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
//				if(requestView !=null && requestView.getRequestId()!=null)
//				{
//				 logger.logInfo(methodName+"|"+" Saving Notes...Start"); 
//				 saveComments(requestView.getRequestId());
//				 logger.logInfo(methodName+"|"+" Saving Notes...Finish");
//				 logger.logInfo(methodName+"|"+" Saving Attachments...Start"); 
//				 saveAttachments(requestView.getRequestId().toString());
//				 logger.logInfo(methodName+"|"+" Saving Attachments...Finish");
//				}
//				isSucceed=true;
//	    	    
//			logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	
//    	catch(Exception ex)
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		logger.LogException(methodName+"|"+" Error Occured...", ex);
//    	}
//    	return isSucceed;
//    	
//    	
//    	
//    }
//    private void ApproveRejectTask(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
//    {
//    	String methodName="ApproveRejectTask";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	try
//    	{
//	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
//	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.CONTRACT_USER_TASK_LIST);
//	    	//userTask.
//			String loggedInUser=getLoggedInUser();
//			//logger.logInfo(methodName+"|"+" TaskId..."+userTask.);
//			logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
//			logger.logInfo(methodName+"|"+" TaskOutCome..."+taskOutCome);
//			logger.logInfo(methodName+"|"+" loggedInUser..."+loggedInUser);
//			   BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
//			   bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
//			logger.logInfo(methodName+"|"+" Finish...");
//    	}
//    	catch(PIMSWorkListException ex)
//    	{
//    		
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    		throw ex;
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    		throw ex;
//    	}
//    }
//    public String getErrorMessages()
//	{
//		String messageList="";
//		if ((errorMessages == null) || (errorMessages.size() == 0)) 
//		{
//			messageList = "";
//		}
//		else
//		{
//			
//			for (String message : errorMessages) 
//				{
//					messageList += "<LI>" +message + "<br></br>" ;
//			    }
//			
//		}
//		return (messageList);
//	}
//    
//    public String getSuccessMessages()
//	{
//		String messageList="";
//		if ((successMessages== null) || (successMessages.size() == 0)) 
//		{
//			messageList = "";
//		}
//		else
//		{
//			
//			for (String message : successMessages) 
//				{
//					messageList +=  "<LI>" +message+ "<br></br>" ;
//			    }
//			
//		}
//		return (messageList);
//	}
//    public void btnOccupierDelete_Click()
//    {
//    	String methodName="btnOccupierDelete_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	PersonView selectedOccupier = (PersonView)propspectiveOccupierDataTable.getRowData();
//    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
//    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.OCCUPIER);
//    	for (PersonView personView : previousOccupiersList) 
//    	{
//		  if(selectedOccupier.getPersonId().compareTo(personView.getPersonId())!=0)
//			  newOccupiersList.add(personView);
//		
//    	}
//    	
//    	sessionMap.put(WebConstants.OCCUPIER, newOccupiersList);
//    	logger.logInfo(methodName+"|"+" Finish...");
//    }
//    public void btnPartnerDelete_Click()
//    {
//    	String methodName="btnPartnerDelete_Click";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	PersonView selectedPartner = (PersonView)propspectivepartnerDataTable.getRowData();
//    	List<PersonView> newOccupiersList= new ArrayList<PersonView>(0);
//    	List<PersonView> previousOccupiersList= (ArrayList<PersonView>)sessionMap.get(WebConstants.PARTNER);
//    	for (PersonView personView : previousOccupiersList) 
//    	{
//          if(personView.getNewPerson())
//          {
//		    if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
//			   newOccupiersList.add(personView);
//		  }
//		  //If already existed and deleted in amend mode
//		  else
//		  {
//		     if(selectedPartner.getPersonId().compareTo(personView.getPersonId())!=0)
//                newOccupiersList.add(personView);
//		  }
//    	}
//    	sessionMap.put(WebConstants.PARTNER, newOccupiersList);
//    	logger.logInfo(methodName+"|"+" Finish...");
//    }
//    private boolean putUpdatedControlValuesInView() throws Exception
//    {
//    	
//    	String methodName="putUpdatedControlValuesInView";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	String loggedInUser=getLoggedInUser();
//    	errorMessages=new ArrayList<String>();
//    	errorMessages.clear();
//    	boolean isValidated=true;
//    	contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
//    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
//    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
//    	try
//    	{
//    		if(viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
//		         contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//        		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
//        	    logger.logInfo(methodName+"|"+" ContractType..."+this.getSelectOneContractType());
//        	    if(!AttachmentBean.mandatoryDocsValidated())
//		    	{
//		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
//		    	      isValidated=false;
//		    	}
//		        if(this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1"))
//			    contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
//			    else
//			    {
//			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
//			    }
//		        
//		    	
//		    	if(contractStartDate!=null && !contractStartDate.equals("") 
//						 && contractEndDate!=null && !contractEndDate.equals("") )
//    			{
//    			    
//		    		
//    				//Contract start date shud only be less than Contract EXPIRY date
//		    		  if (contractStartDate.compareTo(contractEndDate)>=0)
//		    			{
//		    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
//		    				isValidated=false;
//		    			}
//		    		  
//    			}
//		    	else
//		    	{
//		    		
//		    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
//		    	      isValidated=false;
//		    	}
//		    
//        		
//		    	logger.logInfo(methodName+"|"+" tenantRefNum..."+this.getTenantRefNum());
//		    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
//		    	if(viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null )
//		    	{
//		    		tenantsView.setPersonId((new Long(this.getHdnTenantId())));
//		    		//tenantsView.setTenantNumber(tenantRefNum);
//		    		contractView.setTenantView(tenantsView);
//		    		//requestView.setTenantsView(tenantsView);
//		    		
//		    	}
//		    	else
//		    	{
//		    		
//		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
//		    		isValidated=false;
//		    	}
//        		
//		    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
//		    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
//		    	{
//		    	  if(isUnitValid())
//		    	  {
//		    		if(unitRentAmount!=null && unitRentAmount.trim().length()>0)
//		    		{
//		    		  // unitsView.setUnitNumber(this.getUnitRefNum());
//		    		   unitsView.setUnitId(new Long(hdnUnitId));
//		    		   if(viewRootMap.containsKey(AUCTION_UNIT_ID) && viewRootMap.get(AUCTION_UNIT_ID)!=null )
//			    	   {
//			    		    unitsView.setAuctionUnitId(new Long(viewRootMap.get(AUCTION_UNIT_ID).toString()));
//			    		}
//                       contractView.setContractUnitView(getContractUnitSet(loggedInUser));
//		    		}
//		    		else
//			        {
//			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
//			    		isValidated = false;
//			    	}
//		    	  }
//		    	  else
//			        {
//			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UNITS_INVALID));
//			    		isValidated = false;
//			    	}
//		    	}
//		    	else
//		        {
//		    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
//		    		isValidated = false;
//		    	}
//        		if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null)
//		    	{
//		    		//if tenant is company
//		    		if(getIsCompanyTenant())
//		    		{
//		    			//if Contract is commercial and tenant is company then sponsor info is required.
//			    		if(getIsContractCommercial())
//			    		{
//			    			if(this.getTxtSponsor() ==null || this.getTxtSponsor().length()<=0 )
//			    			{
//			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
//					    		isValidated = false;
//			    				
//			    			}
//			    			
//			    			
//			    		}
//			    		//if unit is residential than and tenant is residential than contract can not be added
//			    		else 
//			    		{
//			    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
//			    			isValidated = false;
//			    			
//			    		}
//		    		}
//		    		
//		    		
//		    	}
//        		  
//        		 if(contractStartDate!=null && !contractStartDate.equals("") )
//			        {
//			        	contractView.setStartDate(contractStartDate);
//			        	contractView.setOriginalStartDate(contractStartDate);
//			        	
//			        }
//			        else
//			        {
//			    		
//			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_START_DATE));
//			        	isValidated = false;
//			    	}
//			        
//			        logger.logInfo(methodName+"|"+" contractEndDate..."+contractEndDate);
//			        if(contractEndDate!=null && !contractEndDate.equals(""))
//			        {
//			        	contractView.setEndDate(contractEndDate);
//			        	contractView.setOriginalEndDate(contractEndDate);
//			        	
//			        }
//			        else
//			        {
//			    		
//			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_EXPIRY_DATE));
//			        	isValidated = false;
//			    	}
//             		if(getIsContractValueandRentAmountEqual())
//             		{
//             			if(getIsContractCommercial() && !isCommercialActivitySelected())
//    	    			{
//    	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
//    			    		isValidated = false;
//    	    			}
//             			else if( getIsUnitCommercial())
//	 			        {
//	 			        	contractView.setContractActivitiesView(getCommercialActivities());
//	 			        	
//	 			        }
//				        logger.logInfo(methodName+"|"+" totalContractAmount..."+txttotalContractValue);
//				        contractView.setRentAmount(new Double(txttotalContractValue));
//				        contractView.setPaymentScheduleView(getPaymentSchedule());
//				        contractView.setContractPersonView(getContractPersons());
//				        contractView.setContractPartnerView(getContractParntner());
//				        if(hdnUnitId!=null && hdnUnitId.trim().length()>0) 
//				        {
//				           unitsView = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
//			    		   
//				           contractView.setContractUnitView(getContractUnitSet(loggedInUser));
//				        }
//				        //contractView.set
//				        contractView.setUpdatedBy(loggedInUser);
//				        
//				        contractView.setUpdatedOn(new Date());
//				        
//             		}
//             		else
//             		{
//			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
//			        	isValidated = false;
//			        }
//
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.logError(methodName+"|"+" Exception Occured..."+ex);
//    		throw ex;
//    		
//    	}
//    	logger.logInfo(methodName+"|"+" Finish...");
//    	return isValidated ;
//    }
//
//    private boolean putControlValuesInView() throws Exception
//    {
//    	String methodName="putControlValuesInView";
//    	logger.logInfo(methodName+"|"+" Start...");
//    	errorMessages=new ArrayList<String>();
//    	errorMessages.clear();
//    	boolean isError=false;
//    	contractStartDate= (Date)viewRootMap.get("CONTRACT_START_DATE");
//    	contractEndDate= (Date)viewRootMap.get("CONTRACT_END_DATE");
//    	String loggedInUser=getLoggedInUser();
//    	
//    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());
//    	try
//    	{
//    		
//             		logger.logInfo(methodName+"|"+" ContractId..."+contractId);
////    		        if(contractId!=null && !contractId.equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
////			    	{
////			    	  contractView.setContractId(new Long(contractId));
////
////			    	}
//    		        if(!AttachmentBean.mandatoryDocsValidated())
//    		    	{
//    		    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
//    		    		isError = true;
//    		    	}
////    		        logger.logInfo(methodName+"|"+" ContractType..."+this.getSelectOneContractType());
////    		        if(this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1"))
////    			         contractView.setContractTypeId(new Long(this.getSelectOneContractType()));
////    			    else
////    			    {
////    			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_CONTRACT_TYPE));
////    			    }
//    		        logger.logInfo(methodName+"|"+" datetimerequest..."+this.getDatetimerequest());
//			    	if(this.getDatetimerequest()!=null && !this.getDatetimerequest().equals(""))
//			    	{
////			    	  contractView.setContractDate(dateFormat.parse(this.getDatetimerequest()));
//			    	  requestView.setRequestDate(dateFormat.parse(this.getDatetimerequest()));
//			    	}
//			    	
//			    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_ID) && viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID)!=null)
//			    	{
//			    		logger.logInfo(methodName+"|"+" applicantId..."+viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString());
//			    		PersonView applicantView = new PersonView();
//			    		applicantView.setPersonId(new Long(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_ID).toString()));
//			    		requestView.setApplicantView(applicantView);
//			    		if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) && 
//			    				viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION)!=null)
//			    		  requestView.setDescription(viewRootMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString() );
//			    	}
//			    	else
//			    	{
//			    		errorMessages.add(getBundle().getString(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
//			    	}
////			    	if(contractStartDate!=null && !contractStartDate.equals("") 
////							 && contractEndDate!=null && !contractEndDate.equals("") )
////	    			{
////	    			    
////			    		
////	    				String todayDate=dateFormat.format(new Date());
////		    			logger.logDebug(methodName+"|"+"TodayDate.."+todayDate);
////		    			//Contract start date shud only be less than Contract EXPIRY date
////			    		  if (contractStartDate.compareTo(contractEndDate)>=0)
////			    			{
////			    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
////			    				isError=true;
////			    			}
////			    		  else if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) && contractStartDate.compareTo(dateFormat.parse(todayDate))==-1)
////			    		  {
////			    			  
////						      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_TODAY));
////						      isError=true;
////			    		  }
////	    			}
////			    	else
////			    	{
////			    		
////			    	      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_END_EMPTY));
////					      isError=true;
////			    	}
//			    	logger.logInfo(methodName+"|"+" refNum..."+this.getRefNum());
////			    	if(this.getRefNum()!=null && !this.getRefNum().equals("") && !pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
////			    	{
////			    		contractView.setContractNumber(this.getRefNum());
////			    	}
//			    	
//			    	
//			    	logger.logInfo(methodName+"|"+" tenantId..."+this.getHdnTenantId());
//			    	if(viewRootMap.containsKey(TENANT_INFO) && viewRootMap.get(TENANT_INFO)!=null )
//			    	{
//			    		tenantsView = (PersonView)viewRootMap.get(TENANT_INFO);
//			    		if(tenantsView.getPersonId()!=null)
//			    		this.setHdnTenantId(tenantsView.getPersonId().toString());
//			    		//tenantsView.setPersonId((new Long(this.getHdnTenantId())));
//
////			    		contractView.setTenantView(tenantsView);
//			    		requestView.setTenantsView(tenantsView);
//			    		
//			    	}
////			    	else
////			    	{
////			    		
////			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_TENANT_NUM));
////			    		isError=true;
////			    	}
//			    	//logger.logInfo(methodName+"|"+" unitRefNum..."+this.getUnitRefNum());
//			    	logger.logInfo(methodName+"|"+" hdnUnitId..."+hdnUnitId);
//			    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0)
//			    	{
//			    	  if(isUnitValid())
//			    	  {
//			    		if(unitRentAmount!=null && unitRentAmount.trim().length()>0)
//			    		{
//			    		   //unitsView.setUnitNumber(this.getUnitRefNum());
//			    		   unitsView.setUnitId(new Long(hdnUnitId));
//			    		   if(viewRootMap.containsKey(AUCTION_UNIT_ID) && viewRootMap.get(AUCTION_UNIT_ID)!=null )
//			    		   {
//			    		    unitsView.setAuctionUnitId(new Long(viewRootMap.get(AUCTION_UNIT_ID).toString()));
//			    		   }
//                           requestView.setRequestDetailView(getRequestDetailSet(loggedInUser));
////                           contractView.setContractUnitView(getContractUnitSet(loggedInUser));
//			    		}
//			    		else
//				        {
//				    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_RENT_VALUE));
//				    		isError=true;
//				    	}
//			    	  }
//			    	  else
//				        {
//				    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_UNITS_INVALID));
//				    		isError=true;
//				    	}
//			    	}
//			    	else
//			        {
//			    		errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_UNIT_NUM));
//			    		isError=true;
//			    	}
//			    	if(getIsContractCommercial() && !isCommercialActivitySelected())
//	    			{
//	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_COMMERCIALACTIVITY));
//			    		isError=true;
//	    			}
//			    	if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && this.getTenantRefNum()!=null && this.getTenantRefNum().length()>0)
//			    	{
//			    		//if tenant is company
//			    		if(getIsCompanyTenant())
//			    		{
//			    			//if Contract is commercial and tenant is company then sponsor info is required.
//				    		if(getIsContractCommercial())
//				    		{
//				    			if(this.getTxtSponsor()==null || this.getTxtSponsor().length()<=0 )
//				    			{
//				    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_SPONSOR_INFO));
//						    		isError=true;
//				    				
//				    			}
//				    			
//				    			
//				    		}
//				    		//if unit is residential than and tenant is residential than contract can not be added
//				    		else 
//				    		{
//				    			errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_COMPANY_TENANT_RESIDENTIAL_UNIT));
//					    		isError=true;
//				    			
//				    		}
//			    		}
//			    		
//			    		
//			    	}
//			    	logger.logInfo(methodName+"|"+" contractStartDate..."+contractStartDate);
////			        if(contractStartDate!=null && !contractStartDate.equals("") )
////			        {
////			        	contractView.setStartDate(contractStartDate);
////			        	contractView.setOriginalStartDate(contractStartDate);
////			        	
////			        }
////			        else
////			        {
////			    		
////			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_START_DATE));
////			    		isError=true;
////			    	}
//			        
//			        logger.logInfo(methodName+"|"+" contractEndDate..."+contractEndDate);
////			        if(contractEndDate!=null && !contractEndDate.equals(""))
////			        {
////			        	contractView.setEndDate(contractEndDate);
////			        	contractView.setOriginalEndDate(contractEndDate);
////			        	
////			        }
////			        else
////			        {
////			    		
////			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_REQ_EXPIRY_DATE));
////			    		isError=true;
////			    	}
////			        if(this.getSelectRequestStatus()!=null && !this.getSelectRequestStatus().equals(""))
////			        {
////			        	//requestView.setStatusId(new Long(selectRequestStatus));
////			        	contractView.setStatus(new Long(this.getSelectRequestStatus()));
////			        }
////			        else
////			        {
////			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_SEL_REQ_STATUS));
////			    		isError=true;
////			    	}
//			        
//			        
////			        if(txttotalContractValue!=null && ! txttotalContractValue.equals(""))
////			        {
////			        	contractView.setRentAmount(new Double(txttotalContractValue));
////			        	
////			        }
////			        
////			        
////			        //Check for the units type if its commercial than add partners.
////			        contractView.setContractPartnerView(getContractParntner());
////			        
////			        contractView.setContractPersonView(getContractPersons());
////			        
////			        contractView.setPaymentScheduleView(getPaymentSchedule());
////                                  //For time being 
////			        if(!getIsContractValueandRentAmountEqual())
////			        {
////			        	errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CONTRACT_RENTAMOUNT_NOT_EQUAL_TO_TOTAL_CONTRACT_VALUE));
////			    		isError=true;
////			        }
////			        contractView.setIsDeleted(new Long(0));
////			        contractView.setRecordStatus(new Long(1));
////			        contractView.setUpdatedBy(loggedInUser);
//			        
////			        contractView.setUpdatedOn(new Date());
//			        if(getPageMode()!=null &&( getPageMode().equals(PAGE_MODE_ADD)|| getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)))
//			        {
////			        	contractView.setCreatedOn(new Date());
////			            contractView.setCreatedBy(loggedInUser);
//			            requestView.setCreatedOn(new Date());
//			        }
////			        else if(pageMode!=null && pageMode.equals(PAGE_MODE_UPDATE) )  
////			        {
////			        	if(contractCreatedOn!=null && !contractCreatedOn.equals(""))
////			        	contractView.setCreatedOn(dateFormat.parse(contractCreatedOn));
////			        	if(contractCreatedBy!=null && !contractCreatedBy.equals(""))
////			                contractView.setCreatedBy(contractCreatedBy);
////			        
////			        }
////			        if(pageMode!=null && (pageMode.equals(PAGE_MODE_ADD) || pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) || pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)  )
////			        		 && getIsUnitCommercial())
////			        {
////			        	contractView.setContractActivitiesView(getCommercialActivities());
////			        	
////			        }
//			        
//			        requestView.setUpdatedOn(new Date());
//			        requestView.setIsDeleted(new Long(0));
//			        requestView.setRecordStatus(new Long(1));
//			        requestView.setUpdatedBy(loggedInUser);
//			        requestView.setCreatedBy(loggedInUser);
//			        
//			        requestView.setRequestTypeId(Constant.REQUEST_TYPE_MAINTENANCE_REQUEST);
//			        
//			        if(this.getMaintenanceType() != null && !this.getMaintenanceType().toString().equals("-1"))
//			        	requestView.setMaintenanceTypeId(new Long(this.getMaintenanceType().toString()));
//			        
//			        
//			        if(this.getWorkType() != null && !this.getWorkType().toString().equals("-1"))
//			        	requestView.setWorkTypeId(new Long(this.getWorkType().toString()));
//			        
//			        if(this.unitsView != null && this.unitsView.getPropertyId() != null)
//			        	requestView.setPropertyId(this.unitsView.getPropertyId());
//			        
//			        
//			        
//			        logger.logInfo(methodName+"|"+" Set request status...");
//			        
//			        if(viewRootMap.containsKey(WebConstants.SESSION_REQUEST_STATUS))
//			        {
//			        	List<DomainDataView> ddv=(ArrayList) viewRootMap.get(WebConstants.SESSION_REQUEST_STATUS);
//			          if(getPageMode().equals(PAGE_MODE_ADD))
//			          {
//			        	HashMap hMap=getIdFromType(ddv, WebConstants.REQUEST_STATUS_NEW);
//			          	if(hMap.containsKey("returnId"))
//			          	   requestView.setStatusId(new Long(hMap.get("returnId").toString()));
//			          }
////			          else if(pageMode.equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
////			          {
////			        	  HashMap hMap=getIdFromType(ddv, WebConstants.REQUEST_STATUS_ACCEPTED);
////				          	if(hMap.containsKey("returnId"))
////				          	   requestView.setStatusId(new Long(hMap.get("returnId").toString()));
////			        	  
////				          	RequestTypeView rtv=new RequestTypeView();
////				          	rtv.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
////				          	requestView.setRequestTypeId(rtv.getRequestTypeId());
////				          	
////				          	RequestFieldDetailView requestFieldDetailView=new RequestFieldDetailView();
////				          	setRequestFieldDetailView(requestFieldDetailView);
////							Set<RequestFieldDetailView> requestFieldDetailViewSet = new HashSet<RequestFieldDetailView>(0);
////							requestFieldDetailViewSet.add(requestFieldDetailView);
////							
////							requestView.setRequestFieldDetailsView(requestFieldDetailViewSet);
////				          	List<DomainDataView>ddvList= commonUtil.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS);
////				          	DomainDataView domainDataView=commonUtil.getIdFromType(ddvList, WebConstants.CONTRACT_STATUS_DRAFT);
////				          	contractView.setStatus(domainDataView.getDomainDataId());
////			          }
//			        }
////			        contractView.getRequestsView().add(requestView);
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+" Exception Occured...",ex);
//    		throw ex;
//    		
//    	}
//    	logger.logInfo(methodName+"|"+" Finish...");
//    	return isError;
//    }
//    private boolean getIsContractValueandRentAmountEqual()
//    {
//    	String methodName="getIsContractValueandRentAmountEqual";
//    	logger.logInfo(methodName+"|"+"Start");
//    	boolean isEqual=false;
//    	if(txttotalContractValue!=null && txttotalContractValue.length()>0)
//    	{
//    	  Double contractValue=new Double(txttotalContractValue);	
//    		if(contractValue.compareTo(getSumofAllRentAmounts())==0)
//    			isEqual=true;
//    	}
//    	logger.logDebug(methodName+"|"+"IsContractValueandRentAmountEqual:::"+isEqual);
//    	logger.logInfo(methodName+"|"+"Finish");
//    	return isEqual;
//    }
//    private Double getSumofAllRentAmounts()
//    {
//    	String methodName="getSumofAllRentAmounts";
//    	logger.logInfo(methodName+"|"+"Start");
//    	Double rentAmount=new Double(0);
//    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
//    	
//    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//        {
//		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			for(int i=0;i< paymentScheduleViewList.size();i++)
//			{
//				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
//				//If payment Schedule is for the rent 
//				if( (paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0 )&& 
//						paymentScheduleView.getTypeId().compareTo(WebConstants.PAYMENT_TYPE_RENT_ID)==0)
//				   rentAmount+=paymentScheduleView.getAmount();
//			}
//		    
//        }
//    	DecimalFormat df=new DecimalFormat();
//    	
//    	//rentAmount=commonUtil.getDoubleValue(rentAmount.toString());
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    	return Math.ceil(rentAmount);
//    	
//    }
//    private boolean isCommercialActivitySelected()
//    {
//    	boolean isSelected=false;
//    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
//    	{
//    		List<CommercialActivityView> commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY");
//    		for (CommercialActivityView commercialActivityView : commercialActivityViewList) 
//    		{
//			  if(commercialActivityView.getIsSelected())
//			  {
//				  isSelected=true;
//				  break;
//			  }
//			}
//    	}
//    	
//    	return isSelected;
//    }
//    private Set<ContractActivityView>  getCommercialActivities()
//    {
//    	String methodName ="getCommercialActivities";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	List<CommercialActivityView> commercialActivityViewList=new ArrayList<CommercialActivityView>(0);
//    	if(viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
//    		commercialActivityViewList=(ArrayList<CommercialActivityView>)viewRootMap.get("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"); 
//    	Set<ContractActivityView> contractActivityViewSet=new HashSet<ContractActivityView>(0);
//    	for (CommercialActivityView commercialActivityView : commercialActivityViewList) 
//    	{
//    		if(commercialActivityView.getIsSelected())
//    		{
//    		 ContractActivityView contractActivityView=new ContractActivityView();
//    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Name::"+commercialActivityView.getCommercialActivityName());
//    		 logger.logInfo(methodName+"|"+"Selected Commercial Activity Id::"+commercialActivityView.getCommercialActivityId());
//    		 contractActivityView.setCommercialActivityId(commercialActivityView.getCommercialActivityId());
//    		 contractActivityView.setUpdatedOn(new Date());
//    		 contractActivityView.setUpdatedBy(getLoggedInUser());
//    		 contractActivityView.setCreatedOn(new Date());
//    		 contractActivityView.setCreatedBy(getLoggedInUser());
//    		 contractActivityView.setRecordStatus(new Long(1));
//    		 contractActivityView.setIsDeleted(new Long(0));
//    		 contractActivityViewSet.add(contractActivityView);
//    		
//    		}
//    		
//			
//		}
//    	
//    	
//    	logger.logInfo(methodName+"|"+"Finish...");
//    	return contractActivityViewSet;
//    	
//    }
//   
//    public Set<ContractPartnerView> getContractParntner()
//    {
//    	String methodName="getContractParntner";
//    	logger.logInfo(methodName+"|"+"Start");
//    		Set<ContractPartnerView> contractPartnerViewSet =new HashSet<ContractPartnerView>(0);
//    		if(sessionMap.containsKey(WebConstants.PARTNER))
//   	        {
//    			List<PersonView> personViewList= (ArrayList)sessionMap.get(WebConstants.PARTNER);
//    			for(int i=0;i< personViewList.size();i++)
//    			{
//    			   PersonView personView =(PersonView)personViewList.get(i);
//    			   ContractPartnerView contractPartnerView =new ContractPartnerView();
//    			   
//    			   //For amend to delete already existing partner
//    			   if(personView.getIsDeleted()==null || personView.getIsDeleted().compareTo(new Long(0))==0)
//    				   contractPartnerView.setIsDeleted(new Long(0));
//    			   else if(personView.getIsDeleted().compareTo(new Long(1))==0)
//    			   {
//    			     contractPartnerView.setIsDeleted(new Long(1));
//    			   }
//    			   
//    			   contractPartnerView.setPersonView(personView);
//    			  contractPartnerViewSet.add(contractPartnerView);
//    			}
//    			
//   	        }
//    		logger.logInfo(methodName+"|"+"Finish");
//    	return contractPartnerViewSet;
//    }
//    public Set<ContractUnitView> getContractUnitSet(String loggedInUser)
//    {
//    	String methodName="getContractUnitSet";
//    	logger.logInfo(methodName+"|"+"Start");
//         Set<ContractUnitView> contractUnitViewSet =new HashSet<ContractUnitView>(0);
//        ContractUnitView contractUnitView= new ContractUnitView();
//        //Contract Unit Occupier check for the company here...
////        contractUnitView.setContractUnitPersons(getContractUnitOccupiers());
//        contractUnitView.setUnitView(unitsView);
//        
//        // COMMENT::Rent Value of the Unit on the basis of calculation
//		// logger.logInfo(methodName+"|"+"Units Actual Annual Rent::"+unitRentAmount);
//		// Double unitAmount = getUnitAmuntForSelectedDuration();
//		// logger.logInfo(methodName+"|"+"Units Rent after calculation::"+unitAmount);
//        
//        contractUnitView.setRentValue(new Double(unitRentAmount));
////	    contractUnitView.setPaidFacilitieses(getPaidFacilitiesSet());
//	    Set setContractUnitView =contractView.getContractUnitView();
//	    while(setContractUnitView.iterator().hasNext())
//	    {
//	    	contractUnitView.setContractUnitId(((ContractUnitView)setContractUnitView.iterator().next()).getContractUnitId());
//	    	break;
//	    }
//	    contractUnitViewSet.add(contractUnitView);
//	    logger.logInfo(methodName+"|"+"Finish");
//    	return contractUnitViewSet;
//    }
//
//    private Set<ContractPersonView> getContractPersons()
//    {
//    	String methodName="getContractUnitPersons";
//    	logger.logInfo(methodName+"|"+"Start");
//    	Set<ContractPersonView> contractPersonViewSet=new HashSet<ContractPersonView>(0);
//    	PersonView personView =new PersonView();
//    	ContractPersonView contractPersonView =new ContractPersonView();
//    	List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0); 
//    	//Sponsor
//    	if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
//		{
//			ddvList=(ArrayList) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);
//		    DomainDataView ddvSponsor = commonUtil.getIdFromType(ddvList,WebConstants.SPONSOR);
//		}
//
//    	   logger.logInfo(methodName+"|"+"txtSponsor::"+this.getTxtSponsor());
//    	   logger.logInfo(methodName+"|"+"SponsorId::"+hdnSponsorId);
//    	   
//    	   if(hdnSponsorId!= null && hdnSponsorId.trim().length()>0)
//    	   {
//    	    String[] spnsrInfo = getPersonNameIdFromHidden(hdnSponsorId);
//    	     personView.setPersonId(new Long(spnsrInfo[0]));
//    	     contractPersonView.setPersonView(personView);
//    	     DomainDataView ddvSponsor = commonUtil.getIdFromType(ddvList,WebConstants.SPONSOR);
//    	     contractPersonView.setPersonTypeId(ddvSponsor.getDomainDataId());
//    	     contractPersonViewSet.add(contractPersonView);
//    	     
//    	   }
//    	   //Manager
//    	   logger.logInfo(methodName+"|"+"txtManager::"+this.getTxtManager());
//    	   logger.logInfo(methodName+"|"+"ManagerId::"+hdnManagerId);
//           if(hdnManagerId!= null && hdnManagerId.trim().length()>0)
//           {
//              String[] mgrInfo = getPersonNameIdFromHidden(hdnManagerId);
//    	    
//        	   contractPersonView=new ContractPersonView();
//        	   personView =new PersonView();
// 	           personView.setPersonId(new Long(mgrInfo[0]));
// 	           contractPersonView.setPersonView(personView);
// 	          DomainDataView ddvManager = commonUtil.getIdFromType(ddvList,WebConstants.MANAGER);
// 	          contractPersonView.setPersonTypeId(ddvManager.getDomainDataId());
// 	          contractPersonViewSet.add(contractPersonView);
//           }
//           logger.logInfo(methodName+"|"+"Finish");
//    	return contractPersonViewSet;
//    }
//    
//    
//    private Set<PaymentScheduleView> getPaymentSchedule()
//    {
//    	String methodName="getPaymentSchedule";
//    	logger.logInfo(methodName+"|"+"Start");
//    	Set<PaymentScheduleView> paymentScheduleViewSet=new HashSet<PaymentScheduleView>(0);
//    	
//    	if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//        {
//		    List<PaymentScheduleView> paymentScheduleViewList= (ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			for(int i=0;i< paymentScheduleViewList.size();i++)
//			{
//				PaymentScheduleView paymentScheduleView =(PaymentScheduleView)paymentScheduleViewList.get(i);
//				paymentScheduleView.setOwnerShipTypeId(
//						new Long (viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString())); 
//				
//				paymentScheduleViewSet.add(paymentScheduleView);
//				
//			    
//			}
//		    
//        }
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    	return paymentScheduleViewSet;
//    }
//    private void getPaymentReceiptViewFromReceivePaymentScreen()
//    {
//    	String methodName="getPaymentReceiptViewFromReceivePaymentScreen";
//    	logger.logInfo(methodName+"|"+"Start");
//    	List<PaymentReceiptView> paymentReceiptViewList;
//    	//Checking if hashMAP HAS ALREADY BEEN IN THE SESSION
//    	
//    	    if(sessionMap.containsKey(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS))
//    	    	paymentReceiptViewList=(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS);
//    		else
//    			paymentReceiptViewList=new ArrayList<PaymentReceiptView>();
//
// 	       if(sessionMap.containsKey(WebConstants.PAYMENT_RECEIPT_VIEW))
// 	       {
// 		     PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
// 		    paymentReceiptViewList.add(prv);
// 		     
// 		    	 
// 		       
// 		     sessionMap.put(WebConstants.SESSION_CONTRACT_PAYMENT_RECEIPTS, paymentReceiptViewList);
// 	       
// 	       
// 	       
// 	       
// 	         sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
// 	       }
//    	
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    
//    }
//    public Set<RequestDetailView> getRequestDetailSet(String loggedInUser)
//    {
//    	  Set<RequestDetailView> requestDetailSet=new HashSet<RequestDetailView>();
//    	   requestDetailView.setUnitView(unitsView);
//    	   requestDetailView.setUpdatedBy(getLoggedInUser());
//    	   requestDetailView.setCreatedBy(getLoggedInUser());
//    	   
//    	   requestDetailView.setCreatedOn(new Date());
//    	   requestDetailView.setUpdatedOn(new Date());
//    	   requestDetailView.setRecordStatus(new Long(1));
//    	   requestDetailView.setIsDeleted(new Long(0));
//    	   
//		    
//		
//		requestDetailSet.add(requestDetailView);
//		return requestDetailSet;
//    	
//    	
//    }
//    
//    private void putRequestViewValuesInControl(String requestId) throws PimsBusinessException,Exception
//    {
//    	String methodName="putRequestViewValuesInControl";
//    	
//    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());//format!=null && format.length()>0?format:"dd/MM/yyyy");
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//    	
//    	logger.logInfo(methodName+"|"+" Reference Number:"+requestView.getRequestNumber());
//    	
//    	this.setRefNum(contractView.getContractNumber());
//    	
//    	Iterator iterReqDeatils = requestView.getRequestDetailView().iterator();
//    	
//	  	  while (iterReqDeatils.hasNext()) {
//	  		  
//			RequestDetailView requestDetailView = (RequestDetailView) iterReqDeatils.next();
//			
//			if (requestDetailView.getUnitView() != null
//					&& requestDetailView.getUnitView().getUnitId() != null) {
//
//				this.setUnitsView(propertyServiceAgent.getPropertyUnitViewByID( requestDetailView.getUnitView().getUnitId()));
//				
//				requestDetailView.setUnitView( this.getUnitsView( ));
//				this.setUnitRefNum( requestDetailView.getUnitView().getUnitNumber() );
//				this.unitsView = requestDetailView.getUnitView();
//				
//				this.setUnitsView(requestDetailView.getUnitView());
//				propertyCommercialName = requestDetailView.getUnitView().getPropertyCommercialName();
//				
//					viewRootMap.put(UNIT_DATA_ITEM, requestDetailView.getUnitView());
//	    		  sessionMap.put("UNITINFO",requestDetailView.getUnitView());
//	    		  if( requestDetailView.getUnitView().getPropertyCategoryId() != null )
//	    			  viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,requestDetailView.getUnitView().getPropertyCategoryId());
//	    		  this.setOwnershipType(requestDetailView.getUnitView().getPropertyCategoryEn());
//	    		  hdnUnitId=requestDetailView.getUnitView().getUnitId().toString();
//	    		  
//	    		  if( requestDetailView.getUnitView().getUsageTypeId() != null )
//	    			  hdnUnitUsuageTypeId=requestDetailView.getUnitView().getUsageTypeId().toString();
//	    		  
//	    		  if( requestDetailView.getUnitView().getUsageTypeEn() != null )
//	    			  txtUnitUsuageType=(getIsEnglishLocale()?requestDetailView.getUnitView().getUsageTypeEn():requestDetailView.getUnitView().getUsageTypeAr());
//	    		  
//	    		  propertyAddress=requestDetailView.getUnitView().getUnitAddress();
//	    		  propertyName = requestDetailView.getUnitView().getPropertyCommercialName();
//	    		  
//	    		 
//	    		  
//				break;
//			}
//		}
//	  	  
//    	if( requestView !=null  )
//    	{
//    		this.setDescription(requestView.getDescription());
//    		this.setApplicationNo(requestView.getRequestNumber());
//    		
//    		this.setAppDate(requestView.getRequestDate());
//    		
//    		if(requestView.getWorkTypeId() != null )
//    			this.setWorkType(requestView.getWorkTypeId().toString());
//    		
//    		if(requestView.getMaintenanceTypeId() != null )
//    			this.setMaintenanceType( requestView.getMaintenanceTypeId().toString());
//    		
//    		this.setAppDate(  requestView.getRequestDate() );
//    		
//    		if( requestView.getApplicantView() != null )
//    			this.setName(requestView.getApplicantView().getFirstName() + ' ' + requestView.getApplicantView().getFirstName());
//    		
//	    	if(requestView.getRequestDate()!=null && !requestView.getRequestDate().equals(""))
//	    	{
//	    		this.setDatetimerequest(requestView.getRequestDate().toString());
//	    	}
//	
//	    	if( requestView.getContractId() != null )
//	    		getContractPaymentSchedule(requestView.getContractId().toString());
//	    	
//			
//	    	if( requestView.getRequestId()!=null)
//	    	     loadAttachmentsAndComments(requestView.getRequestId());
//	    	
//	    	if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null )
//	    		hdnApplicantId  = requestView.getApplicantView().getPersonId().toString();
//	    	
//	    	if( requestView.getContractId() != null )
//	    		this.setSvContractId(requestView.getContractId().toString());
//	    	
//	    	// TODO: Get Site Visit
//	    	
//	    	RequestServiceAgent rsv = new RequestServiceAgent();
//	    	
//	    	List siteVisitLst = rsv.getSiteVisitsByRequest(requestView.getRequestId());
//	    	
//	    	if(siteVisitLst != null ){
//	    		
//	    		this.setSiteVisitList(siteVisitLst);
//	    		
//	    	}
//	    	
//	    	
//	    	// TODO : Get Follow Up 
//	    	
//	    	List followupLst = rsv.getFollowupsByRequest( requestView.getRequestId());
//	    	
//	    	if(followupLst != null ){
//	    		
//	    		this.setFollowUpList( followupLst);
//	    		
//	    	}
//    	}
//    	
//    	
//    	logger.logInfo(methodName+"|"+" Finish..");
//    }
//    
//    private void putViewValuesInControl(String contractId) throws PimsBusinessException,Exception
//    {
//    	String methodName="putViewValuesInControl";
//    	
//    	DateFormat dateFormat=new SimpleDateFormat(getDateFormat());//format!=null && format.length()>0?format:"dd/MM/yyyy");
//    	logger.logInfo(methodName+"|"+" Start...");
//    	
//    	
//    	logger.logInfo(methodName+"|"+" Reference Number:"+contractView.getContractNumber());
//    	this.setRefNum(contractView.getContractNumber());
//    	
//    	
//    	logger.logInfo(methodName+"|"+" ContractType:"+contractView.getContractTypeId());
//    	if(contractView.getContractTypeId()!=null)
//    		this.setSelectOneContractType(contractView.getContractTypeId().toString());
//    	
//    	
//    	logger.logInfo(methodName+"|"+" StatusId:"+contractView.getStatus());
//    	if(contractView.getStatus()!=null)
//      	  this.setSelectRequestStatus(contractView.getStatus().toString());
//    	
//    	if(contractView.getTenantView()!=null)
//    	{
//    	  	
//	    	  tenantsView=contractView.getTenantView();
//	      	  this.setHdnTenantId(tenantsView.getPersonId().toString());
//	      	  hdntenanttypeId=tenantsView.getIsCompany().toString();
//	      	  
//	      	  String tenantNames="";
//	          if(tenantsView.getPersonFullName()!=null && tenantsView.getPersonFullName().length()>0)
//	             tenantNames=tenantsView.getPersonFullName();
//	          if(tenantNames.trim().length()<=0)
//	         	 tenantNames=tenantsView.getCompanyName();
//	          if(tenantNames.trim().length()<=0)
//	        	  tenantNames=tenantsView.getGovtDepartment();
//	          logger.logInfo(methodName+"|"+" TenantName:"+tenantNames);
//	    	  txtTenantName=tenantNames;
//	    	  txtTenantType=getTenantType(tenantsView);
//	    	  
//	    		  viewRootMap.put(TENANT_INFO, tenantsView);
//    	}
//    	
//    	logger.logInfo(methodName+"|"+"Checking for nullability of contract Unit ");
//    	if(contractView.getContractUnitView()!=null && contractView.getContractUnitView().size()>0)
//    	{
//    	  logger.logInfo(methodName+"|"+"Getting iterator for contract View ");
//    	  Iterator iter=contractView.getContractUnitView().iterator();
//    	  while(iter.hasNext())
//    	  {
//    		  
//    		  ContractUnitView contractUnitView =(ContractUnitView)iter.next();
//    		  
//    		  //Reason:: Because when contract was created the rent amount of the unit might gets changed due
//    		  //to the fact that contract might come from Auction(which has auction price) or because
//    		  //of duration of contract
//    		  //and when its viewed later the rent value  of the unit in unit table is different than that of 
//    		  //rent value of unit in contract unit.
//    		  contractUnitView.getUnitView().setRentValue(contractUnitView.getRentValue());
//    		  viewRootMap.put(UNIT_DATA_ITEM, contractUnitView.getUnitView());
//    		  sessionMap.put("UNITINFO",contractUnitView.getUnitView());
//    		  viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,contractUnitView.getUnitView().getPropertyCategoryId());
//    		  if(contractUnitView.getUnitView().getAuctionUnitId()!=null)
//    		  {
//    		   getDepositAmountForBidder(null, null, contractView.getContractId());
//    		   viewRootMap.put(AUCTION_UNIT_ID,contractUnitView.getUnitView().getAuctionUnitId());
//    		  }
//    		 logger.logInfo(methodName+"|"+"Unit Ref Number:"+contractUnitView.getUnitView().getUnitNumber());
//    		 this.setUnitRefNum(contractUnitView.getUnitView().getUnitNumber()); 
//      	     logger.logInfo(methodName+"|"+"Unit Id:"+contractUnitView.getUnitView().getUnitId().toString());
//      	       hdnUnitId=contractUnitView.getUnitView().getUnitId().toString();
//      	     logger.logInfo(methodName+"|"+"Unit Usuage Type Id:"+contractUnitView.getUnitView().getUsageTypeId().toString());
//    	       hdnUnitUsuageTypeId=contractUnitView.getUnitView().getUsageTypeId().toString();
//    	     logger.logInfo(methodName+"|"+"Contract Unit Rent Value:"+contractUnitView.getRentValue().toString());
//    	       unitRentAmount=contractUnitView.getRentValue().toString();
//    	     logger.logInfo(methodName+"|"+"PropertyCommercialName:"+contractUnitView.getUnitView().getPropertyCommercialName());
//    	       propertyCommercialName=contractUnitView.getUnitView().getPropertyCommercialName();
//    	     logger.logInfo(methodName+"|"+"Unit Usuage Type :"+contractUnitView.getUnitView().getUsageTypeEn());
//    	       txtUnitUsuageType=(getIsEnglishLocale()?contractUnitView.getUnitView().getUsageTypeEn():contractUnitView.getUnitView().getUsageTypeAr());
//    	     logger.logInfo(methodName+"|"+"Property Address:"+contractUnitView.getUnitView().getUnitAddress());
//    	       propertyAddress=contractUnitView.getUnitView().getUnitAddress();
//    	  }
//    	}
//    	logger.logInfo(methodName+"|"+" size of ContractPerson :"+contractView.getContractPersonView().size());
//    	if(contractView.getContractPersonView().size()>0)
//    	{
//    		getContractPersons(contractView.getContractId());
//    	        
//    	}
//    	logger.logInfo(methodName+"|"+"ContractDate:"+contractView.getContractDateString());
//    	if(contractView.getContractDateString()!=null && !contractView.getContractDateString().equals(""))
//    	{
//    		this.setDatetimerequest(contractView.getContractDateString());
//    	}
//    	logger.logInfo(methodName+"|"+"CreatedOn:"+contractView.getCreatedOn());
//    	if(contractView.getCreatedOn()!=null && !contractView.getCreatedOn().equals(""))
//    	{
//    		contractCreatedOn=dateFormat.format(contractView.getCreatedOn());
//    	}
//    	logger.logInfo(methodName+"|"+"CreatedBy:"+contractView.getCreatedBy());
//    	if(contractView.getCreatedBy()!=null && !contractView.getCreatedBy().equals(""))
//    	{
//    		contractCreatedBy=contractView.getCreatedBy();
//    	}
//    
//    
//    	logger.logInfo(methodName+"|"+"Contract Start Date:"+contractView.getStartDateString());
//    	if(contractView.getStartDateString()!=null && !contractView.getStartDateString().equals(""))
//    	{
//    		
//    		contractStartDate=dateFormat.parse(contractView.getStartDateString());
//    		viewRootMap.put("CONTRACT_START_DATE",contractStartDate);
//    	}
//    	logger.logInfo(methodName+"|"+"Contract End Date:"+contractView.getEndDateString());
//    	if(contractView.getEndDateString()!=null && !contractView.getEndDateString().equals(""))
//    	{
//    		contractEndDate=dateFormat.parse(contractView.getEndDateString());
//    		viewRootMap.put("CONTRACT_END_DATE",contractEndDate);
//    	}
//    	logger.logInfo(methodName+"|"+"Contract Rent Amount:"+contractView.getRentAmount());
//    	if(contractView.getRentAmount()!=null)
//    	{
//    		txttotalContractValue=contractView.getRentAmount().toString();
//    	}
//    	getOccupiers(contractId);
//    	
//    	getPartners(contractId);
//    	
//    	
//    	//not to be used
//    //	getPaidFacilities(contractId);
//    	
//    	getContractPaymentSchedule(contractId);
//    	
//		
//    	if(requestView !=null && requestView.getRequestId()!=null)
//    	     loadAttachmentsAndComments(requestView.getRequestId());
//    	
//    	
//    	
//    	logger.logInfo(methodName+"|"+" Finish..");
//    }
//  
//    private void getContractPersons(Long contractId) throws PimsBusinessException
//    {
//    	String methodName ="getContractPersons(Long contractId)";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	String sponsorTypeId="";
//    	String managerTypeId="";
//    	PropertyServiceAgent psa= new PropertyServiceAgent();
//        List<ContractPersonView> contractPersonViewList =  psa.getContractPersons(contractId);
//        for (ContractPersonView contractPersonView : contractPersonViewList) {
//        	
//        	PersonView personView = contractPersonView.getPersonView();
//    		if(viewRootMap.containsKey(WebConstants.SESSION_PERSON_TYPE))
//    		{
//    			List<DomainDataView> ddvList=(ArrayList) viewRootMap.get(WebConstants.SESSION_PERSON_TYPE);
//    			HashMap hMap=new HashMap(0);
//    		    hMap= getIdFromType(ddvList,WebConstants.SPONSOR);
//    		    if(hMap.containsKey("returnId"))
//    		    	sponsorTypeId= hMap.get("returnId").toString();
//    		    	hMap.clear();
//    		    	hMap= getIdFromType(ddvList,WebConstants.MANAGER);
//    		    	if(hMap.containsKey("returnId"))
//        		    	managerTypeId=hMap.get("returnId").toString();
//
//    		}
//    		
//    		if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(sponsorTypeId))
//    		{
//    			this.setTxtSponsor(personView.getPersonFullName());
//    		     hdnSponsorId=personView.getPersonId().toString()+":"+personView.getPersonFullName();	
//    		}
//    		else if(contractPersonView.getPersonTypeId()!=null && contractPersonView.getPersonTypeId().toString().equals(managerTypeId))
//    		{
//    			this.setTxtManager(personView.getPersonFullName());
//    		     hdnManagerId=personView.getPersonId().toString()+":"+personView.getPersonFullName();	
//    		}	
//		}
//    	logger.logInfo(methodName+"|"+"Finish");
//    	
//    }
//    
//    private void getOccupiers(String contractId) throws PimsBusinessException
//    {
//    	String methodName ="getOccupiers";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	PropertyServiceAgent psa= new PropertyServiceAgent();
//        List<PersonView> personViewList = psa.getContractUnitOccupiers(new Long(contractId));
//    	
//        sessionMap.put(WebConstants.OCCUPIER, personViewList);
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    	
//    }
//    
//    private void getPartners(String contractId) throws PimsBusinessException
//    {
//    	String methodName ="getPartners";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	PropertyServiceAgent psa= new PropertyServiceAgent();
//        List<PersonView> personViewList = psa.getContractPartners(new Long(contractId));
//    	
//        sessionMap.put(WebConstants.PARTNER, personViewList);
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    	
//    }
//    private void getPaidFacilities(String contractId) throws PimsBusinessException
//    {
//    	
//    	String methodName ="getPaidFacilities";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	PropertyServiceAgent psa= new PropertyServiceAgent();
//        List<PaidFacilitiesView> paidFacilitiesViewList = psa.getContractUnitPaidFacilities(new Long(contractId));
//    	
//        viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES, paidFacilitiesViewList );
//    	
//    	logger.logInfo(methodName+"|"+"Finish");
//    	
//    	
//    }
//	private void getContractPaymentSchedule(String contractId) throws PimsBusinessException
//	{
//		
//    	String methodName ="getContractPaymentSchedule";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	PropertyServiceAgent psa= new PropertyServiceAgent();
//    	List<PaymentScheduleView> paymentScheduleViewList = psa.getContractPaymentSchedule(new Long(contractId),null);
//    	
//        viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE,paymentScheduleViewList);
//    	
//    	logger.logInfo(methodName+"|"+"Finish...");
//		
//		
//		
//	}
//    
// 
//    public void valueChangeListerner_RentValue()//ValueChangeEvent e)
//    {
//    	
//    	String methodName="valueChangeListerner_RentValue";
//    	logger.logInfo(methodName+"|"+"Start..");
//    	try
//    	{
//    		contractStartDate=getContractStartDate();
//    		contractEndDate=getContractEndDate();
//    		//only add when there is some unit and contract start and end date are not empty
//    		if(( sessionMap.containsKey("UNITINFO") || sessionMap.containsKey("AUCTIONUNITINFO")) 
//    				&& 
//    				//!getIsPageModeUpdate() &&
//    				contractStartDate!=null && !contractStartDate.equals("") 
//					 && contractEndDate!=null && !contractEndDate.equals("")
//			    )
//			{
//    			logger.logDebug(methodName+"|"+"ContractStartDate.."+contractStartDate);
//    			logger.logDebug(methodName+"|"+"ContractEndDate.."+contractEndDate);
//    			
//    			DateFormat df=new SimpleDateFormat(getDateFormat());
//    			String todayDate=df.format(new Date());
//    			logger.logDebug(methodName+"|"+"TodayDate.."+todayDate);
//    			//Contract start date shud only be less than Contract EXPIRY date
//	    		  if (contractStartDate.compareTo(contractEndDate)>=0)
//	    		  {
//	    				errorMessages=new ArrayList<String>();
//	    				errorMessages.clear();
//	    				errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_EXP));
//	    				
//	    		  }
//	    		  else if(viewRootMap.get("pageMode").equals(PAGE_MODE_ADD) && contractStartDate.compareTo(df.parse(todayDate))==-1)
//	    		  {
//	    			  errorMessages=new ArrayList<String>();
//  				      errorMessages.clear();
//				      errorMessages.add(getBundle().getString(MessageConstants.ContractAdd.MSG_CON_START_SMALL_TODAY));
//	    		  }
//	    		  else
//	    		  {
//		    			CalculateTotalRentValue();
//		    			
//		    			//Only add when in add mode
//		    			logger.logInfo(methodName+"|"+"PageMode.."+this.getPageMode() );
//		    			if(this.getPageMode().equals(PAGE_MODE_ADD) )
//		    			{
//		    				addDepositToPaymentScheduleSession();
//		        			//Only For residential property
//		    				logger.logInfo(methodName+"|"+"Is Contract Commercial.."+getIsContractCommercial());
//		    					if(!getIsContractCommercial() && this.getSelectOneContractType()!=null && !this.getSelectOneContractType().equals("-1"))
//				    			{
//				    			//	  addAdministrativeFeeToPaymentScheduleSession();
//		    						DomainDataView ddv = commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.CONDITION),WebConstants.LEASE_CONTRACT_ADMINISTRATIVE_FEE);
//				    			    feesConditionsMap.put(ddv.getDomainDataId(), ddv.getDataValue());
//		    						addFeeToPaymentScheduleSession();
//		    						feesConditionsMap.remove(ddv.getDomainDataId());
//				    			}
//				    			else
//				    			{
//				    				
//				    				//removeAdministrativeFeeFromPaymentScheduleSession();
//				    			}
//		    				
//		    			}
//	    	    } 
//
//			}
//				
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.logError(methodName+"|"+"Error Occured.."+ex);
//    	}
//    	logger.logInfo(methodName+"|"+"Finish..");
//   
//   
//    	
//    }
//    
//    public static int getDaysDifference( Date fromDate, Date toDate )
//    {
//        Calendar calFirst = Calendar.getInstance();
//        Calendar calSecond = Calendar.getInstance();
//
//        calFirst.setTime( fromDate );
//        calSecond.setTime( toDate );
//        // int dayFirst = calFirst.get(calFirst.DAY_OF_YEAR);
//        // int daySecond = calSecond.get(calSecond.DAY_OF_MONTH);
//
//        return getDaysBetween( calFirst, calSecond );
//    }
//
//    public static int getDaysBetween( Calendar d1, Calendar d2 )
//    {
//        if ( d1.after( d2 ) )
//        { // swap dates so that d1 is start and d2 is end
//            Calendar swap = d1;
//            d1 = d2;
//            d2 = swap;
//        }
//        int days = d2.get( Calendar.DAY_OF_YEAR ) - d1.get( Calendar.DAY_OF_YEAR );
//        int y2 = d2.get( Calendar.YEAR );
//        if ( d1.get( Calendar.YEAR ) != y2 )
//        {
//            d1 = (Calendar) d1.clone();
//            do
//            {
//                days += d1.getActualMaximum( Calendar.DAY_OF_YEAR );
//                d1.add( Calendar.YEAR, 1 );
//            } while ( d1.get( Calendar.YEAR ) != y2 );
//        }
//        return days;
//    }
//    
//    public void CalculateTotalRentValue()
//    {
//    	
//    	String methodName="CalculateTotalRentValue";
//    	logger.logInfo(methodName+"|"+"Start..");
//    	try
//    	{
//    		totalContractValue=new Double(0);
//    		Double unitAmount=new Double(0);
//    		List<PaidFacilitiesView> paidFacilitiesViewList=new ArrayList<PaidFacilitiesView>(0);
//    		if(contractStartDate!=null && !contractStartDate.equals("") && contractEndDate!=null && !contractEndDate.equals(""))
//		    {       
//    			int dateDiffDays=1;
//		        dateDiffDays=getDaysDifference(contractStartDate,contractEndDate);
////		        UnitView uv =(UnitView)viewRootMap.get(UNIT_DATA_ITEM);
////		        uv.getRentValue()
//    			   if(unitRentAmount!=null && unitRentAmount.trim().length()>0)	
//		        	{
//    				   if( this.getPageMode().equals(PAGE_MODE_ADD) || this.getPageMode().equals(PAGE_MODE_UPDATE) || this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT) 
//    						   || this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)||this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE) 						   
//    				   )
//    				   {
//    				       logger.logInfo(methodName+"|"+"Units Annual Rent:"+unitRentAmount);
//		        		   unitAmount = getUnitAmuntForSelectedDuration();
//		        		   logger.logInfo(methodName+"|"+"totalContractValue+(unitAmount):"+totalContractValue+"+"+(unitAmount));
//		        		   
//		        		   
//    				   }
//    				   else 
//    					   unitAmount=new Double(unitRentAmount);
//    				   
//		        	  totalContractValue=totalContractValue+(unitAmount);  
//		        	  logger.logInfo(methodName+"|"+"totalContractValue:"+totalContractValue);
//		        		   
//		        		   
//		        	}
////    			   putSelectedFacilitiesInView();
//			        if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) 
//					           && viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null  )		
//			        {		     
//			        	paidFacilitiesViewList =(ArrayList)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
//			            logger.logInfo(methodName+"|"+"Size of selected paid Facilities list:"+paidFacilitiesViewList.size() );
//			        
//				        for (PaidFacilitiesView paidFacilityView : paidFacilitiesViewList) 
//				        {
//				               Double  facilityRentValue = getFacilityAmountForDuration(
//				            		   dateDiffDays, paidFacilityView);
//				               logger.logInfo(methodName+"|"+"totalContractValue+ facilityRentValue:"+totalContractValue+"+"+ facilityRentValue);
//								totalContractValue=totalContractValue+ facilityRentValue;
//								logger.logInfo(methodName+"|"+"totalContractValue:"+totalContractValue);
//						}
//		            }
//			        logger.logInfo(methodName+"|"+"Applying Ceiling to Total Contract Value:"+totalContractValue);
//			        totalContractValue=Math.ceil(totalContractValue);
//			        if(viewRootMap.containsKey(AUCTION_DEPOSIT_AMOUNT) && viewRootMap.get(AUCTION_DEPOSIT_AMOUNT)!=null)
//			        {
//			        	logger.logInfo(methodName+"|"+"Subtracting Auction Deposit Amount from total Contract Value :"+viewRootMap.get(AUCTION_DEPOSIT_AMOUNT).toString());
//			        	totalContractValue= totalContractValue - new Double(viewRootMap.get(AUCTION_DEPOSIT_AMOUNT).toString());
//			        }
//		       }
//		    logger.logInfo(methodName+"|"+"Total Contract Value Calculated::"+totalContractValue);
//		    logger.logInfo(methodName+"|"+"Finish..");
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+"Error Occured..",ex);
//    	}
//    	
//    	
//    }
//
//
//	private Double getFacilityAmountForDuration(int dateDiffDays,PaidFacilitiesView paidFacilityView) 
//	{
//		String methodName="getFacilityAmountForDuration";
//		logger.logInfo(methodName+"|"+"Start:");
//		Double dailyFacilityRentValue =new Double(0);
//        Double facilityRentValue=new Double(0);
//        logger.logDebug(methodName+"|"+"Paid Facility ID:"+paidFacilityView.getPaidFacilitiesId() );
//        logger.logDebug(methodName+"|"+"Paid Facility Name:"+paidFacilityView.getFacilityName() );
//        logger.logDebug(methodName+"|"+"Paid Facility Rent:"+paidFacilityView.getRent() );
//        if(this.getPageMode().equals(PAGE_MODE_ADD) && paidFacilityView.getRent()!=null)
//        {
//			logger.logInfo(methodName+"|"+"Calculating facilities daily rent amount by formula:Facility Rent / 365::::: "+paidFacilityView.getRent()+"/ 365");
//			 dailyFacilityRentValue=paidFacilityView.getRent()/365;
//			logger.logInfo(methodName+"|"+"Value of facilities daily rent amount:: "+dailyFacilityRentValue);
//			  //dailyFacilityRentValue=new Double(paidFacilityView.getRent())/365;
//			  logger.logInfo(methodName+"|"+"Formula for calculating facility rent :Facility Daily Rent * Diff of Contract Start and End Date:: "+dailyFacilityRentValue+" * "+dateDiffDays);
//			  facilityRentValue=dailyFacilityRentValue*dateDiffDays;
//			  
//			  logger.logInfo(methodName+"|"+"Facility rent "+facilityRentValue);
//        }
//        else if(paidFacilityView.getRent()!=null)
//        {
//        	facilityRentValue=new Double(paidFacilityView.getRent());
//        }
//        logger.logInfo(methodName+"|"+"Finish:");
//		return facilityRentValue;
//	}
//
//
//	private Double getUnitAmuntForSelectedDuration()
//	{
//		String methodName="getUnitAmuntForSelectedDuration";
//		Double unitAmount;
//		int dateDiffDays=1;
//		   Double unitDailyRent=new Double(0);
//		   if(this.getPageMode().equals(PAGE_MODE_ADD)|| this.getPageMode().equals(PAGE_MODE_UPDATE)|| this.getPageMode().equals(PAGE_MODE_APPROVE_REJECT) || 
//				   this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD)||this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE))
//		   {
//			  logger.logInfo(methodName+"|"+"Calculating unit's daily rent amount by formula UnitRentAmount/365:"+unitRentAmount+"/365");
//			  
//			   unitDailyRent=new Double(unitRentAmount)/365;
//			  logger.logInfo(methodName+"|"+"Value of unit's daily rent amount by formula:"+unitDailyRent);
//			  logger.logInfo(methodName+"|"+"Contract Start Date:"+contractStartDate);
//			  logger.logInfo(methodName+"|"+"Contract End Date:"+contractEndDate);
//			   dateDiffDays=getDaysDifference(contractStartDate,contractEndDate);
//			  logger.logInfo(methodName+"|"+"Diff in days:"+dateDiffDays);
//			   unitAmount=unitDailyRent* dateDiffDays;
//		   }
//		   else
//			   unitAmount=new Double(unitRentAmount);
//		   logger.logInfo(methodName+"|"+"unitAmount:"+unitAmount);
//		return unitAmount;
//	}
//
//    
//    private String getLoggedInUser() 
//	{
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpSession session = (HttpSession) context.getExternalContext()
//				.getSession(true);
//		String loggedInUser = "";
//
//		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
//			UserDbImpl user = (UserDbImpl) session
//					.getAttribute(WebConstants.USER_IN_SESSION);
//			loggedInUser = user.getLoginId();
//		}
//
//		return loggedInUser;
//	}
//  
//    
//	
//	private void getPaymentScheduleListFromSession()
//	{
//		
//		int rowId = 0;
//		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE) 
//			    && viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE)!=null
//		  )
//		{
//			 //paymentScheduleDataList =(ArrayList)sessionMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			List<PaymentScheduleView> tempPaymentScheduleDataList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			paymentScheduleDataList.clear();
//			DateFormat df= new SimpleDateFormat(getDateFormat());
//			for (PaymentScheduleView paymentScheduleView : tempPaymentScheduleDataList) {
//    			//IF DURING AMEND THE ALREADY EXISTED ID DELETED
//				if(paymentScheduleView.getIsDeleted()==null || paymentScheduleView.getIsDeleted().compareTo(new Long(0))==0)
//				{
//				  paymentScheduleView.setRowId(rowId);
//				  rowId = rowId + 1 ;
//				  paymentScheduleDataList.add(paymentScheduleView);
//			    }
//		}
//		}
//	}
//	
//	public List<PaymentScheduleView> getPaymentScheduleDataList() {
//		
//		
//		//if(paymentScheduleDataList==null )
//		    getPaymentScheduleListFromSession(); 
//		
//					
//			
//				return paymentScheduleDataList;
//		
//		
//	}
//
//	  public List<PaidFacilitiesView> getPaidFacilitiesDataList()
//		{
//			     /*if(sessionMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) 
//		           && sessionMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null 
//		           )*/
////			    	if(viewRootMap.containsKey(WebConstants.PAID_FACILITIES));
////		        	 paidFacilitiesDataList =(ArrayList)viewRootMap.get(WebConstants.PAID_FACILITIES);
////		
////	
////			return paidFacilitiesDataList;
//		  return null;
//		}
//		
//	
//	public List<PersonView> getProspectiveOccupierDataList()
//	{
//		
////		if (sessionMap.containsKey(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER))
////		{
////			prospectiveOccupierDataList = new ArrayList<PersonView>(0);
////			List<PersonView> tempOccupierDataList  = (ArrayList)sessionMap.get(WebConstants.SESSION_PROSPECTIVE_CONTRACT_PERSON_OCCUPIER); 
////			  for (PersonView personView : tempOccupierDataList  ) {
////				if(personView.getIsDeleted().compareTo(new Long(1))!=0)
////					prospectiveOccupierDataList.add(personView);
////			  }
////		}
//
//		if (sessionMap.containsKey(WebConstants.OCCUPIER))
//		{
//			prospectiveOccupierDataList = new ArrayList<PersonView>(0);
//			List<PersonView> tempOccupierDataList  = (ArrayList)sessionMap.get(WebConstants.OCCUPIER); 
//			  for (PersonView personView : tempOccupierDataList  ) {
//				if(personView.getIsDeleted().compareTo(new Long(1))!=0)
//					prospectiveOccupierDataList.add(personView);
//			  }
//		}
//		return prospectiveOccupierDataList ;
//	}
//	private boolean isUnitValid()
//	{
//		String method="isUnitValid";
//		boolean isUnitValid=false;
//		logger.logInfo(method+"|"+"Start..");
//		List<DomainDataView> ddl= getDomainDataListForDomainType(WebConstants.UNIT_STATUS);
//		String[] validUnitStautses=WebConstants.VALID_UNITS_STATUSES.split(",");
//		//For update mode this criteria should not be checked as 
//		//unit would be in rented state after approval  
//
////		if(pageMode.equals(PAGE_MODE_ADD))
////		{
////			for(int j=0;j<validUnitStautses.length;j++)
////			{
////				HashMap unitStatusmap=getIdFromType(ddl, validUnitStautses[j]);
////				if(this.getUnitStatusId().equals(unitStatusmap.get("returnId").toString()))
////				{
////					isUnitValid=true;
////					break;
////				}
////				
////				
////			}
////		}
////		//if page mode is other than add specifically entered for PAGE_MODE_AMEND
////		else
//			isUnitValid=true;
//		
//		logger.logInfo(method+"|"+"Finish..");
//		return isUnitValid;
//	}
//
//	public void tabPaymentTerms_Click()
//	{
//		String methodName="tabPaymentTerms_Click";
//		logger.logInfo(methodName+"|Start");
//		valueChangeListerner_RentValue();
//		calculatePaymentScheduleSummaray();
//		logger.logInfo(methodName+"|Finish");
//		
//		
//	}
//	public String getRentId()
//	{
//		return WebConstants.PAYMENT_TYPE_RENT_ID.toString();
//		
//	}
//	
//	private void CollectPayments()
//	{
//		String methodName = "CollectPayments";
//		logger.logInfo(methodName+"|Start");
//		successMessages = new ArrayList<String>(0);
//		successMessages.clear();
//		errorMessages = new ArrayList<String>(0);
//		errorMessages.clear();
//		try
//		{
//			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.get(WebConstants.PAYMENT_RECEIPT_VIEW);
//		     sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
//		     PropertyServiceAgent psa=new PropertyServiceAgent();
//		 
////		     Set<FinancialTransactionView> ftvSet=new HashSet<FinancialTransactionView>(0); 
////		     ftvSet.add(getFinancialTransactionView(prv.getAmount(),prv.getPaymentScheduleView().getTypeId()));
////		     prv.setFinancialTransactionsView(ftvSet);			     
//		     psa.collectPayments(prv);
//		     DomainDataView ddvContractStatusActive = commonUtil.getIdFromType((List<DomainDataView>)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS),WebConstants.CONTRACT_STATUS_ACTIVE);
//		     
//		     if(!getIsContrctStatusActive())
//		        ApproveRejectTask(TaskOutcome.COMPLETE);
//		     if(contractView.getStatus().compareTo(ddvContractStatusActive.getDomainDataId())!=0)
//		     {
//		    	 
//		    	 new PropertyServiceAgent().setContractAsActive(contractView.getContractId());
//		    	 contractView.setStatus(ddvContractStatusActive.getDomainDataId());
//		    	 viewRootMap.put("CONTRACTVIEWUPDATEMODE",contractView);
//		     }
//		     saveSystemComments(MessageConstants.ContractEvents.CONTRACT_PAYMENT_COLLECTED);
//		     successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_PAYMENT_RECIVED));
//		     logger.logInfo(methodName + "|" + "Finish...");
//		}
//		catch(Exception ex)
//		{
//			logger.logError(methodName + "|" + "Exception Occured..."+ex);
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//		}
//	}
//	public void calculatePaymentScheduleSummaray()
//	{
//		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//		{
//			
//			List<PaymentScheduleView> paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
//		Long cashId = ddvCash.getDomainDataId();
//		DomainDataView ddvCheque = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CHEQUE);
//		Long chqId = ddvCheque.getDomainDataId();
//		
//		this.setTotalCashAmount(0) ;
//		this.setTotalChqAmount(0) ;
//		this.setTotalRentAmount(0) ;
//		this.setTotalDepositAmount(0) ;
//		this.setTotalFines(0) ;
//		this.setTotalFees(0);
//		this.setInstallments(0) ;
//		
//		for (int i=0; i<paymentSchedules.size(); i++)
//		{
//			//Checking Type
//			if (paymentSchedules.get(i).getIsDeleted().compareTo(new Long(0))==0)
//			{
//				if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_RENT_ID.longValue())
//				{
//					installments++;
//					this.setTotalRentAmount(this.getTotalRentAmount()  + paymentSchedules.get(i).getAmount());
//				}
//				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_DEPOSIT_ID.longValue())
//				{
//					this.setTotalDepositAmount(this.getTotalDepositAmount() + paymentSchedules.get(i).getAmount());
//				}
//				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FEES_ID.longValue())
//				{
//					this.setTotalFees(this.getTotalFees() + paymentSchedules.get(i).getAmount());
//				}
//				else if (paymentSchedules.get(i).getTypeId().longValue() == WebConstants.PAYMENT_TYPE_FINE_ID.longValue())
//				{
//					this.setTotalFines( this.getTotalFines()  + paymentSchedules.get(i).getAmount());
//				}
//				
//				//Checking Mode
//				if (paymentSchedules.get(i).getPaymentModeId() != null)
//				{
//						if (paymentSchedules.get(i).getPaymentModeId().longValue() == cashId.longValue())
//						{
//							this.setTotalCashAmount(this.getTotalCashAmount()  +  paymentSchedules.get(i).getAmount());
//						}
//						else if (paymentSchedules.get(i).getPaymentModeId().longValue() == chqId.longValue())
//						{
//							this.setTotalChqAmount( this.getTotalChqAmount() + paymentSchedules.get(i).getAmount());
//						}
//					}
//			}
//		  }
//		}
//	}
//	
//	private List<PaymentScheduleView> getPaymentScheduleFromPaymentConfiguration(Long paymentType,HashMap conditionsMap) throws PimsBusinessException,Exception
//	{
//		PropertyServiceAgent psa = new PropertyServiceAgent();
//		Long ownerShipTypeId = new Long(viewRootMap.get(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID).toString());
//		if(totalContractValue!=null && totalContractValue>0)
//		 return psa.getPaymentScheduleFromPaymentConfiguration(WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT, paymentType, conditionsMap, totalContractValue, getIsEnglishLocale(),ownerShipTypeId);
//		else 
//		 return null;
//		
//	}
//	private void addFeeToPaymentScheduleSession() throws PimsBusinessException,Exception
//	{
//		PaymentScheduleView psv=new PaymentScheduleView();
//		PropertyServiceAgent psa = new PropertyServiceAgent();
//		List<PaymentScheduleView> NewpaymentScheduleViewList=new ArrayList();
//		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//		{
//			List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			paymentScheduleViewList = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_FEES_ID);
//			NewpaymentScheduleViewList.addAll(paymentScheduleViewList);
//		}
//		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
//		List<PaymentScheduleView>  feesList =getPaymentScheduleFromPaymentConfiguration(WebConstants.PAYMENT_TYPE_FEES_ID,feesConditionsMap) ;
//		if(feesList!=null && feesList.size()>0)
//			for (PaymentScheduleView paymentScheduleView : feesList) {
//				paymentScheduleView.setPaymentModeId(ddvCash.getDomainDataId());
//				paymentScheduleView.setPaymentModeEn(ddvCash.getDataDescEn());
//				paymentScheduleView.setPaymentModeAr(ddvCash.getDataDescAr());
//				NewpaymentScheduleViewList.add(paymentScheduleView);
//			}
//		 //NewpaymentScheduleViewList.addAll(feesList);
//		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, NewpaymentScheduleViewList);
//		getPaymentScheduleListFromSession();
//	}
//	private void addDepositToPaymentScheduleSession() throws PimsBusinessException,Exception
//	{
//		PaymentScheduleView psv=new PaymentScheduleView();
//		
//		List<PaymentScheduleView> NewpaymentScheduleViewList=new ArrayList();
//		DomainDataView ddvCash = (DomainDataView)viewRootMap.get(WebConstants.PAYMENT_SCHEDULE_MODE_CASH);
//		if(viewRootMap.containsKey(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE))
//		{
//			List<PaymentScheduleView> paymentScheduleViewList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
//			paymentScheduleViewList = removePaymentTypeFromList(paymentScheduleViewList,WebConstants.PAYMENT_TYPE_DEPOSIT_ID);
//			NewpaymentScheduleViewList.addAll(paymentScheduleViewList);
//		}
//		List<PaymentScheduleView>  depositsList = getPaymentScheduleFromPaymentConfiguration(WebConstants.PAYMENT_TYPE_DEPOSIT_ID,depositConditionsMap) ;
//		if(depositsList!=null && depositsList.size()>0)
//			for (PaymentScheduleView paymentScheduleView : depositsList) {
//				paymentScheduleView.setPaymentModeId(ddvCash.getDomainDataId());
//				paymentScheduleView.setPaymentModeEn(ddvCash.getDataDescEn());
//				paymentScheduleView.setPaymentModeAr(ddvCash.getDataDescAr());
//				NewpaymentScheduleViewList.add(paymentScheduleView);
//			}
//		 //NewpaymentScheduleViewList.addAll(depositsList);
//		viewRootMap.put(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE, NewpaymentScheduleViewList);
//		getPaymentScheduleListFromSession();
//		
//	}
//	private List<PaymentScheduleView> removePaymentTypeFromList(List<PaymentScheduleView> paymentScheduleViewList,Long paymentType)
//	{
//		
//		List<PaymentScheduleView> NewpaymentScheduleViewList = new ArrayList<PaymentScheduleView>(0); 
//		for(int i=0;i<paymentScheduleViewList.size();i++)
//		{
//				PaymentScheduleView psv =(PaymentScheduleView)paymentScheduleViewList.get(i);
//				if(psv.getTypeId().compareTo(paymentType)!=0 )
//				{
//					NewpaymentScheduleViewList.add(psv);	
//				}
//		}
//		return NewpaymentScheduleViewList;
//	}
//	
//	
//	public HashMap getIdFromType(List<DomainDataView> ddv ,String type)
//	{
//		String methodName="getIdFromType";
//		logger.logDebug(methodName+"|"+"Start...");
//		logger.logDebug(methodName+"|"+"DomainDataType To search..."+type);
//		Long returnId =new Long(0);
//		HashMap hMap=new HashMap();
//		for(int i=0;i<ddv.size();i++)
//		{
//			DomainDataView domainDataView=(DomainDataView)ddv.get(i);
//			if(domainDataView.getDataValue().equals(type))
//			{
//				logger.logDebug(methodName+"|"+"DomainDataId..."+domainDataView.getDomainDataId());
//			   	hMap.put("returnId",domainDataView.getDomainDataId());
//			   	logger.logDebug(methodName+"|"+"DomainDataDesc En..."+domainDataView.getDataDescEn());
//			   	hMap.put("IdEn",domainDataView.getDataDescEn());
//			   	hMap.put("IdAr",domainDataView.getDataDescAr());
//			}
//			
//		}
//		logger.logDebug(methodName+"|"+"Finish...");
//		return hMap;
//		
//	}
//	public HtmlDataTable getPropspectivepartnerDataTable() {
//		return propspectivepartnerDataTable;
//	}
//
//	public void tabAttachmentsComments_Click()
//	{
//		
//		String methodName="tabAttachmentsComments_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		try
//		{
//			if( viewRootMap.containsKey("REQUESTVIEWUPDATEMODE") && viewRootMap.get("REQUESTVIEWUPDATEMODE")!=null)
//			requestView  = (RequestView)viewRootMap.get("REQUESTVIEWUPDATEMODE");
//			if(viewRootMap.containsKey("AMEND_CONTRACT_REQUEST_VIEW") && viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW")!=null && 
//					requestView ==null || requestView.getRequestId()==null )
//			requestView  = (RequestView)viewRootMap.get("AMEND_CONTRACT_REQUEST_VIEW");
//			if(requestView !=null && requestView.getRequestId()!=null)
//	    	     loadAttachmentsAndComments(requestView.getRequestId());
//		}
//		catch (Exception ex)
//		{
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//		logger.logInfo(methodName+"|"+"Finish...");
//		  
//	}
//	public void tabCommercialActivity_Click()
//	{
//		String methodName="tabCommercialActivity_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		try
//		{
//					if(!viewRootMap.containsKey("SESSION_CONTRACT_COMMERCIAL_ACTIVITY"))
//				   CommercialActivityController.getAllCommercialActivities(contractId);
//		    //sessionMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityList);
//		}
//		catch (Exception ex)
//		{
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//		logger.logInfo(methodName+"|"+"Finish...");
//	}
//    public void chkPaidFacilities_Changed()
//    {
//    	String methodName="chkPaidFacilities_Changed";
//		logger.logInfo(methodName+"|"+"Start...");
////		PaidFacilitiesView pfv=(PaidFacilitiesView)paidFacilitiesDataTable.getRowData();
////		logger.logDebug(methodName+"|"+"Facility Name..."+pfv.getFacilityName());
////		logger.logDebug(methodName+"|"+"Facility ID..."+pfv.getFacilityId());
//		
//		if(this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD))
//		{
//		  
////			processPaidFacilitiesForAmend(pfv);
//			
//		}
//		
//		logger.logInfo(methodName+"|"+"Finish...");
//    }
//    private void processPaidFacilitiesForAmend(PaidFacilitiesView pfv)
//    {
//    	String methodName="processPaidFacilitiesForAmend";
//    	logger.logInfo(methodName+"|"+"Start...");
//    	int dateDiffDays=1;
//    	logger.logDebug(methodName+"|"+"If Facility is selected ..."+pfv.getSelected());
//    	//if facility has been selected
//    	
//    	if(pfv.getSelected())
//    	{
//    		logger.logDebug(methodName+"|"+"If Facility is deleted ..."+pfv.getIsDeleted());
//    		//if this facility is being added for the first time
//    		//in contract.
//    		if(pfv.getNewFacility())//pfv.getIsDeleted().compareTo(new Long(0))==0)
//    		{
//    			
//    			//pfv.setIsDeleted(new Long(0));	
//    			
//    			//Since this facility is being added from Now, so its rent amount should be calculated
//    			//from Now to ContractEndDate
//		        addToSelectedFacilities(pfv);
//    			
//    			
//    		}
//    		//if  this facility was binded with contract before amendment
//    		//i.e. this facility was selected before this amendment,
//    		//then unselected during this process and now again has been selected
//    		else
//    		{
//    			pfv.setRent(pfv.getOriginalRent());
//    			pfv.setIsDeleted(new Long(0));
//    			
//    		}
//    	}
//    	//if facility has been marked for deletion
//    	else
//    	{
//    		//if this facility was binded with contract before amendment
//    		if(!pfv.getNewFacility())
//    		{
//	    		pfv.setIsDeleted(new Long(1));
//	    		//Since this facility is being deleted from Now so its rent amount should be calculated
//				//from ContractStartDate to Now
//		    
//	    		  dateDiffDays=getDaysDifference(contractStartDate,new Date());
//	    		  logger.logDebug(methodName+"|"+"Facility's Original Rent..."+pfv.getOriginalRent());
//	    		  //Setting rent of facility back to the original rent 
//	    		  //bcz current rent in paidFacility was set according to duration
//	    		  //selected in at time of contract creation
//	    		  pfv.setRent(pfv.getOriginalRent());
//			        Double  facilityRentValue = getFacilityAmountForDuration(
//		            		   dateDiffDays, pfv);
//		            pfv.setRent(facilityRentValue);
//    		}
//    		//if this facility was not binded with contract before amendment and selected during the
//    		//the process and again unselected,so removing it from ViewRootMap
//    		if(pfv.getNewFacility())
//    		{
//    			pfv.setRent(pfv.getOriginalRent());
//    			removeFromSelectedFacilities(pfv);
//    		}
//  			
//    	}
//    	logger.logInfo(methodName+"|"+"Finish...");
//    }
//    private void addToSelectedFacilities(PaidFacilitiesView pfv)
//    {
//    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
//      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES) && viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES)!=null)
//    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
//       
//      	paidFacilitiesViewList.add(pfv);
//      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,paidFacilitiesViewList);
//    
//    }
//    private void removeFromSelectedFacilities(PaidFacilitiesView pfv)
//    {
//    	List<PaidFacilitiesView> paidFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
//    	List<PaidFacilitiesView> newFacilitiesViewList =new ArrayList<PaidFacilitiesView>();
//      	if(viewRootMap.containsKey(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES))
//    	 paidFacilitiesViewList =(ArrayList<PaidFacilitiesView>)viewRootMap.get(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES);
//        for (PaidFacilitiesView paidFacilitiesView : paidFacilitiesViewList) 
//        {
//        	if(pfv.getFacilityId().compareTo(paidFacilitiesView.getFacilityId())!=0)
//		       newFacilitiesViewList.add(paidFacilitiesView);
//        }
//        //paidFacilitiesViewList.subList(fromIndex, toIndex)
//        //Collections.
//      	viewRootMap.put(WebConstants.CONTRACT_SELECTED_PAID_FACILITIES,newFacilitiesViewList);
//    
//    }
//	public void tabPaidFacility_Click()
//	{
//		String methodName="tabPaidFacility_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		List<PaidFacilitiesView> facilityViewList=new ArrayList<PaidFacilitiesView>(0);
//		if(errorMessages==null)
//		 errorMessages=new ArrayList<String>(0);
//        try
//		{
//					if(!viewRootMap.containsKey(WebConstants.PAID_FACILITIES))
//					{
//						facilityViewList=	 getAllPaidFacilities();
//						for (PaidFacilitiesView paidFacilitiesView : facilityViewList) {
//							if(paidFacilitiesView.getSelected())
//								addToSelectedFacilities(paidFacilitiesView);
//							
//						}
//		                viewRootMap.put(WebConstants.PAID_FACILITIES, facilityViewList);
//					}
//		}
//		catch (Exception ex)
//		{
//			
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//		logger.logInfo(methodName+"|"+"Finish...");
//		
//	}
//	public void tabRequestHistory_Click()
//	{
//		
//		String methodName="tabRequestHistory_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		
//		try
//		{
//			errorMessages=new ArrayList<String>(0);
//	    	
//	    	logger.logInfo(methodName+"|"+"Finish...");
//		}
//		catch(Exception ex)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//		
//	}
//	public void btnContractHistory_Click()
//	{
//		
//		String methodName="tabContractHistory_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		
//		try
//		{
//			errorMessages=new ArrayList<String>(0);
//	    	String javaScriptText ="var screen_width = screen.width;"+
//	        "var screen_height = screen.height;"+
//	        "window.open('ContractHistoryPopUp.jsf?contractId="+contractId+
//	        "','_blank','width='+(screen_width-10)+',height='+(screen_height-260)+',left=0,top=40,scrollbars=yes,status=yes');";
//                
//	        openPopUp("ContractHistoryPopUp.jsf?contractId="+contractId+"'", javaScriptText);
//
//	    	logger.logInfo(methodName+"|"+"Finish...");
//		}
//		catch(Exception ex)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//		
//	}
//	public void saveSystemComments(String sysNoteType) throws Exception
//    {
//    	String methodName="saveSystemComments|";
//    	try{
//	    	logger.logInfo(methodName + "started...");
//	    	
//    		String notesOwner = WebConstants.REQUEST;
//    		logger.logInfo(methodName + "contractId..."+contractId);
//    		if(contractId!=null && contractId.trim().length()>0)
//    		{
//    		  Long contractsId = new Long(contractId);
//    		  logger.logInfo(methodName + "notesOwner..."+notesOwner);
//	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, contractsId );
//	    	logger.logInfo(methodName + "completed successfully!!!");
//    		}
//	    	
//    	}
//    	catch (Exception exception) {
//			logger.LogException(methodName + "crashed ", exception);
//			throw exception;
//		}
//    }
//
//	public void tabAuditTrail_Click()
//    {
//    	String methodName="tabAuditTrail_Click";
//    	logger.logInfo(methodName+"|"+"Start..");
//    	try	
//    	{
//    	  RequestHistoryController rhc=new RequestHistoryController();
//    	  if(contractId!=null && contractId.trim().length()>=0)
//    	    rhc.getAllRequestTasksForRequest(WebConstants.CONTRACT,contractId);
//    	}
//    	catch(Exception ex)
//    	{
//    		logger.LogException(methodName+"|"+"Error Occured..",ex);
//    	}
//    	logger.logInfo(methodName+"|"+"Finish..");
//    }
//	public void tabBasicInfo_Click()
//	{
//		String methodName="tabBasicInfo_Click";
//		logger.logInfo(methodName+"|"+"Start...");
//		
//		try
//		{
//			errorMessages=new ArrayList<String>(0);
//			valueChangeListerner_RentValue();
//	    	logger.logInfo(methodName+"|"+"Finish...");
//		}
//		catch(Exception ex)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//			logger.LogException(methodName+"|"+"Exception Occured...",ex);
//		}
//
//		
//		
//	}
//	
//	
//	
//	private  List<PaidFacilitiesView> getAllPaidFacilities() throws PimsBusinessException
//    {
//    	String methodName="getAllPaidFacilities";
//		logger.logInfo(methodName+"|"+"Start...");
//        PropertyServiceAgent psa=new PropertyServiceAgent();
//        Long conId=contractId!=null && contractId.length()>0? new Long(contractId): null;
//        Long propertyId=new Long(0);
//        if(sessionMap.containsKey("UNITINFO"))
//		{
//		 UnitView unitView=(UnitView)sessionMap.get("UNITINFO");
//		 propertyId=unitView.getPropertyId();
//		}
//		else if(sessionMap.containsKey("AUCTIONUNITINFO"))
//		{
//			AuctionUnitView auctionUnitView=(AuctionUnitView)sessionMap.get("AUCTIONUNITINFO");
//			propertyId=auctionUnitView.getPropertyId();
//		}
//        logger.logInfo(methodName+"|"+"PropertyId..."+propertyId);
//        //List<PaidFacilitiesView> facilityViewList =psa.getFacilitiesByFacilityTypeId(new Long(WebConstants.FACILITY_TYPE_PAID),conId);
//        
//        
//        List<PaidFacilitiesView> facilityViewList =psa.getPropertyFacilitiesForContractByPropertyId(propertyId,conId);
//        logger.logInfo(methodName+"|"+"Finish...");
//    	return facilityViewList;
//        
//    }
////    private List<CommercialActivityView> getAllCommercialActivities() throws PimsBusinessException
////    {
////    	String methodName="getAllCommercialActivities";
////		logger.logInfo(methodName+"|"+"Start...");
////        PropertyServiceAgent psa=new PropertyServiceAgent();
////        List<CommercialActivityView> commercialActivityViewList =psa.getAllCommercialActivities(contractId);
////        viewRootMap.put("SESSION_CONTRACT_COMMERCIAL_ACTIVITY", commercialActivityViewList);
////		logger.logInfo(methodName+"|"+"Finish...");
////    	return commercialActivityViewList;
////    }
//    
//    private Boolean generateNotification(String eventName)
//    {
//    	   String methodName ="generateNotification";
//    	      Boolean success = false;
//    	            try
//    	            {
//    	                  logger.logInfo(methodName+"|Start");
//    	                  HashMap placeHolderMap = new HashMap();
//    	                  contractView = (ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	                  List<ContactInfo> tenantsEmailList    = new ArrayList<ContactInfo>(0);
//    	                  List<ContactInfo> applicantsEmailList = new ArrayList<ContactInfo>(0);
//    	                  NotificationFactory nsfactory = NotificationFactory.getInstance();
//    	                  NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
//    	                  Event event = EventCatalog.getInstance().getMetaEvent(eventName).createEvent();
//    	                  getNotificationPlaceHolder(event);
//    	                  if(viewRootMap.containsKey(TENANT_INFO))
//    	                  {
//    	                        tenantsEmailList = getEmailContactInfos((PersonView)viewRootMap.get(TENANT_INFO));
//    	                        if(tenantsEmailList.size()>0)
//    	                              notifier.fireEvent(event, tenantsEmailList);    
//    	                  }     
//    	                  if(hdnTenantId != hdnApplicantId && 
//    	                              viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW))
//    	                  {
//    	                      applicantsEmailList = getEmailContactInfos((PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW));
//    	                      if(applicantsEmailList.size()>0)
//    	                         notifier.fireEvent(event, applicantsEmailList);
//    	                  }
//    	                  success  = true;
//    	                  logger.logInfo(methodName+"|Finish");
//    	            }
//    	            catch(Exception ex)
//    	            {
//    	                  logger.LogException(methodName+"|Finish", ex);
//    	            }
//    	            return success;
//    }
//    private List<ContactInfo> getEmailContactInfos(PersonView personView)
//    {
//    	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
//    	Iterator iter = personView.getContactInfoViewSet().iterator();
//    	HashMap previousEmailAddressMap = new HashMap();
//    	while(iter.hasNext())
//    	{
//    		ContactInfoView ciView = (ContactInfoView)iter.next();
//    		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
//    		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
//    				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
//            {
//    			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
//    			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
//    			//emailList.add(ciView);
//            }
//    	}
//    	return emailList;
//    }
//	private void  getNotificationPlaceHolder(Event placeHolderMap)
//	{
//		String methodName = "getNotificationPlaceHolder";
//		logger.logDebug(methodName+"|Start");
//		//placeHolderMap.setValue("CONTRACT", contractView);
//		placeHolderMap.setValue("TENANT_NAME",contractView.getTenantView().getPersonFullName());
//		placeHolderMap.setValue("CONTRACT_NO",contractView.getContractNumber());
//		placeHolderMap.setValue("CONTRACT_START_DATE",contractView.getStartDateString());						
//		placeHolderMap.setValue("CONTRACT_END_DATE",contractView.getEndDateString());
//		if(sessionMap.containsKey("UNITINFO"))
//		{
//		 UnitView unitView = (UnitView)sessionMap.get("UNITINFO");
//		 placeHolderMap.setValue("CONTRACT_UNIT_NO",unitView.getUnitNumber()); 
//		 placeHolderMap.setValue("CONTRACT_UNIT_ADDRESS",unitView.getUnitAddress());
//		 placeHolderMap.setValue("CONTRACT_UNIT_PROPERTY_NAME",unitView.getPropertyCommercialName());
//		}
//		logger.logDebug(methodName+"|Finish");
//		
//	}
//	public void setPropspectivepartnerDataTable(
//			HtmlDataTable propspectivepartnerDataTable) {
//		this.propspectivepartnerDataTable = propspectivepartnerDataTable;
//	}
//
//	
//
//	public List<PersonView> getProspectivepartnerDataList() {
// 
//		if (sessionMap.containsKey(WebConstants.PARTNER))
//		{
//		    prospectivepartnerDataList=new ArrayList<PersonView>(0);
//			List<PersonView> tempPartnerDataList  = (ArrayList)sessionMap.get(WebConstants.PARTNER); 
//		  for (PersonView personView : tempPartnerDataList ) {
//			if(personView.getIsDeleted().compareTo(new Long(1))!=0)
//			prospectivepartnerDataList.add(personView);
//		}
//		}
//		
//		return prospectivepartnerDataList;
//	}
//
//
//	public void setProspectivepartnerDataList(
//			List<PersonView> prospectivepartnerDataList) {
//		this.prospectivepartnerDataList = prospectivepartnerDataList;
//	}
//
//
//	public PersonView getProspectivepartnerDataItem() {
//		return prospectivepartnerDataItem;
//	}
//
//   
//	public void setProspectivepartnerDataItem(PersonView prospectivepartnerDataItem) {
//		this.prospectivepartnerDataItem = prospectivepartnerDataItem;
//	}
// 
//	public String openPopUp(String URLtoOpen,String extraJavaScript)
//	{
//		String methodName="openPopUp";
//        final String viewId = "/LeaseContract.jsf";
//		logger.logInfo(methodName+"|"+"Start..");
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
//        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
//        String javaScriptText ="var screen_width = screen.width;"+
//                               "var screen_height = screen.height;"+
//                               "window.open('"+URLtoOpen+"','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');";
//        if(extraJavaScript!=null && extraJavaScript.length()>0)
//        javaScriptText=extraJavaScript;
//        
//        AddResource addResource = AddResourceFactory.getInstance(facesContext);
//        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
//        
//                //AtPosition(facesContext, AddResource., javaScriptText)
//		logger.logInfo(methodName+"|"+"Finish..");
//		return "";
//	}
//
//	
//	public String openUnitsPopUp()
//	{
//		String methodName="openUnitsPopUp";
//		logger.logInfo(methodName+"|"+"Start..");
//        //WebConstants.UNIT
// 
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        if(sessionMap.containsKey("AUCTIONUNITINFO"))
//        	sessionMap.remove("AUCTIONUNITINFO");
//      
//        auctionRefNum="";
//        hdnAuctionunitId="";
//        String javaScriptText ="var screen_width = screen.width;"+
//        "var screen_height = screen.height;"+
//        "window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-260)+',left=0,top=40,scrollbars=yes,status=yes');";
//
//        openPopUp("UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP", javaScriptText);
//          
//		logger.logInfo(methodName+"|"+"Finish..");
//		return "";
//	}
//	public String openAuctionUnitsPopUp()
//	{
//		String methodName="openUnitsPopUp";
//		logger.logInfo(methodName+"|"+"Start..");
//
// 
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        /*unitRefNum="";
//        unitRentAmount="";
//        hdnUnitId="";
//        hdnUnitUsuageTypeId="";
//        */
//        String javaScriptText ="var screen_width = screen.width;"+
//        "var screen_height = screen.height;"+
//        "window.open('AllWonAuctions.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=yes,status=yes');";
//
//        if(sessionMap.containsKey("UNITINFO"))
//        	sessionMap.remove("UNITINFO");
//        
//        openPopUp("AllWonAuctions.jsf", javaScriptText);
//        
//		logger.logInfo(methodName+"|"+"Finish..");
//		return "";
//	}
//
//   
//	public String getRefNum() {
//		if(viewRootMap.containsKey("refNum") && viewRootMap.get("refNum")!=null)
//			refNum = viewRootMap.get("refNum").toString();
//		return refNum;
//	}
//
//
//	public void setRefNum(String refNum) {
//		this.refNum = refNum;
//		if(this.refNum!=null)
//			 viewRootMap.put("refNum",this.refNum);
//			
//	}
//
//
//	public String getDatetimerequest() {
//       
//		if(viewRootMap.containsKey("datetimerequest") && viewRootMap.get("datetimerequest")!=null)
//			datetimerequest = viewRootMap.get("datetimerequest").toString();
//		return datetimerequest;
//	}
//
//
//	public void setDatetimerequest(String datetimerequest) {
//		this.datetimerequest = datetimerequest;
//		if(this.datetimerequest!=null)
//			viewRootMap.put("datetimerequest",this.datetimerequest);
//			
//	}
//
//	public String getTenantRefNum() {
//		if(viewRootMap.containsKey("tenantRefNum") && viewRootMap.get("tenantRefNum")!=null)
//			tenantRefNum = viewRootMap.get("tenantRefNum").toString();
//		return tenantRefNum;
//	}
//
//
//	public void setTenantRefNum(String tenantRefNum) {
//		this.tenantRefNum = tenantRefNum;
//		if(this.tenantRefNum  !=null)
//			viewRootMap.put("tenantRefNum",this.tenantRefNum );
//	}
//
//
//	public String getUnitRefNum() {
//		if(viewRootMap.containsKey("unitRefNum") && viewRootMap.get("unitRefNum")!=null)
//			unitRefNum= viewRootMap.get("unitRefNum").toString();
//		return unitRefNum;
//	}
//
//
//	public void setUnitRefNum(String unitRefNum) {
//		this.unitRefNum = unitRefNum;
//		if(this.unitRefNum!=null)
//			viewRootMap.put("unitRefNum",this.unitRefNum);
//	}
//
//
//	public String getAuctionRefNum() {
//		if(viewRootMap.containsKey("auctionRefNum") && viewRootMap.get("auctionRefNum")!=null)
//			auctionRefNum = viewRootMap.get("auctionRefNum").toString();
//		return auctionRefNum;
//	}
//
//
//	public void setAuctionRefNum(String auctionRefNum) {
//		this.auctionRefNum = auctionRefNum;
//		if(this.auctionRefNum !=null)
//			viewRootMap.put("auctionRefNum",this.auctionRefNum);
//	}
//
//
//	public HtmlInputText getTxtRefNum() {
//		return txtRefNum;
//	}
//
//
//	public void setTxtRefNum(HtmlInputText txtRefNum) {
//		this.txtRefNum = txtRefNum;
//	}
//
//
//
//
//	public List<SelectItem> getStatusList()
//	{
//		statusList.clear();
//		List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
//		for(int i=0;i<ddvList.size();i++)
//		{
//			SelectItem item=null; 
//			DomainDataView ddv=(DomainDataView)ddvList.get(i);
//			//if page mode is in add mode than take the contract status as new
//			if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
//			{
//			  if(getIsArabicLocale())
//			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
//			  else if(getIsEnglishLocale())
//				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
//            		
//			  statusList.add(item);
//			  break;
//			}
//
//			// IF PAGE IS IN UPDATE MODE 
//			else if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_UPDATE) )
//			{
//				  if(getIsArabicLocale())
//				   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
//				  else if(getIsEnglishLocale())
//					item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
//				  statusList.add(item);
//				}
//			
//			
//			
//		}
//		
//		return statusList;
//	}
//
//
//	public void setStatusList(List<SelectItem> statusList) {
//		this.statusList = statusList;
//	}
//
//
//	public String getHdnUnitId() {
//		if(viewRootMap.containsKey("hdnUnitId") && viewRootMap.get("hdnUnitId")!=null)
//			hdnUnitId = viewRootMap.get("hdnUnitId").toString();
//		return hdnUnitId;
//	}
//
//
//	public void setHdnUnitId(String hdnUnitId) {
//		this.hdnUnitId = hdnUnitId;
//		if(this.hdnUnitId!=null)
//			viewRootMap.put("hdnUnitId", this.hdnUnitId);
//	}
//
//
//	public String getHdnTenantId() {
//		if(viewRootMap.containsKey("hdnTenantId") && viewRootMap.get("hdnTenantId")!=null)
//			hdnTenantId = viewRootMap.get("hdnTenantId").toString();
//		return hdnTenantId;
//	}
//
//
//	public void setHdnTenantId(String hdnTenantId) {
//		this.hdnTenantId = hdnTenantId;
//		if(this.hdnTenantId!=null)
//			viewRootMap.put("hdnTenantId", this.hdnTenantId);
//	}
//
//
//	public HtmlPanelTabbedPane getPanelTabbedPane() {
//		return panelTabbedPane;
//	}
//
//
//	public void setPanelTabbedPane(HtmlPanelTabbedPane panelTabbedPane) {
//		this.panelTabbedPane = panelTabbedPane;
//	}
//
//
//	public HtmlPanelTab getPanelTab() {
//		return panelTab;
//	}
//
//
//	public void setPanelTab(HtmlPanelTab panelTab) {
//		this.panelTab = panelTab;
//	}
//
//
//	public String getPersonSponsor() {
//		personSponsor=WebConstants.SPONSOR;
//		return personSponsor;
//	}
//
//
//	public void setPersonSponsor(String personSponsor) {
//		
//		this.personSponsor = personSponsor;
//	}
//
//
//	public String getPersonManager() {
//		personManager=WebConstants.MANAGER;
//		return personManager;
//	}
//
//
//	public void setPersonManager(String personManager) {
//		this.personManager = personManager;
//	}
//
//
//	public String getPersonOccupier() {
//		personOccupier=WebConstants.OCCUPIER;
//		return personOccupier;
//	}
//	public String getPersonTenant() {
//		
//		return WebConstants.PERSON_TYPE_TENANT;
//	}
//
//	public void setPersonOccupier(String personOccupier) {
//		this.personOccupier = personOccupier;
//	}
//
//
//	public String getPersonPartner() {
//		personPartner=WebConstants.PARTNER;
//		return personPartner;
//	}
//
//
//	public void setPersonPartner(String personPartner) {
//		this.personPartner = personPartner;
//	}
//
//
//	public PersonView getProspectiveOccupierDataItem() {
//		return prospectiveOccupierDataItem;
//	}
//
//
//	public void setProspectiveOccupierDataItem(
//			PersonView prospectiveOccupierDataItem) {
//		this.prospectiveOccupierDataItem = prospectiveOccupierDataItem;
//	}
//
//
//	public void setProspectiveOccupierDataList(
//			List<PersonView> prospectiveOccupierDataList) {
//		this.prospectiveOccupierDataList = prospectiveOccupierDataList;
//	}
//
//
//	public HtmlDataTable getPropspectiveOccupierDataTable() {
//		return propspectiveOccupierDataTable;
//	}
//
//
//	public void setPropspectiveOccupierDataTable(
//			HtmlDataTable propspectiveOccupierDataTable) {
//		this.propspectiveOccupierDataTable = propspectiveOccupierDataTable;
//	}
//
//
//	public Date getContractStartDate() {
//	
//	    if(viewRootMap.containsKey("CONTRACT_START_DATE"))
//	    contractStartDate=(Date)viewRootMap.get("CONTRACT_START_DATE");
//		return contractStartDate;
//	}
//
//
//	public void setContractStartDate(Date contractStartDate) {
//		this.contractStartDate = contractStartDate;
//		viewRootMap.put("CONTRACT_START_DATE",this.contractStartDate);
//	}
//
//
//	public Date getContractEndDate() {
//	 if(viewRootMap.containsKey("CONTRACT_END_DATE"))
//	    contractEndDate=(Date)viewRootMap.get("CONTRACT_END_DATE");
//		return contractEndDate;
//	}
//
//
//	public void setContractEndDate(Date contractEndDate) {
//		this.contractEndDate = contractEndDate;
//		viewRootMap.put("CONTRACT_END_DATE",this.contractEndDate);
//	}
//
//
//	public HtmlDataTable getPaymentScheduleDataTable() {
//		return paymentScheduleDataTable;
//	}
//
//
//	public void setPaymentScheduleDataTable(HtmlDataTable paymentScheduleDataTable) {
//		this.paymentScheduleDataTable = paymentScheduleDataTable;
//	}
//
//	public void setPaymentScheduleDataList(
//			List<PaymentScheduleView> paymentScheduleDataList) {
//		this.paymentScheduleDataList = paymentScheduleDataList;
//	}
//
//
//	public PaymentScheduleView getPaymentScheduleDataItem() {
//		return paymentScheduleDataItem;
//	}
//
//
//	public void setPaymentScheduleDataItem(
//			PaymentScheduleView paymentScheduleDataItem) {
//		this.paymentScheduleDataItem = paymentScheduleDataItem;
//	}
//
//
////	public HtmlDataTable getPaidFacilitiesDataTable() {
////		return paidFacilitiesDataTable;
////	}
////
////
////	public void setPaidFacilitiesDataTable(HtmlDataTable paidFacilitiesDataTable) {
////		this.paidFacilitiesDataTable = paidFacilitiesDataTable;
////	}
////
////
////	public PaidFacilitiesView getPaidFacilitiesDataItem() {
////		return paidFacilitiesDataItem;
////	}
////
////
////	public void setPaidFacilitiesDataItem(PaidFacilitiesView paidFacilitiesDataItem) {
////		this.paidFacilitiesDataItem = paidFacilitiesDataItem;
////	}
////
////
////	public void setPaidFacilitiesDataList(
////			List<PaidFacilitiesView> paidFacilitiesDataList) {
////		this.paidFacilitiesDataList = paidFacilitiesDataList;
////	}
//
//
//	public String getUnitRentValue() {
//		if(viewRootMap.containsKey("unitRentValue") &&  viewRootMap.get("unitRentValue")!=null )
//			unitRentValue = viewRootMap.get("unitRentValue").toString();
//		return unitRentValue;
//	}
//
//
//	public void setUnitRentValue(String unitRentValue) {
//		this.unitRentValue = unitRentValue;
//		if(this.unitRentValue!=null)
//			viewRootMap.put("unitRentValue",this.unitRentValue);
//	}
//
//
//	public String getTxtSponsor() {
//		if(viewRootMap.containsKey("txtSponsor") &&  viewRootMap.get("txtSponsor")!=null )
//			txtSponsor = viewRootMap.get("txtSponsor").toString();
//		return txtSponsor;
//	}
//
//
//	public void setTxtSponsor(String txtSponsor) {
//		this.txtSponsor = txtSponsor;
//		if(this.txtSponsor!=null)
//			viewRootMap.put("txtSponsor",this.txtSponsor);
//	}
//
//
//	public String getHdnSponsorId() {
//		return hdnSponsorId;
//	}
//
//
//	public void setHdnSponsorId(String hdnSponsorId) {
//		this.hdnSponsorId = hdnSponsorId;
//	}
//
//
//	public String getTxtManager() {
//		if(viewRootMap.containsKey("txtManager") &&  viewRootMap.get("txtManager")!=null )
//			txtManager = viewRootMap.get("txtManager").toString();
//		
//		return txtManager;
//	}
//
//
//	public void setTxtManager(String txtManager) {
//		this.txtManager = txtManager;
//		if(this.txtManager !=null)
//			viewRootMap.put("txtManager",this.txtManager);
//	}
//
//
//	public String getHdnManagerId() {
//		return hdnManagerId;
//	}
//
//
//	public void setHdnManagerId(String hdnManagerId) {
//		this.hdnManagerId = hdnManagerId;
//	}
//
//
//	public String getHdnUnitUsuageTypeId() {
//		
//		return hdnUnitUsuageTypeId;
//	}
//
//
//	public void setHdnUnitUsuageTypeId(String hdnUnitUsuageTypeId) {
//		this.hdnUnitUsuageTypeId = hdnUnitUsuageTypeId;
//	}
//
//
//	public String getHdntenanttypeId() {
//		if(viewRootMap.containsKey("hdntenanttypeId") && viewRootMap.get("hdntenanttypeId")!=null)
//			hdntenanttypeId= viewRootMap.get("hdntenanttypeId").toString();
//		return hdntenanttypeId;
//	}
//
//
//	public void setHdntenanttypeId(String hdntenanttypeId) {
//		this.hdntenanttypeId = hdntenanttypeId;
//	}
//
//
//	public String getUnitRentAmount() {
//		return unitRentAmount;
//	}
//
//
//	public void setUnitRentAmount(String unitRentAmount) {
//		this.unitRentAmount = unitRentAmount;
//	}
//	
//	public String getPageMode() {
//		if(viewRootMap.containsKey("pageMode") && viewRootMap.get("pageMode")!=null)
//			pageMode= viewRootMap.get("pageMode").toString();
//		
//		return pageMode;
//	}
//
//
//	public void setPageMode(String pageMode) {
//		this.pageMode = pageMode;
//		
//		if(this.pageMode !=null)
//			viewRootMap.put("pageMode",this.pageMode);
//	}
//
//
//	public String getHdnAuctionunitId() {
//		return hdnAuctionunitId;
//	}
//
//
//	public void setHdnAuctionunitId(String hdnAuctionunitId) {
//		this.hdnAuctionunitId = hdnAuctionunitId;
//	}
//
//
//	public Double getTotalContractValue() {
//		return totalContractValue;
//	}
//
//
//	public void setTotalContractValue(Double totalContractValue) {
//		this.totalContractValue = totalContractValue;
//	}
//
//
//	public String getTxttotalContractValue() {
//		if(totalContractValue>0)
//		{
//		txttotalContractValue=totalContractValue.toString();
//		}
//		return txttotalContractValue;
//	}
//
//
//	public void setTxttotalContractValue(String txttotalContractValue) {
//		this.txttotalContractValue = txttotalContractValue;
//	}
//
//
//	public String getContractId() {
//		return contractId;
//	}
//
//
//	public void setContractId(String contractId) {
//		this.contractId = contractId;
//	}
//
//
//	public String getContractCreatedOn() {
//		return contractCreatedOn;
//	}
//
//
//	public void setContractCreatedOn(String contractCreatedOn) {
//		this.contractCreatedOn = contractCreatedOn;
//	}
//
//
//	public String getContractCreatedBy() {
//		return contractCreatedBy;
//	}
//
//
//	public void setContractCreatedBy(String contractCreatedBy) {
//		this.contractCreatedBy = contractCreatedBy;
//	}
//
//
//	public String getSelectRequestStatus() {
//		
//		if(viewRootMap.containsKey("selectRequestStatus") && viewRootMap.get("selectRequestStatus")!=null)
//			selectRequestStatus = viewRootMap.get("selectRequestStatus").toString();
//		return selectRequestStatus;
//	}
//
//
//	public void setSelectRequestStatus(String selectRequestStatus) {
//		this.selectRequestStatus = selectRequestStatus;
//		if(this.selectRequestStatus!=null)
//			viewRootMap.put("selectRequestStatus",this.selectRequestStatus);
//		
//	}
//	 public ResourceBundle getBundle() {
//
//		 
//
//		 ResourceBundle bundle;
//
//		             bundle = ResourceBundle.getBundle(getFacesContext().getApplication().getMessageBundle(), getFacesContext().getViewRoot().getLocale());
//
//
//         return bundle;
//
//   }
//
//
//
//   public void setBundle(ResourceBundle bundle) {
//
//         this.bundle = bundle;
//
//   }
//
//
//public String getTxtTenantName() {
//	return txtTenantName;
//}
//
//
//public void setTxtTenantName(String txtTenantName) {
//	this.txtTenantName = txtTenantName;
//}
//
//
//public String getTxtTenantType() {
//	return txtTenantType;
//}
//
//
//public void setTxtTenantType(String txtTenantType) {
//	this.txtTenantType = txtTenantType;
//}
//
//
//public String getTxtUnitUsuageType() {
//	return txtUnitUsuageType;
//}
//
//
//public void setTxtUnitUsuageType(String txtUnitUsuageType) {
//	this.txtUnitUsuageType = txtUnitUsuageType;
//}
//
//
//public String getPropertyCommercialName() {
//	return propertyCommercialName;
//}
//
//
//public void setPropertyCommercialName(String propertyCommercialName) {
//	this.propertyCommercialName = propertyCommercialName;
//}
//
//
//public String getPropertyAddress() {
//	return propertyAddress;
//}
//
//
//public void setPropertyAddress(String propertyAddress) {
//	this.propertyAddress = propertyAddress;
//}
//
//public boolean getIsItemEditable()
//{
//  boolean isItemEditable = true;	
//  if(this.getPageMode()!=null && (this.getPageMode().equals(PAGE_MODE_ADD)|| this.getPageMode().equals(PAGE_MODE_UPDATE) ||
//		  this.getPageMode().equals(WebConstants.LEASE_AMEND_MODE) || this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD) || 
//		  this.getPageMode().equals(WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE)
//      ))
//	  return isItemEditable ;
//  else if(this.getPageMode()!=null )
//  {
//	  if(getIsContractStatusDraft() || getIsContractStatusNew() || getIsContractStatusApprovalRequired())
//	   return true;
//	  else
//		  return false;
//	  
//  }
//  return isItemEditable;
//}
//
//public String getContractStatus() 
//{
//	String methodName="getContractStatus";
//	logger.logDebug(methodName+"|"+"Start");
//	logger.logDebug(methodName+"|"+"PageMode :"+this.getPageMode());
//	contractStatus="";
//	List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_STATUS);
//	for(int i=0;i<ddvList.size();i++)
//	{
//		 
//		DomainDataView ddv=(DomainDataView)ddvList.get(i);
//		logger.logDebug(methodName+"|"+"Domain Data Id :"+ddv.getDomainDataId());
//		
//		//if page mode is in add mode than take the contract status as new
//		if(this.getPageMode()!=null && this.getPageMode().equals(PAGE_MODE_ADD) && ddv.getDataValue().equals(WebConstants.CONTRACT_STATUS_NEW))
//		{
//		  if(getIsArabicLocale())
//		       contractStatus= ddv.getDataDescAr();
//		  else if(getIsEnglishLocale())
//			   contractStatus= ddv.getDataDescEn();
//		  
//		  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
//        	break;
//		}
//
//		// IF PAGE IS IN UPDATE MODE 
//		else if(this.getPageMode()!=null && !this.getPageMode().equals(PAGE_MODE_ADD)  && viewRootMap.containsKey("CONTRACTVIEWUPDATEMODE"))
//		{
//			
//			  ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	    	  logger.logDebug(methodName+"|"+"Contract Status ..."+contractView.getStatus());
//		      if(contractView.getStatus()!=null &&  ddv.getDomainDataId().toString().equals(contractView.getStatus().toString()) )
//		      {
//				  if(getIsArabicLocale())
//					  contractStatus= ddv.getDataDescAr();  
//				  else if(getIsEnglishLocale())
//					  contractStatus= ddv.getDataDescEn();
//				  
//				  this.setSelectRequestStatus(ddv.getDomainDataId().toString());
//				  break;
//		      }
//			  
//	    }
//	}
//		
//		
//	logger.logDebug(methodName+"|"+"Finish");
//	return contractStatus;
//}
//
//
//public void setContractStatus(String contractStatus) {
//	this.contractStatus = contractStatus;
//}
//
//
//public String getTxtMaintainenceAmount() {
//	return txtMaintainenceAmount;
//}
//
//
//public void setTxtMaintainenceAmount(String txtMaintainenceAmount) {
//	this.txtMaintainenceAmount = txtMaintainenceAmount;
//}
//
//
//public Double getMaintainenceAmount() {
//	return maintainenceAmount;
//}
//
//
//public void setMaintainenceAmount(Double maintainenceAmount) {
//	this.maintainenceAmount = maintainenceAmount;
//}
//
//
//public String getUnitStatusId() {
//	if(viewRootMap.containsKey("unitStatusId") && viewRootMap.get("unitStatusId") !=null)
//		unitStatusId = viewRootMap.get("unitStatusId").toString(); 
//	return unitStatusId;
//}
//
//
//public void setUnitStatusId(String unitStatusId) {
//	this.unitStatusId = unitStatusId;
//	if(this.unitStatusId!=null)
//		viewRootMap.put("unitStatusId", this.unitStatusId);
//}
//
//
//public String getTxtUnitStatus() {
//	return txtUnitStatus;
//}
//
//
//public void setTxtUnitStatus(String txtUnitStatus) {
//	this.txtUnitStatus = txtUnitStatus;
//}
//
//
//
//
//
//public HtmlDataTable getUnitDataTable() {
//	return unitDataTable;
//}
//
//
//public void setUnitDataTable(HtmlDataTable unitDataTable) {
//	this.unitDataTable = unitDataTable;
//}
//
//
//public List<UnitView> getUnitDataList() {
//
//	
//	if(sessionMap.containsKey("AUCTIONUNITINFO"))
//	 {
//		AuctionUnitView auctionUnitView=(AuctionUnitView)sessionMap.get("AUCTIONUNITINFO");
//		if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
//				 auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
//		  )
//		{
//			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
//			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
//			viewRootMap.remove(AUCTION_UNIT_ID);
//		}
//	
//		if(!viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
//				(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
//				 auctionUnitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
//				 )
//		  )
//		 {
//				 viewRootMap.put(PREVIOUS_UNIT_ID,auctionUnitView.getUnitId());
//			     populateAuctionUnitDetail();
//		 }
//	 }
//	 else if(sessionMap.containsKey("UNITINFO"))
//	 {
//		 UnitView unitView=(UnitView)sessionMap.get("UNITINFO");
//		 if(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
//				 unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
//		  )
//		{
//			viewRootMap.remove(AUCTION_DEPOSIT_AMOUNT);
//			viewRootMap.remove(AUCTION_UNIT_BIDDER_LIST);
//			viewRootMap.remove(AUCTION_UNIT_ID);
//		}
//		 if(!viewRootMap.containsKey(PREVIOUS_UNIT_ID) ||
//			(viewRootMap.containsKey(PREVIOUS_UNIT_ID) && viewRootMap.get(PREVIOUS_UNIT_ID)!=null  && 
//			 unitView.getUnitId().compareTo(new Long(viewRootMap.get(PREVIOUS_UNIT_ID).toString()))!=0
//			 )
//			)
//		 {
//			 logger.logInfo("getUnitDataList|PropertyCategoryId",unitView.getPropertyCategoryId());
//		     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
//			 viewRootMap.put(PREVIOUS_UNIT_ID,unitView.getUnitId());
//			 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(unitView.getUnitId().toString()))
//			 {
//			  unitDataList.clear();
//			  unitDataList.add(unitView);
//			  this.setUnitRefNum(unitView.getUnitNumber());
//			  if(unitView.getStatusId()!=null)
//		      {
//		            unitView.setStatusId(unitView.getStatusId());
//		            this.setUnitStatusId(unitView.getStatusId().toString());
//		       }
//			  viewRootMap.put(UNIT_DATA_ITEM, unitView);
//			  if(unitView.getRentValue()!= null && (unitRentAmount== null || unitRentAmount.trim().length()==0))
//		        	 unitRentAmount = unitView.getRentValue().toString();
//			 }
//		 }
//	 }
//		 
//	return unitDataList;
//}
//
//
//private void populateAuctionUnitDetail() {
//	String methodName= "populateAuctionUnitDetail";
//	logger.logInfo(methodName+"|Start");
//	AuctionUnitView auctionUnitView=(AuctionUnitView)sessionMap.get("AUCTIONUNITINFO");
//	 if(hdnUnitId!=null && hdnUnitId.trim().length()>0 && hdnUnitId.equals(auctionUnitView.getUnitId().toString()))
//	 {
//		 
//		 UnitView unitView =auctionUnitView.getUnitView();
//		 unitView.setAuctionUnitId(auctionUnitView.getAuctionUnitId());
//		 viewRootMap.put(AUCTION_UNIT_ID,auctionUnitView.getAuctionUnitId());
//		 unitView.setRentValue(auctionUnitView.getBidPrice());
//		 unitView.setUnitId(auctionUnitView.getUnitId());
//		 
//	     unitView.setUnitNumber( auctionUnitView.getUnitNumber());
//	     this.setUnitRefNum(auctionUnitView.getUnitNumber());
//	     unitView.setUsageTypeId(auctionUnitView.getUnitUsageTypeID());
//	     unitView.setUnitAddress(auctionUnitView.getPropertyAdress());
//	     unitView.setPropertyCommercialName(auctionUnitView.getPropertyCommercialName());
//	     //unitView.setFloorName(floorName)
//	     unitView.setUsageTypeEn(auctionUnitView.getUnitUsageTypeEn());
//	     unitView.setUsageTypeAr(auctionUnitView.getUnitUsageTypeAr());
//	     unitView.setStatusEn( auctionUnitView.getUnitStatusEn());
//	     unitView.setStatusAr(auctionUnitView.getUnitStatusAr());
//	     logger.logInfo(methodName+"|PropertyCategoryId",unitView.getPropertyCategoryId());
//	     viewRootMap.put(PAYMENT_SCHEDULE_OWNERSHIP_TYPE_ID,unitView.getPropertyCategoryId());
//	     if(auctionUnitView.getUnitStatusId()!=null)
//	     {
//	        unitView.setStatusId(auctionUnitView.getUnitStatusId());
//	        this.setUnitStatusId(auctionUnitView.getUnitStatusId().toString());
//	     }
//	     if(auctionUnitView.getBidderId()!=null)
//	     {
//	    	 getDepositAmountForBidder(auctionUnitView.getAuctionUnitId(), auctionUnitView.getBidderId(),null);
//	    	 
//	     }
//	     
//	     unitDataList.clear();
//	     unitDataList.add(unitView);
//	     viewRootMap.put(UNIT_DATA_ITEM, unitView);
//	     if(unitView.getRentValue()!= null && (unitRentAmount== null || unitRentAmount.trim().length()==0))
//	    	 unitRentAmount = unitView.getRentValue().toString(); 
//	     logger.logInfo(methodName+"|Finish");
//	 }
//}
//
//private void getDepositAmountForBidder(Long auctionUnitId,Long bidderId,Long movedToContractId) {
//	 
//	 String methodName = "getDepositAmountForBidder";
//	 RequestServiceAgent rsa ;
//	 try
//	 {
//		 rsa = new RequestServiceAgent();
//		 List<RequestDetailView> rdViewList = new ArrayList<RequestDetailView>(0);
//		 List<RequestDetailView> newRDViewList = new ArrayList<RequestDetailView>(0);
//		 
//		 if(auctionUnitId != null && bidderId != null )
//		  rdViewList = rsa.getRequestDetailForBidderAuctionUnit(auctionUnitId, bidderId);
//		 else if(movedToContractId != null )
//		  rdViewList = rsa.getRequestDetailForMovedToContractId(movedToContractId);
//		 Double auctionDepositAmount =0D;
//		 if(rdViewList!=null)
//	
//		 for (RequestDetailView requestDetailView : rdViewList) {
//			 auctionDepositAmount += (requestDetailView.getDepositAmount() -  requestDetailView.getConfiscatedAmount());
//		     requestDetailView.setUpdatedBy(getLoggedInUser());
//		     newRDViewList.add(requestDetailView);
//		 }
//		 	 viewRootMap.put(AUCTION_UNIT_BIDDER_LIST,newRDViewList);
//		 if(auctionDepositAmount >0D)
//			 viewRootMap.put(AUCTION_DEPOSIT_AMOUNT,auctionDepositAmount);
//	 }
//	 catch(Exception e)
//	 {
//		 logger.LogException(methodName+"|Error Occured",e);
//	 }
//}
//public void setUnitDataList(List<UnitView> unitDataList) {
//	this.unitDataList = unitDataList;
//}
//
//
//public UnitView getUnitDataItem() {
//	List<UnitView> unitViewList = getUnitDataList();
//	if(viewRootMap.containsKey(UNIT_DATA_ITEM))
//		unitDataItem = (UnitView)viewRootMap.get(UNIT_DATA_ITEM);
//	return unitDataItem;
//}
//
//
//public void setUnitDataItem(UnitView unitDataItem) {
//	this.unitDataItem = unitDataItem;
//}
//
//
//
//
//public HtmlCommandButton getBtnOccuipers() {
//	return btnOccuipers;
//}
//
//
//public void setBtnOccuipers(HtmlCommandButton btnOccuipers) {
//	this.btnOccuipers = btnOccuipers;
//}
//
//
//public HtmlCommandButton getBtnPartners() {
//	return btnPartners;
//}
//
//
//public void setBtnPartners(HtmlCommandButton btnPartners) {
//	this.btnPartners = btnPartners;
//}
//
//
//public HtmlCommandButton getBtnSaveNewLease() {
//	return btnSaveNewLease;
//}
//
//
//public void setBtnSaveNewLease(HtmlCommandButton btnSaveNewLease) {
//	this.btnSaveNewLease = btnSaveNewLease;
//}
//
//
//public HtmlCommandButton getBtnSaveAmendLease() {
//	return btnSaveAmendLease;
//}
//
//
//public void setBtnSaveAmendLease(HtmlCommandButton btnSaveAmendLease) {
//	this.btnSaveAmendLease = btnSaveAmendLease;
//}
//
//
//public HtmlCommandButton getBtnCompleteAmendLease() {
//	return btnCompleteAmendLease;
//}
//
//
//public void setBtnCompleteAmendLease(HtmlCommandButton btnCompleteAmendLease) {
//	this.btnCompleteAmendLease = btnCompleteAmendLease;
//}
//
//
//public HtmlCommandButton getBtnSubmitAmendLease() {
//	return btnSubmitAmendLease;
//}
//
//
//public void setBtnSubmitAmendLease(HtmlCommandButton btnSubmitAmendLease) {
//	this.btnSubmitAmendLease = btnSubmitAmendLease;
//}
//
//
//public HtmlGraphicImage getImgSponsor() {
//	return imgSponsor;
//}
//
//
//public void setImgSponsor(HtmlGraphicImage imgSponsor) {
//	this.imgSponsor = imgSponsor;
//}
//
//
//public HtmlGraphicImage getImgManager() {
//	return imgManager;
//}
//
//
//public void setImgManager(HtmlGraphicImage imgManager) {
//	this.imgManager = imgManager;
//}
//
//
//public HtmlCommandButton getBtnApprove() {
//	return btnApprove;
//}
//
//
//public void setBtnApprove(HtmlCommandButton btnApprove) {
//	this.btnApprove = btnApprove;
//}
//
//
//public HtmlCommandButton getBtnReject() {
//	return btnReject;
//}
//
//
//public void setBtnReject(HtmlCommandButton btnReject) {
//	this.btnReject = btnReject;
//}
//
//
//public HtmlCommandButton getBtnGenPayments() {
//	return btnGenPayments;
//}
//
//
//public void setBtnGenPayments(HtmlCommandButton btnGenPayments) {
//	this.btnGenPayments = btnGenPayments;
//}
//
//
//public HtmlCommandButton getBtnApproveAmendLease() {
//	return btnApproveAmendLease;
//}
//
//
//public void setBtnApproveAmendLease(HtmlCommandButton btnApproveAmendLease) {
//	this.btnApproveAmendLease = btnApproveAmendLease;
//}
//
//
//public HtmlCommandButton getBtnRejectAmendLease() {
//	return btnRejectAmendLease;
//}
//
//
//public void setBtnRejectAmendLease(HtmlCommandButton btnRejectAmendLease) {
//	this.btnRejectAmendLease = btnRejectAmendLease;
//}
//
//
//public String getPageTitle() {
//	return pageTitle;
//}
//
//
//public void setPageTitle(String pageTitle) {
//	this.pageTitle = pageTitle;
//}
//
//
//public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkCommercial() {
//	return chkCommercial;
//}
//
//
//public void setChkCommercial(
//		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkCommercial) {
//	this.chkCommercial = chkCommercial;
//}
//
//
//public org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox getChkPaidFacilities() {
//	return chkPaidFacilities;
//}
//
//
//public void setChkPaidFacilities(
//		org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox chkPaidFacilities) {
//	this.chkPaidFacilities = chkPaidFacilities;
//}
//
//
//public HtmlInputText getTxtRefNumber() {
//	
//	return txtRefNumber;
//}
//
//
//public void setTxtRefNumber(HtmlInputText txtRefNumber) {
//	this.txtRefNumber = txtRefNumber;
//}
//
//
//public HtmlOutputLabel getLblRefNum() {
//	return lblRefNum;
//}
//
//
//public void setLblRefNum(HtmlOutputLabel lblRefNum) {
//	this.lblRefNum = lblRefNum;
//}
//
//
//public String getStrTimeZone()
//{
//	
//	 return TimeZone.getDefault().toString();
//	
//}
//
//
//public TimeZone getTimeZone()
//{
//	 return TimeZone.getDefault();
//	
//}
//
//
//
//
//public Date getContractIssueDate() {
//
//    if(viewRootMap.containsKey("CONTRACT_ISSUE_DATE"))
//     contractIssueDate=(Date)viewRootMap.get("CONTRACT_ISSUE_DATE");
//	return contractIssueDate;
//}
//
//
//public void setContractIssueDate(Date contractIssueDate) {
//	this.contractIssueDate = contractIssueDate;
//	if(this.contractIssueDate!=null)
//	 viewRootMap.put("CONTRACT_ISSUE_DATE",this.contractIssueDate);
//}
//
//
//public List<SelectItem> getContractTypeList() {
//	contractTypeList.clear();
//	List<DomainDataView> ddvList=(ArrayList)viewRootMap.get(WebConstants.SESSION_CONTRACT_TYPE);
//	for(int i=0;i<ddvList.size();i++)
//	{
//		SelectItem item=null; 
//		DomainDataView ddv=(DomainDataView)ddvList.get(i);
//			  if(getIsArabicLocale())
//			   item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescAr());
//			  else if(getIsEnglishLocale())
//				item =new SelectItem(ddv.getDomainDataId().toString(),ddv.getDataDescEn());
//			  contractTypeList.add(item);
//
//		
//		
//		
//	}
//	
//	
//	return contractTypeList;
//}
//
//
//public void setContractTypeList(List<SelectItem> contractTypeList) {
//	this.contractTypeList = contractTypeList;
//}
//
//
//public String getSelectOneContractType() {
//	if(viewRootMap.containsKey("selectOneContractType") && viewRootMap.get("selectOneContractType")!=null)
//		selectOneContractType= viewRootMap.get("selectOneContractType").toString();
//		return selectOneContractType;
//}
//
//
//public void setSelectOneContractType(String selectOneContractType) {
//	this.selectOneContractType = selectOneContractType;
//	if(this.selectOneContractType !=null)
//		viewRootMap.put("selectOneContractType",this.selectOneContractType);
//}
//
//public double getTotalRentAmount() {
//	return totalRentAmount;
//}
//
//
//public void setTotalRentAmount(double totalRentAmount) {
//	this.totalRentAmount = totalRentAmount;
//}
//
//
//public double getTotalCashAmount() {
//	return totalCashAmount;
//}
//
//
//public void setTotalCashAmount(double totalCashAmount) {
//	this.totalCashAmount = totalCashAmount;
//}
//
//
//public double getTotalChqAmount() {
//	return totalChqAmount;
//}
//
//
//public void setTotalChqAmount(double totalChqAmount) {
//	this.totalChqAmount = totalChqAmount;
//}
//
//
//public int getInstallments() {
//	return installments;
//}
//
//
//public void setInstallments(int installments) {
//	this.installments = installments;
//}
//
//
//public double getTotalDepositAmount() {
//	return totalDepositAmount;
//}
//
//
//public void setTotalDepositAmount(double totalDepositAmount) {
//	this.totalDepositAmount = totalDepositAmount;
//}
//
//
//public double getTotalFees() {
//	return totalFees;
//}
//
//
//public void setTotalFees(double totalFees) {
//	this.totalFees = totalFees;
//}
//
//
//public double getTotalFines() {
//	return totalFines;
//}
//
//
//public void setTotalFines(double totalFines) {
//	this.totalFines = totalFines;
//}	
//
//
//public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
//	return tabPaymentTerms;
//}
//
//
//public void setTabPaymentTerms(
//		org.richfaces.component.html.HtmlTab tabPaymentTerms) {
//	this.tabPaymentTerms = tabPaymentTerms;
//}
//
//
////public org.richfaces.component.html.HtmlTab getTabFacilities() {
////	return tabFacilities;
////}
////
////
////public void setTabFacilities(org.richfaces.component.html.HtmlTab tabFacilities) {
////	this.tabFacilities = tabFacilities;
////}
//
//public String cmdChangeTenant_Click(){
//	String  METHOD_NAME = "cmdChangeTenant_Click|";		
//    logger.logInfo(METHOD_NAME+"started...");
//    try{
//
//    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	clearApplicationDetails();
//    	PropertyServiceAgent psa=new PropertyServiceAgent();
//    	RequestView requestView =new RequestView();
//    	requestView.setContractView(contractView);//Id(contractView.getContractId());
//    	RequestTypeView requestTypeView =new RequestTypeView();
//    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_CHANGE_TENANT_NAME);
//    	
//    	List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null,null);
//
//    	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
//    			                                    WebConstants.REQUEST_STATUS_COMPLETE);
//    	//if there is some request for change tenant name against this 
//    	//contract and its status is not completed then display that request
//    	//otherwise create new screen
//    	requestView=null;
//    	for (RequestView requestView2 : requestViewList) {
//			
//    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
//    		{
//    			requestView =requestView2;
//    		   break;
//    		}
//		}
//    	if(requestView!=null)
//    	{
//    		
//    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
//    	}
//    	else 
//    	{
//    		//context.
//    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
//    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//    	   
//    	}
//    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	return WebConstants.ContractListNavigation.CHANGE_TENANT_NAME;
//}
//public String cmdAmendLeaseContract_Click(){
//	String  METHOD_NAME = "cmdAmendLeaseContract_Click|";		
//    logger.logInfo(METHOD_NAME+"started...");
//    try{
//    	clearApplicationDetails();
//    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	CommonUtil commonUtil = new CommonUtil();
//    	PropertyServiceAgent psa=new PropertyServiceAgent();
//    	RequestServiceAgent rsa=new RequestServiceAgent();
//    	RequestView requestView =new RequestView();
//    	List<RequestView> requestViewList =new ArrayList<RequestView>(0);
//    	//requestView.setContractView(contractView);//Id(contractView.getContractId());
//    	RequestTypeView requestTypeView =new RequestTypeView();
//    	requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_AMEND_LEASE_CONTRACT);
//    	RequestFieldDetailView rfdv=new RequestFieldDetailView();
//    	RequestKeyView  requestKeyView=commonUtil.getRequestKeysForProcedureKeyName(WebConstants.PROCEDURE_TYPE_AMEND_LEASE_CONTRACT,WebConstants.REQUEST_KEY_CONTRACT_ID_FOR_AMEND);        	
//    	requestViewList= 
//    		              rsa.getRequestFromRequestField(contractView.getContractId().toString(), 
//    		            		                         requestView.getRequestTypeId().toString(),
//    		            		                         requestKeyView.getRequestKeyId().toString(),
//    		            		                         null 
//    		                                            );
//    	
//    	
//    	
//		
//    	//List<RequestView> requestViewList= psa.getAllRequests(requestView, null, null);
//    	
//    	DomainDataView ddv=commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), 
//    			                                    WebConstants.REQUEST_STATUS_COMPLETE);
//    	
//    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("contractId", contractView.getContractId());
//    	//if there is some request for Amend against this 
//    	//contract and its status is not completed then display that request
//    	//otherwise create new screen
//    	requestView=null;
//    	for (RequestView requestView2 : requestViewList) {
//			
//    		if(requestView2.getStatusId().compareTo(ddv.getDomainDataId())!=0)
//    		{
//    			requestView =requestView2;
//    		   break;
//    		}
//		}
//    	if(requestView!=null)
//    	{
//    		
//    		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.REQUEST_VIEW,requestView);
//    		setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_UPDATE);
//    	}
//    	else 
//    	{
//    	   setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
//    	   
//    	   getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//    	   setRequestParam(WebConstants.LEASE_AMEND_MODE,WebConstants.LEASE_CONTRACT_PAGE_MODE_AMEND_ADD);
//    	}
//    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	return WebConstants.ContractListNavigation.LEASE_CONTRACT_SCREEN;
//}
//
//
//public String cmdTransferContract_Click(){
//	String  METHOD_NAME = "cmdTransferContract_Click|";		
//    logger.logInfo(METHOD_NAME+"started...");
//    try{
//    	clearApplicationDetails();
//    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	setRequestParam(WebConstants.Contract.CONTRACT_VIEW, contractView);
//    	setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_LEASE_CONTRACT);
//    	
//    	
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	return WebConstants.ContractListNavigation.TRANSFER_CONTRACT;
//}
//public String cmdLinkCancelContract_Click(){
//	String  METHOD_NAME = "cmdLinkCancelContract_Click|";		
//    logger.logDebug(METHOD_NAME+"started...");
//    try{
//    	clearApplicationDetails();
//    	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//  	    logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	
//	return WebConstants.ContractListNavigation.CANCEL_CONTRACT;
//}
//
//public String cmdLinkSettleContract_Click(){
//	String  METHOD_NAME = "cmdLinkSettleContract_Click|";		
//    logger.logDebug(METHOD_NAME+"started...");
//    try{
//      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//  
//    	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SETTLE_PAGE_MODE","readOnly");
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	
//	return WebConstants.ContractListNavigation.SETTLE_CONTRACT;
//}
//public String issueCLCmdLink_Click(){
//	String  METHOD_NAME = "issueCLCmdLink_Click|";		
//    logger.logInfo(METHOD_NAME+"started...");
//    try{
//    	clearApplicationDetails();
//      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	return WebConstants.ContractListNavigation.ISSUE_CLEARANCE_LETTER;		
//}
//
//
//public String issueNOLCmdLink_Click(){
//	String  METHOD_NAME = "issueNOLmdLink_Click|";		
//    logger.logInfo(METHOD_NAME+"started...");
//    try{
//    	clearApplicationDetails();
//      	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//    	getFacesContext().getExternalContext().getRequestMap().put(WebConstants.Contract.CONTRACT_VIEW, contractView);
//  
//		logger.logInfo(METHOD_NAME+"completed successfully...");
//	}
//	catch (Exception e) {
//		logger.LogException(METHOD_NAME + "crashed...", e);			
//	}
//	return WebConstants.ContractListNavigation.ISSUE_NO_OBJECTION_LETTER;
//}
//
//public String renewContract()
//{
//	clearApplicationDetails();
//  	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	ContractView selectedContract = contractView;
//	
//	setRequestParam("contractId", selectedContract.getContractId());
//	setRequestParam("suggestedRentAmount", 0.0);
//	setRequestParam("oldRentValue", selectedContract.getRentAmount());
//	setRequestParam("fromRenewList", "true");
//	
//	return "renew";
//}
//    
//private Boolean compareContrctStatus(String status) 
//{
//	
//	ContractView contractView=(ContractView)viewRootMap.get("CONTRACTVIEWUPDATEMODE");
//	ContractView selectedContract = contractView;
//	CommonUtil util=new CommonUtil();
//	DomainDataView ddv= util.getIdFromType(util.getDomainDataListForDomainType(WebConstants.CONTRACT_STATUS), status);
//	if(contractView!=null && contractView.getStatus().toString().equals(ddv.getDomainDataId().toString()))
//		return  true;
//	else
//		return  false;
//	
//}
//public Boolean getIsContrctStatusApproved() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVED);
//}
//public Boolean getIsContrctStatusComplete() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_COMPLETE);
//}public Boolean getIsContrctStatusExpired() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_EXPIRED);
//}
//public Boolean getIsContrctStatusRenewDraft() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_RENEW_DRAFT);
//}
//public Boolean getIsContrctStatusActive() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_ACTIVE);
//}
//public Boolean getIsContrctStatusTerminated() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_TERMINATED);
//}
//public Boolean getIsContrctStatusRejected() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_REJECTED);
//}
//public Boolean getIsContractStatusNew() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_NEW);
//}
//public Boolean getIsContractStatusDraft() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_DRAFT);
//}
//public Boolean getIsContractStatusApprovalRequired() {
//	
//	return compareContrctStatus(WebConstants.CONTRACT_STATUS_APPROVAL_REQUIRED);
//}
//
//
//public HtmlCommandButton getBtnSendNewLeaseForApproval() {
//	return btnSendNewLeaseForApproval;
//}
//
//
//public void setBtnSendNewLeaseForApproval(
//		HtmlCommandButton btnSendNewLeaseForApproval) {
//	this.btnSendNewLeaseForApproval = btnSendNewLeaseForApproval;
//}
//
//
//public HtmlGraphicImage getImgDeleteSponsor() {
//	return imgDeleteSponsor;
//}
//
//
//public void setImgDeleteSponsor(HtmlGraphicImage imgDeleteSponsor) {
//	this.imgDeleteSponsor = imgDeleteSponsor;
//}
//
//
//public HtmlGraphicImage getImgDeleteManager() {
//	return imgDeleteManager;
//}
//
//
//public void setImgDeleteManager(HtmlGraphicImage imgDeleteManager) {
//	this.imgDeleteManager = imgDeleteManager;
//}
//
//
//public String getHdnApplicantId() {
//	return hdnApplicantId;
//}
//
//
//public void setHdnApplicantId(String hdnApplicantId) {
//	this.hdnApplicantId = hdnApplicantId;
//}
//
//
////public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
////	return tabAuditTrail;
////}
////
////
////public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
////	this.tabAuditTrail = tabAuditTrail;
////}
//
//
//public HtmlCommandButton getBtnCollectPayment() {
//	return btnCollectPayment;
//}
//
//
//public void setBtnCollectPayment(HtmlCommandButton btnCollectPayment) {
//	this.btnCollectPayment = btnCollectPayment;
//}
//
//
//public HtmlGraphicImage getImgTenant() {
//	return imgTenant;
//}
//
//
//public void setImgTenant(HtmlGraphicImage imgTenant) {
//	this.imgTenant = imgTenant;
//}
//
//
//public HtmlCommandButton getBtnAddUnit() {
//	return btnAddUnit;
//}
//
//
//public void setBtnAddUnit(HtmlCommandButton btnAddUnit) {
//	this.btnAddUnit = btnAddUnit;
//}
//
//
//public HtmlCommandButton getBtnAddAuctionUnit() {
//	return btnAddAuctionUnit;
//}
//
//
//public void setBtnAddAuctionUnit(HtmlCommandButton btnAddAuctionUnit) {
//	this.btnAddAuctionUnit = btnAddAuctionUnit;
//}
//
//
//public org.richfaces.component.html.HtmlCalendar getIssueDateCalendar() {
//	return issueDateCalendar;
//}
//
//
//public void setIssueDateCalendar(
//		org.richfaces.component.html.HtmlCalendar issueDateCalendar) {
//	this.issueDateCalendar = issueDateCalendar;
//}
//
//
//public org.richfaces.component.html.HtmlCalendar getStartDateCalendar() {
//	return startDateCalendar;
//}
//
//
//public void setStartDateCalendar(
//		org.richfaces.component.html.HtmlCalendar startDateCalendar) {
//	this.startDateCalendar = startDateCalendar;
//}
//
//
//public org.richfaces.component.html.HtmlCalendar getEndDateCalendar() {
//	return endDateCalendar;
//}
//
//
//public void setEndDateCalendar(
//		org.richfaces.component.html.HtmlCalendar endDateCalendar) {
//	this.endDateCalendar = endDateCalendar;
//}
//
//
//public HtmlCommandButton getBtnAddPayments() {
//	return btnAddPayments;
//}
//
//
//public void setBtnAddPayments(HtmlCommandButton btnAddPayments) {
//	this.btnAddPayments = btnAddPayments;
//}
//
//
//public HtmlSelectOneMenu getCmbContractType() {
//	return cmbContractType;
//}
//
//
//public void setCmbContractType(HtmlSelectOneMenu cmbContractType) {
//	this.cmbContractType = cmbContractType;
//}
//
//
//public HtmlInputText getTxt_ActualRent() {
//	return txt_ActualRent;
//}
//
//
//public void setTxt_ActualRent(HtmlInputText txt_ActualRent) {
//	this.txt_ActualRent = txt_ActualRent;
//}
//
//
//public HtmlGraphicImage getBtnPartnerDelete() {
//	return btnPartnerDelete;
//}
//
//
//public void setBtnPartnerDelete(HtmlGraphicImage btnPartnerDelete) {
//	this.btnPartnerDelete = btnPartnerDelete;
//}
//
//
//public HtmlGraphicImage getBtnOccupierDelete() {
//	return btnOccupierDelete;
//}
//
//
//public void setBtnOccupierDelete(HtmlGraphicImage btnOccupierDelete) {
//	this.btnOccupierDelete = btnOccupierDelete;
//}
//
//
//
//public String getSource() {
//	return source;
//}
//
//
//
//public void setSource(String source) {
//	this.source = source;
//	
//	if(this.source !=null)
//		viewRootMap.put("source",this.source);
//}
//
//
//
//public String getMaintenanceOn() {
//	return maintenanceOn;
//}
//
//
//
//public void setMaintenanceOn(String maintenanceOn) {
//	this.maintenanceOn = maintenanceOn;
//	
//	if(this.maintenanceOn !=null)
//		viewRootMap.put("maintenanceOn",this.maintenanceOn);
//}
//
//
//
//public String getPropertyName() {
//	return propertyName;
//}
//
//
//
//public void setPropertyName(String propertyName) {
//	this.propertyName = propertyName;
//	
//	if(this.propertyName !=null)
//		viewRootMap.put("propertyName",this.propertyName);
//}
//
//
//
//public String getTenantName() {
//	return tenantName;
//}
//
//
//
//public void setTenantName(String tenantName) {
//	this.tenantName = tenantName;
//	
//	if(this.tenantName !=null)
//		viewRootMap.put("tenantName",this.tenantName);
//}
//
//
//
//public String getOwnershipType() {
//	return ownershipType;
//}
//
//
//
//public void setOwnershipType(String ownershipType) {
//	this.ownershipType = ownershipType;
//	
//	if(this.ownershipType !=null)
//		viewRootMap.put("ownershipType",this.ownershipType);
//}
//
//
//
//public String getWorkType() {
//	return workType;
//}
//
//
//
//public void setWorkType(String workType) {
//	this.workType = workType;
//	
//	if(this.workType !=null)
//		viewRootMap.put("workType",this.workType);
//}
//
//
//
//public String getMaintenanceType() {
//	return maintenanceType;
//}
//
//
//
//public void setMaintenanceType(String maintenanceType) {
//	this.maintenanceType = maintenanceType;
//	
//	if(this.maintenanceType !=null)
//		viewRootMap.put("maintenanceType",this.maintenanceType);
//}
//
//
//
//public HtmlInputText getTxtUnitNo() {
//	return txtUnitNo;
//}
//
//
//
//public void setTxtUnitNo(HtmlInputText txtUnitNo) {
//	this.txtUnitNo = txtUnitNo;
//}
//
//
//
//public HtmlInputText getTxtPropertyName() {
//	return txtPropertyName;
//}
//
//
//
//public void setTxtPropertyName(HtmlInputText txtPropertyName) {
//	this.txtPropertyName = txtPropertyName;
//}
//
//
//
//public HtmlInputText getTxtInputTenantName() {
//	return txtInputTenantName;
//}
//
//
//
//public void setTxtInputTenantName(HtmlInputText txtInputTenantName) {
//	this.txtInputTenantName = txtInputTenantName;
//	
//	
//}
//
//
//
//public String getUnitNumber() {
//	return unitNumber;
//}
//
//
//
//public void setUnitNumber(String unitNumber) {
//	this.unitNumber = unitNumber;
//	
//	if(this.unitNumber !=null)
//		viewRootMap.put("unitNumber",this.unitNumber);
//}
//
//
//
//public List<SelectItem> getOwnershipTypeSelectList() {
//	return ownershipTypeSelectList;
//}
//
//
//
//public void setOwnershipTypeSelectList(List<SelectItem> ownershipTypeSelectList) {
//	this.ownershipTypeSelectList = ownershipTypeSelectList;
//}
//
//
//
//public List<SelectItem> getWorkTypeSelectList() {
//	return workTypeSelectList;
//}
//
//
//
//public void setWorkTypeSelectList(List<SelectItem> workTypeSelectList) {
//	this.workTypeSelectList = workTypeSelectList;
//}
//
//
//
//public List<SelectItem> getMaintenanceTypeSelectList() {
//	return maintenanceTypeSelectList;
//}
//
//
//
//public void setMaintenanceTypeSelectList(
//		List<SelectItem> maintenanceTypeSelectList) {
//	this.maintenanceTypeSelectList = maintenanceTypeSelectList;
//}
//
//
//
//public HtmlSelectOneMenu getRequestSourceCombo() {
//	return requestSourceCombo;
//}
//
//
//
//public void setRequestSourceCombo(HtmlSelectOneMenu requestSourceCombo) {
//	this.requestSourceCombo = requestSourceCombo;
//}
//
//
//
//public HtmlDataTable getSiteVisitDataTable() {
//	return siteVisitDataTable;
//}
//
//
//
//public void setSiteVisitDataTable(HtmlDataTable siteVisitDataTable) {
//	this.siteVisitDataTable = siteVisitDataTable;
//}
//
//
//
//public List<SiteVisitView> getSiteVisitList() {
//	return siteVisitList;
//}
//
//
//
//public void setSiteVisitList(List<SiteVisitView> siteVisitList) {
//	this.siteVisitList = siteVisitList;
//}
//
//
//
//public SiteVisitView getSiteVisitDataItem() {
//	return siteVisitDataItem;
//}
//
//
//
//public void setSiteVisitDataItem(SiteVisitView siteVisitDataItem) {
//	this.siteVisitDataItem = siteVisitDataItem;
//}
//
//
//
//public Integer getPaginatorMaxPages() {
//	return paginatorMaxPages;
//}
//
//
//
//public void setPaginatorMaxPages(Integer paginatorMaxPages) {
//	this.paginatorMaxPages = paginatorMaxPages;
//}
//
//
//
//public Integer getPaginatorRows() {
//	return paginatorRows;
//}
//
//
//
//public void setPaginatorRows(Integer paginatorRows) {
//	this.paginatorRows = paginatorRows;
//}
//
//
//
//public Integer getRecordSize() {
//	return recordSize;
//}
//
//
//
//public void setRecordSize(Integer recordSize) {
//	this.recordSize = recordSize;
//}
//
//
//
//public HtmlDataTable getFollowUpDataTable() {
//	return followUpDataTable;
//}
//
//
//
//public void setFollowUpDataTable(HtmlDataTable followUpDataTable) {
//	this.followUpDataTable = followUpDataTable;
//}
//
//
//
//public List<FollowUpView> getFollowUpList() {
//	return followUpList;
//}
//
//
//
//public void setFollowUpList(List<FollowUpView> followUpList) {
//	this.followUpList = followUpList;
//}
//
//
//
//public FollowUpView getFollowUpDataItem() {
//	return followUpDataItem;
//}
//
//
//
//public void setFollowUpDataItem(FollowUpView followUpDataItem) {
//	this.followUpDataItem = followUpDataItem;
//}
//
//
//public String getRequestId() {
//	if(viewRootMap.containsKey("requestId") && viewRootMap.get("requestId")!=null)
//		requestId= viewRootMap.get("requestId").toString();
//	return requestId;
//}
//
//
//
//public void setRequestId(String requestId) {
//	this.requestId = requestId;
//	
//	if(this.requestId !=null)
//		viewRootMap.put("requestId",this.requestId);
//}
//
//
//
//public RequestView getRequestView() {
//	if(viewRootMap.containsKey("requestView") && viewRootMap.get("requestView")!=null)
//		requestView= (RequestView) viewRootMap.get("requestView");
//	return requestView;
//}
//
//
//
//public void setRequestView(RequestView requestView) {
//	this.requestView = requestView;
//	
//	if(this.requestView !=null)
//		viewRootMap.put("requestView",this.requestView);
//}
//
//
//
//public UnitView getUnitsView() {
//	if(viewRootMap.containsKey("unitsView") && viewRootMap.get("unitsView")!=null)
//		unitsView= (UnitView) viewRootMap.get("unitsView");
//	return unitsView;
//}
//
//
//
//public void setUnitsView(UnitView unitsView) {
//	this.unitsView = unitsView;
//	
//	if(this.unitsView !=null)
//		viewRootMap.put("unitsView",this.unitsView);
//}
//
//
//public String initiateBPELTask(){
//	
//	String methodName="initiateBPELTask";
//	logger.logInfo(methodName+"|"+" Start");
//
//	try {
//		
//		SystemParameters parameters = SystemParameters.getInstance();
//        String endPoint= parameters.getParameter(WebConstants.MAINTENANCE_REQUEST_BPEL_ENDPOINT);
//
//		com.avanza.pims.proxy.pimsmaintenancerequestbpelproxy.PIMSMaintenanceRequestBPELPortClient PIMSMaintenanceRequestBPEL = new com.avanza.pims.proxy.pimsmaintenancerequestbpelproxy.PIMSMaintenanceRequestBPELPortClient();
//		
//		
//		PIMSMaintenanceRequestBPEL.setEndpoint(endPoint);
//		
//		// TODO: Add as the BPEL gets finalized
//		
//		PIMSMaintenanceRequestBPEL.initiate( Long.valueOf( this.getRequestId() ), getLoggedInUser(), null, null);
//		
//		RequestService requestService = new RequestService ();
//		
//		
//		requestService.setRequestAsApprovalRequired(Long.valueOf( this.getRequestId()), getLoggedInUser());
//		
//		this.setPageMode(  PAGE_MODE_APPROVAL_REQUIRED );
//	    
////		setAllViolationsAsSent();
//		
//		setRequestParam("RequestId", this.getRequestId() );
//	
//		logger.logInfo(methodName+"|"+" Finish");
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		logger.logError(methodName+"|"+"Error Occured:"+e);
//	}
//	
//	
//	return "sentTask";
//}
//
//
//
//public org.richfaces.component.html.HtmlPanel getTaskPanel() {
//	return taskPanel;
//}
//
//
//
//public void setTaskPanel(org.richfaces.component.html.HtmlPanel taskPanel) {
//	this.taskPanel = taskPanel;
//}
//
//
//
//public String getSvContractId() {
//	if(viewRootMap.containsKey("svContractId") && viewRootMap.get("svContractId")!=null)
//		svContractId=  viewRootMap.get("svContractId").toString();
//	
//	return svContractId;
//}
//
//
//
//public void setSvContractId(String svContractId) {
//	this.svContractId = svContractId;
//	
//	if(this.svContractId !=null)
//		viewRootMap.put("svContractId",this.svContractId);
//}
//
//
//
//public List<SelectItem> getSvContractSelectList() {
//	return svContractSelectList;
//}
//
//
//
//public void setSvContractSelectList(List<SelectItem> svContractSelectList) {
//	this.svContractSelectList = svContractSelectList;
//	
//	if(this.svContractSelectList !=null)
//		viewRootMap.put("svContractSelectList",this.svContractSelectList);
//}
//
//
//
//public String getEngineerName() {
//	if(viewRootMap.containsKey("engineerName") && viewRootMap.get("engineerName")!=null)
//		engineerName=  viewRootMap.get("engineerName").toString();
//	return engineerName;
//}
//
//
//
//public void setEngineerName(String engineerName) {
//	this.engineerName = engineerName;
//	
//	if(this.engineerName !=null)
//		viewRootMap.put("engineerName",this.engineerName);
//}
//
//
//
//public Date getVisitDate() {
//	if(viewRootMap.containsKey("visitDate") && viewRootMap.get("visitDate")!=null)
//		visitDate= (Date) viewRootMap.get("visitDate");
//	
//	return visitDate;
//}
//
//
//
//public void setVisitDate(Date visitDate) {
//	this.visitDate = visitDate;
//	
//	if(this.visitDate !=null)
//		viewRootMap.put("visitDate",this.visitDate);
//}
//
//
//}
