package com.avanza.pims.web.backingbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.notification.api.ContactInfo;
import com.avanza.notification.api.NotificationFactory;
import com.avanza.notification.api.NotificationProvider;
import com.avanza.notification.api.NotifierType;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.ConstructionServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.RequestReplies;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.AssetClassView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContractorTypeView;
import com.avanza.pims.ws.vo.ContractorView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.pims.ws.vo.ExtendRequestDetailsView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestFieldDetailView;
import com.avanza.pims.ws.vo.RequestRepliesView;
import com.avanza.pims.ws.vo.TenderInquiryView;
import com.avanza.pims.ws.vo.TenderView;

public class TenderInquiryReplayAll extends AbstractController {

	/**
	 * 
	 */
	

	private final Logger logger = Logger.getLogger(TenderInquiryReplayAll.class);

	public interface Keys {
		public static final String TENDER_INQUIRY_LIST = "TENDER_INQUIRY_LIST";
		public static final String TENDER_INQUIRY_LIST_TO_SEND = "TENDER_INQUIRY_LIST_TO_SEND";
		public static final String REQUEST_ID_LIST = "REQUEST_ID_LIST";
	}


	private boolean isEnglishLocale = false;	
	private boolean isArabicLocale = false;
	private List<String> errorMessages;
	private List<String> infoMessages;
	private final String notesOwner = WebConstants.TENDER_INQUIRY_REQUESTS;
	
	Map viewMap = getFacesContext().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
	
	//field used in the tender Inquiry Add jsp
	private Date applicationDate ;
	private String replayText;

	//hdn variable for contractor and tender  
	String hdnRequestId;
	
	private TenderInquiryView tenderInquiryView = new TenderInquiryView();
	private ContractorView contractorView = new ContractorView();
	private TenderView tenderView = new TenderView();
	private CommonUtil commUtil  =new CommonUtil();
	private RequestRepliesView requestRepliesView = new RequestRepliesView();
	private List<TenderInquiryView> tenderInquiryViewList = new ArrayList<TenderInquiryView>();
	
	private List<String> requestIdList = new ArrayList<String>();

	private UploadedFile selectedFile;
	private HtmlInputFileUpload fileUploadCtrl = new HtmlInputFileUpload();
	
	private final String NOTIFICATION_EVENT_PUBLISH = "Event.ExtendApplication.Publish";
	
	ServletContext servletcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		logger.logInfo("Stepped into the init method");

		super.init();

		errorMessages = new ArrayList<String>();
		infoMessages = new ArrayList<String>();
		getApplicationDate();

		try {
			if (!isPostBack()) {
			// load Attactment and Comments 	
				loadAttachmentsAndComments();
			// for setting default date on the date field 	
				setApplicationDate(new Date());
			// for enabling button on the Attachment and Comments tab 	
				canAddAttachmentsAndComments(true);
		 if(!sessionMap.containsKey(WebConstants.FROM_TENDER_INQUIRY_REPLIES))
		   {
			 
				tenderInquiryViewList = (List<TenderInquiryView>)sessionMap.get(Keys.TENDER_INQUIRY_LIST_TO_SEND);
				sessionMap.remove(Keys.TENDER_INQUIRY_LIST_TO_SEND);
				viewMap.put(Keys.TENDER_INQUIRY_LIST_TO_SEND, tenderInquiryViewList);
				
				for (TenderInquiryView tIV:tenderInquiryViewList)
				{
				    requestIdList.add(tIV.getRequestId().toString());
				}
					
			     viewMap.put(Keys.REQUEST_ID_LIST, requestIdList);
		  }
		 else
		 {
		   viewMap.put(WebConstants.FROM_TENDER_INQUIRY_REPLIES, true);	 
		   sessionMap.remove(WebConstants.FROM_TENDER_INQUIRY_REPLIES);
		 }
           
			logger.logInfo("init method completed successfully!");
			}	
		} catch (Exception ex) {
			logger.logError("init crashed due to:" + ex.getStackTrace());
		}
	}
	
	public void prerender(){
		super.prerender();
		
			
	}
	
	
	// Accessors starts from here
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessages() {
		return CommonUtil.getErrorMessages(infoMessages);
	}

	public void setInfoMessages(List<String> infoMessages) {
		this.infoMessages = infoMessages;
	}



	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public String getDateFormat(){
    	return CommonUtil.getDateFormat();
    }
	public TimeZone getTimeZone()
	{
	       return TimeZone.getDefault();
	}
	public String getAsterisk() {
		String asterisk = (String)viewMap.get("asterisk");
		if(asterisk==null)
			asterisk = "";
		return asterisk;
	}
  
	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen==null)
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }
	
	
	
	public Date getApplicationDate() {
		if(viewMap.containsKey("APPLICATION_DATE"))
			applicationDate = (Date)viewMap.get("APPLICATION_DATE");
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
		if(this.applicationDate!=null)
			viewMap.put("APPLICATION_DATE",this.applicationDate);
		
	}

		public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}

	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}
	private String getLoggedInUser()
	{
		String methodName="getLoggedInUser";
    	logger.logInfo(methodName+"|"+" Start");
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
		String loggedInUser = "";
		
		if(session.getAttribute(WebConstants.USER_IN_SESSION) != null)
			{			
			  UserDbImpl user  = (UserDbImpl)session.getAttribute(WebConstants.USER_IN_SESSION);
			  loggedInUser = user.getLoginId();
			}
	
		logger.logInfo(methodName+"|"+" Finish");
		return loggedInUser;
	}
	
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	
	public Boolean saveAttachments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	
	public void loadAttachmentsAndComments(){
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.TENDER_INQUIRY_REQUESTS);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
    	String externalId = WebConstants.Attachment.EXTERNAL_ID_TENDER_INQUIRY;
    	
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.TENDER_INQUIRY_REQUESTS);
	}

	
	public String getReplayText() {
		if(viewMap.containsKey("REPLAY_TEXT"))
			replayText = viewMap.get("REPLAY_TEXT").toString();
		return replayText;

		
	}

	public void setReplayText(String replayText) {
		this.replayText = replayText;
		if(this.replayText!=null)
			viewMap.put("REPLAY_TEXT",this.replayText);

	}
	
	public String replied(){
		
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		DomainDataView ddv = new DomainDataView();
		infoMessages = new ArrayList<String>();
		errorMessages = new ArrayList<String>();
		
		
		try {
    	
		if(replayText!=null && !replayText.equals(""))
		{	
			
			  if(!viewMap.containsKey(WebConstants.FROM_TENDER_INQUIRY_REPLIES))
				  
			  {
		    	
			    	ddv= commUtil.getIdFromType( getDomainDataListForDomainType(WebConstants.REQUEST_STATUS),
							WebConstants.REQUEST_STATUS_REPLIED);
				     
			    	RequestServiceAgent rsa = new RequestServiceAgent();
			    	
			    	requestRepliesView.setAnswer(replayText);
			    	requestRepliesView.setUpdatedBy(getLoggedInUser());
			    	requestRepliesView.setUpdatedOn(new Date());
			    	requestRepliesView.setCreatedBy(getLoggedInUser());
			    	requestRepliesView.setCreatedOn(new Date());
			    	requestRepliesView.setRecordStatus(1L);
			    	requestRepliesView.setIsDeleted(0L);
			    	
			    	DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), "Reply", null);
			    	
			    	if(viewMap.containsKey(Keys.REQUEST_ID_LIST)){
			    	rsa.saveTenderInquiryReply((List<String>)viewMap.get(Keys.REQUEST_ID_LIST),
			    			ddv.getDomainDataId(), requestRepliesView);
			    	}
			    	//put success msg here 
			    	infoMessages.add(CommonUtil.getBundleMessage("tenderInquiryReplies.msg.replaySend"));
			    	generateNotification(WebConstants.Event_TENDERINQUIRY_REPLYSEND);
			  }
			  else
			  {
				 sessionMap.put(WebConstants.TENDER_INQUIRY_ANSWER  , replayText);
				 if(fileUploadCtrl.getUploadedFile()!=null)
				    sessionMap.put(WebConstants.TENDER_INQUIRY_ATTACHED_FILE,fileUploadCtrl.getUploadedFile() );
				 openPopup("callOpenerFn();");
			  }
    	
		}
		else
		{
			//put error msg here that please enter replay first
			 
			errorMessages.add(CommonUtil.getBundleMessage("tenderInquiryReplies.msg.detailRequired"));
			return "success";	
		}
		
		
	     } 
		catch (Exception e) {
			  e.printStackTrace();
		      }
	
		
		return "success";
		
	}
	
	
	private void  getNotificationPlaceHolder(Event placeHolderMap)
	{
		String methodName = "getNotificationPlaceHolder";
		logger.logDebug(methodName+"|Start");
		
		placeHolderMap.setValue("CONTRACTOR_NAME", "Contractor");
		placeHolderMap.setValue("TENDER_NO", "ABC");
		placeHolderMap.setValue("CLOSURE_DATE", "12/12/2009");
		
		logger.logDebug(methodName+"|Finish");
	}
	
	
		
private List<DomainDataView> getDomainDataListForDomainType(String domainType)
{
	List<DomainDataView> ddvList=new ArrayList<DomainDataView>();
	List<DomainTypeView> list = (List<DomainTypeView>) servletcontext.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
	Iterator<DomainTypeView> itr = list.iterator();
	//Iterates for all the domain Types
	while( itr.hasNext() ) 
	{
		DomainTypeView dtv = itr.next();
		if( dtv.getTypeName().compareTo(domainType)==0 ) 
		{
			Set<DomainDataView> dd = dtv.getDomainDatas();
			//Iterates over all the domain datas
			for (DomainDataView ddv : dd) 
			{
			  ddvList.add(ddv);	
			}
			break;
		}
	}
	
	return ddvList;
}

public UploadedFile getSelectedFile() {
	return selectedFile;
}

public void setSelectedFile(UploadedFile selectedFile) {
	this.selectedFile = selectedFile;
}

public HtmlInputFileUpload getFileUploadCtrl() {
	return fileUploadCtrl;
}

public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
	this.fileUploadCtrl = fileUploadCtrl;
}

public String getHdnRequestId() {
	return hdnRequestId;
}

public void setHdnRequestId(String hdnRequestId) {
	this.hdnRequestId = hdnRequestId;
}

public void generateNotification(String eventType)
{
	   String methodName ="generateNotification";
	   String personName ;
	   String tenderNumber;
	   Long personId;
	   List<TenderInquiryView> tenderInquiryList= new ArrayList<TenderInquiryView>();
	   PersonView personView = new PersonView();
	   
	      Boolean success = false;
	            try
	            {
	            	logger.logInfo(methodName+"|Start");
	            	
	            	
	            	
	            	
	            	
	            	PropertyServiceAgent psa =  new PropertyServiceAgent();
	                List<ContactInfo> personsEmailList    = new ArrayList<ContactInfo>(0);
	                NotificationFactory nsfactory = NotificationFactory.getInstance();
	                NotificationProvider notifier = nsfactory.createNotifier(NotifierType.JMSBased);
	                Event event = EventCatalog.getInstance().getMetaEvent(eventType).createEvent();
	                
	                if(viewMap.containsKey(Keys.TENDER_INQUIRY_LIST_TO_SEND)){
	                    
	                    tenderInquiryList = (List<TenderInquiryView>)viewMap.get(Keys.TENDER_INQUIRY_LIST_TO_SEND);
	                    for(TenderInquiryView tIV:tenderInquiryList){
			                    if(getIsEnglishLocale())
			            		  personName = tIV.getContractorNameEn();
			                    else
			                      personName = tIV.getContractorNameAr();
			                    
			            		tenderNumber = tIV.getTenderNumber();
			            		personId = tIV.getContractorId();
			            		personView =  psa.getPersonInformation(personId);
			            		getNotificationPlaceHolder(event,tenderNumber,personName);
			            		personsEmailList = getEmailContactInfos((PersonView)personView);
		                        if(personsEmailList.size()>0)
		                              notifier.fireEvent(event, personsEmailList);
	        			
	                    }
	                  
	                }	
	                  success  = true;
	                  logger.logInfo(methodName+"|Finish");
	            }
	            catch(Exception ex)
	            {
	                  logger.LogException(methodName+"|Finish", ex);
	            }
	          
}
private List<ContactInfo> getEmailContactInfos(PersonView personView)
{
	List<ContactInfo> emailList = new ArrayList<ContactInfo>();
	Iterator iter = personView.getContactInfoViewSet().iterator();
	HashMap previousEmailAddressMap = new HashMap();
	while(iter.hasNext())
	{
		ContactInfoView ciView = (ContactInfoView)iter.next();
		//IF THIS EMAIL ADDRESS IS NOT PRESENT IN PREVIOUS CONTACTINFOS
		if(ciView.getEmail()!=null && ciView.getEmail().length()>0 &&
				!previousEmailAddressMap.containsKey(ciView.getEmail().toLowerCase()))
        {
			previousEmailAddressMap.put(ciView.getEmail().toLowerCase(),ciView.getEmail().toLowerCase());
			emailList.add(new ContactInfo(getLoggedInUser(), personView.getCellNumber(), null, ciView.getEmail()));
			//emailList.add(ciView);
        }
	}
	return emailList;
}



private void openPopup(String javaScriptText) {
	String METHOD_NAME = "openPopup()"; 
	logger.logInfo(METHOD_NAME + " --- STARTED --- ");
	try 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
	} 
	catch (Exception exception) 
	{			
		logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
	}
}
public Integer getPaginatorRows() {		
	return WebConstants.RECORDS_PER_PAGE;
}
public Integer getPaginatorMaxPages() {
	return WebConstants.SEARCH_RESULTS_MAX_PAGES;
}
private void  getNotificationPlaceHolder(Event placeHolderMap,String tenderNumber,String personName)
{
	String methodName = "getNotificationPlaceHolder";
	logger.logDebug(methodName+"|Start");
	
	placeHolderMap.setValue("PERSON_NAME",personName);
	placeHolderMap.setValue("TENDER_NUMBER",tenderNumber);
	
	logger.logDebug(methodName+"|Finish");
	
}




}