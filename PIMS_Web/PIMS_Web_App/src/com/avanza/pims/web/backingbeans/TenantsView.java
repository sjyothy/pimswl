package com.avanza.pims.web.backingbeans;


import java.util.Date;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.avanza.pims.entity.Tenant;
import com.avanza.pims.web.controller.AbstractController;

public class TenantsView  extends AbstractController
{

    String moduleName="TenantsView";
      
    private String tenantId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String datetimeDOB;
    private String personalSecurityCardNo;
    private String passportNo;
    private String datetimepassportissue;
    private String datetimepassportexpiry;
    private String datetimevisaexpiry;
    private String visaNo;
    
    private String workingcompany;
    private String designation;
    private String cellNo;
    private String selectGender;
    private String selectVisaIssuePlace;
    private String selectPassportIssuePlace;
    
    private String company;
    private String selectMaritalStatus;
    private String selectNationality;
    private String govttDeptt;
    private String licenseNo;
    private String licenseIssueDate;
    private String licenseExpiryDate;
    private String selectLicenseIssuePlace;
    private String refNum;
    private String tenantType;
     private HtmlInputText txtRefNum;  
    FacesContext context = null;
    UIViewRoot view =null; 
    
    /** default constructor */
    public  TenantsView()
    {
    	String methodName="Constructor";
    	context = FacesContext.getCurrentInstance();
    	view    = context.getViewRoot();
    	
    	// String passport =context.getExternalContext().getRequestMap().get("passportNumber").toString();
    	//   	Load all the combos present in the screen
    	loadCombos();
    	
    }
    

    /**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Override this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    public void init() {
        ;
    }


    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    public void preprocess() {
        ;
    }


    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    public void prerender() {
        ;
    }


    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called, regardless of whether
     * or not this was the page that was actually rendered.  Override this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     *
     * <p>The default implementation does nothing.</p>
     */
    public void destroy() {
        ;
    }


   
    
    private void loadCombos()
    {
    	String methodName="loadCombos";
    	loadCountries();
    	
    	
    }
    private void loadCountries()
    {
    	String methodName="loadCountries";
           	
    	HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
        //session.getAttribute(name)Attribute("view",view);
    	
    }
    
    public String btnSubmit_Click()
    {
        String methodName = "btnSubmit_Click";
        String eventOutcome="failure";
        
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot view = context.getViewRoot();
        HttpSession session =(HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("view",view);
       /* if(addTenant())
        {
           eventOutcome="success";
        }
        */
        return eventOutcome;
    }
   
    
    private boolean addTenant()
    { 
    	String methodName = "addTenant";
    	boolean isSucceed=true;
    	Tenant tenant= setValues();
    	
    	
    	
    	
    	
    	
    	
    	return isSucceed;
    	
    	
    	
    	
    }
    
    private String rectifyValues(String input)
    {
    	String output=input;
    	output=output.replace("'", "''");
    	
    	return output;
    }
    private Tenant setValues()
    {
    	String methodName = "setValues";
    	Tenant tenant =new Tenant();
    	if(tenantId!=null && !tenantId.equals(""))
    	{
    		tenant.setTenantId(new Long(tenantId));
    	}
    	tenant.setTenantNumber(rectifyValues(refNum));
    	tenant.setTypeId(new Long(tenantType));
    	//tenant.setStatusId(statusId);
    	//tenant.setTitleId(titleId);
    	//tenant.setCreatedBy(createdBy);
    	//tenant.setCreatedOn(createdOn);
    	//tenant.setUpdatedBy(updatedBy);
    	//tenant.setUpdatedOn(updatedOn);
    	//tenant.setIsBlacklisted(isBlacklisted);
    	tenant.setIsDeleted(new Long(0));
    	tenant.setRecordStatus(new Long(1));
    	
    	//if tenant is Individual
    	if(tenantType.equals("1"))
    	{

		    	tenant.setWorkingCompany(rectifyValues(workingcompany));
		    	//tenant.setStatusId(new Long());
		    	tenant.setCellNumber(rectifyValues(cellNo));
		    	tenant.setDesignation(rectifyValues(designation));
		    	tenant.setFirstName(rectifyValues(firstName));
		    	tenant.setGender(selectGender);
		    	tenant.setLastName(rectifyValues(lastName));
		    	tenant.setMiddleName(rectifyValues(middleName));
		    	tenant.setPassportIssuePlaceId(new Long(selectPassportIssuePlace));
		    	tenant.setPassportNumber(rectifyValues(passportNo));
		    	tenant.setPersonalSecurityCardNo(rectifyValues(personalSecurityCardNo));
		    	tenant.setMaritialStatusId(new Long(selectMaritalStatus));
		    	tenant.setNationalityId(new Long(selectNationality));
		    	tenant.setResidenseVisaIssuePlaceId(new Long(selectVisaIssuePlace));
		    	
		    	
		    	tenant.setResidenseVisaExpDate(new Date(datetimevisaexpiry));

		    	tenant.setPassportIssueDate(new Date(datetimepassportissue));
		        tenant.setDateOfBirth(new Date(datetimeDOB));
		    	//tenant.setNationalIdDetail(nationalIdDetail);
		    	
		    	tenant.setPassportExpiryDate(new Date(datetimepassportexpiry));
		    	


    	}
    	//else tenant is either company or Govtt Deptt.
    	else
    	{
		    	tenant.setCompanyName(rectifyValues(company));
		    	tenant.setLicenseIssueDate(new Date(licenseIssueDate));
		    	tenant.setLicenseExpiryDate(new Date(licenseIssueDate));
    		    tenant.setLicenseNumber(rectifyValues(licenseNo));
    		    tenant.setLicenseIssuePlaceId(new Long(selectLicenseIssuePlace));
		    	tenant.setGovtDepartment(rectifyValues(govttDeptt));
    	}
    	return tenant;
    	
    	
    }
    
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDatetimeDOB() {
		return datetimeDOB;
	}
	public void setDatetimeDOB(String datetimeDOB) {
		this.datetimeDOB = datetimeDOB;
	}
	public String getPersonalSecurityCardNo() {
		return personalSecurityCardNo;
	}
	public void setPersonalSecurityCardNo(String personalSecurityCardNo) {
		this.personalSecurityCardNo = personalSecurityCardNo;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getDatetimepassportissue() {
		return datetimepassportissue;
	}
	public void setDatetimepassportissue(String datetimepassportissue) {
		this.datetimepassportissue = datetimepassportissue;
	}
	public String getDatetimevisaexpiry() {
		return datetimevisaexpiry;
	}
	public void setDatetimevisaexpiry(String datetimevisaexpiry) {
		this.datetimevisaexpiry = datetimevisaexpiry;
	}
	public String getVisaNo() {
		return visaNo;
	}
	public void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}
	public String getWorkingcompany() {
		return workingcompany;
	}
	public void setWorkingcompany(String workingcompany) {
		this.workingcompany = workingcompany;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCellNo() {
		return cellNo;
	}
	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
	}
	public String getSelectGender() {
		return selectGender;
	}
	public void setSelectGender(String selectGender) {
		this.selectGender = selectGender;
	}
	public String getSelectVisaIssuePlace() {
		return selectVisaIssuePlace;
	}
	public void setSelectVisaIssuePlace(String selectVisaIssuePlace) {
		this.selectVisaIssuePlace = selectVisaIssuePlace;
	}
	public String getSelectPassportIssuePlace() {
		return selectPassportIssuePlace;
	}
	public void setSelectPassportIssuePlace(String selectPassportIssuePlace) {
		this.selectPassportIssuePlace = selectPassportIssuePlace;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getSelectMaritalStatus() {
		return selectMaritalStatus;
	}
	public void setSelectMaritalStatus(String selectMaritalStatus) {
		this.selectMaritalStatus = selectMaritalStatus;
	}
	public String getSelectNationality() {
		return selectNationality;
	}
	public void setSelectNationality(String selectNationality) {
		this.selectNationality = selectNationality;
	}
	public String getGovttDeptt() {
		return govttDeptt;
	}
	public void setGovttDeptt(String govttDeptt) {
		this.govttDeptt = govttDeptt;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getLicenseIssueDate() {
		return licenseIssueDate;
	}
	public void setLicenseIssueDate(String licenseIssueDate) {
		this.licenseIssueDate = licenseIssueDate;
	}
	public String getLicenseExpiryDate() {
		return licenseExpiryDate;
	}
	public void setLicenseExpiryDate(String licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
	}
	public String getSelectLicenseIssuePlace() {
		return selectLicenseIssuePlace;
	}
	public void setSelectLicenseIssuePlace(String selectLicenseIssuePlace) {
		this.selectLicenseIssuePlace = selectLicenseIssuePlace;
	}
	public UIViewRoot getView() {
		return view;
	}
	public void setView(UIViewRoot view) {
		this.view = view;
	}
	public String getTenantType() {
		return tenantType;
	}
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public HtmlInputText getTxtRefNum() {
		return txtRefNum;
	}
	public void setTxtRefNum(HtmlInputText txtRefNum) {
		this.txtRefNum = txtRefNum;
	}


	public String getDatetimepassportexpiry() {
		return datetimepassportexpiry;
	}


	public void setDatetimepassportexpiry(String datetimepassportexpiry) {
		this.datetimepassportexpiry = datetimepassportexpiry;
	}

}
