package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderView;

public class EditBidPrice extends AbstractController{

	//Fields
	
	 private transient Logger logger = Logger.getLogger(EditBidPrice.class);
	
	 private String unitNumber;
	 
	 private String unitType;
	 
	 private String bidderName;
	 
	 private Long bidPrice;
	 
	 private Boolean bidWon;
	 
	//Constructor
	/** default constructor */
	public EditBidPrice(){				
	}
	
	//Methods
	public String save(){
		String METHOD_NAME = "save()";
		logger.logInfo(METHOD_NAME + " started...");
		try{
			Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			AuctionWinnersView auctionWinnersView = (AuctionWinnersView)sessionMap.get(WebConstants.ConductAuction.AUCTION_WINNER);
			
		//	auctionWinnersView.setBidPrice(getBidPrice());
			auctionWinnersView.setAuctionWin(getBidWon());
			
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			pSAgent.updateAuctionWinner(auctionWinnersView);
			/////////////////////////////////////////////////////////////////////////////
			final String viewId = "/EditBidPrice.jsp"; 
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();        
	
	        // This is the proper way to get the view's url
	        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	        String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	
	        //Your Java script code here
	        String javaScriptText = "javascript:submitAndClose();";        
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
		catch (Exception e) {	
			logger.logInfo(METHOD_NAME + " crashed...");
			e.printStackTrace();
		}		 
        logger.logInfo(METHOD_NAME + " ended...");
		return "";
	}
	
	 private void loadInfo() {
		 String METHOD_NAME = "loadInfo()";
		 logger.logInfo(METHOD_NAME + " started...");
		 try{		 
			 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			 AuctionWinnersView auctionWinnersView = (AuctionWinnersView)sessionMap.get(WebConstants.ConductAuction.AUCTION_WINNER);
			 unitNumber = auctionWinnersView.getUnitNo();		 
			 unitType = auctionWinnersView.getUnitTypeEn();		 
			 bidderName = auctionWinnersView.getBidderName();		 
		//	 bidPrice = auctionWinnersView.getBidPrice();		 
			 bidWon = auctionWinnersView.getAuctionWin();
		 } catch (Exception e) {	
			logger.logInfo(METHOD_NAME + " crashed...");
			e.printStackTrace();
		}		 
		logger.logInfo(METHOD_NAME + " ended...");		 
	}
	//Getters and Setters
	
	/**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Override this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void init() {
		// TODO Auto-generated method stub		
		super.init();	
		if(!isPostBack())
			loadInfo();
	}


	/**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void prerender() {
		super.prerender();
	}
	
    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called, regardless of whether
     * or not this was the page that was actually rendered.  Override this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
    public void destroy() {
        super.destroy();
    }

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getBidderName() {
		return bidderName;
	}

	public void setBidderName(String bidderName) {
		this.bidderName = bidderName;
	}

	public Long getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(Long bidPrice) {
		this.bidPrice = bidPrice;
	}

	public Boolean getBidWon() {
		return bidWon;
	}

	public void setBidWon(Boolean bidWon) {
		this.bidWon = bidWon;
	}


	 
}
