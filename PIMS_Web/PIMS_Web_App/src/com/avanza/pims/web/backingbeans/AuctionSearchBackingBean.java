package com.avanza.pims.web.backingbeans;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.AuctionUnitView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.ui.util.ResourceUtil;
import com.businessobjects.crystalreports.viewer.core.au;
import com.avanza.pims.report.criteria.AdvertisementCriteria;
import com.avanza.pims.report.criteria.AuctionReportCriteria;
import com.avanza.pims.report.criteria.ContractDisclosureReportCriteria;


public class AuctionSearchBackingBean extends AbstractController
{
	private transient Logger logger = Logger.getLogger(AuctionSearchBackingBean.class);

	PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	AdvertisementCriteria advertisementCriteira;
	/**
	 * 
	 */
	private ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	private Map<String,Long> auctionStatusMap = new HashMap<String,Long>();
	
	public AuctionSearchBackingBean(){
		System.out.println("AuctionSearchBackingBean Constructor");
		
	}
	
	private String message;
	
	private AuctionView auctionView = new AuctionView();
	
	private AuctionView dataItem = new AuctionView();
	private List<AuctionView> dataList = new ArrayList<AuctionView>();
	
	private List<SelectItem> auctionStatuses = new ArrayList<SelectItem>();
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private String hhStart = "";
	private String mmStart = "";
	private String hhEnd = "";
	private String mmEnd = "";
	private List<String> errorMessages;
	private String infoMessage = "";
	private HtmlDataTable dataTable;
	private HtmlInputHidden addCount = new HtmlInputHidden();
	private String sortField = null;
	private Boolean sortAscending = true;
	
	private HtmlCalendar htmlCalendarRequestStartDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarRequestStartDateTo = new HtmlCalendar();

	private HtmlCalendar htmlCalendarRequestEndDateFrom = new HtmlCalendar();
	private HtmlCalendar htmlCalendarRequestEndDateTo = new HtmlCalendar();
	
	private HtmlSelectOneMenu htmlSelectOneAuctionStatus = new HtmlSelectOneMenu();
	
	private Boolean isEnglishLocale = false;
	private Boolean isArabicLocale = false;
	Map viewRootMap;
	String VIEW_MODE="VIEW_MODE";
	private String  DEFAULT_SORT_FIELD = "auctionNumber";
	

	/*
	 * Methods
	 */
	
	public boolean getIsViewModePopUp(){
		 
		 if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase("popup"))
			 return true;
		 else 
			 return false;
	 }
	
	@SuppressWarnings("unchecked")
	public void clearSessionMap(){
		logger.logInfo("clearSessionMap() started...");
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		try{
//			sessionMap.remove("auctionList");
   		    /*sessionMap.remove(WebConstants.FROM_SEARCH);
			sessionMap.remove(WebConstants.AUCTION_STATUS_MAP);
			sessionMap.remove(WebConstants.ROW_ID);
			sessionMap.remove(WebConstants.FIRST_TIME);
			sessionMap.remove(WebConstants.VIEW_MODE);
			sessionMap.remove(WebConstants.OPENINIG_PRICE);
			sessionMap.remove(WebConstants.DEPOSIT_AMOUNT);
			sessionMap.remove(WebConstants.SELECT_VENUE_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_MODE);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ACTION);
			sessionMap.remove(WebConstants.CONDUCT_AUCTION_MODE);
			sessionMap.remove(WebConstants.CAN_COMPLETE_TASK);
			sessionMap.remove(WebConstants.ALL_SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.SELECTED_AUCTION_UNITS);
			sessionMap.remove(WebConstants.AUCTION_ID);
			sessionMap.remove(WebConstants.ADVERTISEMENT_ID);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.FULL_VIEW_MODE);*/
			logger.logInfo("clearSessionMap() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("clearSessionMap() crashed ", exception);
		}
	}
	
	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public Boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public Boolean getIsEnglishLocale()
	{
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	
	@SuppressWarnings("unchecked")
	public List<AuctionView> getAuctionDataList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		dataList = (List<AuctionView>)viewMap.get("auctionList");
		if(dataList==null)
			dataList = new ArrayList<AuctionView>();
		return dataList;
	}
	
	private Map<String,Object> getSearchCriteria()
	{
		Map<String,Object> criteria = new HashMap<String,Object>();
		
		Object requestStartDateFrom = htmlCalendarRequestStartDateFrom.getValue();
		Object requestStartDateTo  = htmlCalendarRequestStartDateTo.getValue();
		Object requestEndDateFrom = htmlCalendarRequestEndDateFrom.getValue();
		Object requestEndDateTo = htmlCalendarRequestEndDateTo.getValue();
		
		if(requestStartDateFrom !=null && !requestStartDateFrom.toString().equals(""))
			criteria.put(WebConstants.AUCTION_SEARCH_CRITERIA.REQUEST_START_DATE_FROM, requestStartDateFrom);
		
		if(requestStartDateTo !=null && !requestStartDateTo.toString().equals(""))
			criteria.put(WebConstants.AUCTION_SEARCH_CRITERIA.REQUEST_START_DATE_TO, requestStartDateTo);
		
		if(requestEndDateFrom !=null && !requestEndDateFrom.toString().equals(""))
			criteria.put(WebConstants.AUCTION_SEARCH_CRITERIA.REQUEST_END_DATE_FROM, requestEndDateFrom);
		
		if(requestEndDateTo !=null && !requestEndDateTo.toString().equals(""))
			criteria.put(WebConstants.AUCTION_SEARCH_CRITERIA.REQUEST_END_DATE_TO, requestEndDateTo);
		
		return criteria;
	}
	public void doSearchItemList()
	{
		loadAuctionList();
	}
	@SuppressWarnings("unchecked")
	private void loadAuctionList() {
		logger.logInfo("loadAuctionList() started...");
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		auctionStatusMap = (Map<String,Long>) viewMap.get(WebConstants.AUCTION_STATUS_MAP);
		
    	try{		
			dataList = new ArrayList<AuctionView>();
			
			String dateFormat = getDateFormat();
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			
			if(getIsViewModePopUp())
			{
				Long auctionStatusReadyForRegistration =propertyServiceAgent.getDomainDataByValue(
						WebConstants.Statuses.AUCTION_STATUS,
						WebConstants.Statuses.AUCTION_STATUS_READY_FOR_REQUESTS)
						.getDomainDataId();
				
				auctionView.setAuctionStatusId(auctionStatusReadyForRegistration.toString());
				
			}
			
			Map<String,Object> searchCriteria =  getSearchCriteria();
			searchCriteria.put(WebConstants.AUCTION_SEARCH_CRITERIA.AUCTION_VIEW, auctionView);
			
            PropertyService psa = new PropertyService();
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  psa.searchAuctionGetTotalNumberOfRecords(searchCriteria);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			
			
			List<AuctionView> auctionList = propertyServiceAgent.getAuctionsByCriteria(searchCriteria, getRowsPerPage(), getCurrentPage(), getSortField(), isSortItemListAscending());
//			List<AuctionView> auctionList = propertyServiceAgent.getAuctions(auctionView);
			viewMap.put("auctionList", auctionList);
			recordSize = totalRows;
			viewMap.put("recordSize", recordSize);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
//			getFacesContext().getExternalContext().getRequestMap().containsKey("pageNumber")
//			HttpServletRequest temp1 = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
//			temp.getAttribute("pageNumber")
//			temp1.getParameter("pageNumber")
//			setRequestParam("pageNumber", 1);
			Long statusId = null;
			if(!auctionList.isEmpty())
			{
				for(AuctionView temp: auctionList){
					if(temp.getAuctionDateVal()!=null)
					{
						String dateStr = formatter.format(temp.getAuctionDateVal());
						temp.setAuctionDate(dateStr);
					}
					if(temp.getCreatedOn()!=null)
					{
						String dateStr = formatter.format(temp.getCreatedOn());
						temp.setAuctionCreationDateString(dateStr);
					}
					
					Boolean set = false;
					statusId = Long.valueOf(temp.getAuctionStatusId());
					if(statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_DRAFT_STATUS))==0)
					{
						temp.setAuctionEditMode(true);
						temp.setAuctionViewMode(false);
						temp.setAuctionConductMode(false);
						temp.setAuctionRefundMode(false);
						set = true;
					}
					else if(statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_READY_STATUS))==0)
					{
						set = true;
						temp.setAuctionViewMode(false);
						temp.setAuctionConductMode(true);
						temp.setAuctionEditMode(false);
						temp.setAuctionRefundMode(false);
						Boolean winnerFound = false;
						List<AuctionUnitView> auctionUnits = temp.getAuctionUnits();
						if(!auctionUnits.isEmpty()){
							for(AuctionUnitView auctionUnit: auctionUnits){
								if(auctionUnit.getBidderId()!=null)
									winnerFound = true;
							}
						}
						Date today = new Date();
						if(temp.getAuctionDateVal()!=null && today.before(temp.getAuctionDateVal()) && !winnerFound){
							temp.setAuctionCancelMode(true);
						}
					}
					else if(statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_REFUND_APPROVED_STATUS))==0)
					{
						set = true;
						temp.setAuctionViewMode(true);
						temp.setAuctionConductMode(false);
						temp.setAuctionEditMode(false);
						temp.setAuctionRefundMode(true);
					}
					else
					{
						temp.setAuctionEditMode(false);
						temp.setAuctionViewMode(true);
						temp.setAuctionConductMode(false);
						temp.setAuctionRefundMode(false);
						set = true;
					}
					
					/*if( (statusId.compareTo(auctionViewModeId)==0) ||(statusId.compareTo(auctionCompleteModeId)==0))						
					{
						temp.setAuctionViewMode(true);
						set = true;
					}
					else
						temp.setAuctionViewMode(false);
					
					if(statusId.compareTo(auctionVenueSelectModeId)==0)
					{
						set = true;
						temp.setAuctionVenueSelectMode(true);
					}
					else
						temp.setAuctionVenueSelectMode(false);
					*/
					
					
					/*else
						temp.setAuctionConductMode(false);*/
					
					if(!set)
						temp.setAuctionViewMode(true);
					
					dataList.add(temp);
				}
			}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			logger.logInfo("loadAuctionList() completed successfully!!!");
		}
		catch(PimsBusinessException exception){
			logger.LogException("loadAuctionList() crashed ",exception);
		}
		catch(Exception exception){
			logger.LogException("loadAuctionList() crashed ",exception);
		}
	}	
	
	/*
	 *  Validation Methods
	 */
	
	public String getInvalidMessage(String field){
		
		StringBuffer message = new StringBuffer("");
		logger.logInfo("getInvalidMessage(String) started...");
		try
		{
		message = message.append(CommonUtil.getBundleMessage(field)).append(" ").append(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.INVALID));
		logger.logInfo("getInvalidMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getInvalidMessage(String) crashed ", exception);
		}
		return message.toString();
	}
	
	public Boolean atleastOneCriterionEntered() {
		if(StringHelper.isNotEmpty(auctionView.getAuctionNumber()))
			return true;
		if(StringHelper.isNotEmpty(auctionView.getAuctionTitle()))
			return true;
		Date requestStartDateFrom = (Date) htmlCalendarRequestStartDateFrom.getValue();
		Date requestStartDateTo = (Date) htmlCalendarRequestStartDateTo.getValue();
		Date requestEndDateFrom = (Date) htmlCalendarRequestEndDateFrom.getValue();
		Date requestEndDateTo = (Date) htmlCalendarRequestEndDateTo.getValue();
		if(requestStartDateFrom!=null)
			return true;
		if(requestStartDateTo!=null)
			return true;
		if(requestEndDateFrom!=null)
			return true;
		if(requestEndDateTo!=null)
			return true;
		if(auctionView.getAuctionDateVal()!=null)
			return true;
		if(auctionView.getCreatedOn()!=null)
			return true;
		if(StringHelper.isNotEmpty(hhStart))
			return true;
		if(StringHelper.isNotEmpty(hhEnd))
			return true;
		if(StringHelper.isNotEmpty(mmStart))
			return true;
		if(StringHelper.isNotEmpty(mmEnd))
			return true;
		if(StringHelper.isNotEmpty(auctionView.getVenueName()))
			return true;
		if(StringHelper.isNotEmpty(auctionView.getAuctionStatusId()) && !auctionView.getAuctionStatusId().equals("0"))
			return true;
		return false;
	}
	
	public Boolean validateFields() {
		Boolean validated = true;
		errorMessages = new ArrayList<String>();
		
		Date requestStartDateFrom = (Date) htmlCalendarRequestStartDateFrom.getValue();
		Date requestStartDateTo = (Date) htmlCalendarRequestStartDateTo.getValue();
		Date requestEndDateFrom = (Date) htmlCalendarRequestEndDateFrom.getValue();
		Date requestEndDateTo = (Date) htmlCalendarRequestEndDateTo.getValue();
		
		if(!atleastOneCriterionEntered()){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonsMessages.NO_FILTER_ADDED));
			validated = false;
			return validated;
		}
		
		if(requestStartDateFrom!=null && requestStartDateTo!=null && requestStartDateFrom.after(requestStartDateTo)){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.SearchAuction.MSG_START_DATE_FROM_TO_INVALID));
			validated = false;
		}
		
		if(requestEndDateFrom!=null && requestEndDateTo!=null && requestEndDateFrom.after(requestEndDateTo)){
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.SearchAuction.MSG_END_DATE_FROM_TO_INVALID));
			validated = false;
		}
		
		if( ((hhStart.trim().length()>0) && (mmStart.trim().length()==0)) || ((mmStart.trim().length()>0) && (hhStart.trim().length()==0)) )
		{
			 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.START_TIME));
			 validated = false;
		}
		if( ((hhEnd.trim().length()>0) && (mmEnd.trim().length()==0)) || ((mmEnd.trim().length()>0) && (hhEnd.trim().length()==0)) )
		{
			 errorMessages.add(getInvalidMessage(WebConstants.PropertyKeys.Auction.END_TIME));
			 validated = false;
		}

		/*if (auctionView.getAuctionNumber().lastIndexOf(" ")>=0) {
		 errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.NUMBER) + " " + getBundleMessage(WebConstants.PropertyKeys.Commons.Validation.SHOULD_NOT_HAVE_SPACE));
		 validated = false;
		}*/
		if (auctionView.getAuctionDateVal()==null){
			hhStart = hhStart.trim();
			mmStart = mmStart.trim();
			hhEnd = hhEnd.trim();
			mmEnd = mmEnd.trim();
			String tempStartTime = hhStart + mmStart;
			String tempEndTime = hhEnd + mmEnd;
			if( (!tempStartTime.equals("")) || (!tempEndTime.equals(""))) 
			{
			 errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.Validation.SHOULD_HAVE_AUCTION_DATE));
			 validated = false;
			}
		}

		if(validated){
			auctionView.setAuctionStartTime(hhStart+":"+mmStart);
			auctionView.setAuctionEndTime(hhEnd+":"+mmEnd);
		}
		return validated;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	
	
	/*
	 *  JSF Lifecycle methods 
	 */
			
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();	
		
		try
		{
			if(!isPostBack())
			{
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
		        setSortField(DEFAULT_SORT_FIELD);
				clearSessionMap();
				loadStatusMap();
				
				HttpServletRequest request =
					 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
				
				boolean isPopup = false;
				
				if(request.getParameter("viewMode")!=null)
					 {
					   viewRootMap.put(VIEW_MODE, request.getParameter("viewMode").toString());
					   isPopup = true;
					 }
				 else if(getRequestParam("viewMode")!=null)
				 {
					 viewRootMap.put(VIEW_MODE, getRequestParam("viewMode").toString());
					 isPopup = true;
				 }
				
			}
			logger.logInfo("init() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("init() crashed ", exception);
		}

//		searchAuctions();
		System.out.println("AuctionSearchBackingBean init");
	}

	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		System.out.println("AuctionSearchBackingBean preprocess");
	}

	@Override
	public void prerender() {
		// TODO Auto-generated method stub
		super.prerender();
		System.out.println("AuctionSearchBackingBean prerender");
	}
	
	
	private void loadStatusMap(){
		List<DomainTypeView> list = (List<DomainTypeView>) context.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		while( itr.hasNext() ) {
			DomainTypeView dtv = itr.next();
			if( dtv.getTypeName().compareTo(WebConstants.Statuses.AUCTION_STATUS)==0 ) {
				Set<DomainDataView> dd = dtv.getDomainDatas();
				for (DomainDataView ddv : dd) {
					auctionStatusMap.put(ddv.getDataValue(),ddv.getDomainDataId());
				}
			}
		}
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put(WebConstants.AUCTION_STATUS_MAP,auctionStatusMap);
	}
	
	/*
	 * Button Click Action Handlers 
	 */
	 
	
	public String cancelAuction()
    {
    	logger.logInfo("cancelAuction() started...");
    	try{
    		 clearSessionMap();
    		 AuctionView aucView=(AuctionView)dataTable.getRowData();
    		 Boolean success = propertyServiceAgent.cancelAuction(aucView.getAuctionId(), CommonUtil.getLoggedInUser());
    		 if(success){
    			 DomainDataView domainData = new UtilityServiceAgent().getDomainDataByValue(WebConstants.AUCTION_STATUS, WebConstants.AUCTION_CANCELLED_STATUS);
        		 if(domainData!=null){
        			 aucView.setAuctionStatus(domainData.getDataDescEn());
        			 aucView.setAuctionStatusAr(domainData.getDataDescAr());
        			 aucView.setAuctionCancelMode(false);
        		 }
    			 infoMessage =  CommonUtil.getBundleMessage(MessageConstants.SearchAuction.MSG_CANCEL_AUCTION_SUCCESS);
    		 }
    		 logger.logInfo("cancelAuction() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		logger.LogException("cancelAuction() crashed ",exception);
    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.SearchAuction.MSG_CANCEL_AUCTION_FAILURE));
    	}
    	catch(Exception exception){
    		logger.LogException("cancelAuction() crashed ",exception);
    		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.SearchAuction.MSG_CANCEL_AUCTION_FAILURE));
    	}
        return "cancelAuction";
    }
	
	public String auctionDetailsEditMode()
    {
    	logger.logInfo("auctionDetailsEditMode() started...");
    	try{
    		 clearSessionMap();
    		 AuctionView aucView=(AuctionView)dataTable.getRowData();
    		 Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    		 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    		 sessionMap.put(WebConstants.VIEW_MODE,WebConstants.EDIT_MODE);
    		 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
    		 setRequestParam(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_AUCTION_SEARCH);
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
    		logger.logInfo("auctionDetailsEditMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsEditMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsEditMode() crashed ",exception);
    	}
        return "auctionDetailsEditMode";
    }
	
	
	public String auctionDetailsViewMode()
    {
    	logger.logInfo("auctionDetailsViewMode() started...");
    	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
    	Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		auctionStatusMap = (Map<String,Long>) viewMap.get(WebConstants.AUCTION_STATUS_MAP);
		String viewScreen = "auctionDetailsViewMode";
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Long statusId = Long.parseLong(aucView.getAuctionStatusId());
    	 	sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
    	 	sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
    	 	if((statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_COMPLETED_STATUS))==0) 
    	 		|| (statusId.compareTo(auctionStatusMap.get(WebConstants.Statuses.AUCTION_REFUND_APPROVED_STATUS))==0))
    	 	{
    	 		viewScreen = "auctionCompletedViewMode";
    	 	}
    	 	
   		logger.logInfo("auctionDetailsViewMode() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
    	}
        return viewScreen;
    }
	
	public String auctionDetailsFullViewMode()
    {
    	logger.logInfo("auctionDetailsViewMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.FULL_VIEW_MODE,WebConstants.FULL_VIEW_MODE);
    	 	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionDetailsViewMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
    	}
        return "auctionDetailsViewMode";
    }
	
	public String auctionDetailsConductMode()
    {
    	logger.logInfo("auctionDetailsConductMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.CONDUCT_AUCTION_MODE,WebConstants.CONDUCT_AUCTION_MODE);
    	  	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionDetailsConductMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionDetailsViewMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsConductMode() crashed ",exception);
    	}
        return "auctionDetailsConductMode";
    }
	
	public String auctionDetailsRefundMode()
    {
    	logger.logInfo("auctionDetailsRefundMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	sessionMap.put("isRefundAuctionFinalMode", true);
    	 	
   		logger.logInfo("auctionDetailsRefundMode() completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("auctionDetailsRefundMode() crashed ",exception);
    	}
        return "auctionDetailsRefundMode";
    }
	
	public String auctionSelectDateVenueMode()
    {
    	logger.logInfo("auctionSelectDateVenueMode() started...");
    	try{
    		clearSessionMap();
            AuctionView aucView=(AuctionView)dataTable.getRowData();
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 	 sessionMap.put(WebConstants.AUCTION_ID,aucView.getAuctionId());
    	 	 sessionMap.put(WebConstants.SELECT_VENUE_MODE,WebConstants.SELECT_VENUE_MODE);
    	 	 sessionMap.put(WebConstants.VIEW_MODE,WebConstants.VIEW_MODE);
    	 	 sessionMap.put(WebConstants.FROM_SEARCH,WebConstants.FROM_SEARCH);
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
//           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
   		logger.logInfo("auctionSelectDateVenueMode() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("auctionSelectDateVenueMode() crashed ",exception);
    	}
        return "auctionSelectDateVenueMode";
    }
	
    public String searchAuctions()
    {
    	logger.logInfo("searchAuctions() started...");
    	try{
    		if(validateFields()){
    			//auctionView.setAuctionStartTime(hhStart+":"+mmStart);
    			loadAuctionList();
    		}
    			
    		logger.logInfo("searchAuctions() completed successfully!!!");
    	}
//    	catch(PimsBusinessException exception){
//    		logger.LogException("searchAuctions() crashed ",exception);
//    	}
    	catch(Exception exception){
    		logger.LogException("searchAuctions() crashed ",exception);
    	}
        return "searchAuctions";
    }
	
    public void cancel(ActionEvent event) {
		logger.logInfo("cancel() started...");
		try {
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String javaScriptText = "window.close();";
		
		        clearSessionMap();
		        
		        // Add the Javascript to the rendered page's header for immediate execution
		        AddResource addResource = AddResourceFactory.getInstance(facesContext);
		        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		        logger.logInfo("cancel() completed successfully!!!");
			}
			catch (Exception exception) {
				logger.LogException("cancel() crashed ", exception);
			}
    }
    
    	
	/*
	 * Setters / Getters
	 */
    
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the auctionView
	 */
	public AuctionView getAuctionView() {
//		AuctionVenueView temp = new AuctionVenueView();
//		temp.setVenueName(auctionView.getVenueName());
//		auctionView.setAuctionVenue(temp);
		return auctionView;
	}

	/**
	 * @param auctionView the auctionView to set
	 */
	public void setAuctionView(AuctionView auctionView) {
//		AuctionVenueView temp = new AuctionVenueView();
//		temp.setVenueName(auctionView.getVenueName());
//		auctionView.setAuctionVenue(temp);
		this.auctionView = auctionView;
	}

	/**
	 * @return the dataItem
	 */
	public AuctionView getDataItem() {
		return dataItem;
	}

	/**
	 * @param dataItem the dataItem to set
	 */
	public void setDataItem(AuctionView dataItem) {
		this.dataItem = dataItem;
	}

	/**
	 * @return the dataList
	 */
	public List<AuctionView> getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(List<AuctionView> dataList) {
		this.dataList = dataList;
	}

	

	
	/**
	 * @return the dataTable
	 */
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the addCount
	 */
	public HtmlInputHidden getAddCount() {
		return addCount;
	}

	/**
	 * @param addCount the addCount to set
	 */
	public void setAddCount(HtmlInputHidden addCount) {
		this.addCount = addCount;
	}

	

	/**
	 * @return the sortAscending
	 */
	public Boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the auctionStatuses
	 */
	public List<SelectItem> getAuctionStatuses() {
		return auctionStatuses;
	}

	/**
	 * @param auctionStatuses the auctionStatuses to set
	 */
	public void setAuctionStatuses(List<SelectItem> auctionStatuses) {
		this.auctionStatuses = auctionStatuses;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * @return the infoMessage
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * @param infoMessage the infoMessage to set
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(ServletContext context) {
		this.context = context;
	}

	/**
	 * @return the auctionStatusMap
	 */
	public Map<String, Long> getAuctionStatusMap() {
		return auctionStatusMap;
	}

	/**
	 * @param auctionStatusMap the auctionStatusMap to set
	 */
	public void setAuctionStatusMap(Map<String, Long> auctionStatusMap) {
		this.auctionStatusMap = auctionStatusMap;
	}

	/**
	 * @return the hhStart
	 */
	public String getHhStart() {
		return hhStart;
	}

	/**
	 * @param hhStart the hhStart to set
	 */
	public void setHhStart(String hhStart) {
		this.hhStart = hhStart;
	}

	/**
	 * @return the mmStart
	 */
	public String getMmStart() {
		return mmStart;
	}

	/**
	 * @param mmStart the mmStart to set
	 */
	public void setMmStart(String mmStart) {
		this.mmStart = mmStart;
	}

	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	/**
	 * @return the sortAscending
	 */
	public Boolean getSortAscending() {
		return sortAscending;
	}

	/**
	 * @param isEnglishLocale the isEnglishLocale to set
	 */
	public void setIsEnglishLocale(Boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	/**
	 * @param isArabicLocale the isArabicLocale to set
	 */
	public void setIsArabicLocale(Boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}


	/**
	 * @return the hhEnd
	 */
	public String getHhEnd() {
		return hhEnd;
	}

	/**
	 * @param hhEnd the hhEnd to set
	 */
	public void setHhEnd(String hhEnd) {
		this.hhEnd = hhEnd;
	}

	/**
	 * @return the mmEnd
	 */
	public String getMmEnd() {
		return mmEnd;
	}

	/**
	 * @param mmEnd the mmEnd to set
	 */
	public void setMmEnd(String mmEnd) {
		this.mmEnd = mmEnd;
	}
	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	
	public void auctionDetailsViewMode(javax.faces.event.ActionEvent event){
		logger.logInfo("auctionDetailsViewMode() started...");
		try {
			AuctionView aucView = (AuctionView) dataTable.getRowData();
			Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
			requestMap.put(WebConstants.AUCTION_ID, aucView.getAuctionId());
			requestMap.put(WebConstants.VIEW_MODE, WebConstants.VIEW_MODE);
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			sessionMap.put("isAuctionSelected", true);
			FacesContext facesContext = FacesContext.getCurrentInstance();
            String javaScriptText = "javascript:closeWindowSubmit();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.AUCTION_ID,aucView.getAuctionId().toString());
			// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(WebConstants.DISPLAY_MODE,WebConstants.VIEW_MODE);
			logger
					.logInfo("auctionDetailsViewMode() completed successfully!!!");
		}
		// catch(PimsBusinessException exception){
		// logger.LogException("auctionDetailsViewMode() crashed ",exception);
		// }
		catch (Exception exception) {
			logger.LogException("auctionDetailsViewMode() crashed ", exception);
		}
//		return "auctionDetailsViewMode";
	}
	public void printAdvertisment()
	{
		if(getIsEnglishLocale())
			advertisementCriteira=new AdvertisementCriteria(ReportConstant.Report.ADVERTISEMENT_REPORT_EN, ReportConstant.Processor.ADVERTISEMENT_REPORT,CommonUtil.getLoggedInUser());
		else
			advertisementCriteira=new AdvertisementCriteria(ReportConstant.Report.ADVERTISEMENT_REPORT_AR, ReportConstant.Processor.ADVERTISEMENT_REPORT,CommonUtil.getLoggedInUser());
		AuctionView auctionView=(AuctionView)dataTable.getRowData();
		
		advertisementCriteira.setAuctionId(auctionView.getAuctionId());
		HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY,advertisementCriteira);
		openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
	}
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try { 
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}
	public HtmlSelectOneMenu getHtmlSelectOneAuctionStatus() {
		return htmlSelectOneAuctionStatus;
	}

	public void setHtmlSelectOneAuctionStatus(
			HtmlSelectOneMenu htmlSelectOneAuctionStatus) {
		this.htmlSelectOneAuctionStatus = htmlSelectOneAuctionStatus;
	}

	public HtmlCalendar getHtmlCalendarRequestStartDateFrom() {
		return htmlCalendarRequestStartDateFrom;
	}

	public void setHtmlCalendarRequestStartDateFrom(
			HtmlCalendar htmlCalendarRequestStartDateFrom) {
		this.htmlCalendarRequestStartDateFrom = htmlCalendarRequestStartDateFrom;
	}

	public HtmlCalendar getHtmlCalendarRequestStartDateTo() {
		return htmlCalendarRequestStartDateTo;
	}

	public void setHtmlCalendarRequestStartDateTo(
			HtmlCalendar htmlCalendarRequestStartDateTo) {
		this.htmlCalendarRequestStartDateTo = htmlCalendarRequestStartDateTo;
	}

	public HtmlCalendar getHtmlCalendarRequestEndDateFrom() {
		return htmlCalendarRequestEndDateFrom;
	}

	public void setHtmlCalendarRequestEndDateFrom(
			HtmlCalendar htmlCalendarRequestEndDateFrom) {
		this.htmlCalendarRequestEndDateFrom = htmlCalendarRequestEndDateFrom;
	}

	public HtmlCalendar getHtmlCalendarRequestEndDateTo() {
		return htmlCalendarRequestEndDateTo;
	}

	public void setHtmlCalendarRequestEndDateTo(
			HtmlCalendar htmlCalendarRequestEndDateTo) {
		this.htmlCalendarRequestEndDateTo = htmlCalendarRequestEndDateTo;
	}

	public void printAuctionReport()
	{
		String methodName="printAuctionReport";
		logger.logInfo(methodName+"|Started...");
		try {
				AuctionReportCriteria reportCriteria ;
					reportCriteria=new AuctionReportCriteria(ReportConstant.Report.AUCTION_BIDDER_REPORT, ReportConstant.Processor.AUCTION_BIDDER_REPORT, getLoggedInUserObj().getFullName());
					
				reportCriteria.setAuctionId(((AuctionView)dataTable.getRowData()).getAuctionId().toString());
		    	HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				openPopup("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			   	logger.logInfo(methodName+"|Finish...");    
		} 
		catch(Exception ex)
		{
			errorMessages =  new ArrayList<String>(0);
			logger.LogException(methodName+"|Error Occured...",ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
}

