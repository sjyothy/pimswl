package com.avanza.pims.web.backingbeans.Investment.Asset;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.AssetServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.SectorView;
import com.avanza.pims.ws.vo.SubSectorView;

public class SubSectorSearchBacking extends AbstractController 
{
	private static final long serialVersionUID = -4998960581764962724L;
	
	private Logger logger;
	private Map<String,Object> viewMap;
	private Map<String,Object> sessionMap;
	private List<String> errorMessages; 
	private List<String> successMessages;
	
	private String pageMode;
	private SubSectorView subSectorView;
	private List<SubSectorView> subSectorViewList;
	
	private AssetServiceAgent assetServiceAgent;
	
	private HtmlDataTable dataTable;
	
	@SuppressWarnings("unchecked")
	public SubSectorSearchBacking() 
	{
		logger = Logger.getLogger( SubSectorSearchBacking.class );
		viewMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getExternalContext().getSessionMap();
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		
		pageMode = WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_SEARCH;
		subSectorView = new SubSectorView();
		subSectorViewList = new ArrayList<SubSectorView>(0);
		
		assetServiceAgent = new AssetServiceAgent();
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{		
		final String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() 
	{
		// PAGE MODE
		if ( sessionMap.get( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = ( String ) sessionMap.get( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY );
			sessionMap.remove( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY );			
		}
		else if ( viewMap.get( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY ) != null )
		{
			pageMode = ( String ) viewMap.get( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY );
		}
		
		
		// SUB SECTOR VIEW
		if ( viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW ) != null )
		{
			subSectorView = ( SubSectorView ) viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW );
		}
		
		
		// SUB SECTOR VIEW LIST
		if ( viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW_LIST ) != null )
		{
			subSectorViewList = ( List<SubSectorView> ) viewMap.get( WebConstants.SubSector.SUB_SECTOR_VIEW_LIST );
		}
		
		
		updateValuesToMaps();
	}

	private void updateValuesToMaps() 
	{	
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_KEY , pageMode );
		}
		
		
		// SUB SECTOR VIEW
		if ( subSectorView != null )
		{
			viewMap.put( WebConstants.SubSector.SUB_SECTOR_VIEW , subSectorView );
		}
		
		
		// SUB SECTOR VIEW LIST
		if ( subSectorViewList != null )
		{
			viewMap.put( WebConstants.SubSector.SUB_SECTOR_VIEW_LIST , subSectorViewList );
		}
	}
	
	public String onSearch()
	{
		String METHOD_NAME = "onSearch()";		
		String path = null;
		
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( subSectorView.getParentSectorId() != null && ! subSectorView.getParentSectorId().equalsIgnoreCase("") )
			{
				SectorView sectorView = new SectorView();
				sectorView.setSectorId( Long.parseLong( subSectorView.getParentSectorId() ) );
				sectorView = assetServiceAgent.getSectorList( sectorView ).get( 0 );
				
				subSectorView.setSectorView( sectorView );
			}
			else
			{
				subSectorView.setSectorView( null );
			}
			
			subSectorView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			subSectorView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			
			subSectorViewList = assetServiceAgent.getSubSectorList( subSectorView );
			
			updateValuesToMaps();
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;		
	}
	
	public String onView()
	{
		SubSectorView selectedSubSectorView = (SubSectorView) dataTable.getRowData();
		sessionMap.put( WebConstants.SubSector.SUB_SECTOR_VIEW , selectedSubSectorView );
		sessionMap.put( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY , WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_VIEW );
		return "subSectorManage";		
	}
	
	public String onAdd()
	{
		sessionMap.put( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY , WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_ADD );
		return "subSectorManage";		
	}
	
	public String onEdit()
	{
		SubSectorView selectedSubSectorView = (SubSectorView) dataTable.getRowData();
		sessionMap.put( WebConstants.SubSector.SUB_SECTOR_VIEW , selectedSubSectorView );
		sessionMap.put( WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_KEY , WebConstants.SubSector.SUB_SECTOR_MANAGE_PAGE_MODE_EDIT );
		return "subSectorManage";		
	}
	
	public String onDelete()
	{
		String METHOD_NAME = "onDelete()";
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		Boolean isDeleted = false;
		
		SubSectorView selectedSubSectorView = (SubSectorView) dataTable.getRowData();
		selectedSubSectorView.setUpdatedBy( CommonUtil.getLoggedInUser() );
		selectedSubSectorView.setIsDeleted( 1L );
		
		try
		{
			
			isDeleted = assetServiceAgent.updateSubSector( selectedSubSectorView );
			
			if ( isDeleted )
			{
				subSectorViewList.remove( selectedSubSectorView );
				updateValuesToMaps();
				successMessages.add( CommonUtil.getBundleMessage( "subSector.delete.success" ) );
				logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
			}
		}
		catch (Exception exception) 
		{	
			errorMessages.add( CommonUtil.getBundleMessage( "subSector.delete.error" ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;		
	}
	
	public Integer getRecordSize() 
	{
		return subSectorViewList.size();
	}
	
	public Boolean getIsSearchMode() 
	{
		Boolean isSearchMode = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.SubSector.SUB_SECTOR_SEARCH_PAGE_MODE_SEARCH ) )
			isSearchMode = true;
		
		return isSearchMode;
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	
	public Boolean getIsEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
	
	public String getLocale() 
	{
		return CommonUtil.getLocale();
	}
	
	public String getDateFormat() 
	{
		return CommonUtil.getDateFormat();
	}
	
	public TimeZone getTimeZone() 
	{
		return TimeZone.getDefault();		
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Map<String, Object> getViewMap() {
		return viewMap;
	}

	public void setViewMap(Map<String, Object> viewMap) {
		this.viewMap = viewMap;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages( errorMessages );
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getSuccessMessages() {
		return CommonUtil.getErrorMessages( successMessages );
	}

	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public SubSectorView getSubSectorView() {
		return subSectorView;
	}

	public void setSubSectorView(SubSectorView subSectorView) {
		this.subSectorView = subSectorView;
	}

	public List<SubSectorView> getSubSectorViewList() {
		return subSectorViewList;
	}

	public void setSubSectorViewList(List<SubSectorView> subSectorViewList) {
		this.subSectorViewList = subSectorViewList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public AssetServiceAgent getAssetServiceAgent() {
		return assetServiceAgent;
	}

	public void setAssetServiceAgent(AssetServiceAgent assetServiceAgent) {
		this.assetServiceAgent = assetServiceAgent;
	}
}
