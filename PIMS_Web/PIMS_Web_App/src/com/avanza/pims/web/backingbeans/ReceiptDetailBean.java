package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.pims.web.util.CommonUtil;

public class ReceiptDetailBean extends AbstractController{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6940777137903185412L;

	private static Logger logger = Logger.getLogger(ReceiptDetailBean.class);
	
	private HtmlDataTable dataTable;
	private HtmlDataTable receiptDetaildataTable;
	private BounceChequeView chequeView = new BounceChequeView();
	private Integer paginatorRows = 0;
	private Double totalReceiptAmount= 0.0;
	private String receiptDate = "";
	private List<PaymentScheduleView> psList = new ArrayList<PaymentScheduleView>();
	private List <PaymentReceiptDetailView> receiptDetailList = new ArrayList<PaymentReceiptDetailView>();
	private List<String> errorMessages = new ArrayList<String>();
	@SuppressWarnings("unchecked")
	private Map<String,Object> viewRootMap = getFacesContext().getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		if(request.getAttribute(WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL)!=null) 
		{
			chequeView = (BounceChequeView) request.getAttribute( WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL );
			this.setChequeView( chequeView );
			loadLists();
			loadAttachmentsAndComments( chequeView.getPaymentReceiptId().toString() );
		}
		
	}	
	
	public boolean getIsEnglishLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode().equalsIgnoreCase("en");
	}
	
	private void loadLists()
	{
		String methodName = "load Payment Schedules";
		try
		{
			if( this.getChequeView() !=null )
			{
				chequeView = this.getChequeView();
				PropertyServiceAgent psa = new PropertyServiceAgent();
				psList = psa.getPaymentScheduleListFromReceiptId(chequeView.getPaymentReceiptId());
				receiptDetailList = psa.getPaymentReceiptDetailsListFromReceiptId(chequeView.getPaymentReceiptId());
				
				PaymentReceiptView receiptView = psa.getPaymentReceiptById(chequeView.getPaymentReceiptId());
				this.setReceiptDate(getStringFromDate(receiptView.getCreatedOn()));
				for(PaymentReceiptDetailView detail : receiptDetailList)
				{
					totalReceiptAmount += detail.getAmount();
				}
				viewRootMap.put("Total_Amount", totalReceiptAmount);
				viewRootMap.put("Payment_Schedules", psList);
				viewRootMap.put("Receipt_Detail", receiptDetailList);
				
			}
		}
		catch (Exception e) 
		{		
			logger.LogException(methodName + "|" + "Exception Occured...",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(String entityId)
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewRootMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_TYPE_RECEIVE_PAYMENTS);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
  	    String externalId = WebConstants.Attachment.EXTERNAL_ID_PAYMENT_RECEIPTS;
  	    viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		if(entityId!= null && entityId.trim().length()>0)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void tabRequestHistory_Click() 
	{
		String methodName = "tabRequestHistory_Click";
		logger.logInfo(methodName + "|" + "Start..");
		try
		{
			RequestHistoryController rhc = new RequestHistoryController();
			rhc.getAllRequestTasksForRequest(WebConstants.NOTES_OWNER_PAYMENT_RECEIPT,this.getChequeView().getPaymentReceiptId().toString() );
		}
		catch (Exception ex) 
		{
			logger.LogException(methodName + "|" + "Error Occured..", ex);
		}
		logger.logInfo(methodName + "|" + "Finish..");
	}


	public String btnBack_Click()
	{
		return "chequeListPage";
	}

	
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	
	
	
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public BounceChequeView getChequeView() 
	{
		if(viewRootMap.containsKey(WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL) && 
				viewRootMap.get( WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL ) !=null)
			chequeView  =  (BounceChequeView) viewRootMap.get( WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL );
		return chequeView;
	}

	public void setChequeView(BounceChequeView chequeView) 
	{
		
		this.chequeView = chequeView;
		if(this.chequeView != null)
			viewRootMap.put( WebConstants.CONTRACT_PAYMENT_RECEIPT_DETAIL ,this.chequeView );
	}
    @SuppressWarnings("unchecked")
	public List<PaymentScheduleView> getPsList() {
		if(viewRootMap.containsKey("Payment_Schedules"))
			psList = (ArrayList<PaymentScheduleView>)viewRootMap.get("Payment_Schedules");
		return psList;
	}

	public void setPsList(List<PaymentScheduleView> psList) {
		this.psList = psList;
	}

	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(this.errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}
	
	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	
	public Integer getRecordSize() {
		
		return this.getPsList().size();
	}

	public HtmlDataTable getReceiptDetaildataTable() {
		return receiptDetaildataTable;
	}

	public void setReceiptDetaildataTable(HtmlDataTable receiptDetaildataTable) {
		this.receiptDetaildataTable = receiptDetaildataTable;
	}
    @SuppressWarnings("unchecked")
	public List<PaymentReceiptDetailView> getReceiptDetailList() {
		if(viewRootMap.containsKey("Receipt_Detail"))
			receiptDetailList = (ArrayList<PaymentReceiptDetailView>)viewRootMap.get("Receipt_Detail");
		
		return receiptDetailList;
	}

	public void setReceiptDetailList(
			List<PaymentReceiptDetailView> receiptDetailList) {
		this.receiptDetailList = receiptDetailList;
	}
	
	public Integer getReceiptRecordSize() {
		
		return this.getReceiptDetailList().size();
	}

	public Double getTotalReceiptAmount() {
		
		if(viewRootMap.containsKey("Total_Amount"))
			totalReceiptAmount = (Double)viewRootMap.get("Total_Amount");
		return totalReceiptAmount;
	}

	public void setTotalReceiptAmount(Double totalReceiptAmount) {
		this.totalReceiptAmount = totalReceiptAmount;
	}

	public String getReceiptDate() 
	{
		if(viewRootMap.get("RECEIPT_DATE") != null)
			receiptDate = viewRootMap.get("RECEIPT_DATE").toString();
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) 
	{
		if(receiptDate != null & receiptDate.length() > 0)
			viewRootMap.put("RECEIPT_DATE",receiptDate);
		this.receiptDate = receiptDate;
	}

}
