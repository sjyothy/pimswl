package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderView;

public class BidderLookup extends AbstractController{

	//Fields
	
	 private transient Logger logger = Logger.getLogger(BidderLookup.class);
	
	 private HtmlInputText inputTextSocialSecNo;
	 
	 private HtmlInputText inputTextFirstName;
	 
	 private HtmlInputText inputTextPassport;
	 
	 private HtmlInputText inputTextLastName;
	 
	 private HtmlCommandButton commandButtonSearch;
	 
	 private HtmlDataTable dataTable;

	 private BidderView dataItem = new BidderView();
	 
	 private List<BidderView> dataList = new ArrayList();
	
	 private boolean isEnglishLocale = false;
	 private boolean isArabicLocale = false;
	
	 private List<String> messages;
	
	 private ResourceBundle bundle;
	 
	 private static final String BIDDER_LOOKUP_LIST = "BIDDER_LOOKUP_LIST"; 
	 
	//Constructor
	/** default constructor */
	public BidderLookup(){				
	}
	
	//Methods
	public List<BidderView> getBidderDataList() {
		if (dataList == null || dataList.size() == 0) {
			//get dataList from viewRoot
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object list = viewRootMap.get(BIDDER_LOOKUP_LIST);
			if(list != null){
				dataList = (List<BidderView>)list;
			}
//			loadDataList();
		}
		return dataList;
	}
	
	private void loadDataList() {	
		String  methodName = "loadDataList";
        logger.logInfo(methodName+" started...");
		
		try {
			BidderView bidderView = new BidderView();
			if(inputTextFirstName != null && inputTextFirstName.getValue() != null && !inputTextFirstName.getValue().toString().equals(""))
				bidderView.setFirstName(inputTextFirstName.getValue().toString());
			if(inputTextLastName != null && inputTextLastName.getValue() != null && !inputTextLastName.getValue().toString().equals(""))
				bidderView.setLastName(inputTextLastName.getValue().toString());
			if(inputTextSocialSecNo != null && inputTextSocialSecNo.getValue() != null && !inputTextSocialSecNo.getValue().toString().equals(""))
				bidderView.setSocialSecNumber(inputTextSocialSecNo.getValue().toString());
			if( inputTextPassport != null && inputTextPassport.getValue() != null && !inputTextPassport.getValue().toString().equals(""))
				bidderView.setPassportNumber(inputTextPassport.getValue().toString());
			
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();			
			List<BidderView> bidders = null;// pSAgent.getBidderLike(bidderView);
			dataList = bidders;
			logger.logInfo(methodName+" completed successfully...");
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			logger.LogException(methodName+" crashed ...", exception);
			exception.printStackTrace();
		}		
	}
	
	public String search(){
		String  methodName = "search";
        logger.logInfo(methodName+" started...");
//		if (dataList != null) {
//			loadDataList();
//		}
        Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
        loadDataList();
		viewRootMap.put(BIDDER_LOOKUP_LIST, dataList);
		logger.logInfo(methodName+" ended...");
		return "";
	}

	
	public String theCall(){
		System.out.println("the Call is called.......");
		return "";
	}
	
	public Integer getRecordSize(){
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object list = viewRootMap.get(BIDDER_LOOKUP_LIST);
		if(list != null){
			return ((List<BidderView>)list).size(); 
		}
		else{
			return 0;
		}
	}
	
	
	//Getters and Setters
	public HtmlInputText getInputTextSocialSecNo() {
		return inputTextSocialSecNo;
	}

	public void setInputTextSocialSecNo(HtmlInputText inputSocialSecNo) {
		this.inputTextSocialSecNo = inputSocialSecNo;
	}

	public HtmlInputText getInputTextFirstName() {
		return inputTextFirstName;
	}

	public void setInputTextFirstName(HtmlInputText inputTextFirstName) {
		this.inputTextFirstName = inputTextFirstName;
	}

	public HtmlInputText getInputTextPassport() {
		return inputTextPassport;
	}

	public void setInputTextPassport(HtmlInputText inputTextPassport) {
		this.inputTextPassport = inputTextPassport;
	}

	public HtmlInputText getInputTextLastName() {
		return inputTextLastName;
	}

	public void setInputTextLastName(HtmlInputText inputTextLastName) {
		this.inputTextLastName = inputTextLastName;
	}

	public HtmlCommandButton getCommandButtonSearch() {
		return commandButtonSearch;
	}

	public void setCommandButtonSearch(HtmlCommandButton commandButtonSearch) {
		this.commandButtonSearch = commandButtonSearch;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public BidderView getDataItem() {
		return dataItem;
	}

	public void setDataItem(BidderView dataItem) {
		this.dataItem = dataItem;
	}

	public List<BidderView> getDataList() {
		return dataList;
	}

	public void setDataList(List<BidderView> dataList) {
		this.dataList = dataList;
	}
	
	public void setEnglishLocale(boolean isEnglishLocale) {
		this.isEnglishLocale = isEnglishLocale;
	}

	public void setArabicLocale(boolean isArabicLocale) {
		this.isArabicLocale = isArabicLocale;
	}

	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{
		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		return isEnglishLocale;
	}
	
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public String getMessages() {
		String messageList;
		if ((messages == null) ||
			(messages.size() == 0)) {
			messageList = "";
		} 
		else{
			messageList = "<B><UL>\n";
			for(String message: messages) {
				messageList = messageList + message + "\n";
		}
		messageList = messageList + "</UL></B></FONT>\n";
		}
		return(messageList);
	}

	public ResourceBundle getBundle() {
		if(getIsArabicLocale())
			bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");
		else
			bundle = ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");
		return bundle;
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}
	
	/**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Override this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void init() {
		// TODO Auto-generated method stub		
		super.init();		
	}

	 /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a "post back" request that
     * is processing a form submit.  Override this method to allocate
     * resources that will be required in your event handlers.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
	}

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a post back and then navigated to a different page).  Override
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
	public void prerender() {
		super.prerender();
	}
	
    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called, regardless of whether
     * or not this was the page that was actually rendered.  Override this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     *
     * <p>The default implementation does nothing.</p>
     */
	@Override
    public void destroy() {
        super.destroy();
    }


	 
}
