package com.avanza.pims.web.backingbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.component.html.ext.HtmlDataTable;






import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.InquiryManager;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Inquiry;
import com.avanza.pims.entity.Unit;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.entity.Property;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;


public class InquiryPerson extends AbstractController {
	
	private static Logger logger = Logger.getLogger( InquiryPerson.class );
	
	private HtmlDataTable dataTable;
	private String personId = "";
	private String firstName = "";
	private String middleName = "";
	private String lastName = "";
	private String gender1 = "";
	private Long personType;
	private String designation;
	private String nationality;
    private String passportNumber;
    private Date passportIssueDate;
  //  private Long passportIssuePlaceId;
    private Date passportExpiryDate;
    private String residenseVisaNumber;
    private Date residenseVidaExpDate;
  //  private Long residenseVisaIssuePlaceId;
    private String personalSecCardNo;
    private String drivingLicenseNumber;
    private String socialSecNumber;
	String personTypeToAdd="";
	//private String gender = "";
	private String cellNo = "";
	private Date dateOfBirth;
	private String address1 = ""; 
	private String address2 = "";
	private String street;
	private String postCode;
	
	
	/*private String city;
	private String state;
	private String country;*/
	private String homePhone;
	private String officePhone;
	private String fax; 
	private String email;

	private HtmlSelectOneMenu citySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countrySelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu genderSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu titleSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu personTypeSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu residenseVisaIssuePlaceSelectMenu= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu passportIssuePlaceSelectMenu = new HtmlSelectOneMenu();
	private HtmlInputHidden hiddenStateValue = new HtmlInputHidden();
	private HtmlInputHidden hiddenCityValue = new HtmlInputHidden();
	CommonUtil commonUtil=new CommonUtil();
	String selectOneTitle;

	String selectOneCountry;
	String selectOneState ;
	String selectOneCity ;
	//private SecUserGroupData dataItem = new SecUserGroupData();

	//private SecUserGroupData dataItem = new SecUserGroupData();

	
	private PersonView personView  = new PersonView();
	private ContactInfoView contactInfoView = new ContactInfoView();
	
	
	List<SelectItem> title= new ArrayList();
	List<SelectItem> residenseVisaIssuePlace= new ArrayList();
	List<SelectItem> passportIssuePlace= new ArrayList();
	
	
/*	List<SelectItem> personType= new ArrayList();*/
	List<SelectItem> city= new ArrayList();
	List<SelectItem> state= new ArrayList();
	List<SelectItem> country= new ArrayList();
	List<SelectItem> gender= new ArrayList();
	
	private List<String> errorMessages = new ArrayList<String>();
	private List<String> successMessages=new ArrayList<String>();
	private PropertyInquiryData dataItem = new PropertyInquiryData();
	private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
//	public Unit dataItem = new Unit();
	
	private List<SelectItem> countries = new ArrayList<SelectItem>();
	private List<SelectItem> states = new ArrayList<SelectItem>();
	private List<SelectItem> cities = new ArrayList<SelectItem>();
	
	private Map<Long,RegionView> countryMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> stateMap = new HashMap<Long,RegionView>();
	private Map<Long,RegionView> cityMap = new HashMap<Long,RegionView>();
	
	
	
	FacesContext context=getFacesContext();
    Map sessionMap;
    
    Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    
    private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	
	
	public InquiryPerson (){
		//loadCityList();
		//loadStateList();
		//loadCountryList();
		
		
	}
	
	
	
	
	public void init() 
    {
   	
   	 super.init();
   	 try
   	 {

   		 sessionMap=context.getExternalContext().getSessionMap();
   		//errorMessages = new ArrayList<String>();
   		//successMessages=new ArrayList<String>();
   		loadCountries();
   	    loadStateMap();    
   	    
   		 if(!isPostBack())
   		 {
	   		 if(getRequestParam("PERSON_TYPE")!=null && getRequestParam("PERSON_TYPE").toString().length()>0)
	   			 personTypeToAdd=getRequestParam("PERSON_TYPE").toString();
	   		 
	   		    context.getViewRoot().getAttributes().put("PERSON_TYPE", personTypeToAdd);
	   		    
	   		 
   		 }
   
    if(!isPostBack()){
    	
   	 	loadCombos();
   	
     }
   		 
   	 }
   	 catch(Exception es)
   	 {
   		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
   		 System.out.println(es);
   		 
   	 }
	        
	 }
	
	 private void loadCombos() throws PimsBusinessException
     {
    	 
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	   loadTitleList();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
  loadCountry();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
    	   loadState();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    	
    	   loadCity();
    	 if(!sessionMap.containsKey(WebConstants.TITLES))
    	    
    		  
    	   loadGenderList();
    	 loadRegion();
     }
	 
	 
	 private void  loadState(){
		 
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_STATE))
				{
			          Set<RegionView> regionViewList =  psa.getState(new Long(selectOneCountry));
			          sessionMap.put(WebConstants.SESSION_STATE,regionViewList);
		  	    }
			}
			catch(Exception ex)
			
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		   
	   }
	 private void  loadCity(){
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_CITY))
				{
			          Set<RegionView> regionViewList =  psa.getCity(new Long(selectOneState));
			          sessionMap.put(WebConstants.SESSION_CITY,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		   
	   }
	 
	 private void  loadRegion(){
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_REGION))
				{
			          List<RegionView> regionViewList =  psa.getRegion();
			          sessionMap.put(WebConstants.SESSION_REGION,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				System.out.println("Print"+ex);
			}
		   
	   }
	 
	 private void loadCountry()
	 {
		 PropertyServiceAgent psa = new PropertyServiceAgent();
			
			try 
			{
				if(!sessionMap.containsKey(WebConstants.SESSION_COUNTRY))
				{
			         List<RegionView> regionViewList =  psa.getCountry();
			          sessionMap.put(WebConstants.SESSION_COUNTRY,regionViewList);
		  	    }
			}
			catch(Exception ex)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			
		 
	 }
	 private void loadTitleList() throws PimsBusinessException
     {
    	 
    	 PropertyServiceAgent psa=new PropertyServiceAgent();
    	 List<DomainDataView> ddl= psa.getDomainDataByDomainTypeName(WebConstants.TITLES);
    	 sessionMap.put(WebConstants.TITLES, ddl);
    
     }
	 
	
	
	public List<PropertyInquiryData> getPropertyInquiryDataList() {
		if (dataList == null) {
		//loadDataList(); // Preload by lazy loading.
		}
		return dataList;
	}
	

	public String getFirstName (){
		return this.firstName;
	}
	public void setFirstName (String firstName){
		this.firstName = firstName;
	}

	public String getMiddleName (){
		return this.middleName;
	}
	public void setMiddleName (String middleName){
		this.middleName = middleName;
	}
	
	public String getLastName (){
		return this.lastName;
	}
	public void setLastName (String lastName){
		this.lastName = lastName;
	}
	
/*	public String getGender (){
		return this.gender;
	}
	public void setGender(String gender){
		this.gender= gender;
	}*/
	
	public String getCellNo (){
		return this.cellNo;
	}
	public void setCellNo (String cellNo){
		this.cellNo = cellNo;
	}
	
	public String getAddress1 (){
		return this.address1;
	}
	public void setAddress1 (String address1){
		this.address1 = address1;
	}
	
	public String getAddress2 (){
		return this.address2;
	}
	public void setAddress2 (String address2){
		this.address2 = address2;
	}
	
	public String getStreet (){
		return this.street;
	}
	public void setStreet (String street){
		this.street = street;
	}
	
	public String getPostCode (){
		return this.postCode;
	}
	public void setPostCode (String postCode){
		this.postCode = postCode;
	}
	
/*	public String getCity (){
		return this.city;
	}
	public void setCity (String city){
		this.city = city;
	}
	
	public String getState (){
		return this.state;
	}
	public void setState (String state){
		this.state = state;
	}
	
	public String getCountry (){
		return this.country;
	}
	public void setCountry (String country){
		this.country = country;
	}*/
	
	public String getHomePhone (){
		return this.homePhone;
	}
	public void setHomePhone (String homePhone){
		this.homePhone = homePhone;
	}
	
	public String getOfficePhone (){
		return this.officePhone;
	}
	public void setOfficePhone (String officePhone){
		this.officePhone = officePhone;
	}
	
	public String getFax (){
		return this.fax;
	}
	public void setFax (String fax){
		this.fax = fax;
	}
	
	public String getEmail (){
		return this.email;
	}
	public void setEmail (String email){
		this.email = email;
	}
	

	
	
	
	
	public String doBid() {
		errorMessages = new ArrayList<String>();
		if (getPersonId().equals("")) {
		 errorMessages.add("local message");
		}
		if (errorMessages.size() > 0) {
			return(null);
			} else {
			return("success");
			}
	}
	Inquiry inquiryPOJO  = new Inquiry();
	InquiryManager inquiryManager = new InquiryManager(); 
	List<Unit> inquiryUnit = new ArrayList();
	
	
	public String btnBack_Click()
	{
		String backScreenParameter="BackScreen";
		if(context.getViewRoot().getAttributes().containsKey("PERSON_TYPE") && context.getViewRoot().getAttributes().get("PERSON_TYPE")!=null
				&& context.getViewRoot().getAttributes().get("PERSON_TYPE").toString().trim().length()>0
		)
		{
			String personType=context.getViewRoot().getAttributes().get("PERSON_TYPE").toString();
		  context.getExternalContext().getRequestMap().put("persontype", personType);
	     if(personType.equals(WebConstants.OCCUPIER) || personType.equals(WebConstants.MANAGER) || personType.equals(WebConstants.SPONSOR) || personType.equals(WebConstants.PARTNER))
		    backScreenParameter="BackScreenContractPerson";
		}
	
		return backScreenParameter;
		
	}
		
	
	

	
	public String getErrorMessages() {
		return commonUtil.getErrorMessages(this.errorMessages);
	}

	public String getSuccessMessages() {
		
		return commonUtil.getErrorMessages(this.successMessages);
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	/*public PropertyInquiryData getDataItem() {
		return dataItem;
	}

	public void setDataItem(PropertyInquiryData dataItem) {
		this.dataItem = dataItem;
	}*/

	/*private void loadCityList() {

    city = new ArrayList ();
    
    city.add(new SelectItem("0", "Karachi"));
    city.add(new SelectItem("1", "Lahore"));
    city.add(new SelectItem("2", "Islamabad"));

	for (int i = 0; i < city.size(); i++) {

		getCitySelectMenu().setValue(new SelectItem(city.get(i)));
	}

  }
*/	
/*	private void loadStateList() {

	    state = new ArrayList ();
	    
	    state.add(new SelectItem("0", "Sindh"));
	    state.add(new SelectItem("1", "Panjab"));
	    state.add(new SelectItem("2", "NWFP"));

		for (int i = 0; i < state.size(); i++) {

			getCitySelectMenu().setValue(new SelectItem(state.get(i)));
		}

	  }
*/	
/*	private void loadCountryList() {

	    country = new ArrayList ();
	    
	    country.add(new SelectItem("0", "Dubai"));
	    country.add(new SelectItem("1", "Abu Dhabi"));
	    country.add(new SelectItem("2", "Qatar"));

		for (int i = 0; i < country.size(); i++) {

			getCitySelectMenu().setValue(new SelectItem(country.get(i)));
		}

	  }
*/	
	private void loadGenderList() {

	    gender = new ArrayList ();
	    
	    gender.add(new SelectItem("0", "Male"));
	    gender.add(new SelectItem("1", "Female"));
	    

		for (int i = 0; i < gender.size(); i++) {

			getGenderSelectMenu().setValue(new SelectItem(gender.get(i)));
		}

	  }
	
	public HtmlSelectOneMenu getCitySelectMenu() {
		return citySelectMenu;
	}

	public void setCitySelectMenu(HtmlSelectOneMenu citySelectMenu) {
		this.citySelectMenu = citySelectMenu;
	}

	public HtmlSelectOneMenu getCountrySelectMenu() {
		return countrySelectMenu;
	}

	public void setCountrySelectMenu(HtmlSelectOneMenu countrySelectMenu) {
		this.countrySelectMenu = countrySelectMenu;
	}

	public HtmlSelectOneMenu getStateSelectMenu() {
		return stateSelectMenu;
	}

	public void setStateSelectMenu(HtmlSelectOneMenu stateSelectMenu) {
		this.stateSelectMenu = stateSelectMenu;
	}

	public List<SelectItem> getCity() {
		city.clear();
		
		try {
			selectOneState = (String)hiddenCityValue.getValue();
			
			if(selectOneState != null && !selectOneState.equals("-1"))
			{
				
			/*	
				loadCity();
				Set<RegionView> regionViewList=(HashSet) sessionMap.get(WebConstants.SESSION_CITY); 
				Iterator<RegionView> iter=regionViewList.iterator();
				while(iter.hasNext())
				  {
					  RegionView rV=(RegionView)iter.next();
					  
				      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				      city.add(item);
				  }*/
				
				Set<RegionView> regionViewList=getRegionChildren(new Long(selectOneState)); 
				Iterator<RegionView> iter=regionViewList.iterator();
				while(iter.hasNext())
				  {
					  RegionView rV=(RegionView)iter.next();
					  
				      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				      city.add(item);
				  }
			
			}
		}catch (Exception e){
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return city;
	}

	public void setCity(List<SelectItem> city) {
		this.city = city;
	}

	public List<SelectItem> getState() {
		state.clear();
		
		selectOneCountry= (String)hiddenStateValue.getValue();
		
		try {
			if(selectOneCountry!=null &&  !selectOneCountry.equals("-1"))
			{
				
				
				 
				
				//loadState();
				Set<RegionView> regionViewList=getRegionChildren(new Long(selectOneCountry)); 
				Iterator<RegionView> iter=regionViewList.iterator();
				while(iter.hasNext())
				  {
					  RegionView rV=(RegionView)iter.next();
					  
				      SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());
				      state.add(item);
				  }
			}
		}catch (Exception e)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Error.."+e);
		}
		
		return state;
	}

	public Set<RegionView> getRegionChildren(long parentRegionId)
	{
	    loadRegion();
	    List<RegionView> list = (ArrayList)sessionMap.get(WebConstants.SESSION_REGION);
		Set<RegionView> region=new HashSet<RegionView>();
	    
		for(int i=0;i<list.size();i++)
	    {
	     RegionView regionView =(RegionView)list.get(i);
	      if(regionView.getRegionId().equals(parentRegionId)){
	    	  region=regionView.getRegions();
	      break;
	      }
	    	
	    	
	    }
	return region;
	}
	public List<RegionView> getRegionCountry()
	{
	    loadRegion();
	    List<RegionView> list = (ArrayList)sessionMap.get(WebConstants.SESSION_REGION);
		List<RegionView> region=new ArrayList<RegionView>();
		String regionCountry ;
		DomainData domaindata;
		UtilityManager utilitymanager = new UtilityManager(); 
		regionCountry= "REGION TYPE COUNTRY";
		   
		   try {
			domaindata= utilitymanager.getDomainDataByValue(regionCountry);
		
	    
		for(int i=0;i<list.size();i++)
	    {
	     RegionView regionView =(RegionView)list.get(i);
	      if(regionView.getRegionTypeId().equals(domaindata.getDomainDataId()))
	    	  region.add(regionView);
	    	
	    	
	    }
		
		   }catch (Exception e) {
			   errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	return region;
	}
	public void setState(List<SelectItem> state) {
		this.state = state;
	}

	
	public void methodValueChange(ValueChangeEvent vce){
	 //selectOneCountry = (String)vce.getNewValue();
	 selectOneCountry= (String)hiddenStateValue.getValue();
	    getState();
	   //iP.getState();
	 /*if (selectOneState.equals("-1")){
	    getState();
	 }else {
		getCity();
	}*/
	 
	 
	}
	
	
	public void methodValueChangeState(ValueChangeEvent vce){
		 selectOneState = (String)	vce.getNewValue();
		 selectOneState = (String)hiddenCityValue.getValue();
		   //iP.getState();
		 getCity();
		 
		 }
	
	public List<SelectItem> getCountry() {
		country.clear();
		try 
		{
					//loadCountry();
					System.out.println("hiddenStateValue"+hiddenStateValue.getValue());
		            List<RegionView> regionViewList=getRegionCountry(); 
		            for(int i=0;i<regionViewList.size();i++)
		            {
			          RegionView rV=(RegionView)regionViewList.get(i);
		              SelectItem item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn().toString());
		              country.add(item);
		            }
		}catch (Exception e){
			
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			System.out.println("Error::"+e);
			
		}
		
		
		
		

		return country;
	}

	public void setCountry(List<SelectItem> country) {
		this.country = country;
	}


    public HtmlSelectOneMenu getGenderSelectMenu() {
		return genderSelectMenu;
	}
    public void setGenderSelectMenu(HtmlSelectOneMenu genderSelectMenu) {
		this.genderSelectMenu = genderSelectMenu;
	}

public List<SelectItem> getGender() {
		return gender;
	}
public void setGender(List<SelectItem> gender) {
		this.gender = gender;
	}

public String getPersonId() {
	return personId;
}
public void setPersonId(String personId) {
	this.personId = personId;
}

public void doSearch (){ 
	try{
	
	/*PIMSPropertyWSSoapHttpPortClient pims = new PIMSPropertyWSSoapHttpPortClient();
	List<PersonView> personList = pims.getPersonInformation(new PersonView());
	*/
	
	}
	catch (Exception e){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		e.printStackTrace();	
	}
}

public String getGender1() {
	return gender1;
}

public void setGender1(String gender1) {
	this.gender1 = gender1;
}

public Long getPersonType() {
	return personType;
}

public void setPersonType(Long personType) {
	this.personType = personType;
}

public String btnAdd_Click() throws Exception {
	
	
	String methodName = "btnAdd_Click";
	
	logger.logInfo(methodName+"Starts::::::::::::::::::::");
	
	String eventOutCome = "failure";
	String s1;
	String message;
	errorMessages = new ArrayList<String>();
	successMessages = new ArrayList<String>();
	try {
 
		if(putControlValuesInView())
		{
		PropertyServiceAgent psa  =new PropertyServiceAgent();	
			
		logger.logInfo(methodName+" addPerson Starts::::::::::::::::::::");
			Long personId = 0L; //psa.addPerson(personView);
		logger.logInfo(methodName+" addPerson Ends::::::::::::::::::::");	
			
			message = ResourceUtil.getInstance().getProperty("common.messages.Save");
	    	successMessages.add(message);
	    	
			this.getFacesContext().getExternalContext().getRequestMap().put("personName", personView.getFirstName());
			this.getFacesContext().getExternalContext().getRequestMap().put("person", personId);
			this.getFacesContext().getExternalContext().getRequestMap().put("passportNumber", personView.getPassportNumber());
			this.getFacesContext().getExternalContext().getRequestMap().put("cellNumber", personView.getCellNumber());
			this.getFacesContext().getExternalContext().getRequestMap().put("designation", personView.getDesignation());
			
			eventOutCome = "request";
			logger.logInfo(methodName+"Ends::::::::::::::::::::");	
		}

	} catch (Exception ex) {
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.logInfo(methodName+"Crashed::::::::::::::::::::");
		ex.printStackTrace();
	}

	return eventOutCome;

}

private boolean putControlValuesInView() {
	
	String methodName = "putControlValuesInView";

       boolean isSucceed=true;
       errorMessages.clear();
       String message;
       String loginUser = getLoggedInUser();
       
       
/*       
	if(title !=null && !title.equals(""))
	{
	 personView.setTitleId(title);	
	}*/
       //firstName = "Shiraz";
      
       personView.setDateFormat("dd/MM/yyyy");
      
       if(selectOneTitle!=null && !selectOneTitle.equals("")){
       	
       	personView.setTitleId(new Long(selectOneTitle));
       }
       
       
       if (firstName != null && !firstName.equals(""))
      {
		personView.setFirstName(firstName);
	   }
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.firstName");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed; 
	}
       if (middleName != null && !middleName.equals(""))
       {
 		personView.setMiddleName(middleName);
 	   }
//	lastName= "Siddqui";
       if (lastName != null && !lastName.equals("")) {
		personView.setLastName(lastName);
        }
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.lastName");
        errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}
 
	if (personTypeToAdd != null && personTypeToAdd.trim().length()>0 ) 
	{
		DomainDataView ddv= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.PERSON_TYPE),
				                  personTypeToAdd
				                 );
		
		personView.setPersonTypeId(ddv.getDomainDataId());

	}
	else
	{
		errorMessages.add("Please Enter Person Type");
		isSucceed=false;
		return isSucceed;
	}

//gender1 = "M";
	if (gender1 != null && ! gender1.equals("")) {
		personView.setGender(gender1);

	}
	
	else
	{
		errorMessages.add("Please Enter Gender");
		isSucceed=false;
		return isSucceed;
	}	
	 if (dateOfBirth != null && !dateOfBirth.equals(""))
     {
		personView.setDateOfBirth(dateOfBirth);
	   }
	 if (cellNo != null && !cellNo.equals(""))
     {
		personView.setCellNumber(cellNo);
	   }
	 if (designation != null && !designation.equals(""))
     {
		personView.setDesignation(designation);
	   }
	 if (nationality != null && !nationality.equals(""))
     {
		personView.setNationalityEn(nationality);
	   }
	 if (passportNumber != null && !passportNumber.equals(""))
     {
		personView.setPassportNumber(passportNumber);
	   }
	 if (passportIssueDate != null && !passportIssueDate.equals(""))
     {
		personView.setPassportIssueDate(passportIssueDate);
	   }
	 if (passportExpiryDate != null && !passportExpiryDate.equals(""))
     {
		personView.setPassportExpiryDate(passportExpiryDate);
	   }
	 if (residenseVisaNumber != null && ! residenseVisaNumber.equals(""))
     {
		personView.setResidenseVisaNumber(residenseVisaNumber);
	   }
	 if (residenseVidaExpDate != null && !residenseVidaExpDate.equals(""))
     {
		personView.setResidenseVidaExpDate(residenseVidaExpDate);
	   }
	 if (personalSecCardNo != null && !personalSecCardNo.equals(""))
     {
		 personView.setPersonalSecCardNo(personalSecCardNo);
	   }
	 if (middleName != null && !middleName.equals(""))
     {
		personView.setMiddleName(middleName);
	   }
	 if (drivingLicenseNumber != null && ! drivingLicenseNumber.equals(""))
     {
		personView.setDrivingLicenseNumber(drivingLicenseNumber);
	   }
	 if (socialSecNumber != null && ! socialSecNumber.equals(""))
     {
		personView.setSocialSecNumber(socialSecNumber);
	   }
	 
	
	personView.setCreatedBy(loginUser);
	personView.setCreatedOn(new Date());
	personView.setUpdatedBy(loginUser);
	personView.setUpdatedOn(new Date());
	personView.setIsDeleted(new Long(0));
	personView.setRecordStatus(new Long(1));
	
//	address1="address1";
//	address2="address2";
	
	if (address1 != null && ! address1.equals("")) {
		contactInfoView.setAddress1(address1);

	}
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.address1");
		errorMessages.add(message);
		isSucceed=false;
	}	
	
	if (address2 != null && ! address2.equals("")) {
		contactInfoView.setAddress2(address2);

	}
/*	else
	{ 
		message = ResourceUtil.getInstance().getProperty("person.message.address2");
		errorMessages.add(message);
		isSucceed=false;
	}*/

	if (street != null && ! street.equals("")) {
		contactInfoView.setStreet(street);

	}
	if (postCode != null && ! postCode.equals("")) {
		contactInfoView.setPostCode(postCode);

	}
	if (homePhone != null && ! homePhone.equals("")) {
		contactInfoView.setHomePhone(homePhone);

	}
	if (officePhone != null && ! officePhone.equals("")) {
		contactInfoView.setOfficePhone(officePhone);

	}
	if (fax != null && ! fax.equals("")) {
		contactInfoView.setFax(fax);

	}
	if (email != null && ! email.equals("")) {
		contactInfoView.setEmail(email);

	}
		
	if (selectOneCountry !=null && !selectOneCountry.equals("")){
			
			contactInfoView.setCountryId(new Long(selectOneCountry));
		}
	if (selectOneState !=null && !selectOneState.equals("")){
			
			contactInfoView.setStateId(new Long(selectOneState));
		}
	if (selectOneCity !=null && !selectOneCity.equals("")){
		
		contactInfoView.setCityId(new Long(selectOneCity));
	}
	if (!((email!=null && ! email.equals("") )|| (cellNo != null && !cellNo.equals(""))))
	{
		message = ResourceUtil.getInstance().getProperty("person.message.emailOrCell");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
		
	}
	
	contactInfoView.setCreatedBy(loginUser);
	contactInfoView.setCreatedOn(new Date());
	contactInfoView.setUpdatedBy(loginUser);
	contactInfoView.setUpdatedOn(new Date());
	contactInfoView.setIsDeleted(new Long(0));
	contactInfoView.setRecordStatus(new Long(1));
	
	if (contactInfoView !=null){
		
		//personView.setContactInfoView(contactInfoView);
	}
	else
	{
		message = ResourceUtil.getInstance().getProperty("person.message.contactInfo");
		errorMessages.add(message);
		isSucceed=false;
		return isSucceed;
	}	

      return isSucceed;
}

public HtmlSelectOneMenu getPersonTypeSelectMenu() {
	return personTypeSelectMenu;
}

public void setPersonTypeSelectMenu(HtmlSelectOneMenu personTypeSelectMenu) {
	this.personTypeSelectMenu = personTypeSelectMenu;
}

public Date getDateOfBirth() {
	return dateOfBirth;
}

public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

public String getDesignation() {
	return designation;
}

public void setDesignation(String designation) {
	this.designation = designation;
}

public String getNationality() {
	return nationality;
}

public void setNationality(String nationality) {
	this.nationality = nationality;
}

public String getPassportNumber() {
	return passportNumber;
}

public void setPassportNumber(String passportNumber) {
	this.passportNumber = passportNumber;
}

public Date getPassportIssueDate() {
	return passportIssueDate;
}

public void setPassportIssueDate(Date passportIssueDate) {
	this.passportIssueDate = passportIssueDate;
}

public Date getPassportExpiryDate() {
	return passportExpiryDate;
}

public void setPassportExpiryDate(Date passportExpiryDate) {
	this.passportExpiryDate = passportExpiryDate;
}

public String getResidenseVisaNumber() {
	return residenseVisaNumber;
}

public void setResidenseVisaNumber(String residenseVisaNumber) {
	this.residenseVisaNumber = residenseVisaNumber;
}

public Date getResidenseVidaExpDate() {
	return residenseVidaExpDate;
}

public void setResidenseVidaExpDate(Date residenseVidaExpDate) {
	this.residenseVidaExpDate = residenseVidaExpDate;
}

public String getPersonalSecCardNo() {
	return personalSecCardNo;
}

public void setPersonalSecCardNo(String personalSecCardNo) {
	this.personalSecCardNo = personalSecCardNo;
}

public String getDrivingLicenseNumber() {
	return drivingLicenseNumber;
}


public void setDrivingLicenseNumber(String drivingLicenseNumber) {
	this.drivingLicenseNumber = drivingLicenseNumber;
}

public String getSocialSecNumber() {
	return socialSecNumber;
}

public void setSocialSecNumber(String socialSecNumber) {
	this.socialSecNumber = socialSecNumber;
}

public List<SelectItem> getResidenseVisaIssuePlace() {
	return residenseVisaIssuePlace;
}

public void setResidenseVisaIssuePlace(List<SelectItem> residenseVisaIssuePlace) {
	this.residenseVisaIssuePlace = residenseVisaIssuePlace;
}

public List<SelectItem> getPassportIssuePlace() {
	return passportIssuePlace;
}

public void setPassportIssuePlace(List<SelectItem> passportIssuePlace) {
	this.passportIssuePlace = passportIssuePlace;
}
public HtmlSelectOneMenu getResidenseVisaIssuePlaceSelectMenu() {
	return residenseVisaIssuePlaceSelectMenu;
}
public void setResidenseVisaIssuePlaceSelectMenu(
		HtmlSelectOneMenu residenseVisaIssuePlaceSelectMenu) {
	this.residenseVisaIssuePlaceSelectMenu = residenseVisaIssuePlaceSelectMenu;
}

public HtmlSelectOneMenu getPassportIssuePlaceSelectMenu() {
	return passportIssuePlaceSelectMenu;
}

public void setPassportIssuePlaceSelectMenu(
		HtmlSelectOneMenu passportIssuePlaceSelectMenu) {
	this.passportIssuePlaceSelectMenu = passportIssuePlaceSelectMenu;
}

public String getSelectOneTitle() {
	return selectOneTitle;
}

public void setSelectOneTitle(String selectOneTitle) {
	this.selectOneTitle = selectOneTitle;
}

public List<SelectItem> getTitle() {
	title.clear();
	 try
	 {
	 if(sessionMap.containsKey(WebConstants.TITLES))
		 loadTitleList();
	 
	  List<DomainDataView> ddl=(ArrayList)sessionMap.get(WebConstants.TITLES );	 
	  for(int i=0;i<ddl.size();i++)
	  {
		  DomainDataView ddv=(DomainDataView)ddl.get(i);
	      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
	      title.add(item);
	  }
	  
	 }
	 catch(Exception ex)
	 {
		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	 }
	return title;
}

public void setTitle(List<SelectItem> title) {
	this.title = title;
}

public HtmlSelectOneMenu getTitleSelectMenu() {
	return titleSelectMenu;
}

public void setTitleSelectMenu(HtmlSelectOneMenu titleSelectMenu) {
	this.titleSelectMenu = titleSelectMenu;
}

public String getSelectOneCountry() {
	return selectOneCountry;
}

public void setSelectOneCountry(String selectOneCountry) {
	this.selectOneCountry = selectOneCountry;
}

public String getSelectOneState() {
	return selectOneState;
}

public void setSelectOneState(String selectOneState) {
	this.selectOneState = selectOneState;
}

public String getSelectOneCity() {
	return selectOneCity;
}

public void setSelectOneCity(String selectOneCity) {
	this.selectOneCity = selectOneCity;
}

public HtmlInputHidden getHiddenStateValue() {
	return hiddenStateValue;
}

public void setHiddenStateValue(HtmlInputHidden hiddenStateValue) {
	this.hiddenStateValue = hiddenStateValue;
}

public HtmlInputHidden getHiddenCityValue() {
	return hiddenCityValue;
}

public void setHiddenCityValue(HtmlInputHidden hiddenCityValue) {
	this.hiddenCityValue = hiddenCityValue;
}

private String getLoggedInUser() 
{
	FacesContext context = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession) context.getExternalContext()
			.getSession(true);
	String loggedInUser = "";

	if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
		UserDbImpl user = (UserDbImpl) session
				.getAttribute(WebConstants.USER_IN_SESSION);
		loggedInUser = user.getLoginId();
	}

	return loggedInUser;
}

public String getPersonTypeToAdd() {
	return personTypeToAdd;
}

public void setPersonTypeToAdd(String personTypeToAdd) {
	this.personTypeToAdd = personTypeToAdd;
}
public String getLocale(){
	WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getLanguageCode();
}

public String getDateFormat(){
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	return localeInfo.getDateFormat();
}
/**
 * @param isEnglishLocale the isEnglishLocale to set
 */
public void setEnglishLocale(boolean isEnglishLocale) {
	this.isEnglishLocale = isEnglishLocale;
}

/**
 * @param isArabicLocale the isArabicLocale to set
 */
public void setArabicLocale(boolean isArabicLocale) {
	this.isArabicLocale = isArabicLocale;
}

public boolean getIsArabicLocale()
{
	isArabicLocale = !getIsEnglishLocale();
	return isArabicLocale;
}

public boolean getIsEnglishLocale()
{
	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
	isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
	return isEnglishLocale;
}




public List<SelectItem> getCountries() {
	return countries;
}




public void setCountries(List<SelectItem> countries) {
	this.countries = countries;
}




public List<SelectItem> getStates() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	states = (List<SelectItem>) sessionMap.get("states");
	if(states==null)
		states = new ArrayList<SelectItem>();
	return states;
}




public void setStates(List<SelectItem> states) {
	this.states = states;
}




public List<SelectItem> getCities() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	cities = (List<SelectItem>) sessionMap.get("cities");
	if(cities==null)
		cities = new ArrayList<SelectItem>();
	return cities;
}




public void setCities(List<SelectItem> cities) {
	this.cities = cities;
}




public Map<Long, RegionView> getCountryMap() {
	return countryMap;
}




public void setCountryMap(Map<Long, RegionView> countryMap) {
	this.countryMap = countryMap;
}




public Map<Long, RegionView> getStateMap() {
	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	stateMap = (Map<Long, RegionView>) sessionMap.get("stateMap");
	if(stateMap==null)
		stateMap = new HashMap<Long, RegionView>();
	return stateMap;
}




public void setStateMap(Map<Long, RegionView> stateMap) {
	this.stateMap = stateMap;
}




public Map<Long, RegionView> getCityMap() {
	return cityMap;
}




public void setCityMap(Map<Long, RegionView> cityMap) {
	this.cityMap = cityMap;
}
private void loadCountries() {
	try {
		logger.logInfo("loadCountries() started...");
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> regionViewList = psa.getCountry();
		countries = new ArrayList();
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		if(sessionMap.containsKey("countryMap"))
		{
			countryMap = (HashMap<Long, RegionView>) sessionMap.get("countryMap");
		}
		else{
			countryMap = new HashMap<Long, RegionView>();
			for(RegionView regView :regionViewList)
			{
				countryMap.put(regView.getRegionId(), regView);
			}
			sessionMap.put("countryMap",countryMap);
		}
		if(sessionMap.containsKey("countries"))
		{
			countries = (List<SelectItem>) sessionMap.get("countries");
		}
		else{	 
			for(RegionView regionView :regionViewList)
			{
			  SelectItem item;
			  if (getIsEnglishLocale())
			  {
				 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionEn());			  }
			  else 
				 item = new SelectItem(regionView.getRegionId().toString(), regionView.getDescriptionAr());	  
			  countries.add(item);
			}
			sessionMap.put("countries",countries);
		}
		logger.logInfo("loadCountries() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadCountries() crashed ",exception);
	}


}

public void loadStateMap(){
	try {
		logger.logInfo("loadStateMap() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		stateMap = new HashMap<Long, RegionView>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List <RegionView> statesList = psa.getAllStates();
		for(RegionView regView :statesList)
		{
			stateMap.put(regView.getRegionId(), regView);
		}
		sessionMap.put("stateMap",stateMap);
		logger.logInfo("loadStateMap() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadStateMap() crashed ",exception);
	}
}

public void loadStates(ValueChangeEvent vce)  {
	try {
		logger.logInfo("loadStates() started...");
		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		states = new ArrayList<SelectItem>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		String regionName = "";
		Long regionId = Convert.toLong(vce.getNewValue());
		RegionView temp = (RegionView)countryMap.get(regionId);
		if(temp!=null)
		{
			regionName = temp.getRegionName();
			List <RegionView> statesList = psa.getCountryStates(regionName,getIsArabicLocale());
			
			for(RegionView stateView:statesList)
			{
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionEn());			  }
				else 
					item = new SelectItem(stateView.getRegionId().toString(), stateView.getDescriptionAr());	  
				states.add(item);
			}
			sessionMap.put("states",states);
		}
		else{
			sessionMap.remove("states");
		}
			
//		loadCities(vce);
		logger.logInfo("loadStates() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadStates() crashed ",exception);
	}
}

public void loadCities(ValueChangeEvent vce)  {
	try {
		logger.logInfo("loadCities() started...");
		cities = new ArrayList<SelectItem>();
		PropertyServiceAgent psa = new PropertyServiceAgent();
		String selectedStateName = "";
		Long regionId = Convert.toLong(vce.getNewValue());
		RegionView temp = (RegionView)stateMap.get(regionId);
		if(temp!=null)
		{
			selectedStateName = temp.getRegionName();
			List <RegionView> cityList = psa.getCountryStates(selectedStateName,getIsArabicLocale());
		
		
			for(RegionView cityView:cityList)
			{
				SelectItem item;
				if (getIsEnglishLocale())
				{
					item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionEn());			  }
				else 
					item = new SelectItem(cityView.getRegionId().toString(), cityView.getDescriptionAr());	  
				cities.add(item);
			}
			sessionMap.put("cities",cities);
		}
		else{
			sessionMap.remove("cities");
		}
		logger.logInfo("loadCities() completed successfully!!!");
	}catch(Exception exception){
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		logger.LogException("loadCities() crashed ",exception);
	}
}

}
