package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.proxy.pimsconductauctionbpel.PIMSConductAuctionBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.AuctionRequestRegView;
import com.avanza.pims.ws.vo.AuctionVenueView;
import com.avanza.pims.ws.vo.AuctionView;
import com.avanza.pims.ws.vo.AuctionWinnersView;
import com.avanza.pims.ws.vo.BidderAttendanceView;
import com.avanza.pims.ws.vo.BidderUnitDetailView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.DomainTypeView;
import com.avanza.ui.util.ResourceUtil;

public class NewConductAuction extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Variables
	private transient Logger logger = Logger.getLogger(NewConductAuction.class);
	
	private final String TAB_AUCTION_DETAILS = "auctionDetailsTab";
	private final String TAB_AUCTION_UNITS = "auctionUnitsTab";
	private final String TAB_BIDDERS = "biddersTab";
	private final String TAB_ATTACHMENTS = "attachmentTab";
	private final String TAB_COMMENTS = "commentsTab";
	private HtmlSelectBooleanCheckbox selectAll =  new HtmlSelectBooleanCheckbox();
	private String selectedBidder;
	private List<SelectItem> bidderList= new ArrayList<SelectItem>();
	
	private HtmlTabPanel tabPanel = new HtmlTabPanel();
	private HtmlDataTable dataTableBidder;

	private BidderAttendanceView dataItemBidder = new BidderAttendanceView();

	private List<BidderAttendanceView> dataListBidder = new ArrayList<BidderAttendanceView>();

	private HtmlDataTable dataTableUnitsToExclude;

	private AuctionRequestRegView dataItemUnitsToExclude = new AuctionRequestRegView();

	private List<AuctionRequestRegView> dataListUnitsToExclude = new ArrayList<AuctionRequestRegView>();

	private String hiddenAttendanceIds;

	private String hiddenUnitsToExclude;

	private static final String BIDDER_ATTENDANCE = "BIDDER_ATTENDANCE";

	private static final String AUCTION_UNIT_EXCLUSION = "AUCTION_UNIT_EXCLUSION";

	private static final String AUCTION_REQUEST_LIST = "AUCTION_REQUEST_LIST";
	
	private String auctionId;

	private String auctionNo;

	private Date auctionDate;

	private String auctionTime;

	private String auctionTitle;

	private String auctionVenueName;

	private String outbiddingValue;

	private String confiscationPercentage;

	private String auctionStatus;

	private String publishingDate;

	private String auctionStartDate;

	private String auctionEndDate;

	private HtmlDataTable dataTableAuctionUnitRequests;

	private List<AuctionWinnersView> dataListAuctionUnitRequests = new ArrayList<AuctionWinnersView>();

	private AuctionWinnersView dataItemAuctionUnitRequests = new AuctionWinnersView();

	private List<String> messages;
	private List<String> errorMessages = new ArrayList<String>();
	private String infoMessage = "";


	private String hdnResultPopupCalled;

	
	private String exclusionReason;
	private AuctionView auctionView = new AuctionView();

	private Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
	Map sessionMap = getFacesContext().getExternalContext().getSessionMap();

	// private Boolean attendanceTabVisible;
	// private Boolean unitExclusionTabVisible;
	// private Boolean markWinnerTabVisible;

	private int index;

	private int AUCTION_DETAILS_INDEX = 0;
	private int ATTENDANCE_INDEX = 1;
	private int UNIT_EXCLUSION_INDEX = 2;
	private int UNIT_WINNER_INDEX = 3;
	private int ATTACHMENTS_INDEX = 4;
	private int NOTES_INDEX = 5;

	private String winnerDivStyle;
	// private static final String SHOW_WINNER_DIV = "block";
	// private static final String HIDE_WINNER_DIV = "none";

	private Boolean isViewMode = false;
	private Boolean isAttendanceMode = (index == ATTENDANCE_INDEX);

	ServletContext servletcontext = (ServletContext) FacesContext
			.getCurrentInstance().getExternalContext().getContext();

	// private ResourceBundle bundle;

	// Methods

	@SuppressWarnings("unchecked")
	private void loadDataListBidder() 
	{
		String methodName = "loadDataListBidder";
		logger.logInfo(methodName + " started...");
		try 
		{
				List<BidderAttendanceView> bidders = new ArrayList<BidderAttendanceView>();
				bidders = getAuctionBidders();
				UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
				Long bidderStatusAbsent = utilityServiceAgent.getDomainDataByValue(WebConstants.ConductAuction.BIDDER_AUCTION_STATUS, 
						                                                           WebConstants.ConductAuction.ABSENT).getDomainDataId();
				List<BidderAttendanceView> absentBidders = new ArrayList<BidderAttendanceView>();
				for(BidderAttendanceView bidder: bidders)
				{
					if(bidder.getStatusId()!=null && bidder.getStatusId().compareTo(bidderStatusAbsent)==0)
					{
						BidderAttendanceView bidderAttendanceView = new BidderAttendanceView();
						bidderAttendanceView.setBidderId(bidder.getBidderId());
						absentBidders.add(bidderAttendanceView);
					}
				}
				viewMap.put("absentBidders", absentBidders);
				viewMap.put(BIDDER_ATTENDANCE, bidders);
				viewMap.put("ALL_BIDDER_ATTENDANCE", bidders);
				if(viewMap.containsKey("BIDDER_LIST"))
					bidderList = (List<SelectItem>) viewMap.get("BIDDER_LIST");
				if(bidderList == null || bidderList.size()<=0)
				{
					if(bidders !=null && bidders.size()>0)
					{
						SelectItem item ;
						for(BidderAttendanceView bidder : bidders)
						{
							item= new SelectItem(bidder.getName(),bidder.getName());
							bidderList.add(item);
						}
					}
					if(bidderList!=null && bidderList.size()>0)
					  viewMap.put("BIDDER_LIST",bidderList);
				}
				
			logger.logInfo(methodName + " completed successfully...");
		} 
		catch (Exception exception) 
		{
			logger.LogException(methodName + " crashed ...", exception);
		}
	}

	private void loadUnitsToExclude() {
		String methodName = "loadUnitsToExclude";
		logger.logInfo(methodName + " started...");

		try {
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			Object list = viewRootMap.get(AUCTION_UNIT_EXCLUSION);
//			if (list != null) {
//				dataListUnitsToExclude = (List<AuctionRequestRegView>) list;
//			} else {
				dataListUnitsToExclude = getAuctionUnits();
				viewRootMap.put(AUCTION_UNIT_EXCLUSION, dataListUnitsToExclude);
//			}

			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed ...", exception);
			exception.printStackTrace();
		}
	}

	// private ArrayList<AuctionRequestRegView> getUpdatedAuctionUnitList() {
	// String methodName = "getUpdatedAuctionUnitList";
	// ArrayList<AuctionRequestRegView> updatedAuctionUnits = getAuctionUnits();
	// try {
	// logger.logInfo(methodName + " started...");
	// HashMap auctionUnitIdMap = getIdMap(hiddenUnitsToExclude);
	//
	// Iterator i = updatedAuctionUnits.iterator();
	// while (i.hasNext()) {
	// AuctionRequestRegView auctionUnitView = (AuctionRequestRegView) i
	// .next();
	// if (auctionUnitIdMap.containsKey(auctionUnitView
	// .getAuctionUnitId())) {
	// Boolean excluded = (Boolean) auctionUnitIdMap
	// .get(auctionUnitView.getAuctionUnitId());
	//
	// if (excluded)
	// auctionUnitView.setExcluded(true);
	// else
	// auctionUnitView.setExcluded(false);
	// }
	// }
	// logger.logInfo(methodName + " completed successfully...");
	// } catch (Exception exception) {
	// // TODO Auto-generated catch block
	// logger.LogException(methodName + " crashed ...", exception);
	// exception.printStackTrace();
	// }
	// return updatedAuctionUnits;
	// }

	// //////////////////NEW
	// getUpdatedAuctionUnits////////////////////////////////
	private ArrayList<AuctionRequestRegView> getUpdatedAuctionUnitList(
			ArrayList<AuctionRequestRegView> auctionUnits) {
		String methodName = "getUpdatedAuctionUnitList";
		ArrayList<AuctionRequestRegView> updatedAuctionUnits = auctionUnits;
		try {
			logger.logInfo(methodName + " started...");
			HashMap auctionUnitIdMap = getIdMap(hiddenUnitsToExclude);

			Iterator i = updatedAuctionUnits.iterator();
			while (i.hasNext()) {
				AuctionRequestRegView auctionUnitView = (AuctionRequestRegView) i
						.next();
				if (auctionUnitIdMap.containsKey(auctionUnitView
						.getAuctionUnitId())) {
					Boolean excluded = (Boolean) auctionUnitIdMap
							.get(auctionUnitView.getAuctionUnitId());
					auctionUnitView.setExcluded(excluded);
				}
			}
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			logger.LogException(methodName + " crashed ...", exception);
			exception.printStackTrace();
		}
		return updatedAuctionUnits;
	}

	private ArrayList<AuctionRequestRegView> getAuctionUnits() {
		List<AuctionRequestRegView> auctionUnits =  new ArrayList<AuctionRequestRegView>();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		logger.logInfo("getAuctionUnits started...");

		try {
			AuctionView auctionView = new AuctionView();
			auctionView.setAuctionId(Long.parseLong(getAuctionId()));
			auctionUnits = pSAgent.getAllAuctionUnits(auctionView);
			logger.logInfo("getAuctionUnits completed successfully...");
		} catch (PimsBusinessException e) {
			logger.LogException("getAuctionUnits crashed...",e);
			
		}

		return (ArrayList<AuctionRequestRegView>) auctionUnits;
	}

	private ArrayList<BidderAttendanceView> getAuctionBidders() {
		List<BidderAttendanceView> bidders = new ArrayList<BidderAttendanceView>();
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		logger.logInfo("getAuctionBidders started...");

		try {
			bidders = pSAgent.getAuctionBidders(Long.parseLong(getAuctionId()), true);
			logger.logInfo("getAuctionBidders completed successfully...");
		} catch (PimsBusinessException e) {
			logger.LogException("getAuctionBidders crashed...",e);
		}
		return (ArrayList<BidderAttendanceView>) bidders;
	}

	private HashMap getIdMap(String ids) {
		HashMap map = new HashMap();
		logger.logInfo("getIdMap started...");

		try {
			if (ids.length() > 0) {
				String[] idArray = ids.split(",");
				for (int i = 0; i < idArray.length; i++) {
					Long key = Long.parseLong(idArray[i].split(":")[1]);
					String value = idArray[i].split(":")[0];
					if (value.equals("1"))
						map.put(key, true);
					else
						map.put(key, false);
				}
			}
			logger.logInfo("getIdMap completed successfully...");
		} catch (Exception e) {
			logger.logInfo("getIdMap crashed...");
			e.printStackTrace();
		}
		return map;
	}

	private void loadAuctionInfo() {
		logger.logInfo("loadAuctionInfo started...");

		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			AuctionView auctionView = pSAgent.getAuctionById(Long
					.parseLong(getAuctionId()));
			setAuctionDate(auctionView.getAuctionDateVal());
			setAuctionTime(auctionView.getAuctionStartTime());
			setOutbiddingValue(auctionView.getOutbiddingValue().toString());
			setAuctionNo(auctionView.getAuctionNumber());
			setAuctionTitle(auctionView.getAuctionTitle());
			setAuctionVenueName(auctionView.getVenueName());
			setAuctionVenueIdInSession(auctionView.getAuctionVenue()
					.getVenueId());
			setConfiscationPercentage(auctionView.getConfiscationPercent()
					.toString());
			setAuctionStatus(auctionView.getAuctionStatus());

			if (auctionView.getAuctionAdvertisement() != null
					&& auctionView.getAuctionAdvertisement()
							.getAdvertisementDate() != null)
				setPublishingDate(CommonUtil.getStringFromDate(auctionView.getAuctionAdvertisement()
						.getAdvertisementDate()));

			if (auctionView.getRequestStartDate() != null)
				setAuctionStartDate(CommonUtil.getStringFromDate(auctionView.getRequestStartDate()));
			if (auctionView.getRequestEndDate() != null)
				setAuctionEndDate(CommonUtil.getStringFromDate(auctionView.getRequestEndDate()));

			viewMap.put(WebConstants.AUCTION_ID, auctionId);

			loadAttachmentsAndComments();

//			setScreenToViewMode(auctionView);

			viewMap.put("isAuctionLoaded", true);
			
			logger.logInfo("loadAuctionInfo completed successfully...");
		} catch (Exception e) {
			logger.logInfo("loadAuctionInfo crashed...");
			e.printStackTrace();
		}
	}

	private void setScreenToViewMode(AuctionView auctionView) throws Exception {
		logger.logInfo("setScreenToViewMode|started...");
		if (isAuctionStatusCompleted(auctionView.getAuctionStatusId())) {
			isViewMode = true;
		}
		logger.logInfo("setScreenToViewMode|ended...");
	}

	private boolean isAuctionStatusCompleted(String auctionStatusId)
			throws Exception {
		boolean returnVal = false;
		Long statusId = new Long(0);
		String METHOD_NAME = "isAuctionStatusCompleted|";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			HashMap hMap = getIdFromType(
					getDomainDataListForDomainType(WebConstants.AUCTION_STATUS),
					WebConstants.AUCTION_COMPLETED_STATUS);
			if (hMap.containsKey("returnId")) {
				statusId = Long.parseLong(hMap.get("returnId").toString());
				if (statusId.equals(null))
					throw new PimsBusinessException(new Integer(103),
							"GENERAL_EXCEPTION");
			}
			if (statusId.equals(Long.parseLong(auctionStatusId)))
				returnVal = true;
		} catch (Exception e) {
			logger.logInfo(METHOD_NAME + " ended...");
			throw e;
		}
		return returnVal;
	}

	public HashMap getIdFromType(List<DomainDataView> ddv, String type) {
		String methodName = "getIdFromType";
		logger.logInfo(methodName + "|" + "Start...");
		logger.logInfo(methodName + "|" + "DomainDataType To search..." + type);
		HashMap hMap = new HashMap();
		for (int i = 0; i < ddv.size(); i++) {
			DomainDataView domainDataView = (DomainDataView) ddv.get(i);
			if (domainDataView.getDataValue().equals(type)) {
				logger.logInfo(methodName + "|" + "DomainDataId..."
						+ domainDataView.getDomainDataId());
				hMap.put("returnId", domainDataView.getDomainDataId());
				logger.logInfo(methodName + "|" + "DomainDataDesc En..."
						+ domainDataView.getDataDescEn());
				hMap.put("IdEn", domainDataView.getDataDescEn());
				hMap.put("IdAr", domainDataView.getDataDescAr());
			}

		}
		logger.logInfo(methodName + "|" + "Finish...");
		return hMap;

	}

	private List<DomainDataView> getDomainDataListForDomainType(
			String domainType) {
		List<DomainDataView> ddvList = new ArrayList<DomainDataView>();
		List<DomainTypeView> list = (List<DomainTypeView>) servletcontext
				.getAttribute(WebConstants.DOMAIN_TYPE_DATA_LIST);
		Iterator<DomainTypeView> itr = list.iterator();
		// Iterates for all the domain Types
		while (itr.hasNext()) {
			DomainTypeView dtv = itr.next();
			if (dtv.getTypeName().compareTo(domainType) == 0) {
				Set<DomainDataView> dd = dtv.getDomainDatas();
				// Iterates over all the domain datas
				for (DomainDataView ddv : dd) {
					ddvList.add(ddv);
				}
				break;
			}
		}

		return ddvList;
	}

	private void setAuctionVenueIdInSession(Long venueId) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put(WebConstants.ConductAuction.CONDUCT_AUCTION_VENUE_ID,
						venueId);
	}

	// private Long getAuctionVenueIdFromSession() {
	// Long venueId = 0L;
	// Object objVenueId = FacesContext.getCurrentInstance()
	// .getExternalContext().getSessionMap().get(
	// WebConstants.ConductAuction.CONDUCT_AUCTION_VENUE_ID);
	// if (objVenueId != null)
	// venueId = Long.parseLong(objVenueId.toString());
	// return venueId;
	// }

	private void loadAuctionRequestsList() {
		String METHOD_NAME = "loadAuctionRequestsList";
		logger.logInfo(METHOD_NAME + " started...");

		try {
			// PropertyServiceAgent pSAgent = new PropertyServiceAgent();
			// List<AuctionWinnersView> auctionWinners =
			// pSAgent.getAuctionParticipants(Long.parseLong(getAuctionId()));
			// dataListAuctionUnitRequests = auctionWinners;

			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
					.getAttributes();
			Object list = viewRootMap.get(AUCTION_REQUEST_LIST);
			if (list != null) {
				dataListAuctionUnitRequests = (List<AuctionWinnersView>) list;
			} else {
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
				List<AuctionWinnersView> auctionWinners = pSAgent
						.getAuctionParticipants(Long.parseLong(getAuctionId()));
				dataListAuctionUnitRequests = auctionWinners;
				viewRootMap.put(AUCTION_REQUEST_LIST,
						dataListAuctionUnitRequests);
			}

			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception e) {
			logger.logInfo(METHOD_NAME + " crashed...");
			e.printStackTrace();
		}
	}

	public String editRequest() {
		// OLD IMPLEMENTATION FOR MARK-WINNER's Tab
		// AuctionWinnersView auctionWinnersView = (AuctionWinnersView)
		// dataTableAuctionUnitRequests
		// .getRowData();
		//
		// Map sessionMap =
		// FacesContext.getCurrentInstance().getExternalContext()
		// .getSessionMap();
		// sessionMap.put(WebConstants.ConductAuction.AUCTION_WINNER,
		// auctionWinnersView);
		// //
		// ///////////////////////////////////////////////////////////////////////////
		// final String viewId = "/NewConductAuction.jsp";
		//
		// FacesContext facesContext = FacesContext.getCurrentInstance();
		//
		// // This is the proper way to get the view's url
		// ViewHandler viewHandler = facesContext.getApplication()
		// .getViewHandler();
		// String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		//
		// // Your Java script code here
		// AuctionWinnersView auctionWinnerView = (AuctionWinnersView)
		// dataTableAuctionUnitRequests
		// .getRowData();
		//
		// String javaScriptText = "var screen_width = screen.width;\n "
		// + "var screen_height = screen.height;\n "
		// + "var queryString = '?aucId='+"
		// + auctionWinnerView.getAuctionId()
		// + "+'&aucUnitId='+"
		// + auctionWinnerView.getAuctionUnitId()
		// + "+'&bidPrice='+"
		// + auctionWinnerView.getBidPrice()
		// + ";\n"
		// + "window.open('SelectWinner.jsf'+queryString
		// ,'_blank','width='+(screen_width-150)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=yes,status=yes');";
		//
		// // Add the Javascript to the rendered page's header for immediate
		// // execution
		// AddResource addResource =
		// AddResourceFactory.getInstance(facesContext);
		// addResource.addInlineScriptAtPosition(facesContext,
		// AddResource.HEADER_BEGIN, javaScriptText);
		// return "";

		// NEW IMPLEMENTATION FOR UNIT's tab
		
		
		
		AuctionRequestRegView auctionWinnersView = (AuctionRequestRegView) dataTableUnitsToExclude.getRowData();

		Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		
		
		if(!viewMap.containsKey(BIDDER_ATTENDANCE))
			loadDataListBidder();
		if(viewMap.get("ALL_BIDDER_ATTENDANCE") != null )
		{
			List<BidderAttendanceView> bidders = new ArrayList<BidderAttendanceView>();
			HashMap<Long, Double> bidderIdDepositPair = new HashMap<Long, Double>();
			bidders = (List<BidderAttendanceView>) viewMap.get("ALL_BIDDER_ATTENDANCE");
			if(bidders.size()>0 )
			{
				for(BidderAttendanceView BAV : bidders)
					bidderIdDepositPair.put(BAV.getBidderId(), BAV.getTotalDepositAmount());
				
				sessionMap.put("BIDDER_ID_DEPOSIT_PAIR", bidderIdDepositPair);
			}
		}
		List<BidderAttendanceView> absentBidders = (List<BidderAttendanceView>) viewMap.get("absentBidders");
		if(absentBidders!=null && !absentBidders.isEmpty()){
			sessionMap.put("absentBidders", absentBidders);
		}
		
		//sessionMap.put(WebConstants.ConductAuction.AUCTION_WINNER,auctionWinnersView);
		// ///////////////////////////////////////////////////////////////////////////
		final String viewId = "/NewConductAuction.jsp";

		FacesContext facesContext = FacesContext.getCurrentInstance();

		// This is the proper way to get the view's url
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);

		// Your Java script code here
		AuctionRequestRegView auctionWinnerView = (AuctionRequestRegView) dataTableUnitsToExclude
				.getRowData();
		
		sessionMap.put(WebConstants.ConductAuction.AUCTION_WINNER,auctionWinnerView);

		String javaScriptText = "var screen_width = 1024;\n "
				+ "var screen_height = 450;\n "
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;\n "
				+ "var popup_width = screen_width-80;\n "
				+ "var popup_height = screen_height-383;\n "
				+ "var queryString = '?aucId='+"
				+ auctionWinnerView.getAuctionId()
				+ "+'&aucUnitId='+"
				+ auctionWinnerView.getAuctionUnitId()
				+ "+'&bidPrice='+"
				+ auctionWinnerView.getBidPrice()
				+ ";\n"
				+ "window.open('SelectWinner.jsf'+queryString ,'_blank','width='+(screen_width-160)+',height='+(screen_height)+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes');";
		sessionMap.put("NEW_UNIT_ID", auctionWinnerView.getUnitId());
		sessionMap.put("NEW_UNIT_NUMBER", auctionWinnerView.getUnitNumber());
		
		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}
	
	private Boolean calculateAndSetConfiscationAmounts()throws Exception{
		Boolean success = false;
		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
		logger.logInfo("calculateAndSetConfiscationAmounts started...");
		success = pSAgent.calculateAndSetConfiscationAmounts(Long.parseLong(getAuctionId()));
		logger.logInfo("calculateAndSetConfiscationAmounts completed successfully...");
		return success;
	}

	private Boolean canCompleteAuction(){
		Boolean valid = true;
		Date today = new Date();
		Date auctionDate = getAuctionDate();
		//TODO:Commented for UAT purpose need to open it for Production
//		if(today.before(auctionDate)){
//			valid = false;
//			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.CANNOT_COMPLETE_AUCTION));
//		}
		if(!allWinnersSelected()){
			valid = false;
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.ALL_WINNERS_NOT_SELECTED));
		}
			
		return valid;
	}
	
	private Boolean allWinnersSelected(){
		for( AuctionRequestRegView unitDetails :dataListUnitsToExclude){
			if(unitDetails.getRegistered() && StringHelper.isEmpty(unitDetails.getWinner()))
				return false;
		}
		return true;
	}
	
	public String completeAuction() {
		String METHOD_NAME = "completeAuction";
		try {
			logger.logInfo(METHOD_NAME + " started...");
			if(canCompleteAuction())
			{
				PIMSConductAuctionBPELPortClient client = new PIMSConductAuctionBPELPortClient();
 		        SystemParameters parameters = SystemParameters.getInstance();
				String endPoint = parameters.getParameter(WebConstants.ConductAuction.CONDUCT_AUCTION_BPEL_END_POINT);
				client.setEndpoint(endPoint);
				Long auctionId = Long.parseLong(getAuctionId());
				String loggedInUser = getLoggedInUser();
				
				client.initiate(auctionId, loggedInUser, null, null); // isViewMode
				new PropertyServiceAgent().setAuctionAsCompleted(auctionId,loggedInUser);
				calculateAndSetConfiscationAmounts();
				viewMap.put("isViewMode", true);
				
				writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.AUCTION_COMPLETE_SUCCESS), false);
				
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				
				
				NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, MessageConstants.ConductAuction.AUCTION_COMPLETE_SUCCESS ,Long.parseLong(getAuctionId()));
			
				
			}
			
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception e) {
			logger.LogException( METHOD_NAME + " crashed...",e);
			writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.AUCTION_COMPLETE_FAILURE), true);
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	private String getLoggedInUser() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		User user = webContext.getAttribute(CoreConstants.CurrentUser);
		String loginId = "";
		if (user != null)
			loginId = user.getLoginId();
		return loginId;
	}

	private void setRequestInfoInSession() {
		String METHOD_NAME = "setRequestInfoInSession";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			Object obj = viewMap.get(WebConstants.AUCTION_ID);
			if (obj != null) {
				sessionMap.put(WebConstants.ConductAuction.CONDUCT_AUCTION_ID,
						obj);
			}
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
	}

	private void showResultMessage() {
		String METHOD_NAME = "showResultMessage()";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			if (getHdnResultPopupCalled().equals("Y")) {
				FacesContext.getCurrentInstance().getViewRoot().getAttributes()
						.remove(AUCTION_REQUEST_LIST);
				loadAuctionRequestsList();

				hdnResultPopupCalled = "N";
				// messages = new ArrayList<String>();
				// messages.add(getBundle().getString(WebConstants.CommonMessage.RECORD_SAVED));
				// writeMessage(getBundle().getString(WebConstants.CommonMessage.RECORD_SAVED));
				// writeMessage(getBundle().getString(MessageConstants.ConductAuction.WINNER_SAVED));
				writeMessage(CommonUtil.getBundleMessage(
						MessageConstants.ConductAuction.WINNER_SAVED), false);
			}
			// else{
			// messages = new ArrayList<String>();
			// }
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
	}

	private String saveAuctionDetails() {
		String METHOD_NAME = "saveAuctionDetails()";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			PropertyServiceAgent pSAgent = new PropertyServiceAgent();

			auctionView = pSAgent.getAuctionById(new Long(auctionId));
			auctionView.setAuctionDate(CommonUtil
					.getStringFromDate(auctionDate));
			auctionView.setAuctionDateVal(auctionDate);
			auctionView.setAuctionVenue((AuctionVenueView) viewMap
					.get("auctionVenueView"));
			pSAgent.updateAuction(auctionView);
			writeMessage(CommonUtil.getBundleMessage(
					MessageConstants.ConductAuction.AUCTION_UPDATE_SUCCESS), false);
			
			NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, MessageConstants.ConductAuction.AUCTION_UPDATE_SUCCESS ,Long.parseLong(getAuctionId()));
			
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
		return "";
	}

	private Map getArgMap(){
		Map argMap = new HashMap();
		argMap.put("userId", CommonUtil.getLoggedInUser());
		argMap.put("dateFormat", CommonUtil.getDateFormat());
		argMap.put("isEnglishLocale", CommonUtil.getIsEnglishLocale());
//		argMap.
		return argMap;
	}
	
	public void markAsRefunded(javax.faces.event.ActionEvent event)
    {
    	logger.logInfo("markAsRefunded() started...");
    	Boolean success = true;
    	try{
    		BidderAttendanceView bidderAttendanceView =(BidderAttendanceView)dataTableBidder.getRowData();
    		int index = dataTableBidder.getRowIndex();
    		PropertyServiceAgent pSAgent = new PropertyServiceAgent();
    		success = pSAgent.markAsRefunded(bidderAttendanceView.getBidderUnitDetailViews(), true, CommonUtil.getLoggedInUser());
 
    		if(success)
    		{
    			infoMessage = CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MARKED_AS_REFUNDED_SUCCESS);
    			
    			String sysMsgEn = CommonUtil.getParamBundleMessage("conductAuction.refundTransactionDone","en",bidderAttendanceView.getName());
				String sysMsgAr = CommonUtil.getParamBundleMessage("conductAuction.refundTransactionDone","ar",bidderAttendanceView.getName());

				NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, sysMsgEn, sysMsgAr,Long.parseLong(getAuctionId()));
    			
    			loadDataListBidder();
    		}
 	        logger.logInfo("markAsRefunded() completed successfully!!!");
    	}
    	catch(PimsBusinessException exception){
    		success = false;
    		logger.LogException("markAsRefunded() crashed ",exception);
    	}
    	catch(Exception exception){
    		logger.LogException("markAsRefunded() crashed ",exception);
    		success = false;
    	}
    	finally{
    		if(!success){
    			errorMessages.clear();
        		errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.MARKED_AS_REFUNDED_FAILURE));
    		}
    	}
    }
	
	public String cmdBidderEdit_Click() {
		String methodName = "cmdBidderEdit_Click";
		logger.logInfo(methodName + "|Start");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		BidderAttendanceView dataItem = new BidderAttendanceView();
		if (dataTableBidder.getRowCount() > 0
				&& dataTableBidder.getRowData() != null)
			dataItem = (BidderAttendanceView) dataTableBidder.getRowData();
		logger.logDebug(methodName + "|Person Id::" + dataItem.getBidderId());
		setRequestParam(WebConstants.PERSON_ID, dataItem.getBidderId());
		setRequestParam("viewMode", "popup");
		String javaScriptText = "javascript:showPersonReadOnlyPopup("+dataItem.getBidderId()+");";
		
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		logger.logInfo(methodName + "|Finish");
		return "";

	}
   @SuppressWarnings("unchecked")
	public String markAbsent() 
   {
		String METHOD_NAME = "markAbsent()";
		logger.logInfo(METHOD_NAME + " started...");

		try 
		{
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object list = new ArrayList<BidderAttendanceView>( 0 );
			if(viewMap.containsKey("SELECTED_BIDDER") && viewMap.get("SELECTED_BIDDER")!=null )
				list = (ArrayList<BidderAttendanceView>) viewMap.get("SELECTED_BIDDER");
			
			else if (viewMap.containsKey(BIDDER_ATTENDANCE))
				list = (ArrayList<BidderAttendanceView>) viewMap.get(BIDDER_ATTENDANCE);
			
			if (list != null) 
			{
				List<BidderAttendanceView> bidders = (List<BidderAttendanceView>) list;
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
				dataListBidder = bidders;
				List<BidderAttendanceView> absentBidders = new ArrayList<BidderAttendanceView>();
				for (BidderAttendanceView bidderAttendanceView : bidders)
					if (bidderAttendanceView.getAttendance() && !bidderAttendanceView.getAnyWin().equals("Y")) 
					{
						bidderAttendanceView.setAttendance(false);
						UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
						Long statusId = utilityServiceAgent.getDomainDataByValue(WebConstants.ConductAuction.BIDDER_AUCTION_STATUS, 
								                                                 WebConstants.ConductAuction.ABSENT).getDomainDataId();
						bidderAttendanceView.setStatusId(statusId);
						absentBidders.add(bidderAttendanceView);
					}

				if (absentBidders.isEmpty()) 
					writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.NO_BIDDERS_SELECTED),true);
				else
				{
					pSAgent.updateBidderAttendance(absentBidders);
					writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.BIDDERS_ABSENT_MARKED), false);
				}
			}
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} 
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " crashed...", exception);
		}
		return "";
	}
   @SuppressWarnings ( "unchecked" ) 
	public String markPresent() 
   {
		String METHOD_NAME = "markPresent()";
		logger.logInfo(METHOD_NAME + " started...");

		try 
		{
			Object list = new ArrayList<BidderAttendanceView>( 0 );
			if(viewMap.containsKey("SELECTED_BIDDER") && viewMap.get("SELECTED_BIDDER")!=null )
				list = (ArrayList<BidderAttendanceView>) viewMap.get("SELECTED_BIDDER");
			else if (viewMap.containsKey(BIDDER_ATTENDANCE))
				list = (ArrayList<BidderAttendanceView>) viewMap.get(BIDDER_ATTENDANCE);
			if (list != null) 
			{
				List<BidderAttendanceView> bidders = (List<BidderAttendanceView>) list;
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
				dataListBidder = bidders;

				List<BidderAttendanceView> presentBidders = new ArrayList<BidderAttendanceView>();

				for (BidderAttendanceView bidderAttendanceView : bidders)
					if ( bidderAttendanceView.getAttendance() ) {
						bidderAttendanceView.setAttendance(true);
						UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
						Long statusId = utilityServiceAgent.getDomainDataByValue(WebConstants.ConductAuction.BIDDER_AUCTION_STATUS, WebConstants.ConductAuction.PRESENT).getDomainDataId();
						bidderAttendanceView.setStatusId(statusId);
						presentBidders.add(bidderAttendanceView);
					}

				if (presentBidders.isEmpty()) {
					writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.NO_BIDDERS_SELECTED),true);
				}
				else{
					pSAgent.updateBidderAttendance(presentBidders);
					writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.BIDDERS_PRESENT_MARKED), false);
				}
			}
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
		return "";
	}

	// public String markWinner() {
	// String METHOD_NAME = "markWinner()";
	// logger.logInfo(METHOD_NAME + " started...");
	//
	// Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
	// .getAttributes();
	// Object list = viewRootMap.get(BIDDER_ATTENDANCE);
	// if (list == null)
	// return "";
	//
	// try {
	// List<BidderAttendanceView> bidders = (List<BidderAttendanceView>) list;
	// dataListBidder = bidders;
	//
	// List<BidderAttendanceView> winnerBidder = new
	// ArrayList<BidderAttendanceView>();
	//
	// for (BidderAttendanceView bidderAttendanceView : dataListBidder)
	// if (bidderAttendanceView.getAttendance())
	// winnerBidder.add(bidderAttendanceView);
	//
	// if (winnerBidder.isEmpty()) {
	// writeMessage(CommonUtil.getBundleMessage(
	// MessageConstants.ConductAuction.NO_BIDDERS_SELECTED));
	// return "";
	// }
	//
	// if (winnerBidder.size() >= 1) {
	// writeMessage(CommonUtil.getBundleMessage(
	// MessageConstants.ConductAuction.MULTI_BIDDERS_ERROR));
	// return "";
	// }
	//
	// BidderAttendanceView bidderAttendanceView = (BidderAttendanceView)
	// winnerBidder
	// .get(0);
	// UtilityServiceAgent utilityServiceAgent = new UtilityServiceAgent();
	// Long statusId = utilityServiceAgent.getDomainDataByValue(
	// WebConstants.ConductAuction.BIDDER_AUCTION_STATUS,
	// WebConstants.ConductAuction.WINNER).getDomainDataId();
	// bidderAttendanceView.setStatusId(statusId);
	//
	// PropertyServiceAgent pSAgent = new PropertyServiceAgent();
	// pSAgent.updateBidderAttendance(winnerBidder);
	// writeMessage(CommonUtil.getBundleMessage(
	// MessageConstants.ConductAuction.BIDDER_WINNER_MARKED));
	//
	// logger.logInfo(METHOD_NAME + " completed successfully...");
	// } catch (Exception exception) {
	// logger.LogException(METHOD_NAME + " crashed...", exception);
	// exception.printStackTrace();
	// }
	//
	// return "";
	// }
    @SuppressWarnings("unchecked")
	public String saveAttendance() 
    {
		String METHOD_NAME = "saveAttendance()";
		logger.logInfo(METHOD_NAME + " started...");
		try 
		{

			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object list = viewRootMap.get(BIDDER_ATTENDANCE);
			if (list != null) 
			{
				List<BidderAttendanceView> bidders = (ArrayList<BidderAttendanceView>) list;
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
				dataListBidder = bidders;
				writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.ATTENDANCE_SAVED), false);
				
				NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, MessageConstants.ConductAuction.ATTENDANCE_SAVED ,Long.parseLong(getAuctionId()));
				
				pSAgent.updateBidderAttendance(bidders);
			}

			logger.logInfo(METHOD_NAME + " completed successfully...");
		} 
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " crashed...", exception);
		}
		return "";
	}
   @SuppressWarnings("unchecked")
	public String saveUnitExclusion() {
		String METHOD_NAME = "saveUnitExclusion()";
		logger.logInfo(METHOD_NAME + " started...");
		try 
		{
			Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			Object list = viewRootMap.get(AUCTION_UNIT_EXCLUSION);
			if (list != null)
			{
				ArrayList<AuctionRequestRegView> auctionUnits = (ArrayList<AuctionRequestRegView>) list;
				PropertyServiceAgent pSAgent = new PropertyServiceAgent();
				pSAgent.updateAuctionUnits(auctionUnits);
				dataListUnitsToExclude = auctionUnits;
				// Added to ensure that new unit list of un-excluded units is loaded
				viewRootMap.remove(AUCTION_REQUEST_LIST);
				writeMessage(CommonUtil.getBundleMessage(MessageConstants.ConductAuction.UNIT_EXCLUSION_SAVED), false);
				
				if (auctionUnits.size()>0){
					int excludedUnit = 0;
					String excludedUnitList = "";

					for (AuctionRequestRegView auctionRequestRegView : auctionUnits) {
						if (auctionRequestRegView.getExcluded()) {
							excludedUnit ++;
							excludedUnitList += auctionRequestRegView.getUnitNumber() + ",";
						}
					}
					if (excludedUnit>0) {
						excludedUnitList = excludedUnitList.substring(0, excludedUnitList.length()-1);

						String sysMsgEn = CommonUtil.getParamBundleMessage("conductAuction.excludedUnits","en",excludedUnitList);
						String sysMsgAr = CommonUtil.getParamBundleMessage("conductAuction.excludedUnits","ar",excludedUnitList);

						NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, sysMsgEn, sysMsgAr,Long.parseLong(getAuctionId()));
					}
				}
				
			}
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " crashed...", exception);
			
		}
		return "";
	}

	public String save() {
		String METHOD_NAME = "save()";
		logger.logInfo(METHOD_NAME + " started...");

		if (isAuctionDetailsCurrent())
			saveAuctionDetails();
		else if (isAttendanceCurrent())
			saveAttendance();
		else if (isUnitExclusionCurrent())
			saveUnitExclusion();
		else if (isDocumentsCurrent() || isNotesCurrent()) {
			saveDocuments();
			saveNotes();
		}
		
		

		logger.logInfo(METHOD_NAME + " ended...");

		return "";
	}

	// public String attendanceAction(){
	// attendanceTabVisible = true;
	// unitExclusionTabVisible = false;
	// markWinnerTabVisible = false;
	// return "";
	// }
	//	
	// public String unitExclusionAction(){
	// attendanceTabVisible = false;
	// unitExclusionTabVisible = true;
	// markWinnerTabVisible = false;
	// return "";
	// }
	//	
	// public String auctionWinnersAction(){
	// attendanceTabVisible = false;
	// unitExclusionTabVisible = false;
	// markWinnerTabVisible = true;
	// return "";
	// }

	// public void toggleWinnerDiv(String hideWinnerDiv) {
	// final String viewId = "/NewConductAuction.jsp";
	//		
	// FacesContext facesContext = FacesContext.getCurrentInstance();
	//
	// // This is the proper way to get the view's url
	// ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	// String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	//
	// //Your Java script code here
	// String javaScriptText =
	// "javascript:toggleWinnerDiv('"+hideWinnerDiv+"');";
	// // Add the Javascript to the rendered page's header for immediate
	// execution
	// AddResource addResource = AddResourceFactory.getInstance(facesContext);
	// addResource.addInlineScriptAtPosition(facesContext,
	// AddResource.HEADER_BEGIN, javaScriptText);
	// }

	@SuppressWarnings("unchecked")
	public void openBidderUnitPopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openBidderUnitPopup() started...");
		try
		{
			BidderAttendanceView bidderAttendanceView =(BidderAttendanceView)dataTableBidder.getRowData();
	   		sessionMap.put(WebConstants.AuctionRefund.BIDDER_ATTENDANCE_VIEW ,bidderAttendanceView);
	   		sessionMap.put("isRefundAuctionFinalMode" ,getIsRefundAuctionFinalMode());
	   		sessionMap.put("isRefundAuctionApprovalMode" ,getIsRefundAuctionApprovalMode());
	   		sessionMap.put("isConductMode" ,!getIsRefundMode());
	   		
//	   		sessionMap.put(WebConstants.AuctionRefund.BIDDER_UNIT_DETAIL_VIEW_LIST, bidderAttendanceView.getBidderUnitDetailViews());
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showBidderUnitPopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openBidderUnitPopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openBidderUnitPopup() crashed ", exception);
		}
    }
	
	@SuppressWarnings("unchecked")
	public void checkTask(){
		logger.logInfo("checkTask() started...");
		UserTask userTask = null;
		Long auctionId = 0L;
		try{
			userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(WebConstants.AuctionRefund.TASK_LIST_USER_TASK,userTask);
			
			if(userTask!=null)
			{
				Map taskAttributes =  userTask.getTaskAttributes();				
				if(taskAttributes.containsKey(WebConstants.UserTasks.AUCTION_ID))
				{
					auctionId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.AUCTION_ID));
					viewMap.put(WebConstants.AUCTION_ID, auctionId);
				}
				
				String taskType = userTask.getTaskType();
				if(taskType.equals(WebConstants.UserTasks.ApproveAuctionRefund.TASK_TYPE_NAME))
				{
					viewMap.put("isRefundAuctionApprovalMode",true);
//					viewMap.put("selectedTab", TAB_BIDDERS);
				}	
				
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
				
				viewMap.put("heading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.REFUND_HEADING));
				viewMap.put("bidderTabHeading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.REFUND_BIDDER_TAB_HEADING));
				
				viewMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_TASK_LIST);
			}
			logger.logInfo("checkTask() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("checkTask() crashed ", exception);
		}
	}
	
	@Override
	public void init() {
		String METHOD_NAME = "init|";
		logger.logInfo(METHOD_NAME + "started...");
		super.init();
		
		////// Simulating Refund Approval Mode //////
//		UserTask temp = new UserTask();
//		Map<String,String> taskAtr = new HashMap<String, String>();
//		taskAtr.put(WebConstants.UserTasks.AUCTION_ID,61L+"");
//		temp.setTaskAttributes(taskAtr);
//		temp.setTaskType(WebConstants.UserTasks.ApproveAuctionRefund.TASK_TYPE_NAME);
//		sessionMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,temp);
		/////////////////////////////
		
		if (!isPostBack()) {
			viewMap.remove("isAuctionLoaded");
			
			if(sessionMap.containsKey(WebConstants.AUCTION_ID)){
				Long auctionId = (Long)sessionMap.get(WebConstants.AUCTION_ID);
				if(auctionId!=null)
					viewMap.put(WebConstants.AUCTION_ID,auctionId+"");
				sessionMap.remove(WebConstants.AUCTION_ID);
			}
			
			setRequestInfoInSession();
			viewMap.put("TAB_INDEX", AUCTION_DETAILS_INDEX);
			viewMap.put("canAddAttachment", true);
			viewMap.put("canAddNote", true);
			loadAttachmentsAndComments();
			
			// bidder auction unit details changed from popup and hence need to be reloaded
			if(sessionMap.get("reloadBidders")!=null)
			{
				loadDataListBidder();
				sessionMap.remove("reloadBidders");
			}
			
			// FROM AUCTION SEARCH - VIEW MODE
			if(sessionMap.get(WebConstants.VIEW_MODE)!=null)
			{
				viewMap.put("isViewMode", true);
				sessionMap.remove(WebConstants.VIEW_MODE);
				
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
				
				viewMap.put("heading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.VIEW_HEADING));
				viewMap.put("bidderTabHeading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.REFUND_BIDDER_TAB_HEADING));
				
				viewMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_AUCTION_SEARCH);
			}
			// FROM AUCTION SEARCH - REFUND FINAL MODE
			else if(sessionMap.get("isRefundAuctionFinalMode")!=null)
			{
				Boolean isRefundAuctionFinalMode = (Boolean) sessionMap.get("isRefundAuctionFinalMode");
				viewMap.put("isRefundAuctionFinalMode", isRefundAuctionFinalMode);
				sessionMap.remove("isRefundAuctionFinalMode");
				attendanceAction();
				viewMap.put("canAddAttachment",true);
				viewMap.put("canAddNote", true);
				viewMap.put("selectedTab", TAB_BIDDERS);
				
				viewMap.put("heading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.REFUND_HEADING));
				viewMap.put("bidderTabHeading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.REFUND_BIDDER_TAB_HEADING));
				
				viewMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_AUCTION_SEARCH);
			}
			else{
				// FROM TASK LIST
				if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
				{
					// REFUND APPROVE MODE
					checkTask();
				}
				else{
					// CONDUCT MODE
					viewMap.put("heading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.CONDUCT_HEADING));
					viewMap.put("bidderTabHeading", CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Auction.ConductAndRefund.CONDUCT_BIDDER_TAB_HEADING));
					viewMap.put(WebConstants.BACK_SCREEN, WebConstants.BACK_SCREEN_AUCTION_SEARCH);
				}
			}
		}

		if (sessionMap.containsKey(WebConstants.AUCTION_VENUE_ID)) {
			Long venueId = (Long) sessionMap.get(WebConstants.AUCTION_VENUE_ID);
			if (venueId != null)
				viewMap.put(WebConstants.AUCTION_VENUE_ID, venueId);
			sessionMap.remove(WebConstants.AUCTION_VENUE_ID);
		}

		if (viewMap.containsKey(WebConstants.AUCTION_VENUE_ID)) {
			AuctionVenueView auctionVenue = (AuctionVenueView) viewMap
					.get("auctionVenueView");
			Long auctionVenueId = Convert.toLong(viewMap
					.get(WebConstants.AUCTION_VENUE_ID));
			PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
			try {
				auctionVenue = propertyServiceAgent
						.getAuctionVenueById(auctionVenueId);
			} catch (PimsBusinessException e) {
				e.printStackTrace();
			}
			viewMap.put("auctionVenueView", auctionVenue);
			setAuctionVenueName(auctionVenue.getVenueName());
			auctionView.setAuctionVenue(auctionVenue);
			viewMap.remove(WebConstants.AUCTION_VENUE_ID);
		}

//		if(!(getIsRefundMode() && getIsAuctionLoaded()))
			loadAuctionInfo();
		// try {
		// loadAttachmentsAndComments(getAuctionVenueIdFromSession());
		// } catch (Exception e) {
		// logger.LogException(METHOD_NAME + "crashed...", e);
		// }
		logger.logInfo(METHOD_NAME + "ended...");

	}

	/**
	 * <p>
	 * Callback method that is called after the component tree has been
	 * restored, but before any event processing takes place. This method will
	 * <strong>only</strong> be called on a "post back" request that is
	 * processing a form submit. Override this method to allocate resources that
	 * will be required in your event handlers.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void preprocess() {
		// TODO Auto-generated method stub
		super.preprocess();
		index = AUCTION_DETAILS_INDEX;
	}

	/**
	 * <p>
	 * Callback method that is called just before rendering takes place. This
	 * method will <strong>only</strong> be called for the page that will
	 * actually be rendered (and not, for example, on a page that handled a post
	 * back and then navigated to a different page). Override this method to
	 * allocate resources that will be required for rendering this page.
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void prerender() {
		super.prerender();

		String selectedTab = (String) viewMap.get("selectedTab");
		if(StringHelper.isNotEmpty(selectedTab))
			tabPanel.setSelectedTab(selectedTab);
		viewMap.remove("selectedTab");
		
		if (viewMap.get("TAB_INDEX") != null)
			index = Integer.parseInt(viewMap.get("TAB_INDEX").toString());
		else
			index = AUCTION_DETAILS_INDEX;

		showResultMessage();

		AuctionVenueView auctionVenueView = (AuctionVenueView) viewMap.get("auctionVenueView");
		
		if (auctionVenueView != null)
			setAuctionVenueName(auctionVenueView.getVenueName());

		Integer methodCallCount = (Integer)viewMap.get("methodCallCount");
		int count = methodCallCount==null? 0:methodCallCount.intValue();
		if(count==0){
			if (isAttendanceCurrent())
				loadDataListBidder();
			else if (isUnitExclusionCurrent())
				loadUnitsToExclude();
			else if (isUnitWinnerCurrent())
				loadAuctionRequestsList();
			viewMap.put("methodCallCount",1);
		}
		else{
			viewMap.put("methodCallCount",0);
		}
	}

	/**
	 * <p>
	 * Callback method that is called after rendering is completed for this
	 * request, if <code>init()</code> was called, regardless of whether or
	 * not this was the page that was actually rendered. Override this method to
	 * release resources acquired in the <code>init()</code>,
	 * <code>preprocess()</code>, or <code>prerender()</code> methods (or
	 * acquired during execution of an event handler).
	 * </p>
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	// Getters and Setters
	public HtmlDataTable getDataTableBidder() {
		return dataTableBidder;
	}

	public void setDataTableBidder(HtmlDataTable dataTableBidder) {
		this.dataTableBidder = dataTableBidder;
	}

	public BidderAttendanceView getDataItemBidder() {
		return dataItemBidder;
	}

	public void setDataItemBidder(BidderAttendanceView dataItemBidder) {
		this.dataItemBidder = dataItemBidder;
	}

	public List<BidderAttendanceView> getDataListBidder() {
		if(viewMap.containsKey("SELECTED_BIDDER") && viewMap.get("SELECTED_BIDDER")!=null )
			dataListBidder = (ArrayList<BidderAttendanceView>) viewMap.get("SELECTED_BIDDER");
		
		else if (viewMap.containsKey(BIDDER_ATTENDANCE))
			dataListBidder = (ArrayList<BidderAttendanceView>) viewMap.get(BIDDER_ATTENDANCE);

		return dataListBidder;
	}

	public void setDataListBidder(List<BidderAttendanceView> dataListBidder) {
		this.dataListBidder = dataListBidder;
	}

	public HtmlDataTable getDataTableUnitsToExclude() {
		return dataTableUnitsToExclude;
	}

	public void setDataTableUnitsToExclude(HtmlDataTable dataTableUnitsToExclude) {
		this.dataTableUnitsToExclude = dataTableUnitsToExclude;
	}

	public AuctionRequestRegView getDataItemUnitsToExclude() {
		return dataItemUnitsToExclude;
	}

	public void setDataItemUnitsToExclude(
			AuctionRequestRegView dataItemUnitsToExclude) {
		this.dataItemUnitsToExclude = dataItemUnitsToExclude;
	}

	public List<AuctionRequestRegView> getDataListUnitsToExclude() {
		return dataListUnitsToExclude;
	}

	public void setDataListUnitsToExclude(
			List<AuctionRequestRegView> dataListUnitsToExclude) {
		this.dataListUnitsToExclude = dataListUnitsToExclude;
	}

	public String getHiddenAttendanceIds() {
		return hiddenAttendanceIds;
	}

	public void setHiddenAttendanceIds(String hiddenAttendanceIds) {
		this.hiddenAttendanceIds = hiddenAttendanceIds;
	}

	public String getHiddenUnitsToExclude() {
		return hiddenUnitsToExclude;
	}

	public void setHiddenUnitsToExclude(String hiddenUnitsToExclude) {
		this.hiddenUnitsToExclude = hiddenUnitsToExclude;
	}

	public String getAuctionId() {
		Object obj = viewMap.get(WebConstants.AUCTION_ID);
		if (obj != null)
			auctionId = obj.toString().trim();

		return auctionId;
	}

	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}

	public String getAuctionNo() {
		return auctionNo;
	}

	public void setAuctionNo(String auctionNo) {
		this.auctionNo = auctionNo;
	}

	public Date getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(Date auctionDate) {
		this.auctionDate = auctionDate;
	}

	public String getAuctionTitle() {
		return auctionTitle;
	}

	public void setAuctionTitle(String auctionTitle) {
		this.auctionTitle = auctionTitle;
	}

	public String getAuctionVenueName() {
		return auctionVenueName;
	}

	public void setAuctionVenueName(String auctionVenueName) {
		this.auctionVenueName = auctionVenueName;
	}

	public HtmlDataTable getDataTableAuctionUnitRequests() {
		return dataTableAuctionUnitRequests;
	}

	public void setDataTableAuctionUnitRequests(
			HtmlDataTable dataTableAuctionUnitRequests) {
		this.dataTableAuctionUnitRequests = dataTableAuctionUnitRequests;
	}

	public List<AuctionWinnersView> getDataListAuctionUnitRequests() {
		// loadAuctionRequestsList();
		return dataListAuctionUnitRequests;
	}

	public void setDataListAuctionUnitRequests(
			List<AuctionWinnersView> dataListAuctionUnitRequests) {
		this.dataListAuctionUnitRequests = dataListAuctionUnitRequests;
	}

	public AuctionWinnersView getDataItemAuctionUnitRequests() {
		return dataItemAuctionUnitRequests;
	}

	public void setDataItemAuctionUnitRequests(
			AuctionWinnersView dataItemAuctionUnitRequests) {
		this.dataItemAuctionUnitRequests = dataItemAuctionUnitRequests;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getMessages() {
		String messageList;
		if ((messages == null) || (messages.size() == 0)) {
			messageList = "";
		} else {
			messageList = "<B><UL>\n";
			for (String message : messages) {
				messageList = messageList + message + "<BR>";
			}
			messageList = messageList + "</UL></B></FONT>\n";
		}
		return (messageList);
	}

	public String getHdnResultPopupCalled() {
		if (hdnResultPopupCalled == null)
			hdnResultPopupCalled = "N";
		return hdnResultPopupCalled;
	}

	public void setHdnResultPopupCalled(String hdnResultPopupCalled) {
		this.hdnResultPopupCalled = hdnResultPopupCalled;
	}

	// public Boolean getAttendanceTabVisible() {
	// if(attendanceTabVisible == null)
	// return true;
	// return attendanceTabVisible;
	// }
	//
	// public void setAttendanceTabVisible(Boolean attendanceTabVisible) {
	// this.attendanceTabVisible = attendanceTabVisible;
	// }
	//
	// public Boolean getUnitExclusionTabVisible() {
	// if(unitExclusionTabVisible == null)
	// return false;
	// return unitExclusionTabVisible;
	// }
	//
	// public void setUnitExclusionTabVisible(Boolean unitExclusionTabVisible) {
	// this.unitExclusionTabVisible = unitExclusionTabVisible;
	// }
	//
	// public Boolean getMarkWinnerTabVisible() {
	// if(markWinnerTabVisible == null)
	// return false;
	// return markWinnerTabVisible;
	// }
	//
	// public void setMarkWinnerTabVisible(Boolean markWinnerTabVisible) {
	// this.markWinnerTabVisible = markWinnerTabVisible;
	// }

	// Methods used for Tabs
	public void auctionDetailsAction() {
		index = AUCTION_DETAILS_INDEX;
		viewMap.put("TAB_INDEX",
				index);
	}

	public void attendanceAction() {
		index = ATTENDANCE_INDEX;
		viewMap.put("TAB_INDEX",
				index);
		// toggleWinnerDiv(HIDE_WINNER_DIV);
	}

	public void unitExclusionAction() {
		index = UNIT_EXCLUSION_INDEX;
		viewMap.put("TAB_INDEX",
				index);
		// toggleWinnerDiv(HIDE_WINNER_DIV);
	}

	public void auctionWinnersAction() {
		index = UNIT_WINNER_INDEX;
		viewMap.put("TAB_INDEX",
				index);
		// toggleWinnerDiv(SHOW_WINNER_DIV);
	}

	public void attachmentsAction() {
		index = ATTACHMENTS_INDEX;
		viewMap.put("TAB_INDEX",
				index);
	}

	public void notesAction() {
		index = NOTES_INDEX;
		viewMap.put("TAB_INDEX",
				index);
	}

	public void attendanceTabClicked(ActionEvent ae) {
		attendanceAction();
	}

	public void unitExclusionTabClicked(ActionEvent ae) {
		unitExclusionAction();
	}

	public void unitWinnerTabClicked(ActionEvent ae) {
		auctionWinnersAction();
	}

	public String getAttendanceStyle() {
		return getCSS(ATTENDANCE_INDEX);
	}

	public String getUnitExclusionStyle() {
		return getCSS(UNIT_EXCLUSION_INDEX);
	}

	public String getAuctionWinnersStyle() {
		return getCSS(UNIT_WINNER_INDEX);
	}

	private String getCSS(int forIndex) {
		return forIndex == index ? "tabbedPaneTextSelected" : "tabbedPaneText";
	}

	public boolean isAuctionDetailsCurrent() {
		return index == AUCTION_DETAILS_INDEX;
	}

	public boolean isAttendanceCurrent() {
		return index == ATTENDANCE_INDEX;
	}

	public boolean isUnitExclusionCurrent() {
		return index == UNIT_EXCLUSION_INDEX;
	}

	public boolean isUnitWinnerCurrent() {
		return index == UNIT_WINNER_INDEX;
	}

	public boolean isDocumentsCurrent() {
		return index == ATTACHMENTS_INDEX;
	}

	public boolean isNotesCurrent() {
		return index == NOTES_INDEX;
	}

	public boolean isSaveEnable() {
		boolean returnVal = false;
		if (index == AUCTION_DETAILS_INDEX)
			returnVal = true;
		else if (index == ATTENDANCE_INDEX)
			returnVal = true;
		else if (index == UNIT_EXCLUSION_INDEX)
			returnVal = true;
		else
			returnVal = false;
		return returnVal;
	}

	public boolean isSaveDocNNotes() {
		boolean returnVal = false;
		if (index == ATTACHMENTS_INDEX)
			returnVal = true;
		else if (index == NOTES_INDEX)
			returnVal = true;
		else
			returnVal = false;
		return returnVal;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getWinnerDivStyle() {
		return winnerDivStyle;
	}

	public void setWinnerDivStyle(String winnerDivStyle) {
		this.winnerDivStyle = winnerDivStyle;
	}

	public Boolean getIsArabicLocale()
	{
		return !getIsEnglishLocale();
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}

	// public ResourceBundle getBundle() {
	// if(getIsArabicLocale())
	// bundle =
	// ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages_ar");
	// else
	// bundle =
	// ResourceBundle.getBundle("com.avanza.pims.web.messageresource.Messages");
	// return bundle;
	// }
	//
	// public void setBundle(ResourceBundle bundle) {
	// this.bundle = bundle;
	// }

	public void saveComments(Long auctionId) throws Exception {
		String methodName = "saveComments|";
		try {
			logger.logInfo(methodName + "started...");

			String notesOwner = WebConstants.AUCTION;
			NotesController.saveNotes(notesOwner, auctionId);

			logger.logInfo(methodName + "completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
	}

	public Boolean saveAttachments(Long referenceId) {
		Boolean success = false;
		try {
			logger.logInfo("saveAtttachments started...");
			if (referenceId != null)
				success = CommonUtil.updateDocuments();
			logger.logInfo("saveAtttachments completed successfully!!!");
		} catch (Throwable throwable) {
			success = false;
			logger.LogException("saveAtttachments crashed ", throwable);
		}

		return success;
	}

	// public void saveAttachments(Long auctionId) throws Exception {
	// String methodName = "saveAtttachments|";
	// try {
	// logger.logInfo(methodName + "started...");
	//
	// String repositoryId = "pimsDb";
	// String fileSystemRepository =
	// WebConstants.FILE_SYSTEM_REPOSITORY_AUCTION;
	// String entityId = auctionId.toString();
	// AttachmentController.saveAttachments(repositoryId,
	// fileSystemRepository, getLoggedInUser(), entityId);
	//
	// logger.logInfo(methodName + "completed successfully!!!");
	// } catch (Exception exception) {
	// logger.LogException(methodName + "crashed ", exception);
	// throw exception;
	// }
	// }

	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments() {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,
				WebConstants.PROCEDURE_TYPE_PREPARE_AUCTION);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_AUCTION;

		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.AUCTION);

		if (viewMap.containsKey(WebConstants.AUCTION_ID)) {
			String entityId = (String) viewMap.get(WebConstants.AUCTION_ID);

			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	// @SuppressWarnings("unchecked")
	// public void loadAttachmentsAndComments(Long auctionId) throws Exception {
	// String methodName = "loadAttachmentsAndComments|";
	// try {
	// logger.logInfo(methodName + "started...");
	// String repositoryId = "pimsDb";
	// String fileSystemRepository =
	// WebConstants.FILE_SYSTEM_REPOSITORY_AUCTION;
	// String user = getLoggedInUser();
	// String entityId = auctionId.toString();
	// Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
	// .getAttributes();
	// viewMap.put("repositoryId", repositoryId);
	// viewMap.put("fsRepositoryId", fileSystemRepository);
	// viewMap.put("externalId", user);
	// viewMap.put("associatedObjectId", entityId);
	// viewMap.put("noteowner", WebConstants.AUCTION);
	// viewMap.put("entityId", entityId);
	// logger.logInfo(methodName + "completed successfully!!!");
	// } catch (Exception exception) {
	// logger.LogException(methodName + "crashed ", exception);
	// throw exception;
	// }
	// }

	public String getDateFormat() {
		WebContext webContext = ApplicationContext.getContext().get(
				WebContext.class);
		LocaleInfo localeInfo = webContext
				.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
	}

	public String getLocale() {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}

	// private String getLoggedInUser() {
	// FacesContext context = FacesContext.getCurrentInstance();
	// HttpSession session = (HttpSession) context.getExternalContext()
	// .getSession(true);
	// String loggedInUser = "";
	//
	// if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
	// UserDbImpl user = (UserDbImpl) session
	// .getAttribute(WebConstants.USER_IN_SESSION);
	// loggedInUser = user.getLoginId();
	// }
	//
	// return loggedInUser;
	// }

	@SuppressWarnings("unchecked")
	public void selectVenue(javax.faces.event.ActionEvent event) {
		logger.logInfo("selectVenue() started...");
		try {
			final String viewId = "/NewConductAuction.jsp";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ViewHandler viewHandler = facesContext.getApplication()
					.getViewHandler();
			String javaScriptText = "javascript:showVenuePopup();";

			// Add the Javascript to the rendered page's header for immediate
			// execution
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javaScriptText);
			logger.logInfo("selectVenue() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("selectVenue() crashed ", exception);
		}
	}

	public String saveDocuments() {
		String METHOD_NAME = "saveDocuments()";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			saveAttachments(Long.parseLong(getAuctionId()));
			// writeMessage(getBundle().getString(MessageConstants.CommonMessage.DOCUMENTS_SAVED));
			writeMessage(CommonUtil.getBundleMessage(
					MessageConstants.CommonMessage.DOCUMENTS_SAVED), false);
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
		return "";
	}

	public String saveNotes() {
		String METHOD_NAME = "saveNotes()";
		logger.logInfo(METHOD_NAME + " started...");
		try {
			saveComments(Long.parseLong(getAuctionId()));
			// writeMessage(getBundle().getString(MessageConstants.CommonMessage.NOTES_SAVED));
			writeMessage(CommonUtil.getBundleMessage(
					MessageConstants.CommonMessage.NOTES_SAVED), false);
			logger.logInfo(METHOD_NAME + " completed successfully...");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " crashed...", exception);
			exception.printStackTrace();
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public String approveRefund() 
	{
		logger.logInfo("approveRefund() started...");
		UserTask userTask = null;
		String success = "false";
		Long auctionId = 0L;
		try {
				if(viewMap.containsKey(WebConstants.AUCTION_ID))			
					auctionId = Long.parseLong((String)viewMap.get(WebConstants.AUCTION_ID));
				
				saveAttachments(auctionId);
				saveComments(auctionId);
		        
				String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
		        logger.logInfo("Contextpath is: " + contextPath);
		        String loggedInUser = getLoggedInUser();
		        if(viewMap.containsKey(WebConstants.AuctionRefund.TASK_LIST_USER_TASK))	
					userTask = (UserTask) viewMap.get(WebConstants.AuctionRefund.TASK_LIST_USER_TASK);
				BPMWorklistClient client = new BPMWorklistClient(contextPath);
				logger.logInfo("UserTask is: " + userTask.getTaskType());
				if(userTask!=null){
					client.completeTask(userTask, loggedInUser, TaskOutcome.APPROVE);
//					saveSystemComments(MessageConstants.RequestEvents.AUCTION_APPROVED);
				}
				
				writeMessage(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_REFUND_APPROVE_SUCCESS), false);
				NotesController.saveSystemNotesForRequest(WebConstants.AUCTION, MessageConstants.AuctionRefund.MSG_REFUND_APPROVE_SUCCESS ,Long.parseLong(getAuctionId()));
				viewMap.put("isViewMode", true);
				viewMap.put("canAddAttachment",false);
				viewMap.put("canAddNote", false);
			/*}*/
			logger.logInfo("approveRefund() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("approveRefund() crashed ", exception);
			writeMessage(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_REFUND_APPROVE_FAILURE),true);
		}
		return "approveRefund";
	}

	public String cancel() {
		logger.logInfo("cancel() started...");
		String backScreen = "home";
		try {
				backScreen = (String) viewMap.get(WebConstants.BACK_SCREEN);
				if(backScreen==null)
					backScreen = "home";
		        logger.logInfo("cancel() completed successfully!!!");
			}
		catch (Exception exception) {
			logger.LogException("cancel() crashed ", exception);
		}
		return backScreen;
    }
	
	@SuppressWarnings("unchecked")
	public String rejectRefund() {
		logger.logInfo("rejectRefund() started...");
		UserTask userTask = null;
		String success = "false";
		Long auctionId = 0L;
		try {
			if(viewMap.containsKey(WebConstants.AUCTION_ID))			
				auctionId = (Long)viewMap.get(WebConstants.AUCTION_ID);
			saveAttachments(auctionId);
			saveComments(auctionId);
			
			String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
	        logger.logInfo("Contextpath is: " + contextPath);
	        String loggedInUser = getLoggedInUser();
	        if(viewMap.containsKey(WebConstants.AuctionRefund.TASK_LIST_USER_TASK))	
				userTask = (UserTask) viewMap.get(WebConstants.AuctionRefund.TASK_LIST_USER_TASK);
			BPMWorklistClient client = new BPMWorklistClient(contextPath);
			 logger.logInfo("UserTask is: " + userTask.getTaskType());
			if(userTask!=null){
				client.completeTask(userTask, loggedInUser, TaskOutcome.REJECT);
//				saveSystemComments(MessageConstants.RequestEvents.AUCTION_REJECTED);
			}
//			errorMessages.clear();
			writeMessage(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_REFUND_REJECT_SUCCESS),false);
			viewMap.put("canAddAttachment",false);
			viewMap.put("canAddNote", false);

			logger.logInfo("rejectRefund() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("rejectRefund() crashed ", exception);
			writeMessage(CommonUtil.getBundleMessage(MessageConstants.AuctionRefund.MSG_REFUND_REJECT_FAILURE),true);
		}
		return "rejectRefund";
	}
	
	private void writeMessage(String message) {
		if (messages == null)
			messages = new ArrayList<String>();
		messages.add(message);
	}
	
	private void writeMessage(String message, Boolean isErrorMessage) {
		if(isErrorMessage){
			errorMessages.clear();
			errorMessages.add(message);
		}
		else
			infoMessage = message;
	}
    @SuppressWarnings("unchecked")
	public void selectBidder(ValueChangeEvent event) 
    {
		int index = dataTableBidder.getRowIndex();
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Object list = viewRootMap.get(BIDDER_ATTENDANCE);
		dataListBidder = (ArrayList<BidderAttendanceView>) list;
		BidderAttendanceView bidderAttendanceView = dataListBidder.remove(index);
		bidderAttendanceView.setAttendance((Boolean) event.getNewValue());
		dataListBidder.add(index, bidderAttendanceView);
		viewRootMap.put(BIDDER_ATTENDANCE, dataListBidder);
	}

	public Boolean getIsViewMode() {
		isViewMode = (Boolean) viewMap.get("isViewMode");
		if(isViewMode==null)
			isViewMode = false;
		return isViewMode;
	}

	public void setIsViewMode(Boolean isViewMode) {
		this.isViewMode = isViewMode;
	}

	public Integer getAttendanceRecordSize() {
		Object list = new Object();
		if(viewMap.containsKey("SELECTED_BIDDER") && viewMap.get("SELECTED_BIDDER")!=null )
			list =  viewMap.get("SELECTED_BIDDER");
		
		else if (viewMap.containsKey(BIDDER_ATTENDANCE))
			list =  viewMap.get(BIDDER_ATTENDANCE);
			
		return (list == null) ? 0 : ((List<BidderAttendanceView>) list).size();
	}

	public Integer getUnitExclusionRecordSize() {
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		Object list = viewRootMap.get(AUCTION_UNIT_EXCLUSION);
		return (list == null) ? 0 : ((List<AuctionRequestRegView>) list).size();
	}

	public Integer getAuctionWinnerRecordSize() {
		Map viewRootMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		Object list = viewRootMap.get(AUCTION_REQUEST_LIST);
		return (list == null) ? 0 : ((List<AuctionWinnersView>) list).size();
	}

	public String getNumberFormat() {
		return new CommonUtil().getNumberFormat();
	}

	public String getAuctionTime() {
		return auctionTime;
	}

	public void setAuctionTime(String auctionTime) {
		this.auctionTime = auctionTime;
	}

	public String getOutbiddingValue() {
		return outbiddingValue;
	}

	public void setOutbiddingValue(String outbiddingValue) {
		this.outbiddingValue = outbiddingValue;
	}

	public String getConfiscationPercentage() {
		return confiscationPercentage;
	}

	public void setConfiscationPercentage(String confiscationPercentage) {
		this.confiscationPercentage = confiscationPercentage;
	}

	public String getAuctionStatus() {
		return auctionStatus;
	}

	public void setAuctionStatus(String auctionStatus) {
		this.auctionStatus = auctionStatus;
	}

	public String getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(String publishingDate) {
		this.publishingDate = publishingDate;
	}

	public String getAuctionStartDate() {
		return auctionStartDate;
	}

	public void setAuctionStartDate(String auctionStartDate) {
		this.auctionStartDate = auctionStartDate;
	}

	public String getAuctionEndDate() {
		return auctionEndDate;
	}

	public void setAuctionEndDate(String auctionEndDate) {
		this.auctionEndDate = auctionEndDate;
	}

	public AuctionView getAuctionView() {
		return auctionView;
	}

	public void setAuctionView(AuctionView auctionView) {
		this.auctionView = auctionView;
	}

	public Boolean getIsAttendanceMode() {
		return isAttendanceMode;
	}

	public void setIsAttendanceMode(Boolean isAttendanceMode) {
		this.isAttendanceMode = isAttendanceMode;
	}
	
	public Boolean getIsRefundMode(){
		return getIsRefundAuctionFinalMode() || getIsRefundAuctionApprovalMode();
	}
	
	public Boolean getIsRefundAuctionFinalMode(){
		if(viewMap.get("isRefundAuctionFinalMode")!=null){
			return (Boolean)viewMap.get("isRefundAuctionFinalMode");
		}
		return false;
	}	
	
	public Boolean getIsRefundAuctionApprovalMode(){
		if(viewMap.get("isRefundAuctionApprovalMode")!=null){
			return (Boolean)viewMap.get("isRefundAuctionApprovalMode");
		}
		return false;
	}
	
	public Boolean getIsAuctionLoaded(){
		if(viewMap.get("isAuctionLoaded")!=null){
			return (Boolean)viewMap.get("isAuctionLoaded");
		}
		return false;
	}
	
	public String getHeading() {
		if(viewMap.get("heading")!=null){
			return (String)viewMap.get("heading");
		}
		return "";

	}
	
	public String getBidderTabHeading() {
		if(viewMap.get("bidderTabHeading")!=null){
			return (String)viewMap.get("bidderTabHeading");
		}
		return "";

	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getInfoMessage() {
		List<String> temp = new ArrayList<String>();
		if(!infoMessage.equals(""))
			temp.add(infoMessage);
		return CommonUtil.getErrorMessages(temp);
	}
 
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getExclusionReason() {
		return exclusionReason;
	}

	public void setExclusionReason(String exclusionReason) {
		this.exclusionReason = exclusionReason;
	}

	public String getSelectedBidder() {
		return selectedBidder;
	}

	public void setSelectedBidder(String selectedBidder) {
		this.selectedBidder = selectedBidder;
	}

	public List<SelectItem> getBidderList()
	{
		if(viewMap.containsKey("BIDDER_LIST") && viewMap.get("BIDDER_LIST")!=null)
		{
			bidderList = (List<SelectItem>) viewMap.get("BIDDER_LIST");
			Collections.sort(bidderList , ListComparator.LIST_COMPARE);
		}
		return bidderList;
	}

	public void setBidderList(List<SelectItem> bidderList) {
		this.bidderList = bidderList;
	}
	public void onBidderChange()
	
	{
		String selectedBiderName = selectedBidder;
		List<BidderAttendanceView> allBidders =  new ArrayList<BidderAttendanceView>();
		List<BidderAttendanceView> selectBidder =  new ArrayList<BidderAttendanceView>();
		if(viewMap.containsKey("ALL_BIDDER_ATTENDANCE") && viewMap.get("ALL_BIDDER_ATTENDANCE") != null)
			allBidders = (List<BidderAttendanceView>) viewMap.get("ALL_BIDDER_ATTENDANCE");
		
			if(allBidders !=null && allBidders.size()>0)
			{	
				for(BidderAttendanceView bidderFilter : allBidders)
				{
					if(bidderFilter.getName().compareTo(selectedBiderName) == 0)
					{
						selectBidder.clear();
						bidderFilter.setAttendance(false);
						selectBidder.add(bidderFilter);
						viewMap.put("SELECTED_BIDDER",selectBidder);
						getDataListBidder();
						getAttendanceRecordSize();
						selectAll.setSelected(false);
						break;
					}
					
				}
			} 
	}
	public void showAllBidder()
	{
		if(viewMap.containsKey("ALL_BIDDER_ATTENDANCE") && viewMap.get("ALL_BIDDER_ATTENDANCE") != null)
		{
			viewMap.put(BIDDER_ATTENDANCE,viewMap.get("ALL_BIDDER_ATTENDANCE"));
			if(viewMap.containsKey("SELECTED_BIDDER"))
				viewMap.remove("SELECTED_BIDDER");
			getDataListBidder();
			selectedBidder="-1";
			getSelectedBidder();
		}
	}

	public HtmlSelectBooleanCheckbox getSelectAll() {
		return selectAll;
	}

	public void setSelectAll(HtmlSelectBooleanCheckbox selectAll) {
		this.selectAll = selectAll;
	}
	
	public void tabRequestHistory_Click() { 
		String methodName="tabRequestHistory_Click";
		logger.logInfo(methodName+"|"+"Start..");
		Long auctionId = 0L;
		try	
		{
		  RequestHistoryController rhc=new RequestHistoryController();
		  if(viewMap.containsKey(WebConstants.AUCTION_ID)) {	
			  auctionId = new Long((String) viewMap.get(WebConstants.AUCTION_ID));
			  
		  }
		  if(auctionId!=null && auctionId!=0L) {
			  rhc.getAllRequestTasksForRequest(WebConstants.AUCTION, auctionId.toString());
		  }  
		}
		catch(Exception ex)
		{
			logger.LogException(methodName+"|"+"Error Occured..",ex);
		}
		logger.logInfo(methodName+"|"+"Finish..");
	 }
}

