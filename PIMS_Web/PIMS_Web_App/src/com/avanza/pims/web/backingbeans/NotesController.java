package com.avanza.pims.web.backingbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.RequestContext;
import com.avanza.core.web.ViewContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.context.RequestContextImpl;
import com.avanza.core.web.context.WebContextImpl;
import com.avanza.core.web.context.impl.ViewContextImpl;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.ui.util.ResourceUtil;

public class NotesController extends AbstractController {

	private static Logger logger = Logger.getLogger(NotesController.class);
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private static final String NOTES_LIST_KEY = "NotesListData";
	private static final String NOTES_DATA = "NotesData";
	
	private static PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
	private static NotesServiceAgent notesServiceAgent = new NotesServiceAgent();
	
	private static final String INTERNAL_NOTE_TYPE = "Internal";
	private static final String USER_NOTE_TYPE = "User";
	private static final String SYSTEM_NOTE_TYPE = "System";
	private static final String NOTE_TYPE = "NOTE_TYPE";
	private static final String SYSTEM_NOTE_DESC="SYSTEM_NOTE_DESC";
	private List<NotesBean> notesList = new ArrayList<NotesBean>();
	private HtmlDataTable dataTable;
	private NotesBean notes;
	
	public List<NotesBean> getNotesDisplayList(){
		
		List<NotesBean> list = new ArrayList<NotesBean>();
		for (NotesBean nb : getNotesList()){
			if (!nb.isDeleted())
				list.add(nb);
		}
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		viewMap.put("recordSize",list.size());
		return list;
	}
	
	public List<NotesBean> getNotesList() {
		try
		{
		ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
		this.notesList = view.getAttribute(NOTES_LIST_KEY);
		
		if (this.notesList == null) {
			
			Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			String noteowner = (String) requestMap.get("noteowner");
			String arg = (String) requestMap.get("entityId");
			Long entityId = Long.parseLong(arg == null?"0":arg);
			
			this.notesList = new ArrayList<NotesBean>();

			if (StringHelper.isEmpty(noteowner) || entityId == 0){
				Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
				noteowner = (String) viewMap.get("noteowner");
				arg = (String) viewMap.get("entityId");
				entityId = Long.parseLong(arg == null?"0":arg);
				
				if (StringHelper.isEmpty(noteowner) || entityId == 0)
					return notesList;
			} else {
				view.setAttribute("noteowner", noteowner);
				view.setAttribute("entityId", arg);
			}

			List<NotesVO> notes = notesServiceAgent.getComments(noteowner, entityId);
			for(NotesVO note : notes){
				this.notesList.add(new NotesBean( (long)this.notesList.size(), note.getNote(), 
						                          note.getNoteowner(), note.getEntityId(), note.getNoteTypeId(), 
						                          note.getCreatedBy(), note.getCreatedOn(), note,
						                          note.getSystemNoteEn(),note.getSystemNoteAr(),
						                          getDateFormat(),note.getCreatedByFullName(),
							                      note.getCreatedByFullNameAr(),note.getUpdatedByFullName(),
							                      note.getUpdatedByFullNameAr()
						                         )
				                   );
			}
			
			view.setAttribute(NOTES_LIST_KEY, this.notesList);
		}
		}
		catch (Exception e) {
                logger.LogException("getNotesList|Error Occured", e);
		}
		return notesList;
	}
	@SuppressWarnings( "unchecked" )
    public List<NotesBean> getNotesList(String NotesOwner,String EntityId) 
    {
		
    	List<NotesBean> notesList = new ArrayList<NotesBean>();
		try
		{
			SystemParameters parameters = SystemParameters.getInstance();			
			String dateFormat = parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
			String arg = EntityId;
			Long entityId = Long.parseLong(arg == null?"0":arg );
			String notesOwner = NotesOwner;
			List<NotesVO> notes = notesServiceAgent.getSystemComments(notesOwner, entityId);
			for(NotesVO note : notes)
			{
				notesList.add(new NotesBean( (long)notesList.size(), note.getNote(), note.getNoteowner(), 
						                        note.getEntityId(), note.getNoteTypeId(), note.getCreatedBy(), 
						                        note.getCreatedOn(), note,note.getSystemNoteEn(),
						                        note.getSystemNoteAr(),dateFormat,note.getCreatedByFullName(),
						                        note.getCreatedByFullNameAr(),note.getUpdatedByFullName(),
						                        note.getUpdatedByFullNameAr()
				                               )
				                );
			}
			
		
		}
		catch (Exception e) {
                logger.LogException("getNotesList|Error Occured", e);
		}
		return notesList;
	}
	
	@SuppressWarnings( "unchecked" )
    public List<NotesBean> getSystemNotesListForRequest(String NotesOwner,String EntityId) {
		
    	List<NotesBean> sysNotesList = new ArrayList<NotesBean>();
		try
		{
			SystemParameters parameters = SystemParameters.getInstance();			
			String dateFormat = parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
		    ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
			Map requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
				
			String arg = EntityId;
			Long entityId = Long.parseLong(arg == null?"0":arg );
			
			String notesOwner = NotesOwner;
			sysNotesList = new ArrayList<NotesBean>();

			if (entityId == 0){
				Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			
				arg = (String) viewMap.get("entityId");
				entityId = Long.parseLong(arg == null?"0":arg);
				
				if (entityId == 0)
					return sysNotesList;
			} else {
				view.setAttribute("noteowner", notesOwner);
				view.setAttribute("entityId", arg);
			}

			List<NotesVO> notes = notesServiceAgent.getSystemComments(notesOwner, entityId);
			for(NotesVO note : notes)
			{
				sysNotesList.add(new NotesBean( (long)sysNotesList.size(), note.getNote(), note.getNoteowner(), 
						                        note.getEntityId(), note.getNoteTypeId(), note.getCreatedBy(), 
						                        note.getCreatedOn(), note,note.getSystemNoteEn(),
						                        note.getSystemNoteAr(),dateFormat,note.getCreatedByFullName(),
						                        note.getCreatedByFullNameAr(),note.getUpdatedByFullName(),
						                        note.getUpdatedByFullNameAr()
				                               )
				                );
			}
			getFacesContext().getViewRoot().getAttributes().put(WebConstants.LIST_REQUEST_HISTORY, sysNotesList);
		
		}
		catch (Exception e) {
                logger.LogException("getNotesList|Error Occured", e);
		}
		return sysNotesList;
	}
	
	public void addNotes(ActionEvent evt){
		
		try
		{
			
			ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
			if (StringHelper.isEmpty(this.getNotes().getDescription()))
				return;
			
			Date createdOn = new Date();
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			String loginId = ((User) ctx.getAttribute(CoreConstants.CurrentUser)).getLoginId();
	
			NotesBean notesBean = this.getNotes();
			notesBean.setCreatedBy(loginId);
			notesBean.setCreatedOn(createdOn);
			this.getNotesList().add(notesBean);
			view.setAttribute(NOTES_LIST_KEY, this.notesList);
			this.notes = new NotesBean();
			this.notes.setNotesId((long) this.notesList.size());
			view.setAttribute(NOTES_DATA, this.notes);
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			if( sessionMap.get("noteowner") != null && sessionMap.get("noteowner").toString().trim().length() >0   &&
				sessionMap.get("entityId") != null  && sessionMap.get("entityId").toString().trim().length() >0   
			  )
			{
				
				   saveNotes(  sessionMap.remove("notesOwner").toString(), 
						       Long.parseLong( sessionMap.remove("entityId").toString() ) 
						    );
			}
		}
		catch (Exception e)
		{
			logger.LogException("addNotes|Error", e );
	
		}
		
		
	}
	
	public void addNotes()throws Exception{
		ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
		if (StringHelper.isEmpty(this.getNotes().getDescription()))
			return;
		
		Date createdOn = new Date();
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		User user  = (User) ctx.getAttribute(CoreConstants.CurrentUser);

		NotesBean notesBean = this.getNotes();
		notesBean.setCreatedBy( user.getLoginId() );
		notesBean.setCreatedOn(createdOn);
		notesBean.setCreatedByFullName( user.getFullName()  );
		notesBean.setCreatedByFullNameAr( user.getSecondaryFullName() );
		
		this.getNotesList().add(notesBean);
		view.setAttribute(NOTES_LIST_KEY, this.notesList);
		this.notes = new NotesBean();
		this.notes.setNotesId((long) this.notesList.size());
		view.setAttribute(NOTES_DATA, this.notes);
		Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
		if(sessionMap.get("noteowner") != null && sessionMap.get("noteowner").toString().trim().length() >0   &&
		   sessionMap.get("entityId") != null  && sessionMap.get("entityId").toString().trim().length() >0   
		   )
		{
			{
			   saveNotes(  sessionMap.remove("noteowner").toString(), 
					       Long.parseLong( sessionMap.remove("entityId").toString() ) 
					    );
			}
		}
	}
	
	public void removeNotes(ActionEvent evt){
		
		NotesBean note = (NotesBean) this.dataTable.getRowData();
		for(NotesBean n : this.getNotesList()) {
			if (n.getNotesId().longValue() == note.getNotesId().longValue()) {
				n.setDeleted(true);
			}
		}
	}
	
	/**
     * Opens a popup which adds a new note
     * @param
     * @throws PimsBusinessException
     * @author Syed Hammad Ahmed
     */
	@SuppressWarnings("unchecked")
	public void openAddNotePopup(javax.faces.event.ActionEvent event){
		logger.logInfo("openAddNotePopup() started...");
		try
		{
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			
			String noteOwner = (String)viewMap.get("noteowner");
			if(noteOwner==null)
				noteOwner = "";
			sessionMap.put("noteowner", noteOwner);
			
			String entityId = (String)viewMap.get("entityId");
			if(entityId==null)
				entityId = "";
			sessionMap.put("entityId", entityId);
			
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        String javaScriptText = "javascript:showAddNotePopup();";
	
	        // Add the Javascript to the rendered page's header for immediate execution
	        AddResource addResource = AddResourceFactory.getInstance(facesContext);
	        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
	        logger.logInfo("openAddNotePopup() completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("openAddNotePopup() crashed ", exception);
		}
    }
	
	public static void saveNotes(String notesOwner, Long entityId) throws PimsBusinessException {
		
		ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		try
		{	
			User user = (User) ctx.getAttribute(CoreConstants.CurrentUser);
		Date createdOn = new Date();
		List<NotesBean> notesList = view.getAttribute(NOTES_LIST_KEY);
		if (notesList == null)
			return;
		for(NotesBean note : notesList){
			if(note.getNote() == null && !note.isDeleted()) {
				NotesVO newnote = new NotesVO();
				newnote.setNoteowner(notesOwner);
				newnote.setNoteTypeId(21002l);
				newnote.setEntityId(entityId);
				newnote.setNote(note.getDescription());
				newnote.setDeleted(false);
				newnote.setRecordStatus(1);
				newnote.setCreatedBy( user.getLoginId() );
				newnote.setUpdatedBy( user.getLoginId() );
				newnote.setCreatedOn(createdOn);
				newnote.setUpdatedOn(createdOn);
				newnote.setCreatedByFullName( note.getCreatedByFullName() );
				newnote.setCreatedByFullNameAr(   note.getCreatedByFullNameAr()  );
				newnote.setUpdatedByFullName(  user.getFullName());
				newnote.setUpdatedByFullNameAr(  user.getSecondaryFullName() );
				note.setNote(newnote);
				notesServiceAgent.addNote(note.getNote());
			} else if (note.isDeleted() && note.getNote() != null){
				note.getNote().setDeleted(true);
				note.getNote().setUpdatedBy( user.getLoginId() );
				note.getNote().setUpdatedOn(createdOn);
				notesServiceAgent.updateNote(note.getNote());
			}
		}
		
		notesList = new ArrayList<NotesBean>();
		List<NotesVO> notes = notesServiceAgent.getComments(notesOwner, entityId);
		SystemParameters parameters = SystemParameters.getInstance();
        String dateFormat = parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
        for(NotesVO note : notes){
			notesList.add(new NotesBean( (long)notesList.size(), note.getNote(), note.getNoteowner(), 
					                    note.getEntityId(), note.getNoteTypeId(), note.getCreatedBy(), 
					                    note.getCreatedOn(), note,note.getSystemNoteEn(),
					                    note.getSystemNoteAr(),dateFormat,note.getCreatedByFullName(),
					                    note.getCreatedByFullNameAr(),note.getUpdatedByFullName(),
					                    note.getUpdatedByFullNameAr()
					                   )
                          );
        }
		view.setAttribute(NOTES_LIST_KEY, notesList);
	}
	catch (Exception e) {
            logger.LogException("saveNotes|Error Occured", e);
//            throw new PimsBusinessException(e);
	}
	}
	public static void saveSystemNotesForRequest(String notesOwner, String sysMsgKey, Long entityId,Object... placeHolderVals) throws PimsBusinessException 
	{
		Locale localeAr = new Locale("ar");
		Locale localeEn = new Locale("en");
		String msgEn = "";
		String msgAr = "";
		if(FacesContext.getCurrentInstance() != null )
		{
			ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
			msgAr = bundleAr.getString(sysMsgKey);
			ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
			msgEn = bundleEn.getString(sysMsgKey);
		}
		else
		{
			msgAr = ResourceUtil.getInstance().getProperty(sysMsgKey );
			msgEn = ResourceUtil.getInstance().getProperty(sysMsgKey );
		}
		
		if( placeHolderVals != null )
		{
			msgEn = java.text.MessageFormat.format(msgEn, placeHolderVals);
			msgAr = java.text.MessageFormat.format(msgAr, placeHolderVals);
		}
		saveSystemNotesForRequest( notesOwner, msgEn, msgAr, entityId);
		
	}
    public static void saveSystemNotesForRequest(String notesOwner, String sysMsgEn, String sysMsgAr, Long entityId) throws PimsBusinessException 
    {
		
		ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		List<NotesBean> notesList = new ArrayList<NotesBean>(0);
		if(view!= null && view.getAttribute(WebConstants.LIST_REQUEST_HISTORY)!=null)
		    notesList = view.getAttribute(WebConstants.LIST_REQUEST_HISTORY);
		try 
		{
			
			User user = (User) ctx.getAttribute(CoreConstants.CurrentUser);
			Date createdOn = new Date();
			NotesVO newnote = new NotesVO();
			newnote.setNoteowner(notesOwner);
			newnote.setNoteTypeId(21001l);
			newnote.setEntityId(entityId);
			newnote.setNote("");
			newnote.setDeleted(false);
			newnote.setRecordStatus(1);
			newnote.setCreatedBy( user.getLoginId() );
			newnote.setUpdatedBy( user.getLoginId() );
			newnote.setCreatedOn(createdOn);
			newnote.setUpdatedOn(createdOn);
			newnote.setCreatedByFullName(  user.getFullName());
			newnote.setCreatedByFullNameAr(  user.getSecondaryFullName() );
			newnote.setUpdatedByFullName(  user.getFullName());
			newnote.setUpdatedByFullNameAr(  user.getSecondaryFullName() );
			newnote.setSystemNoteEn( sysMsgEn );
			newnote.setSystemNoteAr( sysMsgAr );
			notesServiceAgent.addNote(newnote);
		    notesList = new ArrayList<NotesBean>();
	        List<NotesVO> notes = notesServiceAgent.getSystemComments(notesOwner, entityId);
	        SystemParameters parameters = SystemParameters.getInstance();
	        String dateFormat = parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
	        for(NotesVO note : notes)
	        {
			 notesList.add( new NotesBean( (long)notesList.size(), note.getNote(), note.getNoteowner(), 
					                      note.getEntityId(), note.getNoteTypeId(), note.getCreatedBy(), 
					                      note.getCreatedOn(), note,note.getSystemNoteEn(),
					                      note.getSystemNoteAr(),dateFormat,note.getCreatedByFullName(),
					                      note.getCreatedByFullNameAr(),note.getUpdatedByFullName(),
					                      note.getUpdatedByFullNameAr()
					                     )
	                        );
		    }
		    FacesContext.getCurrentInstance().getViewRoot().getAttributes().put(WebConstants.LIST_REQUEST_HISTORY, notesList);
    }
	catch (Exception e) 
	{
            logger.LogException("saveSystemNotesForRequest|Error Occured", e);
	}
	}
    public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return  localeInfo.getDateFormat();
	}
    
    public String getLongDateFormat(){
    	SystemParameters parameters = SystemParameters.getInstance();
		String dateFormat = parameters.getParameter(WebConstants.LONG_DATE_FORMAT);
		return dateFormat;
    }
    
    public TimeZone getTimeZone()
	{
       return TimeZone.getDefault();
	}
	       
	public void setNotesList(List<NotesBean> notesList) {
		this.notesList = notesList;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public NotesBean getNotes() {
		
		ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
		if (view == null) {
			ApplicationContext.getContext().add(ViewContext.class.getName(), 
					new ViewContextImpl(FacesContext.getCurrentInstance().getViewRoot().getAttributes()));
			view = ApplicationContext.getContext().get(ViewContext.class);
		}
		this.notes = view.getAttribute(NOTES_DATA);
		
		if (this.notes == null) {
			this.notes = new NotesBean();
			view.setAttribute(NOTES_DATA, this.notes);
		}
		
		return notes;
	}

	public void setNotes(NotesBean notes) {
		this.notes = notes;
	}
	
	public void testSave(ActionEvent evt) throws PimsBusinessException{
		saveNotes("notesOwner", 1000L);
	}
	
	public void init() {
		super.init();
		try
		{
			initContext();
			Map sessionMap = getFacesContext().getExternalContext().getSessionMap();
			if(sessionMap.containsKey(NOTES_DATA)){
				NotesBean notesBean = (NotesBean)sessionMap.remove(NOTES_DATA);
				
				if(notesBean!=null){
					ViewContext view = ApplicationContext.getContext().get(ViewContext.class);
					view.setAttribute(NOTES_DATA,notesBean);
					addNotes();
				}
				
			}
		}
		catch (Exception e)
		{
			logger.LogException("init|Error", e );
	
		}
	}

	private void initContext() {
		webContext = ApplicationContext.getContext().get(WebContext.class);
    	requestContext = ApplicationContext.getContext().get(RequestContext.class);
    	viewContext = ApplicationContext.getContext().get(ViewContext.class);

    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	webContext = new WebContextImpl(req.getSession());
    	requestContext = new RequestContextImpl(req);
    	if (getFacesContext().getViewRoot() != null) {
    		viewContext = new ViewContextImpl(getFacesContext().getViewRoot().getAttributes());
    		ApplicationContext.getContext().add(ViewContext.class.getName(), viewContext);
    	}
    	ApplicationContext.getContext().add(WebContext.class.getName(), webContext);
	}
	
	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Integer recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	
	public Boolean getCanAddNote(){
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		Boolean canAddNote = (Boolean)viewMap.get("canAddNote");
		if(canAddNote==null)
			canAddNote = false;
		return canAddNote;
	}
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	public boolean getIsArabicLocale() {
		return  !getIsEnglishLocale();
	}

	public boolean getIsEnglishLocale() {

		
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return   localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
	}
}
